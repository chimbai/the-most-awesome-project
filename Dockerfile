FROM alpine
COPY ./src/datanet_libraries/lib/_tomcat /usr/local/tomcat/webapps/lib/_tomcat
COPY ./src/datanet_libraries/lib/acegi-security /usr/local/tomcat/webapps/lib/acegi-security
COPY ./src/datanet_libraries/lib/activation /usr/local/tomcat/webapps/lib/activation
COPY ./src/datanet_libraries/lib/antlr /usr/local/tomcat/webapps/lib/antlr
COPY ./src/datanet_libraries/lib/aopalliance /usr/local/tomcat/webapps/lib/aopalliance
COPY ./src/datanet_libraries/lib/asm /usr/local/tomcat/webapps/lib/asm
COPY ./src/datanet_libraries/lib/avalon /usr/local/tomcat/webapps/lib/avalon
COPY ./src/datanet_libraries/lib/axiom /usr/local/tomcat/webapps/lib/axiom
COPY ./src/datanet_libraries/lib/axis2 /usr/local/tomcat/webapps/lib/axis2
COPY ./src/datanet_libraries/lib/backport /usr/local/tomcat/webapps/lib/backport
COPY ./src/datanet_libraries/lib/batik /usr/local/tomcat/webapps/lib/batik
COPY ./src/datanet_libraries/lib/bcmail /usr/local/tomcat/webapps/lib/bcmail
COPY ./src/datanet_libraries/lib/bcprov /usr/local/tomcat/webapps/lib/bcprov
COPY ./src/datanet_libraries/lib/bsh /usr/local/tomcat/webapps/lib/bsh
COPY ./src/datanet_libraries/lib/byte /usr/local/tomcat/webapps/lib/byte
COPY ./src/datanet_libraries/lib/c3p0 /usr/local/tomcat/webapps/lib/c3p0
COPY ./src/datanet_libraries/lib/cglib /usr/local/tomcat/webapps/lib/cglib
COPY ./src/datanet_libraries/lib/chartengineapi /usr/local/tomcat/webapps/lib/chartengineapi
COPY ./src/datanet_libraries/lib/chartitemapi /usr/local/tomcat/webapps/lib/chartitemapi
COPY ./src/datanet_libraries/lib/classmate /usr/local/tomcat/webapps/lib/classmate
COPY ./src/datanet_libraries/lib/com /usr/local/tomcat/webapps/lib/com
COPY ./src/datanet_libraries/lib/commons /usr/local/tomcat/webapps/lib/commons
COPY ./src/datanet_libraries/lib/coreapi /usr/local/tomcat/webapps/lib/coreapi
COPY ./src/datanet_libraries/lib/crosstabcoreapi /usr/local/tomcat/webapps/lib/crosstabcoreapi
COPY ./src/datanet_libraries/lib/dataadapterapi /usr/local/tomcat/webapps/lib/dataadapterapi
COPY ./src/datanet_libraries/lib/dataaggregationapi /usr/local/tomcat/webapps/lib/dataadapterapi
COPY ./src/datanet_libraries/lib/dataextraction /usr/local/tomcat/webapps/lib/dataextraction
COPY ./src/datanet_libraries/lib/dom4j /usr/local/tomcat/webapps/lib/dom4j
COPY ./src/datanet_libraries/lib/dteapi /usr/local/tomcat/webapps/lib/dteapi
COPY ./src/datanet_libraries/lib/ehcache /usr/local/tomcat/webapps/lib/dteapi
COPY ./src/datanet_libraries/lib/emitterconfig /usr/local/tomcat/webapps/lib/emitterconfig
COPY ./src/datanet_libraries/lib/engineapi /usr/local/tomcat/webapps/lib/engineapi
COPY ./src/datanet_libraries/lib/ezmorph /usr/local/tomcat/webapps/lib/ezmorph
COPY ./src/datanet_libraries/lib/filterbuilder /usr/local/tomcat/webapps/lib/filterbuilder
COPY ./src/datanet_libraries/lib/flex /usr/local/tomcat/webapps/lib/flex
COPY ./src/datanet_libraries/lib/fluent /usr/local/tomcat/webapps/lib/fluent
COPY ./src/datanet_libraries/lib/flute /usr/local/tomcat/webapps/lib/flute
COPY ./src/datanet_libraries/lib/fontbox /usr/local/tomcat/webapps/lib/fontbox
COPY ./src/datanet_libraries/lib/fop /usr/local/tomcat/webapps/lib/fop
COPY ./src/datanet_libraries/lib/freemarker /usr/local/tomcat/webapps/lib/freemarker
COPY ./src/datanet_libraries/lib/gson /usr/local/tomcat/webapps/lib/gson
COPY ./src/datanet_libraries/lib/hibernate /usr/local/tomcat/webapps/lib/hibernate
COPY ./src/datanet_libraries/lib/htmlcleaner /usr/local/tomcat/webapps/lib/htmlcleaner
COPY ./src/datanet_libraries/lib/htmllexer /usr/local/tomcat/webapps/lib/htmllexer
COPY ./src/datanet_libraries/lib/htmlparser /usr/local/tomcat/webapps/lib/htmlparser
COPY ./src/datanet_libraries/lib/httpclient /usr/local/tomcat/webapps/lib/httpclient
COPY ./src/datanet_libraries/lib/httpcore /usr/local/tomcat/webapps/lib/httpcore
COPY ./src/datanet_libraries/lib/httpmime /usr/local/tomcat/webapps/lib/httpmime
COPY ./src/datanet_libraries/lib/hyperjaxb3 /usr/local/tomcat/webapps/lib/hyperjaxb3
COPY ./src/datanet_libraries/lib/ids /usr/local/tomcat/webapps/lib/ids
COPY ./src/datanet_libraries/lib/influxdb /usr/local/tomcat/webapps/lib/influxdb
COPY ./src/datanet_libraries/lib/itext /usr/local/tomcat/webapps/lib/itext
COPY ./src/datanet_libraries/lib/jackson /usr/local/tomcat/webapps/lib/jackson
COPY ./src/datanet_libraries/lib/jakarta /usr/local/tomcat/webapps/lib/jakarta
COPY ./src/datanet_libraries/lib/jasperreports /usr/local/tomcat/webapps/lib/jasperreports
COPY ./src/datanet_libraries/lib/javamelody /usr/local/tomcat/webapps/lib/javamelody
COPY ./src/datanet_libraries/lib/javassist /usr/local/tomcat/webapps/lib/javassist
COPY ./src/datanet_libraries/lib/javax /usr/local/tomcat/webapps/lib/javax
COPY ./src/datanet_libraries/lib/jaxb /usr/local/tomcat/webapps/lib/jaxb
COPY ./src/datanet_libraries/lib/jaxb1 /usr/local/tomcat/webapps/lib/jaxb1
COPY ./src/datanet_libraries/lib/jaxen /usr/local/tomcat/webapps/lib/jaxen
COPY ./src/datanet_libraries/lib/jboss /usr/local/tomcat/webapps/lib/jboss
COPY ./src/datanet_libraries/lib/jcifs /usr/local/tomcat/webapps/lib/jcifs
COPY ./src/datanet_libraries/lib/jcommon /usr/local/tomcat/webapps/lib/jcommon
COPY ./src/datanet_libraries/lib/jdom /usr/local/tomcat/webapps/lib/jdom
COPY ./src/datanet_libraries/lib/jfreechart /usr/local/tomcat/webapps/lib/jfreechart
COPY ./src/datanet_libraries/lib/joda /usr/local/tomcat/webapps/lib/joda
COPY ./src/datanet_libraries/lib/jrobin /usr/local/tomcat/webapps/lib/jrobin
COPY ./src/datanet_libraries/lib/js /usr/local/tomcat/webapps/lib/js
COPY ./src/datanet_libraries/lib/json /usr/local/tomcat/webapps/lib/json
COPY ./src/datanet_libraries/lib/jsr /usr/local/tomcat/webapps/lib/jsr
COPY ./src/datanet_libraries/lib/jstl /usr/local/tomcat/webapps/lib/jstl
COPY ./src/datanet_libraries/lib/jta /usr/local/tomcat/webapps/lib/jta
COPY ./src/datanet_libraries/lib/jwks /usr/local/tomcat/webapps/lib/jwks
COPY ./src/datanet_libraries/lib/jwt /usr/local/tomcat/webapps/lib/jwt
COPY ./src/datanet_libraries/lib/kaptcha /usr/local/tomcat/webapps/lib/kaptcha
COPY ./src/datanet_libraries/lib/log4j /usr/local/tomcat/webapps/lib/log4j
COPY ./src/datanet_libraries/lib/mail /usr/local/tomcat/webapps/lib/mail
COPY ./src/datanet_libraries/lib/mariadb /usr/local/tomcat/webapps/lib/mariadb
COPY ./src/datanet_libraries/lib/mchange /usr/local/tomcat/webapps/lib/mchange
COPY ./src/datanet_libraries/lib/modelapi /usr/local/tomcat/webapps/lib/modelapi
COPY ./src/datanet_libraries/lib/modelodaapi /usr/local/tomcat/webapps/lib/modelodaapi
COPY ./src/datanet_libraries/lib/moshi /usr/local/tomcat/webapps/lib/moshi
COPY ./src/datanet_libraries/lib/mssql /usr/local/tomcat/webapps/lib/mssql
COPY ./src/datanet_libraries/lib/mysql /usr/local/tomcat/webapps/lib/mysql
COPY ./src/datanet_libraries/lib/neethi /usr/local/tomcat/webapps/lib/neethi
COPY ./src/datanet_libraries/lib/notyetcommonssl /usr/local/tomcat/webapps/lib/notyetcommonssl
COPY ./src/datanet_libraries/lib/odadesignapi /usr/local/tomcat/webapps/lib/odadesignapi
COPY ./src/datanet_libraries/lib/ok /usr/local/tomcat/webapps/lib/ok
COPY ./src/datanet_libraries/lib/ooxml /usr/local/tomcat/webapps/lib/ooxml
COPY ./src/datanet_libraries/lib/opensaml /usr/local/tomcat/webapps/lib/opensaml
COPY ./src/datanet_libraries/lib/openxml4j /usr/local/tomcat/webapps/lib/openxml4j
COPY ./src/datanet_libraries/lib/org /usr/local/tomcat/webapps/lib/org
COPY ./src/datanet_libraries/lib/owasp /usr/local/tomcat/webapps/lib/owasp
COPY ./src/datanet_libraries/lib/pdfbox /usr/local/tomcat/webapps/lib/pdfbox
COPY ./src/datanet_libraries/lib/protection /usr/local/tomcat/webapps/lib/protection
COPY ./src/datanet_libraries/lib/quartz /usr/local/tomcat/webapps/lib/quartz
COPY ./src/datanet_libraries/lib/retrofit /usr/local/tomcat/webapps/lib/retrofit
COPY ./src/datanet_libraries/lib/sax2 /usr/local/tomcat/webapps/lib/sax2
COPY ./src/datanet_libraries/lib/scriptapi /usr/local/tomcat/webapps/lib/scriptapi
COPY ./src/datanet_libraries/lib/serializer /usr/local/tomcat/webapps/lib/serializer
COPY ./src/datanet_libraries/lib/sitemesh /usr/local/tomcat/webapps/lib/sitemesh
COPY ./src/datanet_libraries/lib/slf4j /usr/local/tomcat/webapps/lib/slf4j
COPY ./src/datanet_libraries/lib/spring /usr/local/tomcat/webapps/lib/spring
COPY ./src/datanet_libraries/lib/sqljdbc /usr/local/tomcat/webapps/lib/sqljdbc
COPY ./src/datanet_libraries/lib/standard /usr/local/tomcat/webapps/lib/standard
COPY ./src/datanet_libraries/lib/stax /usr/local/tomcat/webapps/lib/stax
COPY ./src/datanet_libraries/lib/thumbelina /usr/local/tomcat/webapps/lib/thumbelina
COPY ./src/datanet_libraries/lib/tm-extractors /usr/local/tomcat/webapps/lib/tm-extractors
COPY ./src/datanet_libraries/lib/velocity /usr/local/tomcat/webapps/lib/velocity
COPY ./src/datanet_libraries/lib/wsdl4j /usr/local/tomcat/webapps/lib/wsdl4j
COPY ./src/datanet_libraries/lib/wss4j /usr/local/tomcat/webapps/lib/wss4j
COPY ./src/datanet_libraries/lib/xalan /usr/local/tomcat/webapps/lib/xalan
COPY ./src/datanet_libraries/lib/xerces /usr/local/tomcat/webapps/lib/xerces
COPY ./src/datanet_libraries/lib/xml /usr/local/tomcat/webapps/lib/xml
COPY ./src/datanet_libraries/lib/xmlbeans /usr/local/tomcat/webapps/lib/xmlbeans
COPY ./src/datanet_libraries/lib/xmlgraphics /usr/local/tomcat/webapps/lib/xmlgraphics
COPY ./src/datanet_libraries/lib/xmlschema /usr/local/tomcat/webapps/lib/xmlschema
COPY ./src/datanet_libraries/lib/xmlsecurity /usr/local/tomcat/webapps/lib/xmlsecurity
COPY ./src/datanet_libraries/lib/xpp3 /usr/local/tomcat/webapps/lib/xpp3
COPY ./src/datanet_libraries/lib/xstream /usr/local/tomcat/webapps/lib/xstream
COPY ./src/datanet_libraries/lib/zxing /usr/local/tomcat/webapps/lib/zxing
COPY ./src/datanet_libraries/lib/_datanet/d3 /usr/local/tomcat/webapps/lib/_datanet/d3
COPY ./src/datanet_libraries/lib/_datanet/modunet /usr/local/tomcat/webapps/lib/_datanet/modunet

COPY ./src/datanet_libraries/lib/_datanet/tournet /usr/local/tomcat/webapps/lib/_datanet/tournet
COPY ./src/d2 /usr/local/tomcat/apps/d2
COPY ./src/datanet_libraries/lib/_datanet/d2 /usr/local/tomcat/webapps/lib/_datanet/d2

