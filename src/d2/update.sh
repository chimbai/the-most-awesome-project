#!/bin/bash

## -- make sure user is "tomcat"
current_user=$(whoami)
if [[ "$current_user" != "tomcat" ]]; then
	echo
	echo "[!] Please use \"tomcat\" user to run this script."
	echo
	echo "Script terminated, bye."
	echo
	exit
fi

## -- variables for the script
datanetlibraries_path="/opt/ids/apps/datanet_libraries"
user_libraries="WEB-INF/config/datanet_libraries.userlibraries"
env_ini_file="WEB-INF/config/env.ini"
update_ini_file="WEB-INF/config/update.ini"
mordems_url="http://app.vpn.idsdatanet.com/mordems"
checksum_file="sha256sum.txt"
gitlab_token="XV4Bjaf-dxJUcTUZE6dG"
showmessage="false"
max_download_retry=5

## -- get the OS major version number
os_version="$( cat /etc/centos-release | tr -dc '0-9.' | cut -d \. -f1 )"

## -- get the directory locations
work_dir=${PWD}
site_name=${work_dir##*/}

## -- check for tomcat info
[[ $work_dir =~ ([0-9]{4}) ]]
tomcat_port="${BASH_REMATCH[1]}"
tomcat_version="8"
tomcat_service="tomcat8_$tomcat_port"
if [[ "$work_dir" == *"tomcat7"* ]]; then
	tomcat_version="7"
	tomcat_service="tomcat7_$tomcat_port"
fi


## -- check for env.ini
deployment_name=""
if [ ! -f "$env_ini_file" ]; then
	echo
	echo "[!] Failed to locate the \"$env_ini_file\" file."
	echo
	echo "Please check and make sure the site is properly setup."
	echo
	exit
else
	deployment_found=0
	while read -r line; do
		line="$(echo -e "${line}" | tr -d '[:space:]')"
		if [[ $line == \config.postfix* ]]; then
			deployment_name=${line#"config.postfix="}
			deployment_found=$((deployment_found+1))
		fi
	done < "$env_ini_file"
	if [ $deployment_found -lt 1 ]; then
		echo
		echo "[!] The \"$env_ini_file\" file is not configured properly."
		echo
		echo "Please check and make sure the site is properly setup."
		echo
		exit
	fi
fi

## -- intro
echo
echo "****************************************"
echo
echo "SITE UPDATE SCRIPT"
echo "Ver. 20221017-r1"
echo
echo "Site: $site_name"
if [[ "$deployment_name" != "" ]]; then
	echo "Deployment: $deployment_name"
fi
echo "Tomcat: version $tomcat_version"
echo "Port: $tomcat_port"
echo
echo "****************************************"
echo

## -- get user
echo "Please provide your LDAP credentials..."
echo
while true; do
	read -p "User ID: " user_id
	case $user_id in
		"" )
			echo "[!] Enter your User ID."
			echo
			;;
		* )
			break
			;;
	esac
done

## -- get the branch name
site_branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

## -- make sure branch is "master"
if [[ "$site_branch" != "master" ]]; then
	curl -s "$mordems_url/api.php?task=site-tracker&user=$user_id&name=$deployment_name&site=$site_name&branch=$site_branch&host=$HOSTNAME&action=update&status=failed&notes=Wrong%20branch%3A%20$site_branch."
	echo
	echo "This script is only for \"master\" branch."
	echo "[!] Current branch: $site_branch"
	echo
	echo "Script terminated, bye."
	echo
	exit
fi

## -- update the project
unset SSH_ASKPASS
git_project_name=$( git remote -v | head -n1 | awk '{print $2}' | sed -e 's,.*:\(.*/\)\?,,' -e 's/\.git$//' )
git remote set-url origin http://$user_id@git.vpn.idsdatanet.com/clients/$git_project_name.git
git pull
if [ $? -eq 0 ]; then
	chown -R tomcat: *
	chmod -R 775 *
	echo
else
	curl -s "$mordems_url/api.php?task=site-tracker&user=$user_id&name=$deployment_name&site=$site_name&branch=$site_branch&host=$HOSTNAME&action=update&status=failed&notes=Git%20pull%20failed."
	echo
	echo "[!] Failed to perform git pull!"
	echo
	echo "Script terminated, bye."
	echo
	exit
fi

## -- show git status
git status
echo

## -- check for update ID
update_id=""
if [ ! -e "$update_ini_file" ]; then
	echo "[!] Failed to locate the \"update.ini\" file."
	echo
else
	while read -r line; do
		line="$(echo -e "${line}" | tr -d '[:space:]')"
		if [[ $line == \update-id* ]]; then
			update_id=${line#"update-id="}
		fi
	done < "$update_ini_file"
fi

deployment_type="town"
default_key="t"
deployment_option="[n] for NBB or [x] to exit: "
if [[ "$deployment_name" != *"town"* ]]; then
	deployment_type="NBB"
	default_key="n"
	deployment_option="[t] for town or [x] to exit: "
fi

echo "Detected deployment type: $deployment_type"
echo
while true; do
	read -p "Press [ENTER] to confirm, $deployment_option" tnx
	tnx=${tnx:-$default_key}
	case $tnx in
		[tT] | [tT][oO][wW][nN] )
			deployment_type="town"
			break
			;;
		[nN] | [n|N][b|B][b|B] )
			deployment_type="NBB"
			break
			;;
		[xX] | [e|E][x|X][i|I][t|T] )
			echo
			echo "Script terminated, bye."
			echo
			exit
			break
			;;
		* )
			echo "[!] Invalid answer. Try again..."
			echo
			;;
	esac
done
echo
echo "Selected deployment type: $deployment_type"
echo

## -- check if is "town", proceed if it is NOT
if [[ "$deployment_type" != "town" ]]; then

	## -- check for "datanet_libraries"
	datanetlibraries_path="/opt/ids/apps/datanet_libraries"
	if [ -d "$datanetlibraries_path" ]; then
		if [ -d "$datanetlibraries_path/.git" ]; then
			rm -rf $datanetlibraries_path/.git
			showmessage="true"
		fi
	else
		echo "DataNet Libraries:"
		mkdir $datanetlibraries_path
		if [ ! -d "$datanetlibraries_path" ]; then
			echo "[!] Failed to create folder for DataNet Libraries!"
			echo
			echo "Script terminated, bye."
			echo
			exit
		fi
		echo "[ ] $datanetlibraries_path"
		echo
	fi
	
	## -- function for url encode
	urlencode() {
		old_lc_collate=$LC_COLLATE
		LC_COLLATE=C
		local length="${#1}"
		for (( i = 0; i < length; i++ )); do
			local c="${1:$i:1}"
			case $c in
				[a-zA-Z0-9.~_-]) printf '%s' "$c" ;;
				*) printf '%%%02X' "'$c" ;;
			esac
		done
		LC_COLLATE=$old_lc_collate
	}
	
	## -- getting jar files list from "userlibraries"
	if [ -e "$user_libraries" ]; then
		download_count=0
		download_error=0
		echo "Updating the DataNet Libraries:"
		while read -r line; do
			if [[ $line == \<archive* ]]; then
				jar_path=${line#"<archive path=\"/datanet_libraries/"}
				jar_path=${jar_path%"\"/>"}
				jar_encoded=$( urlencode "$jar_path" )
				jar_destination="$datanetlibraries_path/$jar_path"
				## -- if not found, download the jar file
				if [ ! -f "$jar_destination" ]; then
					download_count=$((download_count+1))
					echo -n "... $jar_path "
					## -- get the checksum for the jar file
					if [ -e "$checksum_file" ]; then
						rm -f "$checksum_file"
					fi
					url="http://git.vpn.idsdatanet.com/api/v4/projects/8/repository/files/$jar_encoded?ref=master"
					result=""
					result=$( curl -s --header "PRIVATE-TOKEN: $gitlab_token" "$url" 2>&1 )
					if [[ "$result" == *"404 File Not Found"* ]]; then
						## -- file not found in remote git repository
						echo
						echo
						echo "[!] The file above not in remote Git repository."
						echo
						echo "Please check on the file, or contact Git administrator."
						echo
						exit
					else
						## -- create checksum file
						echo $result | grep -Po '"content_sha256": *\K"[^"]*"' | tr -d '"' | { tr '\n' ' ' && echo " $jar_destination" ; } > "$checksum_file"
						skipped="false"
						if [ -f "$jar_destination" ]; then
							## -- check local file's checksum
							sha256sum --status -c "$checksum_file"
							if [ $? -ne 0 ]; then
								rm -f "$jar_destination"
							else
								skipped="true"
							fi
						fi
						## -- start download
						retry_count=0
						while : ; do
							retry_count=$((retry_count+1))
							## -- download the jar file
							url="http://git.vpn.idsdatanet.com/api/v4/projects/8/repository/files/$jar_encoded/raw?ref=master"
							curl --header "PRIVATE-TOKEN: $gitlab_token" "$url" -s -o $jar_destination --create-dirs
							if [ -f "$jar_destination" ]; then
								## -- checksum check for the downloaded jar file
								sha256sum --status -c "$checksum_file"
								if [ $? -eq 0 ]; then
									## -- download ok, next
									echo -e "\e[2K\r[ ] $jar_path\r"
									break
								else
									if [ -f "$jar_destination" ]; then
										rm -f "$jar_destination"
									fi
								fi
							else
								echo -e "\e[2K\r[!] $jar_path\r"
								download_error=$((download_error+1))
								break
							fi
							## -- download failed after max retry, 
							if [ $retry_count -ge $max_download_retry ]; then
								echo -e "\e[2K\r[!] $jar_path\r"
								download_error=$((download_error+1))
								break
							fi
						done
					fi
					if [ -e "$checksum_file" ]; then
						rm -f "$checksum_file"
					fi
				fi
			fi
		done < "$user_libraries"
		## -- prompt if jar file(s) not downloaded
		if [ $download_error -gt 0 ]; then
			curl -s "$mordems_url/api.php?task=site-tracker&user=$user_id&name=$deployment_name&site=$site_name&branch=$site_branch&host=$HOSTNAME&action=update&status=failed&notes=Failed%20to%20download%20some%20JAR%20files."
			echo
			echo "[!] Some files are not downloaded!"
			echo
			echo "Script terminated, bye."
			echo
			exit
		else
			if [ $download_count -gt 0 ]; then
				echo
				echo "Total file downloaded = $download_count"
				echo
			else
				echo "[ ] No new file found."
				echo
			fi
		fi
	else
		curl -s "$mordems_url/api.php?task=site-tracker&user=$user_id&name=$deployment_name&site=$site_name&branch=$site_branch&host=$HOSTNAME&action=update&status=failed&notes=Failed%20to%20locate%userlibraries%20file."
		echo "Unable to locate the \"userlibraries\" file:"
		echo "-> $user_libraries"
		echo
		echo "Script terminated, bye."
		echo
		exit
	fi

fi

## -- prompt for tomcat restart
while true; do
	read -p "Restart Tomcat for port \"$tomcat_port\"? [y/n] " yn
	case $yn in
		[yY] | [yY][Ee][Ss] )
			echo
			echo "Restarting Tomcat, please wait..."
			if [ "$os_version" -gt 6 ]; then
				sudo systemctl restart $tomcat_service
			else
				echo
				sudo /sbin/service $tomcat_service restart
				echo
			fi
			sleep 5
			tomcat_status=$( netstat -lnp 2>/dev/null | grep $tomcat_port )
			if [ -z "$tomcat_status" ]; then
				echo "[!] Failed to restart tomcat"
			else
				echo "[ ] Tomcat re-started"
			fi
			echo
			break
			;;
		[nN] | [n|N][O|o] )
			echo "[!] Tomcat is not restarted!"
			echo
			break
			;;
		* )
			echo "[!] Please answer \"y\" or \"n\"."
			echo
			;;
	esac
done

## -- show message
if [ $showmessage == "true" ]; then
	echo "*****************************************************************"
	echo
	echo "IMPORTANT"
	echo 
	echo "Please remember to notify Network Team to disable the cronjob"
	echo "for updating the \"datanet_libraries\" on this host."
	echo
	echo "*****************************************************************"
	echo
fi

## -- the end
if [ -z $update_id ]; then
	curl -s "$mordems_url/api.php?task=site-tracker&user=$user_id&name=$deployment_name&site=$site_name&branch=$site_branch&host=$HOSTNAME&action=update&status=updated&notes=Updated."
	echo "The site has been updated."
else
	curl -s "$mordems_url/api.php?task=site-tracker&user=$user_id&name=$deployment_name&site=$site_name&branch=$site_branch&host=$HOSTNAME&action=update&status=updated&update-id=$update_id&notes=Updated%20to%20$update_id."
	echo "The site has been updated to \"$update_id\"."
fi

echo
echo "Bye."
echo
echo
