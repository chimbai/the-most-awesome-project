(function(window){
	D3.inherits(JrsReportController,D3.Events);
	
	function StateModel(){
		this._canUndo = false;
		this._canRedo = false;
		this._totalPages = 0;
		this._reportConfig = null;
	}
	
	StateModel.prototype.setUndo = function(value){
		this._canUndo = value;
	}
	
	StateModel.prototype.setRedo = function(value){
		this._canRedo = value;
	}
	
	StateModel.prototype.setTotalPages = function(value){
		this._totalPages = value;
	}
	
	StateModel.prototype.getUndo = function(){
		return this._canUndo;
	}
	
	StateModel.prototype.getRedo = function(){
		return this._canRedo;
	}
	
	StateModel.prototype.getTotalPages = function(){
		return this._totalPages;
	}
	
	StateModel.prototype.setReportConfig = function(value){
		this._reportConfig = value;
	}
	
	StateModel.prototype.getReportConfig = function(){
		return this._reportConfig;
	}
	
	D3.StateModel = StateModel;
	
	function DrillDownStateStack(){
		this._previous = null;
		this._current = null;
		this._stateStack = [];
		this.id = Math.random();
	}
	
	DrillDownStateStack.prototype.addState = function(stateModel){
		this._stateStack.push(stateModel);
	}
	
	DrillDownStateStack.prototype.removePrevState= function(){
		this._stateStack.pop();
	}
	
	DrillDownStateStack.prototype.getPrevState = function(){
		return this._stateStack[this._stateStack.length - 1];
	}
	
	DrillDownStateStack.prototype.hasPreviousState = function(){
		return this._stateStack.length >= 1;
	}
	
	DrillDownStateStack.prototype.getStateStack = function(){
		return this._stateStack;
	}
	
	DrillDownStateStack.prototype.clearStack = function(){
		this._stateStack.splice(0, this._stateStack.length);
	}
	
	D3.DrillDownStateStack = DrillDownStateStack;
	
	function JrsReportController(parentEl, jrsClient, reportConfig, config){
		if(!jrsClient){
			return;
		}
		this._reportConfig = this.parseReportConfig(reportConfig);
		this._configs = config;
		this._jrsClient = jrsClient;
		this._parentEl = parentEl;
		this._el = $("<div></div>");
		this._stateModel = new D3.StateModel();
		this._drillDownStack = new D3.DrillDownStateStack();
		this._jrsReportInstance;
		this._id = Math.random();
		
		if(this._configs.hasOwnProperty("jrsReportContainerClass")){
			this._el.addClass(this._configs.jrsReportContainerClass);
		}
		
		this._el.attr('id', reportConfig.containerId);
		this._parentEl.append(this._el);
	}
	
	JrsReportController.prototype.setStateModel = function(stateModel){
		this._stateModel = stateModel;
	}
	
	JrsReportController.prototype.getId = function(config){
		return this._id;
	}
	
	JrsReportController.prototype.setReportConfig = function(config){
		this._reportConfig = config;
	}
	
	JrsReportController.prototype.getDrillDownStack = function(){
		return this._drillDownStack;
	}
	
	JrsReportController.prototype.hasDrillDownHistory = function(){
		return this._drillDownStack.hasPreviousState();
	}
	
	JrsReportController.prototype.resetState = function(){
		
		if(this._drillDownStack.hasPreviousState()){
			var states = this._drillDownStack.getStateStack();
			this._reportConfig = states[0].getReportConfig();
			this._stateModel = new D3.StateModel();
			this._stateModel.setTotalPages(states[0].getTotalPages());
			this.renderReport();
			this._drillDownStack.clearStack();
		}else{
			var currentState = new D3.StateModel();
			currentState.setTotalPages(this._stateModel.getTotalPages());
			this._stateModel = currentState;
			this.renderReport();
		}
	}
	
	JrsReportController.prototype.drillUp = function(){
		this._reportConfig = this._drillDownStack.getPrevState().getReportConfig();
		this._stateModel = new D3.StateModel();
		//this.renderReport();
		this._drillDownStack.removePrevState();
	} 
	
	JrsReportController.prototype.getStateModel = function(){
		return this._stateModel;
	}
	
	JrsReportController.prototype.renderReport = function(){
		this._jrsReportInstance = this._jrsClient.report(this._reportConfig);
	}
	
	JrsReportController.prototype.reportParams = function(params){
		if(params){
			var rptParams = $.extend(true, {}, this._reportConfig.params);
			$.each(params, function(idx, val){
				rptParams[idx] = val;
			});
			this._reportConfig.params = rptParams;
			this._jrsReportInstance.params(params);
		}
	}
	
	JrsReportController.prototype.getReportElement = function(){
		return this._el;
	}
	
	JrsReportController.prototype.isActiveReport = function(){
		return this._el.parents('.js-active-jrsreport').length >= 1;
	}
	
	JrsReportController.prototype.parseReportConfig = function(config){
		var self = this;
		var prop = {};
		prop.resource = config.reportUri;
		prop.params = config.filterParams;
		prop.container = "#" + config.containerId;
		prop.scale = config.scale || "container";
		prop.events = {
				reportCompleted: function(){
					self.onJRSReportCompletedHandler(this);
				},
				
				canUndo: function(canUndo){
					self.onReportCanUndoHandler(canUndo, this);
				},
				
				canRedo: function(canRedo){
					self.onReportCanRedoHandler(canRedo, this);
				},
				
				changeTotalPages: function(totalPages){
					self.onChangeTotalPagesHandler(totalPages, this);
				},
				
				beforeRender: function(el){
					// find all spans
					$(el).find(".jrPage").each(function(i, e){
							$(e).css("background-color","").attr("data-my-attr", "test");
						});
				}
				
		};
		prop.linkOptions = {
				beforeRender: function (linkToElemPairs) {
					linkToElemPairs.forEach(function(pair){
						if(pair.element){
							pair.element.style.cursor = "pointer";
						}
					});
				}
		};
		
		prop.error = function(error) {
			self.onJRSReportFailedHandler(this, error);
		};
		
		if(config.hasOwnProperty("ddLinkOptions")){
			prop.linkOptions.events = {
				click: function(e, link){
					if(link.type == "ReportExecution"){
						//console.log("link object ==> ", link);
						self.onClickDrillDownHandler.call(self, e, link, config.ddLinkOptions);
					}
				}
			};	
		}

		return prop;
	}
	
	JrsReportController.prototype.onClickDrillDownHandler = function(e, link, ddLinkOptions){
		for(var i in ddLinkOptions){
			var currentOption = ddLinkOptions[i];
			var currentProp = $.extend(true, {}, this._reportConfig);
			
			if (currentOption.containerId) {
				if(currentOption.reportUri == link.resource){
					this.dispatchEvent(D3.JasperViewerEvent.getOnBeforeRunReportEvent());
					this._stateModel.setReportConfig(this._reportConfig);
					
					if(currentOption.resetState) {
						this.resetState();
					} else if(this._drillDownStack.getPrevState().getReportConfig().resource == this._reportConfig.resource) {
						this._drillDownStack.removePrevState();
					} else {
						if(currentOption.resetState) {
							this.resetState();
						} else {
							this._drillDownStack.addState(this._stateModel);
							this._stateModel = new D3.StateModel();
						}											
					}
					
					
					var currentProp = $.extend(true, {}, this._reportConfig);
					for(var j in currentOption.params){
						currentProp.params[currentOption.params[j]] = [link.parameters[currentOption.params[j]]];
					}
					if(link.parameters._report) {
						currentProp.resource = link.parameters._report;
					}else {
						currentProp.resource = currentProp.resource;
					}
					currentProp.container = currentOption.containerId;
					this._reportConfig = currentProp;
					this.renderReport();

					if(this._configs.hasOwnProperty("afterDrillDownCallback")){
						this._configs.afterDrillDownCallback.call(this);
					}	
					
				}
			} else {
				
				if(currentOption.reportUri == link.resource){
					this.dispatchEvent(D3.JasperViewerEvent.getOnBeforeRunReportEvent());
					this._stateModel.setReportConfig(this._reportConfig);
					this._drillDownStack.addState(this._stateModel);
					this._stateModel = new D3.StateModel();
					
					var currentProp = $.extend(true, {}, this._reportConfig);
					for(var j in currentOption.params){
						currentProp.params[currentOption.params[j]] = [link.parameters[currentOption.params[j]]];
					}
					if(link.parameters._report) {
						currentProp.resource = link.parameters._report;
					}else {
						currentProp.resource = currentProp.resource;
					}
					
					this._reportConfig = currentProp;
					this.renderReport();

					if(this._configs.hasOwnProperty("afterDrillDownCallback")){
						this._configs.afterDrillDownCallback.call(this);
					}	
					
				}
			}
			
			
		}
	}
	
	JrsReportController.prototype.onChangeActiveTab = function activaTab(tab, currentTab){
		$('.nav-pills a[href="#' + tab + '"]').removeClass('disabled');  
		$('.nav-pills a[href="#' + tab + '"]').tab('show');
		  
		  
		  if (currentTab == 'hseRegion' || currentTab =='hseCat') {
			  $('#tu_region1').empty();
			  $('#tu_region2').empty();
			  $('#tu_rig1').empty();
			  $('#tu_rig2').empty();
			  $('#tu_dt_detail').empty();
		  } else if (currentTab == 'tuRegion' || currentTab =='tuDt') {
			  $('#hse_region1').empty();
			  $('#hse_region2').empty();
			  $('#hse_rig1').empty();
			  $('#hse_rig2').empty();
			  $('#hse_detail').empty();
		  }
	};
	
	
	
	JrsReportController.prototype.onChangeTotalPagesHandler = function(totalPages, reportInstance){	
		this._stateModel._totalPages = totalPages;
		this.dispatchEvent(D3.JasperViewerEvent.getOnChangeTotalPagesEvent());
	}
	
	JrsReportController.prototype.onJRSReportFailedHandler = function(reportInstance, error){
		//this.addErrorMessage(error);
		this.dispatchEvent(D3.JasperViewerEvent.getOnReportFailedEvent(this, error));
	}
	
	JrsReportController.prototype.onJRSReportCompletedHandler = function(reportInstance){
		this.dispatchEvent(D3.JasperViewerEvent.getOnReportCompletedEvent(this));
		if(this._configs.hasOwnProperty("jrsReportCompleted")){
			this._configs.jrsReportCompleted.call(this);
		}
	}
	
	JrsReportController.prototype.onReportCanUndoHandler = function(canUndo, reportInstance){
		this._stateModel._canUndo = (canUndo ? canUndo : false);
		this.dispatchEvent(D3.JasperViewerEvent.getOnReportCanUndoEvent());
	}
	
	JrsReportController.prototype.onReportCanRedoHandler = function(canRedo, reportInstance){
		this._stateModel._canRedo = (canRedo ? canRedo: false);
		this.dispatchEvent(D3.JasperViewerEvent.getOnReportCanRedoEvent());
	}
	
	JrsReportController.prototype.getJrsReportInstance = function(){
		return this._jrsReportInstance;
	}
	
	JrsReportController.prototype.addErrorMessage = function(error) {
		var me = this;
		var errorContainer = $(".ui-rpt-refreshbtn-wrap");
		//console.log("error", error);
		if(errorContainer.length > 0){
			$(".ids-rpt-error-msg").text(error);
		}else{
			var wrapper = $("<div class='ui-rpt-refreshbtn-wrap'></div>");
			var button = $("<button class='DefaultButton'>Refresh</button>").on("click", function(){
				me._jrsReportInstance.refresh().fail(function(error){
					me.addErrorMessage( error);
				});
			});
			wrapper.append(button);
			$(me._jrsReportInstance.container()).prepend("<div class ='ids-rpt-error-msg'>" + error + "</div>");
			$(me._jrsReportInstance.container()).prepend(wrapper);
		}
	}
	
	
	D3.JrsReportController = JrsReportController;
	
}(window))