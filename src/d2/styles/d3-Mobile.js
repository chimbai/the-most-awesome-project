(function(window) {
	
	function Popup()
	{
		
	};
	
	Popup.prototype.hidePopup = function(){
		var t = new Date().getTime() - this.popupStartTime;
		if(t < 500){
			var self = this;
			setTimeout(function(){self._closePopup();}, 500 - t);
		}else{
			this._closePopup();
		}
	};
	
	Popup.prototype._closePopup = function(){
		if(this.$contentPopup && this.$contentPopup.dialog("isOpen")){
			this.$contentPopup.dialog("destroy");
			this.$contentPopup = null;
		}
	};
	
	Popup.prototype.showPopup = function(title, msg, showCloseButton){
		this._closePopup();
		if(! this.popupContentDiv){
			this.popupContentDiv = document.createElement("div");
			this.popupContentDiv.style.padding = "15px 10px";	
		}
		while(this.popupContentDiv.hasChildNodes()) this.popupContentDiv.removeChild(this.popupContentDiv.lastChild);
		this.popupContentDiv.appendChild(document.createTextNode(msg));
		this.$contentPopup = $(this.popupContentDiv).dialog({title: title, minHeight: 10, resizable: false, closeOnEscape: false, modal:true});
		if(! showCloseButton){
			$(".ui-dialog-titlebar-close", this.$contentPopup.dialog("widget")).hide();
		}
		this.popupStartTime = new Date();
	};
	
	var msgPopup = new Popup();
	
	function showPopup(title, msg){
		msgPopup.showPopup(title, msg);
	}
	
	function hidePopup(){
		msgPopup.hidePopup();
	}
	
	// ==============
	//  TABS MANAGER
	// ==============
	function Tab(data) {
		this.isFrameSet = false;
		this.data = data;
		this.id=this.data.id;
		this.view = document.createElement("li");
		var a = document.createElement("a");
		a.style.cursor = "pointer";
		a.appendChild(document.createTextNode(data.title));
		a.onclick = this.onClick.bindAsEventListener(this);
		this.view.appendChild(a);
		
		this.content = document.createElement("div");
		this.content.style.width = "100%";
		this.content.setAttribute("class", "tabContent");
	}
	Tab.prototype.iframeLoaded = function() {
		containerManager.loadingState = "completed";
		hidePopup();
	};
	Tab.prototype.showContent = function() {
		$(this.view).addClass("active");
		this.content.style.display = "block";
		if (!this.isFrameSet) {
			showPopup("Please wait", "Loading content");
			this.isFrameSet = true;
			var iframe = document.createElement("iframe");
			iframe.src = this.data.iframeUrl;
			iframe.frameBorder = 0;
			iframe.style.padding = 0;
			iframe.style.border = 0;
			iframe.style.margin = 0;
			iframe.style.height = "100%";
			iframe.style.width = "100%";
			if (D3.browser.msie && D3.browser.version < 9) {
				iframe.attachEvent('onload',this.iframeLoaded.bindAsEventListener(this));
			} else {
				iframe.onload = this.iframeLoaded.bindAsEventListener(this);
			}
			this.iframeElement =  iframe;
			this.content.appendChild(iframe);
		}
	};
	Tab.prototype.onClick = function(e) {
		var previousTab = tabsManager.getSelectedTab();
		if (this.reloadOnClick){
			this.reload();
		}
		tabsManager.clearSelection();
		this.selected = true;
		this.showContent();
		if (e){
		if (previousTab){
			if (previousTab.tabToReloadOnExitEnter){
				if (previousTab.reloadConfiguredTabOnEnter(this)){
					this.reload();
				}
			}
		}
		}
	};
	Tab.prototype.onLoad = function(e) {
		tabsManager.clearSelection();
		this.selected = true;
		if (this.reloadOnClick){
			this.reload();
		}
		this.showContent();	
	};
	
	Tab.prototype.reload = function(){
		if (this.iframeElement)
		this.iframeElement.src = this.data.iframeUrl;

	};
	
	Tab.prototype.reloadConfiguredTabOnEnter = function(clickedTab){
		if(this.tabToReloadOnExitEnter){
			var split = this.tabToReloadOnExitEnter.split(",");
			
			if (split.indexOf(clickedTab.id) != -1)
			{	
				return true;
			}
		}
		return false;
	}
	
	
	var tabsManager = {
		showAccessDenied: function(){
			this.init(null,true);
		},
		getSelectedTab : function(){
			for (var i=0;i<this.collections.length;i++){
				var tab = this.collections[i];
				if (tab.selected)return tab;
			}
			return null;
		},
		select: function(id){
			var selected = null;
			for (var i=0;i<this.collections.length;i++){
				var tab = this.collections[i];
				if (tab.id==id){
					selected = tab;
					break;
				}
			}
			if (selected)selected.onLoad();
		},
		
		init: function (data, accessDenied) {
			var selectedTab = null;
			this.collections = [];
			if(! this.parent) this.parent = document.getElementById("mainTabsContainer"); 
			var parent = this.parent;
			if (!this.tabs) {
				this.tabs = document.createElement("ul");
				this.tabs.className = "tabs";
				parent.appendChild(this.tabs);
			} else {
				$(this.tabs).empty();
			}
			if (!this.tabContents) {
				this.tabContents = document.createElement("div");
				this.tabContents.className = "tabsContentContainer";
				parent.appendChild(this.tabContents);
			} else {
				$(this.tabContents).empty();
			}
			if(accessDenied){
				var div = document.createElement("div");
				div.className = "access-denied";
				div.appendChild(document.createTextNode("Access Denied"));
				this.tabContents.appendChild(div);
				this.tabs.style.display="none";
			}else{
				var i=0, dataTabs=data.tabs, length=dataTabs.length;
				this.tabs.style.display=length>1?"block":"none";
				for (; i<length; i++) {
					var item = dataTabs[i];
					var tab = new Tab({id:item.id,title: item.title, iframeUrl: item.url});
					tab.tabToReloadOnExitEnter = "";
					tab.reloadOnClick = false;
					if (item.tabToReloadOnExitEnter) tab.tabToReloadOnExitEnter = item.tabToReloadOnExitEnter;
					if (item.reload=="1") tab.reloadOnClick = true;
					if (item.selected == "1") {
						selectedTab = tab;
					}
					if (length>1)
					{
						this.tabs.appendChild(tab.view);
					}
					this.tabContents.appendChild(tab.content);
					this.collections.push(tab);
				}
			}
			if(!this.resizeHandlerAttached){
				this.resizeHandlerAttached = true;
				var self = this;
				containerManager.$window.resize(function() {self.updateContentsHeight();});
			}
			if (selectedTab){
				selectedTab.onClick();
			}else{
				containerManager.loadingState = "completed";
			}
		},
		
		appendTabIdToUrl: function(url, tabId) {
			var tabIdParam = "tabId=" + tabId;
			if (url.indexOf("?") == -1) {
				url += "?" + tabIdParam;
			} else {
				url += "&" + tabIdParam;
			}
			return url;
		},
		
		clearSelection: function() {
			if (this.collections) {
				for (var i=0; i<this.collections.length; i++) {
					var tab = this.collections[i];
					$(tab.view).removeClass("active");
					tab.selected = null;
					tab.content.style.display = "none";
				}
			}
		},
		
		updateContentsHeight: function() {
			if (this.collections) {
				if(!this.mainContainerDiv) this.mainContainerDiv = document.getElementById("mainContainer");
				if(!this.mainCenterPane) this.mainCenterPane = document.getElementById("mainCenterPane");
				if(! this.topContentPane) this.topContentPane = document.getElementById("topContentPane");
				//var contentHeight = (this.mainContainerDiv.clientHeight - this.topContentPane.offsetHeight - this.mainCenterPane.offsetHeight + this.tabContents.clientHeight) + "px";
				var contentHeight = this.mainContainerDiv.clientHeight + "px";
				for (var i=0; i<this.collections.length; i++) {
					var tab = this.collections[i];
					tab.content.style.height = contentHeight;
				}
			}
		},
		
		hideTabLoading: function(id) {
			hidePopup();
		}
	};
	window.tabsManager = tabsManager;
	
	// ============================
	//  OPERATION SELECTOR MANAGER
	// ============================
	var d2ScreenManager = {
		reloadParentHtmlPage: function(clearQueryString, params){
			var url = containerManager.currentUrl;
			if(clearQueryString){
				url = url.replace(/\?.*$/, "");
			}
			if(params != undefined && params != ""){
				url = url + (url.indexOf("?") == -1 ? "?" : "&") + params;
			}
			window.location = url;
		},
		
		submitSummary: function(id) {
			containerManager.submitDay(id);
		},
		gotoUrl:function(url,msg){
			var actualUrl = url;
			var popup = new Popup();
			popup.showPopup("Redirect",msg,false);
			window.location.href = url;	
		},
		
		_popup_msg_box: false,
	
		showSimpleModalMsg: function(title, msg){
			d2ScreenManager._popup_msg_box = jBox.open('inline-jBoxID','inline','<br/><center>' + msg + '</center>',title,'center=true,width=400,height=50,model=true');
		},
		
		hideSimpleModalMsg: function(){
			if(! d2ScreenManager._popup_msg_box) return;
			jBox.close(d2ScreenManager._popup_msg_box);
			d2ScreenManager._popup_msg_box = false;
		},
		
		toggleLogout: function() {
			D3.UIManager.confirm(D3.LanguageManager.getLabel("logout.title"), D3.LanguageManager.getLabel("logout.message"), this.logout, null, this);
		},
		
		logout: function(){
			window.location.href = containerManager.contextPath+"/logout.jsp";
		},
		
		submitRefresh: function() {
			$.post(containerManager.currentUrl, {_sysRefresh:1}, function(response) {
				window.location.href = containerManager.currentUrl;
			});
		}
	};
	window.d2ScreenManager = d2ScreenManager;
	
	
	// ============================
	//  OPERATION SELECTOR MANAGER
	// ============================
	var operationSelector = {
		dialog: undefined,
		loadingPopup: undefined,
		searchKeyUpTimer: undefined,
		searchInput: undefined,
		campaignUid: undefined,
		loaded: false,
		 
	    show:function() {
	    	var wWidth = window.innerWidth;
	    	if(wWidth>460) wWidth = 460;
	    	if(this.dialog === undefined){
	    		$(this.getElement("operationSelector_campaignRow")).css("display", ((containerManager.hasCampaign === true) ? "table-row" : "none"));
	    		$(this.getElement("operationSelector_operationTypeRow")).css("display", (containerManager.showWellOpTypeFilter ? "table-row" : "none"));
	    		this.dialog = $("#operationSelectorDialog").dialog({closeOnEscape:false, resizable:false,draggable:false,autoOpen:false, width: wWidth, minHeight: 10, modal: true});
	    	}
	    	this.dialog.dialog("open");
	    	this.getSearchInput().blur();
	    	if(!this.loaded){
	    		this.loaded = true;
	    		this.campaignUid = this.ifblank(containerManager.currentCampaignUid, "--all_well--");
	    		this.rigInformationUid = this.ifblank(containerManager.currentRigUid, "--all_well--");
	    		this.operationType = "";
	    		this.loadFilter();
	    		this.loadWellList();
	    	}
	    },
	    
	    getElement: function(id){
	    	var e = null;
	    	if(this.elements){
	    		e = this.elements[id];
	    		if(e) return e;
	    	}else{
	    		this.elements = {};
	    	}
	    	e = document.getElementById(id);
	    	this.elements[id] = e;
	    	return e;
	    },
	    
	    ifblank:function(value,replace){
	    	if(value == null || value == undefined || value == "") return replace;
	    	return value;
	    },
	    
	    createOption: function(label, value, isSelected, dropdown) {
	    	var option = document.createElement("option");
			option.value = value;
			option.selected = isSelected;
			option.appendChild(document.createTextNode(label));
			dropdown.append(option);
	    },
	    
	    loadFilter:function() {
	    	var params = {};
	    	if(containerManager.hasCampaign) params.loadCampaign = true;
	    	if(containerManager.showWellOpTypeFilter) params.loadOperationType = true;
	    	if(params.length <= 0) return;
	    	params.loadFilter = true;
	    	if(containerManager.loadRig) params.loadRig = true;
	    	var self = this;
	    	$.post(containerManager.contextPath + "/ajaxmenu.html", params, function(response) {
	    		if(params.loadCampaign){
		    		var dropdown = $(self.getElement("operationSelector_campaign"));
		    		dropdown.empty();
		    		self.createOption("...", "--all_well--", false, dropdown);
	        		$.each(response.campaigns, function(i, item){
	    				self.createOption(item.name, item.id, item.selected, dropdown);
	    			});
	        		if(dropdown.selectedIndex == -1){
	        			dropdown.selectedIndex = 0;
	        		}
	    		}
	    		if(params.loadOperationType){
		    		var dropdown = $(self.getElement("operationSelector_operationType"));
		    		dropdown.empty();
		    		self.createOption("...", "", false, dropdown);
	        		$.each(response.operationTypes, function(i, item){
	    				self.createOption(item.name, item.id, item.selected, dropdown);
	    			});
	        		if(dropdown.selectedIndex == -1){
	        			dropdown.selectedIndex = 0;
	        		}
	    		}
	    		if(params.loadRig){
		    		var dropdown = $(self.getElement("operationSelector_rig"));
		    		dropdown.empty();
		    		self.createOption("...", "", false, dropdown);
	        		$.each(response.Rigs, function(i, item){
	    				self.createOption(item.name, item.id, item.selected, dropdown);
	    			});
	        		if(dropdown.selectedIndex == -1){
	        			dropdown.selectedIndex = 0;
	        		}
	    		}
	    	}, "json").fail(function() {
	    		self.hideLoadingPopup();
                alert("Error occurred while loading data for Well/Ops Selector");
	    	});
	    },

	    RigSelected:function(dropdown){
	    	if(dropdown.selectedIndex != -1){
	    		this.rigInformationUid = (dropdown.options[dropdown.selectedIndex]).getAttribute("value");
	    		this.loadWellList();
	    	}
	    },
	    
	    operationTypeSelected:function(dropdown){
	    	if(dropdown.selectedIndex != -1){
	    		this.operationType = (dropdown.options[dropdown.selectedIndex]).getAttribute("value");
	    		this.loadWellList();
	    	}
	    },
	    
	    campaignSelected:function(dropdown){
	    	if(dropdown.selectedIndex != -1){
	    		this.campaignUid = (dropdown.options[dropdown.selectedIndex]).getAttribute("value");
	    		this.loadWellList();
	    	}
	    },
	    
	    loadWellList:function(){
	    	if (this.rigInformationUid != "" && this.rigInformationUid != "--all_well--" && this.campaignUid == "--all_well--"){
	    		this.load(undefined, "operationSelector_well", 'wellByRigUid', this.rigInformationUid);
	    	} else {
	    		this.load(undefined, "operationSelector_well", 'well', this.campaignUid);
	    	}
	    },
	    
	    hide:function(){
	    	this.dialog.dialog("close");
	    },
	    
	    doNotHidePopup: false,
	    
	    load:function(sourceDropdown, targetDropdownId, requestType, dataId){
	    	var targetDropdown = $(this.getElement(targetDropdownId));
	    	if(dataId === undefined){
	    		if(sourceDropdown.selectedIndex == -1){
	    			targetDropdown.empty();
	    			this.triggerDropdownOnChange(targetDropdown);
	    			return;
	    		}else{
	    			dataId = (sourceDropdown.options[sourceDropdown.selectedIndex]).getAttribute("value");
	    		}
	    	}
	    	this.showLoadingPopup();
	    	this.doNotHidePopup = true;
	    	targetDropdown.empty();
	    	this.triggerDropdownOnChange(targetDropdown);
	    	this.doNotHidePopup = false;
	    	var self = this;
	        $.post(containerManager.contextPath + "/wellOperationMenuAjax.html", {requestType:requestType, id:dataId, campaignUid:this.campaignUid, operationCode: this.operationType, rigInformationUid:this.rigInformationUid}, function(response) {
	            	self.getElement("operationSelector_msg").innerHTML = response.message;
	            	self.populateDropdown(targetDropdown, response);
	        }, "json").fail(function() {
	        	self.hideLoadingPopup();
	            alert("Error occurred while loading data for Well/Ops Selector");
	        });
	    },
	    
	    populateDropdown:function(dropdown, response) {
	    	var self = this;
	    	dropdown.empty();
	        $.each(response.data.dataList, function(i, item){
	        	self.createOption(item.value, item.key, (item.key == response.selectedKey), dropdown);
	    	});
	    	if(dropdown.length == 1){
	    		dropdown.selectedIndex = 0;
	    	}
	    	this.triggerDropdownOnChange(dropdown);
	    },
	    
	    triggerDropdownOnChange:function(dropdown) {
			dropdown.change();
			if(!this.doNotHidePopup){
				this.hideLoadingPopup();
			}
	    },
	    
	    showLoadingPopup:function(){
	    	if(this.loadingPopup === undefined) {
	    		this.loadingPopup = $("#operationSelector_loadingPopup");
	    		this.loadingPopup.css("top", (this.dialog.height() - this.loadingPopup.height()) / 2);
				this.loadingPopup.css("left", (this.dialog.width() - this.loadingPopup.width()) / 2);
			}
			this.loadingPopup.css("visibility", "visible");
	    },
	    
	    hideLoadingPopup:function(){
	    	this.loadingPopup.css("visibility", "hidden");
	    },
	    
	    getSelectedValue:function(id){
	    	var dropdown = this.getElement(id);
	    	if(dropdown.selectedIndex == -1){
	    		return null;
	    	}else{
	    		return dropdown.options[dropdown.selectedIndex].value;
	    	}
	    },
	    
	    submit:function(){
	    	var wellUid = this.getSelectedValue("operationSelector_well");
	    	var wellboreUid = this.getSelectedValue("operationSelector_wellbore");
	    	var operationUid = this.getSelectedValue("operationSelector_operation");
	    	var separator = "--separator--";
	    	
	    	if(wellUid != null && wellboreUid != null && operationUid != null){
	    		containerManager.submitMenu({_sys_menu_Operation: wellUid + separator + wellboreUid + separator + operationUid});
	    		this.hide();
	    	}
	    },
	    
	    submitCampaign:function(){
	    	var dropdown = this.getElement("operationSelector_campaign");
	    	if(dropdown.selectedIndex != -1){
	    		var value = (dropdown.options[dropdown.selectedIndex]).getAttribute("value");
	    		if(value != "--all_well--" && value != ""){
	        		containerManager.submitMenu({_sys_menu_Operation: value});
	    			this.hide();
				}        		
	    	}
	    },
	    
	    searchInputOnFocus: function(source) {
	    	var attrInitValueCleared = source.getAttribute("initValueCleared");
	    	if (attrInitValueCleared === null) {
	    		source.setAttribute("initValueCleared", 1);
	    		source.value = "";
	    	}
	    },
	    
	    searchTextKeyUp:function(event) {
	    	var self = operationSelector;
			if(self.searchKeyUpTimer !== undefined) {
				clearTimeout(self.searchKeyUpTimer);
			}
	
	    	if(event.keyCode == 13){
	    		self.doSearch();
	    	}else{
				self.searchKeyUpTimer = setTimeout("operationSelector.doSearch()",1000);
	    	}
	    },
	
		getSearchInput:function(){
		    if(this.searchInput == undefined){
	        	this.searchInput = $("#operationSelector_search");
	        }
	        return this.searchInput;
	    },
			        
	    doSearch:function(){
	        this.searchKeyUpTimer = undefined;
	        
	        var value = this.getSearchInput().val();
	        if(value == ""){
	        	this.loadWellList();
	        }else{
	        	this.load(undefined, "operationSelector_well", 'wellByWellName', value);
	        }
	    }
	}
	window.operationSelector = operationSelector;
	
	// ==============
	//  MENU MANAGER
	// ==============
	Menu.TOP_MENU_CLASS = "topMenuItem";
	Menu.TOP_MENU_SELECT_CLASS = "topMenuItemSelect";
	Menu.TOP_MENU_SELECTED_CLASS = "topMenuItemSelected";
	
	Menu.SUB_MENU_CLASS = "subMenuItem";
	Menu.SUB_MENU_SELECT_CLASS = "subMenuItemSelect";
	Menu.SUB_MENU_SELECTED_CLASS = "subMenuItemSelected";
	
	function Menu(view, topLevel, parent) {
		this.view = view;
	    this.isTopLevel = topLevel;
	    this.parent = parent;
	    this.traverseItem = null;
	    this.selectedItem = null;
	    this.hardSelectedItem = null;
	    this.isMouseIn = false;
	}
	Menu.prototype.addChild = function(view, topLevel) {
	    var $view = $(view);
	    var menu = new Menu(view, topLevel, this);
	    if (this.child == null) this.child = [];
	    this.child.push(menu);
	    return menu;
	};
	Menu.prototype.mouseIn = function() {
		this.isMouseIn = true;
	};
	Menu.prototype.mouseOut = function() {
	    this.isMouseIn = false;
	};
	Menu.prototype.isMouseActive = function() {
		return this.isMouseIn;
	};
	Menu.prototype.setTraverseItem = function(item) {
		this.traverseItem = item;
	};
	Menu.prototype.setTraverseItemToSelectedItem = function() {
		this.selectedItem = this.traverseItem;
		this.traverseItem = null;
		if (this.parent != null) {
			this.parent.setTraverseItemToSelectedItem();
		}
	};
	Menu.prototype.forceHardSelect = function() {
		this.setTraverseItemToSelectedItem();
		this.hardSelectMenuItemAndItsParent();
	};
	Menu.prototype.deselectSubMenuItem = function(deselectHardSelected) {
		if (this.selectedItem != null) {
			var classname = this.isTopLevel? Menu.TOP_MENU_CLASS : Menu.SUB_MENU_CLASS;
			this.selectedItem.className = classname;
		}
		if (deselectHardSelected) {
			if (deselectHardSelected && this.hardSelectedItem != null) {
				this.hardSelectedItem.className = classname;
				this.hardSelectedItem = null;
			}
		} else {
			if (this.isCurrentMenuItemHardSelected()) {
				this.hardSelectMenuItem();
			}
		}
	};
	Menu.prototype.clearMenuState = function() {
		this.deselectSubMenuItem(true);
		if (this.hasChild()) {
			for (var i=0; i<this.child.length; i++) {
	            var childMenu = this.child[i];
	            childMenu.clearMenuState();
			}
		}
	};
	Menu.prototype.setSelectedSubMenuItem = function(menuItem) {
		this.deselectSubMenuItem(false);
		this.selectedItem = menuItem;
		this.selectedItem.className = this.isTopLevel? Menu.TOP_MENU_SELECT_CLASS : Menu.SUB_MENU_SELECT_CLASS;
	};
	Menu.prototype.removeSelectedChild = function() {
		this.deselectSubMenuItem(false);
	};
	Menu.prototype.hasSelectedChild = function() {
		return (this.selectedItem != null);
	};
	Menu.prototype.isCurrentMenuItemHardSelected = function() {
		return (this.selectedItem != null && this.hardSelectedItem != null) && this.selectedItem.isEqualNode(this.hardSelectedItem);
	};
	Menu.prototype.hardSelectMenuItem = function() {
		this.hardSelectedItem = this.selectedItem;
		if (this.hardSelectedItem) {
			this.hardSelectedItem.className = this.isTopLevel? Menu.TOP_MENU_SELECTED_CLASS : Menu.SUB_MENU_SELECTED_CLASS;
		}
	};
	Menu.prototype.hardSelectMenuItemAndItsParent = function() {
		this.hardSelectMenuItem();
		if (this.isTopLevel) {
			menuManager.setCurrentSelectedTopMenu(this);
		}
		if (this.parent != null) {
			this.parent.hardSelectMenuItemAndItsParent();
		}
	};
	Menu.prototype.hasChild = function() {
	    if (this.child && this.child.length > 0) {
	        return true;
	    }
	    return false;
	};
	Menu.prototype.hideMenu = function() {
	    if (this.view) {
	        if (!this.isTopLevel) this.view.style.visibility = "hidden";
	    }
	    this.removeSelectedChild();
	};
	Menu.prototype.showMenu = function() {
	    if (this.view) {
	    	this.view.style.backgroundColor = "#F6F6F6";
	        this.view.style.position = "fixed";
	        this.view.style.visibility = "visible";
	    }
	};
	Menu.prototype.hide = function(force) {
		if (!force && this.isMouseActive()) return;
	    if (this.hasChild()) {
	        for (var i=0; i<this.child.length; i++) {
	            var childMenu = this.child[i];
	            if (!force && childMenu.isMouseActive()) {
	                return;
	            } else {
	                childMenu.hideMenu();
	            }
	        }
	    }
        this.hideMenu();
        if (this.parent != null) {
            this.parent.hide(force);
        }
	};
	
	var menuManager = {
	    menuIndexCounter: 0,
	    selectedTopMenu : null,
	    menuArray : [],
	    
	    setCurrentSelectedTopMenu: function(menuObj) {
	    	this.selectedTopMenu = menuObj;
	    },
	    
	    clearMenuArray: function() {
	        for (var i = this.menuArray.length; i > 0; i--) {
	            if(this.menuArray[i-1]) {
    	            this.menuArray[i-1].mouseOut();
    	            this.menuArray[i-1].hideMenu();
	            }
	        }
	        this.menuArray = [];
	    },
	    
	    clearMenuState: function() {
	    	if (this.selectedTopMenu != null) {
	    		this.selectedTopMenu.clearMenuState();
	    		this.selectedTopMenu = null;
	    	}
	    	if (window.containerManager.menuOpenOnClick == 1) {
	    	    this.clearMenuArray();
	    	}
	    },
	    
	    createMenuItem: function(data, item, parent, topLevel, hasChild, onClickFunc,onClickContext, menuObj) {
	        var td = document.createElement("td");
	        var a = document.createElement("a");
	        var layer = 0;
	        if (!topLevel && hasChild) {
	        	td.style.background = "url('images/arrowRight.png') no-repeat right center";
	            a.innerHTML = item.label;
	        } else {
	            a.appendChild(document.createTextNode(item.label));
	        }
	        if (topLevel) {
	        	td.style.whiteSpace = "nowrap";
	        	if (item.colorCode) {
	        		td.style.borderBottom = "5px solid " + item.colorCode;
	        	}
			}
	        td.style.cursor = "pointer";
	        td.onclick = onClickFunc.bindAsEventListener(onClickContext?onClickContext:this, item, hasChild, menuObj);
	        td.appendChild(a);
	        return td;
	    },
	    
	    selectMenuItem: function(menuObj, add) {
	        var selectedLayer = menuObj.layer;
	        var maxLayer = this.menuArray.length;
	        this.menuArray = this.menuArray.reduce( function (acc, i) {
	            if (i.layer >= selectedLayer) {
	                i.mouseOut();
	                i.hideMenu();
	                return acc;
	            } else {
	                acc.push(i);
	                return acc;
	            }
	        }, []);
	        if (add) {
	            this.menuArray[selectedLayer] = menuObj;
	        }
	    },
	    
	    createMenu: function(menuData, data, parent, topLevel, parentMenu, onClickFunc, onClickContext, parentTop, parentMenuItem) {
	        var self = this;
	        var div = document.createElement("div");
	        var tbody = document.createElement("tbody");
	        var table = document.createElement("table");
	        table.cellPadding = 0;
	        table.cellSpacing = 0;
	        table.appendChild(tbody);
	        div.appendChild(table);
	        
	        if (!topLevel) {
	            div.id = "menu" + this.menuIndexCounter++;
	            if (parentTop) {
	            	div.className = "submenu parentTop";
	            	parentTop = false;
	            } else {
	            	div.className = "submenu";
	            }
	            	
	            table.width = "100%";
	        } else {
	            div.className = "topMenu";
	            var tr = document.createElement("tr");
	            tbody.appendChild(tr);
	            parentTop = true;
	        }
	        parent.appendChild(div);
	        var menuObj = parentMenu.addChild(div, topLevel);
	        if(parentMenu.layer != undefined && parentMenu != null) {
	            menuObj.layer = parentMenu.layer + 1;
	        } else {
	            menuObj.layer = 0;
	        }
	        
	        // If menuOpenOnClick GWP is set to true (Open on Click)
			if (window.containerManager.menuOpenOnClick == 1) {
    	        $(div).click(function() {
                    if (!menuObj.isMouseActive()) {
    	                menuObj.mouseIn();
    	            } else {
    	                setTimeout(function(){ menuObj.hide(); }, 1);
    	            }
                });
    	        
    	        $(div).hover(function() {
                    menuObj.mouseIn();
                    if(window.menuTimeout != undefined && window.menuTimeout != null) { clearTimeout(window.menuTimeout); }
                }, function() {
                    window.menuTimeout = setTimeout(function(){
                        menuObj.mouseOut();
                        menuObj.hide();
                        menuManager.clearMenuState();
                    }, 1000);
                });
    	        
    	        $.each(data, function(i, item) {
    	            var submenudata = menuData[item.uid];
    	            var hasChild = submenudata !== undefined;
    	            var menuItem = self.createMenuItem(menuData, item, parent, topLevel, hasChild, onClickFunc,onClickContext, menuObj);
    	            menuObj.setTraverseItem(menuItem);
    	            if (topLevel) {
    	                tr.appendChild(menuItem);
    	            } else {
    	            	var topLevelTr = document.createElement("tr");
    	                topLevelTr.appendChild(menuItem);
    	                if (!topLevel && hasChild) {
    	                	menuItem.style.paddingRight = "17px";
    	    	        }
    	                tbody.appendChild(topLevelTr);
    	            }
    	            // hard select menu item and its parent if item.url matches the current url path.
    	            if (containerManager.isPathEqualsToLink(item.url)) {
    	            	menuObj.forceHardSelect();
    	            }
    	            if (hasChild) {
    	                var childMenuObj = self.createMenu(menuData, submenudata, parent, false, menuObj, onClickFunc,onClickContext,parentTop, menuItem);
    	                $(menuItem).hover(function() {
    	                    menuObj.setSelectedSubMenuItem(menuItem);
    	                }, function() {});
    	                
    	                $(menuItem).click(function() {
                            var $this = $(this);
                            
                            if (!childMenuObj.isMouseActive()) {
                                var target = $(childMenuObj.view);
                                var $menuItem = $(menuItem);
                                var origTarget = target.get(0);
                                if (origTarget != null) {
                                    var position = target.offset();
                                    // jquery bug: have to get and set initial position before setting it again
                                    target.offset(position);
                                    if (position != null) {
                                        var left = menuItem.getBoundingClientRect().left;
                                        if (!topLevel) {
                                            left += $menuItem.outerWidth();
                                        }
                                        position.left = left;
                                        // Decide whether menu should show at the left or right side of parent menu.
                                        var constraint = left + target.width();
                                        var calculatedTop = $menuItem.offset().top + (topLevel? $menuItem.outerHeight(false):0);
                                        if (constraint > containerManager.$window.outerWidth()) {
                                            var menuItemOuterWidth =  $menuItem.outerWidth();
                                            target.offset({top:calculatedTop, left:(left - target.width()) + (topLevel? menuItemOuterWidth:-menuItemOuterWidth)});
                                        } else {
                                            target.offset({top:calculatedTop, left:left});
                                        }
                                        
                                        // Show scroll when menu is outside windows bound.
                                        var windowHeight = containerManager.$window.outerHeight();
                                        var totalHeight = target.outerHeight() + calculatedTop;
                                        if (totalHeight > windowHeight) {
                                            var maxHeight = windowHeight - calculatedTop;
                                            target.css("maxHeight", maxHeight);
                                            target.css("overflowY", "auto");
                                        } else {
                                            target.css("maxHeight", '');
                                            target.css("overflowY", '');
                                        }
                                    }
                                }
                                childMenuObj.mouseIn();
                                childMenuObj.showMenu();
                                self.selectMenuItem(childMenuObj, true);
                                menuObj.setSelectedSubMenuItem(menuItem);
                            } else {
                                self.selectMenuItem(childMenuObj);
                                childMenuObj.mouseOut();
                                setTimeout(function(){ childMenuObj.hide(); }, 1);
                            }
                        });
    	            } else {
    	                
    	                $(menuItem).hover(function() {
                            menuObj.setSelectedSubMenuItem(menuItem);
                        }, function() {});
    	                
    	                $(menuItem).click(function() {
    	                    if (!menuObj.isMouseActive()) {
    	                        menuObj.mouseIn();
    	                        menuObj.setSelectedSubMenuItem(menuItem);
    	                    } else {
    	                        menuObj.mouseOut();
    	                    }
                        });
    	            }
    	        });
			} else {
			    $(div).hover(function() {
	                menuObj.mouseIn();
	            }, function() {
	                menuObj.mouseOut();
	                setTimeout(function(){ menuObj.hide(); }, 1);
	            });
	            
	            $.each(data, function(i, item) {
	                var submenudata = menuData[item.uid];
	                var hasChild = submenudata !== undefined;
	                var menuItem = self.createMenuItem(menuData, item, parent, topLevel, hasChild, onClickFunc,onClickContext, menuObj);
	                menuObj.setTraverseItem(menuItem);
	                if (topLevel) {
	                    tr.appendChild(menuItem);
	                } else {
	                    var topLevelTr = document.createElement("tr");
	                    topLevelTr.appendChild(menuItem);
	                    if (!topLevel && hasChild) {
	                        menuItem.style.paddingRight = "17px";
	                    }
	                    tbody.appendChild(topLevelTr);
	                }
	                // hard select menu item and its parent if item.url matches the current url path.
	                if (containerManager.isPathEqualsToLink(item.url)) {
	                    menuObj.forceHardSelect();
	                }
	                if (hasChild) {
	                    var childMenuObj = self.createMenu(menuData, submenudata, parent, false, menuObj, onClickFunc,onClickContext,parentTop, menuItem);
	                    $(menuItem).hover(function() {
	                        var $this = $(this);
	                        var target = $(childMenuObj.view);
	                        var $menuItem = $(menuItem);
	                        var origTarget = target.get(0);
	                        if (origTarget != null) {
	                            var position = target.offset();
	                            // jquery bug: have to get and set initial position before setting it again
	                            target.offset(position);
	                            if (position != null) {
	                                var left = menuItem.getBoundingClientRect().left;
	                                if (!topLevel) {
	                                    left += $menuItem.outerWidth();
	                                }
	                                position.left = left;
	                                // Decide whether menu should show at the left or right side of parent menu.
	                                var constraint = left + target.width();
	                                var calculatedTop = $menuItem.offset().top + (topLevel? $menuItem.outerHeight(false):0);
	                                if (constraint > containerManager.$window.outerWidth()) {
	                                    var menuItemOuterWidth =  $menuItem.outerWidth();
	                                    target.offset({top:calculatedTop, left:(left - target.width()) + (topLevel? menuItemOuterWidth:-menuItemOuterWidth)});
	                                } else {
	                                    target.offset({top:calculatedTop, left:left});
	                                }
	                                
	                                // Show scroll when menu is outside windows bound.
	                                var windowHeight = containerManager.$window.outerHeight();
	                                var totalHeight = target.outerHeight() + calculatedTop;
	                                if (totalHeight > windowHeight) {
	                                    var maxHeight = windowHeight - calculatedTop;
	                                    target.css("maxHeight", maxHeight);
	                                    target.css("overflowY", "auto");
	                                } else {
	                                    target.css("maxHeight", '');
	                                    target.css("overflowY", '');
	                                }
	                            }
	                        }
	                        childMenuObj.mouseIn();
	                        childMenuObj.showMenu();
	                        menuObj.setSelectedSubMenuItem(menuItem);
	                    }, function() {
	                        childMenuObj.mouseOut();
	                        setTimeout(function(){ childMenuObj.hide(); }, 1);
	                    });
	                } else {
	                    $(menuItem).hover(function() {
	                        menuObj.mouseIn();
	                        menuObj.setSelectedSubMenuItem(menuItem);
	                    }, function() {
	                        menuObj.mouseOut();
	                    });
	                }
	            });
			}
	        return menuObj;
	    }
	};
	
	// ===================
	//  CONTAINER MANAGER
	// ===================
	var containerManager = {
		$window: $(window),
		
		urlPath: null,
		menuOpenOnClick: null,
		
		setUrlPath: function() {
			var paths = window.location.pathname.split('/');
			if (paths.length > 2) {
				this.urlPath = paths[2] + window.location.search;
			}
		},
		
		isPathEqualsToLink: function(link) {
			return (this.urlPath != null && (link == this.urlPath));
		},
		
		getElement: function(id){
	    	var e = null;
	    	if(this.elements){
	    		e = this.elements[id];
	    		if(e) return e;
	    	}else{
	    		this.elements = {};
	    	}
	    	e = document.getElementById(id);
	    	this.elements[id] = e;
	    	return e;
	    },
		
		onClickMenu: function(e, data, hasChild, menuObj) {
			if (!menuObj.isTopLevel && !hasChild) {
				menuManager.clearMenuState();
				menuObj.hardSelectMenuItemAndItsParent();
				menuObj.hide(true);
				this.urlPath = data.url;
				this.currentUrl = containerManager.contextPath + "/" + data.url;
				containerManager.showTopStatusBarSelection(false);
				containerManager.loadMainPage(containerManager.contextPath + "/" + data.url, false, data.label);
			}
		},
		
		onClickViewSelector: function(e, data) {
			window.location.href = data.url;
		},
		
		renderMenu: function(response) {
			var menuData = response["menu"];
			this.menuOpenOnClick = response["menuOpenOnClick"];
			if(! this.mainMenuBar) this.mainMenuBar = document.getElementById("mainMenuBar");
        	var $mainMenuBar = $(this.mainMenuBar);
			$mainMenuBar.empty();
			if (this.hasViewSelector) {
				$mainMenuBar.css("marginLeft", "1px");
			}
	        menuManager.createMenu(menuData, menuData["mainmenu"], this.mainMenuBar, true, new Menu(null, true, null), this.onClickMenu);
		},
		
		renderViewSelector: function(response) {
			if(!this.viewSelector) this.viewSelector = document.getElementById("viewSelector");
			var $viewSelector = $(this.viewSelector);
			if ($viewSelector) {
				$viewSelector.empty();
				
				var views = response["viewSelector"];
				this.selectedView = response["selectedView"];
				this.hasViewSelector = (this.selectedView != null);
				var selectedViewItem = views[this.selectedView];
				selectedViewItem.uid = "selectedView";
				delete views[this.selectedView];
				
				menuManager.createMenu({selectedView:views}, [selectedViewItem], this.viewSelector, true, new Menu(null, true, null), this.onClickViewSelector,this);
			}
		},
		
		createOption: function(label, value, isSelected, dropdown) {
			var option = document.createElement("option");
			option.value = value;
			option.selected = isSelected;
			option.appendChild(document.createTextNode(label));
			dropdown.append(option);
		},
		
		showTopStatusBarSelection:function(show){
			//this.updateArrowsDisplay(!show);
			if(this.topStatusBarVisible != undefined && this.topStatusBarVisible != null){
				if(this.topStatusBarVisible == show) return;
			}
			this.topStatusBarVisible = show;
        	$(this.getElement("daysAvailableDropdown")).css("display", show?"inline":"none");
        	$(this.getElement("currentDatumsDropdown")).css("display", show?"inline":"none");
        	$(this.getElement("currentUomTemplatesDropdown")).css("display", show?"inline":"none");
    		$(this.getElement("currentDayLabel")).css("display", show?"none":"inline");
    		$(this.getElement("currentDatumLabel")).css("display", show?"none":"inline");
    		$(this.getElement("currentUomTemplateLabel")).css("display", show?"none":"inline");
    		tabsManager.updateContentsHeight();
        },
        
        updateArrowsDisplay: function(show){
        	var dropdown = $(this.getElement("daysAvailableDropdown"))[0];
        	var lastDayIndex = this.lastDayIndex;
        	if(dropdown.selectedIndex != 0 && dropdown.selectedIndex < lastDayIndex){
				$(this.getElement("nextDay")).css("display", show?"inline":"none");
			} else {
				$(this.getElement("nextDay")).css("display", "none");
			}
			
			if(dropdown.selectedIndex < (lastDayIndex + 1) && dropdown.selectedIndex > 1){
				$(this.getElement("prevDay")).css("display", show?"inline":"none");
			} else {
				$(this.getElement("prevDay")).css("display", "none");
			}
        },

		renderTopStatusBar: function(response) {
			var self = this;
			this.hasCampaign = response["hasCampaign"];
			this.currentCampaignUid = response["currentCampaignUid"];
			this.showWellOpTypeFilter = (response["showWellOpTypeFilter"] == "1");
			
			var currentOpData = response["currentOperation"];
			this.currentOperationValue = currentOpData["selectedValue"];
			var currentOperationLabel = $(this.getElement("currentOperationLabel"));
			currentOperationLabel.empty();
			currentOperationLabel.append(this.currentOperationValue ? currentOpData["shortName"] : "Select Operation");
			
			document.getElementById("currentOperationLabel").onclick = function() {operationSelector.show()};
			
			this.loadRig = response["loadRig"];
			var currentRigData = response["currentRigLabel"];
			this.currentRigUid = response["currentRigUid"];
			var currentRigLabel = $(this.getElement("currentRigLabel"));
			currentRigLabel.empty();
			currentRigLabel.append(currentRigData);
			
			if(this.operationLabelTooltip == undefined) this.operationLabelTooltip = document.getElementById("currentOperationLabel");
			//this.operationLabelTooltip.title = currentOpData["name"] + "<br/>Click to change Well/Operation";
			//$(this.operationLabelTooltip).tipsy({gravity:$.fn.tipsy.autoWE, opacity:1.0, html:true});
			var labelOffset = $(this.operationLabelTooltip).offset();
			
			var completeOpNameSpan = document.createElement("span");
			//when there's no operation "selected"
			var completeOpName;
			if (currentOpData["name"] === "") {
				completeOpName = document.createTextNode(this.operationLabelTooltip.textContent);
			}
			else {
				completeOpName = document.createTextNode(currentOpData["name"]);
			}

			completeOpNameSpan.appendChild(completeOpName);
			completeOpNameSpan.style.display="none";
			/* var div = this.operationLabelTooltip.parentNode;
			if (div) {
				div.appendChild(completeOpNameSpan);
				div.onmouseover = function() {
					completeOpNameSpan.style.display = "inline-block"; 
					completeOpNameSpan.style.zIndex = "100";
					completeOpNameSpan.style.position = "absolute";
					completeOpNameSpan.style.left = labelOffset.left + "px";
					completeOpNameSpan.style.top = labelOffset.top + "px";
					completeOpNameSpan.className = "operationLabelSpan";
				};
				div.onmouseout = function() { completeOpNameSpan.style.display = "none"; };
			}
			
			completeOpNameSpan.onclick = function() {operationSelector.show()}; */
			
			var currentDayData = response["currentDay"];
			var currentDayLabel = $(this.getElement("currentDayLabel"));
			currentDayLabel.empty();
			currentDayLabel.append(currentDayData["selectedValue"]? (currentDayData["label"]===undefined?"Select Day":currentDayData["label"] ):"Select Day");
			
			var daysDropdown = $(this.getElement("daysAvailableDropdown"));
			daysDropdown.empty();
			//if there are no days yet, may be undefined.
			this.createOption("Select Day", "", true, daysDropdown);
			if (currentDayData["days"]) {
				$.each(currentDayData["days"], function(i, item) {
					self.createOption(item.label, item.value, item.selected, daysDropdown);
				});
			}
//			if(daysDropdown.context.options !=null)
//				this.lastDayIndex = daysDropdown.context.options.length - 1;
//			this.updateArrowsDisplay(true);
//			if (response["autoStartNewDay"] !="1"){
//				if(currentDayData["showCreateNewDay"] == "1") this.createOption("[Add New Day]", "createNewDate", false, daysDropdown);
//			}		
			
			var currentDatumData = response["currentDatum"];
			var currentDatumLabel = $(this.getElement("currentDatumLabel"));
			currentDatumLabel.empty();
			currentDatumLabel.append(currentDatumData["selectedLabel"]);
			currentDatumLabel.css("color", currentDatumData["defaultDatumSelected"] == "1"? "":"#cccccc");
			
			var datumsDropdown = $(this.getElement("currentDatumsDropdown"));
			datumsDropdown.empty();
			$.each(currentDatumData["datums"], function(i, item) {
				self.createOption(item.label, item.value, item.selected, datumsDropdown);
			});
			this.currentDatumsDropdownSelectedIndex = datumsDropdown.selectedIndex;

			var currentUomTemplateData = response["currentUomTemplate"];
			var currentUomTemplateLabel = $(this.getElement("currentUomTemplateLabel"));
			currentUomTemplateLabel.empty();
			currentUomTemplateLabel.append(currentUomTemplateData["selectedLabel"]);
			
			var uomTemplatesDropdown = $(this.getElement("currentUomTemplatesDropdown"));
			uomTemplatesDropdown.empty();
			$.each(currentUomTemplateData["uomTemplates"], function(i, item) {
				self.createOption(item.label, item.value, item.selected, uomTemplatesDropdown);
			});
			this.currentUomTemplatesDropdownSelectedIndex = uomTemplatesDropdown.selectedIndex;
		},
		
		collectExtraParamsFromUrl: function(data) {
			if (window.location.search) {
		        var params = window.location.search.slice(1).split("&");
		        for (var i=0; i<params.length; i++) {
		            var tmp = params[i].split("=");
		            var key = tmp[0];
		            data[key] = tmp[1];
		        }
		    }
		},

		loadMenu: function(data, reloadMainPage) {
			var isMobile = false; //initiate as false
			// device detection
			if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
			    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
			    isMobile = true;
			}
			this.loadingState = "loading";
			var self = this;
			this.collectExtraParamsFromUrl(data);
			if (jQuery.isEmptyObject(data)) {
				data.currentUrl = "";
			}
			else {
				data.currentUrl = self.urlPath; 
				if (reloadMainPage) data.reloadMainPage = "true";
			}
			showPopup("Please wait", "Loading menu...");
			$.post(this.contextPath + "/ajaxmenu.html", data, function(response) {
		//		self.setUrlPath();
				
				var runningUrl = response["runningUrl"];
				if (runningUrl==undefined) runningUrl = "";
				if (isMobile) runningUrl = "";
				if (runningUrl!=""){
					containerManager.urlPath = runningUrl;
					containerManager.currentUrl = containerManager.contextPath + "/" + runningUrl;
					containerManager.loadMainPage(containerManager.contextPath + "/" + runningUrl, false, "Operation Information Center");
				}else{
					if(reloadMainPage) self.loadMainPage();
				}
				self.renderViewSelector(response);
				self.renderMenu(response);
				self.renderTopStatusBar(response);
				hidePopup();
			}, "json").fail(function() {
				alert("Error occurred while retrieving menu");
			});
		},

		getLoaderPrefix:function(){
			return "_";
		},
		
		setContextPath: function(path){
			this.contextPath = path;
		},
		setQueryString: function(queryString){
			if (queryString == undefined)
				this.queryString = undefined;
			else{
				this.queryString = D3.Common.convertQueryStringToObject(location.search.substring(1)); 
			}
		},
		setLastDayIndex: function(lastDayIndex){
			this.lastDayIndex = lastDayIndex; 
			
		},
		loadMainPage: function(url, isLoadMenu, title) {
			this.loadingState = "loading";
			if(url) {
				this.currentUrl = url;
				this.urlPath = url.replace(this.contextPath + "/", ""); 
			}
			if (isLoadMenu) {
				var urlSplit = this.urlPath.split(".html");
				var subUrl = "";
				if (urlSplit[1]){
					var urlSp = urlSplit[1].replace("?", "");
					urlSp = urlSp.split("&"); 
					for(i = 0; i < urlSp.length; i++){
						if (!(!(urlSp[i].indexOf("gotoday")) || !(urlSp[i].indexOf("gotowellop")))){
							if (subUrl=="") subUrl = "?" + urlSp[i];
							else subUrl = subUrl + "&" + urlSp[i];
						}
					}
				}
				this.urlPath = urlSplit[0] + ".html" + subUrl;
				this.currentUrl = this.contextPath + "/" + this.urlPath;
				this.loadMenu({}, true);
				return;
			}
			if(history.pushState && url) history.pushState(null, title, url);
			
			var self = this;
			var data = {_d3AjaxContainerLoadTabsInfo: "1", __d3CoreRememberUrl: this.currentUrl};
			this.collectExtraParamsFromUrl(data);
			showPopup("Please wait", "Loading tabs...");
			if (this.currentUrl.indexOf("&webstart=1") != -1){
				window.open(this.currentUrl);
				history.back();
				hidePopup();
			} else {
				$.post(this.currentUrl, data, function(response) {
		        	tabsManager.init(response);
		        	tabsManager.updateContentsHeight();
		        	hidePopup();
		        } , "json").fail(function() {
		        //	alert("Error occured while creating main tabs.");
					alert("Session Time Out.");
		        	window.location.href = "logout.jsp";	
		        });
			}
		},
		
		submitMenu:function(data) {
        	this.loadMenu(data, true);
        	this.showTopStatusBarSelection(false);
        },
        
		submitDay:function(day){
			this.submitMenu({_sys_menu_Daily:day, _sys_menu_Operation: this.currentOperationValue});
		},
        
        onDayChanged:function(source) {
        	if(source.selectedIndex == -1) return;
        	var currentDayLabel = $(this.getElement("currentDayLabel"));
        	currentDayLabel.empty();
        	currentDayLabel.append(source.options[source.selectedIndex].text);
        	
        	var selected = source.options[source.selectedIndex].value;
			if(selected == "createNewDate") {
				this.showTopStatusBarSelection(false);
				this.loadMainPage("reportdaily.html?newRecordInputMode=1");
			}else if(selected == "createNewSiteDate"){
				this.showTopStatusBarSelection(false);
				this.loadMainPage("sitedaily.html?newRecordInputMode=1");
			} else {
				this.submitDay(selected);
			}
		},
		
		datumChanged:function(dropdown){
			var answer = confirm("You are about to change the Datum reference. This change will affect ALL your depth reference. Are you sure?");
			if(answer){
				this.submitMenu({_sys_menu_UOMDatum: dropdown.options[dropdown.selectedIndex].value});
				var currentDatumLabel = $(this.getElement("currentDatumLabel"));
				currentDatumLabel.empty();
				currentDatumLabel.append(dropdown.options[dropdown.selectedIndex].text);
			}else{
				dropdown.selectedIndex = this.currentDatumsDropdownSelectedIndex;
				this.showTopStatusBarSelection(false);
			}
		},
		
		uomTemplateChanged:function(dropdown){
			this.submitMenu({_sys_menu_UOMTemplate: dropdown.options[dropdown.selectedIndex].value});
			var currentUomTemplateLabel = $(this.getElement("currentUomTemplateLabel"));
			currentUomTemplateLabel.empty();
			currentUomTemplateLabel.append(dropdown.options[dropdown.selectedIndex].text);
		},
		
		toggleMainBlockUI: function(isBlocked) {
			var topContentPane = $("#topContentPane");
			this.UIblocked = isBlocked;
			if (isBlocked){
				topContentPane.block({ message: '' });
			}else{
				topContentPane.unblock();
			}
		},
		
		startSessionValidityPing: function(keepAliveIntervalSeconds){
			this.keepAliveIntervalSeconds = keepAliveIntervalSeconds;
			this.scheduleNextSessionValidityPing();
		},
		
		scheduleNextSessionValidityPing: function(interval, extendSession, finalCheck){
			setTimeout(function(){
				window.containerManager.sessionValidityPing(extendSession, finalCheck);
			},interval || this.keepAliveIntervalSeconds * 1000);
		},

		showAutoUpdateCountdownPopup: function(timeout){
			if(!this._autoUpdateCountdownDialog){
				var div = document.createElement("div");
				this.autoUpdateCountdownMessage = div;
				this.autoUpdateCountdownTimeout = timeout;
				this.updateAutoUpdateCountdownMessage();
				var me = this;
				this._autoUpdateCountdownDialog = $(div).dialog({modal:true, title: "Warning: System shutting down for maintenance soon!", resizable: false, width:500, height:130, dialogClass: "no-close-dialog", buttons:[{text:"OK", click:function(){me.closeAutoUpdateCountdownPopup();}}]});
				this.autoUpdateCountdownTimer = setInterval(function(){window.containerManager.autoUpdateCountdownHandler();},1000);
			}
		},
		
		showExtendSessionPingPopup: function(timeout){
			if(!this._extendSessionDialog){
				var div = document.createElement("div");
				div.appendChild(document.createTextNode("You have not changed pages recently... are you still active? For security reasons, this DataNet 2.5 session will end shortly unless you select \"Extend My Session\"."));
				div.appendChild(document.createElement("br"));
				div.appendChild(document.createElement("br"));
				this.countdownMessage = document.createElement("span");
				div.appendChild(this.countdownMessage);
				this.extendSessionTimeout = timeout;
				this.updateExtendSessionPopupMessage();
				var me = this;
				this._extendSessionDialog = $(div).dialog({modal:true, title: "Extend Session", resizable: false, width:600, height:160, dialogClass: "no-close-dialog", buttons:[{text:"Extend My Session", click:function(){me.closeExtendSessionPopup("extendSession");}},{text:"Logout", click:function(){me.closeExtendSessionPopup("logout");}}]});
				this.extendSessionCountdown = setInterval(function(){window.containerManager.extendSessionCountdownHandler();},1000);
			}
		},

		autoUpdateCountdownHandler: function(){
			this.autoUpdateCountdownTimeout -= 1;
			if(this.autoUpdateCountdownTimeout <= 0){
				this.closeAutoUpdateCountdownPopup(true);
			}else{
				this.updateAutoUpdateCountdownMessage();
			}
		},

		extendSessionCountdownHandler: function(){
			this.extendSessionTimeout -= 1;
			if(this.extendSessionTimeout <= 0){
				this.closeExtendSessionPopup("autoClose");
			}else{
				this.updateExtendSessionPopupMessage();
			}
		},
		
		toTime: function(seconds){
			var m = Math.floor(seconds / 60);
			var s = seconds % 60;
			return m + ":" + (s < 10 ? "0" + s : s);
		},
		
		updateAutoUpdateCountdownMessage: function(){
			this.removeChildNodes(this.autoUpdateCountdownMessage);
			this.autoUpdateCountdownMessage.appendChild(document.createTextNode("System will be shutting down for maintenance in " + this.toTime(this.autoUpdateCountdownTimeout) + ", please save your work and logout from the application."));
		},

		updateExtendSessionPopupMessage: function(){
			this.removeChildNodes(this.countdownMessage);
			this.countdownMessage.appendChild(document.createTextNode("Your session will expire in " + this.toTime(this.extendSessionTimeout) + ". Do you wish to extend this session?"));
		},
		
		removeChildNodes: function(node){
			var child;
			while((child = node.firstChild) != null){
				node.removeChild(child);
			}
		},
		
		closeExtendSessionPopup: function(action){
			if(this.extendSessionCountdown){
				clearInterval(this.extendSessionCountdown);
				this.extendSessionCountdown = null;
			}
			this.closePopup("_extendSessionDialog");
			if(action == "autoClose"){
				this.sessionValidityPing(false,true);
			}else if(action == "extendSession"){
				this.sessionValidityPing(true);
			}else if(action == "logout"){
				d2ScreenManager.logout();
			}
		},

		closeAutoUpdateCountdownPopup: function(autoClose){
			if(this.autoUpdateCountdownTimer){
				clearInterval(this.autoUpdateCountdownTimer);
				this.autoUpdateCountdownTimer = null;
			}
			this.closePopup("_autoUpdateCountdownDialog");
			if(autoClose){
				this.sessionValidityPing();
			}else{
				this.scheduleNextSessionValidityPing(60000);
			}
		},

		closePopup: function(name){
			var d = this[name];
			if(d){
				d.dialog("destroy");
				d.remove();
				delete this[name];
			}
		},
		
		sessionValidityPing: function(extendSession, finalCheck){
			var me = this;
			$.ajax({
				url: this.contextPath + "/session_validity_check?v=3&mysid=" + (this.sessionId || "") + (extendSession ? "&extendSession=1" : ""),
				dataType: "json",
				error: function(){
					me.scheduleNextSessionValidityPing(5000, extendSession, finalCheck);	
				},
				success: function(result){
					if(result.invalidSession == "1" || result.autoUpdateStandby == "1"){
						d2ScreenManager.logout();
					}else if(result.autoUpdateShowCountdown){
						me.showAutoUpdateCountdownPopup(result.autoUpdateShowCountdown);
					}else if(result.canTimeout == "1"){
						if(finalCheck){
							d2ScreenManager.logout();
						}else{
							me.showExtendSessionPingPopup(result.showExtendSessionTimeoutSeconds);
						}
					}else{
						if(result.sessionId) me.sessionId = result.sessionId;
						me.scheduleNextSessionValidityPing();
					}
				}
			});
		},
		
		changeDay: function(sourceId, direction){
			if((sourceId != undefined && sourceId != null && sourceId != "") && (direction != undefined && direction != null && direction !="")){
				var dropdown = $(this.getElement(sourceId))[0];
				var selIndex = parseInt(dropdown.selectedIndex);
				var maxIndex = dropdown.options.length - 1;
				if(direction == "forward" && (selIndex + 1) < maxIndex){
					dropdown.selectedIndex = dropdown.selectedIndex + 1;
					this.onDayChanged(dropdown);
				} else if (direction == "backward" && (selIndex - 1) > 0){
					dropdown.selectedIndex = dropdown.selectedIndex - 1;
					this.onDayChanged(dropdown);
				}
			} else return;
        }
	};
	window.containerManager = containerManager;
})(window);