<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
<!--  	<xsl:import href="./ReportTemplate/dvd_graph_record/dvd_graph_record_talisman_uk.xsl" />	-->
    <xsl:import href="./ReportTemplate/dvd_graph_record/dvd_graph_record_type1.xsl" /> 
	
	<xsl:output method="xml" />
	<xsl:preserve-space elements="*" />
	<xsl:template match="/">         
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
					<fo:region-before extent="2.5cm" margin-top="1cm"/>
					<fo:region-after extent="1cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="12pt" padding="10px">
						<fo:table-column column-number="1" column-width="proportional-column-width(88)" />
						<fo:table-column column-number="2" column-width="proportional-column-width(12)" />
						<fo:table-body space-before="10pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="12pt" display-align="center"> 
									<fo:block>
									</fo:block>
								</fo:table-cell>	
								<fo:table-cell font-size="12pt" display-align="center"> 
									<fo:block>
										<fo:external-graphic content-width="scale-to-fit" width="90pt" content-height="40pt" scaling="uniform" src="url(d2://images/client-logo.jpg)" />										
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell number-columns-spanned="2" font-size="6pt" display-align="center">
									<fo:block text-align="center" font-size="12pt" font-weight="bold">
											<fo:inline text-decoration="underline">
												DAYS / COST v DEPTH REPORT #<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportNumber" />
											</fo:inline>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="19cm" table-layout="fixed">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)" />
						<fo:table-column column-number="2" column-width="proportional-column-width(50)" />
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS, 200801119, D2_AWE_DVD
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="8pt">
									<fo:block text-align="right">
										Page
										<fo:page-number />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell font-size="6pt" number-columns-spanned="2">
									<fo:block text-align="left">
										Printed on
										<xsl:variable name="printdate" select="/root/modules/Daily/Daily/dayDate" />
										<xsl:value-of select="d2Utils:getSystemDate('dd MMM yyyy')" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" font-size="8pt">
					<fo:block>
						<fo:table inline-progression-dimension="19cm"
							table-layout="fixed">
							<fo:table-column column-number="1"
								column-width="proportional-column-width(100)" />
							<fo:table-body>
								<!-- DVD Graph -->
								<fo:table-row keep-together="always">
									<fo:table-cell padding-top="2px" padding-bottom="2px">
										<fo:block>
											<xsl:call-template name="dvdgraph"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
													
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

</xsl:stylesheet>
