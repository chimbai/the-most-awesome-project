<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
    
    <xsl:output method="xml" />
    <xsl:preserve-space elements="*" />
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
					<fo:region-before extent="3cm"/>
					<fo:region-after extent="1.5cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
            <fo:page-sequence master-reference="simple" initial-page-number="1">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="4pt">
                        <fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
                        <fo:table-column column-number="2" column-width="proportional-column-width(70)"/>                      
                        <fo:table-body space-before="10pt" space-after="2pt">
                           <fo:table-row>
                               <fo:table-cell font-size="12pt">
                                   <fo:block>                                    
                                       <fo:external-graphic content-width="scale-to-fit"
                                           width="100pt" content-height="50%" scaling="uniform" src="url(d2://images/company_logo.jpg)"/>                                       
                                   </fo:block>
                               </fo:table-cell>                            
                                <fo:table-cell  font-weight="bold"  font-size="12pt" text-align="left">
                                    <fo:block>
                                        Look Ahead for Well <xsl:value-of select="//modules/Well/Well/wellName"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                </fo:static-content>
                <fo:static-content flow-name="xsl-region-after">
                    <fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="12pt">
                        <fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
                        <fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
                        <fo:table-body space-before="10pt" space-after="10pt">
                            <fo:table-row>
                                <fo:table-cell font-size="10pt">
                                    <fo:block text-align="left">
                                        Copyright IDS, 2008, LOOK_AHEAD
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell font-size="8pt">
                                    <fo:block text-align="right">
                                        Page <fo:page-number/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body" font-size="8pt">
                	<fo:block>
                		<fo:table inline-progression-dimension="19cm"
							table-layout="fixed">
							<fo:table-column column-number="1"
								column-width="proportional-column-width(100)" />
							<fo:table-body>	
								
							<xsl:apply-templates select="/root/dayTasks/dayTasksDetails"/>
										
							</fo:table-body>
						</fo:table>   
                    </fo:block>  
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template> 
    
    <!-- <xsl:template name="for.loop"> -->
    <xsl:template match="dayTasks/dayTasksDetails">
	    <fo:table-row keep-together="always">
			<fo:table-cell padding-bottom="2px" padding-top="2px">
				<fo:block>
					<xsl:param name="count" />
					<xsl:variable name="i" select="dayNum"/>           
	        
			        <fo:table inline-progression-dimension="19cm" table-layout="fixed" margin-top="2pt">
			            <fo:table-column column-number="1" column-width="proportional-column-width(1)"/>
			            <fo:table-body>
			                <fo:table-row>  
			                    <fo:table-cell>                       
			                        <fo:block>
			                            <fo:table width="100%" table-layout="fixed" font-size="10pt" font-weight="bold" border="0.5px solid black">
			                                <fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
			                                <fo:table-column column-number="2" column-width="proportional-column-width(85)"/>			                               
			                                <fo:table-body>
			                                    <fo:table-row>
			                                        <fo:table-cell padding="0.1cm" font-size="10pt" font-weight="bold"
			                                        	border-bottom="0.5px solid black" number-columns-spanned="2">
			                                            <fo:block>Day  <xsl:value-of select="$i+1"/> ( <fo:inline><xsl:value-of select="dayTasksDetail/dayDate"/></fo:inline> )</fo:block>
			                                        </fo:table-cell>                                                       
			                                    </fo:table-row>
			                                    <fo:table-row>
			                                        <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black">
			                                            <fo:block>End Time</fo:block>
			                                        </fo:table-cell>                                        
			                                        <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
			                                            <fo:block>Description</fo:block>
			                                        </fo:table-cell>
			                                    </fo:table-row>                                    
			                                    <xsl:apply-templates select="dayTasksDetail"/>
			                                </fo:table-body>
			                            </fo:table>
			                            <fo:table width="100%" table-layout="fixed" font-size="10pt" font-weight="bold" space-after="5pt" border="0.5px solid black" margin-top="2pt">
			                                <fo:table-column column-number="1" column-width="proportional-column-width(85)"/>
			                                <fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
			                                <fo:table-body>
			                                    <fo:table-row>
			                                        <fo:table-cell padding="0.1cm" font-size="10pt" font-weight="bold" border-right="0.5px solid black" border-bottom="0.5px solid black">
			                                            <fo:block>Requirement </fo:block>
			                                        </fo:table-cell>       
			                                        <fo:table-cell padding="0.1cm" font-size="10pt" font-weight="bold" text-align="center" border-bottom="0.5px solid black">
			                                            <fo:block>T/D</fo:block>
			                                        </fo:table-cell>   
			                                    </fo:table-row>   
			                                    <xsl:apply-templates select="/root/theReq/theReqDetails[dayNum=$i]"/>
											</fo:table-body>
										</fo:table>                        
									</fo:block>                    
								</fo:table-cell>                     
							</fo:table-row>
						</fo:table-body>
					</fo:table> 
				</fo:block>
			</fo:table-cell>
		</fo:table-row>               
    </xsl:template>
    
    <xsl:template match="dayTasksDetail">
        <fo:table-row>
            <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" font-weight="normal" >
                <fo:block><xsl:value-of select="hourLabel"/></fo:block>
            </fo:table-cell>          
            <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" font-weight="normal">
                <fo:block>
                	<xsl:choose>
                		<xsl:when test="string-length(description) > 100">
                			<xsl:value-of select="substring(description,1,100)" /> 
                		</xsl:when>
                		<xsl:otherwise>
                			<xsl:value-of select="description"/>
                		</xsl:otherwise>
                	</xsl:choose>                	
                </fo:block>
            </fo:table-cell>
        </fo:table-row>       
    </xsl:template>
    <xsl:template match="theReqDetail">       
        <fo:table-row>
            <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" font-weight="normal" >
                <fo:block><xsl:value-of select="req"/></fo:block>
            </fo:table-cell>        
            <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" font-weight="normal" text-align="center">
                <fo:block><xsl:value-of select="isDaily"/></fo:block>
            </fo:table-cell>
        </fo:table-row>       
    </xsl:template>   
</xsl:stylesheet>