<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:variable name="dvd_records" select="/root/modules/DvdPlanVault/OperationPlanMasterVault" />
	<xsl:variable name="afe_records" select="/root/modules/CostAfeVault/CostAfeMasterVault" />
	<xsl:variable name="afe_summaries" select="/root/modules/CostAfeVaultOthers/AfeItemSummary" />
	<xsl:variable name="phaseCost" select="/root/modules/CostAfeVaultOthers/PhaseCost" />
	<xsl:variable name="categories" select="/root/modules/CostAfeVaultOthers/Category" />
	<xsl:variable name="afe">
		<xsl:choose>
			<xsl:when test="$dvd_records/costPhaseType = 'CS'">
				<xsl:value-of select="sum($afe_records/CostAfeDetailVault[phaseCode!='PA']/itemTotal/@rawNumber)" />
			</xsl:when>
			<xsl:when test="$dvd_records/costPhaseType = 'PA'">
				<xsl:value-of select="sum($afe_records/CostAfeDetailVault[phaseCode!='PC']/itemTotal/@rawNumber)" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="sum($afe_records/CostAfeDetailVault/itemTotal/@rawNumber)" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	
	<xsl:output method="xml" />
	<xsl:preserve-space elements="*" />
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" 
					margin-left="0.5cm" margin-right="0.5cm">
					<fo:region-body margin-top="1.5cm" margin-bottom="1.5cm"/>
					<fo:region-before extent="1.5cm"/>
					<fo:region-after extent="1cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table width="100%" table-layout="fixed">
						<fo:table-column column-number="1" column-width="proportional-column-width(1)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(3)"/>
						<fo:table-column column-number="3" column-width="proportional-column-width(1)"/>
						<fo:table-body space-before="8pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="10pt" padding="0.5mm" padding-top="0.15cm" number-columns-spanned="3">
									<fo:block color="white">-</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell font-size="10pt" padding="0.5mm" text-align="center" font-weight="bold" border="0.1mm solid black" padding-top="0.15cm">
									<fo:block>Santos</fo:block>
								</fo:table-cell>                        
								<fo:table-cell font-size="7pt" padding="0.5mm" text-align="center" font-weight="bold" border="0.1mm solid black" padding-top="0.25cm">
									<fo:block>AFE / COST ESTIMATE</fo:block>
								</fo:table-cell>                        
								<fo:table-cell font-size="7pt" padding="0.5mm" text-align="center" font-weight="bold" border="0.1mm solid black">
									<fo:block>FORM<fo:block/>DMS F-101</fo:block>
								</fo:table-cell>                        
							</fo:table-row>                  
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="12pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)" />
						<fo:table-column column-number="2" column-width="proportional-column-width(50)" />
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="5pt">
									<fo:block text-align="left">
										Copyright IDS 2010, KW. 20100810, SANTOS_CBU_afe_report
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="5pt">
									<fo:block text-align="right">
										Page <fo:page-number />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" font-size="8pt">
					<fo:block>
						<xsl:call-template name="afe_details"/>
						<xsl:apply-templates select="/root/modules/DvdPlanVault/OperationPlanMasterVault"/>
						<fo:table width="100%" table-layout="fixed" keep-together="always">
							<fo:table-column column-width="proportional-column-width(1)"/>
							<fo:table-column column-width="proportional-column-width(3)"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell padding="0.1mm" font-size="5pt" number-columns-spanned="2" >
										<xsl:call-template name="afe_description"/>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.1mm" font-size="5pt">
										<xsl:call-template name="days_summary"/>
									</fo:table-cell>
									<fo:table-cell padding="0.1mm" font-size="5pt" text-align="right">
										<xsl:call-template name="page_footer"/>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
   
	<xsl:template name="afe_description">
		<fo:table width="100%" table-layout="fixed" keep-together="always">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt" text-align="left" border="0.1mm solid black">
						<fo:block font-weight="bold">Project Description Summary</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black" text-align="left">
						<fo:block linefeed-treatment="preserve"><xsl:value-of select="$afe_records/longDescription" /></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt" text-align="right">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
   
	<xsl:template name="page_footer">
		<fo:table width="100%" table-layout="fixed" keep-together="always">
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(3)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(3)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt" text-align="right">
						<fo:block font-weight="bold">Prepared by</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black">
						<fo:block color="white">!<fo:block/>!</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" text-align="right">
						<fo:block font-weight="bold">Reviewed By</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black">
						<fo:block color="white">!<fo:block/>!</fo:block>
					</fo:table-cell>
				</fo:table-row>   	  		
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black" background-color="yellow">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black" background-color="yellow">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
				</fo:table-row>   	  		
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" text-align="left">
						<fo:block color="red" font-weight="bold">Final Cost Estimate</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" number-columns-spanned="3">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
				</fo:table-row>   	  		
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt" text-align="right">
						<fo:block font-weight="bold">Revision</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" number-columns-spanned="3">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
				</fo:table-row>   	  		
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt" number-columns-spanned="5">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
				</fo:table-row>   	  		
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt" text-align="right">
						<fo:block font-weight="bold">Date</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black" text-align="center">
						<fo:block><xsl:value-of select="$afe_records/approvalDate" /></fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" number-columns-spanned="3">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
   
	<xsl:template name="days_summary">
		
		<fo:table width="100%" table-layout="fixed" border="0.1mm solid black">
			<fo:table-column column-width="proportional-column-width(4)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black">
						<fo:block>SPUD TO TD</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black">
						<fo:block>
							<xsl:value-of select="format-number(sum($dvd_records/OperationPlanPhaseVault[phaseCode='SC' or phaseCode='SH' or phaseCode='PH' or phaseCode='IH' or phaseCode='IC']/p50Duration/@rawNumber),'###,##0.00')" />
							<xsl:value-of select="$dvd_records/OperationPlanPhaseVault/p50Duration/@uomSymbol" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black">
						<fo:block>SPUD TO RIG RELEASE</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black">
						<fo:block>
							<xsl:choose>
								<xsl:when test="$dvd_records/costPhaseType = 'CS'">
									<xsl:value-of select="format-number(sum($dvd_records/OperationPlanPhaseVault[phaseCode='SC' or phaseCode='SH' or phaseCode='PH' or phaseCode='EP' or phaseCode='PC' or phaseCode='C' or phaseCode='IH' or phaseCode='IC' or phaseCode='S']/p50Duration/@rawNumber),'###,##0.00')" />
									<xsl:value-of select="$dvd_records/OperationPlanPhaseVault/p50Duration/@uomSymbol" />
								</xsl:when>
								<xsl:when test="$dvd_records/costPhaseType = 'PA'">
									<xsl:value-of select="format-number(sum($dvd_records/OperationPlanPhaseVault[phaseCode='SC' or phaseCode='SH' or phaseCode='PH' or phaseCode='EP' or phaseCode='PA' or phaseCode='C' or phaseCode='IH' or phaseCode='IC' or phaseCode='S']/p50Duration/@rawNumber),'###,##0.00')" />
									<xsl:value-of select="$dvd_records/OperationPlanPhaseVault/p50Duration/@uomSymbol" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="format-number(sum($dvd_records/OperationPlanPhaseVault[phaseCode='SC' or phaseCode='SH' or phaseCode='PH' or phaseCode='EP' or phaseCode='PC' or phaseCode='PA' or phaseCode='C' or phaseCode='IH' or phaseCode='IC' or phaseCode='S']/p50Duration/@rawNumber),'###,##0.00')" />
									<xsl:value-of select="$dvd_records/OperationPlanPhaseVault/p50Duration/@uomSymbol" />
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black">
						<fo:block>RIG MOVE TO RIG RELEASE</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm" font-size="5pt" border="0.1mm solid black">
						<fo:block>
							<xsl:choose>
								<xsl:when test="$dvd_records/costPhaseType = 'CS'">
				  					<xsl:value-of select="format-number(sum($dvd_records/OperationPlanPhaseVault[phaseCode='PS' or phaseCode='SC' or phaseCode='SH' or phaseCode='PH' or phaseCode='EP' or phaseCode='PC' or phaseCode='C' or phaseCode='IH' or phaseCode='IC' or phaseCode='S']/p50Duration/@rawNumber),'###,##0.00')" />
									<xsl:value-of select="$dvd_records/OperationPlanPhaseVault/p50Duration/@uomSymbol" />
								</xsl:when>
								<xsl:when test="$dvd_records/costPhaseType = 'PA'">
				  					<xsl:value-of select="format-number(sum($dvd_records/OperationPlanPhaseVault[phaseCode='PS' or phaseCode='SC' or phaseCode='SH' or phaseCode='PH' or phaseCode='EP' or phaseCode='PA' or phaseCode='C' or phaseCode='IH' or phaseCode='IC' or phaseCode='S']/p50Duration/@rawNumber),'###,##0.00')" />
									<xsl:value-of select="$dvd_records/OperationPlanPhaseVault/p50Duration/@uomSymbol" />
								</xsl:when>
								<xsl:otherwise>
		  							<xsl:value-of select="format-number(sum($dvd_records/OperationPlanPhaseVault[phaseCode='PS' or phaseCode='SC' or phaseCode='SH' or phaseCode='PH' or phaseCode='EP' or phaseCode='PC' or phaseCode='PA' or phaseCode='C' or phaseCode='IH' or phaseCode='IC' or phaseCode='S']/p50Duration/@rawNumber),'###,##0.00')" />
									<xsl:value-of select="$dvd_records/OperationPlanPhaseVault/p50Duration/@uomSymbol" />
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>   	  		
			</fo:table-body>
		</fo:table>
	</xsl:template>
   
	<xsl:template name="afe_details">
		<fo:table width="100%" table-layout="fixed" space-after="12pt" border="0.1mm solid black" table-omit-header-at-break="true">
		  	<fo:table-column column-width="proportional-column-width(1)"/>
		  	<fo:table-column column-width="proportional-column-width(4)"/>
		  	<fo:table-column column-width="proportional-column-width(6)"/>
		  	
		  	<fo:table-header>
		  		<fo:table-row>
		  			<fo:table-cell padding="0.1mm" font-size="5pt">
		  				<fo:block>WELL:</fo:block>
		  			</fo:table-cell>
		  			<fo:table-cell padding="0.1mm" font-size="5pt" number-columns-spanned="2">
		  				<fo:block>
		  					<xsl:value-of select="$dvd_records/dvdPlanName" />
		  				</fo:block>
		  			</fo:table-cell>
		  		</fo:table-row>
		  		<fo:table-row>
		  			<fo:table-cell padding="0.1mm" font-size="5pt">
		  				<fo:block>AFE No:</fo:block>
		  			</fo:table-cell>
		  			<fo:table-cell padding="0.1mm" font-size="5pt" number-columns-spanned="2">
		  				<fo:block>
		  					<xsl:value-of select="$afe_records/afeNumber" />
		  				</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
						<fo:block>Expenditure Code/Task No.</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
						<fo:block>Description</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
						<fo:block>TOTAL COST  
							<xsl:choose>
								<xsl:when test="$dvd_records/costPhaseType = 'CS'">
									<fo:block/>C&amp;S
								</xsl:when>
								<xsl:when test="$dvd_records/costPhaseType = 'PA'">
									<fo:block/>P&amp;A
								</xsl:when>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="/root/modules/CostAfeVaultOthers/Category" mode="body">
					<xsl:sort select="sequence" data-type="number" order="ascending"/>
				</xsl:apply-templates>

				<xsl:apply-templates select="/root/modules/CostAfeVaultOthers/Category" mode="footer">
					<xsl:sort select="sequence" data-type="number" order="ascending"/>
				</xsl:apply-templates>
				<fo:table-row>
					<fo:table-cell padding="0.1mm" font-size="5pt">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1mm" font-size="5pt">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1mm" font-size="5pt">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:call-template name="afe_by_phase" />
			</fo:table-body>
		</fo:table>
	</xsl:template>
   
   <xsl:template name="afe_by_phase">
  		<fo:table-row>
  			<fo:table-cell padding="0.1mm" font-size="5pt">
  				<fo:block color="white">!</fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt">
  				<fo:block color="white">!</fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="right">
  				<fo:block>TOTAL</fo:block>
  			</fo:table-cell>
  		</fo:table-row>
  		
  		<fo:table-row>
  			<fo:table-cell padding="0.1mm" font-size="5pt">
  				<fo:block color="white">!</fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="left" font-weight="bold">
  				<fo:block>
					<xsl:choose>
						<xsl:when test="$dvd_records/costPhaseType = 'CS'">
							C&amp;S Cost
						</xsl:when>
						<xsl:when test="$dvd_records/costPhaseType = 'PA'">
							P&amp;A Cost
						</xsl:when>
						<xsl:otherwise>
							PHASE COST
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="right">
  				<fo:block><xsl:value-of select="format-number($afe,'###,###')" /></fo:block>
  			</fo:table-cell>
  		</fo:table-row>
  		
  		<fo:table-row>
  			<fo:table-cell padding="0.1mm" font-size="5pt">
  				<fo:block color="white">!</fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="left" font-weight="bold">
  				<fo:block>CUMULATIVE TOTAL</fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="right">
  				<fo:block><xsl:value-of select="format-number($afe,'###,###')" /></fo:block>
  			</fo:table-cell>
  		</fo:table-row>
  		
  		<fo:table-row>
  			<fo:table-cell padding="0.1mm" font-size="5pt">
  				<fo:block color="white">!</fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="left" font-weight="bold">
  				<fo:block>Cost/<xsl:value-of select="$dvd_records/OperationPlanPhaseVault/depthMdMsl/@uomSymbol" /></fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="right">
  				<fo:block><xsl:value-of select="format-number($afe div 
  					$dvd_records/OperationPlanPhaseVault[last()]/depthMdMsl/@rawNumber,'###,###')" />/<xsl:value-of select="$dvd_records/OperationPlanPhaseVault/depthMdMsl/@uomSymbol" /></fo:block>
  			</fo:table-cell>
  		</fo:table-row>
   </xsl:template>
   
   <xsl:template match="/root/modules/CostAfeVaultOthers/Category" mode="body">
   		<xsl:variable name="categoryname" select="label" />
  			<fo:table-row>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center">
  				<fo:block><xsl:value-of select="sequence" /></fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
  				<fo:block><xsl:value-of select="$categoryname" /></fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center">
  				<fo:block color="white">!</fo:block>
  			</fo:table-cell>
  		</fo:table-row>
  		<xsl:apply-templates select="/root/modules/CostAfeVaultOthers/AfeItemSummary[category=$categoryname]" />
   </xsl:template>
   
   <xsl:template match="/root/modules/CostAfeVaultOthers/Category" mode="footer">
   		<xsl:variable name="category" select="label" />
		<fo:table-row>
  			<fo:table-cell padding="0.1mm" font-size="5pt" text-align="center">
  				<fo:block color="white">!</fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="left" font-weight="bold">
  				<fo:block><xsl:value-of select="label" /></fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="right">
  				<fo:block>
					<xsl:choose>
						<xsl:when test="$dvd_records/costPhaseType = 'CS'">
							<xsl:value-of select="format-number(sum($afe_records/CostAfeDetailVault[category=$category and number(itemTotal/@rawNumber) and (phaseCode!='PA')]/itemTotal/@rawNumber),'###,###')" />
						</xsl:when>
						<xsl:when test="$dvd_records/costPhaseType = 'PA'">
							<xsl:value-of select="format-number(sum($afe_records/CostAfeDetailVault[category=$category and number(itemTotal/@rawNumber) and (phaseCode='PS' or phaseCode='SC' or phaseCode='SH' or phaseCode='PH' or phaseCode='EP' or phaseCode='PA' or phaseCode='C')]/itemTotal/@rawNumber),'###,###')" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number(sum($afe_records/CostAfeDetailVault[category=$category and number(itemTotal/@rawNumber)]/itemTotal/@rawNumber),'###,###')" />
						</xsl:otherwise>
					</xsl:choose>
  				</fo:block>
  			</fo:table-cell>
  		</fo:table-row>
   </xsl:template>
   
   <xsl:template match="/root/modules/CostAfeVaultOthers/AfeItemSummary">
   		<xsl:variable name="accountCode" select="accountCode" />
   		<xsl:variable name="shortDescription" select="shortDescription" />
   		<xsl:variable name="itemDescription" select="itemDescription" />
   		<xsl:variable name="category" select="category" />
  		<fo:table-row>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border-right="0.1mm solid black" text-align="center">
  				<fo:block><xsl:value-of select="accountCode" /></fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border-right="0.1mm solid black" text-align="left">
  				<fo:block><xsl:value-of select="shortDescription" /> - <xsl:value-of select="itemDescription" /></fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" text-align="right">
  				<fo:block>
					<xsl:choose>
						<xsl:when test="$dvd_records/costPhaseType = 'CS'">
							<xsl:value-of select="format-number(sum($afe_records/CostAfeDetailVault[phaseCode!='PA' and 
									dynaAttr/accountCode=$accountCode and 
		  							shortDescription=$shortDescription and
		  							itemDescription=$itemDescription and
		  							number(itemTotal/@rawNumber) and 
		  							category=$category]/itemTotal/@rawNumber),'###,###')" />
						</xsl:when>
						<xsl:when test="$dvd_records/costPhaseType = 'PA'">
							<xsl:value-of select="format-number(sum($afe_records/CostAfeDetailVault[phaseCode!='PC' and 
									dynaAttr/accountCode=$accountCode and 
		  							shortDescription=$shortDescription and
		  							itemDescription=$itemDescription and
		  							number(itemTotal/@rawNumber) and 
		  							category=$category]/itemTotal/@rawNumber),'###,###')" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number(sum($afe_records/CostAfeDetailVault[dynaAttr/accountCode=$accountCode and 
		  							shortDescription=$shortDescription and
		  							itemDescription=$itemDescription and
		  							number(itemTotal/@rawNumber) and 
		  							category=$category]/itemTotal/@rawNumber),'###,###')" />
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
  			</fo:table-cell>
  		</fo:table-row>
   </xsl:template>
   
	<xsl:template match="/root/modules/DvdPlanVault/OperationPlanMasterVault">
		<fo:table width="100%" table-layout="fixed" space-after="12pt" border="0.1mm solid black">
	   	  	<fo:table-column column-width="proportional-column-width(1)"/>
	   	  	<fo:table-column column-width="proportional-column-width(2)"/>
		  	<fo:table-column column-width="proportional-column-width(2)"/>
		  	<fo:table-column column-width="proportional-column-width(1)"/>
		  	<fo:table-column column-width="proportional-column-width(1)"/>
		  	<fo:table-column column-width="proportional-column-width(1)"/>
		  	<fo:table-column column-width="proportional-column-width(1)"/>
	   	  	<fo:table-column column-width="proportional-column-width(1)"/>
	   	  	<fo:table-header>
	   	  		<fo:table-row>
		  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
		  				<fo:block>No</fo:block>
		  			</fo:table-cell>
		  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
		  				<fo:block>Phase</fo:block>
		  			</fo:table-cell>
		  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
		  				<fo:block>Comment</fo:block>
		  			</fo:table-cell>	  			
		  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
		  				<fo:block>Planned Depth (TMD) (<xsl:value-of select="OperationPlanPhaseVault/depthMdMsl/@uomSymbol" />)</fo:block>
		  			</fo:table-cell>
		  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
		  				<fo:block>Planned Depth (TVD) (<xsl:value-of select="OperationPlanPhaseVault/depthTvdMsl/@uomSymbol" />)</fo:block>
		  			</fo:table-cell>		  			
		  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
		  				<fo:block>Phase Dur (<xsl:value-of select="OperationPlanPhaseVault/p50Duration/@uomSymbol" />)</fo:block>
		  			</fo:table-cell>
		  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
		  				<fo:block>Cum. Phase Dur. (<xsl:value-of select="OperationPlanPhaseVault/dynaAttr/cumP50Duration/@uomSymbol" />)</fo:block>
		  			</fo:table-cell>
		  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center" font-weight="bold">
		  				<fo:block>Cost (<xsl:value-of select="$afe_records/CostAfeDetailVault/itemTotal/@uomSymbol" />)</fo:block>
		  			</fo:table-cell>
	   	  		</fo:table-row>
	   	  	</fo:table-header>
	   	  	<fo:table-body>
	   	  		<xsl:apply-templates select="OperationPlanPhaseVault" />
	        </fo:table-body>
		</fo:table>
	</xsl:template>
	<xsl:template match="OperationPlanPhaseVault">
		<fo:table-row>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center">
  				<fo:block><xsl:value-of select="sequence" /></fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="left">
  				<fo:block><xsl:value-of select="phaseCode/@lookupLabel" /></fo:block>
  			</fo:table-cell>
			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="left">
  				<fo:block><xsl:value-of select="phaseSummary" /></fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center">
  				<fo:block>
  					<xsl:value-of select="depthMdMsl" />
  					<xsl:value-of select="depthMdMsl/@uomSymbol" />
  				</fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center">
  				<fo:block>
  					<xsl:value-of select="depthTvdMsl" />
  					<xsl:value-of select="depthTvdMsl/@uomSymbol" />
  				</fo:block>
  			</fo:table-cell>  			
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center">
  				<fo:block>
  					<xsl:value-of select="p50Duration" />
  					<xsl:value-of select="p50Duration/@uomSymbol" />
  				</fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center">
  				<fo:block>
  					<xsl:value-of select="dynaAttr/cumP50Duration" />
  					<xsl:value-of select="dynaAttr/cumP50Duration/@uomSymbol" />
  				</fo:block>
  			</fo:table-cell>
  			<fo:table-cell padding="0.1mm" font-size="5pt" border="0.1mm solid black" text-align="center">
  				<fo:block>
  					<xsl:variable name="phaseCode" select="phaseCode" />
  					<xsl:variable name="sec_budget" select="sum($afe_records/CostAfeDetailVault[phaseCode=$phaseCode]/itemTotal/@rawNumber)" />
  					<xsl:value-of select="format-number($sec_budget,'###,###')" />
  				</fo:block>
  			</fo:table-cell>
  		</fo:table-row>
   </xsl:template>
</xsl:stylesheet>
