<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- IDS_DEMO MOBILIZATION REPORT -->		
	<xsl:import href="./ReportTemplate/activity_record/ddr_activity_record_ids_demo.xsl" />
	<xsl:import href="./ReportTemplate/activity_record/ddr_next_day_activity_record_ids_demo.xsl" />
	<xsl:import href="./ReportTemplate/marine_record/ddr_marine_record_ids_demo.xsl" />
	<xsl:import href="./ReportTemplate/pob_record/ddr_pob_record_ids_demo.xsl" />
	<xsl:import href="./ReportTemplate/rigstock_record/ddr_rigstock_record_ids_demo.xsl" />
	<xsl:import href="./ReportTemplate/transportdaily_record/ddr_transportdaily_record_ids_demo.xsl" />
	<xsl:import href="./ReportTemplate/weatherenvironment_record/ddr_weatherenvironment_record_ids_demo.xsl" />
	<xsl:import href="./ReportTemplate/well_record/dmobr_wells_record_ids_demo.xsl" /> 
		
	<xsl:output method="xml"/>
	<xsl:preserve-space elements="*"/>
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29cm" page-width="21cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
					<fo:region-before extent="2.2cm"/>
					<fo:region-after extent="2cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1">
				
				<!-- REGION BEFORE - LOGO -->
				<fo:static-content flow-name="xsl-region-before">
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" margin-top="10pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell font-size="12pt">
									<fo:block>
                                    	<fo:external-graphic content-width="scale-to-fit" width="100pt"
                                    		content-height="50%" scaling="uniform" src="url(d2://images/company_logo.jpg)"/>
                                    </fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<!-- END OF REGION BEFORE - LOGO -->
				
				<!-- REGION AFTER - FOOTER -->
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" margin-top="1cm">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS, 20081209, IDS_DEMO_MOBILIZATION
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="right">
										Page <fo:page-number/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
		                        <fo:table-cell font-size="6pt" number-columns-spanned="2">
		                           <fo:block text-align="left">Printed on
		                           <xsl:variable name="printdate" select="/root/modules/Daily/Daily/dayDate"/>
		                           <xsl:value-of select="d2Utils:getSystemDate('dd MMM yyyy')"/>
		                           </fo:block>
		                        </fo:table-cell>
		                     </fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<!-- END OF REGION AFTER - FOOTER -->
				
				<!-- REGION BODY - REPORT SECTION -->
				<fo:flow flow-name="xsl-region-body" font-size="8pt">
					<fo:block>
						<fo:table inline-progression-dimension="19cm" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
							<fo:table-body>
								<!-- WELL SECTION -->
								<fo:table-row keep-together="always">
									<fo:table-cell>
										<fo:block>
											<xsl:apply-templates select="/root/modules/Operation/Operation"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
													
								<!-- ACTIVITY SECTION -->	
								<xsl:if	test="count(/root/modules/Activity/Activity) &gt; 0">						
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates select="/root/modules/Activity"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- NEXT DAY ACTIVITY SECTION -->
								<xsl:if	test="count(/root/modules/NextDayActivity/Activity) &gt; 0">
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates select="/root/modules/NextDayActivity"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- RIG STOCK SECTION -->
								<fo:table-row keep-together="always">
									<fo:table-cell>
										<fo:block>
										<xsl:if	test="count(/root/modules/RigStock/RigStock) &gt; 0">
											<xsl:apply-templates select="/root/modules/RigStock"/>
										</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<!-- POB SECTION -->
								<fo:table-row keep-together="always">
									<fo:table-cell>									
										<fo:block>
										<xsl:if	test="count(/root/modules/PersonnelOnSite/PersonnelOnSite) &gt; 0">
											<xsl:apply-templates select="/root/modules/PersonnelOnSite"/>
										</xsl:if>
										</fo:block>									
									</fo:table-cell>
								</fo:table-row>
								
								<!-- MARINE SECTION -->				
								<fo:table-row keep-together="always">
									<fo:table-cell>
										<fo:block>
										<xsl:if	test="count(/root/modules/MarineProperties/MarineProperties) &gt; 0">
											<xsl:apply-templates select="/root/modules/MarineProperties/MarineProperties"/>
										</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<!-- WEATHER SECTION -->				
								<fo:table-row keep-together="always">
									<fo:table-cell>
										<fo:block>
										<xsl:if	test="count(/root/modules/WeatherEnvironment/WeatherEnvironment) &gt; 0">
											<xsl:apply-templates select="/root/modules/WeatherEnvironment/WeatherEnvironment"/>
										</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>										    
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>	
				<!-- END OF REGION BODY - REPORT SECTION -->
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
