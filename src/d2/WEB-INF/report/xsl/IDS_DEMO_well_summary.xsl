<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:output method="xml"/>
	<xsl:preserve-space elements="*"/>
	
	<xsl:variable name="image_height" select="/root/modules/WellSummary/dynaAttr/imageHeight" />
	<xsl:variable name="hasPs" select="/root/modules/WellSummary/Operation/dynaAttr/hasPs" />
	<xsl:variable name="ps_tallyHeight" select="/root/modules/WellSummary/Operation/dynaAttr/ps_tallyHeight" />
	<xsl:variable name="image_height2" select="$image_height - $ps_tallyHeight" />
	<xsl:variable name="imgpath">d2://../../images/formation</xsl:variable>
	<xsl:variable name="casingcount" select="/root/modules/WellSummary/dynaAttr/casingCount" />
	<xsl:variable name="well_depth" select="/root/modules/WellSummary/dynaAttr/wellDepth" />
	<xsl:variable name="table_height" select="14" />
	
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" page-height="21cm" page-width="29cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="0.5cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="1cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="12pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="12pt">
									<fo:block>
										<fo:external-graphic content-width="scale-to-fit"
											width="100pt" content-height="50%" scaling="uniform" src="url(d2://images/company_logo.jpg)"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="6pt" display-align="center">
									<fo:block text-align="right" font-weight="bold">
										<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="12pt">
											<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
											<fo:table-body space-before="6pt" space-after="6pt">
												<fo:table-row>
													<fo:table-cell font-size="12pt">
														<fo:block>
															<fo:inline text-decoration="underline">WELL SUMMARY REPORT</fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell text-align="right" font-size="12pt" font-weight="bold" display-align="before">
														<fo:block>
															<fo:inline text-decoration="underline">
															<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/completeOperationName"/>
															</fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="100%" table-layout="fixed"
						space-after="10pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS, 20130320, IDS_DEMO_well_summary
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" font-size="7pt">
					<fo:block>
						<xsl:apply-templates select="/root/modules/WellSummary" mode="main" />
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	
	<xsl:template match="WellSummary" mode="main">
		<fo:table width="100%" table-layout="fixed" wrap-option="wrap" border="0.25px solid #999999">
			<fo:table-column column-width="proportional-column-width(5)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			
			
			<xsl:if test="//Bharun">
				<fo:table-column column-width="proportional-column-width(5)"/>
			</xsl:if>
			<xsl:if test="//CementJob">
				<fo:table-column column-width="proportional-column-width(3)"/>
			</xsl:if>
			<xsl:if test="//WirelineRun">
				<fo:table-column column-width="proportional-column-width(3)"/>
			</xsl:if>
			<xsl:if test="//Core">
				<fo:table-column column-width="proportional-column-width(3)"/>
			</xsl:if>
			<xsl:if test="//DrillStemTest">
				<fo:table-column column-width="proportional-column-width(3)"/>
			</xsl:if>
			<!-- <fo:table-column column-width="proportional-column-width(4)"/> -->
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-body>	
				<fo:table-row>			
					<fo:table-cell padding="0.05cm" text-align="center" font-weight="bold">
						<fo:block>Formation</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" text-align="center" font-weight="bold" number-columns-spanned="3">
						<fo:block>Casing</fo:block>
					</fo:table-cell>
					<xsl:if test="//Bharun">
						<fo:table-cell padding="0.05cm" text-align="center" font-weight="bold">
							<fo:block>BHA Run</fo:block>
						</fo:table-cell>
					</xsl:if>
					<xsl:if test="//CementJob">
						<fo:table-cell padding="0.05cm" text-align="center" font-weight="bold">
							<fo:block>Cement Top</fo:block>
						</fo:table-cell>
					</xsl:if>
					<xsl:if test="//WirelineRun">
						<fo:table-cell padding="0.05cm" text-align="center" font-weight="bold">
							<fo:block>Wireline Logging</fo:block>
						</fo:table-cell>
					</xsl:if>
					<xsl:if test="//Core">
						<fo:table-cell padding="0.05cm" text-align="center" font-weight="bold">
							<fo:block>Coring</fo:block>
						</fo:table-cell>
					</xsl:if>
					<xsl:if test="//DrillStemTest">
						<fo:table-cell padding="0.05cm" text-align="center" font-weight="bold">
							<fo:block>DST</fo:block>
						</fo:table-cell>
					</xsl:if>
					<!--  <fo:table-cell padding="0.05cm" text-align="center" font-weight="bold">
						<fo:block>Gas</fo:block>
					</fo:table-cell>  -->
					<fo:table-cell padding="0.05cm" text-align="left" font-weight="bold">
						<fo:block>Well Details</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>			
					<fo:table-cell padding="0.05cm" height="0.1cm" text-align="center" font-weight="bold">
						<fo:block></fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0" height="0.1cm" text-align="center" font-weight="bold" number-columns-spanned="3" border="1px solid black">
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>			
					<fo:table-cell padding="0cm" text-align="center">
						<xsl:apply-templates select="Operation" mode="formation_diagram"/>
					</fo:table-cell>
					<fo:table-cell padding="0cm" text-align="right" background-color="#aaaaaa" display-align="right">
						<xsl:apply-templates select="Operation" mode="casing_diagram"/>
					</fo:table-cell>
					<fo:table-cell padding="0cm" text-align="center" background-color="#aaaaaa">
						<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
							<fo:table-column column-width="proportional-column-width(10)"/>
							<fo:table-column column-width="proportional-column-width(80)"/>
							<fo:table-column column-width="proportional-column-width(10)"/> 
							<fo:table-body>	
								<xsl:if test="$hasPs ='1'">
									<fo:table-row> 
										<fo:table-cell padding="0cm" border-bottom="0.25px solid black" height="{$ps_tallyHeight div $image_height * $table_height}cm" background-color="#00AE23">
											<fo:block></fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0cm" height="{$ps_tallyHeight div $image_height * $table_height}cm" background-color="#ffffff">
											<fo:block></fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0cm" border-bottom="0.25px solid black" height="{$ps_tallyHeight div $image_height * $table_height}cm" background-color="#00AE23">
											<fo:block></fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row> 
										<fo:table-cell padding="0cm" border-bottom="0.25px solid black" height="{$image_height2 div $image_height * $table_height}cm" background-color="#ffffff">
											<fo:block></fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0cm" border-bottom="0.25px solid black" height="{$image_height2 div $image_height * $table_height}cm" background-color="#ffffff">
											<fo:block></fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0cm" border-bottom="0.25px solid black" height="{$image_height2 div $image_height * $table_height}cm" background-color="#ffffff">
											<fo:block></fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								<xsl:if test="$hasPs ='0'">
									<fo:table-row> 
										<fo:table-cell padding="0cm" height="{$image_height div $image_height * $table_height}cm" number-columns-spanned="3" background-color="#ffffff">
											<fo:block></fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
							</fo:table-body>	
						</fo:table>
						
					</fo:table-cell>
					<fo:table-cell padding="0cm" text-align="center" background-color="#aaaaaa" border-right="0.1px solid #aaaaaa" border-left="0.1px solid #aaaaaa">
						<xsl:apply-templates select="Operation" mode="casing_reverse_diagram" />
					</fo:table-cell>
					
					<xsl:if test="//Bharun">
						<fo:table-cell padding="0cm" text-align="center" background-color="#dddddd" border-right="0.1px solid #aaaaaa" border-left="0.1px solid #aaaaaa">
							<xsl:apply-templates select="Operation" mode="bha_diagram" />
						</fo:table-cell>
					</xsl:if>
					
					<xsl:if test="//CementJob">
					<fo:table-cell padding="0cm" text-align="center" background-color="#dddddd" border-right="0.1px solid #aaaaaa" border-left="0.1px solid #aaaaaa">
						<xsl:apply-templates select="Operation" mode="cement_diagram" />
					</fo:table-cell>
					</xsl:if>
					<xsl:if test="//WirelineRun">
						<fo:table-cell padding="0cm" text-align="center" background-color="#dddddd" border-right="0.1px solid #aaaaaa" border-left="0.1px solid #aaaaaa">
							<xsl:apply-templates select="Operation" mode="wireline_diagram" />
						</fo:table-cell>
					</xsl:if>
					<xsl:if test="//Core">
						<fo:table-cell padding="0cm" text-align="center" background-color="#dddddd" border-right="0.1px solid #aaaaaa" border-left="0.1px solid #aaaaaa">
							<xsl:apply-templates select="Operation" mode="core_diagram" />
						</fo:table-cell>
					</xsl:if>
					<xsl:if test="//DrillStemTest">
						<fo:table-cell padding="0cm" text-align="center" background-color="#dddddd" border-right="0.1px solid #aaaaaa" border-left="0.1px solid #aaaaaa">
							<xsl:apply-templates select="Operation" mode="dst_diagram" />
						</fo:table-cell>
					</xsl:if>
					<!--  <fo:table-cell padding="0cm" text-align="center" background-color="#dddddd" border-right="0.1px solid #aaaaaa" border-left="0.1px solid #aaaaaa">
						<xsl:variable name="gas_graph" select="concat('url(',gas_composite_graph,')')" />
						<xsl:if test="gas_composite_graph">
							<fo:external-graphic src="{$gas_graph}" scaling="uniform" height="{$table_height}cm"/>
						</xsl:if>
					</fo:table-cell> -->
					<fo:table-cell padding="0.05cm" text-align="left">
						<xsl:apply-templates select="Operation" mode="well_data" />
						<xsl:apply-templates select="Operation" mode="formation_list" />
						<xsl:apply-templates select="Operation" mode="casing_list" />
						<xsl:apply-templates select="Operation" mode="productionString_list" />
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="Operation" mode="casing_list">
		<fo:block/>
		<fo:block font-weight="bold">Casing Legend</fo:block>
		<fo:table width="100%" table-layout="fixed" wrap-option="wrap" text-align="left" border="0.25px solid black" space-after="6pt">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-body>	
				<xsl:apply-templates select="CasingSection" mode="list"/>
			</fo:table-body>	
		</fo:table>
	</xsl:template>
	<xsl:template match="CasingSection" mode="list">
		<xsl:variable name="bgcolor" select="dynaAttr/bgColor"/>
		<fo:table-row>
			<fo:table-cell padding="0.05cm" background-color="{$bgcolor}" border="0.1px solid black">
				<fo:block></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" >
				<fo:block>					
					<xsl:value-of select="casingOd/@lookupLabel" /> - 
					<xsl:value-of select="casingSummary" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="Operation" mode="productionString_list">
		<xsl:if test="dynaAttr/hasPs ='1'">
			<fo:block/>
			<fo:block font-weight="bold">Production String Legend</fo:block>
			<fo:table width="100%" table-layout="fixed" wrap-option="wrap" text-align="left" border="0.25px solid black" space-after="6pt">
				<fo:table-column column-width="proportional-column-width(1)"/>
				<fo:table-column column-width="proportional-column-width(10)"/>
				<fo:table-body>	
					<fo:table-row>
						<fo:table-cell padding="0.05cm" background-color="#00AE23" border="0.1px solid black">
							<fo:block></fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.05cm" >
							<fo:block>					
								<xsl:value-of select="dynaAttr/ps_desc"/> @ Depth: <xsl:value-of select="dynaAttr/ps_tallyLenght"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>	
			</fo:table>
		</xsl:if>	
	</xsl:template>

	<xsl:template match="Operation" mode="formation_list">
		<fo:block/>
		<fo:block font-weight="bold">Formation Legend</fo:block>
		<fo:table width="100%" table-layout="fixed" wrap-option="wrap" text-align="left" border="0.25px solid black" space-after="6pt">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-body>	
				<xsl:apply-templates select="Formation[string-length(formationName) &gt; 1]" mode="list"/>
			</fo:table-body>	
		</fo:table>
	</xsl:template>
	<xsl:template match="Formation" mode="list">
		<xsl:variable name="bgimg">
			<xsl:choose>
				<xsl:when test="string-length(formationImage) &gt; 3">
					<xsl:value-of select="concat('url(',$imgpath, '/', formationImage,')')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'url(d2://../../images/empty.gif)'"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
	
		<fo:table-row>
			<fo:table-cell padding="0.05cm" background-image="{$bgimg}" background-repeat="repeat" border="0.1px solid black">
				<fo:block></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" >
				<fo:block>					
					<xsl:value-of select="formationName" />
					(<xsl:value-of select="sampleTopMdMsl" /> <xsl:value-of select="sampleTopMdMsl/@uomSymbol" />)
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="Operation" mode="well_data">
		<fo:table width="100%" table-layout="fixed" wrap-option="wrap" text-align="left" border="0.25px solid black" space-after="6pt">
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(5)"/>
			<fo:table-body>	
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Well Name</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/completeOperationName"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Operator</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="dynaAttr/opCo"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Location</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block>
							<xsl:value-of select="concat(dynaAttr/locParish,' ',dynaAttr/state,' ',dynaAttr/country)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Permit</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="dynaAttr/govPermitNumber"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Spud Date</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="d2Utils:formatDateFromEpochMS(spudDate/@epochMS,'dd MMM yyyy HH:mm')"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Status</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="dynaAttr/currentPhase/@lookupLabel"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Rig Name</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="rigInformationUid/@lookupLabel"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Rig RT</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="dynaAttr/currentDatum"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Latitude</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block>
							<xsl:value-of select="dynaAttr/latDeg"/>&#176;
							<xsl:value-of select="dynaAttr/latMinute"/>'
							<xsl:value-of select="dynaAttr/latSecond"/>&quot;
							<xsl:value-of select="dynaAttr/latNs"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Longitude</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block>
							<xsl:value-of select="dynaAttr/longDeg"/>&#176;
							<xsl:value-of select="dynaAttr/longMinute"/>'
							<xsl:value-of select="dynaAttr/longSecond"/>&quot;
							<xsl:value-of select="dynaAttr/longEw"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Easting</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="dynaAttr/gridEw"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Northing</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="dynaAttr/gridNs"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>UTM Zone</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="dynaAttr/locUtmZone"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Spheroid</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="dynaAttr/spheroidType"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>Datum</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="dynaAttr/gridDatum"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" font-weight="bold">
						<fo:block>AFE Code</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block><xsl:value-of select="afeNumber"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>	
		</fo:table>
	</xsl:template>
	
	<xsl:template match="Operation" mode="formation_diagram">
		<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1.5)"/>
			<fo:table-body>	
				<fo:table-row>
					<xsl:variable name="height" select="/root/modules/WellSummary/dynaAttr/formationFirstDepth"/>
					<fo:table-cell padding-right="0.05cm" text-align="right" height="{$height div $image_height * $table_height}cm" overflow="hidden">
						<xsl:if test="($height div $image_height * $table_height) &gt;= 0.2">
							<fo:block>
								<xsl:value-of select="'0.0'"/>
								<xsl:value-of select="/root/modules/WellSummary/Operation/Formation[1]/sampleTopMdMsl/@uomSymbol"/>
							</fo:block>
						</xsl:if>
					</fo:table-cell>
					<fo:table-cell padding="0cm" text-align="center" height="{$height div $image_height * $table_height}cm"  overflow="hidden" border-top="0.1px dotted #999999" background-repeat="repeat">
						<fo:block></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:apply-templates select="Formation" mode="diagram"/>
			</fo:table-body>	
		</fo:table>
	</xsl:template>
	<xsl:template match="Formation" mode="diagram">
		<xsl:variable name="height" select="dynaAttr/height"/>
		<fo:table-row>
			<fo:table-cell padding-right="0.05cm" text-align="right" height="{$height div $image_height * $table_height}cm" overflow="hidden">
				<xsl:if test="($height div $image_height * $table_height) &gt;= 0.2">
					<fo:block>
						<xsl:value-of select="sampleTopMdMsl"/>
						<xsl:value-of select="sampleTopMdMsl/@uomSymbol"/>
					</fo:block>
				</xsl:if>
			</fo:table-cell>
			<xsl:variable name="bgimg">
				<xsl:choose>
					<xsl:when test="string-length(formationImage) &gt; 3">
						<xsl:value-of select="concat('url(',$imgpath, '/', formationImage,')')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="'url(d2://../../images/empty.gif)'"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<fo:table-cell padding="0cm" text-align="center" height="{$height div $image_height * $table_height}cm"  overflow="hidden"
				background-image="{$bgimg}" border-top="0.1px dotted #999999" background-repeat="repeat">
				<fo:block></fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="Operation" mode="casing_diagram">
		<fo:table width="100%" table-layout="fixed" wrap-option="wrap" border-top="0.1px dotted black">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<xsl:for-each select="CasingSection">
				<fo:table-column column-width="proportional-column-width(1)"/>
			</xsl:for-each>
			<fo:table-body>	
				<fo:table-row>
					<fo:table-cell padding="0cm" background-color="#aaaaaa">
					</fo:table-cell>
					<xsl:apply-templates select="CasingSection" mode="diagram">
						<xsl:sort select="casingOd/@rawNumber" data-type="number" order="descending" />
					</xsl:apply-templates>
				</fo:table-row>
			</fo:table-body>	
		</fo:table>
	</xsl:template>
	<xsl:template match="CasingSection" mode="diagram">
		<xsl:variable name="height" select="dynaAttr/height"/>
		<xsl:variable name="heightBefore" select="dynaAttr/height_before"/>
		<xsl:variable name="bgcolor" select="dynaAttr/bgColor"/>
		<fo:table-cell padding="0cm" background-color="#aaaaaa">
			<fo:table width="100%" table-layout="fixed" wrap-option="wrap" padding-bottom="0">
				<fo:table-column column-width="proportional-column-width(1)"/>
				<fo:table-body>
					
					<xsl:if test="($heightBefore) &gt; 0">
					<fo:table-row>
						<fo:table-cell padding="0cm" text-align="center" height="{($heightBefore div $image_height * $table_height) - 0.4}cm" border-right="0.1px solid black" background-color="#FFFFFF">				
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
					<fo:table-row>
						<fo:table-cell padding="0cm" text-align="center" height="{($height div $image_height * $table_height) - 0.4}cm" border-right="0.1px solid black" background-color="{$bgcolor}">
							
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-bottom="0px" text-align="center" border-right="0.1px solid black" background-color="{$bgcolor}">
							<fo:block><fo:external-graphic src="url(d2://../../images/hangleft.gif)" scaling="uniform"/></fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:table-cell>
	</xsl:template>
	
	<xsl:template match="Operation" mode="casing_reverse_diagram">
		<fo:table width="100%" table-layout="fixed" wrap-option="wrap" border-top="0.1px dotted black">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<xsl:for-each select="CasingSection">
				<fo:table-column column-width="proportional-column-width(1)"/>
			</xsl:for-each>
			<fo:table-body>	
				<fo:table-row>
					<xsl:apply-templates select="CasingSection" mode="diagram_reverse">
						<xsl:sort select="casingOd/@rawNumber" data-type="number" />
					</xsl:apply-templates>
					<fo:table-cell padding="0cm" background-color="#aaaaaa">
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>	
		</fo:table>
	</xsl:template>
	<xsl:template match="CasingSection" mode="diagram_reverse">
		<xsl:variable name="height" select="dynaAttr/height"/>
		<xsl:variable name="heightBefore" select="dynaAttr/height_before"/>
		<xsl:variable name="bgcolor" select="dynaAttr/bgColor"/>
		<fo:table-cell padding="0cm" background-color="#aaaaaa">
			<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
				<fo:table-column column-width="proportional-column-width(1)"/>
				<fo:table-body>
					<xsl:if test="($heightBefore) &gt; 0">
					<fo:table-row>
						<fo:table-cell padding="0cm" text-align="center" height="{($heightBefore div $image_height * $table_height) - 0.4}cm" border-right="0.1px solid black" background-color="#FFFFFF">				
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
					<fo:table-row>
						<fo:table-cell padding="0cm" text-align="center" height="{($height div $image_height * $table_height) - 0.4}cm" border-left="0.1px solid black" background-color="{$bgcolor}"  vertical-align="bottom">
							
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding="0cm" text-align="center" border-left="0.1px solid black" background-color="{$bgcolor}"  vertical-align="bottom">
							<fo:block>
								<fo:external-graphic src="url(d2://../../images/hangright.gif)" scaling="uniform"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:table-cell>
	</xsl:template>
	
	
	<xsl:template match="Operation" mode="bha_diagram">
		<fo:table width="{count(Bharun) * 0.3}cm" table-layout="fixed" wrap-option="wrap" text-align="center">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<xsl:for-each select="Bharun">
				<fo:table-column column-width="proportional-column-width(1)"/>
			</xsl:for-each>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-left="0.1cm" text-align="center">
					</fo:table-cell>
					<xsl:for-each select="Bharun">
						<fo:table-cell padding-left="0.1cm" text-align="center">
							<xsl:call-template name="drawdepth">
				            	<xsl:with-param name="height_before" select="dynaAttr/height_before" />
				            	<xsl:with-param name="height" select="dynaAttr/height" />
				            	<xsl:with-param name="label" select="bhaRunNumber" />
							</xsl:call-template>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="Operation" mode="cement_diagram">
		<fo:table width="{count(Bharun) * 0.2}cm" table-layout="fixed" wrap-option="wrap" text-align="center">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<xsl:for-each select="CementJob">
				<xsl:for-each select="CementStage">
					<fo:table-column column-width="proportional-column-width(1)"/>
				</xsl:for-each>
			</xsl:for-each>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-left="0.1cm" text-align="center">
					</fo:table-cell>
					<xsl:for-each select="CementJob">
						<xsl:for-each select="CementStage">
							<fo:table-cell padding-left="0.1cm" text-align="center">
								<xsl:call-template name="drawdepth">
					            	<xsl:with-param name="height_before" select="dynaAttr/height_before" />
					            	<xsl:with-param name="height" select="dynaAttr/height" />
					            	<xsl:with-param name="label" select="concat(../jobNumber,'/',stageNumber)" />
								</xsl:call-template>
							</fo:table-cell>
						</xsl:for-each>
					</xsl:for-each>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template match="Operation" mode="wireline_diagram">
		<fo:table width="{count(WirelineRun) * 0.2}cm" table-layout="fixed" wrap-option="wrap">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<xsl:for-each select="WirelineRun">
				<fo:table-column column-width="proportional-column-width(1)"/>
			</xsl:for-each>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-left="0.1cm" text-align="center">
					</fo:table-cell>
					<xsl:for-each select="WirelineRun">
						<fo:table-cell padding-left="0.1cm" text-align="center">
							<xsl:call-template name="drawdepth">
				            	<xsl:with-param name="height_before" select="dynaAttr/height_before" />
				            	<xsl:with-param name="height" select="dynaAttr/height" />
				            	<xsl:with-param name="label" select="concat(suiteNumber,'/',runNumber)" />
							</xsl:call-template>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
		
	<xsl:template match="Operation" mode="core_diagram">
		<fo:table width="{count(Core) * 0.2}cm" table-layout="fixed" wrap-option="wrap">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<xsl:for-each select="Core">
				<fo:table-column column-width="proportional-column-width(1)"/>
			</xsl:for-each>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-left="0.1cm" text-align="center">
					</fo:table-cell>
					<xsl:for-each select="Core">
						<fo:table-cell padding-left="0.1cm" text-align="center">
							<xsl:call-template name="drawdepth">
				            	<xsl:with-param name="height_before" select="dynaAttr/height_before" />
				            	<xsl:with-param name="height" select="dynaAttr/height" />
				            	<xsl:with-param name="label" select="coreNumber" />
							</xsl:call-template>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="Operation" mode="dst_diagram">
		<fo:table width="{count(DrillStemTest) * 0.2}cm" table-layout="fixed" wrap-option="wrap">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<xsl:for-each select="DrillStemTest">
				<fo:table-column column-width="proportional-column-width(1)"/>
			</xsl:for-each>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-left="0.1cm" text-align="center">
					</fo:table-cell>
					<xsl:for-each select="DrillStemTest">
						<fo:table-cell padding-left="0.1cm" text-align="center">
							<xsl:call-template name="drawdepth">
				            	<xsl:with-param name="height_before" select="dynaAttr/height_before" />
				            	<xsl:with-param name="height" select="dynaAttr/height" />
				            	<xsl:with-param name="label" select="testNum" />
							</xsl:call-template>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="drawdepth">
		<xsl:param name="height_before" />
		<xsl:param name="height" />
		<xsl:param name="label" />
		<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(3)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0cm" text-align="center" height="{$height_before div $image_height * $table_height}cm" border-bottom="0.25px solid black"  vertical-align="bottom" number-columns-spanned="2">
					</fo:table-cell>
				</fo:table-row>
				<xsl:choose>
					<xsl:when test="($height div $image_height * $table_height) &gt; 0.1">
						<fo:table-row>
							<fo:table-cell padding="0cm" height="{$height div $image_height * $table_height}cm" border-right="0.25px solid black" border-bottom="0.25px solid black" >
								
							</fo:table-cell>
							<fo:table-cell padding="0cm" height="{$height div $image_height * $table_height}cm" border-left="0.25px solid black" border-bottom="0.25px solid black" >
								
							</fo:table-cell>
							<fo:table-cell padding="0cm" height="{$height div $image_height * $table_height}cm" display-align="center" wrap-option="no-wrap">
								<xsl:if test="($height div $image_height * $table_height) &gt; 0.5">
									<fo:block>
										<xsl:value-of select="$label" />
									</fo:block>
								</xsl:if>
							</fo:table-cell>
						</fo:table-row>
						<xsl:if test="($height div $image_height * $table_height) &lt; 0.5">
							<fo:table-row>
								<fo:table-cell padding="0cm" text-align="center" number-columns-spanned="3" wrap-option="no-wrap">
									<fo:block>
										<xsl:value-of select="$label" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<fo:table-row>
							<fo:table-cell padding="0cm" text-align="center" number-columns-spanned="3" wrap-option="no-wrap">
								<fo:block>
									<xsl:value-of select="$label" />
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:otherwise>
				</xsl:choose>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>
