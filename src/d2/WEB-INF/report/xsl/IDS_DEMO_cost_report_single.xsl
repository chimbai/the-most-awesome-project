<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils" xmlns:exsl="http://exslt.org/common" xmlns:xalan="http://xml.apache.org/xalan">	
	<xsl:output method="xml"/>
	<xsl:preserve-space elements="*"/>
	<xsl:variable name="dailyUid" select="/root/UserContext/UserSelection/dailyUid"/>
	<xsl:variable name="reportDaily" select="/root/modules/ReportDaily/ReportDaily[dailyUid=$dailyUid]"/>
	<xsl:variable name="dayDate" select="$reportDaily/reportDatetime/@epochMS"/>
	
	<!-- IDS_DEMO SINGLE DAY COST REPORT TEMPLATE -->
	
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1.5cm">
					<fo:region-body margin-top="2.5cm" margin-bottom="1.5cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="1cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1" id="last-page">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table inline-progression-dimension="100%" table-layout="fixed" margin-top="5pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(70)"/>
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="10pt">
									<fo:block>
										<fo:external-graphic content-width="scale-to-fit" width="100pt" content-height="25%" scaling="uniform" src="url(d2://images/company_logo.jpg)"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="6pt">
									<xsl:call-template name="basicwellheader"/>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="12pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS 2000 IDS_COST_REPORT_SINGLE
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="right">
										Page <fo:page-number/> of <fo:page-number-citation-last ref-id="last-page"/> 
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
		                        <fo:table-cell font-size="6pt" number-columns-spanned="2">
		                           <fo:block text-align="left">Printed on
		                           <xsl:variable name="printdate" select="$reportDaily/reportDatetime"/>
		                           <xsl:value-of select="d2Utils:getLocalTimestamp('d MMMM yyyy hh:mm a (z)')" />
		                           </fo:block>
		                        </fo:table-cell>
		                     </fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" font-size="7pt">
					<fo:block>
						<fo:table width="100%" table-layout="fixed" margin-top="10pt">
							<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
							<fo:table-body>
								<fo:table-row keep-together="always">
									<fo:table-cell>
										<fo:block font-size="10pt" font-weight="bold">
											Daily Cost Summary Report
										</fo:block>										
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<xsl:apply-templates select="/root/modules/CostDailysheet" mode="CostDailysheetHeader"/>										
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>						
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	
	<xsl:template match="modules/CostDailysheet" mode="CostDailysheetHeader">
		<xsl:variable name="accountCode_1" select="CostDailysheet/accountCode"/>
		<xsl:variable name="afeShortDescription_1" select="CostDailysheet/afeShortDescription"/>
		<xsl:variable name="primaryCurrency" select="/root/modules/CostAfeMaster/dynaAttr/primaryCurrency"/>
		<fo:block>
			<fo:table inline-progression-dimension="19cm" table-layout="fixed" border-right="0.5px solid black" border-bottom="0.5px solid black" margin-top="10pt" border-before-width="0.5px" 
				border-before-style="solid" border-before-width.conditionality="retain" border-after-width="0.5px" border-after-style="solid" border-after-width.conditionality="retain"
				border-left="0.5px solid black">
	        	<fo:table-column column-number="1" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(1)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(1)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(1)"/>						
				<fo:table-header>					
					<fo:table-row font-size="8pt" font-weight="bold">
						<fo:table-cell padding="0.3px" border-right="0.5px solid black" border-bottom="0.5px solid black">																
							<fo:block text-align="center" background-color="rgb(223,223,223)"> Account Code </fo:block>
							<fo:block text-align="center" background-color="rgb(223,223,223)">
								<fo:inline color="rgb(223,223,223)">.</fo:inline>
							</fo:block>
						</fo:table-cell>				
						<fo:table-cell padding="0.3px" border-right="0.5px solid black" border-bottom="0.5px solid black">	
							<fo:block text-align="center" background-color="rgb(223,223,223)">  AFE Total </fo:block>
							<fo:block text-align="center" background-color="rgb(223,223,223)">
								<xsl:choose>													
									<xsl:when test = "string(/root/modules/CostAfeMaster/dynaAttr/primaryCurrency)=''">
										($)																			
									</xsl:when>													
									<xsl:otherwise>
										(<xsl:value-of select="$primaryCurrency"/>)
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.3px" border-right="0.5px solid black" border-bottom="0.5px solid black">	
							<fo:block text-align="center" background-color="rgb(223,223,223)"> Day Total </fo:block>
							<fo:block text-align="center" background-color="rgb(223,223,223)">
								<xsl:choose>													
									<xsl:when test = "string(/root/modules/CostAfeMaster/dynaAttr/primaryCurrency)=''">
										($)																			
									</xsl:when>													
									<xsl:otherwise>
										(<xsl:value-of select="$primaryCurrency"/>)
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.3px" border-bottom="0.5px solid black">	
							<fo:block text-align="center" background-color="rgb(223,223,223)"> Cost To Date </fo:block>
							<fo:block text-align="center" background-color="rgb(223,223,223)">
								<xsl:choose>													
									<xsl:when test = "string(/root/modules/CostAfeMaster/dynaAttr/primaryCurrency)=''">
										($)																			
									</xsl:when>													
									<xsl:otherwise>
										(<xsl:value-of select="$primaryCurrency"/>)
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>													
					</fo:table-row>														
				</fo:table-header>
				<fo:table-body>
					<xsl:variable name="costdaily" select="CostDailysheet"/>
					<xsl:for-each select="/root/modules/AccountCodeList/costAccountCodeDetail">	
						<xsl:sort select="accountCode" order="ascending"/>
						<xsl:variable name="accountCode_desc" select="concat(accountCode,'::',shortDescription)"/>
						<xsl:variable name="accountCode" select="accountCode"/>
						<xsl:variable name="afeShortDescription" select="shortDescription"/>
						<xsl:variable name="afeTotal" select="sum(/root/modules/CostAfeMaster/CostAfeMaster/CostAfeDetail[accountCode = $accountCode and shortDescription=$afeShortDescription]/itemTotal/@rawNumber)"/>
						<!--  <xsl:if test="generate-id(.)=generate-id($costdaily[accountCode=$accountCode and afeShortDescription=$afeShortDescription])">-->
							<fo:table-row>
								<fo:table-cell padding="2px" border-right="0.5px solid black">																			
									<fo:block font-size="8pt" text-align="left">
										<xsl:value-of select="accountCode"/> - <xsl:value-of select="shortDescription"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black">																			
									<fo:block font-size="8pt" text-align="right">										
										<xsl:value-of select="d2Utils:formatNumber(string($afeTotal), '###,###,##0.00')"/>
									</fo:block>									
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black">																			
									<fo:block font-size="8pt" text-align="right">
										<xsl:variable name="subTotals">
											<xsl:for-each select="/root/modules/CostDailysheet/CostDailysheet[accountCode=$accountCode and afeShortDescription=$afeShortDescription and dailyUid=$dailyUid]">
												<!-- <number><xsl:value-of select="itemCost/@rawNumber * quantity/@rawNumber"/></number> -->
												<number><xsl:value-of select="itemTotal/@rawNumber"/></number>
											</xsl:for-each>	
										</xsl:variable>
										<xsl:value-of select="d2Utils:formatNumber(string(sum(exsl:node-set($subTotals)/number)), '###,###,##0.00')"/>
									</fo:block>									
								</fo:table-cell>						
								<fo:table-cell padding="2px">																			
									<fo:block font-size="8pt" text-align="right">										
										<xsl:variable name="dailycosttodate" select="sum(/root/modules/DailyCostItems/DailyCostItem[accountCode = $accountCode and afeShortDescription = $afeShortDescription]/costToDate)"/>
										<xsl:value-of select="d2Utils:formatNumber(string($dailycosttodate),'###,###,##0.00')"/>
									</fo:block>									
								</fo:table-cell>
							</fo:table-row>
						<!-- </xsl:if>  -->
					</xsl:for-each>
					<!-- <fo:table-row>
						<fo:table-cell padding="2px" border-top="0.5px solid black">																			
							<fo:block font-size="8pt" text-align="left" font-weight="bold">
								Day Total<fo:inline color="white"></fo:inline>
								<xsl:choose>													
									<xsl:when test = "string(/root/modules/CostAfeMaster/dynaAttr/primaryCurrency)=''">
										($)																			
									</xsl:when>													
									<xsl:otherwise>
										(<xsl:value-of select="$primaryCurrency"/>)
									</xsl:otherwise>
								</xsl:choose>
								<fo:inline color="white"></fo:inline>:
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-top="0.5px solid black" border-right="0.5px solid black">																			
							<fo:block font-size="8pt" text-align="right" font-weight="bold">			
														
								<xsl:variable name="afeTotal" select="string(sum(/root/modules/CostAfeMaster/CostAfeMaster/CostAfeDetail/itemTotal/@rawNumber))"/>								
								<xsl:value-of select="d2Utils:formatNumber($afeTotal,'###,###,##0.00')"/>
							</fo:block>									
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="0.5px solid black" border-top="0.5px solid black">																			
							<fo:block font-size="8pt" text-align="right" font-weight="bold">										
								<xsl:variable name="dayTotal" select="string(sum(/root/modules/CostDailysheet/CostDailysheet[dailyUid = $dailyUid]/itemTotal/@rawNumber))"/>								
								<xsl:value-of select="d2Utils:formatNumber($dayTotal,'###,###,##0.00')"/>
							</fo:block>									
						</fo:table-cell>						
						<fo:table-cell padding="2px" border-top="0.5px solid black">																			
							<fo:block font-size="8pt" text-align="right" font-weight="bold">
								
								<xsl:variable name="wellTotal" select="sum(/root/modules/DailyCostItems/DailyCostItem/costToDate)"/>
								<xsl:value-of select="$primaryCurrency"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="d2Utils:formatNumber(string($wellTotal,'###,###,##0.00')"/>
								
							</fo:block>									
						</fo:table-cell>
					</fo:table-row> -->
				</fo:table-body>
			</fo:table>
		</fo:block>		
	</xsl:template>
	
	<xsl:template name="basicwellheader">	 	
      <xsl:variable name="welldata" select="/root/modules/Well/Well"/>
      <xsl:variable name="primary_currency" select="/root/modules/CostAfeMaster/dynaAttr/primaryCurrency"/>
	 <xsl:variable name="afeTotal" select="string(sum(/root/modules/CostAfeMaster/CostAfeMaster/CostAfeDetail/itemTotal/@rawNumber))"/>
	 <xsl:variable name="wellCostToDate" select="sum(/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost/@rawNumber)"/>
	 <xsl:variable name="amountSpentPriorToSpud" select="sum(/root/modules/Operation/Operation/amountSpentPriorToSpud/@rawNumber)"/>
	 <!--  <xsl:variable name="wellTotal" select="$wellCostToDate + $amountSpentPriorToSpud"/> -->
      <fo:table width="100%" table-layout="fixed"
         space-after="0.5cm" border-bottom="1px solid black">
         <fo:table-column column-width="proportional-column-width(0.5)"/>
         <fo:table-column column-width="proportional-column-width(1.5)"/>
         <fo:table-column column-width="proportional-column-width(1)"/>
         <fo:table-column column-width="proportional-column-width(1)"/>
         <fo:table-body>
            <fo:table-row font-size="8pt" font-weight="bold">
               <fo:table-cell number-columns-spanned="4">
                  <fo:block color="white">!</fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row font-size="8pt" font-weight="bold">
               <fo:table-cell padding="2px">
                  <fo:block> Wellname: </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="2px">
                  <fo:block>
                     <xsl:value-of select="$welldata/wellName"/>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell text-align="left" padding="2px">
                  <fo:block>
                     <xsl:value-of select="$welldata/afetype"/> Total AFE: </fo:block>
               </fo:table-cell>
               <fo:table-cell text-align="right" padding="2px">
                  <fo:block>
					 <xsl:value-of select="/root/modules/CostAfeMaster/dynaAttr/primaryCurrency"/><fo:inline color="white">.</fo:inline>
					 <xsl:value-of select="d2Utils:formatNumber(string($afeTotal),'###,###,##0.00')"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row font-size="8pt" font-weight="bold">
               <fo:table-cell padding="2px">
                  <fo:block> Rig: </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="2px">
                  <fo:block>
                     <xsl:value-of select="/root/modules/RigInformation/RigInformation[rigInformationUid=$reportDaily/rigInformationUid]/rigName"/>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell text-align="left" padding="2px">
                  <fo:block>Total Cost To Date: </fo:block>
               </fo:table-cell>
               <fo:table-cell text-align="right" padding="2px">
                  <fo:block>
               		<xsl:value-of select="$primary_currency"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="d2Utils:formatNumber(string($wellCostToDate),'###,###,##0.00')"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row font-size="8pt" font-weight="bold">
               <fo:table-cell padding="2px">
                  <fo:block>Date: </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="2px">
                  <fo:block><xsl:value-of select="d2Utils:formatDateFromEpochMS($reportDaily/reportDatetime/@epochMS,'dd MMM yyyy')"/> (Day #<xsl:value-of
                        select="$reportDaily/reportNumber"/>) </fo:block>
               </fo:table-cell>
               <fo:table-cell text-align="left" padding="2px">
                  <fo:block>Diff (+/-): </fo:block>
               </fo:table-cell>
               <fo:table-cell text-align="right" padding="2px">
               		<xsl:variable name="afe_actual_diff" select="string($afeTotal - $wellCostToDate)"/>
	                  <xsl:choose>
	                     <xsl:when test="$afe_actual_diff &gt;= 0">
	                        <fo:block> 
	                        	<xsl:value-of select="$primary_currency"/><fo:inline color="white">.</fo:inline>
	                        	<xsl:value-of select="d2Utils:formatNumber(string($afe_actual_diff),'###,###,##0.00')"/>
	                        </fo:block>
	                     </xsl:when>
	                     <xsl:otherwise>
	                        <fo:block color="red"> 
			                    <xsl:choose>
			                       <xsl:when test="number($afe_actual_diff)">
			                       		<xsl:value-of select="$primary_currency"/>
	                        			<xsl:value-of select="d2Utils:formatNumber(string($afe_actual_diff),'###,###,##0.00')"/>
			                       </xsl:when>
			                       <xsl:otherwise>$0.00</xsl:otherwise>
			                    </xsl:choose>
	                        </fo:block>
	                     </xsl:otherwise>
	                  </xsl:choose>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row font-size="8pt" font-weight="bold">
               <fo:table-cell padding="2px">
                  <fo:block color="white">!</fo:block>
               </fo:table-cell>
               <fo:table-cell padding="2px">
                  <fo:block color="white">!</fo:block>
               </fo:table-cell>
               <fo:table-cell text-align="left" padding="2px">
                  <fo:block>Total Day Cost: </fo:block>
               </fo:table-cell>
               <fo:table-cell text-align="right" padding="2px">
                 <fo:block>
					<xsl:variable name="itemDayTotals">
						<xsl:for-each select="/root/modules/CostDailysheet/CostDailysheet[dailyUid=$dailyUid]">
							<number><xsl:value-of select="itemTotal/@rawNumber"/></number>
						</xsl:for-each>	
					</xsl:variable> 
					<xsl:value-of select="$primary_currency"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="d2Utils:formatNumber(string(sum(exsl:node-set($itemDayTotals)/number)), '###,###,##0.00')"/>
                </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>
	
</xsl:stylesheet>



