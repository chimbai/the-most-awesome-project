<?xml version="1.0" encoding="utf-8"?>
<!-- edited with XMLSPY v5 U (http://www.xmlspy.com) by Kenny (IDS) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:output method="xml"/>
	<xsl:preserve-space elements="*"/>
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1.5cm">
					<fo:region-body margin-top="2.5cm" margin-bottom="1.5cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="1cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table inline-progression-dimension="19cm" table-layout="fixed"
						space-after="12pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="12pt">
									<fo:block>
										<fo:external-graphic content-width="scale-to-fit"
											width="100pt" content-height="50%" scaling="uniform" src="url(d2://images/company_logo.jpg)"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell number-columns-spanned="2" font-size="6pt"
									display-align="center">
									<fo:block text-align="right" font-weight="bold">
										<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="12pt">
											<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
											<fo:table-column column-number="2" column-width="proportional-column-width(33)"/>
											<fo:table-body space-before="6pt" space-after="6pt">
												<fo:table-row>
													<fo:table-cell font-size="9pt">
														<fo:block>
															<fo:inline text-decoration="underline">
															WELL OPERATION ACTIVITY REPORT #<xsl:value-of select="/root/modules/Daily/Daily/dayNumber"/>
															</fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>												
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="19cm" table-layout="fixed"
						space-after="12pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS, 20080313, D2_IDS DEMO_intv
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="8pt">
									<fo:block text-align="right">
										Page <fo:page-number/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
		                        <fo:table-cell font-size="6pt" number-columns-spanned="2">
		                           <fo:block text-align="left">Printed on
		                           <xsl:variable name="printdate" select="/root/modules/Daily/Daily/dayDate"/>
		                           <xsl:value-of select="d2Utils:getSystemDate('dd MMM yyyy')"/>
		                           </fo:block>
		                        </fo:table-cell>
		                     </fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" font-size="8pt">
					<fo:block>
						<fo:table inline-progression-dimension="19cm" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
							<fo:table-body>
								<fo:table-row keep-together="always">
									<fo:table-cell>
										<fo:block>
											<xsl:apply-templates select="/root/modules/Well/Well"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block>
											<xsl:if	test="count(/root/modules/Activity/Activity) &gt; 0">
												<xsl:apply-templates select="/root/modules/Activity"/>
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block>
										<xsl:if	test="count(/root/modules/NextDayActivity/Activity) &gt; 0">
											<xsl:apply-templates select="/root/modules/NextDayActivity"/>
										</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block keep-together="always">
										<xsl:if	test="count(/root/modules/GeneralComment/GeneralComment) &gt; 0">
											<xsl:apply-templates select="/root/modules/GeneralComment"/>
										</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:apply-templates select="/root/modules/Bharun/Bharun"/>
								<fo:table-row>
									<fo:table-cell>
										<fo:block keep-together="always">
										<xsl:if	test="count(/root/modules/PersonnelOnSite/PersonnelOnSite) &gt; 0">
											<xsl:apply-templates select="/root/modules/PersonnelOnSite"/>
										</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>														
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>				
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

	<!-- wells section BEGIN -->
	<xsl:template match="modules/Well/Well">
		<fo:table inline-progression-dimension="100%" font-size="8pt" table-layout="fixed" border="0.5px solid black">
		<fo:table-column column-width="proportional-column-width(1)" />
		<fo:table-column column-width="proportional-column-width(1)" />
		<fo:table-column column-width="proportional-column-width(1)" />
		<fo:table-column column-width="proportional-column-width(1)" />
		<xsl:variable name="printdate" select="/root/modules/Daily/Daily/dayDate"/>
		<fo:table-body>
            <fo:table-row font-weight="bold" text-align="center">
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">Location</fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">Well Number</fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">Prepared By</fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">Report Date</fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row text-align="center">
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                     <xsl:value-of select="field" />
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                     <xsl:value-of select="wellName" />
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                     <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/sentby" />
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                     <xsl:value-of select="d2Utils:formatDateFromEpochMS($printdate/@epochMS,'dd MMM yyyy')" />
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row font-weight="bold" text-align="center">
               <fo:table-cell number-columns-spanned="4" padding="0.05cm" border-bottom="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">PROGRAMME / PLAN</fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row font-weight="bold" text-align="center">
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">Activity</fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">Programme Ref.</fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">AFI / AFE Ref.</fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)"><!-- Planned Duration (hrs) --> <fo:inline color="rgb(224,224,224)">.</fo:inline> </fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row text-align="center">
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                     <xsl:value-of select="/root/modules/Operation/Operation/operationCode/@lookupLabel" />
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                     <xsl:value-of select="projectname" />
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                     <xsl:value-of select="afebasis" />
                  </fo:block>
               </fo:table-cell>                
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" wrap-option="wrap">
               <!--
                  <fo:block>	
                  	<xsl:variable name="raw_planneddays" select="V_daily.planneddays"/>
                  	<xsl:variable name="planneddays" select="normalize-space($raw_planneddays)"/>
                  	
					<xsl:choose>
						<xsl:when test="string-length($planneddays) &gt; 0">
							<xsl:variable name="plannedhours" select="$planneddays * 24"/>
							<xsl:value-of select="format-number($plannedhours, '#')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="V_daily.planneddays"/>
						</xsl:otherwise>
					</xsl:choose>
					
                  </fo:block>
               -->
               </fo:table-cell>               
            </fo:table-row>
         </fo:table-body>
      </fo:table> 
      <fo:table inline-progression-dimension="19cm" font-size="8pt" table-layout="fixed" border="0.5px solid black">
         <fo:table-column column-width="proportional-column-width(3)" />
         <fo:table-column column-width="proportional-column-width(3)" />
         <fo:table-column column-width="proportional-column-width(2)" />
         <fo:table-column column-width="proportional-column-width(2)" />
         <fo:table-column column-width="proportional-column-width(2)" />
         <fo:table-body>
            <fo:table-row font-weight="bold" text-align="center">
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">Planned Deferred Prod.</fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">Deferred Prod.</fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">Budget Cost</fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">Cost to Date</fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" wrap-option="wrap">
                  <fo:block background-color="rgb(224,224,224)">Current POB</fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row text-align="center">
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                     <xsl:value-of select="planneddeferredprod" /><fo:inline color="white">.</fo:inline>
                     <xsl:value-of select="planneddeferredprod/@uomSymbol" />
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                     <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daydeferredprod" /><fo:inline color="white">.</fo:inline>
                     <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daydeferredprod/@uomSymbol" />
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                  	<!--
                     <xsl:value-of select="translate(//unitsymbols/U_wells.afe,'()','')" /> <xsl:value-of select="format-number(V_wells.afe, '##,###,###,###')" />
                     --> 
						<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/><fo:inline color="white">.</fo:inline>
						<xsl:value-of select="d2Utils:formatNumber(/root/modules/Operation/Operation/afe,'#,###')"/>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                  <!--
                     <xsl:value-of select="translate(//unitsymbols/U_daily.cumcost,'()','')" /> <xsl:value-of select="format-number(V_daily.cumcost, '###,###,###')" />
                  -->
						<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost/@uomSymbol"/><fo:inline color="white">.</fo:inline>
						<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost,'#,###')"/>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" wrap-option="wrap">
                  <fo:block>
                     <!--<xsl:value-of select="sum(//pobpersonnelsum_records/pobpersonnelsum_record/pax)" />-->
                     <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/comment_pob"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <!-- Apply HSE template -->
      
      <xsl:if test="count(/root/modules/HseIncident/HseIncident) &gt;= 1">
      	<xsl:apply-templates select="/root/modules/HseIncident"/>
      </xsl:if>
      
      <fo:table inline-progression-dimension="100%" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="2pt">
         <fo:table-column column-number="1" column-width="proportional-column-width(100)" />
         <fo:table-header>
            <fo:table-row font-size="9pt">
               <fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border="0.25px solid black" font-weight="bold" text-align="center" background-color="rgb(224,224,224)">
                  <fo:block>FORECAST for Next 24 hours</fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-header>
         <fo:table-body>
            <fo:table-row font-size="8pt">
               <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
                  <fo:block linefeed-treatment="preserve">
                     <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPlanSummary"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>           
	</xsl:template>
	<!-- End Well section -->
	
	<!-- hse incident or summary BEGIN -->
	<xsl:template match="modules/HseIncident">
		<fo:table width="100%" table-layout="fixed" font-size="8pt" margin-top="2pt"
			border-left="0.5px solid black" border-right="0.5px solid black" border-bottom="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(40)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm" padding-bottom="-0.05cm"
						number-columns-spanned="4" background-color="rgb(224,224,224)" border-top="0.5px solid black"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							HSE Summary
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>				
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Events</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Date Of Last</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Days Since</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Description</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="HseIncident"/>
			</fo:table-body>
		</fo:table>						
	</xsl:template>
	
	<xsl:template match="HseIncident">
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="incidentCategory/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(hseEventdatetime/@epochMS,'dd MMM yyyy HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="dynaAttr/daysLapsed"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:value-of select="description"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- hse section END -->
	
	<!-- activity section BEGIN -->
	<xsl:template match="modules/Activity">	
	<xsl:if	test="Activity/dayPlus = 0">	
		<fo:block>			
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" border-left="0.5px solid black"
				border-right="0.5px solid black" border-bottom="0.5px solid black" margin-top="2pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(50)"/>
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm" padding-bottom="-0.05cm" background-color="rgb(224,224,224)"
							number-columns-spanned="8" border-top="0.5px solid black" border-left="0.1px solid black"
							border-right="0.1px solid black">
							<fo:block font-weight="bold" font-size="10pt"> Operations For Period 0000 Hrs 
								to 2400 Hrs On <xsl:value-of select="/root/modules/Daily/Daily/dayDate"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>														
					<fo:table-row>						
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">PHSE</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">CLS</fo:block>
							<fo:block text-align="center">(RC)</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">OP</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">From</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">To</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">Hrs</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">Depth</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">Activity Description</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
					<xsl:apply-templates select="Activity[dayPlus=0]" mode="currentday"/>
				</fo:table-body>
			</fo:table>
		</fo:block>	
	</xsl:if>
	
	<xsl:if	test="Activity/dayPlus = 1">
		<fo:block>			
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" 
				table-omit-header-at-break="true" border-left="0.5px solid black" border-right="0.5px solid black"
				border-bottom="0.5px solid black" margin-top="2pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(50)"/>
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm" padding-bottom="-0.05cm" background-color="rgb(224,224,224)"
							number-columns-spanned="8" border-top="0.5px solid black" border-left="0.1px solid black"
							border-right="0.1px solid black">
							<xsl:variable name="date" select="string(/root/modules/Daily/Daily/dayDate/@epochMS + 86400000)"/>
							<fo:block font-weight="bold" font-size="10pt"> Operations For Period 0000 Hrs 
								to 0600 Hrs On <xsl:value-of select="d2Utils:formatDateFromEpochMS($date,'dd MMM yyyy')"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>														
					<fo:table-row>						
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">PHSE</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">CLS</fo:block>
							<fo:block text-align="center">(RC)</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">OP</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">From</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">To</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">Hrs</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">Depth</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">Activity Description</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
					<xsl:apply-templates select="Activity[dayPlus=1 and startDatetime/@epochMS &lt; d2Utils:parseDateAsEpochMS('0600')]" mode="nextday"/>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:if>				
	</xsl:template>
	<!-- 
	<xsl:template match="modules/Activity/Activity" >		
		<xsl:call-template name="ActivityItemDetails"/>
	</xsl:template>
	-->
	<xsl:template match="Activity" mode="currentday">
		<fo:table-row keep-together="always">
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="phaseCode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">					
					<xsl:value-of select="classCode"/>					
				</fo:block>
				<xsl:if test="string(rootCauseCode) != ''">
				<fo:block text-align="center">					
					(<xsl:value-of select="rootCauseCode"/>)					
				</fo:block>	
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="taskCode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="d2Utils:formatNumber(activityDuration,'##0.00')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<!--<xsl:value-of select="d2Utils:applyUom(depthMdMsl)"/>-->
					<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="depthMdMsl/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:value-of select="activityDescription"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="Activity" mode="nextday">
		<fo:table-row keep-together="always">
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="phaseCode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">					
					<xsl:value-of select="classCode"/>					
				</fo:block>
				<xsl:if test="string(rootCauseCode) != ''">
				<fo:block text-align="center">					
					(<xsl:value-of select="rootCauseCode"/>)					
				</fo:block>	
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="taskCode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS &gt; d2Utils:parseDateAsEpochMS('0600')">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(string(d2Utils:parseDateAsEpochMS('0600')),'HH:mm')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS &gt; d2Utils:parseDateAsEpochMS('0600')">
							<xsl:variable name="duration" select="d2Utils:parseDateAsEpochMS('0600') - startDatetime/@epochMS"/>
							<xsl:value-of select="d2Utils:formatNumber(string($duration div (60*60*1000)),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="d2Utils:formatNumber(activityDuration,'##0.00')"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<!--<xsl:value-of select="d2Utils:applyUom(depthMdMsl)"/>-->
					<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="depthMdMsl/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS &gt; d2Utils:parseDateAsEpochMS('0600')">
							<xsl:value-of select="'(IN PROGRESS) '"/><xsl:value-of select="activityDescription"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="activityDescription"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- activity section END -->
	
	<!-- next day activity section BEGIN -->
	<xsl:template match="modules/NextDayActivity">
		<fo:block>			
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" border-left="0.5px solid black"
				border-right="0.5px solid black" border-bottom="0.5px solid black" margin-top="2pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(50)"/>
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm" padding-bottom="-0.05cm" background-color="rgb(224,224,224)"
							number-columns-spanned="8" border-top="0.5px solid black" border-left="0.1px solid black"
							border-right="0.1px solid black">
							<fo:block font-weight="bold" font-size="10pt"> Operations For Period 0000 Hrs 
								to 0600 Hrs On <xsl:value-of select="d2Utils:formatDateFromEpochMS($date,'dd MMM yyyy')"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>														
					<fo:table-row>						
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">PHSE</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">CLS</fo:block>
							<fo:block text-align="center">(RC)</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">OP</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">From</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">To</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">Hrs</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">Depth</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
							<fo:block text-align="center">Activity Description</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
					<xsl:apply-templates select="Activity[startDatetime/@epochMS &lt; d2Utils:parseDateAsEpochMS('0600') and dayPlus/@rawNumber='0']"/>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>
	<xsl:template match="modules/NextDayActivity/Activity" >		
		<xsl:call-template name="NextDayActivityItemDetails"/>
	</xsl:template>
	
	<xsl:template name="NextDayActivityItemDetails">
		<fo:table-row keep-together="always">
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="phaseCode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">					
					<xsl:value-of select="classCode"/>					
				</fo:block>
				<xsl:if test="string(rootCauseCode) != ''">
				<fo:block text-align="center">					
					(<xsl:value-of select="rootCauseCode"/>)					
				</fo:block>	
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="taskCode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">					
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS &gt; d2Utils:parseDateAsEpochMS('0600')">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(string(d2Utils:parseDateAsEpochMS('0600')),'HH:mm')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS &gt; d2Utils:parseDateAsEpochMS('0600')">
							<xsl:variable name="duration" select="d2Utils:parseDateAsEpochMS('0600') - startDatetime/@epochMS"/>
							<xsl:value-of select="d2Utils:formatNumber(string($duration div (60*60*1000)),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="d2Utils:formatNumber(activityDuration,'##0.00')"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<!--<xsl:value-of select="d2Utils:applyUom(depthMdMsl)"/>-->
					<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="depthMdMsl/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS &gt; d2Utils:parseDateAsEpochMS('0600')">
							<xsl:value-of select="'(IN PROGRESS) '"/><xsl:value-of select="activityDescription"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="activityDescription"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<!-- next day activity section END -->
	
	<!-- gencomments section BEGIN -->
	<xsl:template match="modules/GeneralComment">	
		<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt"
		 	border-left="0.5px solid black" border-right="0.5px solid black" border-bottom="0.5px solid black">
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(80)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="2" border-bottom="0.25px solid black" padding="0.05cm" padding-bottom="-0.05cm"
						background-color="rgb(224,224,224)" border-top="0.5px solid black"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block font-size="10pt" font-weight="bold">
							General Comments 00:00 to 24:00 Hrs on <xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Daily/Daily/dayDate/@epochMS,'dd MMM yyyy')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
				<fo:table-row>
					<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Category</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Comments</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates name="GeneralComment"/>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="GeneralComment">
	<xsl:variable name="selected_category" select="category"/>
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block>
					<xsl:value-of select="$selected_category/@lookupLabel"/>
				</fo:block>
				<fo:block><fo:inline color="white">.</fo:inline></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block linefeed-treatment="preserve">
					<xsl:value-of select="comments"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- gencomments section END -->
	
	<!-- bha section START -->	
	<xsl:template match="modules/Bharun/Bharun">
	<fo:table-row keep-together="always">
			<fo:table-cell>
				<fo:table width="100%" table-layout="fixed" font-size="8pt" 
					wrap-option="wrap" space-after="2pt" margin-top="2pt" border-left="0.5px solid black"
					border-right="0.5px solid black" border-bottom="0.5px solid black">
					<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>								
					<fo:table-body>												
						<fo:table-row font-size="8pt">
							<fo:table-cell>
								<fo:block space-after="2pt">	                                            
									<fo:table width="100%" table-layout="fixed" font-size="8pt"
										wrap-option="wrap" space-after="2pt" table-omit-header-at-break="true">
										<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>								
										<fo:table-header>
											<fo:table-row>
												<fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
													number-columns-spanned="2" background-color="rgb(224,224,224)" border-top="0.5px solid black"
													border-left="0.1px solid black" border-right="0.1px solid black">														
													<fo:block font-size="10pt" font-weight="bold"> Toolstring # <xsl:value-of
														select="bhaRunNumber"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-header>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> BHA Description: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left" linefeed-treatment="preserve">
														<xsl:value-of select="descr"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>	
										</fo:table-body>
									</fo:table>	                                            
								</fo:block>
							</fo:table-cell>	
						</fo:table-row>		                              	
			            <!-- Start Bha Component -->
			            <fo:table-row font-size="8pt">
							<fo:table-cell>
			                	<fo:block space-after="2pt">
			                    	<xsl:if test="count(BhaComponent) &gt;= 1"> <!-- check if in bhacomponent got record or not -->
			                        	<xsl:call-template name="BhaComponent">
											<xsl:with-param name="bharunUid" select="bharunUid"/>
										</xsl:call-template>
									</xsl:if>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<!-- End Bha Component -->
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template name="BhaComponent">
		<xsl:param name="bharunUid"/>
		<fo:table inline-progression-dimension="100%" table-layout="fixed" margin-top="2pt" table-omit-header-at-break="true"
			border-top="0.25px solid black">						
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(8)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(14)"/>			
			<fo:table-header>				
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Equipment</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Description</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Length</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">OD</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">ID</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Serial #</fo:block>
					</fo:table-cell>					
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="/root/modules/Bharun/Bharun/BhaComponent[bharunUid=$bharunUid]"/>		
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template match="modules/Bharun/Bharun/BhaComponent">
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="type"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:value-of select="descr"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="jointlength"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="jointlength/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="componentOd"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="componentOd/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="componentId"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="componentId/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="serialNum"/>
				</fo:block>
			</fo:table-cell>			
		</fo:table-row>
	</xsl:template>	
	<!-- bha section END -->
	
	<!-- pob section BEGIN -->
	<xsl:template match="modules/PersonnelOnSite">			
		<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt"
			border-left="0.5px solid black" border-right="0.5px solid black" border-bottom="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm" padding-bottom="-0.05cm"
						number-columns-spanned="4" background-color="rgb(224,224,224)" border-top="0.5px solid black"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Personnel On Site
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
			</fo:table-header>			
			<fo:table-body font-size="8pt">
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Job Title</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Personnel</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Company</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Pax</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:apply-templates select="PersonnelOnSite"/>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3" border-right="0.25px solid black" border-top="0.25px solid black" padding="0.05cm">
						<fo:block text-align="right">Total</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-top="0.25px solid black">
						<fo:block text-align="right">
							<xsl:value-of select="sum(PersonnelOnSite/pax)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
			</fo:table-body>
		</fo:table>											
	</xsl:template>

	<xsl:template match="PersonnelOnSite">
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="crewPosition"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="crewName"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="crewCompany/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="pax"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- pob section END -->
	
</xsl:stylesheet>
