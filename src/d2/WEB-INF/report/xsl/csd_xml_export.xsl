<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text"/>
    <xsl:template match="/">
    	<xsl:call-template name="start_file"></xsl:call-template>
    	<xsl:apply-templates select="/root/modules/Well/Well"></xsl:apply-templates>
    	<xsl:apply-templates select="/root/modules/Formation/Formation"></xsl:apply-templates>
    	<xsl:apply-templates select="/root/modules/CasingSection"></xsl:apply-templates>
    	<xsl:apply-templates select="/root/modules/SurveyReference"></xsl:apply-templates>
    	<xsl:apply-templates select="/root/modules/Bharun"></xsl:apply-templates>
    	<xsl:apply-templates select="/root/modules/ProductionStringFish"></xsl:apply-templates>
    	<xsl:call-template name="end_file"></xsl:call-template>	
    </xsl:template>
    
    <xsl:template name="start_file">
    	<xsl:text>&lt;response version="1.0"&gt;</xsl:text>
    	<xsl:text>&lt;success&gt;true&lt;/success&gt;</xsl:text>
    	<xsl:text>&lt;error-message/&gt;</xsl:text>
    	<xsl:text>&lt;data&gt;</xsl:text>
        <xsl:text>&lt;COMPLETION&gt;</xsl:text>
    </xsl:template>
    
    <xsl:template name="end_file">
        <xsl:text>&lt;/COMPLETION&gt;</xsl:text>
        <xsl:text>&lt;/data&gt;</xsl:text>
        <xsl:text>&lt;/response&gt;</xsl:text>
    </xsl:template>
    
    <xsl:template match="modules/Well/Well">
        <xsl:text>&lt;WELL_COMPLETION Wellbore="</xsl:text><xsl:value-of select="wellName"/><xsl:text>" Id="</xsl:text><xsl:value-of select="wellUid"/><xsl:text>"&gt;</xsl:text>
        <xsl:text>&lt;WELL_COMPLETION_ID&gt;</xsl:text><xsl:value-of select="wellUid"/><xsl:text>&lt;/WELL_COMPLETION_ID&gt;</xsl:text>
        <xsl:text>&lt;WELL_NAME&gt;</xsl:text><xsl:value-of select="wellName"/><xsl:text>&lt;/WELL_NAME&gt;</xsl:text>
        <xsl:text>&lt;/WELL_COMPLETION&gt;</xsl:text>
    </xsl:template>
    
    <xsl:template match="modules/Formation/Formation">
        <xsl:text>&lt;MINERAL_ZONE WellCompletion="</xsl:text><xsl:value-of select="/root/modules/Well/Well/wellUid"/><xsl:text>" Id="</xsl:text><xsl:value-of select="formationUid"/><xsl:text>"&gt;</xsl:text>    	
        <xsl:text>&lt;MINERAL_ZONE_TYPE&gt;</xsl:text><xsl:value-of select="formationName"/><xsl:text>&lt;/MINERAL_ZONE_TYPE&gt;</xsl:text>
        <xsl:text>&lt;MD_TOP&gt;</xsl:text><xsl:value-of select="thinkness_topmd"/><xsl:text>&lt;/MD_TOP&gt;</xsl:text>
        <xsl:text>&lt;TVD_TOP&gt;</xsl:text><xsl:value-of select="thinkness_toptvd"/><xsl:text>&lt;/TVD_TOP&gt;</xsl:text>
        <xsl:text>&lt;/MINERAL_ZONE&gt;</xsl:text>    	
    </xsl:template>
    
    <xsl:template match="modules/CasingSection">
    	<xsl:apply-templates select="CasingSection"/>
    </xsl:template>
    
    <xsl:template match="CasingSection">
        <xsl:text>&lt;CASING_STRING wellid="</xsl:text><xsl:value-of select="/root/modules/Well/Well/wellUid"/><xsl:text>" casingrun_id="</xsl:text><xsl:value-of select="casingSectionUid"/><xsl:text>"&gt;</xsl:text>
        <xsl:text>&lt;SIZE_NUM&gt;</xsl:text><xsl:value-of select="casingOd"/><xsl:text>&lt;/SIZE_NUM&gt;</xsl:text>
        <xsl:text>&lt;MD_BOTTOM&gt;</xsl:text><xsl:value-of select="depthHangOffSurfaceMdMsl"/><xsl:text>&lt;/MD_BOTTOM&gt;</xsl:text>
        <xsl:text>&lt;TVD_BOTTOM&gt;</xsl:text><xsl:value-of select="depthHangOffSurfaceMdMsl"/><xsl:text>&lt;/TVD_BOTTOM&gt;</xsl:text>
        <xsl:text>&lt;WEIGHT&gt;</xsl:text><xsl:value-of select="weightActual"/><xsl:text>&lt;/WEIGHT&gt;</xsl:text>
        <xsl:text>&lt;/CASING_STRING&gt;</xsl:text>    	
    </xsl:template>
    
    <xsl:template match="modules/SurveyReference">
    	<xsl:variable name="surveyReferenceUid" select="SurveyReference/surveyReferenceUid"/>
	    <xsl:for-each select="SurveyReference/SurveyStation[surveyReferenceUid=$surveyReferenceUid]">
	        <xsl:text>&lt;SURVEY_POINT WellCompletion="</xsl:text><xsl:value-of select="/root/modules/Well/Well/wellUid"/><xsl:text>" Id="</xsl:text><xsl:value-of select="surveyStationUid"/><xsl:text>"&gt;</xsl:text>
	        <xsl:text>&lt;MD&gt;</xsl:text><xsl:value-of select="depthMdMsl"/><xsl:text>&lt;/MD&gt;</xsl:text>
	        <xsl:text>&lt;INCL&gt;</xsl:text><xsl:value-of select="inclinationAngle"/><xsl:text>&lt;/INCL&gt;</xsl:text>
	        <xsl:text>&lt;AZIM&gt;</xsl:text><xsl:value-of select="azimuthAngle"/><xsl:text>&lt;/AZIM&gt;</xsl:text>
	        <xsl:text>&lt;/SURVEY_POINT&gt;</xsl:text>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="modules/Bharun">
    	<xsl:apply-templates select="Bharun"/>
    </xsl:template>
    
    <xsl:template match="Bharun">
        <xsl:text>&lt;WELL_HOLE WellCompletion="</xsl:text><xsl:value-of select="/root/modules/Well/Well/wellUid"/><xsl:text>" Id="</xsl:text><xsl:value-of select="Bitrun/bitrunUid"/><xsl:text>"&gt;</xsl:text>
        <xsl:text>&lt;MD_TOP&gt;</xsl:text><xsl:value-of select="Bitrun/depthInMdMsl"/><xsl:text>&lt;/MD_TOP&gt;</xsl:text>
        <xsl:text>&lt;MD_BOTTOM&gt;</xsl:text><xsl:value-of select="Bitrun/depthOutMdMsl"/><xsl:text>&lt;/MD_BOTTOM&gt;</xsl:text>
        <xsl:text>&lt;SIZE_NUM&gt;</xsl:text><xsl:value-of select="Bitrun/bitDiameter"/><xsl:text>&lt;/SIZE_NUM&gt;</xsl:text>
        <xsl:text>&lt;/WELL_HOLE&gt;</xsl:text>    	
    </xsl:template>
    
    <xsl:template match="modules/ProductionStringFish">
    	<xsl:apply-templates select="ProductionStringFish"/>
    </xsl:template>
    
    <xsl:template match="ProductionStringFish">
    	<xsl:text>&lt;FISH WellCompletion="</xsl:text><xsl:value-of select="/root/modules/Well/Well/wellUid"/><xsl:text>" Id="</xsl:text><xsl:value-of select="Bitrun/bitrunUid"/><xsl:text>"&gt;</xsl:text>
    	<xsl:text>&lt;MD_TOP&gt;</xsl:text><xsl:value-of select="depthMdMsl"/><xsl:text>&lt;/MD_TOP&gt;</xsl:text>
    	<xsl:text>&lt;TVD_TOP&gt;</xsl:text><xsl:value-of select="depthTvdMsl"/><xsl:text>&lt;/TVD_TOP&gt;</xsl:text>
    	<xsl:text>&lt;/FISH&gt;</xsl:text>
    </xsl:template>

</xsl:stylesheet>