<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"   
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">    
	            
	<xsl:import href="./ReportTemplate/well_record/dgr_wells_record_type1.xsl" />  
	<xsl:import href="./ReportTemplate/daily_record/dgr_daily_record_type1.xsl" />   
	<xsl:import href="./ReportTemplate/formation_record/dgr_formation_record_geology_type1.xsl" />       
	<xsl:import href="./ReportTemplate/coaldesorption_record/dgr_coal_record_geology_type1.xsl" />           
	<xsl:import href="./ReportTemplate/coaldesorption_record/dgr_coal_record_geology_summary_type1.xsl" />  
	 
    <xsl:import href="./ReportTemplate/coaldesorption_record/dgr_desorbed_gas_record_geology_type1.xsl" />    
    <xsl:import href="./ReportTemplate/coaldesorption_record/dgr_raw_desorbed_gas_1.xsl" />              
    <xsl:import href="./ReportTemplate/coaldesorption_record/dgr_raw_desorbed_gas_2.xsl" />                      
      
	<xsl:import href="./ReportTemplate/coaldesorption_record/dgr_lab_sum_record_geology_type1.xsl" />                   
	
	<xsl:import href="./ReportTemplate/lithology_record/dgr_litho_sum_record_geology_type1.xsl" /> 
	<xsl:import href="./ReportTemplate/lithology_record/dgr_litho_sum_part1.xsl" /> 
	
	<xsl:import href="./ReportTemplate/lithology_record/dgr_oilshows_record_geology_type1.xsl" />
	 
	<xsl:import href="./ReportTemplate/geolshows_record/dgr_geolshows_record_type2.xsl" />   
	<xsl:import href="./ReportTemplate/gasreadings_record/dgr_gasreadings_record_geology_type1.xsl" />
	<xsl:import href="./ReportTemplate/porepressure_record/dgr_porepressure_record_type2.xsl" />      
	<xsl:import href="./ReportTemplate/gencomment_record/dgr_gencomments_record_type1.xsl" />            
	<xsl:import href="./ReportTemplate/daily_record/dgr_daily_record2_type1.xsl" />     
	<xsl:import href="./ReportTemplate/mudlogging_record/dgr_mudlogging_record.xsl" />
	                                              
	                    
	<xsl:output method="xml" />     
	<xsl:preserve-space elements="*" />          
	<xsl:template match="/">      
		<fo:root font-family="Arial">    
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-left="0.7cm" margin-right="0.7cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/> 
					<fo:region-before extent="3cm"/>      
					<fo:region-after extent="1.5cm" margin-bottom="2cm"/>
				</fo:simple-page-master>      
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before"> 
					<fo:block>
					<fo:table inline-progression-dimension="20cm" table-layout="fixed" space-before="12pt" padding="5px">
						<fo:table-column column-number="1" column-width="proportional-column-width(34)" />
						<fo:table-column column-number="2" column-width="proportional-column-width(33)" />  
						<fo:table-column column-number="3" column-width="proportional-column-width(33)" />
						  
						<fo:table-body space-before="10pt">         
							<fo:table-row>          
								<fo:table-cell font-size="12pt"  padding-left="3" padding="13px"> 
									<fo:block> 
										<fo:external-graphic    
											content-width="scale-to-fit" width="100pt"
											content-height="80%" scaling="uniform"
											src="url(d2://images/company_logo.jpg)" />	     									
									</fo:block> 
										         
								</fo:table-cell> 
								 
								<fo:table-cell padding="1px" font-size="6pt" display-align="center">
									<fo:block>
										
									</fo:block> 
								</fo:table-cell>
								 
								<fo:table-cell font-size="6pt" padding="5px" display-align="center">
									<fo:block text-align="right">
										<fo:table
											inline-progression-dimension="6.27cm" table-layout="fixed"
											space-after="6pt"> 
											<fo:table-column column-number="1" column-width="proportional-column-width(100)" />
											<fo:table-body>
												<fo:table-row>
													<fo:table-cell font-size="12pt" padding-right="10" padding-bottom="20">
					 									<fo:block font-weight="bold" >
															<fo:inline text-decoration="underline"><xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/completeOperationName" /></fo:inline>
														</fo:block> 		 
													</fo:table-cell>     
												</fo:table-row>
											</fo:table-body>    
										</fo:table>
									</fo:block>
								</fo:table-cell>   
							</fo:table-row>
																
						</fo:table-body>         
					</fo:table>
							
					</fo:block>
				</fo:static-content>
				 
				   
				
				<fo:static-content flow-name="xsl-region-after">
					<fo:block>
					<fo:table inline-progression-dimension="19.5cm"
						table-layout="fixed" space-after="6pt">
						<fo:table-column column-number="1"
							column-width="proportional-column-width(50)" />
						<fo:table-column column-number="2"
							column-width="proportional-column-width(50)" />  
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell font-size="8pt">
									<fo:block text-align="left">
						 				Copyright IDS, 20140618, IDS_GEOLOGY     
									</fo:block> 
								</fo:table-cell>       
								<fo:table-cell font-size="8pt">
									<fo:block text-align="right">
										Page
										<fo:page-number />
									</fo:block>
								</fo:table-cell>  
							</fo:table-row>
							<fo:table-row>   
								<fo:table-cell font-size="8pt" number-columns-spanned="2">        
									<fo:block text-align="left">
										Printed on
										<xsl:variable name="printdate"
											select="/root/modules/Daily/Daily/dayDate" />     
										<xsl:value-of
											select="d2Utils:getSystemDate('dd MMM yyyy')" />
									</fo:block>
								</fo:table-cell>  
							</fo:table-row>
						</fo:table-body>   
					</fo:table>
					</fo:block>
				</fo:static-content> 
				  
				<fo:flow flow-name="xsl-region-body" font-size="8pt">
					<fo:block>
						<fo:table inline-progression-dimension="19.5cm" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(100)" />
							<fo:table-body>
								
								<fo:table-row keep-together="always" padding="2px">
									<fo:table-cell  padding-top="2px">
										<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" border-top="0.75px solid black" border-left="0.75px solid black" border-right="0.75px solid black">
											<fo:table-column column-number="1" column-width="proportional-column-width(100)" />
											<fo:table-body>   
												<fo:table-row>
													<fo:table-cell text-align="center" font-size="10pt" background-color="rgb(37,87,147)">
														<fo:block color="white" font-weight="bold">
															<fo:inline text-decoration="underline"><xsl:value-of select="/root/modules/Well/Well/wellName" /></fo:inline>
														</fo:block>  
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table> 
									</fo:table-cell> 
								</fo:table-row>    
								
								
								<fo:table-row keep-together="always" padding="2px">
									<fo:table-cell padding-bottom="2px" >
										<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" border="0.8px solid black">
											<fo:table-column column-number="1" column-width="proportional-column-width(34)" />
											<fo:table-column column-number="2" column-width="proportional-column-width(33)" />
											<fo:table-column column-number="3" column-width="proportional-column-width(33)" /> 
											<fo:table-body>
												<fo:table-row>
													<fo:table-cell  
														padding-left="2px"
														padding-top="2px"
														padding-bottom="1px"
														font-size="8pt">
														<fo:block text-align="left" font-weight="bold">
															<fo:inline>Date: </fo:inline><xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS,'dd MMM yyyy')"/>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell 
														padding-top="2px"
														padding-bottom="1px"
														font-size="8pt">
														<fo:block text-align="center" font-weight="bold">
															<fo:inline>DAILY GEOLOGY REPORT NUMBER: </fo:inline><xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportNumber" />
														</fo:block>
													</fo:table-cell>
													<fo:table-cell  
														padding-top="2px"
														padding-bottom="1px"
														font-size="8pt"> 
									   					<fo:block text-align="right" >
															<fo:inline>( associated DDR # </fo:inline>
															<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportNumber" />
															<fo:inline> )</fo:inline>
														</fo:block> 
													</fo:table-cell>    
												</fo:table-row>          
											</fo:table-body>  
										</fo:table>
									</fo:table-cell>    
								</fo:table-row>  
								
								<!-- Well Details -->
								<xsl:if test="count(/root/modules/Well/Well) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-top="2px" padding-bottom="2px">
											<fo:block>
												<xsl:apply-templates select="/root/modules/Well/Well" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- Geology 24hr Operations Summary -->
								<xsl:if test="count(/root/modules/Daily/Daily) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-bottom="2px" padding-top="2px"> 
											<fo:block>
												<xsl:apply-templates select="/root/modules/Daily/Daily" mode="OperationsSummary" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- Mud Logging -->
								<xsl:if test="count(/root/modules/GeologistTeam/GeologistTeam) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-bottom="2px" padding-top="2px"> 
											<fo:block>
												<xsl:apply-templates select="/root/modules/GeologistTeam/GeologistTeam" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- Formation Tops -->
								<xsl:if test="count(/root/modules/Formation/Formation) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-bottom="2px" padding-top="2px">
											<fo:block>
												<xsl:apply-templates select="/root/modules/Formation" />
											</fo:block>
										</fo:table-cell> 
									</fo:table-row>
								</xsl:if>    
														
								<!-- Gross Coal Report (Summary of All Coals Intersected) -->
								<xsl:if test="count(/root/modules/CoalDesorption/CoalDesorption) &gt; 0 and (number(/root/modules/CoalDesorption/CoalDesorption/dateOnTest/@epochMS)) &lt;= (number(/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS))" mode="GrossCoalReport">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-bottom="2px" padding-top="2px">
											<fo:block>
												<xsl:apply-templates select="/root/modules/CoalDesorption" mode="GrossCoalReport"/>  
											</fo:block>    
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- Coal Desorption Summary (Raw Gas to Date) -->  
								
								<xsl:if test="count(/root/modules/CoalDesorption/CoalDesorption) &gt; 0 and (number(/root/modules/CoalDesorption/CoalDesorption/dateOnTest/@epochMS)) &lt;= (number(/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS))" mode="CoalDesorptionSummary">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-bottom="2px" padding-top="2px">                                 
											<fo:block>
												<xsl:variable name="desorption_type" 
												select="/root/modules/CoalDesorption/CoalDesorption/desorptionType" />
										          <xsl:choose>
										               <xsl:when test="($desorption_type ='desorption')">
										               		<xsl:apply-templates select="/root/modules/CoalDesorption" mode="CoalDesorptionSummary"/>
															* Predominantly non Coal lithologies 
										               </xsl:when>
										               <xsl:otherwise></xsl:otherwise>
									              </xsl:choose>  
											</fo:block>
											    
										</fo:table-cell>  
									</fo:table-row>    
<!--									<fo:table-row keep-together="always">-->
<!--										<fo:table-cell padding-bottom="2px" padding-top="2px">-->
<!--											<fo:block>-->
<!--												* Predominantly non Coal lithologies -->
<!--											</fo:block>  -->
<!--										</fo:table-cell>-->
<!--									</fo:table-row>   -->
								</xsl:if>
								
								<!-- Raw Desorbed Gas Test (To Date) -->
								<xsl:if test="count(/root/modules/CoalDesorption/CoalDesorption) &gt; 0 and (number(/root/modules/CoalDesorption/CoalDesorption/dateOnTest/@epochMS)) &lt;= (number(/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS))" mode="DesorbedGasTest">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-bottom="2px" padding-top="2px">
											<fo:block>
												<xsl:variable name="desorption_type" 
												select="/root/modules/CoalDesorption/CoalDesorption/desorptionType" />  
										          	<xsl:choose>
										               <xsl:when test="($desorption_type ='desorption')">
															<xsl:apply-templates select="/root/modules/CoalDesorption" mode="DesorbedGasTest"/>  
														</xsl:when>
														<xsl:otherwise></xsl:otherwise>
													</xsl:choose>
											</fo:block>  
										</fo:table-cell>
									</fo:table-row> 
								</xsl:if>
								
								 
								<!-- Laboratory Sample Summary -->
								
								<xsl:if test="count(/root/modules/CoalDesorption/CoalDesorption) &gt; 0 and (number(/root/modules/CoalDesorption/CoalDesorption/dateOnTest/@epochMS)) &lt;= (number(/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS))" mode="LaboratorySampleSummary">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-bottom="2px" padding-top="2px">                                 
											<fo:block>
												<xsl:variable name="labsample" 
												select="/root/modules/CoalDesorption/CoalDesorption/desorptionType" />
										          <xsl:choose>
										               <xsl:when test="($labsample ='lab_sample')">
										               		<xsl:apply-templates select="/root/modules/CoalDesorption" mode="LaboratorySampleSummary"/>
										               </xsl:when>
										               <xsl:otherwise></xsl:otherwise>
									              </xsl:choose>  
											</fo:block>    
										</fo:table-cell>  
									</fo:table-row>  
								</xsl:if>
								  
								<!-- Lithology Summary -->
								
								<xsl:if test="count(/root/modules/LithologyShows/LithologyShows) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-bottom="2px" padding-top="2px">
											<fo:block>
												<xsl:apply-templates select="/root/modules/LithologyShows" mode="Lithology_sum"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row> 
								</xsl:if>	
								  
								<!-- Oil Shows -->
								<xsl:if test="count(/root/modules/Geolshows/Geolshows) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-bottom="2px" padding-top="2px">
											<fo:block>
												<xsl:apply-templates select="/root/modules/Geolshows"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row> 
								</xsl:if>	
								
								<!-- Gas Summary --> 
								<xsl:if test="count(/root/modules/GasReadings/GasReadings) &gt; 0">
									<fo:table-row keep-together="always"> 
										<fo:table-cell padding-bottom="2px" padding-top="2px">
											<fo:block>
												<xsl:apply-templates select="/root/modules/GasReadings" />
											</fo:block>    
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>  
								
								<!-- Pore Pressure -->       
								<xsl:if test="count(/root/modules/PorePressure/PorePressure) &gt; 0">  
									<fo:table-row keep-together="always">      
										<fo:table-cell padding-bottom="2px" padding-top="2px"> 
											<fo:block>
												<xsl:apply-templates select="/root/modules/PorePressure" />
											</fo:block>   
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>       
								
								<!--General Comments -->    
							   <xsl:if test="count(/root/modules/ReportDailyDGR/ReportDaily) &gt; 0">
									<fo:table-row keep-together="always"> 
										<fo:table-cell padding-bottom="2px" padding-top="2px"> 
											<fo:block>
												<xsl:apply-templates select="/root/modules/ReportDailyDGR/ReportDaily"/>
											</fo:block>   
										</fo:table-cell>        
									</fo:table-row>
								</xsl:if>
								
								<!--06:00 Hrs Update -->
							   <xsl:if test="count(/root/modules/Daily/Daily) &gt; 0">
									<fo:table-row keep-together="always"> 
										<fo:table-cell padding-bottom="2px" padding-top="2px"> 
											<fo:block> 
												<xsl:apply-templates select="/root/modules/Daily/Daily" mode="HrsUpdate"/>
											</fo:block>   
										</fo:table-cell>        
									</fo:table-row>
								</xsl:if> 
								
								<!--Wellsite Geologist(s) -->
								<xsl:variable name="geo" select="/root/modules/ReportDailyDGR/ReportDaily/geologist" />
						              <xsl:choose>
						               <xsl:when test="($geo = '') or ($geo = 'null') ">
						               		
						               </xsl:when>
						               <xsl:otherwise>
						                <fo:table-row keep-together="always" padding="2px">
										<fo:table-cell padding-bottom="2px" padding-top="2px">
											<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" border="0.75px solid black">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)" />
												<fo:table-column column-number="2" column-width="proportional-column-width(50)" />
												<fo:table-body>
													<fo:table-row color="white" background-color="rgb(37,87,147)" >   
														<fo:table-cell number-columns-spanned="2" text-align="center" font-size="10pt" padding="2px"   
															 border-bottom="0.75px solid black">
															<fo:block font-weight="bold">
																Well Geologist(s)
															</fo:block>
														</fo:table-cell>  
													</fo:table-row>       
													<fo:table-row>
														<fo:table-cell text-align="center" font-size="8pt" padding="2px"> 
															<fo:block>
																(Days) - <xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geologist"/>
															</fo:block>  
														</fo:table-cell>
														<fo:table-cell text-align="center" font-size="8pt" padding="2px"> 
															<fo:block>
																(Nights) - <xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geologistNight"/>
															</fo:block>  
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
									</fo:table-row>
						               </xsl:otherwise>
					              </xsl:choose>
					              																			
							</fo:table-body> 
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

</xsl:stylesheet>
