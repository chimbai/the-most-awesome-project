<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!--Daily Cost Sheet Report Header -->
	<xsl:template match="modules/ReportDaily/ReportDaily" mode="dailycostheader">
		<fo:table width="100%" table-layout="fixed" font-size="7pt" margin-top="2pt" border="0.75px solid black">
			<fo:table-body>
				<fo:table-row space-after="6pt">
						<fo:table-cell padding-top="0.05cm" padding-left="0.1cm" font-size="7pt">
							<fo:block>
								<fo:inline font-weight="bold">
								  OPERATIONS SUMMARY (24 HOURS THROUGH MIDNIGHT):
								</fo:inline>
							</fo:block>
						</fo:table-cell> 
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-bottom="0.1cm" padding-left="0.1cm">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="reportPeriodSummary"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="6pt">
					<fo:table-cell padding-left="0.1cm">
						<fo:block>
							<fo:inline font-weight="bold" font-size="7pt">
							  06:00 UPDATE:
							</fo:inline>
						</fo:block>
					</fo:table-cell> 
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-bottom="0.1cm" padding-left="0.1cm">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="reportCurrentStatus"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="6pt">
					<fo:table-cell padding-left="0.1cm">
						<fo:block>
							<fo:inline font-weight="bold"  font-size="7pt">
							  FORECAST:
							</fo:inline>
						</fo:block>
					</fo:table-cell> 
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-bottom="0.1cm" padding-left="0.1cm">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="reportPlanSummary"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>