<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TEMPLATE: modules/OperationCostByPhaseCode -->
	<xsl:template match="modules/OperationCostByPhaseCode">
	
		<xsl:variable name="operation" select="/root/modules/Operation/Operation"/>
		<xsl:variable name="operationCode" select="$operation/operationCode"/>
		<xsl:variable name="reportdaily" select="/root/modules/ReportDaily/ReportDaily"/>
		
		<fo:block keep-together="always">
		
			<!-- TABLE -->
			<fo:table width="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(20)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(20)"/>
				
				<!-- HEADER -->
				<fo:table-header>
				
					<!-- HEADER 1 -->
					<fo:table-row>
						<fo:table-cell padding="2px" background-color="#DDDDDD"
							border-left="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
							<fo:block font-size="10pt" font-weight="bold">
								Cost Data
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="3" padding="2px" background-color="#DDDDDD"
							border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
							<fo:block font-size="10pt" font-weight="bold" text-align="right">
								Daily Cost: 
								<xsl:value-of select="$reportdaily/daycost/@uomSymbol"/>
								<fo:inline color="#DDDDDD">.</fo:inline>
								<xsl:value-of select="$reportdaily/daycost"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<!-- HEADER 2 -->
					<fo:table-row>
						<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black">
							<fo:block/>											
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								AFE
							</fo:block>
							<fo:block text-align="center">
								(<xsl:value-of select="$reportdaily/daycost/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Actual Cost To Date
							</fo:block>
							<fo:block text-align="center">
								(<xsl:value-of select="$reportdaily/daycost/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								EFC
							</fo:block>
							<fo:block text-align="center">
								(<xsl:value-of select="$reportdaily/daycost/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
				</fo:table-header>
				
				<!-- BODY -->
				<fo:table-body>
				
					<!-- MOB/DEMOB -->
					<fo:table-row>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
							<fo:block>
								Mob/Demob
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="string($operation/afebudgetMobdemob) != ''">									
										<xsl:value-of select="d2Utils:formatNumber(string($operation/afebudgetMobdemob), '###,###')"/>
									</xsl:when>
									<xsl:otherwise>									
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="$operationCode='DRLLG_NEWCODE' or $operationCode='DRLLG'">
										<xsl:variable name="mobdemobtotal" select="sum(OperationCostByPhaseCode[(phaseCode='RDM'
				           					or phaseCode='RM' or phaseCode='RDMC' or phaseCode='RMC'
				           					or phaseCode='RMI' or phaseCode='RDMI')]/cost)"/>
				           				<xsl:choose>
				           					<xsl:when test="string($operation/amountSpentPriorToSpud/@rawNumber) != ''">
				           						<xsl:choose>
													<xsl:when test="string($mobdemobtotal) != ''">											
														<xsl:value-of select="d2Utils:formatNumber(string($operation/amountSpentPriorToSpud/@rawNumber + $mobdemobtotal), '###,###')"/>
													</xsl:when>
													<xsl:otherwise>											
														<xsl:value-of select="d2Utils:formatNumber(string($operation/amountSpentPriorToSpud/@rawNumber), '###,###')"/>
													</xsl:otherwise>
												</xsl:choose>
				           					</xsl:when>
				           					<xsl:otherwise>
				           						<xsl:choose>
													<xsl:when test="string($mobdemobtotal) != ''">											
														<xsl:value-of select="d2Utils:formatNumber(string(0 + $mobdemobtotal), '###,###')"/>
													</xsl:when>
													<xsl:otherwise>											
														0
													</xsl:otherwise>
												</xsl:choose>
				           					</xsl:otherwise>
				           				</xsl:choose>
				           			</xsl:when>
									<xsl:otherwise>
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="string($reportdaily/efcMobDemob) != ''">									
										<xsl:value-of select="d2Utils:formatNumber(string($reportdaily/efcMobDemob), '###,###')"/>
									</xsl:when>
									<xsl:otherwise>									
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
					</fo:table-row>
					
					<!-- DRILLING -->
					<fo:table-row background-color="#EEEEEE">
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
							<fo:block>
								Drilling
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="string($operation/afebudgetDrilling) != ''">									
										<xsl:value-of select="d2Utils:formatNumber(string($operation/afebudgetDrilling), '###,###')"/>
									</xsl:when>
									<xsl:otherwise>									
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="$operationCode='DRLLG_NEWCODE' or $operationCode='DRLLG'">
										<xsl:variable name="drillingtotal" select="sum(OperationCostByPhaseCode[(phaseCode='A'
					           				or phaseCode='PS' or phaseCode='SRC' or phaseCode='CH' or phaseCode ='CC'
					           				or phaseCode='SH' or phaseCode='SC' or phaseCode='BOP' or phaseCode='IH'
					           				or phaseCode='IH1' or phaseCode='IH2' or phaseCode='IH3' or phaseCode='IH4'
					           				or phaseCode='IH5' or phaseCode='EVIL' or phaseCode='IC' or phaseCode='IC1'
					           				or phaseCode='IC2' or phaseCode='IC3' or phaseCode='IC4' or phaseCode='IC5'
					           				or phaseCode='PA' or phaseCode='PH' or phaseCode='PC' or phaseCode='EV'
					           				or phaseCode='EVC' or phaseCode='EVTL' or phaseCode='DS' or phaseCode='RE'
					           				or phaseCode='AR' or phaseCode='S')]/cost)"/>
										<xsl:choose>
											<xsl:when test="string($drillingtotal) != ''">									
												<xsl:value-of select="d2Utils:formatNumber(string($drillingtotal), '###,###')"/>
											</xsl:when>
											<xsl:otherwise>									
												0
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="string($reportdaily/efcDrilling) != ''">									
										<xsl:value-of select="d2Utils:formatNumber(string($reportdaily/efcDrilling), '###,###')"/>
									</xsl:when>
									<xsl:otherwise>									
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
					</fo:table-row>
					
					<!-- COMPLETION -->
					<fo:table-row>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
							<fo:block>
								Completion
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="string($operation/afebudgetCompletion) != ''">									
										<xsl:value-of select="d2Utils:formatNumber(string($operation/afebudgetCompletion), '###,###')"/>
									</xsl:when>
									<xsl:otherwise>									
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="$operationCode='WKO' or $operationCode='CMPLT'">						
										<xsl:variable name="completiontotal" select="sum(OperationCostByPhaseCode[(phaseCode='AC' 
											or phaseCode='WPC' or phaseCode='WPSC' or phaseCode='CR' 
											or phaseCode='CS' or phaseCode='XTR' or phaseCode='WOP' 
											or phaseCode='WO' or phaseCode='RSC' or phaseCode='WBSC' 
											or phaseCode='RUCE' or phaseCode='THO' or phaseCode='XTO' 
											or phaseCode='PER' or phaseCode='STO' or phaseCode='SMO' 
											or phaseCode='COMO' or phaseCode='BOPC' or phaseCode='PC' 
											or phaseCode='ARC' or phaseCode='C')]/cost)"/>
										<xsl:choose>
											<xsl:when test="string($completiontotal) != ''">									
												<xsl:value-of select="d2Utils:formatNumber(string($completiontotal), '###,###')"/>
											</xsl:when>
											<xsl:otherwise>									
												0
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="string($reportdaily/efcCompletion) != ''">									
										<xsl:value-of select="d2Utils:formatNumber(string($reportdaily/efcCompletion), '###,###')"/>
									</xsl:when>
									<xsl:otherwise>									
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
					</fo:table-row>
					
					<!-- TESTING -->
					<fo:table-row background-color="#EEEEEE">
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
							<fo:block>
								Testing
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="string($operation/afebudgetTesting) != ''">									
										<xsl:value-of select="d2Utils:formatNumber(string($operation/afebudgetTesting), '###,###')"/>
									</xsl:when>
									<xsl:otherwise>									
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">						
								<xsl:variable name="testtotal" select="sum(OperationCostByPhaseCode[(phaseCode='WT')]/cost)"/>
								<xsl:choose>
									<xsl:when test="string($testtotal) != ''">									
										<xsl:value-of select="d2Utils:formatNumber(string($testtotal), '###,###')"/>
									</xsl:when>
									<xsl:otherwise>									
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="string($reportdaily/efcTest) != ''">									
										<xsl:value-of select="d2Utils:formatNumber(string($reportdaily/efcTest), '###,###')"/>
									</xsl:when>
									<xsl:otherwise>									
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
					</fo:table-row>
					
					<!-- INTERVENTION -->
					<fo:table-row>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
							<fo:block>
								Intervention
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">							
								0
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								<xsl:choose>
									<xsl:when test="$operationCode='INTV'">						
										<xsl:variable name="interventiontotal" select="sum(OperationCostByPhaseCode[(phaseCode='ARI' 
											or phaseCode='AI' or phaseCode='CRI' or phaseCode='CSI' 
											or phaseCode='WSI' or phaseCode='PERI' or phaseCode='SMOI' 
											or phaseCode='COMI')]/cost)"/>
										<xsl:choose>
											<xsl:when test="string($interventiontotal) != ''">									
												<xsl:value-of select="d2Utils:formatNumber(string($interventiontotal), '###,###')"/>
											</xsl:when>
											<xsl:otherwise>									
												0
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										0
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
							<fo:block text-align="right">
								0
							</fo:block>											
						</fo:table-cell>
					</fo:table-row>
					
					<!-- WELL TOTAL -->
					<fo:table-row background-color="#EEEEEE">
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
							<fo:block>
								Well Total
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
							<fo:block text-align="right">
								<xsl:variable name="budget_mobdemob">
									<xsl:choose>
										<xsl:when test="number($operation/afebudgetMobdemob/@rawNumber)">
											<xsl:value-of select="$operation/afebudgetMobdemob/@rawNumber"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="0"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:variable name="budget_drilling">
									<xsl:choose>
										<xsl:when test="number($operation/afebudgetDrilling/@rawNumber)">
											<xsl:value-of select="$operation/afebudgetDrilling/@rawNumber"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="0"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:variable name="budget_completion">
									<xsl:choose>
										<xsl:when test="number($operation/afebudgetCompletion/@rawNumber)">
											<xsl:value-of select="$operation/afebudgetCompletion/@rawNumber"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="0"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:variable name="budget_test">
									<xsl:choose>
										<xsl:when test="number($operation/afebudgetTesting/@rawNumber)">
											<xsl:value-of select="$operation/afebudgetTesting/@rawNumber"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="0"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>							
								<xsl:value-of select="d2Utils:formatNumber(string($budget_mobdemob + $budget_drilling + $budget_completion + $budget_test), '###,###')"/>							
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
							<fo:block text-align="right">
								<xsl:variable name="priorcost">
									<xsl:choose>
										<xsl:when test="number($operation/amountSpentPriorToSpud/@rawNumber)">
											<xsl:value-of select="$operation/amountSpentPriorToSpud/@rawNumber"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="0"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:variable name="totalcost" select="sum(OperationCostByPhaseCode/cost)"/>
								<xsl:choose>
									<xsl:when test="number($totalcost)">									
										<xsl:value-of select="d2Utils:formatNumber(string($totalcost + $priorcost), '###,###')"/>
									</xsl:when>
									<xsl:otherwise>									
										<xsl:value-of select="d2Utils:formatNumber(string(0 + $priorcost), '###,###')"/>
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>											
						</fo:table-cell>
						<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
							<fo:block text-align="right">
								<xsl:variable name="efc_mobdemob">
									<xsl:choose>
										<xsl:when test="number($reportdaily/efcMobDemob/@rawNumber)">
											<xsl:value-of select="$reportdaily/efcMobDemob/@rawNumber"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="0"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:variable name="efc_drilling">
									<xsl:choose>
										<xsl:when test="number($reportdaily/efcDrilling/@rawNumber)">
											<xsl:value-of select="$reportdaily/efcDrilling/@rawNumber"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="0"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:variable name="efc_completion">
									<xsl:choose>
										<xsl:when test="number($reportdaily/efcCompletion/@rawNumber)">
											<xsl:value-of select="$reportdaily/efcCompletion/@rawNumber"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="0"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:variable name="efc_test">
									<xsl:choose>
										<xsl:when test="number($reportdaily/efcTest/@rawNumber)">
											<xsl:value-of select="$reportdaily/efcTest/@rawNumber"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="0"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>							
								<xsl:value-of select="d2Utils:formatNumber(string($efc_mobdemob + $efc_drilling + $efc_completion + $efc_test), '###,###')"/>
							</fo:block>											
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>					
	</xsl:template>
	
</xsl:stylesheet>	