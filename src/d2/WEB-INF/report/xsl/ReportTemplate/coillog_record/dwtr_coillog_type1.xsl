<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TEMPLATE: modules/SurveyReference/SurveyReference -->
	<xsl:template match="modules/CoilLog">
	
		<xsl:variable name="fracCoilLogUid" select="fracCoilLogUid"/>
		
		<fo:block keep-together="always">
		
			<!-- TABLE -->
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" border="0.5px solid black" space-after="6pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(6)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="9" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="10" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="11" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="12" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="13" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="14" column-width="proportional-column-width(13)"/>
				<fo:table-column column-number="15" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="16" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="17" column-width="proportional-column-width(14)"/>
				
				<!-- HEADER -->
				<fo:table-header>
				
					<!-- HEADER 1 -->
					<fo:table-row>
						<fo:table-cell number-columns-spanned="17" padding="2px" background-color="#DDDDDD" display-align="center" border-bottom="0.5px solid black">
							<fo:block font-size="8pt" font-weight="bold" text-align="left">
								Coil Log Data
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<!-- HEADER 2 -->
					<fo:table-row text-align="center" font-size="6pt">
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black">
							<fo:block>
								Time
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black">
							<fo:block>
								Depth MEC
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/depthFromMechanicalCounter/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black">
							<fo:block>
								Depth ELE
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/depthFromElectronicCounter/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black" >
							<fo:block>
								WT Static
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/staticWeight/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black" >
							<fo:block>
								WT RIH
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/runInHoleWeight/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black" >
							<fo:block>
								WT 
							</fo:block>
							<fo:block>
								POOH
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/pullOutOfHoleWeight/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black">
							<fo:block>
								Hyd Motor
							</fo:block>
							<fo:block>
								Pressure
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/hydraulicMotorPressure/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black">
							<fo:block>
								P CT
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/coilTubingPressure/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black" >
							<fo:block>
								P WH
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/wellheadPressure/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black" >
							<fo:block>
								Rate Fluid
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/fluidRate/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black" >
							<fo:block>
								Rate N2
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/nitrogenRate/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black" >
							<fo:block>
								Choke Size
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black" >
							<fo:block>
								Rtms
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black" >
							<fo:block>
								Return Type
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black" >
							<fo:block>
								Stripper
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/stripperPressure/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" border-right="0.5px solid black" >
							<fo:block>
								Linear
							</fo:block>
							<fo:block text-align="center">
									(<xsl:value-of select="FracCoilLog/linearBeamPressure/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black" >
							<fo:block>
								Job Desc
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
				</fo:table-header>
				
				<!-- BODY -->
				<fo:table-body>
					<xsl:apply-templates select="FracCoilLog"/>

				</fo:table-body>
			</fo:table>
			
		</fo:block>
		
	</xsl:template>
	
	<!-- TEMPLATE: SurveyStation -->
	<xsl:template match="FracCoilLog">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row font-size="6pt">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
						<xsl:choose>
					      		<xsl:when test="coilLogDateTime/@epochMS = '86400000'">
							       <fo:inline>24:00</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS(coilLogDateTime/@epochMS,'HH:mm')"/>
							    </xsl:otherwise>
					     </xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="depthFromMechanicalCounter"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="depthFromElectronicCounter"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="staticWeight"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="runInHoleWeight"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="pullOutOfHoleWeight"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="hydraulicMotorPressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="coilTubingPressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="wellheadPressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="fluidRate"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="nitrogenRate"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="chokeSizeDescription"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="isReturns/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="left">
							<xsl:value-of select="returnsType"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="stripperPressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="linearBeamPressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block text-align="left">
							<xsl:value-of select="jobDescription"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE" font-size="6pt">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
						<xsl:choose>
					      		<xsl:when test="coilLogDateTime/@epochMS = '86400000'">
							       <fo:inline>24:00</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS(coilLogDateTime/@epochMS,'HH:mm')"/>
							    </xsl:otherwise>
					     </xsl:choose>
					     	</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="depthFromMechanicalCounter"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="depthFromElectronicCounter"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="staticWeight"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="runInHoleWeight"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="pullOutOfHoleWeight"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="hydraulicMotorPressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="coilTubingPressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="wellheadPressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="fluidRate"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="nitrogenRate"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="chokeSizeDescription"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="isReturns/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="left">
							<xsl:value-of select="returnsType"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="stripperPressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="linearBeamPressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block text-align="left">
							<xsl:value-of select="jobDescription"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
</xsl:stylesheet>