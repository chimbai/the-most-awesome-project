<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- drillingParameters START -->
	<xsl:template match="modules/DrillingParameters">
		<fo:table-row keep-together="always">
				<fo:table-cell>			
					<fo:table width="100%" table-layout="fixed" font-size="8pt" border-left="0.5px solid black"
						border-right="0.5px solid black" border-bottom="0.25px solid black" 
						wrap-option="wrap" space-after="2pt" margin-top="2pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>								
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black" background-color="rgb(224,224,224)"
									border-top="0.5px solid black" border-left="0.1px solid black" border-right="0.1px solid black">
									<fo:block text-align="left" font-weight="bold" font-size="10pt">
										Drilling Parameters
									</fo:block>
								</fo:table-cell>
							</fo:table-row>							
							<fo:table-row font-size="8pt">
								<fo:table-cell>
									<fo:block space-after="2pt">	
										<xsl:variable name="dailyUid" select="/root/modules/Daily/Daily/dailyUid"/>                           
										<xsl:apply-templates select="DrillingParameters[dailyUid=$dailyUid]"/>
									</fo:block>
								</fo:table-cell>	
							</fo:table-row>
						</fo:table-body>
					</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="DrillingParameters">		
		<fo:table inline-progression-dimension="100%" table-layout="fixed" table-omit-header-at-break="true" border="0.25px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(25)"/>			
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm" number-columns-spanned="4">
						<fo:block text-align="left" font-weight="bold">BHA Run # <xsl:value-of select="bharunUid/@lookupLabel"/></fo:block>
					</fo:table-cell>					
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="left"> Top Depth: </fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="right">
							<xsl:value-of select="depthTopMdMsl"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="depthTopMdMsl/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="left">PWD ECD: </fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="right">
							<xsl:value-of select="pwd_ecd"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="pwd_ecd/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="left"> Bottom Depth: </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="right">
							<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="depthMdMsl/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" number-columns-spanned="2" padding="0.05cm">
						<fo:block color="white"> </fo:block>
					</fo:table-cell>								
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
						<fo:block color="white"> * </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Min</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Avg</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Max</fo:block>
					</fo:table-cell>					
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="left"> Flow </fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="flowMin"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="flowMin/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="flowAvg"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="flowAvg/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="flowMax"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="flowMax/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="left"> Surface RPM </fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="surfaceRpmMin"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="surfaceRpmMin/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="surfaceRpmAvg"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="surfaceRpmAvg/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="surfaceRpmMax"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="surfaceRpmMax/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="left"> Downhole RPM </fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="dhRpmMin"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="dhRpmMin/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="dhRpmAvg"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="dhRpmAvg/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="dhRpmMax"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="dhRpmMax/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="left"> Pressure </fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="pressureMin"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="pressureMin/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="pressureAvg"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="pressureAvg/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="pressureMax"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="pressureMax/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
				<!-- 
				<fo:table-row>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="left"> SPM </fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="spmMin"/>							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="spmAvg"/>							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="spmMax"/>							
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
				-->
				<fo:table-row>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="left"> Torque </fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="torqueMin"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="torqueMin/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="torqueAvg"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="torqueAvg/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="torqueMax"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="torqueMax/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="left"> WOB </fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="wobMin"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="wobMin/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="wobAvg"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="wobAvg/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="wobMax"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="wobMax/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="left"> ROP </fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="ropMin"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="ropMin/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="ropAvg"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="ropAvg/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="center">
							<xsl:value-of select="ropMax"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="ropMax/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- drillingParameter END -->

</xsl:stylesheet>