<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- TEMPLATE: modules/DrillingParameters -->
	<xsl:template match="modules/DrillingParameters">
	
		<fo:table-row keep-together="always">
			<fo:table-cell padding-bottom="2px" padding-top="2px">
			
				<fo:block keep-together="always">
					
					<!-- TABLE -->
					<fo:table width="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						
						<!-- HEADER -->
						<fo:table-header>
							<fo:table-row>
								<fo:table-cell padding="2px" background-color="#DDDDDD" border="thin solid black" display-align="center">
									<fo:block font-size="8pt" font-weight="bold">
										Drilling Parameters
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-header>
						
						<!-- BODY -->
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell>
									<fo:block>	
										<xsl:variable name="dailyUid" select="/root/modules/Daily/Daily/dailyUid"/>                           
										<xsl:apply-templates select="DrillingParameters[dailyUid=$dailyUid]"/>
									</fo:block>
								</fo:table-cell>	
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				
				</fo:block>
				
			</fo:table-cell>
		</fo:table-row>
		
	</xsl:template>
	
	<!-- TEMPLATE: DrillingParameters -->
	<xsl:template match="DrillingParameters">		
		<fo:table width="100%" table-layout="fixed">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
			
			<!-- HEADER -->
			<fo:table-header>
			
				<!-- BHA RUN # -->
				<fo:table-row>
					<fo:table-cell number-columns-spanned="4" padding="2px" display-align="center"
						border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block font-weight="bold">
							BHA Run #<xsl:value-of select="bharunUid/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
				
				<!-- TOP DEPTH -->
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black">
						<fo:block>
							Top Depth:
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:choose>
								<xsl:when test="(depthTopMdMsl = '') or (depthTopMdMsl = 'null') ">
								</xsl:when>
                  				<xsl:otherwise>
									<xsl:value-of select="depthTopMdMsl"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="depthTopMdMsl/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block>
							PWD ECD:
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:choose>
								<xsl:when test="(pwdEcdDensity = '') or (pwdEcdDensity = 'null') ">
								</xsl:when>
	                 			<xsl:otherwise>
									<xsl:value-of select="pwdEcdDensity"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="pwdEcdDensity/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<!-- BOTTOM DEPTH -->
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-bottom="thin solid black">
						<fo:block>
							Bottom Depth:
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="right">
							<xsl:choose>
								<xsl:when test="(depthMdMsl = '') or (depthMdMsl = 'null') ">
								</xsl:when>
	                 			<xsl:otherwise>
									<xsl:value-of select="depthMdMsl"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="depthMdMsl/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block/>
					</fo:table-cell>								
				</fo:table-row>
				
				<!-- MIN/AVG/MAX -->
				<fo:table-row>
					<fo:table-cell border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block/>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
						<fo:block text-align="center">
							Min
						</fo:block>
					</fo:table-cell>					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
						<fo:block text-align="center">
							Avg
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
						<fo:block text-align="center">
							Max
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
			</fo:table-header>
			
			<!-- BODY -->
			<fo:table-body>
			
				<!-- FLOW -->
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							Flow 
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(flowMin)=''">
									<xsl:value-of select="flowMin"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="flowMin"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="flowMin/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(flowAvg)=''">
									<xsl:value-of select="flowAvg"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="flowAvg"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="flowAvg/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(flowMax)=''">
									<xsl:value-of select="flowMax"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="flowMax"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="flowMax/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
							
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				
				<!-- SURFACE -->
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							Surface RPM
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(surfaceRpmMin)=''">
									<xsl:value-of select="surfaceRpmMin"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="surfaceRpmMin"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="surfaceRpmMin/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(surfaceRpmAvg)=''">
									<xsl:value-of select="surfaceRpmAvg"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="surfaceRpmAvg"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="surfaceRpmAvg/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(surfaceRpmMax)=''">
									<xsl:value-of select="surfaceRpmMax"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="surfaceRpmMax"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="surfaceRpmMax/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
							
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<!-- DOWNHOLE RPM -->
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							Downhole RPM
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(dhRpmMin)=''">
									<xsl:value-of select="dhRpmMin"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="dhRpmMin"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="dhRpmMin/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(dhRpmAvg)=''">
									<xsl:value-of select="dhRpmAvg"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="dhRpmAvg"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="dhRpmAvg/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(dhRpmMax)=''">
									<xsl:value-of select="dhRpmMax"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="dhRpmMax"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="dhRpmMax/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
							
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<!-- SPP/PRESSURE -->
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							Pressure
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(pressureMin)=''">
									<xsl:value-of select="pressureMin"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="pressureMin"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="pressureMin/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
						<xsl:choose>													
								<xsl:when test="string(pressureAvg)=''">
									<xsl:value-of select="pressureAvg"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="pressureAvg"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="pressureAvg/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" >
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(pressureMax)=''">
									<xsl:value-of select="pressureMax"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="pressureMax"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="pressureMax/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<!-- TORQUE -->
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							Torque 
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(torqueMin)=''">
									<xsl:value-of select="torqueMin"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="torqueMin"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="torqueMin/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(torqueAvg)=''">
									<xsl:value-of select="torqueAvg"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="torqueAvg"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="torqueAvg/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(torqueMax)=''">
									<xsl:value-of select="torqueMax"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="torqueMax"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="torqueMax/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<!-- SPM 
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							SPM
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="spmMin"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="spmAvg"/>							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="spmMax"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				-->
				
				<!-- WOB -->
				<fo:table-row  background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							WOB
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(wobMinForce)=''">
									<xsl:value-of select="wobMinForce"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="wobMinForce"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="wobMinForce/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(wobAvgForce)=''">
									<xsl:value-of select="wobAvgForce"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="wobAvgForce"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="wobAvgForce/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(wobMaxForce)=''">
									<xsl:value-of select="wobMaxForce"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="wobMaxForce"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="wobMaxForce/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<!-- ROP -->
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"
						border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block>
							ROP 
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(ropMin)=''">
									<xsl:value-of select="ropMin"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="ropMin"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="ropMin/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(ropAvg)=''">
									<xsl:value-of select="ropAvg"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="ropAvg"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="ropAvg/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>													
								<xsl:when test="string(ropMax)=''">
									<xsl:value-of select="ropMax"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="ropMax"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="ropMax/@uomSymbol"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
			</fo:table-body>
			
		</fo:table>
		
	</xsl:template>

</xsl:stylesheet>