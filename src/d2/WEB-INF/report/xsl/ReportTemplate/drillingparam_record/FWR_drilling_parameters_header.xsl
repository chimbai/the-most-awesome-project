<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:import href="./FWR_drilling_parameters_records.xsl" />
	
	<!-- Drilling Parameters header START -->
	<xsl:template match="modules/DrillingParameters" mode="drilling_parameters_header">
		
		<xsl:variable name="dailyid" select="/root/modules/Daily/Daily/dailyUid"/>		
			<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="6pt" border="0.5px solid black" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
				<fo:table-column column-number="1" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="9" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="10" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="11" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="12" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="13" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="14" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="15" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="16" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="17" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="18" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="19" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="20" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="21" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="22" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="23" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="24" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="25" column-width="proportional-column-width(4)"/>												
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black"
							number-rows-spanned="2">
							<fo:block text-align="center">
							BHA Run# 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black"
							number-rows-spanned="2">
							<fo:block text-align="center">
							Top Depth 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black"
							number-rows-spanned="2">
							<fo:block text-align="center">
							Bottom Depth 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black"
							number-rows-spanned="2">
							<fo:block text-align="center">
							PWD ECD 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black"
							number-columns-spanned="7">
							<fo:block text-align="center">
							Min	
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black"
							number-columns-spanned="7">
							<fo:block text-align="center">
							Avg 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black"
							number-columns-spanned="7">
							<fo:block text-align="center">
							Max 
							</fo:block>
						</fo:table-cell>						
					</fo:table-row>
					<fo:table-row>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Flow
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Surface 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Downhole 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Pressure 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Torque 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							WOB 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							ROP 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Flow
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Surface 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Downhole 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Pressure 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Torque 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							WOB 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							ROP 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Flow
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Surface 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Downhole 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Pressure 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Torque 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							WOB 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							ROP 
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>					 
					<xsl:apply-templates select="DrillingParameters">
						<xsl:sort select="bharunUid/@lookupLabel"/>
					</xsl:apply-templates>					
				</fo:table-body>
			</fo:table>		
	</xsl:template>
	<!-- Drilling Parameters header END -->
	
</xsl:stylesheet>