<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	<!-- TEMPLATE: modules/LessonTicket -->
	<xsl:template match="modules/LessonTicketPotentialLearning">
	
		<fo:block keep-together="always">
		
			<!-- TABLE -->
			<fo:table width="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black" space-after="6pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(14)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(14)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(14)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(30)"/>
				
				<!-- HEADER -->
				<fo:table-header>
				
					<!-- HEADER 1 -->
					<fo:table-row>
						<fo:table-cell number-columns-spanned="8" padding="2px" background-color="#DDDDDD" border="thin solid black" display-align="center">
							<fo:block font-size="8pt" font-weight="bold">
								Potential Lesson Learned
							</fo:block>
						</fo:table-cell>					
					</fo:table-row>
					
					<!-- HEADER 2 -->
					<fo:table-row>
						<fo:table-cell padding="2px" border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="left">
								Ticket #
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="left">
								Stage of PLI
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="left">
								Date
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="left">
								Phase Code
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="left">
								Task Code
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="left">
								Root Cause Code
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="left">
								Status
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="left">
								Potential Learning Suggestion (PLS)
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
				</fo:table-header>
				
				<!-- BODY -->
				<fo:table-body>
					<xsl:apply-templates select="LessonTicket"/>
				</fo:table-body>
				
			</fo:table>
			
		</fo:block>
		
	</xsl:template>
	
	<!-- TEMPLATE: LessonTicket -->
	<xsl:template match="LessonTicket">
		
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="lessonTicketNumber"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="potentialLearningIssue"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="rp2Date"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="phaseCode/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="taskCode/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="rootcauseCode/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="lessonStatus/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="potentialLearningSuggestion"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="lessonTicketNumber"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="potentialLearningIssue"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="rp2Date"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="phaseCode/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="taskCode/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="rootcauseCode/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="lessonStatus/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="potentialLearningSuggestion"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
</xsl:stylesheet>