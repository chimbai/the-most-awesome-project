<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template name="dvdgraph">
		<xsl:variable name="operationUid" select="/root/modules/Operation/Operation/operationUid"/>
		<xsl:variable name="dailyUid" select="/root/modules/ReportDaily/ReportDaily/dailyUid"/>
		<xsl:variable name="dvd" select="concat($operationUid,'_dvd')"/>
		<xsl:variable name="dvdgraph_noactualcost" select="concat($operationUid,'_dvdgraph_noactualcost')"/>
		<xsl:variable name="dvdgraph_nocost" select="concat($operationUid,'_dvdgraph_nocost')"/>
		<xsl:variable name="dvdgraph_noplannedcost" select="concat($operationUid,'_dvdgraph_noplannedcost')"/>
		<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="8pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						
						<fo:block>							
							<fo:external-graphic content-width="scale-to-fit" width="450pt"
								content-height="5in" scaling="uniform" src="url(d2://../graph/dvd/{$dailyUid}_dvdgraph_todate.jpg)" />
						</fo:block>
						
						<!--
						<fo:block>							
							<fo:external-graphic content-width="scale-to-fit" width="450pt"
								content-height="5in" scaling="uniform" src="url(d2://../graph/dvd/{$dvdgraph_noactualcost}.jpg)" />
						</fo:block>
						-->
						<!--
						<fo:block>							
							<fo:external-graphic content-width="scale-to-fit" width="450pt"
								content-height="5in" scaling="uniform" src="url(d2://../graph/dvd/{$dvdgraph_nocost}.jpg)" />
						</fo:block>
						-->
						<!--
						<fo:block>							
							<fo:external-graphic content-width="scale-to-fit" width="450pt"
								content-height="5in" scaling="uniform" src="url(d2://../graph/dvd/{$dvdgraph_noplannedcost}.jpg)" />
						</fo:block>
						-->
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>
			