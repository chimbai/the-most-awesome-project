<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:import href="./bharun_component_header.xsl" />
	<xsl:import href="./bharun_component_records.xsl" />

	<!-- bha section BEGIN -->
	<xsl:template match="modules/Bharun">
		<xsl:apply-templates select="Bharun"/>
	</xsl:template>
	<xsl:template match="Bharun">
	<fo:table-row keep-together="always">
			<fo:table-cell>
				<fo:table width="100%" table-layout="fixed" font-size="8pt" 
					wrap-option="wrap" space-after="2pt" margin-top="2pt" border-left="0.5px solid black"
					border-right="0.5px solid black" border-bottom="0.5px solid black">
					<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>								
					<fo:table-body>												
						<fo:table-row font-size="8pt">
							<fo:table-cell>
								<fo:block space-after="2pt">	                                            
									<fo:table width="100%" table-layout="fixed" font-size="8pt"
										wrap-option="wrap" space-after="2pt" table-omit-header-at-break="true">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>								
										<fo:table-header>
											<fo:table-row>
												<fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
													number-columns-spanned="2" background-color="rgb(224,224,224)" border-top="0.5px solid black"
													border-left="0.1px solid black" border-right="0.1px solid black">														
													<fo:block font-size="10pt" font-weight="bold"> BHA # <xsl:value-of
														select="bhaRunNumber"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-header>
										<fo:table-body>
											<fo:table-row font-size="8pt">
												<fo:table-cell border-right="0.25px solid black"
													border-bottom="0.25px solid black">
		                                            <fo:block>
		                                            	<fo:table width="100%" table-layout="fixed">
		                                            		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
															<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>																	
															<fo:table-body>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> BHA Type: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="bhaType"/>																					
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Depth In: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="depthInMdMsl"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="depthInMdMsl/@uomSymbol"/>																					
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Depth Out: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="depthOutMdMsl"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="depthOutMdMsl/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Total Length: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="bhaTotalLength"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="bhaTotalLength/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Total Weight Dry: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="weightBhaTotalDry"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="weightBhaTotalDry/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Total Weight Wet: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="weightBhaTotalWet"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="weightBhaTotalWet/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>															
															</fo:table-body>
														</fo:table>
													</fo:block>
												</fo:table-cell>
												<fo:table-cell border-bottom="0.25px solid black">
		                                            <fo:block>
		                                            	<fo:table width="100%" table-layout="fixed">
		                                            		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
															<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
															<fo:table-body>																
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Weight Below Jar Dry </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="weightbelowjardry"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="weightbelowjardry/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>																
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Weight Below Jar Wet: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="weightbelowjarwet"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="weightbelowjarwet/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> D.P. Ann Velocity </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/annvelDp"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/annvelDp/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> H.W.D.P. Ann Velocity </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/annvelHwdp"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/annvelHwdp/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>																
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> D.C. (1) Ann Velocity </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/annvelDc1"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/annvelDc1/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> D.C. (2) Ann Velocity </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/annvelDc2"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/annvelDc2/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>												
															</fo:table-body>
														</fo:table>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell number-columns-spanned="2">
													<fo:block>
														<fo:table inline-progression-dimension="100%" table-layout="fixed" border-bottom="0.25px solid black"
															wrap-option="wrap">
															<fo:table-column column-number="1" column-width="proportional-column-width(18)"/>
															<fo:table-column column-number="2" column-width="proportional-column-width(82)"/>																																
															<fo:table-body>		
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">
																		<fo:block text-align="left"> BHA Description: </fo:block>
																	</fo:table-cell>
																	<fo:table-cell padding="0.05cm">
																		<fo:block text-align="left" linefeed-treatment="preserve">
																			<xsl:value-of select="descr"/>
																		</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">
																		<fo:block text-align="left"> BHA Run Comment: </fo:block>
																	</fo:table-cell>
																	<fo:table-cell padding="0.05cm">
																		<fo:block text-align="left" linefeed-treatment="preserve">
																			<xsl:value-of select="bhaDescription"/>
																		</fo:block>
																	</fo:table-cell>
																</fo:table-row>
															</fo:table-body>
														</fo:table>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>	                                            
								</fo:block>
							</fo:table-cell>	
						</fo:table-row>
						<!-- Start bhadaily summary -->
						<fo:table-row font-size="8pt">
							<fo:table-cell>
								<fo:block space-after="2pt">	                                            
									<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="2pt" margin-top="2pt" table-omit-header-at-break="true"
										border-bottom="0.5px solid black">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>												
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>								
										<fo:table-header>
											<fo:table-row>
												<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" number-columns-spanned="2">																
													<fo:block text-align="left" font-weight="bold"> BHA Daily Summary </fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-header>
										<fo:table-body>
											<fo:table-row font-size="8pt">
												<fo:table-cell border-bottom="0.25px solid black">
													<fo:block>
			                                           	<fo:table width="100%" table-omit-header-at-break="true" table-layout="fixed">
			                                           		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
															<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
															<fo:table-body>																
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Pickup Weight: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/weightPickup"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/weightPickup/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Slack-Off Weight: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/weightSlackOff"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/weightSlackOff/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> String Weight: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/weightString"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/weightString/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="2px">																
																		<fo:block text-align="left"> Jars Rotary Hours Logged: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="2px" border-right="0.5px solid black">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/jarRotatingHours"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/jarRotatingHours/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>																	
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="2px">																
																		<fo:block text-align="left"> Jars Circulating Hours Logged: </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="2px" border-right="0.5px solid black">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/jarhourcirculate"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/jarhourcirculate/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>																	
																</fo:table-row>
															</fo:table-body>
														</fo:table>
													</fo:block>
												</fo:table-cell>												
												<fo:table-cell border-bottom="0.5px solid black">												
													<fo:block>																									
			                                           	<fo:table width="100%" table-omit-header-at-break="true" table-layout="fixed">
			                                           		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
															<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
															<fo:table-body>																
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Torque (Max) </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/torqueMax"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/torqueMax/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Torque (Avg) </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/torqueAvg"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/torqueAvg/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="0.05cm">																
																		<fo:block text-align="left"> Torque (Min) </fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="0.05cm">	
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/torqueMin"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="BharunDailySummary/torqueMin/@uomSymbol"/>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
															</fo:table-body>
														</fo:table>																					
													</fo:block>
												</fo:table-cell>														
											</fo:table-row>
																						
											<fo:table-row>
												<fo:table-cell number-columns-spanned="2">
													<fo:table inline-progression-dimension="100%" table-layout="fixed"
														table-omit-header-at-break="true" wrap-option="wrap">
														<fo:table-column column-number="1"
															column-width="proportional-column-width(18)"/>
														<fo:table-column column-number="2"
															column-width="proportional-column-width(82)"/>																																
														<fo:table-body>		
															<fo:table-row>
																<fo:table-cell padding="0.05cm">
																	<fo:block text-align="left"> Summary: </fo:block>
																</fo:table-cell>
																<fo:table-cell padding="0.05cm">
																	<fo:block text-align="left" linefeed-treatment="preserve">
																		<xsl:value-of select="BharunDailySummary/summary"/>
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>																		
														</fo:table-body>
													</fo:table>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>    
								</fo:block>
							</fo:table-cell>	
						</fo:table-row>
						<!-- Bha daily summary end -->
		                              	
			            <!-- Start Bha Component -->
			            <fo:table-row font-size="8pt">
							<fo:table-cell>
					              	<fo:table inline-progression-dimension="100%" table-layout="fixed" margin-top="2pt" table-omit-header-at-break="true" border-bottom="0.25px solid black">
									<fo:table-column column-number="1" column-width="proportional-column-width(3)"/>
									<fo:table-column column-number="2" column-width="proportional-column-width(20)"/>
									<fo:table-column column-number="3" column-width="proportional-column-width(35)"/>
									<fo:table-column column-number="4" column-width="proportional-column-width(9)"/>
									<fo:table-column column-number="5" column-width="proportional-column-width(9)"/>
									<fo:table-column column-number="6" column-width="proportional-column-width(9)"/>
									<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
									<fo:table-column column-number="8" column-width="proportional-column-width(5)"/>
									<fo:table-header>
										<fo:table-row>
											<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm" number-columns-spanned="8">
												<fo:block text-align="left" font-weight="bold">BHA Component</fo:block>
											</fo:table-cell>
										</fo:table-row>
			            				<xsl:call-template name="bharun_component_header"/>
			            			</fo:table-header>
									<fo:table-body>
										<xsl:variable name="bharunUid" select="bharunUid"/>
										<xsl:apply-templates select="BhaComponent[bharunUid=$bharunUid]">
											<xsl:sort select="sequence" data-type="number"/>
										</xsl:apply-templates>		
									</fo:table-body>
								</fo:table>
							</fo:table-cell>
						</fo:table-row>
						<!-- End Bha Component -->
					
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
		<!-- Start Bitrun -->
		<fo:table-row keep-together="always">
			<fo:table-cell>
				<fo:block>
					<xsl:apply-templates select="BharunDailySummary"/>
					<xsl:variable name="bharunUid" select="bharunUid"/>                                   	
					<xsl:apply-templates select="Bitrun[bharunUid=$bharunUid]">
						<xsl:sort select="bitrunNumber" data-type="varchar"/>						
					</xsl:apply-templates>					
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<!-- End Bitrun -->
	</xsl:template>
	
	<xsl:template match="BharunDailySummary">
	<fo:table width="100%" space-after="2pt" font-size="8pt" table-omit-header-at-break="true" margin-top="2pt" border="0.5px solid black"
		table-layout="fixed">
		<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
		<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
		<fo:table-column column-number="3" column-width="proportional-column-width(50)"/>
		<fo:table-body>						
			<fo:table-row>
				<fo:table-cell border-bottom="0.25px solid black" number-columns-spanned="3">
					<fo:block>
						<fo:table width="100%" margin-top="2pt" table-omit-header-at-break="true" table-layout="fixed">
	                        <fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
							<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>											
							<fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
							<fo:table-header>
								<fo:table-row>
									<fo:table-cell padding="0.05cm" number-columns-spanned="4" border-bottom="0.25px solid black">
										<fo:block text-align="left" font-weight="bold"> Bit Hours </fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-header>											
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell><!-- First Cell -->																
										<fo:block text-align="left">
											<fo:table width="100%" table-layout="fixed">
	                                          	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Duration: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="duration"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="duration/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Cum. Duration: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="dynaAttr/totalDuration"/>																				
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>																		
												</fo:table-body>
											</fo:table>		
										</fo:block>
									</fo:table-cell>								
									<fo:table-cell><!-- Second Cell -->																	
										<fo:block text-align="left">
											<fo:table width="100%" table-layout="fixed">
	                                          		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> IADC Hrs: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="iADCDuration"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="iADCDuration/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Cum. IADC Hrs: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="dynaAttr/totalIADCDuration"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>																		
												</fo:table-body>
											</fo:table>		
										</fo:block>
									</fo:table-cell>									
									<fo:table-cell><!-- Third Cell -->																
										<fo:block text-align="left">
											<fo:table width="100%" table-layout="fixed">
	                                          		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Progress: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="progress"/><fo:inline color="white">.</fo:inline>																					
																<xsl:value-of select="progress/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Cum. Progress: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="dynaAttr/totalProgress"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>																		
												</fo:table-body>
											</fo:table>		
										</fo:block>
									</fo:table-cell>
									<fo:table-cell><!-- First Cell -->																
										<fo:block text-align="left">
											<fo:table width="100%" table-layout="fixed">
	                                          		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> ROP: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm">	
															<fo:block text-align="right">																			
																<xsl:value-of select="rop"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="rop/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> ROP (Avg): </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm">	
															<fo:block text-align="right">																			
																<xsl:value-of select="dynaAttr/avgROP"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>																		
												</fo:table-body>
											</fo:table>		
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
										
			<fo:table-row>
				<fo:table-cell border-bottom="0.25px solid black" number-columns-spanned="3">		
					<fo:block>
						<fo:table width="100%" margin-top="2pt" table-omit-header-at-break="true" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(34)"/>
							<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>																	
							<fo:table-header>
								<fo:table-row>
									<fo:table-cell padding="0.05cm" number-columns-spanned="3" border-bottom="0.25px solid black">
										<fo:block text-align="left" font-weight="bold"> Directional Data </fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-header>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>																
										<fo:block text-align="left">
											<fo:table width="100%" table-layout="fixed">
	                                          		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Slide Time: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="slideHours"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="slideHours/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Slide (%): </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="percentSlide"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="percentSlide/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Total Slide Time: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="dynaAttr/totalSlideHours"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="dynaAttr/totalSlideHours/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Total KRevs: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="krevs"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="krevs/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>		
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>																
										<fo:block text-align="left">
											<fo:table width="100%" table-layout="fixed">
	                                          		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Rotate Time: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="durationRotated"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="durationRotated/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Rotate (%): </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="percentRotated"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="percentRotated/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Total Rotate Time: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="dynaAttr/totalDurationRotated"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="dynaAttr/totalDurationRotated/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> HSI: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm" border-right="0.25px solid black">	
															<fo:block text-align="right">																			
																<xsl:value-of select="hsi"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="hsi/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>		
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>																
										<fo:block text-align="left">
											<fo:table width="100%" table-omit-header-at-break="true" table-layout="fixed">
	                                          		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Circ. Time: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm">	
															<fo:block text-align="right">																			
																<xsl:value-of select="durationCirculated"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="durationCirculated/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Circ. (%): </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm">	
															<fo:block text-align="right">																			
																<xsl:value-of select="percentCirculated"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="percentCirculated/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm">																
															<fo:block text-align="left"> Total Circ. Time: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="0.05cm">	
															<fo:block text-align="right">																			
																<xsl:value-of select="dynaAttr/totalDurationCirculated"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="dynaAttr/totalDurationCirculated/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="0.05cm" number-columns-spanned="2">																
															<fo:block text-align="left"> </fo:block>
														</fo:table-cell>																								
													</fo:table-row>
												</fo:table-body>
											</fo:table>		
										</fo:block>
									</fo:table-cell>
								</fo:table-row>																																			
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>				
	</xsl:template>
	
	<xsl:template match="modules/Bharun/Bharun/Bitrun">																							
	<fo:table width="100%" space-after="2pt" font-size="8pt" table-omit-header-at-break="true" margin-top="2pt" border="0.5px solid black"
		table-layout="fixed">
		<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
		<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
		<fo:table-column column-number="3" column-width="proportional-column-width(50)"/>						
		<fo:table-header>
			<fo:table-row>
				<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black"
					padding="0.05cm">
					<fo:block font-weight="bold" font-size="10pt">Bit #<xsl:value-of
							select="bitrunNumber"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-bottom="0.25px solid black" number-columns-spanned="2">
					<fo:block>
						<fo:table inline-progression-dimension="100%" table-layout="fixed"
							font-size="8pt">
							<fo:table-column column-number="1"
								column-width="proportional-column-width(11)"/>
							<fo:table-column column-number="2"
								column-width="proportional-column-width(11)"/>
							<fo:table-column column-number="3"
								column-width="proportional-column-width(11)"/>
							<fo:table-column column-number="4"
								column-width="proportional-column-width(11)"/>
							<fo:table-column column-number="5"
								column-width="proportional-column-width(11)"/>
							<fo:table-column column-number="6"
								column-width="proportional-column-width(11)"/>
							<fo:table-column column-number="7"
								column-width="proportional-column-width(11)"/>
							<fo:table-column column-number="8"
								column-width="proportional-column-width(11)"/>
							<fo:table-column column-number="9"
								column-width="proportional-column-width(12)"/>
							<fo:table-header>
								<fo:table-row>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block text-align="center" padding="0.05cm"> Wear
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block text-align="center" padding="0.05cm"> I
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block text-align="center" padding="0.05cm"> O1
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block text-align="center" padding="0.05cm"> D
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block text-align="center" padding="0.05cm"> L
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block text-align="center" padding="0.05cm"> B
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block text-align="center" padding="0.05cm"> G
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block text-align="center" padding="0.05cm"> O2
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block text-align="center" padding="0.05cm"> R
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-header>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block padding="0.05cm" color="white" text-align="center"> ! </fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block padding="0.05cm" text-align="center">
											<xsl:value-of select="condFinalInner"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block padding="0.05cm" text-align="center">
											<xsl:value-of select="condFinalOuter"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block padding="0.05cm" text-align="center">
											<xsl:value-of select="condFinalDull"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block padding="0.05cm" text-align="center">
											<xsl:value-of select="condFinalLocation"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block padding="0.05cm" text-align="center">
											<xsl:value-of select="condFinalBearing"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block padding="0.05cm" text-align="center">
											<xsl:value-of select="condFinalGauge"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black">
										<fo:block padding="0.05cm" text-align="center">
											<xsl:for-each select="condFinalOther/multiselect">
											      <xsl:sort select="@value"/>
											      <xsl:value-of select="@value"/>
											      <xsl:if test="position() != last()">
											              <fo:inline>/</fo:inline>
											       </xsl:if>
											</xsl:for-each>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block padding="0.05cm" text-align="center">
											<xsl:value-of select="condFinalReason"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-header>
		<fo:table-body font-size="8pt">
			<!-- ROW 1 -->
			<xsl:variable name="bharunid" select="bharunUid"/>
			<fo:table-row>
				<!-- Column 1 -->
				<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black">
					<fo:block>
						<fo:table inline-progression-dimension="100%" table-layout="fixed" table-omit-header-at-break="true">
							<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left">Size ("):</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:choose>													
												<xsl:when test = "string(bitDiameter/@lookupLabel)=''">
													<xsl:value-of select="bitDiameter"/>										
												</xsl:when>													
												<xsl:otherwise>
													<xsl:value-of select="bitDiameter/@lookupLabel"/>
												</xsl:otherwise>
											</xsl:choose>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left">Manufacturer:</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:choose>													
												<xsl:when test = "string(make/@lookupLabel)=''">
													<xsl:value-of select="make"/>										
												</xsl:when>													
												<xsl:otherwise>
													<xsl:value-of select="make/@lookupLabel"/>
												</xsl:otherwise>
											</xsl:choose>															
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left">Type:</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:choose>													
												<xsl:when test = "string(bitType/@lookupLabel)=''">
													<xsl:value-of select="bitType"/>										
												</xsl:when>													
												<xsl:otherwise>
													<xsl:value-of select="bitType/@lookupLabel"/>
												</xsl:otherwise>
											</xsl:choose>											
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left">Serial No:</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="serialNumber"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left">Bit Model:</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="model"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:table-cell>
				
				<!-- Column 2 -->
				<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black">
					<fo:block>
						<fo:table inline-progression-dimension="100%" table-layout="fixed" table-omit-header-at-break="true">
							<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
							<fo:table-body>												
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left">IADC#:</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="iadcCode"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left">TFA:</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="tfa"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="tfa/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>												
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:table-cell>
				
				<!-- Column 3 -->
				<fo:table-cell border-bottom="0.25px solid black">
					<fo:block>
						<fo:table inline-progression-dimension="100%" table-layout="fixed" table-omit-header-at-break="true">
							<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>											
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
										<fo:block text-align="center">Nozzles</fo:block>
									</fo:table-cell>													
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="center">
											<xsl:call-template name="BitNozzle">
												<xsl:with-param name="bitrunUid" select="bitrunUid"/>
											</xsl:call-template>
										</fo:block>
									</fo:table-cell>													
								</fo:table-row>				
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell number-columns-spanned="3">
					<fo:block>
						<fo:table inline-progression-dimension="100%" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(18)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(82)"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left">Bit run comment: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left" linefeed-treatment="preserve">
											<xsl:value-of select="comment"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left">Bit wear comment: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left" linefeed-treatment="preserve">
											<xsl:value-of select="bitwearComment"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
	</xsl:template>
	
	<xsl:template name="BitNozzle">
		<xsl:param name="bitrunUid"/>
		<fo:table inline-progression-dimension="100%" table-layout="fixed"
			border="0.25px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body font-size="8pt">
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">No. x Size</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:apply-templates select="/root/modules/Bharun/Bharun/Bitrun/BitNozzle[bitrunUid=$bitrunUid]"/>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template match="modules/Bharun/Bharun/Bitrun/BitNozzle">
		<fo:table-row>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="nozzleQty"/> x <xsl:value-of select="nozzleSize"/>
					<fo:inline color="white">.</fo:inline><xsl:value-of select="nozzleSize/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- bha section END -->

</xsl:stylesheet>