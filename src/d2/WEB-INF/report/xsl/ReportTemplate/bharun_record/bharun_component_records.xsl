<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- Bharun Component records START -->
	<xsl:template match="BhaComponent">
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">					
					<xsl:choose>													
						<xsl:when test = "string(type/@lookupLabel)=''">
							<xsl:value-of select="type"/>										
						</xsl:when>													
						<xsl:otherwise>
							<xsl:value-of select="type/@lookupLabel"/>
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:value-of select="descr"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="jointlength"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="jointlength/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="componentOd"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="componentOd/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="componentId"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="componentId/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="serialNum"/>
				</fo:block>
			</fo:table-cell>		
		</fo:table-row>
	</xsl:template>
	<!-- Bharun Component records END -->
	
</xsl:stylesheet>