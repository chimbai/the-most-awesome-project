<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:import href="./bharun_component_header.xsl" />	
	<xsl:import href="./bharun_component_records.xsl" />
	
	<xsl:template name="fwr_bha_records">
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-left="0.25px
				solid black">
				<fo:block text-align="left">Date In</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" text-align="left"
				padding-end="0.1cm">
				<fo:block>
					<xsl:variable name="datein" select="datein"/>					
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Daily/Daily[dailyUid=$datein]/dayDate/@epochMS,'dd MMM yyyy')"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell border-bottom="0.25px solid black" number-rows-spanned="10"
				border-left="0.25px solid black" border-right="0.25px solid black"> <!-- number-rows-spanned depends on how many rows in the bharun field -->
				<fo:block>
					<xsl:call-template name="bhacomponent">
						<xsl:with-param name="bharunid" select="bharunUid"/>
					</xsl:call-template>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-left="0.25px
				solid black">
				<fo:block text-align="left">Depth In/ Depth Out</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" text-align="left"
				padding-end="0.2cm">
				<fo:block>
					<xsl:value-of select="depthInMdMsl"/>/<xsl:value-of select="depthOutMdMsl"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-left="0.25px
				solid black">
				<fo:block text-align="left">Length</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" text-align="left"
				padding-end="0.2cm">
				<fo:block>
					<xsl:value-of select="bhaTotalLength"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-left="0.25px
				solid black">
				<fo:block text-align="left">Weight Wet</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" text-align="left"
				padding-end="0.2cm">
				<fo:block>
					<xsl:value-of select="weightBhaTotalWet"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			 <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-left="0.25px
				solid black">
				<fo:block text-align="left">Weight Blw/Jar Wet</fo:block>
			 </fo:table-cell>
			 <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" text-align="left"
				padding-end="0.2cm">
				<fo:block>
					<xsl:value-of select="weightbelowjarwet"/>
				</fo:block>
			 </fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-left="0.25px
				solid black">
				<fo:block text-align="left">String Weight</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" text-align="left"
				padding-end="0.2cm">
				<fo:block>
					<xsl:value-of select="BharunDailySummary/weightString"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-left="0.25px
				solid black">
				<fo:block text-align="left">Pick-Up Weight</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" text-align="left"
				padding-end="0.2cm">
				<fo:block>
					<xsl:value-of select="BharunDailySummary/weightPickup"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-left="0.25px
				solid black">
				<fo:block text-align="left">Slack-Off Weight</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" text-align="left"
				padding-end="0.2cm">
				<fo:block>
					<xsl:value-of select="BharunDailySummary/weightSlackOff"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>        
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-left="0.25px
				solid black" text-align="left" number-columns-spanned="2">
				<fo:block text-align="left">BHA Description: <xsl:value-of select="descr"/></fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-left="0.25px
				solid black" text-align="left" number-columns-spanned="2">
				<fo:block> BHA Run Comment: <xsl:value-of select="bhaDescription"/></fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
</xsl:stylesheet>