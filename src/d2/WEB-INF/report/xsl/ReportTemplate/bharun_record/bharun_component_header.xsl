<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Bharun Component header START -->
	<xsl:template name="bharun_component_header">		
		<fo:table-row>
			<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Equipment</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Description</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Length</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">OD</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">ID</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Serial #</fo:block>
			</fo:table-cell>					
		</fo:table-row>					
	</xsl:template>
	<!-- Bharun Component header END -->
	
</xsl:stylesheet>