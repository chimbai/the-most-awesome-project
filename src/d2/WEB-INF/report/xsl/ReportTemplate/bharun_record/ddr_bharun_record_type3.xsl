<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TEMPLATE: modules/Bharun -->
	<xsl:template match="modules/Bharun">
		<xsl:apply-templates select="Bharun"/>
	</xsl:template>
	
	<!-- TEMPLATE: Bharun -->
	<xsl:template match="Bharun">
	
	<!-- BITRUN -->
	<!--  
		<fo:table-row>
			<fo:table-cell>
				<fo:block>
					<xsl:variable name="bharunUid" select="bharunUid"/>                                   	
					<xsl:apply-templates select="Bitrun[bharunUid=$bharunUid]">
						<xsl:sort select="bitrunNumber" data-type="varchar"/>						
					</xsl:apply-templates>
					<xsl:apply-templates select="BharunDailySummary"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		-->
	
		<fo:table-row>
			<fo:table-cell>
			
				<fo:block keep-together="always">
				
					<!-- TABLE -->
					<fo:table width="100%" table-layout="fixed" space-after="6pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						
						<!-- BODY -->
						<fo:table-body>
																		
							<!-- BHA # -->
							<fo:table-row>
								<fo:table-cell>
									<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
										<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
										<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
										<fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
										
										<!-- HEADER -->
										<fo:table-header>
											<fo:table-row>
												<fo:table-cell number-columns-spanned="4" padding="2px" padding-bottom="0px" background-color="#DDDDDD"
													border="thin solid black">														
													<fo:block font-size="10pt" font-weight="bold">
														BHA # <xsl:value-of select="bhaRunNumber"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-header>
										
										<!-- BODY -->
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
		                                            <fo:table width="100%" table-layout="fixed">
	                                            		<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
														<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
														<fo:table-body>
															<fo:table-row>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																	<fo:block>
																		Weight Dry:
																	</fo:block>
																</fo:table-cell>				
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																	<fo:block text-align="right">
																		<xsl:value-of select="weightBhaTotalDry"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:if test="weightBhaTotalDry!=''">
																		<xsl:value-of select="weightBhaTotalDry/@uomSymbol"/>
																		</xsl:if>
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>
															<fo:table-row>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																	<fo:block>
																		Type:
																	</fo:block>
																</fo:table-cell>				
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																	<fo:block text-align="right">																			
																		<xsl:value-of select="bhaType"/>																					
																	</fo:block>																
																</fo:table-cell>
															</fo:table-row>								
														</fo:table-body>
													</fo:table>
												</fo:table-cell>
												
												<fo:table-cell border-right="thin solid black" border-bottom="thin solid black">
		                                            <fo:block>
		                                            	<fo:table width="100%" table-layout="fixed">
		                                            		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
															<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
															<fo:table-body>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block>
																			Length:
																		</fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block text-align="right">																			
																			<xsl:value-of select="bhaTotalLength"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:if test="bhaTotalLength!=''">
																			<xsl:value-of select="bhaTotalLength/@uomSymbol"/>
																			</xsl:if>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block>
																			String Weight:
																		</fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/weightString"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:if test="BharunDailySummary/weightString!=''">
																			<xsl:value-of select="BharunDailySummary/weightString/@uomSymbol"/>
																			</xsl:if>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block>
																			Pickup Weight:
																		</fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/weightPickup"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:if test="BharunDailySummary/weightPickup!=''">
																			<xsl:value-of select="BharunDailySummary/weightPickup/@uomSymbol"/>
																			</xsl:if>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block>
																			Slack-Off Weight:
																		</fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/weightSlackOff"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:if test="BharunDailySummary/weightSlackOff!=''">
																			<xsl:value-of select="BharunDailySummary/weightSlackOff/@uomSymbol"/>
																			</xsl:if>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
															</fo:table-body>
														</fo:table>
													</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border-right="thin solid black" border-bottom="thin solid black">
		                                            <fo:block>
		                                            	<fo:table width="100%" table-layout="fixed">
		                                            		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
															<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
															<fo:table-body>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block>
																			Torque (max):
																		</fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/torqueMax"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:if test="BharunDailySummary/torqueMax!=''">
																			<xsl:value-of select="BharunDailySummary/torqueMax/@uomSymbol"/>
																			</xsl:if>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block>
																			Torque On Btm:
																		</fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/torqueOnBottomAvg"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:if test="BharunDailySummary/torqueOnBottomAvg!=''">
																			<xsl:value-of select="BharunDailySummary/torqueOnBottomAvg/@uomSymbol"/>
																			</xsl:if>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block>
																			Torque Off Btm:
																		</fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/torqueOffBottomAvg"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:if test="BharunDailySummary/torqueOffBottomAvg!=''">
																			<xsl:value-of select="BharunDailySummary/torqueOffBottomAvg/@uomSymbol"/>
																			</xsl:if>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
															</fo:table-body>
														</fo:table>
													</fo:block>
												</fo:table-cell>
												
												<fo:table-cell border-right="thin solid black" border-bottom="thin solid black">
		                                            <fo:block>
		                                            	<fo:table width="100%" table-layout="fixed">
		                                            		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
															<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
															<fo:table-body>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block>
																			DC (1) Ann Vel.:
																		</fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/annvelDc1"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:if test="BharunDailySummary/annvelDc1!=''">
																			<xsl:value-of select="BharunDailySummary/annvelDc1/@uomSymbol"/>
																			</xsl:if>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block>
																			DC (2) Ann Vel.:
																		</fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/annvelDc2"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:if test="BharunDailySummary/annvelDc2!=''">
																			<xsl:value-of select="BharunDailySummary/annvelDc2/@uomSymbol"/>
																			</xsl:if>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block>
																			HWDP Ann. Vel.:
																		</fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/annvelHwdp"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:if test="BharunDailySummary/annvelHwdp!=''">
																			<xsl:value-of select="BharunDailySummary/annvelHwdp/@uomSymbol"/>
																			</xsl:if>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block>
																			DP Ann. Vel.:
																		</fo:block>
																	</fo:table-cell>				
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block text-align="right">																			
																			<xsl:value-of select="BharunDailySummary/annvelDp"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:if test="BharunDailySummary/annvelDp!=''">
																			<xsl:value-of select="BharunDailySummary/annvelDp/@uomSymbol"/>
																			</xsl:if>
																		</fo:block>																
																	</fo:table-cell>
																</fo:table-row>
															</fo:table-body>
														</fo:table>
													</fo:block>
												</fo:table-cell>
												
											</fo:table-row>
											<!--  
											<fo:table-row>
												<fo:table-cell number-columns-spanned="2" border-left="thin solid black" border-right="thin solid black">
													<fo:block>
														<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
															<fo:table-column column-number="1" column-width="proportional-column-width(18)"/>
															<fo:table-column column-number="2" column-width="proportional-column-width(82)"/>
															<fo:table-body>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block font-weight="bold">
																			BHA Description:
																		</fo:block>
																	</fo:table-cell>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block linefeed-treatment="preserve">
																			<xsl:value-of select="descr"/>
																		</fo:block>
																	</fo:table-cell>
																</fo:table-row>
																<fo:table-row>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block font-weight="bold">
																			BHA Run Comment:
																		</fo:block>
																	</fo:table-cell>
																	<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																		<fo:block linefeed-treatment="preserve">
																			<xsl:value-of select="bhaDescription"/>
																		</fo:block>
																	</fo:table-cell>
																</fo:table-row>
															</fo:table-body>
														</fo:table>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											-->
										</fo:table-body>
									</fo:table>	                                            
								</fo:table-cell>	
							</fo:table-row>
							
				            <!-- BHA COMPONENT -->
				            <fo:table-row>
								<fo:table-cell>
				                	<fo:table width="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black">
				                		<fo:table-column column-number="1" column-width="proportional-column-width(3)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(20)"/>
										<fo:table-column column-number="3" column-width="proportional-column-width(30)"/>
										<fo:table-column column-number="4" column-width="proportional-column-width(9)"/>
										<fo:table-column column-number="5" column-width="proportional-column-width(9)"/>
										<fo:table-column column-number="6" column-width="proportional-column-width(9)"/>
										<fo:table-column column-number="7" column-width="proportional-column-width(13)"/>
										<fo:table-column column-number="8" column-width="proportional-column-width(7)"/>		
										<fo:table-header>
											<!--  
											<fo:table-row>
												<fo:table-cell number-columns-spanned="8" padding="2px" padding-bottom="1px" background-color="#DDDDDD"
													border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
													<fo:block font-weight="bold">
														BHA Component
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											-->
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
													<fo:block text-align="center">
														#
													</fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
													<fo:block text-align="center">
														Equipment
													</fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
													<fo:block text-align="center">
														Tool Description
													</fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
													<fo:block text-align="center">
														Length
													</fo:block>
													<xsl:if test="string(BhaComponent/jointlength) != ''">
														<fo:block text-align="center">
															(<xsl:value-of select="BhaComponent/jointlength/@uomSymbol"/>)
														</fo:block>
													</xsl:if>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
													<fo:block text-align="center">
														O.D.
													</fo:block>
													<xsl:if test="string(BhaComponent/componentOd) != ''">
														<fo:block text-align="center">
															(<xsl:value-of select="BhaComponent/componentOd/@uomSymbol"/>)
														</fo:block>
													</xsl:if>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
													<fo:block text-align="center">
														I.D.
													</fo:block>
													<xsl:if test="string(BhaComponent/componentId) != ''">
														<fo:block text-align="center">
															(<xsl:value-of select="BhaComponent/componentId/@uomSymbol"/>)
														</fo:block>
													</xsl:if>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
													<fo:block text-align="center">
														Serial #
													</fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
													<fo:block text-align="center">
														Hours
													</fo:block>
													<xsl:if test="string(BhaComponent/totalHoursUsed) != ''">
														<fo:block text-align="center">
															(<xsl:value-of select="BhaComponent/totalHoursUsed/@uomSymbol"/>)
														</fo:block>
													</xsl:if>
												</fo:table-cell>					
											</fo:table-row>										
										</fo:table-header>
										<fo:table-body>
											<xsl:variable name="bharunUid" select="bharunUid"/>
											<xsl:apply-templates select="BhaComponent[bharunUid=$bharunUid]">
												<xsl:sort select="sequence" data-type="number"/>
											</xsl:apply-templates>		
										</fo:table-body>
									</fo:table>
								</fo:table-cell>
							</fo:table-row>
						
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>

	</xsl:template>
	
	<!-- TEMPLATE: BharunDailySummary -->
	<!--  
	<xsl:template match="BharunDailySummary">
	
		<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(50)"/>
			<fo:table-body>						
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3">
						<fo:block>
							<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
	                           	<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(33)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(34)"/>											
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell number-columns-spanned="3" padding="2px" background-color="#DDDDDD"
											border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
											<fo:block font-weight="bold">
												Bit Hours
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>											
								<fo:table-body>
									<fo:table-row>
									-->
										<!-- CELL 1 -->
										<!--  
										<fo:table-cell>
											<fo:block>
												<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
	                                           		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
													<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
													<fo:table-body>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black">																
																<fo:block>
																	Duration:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="duration"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="duration/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-bottom="thin solid black">																
																<fo:block>
																	Total Duration:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="dynaAttr/totalDuration"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="dynaAttr/totalDuration/@uomSymbol"/>																				
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>																		
													</fo:table-body>
												</fo:table>		
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
	                                           		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
													<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
													<fo:table-body>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																<fo:block>
																	Progress:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="progress"/>
																	<fo:inline color="white">.</fo:inline>																					
																	<xsl:value-of select="progress/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-bottom="thin solid black">													
																<fo:block>
																	Total Progress:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="dynaAttr/totalProgress"/>
																	<fo:inline color="white">.</fo:inline>																					
																	<xsl:value-of select="dynaAttr/totalProgress/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>																		
													</fo:table-body>
												</fo:table>		
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
	                                           		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
													<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
													<fo:table-body>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
																<fo:block>
																	ROP:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="rop"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="rop/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-bottom="thin solid black">																
																<fo:block>
																	ROP (Avg):
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="dynaAttr/avgROP"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="dynaAttr/avgROP/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>																		
													</fo:table-body>
												</fo:table>		
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
						
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3">		
						<fo:block>
	                       	<fo:table width="100%" table-layout="fixed">
	                           	<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(33)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(34)"/>																	
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell number-columns-spanned="3" padding="2px" padding-bottom="2px" background-color="#DDDDDD" display-align="center"
											border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
											<fo:block text-align="left" font-weight="bold"> Directional Data </fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block text-align="left">
												<fo:table width="100%" table-layout="fixed">
	                                           		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
													<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
													<fo:table-body>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black">
																<fo:block>
																	Slide Time:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="slideHours"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="slideHours/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black">
																<fo:block>
																	Slide (%):
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="percentSlide"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="percentSlide/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black">
																<fo:block>
																	Total Slide Time:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="dynaAttr/totalSlideHours"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="dynaAttr/totalSlideHours/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-bottom="thin solid black">
																<fo:block>
																	Total KRevs:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="krevs"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="krevs/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>		
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>																
											<fo:block>
												<fo:table width="100%" table-layout="fixed">
	                                           		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
													<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
													<fo:table-body>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																<fo:block>
																	Rotate Time:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="durationRotated"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="durationRotated/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																<fo:block>
																	Rotate (%):
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="percentRotated"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="percentRotated/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																<fo:block>
																	Total Rotate Time:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="dynaAttr/totalDurationRotated"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="dynaAttr/totalDurationRotated/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-bottom="thin solid black">
																<fo:block>
																	HSI:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="hsi"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="hsi/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>		
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>																
											<fo:block>
												<fo:table width="100%" table-layout="fixed">
	                                           		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
													<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
													<fo:table-body>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																<fo:block>
																	Circ. Time:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="durationCirculated"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="durationCirculated/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																<fo:block>
																	Circ. (%):
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="percentCirculated"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="percentCirculated/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
																<fo:block>
																	Total Circ. Time:
																</fo:block>
															</fo:table-cell>
															<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
																<fo:block text-align="right">																			
																	<xsl:value-of select="dynaAttr/totalDurationCirculated"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="dynaAttr/totalDurationCirculated/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell number-columns-spanned="2" padding="1px" padding-left="2px" padding-right="2px"
																 border-right="thin solid black" border-bottom="thin solid black">																
																<fo:block>
																	<fo:inline color="white">.</fo:inline>
																</fo:block>
															</fo:table-cell>																								
														</fo:table-row>
													</fo:table-body>
												</fo:table>		
											</fo:block>
										</fo:table-cell>
									</fo:table-row>																																			
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
				
	</xsl:template>
	-->
	
	<!-- TEMPLATE: modules/Bharun/Bharun/Bitrun -->
	<!--  
	<xsl:template match="modules/Bharun/Bharun/Bitrun">
																							
		<fo:table width="100%" table-layout="fixed">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(50)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell padding="2px" background-color="#DDDDDD" border="thin solid black" display-align="center">
						<fo:block font-size="10pt" font-weight="bold">
							Bit #<xsl:value-of select="bitrunNumber"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2">
						<fo:table width="100%" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(1)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(1)"/>
							<fo:table-column column-number="3" column-width="proportional-column-width(1)"/>
							<fo:table-column column-number="4" column-width="proportional-column-width(1)"/>
							<fo:table-column column-number="5" column-width="proportional-column-width(1)"/>
							<fo:table-column column-number="6" column-width="proportional-column-width(1)"/>
							<fo:table-column column-number="7" column-width="proportional-column-width(1)"/>
							<fo:table-column column-number="8" column-width="proportional-column-width(1)"/>
							<fo:table-column column-number="9" column-width="proportional-column-width(1)"/>
							<fo:table-header>
								<fo:table-row>
									<fo:table-cell padding="2px" border-right="thin solid black" border-top="thin solid black">
										<fo:block text-align="center" font-weight="bold">
											Wear
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="2px" border-right="thin solid black" border-top="thin solid black">
										<fo:block text-align="center" font-weight="bold">
											I
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="2px" border-right="thin solid black" border-top="thin solid black">
										<fo:block text-align="center" font-weight="bold">
											O1
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="2px" border-right="thin solid black" border-top="thin solid black">
										<fo:block text-align="center" font-weight="bold">
											D
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="2px" border-right="thin solid black" border-top="thin solid black">
										<fo:block text-align="center" font-weight="bold">
											L
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="2px" border-right="thin solid black" border-top="thin solid black">
										<fo:block text-align="center" font-weight="bold">
											B
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="2px" border-right="thin solid black" border-top="thin solid black">
										<fo:block text-align="center" font-weight="bold">
											G
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="2px" border-right="thin solid black" border-top="thin solid black">
										<fo:block text-align="center" font-weight="bold">
											O2
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="2px" border-right="thin solid black" border-top="thin solid black">
										<fo:block text-align="center" font-weight="bold">
											R
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-header>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
										<fo:block>
											<fo:inline color="white">.</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
										<fo:block text-align="center">
											<xsl:value-of select="condFinalInner"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
										<fo:block text-align="center">
											<xsl:value-of select="condFinalOuter"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
										<fo:block text-align="center">
											<xsl:value-of select="condFinalDull"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
										<fo:block text-align="center">
											<xsl:value-of select="condFinalLocation"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
										<fo:block text-align="center">
											<xsl:value-of select="condFinalBearing"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
										<fo:block text-align="center">
											<xsl:value-of select="condFinalGauge"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
										<fo:block text-align="center">
											<xsl:for-each select="condFinalOther/multiselect">
												<xsl:sort select="@value"/>
												<xsl:value-of select="@value"/>
												<xsl:if test="position() != last()">
													<fo:inline>/</fo:inline>
												</xsl:if>
											</xsl:for-each>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
										<fo:block text-align="center">
											<xsl:value-of select="condFinalReason"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
			-->
				<!-- ROW 1 -->
				<!--  
				<xsl:variable name="bharunid" select="bharunUid"/>
				<fo:table-row>
				-->
					<!-- Column 1 -->
					<!--  
					<fo:table-cell border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block>
							<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Size:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:choose>													
													<xsl:when test="string(bitDiameter/@lookupLabel)=''">
														<xsl:value-of select="bitDiameter"/>										
													</xsl:when>													
													<xsl:otherwise>
														<xsl:value-of select="bitDiameter/@lookupLabel"/>
													</xsl:otherwise>
												</xsl:choose>												
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Manufacturer:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:choose>													
													<xsl:when test="string(make/@lookupLabel)=''">
														<xsl:value-of select="make"/>										
													</xsl:when>													
													<xsl:otherwise>
														<xsl:value-of select="make/@lookupLabel"/>
													</xsl:otherwise>
												</xsl:choose>															
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Type:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="cutterType/@lookupLabel"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Serial No:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="serialNumber"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Bit Model:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="model"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					-->
					
					<!-- Column 2 -->
					<!--  
					<fo:table-cell border-right="thin solid black" border-bottom="thin solid black">
						<fo:block>
							<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								<fo:table-body>												
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												IADC#:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="iadcCode"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												TFA:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="tfa"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="tfa/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>												
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					-->
					
					<!-- Column 3 -->
					<!--  
					<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block>
							<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-width="proportional-column-width(100)"/>											
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" background-color="#DDDDDD" border="thin solid black">
											<fo:block font-weight="bold" text-align="center">
												Nozzles
											</fo:block>
										</fo:table-cell>													
									</fo:table-row>
									<xsl:call-template name="BitNozzle">
										<xsl:with-param name="bitrunUid" select="bitrunUid"/>
									</xsl:call-template>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3">
						<fo:block>
							<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-number="1" column-width="proportional-column-width(18)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(82)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black">
											<fo:block font-weight="bold">
												Bit run comment:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
											<fo:block linefeed-treatment="preserve">
												<xsl:value-of select="comment"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-bottom="thin solid black">
											<fo:block font-weight="bold">
												Bit wear comment:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
											<fo:block linefeed-treatment="preserve">
												<xsl:value-of select="bitwearComment"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>									
				
	</xsl:template>
	-->
	
	<!-- TEMPLATE: BitNozzle -->
	<!--  
	<xsl:template name="BitNozzle">
		<xsl:param name="bitrunUid"/>
		<xsl:choose>
			<xsl:when test="count(/root/modules/Bharun/Bharun/Bitrun/BitNozzle[bitrunUid=$bitrunUid]) = '0'">
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"
						border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="center">
							No. x Size
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"
						border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="center">
							No. x Size
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell
						border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
						<fo:table width="100%" table-layout="fixed">
							<fo:table-column column-width="proportional-column-width(100)"/>
							<fo:table-body>
								<xsl:apply-templates select="/root/modules/Bharun/Bharun/Bitrun/BitNozzle[bitrunUid=$bitrunUid]"/>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	-->

	<!-- TEMPLATE: modules/Bharun/Bharun/Bitrun/BitNozzle -->
	<!--  
	<xsl:template match="modules/Bharun/Bharun/Bitrun/BitNozzle">
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block text-align="center">
							<xsl:value-of select="nozzleQty"/> x <xsl:value-of select="nozzleSize"/>
							<xsl:value-of select="nozzleSize/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE" border-left="thin solid black" border-right="thin solid black">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block text-align="center">
							<xsl:value-of select="nozzleQty"/> x <xsl:value-of select="nozzleSize"/>
							<xsl:value-of select="nozzleSize/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	-->
	
	<!-- TEMPLATE: BhaComponent -->
	<xsl:template match="BhaComponent">
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="sequence"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>     
							<xsl:choose>
								<xsl:when test="string(type/@lookupLabel)=''">
									<xsl:value-of select="type"/>          
								</xsl:when>             
								<xsl:otherwise>
									<xsl:value-of select="type/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
		   			</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="descr"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="jointlength"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="componentOd"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="componentId"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="serialNum"/>
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="totalHoursUsed"/>
						</fo:block>
					</fo:table-cell>				
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="sequence"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>     
							<xsl:choose>
								<xsl:when test="string(type/@lookupLabel)=''">
									<xsl:value-of select="type"/>          
								</xsl:when>             
								<xsl:otherwise>
									<xsl:value-of select="type/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
		   			</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="descr"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="jointlength"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="componentOd"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="componentId"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="serialNum"/>
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="totalHoursUsed"/>
						</fo:block>
					</fo:table-cell>				
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>