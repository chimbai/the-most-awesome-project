<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	<!-- TEMPLATE: modules/GeneralComment -->
	<xsl:template match="modules/WellHead">
	<fo:block margin="0cm" padding="2px" background-color="#DDDDDD" border-top="thin solid black" border-left="thin solid black" border-right="thin solid black" font-size="10pt" font-weight="bold" text-align="left">
		Wellhead Data
	</fo:block>
	<xsl:apply-templates select="WellHead"/>
	</xsl:template>
	
	<!-- TEMPLATE: WellHead -->
	<xsl:template match="WellHead">
	<fo:block keep-together="always">
			<!-- TABLE -->
			<fo:table border-top="thin solid black" width="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt">>
				<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
			
				
				<!-- BODY -->
				<fo:table-body>
		
				<fo:table-row>
					<fo:table-cell padding="2px" border-left="thin solid black">
						<fo:block text-align="left">
							Wellhead Location
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="thin solid black">
						<fo:block text-align="right">
							N:<xsl:value-of select="locationNorthing"/>
							E:<xsl:value-of select="locationEasting"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px">
						<fo:block text-align="left">
							Install Date
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(installDateTime/@epochMS,'dd MMM yyyy')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="left">
							TVDSS
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="tvdss"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="tvdss/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-bottom="thin solid black">
						<fo:block text-align="left">
							Install Cost
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="right">
							$ <xsl:value-of select="installCost"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="1" padding="2px" border-left="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="left">
							General Comment
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="3" padding="2px" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="left">
							<xsl:value-of select="comment"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
	
				</fo:table-body>
				
			</fo:table>	
			</fo:block>	
	</xsl:template>

</xsl:stylesheet>
