<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/Daily/Daily" mode="OperationsSummary">  
    <fo:table inline-progression-dimension="100%" table-layout="fixed" border="0.75px solid black" wrap-option="wrap" space-after="2pt">
    <fo:table-column column-number="1" column-width="proportional-column-width(30)" />
    <fo:table-column column-number="2" column-width="proportional-column-width(70)" />
    <fo:table-header>
    <fo:table-row font-size="10pt">
    	<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" font-weight="bold" number-columns-spanned="2" text-align="center" background-color="rgb(37,87,147)">
        	<fo:block color="white">Geology 24hr Operations Summary</fo:block>
		</fo:table-cell> 
    </fo:table-row>
	</fo:table-header>
	<fo:table-body>  
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm">24hr Summary:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm" border-bottom="0.75px solid black" display-align="center">
			<fo:block linefeed-treatment="preserve">
			<fo:inline> <xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportPeriodSummary"/> </fo:inline>
			</fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm" font-weight="bold"  border-right="0.75px solid black" display-align="center">
			<fo:block>24hr Forward Plan:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm" display-align="center">
			<fo:block linefeed-treatment="preserve">
			<fo:inline> <xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geolOps"/> </fo:inline>
			</fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	
	</fo:table-body>
	</fo:table>
	</xsl:template>
</xsl:stylesheet>