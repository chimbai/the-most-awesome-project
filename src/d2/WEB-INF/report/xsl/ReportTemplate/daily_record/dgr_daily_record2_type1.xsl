<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/Daily/Daily" mode="HrsUpdate">  
    <fo:table inline-progression-dimension="19.5cm" table-layout="fixed" border="0.75px solid black" wrap-option="wrap">
    <fo:table-column column-number="1" column-width="proportional-column-width(23)" />
    <fo:table-column column-number="2" column-width="proportional-column-width(77)" />
    <fo:table-header>
    <fo:table-row font-size="10pt">
    	<fo:table-cell padding="0.05cm" border-bottom="0.75px solid black" font-weight="bold" number-columns-spanned="2" text-align="center" background-color="rgb(37,87,147)">
        	<fo:block color="white">06:00 Hrs Update</fo:block>
		</fo:table-cell> 
    </fo:table-row>
	</fo:table-header>
	<fo:table-body>  
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm" font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block>Time / Date:</fo:block>
		</fo:table-cell>
		<fo:table-cell text-align="left" padding="3px" border-bottom="0.75px solid black">
						<fo:block>											
									06:00 Hrs on<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/cutOffDateTime"/>																				
						</fo:block>
	</fo:table-cell>
	</fo:table-row>
	
	
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm" font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block>Depth (MDRT):</fo:block>
		</fo:table-cell>
		<fo:table-cell text-align="left" padding="3px" border-bottom="0.75px solid black">
						<fo:block>	
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/depth0600MdMsl)!=''">
									<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/depth0600MdMsl"/>
									<fo:inline color="white">.</fo:inline>	
									<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/depth0600MdMsl/@uomSymbol"/>										
								</xsl:when>													
								<xsl:otherwise>
									<fo:inline color="white">.</fo:inline>
								</xsl:otherwise>
							</xsl:choose>																				
						</fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm" font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block>Progress Since Midnight:</fo:block>
		</fo:table-cell>
		<fo:table-cell text-align="left" padding="3px" border-bottom="0.75px solid black">
						<fo:block>	
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/geology0600ProgressSinceMidnight)!=''">
									<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geology0600ProgressSinceMidnight"/>	
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geology0600ProgressSinceMidnight/@uomSymbol"/>										
								</xsl:when>													
								<xsl:otherwise>
									<fo:inline color="white">.</fo:inline>
								</xsl:otherwise>
							</xsl:choose>																				
						</fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm" font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block>Status @ 0600hrs:</fo:block>
		</fo:table-cell>
		<fo:table-cell text-align="left" padding="3px" border-bottom="0.75px solid black">
						<fo:block linefeed-treatment="preserve">											
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportCurrentStatus"/>																				
						</fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm" font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block>ROP Summary:</fo:block>
		</fo:table-cell>
		<fo:table-cell text-align="left" padding="3px" border-bottom="0.75px solid black">
						<fo:block linefeed-treatment="preserve">											
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geology0600RopSummary"/>																				
						</fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm" font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block>Formation Summary:</fo:block>
		</fo:table-cell>
		<fo:table-cell text-align="left" padding="3px" border-bottom="0.75px solid black">
						<fo:block linefeed-treatment="preserve">											
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geology0600FormationSummary"/>																				
						</fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm" font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block>Lithology Summary:</fo:block>
		</fo:table-cell>
		<fo:table-cell text-align="left" padding="3px" border-bottom="0.25px solid black">
						<fo:block linefeed-treatment="preserve">											
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geology0600LithologySummary"/>																				
						</fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm" font-weight="bold" border-right="0.75px solid black" display-align="center">
			<fo:block>Gas Summary:</fo:block>
		</fo:table-cell>
		<fo:table-cell text-align="left" padding="3px">
						<fo:block linefeed-treatment="preserve">											
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geology0600GasSummary"/>																				
						</fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	</fo:table-body>
	</fo:table>
	</xsl:template>
</xsl:stylesheet>