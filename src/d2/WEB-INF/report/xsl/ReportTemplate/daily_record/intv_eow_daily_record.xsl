<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- IDS INTERVENTION REPORT DAILY SECTION -->
	<xsl:template match="modules/ReportDaily/ReportDaily" mode="eow_daily">
		<fo:table-row space-after="6pt">
			<fo:table-cell padding="0.1cm">
				<fo:block>
					<fo:inline font-weight="bold">
					   <xsl:value-of select="d2Utils:formatDateFromEpochMS(dynaAttr/daydate/@epochMS,'dd MMM yyyy')"/>
					</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm">
				<fo:block linefeed-treatment="preserve">
					<xsl:value-of select="reportPeriodSummary"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
</xsl:stylesheet>