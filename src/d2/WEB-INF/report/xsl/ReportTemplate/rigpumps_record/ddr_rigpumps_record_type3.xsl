<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- TEMPLATE: modules/RigPump -->
	<xsl:template match="modules/RigPumpDaily">
	
		<fo:block keep-together="always">
			
			<!-- TABLE -->
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black" space-after="6pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(18)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(9)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(9)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(6)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(9)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(9)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(9)"/>
				<fo:table-column column-number="9" column-width="proportional-column-width(9)"/>
				<fo:table-column column-number="10" column-width="proportional-column-width(9)"/>
				<fo:table-column column-number="11" column-width="proportional-column-width(9)"/>
				
				<!-- HEADER -->
				<fo:table-header>
				
					<!-- HEADER 1 -->
					<fo:table-row>
						<fo:table-cell number-columns-spanned="11" padding="1px" background-color="#DDDDDD" border="thin solid black" display-align="center">
							<fo:block font-size="10pt" font-weight="bold">
								Pumps
							</fo:block>
						</fo:table-cell>					
					</fo:table-row>
					
					<!-- HEADER 2 -->
					<fo:table-row>
						<fo:table-cell number-columns-spanned="7" padding="2px" display-align="center"
							border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
							<fo:block text-align="left">
								Pump Data - Last 24 Hrs
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="4" padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="left">
								Slow Pump Data
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<!-- HEADER 3 -->
					<fo:table-row>
						<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								No.
							</fo:block>							
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Type
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Liner
							</fo:block>
							<fo:block text-align="center">
								(<xsl:value-of select="RigPumpParam/liner/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								SPM
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Eff.
							</fo:block>
							<fo:block text-align="center">
								(<xsl:value-of select="RigPumpParam/efficiency/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Flow
							</fo:block>
							<fo:block text-align="center">
								(<xsl:value-of select="RigPumpParam/volumeOut/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								SPP
							</fo:block>
							<fo:block text-align="center">
								(<xsl:value-of select="RigPumpParam/pressureOut/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								SPM
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								SPP
							</fo:block>
							<xsl:if test="RigPumpParam/RigSlowPumpParam[last()]/pressure!=''">
							<fo:block text-align="center">
								(<xsl:value-of select="RigPumpParam/RigSlowPumpParam/pressure/@uomSymbol"/>)
							</fo:block>
							</xsl:if>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Depth
							</fo:block>
							<fo:block text-align="center">
								(<xsl:value-of select="RigPumpParam/sampleDepth/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								MW
							</fo:block>
							<fo:block text-align="center">
								(<xsl:value-of select="RigPumpParam/mw/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
				</fo:table-header>
				
				<!-- BODY -->
				<fo:table-body>
					<xsl:apply-templates select="RigPumpParam"/>
				</fo:table-body>
				
			</fo:table>
			
		</fo:block>
													
	</xsl:template>

	<!-- TEMPLATE: RigPump -->
	<xsl:template match="RigPumpParam">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
		
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="unitNumber"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="unitName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="liner"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="spm"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="efficiency"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="volumeOut"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="pressureOut"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="RigSlowPumpParam[last()]/rate"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="RigSlowPumpParam[last()]/pressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="sampleDepth"/>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="mw"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
			</xsl:when>
			<xsl:otherwise>
			
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="unitNumber"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="unitName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="liner"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="spm"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="efficiency"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="volumeOut"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="pressureOut"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="RigSlowPumpParam[last()]/rate"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="RigSlowPumpParam[last()]/pressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="sampleDepth"/>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="mw"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- <xsl:template match="RigPumpParam">
		<xsl:apply-templates select="RigSlowPumpParam"/>
	</xsl:template>
	
	<xsl:template match="RigSlowPumpParam">
		<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap">
			<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
			
			<fo:table-body>
				<fo:table-row font-size="7pt">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="rate"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block text-align="right">
							<xsl:value-of select="pressure"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>		-->
</xsl:stylesheet>