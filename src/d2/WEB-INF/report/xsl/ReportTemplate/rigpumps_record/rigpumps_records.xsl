<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Rig Pump Param records START -->
	<xsl:template match="RigPump/RigPumpParam">
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="../unitNumber"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="../unitName"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="../linerId"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="mw"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="efficiency"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="spm"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="pressureOut"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="volumeOut"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="sampleDepth"/>
				</fo:block>
			</fo:table-cell>
			<!-- make a loop for RigSlowPumpParam -->
			<xsl:if test="count(RigSlowPumpParam) &gt; 0">
				<fo:table-cell number-columns-spanned="4">
					<fo:block>
						<xsl:apply-templates select="RigSlowPumpParam" mode="RigParam"/>
					</fo:block>	
				</fo:table-cell>
			</xsl:if>
			<xsl:if test="count(RigSlowPumpParam) = 0">
				<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
					padding-start="0.05cm" padding-end="0.05cm">
					<fo:block text-align="right">
						<fo:inline color="white" font-weight="bold"> ! </fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
					padding-start="0.05cm" padding-end="0.05cm">
					<fo:block text-align="right">
						<fo:inline color="white" font-weight="bold"> ! </fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
					padding-start="0.05cm" padding-end="0.05cm">
					<fo:block text-align="right">
						<fo:inline color="white" font-weight="bold"> ! </fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
					<fo:block text-align="right">
						<fo:inline color="white" font-weight="bold"> ! </fo:inline>
					</fo:block>
				</fo:table-cell>
			</xsl:if>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="RigSlowPumpParam" mode="RigParam">
		<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
						padding-start="0.05cm" padding-end="0.05cm">
						<fo:block text-align="right">
							<xsl:value-of select="checkNumber"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
						padding-start="0.05cm" padding-end="0.05cm">
						<fo:block text-align="right">
							<xsl:value-of select="rate"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
						padding-start="0.05cm" padding-end="0.05cm">
						<fo:block text-align="right">
							<xsl:value-of select="pressure"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
						<fo:block text-align="right">
							<xsl:value-of select="flow"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- Rig Pump Param records END -->

</xsl:stylesheet>