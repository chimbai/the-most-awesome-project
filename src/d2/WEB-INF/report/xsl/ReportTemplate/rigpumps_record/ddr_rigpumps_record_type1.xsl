<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<xsl:import href="./rigpumps_header.xsl"/>
	<xsl:import href="./rigpumps_records.xsl"/>

	<!-- RigPump / pumps section BEGIN -->
	<xsl:template match="modules/RigPump">		
		<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt"
			border-left="0.5px solid black" border-right="0.5px solid black" border-bottom="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(3)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(12)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="8" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="10" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="11" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="12" column-width="proportional-column-width(6)"/>		
			<fo:table-column column-number="13" column-width="proportional-column-width(6)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="13" padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
						background-color="rgb(224,224,224)" border-top="0.5px solid black"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Pumps
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black"
						padding="0.05cm" number-columns-spanned="9" border-right="0.25px solid black">
						<fo:block font-weight="bold" font-size="8pt">Pump Data - Last 24
						Hrs</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black"
						padding="0.05cm" number-columns-spanned="4">
						<fo:block font-weight="bold" font-size="8pt">Slow Pump Data</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body width="3in" font-size="8pt">
				<xsl:call-template name="rigpumps_header"/>
				<xsl:apply-templates select="RigPump/RigPumpParam"/>
			</fo:table-body>
		</fo:table>											
	</xsl:template>
	<!-- RigPump / pumps section END -->
	
</xsl:stylesheet>