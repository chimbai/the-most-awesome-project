<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Rig Pumps header START -->
	<xsl:template name="rigpumps_header">	
		<fo:table-row>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">No.</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Type</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Liner</fo:block>
				<fo:block text-align="center">(<xsl:value-of
					select="RigPump/linerId/@uomSymbol"/>)</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">MW</fo:block>
				<fo:block text-align="center">(<xsl:value-of
					select="RigPump/RigPumpParam/mw/@uomSymbol"/>)</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Eff</fo:block>
				<fo:block text-align="center"><xsl:value-of
					select="RigPump/RigPumpParam/efficiency/@uomSymbol"/></fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">SPM</fo:block>						
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">SPP</fo:block>
				<fo:block text-align="center">(<xsl:value-of
					select="RigPump/RigPumpParam/pressureOut/@uomSymbol"/>)</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">																
				<fo:block text-align="center">Flow</fo:block>
				<fo:block text-align="center">(<xsl:value-of
					select="RigPump/RigPumpParam/volumeOut/@uomSymbol"/>)</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.05cm">																
				<fo:block text-align="center">Depth</fo:block>
				<fo:block text-align="center">(<xsl:value-of
					select="RigPump/RigPumpParam/sampleDepth/@uomSymbol"/>)</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Check</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">SPM</fo:block>
				<!-- <fo:block text-align="center"><xsl:value-of
					select="RigPumpParam/RigSlowPumpParam/rate/@uomSymbol"/></fo:block> -->
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Pressure</fo:block>
				<fo:block text-align="center">(<xsl:value-of
					select="RigPump/RigPumpParam/RigSlowPumpParam/pressure/@uomSymbol"/>)</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">						
				<fo:block text-align="center">Flow</fo:block>
				<fo:block text-align="center">(<xsl:value-of
					select="RigPump/RigPumpParam/RigSlowPumpParam/flow/@uomSymbol"/>)</fo:block>
			</fo:table-cell>					
		</fo:table-row>
	</xsl:template>
	<!-- Rig Pumps header END -->
	
</xsl:stylesheet>