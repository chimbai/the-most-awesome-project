<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:variable name="costUomSymbol" select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol" />
	<xsl:variable name="dailyUid" select="/root/modules/ReportDaily/ReportDaily/dailyUid"/>
	<xsl:variable name="timeBreakdown" select="concat('TB_',$dailyUid)"/>
	
		<!--Pie chart -->
	<xsl:template match="modules/EstimatedCostReviewReportDaily" mode="piechart">
		<fo:table width="100%" table-layout="fixed" font-size="7pt" border="0.75px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row space-after="6pt">
						<fo:table-cell padding-bottom="4px" padding-top="4px" padding-left="0.1px" padding-right="2px">
						<fo:block text-align="center">
							<fo:inline font-weight="bold">
							 Time Analysis by Class Code (%)
							</fo:inline>
						</fo:block>
					</fo:table-cell> 
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>
						<fo:block text-align="center" border="0.75px solid black"> 
							<fo:external-graphic content-width="scale-to-fit" width="130pt" content-height="100%" scaling="uniform" src="url(d2://../graph/{$timeBreakdown}.jpg)" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:table width="100%" table-layout="fixed" font-size="4pt" padding="1px">
								<fo:table-column column-number="1" column-width="proportional-column-width(34)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(30)"/>
									<fo:table-header>
										<fo:table-row>
											<fo:table-cell font-weight="bold" text-align="center" background-color="#FFFACD" padding-left="0.1cm" padding-top="0.05cm" border="0.5px solid black" number-columns-spanned="3">
												<fo:block>
													  Time Breakdown with Estimated Cost Review
												</fo:block>
											</fo:table-cell> 
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm" border-left="0.5px solid black"  border-bottom="0.5px solid black"   border-right="0.5px solid black"> 
												<fo:block>
													  Class Code
												</fo:block>
											</fo:table-cell> 
											<fo:table-cell  background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm" border-bottom="0.5px solid black"    border-right="0.5px solid black"> 
												<fo:block>
													  Duration (days)
												</fo:block>
											</fo:table-cell> 
											<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"  border-bottom="0.5px solid black"   border-right="0.5px solid black"> 
												<fo:block>
													 Estimated Cost (<xsl:value-of select="$costUomSymbol"/>)
												</fo:block>
											</fo:table-cell> 
										</fo:table-row>
									</fo:table-header>
								<fo:table-body space-after="6pt">
									<xsl:apply-templates select="DailyCostData"/>	
								</fo:table-body>								
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="DailyCostData">
		<xsl:apply-templates select="ClassCode"/>
	</xsl:template>
	
	<xsl:template match="ClassCode">
			<fo:table-row>
				<fo:table-cell padding-left="0.1cm" padding-top="0.05cm" border-bottom="0.5px solid black" border-left="0.5px solid black"   border-right="0.5px solid black"> 
					<fo:block>
					<xsl:value-of select="name"/>
					</fo:block>
				</fo:table-cell> 
				<fo:table-cell padding-left="0.1cm" padding-top="0.05cm" border-bottom="0.5px solid black"  border-right="0.5px solid black">
					<fo:block linefeed-treatment="preserve">
					<xsl:value-of select="d2Utils:formatNumber(string(duration),'###,###,##0.00')"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding-left="0.1cm" padding-top="0.05cm" border-right="0.5px solid black"  border-bottom="0.5px solid black"  >
					<fo:block linefeed-treatment="preserve">
								<xsl:value-of select="d2Utils:formatNumber(string(estCost),'###,###,##0.00')"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
	</xsl:template>
</xsl:stylesheet>