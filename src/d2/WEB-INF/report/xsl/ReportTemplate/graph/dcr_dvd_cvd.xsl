<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:variable name="operationUid" select="/root/modules/Operation/Operation/operationUid"/>
	<xsl:variable name="dailyUid" select="/root/modules/ReportDaily/ReportDaily/dailyUid"/>
	<xsl:variable name="dvd" select="concat($dailyUid,'_dvdgraph_nocost_toDate')"/>
	<xsl:variable name="cvd" select="concat($operationUid,'_cvd_toDate')"/>
	
	
	<!--DVD and CVD Graph -->
	<xsl:template match="modules/Activity" mode="depthgraph">
	<fo:table inline-progression-dimension="100%" table-layout="fixed" font-size="7pt" margin-top="4pt">
		<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
		<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell padding-top="0.05cm" padding-left="1cm" padding-right="1cm" font-size="7pt" border="0.5px solid black">
						<fo:block text-align="center">
							<fo:inline font-weight="bold">
							  DAYS VS DEPTH
							</fo:inline>
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-top="0.05cm" padding-left="1cm" padding-right="1cm" font-size="7pt" border="0.5px solid black" >
						<fo:block text-align="center">
							<fo:inline font-weight="bold">
							  COST VS DEPTH 
							</fo:inline>
						</fo:block>
					</fo:table-cell> 
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-top="0.05cm" padding-left="1cm" padding-right="1cm" font-size="7pt" border="0.5px solid black">
						<fo:block text-align="right">
							<fo:inline font-weight="bold">
							  <fo:external-graphic content-width="scale-to-fit" width="50pt" content-height="100%" scaling="uniform" src="url(d2://images/client-logo.jpg)"/>
							</fo:inline>
						</fo:block>
						<fo:block text-align="center">
							<fo:inline font-weight="bold">
							  <fo:external-graphic content-width="scale-to-fit" width="200pt" content-height="100%" scaling="uniform" src="url(d2://../graph/{$dvd}.jpg)" />
							</fo:inline>
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-top="0.05cm" padding-left="1cm" padding-right="1cm" font-size="7pt" border="0.5px solid black" >
						<fo:block text-align="right">
							<fo:inline font-weight="bold">
							  <fo:external-graphic content-width="scale-to-fit" width="50pt" content-height="100%" scaling="uniform" src="url(d2://images/client-logo.jpg)"/>
							</fo:inline>
						</fo:block>
						<fo:block text-align="center">
							<fo:inline font-weight="bold">
							  <fo:external-graphic content-width="scale-to-fit" width="200pt" content-height="100%" scaling="uniform" src="url(d2://../graph/cvd/{$cvd}.jpg)" />
							</fo:inline>
						</fo:block>
					</fo:table-cell> 
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>