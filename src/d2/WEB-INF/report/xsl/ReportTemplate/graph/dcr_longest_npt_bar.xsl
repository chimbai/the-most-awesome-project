<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:variable name="dailyUid" select="/root/modules/ReportDaily/ReportDaily/dailyUid"/>
	<xsl:variable name="longestNPT" select="concat('NPT_',$dailyUid)"/>
	
		<!--Bar chart --> 
	<xsl:template match="modules/EstimatedCostReviewReportDaily/DailyCostData" mode="bargraph">
		<fo:table width="100%" table-layout="fixed" font-size="7pt"  border="0.75px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row space-after="6pt">
						<fo:table-cell padding-bottom="4px" padding-top="4px" padding-left="2px" padding-right="2px">
						<fo:block text-align="center">
							<fo:inline font-weight="bold">
							Longest NPT Events (Hours)
							</fo:inline>
						</fo:block>
					</fo:table-cell> 
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block border="0.75px solid black">
							<fo:external-graphic content-width="scale-to-fit" width="220pt" content-height="100%" scaling="uniform" src="url(d2://../graph/{$longestNPT}.jpg)" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>