<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:import href="./rigstock_header.xsl" />
	<xsl:import href="./rigstock_records.xsl" />
	
	<!-- RigStock / Bulk section BEGIN -->
	<xsl:template match="modules/RigStock">		 
		<fo:table inline-progression-dimension="100%" table-layout="fixed"
			font-size="8pt" wrap-option="wrap" border="0.5px solid black" margin-top="2pt" border-top="0.5px solid black"
			border-left="0.5px solid black" border-right="0.5px solid black" border-bottom="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="7" padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
						background-color="rgb(224,224,224)" border-left="0.1px solid black"
						border-right="0.1px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Rig Stock
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:call-template name="rigstock_header"/>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="RigStock" mode="rigstock"/>
			</fo:table-body>
		</fo:table>											
	</xsl:template>
	<!-- RigStock / Bulk section END -->

</xsl:stylesheet>