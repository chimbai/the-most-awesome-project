<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- rig Stock header START -->
	<xsl:template name="rigstock_header">	
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
				<fo:block text-align="center">Name</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
				<fo:block text-align="center">Unit</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
				<fo:block text-align="center">Previous Balance</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
				<fo:block text-align="center">In</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
				<fo:block text-align="center">Used</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
				<fo:block text-align="center">Adjust</fo:block>
			</fo:table-cell>					
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
				<fo:block text-align="center">Balance</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Rig Stock header END -->

</xsl:stylesheet>