<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TEMPLATE: modules/RigStock -->
	<xsl:template match="modules/RigStock">
	
		<fo:block keep-together="always">
	
			<!-- TABLE -->	 
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black" space-after="6pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(10)"/>
				
				<!-- HEADER -->
				<fo:table-header>
				
					<!-- HEADER 1 -->
					<fo:table-row>
						<fo:table-cell number-columns-spanned="8" padding="2px" background-color="rgb(223,223,223)" border="thin solid black" display-align="center">
							<fo:block font-size="10pt" font-weight="bold">
								Bulk Stocks
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<!-- HEADER 2 -->
					<fo:table-row>
						<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Name
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Unit
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Start Amount
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Previous Balance
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								In
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Used
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Adjust
							</fo:block>
						</fo:table-cell>					
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Balance
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				
				<!--  BODY -->
				<fo:table-body>
					<xsl:apply-templates select="RigStock" mode="rigstock"/>
					<fo:table-row>
						<fo:table-cell padding="2px" border-left="thin solid black" border-right="thin solid black" border-top="thin solid black" number-columns-spanned="8">
							<fo:block>* stocks that were replaced.</fo:block>
						</fo:table-cell>
					</fo:table-row>	
				</fo:table-body>
				
			</fo:table>
		
		</fo:block>
													
	</xsl:template>
	
	<!-- TEMPLATE: RigStock -->
	<xsl:template match="RigStock" mode="rigstock">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
				
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:choose>													
								<xsl:when test="string(stockCode/@lookupLabel)=''">
									<xsl:value-of select="stockCode"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="stockCode/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="storedUnit"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="amtInitial"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="dynaAttr/previousBalance"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="amtStart"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="amtUsed"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="amtDiscrepancy"/>
						</fo:block>
					</fo:table-cell>
								
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="dynaAttr/currentBalance"/>
						</fo:block>
					</fo:table-cell>
					
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
				
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:choose>													
								<xsl:when test="string(stockCode/@lookupLabel)=''">
									<xsl:value-of select="stockCode"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="stockCode/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="storedUnit"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="amtInitial"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="dynaAttr/previousBalance"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="amtStart"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="amtUsed"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="amtDiscrepancy"/>
						</fo:block>
					</fo:table-cell>
								
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="dynaAttr/currentBalance"/>
						</fo:block>
					</fo:table-cell>
					
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>

</xsl:stylesheet>