<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- NEW XSL FOR D2_CHOC_MY BULK STOCKS SECTION CONTENT 20080912 -->
	
	<xsl:template match="RigStock" mode="rigstock">
		<fo:table-row>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="left">
					<xsl:choose>													
						<xsl:when test = "string(stockCode/@lookupLabel)=''">
							<xsl:value-of select="stockCode"/>										
						</xsl:when>													
						<xsl:otherwise>
							<xsl:value-of select="stockCode/@lookupLabel"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="left">
					<xsl:value-of select="storedUnit"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="right">
					<xsl:value-of select="d2Utils:formatNumber(amtInitial,'###0.0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="right">
					<xsl:value-of select="d2Utils:formatNumber(dynaAttr/previousBalance,'#,##0.0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="right">
					<xsl:value-of select="amtStart"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="right">
					<xsl:value-of select="amtUsed"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="right">
					<xsl:value-of select="amtDiscrepancy"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="2px">
				<fo:block text-align="right">
					<xsl:value-of select="d2Utils:formatNumber(dynaAttr/currentBalance,'#,##0.0')"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
</xsl:stylesheet>