<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- D2_OMV_GLOBAL BULK STOCKS SECTION -->
	
	<xsl:import href="./rigstock_header_ids_demo.xsl" />
	<xsl:import href="./rigstock_records_ids_demo.xsl" />
		
	<xsl:template match="modules/RigStock">		 
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" border="0.5px solid black"
			margin-top="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="8" column-width="proportional-column-width(10)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="8" padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Bulk Stocks
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:call-template name="rigstock_header"/>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="RigStock" mode="rigstock"/>
			</fo:table-body>
		</fo:table>											
	</xsl:template>

</xsl:stylesheet>