<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/FiltrationJob/FiltrationJob" mode="main">	
			<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-before="2pt" border="0.5px solid black">
				<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
					
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell padding="2px" background-color="#b8b8ba" display-align="center">
							<fo:block text-align="left" font-size="8pt" font-weight="bold">
								Filtration Job # <xsl:value-of select="jobNumber"/>
							</fo:block>
						</fo:table-cell>					 
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
				</fo:table-body>
			</fo:table>
	</xsl:template>
	
	<xsl:template match="modules/FiltrationJob/FiltrationJob" mode="summary">
		<xsl:if test="count(/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties) &gt; 0">
			<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-before="2pt" border="0.5px solid black" keep-together="always">
			<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="8" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="10" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="11" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="12" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="13" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="14" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="15" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="16" column-width="proportional-column-width(10)"/>
			
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell number-columns-spanned="16"  padding="2px" background-color="#b8b8ba" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="left" font-size="8pt" font-weight="bold">
								Filtration Summary
							</fo:block>
						</fo:table-cell>					 
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">Cycle #</fo:block>
						</fo:table-cell> 
						<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm">
							<fo:block text-align="center" font-weight="bold">
								<xsl:value-of select="FiltrationFluidProperties[position() =1]/cycleReferenceNo"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm">
							<fo:block text-align="center" font-weight="bold">
								<xsl:value-of select="FiltrationFluidProperties[position() =2]/cycleReferenceNo"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm">
							<fo:block text-align="center" font-weight="bold">
								<xsl:value-of select="FiltrationFluidProperties[position() =3]/cycleReferenceNo"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm">
							<fo:block text-align="center" font-weight="bold">
								<xsl:value-of select="FiltrationFluidProperties[position() =4]/cycleReferenceNo"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm">
							<fo:block text-align="center" font-weight="bold">
								<xsl:value-of select="FiltrationFluidProperties[position() =5]/cycleReferenceNo"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm">
							<fo:block text-align="center" font-weight="bold">
								<xsl:value-of select="FiltrationFluidProperties[position() =6]/cycleReferenceNo"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm">
							<fo:block text-align="center" font-weight="bold">
								<xsl:value-of select="FiltrationFluidProperties[position() =7]/cycleReferenceNo"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm">
							<fo:block text-align="center" font-weight="bold">
								Total
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">Start Time</fo:block>
						</fo:table-cell> 
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =1]/startDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =2]/startDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =3]/startDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =4]/startDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =5]/startDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =6]/startDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =7]/startDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm">
							<fo:block text-align="center">
								<fo:inline color="white">.</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">Stop Time</fo:block>
						</fo:table-cell> 
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =1]/stopDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =2]/stopDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =3]/stopDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =4]/stopDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =5]/stopDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =6]/stopDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="d2Utils:formatDateFromEpochMS(FiltrationFluidProperties[position() =7]/stopDatetime/@epochMS,'HH:mm')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm">
							<fo:block text-align="center">
								<fo:inline color="white">.</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">Pressure (<xsl:value-of select="FiltrationFluidProperties/pressure/@uomSymbol"/>)</fo:block>
						</fo:table-cell> 
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =1]/pressure != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =1]/pressure"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =2]/pressure != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =2]/pressure"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =3]/pressure != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =3]/pressure"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =4]/pressure != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =4]/pressure"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =5]/pressure != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =5]/pressure"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =6]/pressure != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =6]/pressure"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =7]/pressure != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =7]/pressure"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm">
							<fo:block text-align="center">
								<fo:inline color="white">.</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">Micron Size</fo:block>
						</fo:table-cell> 
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =1]/micronSize"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =2]/micronSize"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =3]/micronSize"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =4]/micronSize"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =5]/micronSize"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =6]/micronSize"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =7]/micronSize"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm">
							<fo:block text-align="center">
								<fo:inline color="white">.</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">
								Rate (<xsl:value-of select="FiltrationFluidProperties/flowRate/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell> 
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =1]/flowRate != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =1]/flowRate"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =2]/flowRate != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =2]/flowRate"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =3]/flowRate != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =3]/flowRate"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =4]/flowRate != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =4]/flowRate"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =5]/flowRate != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =5]/flowRate"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =6]/flowRate != ''">							
									<xsl:value-of select="FiltrationFluidProperties[position() =6]/flowRate"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =7]/flowRate != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =7]/flowRate"/>
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm">
							<fo:block text-align="center">
								<fo:inline color="white">.</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">BS:W</fo:block>
						</fo:table-cell> 
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =1]/bsw"/> 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =2]/bsw"/> 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =3]/bsw"/> 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =4]/bsw"/> 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =5]/bsw"/> 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =6]/bsw"/> 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:value-of select="FiltrationFluidProperties[position() =7]/bsw"/> 
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm">
							<fo:block text-align="center">
								<fo:inline color="white">.</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">Downtime (<xsl:value-of select="FiltrationFluidProperties/downtimeDuration/@uomSymbol"/>)</fo:block>
						</fo:table-cell> 
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =1]/downtimeDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =1]/downtimeDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =2]/downtimeDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =2]/downtimeDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =3]/downtimeDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =3]/downtimeDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =4]/downtimeDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =4]/downtimeDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =5]/downtimeDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =5]/downtimeDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =6]/downtimeDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =6]/downtimeDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =7]/downtimeDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =7]/downtimeDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm">
							<fo:block text-align="center">
								<fo:inline color="white">.</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">Run Time (<xsl:value-of select="FiltrationFluidProperties/runDuration/@uomSymbol"/>)</fo:block>
						</fo:table-cell> 
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =1]/runDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =1]/runDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =2]/runDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =2]/runDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =3]/runDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =3]/runDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =4]/runDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =4]/runDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =5]/runDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =5]/runDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =6]/runDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =6]/runDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =7]/runDuration != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =7]/runDuration"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm" border-bottom="0.5px solid black">
							<fo:block text-align="center">
								<fo:inline color="white">.</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">Volume/ Cycle (<xsl:value-of select="FiltrationFluidProperties/filteredVolume/@uomSymbol"/>)</fo:block>
						</fo:table-cell> 
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =1]/filteredVolume != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =1]/filteredVolume"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =2]/filteredVolume != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =2]/filteredVolume"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =3]/filteredVolume != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =3]/filteredVolume"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =4]/filteredVolume != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =4]/filteredVolume"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =5]/filteredVolume != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =5]/filteredVolume"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =6]/filteredVolume != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =6]/filteredVolume"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =7]/filteredVolume != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =7]/filteredVolume"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm">
							<fo:block text-align="center">
								<xsl:if test ="dynaAttr/totalFilteredVolume != 'NaN'">
									<xsl:value-of select="dynaAttr/totalFilteredVolume"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">DE Sacks/ Cycle (<xsl:value-of select="FiltrationFluidProperties/diatomaceousEarthMass/@uomSymbol"/>)</fo:block>
						</fo:table-cell> 
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =1]/diatomaceousEarthMass != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =1]/diatomaceousEarthMass"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =2]/diatomaceousEarthMass != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =2]/diatomaceousEarthMass"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =3]/diatomaceousEarthMass != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =3]/diatomaceousEarthMass"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =4]/diatomaceousEarthMass != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =4]/diatomaceousEarthMass"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =5]/diatomaceousEarthMass != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =5]/diatomaceousEarthMass"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =6]/diatomaceousEarthMass != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =6]/diatomaceousEarthMass"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =7]/diatomaceousEarthMass != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =7]/diatomaceousEarthMass"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm">
							<fo:block text-align="center">
								<xsl:if test ="dynaAttr/totalDiatomaceousEarthWeight != 'NaN'">
									<xsl:value-of select="dynaAttr/totalDiatomaceousEarthWeight"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>						
						<fo:table-cell padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">Cartridge/ Cycle</fo:block>
						</fo:table-cell> 
						<fo:table-cell border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =1]/noOfCartridgeUsed != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =1]/noOfCartridgeUsed"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =2]/noOfCartridgeUsed != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =2]/noOfCartridgeUsed"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =3]/noOfCartridgeUsed != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =3]/noOfCartridgeUsed"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =4]/noOfCartridgeUsed != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =4]/noOfCartridgeUsed"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =5]/noOfCartridgeUsed != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =5]/noOfCartridgeUsed"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =6]/noOfCartridgeUsed != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =6]/noOfCartridgeUsed"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="0.07cm" number-columns-spanned="2">
							<fo:block text-align="center">
								<xsl:if test= "FiltrationFluidProperties[position() =7]/noOfCartridgeUsed != ''">
									<xsl:value-of select="FiltrationFluidProperties[position() =7]/noOfCartridgeUsed"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm">
							<fo:block text-align="center">
								<xsl:if test ="dynaAttr/totalCartridgeUsed != 'NaN'">
									<xsl:value-of select="dynaAttr/totalCartridgeUsed"/> 
								</xsl:if>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</xsl:if>
		<xsl:if test="count(/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties/FiltrationFluidSampleDetail) &gt; 0">
			<xsl:apply-templates select="/root/modules/CommonLookupFiltrationSampleType"/>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="CommonLookupFiltrationSampleType"> 
	<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-before="2pt" border-left="0.5px solid black" keep-together="always">
			<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="8" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="10" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="11" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="12" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="13" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="14" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="15" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="16" column-width="proportional-column-width(10)"/>
			
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="15"  padding="2px" background-color="#b8b8ba" display-align="center" border-top="0.5px solid black" border-right="0.5px solid black">
						<fo:block text-align="left" font-size="8pt" font-weight="bold">
							Fluid Analysis (NTU/TSS)
						</fo:block>
					</fo:table-cell>					 
				</fo:table-row>
				<fo:table-row>						
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="left">Filtration Sample</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">NTU</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">TSS</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">NTU</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">TSS</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">NTU</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">TSS</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">NTU</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">TSS</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">NTU</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">TSS</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">NTU</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">TSS</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">NTU</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
							<fo:block text-align="center">TSS</fo:block>
						</fo:table-cell>
						<fo:table-cell  padding="0.07cm" >
							<fo:block text-align="center">
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="CommonLookup"/>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="CommonLookup"> 
		<xsl:variable name="shortCode" select="shortCode"/>
		<xsl:if test="count(/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]) &gt; 0">
			<fo:table-row>
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="left">
						<xsl:value-of select="lookupLabel"></xsl:value-of>
					</fo:block>
				</fo:table-cell>		
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=1]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfTransferUnit"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=1]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfSuspendedSolid"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=2]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfTransferUnit"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=2]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfSuspendedSolid"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=3]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfTransferUnit"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=3]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfSuspendedSolid"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=4]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfTransferUnit"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=4]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfSuspendedSolid"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=5]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfTransferUnit"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=5]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfSuspendedSolid"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=6]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfTransferUnit"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=6]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfSuspendedSolid"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=7]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfTransferUnit"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
				
				<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="/root/modules/FiltrationJob/FiltrationJob/FiltrationFluidProperties[position()=7]/FiltrationFluidSampleDetail[filtrationSampleType=$shortCode]/noOfSuspendedSolid"></xsl:value-of>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
	</xsl:template>
 	
	
	
	
	<xsl:template match="modules/FiltrationJob/FiltrationJob" mode="fluidInfo">
		<xsl:if test="count(/root/modules/FiltrationJob/FiltrationJob[jobNumber != '']) &gt; 0">
			<xsl:if test="count(/root/modules/FiltrationJob/FiltrationJob/FiltrationJobFluid) &gt; 0">
				<fo:table width="100%" table-layout="fixed" font-size="8pt" space-before="2pt" border="0.5px solid black">
					<fo:table-column column-number="1" column-width="proportional-column-width(35)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(35)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(40)"/>
					
					<fo:table-header>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="3" padding="2px" background-color="#b8b8ba" display-align="center">
								<fo:block text-align="left" font-size="8pt" font-weight="bold">
									Fluid Information
								</fo:block>
							</fo:table-cell>					 
						</fo:table-row>
						
						<fo:table-row>						
							<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
								<fo:block text-align="left">Fluid Type</fo:block>
							</fo:table-cell>
							<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
								<fo:block text-align="left">Density (ppg)</fo:block>
							</fo:table-cell>
							<fo:table-cell border-top="0.5px solid black" padding="0.07cm" >
								<fo:block text-align="left">Location</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					
					<fo:table-body>
						<xsl:for-each select="FiltrationJobFluid">
							<fo:table-row>						
								<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
									<fo:block text-align="left">
										<xsl:value-of select="fluidType"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
									<fo:block text-align="left">
										<xsl:value-of select="fluidDensity"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell border-top="0.5px solid black" padding="0.07cm" >
									<fo:block text-align="left">
										<xsl:value-of select="fluidLocation"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:for-each>
					</fo:table-body>
				</fo:table>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="modules/MaterialConsumption" mode="material">
		<xsl:if test="count(/root/modules/FiltrationJob/FiltrationJob[jobNumber != '']) &gt; 0">
			<xsl:if test="count(/root/modules/MaterialConsumption/RigStock) &gt; 0">
			
				<fo:table width="100%" table-layout="fixed" font-size="8pt" space-before="2pt" border="0.5px solid black" >
					<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(8)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(8)"/>
					<fo:table-column column-number="4" column-width="proportional-column-width(8)"/>
					<fo:table-column column-number="5" column-width="proportional-column-width(8)"/>
					<fo:table-column column-number="6" column-width="proportional-column-width(8)"/>
					
					<fo:table-header>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="6"  padding="2px" background-color="#b8b8ba" display-align="center">
								<fo:block text-align="left" font-size="8pt" font-weight="bold">
									Material Consumption
								</fo:block>
							</fo:table-cell>					 
						</fo:table-row>
						
						<fo:table-row>						
							<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
								<fo:block text-align="left">Material</fo:block>
							</fo:table-cell>
							<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
								<fo:block text-align="left">Opening Stock</fo:block>
							</fo:table-cell>
							<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
								<fo:block text-align="left">Stock Received</fo:block>
							</fo:table-cell>
							<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
								<fo:block text-align="left">Daily Used</fo:block>
							</fo:table-cell>
							<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
								<fo:block text-align="left">Total Used</fo:block>
							</fo:table-cell>
							<fo:table-cell border-top="0.5px solid black" padding="0.07cm">
								<fo:block text-align="left">Final Stock</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<xsl:for-each select="RigStock">
							<fo:table-row>						
								<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
									<fo:block text-align="left">
										<xsl:value-of select="dynaAttr/stockName"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
									<fo:block text-align="center">
										<xsl:value-of select="dynaAttr/previousBalance"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
									<fo:block text-align="center">
										<xsl:value-of select="amtStart"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
									<fo:block text-align="center">
										<xsl:value-of select="amtUsed"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
									<fo:block text-align="center">
										<xsl:value-of select="dynaAttr/cumUsed"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell border-top="0.5px solid black" padding="0.07cm" >
									<fo:block text-align="center">
										<xsl:value-of select="dynaAttr/currentBalance"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</xsl:for-each>
					</fo:table-body>
				</fo:table> 
			</xsl:if>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="modules/FiltrationJob/FiltrationJob" mode="personnel">
		<xsl:if test = "(comment!='') or (personnelName != '') or (cumVolumeFiltered != '') ">
			<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-before="2pt" border="0.5px solid black">
				<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(60)"/>
					
				<fo:table-header>
					<fo:table-row>						
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black" background-color="#b8b8ba">
							<fo:block text-align="left">Comments</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" background-color="#b8b8ba" >
							<fo:block text-align="left">Filtration Personnel</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
					<fo:table-row>						
						<fo:table-cell padding="0.07cm" border-right="0.5px solid black" >
							<xsl:choose>
								<xsl:when test = "comment != '' ">
									<fo:block text-align="left">
										<xsl:value-of select="comment"/>
									</fo:block>
								</xsl:when>
								<xsl:otherwise>
									<fo:block text-align="left">
										<fo:inline color="white">.</fo:inline>
									</fo:block>
								</xsl:otherwise>
							</xsl:choose>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm" >
							<xsl:choose>
								<xsl:when test = "personnelName != '' ">
									<fo:block text-align="left">
										<xsl:value-of select="personnelName"/>
									</fo:block>
								</xsl:when>
								<xsl:otherwise>
									<fo:block text-align="left">
										<fo:inline color="white">.</fo:inline>
									</fo:block>
								</xsl:otherwise>
							</xsl:choose>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>						
						<fo:table-cell padding="0.07cm" border-right="0.5px solid black" >
							<fo:block text-align="left">
								<fo:inline color="white">.</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-top="0.5px solid black" border-bottom="0.5px solid black" padding="0.07cm" background-color="#b8b8ba">
							<fo:block text-align="left">
								Cummulative Volume
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>						
						<fo:table-cell padding="0.07cm" border-right="0.5px solid black">
							<fo:block text-align="left">
								<fo:inline color="white">.</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm">
							<fo:block text-align="left">
								<xsl:choose>
									<xsl:when test = "cumVolumeFiltered != '' ">
										<fo:block text-align="left">
											<xsl:value-of select="cumVolumeFiltered"/>&#160;
											<xsl:value-of select="cumVolumeFiltered/@uomSymbol"/>
										</fo:block>
									</xsl:when>
									<xsl:otherwise>
										<fo:block text-align="left">
											<fo:inline color="white">.</fo:inline>
										</fo:block>
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="/root/modules" mode="mix">
		<fo:table width="89.5%" table-layout="fixed" font-size="8pt" padding-top="2px">
			<fo:table-column column-number="1" column-width="proportional-column-width(35)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(65)"/>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<xsl:apply-templates select="/root/modules/FiltrationJob" mode="fluidInfo"/> 
					</fo:table-cell>
					<fo:table-cell padding-left="22px"> 
						<xsl:apply-templates select="/root/modules/MaterialConsumption" mode="material"/>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
</xsl:stylesheet>