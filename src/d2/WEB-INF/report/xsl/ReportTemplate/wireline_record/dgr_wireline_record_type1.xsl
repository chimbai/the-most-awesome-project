<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- D2_TALISMAN_NO DGR WIRELINE SECTION -->	
		
	<xsl:template match="modules/WirelineRun/WirelineSuite">
		<fo:table-row keep-together="always">
			<fo:table-cell>				
			<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" margin-top="2pt">
					<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>							
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.3px">
								<fo:block text-align="left" font-weight="bold" font-size="9pt" padding="2px" padding-left="-0.3px" padding-right="-0.3px" background-color="rgb(223,223,223)">
									Wireline Suite
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row font-size="8pt">
							<fo:table-cell border-right="0.5px solid black">
								<fo:block>
			                       	<fo:table inline-progression-dimension="100%" table-layout="fixed">
			                           	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Suite #: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="suiteNumber"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Hole Depth (MD): </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="depthMdMsl/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Shoe Depth (MD): </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="shoeDepthMdMsl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="shoeDepthMdMsl/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
																			
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block>
			                       	<fo:table inline-progression-dimension="100%" table-layout="fixed">
			                           	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Witness: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="wirelineWitness"/>													
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Engineer #1: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">
														<xsl:value-of select="wirelineEngineer"/>													
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Engineer #2: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="wirelineEngineer02"/>													
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Max Deviation Depth: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="maxDeviationDepth"/><fo:inline color="white">.</fo:inline>													
														<xsl:value-of select="maxDeviationDepth/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>											
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>					
						</fo:table-row>
						<fo:table-row font-size="8pt">
							<fo:table-cell border-top="0.5px solid black" number-columns-spanned="2">
								<fo:block>
			                       	<fo:table inline-progression-dimension="100%" table-layout="fixed">
			                           	<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(80)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Objectives: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="left" linefeed-treatment="preserve">																			
														<xsl:value-of select="comments"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>									
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
																
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>		
		<xsl:if test="count(WirelineRun) &gt; 0">
			<xsl:apply-templates select="WirelineRun"/>
		</xsl:if>		
	</xsl:template>
	
	<xsl:template match="WirelineRun">
		<fo:table-row keep-together="always">
			<fo:table-cell>	
				<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="2pt" margin-top="2pt">
					<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(33)"/>							
					<fo:table-column column-number="3" column-width="proportional-column-width(34)"/>
					<fo:table-header>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="3" border-bottom="0.5px solid black" padding="0.3px">
								<fo:block text-align="left" font-weight="bold" font-size="9pt" padding="2px" padding-left="-0.3px" padding-right="-0.3px" background-color="rgb(223,223,223)">
									Wireline Run 
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell border-right="0.5px solid black" border-bottom="0.5px solid black">
								<fo:block>
			                       	<fo:table inline-progression-dimension="100%" table-layout="fixed">
			                           	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Run #: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">
														<xsl:value-of select="runNumber"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Witness: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="wirelineEngineer"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Conveyance: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="conveyanceMethod"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>																																								
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell border-right="0.5px solid black" border-bottom="0.5px solid black">
								<fo:block>
			                       	<fo:table inline-progression-dimension="100%" table-layout="fixed">
			                           	<fo:table-column column-number="1" column-width="proportional-column-width(70)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Mud Source: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">
														<xsl:value-of select="mudSource"/>													
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Hole Size: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="holeSize"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="holeSize/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Service Company: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="svcCo/@lookupLabel"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell border-bottom="0.5px solid black">
								<fo:block>
			                       	<fo:table inline-progression-dimension="100%" table-layout="fixed">
			                           	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
										<fo:table-body>											
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Log Top / Bottom Depth: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="logTop"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="logTop/@uomSymbol"/> / <xsl:value-of select="logBottom"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="logBottom/@uomSymbol"/>														  													
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Actual Hydrostatic Over Balance: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">
														<xsl:value-of select="actualHydrostatic"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="actualHydrostatic/@uomSymbol"/>
														
													</fo:block>			
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Expected Hydrostatic Over Balance: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">
														<xsl:value-of select="expectedHydrostatic"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="expectedHydrostatic/@uomSymbol"/>
													</fo:block>			
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>					
						</fo:table-row>																	
						<fo:table-row>
							<fo:table-cell border-right="0.5px solid black">
								<fo:block>
			                       	<fo:table inline-progression-dimension="100%" table-layout="fixed">
			                           	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>										
										<fo:table-header>
											<fo:table-row>
												<fo:table-cell padding="2px" number-columns-spanned="2" border-bottom="0.5px solid black"
													font-weight="bold" text-align="center">
													<fo:block>
														Time Summary
													</fo:block>
												</fo:table-cell>
											</fo:table-row>								
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black"
													text-align="center">
													<fo:block>
														Description
													</fo:block>
												</fo:table-cell>
												<fo:table-cell padding="2px" border-bottom="0.5px solid black" text-align="center">
													<fo:block>
														Date/Time
													</fo:block>
												</fo:table-cell>												
											</fo:table-row>
										</fo:table-header>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"> Start Of Run: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="center">																			
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(rigupStartdateRigupStarttime/@epochMS,'dd MMM yyyy HH:mm')"/>												
													</fo:block>																
												</fo:table-cell>												
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"> End Of Run: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="center">																			
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(rigdownEnddateRigdownEndtime/@epochMS,'dd MMM yyyy HH:mm')"/>												
													</fo:block>																
												</fo:table-cell>												
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"> Bit Reached TD: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="center">																			
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(dateTdReachedTimeTdReached/@epochMS,'dd MMM yyyy HH:mm')"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"> Tool Left Max. Depth: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="center">																			
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(dateToolLeftbtmTimeToolLeftbtm/@epochMS,'dd MMM yyyy HH:mm')"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>											
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"> Stop Circ.: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="center">																			
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(dateCircStoppedTimeCircStopped/@epochMS,'dd MMM yyyy HH:mm')"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell border-right="0.5px solid black">
								<fo:block>
			                       	<fo:table inline-progression-dimension="100%" table-layout="fixed">
			                           	<fo:table-column column-number="1" column-width="proportional-column-width(34)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(33)"/>
										<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>
										<fo:table-header>
											<fo:table-row>
												<fo:table-cell padding="2px" number-columns-spanned="3" border-bottom="0.5px solid black"
													font-weight="bold" text-align="center">
													<fo:block>
														Mud Resistivity Summary
													</fo:block>
												</fo:table-cell>
											</fo:table-row>								
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black"
													text-align="center">
													<fo:block>
														Description
													</fo:block>
												</fo:table-cell>
												<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black"
													text-align="center">
													<fo:block>
														(ohm m)
													</fo:block>
												</fo:table-cell>
												<fo:table-cell padding="2px" border-bottom="0.5px solid black" text-align="center">
													<fo:block>
														Temperature
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											</fo:table-header>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"> RM: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px" border-right="0.5px solid black">	
													<fo:block text-align="right">																			
														<xsl:value-of select="rmValue"/>												
													</fo:block>																
												</fo:table-cell>
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="rmTemp"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="rmTemp/@uomSymbol"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"> RMF: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px" border-right="0.5px solid black">	
													<fo:block text-align="right">																			
														<xsl:value-of select="rmfValue"/>												
													</fo:block>																
												</fo:table-cell>
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="rmfTemp"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="rmfTemp/@uomSymbol"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"> RMC: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px" border-right="0.5px solid black">	
													<fo:block text-align="right">																			
														<xsl:value-of select="rmcValue"/>												
													</fo:block>																
												</fo:table-cell>
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="rmcTemp"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="rmcTemp/@uomSymbol"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"><fo:inline color="white">.</fo:inline></fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px" border-right="0.5px solid black">	
													<fo:block text-align="right"><fo:inline color="white">.</fo:inline></fo:block>																
												</fo:table-cell>
												<fo:table-cell padding="2px">	
													<fo:block text-align="right"><fo:inline color="white">.</fo:inline></fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"><fo:inline color="white">.</fo:inline></fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px" border-right="0.5px solid black">	
													<fo:block text-align="right"><fo:inline color="white">.</fo:inline></fo:block>																
												</fo:table-cell>
												<fo:table-cell padding="2px">	
													<fo:block text-align="right"><fo:inline color="white">.</fo:inline></fo:block>																
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block>
			                       	<fo:table inline-progression-dimension="100%" table-layout="fixed">
			                           	<fo:table-column column-number="1" column-width="proportional-column-width(34)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(33)"/>
										<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>
										<fo:table-header>
											<fo:table-row>
												<fo:table-cell padding="2px" number-columns-spanned="3" border-bottom="0.5px solid black"
													font-weight="bold" text-align="center">
													<fo:block>
														Thermometer Summary
													</fo:block>
												</fo:table-cell>
											</fo:table-row>								
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black"
													text-align="center">
													<fo:block>
														Description
													</fo:block>
												</fo:table-cell>
												<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black"
													text-align="center">
													<fo:block>
														Depth
													</fo:block>
												</fo:table-cell>
												<fo:table-cell padding="2px" border-bottom="0.5px solid black" text-align="center">
													<fo:block>
														Temperature
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-header>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"> Thermometer 1: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px" border-right="0.5px solid black">	
													<fo:block text-align="right">																			
														<xsl:value-of select="thermometerDepth"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="thermometerDepth/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="temperature"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="temperature/@uomSymbol"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"> Thermometer 2: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px" border-right="0.5px solid black">	
													<fo:block text-align="right">																			
														<xsl:value-of select="thermometerDepth2"/><fo:inline color="white">.</fo:inline>												
														<xsl:value-of select="thermometerDepth2/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="temperature2"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="temperature2/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"> Thermometer 3: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px" border-right="0.5px solid black">	
													<fo:block text-align="right">																			
														<xsl:value-of select="thermometerDepth3"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="thermometerDepth3/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
												<fo:table-cell padding="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="temperature3"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="temperature3/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"><fo:inline color="white">.</fo:inline></fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px" border-right="0.5px solid black">	
													<fo:block text-align="right"><fo:inline color="white">.</fo:inline></fo:block>																
												</fo:table-cell>
												<fo:table-cell padding="2px">	
													<fo:block text-align="right"><fo:inline color="white">.</fo:inline></fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px" border-right="0.5px solid black">																
													<fo:block text-align="left"><fo:inline color="white">.</fo:inline></fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px" border-right="0.5px solid black">	
													<fo:block text-align="right"><fo:inline color="white">.</fo:inline></fo:block>																
												</fo:table-cell>
												<fo:table-cell padding="2px">	
													<fo:block text-align="right"><fo:inline color="white">.</fo:inline></fo:block>																
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="3" border-top="0.5px solid black">
								<fo:block>
			                       	<fo:table inline-progression-dimension="100%" table-layout="fixed">
			                           	<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Tool String: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="left" linefeed-treatment="preserve">																			
														<xsl:value-of select="wirelineLogStringDescr"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Temperature Buildup Comment: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="left" linefeed-treatment="preserve">																			
														<xsl:value-of select="temperatureComments"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Log Quality Remarks: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="left" linefeed-treatment="preserve">																			
														<xsl:value-of select="logQualityRemarks"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="2px">																
													<fo:block text-align="left"> Run Summary: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="2px">	
													<fo:block text-align="left" linefeed-treatment="preserve">																			
														<xsl:value-of select="comments"/>												
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>	
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
		<xsl:if test="count(WirelineLog) &gt; 0">
			<fo:table-row>
				<fo:table-cell>	
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" border-right="0.5px solid black" border-bottom="0.5px solid black" margin-top="2pt" border-before-width="0.5px" 
						border-before-style="solid" border-before-width.conditionality="retain" border-after-width="0.5px" border-after-style="solid" border-after-width.conditionality="retain"
						border-left="0.5px solid black">
						<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(7)"/>							
						<fo:table-column column-number="3" column-width="proportional-column-width(7)"/>
						<fo:table-column column-number="4" column-width="proportional-column-width(7)"/>
						<fo:table-column column-number="5" column-width="proportional-column-width(7)"/>							
						<fo:table-column column-number="6" column-width="proportional-column-width(7)"/>
						<fo:table-column column-number="7" column-width="proportional-column-width(15)"/>
						<fo:table-column column-number="8" column-width="proportional-column-width(15)"/>
						<fo:table-column column-number="9" column-width="proportional-column-width(25)"/>
						<fo:table-header>
							<fo:table-row>
								<fo:table-cell number-columns-spanned="9" border-bottom="0.5px solid black" padding="0.3px">
									<fo:block text-align="left" font-weight="bold" font-size="9pt" padding="2px" padding-left="-0.3px" padding-right="-0.3px" background-color="rgb(223,223,223)">
										Wireline Log 
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="2px" border-right="0.5px solid black" display-align="center">																
									<fo:block text-align="center"> Date </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" number-columns-spanned="3" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> Time </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" number-columns-spanned="2" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> End Depth </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" number-columns-spanned="2" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> Codes </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" display-align="center">																
									<fo:block text-align="center"> Description </fo:block>
								</fo:table-cell>							
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> Start </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> End </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> Duration </fo:block>
									<fo:block text-align="center"> (<xsl:value-of select="WirelineLog/duration/@uomSymbol"/>) </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">
									<fo:block text-align="center"> MDBRT </fo:block>																
									<fo:block text-align="center"> (<xsl:value-of select="WirelineLog/mdMsl/@uomSymbol"/>) </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">
									<fo:block text-align="center"> TVDBRT </fo:block>																
									<fo:block text-align="center"> (<xsl:value-of select="WirelineLog/tvdMsl/@uomSymbol"/>) </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> Class </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> Ops. </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> </fo:block>
								</fo:table-cell>						
							</fo:table-row>
						</fo:table-header>
						<fo:table-body>
							<xsl:apply-templates select="WirelineLog"/>
						</fo:table-body>
					</fo:table>
				</fo:table-cell>
			</fo:table-row>			
		</xsl:if>
		
		<xsl:if test="count(WirelineTool) &gt; 0">
			<fo:table-row>
				<fo:table-cell>	
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" border-right="0.5px solid black" border-bottom="0.5px solid black" margin-top="2pt" border-before-width="0.5px" 
						border-before-style="solid" border-before-width.conditionality="retain" border-after-width="0.5px" border-after-style="solid" border-after-width.conditionality="retain"
						border-left="0.5px solid black">
						<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>							
						<fo:table-column column-number="3" column-width="proportional-column-width(30)"/>
						<fo:table-column column-number="4" column-width="proportional-column-width(30)"/>		
						<fo:table-header>
							<fo:table-row>
								<fo:table-cell number-columns-spanned="4" border-bottom="0.5px solid black" padding="0.3px">
									<fo:block text-align="left" font-weight="bold" font-size="9pt" padding="2px" padding-left="-0.3px" padding-right="-0.3px" background-color="rgb(223,223,223)">
										Wireline Tool 
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="2px" border-right="0.5px solid black">																
									<fo:block text-align="center"> # </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black">																
									<fo:block text-align="center"> Tool </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black">																
									<fo:block text-align="center"> Distance to Tool Zero </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">																
									<fo:block text-align="center"> Date/Time of Tool Failure </fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> (<xsl:value-of select="WirelineTool/distanceToToolZero/@uomSymbol"/>) </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-bottom="0.5px solid black">																
									<fo:block text-align="center"> </fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-header>
						<fo:table-body>
							<xsl:apply-templates select="WirelineTool"/>
						</fo:table-body>
					</fo:table>
				</fo:table-cell>
			</fo:table-row>					
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="WirelineLog">
		<fo:table-row>
			<fo:table-cell padding="2px" border-right="0.5px solid black">																
				<fo:block text-align="center">
					<xsl:value-of select="dailyUid/@lookupLabel"/> 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">																
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(startTime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">																
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(endTime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">																
				<fo:block text-align="center">
					<xsl:value-of select="duration"/>
				</fo:block>				
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">
				<fo:block text-align="right">
					<xsl:value-of select="mdMsl"/>		
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">
				<fo:block text-align="right">
					<xsl:value-of select="tvdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">																
				<fo:block text-align="left">
					<xsl:value-of select="actLogClass/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">																
				<fo:block text-align="left">
					<xsl:value-of select="actLogOp/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px">																
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:value-of select="descr"/>
				</fo:block>
			</fo:table-cell>						
		</fo:table-row>	
	</xsl:template>
	
	<xsl:template match="WirelineTool">
		<fo:table-row>
			<fo:table-cell padding="2px" border-right="0.5px solid black">																
				<fo:block text-align="center">
					<xsl:value-of select="sequence"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">																
				<fo:block text-align="left">
					<xsl:value-of select="toolCode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">																
				<fo:block text-align="right">
					<xsl:value-of select="distanceToToolZero"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="2px">																
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(failureDatetime/@epochMS,'dd MMM yyyy HH:mm')"/>					
				</fo:block>
			</fo:table-cell>						
		</fo:table-row>	
	</xsl:template>	
			
</xsl:stylesheet>