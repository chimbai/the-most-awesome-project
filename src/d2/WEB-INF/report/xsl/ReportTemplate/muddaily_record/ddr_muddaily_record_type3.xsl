<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- TEMPLATE: modules/MudProperties -->
	<xsl:template match="modules/MudProperties">
		<xsl:apply-templates select="MudProperties"/>
	</xsl:template>
	
	<!-- TEMPLATE: MudProperties -->
	<xsl:template match="MudProperties">
		
		<xsl:choose>
			
			<!-- MUDTYPE = CMPT FLUID -->
			<xsl:when test="mudType='cmpt_fluid'">
				<fo:table-row keep-together="always">
					<fo:table-cell padding-bottom="2px" padding-top="2px">
					
						<fo:block keep-together="always">
					
							<!-- TABLE -->
							<fo:table width="100%" table-layout="fixed" font-size="7pt" wrap-option="wrap" space-after="6pt">
								<fo:table-column column-number="1" column-width="proportional-column-width(45)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(55)"/>
								
								<!-- HEADER -->
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell padding="2px" number-columns-spanned="2" background-color="#DDDDDD" display-align="center"
											border-left="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
											<fo:block font-size="10pt" font-weight="bold">
												Completion Fluid Data
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								
								<!-- BODY -->
								<fo:table-body>
									<fo:table-row>
									
										<!-- COLUMN 1 -->
										<fo:table-cell number-columns-spanned="2" border-left="thin solid black" border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(45)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(55)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Mud Desc:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="left">																			
																<xsl:value-of select="mudMedium"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Check Depth:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="left">
																<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:if test="depthMdMsl!=''">
																<xsl:value-of select="depthMdMsl/@uomSymbol"/>
																</xsl:if>														
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
																																
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Time:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="left">
																<xsl:choose>
					     									 		<xsl:when test="reportTime/@epochMS = '86400000'">
							      							 			<fo:inline>24:00</fo:inline>
					      											</xsl:when>
							     							 		<xsl:otherwise>
							       										<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
							      									</xsl:otherwise>
					    								 		</xsl:choose>				
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Weight:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="left">
																<xsl:value-of select="mudWeight"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudWeight!=''">
																<xsl:value-of select="mudWeight/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="left">
																<xsl:value-of select="mudTestTemperature"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudTestTemperature!=''">
																<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>

												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>

									</fo:table-row>
									
									<!-- COMMENT & RHEOLOGY -->
									<fo:table-row>
										<fo:table-cell number-columns-spanned="4" border="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(12)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(88)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block font-weight="bold">
																Comment:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block linefeed-treatment="preserve">
																<xsl:value-of select="mudEngineerSummary"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>							
										</fo:table-cell>
										
										<fo:table-cell border-right="thin solid black" border-bottom="thin solid black" border-top="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Rheo. (3/6/100):
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="MudRheology[rpm='3']/rheologyReading" />
																<xsl:if test="MudRheology[rpm='6']/rheologyReading!=''">
																<xsl:value-of select="' / '" />
																</xsl:if>
																<xsl:value-of select="MudRheology[rpm='6']/rheologyReading" />
																<xsl:if test="MudRheology[rpm='100']/rheologyReading!=''">
																<xsl:value-of select="' / '" />
																</xsl:if>
																<xsl:value-of select="MudRheology[rpm='100']/rheologyReading" />
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>							
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>
								
							</fo:table>
							
						</fo:block>
						
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			
			<!-- MUDTYPE = OIL -->
			<xsl:when test="mudType='oil'">
				<fo:table-row keep-together="always">
					<fo:table-cell padding-bottom="2px" padding-top="2px">
					
						<fo:block keep-together="always">
					
							<!-- TABLE -->
							<fo:table width="100%" table-layout="fixed" font-size="7pt" wrap-option="wrap" space-after="6pt">
								<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(18)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(18)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(22)"/>
								
								<!-- HEADER -->
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell padding="2px" background-color="#DDDDDD" display-align="center"
											border-left="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
											<fo:block font-size="10pt" font-weight="bold">
												OBM Data
											</fo:block>
										</fo:table-cell>
										<fo:table-cell number-columns-spanned="4" padding="2px" background-color="#DDDDDD" display-align="center"
											border-top="thin solid black" border-bottom="thin solid black" border-right="thin solid black">
											<fo:block font-size="10pt" font-weight="bold" text-align="right">
												Cost Today 
												<xsl:if test="mudCost!=''">
												<xsl:value-of select="mudCost/@uomSymbol"/>
												</xsl:if>
												<fo:inline color="#DDDDDD">.</fo:inline>
												<xsl:value-of select="mudCost"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								
								<!-- BODY -->
								<fo:table-body>
									<fo:table-row>
									
										<!-- COLUMN 1 -->
										<fo:table-cell border-left="thin solid black" border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Mud Desc:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">																			
																<xsl:value-of select="mudMedium"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Oil Type:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="oiltype"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sample From:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="sampleTakenFrom"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Check Depth:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:if test="depthMdMsl!=''">
																<xsl:value-of select="depthMdMsl/@uomSymbol"/>
																</xsl:if>														
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>Time:</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Weight:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudWeight"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudWeight!=''">
																<xsl:value-of select="mudWeight/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTestTemperature"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudTestTemperature!=''">
																<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 2 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(60)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																API FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="fluidLossApiVolume"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="fluidLossApiVolume!=''">
																<xsl:value-of select="fluidLossApiVolume/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Filter-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="mudApiCake"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudApiCake!=''">
																<xsl:value-of select="mudApiCake/@uomSymbol"/>		
																</xsl:if>												
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="hthpFl"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpFl!=''">
																<xsl:value-of select="hthpFl/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpCake"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpCake!=''">
																<xsl:value-of select="hthpCake/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Temp:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpTestTemperature"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpTestTemperature!=''">
																<xsl:value-of select="hthpTestTemperature/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Press:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpTestPressure"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpTestPressure!=''">
																<xsl:value-of select="hthpTestPressure/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													
									
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 3 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																Cl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="mudChlorideIonConcentration"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudChlorideIonConcentration!=''">
																<xsl:value-of select="mudChlorideIonConcentration/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																O/W Ratio:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="owratio"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																CaCl Mud:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="caclMud"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="caclMud/@uomSymbol"/>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																CaCl2 WPS:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="caclWp"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="caclWp/@uomSymbol"/>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">										
															<fo:block>
																Excess Lime:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">																			
																<xsl:value-of select="mudExcessLimeConcentration"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudExcessLimeConcentration!=''">
																<xsl:value-of select="mudExcessLimeConcentration/@uomSymbol"/>
																</xsl:if>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
														
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 4 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Solids:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">																			
																<xsl:value-of select="mudDissolvedSolids"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudDissolvedSolids!=''">
																<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>
																</xsl:if>														
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																H2O:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudH2o"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudH2o!=''">
																<xsl:value-of select="mudH2o/@uomSymbol"/>														
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Oil:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudOil"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudOil!=''">
																<xsl:value-of select="mudOil/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sand:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test = "sandTxt='0.001'">
																		tr
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="sandTxt"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="sandTxt/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
																<!--
																<xsl:value-of select="mudSand"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudSand/@uomSymbol"/>
																-->
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																LGS:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="lgs"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="lgs!=''">
																<xsl:value-of select="lgs/@uomSymbol"/>		
																</xsl:if>												
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Oil On Cut:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="oilcuttings"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="oilcuttings!=''">
																<xsl:value-of select="oilcuttings/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 5 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Viscosity:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudFv"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudFv!=''">
																<xsl:value-of select="mudFv/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PV:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudPv"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudPv!=''">
																<xsl:value-of select="mudPv/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																YP:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudYp"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudYp!=''">
																<xsl:value-of select="mudYp/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10s:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:if test="string($mudGel10)!=''">
																<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10s/@uomSymbol"/>
																</fo:block>
															</xsl:if>
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10m:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10m"/>
															<xsl:if test="string($mudGel10)!=''">
																<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10m/@uomSymbol"/>
																</fo:block>
															</xsl:if>
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Electric Stability:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="electricstab"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="electricstab!=''">
																<xsl:value-of select="electricstab/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
							
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
									</fo:table-row>
									
									<!-- COMMENT & RHEOLOGY -->
									<fo:table-row>
										<fo:table-cell number-columns-spanned="4" border="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(12)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(88)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block font-weight="bold">
																Comment:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block linefeed-treatment="preserve">
																<xsl:value-of select="mudEngineerSummary"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>							
										</fo:table-cell>
										
										<fo:table-cell border-right="thin solid black" border-bottom="thin solid black" border-top="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Rheo. (3/6/100):
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="MudRheology[rpm='3']/rheologyReading" />
																<xsl:if test="MudRheology[rpm='6']/rheologyReading!=''">
																<xsl:value-of select="' / '" />
																</xsl:if>
																<xsl:value-of select="MudRheology[rpm='6']/rheologyReading" />
																<xsl:if test="MudRheology[rpm='100']/rheologyReading!=''">
																<xsl:value-of select="' / '" />
																</xsl:if>
																<xsl:value-of select="MudRheology[rpm='100']/rheologyReading" />
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>							
										</fo:table-cell>											
									</fo:table-row>
									
								</fo:table-body>
								
							</fo:table>
							
						</fo:block>
						
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			
			<!-- MUDTYPE = SBM -->
			<xsl:when test="mudType='sbm'">
				<fo:table-row keep-together="always">
					<fo:table-cell padding-bottom="2px" padding-top="2px">
					
						<fo:block keep-together="always">
						
							<fo:table width="100%" table-layout="fixed" font-size="7pt" wrap-option="wrap" space-after="6pt">
								<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(18)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(18)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(22)"/>
								
								<!-- HEADER -->
								<fo:table-header>
									<fo:table-row font-size="10pt">
										<fo:table-cell padding="2px" background-color="#DDDDDD" display-align="center"
											border-left="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
											<fo:block font-size="10pt" font-weight="bold">
												SBM Data
											</fo:block>
										</fo:table-cell>
										<fo:table-cell number-columns-spanned="4" padding="2px" background-color="#DDDDDD" display-align="center"
											border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
											<fo:block font-size="10pt" font-weight="bold" text-align="right">
												Cost Today 
												<xsl:if test="mudCost!=''">
												<xsl:value-of select="mudCost/@uomSymbol"/>
												</xsl:if>
												<fo:inline color="#DDDDDD">.</fo:inline>
												<xsl:value-of select="mudCost"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								
								<!-- BODY -->
								<fo:table-body>
									<fo:table-row>
									
										<!-- COLUMN 1 -->
										<fo:table-cell border-left="thin solid black" border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																Mud Desc:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="mudMedium"/>														
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sample From:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="sampleTakenFrom"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Check Depth:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="depthMdMsl"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="depthMdMsl!=''">
																<xsl:value-of select="depthMdMsl/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Time:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
					     									 		<xsl:when test="reportTime/@epochMS = '86400000'">
							     							  			<fo:inline>24:00</fo:inline>
					   									   			</xsl:when>
							     							 		<xsl:otherwise>
							     							  			<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
							    							  		</xsl:otherwise>
					   									  		</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Weight:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudWeight"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudWeight!=''">
																<xsl:value-of select="mudWeight/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTestTemperature"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudTestTemperature!=''">
																<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
																											
										<!-- COLUMN 2 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(60)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																API FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="fluidLossApiVolume"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="fluidLossApiVolume!=''">
																<xsl:value-of select="fluidLossApiVolume/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Filter-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="mudApiCake"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudApiCake!=''">
																<xsl:value-of select="mudApiCake/@uomSymbol"/>		
																</xsl:if>												
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="hthpFl"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpFl!=''">
																<xsl:value-of select="hthpFl/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpCake"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpCake!=''">
																<xsl:value-of select="hthpCake/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Temp:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpTestTemperature"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpTestTemperature!=''">
																<xsl:value-of select="hthpTestTemperature/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Press:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpTestPressure"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpTestPressure!=''">
																<xsl:value-of select="hthpTestPressure/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
																										
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 3 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																Cl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="mudChlorideIonConcentration"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudChlorideIonConcentration!=''">
																<xsl:value-of select="mudChlorideIonConcentration/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Cl (in whole mud):
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudChlorideIonDensity"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudChlorideIonDensity/@uomSymbol"/>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Syn/Wtr Ratio:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="owratio"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">										
															<fo:block>
																Excess Lime:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">																			
																<xsl:value-of select="mudExcessLimeConcentration"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudExcessLimeConcentration!=''">
																<xsl:value-of select="mudExcessLimeConcentration/@uomSymbol"/>
																</xsl:if>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
														
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 4 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Solids:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">																			
																<xsl:value-of select="mudDissolvedSolids"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudDissolvedSolids!=''">
																<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>
																</xsl:if>														
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																H2O:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudH2o"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudH2o!=''">
																<xsl:value-of select="mudH2o/@uomSymbol"/>														
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Oil:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudOil"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudOil!=''">
																<xsl:value-of select="mudOil/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sand:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test = "sandTxt='0.001'">
																		tr
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="sandTxt"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="sandTxt/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
																<!--
																<xsl:value-of select="mudSand"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudSand/@uomSymbol"/>
																-->
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																LGS:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="lgs"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="lgs!=''">
																<xsl:value-of select="lgs/@uomSymbol"/>		
																</xsl:if>												
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Oil On Cut:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="oilcuttings"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="oilcuttings!=''">
																<xsl:value-of select="oilcuttings/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 5 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Viscosity:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudFv"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudFv!=''">
																<xsl:value-of select="mudFv/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PV:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudPv"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudPv!=''">
																<xsl:value-of select="mudPv/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																YP:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudYp"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudYp!=''">
																<xsl:value-of select="mudYp/@uomSymbol"/>
																</xsl:if>														
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
														
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10s:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:if test="string($mudGel10)!=''">
																<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10s/@uomSymbol"/>
																</fo:block>
															</xsl:if>
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10m:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10m"/>
															<xsl:if test="string($mudGel10)!=''">
																<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10m/@uomSymbol"/>
																</fo:block>
															</xsl:if>
														</fo:table-cell>
													</fo:table-row>
												
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
									</fo:table-row>
									
									<!-- COMMENT & RHEOLOGY -->
									<fo:table-row>
										<fo:table-cell number-columns-spanned="4" border="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(12)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(88)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block font-weight="bold">
																Comment:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block linefeed-treatment="preserve">
																<xsl:value-of select="mudEngineerSummary"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>							
										</fo:table-cell>
										
										<fo:table-cell border-right="thin solid black" border-bottom="thin solid black" border-top="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Rheo. (3/6/100):
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="MudRheology[rpm='3']/rheologyReading" />
																<xsl:if test="MudRheology[rpm='6']/rheologyReading!=''">
																<xsl:value-of select="' / '" />
																</xsl:if>
																<xsl:value-of select="MudRheology[rpm='6']/rheologyReading" />
																<xsl:if test="MudRheology[rpm='100']/rheologyReading!=''">
																<xsl:value-of select="' / '" />
																</xsl:if>
																<xsl:value-of select="MudRheology[rpm='100']/rheologyReading" />
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>							
										</fo:table-cell>								
									</fo:table-row>
									
								</fo:table-body>
							</fo:table>
							
						</fo:block>
						
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>

			<!-- MUDTYPE = H2O -->
			<xsl:when test="mudType='h2o'">	
				<fo:table-row keep-together="always">
					<fo:table-cell padding-bottom="2px" padding-top="2px">
					
						<fo:block keep-together="always">
				
							<fo:table width="100%" table-layout="fixed" font-size="7pt" wrap-option="wrap" space-after="6pt">
								<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(18)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(18)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(22)"/>
								
								<!-- HEADER -->
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell padding="2px" background-color="#DDDDDD" display-align="center"
											border-left="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
											<fo:block font-size="10pt" font-weight="bold">
												WBM Data
											</fo:block>
										</fo:table-cell>
										<fo:table-cell number-columns-spanned="4" padding="2px" background-color="#DDDDDD" display-align="center"
											border-top="thin solid black" border-bottom="thin solid black" border-right="thin solid black">
											<fo:block font-size="10pt" font-weight="bold" text-align="right">
												Cost Today 
												<xsl:if test="mudCost!=''">
												<xsl:value-of select="mudCost/@uomSymbol"/>
												</xsl:if>
												<fo:inline color="#DDDDDD">.</fo:inline>
												<xsl:value-of select="mudCost"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								
								<!-- BODY -->
								<fo:table-body>
									<fo:table-row>
									
										<!-- COLUMN 1 -->
										<fo:table-cell border-left="thin solid black" border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																Mud Desc:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="mudMedium"/>														
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Check Depth:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:if test="depthMdMsl!=''">
																<xsl:value-of select="depthMdMsl/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
																																
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Time:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
					     									 		<xsl:when test="reportTime/@epochMS = '86400000'">
							      							 			<fo:inline>24:00</fo:inline>
					      											</xsl:when>
							     							 		<xsl:otherwise>
							       										<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
							      									</xsl:otherwise>
					    								 		</xsl:choose>				
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Weight:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudWeight"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudWeight!=''">
																<xsl:value-of select="mudWeight/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTestTemperature"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudTestTemperature!=''">
																<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
																																																					
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
																											
										<!-- COLUMN 2 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(60)"/>
												
												<fo:table-body>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																API FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="fluidLossApiVolume"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="fluidLossApiVolume!=''">
																<xsl:value-of select="fluidLossApiVolume/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Filter-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="mudApiCake"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudApiCake!=''">
																<xsl:value-of select="mudApiCake/@uomSymbol"/>		
																</xsl:if>												
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="hthpFl"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpFl!=''">
																<xsl:value-of select="hthpFl/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpCake"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpCake!=''">
																<xsl:value-of select="hthpCake/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Temp:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpTestTemperature"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpTestTemperature!=''">
																<xsl:value-of select="hthpTestTemperature/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Press:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpTestPressure"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpTestPressure!=''">
																<xsl:value-of select="hthpTestPressure/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>	
													
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 3 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																Cl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="mudChlorideIonConcentration"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudChlorideIonConcentration!=''">
																<xsl:value-of select="mudChlorideIonConcentration/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																KCl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudKclConcentration"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudKclConcentration!=''">
																<xsl:value-of select="mudKclConcentration/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Hard/Ca:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="mudCaConcentration"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudCaConcentration!=''">
																<xsl:value-of select="mudCaConcentration/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																MBT/CEC:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudMbtCec"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudMbtCec/@uomSymbol"/>														
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Pm:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudPmVolume"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudPmVolume/@uomSymbol"/>														
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Pf:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudPf"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudPf/@uomSymbol"/>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 4 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(45)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(55)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																Solids:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="mudDissolvedSolids"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudDissolvedSolids!=''">
																<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																H2O:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudH2o"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudH2o!=''">
																<xsl:value-of select="mudH2o/@uomSymbol"/>														
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sand:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test = "sandTxt='0.001'">
																		tr
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="sandTxt"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="sandTxt/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
																<!--
																<xsl:value-of select="mudSand"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudSand/@uomSymbol"/>
																-->
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																pH:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="mudPh"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudPh/@uomSymbol"/>														
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PHPA:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudPhpaDensity"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudPhpaDensity!=''">
																<xsl:value-of select="mudPhpaDensity/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Mf:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="mudMfVolume"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudMfVolume!=''">
																<xsl:value-of select="mudMfVolume/@uomSymbol"/>														
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 5 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Glycol:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="glycol"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="glycol!=''">
																<xsl:value-of select="glycol/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Viscosity:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudFv"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudFv!=''">
																<xsl:value-of select="mudFv/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PV:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudPv"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudPv!=''">
																<xsl:value-of select="mudPv/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																YP:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudYp"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudYp!=''">
																<xsl:value-of select="mudYp/@uomSymbol"/>
																</xsl:if>														
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
														
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10s:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:if test="string($mudGel10)!=''">
																<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10s/@uomSymbol"/>
																</fo:block>
															</xsl:if>
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10m:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10m"/>
															<xsl:if test="string($mudGel10)!=''">
																<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10m/@uomSymbol"/>
																</fo:block>
															</xsl:if>
														</fo:table-cell>
													</fo:table-row>
												
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
									</fo:table-row>
									
									<!-- COMMENT & RHEOLOGY -->
									<fo:table-row>
										<fo:table-cell number-columns-spanned="4" border="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(12)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(88)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block font-weight="bold">
																Comment:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block linefeed-treatment="preserve">
																<xsl:value-of select="mudEngineerSummary"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>							
										</fo:table-cell>
										
										<fo:table-cell border-right="thin solid black" border-bottom="thin solid black" border-top="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Rheo. (3/6/100):
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="MudRheology[rpm='3']/rheologyReading" />
																<xsl:if test="MudRheology[rpm='6']/rheologyReading!=''">
																<xsl:value-of select="' / '" />
																</xsl:if>
																<xsl:value-of select="MudRheology[rpm='6']/rheologyReading" />
																<xsl:if test="MudRheology[rpm='100']/rheologyReading!=''">
																<xsl:value-of select="' / '" />
																</xsl:if>
																<xsl:value-of select="MudRheology[rpm='100']/rheologyReading" />
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>							
										</fo:table-cell>	
																				
									</fo:table-row>
									
								</fo:table-body>
								
							</fo:table>
							
						</fo:block>
						
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row keep-together="always">
					<fo:table-cell padding-bottom="2px" padding-top="2px">
					
						<fo:block keep-together="always">
						
							<fo:table width="100%" table-layout="fixed" font-size="7pt" wrap-option="wrap" space-after="6pt">
								<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(18)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(18)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(22)"/>
								
								<!-- HEADER -->
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell padding="2px" background-color="#DDDDDD" display-align="center"
											border-left="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
											<xsl:if test="mudType='ester'">
												<fo:block font-size="10pt" font-weight="bold">
													Ester Data
												</fo:block>	
											</xsl:if>
											<xsl:if test="mudType='foam'">
												<fo:block font-size="10pt" font-weight="bold">
													Foam Data
												</fo:block>	
											</xsl:if>
											<xsl:if test="mudType='air'">
												<fo:block font-size="10pt" font-weight="bold">
													Air Data
												</fo:block>	
											</xsl:if>
											<xsl:if test="mudType='oth'">
												<fo:block font-size="10pt" font-weight="bold">
													Fluid Data
												</fo:block>	
											</xsl:if>
											<xsl:if test="mudType=''">
												<fo:block font-size="10pt" font-weight="bold"/>	
											</xsl:if>
										</fo:table-cell>
										<fo:table-cell number-columns-spanned="4" padding="2px" background-color="#DDDDDD" display-align="center"
											border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
											<fo:block font-size="10pt" font-weight="bold" text-align="right">
												Cost Today 
												<xsl:if test="mudCost!=''">
												<xsl:value-of select="mudCost/@uomSymbol"/>
												</xsl:if>
												<fo:inline color="#DDDDDD">.</fo:inline>
												<xsl:value-of select="mudCost"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								
								<!-- BODY -->
								<fo:table-body>
									<fo:table-row>
									
										<!-- COLUMN 1 -->
										<fo:table-cell border-left="thin solid black" border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																Mud Desc:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="mudMedium"/>														
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Check Depth:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:if test="depthMdMsl!=''">
																<xsl:value-of select="depthMdMsl/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
																																
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Time:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
					     									 		<xsl:when test="reportTime/@epochMS = '86400000'">
							      							 			<fo:inline>24:00</fo:inline>
					      											</xsl:when>
							     							 		<xsl:otherwise>
							       										<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
							      									</xsl:otherwise>
					    								 		</xsl:choose>				
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Weight:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudWeight"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudWeight!=''">
																<xsl:value-of select="mudWeight/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTestTemperature"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudTestTemperature!=''">
																<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
																																																					
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
																											
										<!-- COLUMN 2 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(60)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																API FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="fluidLossApiVolume"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="fluidLossApiVolume!=''">
																<xsl:value-of select="fluidLossApiVolume/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Filter-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="mudApiCake"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudApiCake!=''">
																<xsl:value-of select="mudApiCake/@uomSymbol"/>		
																</xsl:if>												
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="hthpFl"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpFl!=''">
																<xsl:value-of select="hthpFl/@uomSymbol"/>
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpCake"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpCake!=''">
																<xsl:value-of select="hthpCake/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Temp:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpTestTemperature"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpTestTemperature!=''">
																<xsl:value-of select="hthpTestTemperature/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Press:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="hthpTestPressure"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="hthpTestPressure!=''">
																<xsl:value-of select="hthpTestPressure/@uomSymbol"/>
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>		
													
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 3 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																Cl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="mudChlorideIonConcentration"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudChlorideIonConcentration!=''">
																<xsl:value-of select="mudChlorideIonConcentration/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																KCl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudKclConcentration"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudKclConcentration!=''">
																<xsl:value-of select="mudKclConcentration/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Hard/Ca:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="mudCaConcentration"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudCaConcentration!=''">
																<xsl:value-of select="mudCaConcentration/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																MBT/CEC:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudMbtCec"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudMbtCec/@uomSymbol"/>														
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Pm:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudPmVolume"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudPmVolume/@uomSymbol"/>														
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Pf:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudPf"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudPf/@uomSymbol"/>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 4 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(45)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(55)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block>
																Solids:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="mudDissolvedSolids"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudDissolvedSolids!=''">
																<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																H2O:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudH2o"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudH2o!=''">
																<xsl:value-of select="mudH2o/@uomSymbol"/>														
																</xsl:if>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sand:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test = "sandTxt='0.001'">
																		tr
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="sandTxt"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="sandTxt/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
																<!--
																<xsl:value-of select="mudSand"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudSand/@uomSymbol"/>
																-->
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																pH:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="mudPh"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudPh/@uomSymbol"/>														
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PHPA:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudPhpaDensity"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudPhpaDensity!=''">
																<xsl:value-of select="mudPhpaDensity/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Mf:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
															<fo:block text-align="right">
																<xsl:value-of select="mudMfVolume"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudMfVolume!=''">
																<xsl:value-of select="mudMfVolume/@uomSymbol"/>														
																</xsl:if>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 5 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Glycol:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="glycol"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="glycol!=''">
																<xsl:value-of select="glycol/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Viscosity:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudFv"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudFv!=''">
																<xsl:value-of select="mudFv/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PV:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudPv"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudPv!=''">
																<xsl:value-of select="mudPv/@uomSymbol"/>	
																</xsl:if>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																YP:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="mudYp"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:if test="mudYp!=''">
																<xsl:value-of select="mudYp/@uomSymbol"/>
																</xsl:if>														
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
														
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10s:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:if test="string($mudGel10)!=''">
																<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10s/@uomSymbol"/>
																</fo:block>
															</xsl:if>
														</fo:table-cell>
													</fo:table-row>	
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10m:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10m"/>
															<xsl:if test="string($mudGel10)!=''">
																<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10m/@uomSymbol"/>
																</fo:block>
															</xsl:if>
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
									</fo:table-row>
									
									<!-- COMMENT & RHEOLOGY -->
									<fo:table-row>
										<fo:table-cell number-columns-spanned="4" border="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(12)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(88)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block font-weight="bold">
																Comment:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block linefeed-treatment="preserve">
																<xsl:value-of select="mudEngineerSummary"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>							
										</fo:table-cell>
										
										<fo:table-cell border-right="thin solid black" border-bottom="thin solid black" border-top="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Rheo. (3/6/100):
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
																<xsl:value-of select="MudRheology[rpm='3']/rheologyReading" />
																<xsl:if test="MudRheology[rpm='6']/rheologyReading!=''">
																<xsl:value-of select="' / '" />
																</xsl:if>
																<xsl:value-of select="MudRheology[rpm='6']/rheologyReading" />
																<xsl:if test="MudRheology[rpm='100']/rheologyReading!=''">
																<xsl:value-of select="' / '" />
																</xsl:if>
																<xsl:value-of select="MudRheology[rpm='100']/rheologyReading" />
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>							
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>

</xsl:stylesheet>