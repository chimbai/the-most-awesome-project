<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- OBM Mud Type START -->
	<xsl:template match="modules/MudProperties" mode="oil_header">
	
		<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="left" font-size="12pt">OBM
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<xsl:variable name="dailyid" select="/root/modules/Daily/Daily/dailyUid"/>
		<xsl:if test="MudProperties[mudType='oil']">
			<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="6pt" border="0.5px solid black" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
				<fo:table-column column-number="1" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(7)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="9" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="10" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="11" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="12" column-width="proportional-column-width(2)"/>
				<fo:table-column column-number="13" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="14" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="15" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="16" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="17" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="18" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="19" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="20" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="21" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="22" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="23" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="24" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="25" column-width="proportional-column-width(5)"/>												
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							R# <xsl:value-of select="//muddaily/L_muddaily.daynum"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Date - Time <xsl:value-of select="//muddaily/L_muddaily.checkdt"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Type <xsl:value-of select="//muddaily/L_muddaily.mud_type_descr"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Depth <xsl:value-of select="//muddaily/L_muddaily.depth"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Oil Type	 <xsl:value-of select="//muddaily/L_muddaily.oiltype"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							TMP <xsl:value-of select="//muddaily/L_muddaily.temperature"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							MW <xsl:value-of select="//muddaily/L_muddaily.mwsurface"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							VIS <xsl:value-of select="//muddaily/L_muddaily.funnelvis"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							PV <xsl:value-of select="//muddaily/L_muddaily.pv"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							YP <xsl:value-of select="//muddaily/L_muddaily.yp"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black" number-columns-spanned="2">
							<fo:block text-align="center">
							Gel10s <xsl:value-of select="//muddaily/L_muddaily.gel10s"/> / 
							</fo:block>
							<fo:block text-align="center">
							10m <xsl:value-of select="//muddaily/L_muddaily.gel10m"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							F.L. API <xsl:value-of select="//muddaily/L_muddaily.apifl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Cake <xsl:value-of select="//muddaily/L_muddaily.hthpcake"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Sol	<xsl:value-of select="//muddaily/L_muddaily.solids"/>
							</fo:block>
						</fo:table-cell>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							%LGS <xsl:value-of select="//muddaily/L_muddaily.lgs"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							SND <xsl:value-of select="//muddaily/L_muddaily.sand"/>
							</fo:block>
						</fo:table-cell>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Oil on <xsl:value-of select="//muddaily/L_muddaily.oil"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							O/W Ratio <xsl:value-of select="//muddaily/L_muddaily.owratio"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Mp <xsl:value-of select="//muddaily/L_muddaily.mp"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Cl <xsl:value-of select="//muddaily/L_muddaily.cl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							h2o Sal <xsl:value-of select="//muddaily/L_muddaily.h2osalinity"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Excs Lime <xsl:value-of select="//muddaily/L_muddaily.excesslime"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Elec Stab <xsl:value-of select="//muddaily/L_muddaily.electricstab"/>
							</fo:block>
						</fo:table-cell>					
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block text-align="center">
							Daily Cost <xsl:value-of select="//muddaily/L_muddaily.cost"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
					<xsl:apply-templates select="MudProperties[mudType='oil']" mode="obm_records">
						<xsl:sort select="reportNumber" data-type="number"/>
					</xsl:apply-templates>
				</fo:table-body>
			</fo:table>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="MudProperties" mode="obm_records">	
		<fo:table-row>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="reportNumber"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:variable name="dailyid" select="dailyUid"/>
					<xsl:value-of select="/root/modules/Daily/Daily[dailyUid=$dailyid]/dayDate"/> - 
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudMedium"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="depthMdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="oiltype"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudTestTemperature"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudWeight"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudFv"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudPv"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudYp"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black" number-columns-spanned="2">
				<fo:block>
					<xsl:value-of select="mudGel10s"/> / <xsl:value-of select="mudGel10m"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudApiFl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="hthpCake"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudDissolvedSolids"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="lgs"/>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="sand"/>					
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudOil"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="owratio"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mp"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudChlorideIon"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="h2osalinity"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudExcessLime"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="electricstab"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="0.1cm">
				<fo:block>
					<xsl:value-of select="mudCost"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- OBM Mud Type END -->
	
</xsl:stylesheet>