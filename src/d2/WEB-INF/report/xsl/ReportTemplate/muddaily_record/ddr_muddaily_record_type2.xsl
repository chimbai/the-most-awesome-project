<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- TEMPLATE: modules/MudProperties -->
	<xsl:template match="modules/MudProperties">
		<xsl:apply-templates select="MudProperties"/>
	</xsl:template>
	
	<!-- TEMPLATE: MudProperties -->
	<xsl:template match="MudProperties">
		
		<xsl:choose>
		
			<!-- MUDTYPE = OIL -->
			<xsl:when test="mudType='oil'">
				<fo:table-row keep-together="always">
					<fo:table-cell padding-bottom="2px" padding-top="2px">
					
						<fo:block keep-together="always">
					
							<!-- TABLE -->
							<fo:table width="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt" border-bottom="thin solid black">
								<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(21)"/>
								
								<!-- HEADER -->
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell padding="2px" background-color="#DDDDDD" display-align="center" number-columns-spanned="4"
											border-left="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
											<fo:block font-size="8pt" font-weight="bold">
												OBM Data
											</fo:block>
										</fo:table-cell>
										<fo:table-cell  padding="2px" background-color="#DDDDDD" display-align="center" border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black"> 
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell background-color="#DDDDDD" display-align="center">
															<fo:block font-size="7pt" font-weight="bold" text-align="left">
																Cost Today:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell background-color="#DDDDDD" display-align="center">
															<fo:block font-size="7pt" font-weight="bold" text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudCost = '') or (mudCost = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudCost/@uomSymbol"/>
																		<xsl:value-of select="mudCost"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								
								<!-- BODY -->
								<fo:table-body>
									<fo:table-row>
									
										<!-- COLUMN 1 -->
										<fo:table-cell border-left="thin solid black" border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(57)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(43)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Mud Desc.:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">																			
																<xsl:value-of select="mudMedium"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Oil Type:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="oiltype"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sample From:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>													
																	<xsl:when test="string(sampleTakenFrom/@lookupLabel)=''">
																		<xsl:value-of select="sampleTakenFrom"/>										
																	</xsl:when>													
																	<xsl:otherwise>
																		<xsl:value-of select="sampleTakenFrom/@lookupLabel"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Check Depth:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(depthMdMsl = '') or (depthMdMsl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="depthMdMsl"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="depthMdMsl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>Time:</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Weight:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudWeight = '') or (mudWeight = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudWeight"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudWeight/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp.:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudTestTemperature = '') or (mudTestTemperature = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudTestTemperature"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<!--
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp. In:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTemperatureBeforeEnter"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudTemperatureBeforeEnter/@uomSymbol"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp. Out:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTemperatureBeforeLeave"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudTemperatureBeforeLeave/@uomSymbol"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													-->
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 2 -->
										<fo:table-cell border-right="thin solid black" >
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(48)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(52)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																API FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">	
																<xsl:choose>
																	<xsl:when test="(fluidLossApiVolume = '') or (fluidLossApiVolume = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																		
																		<xsl:value-of select="fluidLossApiVolume"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="fluidLossApiVolume/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Filter-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudApiCake = '') or (mudApiCake = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>	
																		<xsl:value-of select="mudApiCake"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudApiCake/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpFl = '') or (hthpFl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpFl"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpFl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">	
																<xsl:choose>
																	<xsl:when test="(hthpCake = '') or (hthpCake = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpCake"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpCake/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>						
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Temp:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpTestTemperature = '') or (hthpTestTemperature = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																			
																		<xsl:value-of select="hthpTestTemperature"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpTestTemperature/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Press:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpTestPressure = '') or (hthpTestPressure = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpTestPressure"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpTestPressure/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 3 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(60)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Cl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudChlorideIonConcentration = '') or (mudChlorideIonConcentration = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudChlorideIonConcentration"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudChlorideIonConcentration/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																O/W Ratio:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="owratio"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																CaCl Mud:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(caclMud = '') or (caclMud = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="caclMud"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="caclMud/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																CaCl2 WPS:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(caclWp = '') or (caclWp = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="caclWp"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="caclWp/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">										
															<fo:block>
																Excess Lime:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudExcessLimeDensity = '') or (mudExcessLimeDensity = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																			
																		<xsl:value-of select="mudExcessLimeDensity"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudExcessLimeDensity/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>									
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 4 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(54)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(46)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Solids:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">	
																<xsl:choose>
																	<xsl:when test="(mudDissolvedSolids = '') or (mudDissolvedSolids = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																			
																		<xsl:value-of select="mudDissolvedSolids"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																H2O:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudH2o = '') or (mudH2o = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudH2o"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudH2o/@uomSymbol"/>		
																	</xsl:otherwise>
																</xsl:choose>												
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Oil:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudH2o = '') or (mudH2o = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudOil"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudOil/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sand:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(sandTxt = '') or (sandTxt = 'null') ">
																	</xsl:when>
																	<xsl:when test = "sandTxt='0.001'">
																		tr
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="sandTxt"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="sandTxt/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
																<!--
																<xsl:value-of select="mudSand"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudSand/@uomSymbol"/>
																-->
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																LGS:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(lgs = '') or (lgs = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="lgs"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="lgs/@uomSymbol"/>		
																	</xsl:otherwise>
																</xsl:choose>							
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Oil On Cut:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(oilcuttings = '') or (oilcuttings = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="oilcuttings"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="oilcuttings/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										
										<!-- COLUMN 5 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(58)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(42)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Viscosity:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudFv = '') or (mudFv = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudFv"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudFv/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PV:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudPv = '') or (mudPv = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudPv"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudPv/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																YP:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudYp = '') or (mudYp = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudYp"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudYp/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10s:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:choose>
																<xsl:when test="string($mudGel10)!=''">
																	<fo:block text-align="right">		
																		<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10s/@uomSymbol"/>
																	</fo:block>
																</xsl:when>
																<xsl:otherwise>
																	<fo:block text-align="right">												
																		<xsl:value-of select="mudGel10s"/>
																	</fo:block>
																</xsl:otherwise>
															</xsl:choose>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10m:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:choose>
																<xsl:when test="string($mudGel10)!=''">
																	<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10m/@uomSymbol"/>
																	</fo:block>
																</xsl:when>
																<xsl:otherwise>
																	<fo:block text-align="right">												
																		<xsl:value-of select="mudGel10m"/>
																	</fo:block>
																</xsl:otherwise>
															</xsl:choose>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Electric Stability:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(electricstab = '') or (electricstab = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="electricstab"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="electricstab/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
																						
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
									</fo:table-row>
									
									<!-- COMMENT -->
									<fo:table-row border-bottom="thin solid black">
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" number-columns-spanned="4" border-left="thin solid black" border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black" background-color="#DDDDDD">
											<fo:block font-weight="bold" >
												Comment:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell number-rows-spanned="2" border-right="thin solid black" border-top="thin solid black">
											<xsl:variable name="mudID" select="mudPropertiesUid"/>
											<xsl:choose>
												<xsl:when test="count(MudRheology[mudPropertiesUid = $mudID]) = '0'">
													<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row background-color="#DDDDDD">
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	 border-bottom="thin solid black" border-right="thin solid black" >
																	<fo:block text-align="center" font-weight="bold" >
																		RPM
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"
																	border-bottom="thin solid black">
																	<fo:block text-align="center" font-weight="bold" >
																		Reading
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>															
														</fo:table-header>
														<fo:table-body>
															<fo:table-row>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	  number-columns-spanned="2">
																	<fo:block>
																		<fo:inline color="white">.</fo:inline>
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>	
														</fo:table-body>
													</fo:table>
												</xsl:when>
												<xsl:otherwise>
													<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row background-color="#DDDDDD">
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	 border-bottom="thin solid black" border-right="thin solid black" >
																	<fo:block text-align="center" font-weight="bold" >
																		RPM
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"
																	border-bottom="thin solid black">
																	<fo:block text-align="center" font-weight="bold" >
																		Reading
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>															
														</fo:table-header>
														<fo:table-body>
															<xsl:apply-templates select="MudRheology[mudPropertiesUid = $mudID]"/>
														</fo:table-body>
													</fo:table>
												</xsl:otherwise>
											</xsl:choose>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" number-columns-spanned="4" border-left="thin solid black" border-right="thin solid black" >
											<fo:block linefeed-treatment="preserve">
												<xsl:value-of select="mudEngineerSummary"/>
											</fo:block>	
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>
								
							</fo:table>
							
						</fo:block>
						
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			
			<!-- MUDTYPE = SBM -->
			<xsl:when test="mudType='sbm'">
				<fo:table-row keep-together="always">
					<fo:table-cell padding-bottom="2px" padding-top="2px">
					
						<fo:block keep-together="always">
					
							<!-- TABLE -->
							<fo:table width="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt">
								<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(21)"/>
								
								<!-- HEADER -->
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell padding="2px" background-color="#DDDDDD" display-align="center" number-columns-spanned="4"
											border-left="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
											<fo:block font-size="8pt" font-weight="bold">
												SBM Data
											</fo:block>
										</fo:table-cell>
										<fo:table-cell  padding="2px" background-color="#DDDDDD" display-align="center" border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black"> 
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell background-color="#DDDDDD" display-align="center">
															<fo:block font-size="7pt" font-weight="bold" text-align="left">
																Cost Today:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell background-color="#DDDDDD" display-align="center">
															<fo:block font-size="7pt" font-weight="bold" text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudCost = '') or (mudCost = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudCost/@uomSymbol"/>
																		<xsl:value-of select="mudCost"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								
								<!-- BODY -->
								<fo:table-body>
									<fo:table-row>
									
										<!-- COLUMN 1 -->
										<fo:table-cell border-left="thin solid black" border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(57)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(43)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Mud Desc.:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">																			
																<xsl:value-of select="mudMedium"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sample From:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>													
																	<xsl:when test="string(sampleTakenFrom/@lookupLabel)=''">
																		<xsl:value-of select="sampleTakenFrom"/>										
																	</xsl:when>													
																	<xsl:otherwise>
																		<xsl:value-of select="sampleTakenFrom/@lookupLabel"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Check Depth:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(depthMdMsl = '') or (depthMdMsl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="depthMdMsl"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="depthMdMsl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>Time:</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Weight:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudWeight = '') or (mudWeight = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudWeight"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudWeight/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp.:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudTestTemperature = '') or (mudTestTemperature = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudTestTemperature"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<!--
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp. In:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTemperatureBeforeEnter"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudTemperatureBeforeEnter/@uomSymbol"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp. Out:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTemperatureBeforeLeave"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudTemperatureBeforeLeave/@uomSymbol"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													-->
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 2 -->
										<fo:table-cell border-right="thin solid black" >
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(48)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(52)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																API FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">	
																<xsl:choose>
																	<xsl:when test="(fluidLossApiVolume = '') or (fluidLossApiVolume = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																		
																		<xsl:value-of select="fluidLossApiVolume"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="fluidLossApiVolume/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Filter-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudApiCake = '') or (mudApiCake = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudApiCake"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudApiCake/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpFl = '') or (hthpFl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpFl"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpFl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpCake = '') or (hthpCake = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpCake"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpCake/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>						
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Temp:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpTestTemperature = '') or (hthpTestTemperature = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																			
																		<xsl:value-of select="hthpTestTemperature"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpTestTemperature/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Press:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpTestPressure = '') or (hthpTestPressure = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpTestPressure"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpTestPressure/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 3 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(60)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Cl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudChlorideIonConcentration = '') or (mudChlorideIonConcentration = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudChlorideIonConcentration"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudChlorideIonConcentration/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Cl (in whole mud):
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudChlorideIonDensity = '') or (mudChlorideIonDensity = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudChlorideIonDensity"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudChlorideIonDensity/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Syn/Water Ratio:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="owratio"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">										
															<fo:block>
																Excess Lime:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudExcessLimeDensity = '') or (mudExcessLimeDensity = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																			
																		<xsl:value-of select="mudExcessLimeDensity"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudExcessLimeDensity/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>									
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 4 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(54)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(46)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Solids:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">				
																<xsl:choose>
																	<xsl:when test="(mudDissolvedSolids = '') or (mudDissolvedSolids = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>															
																		<xsl:value-of select="mudDissolvedSolids"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>														
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																H2O:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudH2o = '') or (mudH2o = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudH2o"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudH2o/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Oil:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudOil = '') or (mudOil = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudOil"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudOil/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sand:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(sandTxt = '') or (sandTxt = 'null') ">
																	</xsl:when>
																	<xsl:when test = "sandTxt='0.001'">
																		tr
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="sandTxt"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="sandTxt/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
																<!--
																<xsl:value-of select="mudSand"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudSand/@uomSymbol"/>
																-->
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																LGS:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(lgs = '') or (lgs = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="lgs"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="lgs/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Oil On Cut:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(oilcuttings = '') or (oilcuttings = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="oilcuttings"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="oilcuttings/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>														
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										
										<!-- COLUMN 5 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Viscosity:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudFv = '') or (mudFv = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudFv"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudFv/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PV:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudFv = '') or (mudFv = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudPv"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudPv/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																YP:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudYp = '') or (mudYp = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudYp"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudYp/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10s:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:choose>
																<xsl:when test="string($mudGel10)!=''">
																	<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10s/@uomSymbol"/>
																	</fo:block>
																</xsl:when>
																<xsl:otherwise>
																	<fo:block text-align="right">												
																		<xsl:value-of select="mudGel10s"/>
																	</fo:block>
																</xsl:otherwise>
															</xsl:choose>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10m:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:choose>
																<xsl:when test="string($mudGel10)!=''">
																	<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10m/@uomSymbol"/>
																	</fo:block>
																</xsl:when>
																<xsl:otherwise>
																	<fo:block text-align="right">												
																		<xsl:value-of select="mudGel10m"/>
																	</fo:block>
																</xsl:otherwise>
															</xsl:choose>
														</fo:table-cell>
													</fo:table-row>
																						
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
									</fo:table-row>
									
									<!-- COMMENT -->
									<fo:table-row >
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" number-columns-spanned="4" border-left="thin solid black" border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black" background-color="#DDDDDD">
											<fo:block font-weight="bold" >
												Comment:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell number-rows-spanned="2" border-right="thin solid black" border-top="thin solid black">
											<xsl:variable name="mudID" select="mudPropertiesUid"/>
											<xsl:choose>
												<xsl:when test="count(MudRheology[mudPropertiesUid = $mudID]) = '0'">
													<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row background-color="#DDDDDD">
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	 border-bottom="thin solid black" border-right="thin solid black" >
																	<fo:block text-align="center" font-weight="bold" >
																		RPM
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"
																	border-bottom="thin solid black">
																	<fo:block text-align="center" font-weight="bold" >
																		Reading
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>															
														</fo:table-header>
														<fo:table-body>
															<fo:table-row>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	 border-bottom="thin solid black" number-columns-spanned="2">
																	<fo:block>
																		<fo:inline color="white">.</fo:inline>
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>	
														</fo:table-body>
													</fo:table>
												</xsl:when>
												<xsl:otherwise>
													<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row background-color="#DDDDDD">
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	 border-bottom="thin solid black" border-right="thin solid black" >
																	<fo:block text-align="center" font-weight="bold" >
																		RPM
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"
																	border-bottom="thin solid black">
																	<fo:block text-align="center" font-weight="bold" >
																		Reading
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>															
														</fo:table-header>
														<fo:table-body>
															<xsl:apply-templates select="MudRheology[mudPropertiesUid = $mudID]"/>
														</fo:table-body>
													</fo:table>
												</xsl:otherwise>
											</xsl:choose>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" number-columns-spanned="4" border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
											<fo:block linefeed-treatment="preserve">
												<xsl:value-of select="mudEngineerSummary"/>
											</fo:block>	
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>
								
							</fo:table>
							
						</fo:block>
						
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>

			<!-- MUDTYPE = H2O -->
			<xsl:when test="mudType='h2o'">
				<fo:table-row keep-together="always">
					<fo:table-cell padding-bottom="2px" padding-top="2px">
					
						<fo:block keep-together="always">
					
							<!-- TABLE -->
							<fo:table width="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt">
								<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(21)"/>
								
								<!-- HEADER -->
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell padding="2px" background-color="#DDDDDD" display-align="center" number-columns-spanned="4"
											border-left="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
											<fo:block font-size="8pt" font-weight="bold">
												WBM Data
											</fo:block>
										</fo:table-cell>
										<fo:table-cell  padding="2px" background-color="#DDDDDD" display-align="center" border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black"> 
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell background-color="#DDDDDD" display-align="center">
															<fo:block font-size="7pt" font-weight="bold" text-align="left">
																Cost Today:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell background-color="#DDDDDD" display-align="center">
															<fo:block font-size="7pt" font-weight="bold" text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudCost = '') or (mudCost = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudCost/@uomSymbol"/>
																		<xsl:value-of select="mudCost"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								
								<!-- BODY -->
								<fo:table-body>
									<fo:table-row>
									
										<!-- COLUMN 1 -->
										<fo:table-cell border-left="thin solid black" border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(57)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(43)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Mud Desc.:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">																			
																<xsl:value-of select="mudMedium"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Check Depth:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(depthMdMsl = '') or (depthMdMsl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="depthMdMsl"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="depthMdMsl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>Time:</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Weight:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudWeight = '') or (mudWeight = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudWeight"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudWeight/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp.:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudTestTemperature = '') or (mudTestTemperature = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudTestTemperature"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<!--
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp. In:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTemperatureBeforeEnter"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudTemperatureBeforeEnter/@uomSymbol"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp. Out:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTemperatureBeforeLeave"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudTemperatureBeforeLeave/@uomSymbol"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													-->
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 2 -->
										<fo:table-cell border-right="thin solid black" >
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(48)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(52)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																API FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(fluidLossApiVolume = '') or (fluidLossApiVolume = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																			
																		<xsl:value-of select="fluidLossApiVolume"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="fluidLossApiVolume/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Filter-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudApiCake = '') or (mudApiCake = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudApiCake"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudApiCake/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpFl = '') or (hthpFl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpFl"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpFl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpCake = '') or (hthpCake = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpCake"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpCake/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>						
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Temp:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">	
																<xsl:choose>
																	<xsl:when test="(hthpTestTemperature = '') or (hthpTestTemperature = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																		
																		<xsl:value-of select="hthpTestTemperature"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpTestTemperature/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Press:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpTestPressure = '') or (hthpTestPressure = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpTestPressure"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpTestPressure/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 3 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(60)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Cl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudChlorideIonConcentration = '') or (mudChlorideIonConcentration = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudChlorideIonConcentration"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudChlorideIonConcentration/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																KCl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudKclConcentration = '') or (mudKclConcentration = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudKclConcentration"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudKclConcentration/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Hard/Ca:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudCaConcentration = '') or (mudCaConcentration = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudCaConcentration"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudCaConcentration/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">										
															<fo:block>
																MBT:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">	
																<xsl:choose>
																	<xsl:when test="(mudMbtCec = '') or (mudMbtCec = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																		
																		<xsl:value-of select="mudMbtCec"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudMbtCec/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>		
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">										
															<fo:block>
																Pm:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">	
																<xsl:choose>
																	<xsl:when test="(mudPmVolume = '') or (mudPmVolume = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																			
																		<xsl:value-of select="mudPmVolume"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudPmVolume/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">										
															<fo:block>
																Pf:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudPf = '') or (mudPf = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																				
																		<xsl:value-of select="mudPf"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudPf/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
																				
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 4 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(54)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(46)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Solids:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">		
																<xsl:choose>
																	<xsl:when test="(mudDissolvedSolids = '') or (mudDissolvedSolids = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																	
																		<xsl:value-of select="mudDissolvedSolids"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																H2O:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudH2o = '') or (mudH2o = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudH2o"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudH2o/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Oil:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudOil = '') or (mudOil = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudOil"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudOil/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sand:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test = "sandTxt='0.001'">
																		tr
																	</xsl:when>
																	<xsl:when test="(sandTxt = '') or (sandTxt = 'null') ">
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="sandTxt"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="sandTxt/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
																<!--
																<xsl:value-of select="mudSand"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudSand/@uomSymbol"/>
																-->
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																pH:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudPh"/>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PHPA:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudPhpaDensity = '') or (mudPhpaDensity = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudPhpaDensity"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudPhpaDensity/@uomSymbol"/>		
																	</xsl:otherwise>
																</xsl:choose>												
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Mf:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudMfVolume = '') or (mudMfVolume = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudMfVolume"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudMfVolume/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										
										<!-- COLUMN 5 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Glycol:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(glycol = '') or (glycol = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="glycol"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="glycol/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Viscosity:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudFv = '') or (mudFv = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudFv"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudFv/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PV:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudPv = '') or (mudPv = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudPv"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudPv/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																YP:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudYp = '') or (mudYp = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudYp"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudYp/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10s:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:choose>
																<xsl:when test="string($mudGel10)!=''">
																	<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10s/@uomSymbol"/>
																	</fo:block>
																</xsl:when>
																<xsl:otherwise>
																	<fo:block text-align="right">												
																		<xsl:value-of select="mudGel10s"/>
																	</fo:block>
																</xsl:otherwise>
															</xsl:choose>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10m:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:choose>
																<xsl:when test="string($mudGel10)!=''">
																	<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10m/@uomSymbol"/>
																	</fo:block>
																</xsl:when>
																<xsl:otherwise>
																	<fo:block text-align="right">												
																		<xsl:value-of select="mudGel10m"/>
																	</fo:block>
																</xsl:otherwise>
															</xsl:choose>
														</fo:table-cell>
													</fo:table-row>
																						
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
									</fo:table-row>
									
									<!-- COMMENT -->
									<fo:table-row >
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" number-columns-spanned="4" border-left="thin solid black" border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black" background-color="#DDDDDD">
											<fo:block font-weight="bold" >
												Comment:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell number-rows-spanned="2" border-right="thin solid black" border-top="thin solid black">
											<xsl:variable name="mudID" select="mudPropertiesUid"/>
											<xsl:choose>
												<xsl:when test="count(MudRheology[mudPropertiesUid = $mudID]) = '0'">
													<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row background-color="#DDDDDD">
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	 border-bottom="thin solid black" border-right="thin solid black" >
																	<fo:block text-align="center" font-weight="bold" >
																		RPM
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"
																	border-bottom="thin solid black">
																	<fo:block text-align="center" font-weight="bold" >
																		Reading
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>															
														</fo:table-header>
														<fo:table-body>
															<fo:table-row>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	 border-bottom="thin solid black" number-columns-spanned="2">
																	<fo:block>
																		<fo:inline color="white">.</fo:inline>
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>	
														</fo:table-body>
													</fo:table>
												</xsl:when>
												<xsl:otherwise>
													<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row background-color="#DDDDDD">
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	 border-bottom="thin solid black" border-right="thin solid black" >
																	<fo:block text-align="center" font-weight="bold" >
																		RPM
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"
																	border-bottom="thin solid black">
																	<fo:block text-align="center" font-weight="bold" >
																		Reading
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>															
														</fo:table-header>
														<fo:table-body>
															<xsl:apply-templates select="MudRheology[mudPropertiesUid = $mudID]"/>
														</fo:table-body>
													</fo:table>
												</xsl:otherwise>
											</xsl:choose>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" number-columns-spanned="4" border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
											<fo:block linefeed-treatment="preserve">
												<xsl:value-of select="mudEngineerSummary"/>
											</fo:block>	
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>
								
							</fo:table>
							
						</fo:block>
						
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row keep-together="always">
					<fo:table-cell padding-bottom="2px" padding-top="2px">
					
						<fo:block keep-together="always">
						
							<fo:table width="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt">
								<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(21)"/>
								
								<!-- HEADER -->
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell padding="2px" background-color="#DDDDDD" display-align="center" number-columns-spanned="4"
											border-left="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
											<xsl:if test="mudType='ester'">
												<fo:block font-size="8pt" font-weight="bold">
													Ester Data
												</fo:block>	
											</xsl:if>
											<xsl:if test="mudType='foam'">
												<fo:block font-size="8pt" font-weight="bold">
													Foam Data
												</fo:block>	
											</xsl:if>
											<xsl:if test="mudType='air'">
												<fo:block font-size="8pt" font-weight="bold">
													Air Data
												</fo:block>	
											</xsl:if>
											<xsl:if test="mudType='oth'">
												<fo:block font-size="8pt" font-weight="bold">
													Fluid Data
												</fo:block>	
											</xsl:if>
											<xsl:if test="mudType=''">
												<fo:block font-size="8pt" font-weight="bold"/>	
											</xsl:if>
										</fo:table-cell>
										<fo:table-cell  padding="2px" background-color="#DDDDDD" display-align="center" border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black"> 
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell background-color="#DDDDDD" display-align="center">
															<fo:block font-size="7pt" font-weight="bold" text-align="left">
																Cost Today:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell background-color="#DDDDDD" display-align="center">
															<fo:block font-size="7pt" font-weight="bold" text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudCost = '') or (mudCost = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudCost/@uomSymbol"/>
																		<xsl:value-of select="mudCost"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								
								<!-- BODY -->
								<fo:table-body>
									<fo:table-row>
									
										<!-- COLUMN 1 -->
										<fo:table-cell border-left="thin solid black" border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(57)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(43)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Mud Desc.:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">																			
																<xsl:value-of select="mudMedium"/>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Check Depth:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(depthMdMsl = '') or (depthMdMsl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="depthMdMsl"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="depthMdMsl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>Time:</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Weight:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudWeight = '') or (mudWeight = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudWeight"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudWeight/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp.:
															</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudTestTemperature = '') or (mudTestTemperature = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudTestTemperature"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<!--
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp. In:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTemperatureBeforeEnter"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudTemperatureBeforeEnter/@uomSymbol"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Temp. Out:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudTemperatureBeforeLeave"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudTemperatureBeforeLeave/@uomSymbol"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													-->
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 2 -->
										<fo:table-cell border-right="thin solid black" >
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(48)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(52)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																API FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(fluidLossApiVolume = '') or (fluidLossApiVolume = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																			
																		<xsl:value-of select="fluidLossApiVolume"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="fluidLossApiVolume/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Filter-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudApiCake = '') or (mudApiCake = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudApiCake"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudApiCake/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-FL:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpFl = '') or (hthpFl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpFl"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpFl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Cake:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpCake = '') or (hthpCake = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpCake"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpCake/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>						
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Temp:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpTestTemperature = '') or (hthpTestTemperature = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																			
																		<xsl:value-of select="hthpTestTemperature"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpTestTemperature/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																HTHP-Press:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(hthpTestPressure = '') or (hthpTestPressure = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="hthpTestPressure"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="hthpTestPressure/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 3 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(60)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Cl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudChlorideIonConcentration = '') or (mudChlorideIonConcentration = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudChlorideIonConcentration"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudChlorideIonConcentration/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																KCl:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudKclConcentration = '') or (mudKclConcentration = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudKclConcentration"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudKclConcentration/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Hard/Ca:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudCaConcentration = '') or (mudCaConcentration = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudCaConcentration"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudCaConcentration/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">										
															<fo:block>
																MBT:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">		
																<xsl:choose>
																	<xsl:when test="(mudMbtCec = '') or (mudMbtCec = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																	
																		<xsl:value-of select="mudMbtCec"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudMbtCec/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>		
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">										
															<fo:block>
																Pm:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">		
																<xsl:choose>
																	<xsl:when test="(mudPmVolume = '') or (mudPmVolume = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																		
																		<xsl:value-of select="mudPmVolume"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudPmVolume/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">										
															<fo:block>
																Pf:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">	
																<xsl:choose>
																	<xsl:when test="(mudPf = '') or (mudPf = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																		
																		<xsl:value-of select="mudPf"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudPf/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
																				
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										<!-- COLUMN 4 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(54)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(46)"/>
												
												<fo:table-body>
												
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Solids:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">	
																<xsl:choose>
																	<xsl:when test="(mudDissolvedSolids = '') or (mudDissolvedSolids = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>																			
																		<xsl:value-of select="mudDissolvedSolids"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>																
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																H2O:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudDissolvedSolids = '') or (mudDissolvedSolids = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudH2o"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudH2o/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Sand:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test = "sandTxt='0.001'">
																		tr
																	</xsl:when>
																	<xsl:when test="(sandTxt = '') or (sandTxt = 'null') ">
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="sandTxt"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="sandTxt/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
																<!--
																<xsl:value-of select="mudSand"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="mudSand/@uomSymbol"/>
																-->
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																pH:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:value-of select="mudPh"/>													
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PHPA:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudPhpaDensity = '') or (mudPhpaDensity = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudPhpaDensity"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudPhpaDensity/@uomSymbol"/>	
																	</xsl:otherwise>
																</xsl:choose>													
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Mf:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudMfVolume = '') or (mudMfVolume = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudMfVolume"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudMfVolume/@uomSymbol"/>			
																	</xsl:otherwise>
																</xsl:choose>											
															</fo:block>																		
														</fo:table-cell>
													</fo:table-row>
													
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
										
										<!-- COLUMN 5 -->
										<fo:table-cell border-right="thin solid black">
											<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
												<fo:table-body>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Glycol:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(glycol = '') or (glycol = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="glycol"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="glycol/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Viscosity:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudFv = '') or (mudFv = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudFv"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudFv/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																PV:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudPv = '') or (mudPv = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudPv"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudPv/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																YP:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(mudYp = '') or (mudYp = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="mudYp"/>
																		<fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="mudYp/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>

													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10s:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:choose>
																<xsl:when test="string($mudGel10)!=''">
																	<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10s/@uomSymbol"/>
																	</fo:block>
																</xsl:when>
																<xsl:otherwise>
																	<fo:block text-align="right">												
																		<xsl:value-of select="mudGel10s"/>
																	</fo:block>
																</xsl:otherwise>
															</xsl:choose>
														</fo:table-cell>
													</fo:table-row>
													
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block>
																Gel 10m:
															</fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<xsl:variable name="mudGel10" select="mudGel10s"/>
															<xsl:choose>
																<xsl:when test="string($mudGel10)!=''">
																	<fo:block text-align="right">						
																		<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudGel10m/@uomSymbol"/>
																	</fo:block>
																</xsl:when>
																<xsl:otherwise>
																	<fo:block text-align="right">												
																		<xsl:value-of select="mudGel10m"/>
																	</fo:block>
																</xsl:otherwise>
															</xsl:choose>
														</fo:table-cell>
													</fo:table-row>
																						
												</fo:table-body>
												
											</fo:table>
										</fo:table-cell>
										
									</fo:table-row>
									
									<!-- COMMENT -->
									<fo:table-row >
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" number-columns-spanned="4" border-left="thin solid black" border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black" background-color="#DDDDDD">
											<fo:block font-weight="bold" >
												Comment:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell number-rows-spanned="2" border-right="thin solid black" border-top="thin solid black">
											<xsl:variable name="mudID" select="mudPropertiesUid"/>
											<xsl:choose>
												<xsl:when test="count(MudRheology[mudPropertiesUid = $mudID]) = '0'">
													<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row background-color="#DDDDDD">
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	 border-bottom="thin solid black" border-right="thin solid black" >
																	<fo:block text-align="center" font-weight="bold" >
																		RPM
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"
																	border-bottom="thin solid black">
																	<fo:block text-align="center" font-weight="bold" >
																		Reading
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>															
														</fo:table-header>
														<fo:table-body>
															<fo:table-row>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	 border-bottom="thin solid black" number-columns-spanned="2">
																	<fo:block>
																		<fo:inline color="white">.</fo:inline>
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>	
														</fo:table-body>
													</fo:table>
												</xsl:when>
												<xsl:otherwise>
													<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row background-color="#DDDDDD">
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" 
																	 border-bottom="thin solid black" border-right="thin solid black" >
																	<fo:block text-align="center" font-weight="bold" >
																		RPM
																	</fo:block>
																</fo:table-cell>
																<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"
																	border-bottom="thin solid black">
																	<fo:block text-align="center" font-weight="bold" >
																		Reading
																	</fo:block>
																</fo:table-cell>
															</fo:table-row>															
														</fo:table-header>
														<fo:table-body>
															<xsl:apply-templates select="MudRheology[mudPropertiesUid = $mudID]"/>
														</fo:table-body>
													</fo:table>
												</xsl:otherwise>
											</xsl:choose>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" number-columns-spanned="4" border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
											<fo:block linefeed-treatment="preserve">
												<xsl:value-of select="mudEngineerSummary"/>
											</fo:block>	
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>
								
							</fo:table>
							
						</fo:block>
						
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<!-- TEMPLATE: MudRheology -->
	<xsl:template match="MudRheology">
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
					<fo:table-cell border-bottom="thin solid black" border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px">
						<fo:block text-align="center">
							<xsl:choose>
								<xsl:when test="rpm/@rawNumber = '0'">
									Low Range
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="rpm"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-bottom="thin solid black" padding="1px" padding-left="2px" padding-right="2px">
						<fo:block text-align="center">
							<xsl:value-of select="rheologyReading"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell  border-bottom="thin solid black" border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px">
						<fo:block text-align="center">
							<xsl:choose>
								<xsl:when test="rpm/@rawNumber = '0'">
									Low Range
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="rpm"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  border-bottom="thin solid black" padding="1px" padding-left="2px" padding-right="2px">
						<fo:block text-align="center">
							<xsl:value-of select="rheologyReading"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>