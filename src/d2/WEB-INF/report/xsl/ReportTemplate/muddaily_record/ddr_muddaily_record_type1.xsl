<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- Mud Daily Begin -->
	<xsl:template match="modules/MudProperties">
		<xsl:apply-templates select="MudProperties"/>
		<fo:table-row>
			<fo:table-cell>
				<fo:table inline-progression-dimension="100%" table-layout="fixed" 
					font-size="10pt" wrap-option="wrap" border-left="0.5px solid black"
					border-right="0.5px solid black" border-bottom="0.5px solid black">
					<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell text-align="right" padding="0.05cm">
								<fo:block font-weight="bold">
									Cumulative Mud Cost:<fo:inline color="white">.</fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="left" padding="0.05cm">
								<fo:block font-weight="bold">
									<xsl:value-of select="dynaAttr/cumMudCost/@uomSymbol"/>
									<xsl:value-of select="dynaAttr/cumMudCost"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="MudProperties">	
	<xsl:choose>
		<xsl:when test="mudType='oil'">
		<fo:table-row keep-together="always">
			<fo:table-cell>
				<fo:table inline-progression-dimension="100%" table-layout="fixed" 
					font-size="8pt" wrap-option="wrap" margin-top="2pt" border-left="0.5px solid black"
					border-right="0.5px solid black" border-bottom="0.5px solid black">
					<fo:table-column column-number="1"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="2"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="3"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="4"
						column-width="proportional-column-width(25)"/>
					<fo:table-header>
						<fo:table-row font-size="8pt">
							<fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
								number-columns-spanned="2" background-color="rgb(224,224,224)" border-top="0.5px solid black"
								border-left="0.1px solid black">
								<fo:block font-size="10pt" font-weight="bold">OBM
								Data</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
								number-columns-spanned="2" background-color="rgb(224,224,224)" border-top="0.5px solid black"
								border-right="0.1px solid black">
								<fo:block font-size="10pt" font-weight="bold">Cost Today
									 <xsl:value-of select="mudCost/@uomSymbol"/> <xsl:value-of select="mudCost"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<fo:table-row>
							<!-- 1st Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Mud Desc.: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudMedium"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Oil Type: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="oiltype"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Sample From: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "string(sampleTakenFrom/@lookupLabel)=''">
																<xsl:value-of select="sampleTakenFrom"/>										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="sampleTakenFrom/@lookupLabel"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Time: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Weight: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudWeight"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudWeight/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Flow Line Temp.: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="flowLineTemperature"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="flowLineTemperature/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Temperature: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudTestTemperature"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<!-- 										
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Viscosity: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudFv"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudFv/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											-->	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> PV: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudPv"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudPv/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> YP: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudYp"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudYp/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Gels 10s: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudGel10s/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Gels 10m: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">																									
														<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudGel10m/@uomSymbol"/>													
													</fo:block>
												</fo:table-cell>
											</fo:table-row>																																																										
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<!-- 2nd Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Excess Lime: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudExcessLimeConcentration"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudExcessLimeConcentration/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> O/W Ratio: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="owratio"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Electric Stability: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="electricstab"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="electricstab/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<!-- 
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> CaCl Mud: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="cacl_mud"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="cacl_mud/@uomSymbol"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											-->
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> CaCl2 WPS: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="cacl_wp"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="cacl_wp/@uomSymbol"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm" number-columns-spanned="2">
													<fo:table width="100%" table-layout="fixed" border="0.25px solid black" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm" number-columns-spanned="2"
																	border-bottom="0.25px solid black" background-color="rgb(224,224,224)">
																	<fo:block text-align="center">Low Range Rheology:</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</fo:table-header>
														<fo:table-body>
															<fo:table-row>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
																	<fo:block text-align="center">RPM </fo:block>
																</fo:table-cell>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
																	<fo:block text-align="center">Reading </fo:block>
																</fo:table-cell>
															</fo:table-row>
															<xsl:variable name="mudID" select="mudPropertiesUid"/>													
															<xsl:apply-templates select="MudRheology[mudPropertiesUid = $mudID]"/>
														</fo:table-body>
													</fo:table>																								
												</fo:table-cell>
											</fo:table-row>																																																																															
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>																		
							<!-- 3rd Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> HTHP-Temp: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="hthpTestTemperature"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="hthpTestTemperature/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> HTHP-Press: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="hthpTestPressure"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="hthpTestPressure/@uomSymbol"/>													
														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> HTHP-FL: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="hthpFl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="hthpFl/@uomSymbol"/>
														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> HTHP-Cake: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="hthpCake"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="hthpCake/@uomSymbol"/>
														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>										
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<!-- 4th Column -->
							<fo:table-cell>
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Solids: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudDissolvedSolids"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> LGS: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="lgs"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="lgs/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Oil: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="mudOil"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudOil/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Sand: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "sand='0.001'">
																tr										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="sand"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="sand/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> H2O: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="mudH2o"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudH2o/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<!-- 
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Oil On Cut: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="oilcuttings"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="oilcuttings/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											-->																																																																																																					
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<!-- 2nd row -->
						<fo:table-row>
							<fo:table-cell border-top="0.25px solid black" padding="0.05cm" number-columns-spanned="4">
								<fo:block text-align="left">
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(90)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Comment: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="left" linefeed-treatment="preserve">
														<xsl:value-of select="mudEngineerSummary"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>										
							</fo:table-cell>							
						</fo:table-row>																	
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
		</xsl:when>
		
		<xsl:when test="mudType='sbm'">	
		<fo:table-row keep-together="always">
			<fo:table-cell>		
				<fo:table inline-progression-dimension="100%" table-layout="fixed" 
					font-size="8pt" wrap-option="wrap" margin-top="2pt" border-left="0.5px solid black"
					border-right="0.5px solid black" border-bottom="0.5px solid black">
					<fo:table-column column-number="1"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="2"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="3"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="4"
						column-width="proportional-column-width(25)"/>
					<fo:table-header>
						<fo:table-row font-size="8pt">
							<fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
								number-columns-spanned="2" background-color="rgb(224,224,224)" border-top="0.5px solid black"
								border-left="0.1px solid black">
								<fo:block font-size="10pt" font-weight="bold">SBM
								Data</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
								number-columns-spanned="2" background-color="rgb(224,224,224)" border-top="0.5px solid black"
								border-right="0.1px solid black">
								<fo:block font-size="10pt" font-weight="bold">Cost Today
									 <xsl:value-of select="mudCost/@uomSymbol"/> <xsl:value-of select="mudCost"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<fo:table-row>
							<!-- 1st Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Mud Desc.: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudMedium"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Oil Type: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="oiltype"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Sample From: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "string(sampleTakenFrom/@lookupLabel)=''">
																<xsl:value-of select="sampleTakenFrom"/>										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="sampleTakenFrom/@lookupLabel"/>
															</xsl:otherwise>
														</xsl:choose>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Sample-Depth: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="depthMdMsl/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Time: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Weight: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudWeight"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudWeight/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Flow Line Temp.: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="flowLineTemperature"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="flowLineTemperature/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Temperature: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudTestTemperature"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<!-- 
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Funnel Viscosity: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudFv"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudFv/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											-->	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> PV: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudPv"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudPv/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> YP: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudYp"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudYp/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Gels 10s: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudGel10s/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>																																																										
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Gels 10m: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudGel10m/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<!-- 2nd Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> HTHP-Temp: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="hthpTestTemperature"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="hthpTestTemperature/@uomSymbol"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> HTHP-Press: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="hthpTestPressure"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="hthpTestPressure/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> HTHP-FL: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="hthpFl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="hthpFl/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> HTHP-Cake: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="hthpCake"/>
														<xsl:value-of select="hthpCake/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm" number-columns-spanned="2">
													<fo:table width="100%" table-layout="fixed" border="0.25px solid black" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm" number-columns-spanned="2"
																	border-bottom="0.25px solid black" background-color="rgb(224,224,224)">
																	<fo:block text-align="center">Low Range Rheology:</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</fo:table-header>
														<fo:table-body>
															<fo:table-row>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
																	<fo:block text-align="center">RPM </fo:block>
																</fo:table-cell>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
																	<fo:block text-align="center">Reading </fo:block>
																</fo:table-cell>
															</fo:table-row>
															<xsl:variable name="mudID" select="mudPropertiesUid"/>													
															<xsl:apply-templates select="MudRheology[mudPropertiesUid = $mudID]"/>
														</fo:table-body>
													</fo:table>																								
												</fo:table-cell>
											</fo:table-row>																																																																																								
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>																		
							<!-- 3rd Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Excess Lime: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudExcessLimeConcentration"/>
														<xsl:value-of select="mudExcessLimeConcentration/@uomSymbol"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Salinity: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="h2osalinity"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="h2osalinity/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Electric Stability: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="electricstab"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="electricstab/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> CaCl2 WP: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="cacl_wp"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="cacl_wp/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> CaCl Mud: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="cacl_mud"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="cacl_mud/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> PPA Temp: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="ppatemp"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="ppatemp/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> PPA FL: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="ppafl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="ppafl/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> PPA Pres: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="ppapress"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="ppapress/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<!-- 4th Column -->
							<fo:table-cell>
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Solids: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudDissolvedSolids"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> LGS: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="lgs"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="lgs/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Oil: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="mudOil"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudOil/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Sand: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "sand='0.001'">
																tr										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="sand"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="sand/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> H2O: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="mudH2o"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudH2o/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Oil On Cut: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="oilcuttings"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="oilcuttings/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>																																																																																																				
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>	
						<!-- 2nd row -->
						<fo:table-row>
							<fo:table-cell border-top="0.25px solid black" padding="0.05cm" number-columns-spanned="4">
								<fo:block text-align="left">
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(90)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Comment: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="left" linefeed-treatment="preserve">
														<xsl:value-of select="mudEngineerSummary"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>										
							</fo:table-cell>							
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
		</xsl:when>
							
		<xsl:when test="mudType='h2o'">	
		<fo:table-row keep-together="always">
			<fo:table-cell>											
				<fo:table inline-progression-dimension="100%" table-layout="fixed" 
					font-size="8pt" wrap-option="wrap" margin-top="2pt" border-left="0.5px solid black"
					border-right="0.5px solid black" border-bottom="0.5px solid black">
					<fo:table-column column-number="1"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="2"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="3"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="4"
						column-width="proportional-column-width(25)"/>
					<fo:table-header>
						<fo:table-row font-size="8pt">
							<fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
								number-columns-spanned="2" background-color="rgb(224,224,224)" border-top="0.5px solid black"
								border-left="0.1px solid black">
								<fo:block font-size="10pt" font-weight="bold">WBM
								Data</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
								number-columns-spanned="2" background-color="rgb(224,224,224)" border-top="0.5px solid black"
								border-right="0.1px solid black">
								<fo:block font-size="10pt" font-weight="bold">Cost Today
									 <xsl:value-of select="mudCost/@uomSymbol"/> <xsl:value-of select="mudCost"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<fo:table-row>
							<!-- 1st Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Mud Desc.: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudMedium"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Sample From: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "string(sampleTakenFrom/@lookupLabel)=''">
																<xsl:value-of select="sampleTakenFrom"/>										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="sampleTakenFrom/@lookupLabel"/>
															</xsl:otherwise>
														</xsl:choose>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Sample Depth: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="depthMdMsl/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>																			
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Time: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Weight: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudWeight"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudWeight/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Flow Line Temp.: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="flowLineTemperature"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="flowLineTemperature/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Temperature: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudTestTemperature"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<!-- 
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Viscosity: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudFv"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudFv/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											-->
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> PV: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudPv"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudPv/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> YP: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudYp"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudYp/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Gels 10s: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">												
														<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudGel10s/@uomSymbol"/>															
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Gels 10m: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudGel10m/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>																																																													
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<!-- 2nd Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> API FL: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudApiFl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudApiFl/@uomSymbol"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Filter Cake: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="mudApiCake"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudApiCake/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm" number-columns-spanned="2">
													<fo:table width="100%" table-layout="fixed" border="0.25px solid black" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm" number-columns-spanned="2"
																	border-bottom="0.25px solid black" background-color="rgb(224,224,224)">
																	<fo:block text-align="center">Low Range Rheology:</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</fo:table-header>
														<fo:table-body>
															<fo:table-row>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
																	<fo:block text-align="center">RPM </fo:block>
																</fo:table-cell>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
																	<fo:block text-align="center">Reading </fo:block>
																</fo:table-cell>
															</fo:table-row>
															<xsl:variable name="mudID" select="mudPropertiesUid"/>													
															<xsl:apply-templates select="MudRheology[mudPropertiesUid = $mudID]"/>
														</fo:table-body>
													</fo:table>																								
												</fo:table-cell>
											</fo:table-row>																																																																				
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>																		
							<!-- 3rd Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Cl: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudChlorideIonConcentration"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudChlorideIonConcentration/@uomSymbol"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Total Hard: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="total_hardness"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="total_hardness/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>									
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Ca Hard: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="mudCaConcentration"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudCaConcentration/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> MBT: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="mudMbt"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudMbt/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> PM: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="mudPm"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudPm/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> PF/MF: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="mudPf"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudPf/@uomSymbol"/> / <xsl:value-of select="mudMf"/>
														<fo:inline color="white">.</fo:inline><xsl:value-of select="mudMf/@uomSymbol"/> 													
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> pH: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="mudPh"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudPh/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<!-- 4th Column -->
							<fo:table-cell>
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Solids: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudDissolvedSolids"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> H2O: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="mudH2o"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudH2o/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Oil: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="mudOil"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudOil/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Sand: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "sand='0.001'">
																tr										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="sand"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="sand/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>																																	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> KCL: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="kclLossConcentration"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="kclLossConcentration/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> LGS: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="lgs"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="lgs/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> HGS: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="hgs"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="hgs/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<!-- 2nd row -->
						<fo:table-row>
							<fo:table-cell border-top="0.25px solid black" padding="0.05cm" number-columns-spanned="4">
								<fo:block text-align="left">
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(90)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Comment: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="left" linefeed-treatment="preserve">
														<xsl:value-of select="mudEngineerSummary"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>										
							</fo:table-cell>							
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
		</xsl:when>
		
		<xsl:otherwise>
		<fo:table-row keep-together="always">
			<fo:table-cell>
				<fo:table inline-progression-dimension="100%" table-layout="fixed" 
					font-size="8pt" wrap-option="wrap" margin-top="2pt" border-left="0.5px solid black"
					border-right="0.5px solid black" border-bottom="0.5px solid black">
					<fo:table-column column-number="1"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="2"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="3"
						column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="4"
						column-width="proportional-column-width(25)"/>
					<fo:table-header>
						<fo:table-row font-size="8pt">
							<fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
								number-columns-spanned="2" background-color="rgb(224,224,224)" border-top="0.5px solid black"
								border-left="0.1px solid black">
								<xsl:if test="mudType='ester'">
									<fo:block font-size="10pt" font-weight="bold">Ester Data</fo:block>	
								</xsl:if>
								<xsl:if test="mudType='foam'">
									<fo:block font-size="10pt" font-weight="bold">Foam Data</fo:block>	
								</xsl:if>
								<xsl:if test="mudType='air'">
									<fo:block font-size="10pt" font-weight="bold">Air Data</fo:block>	
								</xsl:if>
								<xsl:if test="mudType='oth'">
									<fo:block font-size="10pt" font-weight="bold">Other Data</fo:block>	
								</xsl:if>
								<xsl:if test="mudType=''">
									<fo:block font-size="10pt" font-weight="bold"></fo:block>	
								</xsl:if>
							</fo:table-cell>
							<fo:table-cell padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
								number-columns-spanned="2" background-color="rgb(224,224,224)" border-top="0.5px solid black"
								border-right="0.1px solid black">
								<fo:block font-size="10pt" font-weight="bold">Cost Today
									 <xsl:value-of select="mudCost/@uomSymbol"/> <xsl:value-of select="mudCost"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<fo:table-row>
							<!-- 1st Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Mud Desc.: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudMedium"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Sample From: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "string(sampleTakenFrom/@lookupLabel)=''">
																<xsl:value-of select="sampleTakenFrom"/>										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="sampleTakenFrom/@lookupLabel"/>
															</xsl:otherwise>
														</xsl:choose>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Sample Depth: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="depthMdMsl/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>																			
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Time: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Weight: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudWeight"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudWeight/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Flow Line Temp.: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="flowLineTemperature"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="flowLineTemperature/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Temperature: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudTestTemperature"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudTestTemperature/@uomSymbol"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<!-- 										
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Viscosity: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudFv"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudFv/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											-->
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> PV: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudPv"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudPv/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> YP: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudYp"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudYp/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Gels 10s: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">												
														<xsl:value-of select="mudGel10s"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudGel10s/@uomSymbol"/>															
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Gels 10m: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">			
													<fo:block text-align="right">
														<xsl:value-of select="mudGel10m"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudGel10m/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>																																																													
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<!-- 2nd Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> API FL: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudApiFl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudApiFl/@uomSymbol"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Filter Cake: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="mudApiCake"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudApiCake/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm" number-columns-spanned="2">
													<fo:table width="100%" table-layout="fixed" border="0.25px solid black" wrap-option="wrap">
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-column column-width="proportional-column-width(50)"/>
														<fo:table-header>
															<fo:table-row>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm" number-columns-spanned="2"
																	border-bottom="0.25px solid black" background-color="rgb(224,224,224)">
																	<fo:block text-align="center">Low Range Rheology:</fo:block>
																</fo:table-cell>
															</fo:table-row>
														</fo:table-header>
														<fo:table-body>
															<fo:table-row>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
																	<fo:block text-align="center">RPM </fo:block>
																</fo:table-cell>
																<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
																	<fo:block text-align="center">Reading </fo:block>
																</fo:table-cell>
															</fo:table-row>
															<xsl:variable name="mudID" select="mudPropertiesUid"/>													
															<xsl:apply-templates select="MudRheology[mudPropertiesUid = $mudID]"/>
														</fo:table-body>
													</fo:table>																								
												</fo:table-cell>
											</fo:table-row>																																																																				
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>																		
							<!-- 3rd Column -->
							<fo:table-cell border-right="0.25px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Cl: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudChlorideIonConcentration"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudChlorideIonConcentration/@uomSymbol"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>																	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Ca Hard: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="mudCaConcentration"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudCaConcentration/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> MBT: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="mudMbt"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudMbt/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> PM: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="mudPm"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudPm/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> PF/MF: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="mudPf"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudPf/@uomSymbol"/> / <xsl:value-of select="mudMf"/>
														<fo:inline color="white">.</fo:inline><xsl:value-of select="mudMf/@uomSymbol"/> 													
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> pH: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="mudPh"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudPh/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
							<!-- 4th Column -->
							<fo:table-cell>
								<fo:block>
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Solids: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">																			
														<xsl:value-of select="mudDissolvedSolids"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudDissolvedSolids/@uomSymbol"/>														
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> H2O: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="mudH2o"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudH2o/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Oil: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="right">
														<xsl:value-of select="mudOil"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="mudOil/@uomSymbol"/>														
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> Sand: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "sand='0.001'">
																tr										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="sand"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="sand/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>																																	
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> KCL: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="kclLossConcentration"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="kclLossConcentration/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<!--
											<fo:table-row>
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="left"> LGS: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">
													<fo:block text-align="right">
														<xsl:value-of select="lgs"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="lgs/@uomSymbol"/>														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											-->										
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<!-- 2nd row -->
						<fo:table-row>
							<fo:table-cell border-top="0.25px solid black" padding="0.05cm" number-columns-spanned="4">
								<fo:block text-align="left">
									<fo:table width="100%" table-layout="fixed" font-size="8pt" 
										wrap-option="wrap" space-after="2pt">
										<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(90)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="0.05cm">																
													<fo:block text-align="left"> Comment: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="0.05cm">	
													<fo:block text-align="left" linefeed-treatment="preserve">
														<xsl:value-of select="mudEngineerSummary"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>										
							</fo:table-cell>							
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
		</xsl:otherwise>
	</xsl:choose>
	</xsl:template>
	
	<!-- rheology section BEGIN -->	
	<xsl:template match="MudRheology">
		<fo:table-row>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="rpm"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center"><xsl:value-of select="rheologyReading"/></fo:block>
			</fo:table-cell>
		</fo:table-row>	
	</xsl:template>
	<!-- rheology section END -->	
	<!-- Mud Daily End -->

</xsl:stylesheet>