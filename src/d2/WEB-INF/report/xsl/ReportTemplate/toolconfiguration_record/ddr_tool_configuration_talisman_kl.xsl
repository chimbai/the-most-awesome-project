<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	<xsl:import href="tool_configuration_talisman_kl.xsl" />
		
	<xsl:template match="modules/ToolConfiguration">	
		<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-before="2pt" border="0.5px solid black">
				<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(55)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(20)"/>
				
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							Run #
						</fo:block>
					</fo:table-cell>					
				
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							Tools Configurations
						</fo:block>
					</fo:table-cell>					
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							Top Depth MD (<xsl:value-of select="/root/datum/currentDatumCodeWithUOM"/>)
						</fo:block>
					</fo:table-cell>					
					<fo:table-cell padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							Bottom Depth MD (<xsl:value-of select="/root/datum/currentDatumCodeWithUOM"/>)
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>				
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="ToolConfiguration"/>
			</fo:table-body>
		</fo:table>	
	</xsl:template>
		
</xsl:stylesheet>