<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	
	
	<xsl:template match="ToolConfiguration">
		<fo:table-row keep-together="always">
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm">
				<fo:block text-align="center">
					<xsl:value-of select="runNumber"/>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm">
				<fo:block text-align="left">
					<xsl:value-of select="toolConfiguration"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" padding-right="0.2cm">
				<fo:block text-align="center">
					<xsl:value-of select="topDepthMdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.07cm">
				<fo:block text-align="center">
					<xsl:value-of select="bottomDepthMdMsl"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>	
	</xsl:template>
	
</xsl:stylesheet>