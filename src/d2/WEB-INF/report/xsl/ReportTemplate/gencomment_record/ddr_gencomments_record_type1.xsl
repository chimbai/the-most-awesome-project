<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:import href="./gencomments_header.xsl" />
	<xsl:import href="./gencomments_record.xsl" />
	
	<!-- gencomments section BEGIN -->	
	<xsl:template match="modules/GeneralComment">	
		<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt"
		 	border-left="0.5px solid black" border-right="0.5px solid black" border-bottom="0.5px solid black">
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(80)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="2" border-bottom="0.25px solid black" padding-bottom="-0.05cm" padding="0.05cm"
						background-color="rgb(224,224,224)" border-top="0.5px solid black"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block font-size="10pt" font-weight="bold">
							General Comments 00:00 to 24:00 Hrs on <xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Daily/Daily/dayDate/@epochMS,'dd MMM yyyy')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>			
				<xsl:call-template name="gencomment_header"/>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates name="GeneralComment"/>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- gencomments section END -->

</xsl:stylesheet>	