<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/ReportDailyDGR/ReportDaily">	
		<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" border ="0.75px solid black"  margin-top="2pt" wrap-option="wrap">
			<fo:table-column column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="2" padding="1px"
						background-color="rgb(37,87,147)" border-left="0.25px solid black"  border-top="0.25px solid black" border-bottom="0.25px solid black"
						padding-left="3px" padding-right="3px">
						<fo:block font-size="10pt" font-weight="bold" text-align="center" color="white">
							<fo:inline> General Comments </fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="2" padding="1px"
						background-color="rgb(149, 179, 215)" border-left="0.25px solid black"  border-top="0.25px solid black" border-bottom="0.25px solid black"
						padding-left="3px" padding-right="3px">
						<fo:block font-size="8pt" font-weight="bold" text-align="center">
							<fo:inline> Comments </fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
			<fo:table-row>
				<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
					<fo:block linefeed-treatment="preserve">
						<xsl:value-of select="generalComment"/>
					</fo:block>				
				</fo:table-cell>
			</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- gencomments section END -->

</xsl:stylesheet>