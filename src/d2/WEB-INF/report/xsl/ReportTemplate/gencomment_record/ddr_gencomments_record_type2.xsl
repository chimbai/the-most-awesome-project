<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/GeneralComment">	
		<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt" border-bottom="thin solid black"
		 	space-after="6pt" space-before="6pt">
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(80)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="2"	padding="2px"
						background-color="#DDDDDD" border="thin solid black">
						<fo:block font-size="8pt" font-weight="bold">
						<fo:inline> General Comments for Period 0000 Hrs to 2400 Hrs on <xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Daily/Daily/dayDate/@epochMS,'dd MMM yyyy')"/>
						</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
				<fo:table-row>
					<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black">
						<fo:block text-align="center">Category</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-bottom="thin solid black" border-right="thin solid black">
						<fo:block text-align="center">Comments</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates name="GeneralComment">
					<xsl:sort select="category" order="ascending"/>
				</xsl:apply-templates>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="GeneralComment">
		<xsl:variable name="selected_category" select="category"/>
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
					<fo:table-cell border-left="thin solid black" border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px">
						<fo:block>
							<xsl:value-of select="$selected_category/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="comments"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
			
				<fo:table-row background-color="#EEEEEE">
					
					<fo:table-cell border-left="thin solid black" border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px">
						<fo:block>
							<xsl:value-of select="$selected_category/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="comments"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>	
	<!-- gencomments section END -->

</xsl:stylesheet>	