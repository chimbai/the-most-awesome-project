<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- General Comment records START -->
	<xsl:template match="GeneralComment">
		<xsl:variable name="selected_category" select="category"/>
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block>
					<xsl:value-of select="$selected_category/@lookupLabel"/>
				</fo:block>
				<fo:block><fo:inline color="white">.</fo:inline></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block linefeed-treatment="preserve">
					<xsl:value-of select="comments"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- General Comment records END -->

</xsl:stylesheet>