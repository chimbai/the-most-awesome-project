<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- General Comment header START -->
	<xsl:template name="gencomment_header">		
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
				<fo:block text-align="center">Category</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
				<fo:block text-align="center">Comments</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- General Comment header END -->
	
</xsl:stylesheet>	