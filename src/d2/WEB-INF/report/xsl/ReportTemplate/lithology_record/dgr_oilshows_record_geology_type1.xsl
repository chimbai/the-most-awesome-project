<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- casing section BEGIN -->
	<xsl:template match="modules/LithologyShows" mode="OilShows">
	
		<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" wrap-option="wrap" border="0.75px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(4)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(4)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(5)"/>
			
			<fo:table-column column-number="8" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="10" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="11" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="12" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="13" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="14" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="15" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="16" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="17" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="18" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="19" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="20" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="21" column-width="proportional-column-width(6)"/>
			
			<fo:table-header>
				<fo:table-row color="white" background-color="rgb(37,87,147)">
					<fo:table-cell number-columns-spanned="21" padding="2px" border-bottom="0.75px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="10pt">
							Oil Shows
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
			
									<fo:table-row font-weight="bold" background-color="rgb(149, 179, 215)">
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black" number-columns-spanned="7">
											<fo:block text-align="center">
												Basic Details
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black" number-columns-spanned="14">
											<fo:block text-align="center">
												Oil Stain Details
											</fo:block>
										</fo:table-cell>
			 						</fo:table-row>
									
									<fo:table-row font-size="5pt" font-weight="bold" text-align="center" background-color="rgb(149, 179, 215)">
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Depth From (ft)
											</fo:block>
											
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Depth To  (ft)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Thickness (m)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Formation
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Lithology
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												MW   Over Balance
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Increase in ROP into Show Interval
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Visual Porosity
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Show Lithology With Fluor
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												White Light: Amount of Stain
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												White Light: Stain Color
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												White Light: Cut Color
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												White Light: Amount of Residue
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Fluor Colour
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Fluor Distrib
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Fluor Intensity
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Dominant Cut Fluor
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												HC Residue Spot Dish
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												HC Odour
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Oil Scum in Mud
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Show Rate (Category)
											</fo:block>
										</fo:table-cell> 
									</fo:table-row>						
			</fo:table-header>
			<fo:table-body>				
				<xsl:apply-templates select="LithologyShows"/>
			</fo:table-body>
		</fo:table>
												
	</xsl:template>

	<xsl:template match="LithologyShows" >
	
		<fo:table-row text-align="center" font-size="6pt">
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/fromDepthMdMsl)= 'NULL' or string(/../../Geolshows/Geolshows/fromDepthMdMsl)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/fromDepthMdMsl"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/toDepthMdMsl)= 'NULL' or string(/../../Geolshows/Geolshows/toDepthMdMsl)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/toDepthMdMsl"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/thickness)= 'NULL' or string(/../../Geolshows/Geolshows/thickness)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/thickness"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/formationName)= 'NULL' or string(/../../Geolshows/Geolshows/formationName)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/formationName"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/lithology)= 'NULL' or string(/../../Geolshows/Geolshows/lithology)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/lithology"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/mudWeightOverbalanceRate/@lookupLabel)= 'NULL' or string(/../../Geolshows/Geolshows/mudWeightOverbalanceRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/mudWeightOverbalanceRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/ropIntoShowIncreaseRate/@lookupLabel)= 'NULL' or string(/../../Geolshows/Geolshows/ropIntoShowIncreaseRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/ropIntoShowIncreaseRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/visualPorosityRate/@lookupLabel)= 'NULL' or string(/../../Geolshows/Geolshows/visualPorosityRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/visualPorosityRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/fluoInShowLithologyRate/@lookupLabel)= 'NULL' or string(/../../Geolshows/Geolshows/fluoInShowLithologyRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/fluoInShowLithologyRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/whitelightStain)= 'NULL' or string(/../../Geolshows/Geolshows/whitelightStain)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/whitelightStain"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/showColour)= 'NULL' or string(/../../Geolshows/Geolshows/showColour)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/showColour"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>	
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/whitelightCutColor)= 'NULL' or string(/../../Geolshows/Geolshows/whitelightCutColor)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/whitelightCutColor"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/whitelightResidue)= 'NULL' or string(/../../Geolshows/Geolshows/whitelightResidue)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/whitelightResidue"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/fluorColourRate/@lookupLabel)= 'NULL' or string(/../../Geolshows/Geolshows/fluorColourRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/fluorColourRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
		
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/fluorDistributionRate/@lookupLabel)= 'NULL' or string(/../../Geolshows/Geolshows/fluorDistributionRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/fluorDistributionRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/fluorIntensityRate/@lookupLabel)= 'NULL' or string(/../../Geolshows/Geolshows/fluorIntensityRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/fluorIntensityRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/dominantCutTypeRate/@lookupLabel)= 'NULL' or string(/../../Geolshows/Geolshows/dominantCutTypeRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/dominantCutTypeRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/hcResidueRate/@lookupLabel)= 'NULL' or string(/../../Geolshows/Geolshows/hcResidueRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/hcResidueRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/showevalOdour)= 'NULL' or string(/../../Geolshows/Geolshows/showevalOdour)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/showevalOdour"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/showeval)= 'NULL' or string(/../../Geolshows/Geolshows/showeval)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/showeval"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px">
				<xsl:choose>													
					<xsl:when test = "string(../../Geolshows/Geolshows/rating)= ''">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="../../Geolshows/Geolshows/rating"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
		</fo:table-row>

	</xsl:template>

</xsl:stylesheet>