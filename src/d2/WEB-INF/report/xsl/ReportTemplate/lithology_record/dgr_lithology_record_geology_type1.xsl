<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- casing section BEGIN -->
	<xsl:template match="modules/LithologyShows">
	
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap"
			border="0.5px solid black" space-after="6pt" space-before="6pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(40)"/>
			
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="4" padding="2px" border="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="center" font-weight="bold" font-size="10pt">
							Lithology Summary
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
				<fo:table-row>
					<fo:table-cell padding="2px">
						<fo:block>
							<fo:table table-layout="fixed">
								<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell number-columns-spanned="2">
											<fo:block text-align="center">
												Interval MDBRT (m)
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell>
											<fo:block text-align="center">
												From
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block text-align="center">
												To
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px">
						<fo:block text-align="center">Lithology</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px">
						<fo:block text-align="center">%</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px">
						<fo:block text-align="center">Description</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>				
				<xsl:apply-templates select="LithologyShows"/>
			</fo:table-body>
		</fo:table>
												
	</xsl:template>

	<xsl:template match="LithologyShows">
		<fo:table-row>
			<fo:table-cell border-right="0.5px solid black" padding-before="2px"
				padding-start="2px" padding-end="2px" border-top="0.5px solid black">
				<fo:block text-align="center">
					<xsl:choose>
	                	<xsl:when test="normalize-space(isDominant) = 'true'">
	                    	<xsl:value-of select="topMdMsl"/> 
	                    	<fo:inline> - </fo:inline> 
	                    	<xsl:value-of select="bottomMdMsl"/>
	                  	</xsl:when>
	                  	<xsl:otherwise></xsl:otherwise>
	            	</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding-before="2px"
				padding-start="2px" padding-end="2px" border-top="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="lithologyType/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding-before="2px"
				padding-start="2px" padding-end="2px" border-top="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="percentageRange"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="2px" padding-start="2px" padding-end="2px" 
				border-top="0.5px solid black">
				<fo:block text-align="left">
					<xsl:choose>
	                	<xsl:when test="normalize-space(isDominant) = 'true'">
	                    	<fo:inline font-weight="bold">
	                    		<xsl:value-of select="lithologyDom"/>
		                    </fo:inline>
		                   	<fo:block>
		                   		<xsl:value-of select="descr"/>
		                   	</fo:block>
	                  	</xsl:when>
	                  	<xsl:otherwise>
	                  		<xsl:value-of select="descr"/>
	                  	</xsl:otherwise>
	            	</xsl:choose>
				</fo:block>
			</fo:table-cell>
			
		</fo:table-row>
	</xsl:template>
	<!-- lithology section END -->

</xsl:stylesheet>