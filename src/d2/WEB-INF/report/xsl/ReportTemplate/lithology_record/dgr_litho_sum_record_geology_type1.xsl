<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/LithologyShows"  mode="Lithology_sum">	
		<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" border-right="0.75px solid black" border-left="0.75px solid black" border-top="0.75px solid black" margin-top="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
	       
	        			
			<fo:table-header>
				<fo:table-row font-size="10pt">
			    	<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" font-weight="bold" text-align="center" background-color="rgb(37,87,147)">
			        	<fo:block color="white">Lithology Summary</fo:block>
					</fo:table-cell> 
			    </fo:table-row>
				
			</fo:table-header>
			<fo:table-body>
			<fo:table-row >
				<xsl:apply-templates select="/root/modules/LithologyShows" mode="Lithology_sum_1">
				</xsl:apply-templates>
			</fo:table-row>	
			
			</fo:table-body>
		</fo:table>
												
	</xsl:template>

	 
	

</xsl:stylesheet>







