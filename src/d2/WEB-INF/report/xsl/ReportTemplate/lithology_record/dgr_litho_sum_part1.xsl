<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
		
	<xsl:template match="modules/LithologyShows"  mode="Lithology_sum_1">		              
		<fo:table-cell>
			<fo:block>
				<fo:table width="100%" table-layout="fixed" font-size="6" border-bottom="0.75px solid black">
					<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(70)"/>
						<fo:table-body>
							<fo:table-row font-size="8pt" font-weight="bold" text-align="center" background-color="rgb(149, 179, 215)">
								<fo:table-cell border-bottom="0.75px solid black" border-right="0.75px solid black">
									<fo:block text-align="center" padding="2px">
										Interval
										(<xsl:value-of select="LithologyShows/topMdMsl/@uomSymbol" />)
										MD<xsl:value-of select="/root/datum/currentDatumType" />
									</fo:block>
								</fo:table-cell>
								<fo:table-cell border-bottom="0.75px solid black" border-right="0.75px solid black">
									<fo:block text-align="center" padding="2px">
										ROP
										(<xsl:value-of select="LithologyShows/ropavg/@uomSymbol" />)
									</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block text-align="center" padding="2px">
										Lithology Comments
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
	
							<xsl:apply-templates select="LithologyShows" mode="Lithology_sum_1"></xsl:apply-templates>
							
						</fo:table-body>															
				</fo:table>
	                      			
	         </fo:block>
	   </fo:table-cell>					
	</xsl:template>
	
	<xsl:template match="LithologyShows" mode="Lithology_sum_1">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
			
				<fo:table-row font-size="8pt">
					<fo:table-cell border-right="0.75px solid black" padding-left="2px">
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							<xsl:value-of select="topMdMsl" />
							-
							<xsl:value-of select="bottomMdMsl" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px solid black" padding-left="2px">
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							Min :<xsl:value-of select="ropmin" />
						</fo:block>
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							Avg :<xsl:value-of select="ropavg" />
						</fo:block>
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							Max :<xsl:value-of select="ropmax" /> 
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="2px" border-top="0.75px solid black" border-botom="0.5px solid black">
						<fo:block>
							<fo:table width="100%" table-layout="fixed" font-size="6" border="0.5px solid grey">
								<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(70)"/>
									<fo:table-body>
										<fo:table-row font-size="8pt">
											<fo:table-cell font-weight="bold" border-bottom="0.5px solid grey" border-right="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding-left="2px">
													Lithology Summary
												</fo:block>
											</fo:table-cell>
											<fo:table-cell border-bottom="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px" linefeed-treatment="preserve">
													<xsl:value-of select="lithologyDom"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row font-size="8pt">
											<fo:table-cell font-weight="bold" border-bottom="0.5px solid grey" border-right="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px">
													Lithology Description
												</fo:block>
											</fo:table-cell>
											<fo:table-cell border-bottom="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px" linefeed-treatment="preserve">
													<xsl:value-of select="descr"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row font-size="8pt">
											<fo:table-cell font-weight="bold" border-bottom="0.5px solid grey" border-right="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px">
													Gas &amp; Shows Comments
												</fo:block>
											</fo:table-cell>
											<fo:table-cell border-bottom="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px" linefeed-treatment="preserve">
													<xsl:value-of select="gasComments"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row font-size="8pt">
											<fo:table-cell font-weight="bold" border-bottom="0.5px solid grey" border-right="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px">
													ROP Comments
												</fo:block>
											</fo:table-cell>
											<fo:table-cell border-bottom="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px" linefeed-treatment="preserve">
													<xsl:value-of select="ropComments"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>															
							</fo:table>
				                      			
				         </fo:block>
				   </fo:table-cell>	
				
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
			
       			<fo:table-row font-size="8pt" background-color="#EEEEEE">
					<fo:table-cell border-bottom="0.5px solid black" border-right="0.75px solid black" padding-left="2px">
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							<xsl:value-of select="topMdMsl" />
							-
							<xsl:value-of select="bottomMdMsl" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-right="0.75px solid black" padding-left="2px">
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							Min :<xsl:value-of select="ropmin" />
						</fo:block>
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							Avg :<xsl:value-of select="ropavg" />
						</fo:block>
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							Max :<xsl:value-of select="ropmax" /> 
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" border="0.5px solid black" border-left="0.5px solid black" border-top="0.5px solid black" border-botom="0.5px solid black">
						<fo:block>
							<fo:table width="100%" table-layout="fixed" font-size="6" border="0.5px solid grey">
								<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(70)"/>
									<fo:table-body>
										<fo:table-row font-size="8pt">
											<fo:table-cell font-weight="bold" border-bottom="0.5px solid grey" border-right="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding-left="2px">
													Lithology Summary
												</fo:block>
											</fo:table-cell>
											<fo:table-cell border-bottom="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px" linefeed-treatment="preserve">
													<xsl:value-of select="lithologyDom"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row font-size="8pt">
											<fo:table-cell font-weight="bold" border-bottom="0.5px solid grey" border-right="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px">
													Lithology Description
												</fo:block>
											</fo:table-cell>
											<fo:table-cell border-bottom="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px" linefeed-treatment="preserve">
													<xsl:value-of select="descr"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row font-size="8pt">
											<fo:table-cell font-weight="bold" border-bottom="0.5px solid grey" border-right="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px">
													Gas &amp; Shows Comments
												</fo:block>
											</fo:table-cell>
											<fo:table-cell border-bottom="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px" linefeed-treatment="preserve">
													<xsl:value-of select="gasComments"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row font-size="8pt">
											<fo:table-cell font-weight="bold" border-bottom="0.5px solid grey" border-right="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px" linefeed-treatment="preserve">
													ROP Comments
												</fo:block>
											</fo:table-cell>
											<fo:table-cell border-bottom="0.5px solid grey" padding-left="2px">
												<fo:block text-align="left" padding="2px" linefeed-treatment="preserve">
													<xsl:value-of select="ropComments"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>															
							</fo:table>
				                      			
				         </fo:block>
				   </fo:table-cell>	
				
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	
</xsl:stylesheet>