<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- casing section BEGIN -->
	<xsl:template match="modules/LithologyShows">
	
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap"
			border="0.5px solid black" space-after="6pt" space-before="6pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(65)"/>
			
			
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3" padding="2px" border-bottom="0.5px solid black" background-color="rgb(37,87,147)">
						<fo:block text-align="center" font-weight="bold" font-size="10pt" color="white">
							Lithology Summary
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
				<fo:table-row background-color="rgb(149, 179, 215)" font-weight="bold">
					<fo:table-cell>
						<fo:block>
							<fo:table>
								<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								<fo:table-body>
									<fo:table-row background-color="rgb(149, 179, 215)" font-weight="bold">
										<fo:table-cell number-columns-spanned="2" border-right="0.5px solid black" padding="2px">
											<fo:block text-align="center">
												Interval <xsl:value-of select="/root/modules/LithologyShows/LithologyShows/topMdMsl/@uomSymbol" /> MD<xsl:value-of select="/root/datum/currentDatumType" /> 
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<!-- 
									<fo:table-row background-color="rgb(149, 179, 215)" font-weight="bold">
										<fo:table-cell number-columns-spanned="2" border-right="0.5px solid black">
											<fo:block text-align="center">
												(Interval <xsl:value-of select="/root/modules/LithologyShows/LithologyShows/topMdMsl/@uomSymbol" /> TVSS)
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									-->
									<!-- 
									<fo:table-row background-color="rgb(149, 179, 215)" font-weight="bold">
										<fo:table-cell>
											<fo:block text-align="center">
												From
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.5px solid black">
											<fo:block text-align="center">
												To
											</fo:block>
										</fo:table-cell>
									</fo:table-row>-->
									
								</fo:table-body>
							</fo:table>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center">ROP (<xsl:value-of select="/root/modules/LithologyShows/LithologyShows/ropavg/@uomSymbol" />)</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px">
						<fo:block text-align="center">Lithology</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>				
				<xsl:apply-templates select="LithologyShows"/>
			</fo:table-body>
		</fo:table>							
	</xsl:template>

	<xsl:template match="LithologyShows">
	    <xsl:choose>
				<xsl:when test="position() mod 2 = '0'">
					<fo:table-row background-color="rgb(224,224,224)">
						<fo:table-cell border-right="0.5px solid black" padding-before="2px"
							padding-start="2px" padding-end="2px" border-top="0.5px solid black">
							<fo:block text-align="center">
			                   	<fo:table>
									<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
										<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block>
													<xsl:value-of select="translate(topMdMsl,',','')"/> 
								                   	<fo:inline> - </fo:inline> 
								                   	<xsl:value-of select="translate(bottomMdMsl,',','')"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<!-- 
										<fo:table-row>
											<fo:table-cell>
												<fo:block>
													(<xsl:value-of select="translate(depthFromSubseaTvd,',','')"/> 
								                   	<fo:inline> - </fo:inline> 
								                   	<xsl:value-of select="translate(depthToSubseaTvd,',','')"/>)
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										-->
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding-before="2px"
							padding-start="2px" padding-end="2px" border-top="0.5px solid black">
							<fo:block text-align="center">
								<fo:table>
									<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
										<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block>
													Min : <xsl:value-of select="translate(ropmin,',','')"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell>
												<fo:block>
													Avg : <xsl:value-of select="translate(ropavg,',','')"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell>
												<fo:block>
													Max : <xsl:value-of select="translate(ropmax,',','')"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-before="2px"	padding-start="2px" padding-end="2px" border-top="0.5px solid black">
							<fo:block text-align="left" linefeed-treatment="preserve">
								<xsl:value-of select="lithologyDom"/>
							</fo:block>
							<fo:block text-align="left" linefeed-treatment="preserve">
								<xsl:value-of select="descr"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:when>
				<xsl:otherwise>
						<fo:table-row>
						<fo:table-cell border-right="0.5px solid black" padding-before="2px"
							padding-start="2px" padding-end="2px" border-top="0.5px solid black">
							<fo:block text-align="center">
			                   		<fo:table>
									<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
										<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block>
													<xsl:value-of select="translate(topMdMsl,',','')"/> 
								                   	<fo:inline> - </fo:inline> 
								                   	<xsl:value-of select="translate(bottomMdMsl,',','')"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<!-- 
										<fo:table-row>
											<fo:table-cell>
												<fo:block>
													(<xsl:value-of select="translate(depthFromSubseaTvd,',','')"/> 
								                   	<fo:inline> - </fo:inline> 
								                   	<xsl:value-of select="translate(depthToSubseaTvd,',','')"/>)
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										-->
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding-before="2px"
							padding-start="2px" padding-end="2px" border-top="0.5px solid black">
							<fo:block text-align="center">
								<fo:table>
									<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
										<fo:table-body>
										<fo:table-row>
											<fo:table-cell>
												<fo:block>
													Min : <xsl:value-of select="translate(ropmin,',','')"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell>
												<fo:block>
													Avg : <xsl:value-of select="translate(ropavg,',','')"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
										<fo:table-row>
											<fo:table-cell>
												<fo:block>
													Max : <xsl:value-of select="translate(ropmax,',','')"/>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</fo:table-body>
								</fo:table>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-before="2px"	padding-start="2px" padding-end="2px" border-top="0.5px solid black">
							<fo:block text-align="left" linefeed-treatment="preserve">
								<xsl:value-of select="lithologyDom"/>
							</fo:block>
							<fo:block text-align="left" linefeed-treatment="preserve">
								<xsl:value-of select="descr"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:otherwise>
		</xsl:choose>		
		
	</xsl:template>
	<!-- lithology section END -->

</xsl:stylesheet>