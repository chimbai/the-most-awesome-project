<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	<!-- TEMPLATE: modules/ProductionString -->
	<xsl:template match="root/modules/ProductionString">
			
		<!-- TABLE -->
		<fo:table width="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt">				
			<fo:table-column column-number="1" column-width="proportional-column-width(23)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(23)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(27)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(27)"/>
	
			<!-- HEADER -->
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="4"  padding="2px" background-color="#b8b8ba" border="thin solid black" display-align="center">
						<fo:block text-align="center" font-size="8pt" font-weight="bold">
							Completion String
						</fo:block>
					</fo:table-cell>					 
				</fo:table-row>						
			</fo:table-header>  
				
			<!-- BODY -->
			<fo:table-body>
				<xsl:apply-templates select="ProductionString"/> <!-- modules/PS/PS -->
				<!-- Component Details Table -->
				<xsl:apply-templates select="ProductionStringDetail"/> <!-- modules/PS/PS/PSD -->
				<!-- Packer -->
				<xsl:apply-templates select="../../ProductionStringPacker"/> <!-- modules/PSP -->
				<!-- Other In Hole -->				
				<xsl:apply-templates select="../../DroppedInHole"/> <!-- modules/DIH -->
			</fo:table-body>
		</fo:table>
	</xsl:template>
		
	<!-- completion/production string -->
	<xsl:template match="ProductionString">
		<xsl:variable name="productionStringUid" select="productionStringUid"/> <!-- define a variable for the first time -->
		<fo:table-row>
			<fo:table-cell number-columns-spanned="4"  padding="2px" background-color="#919194" border="thin solid black" display-align="center">
				<fo:block text-align="center" font-size="8pt" font-weight="bold">
					Type of Completion :&#160;
						<xsl:value-of select="productionType"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row >
			<fo:table-cell padding="2px"  border-left="thin solid black" >
				<fo:block text-align="left">
					No. of Completion String
				</fo:block>
			</fo:table-cell> 
			<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" > 
				<fo:block text-align="right">
					<xsl:value-of select="stringNo/@lookupLabel"/>
				</fo:block> 
			</fo:table-cell>
			<fo:table-cell padding="2px" border-left="thin solid black" >
				<fo:block text-align="left">
					Intelligent Completion
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"  border-right="thin solid black">
				<fo:block text-align="right">
					<xsl:value-of select="intelligenceCouple"/>													
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding="2px" border-left="thin solid black">
					<fo:block text-align="left">
						Length of Completion String
					</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" >
				<fo:block text-align="right">
					<xsl:value-of select="stringLength"/>&#160;
					<xsl:value-of select="stringLength/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-left="thin solid black">
				<fo:block text-align="left">
					Design Life
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" >
				<fo:block text-align="right">
					<xsl:value-of select="designLife"/>&#160;
					<xsl:value-of select="designLife/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
					
		<fo:table-row>
			<fo:table-cell padding="2px" border-left="thin solid black" >
				<fo:block text-align="left">
					Max. Completion Angle
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"  >
				<fo:block text-align="right">
					<xsl:value-of select="maximumAngle"/>&#160;
					<xsl:value-of select="maximumAngle/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
				
			<fo:table-cell padding="2px" border-left="thin solid black" >
				<fo:block text-align="left">
					Well Intervention Method
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" >
				<fo:block text-align="right">
					<fo:table width="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
							<fo:table-body>
								<xsl:apply-templates select="interventionMethod/multiselect"/>	
					</fo:table-body>
					</fo:table>			
				</fo:block>
				</fo:table-cell>
			</fo:table-row>
					
			<fo:table-row>			
				<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black">
					<fo:block text-align="left">
						Tubing String Material
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-bottom="thin solid black">
					<fo:block text-align="right">
						<xsl:value-of select="stringMaterialType/@lookupLabel"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black">
					<fo:block text-align="left">
						Tubing String Protection
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black">
					<fo:block text-align="right">
						<xsl:value-of select="stringProtection/@lookupLabel"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
					
		<xsl:if test="count(ProductionStringDetail) &gt; 0">
		<fo:table-row>
			<fo:table-cell  padding="0px" number-columns-spanned="4" >
				<fo:block text-align="right"> 	
					<fo:table width="100%" table-layout="fixed" space-after="2pt" border-bottom="thin solid black">
		 	           	<fo:table-column column-number="1" column-width="proportional-column-width(4)"/>
			            <fo:table-column column-number="2" column-width="proportional-column-width(38)"/>
			            <fo:table-column column-number="3" column-width="proportional-column-width(8)"/>
			            <fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
			            <fo:table-column column-number="5" column-width="proportional-column-width(10)"/>
			            <fo:table-column column-number="6" column-width="proportional-column-width(15)"/>
			            <fo:table-column column-number="7" column-width="proportional-column-width(15)"/>
	            
					<!-- header -->
						<fo:table-header>
							<fo:table-row>
								<fo:table-cell number-columns-spanned="7" padding="2px" background-color="#eaeaeb" display-align="center" 
								border-bottom="thin solid black" border-left="thin solid black" border-right="thin solid black" >
									<fo:block text-align="center" font-size="8pt" font-weight="bold">
									Component Details
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
			                    <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
			                        <fo:block font-weight="bold">#</fo:block>
			                    </fo:table-cell>
			                    <fo:table-cell font-size="8pt" padding="1px"  text-align="left" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
			                        <fo:block font-weight="bold">Component Description</fo:block>
			                    </fo:table-cell>
			                    <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
			                        <fo:block font-weight="bold">Qty</fo:block>
			            	    </fo:table-cell>
				                    <fo:table-cell font-size="8pt" padding="1px"  text-align="center" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
			                        <fo:block font-weight="bold">OD (<xsl:value-of select="/root/modules/ProductionString/ProductionString/ProductionStringDetail/sizeOd/@uomSymbol"/>)</fo:block>
			                    </fo:table-cell>
			                    <fo:table-cell font-size="8pt" padding="1px"  text-align="center" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
			                        <fo:block font-weight="bold">ID (<xsl:value-of select="/root/modules/ProductionString/ProductionString/ProductionStringDetail/sizeId/@uomSymbol"/>)</fo:block>
			                    </fo:table-cell>
			                    <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
			                        <fo:block font-weight="bold">Length (<xsl:value-of select="/root/modules/ProductionString/ProductionString/ProductionStringDetail/length/@uomSymbol"/>)</fo:block>
			                    </fo:table-cell>
			                    <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
			                        <fo:block font-weight="bold">Depth (<xsl:value-of select="/root/modules/ProductionString/ProductionString/ProductionStringDetail/depthMdMsl/@uomSymbol"/>)</fo:block>
			                    </fo:table-cell>
			              	</fo:table-row>
						</fo:table-header>
						<!-- body -->	
							<fo:table-body>
								<xsl:apply-templates select="ProductionStringDetail">
									<xsl:sort select="sequence" order="ascending"/>
								</xsl:apply-templates>
							</fo:table-body>
						</fo:table>
					</fo:block>  
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
		
		<xsl:if test="count(/root/modules/ProductionStringPacker/ProductionStringPacker[productionStringUid=$productionStringUid]) &gt; 0">
		<fo:table-row>
			<fo:table-cell  padding="0px" number-columns-spanned="4">
				<xsl:apply-templates select="/root/modules/ProductionStringPacker">
					<xsl:with-param name="productionStringUid" select="$productionStringUid"/> <!-- reuse/pass the variable to another template -->
				</xsl:apply-templates>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		
		<xsl:if test="count(/root/modules/DroppedInHole/DroppedInHole[productionStringUid=$productionStringUid]) &gt; 0">
		<fo:table-row>
			<fo:table-cell  padding="0px" number-columns-spanned="4">
				<fo:block>
					<xsl:apply-templates select="/root/modules/DroppedInHole">
						<xsl:with-param name="productionStringUid" select="$productionStringUid"/>
					</xsl:apply-templates>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		
		<fo:table-row> <!-- table for Other Details -->
			<fo:table-cell number-columns-spanned="4"  padding="2px" background-color="#eaeaeb" display-align="center" border-left="thin solid black" border-bottom="thin solid black" border-right="thin solid black">
				<fo:block text-align="center" font-size="8pt" font-weight="bold">
				Other Details
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row >
				<fo:table-cell padding="2px"  border-left="thin solid black" >
						<fo:block text-align="left">
							TreeType
						</fo:block>
					</fo:table-cell> 
				<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" > 
					<fo:block text-align="right">
						<xsl:value-of select="treeType/@lookupLabel"/>	
					</fo:block> 
				</fo:table-cell>
				<fo:table-cell  padding="2px" border-left="thin solid black" >
						<fo:block text-align="left">
							Bottom Hole Temperature
						</fo:block>
				</fo:table-cell>
				<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"  border-right="thin solid black">
						<fo:block text-align="right">
						<xsl:value-of select="holeTemperature"/>&#160;
						<xsl:value-of select="holeTemperature/@uomSymbol"/>													
					</fo:block>
				</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding="2px" border-left="thin solid black" >
						<fo:block text-align="left">
							No. Isolation Zones
						</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block text-align="right">
						<xsl:value-of select="isolationZone"/>													
					</fo:block>
				</fo:table-cell>
			<fo:table-cell padding="2px" border-left="thin solid black">
				<fo:block text-align="left">
							Bottom Hole Pressure
				</fo:block>
			</fo:table-cell>
				<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" >
						<fo:block text-align="right">
						<xsl:value-of select="holePressure"/>&#160;
						<xsl:value-of select="holePressure/@uomSymbol"/>
					</fo:block>
				</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black">
					<fo:block text-align="left">
						Artificial Lift Equipment
					</fo:block>
				</fo:table-cell>
			<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-bottom="thin solid black">
				<fo:block text-align="right">
					<xsl:value-of select="artificialLiftEquipment/@lookupLabel"/>&#160;
					<xsl:value-of select="mandrel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black">
					<fo:block text-align="left">
						<!-- empty -->
					</fo:block>
				</fo:table-cell>
			<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"  border-right="thin solid black" border-bottom="thin solid black">
					<fo:block text-align="right">
					 <!-- empty -->
					</fo:block>
			</fo:table-cell>
		</fo:table-row>	<!-- end other details table -->
	</xsl:template> <!-- end main table -->
	
		<xsl:template match="interventionMethod/multiselect">
			<fo:table-row border-bottom="thin solid black">
				<fo:table-cell padding="0px"  >
					<fo:block text-align="right">
						<xsl:value-of select="@lookupLabel"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:template>
	 
	<!--Component Details -->
	<xsl:template match="ProductionStringDetail"> 
	         
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
		        	<fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black" >
		            	<fo:block>
							<xsl:value-of select="sequence/@rawNumber"/>
		                 </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="0.5px solid black">
	                     <fo:block>
	                         <xsl:value-of select="description"/> 
	                     </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black"  >
	                     <fo:block>
                         	<xsl:value-of select="numJoints"/>
	                     </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black">
	                     <fo:block>
                         	<xsl:value-of select="sizeOd"/>
	                     </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black" >
	                     <fo:block>
                         	<xsl:value-of select="sizeId"/>
	                     </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap"  border-left="0.5px solid black">
	                     <fo:block>
                         	<xsl:value-of select="length"/>
	                     </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black"  border-right="0.5px solid black">
	                     <fo:block>
	                     	<xsl:value-of select="depthMdMsl"/>
	                     </fo:block>
	                 </fo:table-cell>
	             </fo:table-row>
		             
           	</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
		        	<fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black" >
		            	<fo:block>
							<xsl:value-of select="sequence/@rawNumber"/>
		                 </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="0.5px solid black">
	                     <fo:block>
	                         <xsl:value-of select="description"/> 
	                     </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black"  >
	                     <fo:block>
                         	<xsl:value-of select="numJoints"/>
	                     </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black">
	                     <fo:block>
                         	<xsl:value-of select="sizeOd" />
	                     </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black" >
	                     <fo:block>
                         	<xsl:value-of select="sizeId"/>
	                     </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap"  border-left="0.5px solid black">
	                     <fo:block>
                         	<xsl:value-of select="length"/>
	                     </fo:block>
	                 </fo:table-cell>
	                 <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black"  border-right="0.5px solid black">
	                     <fo:block>
	                     	<xsl:value-of select="depthMdMsl"/>
	                     </fo:block>
	                 </fo:table-cell>
	             </fo:table-row>
            </xsl:otherwise>  
		</xsl:choose>
	</xsl:template>	<!--Component Details -->
	
	<xsl:template match="modules/ProductionStringPacker">
		<xsl:param name="productionStringUid"/>
		<fo:table width="100%" table-layout="fixed" space-after="2pt" border-bottom="thin solid black">
            <fo:table-column column-number="1" column-width="proportional-column-width(4)"/>
            <fo:table-column column-number="2" column-width="proportional-column-width(20)"/>
            <fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
            <fo:table-column column-number="4" column-width="proportional-column-width(56)"/>
            
		<!-- header -->
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="4"  padding="2px" background-color="#eaeaeb" display-align="center" 
				border-left="thin solid black"	border-bottom="thin solid black" border-right="thin solid black">
						<fo:block text-align="center" font-size="8pt" font-weight="bold">
						Packer Details
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
					
			<fo:table-row>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="thin solid black">
                        <fo:block font-weight="bold">#</fo:block>
                    </fo:table-cell>
                    <fo:table-cell number-columns-spanned="2" font-size="8pt" padding="1px"  text-align="center" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
                        <fo:block font-weight="bold">Depth (<xsl:value-of select="/root/modules/ProductionStringPacker/ProductionStringPacker/depthMdMsl/@uomSymbol"/>)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black" border-right="thin solid black" >
                        <fo:block font-weight="bold">Description</fo:block>
                    </fo:table-cell>
             </fo:table-row>
             
            <fo:table-row>	            
            	<fo:table-cell number-columns-spanned="1"  padding-left="2px" padding-right="2px" border-left="thin solid black" border-bottom="thin solid black">
				<fo:block> <!-- Empty cell -->	</fo:block>  
				</fo:table-cell>
				
				<fo:table-cell padding-left="2px" padding-right="2px" border-left="thin solid black" border-bottom="thin solid black">
				<fo:block font-weight="bold" text-align="center"> MD </fo:block>  
				</fo:table-cell>
				
				<fo:table-cell padding-left="2px" padding-right="2px" border-left="thin solid black" border-bottom="thin solid black">
				<fo:block font-weight="bold" text-align="center"> TVD</fo:block>  
				</fo:table-cell>
				
				<fo:table-cell number-columns-spanned="1"  padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black"  border-bottom="thin solid black">
				<fo:block> <!-- Empty cell -->	</fo:block>  
				</fo:table-cell>
            
            </fo:table-row>
			</fo:table-header>
					
			<!-- body -->	
				<fo:table-body>
					<xsl:apply-templates select="ProductionStringPacker[productionStringUid=$productionStringUid]">
					<xsl:sort select="sequence" order="ascending"/>
					</xsl:apply-templates>
				</fo:table-body>
		</fo:table>
	</xsl:template>
	
	 <!-- Packer Details -->
	 <xsl:template match="ProductionStringPacker">
	 
	 		<xsl:choose>
				<xsl:when test="position() mod 2 != '0'">
					<fo:table-row>
	             	<fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="thin solid black">
                        <fo:block>
							<xsl:value-of select="sequence/@rawNumber"/> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black">
                        <fo:block>
                            <xsl:value-of select="depthMdMsl"/> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap"  border-left="0.5px solid black">
                        <fo:block>
                            <xsl:value-of select="depthTvdMsl" /> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="0.5px solid black" border-right="thin solid black">
                        <fo:block>
                            <xsl:value-of select="description" />  
                        </fo:block>
                    </fo:table-cell>
         
                </fo:table-row>
                
               </xsl:when>
			<xsl:otherwise>
			
				<fo:table-row background-color="#EEEEEE">
                 
                     <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="thin solid black">
                        <fo:block>
							<xsl:value-of select="sequence/@rawNumber"/> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap" border-left="0.5px solid black">
                        <fo:block>
                            <xsl:value-of select="depthMdMsl"/> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="center" wrap-option="wrap"  border-left="0.5px solid black">
                        <fo:block>
                            <xsl:value-of select="depthTvdMsl" /> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="0.5px solid black" border-right="thin solid black">
                        <fo:block>
                            <xsl:value-of select="description" />  
                        </fo:block>
                    </fo:table-cell>
               </fo:table-row>
            </xsl:otherwise>  
		</xsl:choose>
		
	</xsl:template>		<!-- Packer Details -->
	
	<xsl:template match="modules/DroppedInHole">
		<xsl:param name="productionStringUid"/>
			<fo:table width="100%" table-layout="fixed" space-after="2pt" border-bottom="thin solid black">
	            <fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
	            <fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
	            <fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
	            <fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
	            <fo:table-column column-number="5" column-width="proportional-column-width(15)"/>
	            <fo:table-column column-number="6" column-width="proportional-column-width(15)"/>
	            
				<!-- header -->
				<fo:table-header>
					<fo:table-row> 
						<fo:table-cell number-columns-spanned="6"  padding="2px" background-color="#eaeaeb" display-align="center" 
						 border-bottom="thin solid black" border-right="thin solid black" border-left="thin solid black">
							<fo:block text-align="center" font-size="8pt" font-weight="bold">
							Other In Hole Details
							</fo:block>
						</fo:table-cell> 
					</fo:table-row> 
					<fo:table-row>
	                    <fo:table-cell font-size="8pt" padding="1px"  text-align="left" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
	                        <fo:block font-weight="bold">Item Type</fo:block>
	                    </fo:table-cell>
	                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
	                        <fo:block font-weight="bold">Event Date</fo:block>
	                    </fo:table-cell>
	                    <fo:table-cell font-size="8pt" padding="1px"  text-align="left" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
	                        <fo:block font-weight="bold">Event Type</fo:block>
	                    </fo:table-cell>
	                    
	                    <fo:table-cell font-size="8pt" padding="1px"  text-align="left" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
	                        <fo:block font-weight="bold">Fishing Tool</fo:block>
	                    </fo:table-cell>
	                    
	                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black" border-bottom="thin solid black">
	                        <fo:block font-weight="bold">Root Cause</fo:block>
	                    </fo:table-cell>
	                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-right="thin solid black" border-left="thin solid black" border-bottom="thin solid black">
	                        <fo:block font-weight="bold">Regulatory Req.</fo:block>
	                   </fo:table-cell>                    
	                </fo:table-row>
				</fo:table-header>
			
				<!-- body -->	
				<fo:table-body>
					<xsl:apply-templates select="DroppedInHole[productionStringUid=$productionStringUid]">
						<xsl:sort select="sequence" order="ascending"/>
					</xsl:apply-templates>
				</fo:table-body>
			</fo:table>
	</xsl:template>
	
	<!-- Other In Hole Details -->
	<xsl:template match="DroppedInHole">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
                    
                <fo:table-row>
                <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black" >
                        <fo:block>
							<xsl:value-of select="droppedItemType/@lookupLabel"/> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black">
                        <fo:block>
                            <xsl:value-of select="dynaAttr/eventStartDateTime"/> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black">
                        <fo:block>
                            <xsl:value-of select="dynaAttr/eventType/@lookupLabel" /> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black">
                        <fo:block>
                            <xsl:value-of select="dynaAttr/fishingTool"/>  
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap"  border-left="thin solid black">
                        <fo:block>
                            <xsl:value-of select="rootCause"/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black"  border-right="0.5px solid black">
                        <fo:block>
                            <xsl:value-of select="regulatoryRequirement"/>
                        </fo:block>
                    </fo:table-cell>                  
                </fo:table-row>
                
               </xsl:when>
			<xsl:otherwise>
			
				<fo:table-row background-color="#EEEEEE">
	              	<fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black" >
                        <fo:block>
							<xsl:value-of select="droppedItemType/@lookupLabel"/> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black">
                        <fo:block>
                            <xsl:value-of select="dynaAttr/eventStartDateTime"/> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black">
                        <fo:block>
                            <xsl:value-of select="dynaAttr/eventType/@lookupLabel" /> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black">
                        <fo:block>
                            <xsl:value-of select="dynaAttr/fishingTool"/>  
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap"  border-left="thin solid black">
                        <fo:block>
                            <xsl:value-of select="rootCause"/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap" border-left="thin solid black"  border-right="0.5px solid black">
                        <fo:block>
                            <xsl:value-of select="regulatoryRequirement"/>
                        </fo:block>
                    </fo:table-cell>                    
	                </fo:table-row>
	                
            	</xsl:otherwise>
			</xsl:choose>
	</xsl:template>	<!-- Other In Hole Details -->

</xsl:stylesheet>