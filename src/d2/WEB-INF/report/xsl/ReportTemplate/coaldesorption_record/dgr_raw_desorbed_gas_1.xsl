<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
		
	<xsl:template match="modules/CoalDesorption" mode="DesorbedGasTest_part1">	 
	              
		<fo:table-cell>
			<fo:block>
				<fo:table width="100%" table-layout="fixed" border-left="0.75px solid black" border-top="0.75px solid black" border-right="0.75px solid black">
					<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(50)"/>
						<fo:table-body>
							<fo:table-row font-size="8pt" font-weight="bold" text-align="center" background-color="rgb(149, 179, 215)">
								<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
									<fo:block text-align="center" padding="2px">Sample No.</fo:block>
								</fo:table-cell>
								<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
									<fo:block text-align="center" padding="2px">Sample Date Test</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block>
										<fo:table width="100%" table-layout="fixed">
											<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
											<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												<fo:table-body>
													<fo:table-row>
														<fo:table-cell number-columns-spanned="2" border-bottom="0.75px solid black">
															<fo:block text-align="center" padding="2px">Raw Desorbed Gas</fo:block>
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell border-bottom="0.75px solid black" border-right="0.75px solid black">
															<fo:block text-align="center" padding="2px">Date Taken</fo:block>
														</fo:table-cell>
														<fo:table-cell border-bottom="0.75px solid black" >
															<fo:block text-align="center" padding="2px">
																Amount
																(<xsl:value-of select="/root/modules/CoalDesorption/CoalDesorption/CoalDesorptionDetails/RawDesorbedGasTest/amount/@uomSymbol"/>)
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
												</fo:table-body>															
										</fo:table>  			
							         </fo:block>
							   </fo:table-cell>	
							</fo:table-row>
							
							<xsl:apply-templates select="CoalDesorption
							[((number(dateOnTest/@epochMS)) &lt;= (number(/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS)))and (desorptionType='desorption')]" 
							mode="DesorbedGasTest_part1"></xsl:apply-templates>
							
						</fo:table-body>															
				</fo:table>
	                      			
	         </fo:block>
	   </fo:table-cell>					
	</xsl:template>
	
	<xsl:template match="CoalDesorption" mode="DesorbedGasTest_part1">
			
				<fo:table-row font-size="8pt">
					<fo:table-cell border-right="0.75px solid black" padding-left="2px" border-bottom="0.75px solid black">
						<fo:block text-align="center" padding-top="2px" padding-left="2px" padding-right="2px" padding-bottom="2px">
							<xsl:value-of select="sampleNo"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px solid black" padding-left="2px" border-bottom="0.75px solid black">
						<fo:block text-align="center" padding-top="2px" padding-left="2px" padding-right="2px" padding-bottom="2px">
							<xsl:value-of select="dateOnTest"/>
						</fo:block>
					</fo:table-cell>
					
					<xsl:apply-templates select="CoalDesorptionDetails" mode="DesorbedGasTest_part2"></xsl:apply-templates>
				</fo:table-row>
			
	</xsl:template>
	
	<xsl:template match="CoalDesorptionDetails" mode="DesorbedGasTest_part2">
			 	<fo:table-cell>
					<fo:block>
						<fo:table width="100%" table-layout="fixed" border-bottom="0.75px solid black" border-top="0.5px solid black">
							<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								<fo:table-body>
									<xsl:apply-templates select="RawDesorbedGasTest[((number(dateTestTaken/@epochMS)) &lt;= (number(/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS)))]" 
									mode="DesorbedGasTest_part2_1"></xsl:apply-templates>
								</fo:table-body>															
						</fo:table>  			
			         </fo:block>
			   </fo:table-cell>	
			
	</xsl:template>
	
</xsl:stylesheet>