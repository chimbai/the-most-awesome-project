<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/CoalDesorption"  mode="LaboratorySampleSummary">	
	
		<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" border="0.75px solid black" margin-top="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
	        <fo:table-column column-number="2" column-width="proportional-column-width(33)"/>
	        <fo:table-column column-number="3" column-width="proportional-column-width(34)"/>
	       
	        
	        
	        			
			<fo:table-header>
				<fo:table-row font-size="10pt">
			    	<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" font-weight="bold" number-columns-spanned="3" text-align="center" background-color="rgb(37,87,147)">
			        	<fo:block color="white">Laboratory Sample Summary</fo:block>
					</fo:table-cell> 
			    </fo:table-row>
				
				<fo:table-row font-weight="bold" text-align="center" background-color="rgb(149, 179, 215)">
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>Sample No.</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>
								<fo:block>
									Interval
									(<xsl:value-of select="/root/modules/CoalDesorption/CoalDesorption/bottomMdMsl/@uomSymbol"/>)
								</fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
				
				
				
					<fo:table-cell padding="2px" border-bottom="0.75px solid black" >
						<fo:block>Sample Date</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
				
			
			<fo:table-body>				
				<xsl:apply-templates select="CoalDesorption" mode="LaboratorySampleSummary">
					
				</xsl:apply-templates>
			</fo:table-body>
			
		</fo:table>										
	</xsl:template>
	<xsl:template match="CoalDesorption[((number(dateOnTest/@epochMS)) &lt;= (number(/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS)))
										and (desorptionType='lab_sample')
									   ]" mode="LaboratorySampleSummary">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row background-color="white" text-align="center">
					<fo:table-cell border-right="0.5px thin solid black" padding="2px" >
						<fo:block>
							<xsl:value-of select="sampleNo"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell border-right="0.5px thin solid black"  padding="2px" >
						<fo:block>
							<xsl:value-of select="topMdMsl"/> - 
							<xsl:value-of select="bottomMdMsl"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="2px" >
						<fo:block>
							<xsl:value-of select="dateOnTest"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				
				<fo:table-row background-color="#EEEEEE" text-align="center">
					<fo:table-cell border-right="0.5px thin solid black" padding="2px" >
						<fo:block>
							<xsl:value-of select="sampleNo"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell border-right="0.5px thin solid black"  padding="2px" >
						<fo:block>
							<xsl:value-of select="topMdMsl"/> - 
							<xsl:value-of select="bottomMdMsl"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="2px" >
						<fo:block>
							<xsl:value-of select="dateOnTest"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	
			
	
	</xsl:template>

</xsl:stylesheet>