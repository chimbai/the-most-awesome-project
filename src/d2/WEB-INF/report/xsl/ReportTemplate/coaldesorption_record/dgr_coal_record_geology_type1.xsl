<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/CoalDesorption"  mode="GrossCoalReport">	
	
		<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" border-top="0.75px solid black" border-left ="0.75px solid black" border-right ="0.75px solid black" margin-top="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(17)"/>
	        <fo:table-column column-number="2" column-width="proportional-column-width(17)"/>
	        <fo:table-column column-number="3" column-width="proportional-column-width(17)"/>
	        <fo:table-column column-number="4" column-width="proportional-column-width(17)"/>
	        <fo:table-column column-number="5" column-width="proportional-column-width(17)"/>
	        <fo:table-column column-number="6" column-width="proportional-column-width(15)"/> 
	        
	        			
			<fo:table-header>
				<fo:table-row font-size="10pt">
			    	<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" font-weight="bold" number-columns-spanned="6" text-align="center" background-color="rgb(37,87,147)">
			        	<fo:block color="white">Gross Coal Report (Summary of All Coals Intersected)</fo:block>
					</fo:table-cell> 
			    </fo:table-row>	
							
				<fo:table-row font-size="8pt" font-weight="bold" text-align="center" background-color="rgb(149, 179, 215)">
					<fo:table-cell padding="2px" border-bottom="0.75px solid black" border-right="0.75px solid black">
						<fo:block>Formation</fo:block>
					</fo:table-cell>
					
						
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>	
							Interval
						</fo:block>
						<fo:block text-align="center">	
							(<xsl:value-of select="/root/modules/CoalDesorption/CoalDesorption/topMdMsl/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>	
							Net Coal
						</fo:block>
						<fo:block text-align="center">	
							(<xsl:value-of select="/root/modules/CoalDesorption/CoalDesorption/netCoal/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>	
							Ave. Coal Brightness
						</fo:block>
						
					</fo:table-cell>
					
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>
								<fo:block>Desorption / Lab Sample No.</fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>
								<fo:block>Comment</fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				

			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="CoalDesorption[((number(dateOnTest/@epochMS)) &lt;= (number(/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS)))]" 
				mode="GrossCoalReport">
					
				</xsl:apply-templates>
				<fo:table-row font-weight="bold" text-align="right" background-color="rgb(149, 179, 215)">
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-top="0.75px solid black" border-bottom="0.75px solid black" number-columns-spanned="2">
								<fo:block>Total : </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" padding="2px" border-right="0.75px solid black" border-top="0.75px solid black" border-bottom="0.75px solid black"> 
						<fo:block>
							<fo:inline>
								<xsl:value-of select="format-number(sum(CoalDesorption[((number(dateOnTest/@epochMS)) &lt;= (number(/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS)))]/netCoal/@rawNumber), '#,###.00')"/>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					 
					<fo:table-cell text-align="center" padding="2px" border-bottom="0.75px solid black" border-right="0.75px solid black" border-top="0.75px solid black">
						<fo:block>
							<fo:inline>
								<fo:block></fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" padding="2px" border-bottom="0.75px solid black" border-right="0.75px solid black" border-top="0.75px solid black">
						<fo:block>
							<fo:inline>
								<fo:block></fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" padding="2px" border-bottom="0.75px solid black" border-top="0.75px solid black">
						<fo:block> 
							<fo:inline>
								<fo:block></fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
			</fo:table-body>
			
		</fo:table>										
	</xsl:template>

	<xsl:template match="CoalDesorption" mode="GrossCoalReport">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row text-align="center" font-size="8pt" border-right="0.75px solid black" background-color="white">
						<fo:table-cell border-right="0.75px thin solid black" padding="2px" >
						<fo:block>
							<xsl:value-of select="formation/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px thin solid black"  padding="2px" >
						<fo:block>
							<xsl:value-of select="topMdMsl"/> - 
							<xsl:value-of select="bottomMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px thin solid black" padding="2px" >
						<fo:block>
							<xsl:value-of select="netCoal"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px thin solid black" padding="2px" >
						<fo:block>
							<xsl:value-of select="aveCoalBrightness"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px thin solid black" padding="2px" >
						<fo:block>
							<xsl:value-of select="desorptionType/@lookupLabel"/>
							<xsl:value-of select="sampleNo"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px">
						<fo:block linefeed-treatment="preserve" text-align="left">
							<xsl:value-of select="comment"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				
				<fo:table-row text-align="center" font-size="8pt" border-right="0.75px solid black" background-color="#EEEEEE">
						<fo:table-cell border-right="0.75px thin solid black" padding="2px" >
						<fo:block>
							<xsl:value-of select="formation/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px thin solid black"  padding="2px" >
						<fo:block>
							<xsl:value-of select="topMdMsl"/> - 
							<xsl:value-of select="bottomMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px thin solid black" padding="2px" >
						<fo:block>
							<xsl:value-of select="netCoal"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px thin solid black" padding="2px" >
						<fo:block>
							<xsl:value-of select="aveCoalBrightness"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px thin solid black" padding="2px" >
						<fo:block>
							<xsl:value-of select="desorptionType/@lookupLabel"/>
							<xsl:value-of select="sampleNo"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px">
						<fo:block linefeed-treatment="preserve" text-align="left">
							<xsl:value-of select="comment"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	
			
	
	</xsl:template>
	
</xsl:stylesheet>