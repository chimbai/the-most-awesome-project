<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
		
	<xsl:template match="RawDesorbedGasTest" mode="DesorbedGasTest_part2_1">	              
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row font-size="8pt" background-color="white">
					<fo:table-cell border-right="0.5px solid black" padding-left="2px">
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							<xsl:value-of select="dateTestTaken"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px">
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							<xsl:value-of select="amount"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row font-size="8pt" background-color="#EEEEEE">
					<fo:table-cell border-right="0.5px solid black" padding-left="2px">
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							<xsl:value-of select="dateTestTaken"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px">
						<fo:block text-align="center" padding="2px" padding-left="2px" padding-right="2px">
							<xsl:value-of select="amount"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	
	</xsl:template>
</xsl:stylesheet>