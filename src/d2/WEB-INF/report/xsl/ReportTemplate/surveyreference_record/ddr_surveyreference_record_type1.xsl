<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:import href="./surveyreference_header.xsl" />
	<xsl:import href="./surveyreference_record.xsl" />
		
	<!-- survey section BEGIN -->		 
	<xsl:template match="modules/SurveyReference/SurveyReference">
		<xsl:variable name="surveyReferenceUid" select="surveyReferenceUid"/>		                      	
		<fo:table inline-progression-dimension="100%" table-layout="fixed" font-size="8pt" border-bottom="0.5px solid black"
			border-left="0.5px solid black" border-right="0.5px solid black" margin-top="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="8" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(20)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="9" padding="0.05cm" padding-bottom="-0.05cm" border-top="0.25px solid black"
						border-bottom="0.25px solid black" background-color="rgb(224,224,224)"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Survey 
							<xsl:if test="stracknum != '0'">
								(Sidetrack#<xsl:value-of select="stracknum"/>)
							</xsl:if>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:call-template name="surveyreference_header"/>			
			</fo:table-header>
			<fo:table-body width="100%">				
				<xsl:apply-templates name="SurveyStation[surveyReferenceUid=$surveyReferenceUid]" mode="survey_detail">					
					<xsl:sort select="depthMdMsl/@rawNumber" data-type="number" order="ascending"/>
				</xsl:apply-templates>
			</fo:table-body>
		</fo:table>											
	</xsl:template>	
	<!-- survey section END -->
	
</xsl:stylesheet>