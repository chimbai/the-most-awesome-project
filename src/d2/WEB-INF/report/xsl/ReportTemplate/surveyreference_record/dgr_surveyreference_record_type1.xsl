<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/SurveyReference/SurveyReference">
		<xsl:variable name="surveyReferenceUid" select="surveyReferenceUid"/>		                      	
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" border-left="0.5px solid black"
			border-right="0.5px solid black" border-bottom="0.5px solid black" margin-top="2pt"
			space-before="6pt" space-after="6pt" border-before-width="0.5px" border-before-style="solid"
			border-before-width.conditionality="retain" border-after-width="0.5px" border-after-style="solid"
			border-after-width.conditionality="retain">
			<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="8" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(20)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="9" padding="2px" padding-left="3px" padding-right="3px" border-top="0.5px solid black"
						border-bottom="0.5px solid black" background-color="rgb(37,87,147)"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block color="white" text-align="center" font-weight="bold" font-size="10pt">
							Survey
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row font-size="8pt" text-align="center" background-color="rgb(149, 179, 215)" font-weight="bold">
					<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>MD.</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>Incl.</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>Corr. Az</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>TVD</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>'V' Sect</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>Dogleg</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>N/S</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>E/W</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>Tool Type</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row font-size="8pt" text-align="center" background-color="rgb(149, 179, 215)" font-weight="bold">
					<fo:table-cell border-bottom="0.5px solid black" padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>
							(<xsl:value-of select="SurveyStation/depthMdMsl/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>
							(<xsl:value-of select="SurveyStation/inclinationAngle/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>
							(<xsl:value-of select="SurveyStation/azimuthAngle/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>
							(<xsl:value-of select="SurveyStation/depthTvdMsl/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>
							(<xsl:value-of select="SurveyStation/verticalSectionExtrusion/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>
							(<xsl:value-of select="SurveyStation/dlsAngle/@uomSymbol"/> )
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>
							(<xsl:value-of select="SurveyStation/nsOffset/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px" padding-left="3px" padding-right="3px">
						<fo:block>
							(<xsl:value-of select="SurveyStation/ewOffset/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px" padding-left="3px" padding-right="3px">
						<fo:block color="white">.</fo:block>
					</fo:table-cell>
				</fo:table-row>			
			</fo:table-header>
			<fo:table-body width="100%">
				<xsl:apply-templates select="SurveyStation[surveyReferenceUid=$surveyReferenceUid]">
					<xsl:sort select="depthMdMsl/@rawNumber" data-type="number"/>
				</xsl:apply-templates>
			</fo:table-body>
		</fo:table>											
	</xsl:template>
	
	<xsl:template match="SurveyStation">
	   <xsl:choose>
				<xsl:when test="position() mod 2 = '0'">
					<fo:table-row font-size="8pt" background-color="rgb(224,224,224)">
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="depthMdMsl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="inclinationAngle"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="azimuthAngle"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="depthTvdMsl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="verticalSectionExtrusion"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="dlsAngle"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="nsOffset"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="ewOffset"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:choose>
									<xsl:when test="string(toolType/@lookupLabel)=''">
										<xsl:value-of select="toolType"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="toolType/@lookupLabel"/>
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:when>
				<xsl:otherwise>
					<fo:table-row font-size="8pt">
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="depthMdMsl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="inclinationAngle"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="azimuthAngle"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="depthTvdMsl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="verticalSectionExtrusion"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="dlsAngle"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="nsOffset"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.5px solid black" padding="2px"
							padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:value-of select="ewOffset"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" padding-left="3px" padding-right="3px">
							<fo:block text-align="center">
								<xsl:choose>
									<xsl:when test="string(toolType/@lookupLabel)=''">
										<xsl:value-of select="toolType"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="toolType/@lookupLabel"/>
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
</xsl:stylesheet>