<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TEMPLATE: modules/SurveyReference/SurveyReference -->
	<xsl:template match="modules/SurveyReference/SurveyReference">
	
		<xsl:variable name="surveyReferenceUid" select="surveyReferenceUid"/>
		
		<fo:block keep-together="always">
		
			<!-- TABLE -->
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black" space-after="6pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(12)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="9" column-width="proportional-column-width(48)"/>
				
				<!-- HEADER -->
				<fo:table-header>
				
					<!-- HEADER 1 -->
					<fo:table-row>
						<fo:table-cell number-columns-spanned="9" padding="2px" background-color="#DDDDDD" border="thin solid black" display-align="center">
							<fo:block font-size="8pt" font-weight="bold">
								Survey
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<!-- HEADER 2 -->
					<fo:table-row text-align="center">
						<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black" >
							<fo:block>
								MD.
							</fo:block>
							<fo:block>
								(<xsl:value-of select="SurveyStation/depthMdMsl/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" >
							<fo:block>
								Incl.
							</fo:block>
							<fo:block>
								(<xsl:value-of select="SurveyStation/inclinationAngle/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black">
							<fo:block>
								Corr. Az
							</fo:block>
							<fo:block>
								(<xsl:value-of select="SurveyStation/azimuthAngle/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" >
							<fo:block>
								TVD
							</fo:block>
							<fo:block>
								(<xsl:value-of select="SurveyStation/depthTvdMsl/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" >
							<fo:block>
								'V' Sect
							</fo:block>
							<fo:block>
								(<xsl:value-of select="SurveyStation/verticalSectionExtrusion/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" >
							<fo:block>
								Dogleg
							</fo:block>
							<fo:block>
								(<xsl:value-of select="SurveyStation/dlsAngle/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black">
							<fo:block>
								N/S
							</fo:block>
							<fo:block>
								(<xsl:value-of select="SurveyStation/nsOffset/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black">
							<fo:block>
								E/W
							</fo:block>
							<fo:block>
								(<xsl:value-of select="SurveyStation/ewOffset/@uomSymbol"/>)
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" >
							<fo:block>
								Tool Type
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
				</fo:table-header>
				
				<!-- BODY -->
				<fo:table-body>
					<xsl:apply-templates select="SurveyStation">
						<xsl:sort select="depthMdMsl/@rawNumber" data-type="number"/>
					</xsl:apply-templates>
				</fo:table-body>
			</fo:table>
			
		</fo:block>
		
	</xsl:template>
	
	<!-- TEMPLATE: SurveyStation -->
	<xsl:template match="SurveyStation">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="depthMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="inclinationAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="azimuthAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="depthTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="verticalSectionExtrusion"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="dlsAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="nsOffset"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="ewOffset"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:choose>
								<xsl:when test="string(toolType/@lookupLabel)=''">
									<xsl:value-of select="toolType"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="toolType/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="depthMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="inclinationAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="azimuthAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="depthTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="verticalSectionExtrusion"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="dlsAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="nsOffset"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="ewOffset"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:choose>
								<xsl:when test="string(toolType/@lookupLabel)=''">
									<xsl:value-of select="toolType"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="toolType/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
</xsl:stylesheet>