<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Survey Reference header START -->
	<xsl:template name="surveyreference_header">	
		<fo:table-row font-size="8pt" text-align="center">
			<fo:table-cell padding="0.05cm">
				<fo:block>MD.</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block>Incl.</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block>Corr. Az</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block>TVD</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block>'V' Sect</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block>Dogleg</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block>N/S</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block>E/W</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block>Tool Type</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row font-size="8pt" text-align="center">
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block>
					(<xsl:value-of select="SurveyStation/depthMdMsl/@uomSymbol"/>)
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block>
					(<xsl:value-of select="SurveyStation/inclinationAngle/@uomSymbol"/>)
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block>
					(<xsl:value-of select="SurveyStation/azimuthAngle/@uomSymbol"/>)
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block>
					(<xsl:value-of select="SurveyStation/depthTvdMsl/@uomSymbol"/>)
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block>
					(<xsl:value-of select="SurveyStation/verticalSectionExtrusion/@uomSymbol"/>)
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block>
					(<xsl:value-of select="SurveyStation/dlsAngle/@uomSymbol"/> )
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block>
					(<xsl:value-of select="SurveyStation/nsOffset/@uomSymbol"/>)
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block>
					(<xsl:value-of select="SurveyStation/ewOffset/@uomSymbol"/>)
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block color="white">.</fo:block>
			</fo:table-cell>
		</fo:table-row>	
	</xsl:template>
	<!-- Survey Reference header END -->
	
</xsl:stylesheet>