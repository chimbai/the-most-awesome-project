<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<xsl:import href="./divider_data.xsl" />

	<!-- Part 2 divider section BEGIN -->
	<xsl:template name="dividers_part2">
		<fo:page-sequence master-reference="front_page">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<!--<xsl:call-template name="header"/>-->
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="24pt">
				<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="18pt" wrap-option="wrap" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell height="26cm" width="19cm" border="1px double black" text-align="center">
									<fo:block padding-before="5cm">
										<fo:table inline-progression-dimension="19cm" table-layout="fixed" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
											<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
											<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
											<fo:table-header>
												<fo:table-row>
													<fo:table-cell number-columns-spanned="2" padding="1cm">
														<fo:block>Part 2 : Drilling Engineering</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-header>
											<fo:table-body>											   
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/ReportDaily/ReportDaily" />
											      <xsl:with-param name="label" select="'Well History'" />
											   </xsl:call-template>
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/CasingSection/CasingSection" />
											      <xsl:with-param name="label" select="'Casing and Hole Summary'" />	
											   </xsl:call-template>
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/MudProperties/MudProperties" />
											      <xsl:with-param name="label" select="'Drilling Fluids Record'" />	
											   </xsl:call-template>
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/SurveyReference/SurveyReference" />
											      <xsl:with-param name="label" select="'Survey Data'" />	
											   </xsl:call-template>
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	<!-- Part 2 divider section END -->

</xsl:stylesheet>