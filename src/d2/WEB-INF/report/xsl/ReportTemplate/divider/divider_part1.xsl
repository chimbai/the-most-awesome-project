<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<xsl:import href="../../IDS_DEMO_fwr.xsl" />
	<xsl:import href="./divider_data.xsl" />

	<!-- Part 1 divider section BEGIN -->
	<xsl:template name="dividers_part1">
		<fo:page-sequence master-reference="front_page">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<!--<xsl:call-template name="header"/>-->
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="24pt">
				<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="18pt" wrap-option="wrap" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell height="26cm" width="19cm" border="1px double black" text-align="center">
									<fo:block padding-before="5cm">
										<fo:table inline-progression-dimension="19cm" table-layout="fixed" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
											<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
											<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
											<fo:table-header>
												<fo:table-row>
													<fo:table-cell number-columns-spanned="2" padding="1cm">
														<fo:block>Part 1 : Summary Data</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-header>
											<fo:table-body>
												<!-- 
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/Well/Well" />
											      <xsl:with-param name="label" select="'General Well Details'" />
											   </xsl:call-template>
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="//dvd" />
											      <xsl:with-param name="label" select="'DVD'" />	
											   </xsl:call-template>
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/HseIncident/HseIncident" />
											      <xsl:with-param name="label" select="'HSE Performance'" />	
											   </xsl:call-template>
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/GeneralComment/GeneralComment" />
											      <xsl:with-param name="label" select="'General Comments'" />	
											   </xsl:call-template>
											   	-->		
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	<!-- Part 1 divider section END -->

</xsl:stylesheet>