<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Divider data START -->
	<xsl:template name="divider" >
	   <xsl:param name="node" />
	   <xsl:param name="label" />
	   <xsl:if test="$node">
	   <fo:table-row font-size="14pt">
			<fo:table-cell text-align="right" padding="0.1cm">
				<fo:block> - </fo:block>
			</fo:table-cell>
			<fo:table-cell text-align="left" padding="0.1cm">
				<fo:block>
					<xsl:value-of select="$label"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
	</xsl:template>
	<!-- Divider data END -->
	
</xsl:stylesheet>