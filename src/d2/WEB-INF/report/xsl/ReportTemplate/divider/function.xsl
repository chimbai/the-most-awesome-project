<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template name="valueWithUnit" >
		<xsl:param name="value" />
	  	<xsl:param name="unit" />
	  	<xsl:if test="$value!=''">
	  		<xsl:value-of select="$value"/><xsl:value-of select="$unit"/>
	  	</xsl:if>
	</xsl:template>
	
	<xsl:template name="unitHeader" >
	  	<xsl:param name="unit" />
	  	<xsl:if test="$unit">
	  		(<xsl:value-of select="$unit"/>)
	  	</xsl:if>
	</xsl:template>		
</xsl:stylesheet>