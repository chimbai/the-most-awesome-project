<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	<xsl:template match="/root/modules/Activity" mode="timebreak">	
	
		<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-before="2pt" border="0.5px solid black">
				<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(15)"/>
				
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell padding="0.07cm" border-bottom="0.5px solid black" number-columns-spanned="5">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							TIME BREAKDOWN
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							PROD
						</fo:block>
					</fo:table-cell>					
				
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							NPT
						</fo:block>
					</fo:table-cell>					
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							WOW
						</fo:block>
					</fo:table-cell>					
					<fo:table-cell padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							TOTAL
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							Cumulative Hours for this well
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-size="8pt" color="#0000FF">
							<xsl:value-of select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='SDFN' or name='MAD' or name='POT' or name='CCG' or name='POE' or name='PRU' or name='PUO']/duration)), '##0.00')"/>  
						</fo:block>
					</fo:table-cell>					
				
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-size="8pt" color="#FF0000">
							<xsl:value-of select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='WOE' or name='WOP' or name='NDP']/duration)), '##0.00')"/>
						</fo:block>
					</fo:table-cell>					
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-size="8pt" color="#FF0000">
							<xsl:value-of select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='WOW' or name='NEP' or name='NUE']/duration)), '##0.00')"/>
						</fo:block>
					</fo:table-cell>					
					<fo:table-cell padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							<xsl:variable name ="sumProd" select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='SDFN' or name='MAD' or name='POT' or name='CCG' or name='POE' or name='PRU' or name='PUO']/duration)), '##0.00')"/>
							<xsl:variable name = "sumNpt" select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='WOE' or name='WOP' or name='NDP']/duration)), '##0.00')"/>
							<xsl:variable name = "sumWow" select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='WOW' or name='NEP' or name='NUE']/duration)), '##0.00')"/>
							
							<xsl:value-of select="d2Utils:formatNumber(string($sumProd + $sumWow +$sumNpt), '##0.00')"/>
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							Cumulative Days for this well
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm">
						<fo:block text-align="center" font-size="8pt" color="#0000FF">
							<xsl:value-of select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='SDFN' or name='MAD' or name='POT' or name='CCG' or name='POE' or name='PRU' or name='PUO']/duration) div 24), '##0.00')"/>  
						</fo:block>
					</fo:table-cell>					
				
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm">
						<fo:block text-align="center" font-size="8pt" color="#FF0000">
							<xsl:value-of select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='WOE' or name='WOP' or name='NDP']/duration) div 24), '##0.00')"/>
						</fo:block>
					</fo:table-cell>					
					<fo:table-cell border-right="0.5px solid black" padding="0.07cm">
						<fo:block text-align="center" font-size="8pt" color="#FF0000">
							<xsl:value-of select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='WOW' or name='NEP' or name='NUE']/duration) div 24), '##0.00')"/>
						</fo:block>
					</fo:table-cell>					
					<fo:table-cell padding="0.07cm">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							<xsl:variable name="divProd" select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='SDFN' or name='MAD' or name='POT' or name='CCG' or name='POE' or name='PRU' or name='PUO']/duration) div 24), '##0.00')"/>  
							<xsl:variable name="divNpt" select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='WOE' or name='WOP' or name='NDP']/duration) div 24), '##0.00')"/>
							<xsl:variable name="divWow" select="d2Utils:formatNumber(string(sum(/root/modules/OperationActivityDurationByTaskCodeToNextDay0600/code[name='WOW' or name='NEP' or name='NUE']/duration) div 24), '##0.00')"/>
							
							<xsl:value-of select="d2Utils:formatNumber(string($divProd + $divNpt + $divWow), '##0.00')"/>
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>				
			</fo:table-header>
			<fo:table-body>
			</fo:table-body>
		</fo:table>	
	</xsl:template>
		
</xsl:stylesheet>