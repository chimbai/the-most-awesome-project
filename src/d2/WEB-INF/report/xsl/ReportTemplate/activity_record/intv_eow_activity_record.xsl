<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- IDS INTERVENTION REPORT ACTIVITY SECTION -->
	<xsl:template match="modules/ReportDaily/ReportDaily" mode="eow_activity">
		<xsl:variable name="currentdailyuid" select="dailyUid"/>
		<!-- TO GET CURRENT DAY DATE -->		
		<fo:table-row space-after="6pt">
			<fo:table-cell padding="0.05cm" number-columns-spanned="7">
				<fo:block background-color="rgb(224,224,224)">
					<fo:inline font-weight="bold">
					   <xsl:value-of select="d2Utils:formatDateFromEpochMS(dynaAttr/daydate/@epochMS,'dd MMM yyyy')"/>
					</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
		<!-- DISPLAY ACTIVITY RECORD FILTER BY SELECTED DAILY_UID -->
		<xsl:apply-templates select="/root/modules/Activity/Activity[dailyUid = $currentdailyuid][dayPlus=0]" mode="activity_record"/>
		
		<!-- TOTAL DURATION FOR ACTIVITY -->
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm" number-columns-spanned="2">
				<fo:block text-align="center" background-color="rgb(224,224,224)">
					TOTALS
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center" background-color="rgb(224,224,224)">
					<xsl:value-of select="d2Utils:formatNumber(string(sum(/root/modules/Activity/Activity[dailyUid = $currentdailyuid][dayPlus=0]/activityDuration/@rawNumber)), '#.##')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center" background-color="rgb(224,224,224)">					
					<xsl:value-of select="d2Utils:formatNumber(string(sum(/root/modules/Activity/Activity[dailyUid = $currentdailyuid and (classCode='TP' or classCode='TU')][dayPlus=0]/activityDuration/@rawNumber)), '#.##')"/>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center" background-color="rgb(224,224,224)">					
					<fo:inline color="rgb(224,224,224)">.</fo:inline>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center" background-color="rgb(224,224,224)">
					<fo:inline color="rgb(224,224,224)">.</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block background-color="rgb(224,224,224)">
					<fo:inline color="rgb(224,224,224)">.</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="modules/Activity/Activity" mode="activity_record">				
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS = '86400000'">
							<fo:inline>24:00</fo:inline>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/>
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatNumber(string(activityDuration), '0.00')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center" color="red">
					<xsl:if test="contains(classCode, 'TP') or contains(classCode, 'TU')">
						<xsl:value-of select="d2Utils:formatNumber(string(activityDuration), '0.00')"/>
					</xsl:if>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="taskCode"/>
				</fo:block>
				<xsl:if test="contains(classCode, 'TP') or contains(classCode, 'TU')">
					<fo:block text-align="center" color="red"> (<xsl:value-of select="rootCauseCode"/>)
					</fo:block>
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="classCode"/>
				</fo:block>				
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block linefeed-treatment="preserve">
					<xsl:value-of select="activityDescription"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
</xsl:stylesheet>