<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
		
	<xsl:template match="modules/PerformanceSummary/PerformanceSummary">
        <fo:table space-before="2pt" width="100%" table-layout="fixed" border="0.25px solid black" font-size="8pt" margin-top="2pt">
            <fo:table-column column-width="proportional-column-width(20)"/>
            <fo:table-column column-width="proportional-column-width(20)"/>
            <fo:table-column column-width="proportional-column-width(20)"/>
            <fo:table-column column-width="proportional-column-width(20)"/>
            <fo:table-column column-width="proportional-column-width(20)"/>
            <fo:table-header>
                <fo:table-row>
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" background-color="rgb(223,223,223)" font-size="8pt" font-weight="bold" number-columns-spanned="5">
                        <fo:block>Performance Summary</fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-header>
            <fo:table-body>
				<fo:table-row>
					<fo:table-cell border-left="0.25px solid black" border-right="0.25px solid black" border-top="0.25px solid black" padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" text-align="center">
						<fo:block></fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" background-color="rgb(223,223,223)" padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" text-align="center" number-columns-spanned="2">
						<fo:block>Daily</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" background-color="rgb(223,223,223)" padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" text-align="center" number-columns-spanned="2">
						<fo:block>Cumulative Well</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border-left="0.25px solid black" border-right="0.25px solid black" border-bottom="0.25px solid black" padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" text-align="center">
						<fo:block></fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" text-align="center">
						<fo:block>Hrs</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" text-align="center">
						<fo:block>%</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" text-align="center">
						<fo:block>Hrs</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" text-align="center">
						<fo:block>%</fo:block>
					</fo:table-cell>
				</fo:table-row>	
				
                <xsl:apply-templates select="CumulativeDurationByClass[classCode!='']"/>
                <xsl:apply-templates select="CumulativeDurationByClass[classCode='']"/>
                
                <fo:table-row font-size="8pt">
			<fo:table-cell padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" border="0.25px solid black" text-align="center" background-color="rgb(223,223,223)">
				<fo:block>				
					Total
				</fo:block>
			</fo:table-cell>
			<!-- TOTAL DAILY HOURS -->
			<fo:table-cell padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:value-of select="/root/modules/PerformanceSummary/PerformanceSummary/dailyTotalDuration"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					100.0
				</fo:block>
			</fo:table-cell>
			<!-- TOTAL CUMULATIVE HOURS -->			
			<fo:table-cell padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" border="0.25px solid black" text-align="center">
				<fo:block>
					<xsl:value-of select="/root/modules/PerformanceSummary/PerformanceSummary/cumulativeTotalDuration"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					100.0
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>	
	
	<xsl:template match="CumulativeDurationByClass"> 
		<xsl:variable name="name" select="classCode"/>
		<xsl:variable name="name_hr" select="/root/modules/PerformanceSummary/PerformanceSummary/DailyDurationByClass[classCode=$name]/duration"/>
		<xsl:variable name="daily_total" select="/root/modules/PerformanceSummary/PerformanceSummary/dailyTotalDuration"/>
		<xsl:variable name="name_cm" select="/root/modules/PerformanceSummary/PerformanceSummary/CumulativeDurationByClass[classCode=$name]/duration"/>
		<xsl:variable name="cumulative_total" select="/root/modules/PerformanceSummary/PerformanceSummary/cumulativeTotalDuration"/>
		
		<fo:table-row font-size="8pt">
			 <fo:table-cell border="0.25px solid black" padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" text-align="center">
				<fo:block>
					
					<xsl:choose>
						<xsl:when test="$name !=''">
							<xsl:value-of select="$name"/>
						</xsl:when>
						<xsl:otherwise>
							Undefined
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			 </fo:table-cell>
			<!-- CLASS - DAILY -->
			
			<fo:table-cell padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" border="0.25px solid black" text-align="center">
				<fo:block>
					<xsl:choose>
						<xsl:when test="$name_hr &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber($name_hr,'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" border="0.25px solid black" text-align="center">
				<fo:block>
					<xsl:choose>
						<xsl:when test="$name_hr &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber(string($name_hr div $daily_total * 100),'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<!-- CLASS - CUMULATIVE -->
			<fo:table-cell padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="$name_cm &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber($name_cm,'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.075cm" padding-start="0.075cm" padding-end="0.075cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="$name_cm &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber(string($name_cm div $cumulative_total * 100),'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>	    

</xsl:stylesheet>