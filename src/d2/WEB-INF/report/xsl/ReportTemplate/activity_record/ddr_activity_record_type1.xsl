<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TEMPLATE: modules/Activity -->
	<xsl:template match="modules/Activity">
	
		<xsl:if test="Activity/dayPlus = 0 or string(dayPlus)=''">
		
			<fo:block keep-together="always">
			
				<!-- TABLE -->
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black" space-after="6pt">
					<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(6)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(5)"/>
					<fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
					<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
					<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
					<fo:table-column column-number="7" column-width="proportional-column-width(11)"/>
					<fo:table-column column-number="8" column-width="proportional-column-width(58)"/>
					
					<!-- HEADER -->
					<fo:table-header>
						
						<!-- HEADER 1 -->
						<fo:table-row>
							<fo:table-cell number-columns-spanned="8" padding="2px" background-color="#DDDDDD" border="thin solid black" display-align="center">
								<fo:block font-weight="bold" font-size="8pt">
									Operations for Period 0000 Hrs to 2400 Hrs on <xsl:value-of select="/root/modules/Daily/Daily/dayDate"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<!-- HEADER 2 -->
						<fo:table-row>						
							<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black">
								<fo:block text-align="center">
									PHSE
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									CLS
								</fo:block>
								<fo:block text-align="center">
									(RC)
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									OP
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									From
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									To
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									Hrs
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									Depth 
								</fo:block>
								<fo:block text-align="center">
									(<xsl:value-of select="/root/modules/Activity/Activity/depthMdMsl/@uomSymbol"/>)
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center">
									Activity Description
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
					</fo:table-header>
					
					<!-- BODY -->
					<fo:table-body>
						<xsl:apply-templates select="Activity[(string(dayPlus)='' or dayPlus=0) and (string(isSimop)='' or isSimop=0 or string(isSimop)='false') and (string(isOffline)='' or isOffline=0 or string(isOffline)='false')]" mode="currentday"/>
					</fo:table-body>
					
				</fo:table>
				
			</fo:block>
			
		</xsl:if>
			
		<!-- IMPORTANT - This will check on last day, if user enter NextDayActivity and no NextDay exists. -->
		<xsl:if	test="Activity/dayPlus = 1">
			
			<fo:block keep-together="always">
			
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black" space-after="6pt">>
					<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(6)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(5)"/>
					<fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
					<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
					<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
					<fo:table-column column-number="7" column-width="proportional-column-width(8)"/>
					<fo:table-column column-number="8" column-width="proportional-column-width(61)"/>
					
					<!-- HEADER -->
					<fo:table-header>
					
						<!-- HEADER 1 -->
						<fo:table-row>
							<fo:table-cell number-columns-spanned="8" padding="2px" background-color="#DDDDDD" border="thin solid black" display_align="center">
								<xsl:variable name="date" select="string(/root/modules/Daily/Daily/dayDate/@epochMS + 86400000)"/>
								<fo:block font-weight="bold" font-size="8pt">
									Operations for Period 0000 Hrs to 0600 Hrs on <xsl:value-of select="d2Utils:formatDateFromEpochMS($date,'dd MMM yyyy')"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<!-- HEADER 2 -->
						<fo:table-row>						
							<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center">
									PHSE
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center">
									CLS
								</fo:block>
								<fo:block text-align="center">
									(RC)
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center">
									OP
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center">
									From
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center">
									To
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center">
									Hrs
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center">
									Depth
								</fo:block>
								<fo:block text-align="center">
									(<xsl:value-of select="/root/modules/Activity/Activity/depthMdMsl/@uomSymbol"/>)
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center">
									Activity Description
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
					</fo:table-header>
					
					<!-- BODY -->
					<fo:table-body>
						<xsl:apply-templates select="Activity[dayPlus=1 and d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HHmm'))  &lt; d2Utils:parseDateAsEpochMS('0600') and (string(isSimop)='' or isSimop=0 or string(isSimop)='false') and (string(isOffline)='' or isOffline=0 or string(isSimop)='isOffline')]" mode="nextday"/>		
					</fo:table-body>
					
				</fo:table>
				
			</fo:block>
			
		</xsl:if>
		
	</xsl:template>
	
	<!-- This section is for Current day activity-->
	<xsl:template match="Activity" mode="currentday">
		
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="phaseCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">					
							<xsl:value-of select="classCode"/>					
						</fo:block>
						<xsl:if test="string(rootCauseCode) != ''">
							<fo:block text-align="center">					
								(<xsl:value-of select="rootCauseCode"/>)					
							</fo:block>	
						</xsl:if>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="taskCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>
					      		<xsl:when test="startDatetime/@epochMS = '86400000'">
							       <fo:inline>24:00</fo:inline>
					      		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
					    </fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>
					      		<xsl:when test="endDatetime/@epochMS = '86400000'">
							       <fo:inline>24:00</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
					     </fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="d2Utils:formatNumber(activityDuration,'##0.00')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="depthMdMsl"/>
							<fo:inline color="white">.</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="additionalRemarks"/>
						</fo:block>
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="activityDescription"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="phaseCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">					
							<xsl:value-of select="classCode"/>					
						</fo:block>
						<xsl:if test="string(rootCauseCode) != ''">
							<fo:block text-align="center">					
								(<xsl:value-of select="rootCauseCode"/>)					
							</fo:block>	
						</xsl:if>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="taskCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>
					      		<xsl:when test="startDatetime/@epochMS = '86400000'">
							       <fo:inline>24:00</fo:inline>
					      		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
					    </fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>
					      		<xsl:when test="endDatetime/@epochMS = '86400000'">
							       <fo:inline>24:00</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
					     </fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="d2Utils:formatNumber(activityDuration,'##0.00')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="depthMdMsl"/>
							<fo:inline color="#EEEEEE">.</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="additionalRemarks"/>
						</fo:block>
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="activityDescription"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>
	
	<!-- This section will call when it is on last day. Logic to call this applied above -->
	<xsl:template match="Activity" mode="nextday">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
				<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="phaseCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">					
							<xsl:value-of select="classCode"/>					
						</fo:block>
						<xsl:if test="string(rootCauseCode) != ''">
							<fo:block text-align="center">					
								(<xsl:value-of select="rootCauseCode"/>)					
							</fo:block>	
						</xsl:if>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="taskCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>
								<xsl:when test="d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HHmm')) &gt; d2Utils:parseDateAsEpochMS('0600')">
									<xsl:value-of select="d2Utils:formatDateFromEpochMS(string(d2Utils:parseDateAsEpochMS('0600')),'HH:mm')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/>
								</xsl:otherwise>
							</xsl:choose>					
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>
								<xsl:when test="d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HHmm')) &gt; d2Utils:parseDateAsEpochMS('0600')">
									<xsl:variable name="duration" select="d2Utils:parseDateAsEpochMS('0600') - d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HHmm'))"/>
									 <xsl:value-of select="d2Utils:formatNumber(string($duration div (60*60*1000)),'##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="d2Utils:formatNumber(activityDuration,'##0.00')"/>
								</xsl:otherwise>
							</xsl:choose>					
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">					
							<xsl:value-of select="depthMdMsl"/>
							<fo:inline color="#EEEEEE">.</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="additionalRemarks"/>
						</fo:block>
						<fo:block linefeed-treatment="preserve">
							<xsl:choose>
								<xsl:when test="d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HHmm')) &gt; d2Utils:parseDateAsEpochMS('0600')">
									<xsl:value-of select="'[In Progress] '"/><xsl:value-of select="activityDescription"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="activityDescription"/>
								</xsl:otherwise>
							</xsl:choose>		
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="phaseCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">					
							<xsl:value-of select="classCode"/>					
						</fo:block>
						<xsl:if test="string(rootCauseCode) != ''">
							<fo:block text-align="center">					
								(<xsl:value-of select="rootCauseCode"/>)					
							</fo:block>	
						</xsl:if>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="taskCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>
								<xsl:when test="d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HHmm')) &gt; d2Utils:parseDateAsEpochMS('0600')">
									<xsl:value-of select="d2Utils:formatDateFromEpochMS(string(d2Utils:parseDateAsEpochMS('0600')),'HH:mm')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/>
								</xsl:otherwise>
							</xsl:choose>					
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:choose>
								<xsl:when test="d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HHmm')) &gt; d2Utils:parseDateAsEpochMS('0600')">
									<xsl:variable name="duration" select="d2Utils:parseDateAsEpochMS('0600') - d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HHmm'))"/>
									 <xsl:value-of select="d2Utils:formatNumber(string($duration div (60*60*1000)),'##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="d2Utils:formatNumber(activityDuration,'##0.00')"/>
								</xsl:otherwise>
							</xsl:choose>					
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">					
							<xsl:value-of select="depthMdMsl"/>
							<fo:inline color="#EEEEEE">.</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="additionalRemarks"/>
						</fo:block>
						<fo:block linefeed-treatment="preserve">
							<xsl:choose>
								<xsl:when test="d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HHmm')) &gt; d2Utils:parseDateAsEpochMS('0600')">
									<xsl:value-of select="'[In Progress] '"/><xsl:value-of select="activityDescription"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="activityDescription"/>
								</xsl:otherwise>
							</xsl:choose>		
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
</xsl:stylesheet>