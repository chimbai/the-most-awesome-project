<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Activity Header START -->
	<xsl:template name="activity_header">
		<fo:table-row>						
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">PHSE</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">CLS</fo:block>
				<fo:block text-align="center">(RC)</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">OP</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">From</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">To</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Hrs</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Depth</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Activity Description</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Activity Header END -->
	
</xsl:stylesheet>