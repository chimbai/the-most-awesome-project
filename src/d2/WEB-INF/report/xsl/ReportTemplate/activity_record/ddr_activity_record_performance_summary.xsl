<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
		
	<xsl:template match="modules/PerformanceSummary">
        <fo:table space-before="2pt" width="100%" table-layout="fixed" border="0.25px solid black" font-size="8pt" margin-top="2pt">
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>            
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(10)"/>
            <fo:table-column column-width="proportional-column-width(15)"/>
            <fo:table-header>
                <fo:table-row>
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" background-color="rgb(223,223,223)" font-size="10pt" font-weight="bold" number-columns-spanned="17">
                        <fo:block>Performance Summary</fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-header>
            <fo:table-body>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center" number-columns-spanned="8">
						<fo:block>Daily</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center" number-columns-spanned="9">
						<fo:block>Cumulative Well</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center" number-columns-spanned="2">
						<fo:block>P</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center" number-columns-spanned="2">
						<fo:block>TP</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center" number-columns-spanned="2">
						<fo:block>TU</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center" number-columns-spanned="2">
						<fo:block>U</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center" number-columns-spanned="2">
						<fo:block>P</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center" number-columns-spanned="2">
						<fo:block>TP</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center" number-columns-spanned="2">
						<fo:block>TU</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center" number-columns-spanned="2">
						<fo:block>U</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>Total</fo:block>
					</fo:table-cell>
				</fo:table-row>					
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>Hrs <xsl:value-of select="d_P"/> </fo:block> 
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>%</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>Hrs</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>%</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>Hrs</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>%</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>Hrs</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>%</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>Hrs</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>%</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>Hrs</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>%</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>Hrs</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>%</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>Hrs</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" text-align="center">
						<fo:block>%</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.25px solid black" border="0.25px solid black" border-bottom="0.25px solid black" padding-before="0.1cm" text-align="center">
						<fo:block>Hours</fo:block>
					</fo:table-cell>
				</fo:table-row>			
                <xsl:apply-templates select="PerformanceSummary"/>
            </fo:table-body>
        </fo:table>
    </xsl:template>	
	
	<xsl:template match="PerformanceSummary"> 
		<xsl:variable name="daily_total" select="dailyTotalDuration"/>
		<xsl:variable name="cumulative_total" select="cumulativeTotalDuration"/>
		<fo:table-row font-size="9pt">
			<!-- P CLASS - DAILY -->
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>
					<xsl:choose>
						<xsl:when test="DailyDurationByClass[classCode='P']/duration &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber(string(DailyDurationByClass[classCode='P']/duration),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							0.00	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>
					<xsl:choose>
						<xsl:when test="($daily_total &gt; 0) and (DailyDurationByClass[classCode='P']/duration &gt; 0)">
							<xsl:value-of select="d2Utils:formatNumber(string(DailyDurationByClass[classCode='P']/duration div $daily_total * 100),'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			
			<!-- TP CLASS - DAILY -->
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>
					<xsl:choose>
						<xsl:when test="DailyDurationByClass[classCode='TP']/duration &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber(string(DailyDurationByClass[classCode='TP']/duration),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							0.00	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>
					<xsl:choose>
						<xsl:when test="($daily_total &gt; 0) and (DailyDurationByClass[classCode='TP']/duration &gt; 0)">
							<xsl:value-of select="d2Utils:formatNumber(string(DailyDurationByClass[classCode='TP']/duration div $daily_total * 100),'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			
			<!-- TU CLASS - DAILY -->
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="DailyDurationByClass[classCode='TU']/duration &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber(string(DailyDurationByClass[classCode='TU']/duration),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							0.00	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>
					<xsl:choose>
						<xsl:when test="($daily_total &gt; 0) and (DailyDurationByClass[classCode='TU']/duration &gt; 0)">
							<xsl:value-of select="d2Utils:formatNumber(string(DailyDurationByClass[classCode='TU']/duration div $daily_total * 100),'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			
			<!-- U CLASS - DAILY -->
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="DailyDurationByClass[classCode='U']/duration &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber(string(DailyDurationByClass[classCode='U']/duration),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							0.00	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="($daily_total &gt; 0) and (DailyDurationByClass[classCode='U']/duration &gt; 0)">
							<xsl:value-of select="d2Utils:formatNumber(string(DailyDurationByClass[classCode='U']/duration div $daily_total * 100),'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			
			<!-- P CLASS - CUMULATIVE -->
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="CumulativeDurationByClass[classCode='P']/duration &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber(string(CumulativeDurationByClass[classCode='P']/duration),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							0.00	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="($cumulative_total &gt; 0) and (CumulativeDurationByClass[classCode='P']/duration &gt; 0)">
							<xsl:value-of select="d2Utils:formatNumber(string(CumulativeDurationByClass[classCode='P']/duration div $cumulative_total * 100),'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			
			<!-- TP CLASS - CUMULATIVE -->
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="CumulativeDurationByClass[classCode='TP']/duration &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber(string(CumulativeDurationByClass[classCode='TP']/duration),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							0.00	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="($cumulative_total &gt; 0) and (CumulativeDurationByClass[classCode='TP']/duration &gt; 0)">
							<xsl:value-of select="d2Utils:formatNumber(string(CumulativeDurationByClass[classCode='TP']/duration div $cumulative_total * 100),'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			
			<!-- TU CLASS - CUMULATIVE -->
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="CumulativeDurationByClass[classCode='TU']/duration &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber(string(CumulativeDurationByClass[classCode='TU']/duration),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							0.00	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="($cumulative_total &gt; 0) and (CumulativeDurationByClass[classCode='TU']/duration &gt; 0)">
							<xsl:value-of select="d2Utils:formatNumber(string(CumulativeDurationByClass[classCode='TU']/duration div $cumulative_total * 100),'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			
			<!-- U CLASS - CUMULATIVE -->
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="CumulativeDurationByClass[classCode='U']/duration &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber(string(CumulativeDurationByClass[classCode='U']/duration),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							0.00	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="($cumulative_total &gt; 0) and (CumulativeDurationByClass[classCode='U']/duration &gt; 0)">
							<xsl:value-of select="d2Utils:formatNumber(string(CumulativeDurationByClass[classCode='U']/duration div $cumulative_total * 100),'##0.0')"/>
						</xsl:when>
						<xsl:otherwise>
							0.0	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			
			<!-- TOTAL CUMULATIVE HOURS -->			
			<fo:table-cell padding="0.1cm" border="0.25px solid black" text-align="center">
				<fo:block>				
					<xsl:choose>
						<xsl:when test="$cumulative_total &gt; 0">
							<xsl:value-of select="d2Utils:formatNumber(string($cumulative_total),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							0.00	
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>	    
</xsl:stylesheet>