<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TODAY'S ACTIVITY SECTION 20080921 --> 

	<xsl:template match="modules/ActivityDetails0600"> 
		<fo:table width="100%" table-layout="fixed" text-align="center" font-size="8pt" wrap-option="wrap" space-before="2pt" space-after="2pt" margin-top="2pt">
			<fo:table-body>		
				<fo:table-row font-size="8pt">
					
					<fo:table-cell >
						<fo:block>			
							<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" border="0.5px solid black">
								<fo:table-column column-number="1" column-width="proportional-column-width(9)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(5)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(5)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
								<fo:table-column column-number="6" column-width="proportional-column-width(63)"/>
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell padding="0.07cm" number-columns-spanned="6">
											<fo:block text-align="center" font-weight="bold" font-size="8pt">
												ACTIVITY DETAILS
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								
									<fo:table-row>						
										<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
											<fo:block text-align="center" font-weight="bold">Date</fo:block>
										</fo:table-cell>
										<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
											<fo:block text-align="center" font-weight="bold">Time From</fo:block>
										</fo:table-cell>
										<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
											<fo:block text-align="center" font-weight="bold">Time To</fo:block>
										</fo:table-cell>
										<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
											<fo:block text-align="center" font-weight="bold">Code</fo:block>
										</fo:table-cell>
										<fo:table-cell border-top="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
											<fo:block text-align="center" font-weight="bold">HRS</fo:block>
										</fo:table-cell>
										<fo:table-cell border-top="0.5px solid black" padding="0.07cm" >
											<fo:block text-align="center" font-weight="bold">Activity Description</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								<fo:table-body>

<!--									<xsl:choose> -->
<!--										<xsl:when test = "/root/modules/PreviousDayActivity/Activity/carriedForwardActivityUid = Activity/activityUid">-->
<!--											<xsl:apply-templates select="Activity[d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HHmm')) &gt;= d2Utils:parseDateAsEpochMS('0600')]" mode="currentday"/>-->
<!--											<xsl:apply-templates select="/root/modules/NextDayActivity/Activity[d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HHmm')) &lt; d2Utils:parseDateAsEpochMS('0600')]" mode="nextday"/>-->
<!--										</xsl:when>-->
<!--										<xsl:otherwise>-->
<!--											<xsl:choose>-->
<!--												<xsl:when test ="Activity[carriedForwardActivityUid = '' and dayPlus=0]">-->
<!--													<xsl:apply-templates select="Activity" mode="currentday"/>-->
<!--													<xsl:apply-templates select="/root/modules/NextDayActivity/Activity[d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HHmm')) &lt; d2Utils:parseDateAsEpochMS('0600') and dayPlus='0']" mode="nextday"/>-->
<!--												</xsl:when>-->
<!--												<xsl:otherwise>-->
<!--													<xsl:if test ="Activity[carriedForwardActivityUid = '' and dayPlus=1]">-->
<!--														<xsl:apply-templates select="Activity" mode="currentday"/>-->
<!--														<xsl:apply-templates select="Activity[dayPlus =1]" mode="currentday"/>-->
<!--													</xsl:if>-->
<!--												</xsl:otherwise>-->
<!--											</xsl:choose>-->
<!--										</xsl:otherwise>-->
<!--									</xsl:choose>-->
									
<!--									<xsl:for-each select="/root/modules/PreviousDayActivity/Activity">-->
<!--										<xsl:if test="carriedForwardActivityUid = Activity/activityUid">-->
<!--											<xsl:apply-templates select="Activity[d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HHmm')) &gt;= d2Utils:parseDateAsEpochMS('0600')]" mode="currentday"/>-->
<!--											<xsl:apply-templates select="/root/modules/NextDayActivity/Activity[d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HHmm')) &lt; d2Utils:parseDateAsEpochMS('0600')]" mode="nextday"/>-->
<!--										</xsl:if>-->
<!--									</xsl:for-each>-->
												
										<xsl:apply-templates select="ActivityDetails" mode="currentday"/> 
	<!--									<xsl:apply-templates select="Activity[d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HHmm')) &gt;= d2Utils:parseDateAsEpochMS('0600') and dayPlus='0']" mode="currentday"/>-->
	<!--									<xsl:apply-templates select="/root/modules/NextDayActivity/Activity[d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HHmm')) &lt; d2Utils:parseDateAsEpochMS('0600') and dayPlus='0']" mode="nextday"/>-->
	<!--									<xsl:apply-templates select="Activity[dayPlus=1 and d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HHmm'))  &lt; d2Utils:parseDateAsEpochMS('0600') and (string(isSimop)='' or isSimop=0 or string(isSimop)='false') and (string(isOffline)='' or isOffline=0 or string(isSimop)='isOffline')]" mode="nextday"/>-->
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>	
	</xsl:template>
	
	
	<!-- CURRENT ACTIVITY RECORDS -->
	<xsl:template match="ActivityDetails" mode="currentday">
		<fo:table-row>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-top="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(start,'dd-MMM-yyyy')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-top="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(start,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-top="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(end,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-top="0.5px solid black">
				<xsl:if test="code='WOW' or code='WOP' or code='WOE' or code='NDP' or code='NEP' or code='NUE'">
					<fo:block text-align="center" color="#FF0000">
						<xsl:value-of select="code"/>  
					</fo:block>
				</xsl:if>
				<xsl:if test="code='POT' or code='CCG' or code='POE' or code='PRU' or code='PUO' or code='SDFN' or code='MAD'">
					<fo:block text-align="center" color="#0000FF">
						<xsl:value-of select="code"/>  
					</fo:block>
				</xsl:if>
				<xsl:if test="code!='WOW' and code!='WOP' and code!='WOE' and code!='NDP' and code!='NEP' and code!='NUE' and code!='POT' and code!='CCG' and code!='POE' and code!='PRU' and code!='PUO' and code!='SDFN' and code!='MAD'">
					<fo:block text-align="center">
						<xsl:value-of select="code"/>  
					</fo:block>
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-top="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatNumber(string(sum(duration) div 3600) ,'##0.00')"/>
				</fo:block>
			</fo:table-cell>

			<fo:table-cell padding="0.07cm"  border-top="0.5px solid black">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:value-of select="description"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<!-- NEXT DAY ACTIVITY RECORDS, IF CURRENT DAY IS THE LAST DAY OF OPERATION -->
	<xsl:template match="Activity" mode="nextday">
	<xsl:variable name="date" select="string(/root/modules/NextDayActivity/Activity/startDatetime/@epochMS )"/>
		<fo:table-row>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-top="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS($date,'dd-MMM-yyyy')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-top="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-top="0.5px solid black">
				<fo:block text-align="center">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS = '86400000'">
							<fo:inline>24:00</fo:inline>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-top="0.5px solid black">
				<xsl:if test="taskcode='WOW' or taskcode='WOP' or taskcode='WOE' or taskcode='NDP' or taskcode='NEP' or taskcode='NUE'">
					<fo:block text-align="center" color="#FF0000">
						<xsl:value-of select="taskcode"/>  
					</fo:block>
				</xsl:if>
				<xsl:if test="taskcode='POT' or taskcode='CCG' or taskcode='POE' or taskcode='PRU' or taskcode='PUO' or taskcode='SDFN' or taskcode='MAD'">
					<fo:block text-align="center" color="#0000FF">
						<xsl:value-of select="taskcode"/>  
					</fo:block>
				</xsl:if>
				<xsl:if test="taskcode!='WOW' and taskcode!='WOP' and taskcode!='WOE' and taskcode!='NDP' and taskcode!='NEP' and taskcode!='NUE' and taskcode!='POT' and taskcode!='CCG' and taskcode!='POE' and taskcode!='PRU' and taskcode!='PUO' and taskcode!='SDFN' and taskcode!='MAD'">
					<fo:block text-align="center">
						<xsl:value-of select="taskcode"/>  
					</fo:block>
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" border-top="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatNumber(activityDuration,'##0.00')"/>					
				</fo:block>
			</fo:table-cell>

			<fo:table-cell padding="0.07cm" border-top="0.5px solid black">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:choose>
						<xsl:when test="d2Utils:parseDateAsEpochMS(d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HHmm')) &gt; d2Utils:parseDateAsEpochMS('0600')">
							<xsl:value-of select="'(IN PROGRESS) '"/><xsl:value-of select="activityDescription"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="activityDescription"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
</xsl:stylesheet>