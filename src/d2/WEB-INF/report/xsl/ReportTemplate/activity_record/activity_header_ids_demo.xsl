<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- NEW XSL FOR ACTIVITY AND NEXT DAY ACTIVITY SECTION HEADER 20080911 -->
	<xsl:template name="activity_header">
		<fo:table-row>						
			<fo:table-cell border-bottom="0.5px solid black" padding="2px">
				<fo:block text-align="center">Phse</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" padding="2px">
				<fo:block text-align="center">Cls</fo:block>
				<fo:block text-align="center">(RC)</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" padding="2px">
				<fo:block text-align="center">Op</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" padding="2px">
				<fo:block text-align="center">From</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" padding="2px">
				<fo:block text-align="center">To</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" padding="2px">
				<fo:block text-align="center">Hrs</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" padding="2px">
				<fo:block text-align="center">Depth</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" padding="2px">
				<fo:block text-align="center">Activity Description</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
		
</xsl:stylesheet>