<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Next day activity record START -->
	<xsl:template match="Activity" mode="nextday">
		<fo:table-row keep-together="always">
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="phaseCode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">					
					<xsl:value-of select="classCode"/>					
				</fo:block>
				<xsl:if test="string(rootCauseCode) != ''">
				<fo:block text-align="center">					
					(<xsl:value-of select="rootCauseCode"/>)					
				</fo:block>	
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="taskCode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS &gt; d2Utils:parseDateAsEpochMS('0600')">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(string(d2Utils:parseDateAsEpochMS('0600')),'HH:mm')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS &gt; d2Utils:parseDateAsEpochMS('0600')">
							<xsl:variable name="duration" select="d2Utils:parseDateAsEpochMS('0600') - startDatetime/@epochMS"/>
							<xsl:value-of select="d2Utils:formatNumber(string($duration div (60*60*1000)),'##0.00')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="d2Utils:formatNumber(activityDuration,'##0.00')"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">					
					<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="depthMdMsl/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS &gt; d2Utils:parseDateAsEpochMS('0600')">
							<xsl:value-of select="'(IN PROGRESS) '"/><xsl:value-of select="activityDescription"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="activityDescription"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Next day activity record END -->
	
</xsl:stylesheet>