<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!--D2_OMV_GLOBAL DDR NEXT DAY ACTIVITY SECTION  -->
	
	<xsl:import href="./activity_header_ids_demo.xsl" />
	<xsl:import href="./activity_record_next_day_ids_demo.xsl" />
		
	<!-- THIS SECTION WILL BE PRINTED IF NEXT DAY ACTIVITY DOES EXIST, AND CURRENT DAY IS NOT LAST DAY -->
	<xsl:template match="modules/NextDayActivity">
		<xsl:variable name="date" select="string(/root/modules/Daily/Daily/dayDate/@epochMS + 86400000)"/>
		<fo:block font-weight="bold" font-size="8pt">    
			Operations for Period 0000 Hrs to 0600 Hrs on <xsl:value-of select="d2Utils:formatDateFromEpochMS($date,'dd MMM yyyy')"/>
		</fo:block>
		<fo:block>			
			<fo:table inline-progression-dimension="19cm" table-layout="fixed" table-omit-header-at-break="false" wrap-option="wrap" border-before-width="0.5px" 
				border-before-style="solid" border-before-width.conditionality="retain" border-after-width="0.5px" border-after-style="solid" border-after-width.conditionality="retain"
				border="0.5px solid black" margin-top="2pt" margin-bottom="5pt" margin-bottom.conditionality="retain">
				<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(50)"/>
				<fo:table-header>
					<xsl:call-template name="activity_header"/>
				</fo:table-header>
				<fo:table-body>
					<xsl:apply-templates select="Activity[startDatetime/@epochMS &lt; d2Utils:parseDateAsEpochMS('0600') and dayPlus/@rawNumber='0']" mode="nextday"/>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>
		
</xsl:stylesheet>