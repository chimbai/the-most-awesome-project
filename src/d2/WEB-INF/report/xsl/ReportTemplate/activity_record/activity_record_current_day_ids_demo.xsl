<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- D2_OMV_GLOBAL DDR CURRENT DAY ACTIVITY RECORD -->
	
	<xsl:template match="Activity" mode="currentday">
		<fo:table-row>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="center">
					<xsl:value-of select="phaseCode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="center">					
					<xsl:value-of select="classCode"/>					
				</fo:block>
				<xsl:if test="string(rootCauseCode) != ''">
				<fo:block text-align="center">					
					(<xsl:value-of select="rootCauseCode"/>)					
				</fo:block>	
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="center">
					<xsl:value-of select="taskCode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="center">
					<xsl:choose>
						<xsl:when test="endDatetime/@epochMS = '86400000'">
							<fo:inline>24:00</fo:inline>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/>
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatNumber(activityDuration,'##0.00')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="center">
					<xsl:value-of select="depthMdMsl"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="depthMdMsl/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:value-of select="activityDescription"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
</xsl:stylesheet>