<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TEMPLATE: modules/UnwantedEvent -->
	<xsl:template match="modules/UnwantedEvent">
	
		<fo:block keep-together="always">
		
			<!-- TABLE -->
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black" space-after="6pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(17)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(17)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(14)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(13)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(35)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(15)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(30)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(13)"/>
				
				<!-- HEADER -->
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell number-columns-spanned="8" padding="2px" background-color="#DDDDDD" border="thin solid black" display-align="center">
							<fo:block font-size="8pt" font-weight="bold">
								Unwanted Event
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				
				<!-- BODY -->
				<fo:table-body>
					<xsl:apply-templates select="UnwantedEvent"/>				
				</fo:table-body>
			</fo:table>
			
		</fo:block>
		
	</xsl:template>
	
	<!-- TEMPLATE: UnwantedEvent -->
	<xsl:template match="UnwantedEvent">
	
		<xsl:variable name="actdailyid" select="dailyUid"/>	
		<xsl:variable name="selected_date" select="/root/modules/Daily/Daily[dailyUid=$actdailyid]/dayDate"/>
		
		<fo:table-row>	
			<fo:table-cell number-columns-spanned="4" number-rows-spanned="5" padding="2px" border-left="thin solid black" border-right="thin solid black">	
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
					<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
					
					<fo:table-body>
					
						<fo:table-row>
							<xsl:choose>
								<xsl:when test="activityUid !=''">	
									<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
										<fo:block>
											Activity Date/Time:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="3" padding="1px" padding-left="2px" padding-right="2px">
										<fo:block>
											<xsl:value-of select="$selected_date"/>
										</fo:block>
									</fo:table-cell>
								</xsl:when>
								<xsl:otherwise>
									<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
										<fo:block>
											Date/Time:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="3" padding="1px" padding-left="2px" padding-right="2px">
										<fo:block text-align="left">
											<xsl:value-of select="eventDatetime"/>
										</fo:block>
									</fo:table-cell>
								</xsl:otherwise>
							</xsl:choose>
						</fo:table-row>
						
						<fo:table-row>	
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Event:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell number-columns-spanned="3" padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>					
									<xsl:value-of select="eventSummary"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
					</fo:table-body>
					
				</fo:table>
								
			</fo:table-cell>
			 
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>
					Report No:
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell number-columns-spanned="3" padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>					
					<xsl:value-of select="eventRef"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		 
		<fo:table-row>
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>
					Operations shut down (Yes/No):
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>
					<xsl:choose>
						<xsl:when test="operationShutdown = 'false'">
							<fo:block>
								No
							</fo:block>	
						</xsl:when>
						<xsl:otherwise>
							<fo:block>
								Yes
							</fo:block>
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>
					Environmental Discharge:
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>					
					<xsl:value-of select="envDischarge/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>				
		</fo:table-row>
		
		<fo:table-row>
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>
					NPT Duration:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>					
					<xsl:value-of select="nptDuration"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="nptDuration/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>
					Name of product spilled:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>					
					<xsl:value-of select="nameOfProductSpilled"/>					
				</fo:block>
			</fo:table-cell>			
		</fo:table-row>
		
		<fo:table-row>			
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>
					Estimated loss/repair cost:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>					
					<xsl:value-of select="estimatedLoss"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>
					Estimated quantity spilled:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
				<fo:block>					
					<xsl:value-of select="estimatedQtySpilled"/>
				</fo:block>
			</fo:table-cell>			
		</fo:table-row>
		
		<fo:table-row>			
			<fo:table-cell padding="2px" border-right="thin solid black">
				<fo:block>
					Name of Contractor:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="3" padding="2px" border-right="thin solid black">
				<fo:block>
					<xsl:choose>													
						<xsl:when test="string(nameOfContractor/@lookupLabel)=''">
							<xsl:value-of select="nameOfContractor"/>										
						</xsl:when>													
						<xsl:otherwise>
							<xsl:value-of select="nameOfContractor/@lookupLabel"/>
						</xsl:otherwise>
					</xsl:choose>										
				</fo:block>
			</fo:table-cell>			
		</fo:table-row>
		
	</xsl:template>
	
</xsl:stylesheet>