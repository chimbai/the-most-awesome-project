<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- NEW XSL FOR D2_CHOC_MY DDR UNWANTED EVENT SECTION 20080915 -->
	
	<xsl:template match="modules/UnwantedEvent">			                                            
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" margin-top="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(17)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(17)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(14)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(13)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(35)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="8" column-width="proportional-column-width(13)"/>
			<fo:table-header>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="0.1cm" number-columns-spanned="8">
						<fo:block font-size="10pt" font-weight="bold">
							Unwanted Event
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body keep-together="always">
				<xsl:apply-templates select="UnwantedEvent"/>				
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="UnwantedEvent">
		<xsl:variable name="actdailyid" select="dailyUid"/>	
		<xsl:variable name="selected_date" select="/root/modules/Daily/Daily[dailyUid=$actdailyid]/dayDate"/>
		<fo:table-row>	
			<fo:table-cell padding="0.07cm" border-top="0.5px solid black" number-columns-spanned="4" number-rows-spanned="5" border-right="0.5px solid black">	
				<fo:table width="100%" table-layout="fixed">
					<fo:table-column column-number="1" column-width="proportional-column-width(17)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(17)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(14)"/>
					<fo:table-column column-number="4" column-width="proportional-column-width(13)"/>
					<fo:table-body>
					<fo:table-row>
						<xsl:choose>
							<xsl:when test="activityUid !=''">	
								<fo:table-cell padding="0.07cm">
									<fo:block text-align="left">
										Activity Date/Time:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" number-columns-spanned="3">
									<fo:block text-align="left">
										<xsl:value-of select="$selected_date"/>
									</fo:block>
								</fo:table-cell>
							</xsl:when>
							<xsl:otherwise>
								<fo:table-cell padding="0.07cm">
									<fo:block text-align="left">
										Date/Time:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" number-columns-spanned="3">
									<fo:block text-align="left">
										<xsl:value-of select="eventDatetime"/>
									</fo:block>
								</fo:table-cell>
							</xsl:otherwise>
						</xsl:choose>
					</fo:table-row>	
					<fo:table-row>	
						<fo:table-cell padding="0.07cm">
							<fo:block text-align="left" >
								Event:
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.07cm" number-columns-spanned="3">
							<fo:block text-align="left">					
								<xsl:value-of select="eventSummary"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>	
					</fo:table-body>
				</fo:table>				
			</fo:table-cell>
			<fo:table-cell padding="0.07cm" border-top="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left">
					Report No:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.07cm" border-top="0.5px solid black" number-columns-spanned="3" border-bottom="0.5px solid black">
				<fo:block text-align="left" >					
					<xsl:value-of select="eventRef"/>
				</fo:block>
			</fo:table-cell>					
		</fo:table-row>
		<fo:table-row>		
			<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.05cm">
				<fo:block text-align="left">
					Operations shut down (Yes/No):
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.05cm">
				<fo:block text-align="left">
					<xsl:choose>
						<xsl:when test="operationShutdown = 'false'">
							<fo:block text-align="left">No</fo:block>	
						</xsl:when>
						<xsl:otherwise>
							<fo:block text-align="left">Yes</fo:block>
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black" padding="0.05cm">
				<fo:block text-align="left">
					Environmental Discharge:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" padding="0.05cm">
				<fo:block text-align="left">					
					<xsl:value-of select="envDischarge"/>
				</fo:block>
			</fo:table-cell>					
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell border-right="0.5px solid black" border-bottom="0.5px solid black" padding="0.07cm">
				<fo:block text-align="left">
					NPT Duration:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" border-bottom="0.5px solid black" padding="0.07cm">
				<fo:block text-align="left">					
					<xsl:value-of select="nptDuration"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" border-bottom="0.5px solid black" padding="0.07cm">
				<fo:block text-align="left">
					Name of product spilled:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm">
				<fo:block text-align="left">					
					<xsl:value-of select="nameOfProductSpilled"/>					
				</fo:block>
			</fo:table-cell>			
		</fo:table-row>
		<fo:table-row>			
			<fo:table-cell border-right="0.5px solid black" border-bottom="0.5px solid black" padding="0.07cm">
				<fo:block text-align="left">
					Estimated loss/repair cost:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" border-bottom="0.5px solid black" padding="0.07cm">
				<fo:block text-align="left">					
					<xsl:value-of select="estimatedLoss"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" border-bottom="0.5px solid black" padding="0.07cm">
				<fo:block text-align="left">
					Estimated quantity spilled:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm">
				<fo:block text-align="left">					
					<xsl:value-of select="estimatedQtySpilled"/>
				</fo:block>
			</fo:table-cell>			
		</fo:table-row>
		<fo:table-row>			
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm">
				<fo:block text-align="left">
					Name of Contractor:
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.07cm" number-columns-spanned="3">
				<fo:block text-align="left">
					<xsl:choose>													
						<xsl:when test = "string(nameOfContractor/@lookupLabel)=''">
							<xsl:value-of select="nameOfContractor"/>										
						</xsl:when>													
						<xsl:otherwise>
							<xsl:value-of select="nameOfContractor/@lookupLabel"/>
						</xsl:otherwise>
					</xsl:choose>										
				</fo:block>
			</fo:table-cell>			
		</fo:table-row>
	</xsl:template>
		
</xsl:stylesheet>