<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:import href="../divider/function.xsl" />
	
	<xsl:template name="summaryTable">	                                            
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" border-right="0.5px solid black" border-top="0.5px solid black" border-left="0.5px solid black"   border-bottom="0.5px solid black" font-size="8pt" vertical-align="middle">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)" />
			<fo:table-column column-number="2" column-width="proportional-column-width(75)" />
			<fo:table-body>
				<fo:table-row background-color="rgb(223,223,223)">
					<fo:table-cell padding="0.02cm" padding-top="0.05cm" padding-left="0.05cm" ><fo:block>Total NPT Hours: </fo:block></fo:table-cell>
					<fo:table-cell padding="0.02cm" padding-top="0.05cm"><fo:block><xsl:value-of select="format-number(sum(/root/modules/ActivityNptEvent/UnwantedEvent/Activity/activityDuration/@rawNumber), '###,###,##0.00')"/></fo:block></fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="ActivityNptEvent/UnwantedEvent">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed"  space-before="12pt" border-top="0.5px solid black" border-bottom="0.5px solid black" border-right="0.5px solid black" border-left="0.5px solid black"  font-size="8pt">
        	<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="left">Event #:</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="left"><xsl:value-of select="eventRef"/></fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="left">Status:</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="left"><xsl:value-of select="eventStatus/@lookupLabel"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="left">Start Date:</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="left"><xsl:value-of select="dynaAttr/eventDateTime"/></fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-left="0.5px solid black">
						<fo:block text-align="left">Total Hour:</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm">
						<fo:block text-align="left"><xsl:value-of select="nptDuration"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black">
						<fo:block text-align="left">Start Depth:</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black">
						<fo:block text-align="left">
							<xsl:call-template name="valueWithUnit">	
								<xsl:with-param name="value" select="dynaAttr/eventStartDepth" />
								<xsl:with-param name="unit" select="dynaAttr/eventStartDepth/@uomSymbol"/>
							</xsl:call-template>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block text-align="left">Severity:</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black">
						<fo:block text-align="left"><xsl:value-of select="eventSeverity/@lookupLabel"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black">
						<fo:block text-align="left">Comment:</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" number-columns-spanned="3">
						<fo:block text-align="left"><xsl:value-of select="eventSummary"/></fo:block>
					</fo:table-cell>
				</fo:table-row>	
			</fo:table-header>
			<fo:table-body>				
				<fo:table-row>
					<fo:table-cell number-columns-spanned="4">
						<fo:block>
							<fo:table inline-progression-dimension="19cm" table-layout="fixed"  border-top="0.5px solid black" border-right="0.5px solid black" border-left="0.5px solid black"  font-size="8pt">
					        	<fo:table-column column-number="1" column-width="proportional-column-width(12)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(9)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(4)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(39)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
								<fo:table-column column-number="6" column-width="proportional-column-width(5)"/>
								<fo:table-column column-number="7" column-width="proportional-column-width(5)"/>
								<fo:table-column column-number="8" column-width="proportional-column-width(5)"/>
								<fo:table-column column-number="9" column-width="proportional-column-width(16)"/>
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
											<fo:block text-align="left">Day</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
											<fo:block text-align="left">Start/End Time</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
											<fo:block text-align="left">Hrs</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
											<fo:block text-align="left">Desc</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
											<fo:block text-align="left">Cls</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
											<fo:block text-align="left">Phs</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
											<fo:block text-align="left">Ops</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
											<fo:block text-align="left">RC</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
											<fo:block text-align="left">Resp. Party</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								<fo:table-body>
									<xsl:variable name="nptEventUid" select="unwantedEventUid"/>
									<xsl:apply-templates select="Activity[nptEventUid=$nptEventUid]"/>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>						
			</fo:table-body>
		</fo:table>			
	</xsl:template>
	
	<xsl:template match="Activity">		
		<fo:table-row>
			<xsl:if test="position() mod 2 = 0">
		    	<xsl:attribute name="background-color">#EEEEEE</xsl:attribute>
		    </xsl:if>
			<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="dynaAttr/reportNumber"/>/<xsl:value-of select="dynaAttr/reportDatetime"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="d2Utils:formatDateFromEpochMS(startDatetime/@epochMS,'HH:mm')"/>/<xsl:value-of select="d2Utils:formatDateFromEpochMS(endDatetime/@epochMS,'HH:mm')"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="activityDuration"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left" linefeed-treatment="preserve"><xsl:value-of select="activityDescription"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="classCode"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="phaseCode"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="taskCode"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="rootCauseCode"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="lookupCompanyUid/@lookupLabel"/></fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	
		<xsl:template match="modules/ActivityNptEvent" mode="summary">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed"  space-before="12pt" border-top="0.5px solid black" border-right="0.5px solid black" border-left="0.5px solid black"  font-size="8pt">
        	<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(15)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="left">Event #</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="left">Comment</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="left">Start Date</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="left">Start Depth</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="left">Total Hours</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>				
				<xsl:apply-templates select="UnwantedEvent" mode="summary"/>
			</fo:table-body>
		</fo:table>			
	</xsl:template>
	
	<xsl:template match="UnwantedEvent" mode="summary">	
		<fo:table-row>
			<xsl:if test="position() mod 2 = 0">
		    	<xsl:attribute name="background-color">#EEEEEE</xsl:attribute>
		    </xsl:if>
			<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="eventRef"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left" linefeed-treatment="preserve"><xsl:value-of select="eventSummary"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="dynaAttr/eventDateTime"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="dynaAttr/eventStartDepth"/> <xsl:value-of select="dynaAttr/eventStartDepth/@uomSymbol"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-left="0.5px solid black" border-bottom="0.5px solid black">
				<fo:block text-align="left"><xsl:value-of select="nptDuration"/></fo:block>
			</fo:table-cell>
		</fo:table-row>		
	</xsl:template>
</xsl:stylesheet>