<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<!-- OFFSHORE @ ONSHORE TEMPLATE -->
	<!-- wells section BEGIN -->
	<xsl:template match="modules/Well/Well">
	<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="2pt" border="0.5px solid black">
            <fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(40)"/>
            <fo:table-body>
            	<fo:table-row>
            		<fo:table-cell padding="2px" background-color="rgb(184,184,186)"  number-columns-spanned="3" 
						border-bottom="0.5px solid black" padding-left="2px" padding-right="2px">
                        <fo:block font-size="8pt" font-weight="bold" text-align="center" text-decoration="underline">
                            <fo:inline color="rgb(64,128,128)">
                                <xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/completeOperationName" />
                            </fo:inline>
                        </fo:block>
                    </fo:table-cell>
            	</fo:table-row>
            	<fo:table-row font-size="8pt">
					<fo:table-cell>
						<fo:block> 
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>								
								<fo:table-body>
								<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block  font-weight="bold" text-align="left"> Date: </fo:block>
											</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
													<fo:block text-align="left">									
														<xsl:variable name="dailyid"
										 				select="/root/modules/ReportDaily/ReportDaily/dailyUid"/>
														<xsl:variable name="daily" select="/root/modules/Daily/Daily"/>
														<xsl:value-of
														select="d2Utils:formatDateFromEpochMS($daily[dailyUid=$dailyid]/
														dayDate/@epochMS,'dd MMM yyyy')"/>
												 	</fo:block>
											</fo:table-cell>
								</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block font-weight="bold" text-align="left"> Comp Rpt. No: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="left">																			
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportNumber" />
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									
									
								</fo:table-body>															
							</fo:table>
						</fo:block>											
					</fo:table-cell>
					<fo:table-cell >
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block font-weight="bold" text-align="left"> Com Rep Days:
										
					                    		<!--<xsl:choose>
						                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA'">
						                    			Sent To:
						                    		</xsl:when>
							                    	<xsl:otherwise>
							                    		Com Rep Days:
							                    	</xsl:otherwise>
						                          </xsl:choose>-->
						                          
					                    	</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="left">
											  <xsl:value-of select="/root/modules/ReportDailyCR/ReportDaily/compEngineer"/>
											<!--																			
												<xsl:choose>
						                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA'">
						                    			<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/sentto"/>
						                    		</xsl:when>
							                    	<xsl:otherwise>
							                    		<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/snrdrillsupervisor"/>
							                    	</xsl:otherwise>
						                        </xsl:choose> --> 
						                      
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block font-weight="bold" text-align="left">	Com Rep Nights:
					                    		<!--<xsl:choose>
						                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA'">
						                    			Sent By:
						                    		</xsl:when>
							                    	<xsl:otherwise>
							                    		Com Rep Nights:
							                    	</xsl:otherwise>
						                        </xsl:choose>-->
					                    	</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block  text-align="left">																			
												<xsl:choose>
						                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA'">
						                    			<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/sentby"/>
						                    		</xsl:when>
							                    	<xsl:otherwise>
							                    		<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/drillsupervisor"/>
							                    	</xsl:otherwise>
						                        </xsl:choose>
						                        <xsl:value-of select="/root/modules/ReportDailyCR/ReportDaily/sentby"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>															
							</fo:table>
						</fo:block>											
					</fo:table-cell> 
					<fo:table-cell>
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="6px" padding-left="8px" padding-right="8px">																
												<fo:block text-align="left"> 
													<!-- empty -->
		                        				</fo:block>
											</fo:table-cell>				
										</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block font-weight="bold" text-align="center"> Completion Type: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="left">
									            <xsl:value-of select="/root/modules/Operation/Operation/typeCompletions/@lookupLabel"/>
									        </fo:block>																
										</fo:table-cell>
									</fo:table-row>									
								</fo:table-body>															
							</fo:table>
						</fo:block>											
					</fo:table-cell>
            	</fo:table-row>
            </fo:table-body>
        </fo:table>
        
		<xsl:choose>
       		<xsl:when test="/root/modules/Operation/Operation/operationCode='RM'">
       			<!-- Start Well Data Section For Operation Type = Rig Mobilization -->
				<fo:table width="100%" table-layout="fixed" font-size="8pt"
					wrap-option="wrap" space-after="2pt" border="0.5px solid black">
					<fo:table-column column-number="1" column-width="proportional-column-width(29)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(31)"/>
					
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding="1px" background-color="rgb(184,184,186)" number-columns-spanned="3" 
								border-bottom="0.5px solid black" padding-left="2px" padding-right="2px">
								<fo:block text-align="left" font-weight="bold" font-size="8pt">
									Well Data
								</fo:block>
							</fo:table-cell>					
						</fo:table-row>				
						<fo:table-row font-size="8pt">
							<fo:table-cell border-right="0.5px solid black">
								<fo:block>
		                        	<fo:table width="100%" table-layout="fixed">
		                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Country: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="country/@lookupLabel"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>									
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Field: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
													<fo:block text-align="right">
														<xsl:value-of select="field"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>									
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Rig: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="right">												
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>
														<!-- <xsl:value-of select="/root/modules/Operation/Operation/rigInformationUid/@lookupLabel"/> -->
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Days On Well: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysOnWell,'d','')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>															
									</fo:table>
								</fo:block>											
							</fo:table-cell>
							
							<fo:table-cell border-right="0.5px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed">
										<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> AFE Number: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "string(/root/modules/Operation/Operation/afeNumber/@lookupLabel)=''">
																<xsl:value-of select="/root/modules/Operation/Operation/afeNumber"/>										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="/root/modules/Operation/Operation/afeNumber/@lookupLabel"/>
															</xsl:otherwise>
														</xsl:choose>												
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Original AFE: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="afe" select="/root/modules/Operation/Operation/afe" />
														<xsl:choose>
															<xsl:when test="($afe = '') or ($afe = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="d2Utils:formatNumber(/root/modules/Operation/Operation/afe,'#,###,###,##0')"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Supp. AFE No: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:value-of select="/root/modules/Operation/Operation/secondaryafeRef"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Orig. &amp; Sup. AFE: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="afe" select="/root/modules/Operation/Operation/afe" />
													<xsl:variable name="secondaryafe" select="/root/modules/Operation/Operation/secondaryafeAmt" />
														<xsl:choose>
				                                            <xsl:when test="(($afe = '') or ($afe = 'null')) and (($secondaryafe = '') or ($secondaryafe = 'null'))">
				                                            </xsl:when>
				                                            <xsl:when test="(($afe != '') or ($afe != 'null')) and (($secondaryafe = '') or ($secondaryafe = 'null'))">
				                                            	<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Operation/Operation/afe"/>
															</xsl:when>
				                                            <xsl:when test="($afe = '') or ($afe = 'null') ">
				                                            </xsl:when>
				                                            <xsl:otherwise>
				                                            	<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/><fo:inline color="white">.</fo:inline>
				                                                <xsl:value-of select="d2Utils:formatNumber(string(/root/modules/Operation/Operation/afe/@rawNumber + /root/modules/Operation/Operation/secondaryafeAmt/@rawNumber),'#,###,###,##0')"/>
				                                            </xsl:otherwise>
				                                        </xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>															
		                            </fo:table>
								</fo:block>											
							</fo:table-cell>
							
							<fo:table-cell>
		                        <fo:block>
		                        	<fo:table width="100%" table-layout="fixed">
		                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
										<fo:table-body>	
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Daily Cost: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="right">
													<xsl:variable name="daycost" select="/root/modules/ReportDaily/ReportDaily/daycost" />
														<xsl:choose>
															<xsl:when test="($daycost = '') or ($daycost = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$daycost/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$daycost"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Cum. Cost: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="cumcost" select="/root/modules/ReportDaily/ReportDaily/cumcost" />
														<xsl:choose>
															<xsl:when test="($cumcost = '') or ($cumcost = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>												
																<xsl:value-of select="d2Utils:formatNumber($cumcost,'##,###')"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Last LTI Date: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">											
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDaily/ReportDaily/lastLtiDate/@epochMS,'dd MMM yyyy')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Days Since LTI: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">											
														<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/daysSinceLta,'#,##0')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
				<!-- End Well Data Section For Operation Type = Rig Mobilization -->
       		</xsl:when>
          	<xsl:otherwise>
				<!-- Start Well Data Section for other operation type -->
				<fo:table width="100%" table-layout="fixed" font-size="8pt"
					wrap-option="wrap" space-after="2pt" border="0.5px solid black">
					<fo:table-column column-number="1" column-width="proportional-column-width(29)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(31)"/>
					
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding="1px" background-color="rgb(184,184,186)" number-columns-spanned="4" 
								border-bottom="0.5px solid black" padding-left="2px" padding-right="2px">
								<fo:block text-align="center" font-weight="bold" font-size="8pt">
									Well Details
								</fo:block>
							</fo:table-cell>					
						</fo:table-row>				
						<fo:table-row font-size="8pt">
							<fo:table-cell border-right="0.5px solid black">
								<fo:block>
		                        	<fo:table width="100%" table-layout="fixed">
		                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Country: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="country/@lookupLabel"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>									
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Field: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
													<fo:block text-align="right">
														<xsl:value-of select="field"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>									
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Rig: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="right">												
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>
														<!-- <xsl:value-of select="/root/modules/Operation/Operation/rigInformationUid/@lookupLabel"/> -->
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											
											<!-- Ground Level / Water Depth -->
										<xsl:choose>													
										<xsl:when test = "/root/modules/Well/Well/onOffShore='OFF'">
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
													<fo:block text-align="left">Water Depth: </fo:block>
												</fo:table-cell>		
												<xsl:variable name="waterDepth" select="/root/modules/Well/Well/waterDepth"></xsl:variable>		
												<xsl:choose>
													<xsl:when test="$waterDepth!=''">
														<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="$waterDepth"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Well/Well/waterDepth/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</xsl:when>
												</xsl:choose>
											</fo:table-row>
											</xsl:when>
										<xsl:otherwise>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
													<fo:block text-align="left">Ground Level: </fo:block>
												</fo:table-cell>		
												<xsl:variable name="glHeightMsl" select="/root/modules/Well/Well/glHeightMsl"></xsl:variable>	
												<xsl:choose>
													<xsl:when test="$glHeightMsl!=''">
														<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="$glHeightMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Well/Well/glHeightMsl/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</xsl:when>
												</xsl:choose>	
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
													<fo:block text-align="left">
														<xsl:variable name="currentDatumType" select="/root/datum/currentDatumType"></xsl:variable>
														<xsl:choose>
															<xsl:when test="$currentDatumType!=''">
																<xsl:value-of select="$currentDatumType"/> - 
															</xsl:when>
														</xsl:choose>
														Hanger:
													</fo:block>
												</fo:table-cell>
												<xsl:variable name="zRtthgr" select="/root/modules/Operation/Operation/zRtthgr"></xsl:variable>	
												<xsl:choose>
													<xsl:when test="$zRtthgr!=''">
														<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="$zRtthgr"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Operation/Operation/zRtthgr/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</xsl:when>
												</xsl:choose>
											</fo:table-row>
										</xsl:otherwise>
									</xsl:choose> <!-- /Water Depth -->
											
									<!-- Datum -->
									<fo:table-row>
										<xsl:variable name="currentDatumReferencePoint" select="/root/datum/currentDatumReferencePoint"/>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
											<fo:block text-align="left">
												<xsl:variable name="currentDatumType" select="/root/datum/currentDatumType"/>
												<xsl:choose>
													<xsl:when test="$currentDatumType!=''">
														<xsl:value-of select="$currentDatumType"/> - 
													</xsl:when>
												</xsl:choose>
												<xsl:value-of select="$currentDatumReferencePoint"/>:
											</fo:block>
										</fo:table-cell>
										<xsl:variable name="currentDatumDisplayValue" select="/root/modules/Operation/Operation/dynaAttr/currentDatumDisplayValue"></xsl:variable>
										<xsl:choose>
											<xsl:when test="$currentDatumDisplayValue!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$currentDatumDisplayValue"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/currentDatumDisplayValue/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>											
											</xsl:when>
											<xsl:otherwise>
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$currentDatumReferencePoint"/>
													</fo:block>		
												</fo:table-cell>										
											</xsl:otherwise>
										</xsl:choose>
									</fo:table-row>
									<!-- Datum -->
									<!-- Seabed -->
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
												<fo:block text-align="left">
													<xsl:variable name="currentDatumType" select="/root/datum/currentDatumType"></xsl:variable>
													<xsl:choose>
														<xsl:when test="$currentDatumType!=''">
														<xsl:value-of select="$currentDatumType"/> - 
													</xsl:when>
												</xsl:choose>
												 Seabed:
											</fo:block>
										</fo:table-cell>
											<xsl:variable name="rkbToMlGl" select="/root/modules/Operation/Operation/rkbToMlGl"></xsl:variable>
												<xsl:choose>
													<xsl:when test="$rkbToMlGl!=''">
														<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
															<fo:block text-align="right">																		
																<xsl:value-of select="$rkbToMlGl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Operation/Operation/rkbToMlGl/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</xsl:when>
												</xsl:choose>
										</fo:table-row>
									
									</fo:table-body>								
								</fo:table>
							</fo:block>
						</fo:table-cell>
								
							<fo:table-cell border-right="0.5px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed">
										<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Measured Depth: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="right">
														<xsl:variable name="depthMdMsl" select="/root/modules/ReportDaily/ReportDaily/depthMdMsl" />
														<xsl:choose>
															<xsl:when test="($depthMdMsl = '') or ($depthMdMsl = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$depthMdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$depthMdMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> TVD: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:variable name="depthTvdMsl" select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl" />
														<xsl:choose>
															<xsl:when test="($depthTvdMsl = '') or ($depthTvdMsl = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$depthTvdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$depthTvdMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Days On Comp: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysOnWell,'d','')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Last BOP Test: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">											
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDaily/ReportDaily/lastbopDate/@epochMS,'dd MMM yyyy')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Last LTI: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right" wrap-option="no-wrap">
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDaily/ReportDaily/lastLtiDate/@epochMS,'dd MMM yyyy')"/>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>	
											<!-- empty cell -->
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left">
													<!-- empty -->
													 </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">											
														<!-- empty -->
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<!-- end empty cell-->
										</fo:table-body>															
		                            </fo:table>
								</fo:block>											
							</fo:table-cell>
							
							<fo:table-cell>
		                        <fo:block>
		                        	<fo:table width="100%" table-layout="fixed">
		                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
										<fo:table-body>	
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Comp AFE Cost: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">		
													<xsl:variable name="afe" select="/root/modules/Operation/Operation/afe" />
														<xsl:choose>
															<xsl:when test="($afe = '') or ($afe = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="d2Utils:formatNumber(/root/modules/Operation/Operation/afe,'#,###,###,##0')"/>
															</xsl:otherwise>
														</xsl:choose>								
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Comp AFE No: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "string(/root/modules/Operation/Operation/afeNumber/@lookupLabel)=''">
																<xsl:value-of select="/root/modules/Operation/Operation/afeNumber"/>										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="/root/modules/Operation/Operation/afeNumber/@lookupLabel"/>
															</xsl:otherwise>
														</xsl:choose>	
														
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>	
								
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Daily Cost: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="right">
													<xsl:variable name="daycost" select="/root/modules/ReportDaily/ReportDaily/daycost" />
														<xsl:choose>
															<xsl:when test="($daycost = '') or ($daycost = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$daycost/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$daycost"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Cum. Comp Cost: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="cumcost" select="/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost" />
														<xsl:choose>
															<xsl:when test="($cumcost = '') or ($cumcost = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>												
																<xsl:value-of select="d2Utils:formatNumber($cumcost,'##,###')"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<!-- empty cells -->
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left">
													<!-- empty -->
													 </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">											
														<!-- empty -->
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left">
													<!-- empty -->
													 </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">											
														<!-- empty -->
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<!-- empty cells end -->
											
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			<!-- End Well Data Section for other operation type -->
			
			</xsl:otherwise>
	    </xsl:choose>	
		<fo:table padding="0px" width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="2pt" border="0.5px solid black">
			<fo:table-body>
				<fo:table-row>					
					<fo:table-cell>
                        <fo:block>
                        	<fo:table width="100%" table-layout="fixed" font-size="8pt"
								wrap-option="wrap" space-after="2pt">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Current Op @ 0600: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left" linefeed-treatment="preserve">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportCurrentStatus"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Planned Op: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left" linefeed-treatment="preserve">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPlanSummary"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>	
			</fo:table-body>
		</fo:table>	
		
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="2pt" space-before="3pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" border-bottom="0.5px solid black" font-size="8pt" font-weight="bold" 
											padding-left="2px" padding-right="2px">
						<fo:block text-align="left">
							Summary for Period 0000 Hrs to 2400 Hrs
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPeriodSummary"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body> 
		</fo:table>								
	</xsl:template>
	<!-- wells section END -->
</xsl:stylesheet>	