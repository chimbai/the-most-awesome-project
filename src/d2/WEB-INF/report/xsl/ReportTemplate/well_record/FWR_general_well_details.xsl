<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	

	<!-- FWR General Well Detail END -->
	<xsl:template match="modules/Well/Well" mode="general_well_details">	
	<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="10pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
		<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
		<fo:table-column column-number="2" column-width="proportional-column-width(35)"/>
		<fo:table-column column-number="3" column-width="proportional-column-width(35)"/>
		<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
		<fo:table-body>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell number-columns-spanned="2" padding="0.2cm" border-left="0.25px solid grey" border-top="0.25px solid grey" border-right="0.25px solid grey">
					<fo:block text-align="left" font-weight="bold">Well Objective : </fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell number-columns-spanned="2" padding="0.3cm" border-right="0.25px solid grey" border-left="0.25px solid grey" border-bottom="0.25px solid grey">
					<fo:block text-align="left">
						<xsl:value-of select="well_objective"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Country : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="country"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">License : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="licenseNumber"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Field : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="field"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Well : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="wellName"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Well Type : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
				      	<xsl:value-of select="/root/modules/Operation/Operation/operationCode/@lookupLabel" />
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<!--  
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Status : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="wells.status"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			-->
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Operating Company : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="opCo"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Rig : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="rigid"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm" number-columns-spanned="2">
					<fo:block text-align-last="justify">
						<fo:leader leader-pattern="rule"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Latitude : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="latDeg"/> Deg <xsl:value-of select="latMinute"/> Min <xsl:value-of select="latSecond"/> Sec</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Longitude : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="longDeg"/> Deg <xsl:value-of select="longMinute"/> Min <xsl:value-of select="longSecond"/> Sec</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Spheroid : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="spheroidType"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Cart. Datum : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="gridDatum"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Seismic Line : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="seismicLine"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Permit : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="govPermitNumber"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Projection : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="loc_utm_zone"/> Zone
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">UTM North : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="gridNs"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">UTM East : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="gridEw"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<xsl:choose>
				<xsl:when test="onOffShore = 'ON'" >
					<fo:table-row>
						<fo:table-cell>
					    </fo:table-cell>
						<fo:table-cell padding="0.075cm">
							<fo:block font-weight="bold">RT - GL : </fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.075cm">
							<fo:block>
								<xsl:value-of select="z_airgap"/>
								<xsl:value-of select="z_airgap/@uomSymbol"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
					    </fo:table-cell>
						<fo:table-cell padding="0.075cm">
							<fo:block font-weight="bold">GL - AMSL : </fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.075cm">
							<fo:block>
								<xsl:value-of select="waterDepth"/>
								<xsl:value-of select="waterDepth/@uomSymbol"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:when>
				<xsl:otherwise>
					<fo:table-row>
						<fo:table-cell>
					    </fo:table-cell>
						<fo:table-cell padding="0.075cm">
							<fo:block font-weight="bold">Datum : </fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.075cm">
							<fo:block>
								<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl"/>
								<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl/@uomSymbol"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
					    </fo:table-cell>
						<fo:table-cell padding="0.075cm">
							<fo:block font-weight="bold"> Water Depth : </fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.075cm">
							<fo:block>
								<xsl:value-of select="waterDepth"/>
								<xsl:value-of select="waterDepth/@uomSymbol"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:otherwise>
			</xsl:choose>
			<!--<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Plug Back Depth : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block><xsl:value-of select="wells.z_pbdepth"/></fo:block>
				</fo:table-cell>
			</fo:table-row>-->
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Planned TD : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="z_targetdepth"/> MDBRT
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Driller's TD : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="z_tdmd"/> MDBRT
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm" number-columns-spanned="2">
					<fo:block text-align-last="justify">
						<fo:leader leader-pattern="rule"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>							
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Rig Move In Date / Time : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/onloc_date_onloc_time,'dd MMM yyyy HH:mm')"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Spud Date / Time : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/spudDate,'dd MMM yyyy HH:mm')"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">TD Reached Date / Time : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/td_date,'dd MMM yyyy HH:mm')"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Rig Released Date / Time : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/rigOffHireDate,'dd MMM yyyy HH:mm')"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Total Days Spud : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="wells.dayssincespud"/><!-- Not sure mapping -->
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Total Days on Operations : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="wells.daysonops"/><!-- Not sure mapping -->
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Total Days Budgeted : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="wells.planneddays"/><!-- Not sure mapping -->
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm" number-columns-spanned="2">
					<fo:block text-align-last="justify">
						<fo:leader leader-pattern="rule"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">AFE Well Cost : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="/root/modules/Operation/Operation/afe"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block font-weight="bold">Actual Well Cost : </fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.075cm">
					<fo:block>
						<xsl:value-of select="//wells/wells.totalwellcost"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell>
			    </fo:table-cell>
				<fo:table-cell padding="0.075cm" number-columns-spanned="2">
					<fo:block text-align-last="justify">
						<fo:leader leader-pattern="rule"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
	</fo:table>
	</xsl:template>
	<!-- FWR General Well Detail END -->
	
</xsl:stylesheet>