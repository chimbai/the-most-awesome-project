<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<!-- wells section BEGIN -->
	<xsl:template match="modules/Well/Well">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap"
			space-after="4pt" space-before="6pt" border="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(33)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>
			
			<fo:table-body>
				<fo:table-row>
                    <fo:table-cell padding="1px" background-color="rgb(37,87,147)" border-bottom="0.5px solid black" number-columns-spanned="6" padding-left="3px" padding-right="3px">
                        <fo:block font-size="10pt" font-weight="bold" text-align="center">
                            <fo:inline text-decoration="underline" color="white">
                                <xsl:value-of select="/root/modules/Operation/Operation/operationName" />
                            </fo:inline>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
				<fo:table-row>
					<fo:table-cell text-align="left" font-weight="bold" padding="3px">
						<fo:block>
							<fo:inline>Date: 
								<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportDatetime" />
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" font-weight="bold" padding="3px">
						<fo:block>
							<fo:inline>DAILY GEOLOGY REPORT NUMBER: 
								<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportNumber" />
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right"  padding="3px">
						<fo:block>
							<fo:inline>( associated DDR # 
								<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportNumber" /> )
							</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap"
			border="0.5px solid black" space-after="4pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(33)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell text-align="left" padding="2px" number-columns-spanned="3" border-bottom="0.5px solid black" background-color="rgb(37,87,147)">
						<fo:block text-align="center" font-weight="bold" font-size="10pt">
							<fo:inline color="white">Well Details</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row font-size="8pt">
					<!-- First Column -->
					<fo:table-cell border-right="0.5px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Depth MDBRT </fo:block>
										</fo:table-cell>
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">				
												<xsl:if	test="translate(/root/modules/ReportDaily/ReportDaily/depthMdMsl,',','')!= ''">
													: <xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/depthMdMsl,',','')"/><fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl/@uomSymbol"/>
												</xsl:if>															
												<xsl:if	test="translate(/root/modules/ReportDaily/ReportDaily/depthMdMsl,',','')= ''">
													: 
												</xsl:if>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Depth TVDBRT </fo:block>
										</fo:table-cell>
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">			
												<xsl:if	test="translate(/root/modules/ReportDaily/ReportDaily/depthTvdMsl,',','')!= ''">
													: <xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/depthTvdMsl,',','')"/><fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@uomSymbol"/>
												</xsl:if>																
												<xsl:if	test="translate(/root/modules/ReportDaily/ReportDaily/depthTvdMsl,',','')= ''">
													: 
												</xsl:if>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Depth TVDSS </fo:block>
										</fo:table-cell>
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">	
											<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/subseaTvd,',','')!= ''">
												: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/subseaTvd,',','')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/subseaTvd/@uomSymbol"/>
											</xsl:if>																		
											<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/subseaTvd,',','')= ''">
												: 
											</xsl:if>	
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<!-- 
									<fo:table-row>
										<fo:table-cell padding="3px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Depth TVDSS: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="3px" padding-left="3px" padding-right="3px">	
											<fo:block text-align="right">	
												<xsl:variable name="z_depthTvdMsl" select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@rawNumber"/>
												<xsl:variable name="z_kbelevation" select="/root/modules/Operation/Operation/zKbelevation/@rawNumber"/>																		
												<xsl:choose>													
													<xsl:when test = "string($z_depthTvdMsl)!=''">
														<xsl:choose>
															<xsl:when test="string($z_kbelevation)!=''">
																<xsl:choose>
																	<xsl:when test="/root/modules/GroupWidePreference/gwp[settingName = structureformationgeol]/settingValue = positive">
																		<xsl:variable name="depthTvdSs" select="$z_depthTvdMsl - $z_kbelevation"/>
																		<xsl:value-of select="format-number($depthTvdSs,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:variable name="depthTvdSs" select="$z_kbelevation - $z_depthTvdMsl"/>
																		<xsl:value-of select="format-number($depthTvdSs,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
																	</xsl:otherwise>
																</xsl:choose>
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@uomSymbol"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:choose>
																	<xsl:when test="/root/modules/GroupWidePreference/gwp[settingName = structureformationgeol]/settingValue = positive">
																		<xsl:variable name="depthTvdSs" select="$z_depthTvdMsl - 0"/>
																		<xsl:value-of select="format-number($depthTvdSs,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:variable name="depthTvdSs" select="0 - $z_depthTvdMsl"/>
																		<xsl:value-of select="format-number($depthTvdSs,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
																	</xsl:otherwise>
																</xsl:choose>
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>														
													</xsl:when>													
													<xsl:otherwise>
														<xsl:choose>
															<xsl:when test="string($z_kbelevation)!=''">
																<xsl:choose>
																	<xsl:when test="/root/modules/GroupWidePreference/gwp[settingName = structureformationgeol]/settingValue = positive">
																		<xsl:variable name="depthTvdSs" select="0 - $z_kbelevation"/>
																		<xsl:value-of select="format-number($depthTvdSs,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:variable name="depthTvdSs" select="$z_kbelevation"/>
																		<xsl:value-of select="format-number($depthTvdSs,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
																	</xsl:otherwise>
																</xsl:choose>
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@uomSymbol"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="0.0"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									 -->
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">
											<fo:block text-align="left"><xsl:value-of select="/root/datum/currentDatumType"/> - <xsl:value-of select="/root/datum/currentDatumReferencePoint"/> </fo:block>
										</fo:table-cell>
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">																			
												: <xsl:value-of select="format-number(translate(/root/modules/Operation/Operation/dynaAttr/rtmsl,',',''), '0.0')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<xsl:choose>													
										<xsl:when test = "/root/modules/Well/Well/onOffShore='OFF'">
											<fo:table-row>
												<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">
													<fo:block text-align="left">Water Depth </fo:block>
												</fo:table-cell>
												<fo:table-cell padding-left="3px" padding-right="3px">	
													<fo:block text-align="left">	
														<xsl:if	test="translate(/root/modules/Well/Well/waterDepth,',','')!= ''">
															: <xsl:value-of select="translate(/root/modules/Well/Well/waterDepth,',','')"/><fo:inline color="white">.</fo:inline>
															<xsl:value-of select="/root/modules/Well/Well/waterDepth/@uomSymbol"/>
														</xsl:if>																		
														<xsl:if	test="translate(/root/modules/Well/Well/waterDepth,',','')= ''">
															: 
														</xsl:if>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">
													<fo:block text-align="left"><xsl:value-of select="/root/datum/currentDatumType"/> - Seabed </fo:block>
												</fo:table-cell>
												<fo:table-cell padding-left="3px" padding-right="3px">	
													<fo:block text-align="left">
														<xsl:if	test="translate(/root/modules/Operation/Operation/rkbToMlGl,',','')!= ''">
															: <xsl:value-of select="translate(/root/modules/Operation/Operation/rkbToMlGl,',','')"/><fo:inline color="white">.</fo:inline>
															<xsl:value-of select="/root/modules/Operation/Operation/rkbToMlGl/@uomSymbol"/>
														</xsl:if>																			
														<xsl:if	test="translate(/root/modules/Operation/Operation/rkbToMlGl,',','')= ''">
															: 
														</xsl:if>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
										</xsl:when>
										<xsl:otherwise>
											<fo:table-row>
												<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">
													<fo:block text-align="left">Ground Level </fo:block>
												</fo:table-cell>
												<fo:table-cell padding-left="3px" padding-right="3px">	
													<fo:block text-align="left">		
														<xsl:if	test="translate(/root/modules/Well/Well/glHeightMsl,',','')!= ''">
															: <xsl:value-of select="translate(/root/modules/Well/Well/glHeightMsl,',','')"/><fo:inline color="white">.</fo:inline>
															<xsl:value-of select="/root/modules/Well/Well/glHeightMsl/@uomSymbol"/>
														</xsl:if>																	
														<xsl:if	test="translate(/root/modules/Well/Well/glHeightMsl,',','')= ''">
															: 
														</xsl:if>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">
													<fo:block text-align="left"><xsl:value-of select="/root/datum/currentDatumType"/> - Hanger </fo:block>
												</fo:table-cell>
												<fo:table-cell padding-left="3px" padding-right="3px">	
													<fo:block text-align="left">
													<xsl:if	test="translate(/root/modules/Operation/Operation/zRtthgr,',','')!= ''">
														: <xsl:value-of select="translate(/root/modules/Operation/Operation/zRtthgr,',','')"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/Operation/Operation/zRtthgr/@uomSymbol"/>
													</xsl:if>																			
													<xsl:if	test="translate(/root/modules/Operation/Operation/zRtthgr,',','')= ''">
														: 
													</xsl:if>	
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>
										</xsl:otherwise>
									</xsl:choose>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Hole Size </fo:block>
										</fo:table-cell>
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">	
												<xsl:if	test="translate(/root/modules/ReportDaily/ReportDaily/lastHolesize,',','')!= ''">
													: <xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/lastHolesize,',','')"/><fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastHolesize/@uomSymbol"/>
												</xsl:if>																		
												<xsl:if	test="translate(/root/modules/ReportDaily/ReportDaily/lastHolesize,',','')= ''">
													: 
												</xsl:if>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Lag Depth </fo:block>
										</fo:table-cell>
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">			
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lagDepthMd,',','')!= ''">
													: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lagDepthMd,',','')"/><fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lagDepthMd/@uomSymbol"/>
												</xsl:if>																
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lagDepthMd,',','')= ''">
													: 
												</xsl:if>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Last Survey (MD<xsl:value-of select="/root/datum/currentDatumType" />/TVD<xsl:value-of select="/root/datum/currentDatumType" />) </fo:block>
										</fo:table-cell>
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">
											<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl,',','')!= '' and translate(/root/modules/ReportDailyDGR/ReportDaily/lastTvdMsl,',','')!=''">
												: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl,',','')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl/@uomSymbol"/> / <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastTvdMsl,',','')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastTvdMsl/@uomSymbol"/>
											</xsl:if>																			
											<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl,',','')= '' and translate(/root/modules/ReportDailyDGR/ReportDaily/lastTvdMsl,',','')!=''">
												: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl,',','')"/><fo:inline color="white">.</fo:inline>
												 / <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastTvdMsl,',','')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastTvdMsl/@uomSymbol"/>
											</xsl:if>
											<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl,',','')= '' and translate(/root/modules/ReportDailyDGR/ReportDaily/lastTvdMsl,',','')=''">
												: 
											</xsl:if>
											<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl,',','')!= '' and translate(/root/modules/ReportDailyDGR/ReportDaily/lastTvdMsl,',','')=''">
												: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl,',','')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl/@uomSymbol"/> / <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastTvdMsl,',','')"/><fo:inline color="white">.</fo:inline>
												
											</xsl:if>	
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Survey Deviation </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">																			
												: Inc.
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/inclinationAngle,',','')!= ''">
													<xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/inclinationAngle,',','')"/><fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/inclinationAngle/@uomSymbol"/>
												</xsl:if>
												
											</fo:block>
											<fo:block text-align="left">																			
												<fo:inline color="white">.</fo:inline> Az.
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/azimuthAngle,',','')!= ''">
													<xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/azimuthAngle,',','')"/><fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/azimuthAngle/@uomSymbol"/>
												</xsl:if>
												
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>								
							</fo:table>
						</fo:block>
					</fo:table-cell>
					
					<!-- Second Column -->
					<fo:table-cell border-right="0.5px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Report Period </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">																			
												: <xsl:choose>
										      		<xsl:when test="/root/modules/ReportDailyDGR/ReportDaily/dailyReportStartDatetime/@epochMS = '86400000'">
												       <fo:inline>24:00</fo:inline>
										     		</xsl:when>
												    <xsl:otherwise>
												       <xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDailyDGR/ReportDaily/dailyReportStartDatetime/@epochMS,'HH:mm')"/>
												    </xsl:otherwise>
										     	</xsl:choose> - 
										     	<xsl:choose>
										      		<xsl:when test="/root/modules/ReportDailyDGR/ReportDaily/dailyReportEndDatetime/@epochMS = '86400000'">
												       <fo:inline>24:00</fo:inline>
										     		</xsl:when>
												    <xsl:otherwise>
												       <xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDailyDGR/ReportDaily/dailyReportEndDatetime/@epochMS,'HH:mm')"/>
												    </xsl:otherwise>
										     	</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Last Csg Size </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">																			
												: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/dynaAttr/ddr.lastCsgsize,',','')"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/dynaAttr/ddr.lastCsgsize/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Last Csg Shoe MD </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">																			
												: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/dynaAttr/ddr.lastCsgshoeMdMsl,',','')"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/dynaAttr/ddr.lastCsgshoeMdMsl/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Last Csg Shoe TVD </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">																			
												: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/dynaAttr/ddr.lastCsgshoeTvdMsl,',','')"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/dynaAttr/ddr.lastCsgshoeTvdMsl/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Liner MD </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">																			
												: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/dynaAttr/ddr.lastLinerMdMsl,',','')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/dynaAttr/ddr.lastLinerMdMsl/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Liner TVD </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">																			
												: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/dynaAttr/ddr.lastLinerTvdMsl,',','')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/dynaAttr/ddr.lastLinerTvdMsl/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> FIT / LOT </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">
												<xsl:choose>
													<xsl:when test="translate(/root/modules/ReportDaily/ReportDaily/lastFit,',','')='' and translate(/root/modules/ReportDaily/ReportDaily/lastLot,',','')=''">	
													    :
													</xsl:when>
													<xsl:otherwise>
														: <xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/lastFit,',','')"/><fo:inline color="white">.</fo:inline>
														  <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit/@uomSymbol"/> / 
														  <xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/lastLot,',','')"/><fo:inline color="white">.</fo:inline>
														  <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot/@uomSymbol"/>	
													</xsl:otherwise>
												</xsl:choose>																		
												
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Hole Size Carbide </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">		
											<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastHolesizeCarbide,',','')!= ''">
												: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastHolesizeCarbide,',','')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastHolesizeCarbide/@uomSymbol"/>
											</xsl:if>																	
											<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastHolesizeCarbide,',','')= ''">
												: 
											</xsl:if>	
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Liner (MD<xsl:value-of select="/root/datum/currentDatumType" />/TVD<xsl:value-of select="/root/datum/currentDatumType" />) </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">	
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl,',','')!= '' and translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl,',','')!=''">
													: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl,',','')"/><fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl/@uomSymbol"/> / <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl,',','')"/><fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl/@uomSymbol"/>
												</xsl:if>																		
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl,',','')= '' and translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl,',','')!=''">
													: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl,',','')"/><fo:inline color="white">.</fo:inline>
													 / <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl,',','')"/><fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl/@uomSymbol"/>
												</xsl:if>
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl,',','')!= '' and translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl,',','')=''">
													: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl,',','')"/><fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl/@uomSymbol"/> / <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl,',','')"/><fo:inline color="white">.</fo:inline>
												</xsl:if>
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl,',','')= '' and translate(/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl,',','')=''">
													: 
												</xsl:if>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px">																
											<fo:block text-align="left">  </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="right">																			
												
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>								
							</fo:table>
						</fo:block>
					</fo:table-cell>
					
					<!-- Third Column -->
					<fo:table-cell>
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Date </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">
												: <xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportDatetime"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Progress </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">
												: <xsl:variable name="dailyid" select="/root/modules/Daily/Daily/dailyUid"/>
												<xsl:variable name="progress" select="string(sum(/root/modules/Bharun/Bharun/BharunDailySummary[dailyUid = $dailyid]/progress/@rawNumber))"/>												
												<xsl:value-of select="d2Utils:formatNumber($progress,'###0.00')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Bharun/Bharun/BharunDailySummary[dailyUid = $dailyid]/progress/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Report Start Depth </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/reportStartDepthMdMsl,',','')!= ''">
													: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/reportStartDepthMdMsl,',','')"/>
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportStartDepthMdMsl/@uomSymbol"/>
												</xsl:if>																			
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/reportStartDepthMdMsl,',','')= ''">
													: 
												</xsl:if>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Report End Depth </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">	
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/reportEndDepthMdMsl,',','')!= ''">
													: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/reportEndDepthMdMsl,',','')"/>
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportEndDepthMdMsl/@uomSymbol"/>
												</xsl:if>																		
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/reportEndDepthMdMsl,',','')= ''">
													: 
												</xsl:if>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Days since Spud </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">																			
												: <xsl:value-of select="translate(translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysFromSpud,',',''),'d','')"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Rig </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">																			
												: <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Mud Weight </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastMw,',','')!= ''">
													: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/lastMw,',','')"/>
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastMw/@uomSymbol"/>
												</xsl:if>																			
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/lastMw,',','')= ''">
													: 
												</xsl:if>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Mud Type </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">																			
												: <xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/mudType"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/mudType/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px" font-weight="bold">																
											<fo:block text-align="left"> Est. Pore Pressure </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="left">
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/estimatedPorepressureDensity,',','')!= ''">
													: <xsl:value-of select="translate(/root/modules/ReportDailyDGR/ReportDaily/estimatedPorepressureDensity,',','')"/>
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/estimatedPorepressureDensity/@uomSymbol"/>
												</xsl:if>																			
												<xsl:if	test="translate(/root/modules/ReportDailyDGR/ReportDaily/estimatedPorepressureDensity,',','')= ''">
													: 
												</xsl:if>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding-left="3px" padding-right="3px">																
											<fo:block text-align="left">  </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding-left="3px" padding-right="3px">	
											<fo:block text-align="right">
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>								
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>	
		
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="2pt">
         <fo:table-column column-number="1" column-width="proportional-column-width(20)" />
         <fo:table-column column-number="2" column-width="proportional-column-width(20)" />
         <fo:table-column column-number="3" column-width="proportional-column-width(20)" />
         <fo:table-column column-number="4" column-width="proportional-column-width(20)" />
         <fo:table-column column-number="5" column-width="proportional-column-width(20)" />
         <fo:table-header>
            <fo:table-row font-size="10pt">
               <fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" number-columns-spanned="5" text-align="center" background-color="rgb(37,87,147)">
                  <fo:block color="white">Geology 24hr Operations Summary</fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-header>
         <fo:table-body>
         	<fo:table-row font-size="8pt">
         		<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" font-weight="bold" border-right="0.25px solid black">
                	<fo:block>
                     	24hr Summary:
                	</fo:block>
                </fo:table-cell>
				<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black" number-columns-spanned="4" linefeed-treatment="preserve">
                	<fo:block>
                    	<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportPeriodSummary"/>
                	</fo:block>
            	</fo:table-cell>
			</fo:table-row>
			<fo:table-row font-size="8pt">
         		<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" font-weight="bold" border-right="0.25px solid black">
                	<fo:block>
                     	24hr Forward Plan:
                	</fo:block>
                </fo:table-cell>
				<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black" number-columns-spanned="4" linefeed-treatment="preserve">
                	<fo:block>
                    	<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geolOps"/>
                	</fo:block>
            	</fo:table-cell>
			</fo:table-row>
         </fo:table-body>
    </fo:table>				
	</xsl:template>
	<!-- wells section END -->
</xsl:stylesheet>