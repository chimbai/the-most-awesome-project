<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- wells section BEGIN -->
	<xsl:template match="modules/Well/Well">
		<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="8pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(35)"/>			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell font-size="8pt" font-weight="bold">
						<fo:block>
							<xsl:variable name="dailyid" select="/root/modules/ReportDaily/ReportDaily/reportDailyUid"/>
							<xsl:variable name="daily" select="/root/modules/Daily/Daily"/>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($daily[dailyUid=$dailyid]/dayDate/@epochMS,'dd MMM yyyy')"/> 
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-size="8pt" font-weight="bold" text-align="right">
						<fo:block>
							From :
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" font-size="8pt" font-weight="bold" text-align="left" wrap-option="wrap">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/sentby"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="2" font-weight="bold" font-size="8pt" text-align="right">
						<fo:block>
							To :
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" font-size="8pt" font-weight="bold" text-align="left" wrap-option="wrap">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/sentto"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<fo:table width="100%" table-layout="fixed" font-size="8pt"
			wrap-option="wrap" space-after="2pt" border="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(23)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(27)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(24)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(26)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="4" padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
						background-color="rgb(224,224,224)" border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Well Data
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row font-size="8pt">
					<fo:table-cell border-right="0.25px solid black"
						border-bottom="0.25px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Country: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">																			
												<xsl:value-of select="country"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> Field: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">
												<xsl:value-of select="field"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<!-- 
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> Drill Co. </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">																								
												<xsl:value-of select="opCo"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									-->
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> Rig: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="right">												
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>
												<!-- <xsl:value-of select="/root/modules/Operation/Operation/rigInformationUid/@lookupLabel"/> -->
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> Mud Co.: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/mudengineer_co/@lookupLabel"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>									
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"><xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/currentDatumDisplayLabel"/>: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/currentDatumDisplayValue"/>&#160;
												<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/currentDatumDisplayValue/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>									
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> GL-AMSL: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="waterDepth"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="waterDepth/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>														
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> <xsl:value-of select="/root/datum/currentDatumType"/>-GL: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="z_airgap"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="z_airgap/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>															
							</fo:table>
						</fo:block>											
					</fo:table-cell>
					
					<fo:table-cell border-right="0.25px solid black"
						border-bottom="0.25px solid black">
						<fo:block>
							<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								<fo:table-body>
									<!--
									<fo:table-row>										
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> RT To Seabed </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="right">
											<xsl:variable name="z_rtmsl" select="/root/modules/Operation/Operation/z_rtmsl/@rawNumber"/>
												<xsl:choose>													
													<xsl:when test = "string(waterDepth/@rawNumber)!=''">
														<xsl:choose>
															<xsl:when test="string($z_rtmsl)!=''">
																<xsl:variable name="rttoseabed" select="waterDepth/@rawNumber + $z_rtmsl"/>
																<xsl:value-of select="format-number($rttoseabed,'#,##0.0')"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="waterDepthMsl/@uomSymbol"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:variable name="rttoseabed" select="waterDepthMsl/@rawNumber + 0"/>
																<xsl:value-of select="format-number($rttoseabed,'#,##0.0')"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="waterDepthMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>														
													</xsl:when>													
													<xsl:otherwise>
														<xsl:choose>
															<xsl:when test="string(z_rtmsl)!=''">
																<xsl:variable name="rttoseabed" select="0 + $z_rtmsl"/>
																<xsl:value-of select="format-number($rttoseabed,'#,##0.0')"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="waterDepthMsl/@uomSymbol"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="0.0"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="waterDepthMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									-->
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> M. Depth: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl/@uomSymbol"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> TVD: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@uomSymbol"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Progress: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:variable name="dailyid" select="/root/modules/Daily/Daily/dailyUid"/>
												<xsl:value-of select="/root/modules/Bharun/Bharun/BharunDailySummary[dailyUid = $dailyid]/dynaAttr/totalProgress"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> Days On Well: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/dynaAttr/daysOnWell"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Days From Spud: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/dynaAttr/daysFromSpud"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> Days +/- Curve: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycurve"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>									
								</fo:table-body>															
                            </fo:table>
						</fo:block>											
					</fo:table-cell>
					
					<fo:table-cell border-right="0.25px solid black"
						border-bottom="0.25px solid black">
                        <fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> Cur. Hole Size: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/last_holesize"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/last_holesize/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Casing OD: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/last_csgsize"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/last_csgsize/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Shoe MD: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/last_csgshoe_md"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/last_csgshoe_md/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> FIT: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/last_fit"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/last_fit/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> LOT: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/last_lot"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/last_lot/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> AFE Basis: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:choose>													
													<xsl:when test = "string(/root/modules/Operation/Operation/afeNumber/@lookupLabel)=''">
														<xsl:value-of select="/root/modules/Operation/Operation/afeNumber"/>										
													</xsl:when>													
													<xsl:otherwise>
														<xsl:value-of select="/root/modules/Operation/Operation/afeNumber/@lookupLabel"/>
													</xsl:otherwise>
												</xsl:choose>												
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>	
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> AFE Cost: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Operation/Operation/afe"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>															
							</fo:table>
						</fo:block>											
					</fo:table-cell>
					
					<fo:table-cell border-bottom="0.25px solid black">
                        <fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								<fo:table-body>									
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> Daily Drllg. Cost: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Cum. Drllg. Cost: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
												<fo:inline color="white">.</fo:inline>												
												<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost,'##,###')"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Compl. Cost: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycompletioncost/@uomSymbol"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/daycompletioncost,'##,###')"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Cum. Compl.: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcompletioncost,'##,###')"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> Total Day Cost: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/dynaAttr/totalcost,'##,###')"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left"> Total Cum. Cost: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/dynaAttr/cumtotalcost/@uomSymbol"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/dynaAttr/cumtotalcost,'##,###')"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>	
									<fo:table-row>
										<fo:table-cell padding="0.05cm" number-columns-spanned="2">
											<fo:block><fo:inline color="white">.</fo:inline></fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row font-size="8pt">
					<fo:table-cell border-right="0.25px solid black" number-columns-spanned="2"
                        border-bottom="0.25px solid black">
                        <fo:block>
                        	<fo:table width="100%" table-layout="fixed" font-size="8pt"
								wrap-option="wrap" space-after="2pt">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(32)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(68)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left" font-weight="bold"> Current Op @ 0600: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left" linefeed-treatment="preserve">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportCurrentStatus"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-bottom="0.25px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed" font-size="8pt"
								wrap-option="wrap" space-after="2pt">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(22)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(78)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left" font-weight="bold"> Planned Op: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left" linefeed-treatment="preserve">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPlanSummary"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>								
				
				<fo:table-row font-size="8pt">
					<fo:table-cell number-columns-spanned="4">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed" font-size="8pt"
								wrap-option="wrap" space-after="2pt">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(28)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(72)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left" font-weight="bold"> Summary of Period 0000 to 2400 Hrs: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.05cm">
											<fo:block text-align="left" linefeed-treatment="preserve">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPeriodSummary"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>										
				</fo:table-row>
			</fo:table-body>
		</fo:table>								
	</xsl:template>
	<!-- wells section END -->
	
</xsl:stylesheet>	