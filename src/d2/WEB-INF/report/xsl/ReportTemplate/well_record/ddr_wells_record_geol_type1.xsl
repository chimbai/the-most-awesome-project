<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<!-- wells section BEGIN -->
	<xsl:template match="modules/Well/Well">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" border="0.5px solid black" margin-top="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(30)"/>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>RIG</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel)=''">
									<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>RIG TYPE</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:variable name="rigInformationUid" select="/root/modules/ReportDaily/ReportDaily/rigInformationUid"/>
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/RigInformation/RigInformation[rigInformationUid = $rigInformationUid]/rigSubType/@lookupLabel)=''">
									<xsl:value-of select="/root/modules/RigInformation/RigInformation[rigInformationUid = $rigInformationUid]/rigSubType"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="/root/modules/RigInformation/RigInformation[rigInformationUid = $rigInformationUid]/rigSubType/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>COMPLETION TYPE</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/Operation/Operation/typeCompletions/@lookupLabel)=''">
									<xsl:value-of select="/root/modules/Operation/Operation/typeCompletions"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="/root/modules/Operation/Operation/typeCompletions/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>TARGET</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:value-of select="/root/modules/Operation/Operation/typeTarget"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" border="0.5px solid black" margin-top="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(25)"/>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>TIME</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDailyDGR/ReportDaily/geolReportStarttime/@epochMS,'HH:mm')"/> - 
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDailyDGR/ReportDaily/geolReportEndtime/@epochMS,'HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>SPUD DATE</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/spudDate/@epochMS,'dd MMM yyyy')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>DAYS SINCE SPUD</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysFromSpud,'d','')"/> (
							Days on well: <xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysOnWell,'d','')"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>LAST CASING</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize/@uomSymbol"/>
							<fo:inline> @ </fo:inline>
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl/@uomSymbol"/>
							<fo:inline> MD </fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>PRESENT DEPTH MD</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/depth0600MdMsl"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/depth0600MdMsl/@uomSymbol"/>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>BACKGROUND GAS</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/maxgasbgFrom"/>
							<fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/maxgasbgFrom/@uomSymbol"/>
							<fo:inline> - </fo:inline>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/maxgasbgTo"/>
							<fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/maxgasbgTo/@uomSymbol"/>
							<xsl:if test="string-length(normalize-space(/root/modules/ReportDailyDGR/ReportDaily/maxgasbgDepthMdMsl)) &gt; 0">
								<fo:inline> @ </fo:inline>
								<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/maxgasbgDepthMdMsl"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/maxgasbgDepthMdMsl/@uomSymbol"/>
							</xsl:if>						
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>PRESENT DEPTH TVD</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/depth0600TvdMsl"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/depth0600TvdMsl/@uomSymbol"/>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>MAX GAS</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/maxgas"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/maxgas/@uomSymbol"/>
														
							<xsl:if test="string-length(normalize-space(/root/modules/ReportDailyDGR/ReportDaily/maxgasDepthMdMsl)) &gt; 0">
								<fo:inline> @ </fo:inline>
								<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/maxgasDepthMdMsl"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/maxgasDepthMdMsl/@uomSymbol"/>
							</xsl:if>						
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>DISTANCE DRILLED (last 24 hrs)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geology24hrProgress"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geology24hrProgress/@uomSymbol"/>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>MUD WEIGHT</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:value-of select="/root/modules/MudProperties/MudProperties/mudWeight"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/MudProperties/MudProperties/mudWeight/@uomSymbol"/>
							<xsl:if test="string-length(normalize-space(/root/modules/MudProperties/MudProperties/mudWeight)) &gt; 0">
								<xsl:variable name="mudtype" select="/root/modules/MudProperties/MudProperties/mudType"/>
								<xsl:choose>
									<xsl:when test="$mudtype='h2o'">
										<fo:inline> (WBM)</fo:inline>
									</xsl:when>
									<xsl:when test="$mudtype='sbm'">
										<fo:inline> (SBM)</fo:inline>
									</xsl:when>
									<xsl:when test="$mudtype='oil'">
										<fo:inline> (OBM)</fo:inline>
									</xsl:when>
								</xsl:choose>
							</xsl:if>
							
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>AVERAGE ROP</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geology24hrAvgrop"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geology24hrAvgrop/@uomSymbol"/>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>ECD</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:value-of select="/root/modules/PorePressure/PorePressure/ecd"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/PorePressure/PorePressure/ecd/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>CURRENT ACTIVITY</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px" border-right="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportCurrentStatus"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>ESTIMATED PORE PRESSURE DENSITY</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:value-of select="/root/modules/PorePressure/PorePressure/estimatedPorepressureDensity"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/PorePressure/PorePressure/estimatedPorepressureDensity/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>PROGNOSED SECTION TD</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/prognosedSectionMdMsl"/>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/prognosedSectionMdMsl/@uomSymbol"/>	
							<fo:inline> MD </fo:inline>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/prognosedSectionTvdMsl"/>
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/prognosedSectionTvdMsl/@uomSymbol"/>
							<fo:inline> TVD</fo:inline>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px" number-columns-spanned="2">
						<fo:block>
							<fo:inline color="white">.</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					
					<!--
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>PROGNOSED WELL TD</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:value-of select="/root/modules/Operation/Operation/zTdMdMsl"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/Operation/Operation/zTdMdMsl/@uomSymbol"/>	
						</fo:block>
					</fo:table-cell>
					-->
				</fo:table-row>
			</fo:table-body>
		</fo:table>	
		
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" border="0.5px solid black" margin-top="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(12)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(21)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(12)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(21)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(12)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(22)"/>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell text-align="left" number-columns-spanned="6" border-bottom="0.5px solid black" padding="0.3px">
						<fo:block text-align="center" font-weight="bold" font-size="9pt" padding="2px" padding-left="-0.3px" padding-right="-0.3px" background-color="rgb(223,223,223)">
							<fo:inline>Well Details</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>Latitude: </fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:value-of select="/root/modules/Well/Well/latDeg"/>
							<fo:inline>&#x00B0;</fo:inline><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/Well/Well/latMinute"/>
							<fo:inline>&#34;</fo:inline><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/Well/Well/latSecond"/>
							<fo:inline>&#39;</fo:inline><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/Well/Well/latNs/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>UTM(N/S): </fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:value-of select="/root/modules/Well/Well/gridNs"/>
							<xsl:if test="string-length(normalize-space(/root/modules/Well/Well/gridNs)) &gt; 0">
								<fo:inline> m N</fo:inline>
							</xsl:if>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>RT - MSL:</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:value-of select="/root/modules/Operation/dynaAttr/datumLabel"/> 
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>Longitude:</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:value-of select="/root/modules/Well/Well/longDeg"/>
							<fo:inline>&#x00B0;</fo:inline><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/Well/Well/longMinute"/>
							<fo:inline>&#34;</fo:inline><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/Well/Well/longSecond"/>
							<fo:inline>&#39;</fo:inline><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/Well/Well/longEw/@lookupLabel"/>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<fo:inline>UTM(E/W):</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<xsl:value-of select="/root/modules/Well/Well/gridEw"/>
							<xsl:if test="string-length(normalize-space(/root/modules/Well/Well/gridEw)) &gt; 0">
								<fo:inline> m E</fo:inline>
							</xsl:if>						
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="3px">
						<fo:block>
							<xsl:choose>
								<xsl:when test="/root/modules/Well/Well/onOffShore = 'ON'">
									<fo:inline>GL Elevation:</fo:inline>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>Water Depth:</fo:inline>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block>
							<xsl:choose>
								<xsl:when test="/root/modules/Well/Well/onOffShore = 'ON'">
									<xsl:value-of select="/root/modules/Well/Well/glHeightMsl"/>
									<xsl:if test="string-length(normalize-space(/root/modules/Well/Well/glHeightMsl)) &gt; 0">
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/Well/Well/glHeightMsl/@uomSymbol"/>
									</xsl:if>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="/root/modules/Well/Well/waterDepth"/>
									<xsl:if test="string-length(normalize-space(/root/modules/Well/Well/waterDepth)) &gt; 0">
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/Well/Well/waterDepth/@uomSymbol"/>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" border="0.5px solid black" margin-top="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
						
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell text-align="left" number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.3px">
						<fo:block text-align="center" font-weight="bold" font-size="9pt" padding="2px" padding-left="-0.3px" padding-right="-0.3px" background-color="rgb(223,223,223)">
							<fo:inline>Operations Summary and General Remarks</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<!--
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<fo:inline>REMARKS:</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-bottom="0.5px solid black">
						<fo:block text-align="left">
							<xsl:value-of select="/root/modules/Well/Well/remarks"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>		
				-->
				
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<fo:inline>OPERATION SUMMARY:</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px" border-bottom="0.5px solid black">
						<fo:block text-align="left" linefeed-treatment="preserve">
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportPeriodSummary"/>	
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell text-align="left" padding="3px" border-right="0.5px solid black">
						<fo:block>
							<fo:inline>NEXT OPERATION:</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="3px">
						<fo:block text-align="left" linefeed-treatment="preserve">
							<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geolOps"/>	
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>		
							
	</xsl:template>
	
</xsl:stylesheet>	