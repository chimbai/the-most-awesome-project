<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<!-- OFFSHORE @ ONSHORE TEMPLATE -->
	<!-- wells section BEGIN -->
	<xsl:template match="modules/Well/Well">
	<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" padding-right="0.5px" padding-left="0.2px" padding-top="0.25px"
	space-after="2pt" border-top="0.5px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" border-right="0.5px solid black">
            <fo:table-column column-number="1" column-width="proportional-column-width(29)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(31)"/>
            <fo:table-body>
            	<fo:table-row>
            		<fo:table-cell number-columns-spanned="3"  background-color="rgb(223,223,223)" 
						border-bottom="0.5px solid black" padding-left="2px">
                        <fo:block font-size="8pt" font-weight="bold" text-align="left">
                            	<fo:inline color="black">
	                                <xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/completeOperationName" />
	                            </fo:inline>
                        </fo:block>
                    </fo:table-cell>
            	</fo:table-row>
            	<fo:table-row font-size="8pt">
					<fo:table-cell border-right="0.5px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Report Number: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="right">																			
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportNumber" />
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Latitude (<xsl:value-of select="latNs/@lookupLabel"/>): </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="right">																			
												<xsl:value-of select="latDeg" /> &#176;
					                        	<xsl:value-of select="latMinute" /> &#39;
					                        	<xsl:value-of select="latSecond" /> &#34;
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Longitude (<xsl:value-of select="longEw/@lookupLabel"/>): </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="right">																			
												<xsl:value-of select="longDeg" /> &#176;
					                        	<xsl:value-of select="longMinute" /> &#39;
					                        	<xsl:value-of select="longSecond" /> &#34; 
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>															
							</fo:table>
						</fo:block>											
					</fo:table-cell>
					<fo:table-cell border-right="0.5px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left">
					                    		<xsl:choose>
						                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA'">
						                    			Sent By:
						                    		</xsl:when>
							                    	<xsl:otherwise>
							                    		Day Wellsite Representative:
							                    	</xsl:otherwise>
						                        </xsl:choose>
					                    	</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="right">																			
												<xsl:choose>
						                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA'">
						                    			<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/sentby"/>
						                    		</xsl:when>
							                    	<xsl:otherwise>
							                    		<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/snrdrillsupervisor"/>
							                    	</xsl:otherwise>
						                        </xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left">
					                    		<xsl:choose>
						                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA'">
						                    			Sent To:
						                    		</xsl:when>
							                    	<xsl:otherwise>
							                    		Night Wellsite Representative:
							                    	</xsl:otherwise>
						                        </xsl:choose>
					                    	</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="right">																			
												<xsl:choose>
						                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA'">
						                    			<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/sentto"/>
						                    		</xsl:when>
							                    	<xsl:otherwise>
							                    		<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/drillsupervisor"/>
							                    	</xsl:otherwise>
						                        </xsl:choose>	
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Operator: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="right">																			
												<xsl:value-of select="/root/modules/Operation/Operation/LookupCompany/companyName"/> 
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>															
							</fo:table>
						</fo:block>											
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> 
												<xsl:choose>
						                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA'">
						                    			POB:
						                    		</xsl:when>
							                    	<xsl:otherwise>
							                    		Rig Manager:
							                    	</xsl:otherwise>
						                        </xsl:choose>
	                        				</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="right">																			
												<xsl:choose>
						                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA'">
						                    			<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/commentPob"/>
						                    		</xsl:when>
							                    	<xsl:otherwise>
							                    		<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigsuperintendent"/> 
							                    	</xsl:otherwise>
						                        </xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Drilling Company:</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="right">																			
												<xsl:variable name="currentrig" select="/root/modules/Operation/Operation/rigInformationUid" />
									            <xsl:variable name="current_drillcoid" select="/root/modules/RigInformation/RigInformation[rigInformationUid=$currentrig]/rigManager/@lookupLabel" />
									            <xsl:value-of select="$current_drillcoid"/>
									        </fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> 
												<xsl:choose>
						                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA' or /root/modules/Operation/Operation/operationCode='RM'">
						                    			<fo:inline color="white">.</fo:inline>
						                    		</xsl:when>
							                    	<xsl:otherwise>
							                    		Wellsite Geologist:
							                    	</xsl:otherwise>
						                        </xsl:choose>
											</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="right">																			
												<xsl:choose>
					                    		<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA' or /root/modules/Operation/Operation/operationCode='RM'">
					                    			<fo:inline color="white">.</fo:inline>
					                    		</xsl:when>
						                    	<xsl:otherwise>
						                    		<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/geologist" /> 
						                    	</xsl:otherwise>
					                        </xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>															
							</fo:table>
						</fo:block>											
					</fo:table-cell>
            	</fo:table-row>
            </fo:table-body>
        </fo:table>
        
		<xsl:choose>
       		<xsl:when test="/root/modules/Operation/Operation/operationCode='RM'">
       			<!-- Start Well Data Section For Operation Type = Rig Mobilization -->
				<fo:table width="100%" table-layout="fixed" font-size="8pt"
					wrap-option="wrap" space-after="2pt" border="0.5px solid black" padding="0.5px">
					<fo:table-column column-number="1" column-width="proportional-column-width(29)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(31)"/>
					
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding="1px" background-color="rgb(223,223,223)" number-columns-spanned="3" 
								border-bottom="0.5px solid black" padding-left="2px" padding-right="2px">
								<fo:block text-align="left" font-weight="bold" font-size="8pt">
									Well Data
								</fo:block>
							</fo:table-cell>					
						</fo:table-row>				
						<fo:table-row font-size="8pt">
							<fo:table-cell border-right="0.5px solid black"> 
								<fo:block>
		                        	<fo:table width="100%" table-layout="fixed">
		                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Country: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="country/@lookupLabel"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>									
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Field: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
													<fo:block text-align="right">
														<xsl:value-of select="field"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>									
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Rig: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="right">												
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>
														<!-- <xsl:value-of select="/root/modules/Operation/Operation/rigInformationUid/@lookupLabel"/> -->
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Days On Well: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysOnWell,'d','')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>															
									</fo:table>
								</fo:block>											
							</fo:table-cell>
							
							<fo:table-cell>
								<fo:block>
									<fo:table width="100%" table-layout="fixed">
										<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> AFE Number: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "string(/root/modules/Operation/Operation/afeNumber/@lookupLabel)=''">
																<xsl:value-of select="/root/modules/Operation/Operation/afeNumber"/>										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="/root/modules/Operation/Operation/afeNumber/@lookupLabel"/>
															</xsl:otherwise>
														</xsl:choose>												
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Original AFE: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="afe" select="/root/modules/Operation/Operation/afe" />
														<xsl:choose>
															<xsl:when test="($afe = '') or ($afe = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="d2Utils:formatNumber(/root/modules/Operation/Operation/afe,'#,###,###,##0')"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Supp. AFE No: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:value-of select="/root/modules/Operation/Operation/secondaryafeRef"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Orig. &amp; Sup. AFE: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="afe" select="/root/modules/Operation/Operation/afe" />
													<xsl:variable name="secondaryafe" select="/root/modules/Operation/Operation/secondaryafeAmt" />
														<xsl:choose>
				                                            <xsl:when test="(($afe = '') or ($afe = 'null')) and (($secondaryafe = '') or ($secondaryafe = 'null'))">
				                                            </xsl:when>
				                                            <xsl:when test="(($afe != '') or ($afe != 'null')) and (($secondaryafe = '') or ($secondaryafe = 'null'))">
				                                            	<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Operation/Operation/afe"/>
															</xsl:when>
				                                            <xsl:when test="($afe = '') or ($afe = 'null') ">
				                                            </xsl:when>
				                                            <xsl:otherwise>
				                                            	<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/><fo:inline color="white">.</fo:inline>
				                                                <xsl:value-of select="d2Utils:formatNumber(string(/root/modules/Operation/Operation/afe/@rawNumber + /root/modules/Operation/Operation/secondaryafeAmt/@rawNumber),'#,###,###,##0')"/>
				                                            </xsl:otherwise>
				                                        </xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>															
		                            </fo:table>
								</fo:block>											
							</fo:table-cell>
							
							<fo:table-cell>
		                        <fo:block>
		                        	<fo:table width="100%" table-layout="fixed">
		                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
										<fo:table-body>	
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Daily Cost: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="right">
													<xsl:variable name="daycost" select="/root/modules/ReportDaily/ReportDaily/daycost" />
														<xsl:choose>
															<xsl:when test="($daycost = '') or ($daycost = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$daycost/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$daycost"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Cum. Cost: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="cumcost" select="/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost" />
														<xsl:choose>
															<xsl:when test="($cumcost = '') or ($cumcost = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>												
																<xsl:value-of select="d2Utils:formatNumber($cumcost,'##,###')"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Last LTI Date: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">											
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDaily/ReportDaily/lastLtiDate/@epochMS,'dd MMM yyyy')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Days Since LTI: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">											
														<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/daysSinceLta,'#,##0')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
				<!-- End Well Data Section For Operation Type = Rig Mobilization -->
       		</xsl:when>
          	<xsl:otherwise>
				<!-- Start Well Data Section for other operation type -->
				<fo:table width="100%" table-layout="fixed" font-size="8pt"
					wrap-option="wrap" space-after="2pt" border="0.5px solid black" padding-left="0.2px" padding-top="0.5px" padding-right="0.5px" padding-bottom="0.5px">
					<fo:table-column column-number="1" column-width="proportional-column-width(26)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
					<fo:table-column column-number="4" column-width="proportional-column-width(29)"/>
					
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding="1px" background-color="rgb(223,223,223)" number-columns-spanned="4" 
								border-bottom="0.5px solid black" padding-left="2px" padding-right="2px">
								<fo:block text-align="left" font-weight="bold" font-size="8pt">
									Well Data
								</fo:block>
							</fo:table-cell>					
						</fo:table-row>				
						<fo:table-row font-size="8pt">
							<fo:table-cell border-right="0.5px solid black">
								<fo:block>
		                        	<fo:table width="100%" table-layout="fixed">
		                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Country: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="country/@lookupLabel"/>
													</fo:block>																
												</fo:table-cell>
											</fo:table-row>									
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Field: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
													<fo:block text-align="right">
														<xsl:value-of select="field"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>									
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Rig: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="right">												
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>
														<!-- <xsl:value-of select="/root/modules/Operation/Operation/rigInformationUid/@lookupLabel"/> -->
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											
											
											<xsl:choose>
									      		<xsl:when test="onOffShore = 'OFF'">
									      			<!-- WATER DEPTH -->
											    	<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="left"> Water Depth: </fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="right">
																<xsl:choose>
																	<xsl:when test="(waterDepth = '') or (waterDepth ='null')">
																	</xsl:when>
					                       							<xsl:otherwise>			
																		<xsl:value-of select=" waterDepth"/><fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="waterDepth/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>   
									      		</xsl:when>
											    <xsl:otherwise>
											    	<!-- GROUND LEVEL -->																							
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
															<fo:block text-align="left"> Ground Level: </fo:block>
														</fo:table-cell>				
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
															<xsl:variable name="glHeightMsl" select="/root/modules/Well/Well/glHeightMsl" />
																<xsl:choose>
																	<xsl:when test=" ($glHeightMsl = '') or ($glHeightMsl='null') ">
																	</xsl:when>
					                       							<xsl:otherwise>	
																		<xsl:value-of select="$glHeightMsl"/><fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="$glHeightMsl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>
											    </xsl:otherwise>
									     	</xsl:choose>		
											
											<!-- DATUM -->
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"><xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/currentDatumDisplayLabel"/>: </fo:block>
												</fo:table-cell>				
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="currentDatumDisplayValue" select="/root/modules/Operation/Operation/dynaAttr/currentDatumDisplayValue" />
														<xsl:choose>
															<xsl:when test="($currentDatumDisplayValue = '') or ($currentDatumDisplayValue = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>	
																<xsl:value-of select="$currentDatumDisplayValue"/>&#160;
																<xsl:value-of select="$currentDatumDisplayValue/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											
											<xsl:choose>
									      		<xsl:when test="onOffShore = 'OFF'">
												<!-- TO SEABED -->
												<fo:table-row>
													<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
														<fo:block text-align="left"> <xsl:value-of select="/root/datum/currentDatumType"/> To Seabed: </fo:block>
													</fo:table-cell>				
													<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
														<fo:block text-align="right">
														<xsl:variable name="rkbToMlGl" select="/root/modules/Operation/Operation/rkbToMlGl" />
															<xsl:choose>
																<xsl:when test="($rkbToMlGl = '') or ($rkbToMlGl = 'null') ">
																</xsl:when>
				                       							<xsl:otherwise>
																	<xsl:value-of select="$rkbToMlGl"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="$rkbToMlGl/@uomSymbol"/>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												</xsl:when>
												<xsl:otherwise>
												</xsl:otherwise>
											</xsl:choose>
		
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Plan TD (MD): </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="plannedTdMdMsl" select="/root/modules/Wellbore/Wellbore/plannedTdMdMsl" />
														<xsl:choose>
															<xsl:when test="($plannedTdMdMsl = '') or ($plannedTdMdMsl = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$plannedTdMdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$plannedTdMdMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Plan TD (TVD): </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="plannedTdTvdMsl" select="/root/modules/Wellbore/Wellbore/plannedTdTvdMsl" />
														<xsl:choose>
															<xsl:when test="($plannedTdTvdMsl = '') or ($plannedTdTvdMsl = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$plannedTdTvdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$plannedTdTvdMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>															
									</fo:table>
								</fo:block>											
							</fo:table-cell>
							
							<fo:table-cell border-right="0.5px solid black">
								<fo:block>
									<fo:table width="100%" table-layout="fixed">
										<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
										<fo:table-body>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Current Hole Size: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="right">
														<xsl:variable name="lastHolesize" select="/root/modules/ReportDaily/ReportDaily/lastHolesize" />
														<xsl:choose>
															<xsl:when test="($lastHolesize = '') or ($lastHolesize = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$lastHolesize"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$lastHolesize/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Measured Depth: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="right">
														<xsl:variable name="depthMdMsl" select="/root/modules/ReportDaily/ReportDaily/depthMdMsl" />
														<xsl:choose>
															<xsl:when test="($depthMdMsl = '') or ($depthMdMsl = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$depthMdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$depthMdMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> True Vertical Depth: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:variable name="depthTvdMsl" select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl" />
														<xsl:choose>
															<xsl:when test="($depthTvdMsl = '') or ($depthTvdMsl = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$depthTvdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$depthTvdMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> 24 Hr Progress: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:variable name="dailyid" select="/root/modules/Daily/Daily/dailyUid"/>
														<xsl:variable name="progress" select="string(sum(/root/modules/Bharun/Bharun/BharunDailySummary[dailyUid = $dailyid]/progress/@rawNumber))"/>
														<xsl:choose>
															<xsl:when test="($progress = '') or ($progress = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>												
																<xsl:value-of select="d2Utils:formatNumber($progress,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Bharun/Bharun/BharunDailySummary[dailyUid = $dailyid]/progress/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Days On Well: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysOnWell,'d','')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Days Since Spud: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysFromSpud,'d','')"/>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Last BOP Date: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">											
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDaily/ReportDaily/lastbopDate/@epochMS,'dd MMM yyyy')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> FIT/LOT: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right" wrap-option="no-wrap">
													<xsl:variable name="lastFit" select="/root/modules/ReportDaily/ReportDaily/lastFit" />
													<xsl:variable name="lastLot" select="/root/modules/ReportDaily/ReportDaily/lastLot" />
														<xsl:choose>
															<xsl:when test="(($lastFit = '') or ($lastFit = 'null')) and (($lastLot = '') or ($lastLot = 'null'))">
															</xsl:when>
															<xsl:when test="(($lastFit != '') or ($lastFit != 'null')) and (($lastLot = '') or ($lastLot = 'null'))">
																<xsl:value-of select="$lastFit"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$lastFit/@uomSymbol"/>
																/
															</xsl:when>
															<xsl:when test="(($lastFit = '') or ($lastFit = 'null')) and (($lastLot != '') or ($lastLot != 'null'))">
																/
																<xsl:value-of select="$lastLot"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$lastLot/@uomSymbol"/>
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit/@uomSymbol"/> / 
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>	
										</fo:table-body>															
		                            </fo:table>
								</fo:block>											
							</fo:table-cell>
							
							<fo:table-cell border-right="0.5px solid black">
		                        <fo:block>
		                        	<fo:table width="100%" table-layout="fixed">
		                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
										<fo:table-body>
											
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Casing OD: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="lastCsgsize" select="/root/modules/ReportDaily/ReportDaily/lastCsgsize" />
														<xsl:choose>
															<xsl:when test="($lastCsgsize = '') or ($lastCsgsize = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$lastCsgsize"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$lastCsgsize/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Casing MD: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="lastCsgshoeMdMsl" select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl" />
														<xsl:choose>
															<xsl:when test="($lastCsgshoeMdMsl = '') or ($lastCsgshoeMdMsl = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$lastCsgshoeMdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$lastCsgshoeMdMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Casing TVD: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="lastCsgshoeTvdMsl" select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeTvdMsl" />
														<xsl:choose>
															<xsl:when test="($lastCsgshoeTvdMsl = '') or ($lastCsgshoeTvdMsl = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$lastCsgshoeTvdMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$lastCsgshoeTvdMsl/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<!-- If Operation Type = PNA -->
											<xsl:choose>
		                    					<xsl:when test="/root/modules/Operation/Operation/operationCode='PNA'">
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block text-align="left"> Daily Def. Prod.:</fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
															<xsl:variable name="daydeferredprod" select="/root/modules/ReportDaily/ReportDaily/daydeferredprod" />
																<xsl:choose>
																	<xsl:when test="($daydeferredprod = '') or ($daydeferredprod = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="$daydeferredprod"/><fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="$daydeferredprod/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																	
														</fo:table-cell>
													</fo:table-row>
													
												</xsl:when>
												<!-- ELSE (other operation types) -->
						                    	<xsl:otherwise>
						                    		<fo:table-row>
							                    		<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block text-align="left"> TOL MD: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
															<xsl:variable name="lastLinerTopMdMsl" select="/root/modules/ReportDaily/ReportDaily/lastLinerTopMdMsl" />
																<xsl:choose>
																	<xsl:when test="($lastLinerTopMdMsl = '') or ($lastLinerTopMdMsl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="$lastLinerTopMdMsl"/><fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="$lastLinerTopMdMsl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																	
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block text-align="left"> TOL TVD: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
															<xsl:variable name="lastLinerTopTvdMsl" select="/root/modules/ReportDaily/ReportDaily/lastLinerTopTvdMsl" />
																<xsl:choose>
																	<xsl:when test="($lastLinerTopTvdMsl = '') or ($lastLinerTopTvdMsl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="$lastLinerTopTvdMsl"/><fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="$lastLinerTopTvdMsl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																	
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block text-align="left"> Liner Shoe MD: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
															<xsl:variable name="lastLinerMdMsl" select="/root/modules/ReportDaily/ReportDaily/lastLinerMdMsl" />
																<xsl:choose>
																	<xsl:when test="($lastLinerMdMsl = '') or ($lastLinerMdMsl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="$lastLinerMdMsl"/><fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="$lastLinerMdMsl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>	
															</fo:block>																	
														</fo:table-cell>
													</fo:table-row>
													<fo:table-row>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
															<fo:block text-align="left"> Liner Shoe TVD: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
															<fo:block text-align="right">
															<xsl:variable name="lastLinerTvdMsl" select="/root/modules/ReportDaily/ReportDaily/lastLinerTvdMsl" />
																<xsl:choose>
																	<xsl:when test="($lastLinerTvdMsl = '') or ($lastLinerTvdMsl = 'null') ">
																	</xsl:when>
					                       							<xsl:otherwise>
																		<xsl:value-of select="$lastLinerTvdMsl"/><fo:inline color="white">.</fo:inline>
																		<xsl:value-of select="$lastLinerTvdMsl/@uomSymbol"/>
																	</xsl:otherwise>
																</xsl:choose>
															</fo:block>																	
														</fo:table-cell>
													</fo:table-row>
						                    	</xsl:otherwise>
						                    </xsl:choose>
						                    <!-- END -->
										</fo:table-body>
									</fo:table>
								</fo:block>											
							</fo:table-cell>
							
							<fo:table-cell>
		                        <fo:block>
		                        	<fo:table width="100%" table-layout="fixed">
		                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
										<fo:table-body>	
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> AFE Number: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:choose>													
															<xsl:when test = "string(/root/modules/Operation/Operation/afeNumber/@lookupLabel)=''">
																<xsl:value-of select="/root/modules/Operation/Operation/afeNumber"/>										
															</xsl:when>													
															<xsl:otherwise>
																<xsl:value-of select="/root/modules/Operation/Operation/afeNumber/@lookupLabel"/>
															</xsl:otherwise>
														</xsl:choose>												
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Original AFE: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="afe" select="/root/modules/Operation/Operation/afe" />
														<xsl:choose>
															<xsl:when test="($afe = '') or ($afe = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="d2Utils:formatNumber(/root/modules/Operation/Operation/afe,'#,###,###,##0')"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>	
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Supp. AFE No: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
														<xsl:value-of select="/root/modules/Operation/Operation/secondaryafeRef"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Orig. &amp; Sup. AFE: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="afe" select="/root/modules/Operation/Operation/afe" />
													<xsl:variable name="secondaryafe" select="/root/modules/Operation/Operation/secondaryafeAmt" />
														<xsl:choose>
				                                            <xsl:when test="(($afe = '') or ($afe = 'null')) and (($secondaryafe = '') or ($secondaryafe = 'null'))">
				                                            </xsl:when>
				                                            <xsl:when test="(($afe != '') or ($afe != 'null')) and (($secondaryafe = '') or ($secondaryafe = 'null'))">
				                                            	<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Operation/Operation/afe"/>
															</xsl:when>
				                                            <xsl:when test="($afe = '') or ($afe = 'null') ">
				                                            </xsl:when>
				                                            <xsl:otherwise>
				                                            	<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/><fo:inline color="white">.</fo:inline>
				                                                <xsl:value-of select="d2Utils:formatNumber(string(/root/modules/Operation/Operation/afe/@rawNumber + /root/modules/Operation/Operation/secondaryafeAmt/@rawNumber),'#,###,###,##0')"/>
				                                            </xsl:otherwise>
				                                        </xsl:choose>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Daily Cost: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="right">
													<xsl:variable name="daycost" select="/root/modules/ReportDaily/ReportDaily/daycost" />
														<xsl:choose>
															<xsl:when test="($daycost = '') or ($daycost = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="$daycost/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>
																<xsl:value-of select="$daycost"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
													<fo:block text-align="left"> Cum. Cost: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">
													<xsl:variable name="cumcost" select="/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost" />
														<xsl:choose>
															<xsl:when test="($cumcost = '') or ($cumcost = 'null') ">
															</xsl:when>
			                       							<xsl:otherwise>
																<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
																<fo:inline color="white">.</fo:inline>												
																<xsl:value-of select="d2Utils:formatNumber($cumcost,'##,###')"/>
															</xsl:otherwise>
														</xsl:choose>
													</fo:block>																	
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Last LTI Date: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">											
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDaily/ReportDaily/lastLtiDate/@epochMS,'dd MMM yyyy')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
													<fo:block text-align="left"> Days Since LTI: </fo:block>
												</fo:table-cell>
												<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
													<fo:block text-align="right">											
														<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/daysSinceLta,'#,##0')"/>
													</fo:block>																		
												</fo:table-cell>
											</fo:table-row>
										</fo:table-body>
									</fo:table>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
				<!-- End Well Data Section for other operation type -->
			</xsl:otherwise>
	    </xsl:choose>	
		<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="2pt" border="0.5px solid black" padding="0.5px">
			<fo:table-body>
				<fo:table-row>					
					<fo:table-cell>
                        <fo:block>
                        	<fo:table width="100%" table-layout="fixed" font-size="8pt"
								wrap-option="wrap" space-after="2pt">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Current Op @ 0600: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left" linefeed-treatment="preserve">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportCurrentStatus"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Planned Op: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left" linefeed-treatment="preserve">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPlanSummary"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>	
			</fo:table-body>
		</fo:table>	
		
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" padding="0.5px" wrap-option="wrap" space-after="2pt" space-before="3pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" border-bottom="0.5px solid black" font-size="8pt" font-weight="bold" background-color="rgb(223,223,223)"
						padding-left="2px" padding-right="2px">
						<fo:block text-align="left">
							Summary for Period 0000 Hrs to 2400 Hrs on <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportDatetime"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPeriodSummary"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>								
	</xsl:template>
	<!-- wells section END -->
</xsl:stylesheet>	