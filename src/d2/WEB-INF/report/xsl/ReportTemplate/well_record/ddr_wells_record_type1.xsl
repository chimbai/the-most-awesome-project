<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<!-- ONSHORE TEMPLATE -->
	<!-- wells section BEGIN -->
	<xsl:template match="modules/Well/Well">
		<fo:table width="100%" table-layout="fixed" space-after="2pt" border="0.5px solid black">
            <fo:table-column column-number="1" column-width="proportional-column-width(14)"/>
            <fo:table-column column-number="2" column-width="proportional-column-width(16)"/>
            <fo:table-column column-number="3" column-width="proportional-column-width(23)"/>
            <fo:table-column column-number="4" column-width="proportional-column-width(17)"/>
            <fo:table-column column-number="5" column-width="proportional-column-width(14)"/>
            <fo:table-column column-number="6" column-width="proportional-column-width(16)"/>
            <fo:table-body>
            	<fo:table-row>
                    <fo:table-cell padding="1px" background-color="rgb(223,223,223)"  number-columns-spanned="6" 
						border-bottom="0.5px solid black" padding-left="2px" padding-right="2px">
                        <fo:block font-size="10pt" font-weight="bold" text-align="left">
                            <fo:inline text-decoration="underline" color="black">
                                <xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/completeOperationName" />
                            </fo:inline>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap">
                        <fo:block>Date:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="right" wrap-option="wrap" border-right="0.5px solid black">
                        <fo:block>
							<xsl:variable name="dailyid" select="/root/modules/ReportDaily/ReportDaily/dailyUid"/>
							<xsl:variable name="daily" select="/root/modules/Daily/Daily"/>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($daily[dailyUid=$dailyid]/dayDate/@epochMS,'dd MMM yyyy')"/> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px"  text-align="left" wrap-option="wrap">
                        <fo:block>Day Wellsite Representative:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="right" wrap-option="wrap"  border-right="0.5px solid black">
                        <fo:block>
                            <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/snrdrillsupervisor"/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap">
                        <fo:block>Rig Manager:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="right" wrap-option="wrap">
                        <fo:block>
                            <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigsuperintendent"/> 
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell font-size="8pt" padding="1px"  text-align="left" wrap-option="wrap">
                        <fo:block>Report Number:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="right" wrap-option="wrap"  border-right="0.5px solid black">
                        <fo:block>
                            <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportNumber" /> 
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px"  text-align="left" wrap-option="wrap">
                        <fo:block>Night Wellsite Representative:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="right" wrap-option="wrap"  border-right="0.5px solid black">
                        <fo:block>
                            <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/drillsupervisor"/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap">
                        <fo:block>Drilling Company:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="right" wrap-option="wrap">
                        <fo:block>
                            <xsl:variable name="currentrig" select="/root/modules/Operation/Operation/rigInformationUid" />
				            <xsl:variable name="current_drillcoid" select="/root/modules/RigInformation/RigInformation[rigInformationUid=$currentrig]/rigManager/@lookupLabel" />
				            <xsl:value-of select="$current_drillcoid"/> 
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap">
                        <!-- <fo:block>Latitude: (<xsl:value-of select="//wells/V_wells.loc_lat_ns"/>)</fo:block> -->
                        <fo:block>Latitude (South):</fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="right" wrap-option="wrap"  border-right="0.5px solid black">
                        <fo:block>
                        	<xsl:value-of select="latDeg" /> &#176;
                        	<xsl:value-of select="latMinute" /> &#39;
                        	<xsl:value-of select="latSecond" /> &#34;
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap">                        
                        <fo:block>Longitude (East):</fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="right" wrap-option="wrap" border-right="0.5px solid black">
                        <fo:block>
                        	<xsl:value-of select="longDeg" /> &#176;
                        	<xsl:value-of select="longMinute" /> &#39;
                        	<xsl:value-of select="longSecond" /> &#34;                            
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="left" wrap-option="wrap">
                        <fo:block>Wellsite Geologist:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell font-size="8pt" padding="1px" text-align="right" wrap-option="wrap">
                        <fo:block>
                            <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/geologist" /> 
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
		
		<fo:table width="100%" table-layout="fixed" font-size="8pt"
			wrap-option="wrap" space-after="2pt" border="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(26)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(29)"/>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="1px" background-color="rgb(223,223,223)" number-columns-spanned="4" 
						border-bottom="0.5px solid black" padding-left="2px" padding-right="2px">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Well Details
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>				
				<fo:table-row font-size="8pt">
					<fo:table-cell border-right="0.5px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Country: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="right">																			
												<xsl:value-of select="country/@lookupLabel"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Field: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">	
											<fo:block text-align="right">
												<xsl:value-of select="field"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Rig: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">												
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>
												<!-- <xsl:value-of select="/root/modules/Operation/Operation/rigInformationUid/@lookupLabel"/> -->
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
									
									<!-- GROUND LEVEL -->																							
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Ground Level: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Well/Well/glHeightMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Well/Well/glHeightMsl/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
									<!-- DATUM -->
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"><xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/currentDatumDisplayLabel"/>: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/currentDatumDisplayValue"/>&#160;
												<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/currentDatumDisplayValue/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
									<!-- TO GROUND -->
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> <xsl:value-of select="/root/datum/currentDatumType"/> to Ground: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Operation/Operation/rkbToMlGl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Operation/Operation/rkbToMlGl/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>

									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Planned TD (MD): </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Wellbore/Wellbore/plannedTdMdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Wellbore/Wellbore/plannedTdMdMsl/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>	
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Planned TD (TVD): </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Wellbore/Wellbore/plannedTdTvdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Wellbore/Wellbore/plannedTdTvdMsl/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>															
							</fo:table>
						</fo:block>											
					</fo:table-cell>
					
					<fo:table-cell border-right="0.5px solid black">
						<fo:block>
							<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Current Hole Size: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastHolesize"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastHolesize/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Measured Depth: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl/@uomSymbol"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> True Vertical Depth: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@uomSymbol"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> 24 Hr Progress: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:variable name="dailyid" select="/root/modules/Daily/Daily/dailyUid"/>
												<xsl:variable name="progress" select="string(sum(/root/modules/Bharun/Bharun/BharunDailySummary[dailyUid = $dailyid]/progress/@rawNumber))"/>												
												<xsl:value-of select="d2Utils:formatNumber($progress,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Bharun/Bharun/BharunDailySummary[dailyUid = $dailyid]/progress/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Days On Well: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysOnWell,'d','')"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Days Since Spud: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysFromSpud,'d','')"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Last BOP Date: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">											
												<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDaily/ReportDaily/lastbopDate/@epochMS,'dd MMM yyyy')"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>	
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> FIT/LOT: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right" wrap-option="no-wrap">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit/@uomSymbol"/> / 
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>	
								</fo:table-body>															
                            </fo:table>
						</fo:block>											
					</fo:table-cell>
					
					<fo:table-cell border-right="0.5px solid black">
                        <fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
								<fo:table-body>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Casing OD: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Casing MD: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Casing TVD: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeTvdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeTvdMsl/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> TOL MD: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLinerTopMdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLinerTopMdMsl/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> TOL TVD: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLinerTopTvdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLinerTopTvdMsl/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Liner Shoe MD: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLinerMdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLinerMdMsl/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Liner Shoe TVD: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLinerTvdMsl"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLinerTvdMsl/@uomSymbol"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
																	
								</fo:table-body>
							</fo:table>
						</fo:block>											
					</fo:table-cell>
					
					<fo:table-cell>
                        <fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
								<fo:table-body>	
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> AFE Number: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:choose>													
													<xsl:when test = "string(/root/modules/Operation/Operation/afeNumber/@lookupLabel)=''">
														<xsl:value-of select="/root/modules/Operation/Operation/afeNumber"/>										
													</xsl:when>													
													<xsl:otherwise>
														<xsl:value-of select="/root/modules/Operation/Operation/afeNumber/@lookupLabel"/>
													</xsl:otherwise>
												</xsl:choose>												
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Original AFE: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="d2Utils:formatNumber(/root/modules/Operation/Operation/afe,'#,###,###,##0')"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>	
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Supp. AFE No: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Operation/Operation/secondaryafeRef"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Orig. &amp; Sup. AFE: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:choose>
		                                            <xsl:when test="string-length(/root/modules/Operation/Operation/secondaryafeAmt)&lt;2">
		                                            	<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/Operation/Operation/afe"/>
		                                            </xsl:when>
		                                            <xsl:otherwise>
		                                            	<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/><fo:inline color="white">.</fo:inline>
		                                                <xsl:value-of select="d2Utils:formatNumber(string(/root/modules/Operation/Operation/afe/@rawNumber + /root/modules/Operation/Operation/secondaryafeAmt/@rawNumber),'#,###,###,##0')"/>
		                                            </xsl:otherwise>
		                                        </xsl:choose>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Daily Cost: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">																
											<fo:block text-align="left"> Cum. Cost: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
												<fo:inline color="white">.</fo:inline>												
												<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost,'##,###')"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Last LTI Date: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">											
												<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDaily/ReportDaily/lastLtiDate/@epochMS,'dd MMM yyyy')"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Days Since Last LTI: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">			
											<fo:block text-align="right">											
												<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/daysSinceLta,'#,##0')"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>					
					<fo:table-cell border-top="0.5px solid black" number-columns-spanned="4">
                        <fo:block>
                        	<fo:table width="100%" table-layout="fixed" font-size="8pt"
								wrap-option="wrap" space-after="2pt">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Current Op @ 0600: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left" linefeed-treatment="preserve">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportCurrentStatus"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Planned Op: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left" linefeed-treatment="preserve">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPlanSummary"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>	
								
			</fo:table-body>
		</fo:table>	
		
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="2pt" space-before="3pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" border-bottom="0.5px solid black" font-size="10pt" font-weight="bold" background-color="rgb(223,223,223)"
						padding-left="2px" padding-right="2px">
						<fo:block text-align="left">
							Summary of Period 0000 to 2400 Hrs
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block><xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPeriodSummary"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>								
	</xsl:template>
	<!-- wells section END -->
</xsl:stylesheet>	