<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<!-- OFFSHORE TEMPLATE -->
	<!-- wells section BEGIN -->
	<xsl:template match="modules/Well/Well">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="8pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(35)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell font-size="10pt" font-weight="bold">
						<fo:block>
							<xsl:variable name="dailyid" select="/root/modules/ReportDaily/ReportDaily/dailyUid"/>
							<xsl:variable name="daily" select="/root/modules/Daily/Daily"/>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($daily[dailyUid=$dailyid]/dayDate/@epochMS,'dd MMM yyyy')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-size="10pt" font-weight="bold" text-align="right">
						<fo:block>
							From :
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" font-size="10pt" font-weight="bold" text-align="left" wrap-option="wrap">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/sentby"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="2" font-weight="bold" font-size="10pt" text-align="right">
						<fo:block>
							To :
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" font-size="10pt" font-weight="bold" text-align="left" wrap-option="wrap">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/sentto"/>
						</fo:block>
					 </fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<fo:table width="100%" table-layout="fixed" space-after="2pt" border="0.5px solid black">
            <fo:table-column column-number="1" column-width="proportional-column-width(22)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(27)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(26)"/>
			<fo:table-body>
            	<fo:table-row>
                    <fo:table-cell padding="1px" background-color="rgb(223,223,223)"  number-columns-spanned="2" 
						border-bottom="0.5px solid black" padding-left="2px" padding-right="2px">
                        <fo:block font-size="10pt" font-weight="bold" text-align="left">
							Well Data
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell padding="1px" background-color="rgb(223,223,223)"  number-columns-spanned="2"
						border-bottom="0.5px solid black" padding-left="2px" padding-right="2px">
						<fo:block font-size="10pt" font-weight="bold" text-align="right">
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/qcFlag/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>				
				</fo:table-row>	
                <fo:table-row font-size="8pt">
                	<!-- First Column -->
					<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
												<fo:block text-align="left"> Country: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Well/Well/country/@lookupLabel"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Field: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Well/Well/field"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
					                        <fo:block text-align="left"> Rig: </fo:block>
					                    </fo:table-cell>
					                    <fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
					                        <fo:block text-align="right">
					                            <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>
					                        </fo:block>
					                    </fo:table-cell>
				                    </fo:table-row>
									<fo:table-row>
										<xsl:variable name="currentDatumReferencePoint" select="/root/datum/currentDatumReferencePoint"/>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
											<fo:block text-align="left">
												<xsl:variable name="currentDatumType" select="/root/datum/currentDatumType"/>
												<xsl:choose>
													<xsl:when test="$currentDatumType!=''">
														<xsl:value-of select="$currentDatumType"/> - 
													</xsl:when>
												</xsl:choose>
												<xsl:value-of select="$currentDatumReferencePoint"/>:
											</fo:block>
										</fo:table-cell>
										<xsl:variable name="rtmsl" select="/root/modules/Operation/Operation/dynaAttr/rtmsl"></xsl:variable>
										<xsl:choose>
											<xsl:when test="$rtmsl!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$rtmsl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>											
											</xsl:when>
											<xsl:otherwise>
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$currentDatumReferencePoint"/>
													</fo:block>		
												</fo:table-cell>										
											</xsl:otherwise>
										</xsl:choose>
									</fo:table-row>
									<xsl:choose>													
										<xsl:when test = "/root/modules/Well/Well/onOffShore='OFF'">
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
													<fo:block text-align="left">Water Depth: </fo:block>
												</fo:table-cell>		
												<xsl:variable name="waterDepth" select="/root/modules/Well/Well/waterDepth"></xsl:variable>		
												<xsl:choose>
													<xsl:when test="$waterDepth!=''">
														<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="$waterDepth"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Well/Well/waterDepth/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</xsl:when>
												</xsl:choose>
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
												<fo:block text-align="left">
												<xsl:variable name="currentDatumType" select="/root/datum/currentDatumType"></xsl:variable>
												<xsl:choose>
													<xsl:when test="$currentDatumType!=''">
														<xsl:value-of select="$currentDatumType"/> - 
													</xsl:when>
												</xsl:choose>
												Seabed:
												</fo:block>
												</fo:table-cell>
												<xsl:variable name="rkbToMlGl" select="/root/modules/Operation/Operation/rkbToMlGl"></xsl:variable>
												<xsl:choose>
													<xsl:when test="$rkbToMlGl!=''">
														<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
															<fo:block text-align="right">																		
																<xsl:value-of select="$rkbToMlGl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Operation/Operation/rkbToMlGl/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</xsl:when>
												</xsl:choose>
											</fo:table-row>
										</xsl:when>
										<xsl:otherwise>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
													<fo:block text-align="left">Ground Level: </fo:block>
												</fo:table-cell>		
												<xsl:variable name="glHeightMsl" select="/root/modules/Well/Well/glHeightMsl"></xsl:variable>	
												<xsl:choose>
													<xsl:when test="$glHeightMsl!=''">
														<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="$glHeightMsl"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Well/Well/glHeightMsl/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</xsl:when>
												</xsl:choose>	
											</fo:table-row>
											<fo:table-row>
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
													<fo:block text-align="left">
														<xsl:variable name="currentDatumType" select="/root/datum/currentDatumType"></xsl:variable>
														<xsl:choose>
															<xsl:when test="$currentDatumType!=''">
																<xsl:value-of select="$currentDatumType"/> - 
															</xsl:when>
														</xsl:choose>
														Hanger:
													</fo:block>
												</fo:table-cell>
												<xsl:variable name="zRtthgr" select="/root/modules/Operation/Operation/zRtthgr"></xsl:variable>	
												<xsl:choose>
													<xsl:when test="$zRtthgr!=''">
														<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
															<fo:block text-align="right">																			
																<xsl:value-of select="$zRtthgr"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Operation/Operation/zRtthgr/@uomSymbol"/>
															</fo:block>																
														</fo:table-cell>
													</xsl:when>
												</xsl:choose>
											</fo:table-row>
										</xsl:otherwise>
									</xsl:choose>
								</fo:table-body>								
							</fo:table>
						</fo:block>
					</fo:table-cell>
                	
                	<!-- Second Column -->
					<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Current Hole Size: </fo:block>
										</fo:table-cell>		
										<xsl:variable name="lastHolesize" select="/root/modules/ReportDaily/ReportDaily/lastHolesize"></xsl:variable>
										<xsl:choose>
											<xsl:when test="$lastHolesize!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$lastHolesize"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastHolesize/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</xsl:when>
										</xsl:choose>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-ht="3px">																
											<fo:block text-align="left"> Measured Depth: </fo:block>
										</fo:table-cell>	
										<xsl:variable name="depthMdMsl" select="/root/modules/ReportDaily/ReportDaily/depthMdMsl"></xsl:variable>		
										<xsl:choose>
											<xsl:when test="$depthMdMsl!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$depthMdMsl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</xsl:when>
										</xsl:choose>	
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> True Vertical Depth: </fo:block>
										</fo:table-cell>	
										<xsl:variable name="depthTvdMsl" select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl"></xsl:variable>	
										<xsl:choose>
											<xsl:when test="$depthTvdMsl!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$depthTvdMsl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</xsl:when>
										</xsl:choose>	
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Plan TD (MD): </fo:block>
										</fo:table-cell>	
										<xsl:variable name="prognosedSectionMdMsl" select="/root/modules/ReportDaily/ReportDaily/prognosedSectionMdMsl"></xsl:variable>	
										<xsl:choose>
											<xsl:when test="$prognosedSectionMdMsl!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$prognosedSectionMdMsl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/prognosedSectionMdMsl/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</xsl:when>
										</xsl:choose>	
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Plan TD (TVD): </fo:block>
										</fo:table-cell>	
										<xsl:variable name="prognosedSectionTvdMsl" select="/root/modules/ReportDaily/ReportDaily/prognosedSectionTvdMsl"></xsl:variable>	
										<xsl:choose>
											<xsl:when test="$prognosedSectionTvdMsl!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$prognosedSectionTvdMsl"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/prognosedSectionTvdMsl/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</xsl:when>
										</xsl:choose>	
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left">  </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
											<fo:block text-align="right">
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left">  </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
											<fo:block text-align="right">
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>								
							</fo:table>
						</fo:block>
					</fo:table-cell>
					
					<!-- Third Column -->
					<fo:table-cell border-bottom="0.5px solid black" border-right="0.5px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Last Csg Size: </fo:block>
										</fo:table-cell>	
										<xsl:variable name="lastCsgsize" select="/root/modules/ReportDaily/ReportDaily/lastCsgsize"></xsl:variable>
										<xsl:choose>
											<xsl:when test="$lastCsgsize!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$lastCsgsize"/>
														<fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</xsl:when>
										</xsl:choose>			
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Last Csg Shoe MD: </fo:block>
										</fo:table-cell>				
										<xsl:variable name="lastCsgshoeMdMsl" select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl"></xsl:variable>
										<xsl:choose>
											<xsl:when test="$lastCsgshoeMdMsl!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$lastCsgshoeMdMsl"/>
														<fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</xsl:when>
										</xsl:choose>				

									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Last Csg Shoe TVD: </fo:block>
										</fo:table-cell>				
										<xsl:variable name="lastCsgshoeTvdMsl" select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeTvdMsl"></xsl:variable>
										<xsl:choose>
											<xsl:when test="$lastCsgshoeTvdMsl!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
													<fo:block text-align="right">																			
														<xsl:value-of select="$lastCsgshoeTvdMsl"/>
														<fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeTvdMsl/@uomSymbol"/>
													</fo:block>																
												</fo:table-cell>
											</xsl:when>
										</xsl:choose>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Last FIT: </fo:block>
										</fo:table-cell>				
										<xsl:variable name="lastFit" select="/root/modules/ReportDaily/ReportDaily/lastFit"></xsl:variable>																		
										<xsl:choose>
											<xsl:when test="$lastFit!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
													<fo:block text-align="right">
														<xsl:value-of select="$lastFit"/>
														<fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit/@uomSymbol"/>
													</fo:block>	
												</fo:table-cell>												
											</xsl:when>
										</xsl:choose>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Last LOT: </fo:block>
										</fo:table-cell>				
										<xsl:variable name="lastLot" select="/root/modules/ReportDaily/ReportDaily/lastLot"></xsl:variable>																		
										<xsl:choose>
											<xsl:when test="$lastLot!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
													<fo:block text-align="right">
														<xsl:value-of select="$lastLot"/>
														<fo:inline color="white">.</fo:inline>
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot/@uomSymbol"/>
													</fo:block>	
												</fo:table-cell>												
											</xsl:when>
										</xsl:choose>
									</fo:table-row>
								</fo:table-body>								
							</fo:table>
						</fo:block>
					</fo:table-cell>
					
					<!-- Fourth Column -->
					<fo:table-cell border-bottom="0.5px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Daily Cost: </fo:block>
										</fo:table-cell>
										<xsl:variable name="daycost" select="/root/modules/ReportDaily/ReportDaily/daycost"></xsl:variable>				
										<xsl:choose>
											<xsl:when test="$daycost!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
													<fo:block text-align="right">
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
														<fo:inline color="white">.</fo:inline>
														<xsl:value-of select="$daycost"/>
													</fo:block>	
												</fo:table-cell>												
											</xsl:when>
										</xsl:choose>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Cum. Cost: </fo:block> 
										</fo:table-cell>	
										<xsl:variable name="cumcost" select="/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost"></xsl:variable>				
										<xsl:choose>
											<xsl:when test="$cumcost!=''">
												<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">
													<fo:block text-align="right">
														<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
														<fo:inline color="white">.</fo:inline>	
														<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost,'##,###')"/> 
														</fo:block>
												</fo:table-cell>												
											</xsl:when>
										</xsl:choose>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Latitude (<xsl:value-of select="/root/modules/Well/Well/latNs/@lookupLabel"/>): </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Well/Well/latDeg"/>
												<fo:inline>&#x00B0;</fo:inline><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Well/Well/latMinute"/>
												<fo:inline>&#39;</fo:inline><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Well/Well/latSecond"/>
												<fo:inline>&#34;</fo:inline>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">																
											<fo:block text-align="left"> Longitude (<xsl:value-of select="/root/modules/Well/Well/longEw/@lookupLabel"/>): </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="3px" padding-right="3px">	
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Well/Well/longDeg"/>
												<fo:inline>&#x00B0;</fo:inline><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Well/Well/longMinute"/>
												<fo:inline>&#39;</fo:inline><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Well/Well/longSecond"/>
												<fo:inline>&#34;</fo:inline>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>								
							</fo:table>
						</fo:block>
					</fo:table-cell>
                </fo:table-row> 
                
                  <fo:table-row>					
					<fo:table-cell number-columns-spanned="8">
                        <fo:block>
                        	<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="2pt">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(90)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Current Op's @ 0600: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left" linefeed-treatment="preserve">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportCurrentStatus"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left"> Planned Op: </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="left" linefeed-treatment="preserve">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPlanSummary"/>
											</fo:block>																	
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>	
			</fo:table-body>
        </fo:table>
 		
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="2pt" space-before="3pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" border-bottom="0.5px solid black" font-size="10pt" font-weight="bold" background-color="rgb(223,223,223)"
						padding-left="2px" padding-right="2px">
						<fo:block text-align="left">
							Summary of Period 0000 to 2400 Hrs
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block><xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPeriodSummary"/><fo:inline color="white">!</fo:inline></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>	
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="2pt" space-before="3pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" border-bottom="0.5px solid black" font-size="10pt" font-weight="bold" background-color="rgb(223,223,223)"
						padding-left="2px" padding-right="2px">
						<fo:block text-align="left">
							Critical Issues (Well Related)
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block><xsl:value-of select="/root/modules/ReportDaily/ReportDaily/criticalIssues"/><fo:inline color="white">!</fo:inline></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>	
		<!--
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="2pt" space-before="3pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" border-bottom="0.5px solid black" font-size="10pt" font-weight="bold" background-color="rgb(223,223,223)"
						padding-left="2px" padding-right="2px">
						<fo:block text-align="left">
							EHS LEADERSHIP SECTION
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block><xsl:value-of select="/root/modules/GeneralComment/GeneralComment[commentType='ehs']/comments"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>	
		-->
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="2pt" space-before="3pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" border-bottom="0.5px solid black" font-size="10pt" font-weight="bold" background-color="rgb(223,223,223)"
						padding-left="2px" padding-right="2px">
						<fo:block text-align="left">
							Critical Issues (Rig Related)
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block><xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigCriticalIssues"/><fo:inline color="white">!</fo:inline></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>					
	</xsl:template>
	<!-- wells section END -->
</xsl:stylesheet>	