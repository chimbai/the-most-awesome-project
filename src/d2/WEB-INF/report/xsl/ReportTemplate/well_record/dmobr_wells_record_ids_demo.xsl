<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- D2_OMV_GLOBAL DMOBR WELL SECTION -->
	
	<xsl:template match="modules/Operation/Operation">
				
		<!-- PAGE HEADER -->
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="8pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(40)"/>	
				
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell font-size="8pt" font-weight="bold">
						<fo:block>
							<xsl:variable name="dailyid" select="/root/modules/ReportDaily/ReportDaily/dailyUid"/>
							<xsl:variable name="daily" select="/root/modules/Daily/Daily"/>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($daily[dailyUid=$dailyid]/dayDate/@epochMS,'dd MMM yyyy')"/> 
							<fo:inline color="white">.</fo:inline>
							<xsl:choose>
								<xsl:when test="/root/modules/Well/Well/tzGmtOffset/@rawNumber &gt; 0">
									(GMT +<xsl:value-of select="format-number(/root/modules/Well/Well/tzGmtOffset/@rawNumber, '##.0')"/>)
								</xsl:when>
								<xsl:otherwise>
									(GMT <xsl:value-of select="format-number(/root/modules/Well/Well/tzGmtOffset/@rawNumber, '##.0')"/>)
								</xsl:otherwise>
							</xsl:choose>	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:table inline-progression-dimension="100%" table-layout="fixed" font-size="8pt" font-weight="bold">	
								<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(70)"/>
								<fo:table-body>	
									<fo:table-row>
										<fo:table-cell text-align="right">
											<fo:block>
												From:<fo:inline color="white">..</fo:inline>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/sentby"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell text-align="right">
											<fo:block>
												To:<fo:inline color="white">..</fo:inline>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/sentto"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell text-align="right">
											<fo:block>
												QC Status:<fo:inline color="white">..</fo:inline>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/qcFlag/@lookupLabel"/>	
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:table inline-progression-dimension="100%" table-layout="fixed" font-size="12pt" font-weight="bold" text-align="right">	
								<fo:table-column column-number="1" column-width="proportional-column-width(1)"/>
								<fo:table-body>	
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
												<fo:inline text-decoration="underline">
													MOBILIZATION REPORT #<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportNumber"/>
												</fo:inline>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
												<fo:inline text-decoration="underline">
													<xsl:value-of select="operationName"/>
												</fo:inline>
												<fo:inline color="white">.</fo:inline>
												<xsl:if test="/root/modules/Well/Well/govPermitNumber">
													(<xsl:value-of select="/root/modules/Well/Well/govPermitNumber"/>)
												</xsl:if>
											</fo:block>
								
										</fo:table-cell>
									</fo:table-row>
						
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="2pt" border="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
			<fo:table-body>
			
				<!-- HEADER -->
				<fo:table-row>
					<fo:table-cell padding="2px" border-bottom="0.5px solid black" number-columns-spanned="2">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Well Data
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<!-- SECTION CONTENT -->			
				<fo:table-row font-size="8pt">
				
					<!-- COLUMN 1 -->
					<fo:table-cell border-right="0.5px solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(45)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(55)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="2px">																
											<fo:block text-align="left"> Country: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="2px">	
											<fo:block text-align="right">																			
												<xsl:value-of select="/root/modules/Well/Well/country"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
																	
									<fo:table-row>
										<fo:table-cell padding="2px">
											<fo:block text-align="left"> Field: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="2px">	
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Well/Well/field"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>									
									<fo:table-row>
										<fo:table-cell padding="2px">
											<fo:block text-align="left"> Drill Co. </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="2px">	
											<fo:block text-align="right">
												<xsl:variable name="rigInformationUid" select="/root/modules/ReportDaily/ReportDaily/rigInformationUid"/>																								
												<xsl:value-of select="/root/modules/RigInformation/RigInformation[rigInformationUid = $rigInformationUid]/rigOwner/@lookupLabel"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="2px">
											<fo:block text-align="left"> Rig: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="2px">
											<fo:block text-align="right">												
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>
											</fo:block>																		
										</fo:table-cell>
									</fo:table-row>
											
									<fo:table-row>
										<fo:table-cell padding="2px">
											<fo:block text-align="left"> Water Depth: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Well/Well/waterDepth"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Well/Well/waterDepth/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>	
									
									<fo:table-row>
										<fo:table-cell padding="2px">
											<fo:block text-align="left"> Datum: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="2px">			
											<fo:block text-align="right">
												<xsl:value-of select="/root/modules/Operation/dynaAttr/datumLabel"/>									
											</fo:block>
										</fo:table-cell>
									</fo:table-row>	
									
									<fo:table-row>
										<fo:table-cell padding="2px">
											<fo:block text-align="left"> RT To Seabed: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="2px">			
											<fo:block text-align="right">
												<xsl:variable name="z_rtmsl" select="/root/modules/Operation/Operation/dynaAttr/rtmsl/@rawNumber"/>
												<xsl:choose>													
													<xsl:when test = "string(/root/modules/Well/Well/waterDepth/@rawNumber)!=''">
														<xsl:choose>
															<xsl:when test="string($z_rtmsl)!=''">
																<xsl:variable name="rttoseabed" select="/root/modules/Well/Well/waterDepth/@rawNumber + $z_rtmsl"/>
																<xsl:value-of select="format-number($rttoseabed,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Well/Well/waterDepth/@uomSymbol"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:variable name="rttoseabed" select="/root/modules/Well/Well/waterDepth/@rawNumber + 0"/>
																<xsl:value-of select="format-number($rttoseabed,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Well/Well/waterDepth/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>														
													</xsl:when>		
																								
													<xsl:otherwise>
														<xsl:choose>
															<xsl:when test="string(/root/modules/Well/Well/z_rtmsl)!=''">
																<xsl:variable name="rttoseabed" select="0 + $z_rtmsl"/>
																<xsl:value-of select="format-number($rttoseabed,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Well/Well/waterDepth/@uomSymbol"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="0.0"/><fo:inline color="white">.</fo:inline>
																<xsl:value-of select="/root/modules/Well/Well/waterDepth/@uomSymbol"/>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:otherwise>
												</xsl:choose>											
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>															
							</fo:table>
						</fo:block>											
					</fo:table-cell>
										
					<fo:table-cell>
						<fo:block>
							<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-number="1" column-width="proportional-column-width(35)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(32)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>
								<fo:table-body>
									<fo:table-row>
										<!-- COLUMN 2 -->
										<fo:table-cell>
											<fo:block>
												<fo:table width="100%" table-layout="fixed" border-right="0.5px solid black" border-bottom="0.5px solid black">
													<fo:table-column column-number="1" column-width="proportional-column-width(55)"/>
													<fo:table-column column-number="2" column-width="proportional-column-width(45)"/>
													<fo:table-body>
																				
														<fo:table-row>
															<fo:table-cell padding="2px">
																<fo:block text-align="left"> Measured Depth: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">
																<fo:block text-align="right">
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl/@uomSymbol"/>
																</fo:block>																		
															</fo:table-cell>
														</fo:table-row>
														
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> True Vertical Depth: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">			
																<fo:block text-align="right">
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@uomSymbol"/>
																</fo:block>																		
															</fo:table-cell>
														</fo:table-row>
														
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Progress: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">			
																<fo:block text-align="right">
																	<xsl:variable name="dailyid" select="/root/modules/Daily/Daily/dailyUid"/>
																	<xsl:variable name="progress" select="string(sum(/root/modules/Bharun/Bharun/BharunDailySummary[dailyUid = $dailyid]/progress/@rawNumber))"/>												
																	<xsl:value-of select="d2Utils:formatNumber($progress,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="/root/modules/Bharun/Bharun/BharunDailySummary[dailyUid = $dailyid]/progress/@uomSymbol"/>
																</fo:block>																	
															</fo:table-cell>
														</fo:table-row>
														
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Days From Spud: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">			
																<fo:block text-align="right">
																	<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysFromSpud,'d','')"/>
																</fo:block>
															</fo:table-cell>
														</fo:table-row>
														
					 									<fo:table-row>
															<fo:table-cell padding="2px">
																<fo:block text-align="left"> Days On Well: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">			
																<fo:block text-align="right">
																	<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysOnWell,'d','')"/>
																</fo:block>																		
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>								
					                            </fo:table>
											</fo:block>
										</fo:table-cell>
										
										<!-- COLUMN 3 -->
										<fo:table-cell border-right="0.5px solid black" border-bottom=".5px solid black">
											<fo:block>
												<fo:table width="100%" table-layout="fixed">
					                            	<fo:table-column column-number="1" column-width="proportional-column-width(55)"/>
													<fo:table-column column-number="2" column-width="proportional-column-width(45)"/>
													<fo:table-body>
														<fo:table-row>
															<fo:table-cell padding="2px">
																<fo:block text-align="left"> Current Hole Size: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">
																<fo:block text-align="right">
																	<xsl:choose>
																		<xsl:when test="string(/root/modules/ReportDaily/ReportDaily/lastHolesize/@lookupLabel)=''">
																			<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastHolesize"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastHolesize/@uomSymbol"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastHolesize/@lookupLabel"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:block>																	
															</fo:table-cell>
														</fo:table-row>
														
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Casing OD: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">			
																<fo:block text-align="right">												
																	<xsl:choose>
																		<xsl:when test="string(/root/modules/ReportDaily/ReportDaily/lastCsgsize/@lookupLabel)=''">
																			<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize"/><fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize/@uomSymbol"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize/@lookupLabel"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</fo:block>																	
															</fo:table-cell>
														</fo:table-row>
																							
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Shoe TVD: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">			
																<fo:block text-align="right">
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeTvdMsl"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeTvdMsl/@uomSymbol"/>
																</fo:block>																	
															</fo:table-cell>
														</fo:table-row>
																							
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> F.I.T.: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">			
																<fo:block text-align="right">
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit/@uomSymbol"/>
																</fo:block>																	
															</fo:table-cell>
														</fo:table-row>
														
														<fo:table-row>
															<fo:table-cell padding="2px">
																<fo:block text-align="left"> L.O.T.: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">			
																<fo:block text-align="right">
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot/@uomSymbol"/>
																</fo:block>																	
															</fo:table-cell>
														</fo:table-row>
																						
													</fo:table-body>															
												</fo:table>
											</fo:block>
										</fo:table-cell>
										
										<!-- COLUMN 4 -->
										<fo:table-cell border-bottom=".5px solid black">
											<fo:block>
												<fo:table width="100%" table-layout="fixed">
					                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
													<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
													<fo:table-body>										
														<fo:table-row>
															<fo:table-cell padding="2px">
																<fo:block text-align="left"> AFE Cost: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">			
																<fo:block text-align="right">
																	<xsl:value-of select="afe/@uomSymbol"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="afe"/>
																</fo:block>																		
															</fo:table-cell>
														</fo:table-row>
														
														<fo:table-row>
															<fo:table-cell padding="2px">
																<fo:block text-align="left"> AFE Number: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">			
																<fo:block text-align="right">
																	<xsl:choose>													
																		<xsl:when test = "string(afeNumber/@lookupLabel)=''">
																			<xsl:value-of select="afeNumber"/>										
																		</xsl:when>													
																		<xsl:otherwise>
																			<xsl:value-of select="afeNumber/@lookupLabel"/>
																		</xsl:otherwise>
																	</xsl:choose>												
																</fo:block>																	
															</fo:table-cell>
														</fo:table-row>
														
														<fo:table-row>
															<fo:table-cell padding="2px">
																<fo:block text-align="left"> Daily Cost: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">
																<fo:block text-align="right">
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
																	<fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost"/>
																</fo:block>																	
															</fo:table-cell>
														</fo:table-row>
														
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Cum. Cost: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">			
																<fo:block text-align="right">
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
																	<fo:inline color="white">.</fo:inline>												
																	<xsl:value-of select="d2Utils:formatNumber(/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost,'##,###')"/>
																</fo:block>																	
															</fo:table-cell>
														</fo:table-row>
														
														<fo:table-row>
															<fo:table-cell padding="2px">
																<fo:block text-align="left"> Planned TD: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">
																<fo:block text-align="right">
																	<xsl:value-of select="/root/modules/Wellbore/Wellbore/finalTdMdMsl"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="/root/modules/Wellbore/Wellbore/finalTdMdMsl/@uomSymbol"/>
																</fo:block>																		
															</fo:table-cell>
														</fo:table-row>
														
													</fo:table-body>
												</fo:table>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
									<!-- CURRENT OPERATION @ 0600 AND PLANNED OPERATIONS -->
									<fo:table-row>
										<fo:table-cell number-columns-spanned="3">
											<fo:block>
												<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap">
													<fo:table-column column-width="proportional-column-width(25)"/>
													<fo:table-column column-width="proportional-column-width(75)"/>
													<fo:table-body>
														<fo:table-row font-size="8pt">
															<fo:table-cell padding="2px">
																<fo:block> Current Op's @ 0600: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">
																<fo:block>
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportCurrentStatus"/>
																</fo:block>
															</fo:table-cell>
														</fo:table-row>
														
														<fo:table-row font-size="8pt">
															<fo:table-cell padding="2px">
																<fo:block> Planned Operations: </fo:block>
															</fo:table-cell>
															<fo:table-cell padding="2px">
																<fo:block>
																	<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPlanSummary"/>
																</fo:block>
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>											
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>	
		
		<!-- SUMMARY OF 0000 - 2400 -->
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="2px" border-bottom="0.5px solid black" font-size="10pt" font-weight="bold">
						<fo:block> Summary of Period 0000 to 2400 Hrs </fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row font-size="8pt">
					<fo:table-cell padding="2px">
						<fo:block>
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPeriodSummary"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
										
	</xsl:template>
	<!-- END OF WELL SECTION -->
</xsl:stylesheet>	