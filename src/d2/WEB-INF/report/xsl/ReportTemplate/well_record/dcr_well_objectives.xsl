<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!--WELL OBJECTIVES -->
	<xsl:template match="modules/Well/Well" mode="wellobjectives">
		<fo:table width="100%" table-layout="fixed" font-size="7pt" margin-top="2pt" border="0.75px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(50)"/> 
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell background-color="#FFFACD" padding-left="0.1cm" padding="0.05cm" font-size="7pt" number-columns-spanned="2" font-weight="bold" border-bottom="thin solid black" text-align="center"> <!-- background-color="#FFFACD" -->
						<fo:block>
							WELL OBJECTIVES
						</fo:block>
					</fo:table-cell> 
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"    border-right="0.5px solid black" border-bottom="0.5px solid black"> <!-- background-color="#F5F5F5" -->
						<fo:block>
							 Primary Well Objective
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell border-bottom="0.5px solid black" padding-left="0.1cm" padding-top="0.05cm" >
						<fo:block linefeed-treatment="preserve">
								<xsl:value-of select="(/root/modules/Well/Well/wellObjective)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"    border-right="0.5px solid black">
						<fo:block>
							Secondary Well Objective
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-left="0.1cm" padding-top="0.05cm" >
						<fo:block linefeed-treatment="preserve">
								<xsl:value-of select="(/root/modules/Well/Well/wellObjectiveSecondary)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>