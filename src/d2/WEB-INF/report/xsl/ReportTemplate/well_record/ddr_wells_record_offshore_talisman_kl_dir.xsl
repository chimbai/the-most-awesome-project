<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- WELL SECTION - 20080921 -->
	
	<xsl:template match="modules/Well/Well">
	
		<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-before="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.07cm" number-columns-spanned="2">
						<fo:block text-align="left" font-size="8pt">
							Report Date @ <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/taskType"/>hrs : <xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Daily/Daily/dayDate/@epochMS,'dd MMM yyyy')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
	
		<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-before="2pt" border="0.5px solid black" >
			<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(22)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(27)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(31)"/>
		
		<fo:table-body>
			<fo:table-row>
				<fo:table-cell padding="0.07cm" border-bottom="0.5px solid black" number-columns-spanned="2">
					<fo:block text-align="left" font-size="8pt" font-weight="bold">
						Well Data
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.07cm" border-bottom="0.5px solid black">
					<fo:block text-align="left" font-size="8pt">
						Start Date: <xsl:value-of select="/root/modules/Operation/Operation/startDate"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.07cm" border-bottom="0.5px solid black">
					<fo:block text-align="left" font-size="8pt">
						End Date: <xsl:value-of select="/root/modules/Operation/Operation/rigreleaseDateRigreleaseTime"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			
			<fo:table-row>
				<fo:table-cell padding="0.07cm" border-right="0.5px solid black">
					<fo:block font-size="8pt" text-align="left">
					
					<fo:table>
						<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
						
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.07cm" >
									<fo:block font-size="8pt" text-align="left">
										Field: 
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell padding="0.07cm" >
									<fo:block font-size="8pt" text-align="right">
										<xsl:value-of select="field"/>
									</fo:block>																		
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="0.07cm" >																
									<fo:block font-size="8pt" text-align="left">
										Well No: 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" >			
									<fo:block font-size="8pt" text-align="right">
										<xsl:value-of select="wellReferenceNumber"/>
									</fo:block>																		
								</fo:table-cell>
							</fo:table-row>	
							
							<fo:table-row>
								<fo:table-cell padding="0.07cm" >																
									<fo:block font-size="8pt" text-align="left">
										Well Type: 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" >			
									<fo:block text-align="right">
										<xsl:value-of select="typeWell"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
								
							<fo:table-row>
								<fo:table-cell padding="0.07cm" >																
									<fo:block font-size="8pt" text-align="left">
										Max Deviation: 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" >			
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/Wellbore/Wellbore/maxDeviation"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/Wellbore/Wellbore/maxDeviation/@uomSymbol"/>@
										<xsl:value-of select="/root/modules/Wellbore/Wellbore/maxDeviationDepth"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/Wellbore/Wellbore/maxDeviationDepth/@uomSymbol"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>	
								
							<fo:table-row>
								<fo:table-cell padding="0.07cm">																
									<fo:block font-size="8pt" text-align="left">Tubing Size: </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm">			
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize/@uomSymbol"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="0.07cm">																
									<fo:block font-size="8pt" text-align="left">Min ID: </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm">			
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/nextTubularSize"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/nextTubularSize/@uomSymbol"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="0.07cm">																
									<fo:block font-size="8pt" text-align="left">Weight: </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm">			
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/nextTubularWeight"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/nextTubularWeight/@uomSymbol"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell padding="0.07cm" border-right="0.5px solid black">
					<fo:block font-size="8pt" text-align="left">
					
					<fo:table>
						<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
						
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.07cm" font-size="8pt">
									<fo:block text-align="left">
										SITHP:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" font-size="8pt">
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/shutInTubingHeadPressure"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/shutInTubingHeadPressure/@uomSymbol"/>
									</fo:block>																		
								</fo:table-cell>
							</fo:table-row>
								
							<fo:table-row>
								<fo:table-cell padding="0.07cm" font-size="8pt">																
									<fo:block text-align="left">
										PCP: 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" font-size="8pt">			
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/macp"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/macp/@uomSymbol"/>
									</fo:block>																		
								</fo:table-cell>
							</fo:table-row>
								
							<fo:table-row>
								<fo:table-cell padding="0.07cm" font-size="8pt">																
									<fo:block text-align="left">
										SCP: 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" font-size="8pt">			
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/casingPressure"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/casingPressure/@uomSymbol"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="0.07cm" padding-bottom="0.00cm">																
									<fo:block text-align="left"> WH Tree Top Conn: </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" padding-bottom="0.00cm">			
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/bopConnectionDetail"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell padding="0.07cm" border-right="0.5px solid black">
					<fo:block font-size="8pt" text-align="left">
					
					<fo:table>
						<fo:table-column column-number="1" column-width="proportional-column-width(60)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(40)"/>
						
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell font-size="8pt" padding="0.07cm">
									<fo:block text-align="left"> 
										Work Type: 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" font-size="8pt">
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/Well/Well/operationalCode"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
								
							<fo:table-row>
								<fo:table-cell padding="0.07cm" font-size="8pt">																
									<fo:block text-align="left"> 
										Intervention Type: 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm">			
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/Operation/Operation/operationalUnit/@lookupLabel"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="0.07cm">																
									<fo:block text-align="left"> 
										AFI Days: 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm">			
									<fo:block text-align="right">
										<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/projecteddays,'d','')"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="0.07cm">																
									<fo:block text-align="left"> 
										Actual Days: 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm">			
									<fo:block text-align="right">
										<xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysOnWell,'d','')"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
								
							<fo:table-row>
								<fo:table-cell padding="0.07cm">																
									<fo:block text-align="left"> 
										AFI Ref No.: 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm">			
									<fo:block text-align="right"> 
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/dynaAttr/afeNumber"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell padding="0.07cm">
					<fo:block font-size="8pt" text-align="left">
					
					<fo:table>
						<fo:table-column column-number="1" column-width="proportional-column-width(70)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
						
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell font-size="8pt" padding="0.07cm">
									<fo:block text-align="left"> 
										AFI Cost(USD):
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" font-size="8pt">
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/Operation/Operation/afe/@uomSymbol"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/Operation/Operation/afe"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="0.07cm" font-size="8pt">																
									<fo:block text-align="left"> 
										Cum Actual Cost to date:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm">			
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/cumtotalcost/@uomSymbol"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/cumtotalcost"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="0.07cm">																
									<fo:block text-align="left"> 
										Current Production (mmscfd): 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm">			
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/productionGasVolumeFlowRate"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/productionGasVolumeFlowRate/@uomSymbol"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="0.07cm">																
									<fo:block text-align="left"> 
										Current Production (bopd): 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm">			
									<fo:block text-align="right">
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/productionOilVolumeFlowRate"/><fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/productionOilVolumeFlowRate/@uomSymbol"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="0.07cm">																
									<fo:block text-align="left" > 
										Wells Supervisor: 
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm">			
									<fo:block text-align="right"> 
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/supervisor"/>
									</fo:block>																	
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:table-cell>
			
		</fo:table-row>
		
		<fo:table-row>
			<fo:table-cell border-right="0.5px solid black">
				<fo:block>
				
				</fo:block>	
			</fo:table-cell>
			
			<fo:table-cell padding="0.07cm" number-columns-spanned="3" border-top="0.5px solid black">
				<fo:block>
					<fo:table>
						<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
						<fo:table-column column-number="3" column-width="proportional-column-width(30)"/>
						<fo:table-column column-number="4" column-width="proportional-column-width(30)"/>
						
						<fo:table-body>
							<fo:table-row>
							
								<fo:table-cell padding="0.07cm" >																
									<fo:block text-align="left"> Next 24 hours forward plan: </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.07cm" number-columns-spanned="3">			
									<fo:block text-align="left">
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPlanSummary"/>
									</fo:block>
								</fo:table-cell>
						
							</fo:table-row>
						</fo:table-body>
					
					</fo:table>
				
				
				</fo:block>	
			</fo:table-cell>
			
		</fo:table-row>
				
		</fo:table-body>
		</fo:table>
		
		<fo:table table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-before="5pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(85)"/>
			
			<fo:table-header>
				<fo:table-row font-size="8pt">
					<fo:table-cell text-align="center" number-columns-spanned="2" padding="0.07cm" font-size="8pt" font-weight="bold" border-bottom="0.5px solid black">
						<fo:block>
							Summary of 24 hours activities
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			
			<fo:table-body>
				<fo:table-row font-size="8pt" >
					<fo:table-cell padding="0.07cm" text-align="left" number-columns-spanned="2" border-bottom="0.5px solid black">
						<xsl:choose>
							<xsl:when test="count(/root/modules/ReportDaily/ReportDaily/reportPeriodSummary) &gt; 0">
								<fo:block><xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportPeriodSummary"/></fo:block>
							</xsl:when>
							<xsl:otherwise>
								<fo:block><fo:inline color="white">.</fo:inline></fo:block>
							</xsl:otherwise>
						</xsl:choose>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row font-size="8pt" >
					<fo:table-cell padding="0.07cm" font-weight="bold" text-align="left" border-bottom="0.5px solid black" border-right="0.5px solid black">
						<fo:block>Lesson Learned</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.07cm" text-align="left" border-bottom="0.5px solid black">
						<fo:block><xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportCurrentStatus"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row font-size="8pt" >
					<fo:table-cell padding="0.07cm" font-weight="bold" text-align="left" border-right="0.5px solid black">
						<fo:block>UER</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.07cm" text-align="left">
						<fo:block><xsl:value-of select="/root/modules/ReportDaily/ReportDaily/generalComment"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>	
		
	</xsl:template>
	
	
</xsl:stylesheet>	