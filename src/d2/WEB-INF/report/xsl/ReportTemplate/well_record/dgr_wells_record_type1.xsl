<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<!-- wells section BEGIN -->
	<xsl:template match="modules/Well/Well">
		
		
		<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" border="0.75px solid black" margin-top="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(14)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(17)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(17)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(17)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(15)"/>
			
			<fo:table-body font-size="8pt">
				<fo:table-row>
					<fo:table-cell text-align="left" number-columns-spanned="6" border-bottom="0.75px solid black" >
						<fo:block color="white" text-align="center" font-weight="bold" font-size="10pt" background-color="rgb(37,87,147)">
							<fo:inline>Well Details</fo:inline> 
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell font-weight="bold" text-align="left" padding="2px">
						<fo:block>
							<fo:inline>Depth MDBRT </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" border-right="0.75px solid black" padding="2px">
						<fo:block>
						<xsl:choose>													
								<xsl:when test = "string(/root/modules/ReportDaily/ReportDaily/depthMdMsl)!=''"> 
									: 
									<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl/@uomSymbol"/>									
								</xsl:when>													
								<xsl:otherwise>
									:
								</xsl:otherwise>
						</xsl:choose> 
						</fo:block>
					</fo:table-cell>
					 
					<fo:table-cell font-weight="bold" text-align="left" padding="2px">
						<fo:block>
							<fo:inline>Report Period </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" border-right="0.75px solid black" padding="2px">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/dailyReportStartDatetime)!=''"> 
										: 
										<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDailyDGR/ReportDaily/dailyReportStartDatetime/@epochMS,'HH:mm')"/>	
										-										
										<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDailyDGR/ReportDaily/dailyReportEndDatetime/@epochMS,'HH:mm')"/>									
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
						
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding="2px">
						<fo:block>
							<fo:inline>Date</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding="2px">
						<fo:block>
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/ReportDaily/ReportDaily/reportDatetime)!=''"> 
									: 
									<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportDatetime"/>									
								</xsl:when>													
								<xsl:otherwise>
									:
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>	
						
				</fo:table-row>
				
				

				<fo:table-row>
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Depth TVDBRT </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding="1px" border-right="0.75px solid black" padding-left="2px">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDaily/ReportDaily/depthTvdMsl)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl"/>	
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@uomSymbol"/> 								
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Last Csg Size </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding="1px" border-right="0.75px solid black" padding-left="2px">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDaily/ReportDaily/lastCsgsize)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize"/>	
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgsize/@uomSymbol"/> 								
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding="1px" padding-left="2px">
						<fo:block>
							<fo:inline>Progress</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px">
						<fo:block>
							: <fo:inline> 
							<xsl:value-of select="format-number(sum(/root/modules/Bharun/Bharun/BharunDailySummary/progress/@rawNumber), '#,###.0')"/>
							<fo:inline color="white">.</fo:inline>
							<xsl:value-of select="/root/modules/Bharun/Bharun/BharunDailySummary/progress/@uomSymbol"/> 
							</fo:inline>
						</fo:block>
					</fo:table-cell>		
				</fo:table-row>
				
				
				<fo:table-row>
					<fo:table-cell font-weight="bold" text-align="left" padding="1px" padding-left="2px">
						<fo:block>
							<fo:inline>Depth TVDSS </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" border-right="0.75px solid black" padding-left="2px">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/subseaTvd)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/subseaTvd"/>	
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/subseaTvd/@uomSymbol"/> 								
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Last Csg Shoe MD </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" border-right="0.75px solid black" padding-left="2px">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl"/>	
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl/@uomSymbol"/> 								
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Report Start Depth</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/reportStartDepthMdMsl)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportStartDepthMdMsl"/>	
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportStartDepthMdMsl/@uomSymbol"/> 								
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>		
				</fo:table-row>
				
				
				<fo:table-row>
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline><xsl:value-of select="/root/datum/currentDatumType"/> - <xsl:value-of select="/root/datum/currentDatumReferencePoint"/> </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black" >
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/Operation/Operation/dynaAttr/rtmsl)!=''"> 
										: 
										<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl"/>	
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl/@uomSymbol"/> 								
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Last Csg Shoe TVD </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/lastCsgshoeTvdMsl)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastCsgshoeTvdMsl"/>	
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastCsgshoeTvdMsl/@uomSymbol"/> 								
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Report End Depth</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/reportEndDepthMdMsl)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportEndDepthMdMsl"/>	
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportEndDepthMdMsl/@uomSymbol"/> 								
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>		
				</fo:table-row>
				
				
				<fo:table-row>
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/Well/Well/onOffShore)='OFF'"> 
									<fo:inline> Water Depth </fo:inline>										
								</xsl:when>													
								<xsl:otherwise>
									<fo:inline> Ground Level </fo:inline>	
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black" >
						<fo:block> 
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/Well/Well/onOffShore)='OFF'"> 	
									: <xsl:value-of select="/root/modules/Well/Well/waterDepth"/><fo:inline color="white">.</fo:inline>
                					<fo:inline color="white">.</fo:inline>
                					<xsl:value-of select="/root/modules/Well/Well/waterDepth/@uomSymbol"/> 						
								</xsl:when>													
								<xsl:otherwise>
									: <xsl:value-of select="/root/modules/Well/Well/glHeightMsl"/><fo:inline color="white">.</fo:inline>
                					<fo:inline color="white">.</fo:inline>
                					<xsl:value-of select="/root/modules/Well/Well/glHeightMsl/@uomSymbol"/> 	
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Liner MD </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl"/>		
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastLinerMdMsl/@uomSymbol"/>						
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Days since Spud</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysFromSpud)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/dynaAttr/daysFromSpud"/>								
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>		
				</fo:table-row>
				
				
				<fo:table-row>
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block> 
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/Well/Well/onOffShore)='OFF'"> 
								<fo:inline><xsl:value-of select="/root/datum/currentDatumType"/> - Seabed</fo:inline>									
								</xsl:when>													
								<xsl:otherwise>
									<fo:inline><xsl:value-of select="/root/datum/currentDatumType"/> - Hanger</fo:inline>
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black">
						<fo:block> 
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/Well/Well/onOffShore)='OFF'"> 
									: <xsl:value-of select="/root/modules/Operation/Operation/rkbToMlGl"/><fo:inline color="white">.</fo:inline>
                					<fo:inline color="white">.</fo:inline>
                					<xsl:value-of select="/root/modules/Operation/Operation/rkbToMlGl/@uomSymbol"/> 								
								</xsl:when>													
								<xsl:otherwise>
									: <xsl:value-of select="/root/modules/Operation/Operation/zRtthgr"/><fo:inline color="white">.</fo:inline>
                					<fo:inline color="white">.</fo:inline>
                					<xsl:value-of select="/root/modules/Operation/Operation/zRtthgr/@uomSymbol"/> 
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Liner TVD </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl"/>								
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastLinerTvdMsl/@uomSymbol"/>
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>	
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Rig</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px">
						<fo:block>
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel)!=''">
									: <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>										
								</xsl:when>													
								<xsl:otherwise>
									: 
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
					</fo:table-cell>
						
				</fo:table-row>
				
				
				<fo:table-row>
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Hole Size </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black">
						<fo:block>	
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDaily/ReportDaily/lastHolesize)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastHolesize"/>	
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastHolesize/@uomSymbol"/> 								
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>FIT / LOT </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black">
						<fo:block>	
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDaily/ReportDaily/lastFit)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit"/><xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit/@uomSymbol"/>
										/
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot"/><xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot/@uomSymbol"/>	
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Mud Weight</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/lastMw)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastMw"/>	
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastMw/@uomSymbol"/> 							
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>		
				</fo:table-row>
				
				
				<fo:table-row>
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px" >
						<fo:block> 
							
							<fo:inline> Last Survey (MD<xsl:value-of select="/root/datum/currentDatumType"/>/</fo:inline>									
									
						</fo:block>
						<fo:block>
							TVD<xsl:value-of select="/root/datum/currentDatumType"/>)
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl"/>	
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastMdMsl/@uomSymbol"/>
										/
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastTvdMsl"/>
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastTvdMsl/@uomSymbol"/>						
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>	
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Hole Size Carbide </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black">
						<fo:block>	
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/lastHolesizeCarbide)!=''"> 
									: <xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastHolesizeCarbide"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/lastHolesizeCarbide/@uomSymbol"/> 									
								</xsl:when>													
								<xsl:otherwise>
									:
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Mud Type</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px">
						<fo:block>
							<xsl:choose>													
									<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/mudType)!=''"> 
										: 
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/mudType/@lookupLabel"/>							
									</xsl:when>													
									<xsl:otherwise>
										:
									</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>	
					
					
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell font-weight="bold" text-align="left" padding-left="2px">
						<fo:block>
							<fo:inline>Survey Deviation </fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black">
						<fo:block>
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/inclinationAngle/@rawNumber)!=''"> 
									: Inc. 
									<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/inclinationAngle/@rawNumber"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/inclinationAngle/@uomSymbol"/>									
								</xsl:when>													
								<xsl:otherwise>
									:
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
						
					</fo:table-cell>	
					<fo:table-cell number-columns-spanned="2" text-align="left" padding-left="2px" border-right="0.75px solid black">
						<fo:block>
										
						</fo:block>
					</fo:table-cell>		
					<fo:table-cell number-columns-spanned="2" text-align="left" padding-left="2px">
						<fo:block>
										
						</fo:block>
					</fo:table-cell>	
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell text-align="left" padding-left="2px">
						<fo:block>
							
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="left" padding-left="2px" border-right="0.75px solid black">
						<fo:block>	
							<xsl:choose>													
								<xsl:when test = "string(/root/modules/ReportDailyDGR/ReportDaily/azimuthAngle/@rawNumber)!=''"> 
									: Az
									<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/azimuthAngle/@rawNumber"/>
									<fo:inline color="white">.</fo:inline>
									<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/azimuthAngle/@uomSymbol"/>									
								</xsl:when>													
								<xsl:otherwise>
									:
								</xsl:otherwise>
							</xsl:choose>							
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell number-columns-spanned="2" text-align="left" padding="1px" border-right="0.75px solid black">
						<fo:block>
										
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell number-columns-spanned="2" text-align="left" padding="1px">
						<fo:block>
										
						</fo:block>
					</fo:table-cell>			
				</fo:table-row> 
			</fo:table-body>
		</fo:table>
							
	</xsl:template>
	
</xsl:stylesheet>	