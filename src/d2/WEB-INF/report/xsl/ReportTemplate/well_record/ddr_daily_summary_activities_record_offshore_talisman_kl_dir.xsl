<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- WELL SECTION - 20080921 -->
	
	<xsl:template match="/root/modules/ReportDailyForFWR">

<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-before="2pt" table-omit-header-at-break="false">
			<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(80)"/>
			<fo:table-header>
				<fo:table-row font-size="8pt">
					<fo:table-cell text-align="center" padding="0.07cm" padding-bottom="0.00cm" font-size="8pt" font-weight="bold" border-bottom="0.5px solid black" number-columns-spanned="3">
						<fo:block>
							Daily Summary Activities Record
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block text-align="left" font-weight="bold"> Date </fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.07cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block text-align="center" font-weight="bold">
							Day #
						</fo:block>																		
					</fo:table-cell>
					<fo:table-cell padding="0.07cm" border-bottom="0.5px solid black">
						<fo:block text-align="left" font-weight="bold"> Descriptions </fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>	
				<xsl:apply-templates select="/root/modules/ReportDailyForFWR/ReportDaily[reportType = 'DIR']" mode="nextday"/>
			</fo:table-body>
			<fo:table-body>
				<fo:table-row font-size="8pt" >
					<fo:table-cell >
						<fo:block>
							<fo:table width="100%" table-layout="fixed" table-omit-header-at-break="true" wrap-option="wrap">
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								<fo:table-body><fo:table-row><fo:table-cell><fo:block></fo:block></fo:table-cell></fo:table-row></fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
			</fo:table>
			
			</xsl:template>
			
				
					
			<xsl:template match="ReportDaily" mode="nextday">
			
			<xsl:choose>
					<xsl:when test="/root/modules/Daily/Daily/dayDate/@epochMS &gt;=  reportDatetime/@epochMS">
				<fo:table-row>
					<fo:table-cell padding="0.07cm" border-right="0.5px solid black">
						<fo:block text-align="left">
							<xsl:value-of select="reportDatetime"/>
						 </fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.07cm" border-right="0.5px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="reportNumber"/>
						</fo:block>																		
					</fo:table-cell>
					<fo:table-cell padding="0.07cm" >
						<fo:block text-align="left">
						 	<xsl:value-of select="reportPeriodSummary"/>
						 </fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
			</xsl:choose>
				
			</xsl:template>
	
</xsl:stylesheet>	