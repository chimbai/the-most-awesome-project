<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
		<!--WELL STATUS -->
	<xsl:template match="modules/Well/Well" mode="wellstatus">

		<xsl:variable name="reportDaily" select="/root/modules/ReportDaily/ReportDaily"/>
		<xsl:variable name="dayDate" select="$reportDaily/dynaAttr/daydate/@epochMS" />
		
		<xsl:variable name="dryHoleEndDateTimeEpochMS" select="number(/root/modules/Operation/Operation/dryHoleEndDateTime/@epochMS)"/>
		<xsl:variable name="spudDateEpochMS" select="number(/root/modules/Operation/Operation/spudDate/@epochMS)"/>
		<xsl:variable name="dryholedays" select="($dryHoleEndDateTimeEpochMS - $spudDateEpochMS) div 86400000"/>
		<xsl:variable name="totalNptDuration" select="number(/root/modules/TotalNptDurationInWell/TotalNptHour/nptDuration)"/>
		<xsl:variable name="totalActivityDuration" select="number(/root/modules/TotalNptDurationInWell/TotalNptHour/totalActivityDuration)"/>
		<xsl:variable name="nptpercent" select="string((($totalNptDuration) div ($totalActivityDuration))*100)"/>
		<xsl:variable name="budgetCostToDate" select="sum(/root/modules/DvdPlan/OperationPlanMaster/OperationPlanPhase/phaseBudget/@rawNumber)"/>
		<xsl:variable name="dayCostToDate" select="/root/modules/ReportDaily/ReportDaily/dynaAttr/cumcost/@rawNumber"/>
		<xsl:variable name="budgetCostPerMetre" select="number(/root/modules/DvdPlan/OperationPlanMaster/OperationPlanPhase/depthMdMsl/@rawNumber[not(. &lt; /root/modules/DvdPlan/OperationPlanMaster/OperationPlanPhase/depthMdMsl/@rawNumber)][1])"/>
		<xsl:variable name="dayCostPerMetre" select="number(/root/modules/ReportDaily/ReportDaily/depthMdMsl/@rawNumber)"/>
		<xsl:variable name="totalbudget" select="(($budgetCostToDate) div ($budgetCostPerMetre))"/>
		<xsl:variable name="totaldayCost" select="(($dayCostToDate) div ($dayCostPerMetre))"/>
		<xsl:variable name="currentDepth" select="(/root/modules/Prev7daysReportDaily/Prev7daysReportDaily/currentDepth)"/>
		<xsl:variable name="prevDepth" select="(/root/modules/Prev7daysReportDaily/Prev7daysReportDaily/prevDepth)"/>
		<xsl:variable name="depthProgress" select="$currentDepth - $prevDepth"/>
		<xsl:variable name="costUomSymbol" select="/root/modules/ReportDaily/ReportDaily/daycost/@uomSymbol"/>
		<xsl:variable name="depthUomSymbol" select="/root/modules/ReportDaily/ReportDaily/depthMdMsl/@uomSymbol"/>
		<xsl:variable name="afe" select="/root/modules/Operation/Operation/afe" />
 

		<fo:table width="100%" table-layout="fixed" font-size="7pt" margin-top="2pt" border="0.75px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(25)"/> 
			<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell font-size="7pt" padding="0.05cm" number-columns-spanned="3" font-weight="bold" border-bottom="0.5px solid black" text-align="center"  background-color="#FFFACD"> <!-- background-color="#FFFACD" -->
						<fo:block> 
							WELL STATUS
						</fo:block>
					</fo:table-cell> 
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="0.1cm" padding-top="0.05cm" font-weight="bold" background-color="#F5F5F5" border-bottom="0.5px solid black" border-right="0.5px solid black" text-align="left">
						<fo:block>
							Data
						</fo:block>
					</fo:table-cell>
					<fo:table-cell  padding-right="0.1cm" padding-top="0.05cm" font-weight="bold" background-color="#F5F5F5" border-bottom="0.5px solid black" border-right="0.5px solid black" text-align="right">
						<fo:block>
							Planned (AFE)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-right="0.1cm" padding-top="0.05cm" font-weight="bold" background-color="#F5F5F5" border-bottom="0.5px solid black" text-align="right">
						<fo:block>
							Actual (To Date)
						</fo:block>
					</fo:table-cell> 
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
							 Arrival Date &amp; Time on Location
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell number-columns-spanned="2" padding-top="0.05cm" text-align="center" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/onlocDateOnlocTime/@epochMS,'dd-MMM-yyyy HH:mm')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row> 
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Spud Date &amp; Time
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-top="0.05cm" number-columns-spanned="2" text-align="center" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/spudDate/@epochMS,'dd-MMM-yyyy HH:mm')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Days on Location
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-top="0.05cm" number-columns-spanned="2" text-align="center" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">	
							<xsl:value-of select="(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysOnLocation)"/>										
						</fo:block>
					</fo:table-cell>
				</fo:table-row> 
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Dry Hole Days
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-top="0.05cm" number-columns-spanned="2" text-align="center" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
						<xsl:choose>
							<xsl:when test="number($dryHoleEndDateTimeEpochMS &gt; 0 and$spudDateEpochMS &gt; 0)">
								<xsl:value-of select="d2Utils:formatNumber(string($dryholedays),'###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
									<fo:inline>0.00</fo:inline>
								</xsl:otherwise>   
						</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Total Depth (<xsl:value-of select="$depthUomSymbol"/>) 
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell  padding-right="0.1cm" padding-top="0.05cm" text-align="right" border-bottom="0.5px solid black" border-right="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="d2Utils:formatNumber(string(($budgetCostPerMetre)),'###,##0.0')"/>
							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-right="0.1cm" padding-top="0.05cm" text-align="right" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Weekly Drilling Progress (<xsl:value-of select="$depthUomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-right="0.1cm" padding-top="0.05cm" text-align="right" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
							N/A
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-right="0.1cm" padding-top="0.05cm" text-align="right" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:choose>
								<xsl:when test="number($depthProgress &gt; 0)">
									<xsl:value-of select="d2Utils:formatNumber(string($depthProgress),'###,###,##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>0.00</fo:inline>
								</xsl:otherwise>   
							</xsl:choose>
						</fo:block>
					</fo:table-cell> 
				</fo:table-row>
				 
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"  border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<fo:inline>
							
								<xsl:choose>
									<xsl:when test="((/root/UserContext/UserSelection/uomTemplateUid)='PETROFAC_RO_Imperial_Template')">
										Cost per Foot
									</xsl:when>
									<xsl:otherwise>
										Cost per Metre
									</xsl:otherwise>   
								</xsl:choose>
							</fo:inline>
						</fo:block> 
					</fo:table-cell> 
					<fo:table-cell padding-right="0.1cm" padding-top="0.05cm" text-align="right" border-bottom="0.5px solid black" border-right="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:choose>
								<xsl:when test="number($totalbudget &gt; 0 and $dayCostPerMetre &gt; 0)">
									<xsl:value-of select="concat($costUomSymbol, d2Utils:formatNumber(string($totalbudget),'###,###,##0.00'))"/>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>0.00</fo:inline>
								</xsl:otherwise>   
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-right="0.1cm" padding-top="0.05cm" text-align="right" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:choose>
								<xsl:when test="string($totaldayCost &gt; 0)">
									<xsl:value-of select="concat($costUomSymbol, d2Utils:formatNumber(string($totaldayCost),'###,###,##0.00'))"/>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>0.00</fo:inline>
								</xsl:otherwise>   
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Estimated EOW Date
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-right="0.1cm" padding-top="0.05cm" number-columns-spanned="2" text-align="center" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/estimatedJobCompletionDateTime/@epochMS,'dd-MMM-yyyy')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Total Well NPT (hrs)
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-top="0.05cm" number-columns-spanned="2" text-align="center" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:choose>
								<xsl:when test="number($totalNptDuration &gt; 0)">
									<xsl:value-of select="d2Utils:formatNumber(string(($totalNptDuration)),'###,##0.0')"/> 
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>0.0</fo:inline>
								</xsl:otherwise>   
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Total Well NPT (%)
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-top="0.05cm" number-columns-spanned="2" text-align="center" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:choose>
								<xsl:when test="number($nptpercent &gt; 0)">
									<xsl:value-of select="d2Utils:formatNumber($nptpercent, '###,##0.0')"/> 
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>0.0</fo:inline>
								</xsl:otherwise>   
							</xsl:choose>
						</fo:block> 
					</fo:table-cell> 
				</fo:table-row> 
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Total Cost
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-right="0.1cm" padding-top="0.05cm" text-align="right" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:choose>
								<xsl:when test="number($budgetCostToDate &gt; 0)">
									<xsl:value-of select="concat($costUomSymbol, d2Utils:formatNumber(string($budgetCostToDate),'###,###,##0.00'))"/> 
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>0.00</fo:inline>
								</xsl:otherwise>   
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-right="0.1cm" padding-top="0.05cm" text-align="right" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:choose>
								<xsl:when test="number($dayCostToDate &gt; 0)">
									<xsl:value-of select="concat($costUomSymbol, d2Utils:formatNumber(string($dayCostToDate),'###,###,##0.00'))"/>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>0.00</fo:inline>
								</xsl:otherwise>   
							</xsl:choose>
                  		</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Estimated EOW Cost <fo:inline font-size="6pt">(based on forecast)</fo:inline>
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-top="0.05cm" number-columns-spanned="2" text-align="center" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:choose>
								<xsl:when test="not($afe = '0') and not($afe = '')">
									<xsl:value-of select="concat($costUomSymbol, d2Utils:formatNumber(string($afe),'###,###,##0.00'))"/>
								</xsl:when>
								<xsl:otherwise>
									<fo:inline>0.00</fo:inline>
								</xsl:otherwise>   
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"  border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Rig(s)
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-left="0.1cm" padding-top="0.05cm" number-columns-spanned="2" text-align="left" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="(/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel)"/> 
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"  border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						Location
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-left="0.1cm" padding-top="0.05cm" number-columns-spanned="2" text-align="left" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="(/root/modules/Well/Well/block)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>
						AFE Number
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-left="0.1cm" padding-top="0.05cm" number-columns-spanned="2" text-align="left" border-bottom="0.5px solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="(/root/modules/ReportDaily/ReportDaily/dynaAttr/afeNumber)"/> 
						</fo:block>
					</fo:table-cell> 
				</fo:table-row>
					
				<fo:table-row space-after="3pt">
					<fo:table-cell background-color="#F5F5F5" padding-left="0.1cm" padding-top="0.05cm"   border-right="0.5px solid black">
						<fo:block>
						Engineers
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="0.1cm" padding-top="0.05cm" text-align="left">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="(/root/modules/ReportDaily/ReportDaily/snrdrillsupervisor)"/>
						</fo:block>
					</fo:table-cell> 
					<fo:table-cell padding-left="0.1cm" padding-top="0.05cm" text-align="left">
						<fo:block linefeed-treatment="preserve">
						<xsl:value-of select="(/root/modules/ReportDaily/ReportDaily/drillsupervisor)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>