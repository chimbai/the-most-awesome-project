<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Well History START -->
	<xsl:template match="modules/ReportDaily" mode="well_history_header">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" 
			border="0.5px solid black" wrap-option="wrap" space-after="6pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(12)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(73)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell padding="0.1cm" text-align="center" border-top="0.25px solid black"
						border-bottom="0.25px solid black">
						<fo:block>#</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" text-align="center" border-top="0.25px solid black"
						border-bottom="0.25px solid black">
						<fo:block>Date</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" text-align="center" border-top="0.25px solid black"
						border-bottom="0.25px solid black">
						<fo:block>Depth</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" text-align="center" border-top="0.25px solid black"
						border-bottom="0.25px solid black">
						<fo:block>24 Hour Summary</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="ReportDaily" mode="well_history_records">
					<xsl:sort select="reportNumber" data-type="number"/>
				</xsl:apply-templates>				
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="ReportDaily" mode="well_history_records">
		<fo:table-row>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="reportNumber"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportDatetime/@epochMS,'dd MMM yyyy')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="depthMdMsl"/>
					<xsl:value-of select="depthMdMsl/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm">
				<fo:block>
					<xsl:value-of select="reportPeriodSummary"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Well History END -->
	
</xsl:stylesheet>