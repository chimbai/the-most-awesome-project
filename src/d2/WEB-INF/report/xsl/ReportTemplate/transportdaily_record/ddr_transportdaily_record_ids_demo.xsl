<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- transportDaily START -->
	<xsl:template match="modules/TransportDailyParam">
		<fo:table width="100%" table-layout="fixed" margin-top="2pt"
			border="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(40)"/>
			<fo:table-header>	
				<fo:table-row>
					<fo:table-cell number-columns-spanned="5" padding="0.1cm" padding-left="0.15cm" padding-right="0.15cm"
						border-bottom="0.25px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Transport
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>	
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm" padding-left="0.15cm" padding-right="0.15cm">
						<fo:block text-align="center">Transport Type</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm" padding-left="0.15cm" padding-right="0.15cm">
						<fo:block text-align="center">Transport Name</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm" padding-left="0.15cm" padding-right="0.15cm">
						<fo:block text-align="center">Arrived Time</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm" padding-left="0.15cm" padding-right="0.15cm">
						<fo:block text-align="center">Departed Time</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm" padding-left="0.15cm" padding-right="0.15cm">
						<fo:block text-align="center">Comment</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:apply-templates select="TransportDailyParam"/>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="TransportDailyParam">
		<fo:table-row font-size="8pt">
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm" padding-left="0.15cm" padding-right="0.15cm">
				<fo:block text-align="left">
					<xsl:choose>													
						<xsl:when test = "string(transportType/@lookupLabel)=''">
							<xsl:value-of select="transportType"/>										
						</xsl:when>													
						<xsl:otherwise>
							<xsl:value-of select="transportType/@lookupLabel"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm" padding-left="0.15cm" padding-right="0.15cm">
				<fo:block text-align="left">
					<xsl:value-of select="transportName"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm" padding-left="0.15cm" padding-right="0.15cm">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(arrivalTime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm" padding-left="0.15cm" padding-right="0.15cm">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(departureTime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" padding-left="0.15cm" padding-right="0.15cm">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:value-of select="comments"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- transportDaily END -->
	
</xsl:stylesheet>