<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- TEMPLATE: modules/TransportDailyParam -->
	<xsl:template match="modules/TransportDailyParam">
	
		<fo:block keep-together="always">
		
			<!-- TABLE -->
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap"  border-bottom="thin solid black" space-after="6pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(40)"/>
				
				<!-- HEADER -->
				<fo:table-header>
					
					<!-- HEADER 1 -->
					<fo:table-row>
						<fo:table-cell number-columns-spanned="5" padding="2px" background-color="#DDDDDD" border="thin solid black" display-align="center">
							<fo:block font-size="8pt" font-weight="bold">
								Transport
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<!-- HEADER 2 -->
					<fo:table-row>
						<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Transport Type
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px"  border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Transport Name
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px"  border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Arrived Time
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px"  border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Departed Time
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Comment
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
				</fo:table-header>
				
				<!-- BODY -->
				<fo:table-body>
					<xsl:apply-templates select="TransportDailyParam"/>
				</fo:table-body>
				
			</fo:table>
			
		</fo:block>
		
	</xsl:template>
	
	<!-- TEMPLATE: TransportDailyParam -->
	<xsl:template match="TransportDailyParam">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
			
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="left">
							<xsl:choose>													
								<xsl:when test="string(transportType/@lookupLabel)=''">
									<xsl:value-of select="transportType"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="transportType/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>					
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="transportName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(arrivalTime/@epochMS,'HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(departureTime/@epochMS,'HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="comments"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
			</xsl:when>
			<xsl:otherwise>
			
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="left">
							<xsl:choose>													
								<xsl:when test="string(transportType/@lookupLabel)=''">
									<xsl:value-of select="transportType"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="transportType/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>					
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="transportName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(arrivalTime/@epochMS,'HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(departureTime/@epochMS,'HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="comments"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			
			</xsl:otherwise>
			
		</xsl:choose>
		
	</xsl:template>
	
</xsl:stylesheet>