<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- weather section BEGIN -->
	<xsl:template match="/root/modules/WeatherEnvironment/WeatherEnvironment">		                                     
		<fo:table inline-progression-dimension="100%" table-layout="fixed"
			wrap-option="wrap" table-omit-header-at-break="true" margin-top="2pt" border-left="0.5px solid black"
			border-right="0.5px solid black" border-bottom="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>													
			<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
			<fo:table-header>	
				<fo:table-row>
					<fo:table-cell number-columns-spanned="2" padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
						background-color="rgb(224,224,224)" border-top="0.5px solid black"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Weather
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black"
						border-bottom-color="rgb(44,37,96)">
						<fo:table inline-progression-dimension="100%" table-layout="fixed"
							table-omit-header-at-break="true" wrap-option="wrap">
							<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>								
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Report Time: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportDatetime/@epochMS,'HH:mm')"/>											
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Visibility: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="visibilityDistance"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="visibilityDistance/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>																		
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Wind Speed: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="windSpeed"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="windSpeed/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>																			
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Wind Dir.: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="windDirection"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="windDirection/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>																		
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Pressure.: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="barometerMax"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="barometerMax/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Air Temp.: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="airTemperatureAvg"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="airTemperatureAvg/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>																			
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
					<!-- 2nd column -->
					<fo:table-cell border-bottom="0.25px solid black">
						<fo:table inline-progression-dimension="100%" table-layout="fixed"
							table-omit-header-at-break="true" wrap-option="wrap">
							<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>																																
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Wave Dir.: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="waveDirection"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="waveDirection/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Wave Height: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="waveHeight"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="waveHeight/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Wave Frequency: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="waveFrequency"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="waveFrequency/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Swell Dir.: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="swellDirection"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="swellDirection/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Swell Height: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="swellHeight"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="swellHeight/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Swell Frequency: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="right">
											<xsl:value-of select="swellFrequency"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="swellFrequency/@uomSymbol"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>								
							</fo:table-body>
						</fo:table>
					</fo:table-cell>															
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="2">
						<fo:table inline-progression-dimension="100%" table-layout="fixed"
							table-omit-header-at-break="true" wrap-option="wrap">
							<fo:table-column column-number="1" column-width="proportional-column-width(18)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(82)"/>																																
							<fo:table-body>		
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left"> Weather Comment:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left" linefeed-treatment="preserve">
											<xsl:value-of select="weatherComment"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>																		
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>											
	</xsl:template>
	<!-- weather section END -->
	
</xsl:stylesheet>