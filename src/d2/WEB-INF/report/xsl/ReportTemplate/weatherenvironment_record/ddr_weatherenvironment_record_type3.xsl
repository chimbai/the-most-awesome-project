<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- WEATHER SECTION FOR ONSHORE WELL -->
	
	<!-- weather section BEGIN -->
	<xsl:template match="/root/modules/WeatherEnvironment/WeatherEnvironment">
	
		<fo:block keep-together="always">
	
			<!-- TABLE -->
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>													
				<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
				
				<!-- HEADER -->
				<fo:table-header>	
					<fo:table-row>
						<fo:table-cell number-columns-spanned="2" padding="2px" background-color="#DDDDDD" border="thin solid black" display-align="center">
							<fo:block font-size="10pt" font-weight="bold">
								Weather
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				
				<!-- BODY -->
				<fo:table-body>
					<fo:table-row>
					
						<!-- COLUMN 1 -->
						<fo:table-cell border-left="thin solid black" border-right="thin solid black">
							<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
								<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
															
								<fo:table-body>
								
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Report Time:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:choose>
													<xsl:when test="reportDatetime/@epochMS = '86400000'">
														<fo:inline>24:00</fo:inline>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportDatetime/@epochMS,'HH:mm')"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Visibility:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="visibilityDistance"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="visibilityDistance/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Air Temp.:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="airTemperatureAvg"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="airTemperatureAvg/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>																			
									</fo:table-row>
									
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
						
						<!-- COLUMN 2 -->
						<fo:table-cell border-right="thin solid black">
						
							<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
								<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Wind Speed:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="windSpeed"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="windSpeed/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Wind Dir.:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="windDirection"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="windDirection/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Pressure:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:value-of select="barometerMax"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="barometerMax/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>
								
							</fo:table>
						</fo:table-cell>
																					
					</fo:table-row>
								
					<fo:table-row>
						<fo:table-cell number-columns-spanned="2" border="thin solid black">
							<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
								<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(85)"/>					
								<fo:table-body>		
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block font-weight="bold">
												Weather Comment:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block linefeed-treatment="preserve">
												<xsl:value-of select="weatherComment"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>																		
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>
					
				</fo:table-body>
				
			</fo:table>
			
		</fo:block>
		
	</xsl:template>
	
</xsl:stylesheet>