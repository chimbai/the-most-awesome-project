<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Marine Properties START -->
	<xsl:template match="modules/WeatherEnvironment">	
		<xsl:variable name="dailyid" select="/root/modules/Daily/Daily/dailyUid"/>		
			<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="6pt" border="0.5px solid black" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
				<fo:table-column column-number="1" column-width="proportional-column-width(6)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(7)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="9" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="10" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="11" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="12" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="13" column-width="proportional-column-width(8)"/>
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Report Time <xsl:value-of select="//muddaily/L_muddaily.daynum"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Date <xsl:value-of select="//muddaily/L_muddaily.checkdt"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Visibility <xsl:value-of select="//muddaily/L_muddaily.mud_type_descr"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Wind Speed <xsl:value-of select="//muddaily/L_muddaily.depth"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Wind Dir. <xsl:value-of select="//muddaily/L_muddaily.oiltype"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Pressure <xsl:value-of select="//muddaily/L_muddaily.temperature"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Air Temp. <xsl:value-of select="//muddaily/L_muddaily.mwsurface"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Wave Dir. <xsl:value-of select="//muddaily/L_muddaily.funnelvis"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Wave Height <xsl:value-of select="//muddaily/L_muddaily.pv"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Wave Frequency <xsl:value-of select="//muddaily/L_muddaily.yp"/>
							</fo:block>
						</fo:table-cell>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Swell Dir. <xsl:value-of select="//muddaily/L_muddaily.apifl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Swell Height <xsl:value-of select="//muddaily/L_muddaily.hthpcake"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Swell Frequency <xsl:value-of select="//muddaily/L_muddaily.hthpcake"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>					
					<xsl:apply-templates select="WeatherEnvironment"/>											
				</fo:table-body>
			</fo:table>		
	</xsl:template>
	
	<xsl:template match="WeatherEnvironment">	
		<fo:table-row>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportDatetime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:variable name="dailyid" select="dailyUid"/>
					<xsl:value-of select="/root/modules/Daily/Daily[dailyUid=$dailyid]/dayDate"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="visibilityDistance"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="windSpeed"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="windDirection"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="barometerMax"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="airTemperatureAvg"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="waveDirection"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="waveHeight"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="waveFrequency"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="swellDirection"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="swellHeight"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="swellFrequency"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Marine Properties END -->
	
</xsl:stylesheet>