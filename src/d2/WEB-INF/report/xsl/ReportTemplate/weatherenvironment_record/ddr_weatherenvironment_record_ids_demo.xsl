<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- D2_OMV_GLOBAL WEATHER -->
	
	<xsl:template match="/root/modules/WeatherEnvironment/WeatherEnvironment">		                                     
		<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" table-omit-header-at-break="true" margin-top="2pt" border="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(11)"/>													
			<fo:table-column column-number="2" column-width="proportional-column-width(11)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(11)"/>													
			<fo:table-column column-number="4" column-width="proportional-column-width(11)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(11)"/>													
			<fo:table-column column-number="6" column-width="proportional-column-width(11)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(11)"/>													
			<fo:table-column column-number="8" column-width="proportional-column-width(11)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(12)"/>													
						
			<fo:table-header>	
				<fo:table-row>
					<fo:table-cell number-columns-spanned="9" padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							<xsl:variable name="dailyid" select="/root/modules/ReportDaily/ReportDaily/reportDailyUid"/>
							<xsl:variable name="daily" select="/root/modules/Daily/Daily"/>
							Weather on <xsl:value-of select="d2Utils:formatDateFromEpochMS($daily[dailyUid=$dailyid]/dayDate/@epochMS,'dd MMM yyyy')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<fo:table-cell padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="center">
							Visibility
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="center">
							Wind Speed
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="center">
							Wind Dir.
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="center">
							Pressure
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="center">
							Air Temp.
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="center">
							Wave Height
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="center">
							Wave Dir.
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="center">
							Swell Height
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="center">
							Swell Dir.
						</fo:block>
					</fo:table-cell>		
				</fo:table-row>
			</fo:table-header>
			
			<fo:table-body>
				<!-- ROW 1 -->
				<fo:table-row>
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<fo:block text-align="center">
								<xsl:value-of select="visibilityDistance"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="visibilityDistance/@uomSymbol"/>
							</fo:block>		
						</fo:block>
					</fo:table-cell>	
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<fo:block text-align="center">
								<xsl:value-of select="windSpeed"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="windSpeed/@uomSymbol"/>
							</fo:block>		
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<fo:block text-align="center">
								<xsl:value-of select="windDirection"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="windDirection/@uomSymbol"/>
							</fo:block>		
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<fo:block text-align="center">
								<xsl:value-of select="barometerMax"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="barometerMax/@uomSymbol"/>
							</fo:block>		
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<fo:block text-align="center">
								<xsl:value-of select="airTemperatureAvg"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="airTemperatureAvg/@uomSymbol"/>
							</fo:block>		
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<fo:block text-align="center">
								<xsl:value-of select="waveHeight"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="waveHeight/@uomSymbol"/>
							</fo:block>		
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<fo:block text-align="center">
								<xsl:value-of select="waveDirection"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="waveDirection/@uomSymbol"/>
							</fo:block>		
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center">
							<fo:block text-align="center">
								<xsl:value-of select="swellHeight"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="swellHeight/@uomSymbol"/>
							</fo:block>		
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px">
						<fo:block text-align="center">
							<fo:block text-align="center">
								<xsl:value-of select="swellDirection"/><fo:inline color="white">.</fo:inline>
								<xsl:value-of select="swellDirection/@uomSymbol"/>
							</fo:block>		
						</fo:block>
					</fo:table-cell>	
				</fo:table-row>
						
				<!-- ROW 2 - COMMENTS -->
				<fo:table-row>
					<fo:table-cell number-columns-spanned="9" border-top="0.5px solid black">
						<fo:table inline-progression-dimension="100%" table-layout="fixed" table-omit-header-at-break="true" wrap-option="wrap">
							<fo:table-column column-number="1" column-width="proportional-column-width(18)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(82)"/>																																
							<fo:table-body>		
								<fo:table-row>
									<fo:table-cell padding="2px">
										<fo:block text-align="left"> Weather Comments:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="2px">
										<fo:block text-align="left" linefeed-treatment="preserve">
											<xsl:value-of select="weatherComment"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>																		
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>											
	</xsl:template>
		
</xsl:stylesheet>