<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- WEATHER SECTION FOR OFFSHORE WELL -->
	
	<!-- weather section BEGIN -->
	<xsl:template match="/root/modules/WeatherEnvironment">
	
		<fo:block keep-together="always">
		
			<!-- TABLE -->
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>													
				<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
				
				<!-- HEADER -->
				<fo:table-header>	
					<fo:table-row>
						<fo:table-cell number-columns-spanned="2" padding="2px" background-color="#DDDDDD" border="thin solid black" display-align="center">
							<fo:block font-size="8pt" font-weight="bold">
								Weather
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
					<xsl:apply-templates select="WeatherEnvironment" />
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>
	
	<xsl:template match="WeatherEnvironment">
	<xsl:choose>
	<xsl:when test="/root/modules/Well/Well/onOffShore = 'OFF'">
		<fo:table-row>
		
			<!-- COLUMN 1 -->
			<fo:table-cell border-left="thin solid black" border-right="thin solid black">
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
					<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
					<fo:table-body>
					
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Report Time:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="reportDatetime/@epochMS = '86400000'">
											<fo:inline>24:00</fo:inline>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportDatetime/@epochMS,'HH:mm')"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Visibility:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(visibilityDistance = '') or (visibilityDistance = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="visibilityDistance"/>
											<fo:inline color="white">.</fo:inline>
											<xsl:value-of select="visibilityDistance/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Wind Speed:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(windSpeed = '') or (windSpeed = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="windSpeed"/>
											<fo:inline color="white">.</fo:inline>
											<xsl:value-of select="windSpeed/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>																			
						</fo:table-row>
						
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Wind Dir.:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(windDirection = '') or (windDirection = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="windDirection"/>
											<fo:inline color="white">.</fo:inline>
											<xsl:value-of select="windDirection/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
																								
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Pressure.:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(barometerMax = '') or (barometerMax = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="barometerMax"/>
											<fo:inline color="white">.</fo:inline>
											<xsl:value-of select="barometerMax/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Air Temp.:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(airTemperatureAvg = '') or (airTemperatureAvg = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="airTemperatureAvg"/>
											<fo:inline color="white">.</fo:inline>
											<xsl:value-of select="airTemperatureAvg/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>																			
						</fo:table-row>
						
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
			
			<!-- COLUMN 2 -->
			<fo:table-cell border-right="thin solid black">
			
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
					<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
					
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Wave Dir.:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(waveDirection = '') or (waveDirection = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="waveDirection"/>
											<fo:inline color="white">.</fo:inline>
											<xsl:value-of select="waveDirection/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Wave Height:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(waveHeight = '') or (waveHeight = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="waveHeight"/>
											<fo:inline color="white">.</fo:inline>
											<xsl:value-of select="waveHeight/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Wave Frequency:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(waveFrequency = '') or (waveFrequency = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="waveFrequency"/>
											<fo:inline color="white">.</fo:inline>
											<xsl:value-of select="waveFrequency/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Swell Dir.:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(swellDirection = '') or (swellDirection = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="swellDirection"/>
											<fo:inline color="white">.</fo:inline>
											<xsl:value-of select="swellDirection/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Swell Height:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(swellHeight = '') or (swellHeight = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="swellHeight"/>
											<fo:inline color="white">.</fo:inline>
											<xsl:value-of select="swellHeight/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block>
									Swell Frequency:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(swellFrequency = '') or (swellFrequency = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="swellFrequency"/>
											<fo:inline color="white">.</fo:inline>
											<xsl:value-of select="swellFrequency/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
					</fo:table-body>
					
				</fo:table>
			</fo:table-cell>
																		
		</fo:table-row>
		
		<fo:table-row>
			<fo:table-cell number-columns-spanned="2" border="thin solid black">
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
					<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(85)"/>					
					<fo:table-body>		
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block font-weight="bold">
									Weather Comment:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
								<fo:block linefeed-treatment="preserve">
									<xsl:value-of select="weatherComment"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>																		
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
		</xsl:when>
	<xsl:otherwise>
		<fo:table-row>
			<fo:table-cell border-right="thin solid black" border-bottom="thin solid black"  border-left="thin solid black" 
				border-bottom-color="rgb(44,37,96)">
				<fo:table inline-progression-dimension="100%" table-layout="fixed"
					table-omit-header-at-break="true" wrap-option="wrap">
					<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>								
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="left"> Report Time: </fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="right">
									<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportDatetime/@epochMS,'HH:mm')"/>											
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="left"> Visibility: </fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(swellFrequency = '') or (swellFrequency = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="swellFrequency"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="swellFrequency/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="left"> Air Temp.: </fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(airTemperatureAvg = '') or (airTemperatureAvg = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="airTemperatureAvg"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="airTemperatureAvg/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>																			
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
			<!-- 2nd column -->
			<fo:table-cell border-bottom="thin solid black"  border-right="thin solid black" >
				<fo:table inline-progression-dimension="100%" table-layout="fixed"
					table-omit-header-at-break="true" wrap-option="wrap">
					<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>																																
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="left"> Wind Speed: </fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(windSpeed = '') or (windSpeed = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="windSpeed"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="windSpeed/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>																			
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="left"> Wind Dir.: </fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(windDirection = '') or (windDirection = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="windDirection"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="windDirection/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>																		
						<fo:table-row>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="left"> Pressure.: </fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0.05cm">
								<fo:block text-align="right">
									<xsl:choose>
										<xsl:when test="(barometerMax = '') or (barometerMax = 'null') ">
										</xsl:when>
                     							<xsl:otherwise>
											<xsl:value-of select="barometerMax"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="barometerMax/@uomSymbol"/>
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>								
					</fo:table-body>
				</fo:table>
			</fo:table-cell>															
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell number-columns-spanned="2">
				<fo:table inline-progression-dimension="100%" table-layout="fixed"
					table-omit-header-at-break="true" wrap-option="wrap">
					<fo:table-column column-number="1" column-width="proportional-column-width(18)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(82)"/>																																
					<fo:table-body>		
						<fo:table-row>
							<fo:table-cell padding="0.05cm"  border-bottom="thin solid black"  border-left="thin solid black"  >
								<fo:block text-align="left"> Weather Comment:
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0.05cm"  border-right="thin solid black" border-bottom="thin solid black">
								<fo:block text-align="left" linefeed-treatment="preserve">
									<xsl:value-of select="weatherComment"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>																		
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
		</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>