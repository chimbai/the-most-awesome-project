<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- casing section BEGIN -->
	<xsl:template match="modules/GasReadings">
	
		<fo:table inline-progression-dimension="19.5cm" table-layout="fixed"  wrap-option="wrap" border="0.85px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(12)"/>
	        <fo:table-column column-number="2" column-width="proportional-column-width(13)"/>
	        <fo:table-column column-number="3" column-width="proportional-column-width(7)"/>
	        <fo:table-column column-number="4" column-width="proportional-column-width(6)"/>
	        <fo:table-column column-number="5" column-width="proportional-column-width(6)"/>
	        <fo:table-column column-number="6" column-width="proportional-column-width(6)"/>
	        <fo:table-column column-number="7" column-width="proportional-column-width(6)"/>
	       	<fo:table-column column-number="8" column-width="proportional-column-width(6)"/>
	        <fo:table-column column-number="9" column-width="proportional-column-width(6)"/>
	        <fo:table-column column-number="10" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="11" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="12" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="13" column-width="proportional-column-width(14)"/>
			
			
			<fo:table-header>
				<fo:table-row color="white" background-color="rgb(37,87,147)">
					<fo:table-cell font-size="10pt" number-columns-spanned="13" padding="2px" border-bottom="0.75px solid black" >
						<fo:block text-align="center" font-weight="bold" font-size="10pt">
							Gas Summary
						</fo:block>
					</fo:table-cell>
				</fo:table-row>	
							
				<fo:table-row font-size="8pt" text-align="center" background-color="rgb(149, 179, 215)">
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>Gas Type</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>Depth</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>Total</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>C1</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>C2</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>C3</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>iC4</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>nC4</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>iC5</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>nC5</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>CO2</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black">
						<fo:block>H2S</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px">
						<fo:block>(GWR/LHR/OCQ)</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row font-size="8pt" text-align="center" background-color="rgb(149, 179, 215)">
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>(<xsl:value-of select="GasReadings/topMdMsl/@uomSymbol"/>)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>Gas <xsl:value-of select="GasReadings/totalgasvalue/@uomSymbol"/></fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>(<xsl:value-of select="GasReadings/c1MethaneFrom/@uomSymbol"/>)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>(<xsl:value-of select="GasReadings/c2EthaneFrom/@uomSymbol"/>)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>(<xsl:value-of select="GasReadings/c3PropaneFrom/@uomSymbol"/>)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>(<xsl:value-of select="GasReadings/ic4IsoButaneFrom/@uomSymbol"/>)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>(<xsl:value-of select="GasReadings/nc4NorButaneFrom/@uomSymbol"/>)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>(<xsl:value-of select="GasReadings/ic5SoPethaneFrom/@uomSymbol"/>)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>(<xsl:value-of select="GasReadings/nc5from/@uomSymbol"/>)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>(<xsl:value-of select="GasReadings/c02CarbonDioxideFrom/@uomSymbol"/>)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-weight="bold" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>(<xsl:value-of select="GasReadings/h2sHydrogenSulfideFrom/@uomSymbol"/>)</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-bottom="0.75px solid black">
						<fo:block>
							
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>				
				<xsl:apply-templates select="GasReadings"/>
			</fo:table-body>
		</fo:table>
												
	</xsl:template>

	<xsl:template match="GasReadings">
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
			
				<fo:table-row text-align="center">
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="gastype"/>
							</fo:block>
						</fo:table-cell>
						
						<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="topMdMsl"/>
								<fo:inline> - </fo:inline>
								<xsl:value-of select="bottomMdMsl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="totalgasvalue"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="c1MethaneFrom"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="c2EthaneFrom"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="c3PropaneFrom"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="ic4IsoButaneFrom"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="nc4NorButaneFrom"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="ic5SoPethaneFrom"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="nc5PentaneFrom"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="c02CarbonDioxideFrom"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
							<fo:block>
								<xsl:value-of select="h2sHydrogenSulfideFrom"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="0.5px solid black">
							<fo:block text-align="left">
										<xsl:choose>													
											<xsl:when test = "string(gastype)='Peak'">
												GWR - <xsl:value-of select="gwrFrom"/>										
											</xsl:when>													
											<xsl:otherwise>
												GWR - not required
											</xsl:otherwise>
										</xsl:choose>	
									</fo:block>
									<fo:block text-align="left">
										<xsl:choose>													
											<xsl:when test = "string(gastype)='Peak'">
												LHR - <xsl:value-of select="lhrFrom"/>										
											</xsl:when>													
											<xsl:otherwise>
												LHR - not required
											</xsl:otherwise>
										</xsl:choose>	
									</fo:block>
									<fo:block text-align="left">
										<xsl:choose>													
											<xsl:when test = "string(gastype)='Peak'">
												OCQ - <xsl:value-of select="ocqFrom"/>										
											</xsl:when>													
											<xsl:otherwise>
												OCQ - not required
											</xsl:otherwise> 
										</xsl:choose>	
									</fo:block>
						</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
			
       			<fo:table-row text-align="center" background-color="#EEEEEE">
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:value-of select="gastype"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:choose>													
								<xsl:when test = "string(gastype)='Peak'">		
									<xsl:value-of select="topMdMsl"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="topMdMsl"/>
									<fo:inline> - </fo:inline>
									<xsl:value-of select="bottomMdMsl"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:value-of select="totalgasvalue"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:value-of select="c1MethaneFrom"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:value-of select="c2EthaneFrom"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:value-of select="c3PropaneFrom"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:value-of select="ic4IsoButaneFrom"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:value-of select="nc4NorButaneFrom"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:value-of select="ic5SoPethaneFrom"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:value-of select="nc5PentaneFrom"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:value-of select="c02CarbonDioxideFrom"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.5px solid black">
						<fo:block>
							<xsl:value-of select="h2sHydrogenSulfideFrom"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-bottom="0.5px solid black">
						<fo:block text-align="left">
									<xsl:choose>													
										<xsl:when test = "string(gastype)='Peak'">
											GWR - <xsl:value-of select="gwrFrom"/>										
										</xsl:when>													
										<xsl:otherwise>
											GWR - not required
										</xsl:otherwise>
									</xsl:choose>	
								</fo:block>
								<fo:block text-align="left">
									<xsl:choose>													
										<xsl:when test = "string(gastype)='Peak'">
											LHR - <xsl:value-of select="lhrFrom"/>										
										</xsl:when>													
										<xsl:otherwise>
											LHR - not required
										</xsl:otherwise>
									</xsl:choose>	
								</fo:block>
								<fo:block text-align="left">
									<xsl:choose>													
										<xsl:when test = "string(gastype)='Peak'">
											OCQ - <xsl:value-of select="ocqFrom"/>										
										</xsl:when>													
										<xsl:otherwise>
											OCQ - not required
										</xsl:otherwise> 
									</xsl:choose>	
								</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- gasreadings section END -->

</xsl:stylesheet>