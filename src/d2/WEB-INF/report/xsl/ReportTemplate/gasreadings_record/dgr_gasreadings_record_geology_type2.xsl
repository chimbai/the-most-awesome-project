<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- casing section BEGIN -->
	<xsl:template match="modules/GasReadings">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" border="0.5px solid black" space-after="2pt" font-size="8pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(12)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(13)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="8" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="10" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="11" column-width="proportional-column-width(7)"/>
			<fo:table-column column-number="12" column-width="proportional-column-width(7)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding="0.075cm" number-columns-spanned="11" text-align="center" background-color="rgb(37,87,147)">
						<fo:block font-size="10pt" font-weight="bold" color="white">Gas Summary </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row font-weight="bold" background-color="rgb(149, 179, 215)">
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					Gas
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					Depth
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					Total
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					C1
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					C2
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					C3
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					iC4
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					nC4
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					C5
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
				<!--	CO2 -->
					 CO<fo:inline vertical-align="sub">2</fo:inline> 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
				<!--	H2S -->
					 H<fo:inline vertical-align="sub">2</fo:inline>S 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					(GWR/LHR/OCQ)
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row font-weight="bold" background-color="rgb(149, 179, 215)">
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
					Type <xsl:value-of select="GasReadings/topMdMsl/@uomSymbol" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
					Interval
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
					Gas <xsl:value-of select="GasReadings/totalgasvalue/@uomSymbol" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
				<xsl:choose>
					<xsl:when test="string-length(GasReadings/c1MethaneFrom) &gt; 0">
				   		<xsl:value-of select="GasReadings/c1MethaneFrom/@uomSymbol" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="GasReadings/c1MethaneFrom/@uomSymbol" />
					</xsl:otherwise>
				</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
				<xsl:choose>
					<xsl:when test="string-length(GasReadings/c2EthaneFrom) &gt; 0">
				   		<xsl:value-of select="GasReadings/c2EthaneFrom/@uomSymbol" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="GasReadings/c2EthaneFrom/@uomSymbol" />
					</xsl:otherwise>
				</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
				<xsl:choose>
					<xsl:when test="string-length(GasReadings/c3PropaneFrom) &gt; 0">
				   		<xsl:value-of select="GasReadings/c3PropaneFrom/@uomSymbol" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="GasReadings/c3PropaneFrom/@uomSymbol" />
					</xsl:otherwise>
				</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
				<xsl:choose>
					<xsl:when test="string-length(GasReadings/ic4IsoButaneFrom) &gt; 0">
				   		<xsl:value-of select="GasReadings/ic4IsoButaneFrom/@uomSymbol" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="GasReadings/ic4IsoButaneFrom/@uomSymbol" />
					</xsl:otherwise>
				</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
				<xsl:choose>
					<xsl:when test="string-length(GasReadings/nc4NorButaneFrom) &gt; 0">
				   		<xsl:value-of select="GasReadings/nc4NorButaneFrom/@uomSymbol" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="GasReadings/nc4NorButaneFrom/@uomSymbol" />
					</xsl:otherwise>
				</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
				<xsl:choose>
					<xsl:when test="string-length(GasReadings/ic5SoPethaneFrom)  &gt; 0">
				   		<xsl:value-of select="GasReadings/ic5SoPethaneFrom/@uomSymbol" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="GasReadings/ic5SoPethaneFrom/@uomSymbol" />
					</xsl:otherwise>
				</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
				<xsl:choose>
					<xsl:when test="string-length(GasReadings/c02CarbonDioxideFrom) &gt; 0">
				   		<xsl:value-of select="GasReadings/c02CarbonDioxideFrom/@uomSymbol" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="GasReadings/c02CarbonDioxideFrom/@uomSymbol" />
					</xsl:otherwise>
				</xsl:choose>
				</fo:block>
			</fo:table-cell>
			
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
				<xsl:choose>
					<xsl:when test="string-length(GasReadings/h2sHydrogenSulfideFrom) &gt; 0">
				   		<xsl:value-of select="GasReadings/h2sHydrogenSulfideFrom/@uomSymbol" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="GasReadings/h2sHydrogenSulfideFrom/@uomSymbol" />
					</xsl:otherwise>
				</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding-after="0.05cm">
				<fo:block text-align="center">
					<!-- empty -->
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="GasReadings"/>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="GasReadings">
	        <xsl:choose>
				<xsl:when test="position() mod 2 = '0'">
				   <fo:table-row background-color="rgb(224,224,224)">
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="gastype"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<xsl:variable name="gasType2" select="gastype"/>
				<xsl:choose>
					<xsl:when test="$gasType2='Peak'">
						<fo:block text-align="center">
							<xsl:value-of select="translate(topMdMsl,',','')"/>
						</fo:block>
					</xsl:when>
					<xsl:otherwise>
						<fo:block text-align="center">
							<xsl:value-of select="translate(topMdMsl,',','')"/> - <xsl:value-of select="translate(bottomMdMsl,',','')"/>
						</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<xsl:variable name="gasType2" select="gastype"/>
				<xsl:choose>
					<xsl:when test="$gasType2='Drilled'">
						<fo:block text-align="center">
							(Min)<xsl:value-of select="translate(totalfrom,',','')"/>
						</fo:block>
						<fo:block text-align="center">
							(Max)<xsl:value-of select="translate(totalto,',','')"/>
						</fo:block>
						<fo:block text-align="center">
							(Avg.)<xsl:value-of select="translate(avgGasConcentration,',','')"/>
						</fo:block>
					</xsl:when>
					<xsl:otherwise>
						<fo:block text-align="center">
							<xsl:value-of select="translate(totalgasvalue,',','')"/>
						</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(c1MethaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(c2EthaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(c3PropaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(ic4IsoButaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(nc4NorButaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(ic5SoPethaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(c02CarbonDioxideFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(h2sHydrogenSulfideFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<xsl:variable name="gasType" select="gastype"/>
					<xsl:choose>
						<xsl:when test="$gasType='Peak'">
							<fo:block text-align="center">
								GWR - <xsl:value-of select="translate(gwrFrom,',','')"/>
							</fo:block>
							<fo:block text-align="center">
								LHR - <xsl:value-of select="translate(lhrFrom,',','')"/>
							</fo:block>
							<fo:block text-align="center">
								OCQ - <xsl:value-of select="translate(ocqFrom,',','')"/>
							</fo:block>
						</xsl:when>
						<xsl:otherwise>
							<fo:block text-align="center">
								GWR - Not Required
							</fo:block>
							<fo:block text-align="center">
								LHR - Not Required
							</fo:block>
							<fo:block text-align="center">
								OCQ - Not Required
							</fo:block>
						</xsl:otherwise>
					</xsl:choose>
			</fo:table-cell>
		</fo:table-row>
		<xsl:choose>
		 <xsl:when test="string-length(descr) &gt; 3">
		    <fo:table-row background-color="rgb(224,224,224)">
              <fo:table-cell number-columns-spanned="11" padding="0.05cm" border-bottom="0.25px solid black">
                 <fo:list-block provisional-distance-between-starts="22mm">
                    <fo:list-item>
                       <fo:list-item-label end-indent="label-end()">
                          <fo:block font-weight="bold">Comment - </fo:block>
                       </fo:list-item-label>
                       <fo:list-item-body start-indent="body-start()" end-indent="1mm">
                          <fo:block text-align="left">
                             <xsl:value-of select="descr"/>
                          </fo:block>
                       </fo:list-item-body>
                    </fo:list-item>
                 </fo:list-block>
              </fo:table-cell>
           </fo:table-row>
		 </xsl:when>
		 <xsl:otherwise>
		 </xsl:otherwise>
	    </xsl:choose>
		</xsl:when>
				<xsl:otherwise>
					<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="gastype"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<xsl:variable name="gasType2" select="gastype"/>
				<xsl:choose>
					<xsl:when test="$gasType2='Peak'">
						<fo:block>
							<xsl:value-of select="translate(topMdMsl,',','')"/>
						</fo:block>
					</xsl:when>
					<xsl:otherwise>
						<fo:block>
							<xsl:value-of select="translate(topMdMsl,',','')"/> - <xsl:value-of select="translate(bottomMdMsl,',','')"/>
						</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<xsl:variable name="gasType2" select="gastype"/>
				<xsl:choose>
					<xsl:when test="$gasType2='Drilled'">
						<fo:block text-align="center">
							(Min)<xsl:value-of select="translate(totalfrom,',','')"/>
						</fo:block>
						<fo:block text-align="center">
							(Max)<xsl:value-of select="translate(totalto,',','')"/>
						</fo:block>
						<fo:block text-align="center">
							(Avg.)<xsl:value-of select="translate(avgGasConcentration,',','')"/>
						</fo:block>
					</xsl:when>
					<xsl:otherwise>
						<fo:block text-align="center">
							<xsl:value-of select="translate(totalgasvalue,',','')"/>
						</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(c1MethaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(c2EthaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(c3PropaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(ic4IsoButaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(nc4NorButaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(ic5SoPethaneFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(c02CarbonDioxideFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="translate(h2sHydrogenSulfideFrom,',','')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<xsl:variable name="gasType" select="gastype"/>
					<xsl:choose>
						<xsl:when test="$gasType='Peak'">
							<fo:block text-align="center">
								GWR - <xsl:value-of select="translate(gwrFrom,',','')"/>
							</fo:block>
							<fo:block text-align="center">
								LHR - <xsl:value-of select="translate(lhrFrom,',','')"/>
							</fo:block>
							<fo:block text-align="center">
								OCQ - <xsl:value-of select="translate(ocqFrom,',','')"/>
							</fo:block>
						</xsl:when>
						<xsl:otherwise>
							<fo:block text-align="center">
								GWR - Not Required
							</fo:block>
							<fo:block text-align="center">
								LHR - Not Required
							</fo:block>
							<fo:block text-align="center">
								OCQ - Not Required
							</fo:block>
						</xsl:otherwise>
					</xsl:choose>
			</fo:table-cell>
		</fo:table-row>
		<xsl:choose>
		 <xsl:when test="string-length(descr) &gt; 3">
		    <fo:table-row>
              <fo:table-cell number-columns-spanned="11" padding="0.05cm" border-bottom="0.25px solid black">
                 <fo:list-block provisional-distance-between-starts="22mm">
                    <fo:list-item>
                       <fo:list-item-label end-indent="label-end()">
                          <fo:block font-weight="bold">Comment - </fo:block>
                       </fo:list-item-label>
                       <fo:list-item-body start-indent="body-start()" end-indent="1mm">
                          <fo:block text-align="left">
                             <xsl:value-of select="descr"/>
                          </fo:block>
                       </fo:list-item-body>
                    </fo:list-item>
                 </fo:list-block>
              </fo:table-cell>
           </fo:table-row>
		 </xsl:when>
		 <xsl:otherwise>
		 </xsl:otherwise>
	    </xsl:choose>
				</xsl:otherwise>
		    </xsl:choose>
	</xsl:template>
	<!-- gasreadings section END -->

</xsl:stylesheet>