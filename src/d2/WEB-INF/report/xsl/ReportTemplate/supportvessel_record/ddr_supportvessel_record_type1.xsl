<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/SupportVesselParam">
			<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="2pt"
				border-top="thin solid black" border-left="thin solid black" border-right="thin solid black">
				<fo:table-column column-number="1" column-width="proportional-column-width(14)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(14)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(14)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(14)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(44)"/>
											
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell padding="2px" border-bottom="thin solid black" number-columns-spanned="5" background-color="rgb(223,223,223)">
							<fo:block font-weight="bold" font-size="8pt">
								Support Vessel
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell border-bottom="thin solid black" padding="1px" padding-left="2px" padding-right="2px" padding-top="2px">
							<fo:block text-align="center">
								Vessel Name
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="thin solid black" padding="1px" padding-left="2px" padding-right="2px" padding-top="2px">
							<fo:block text-align="center">
								Arrived Time
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="thin solid black" padding="1px" padding-left="2px" padding-right="2px" padding-top="2px">
							<fo:block text-align="center">
								Departed Time
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="thin solid black" padding="1px" padding-left="2px" padding-right="2px" padding-top="2px">
							<fo:block text-align="center">
								Status
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="thin solid black" padding="1px" padding-left="2px" padding-right="2px" padding-top="2px">
							<fo:block text-align="center">
								Bulks
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
					<xsl:apply-templates select="SupportVesselParam">
						<xsl:sort select="SupportVesselParam/sequence/@rawNumber" data-type="number"/>
					</xsl:apply-templates>		
				</fo:table-body>
			</fo:table>
	</xsl:template>	
	
	<xsl:template match="SupportVesselParam">
			<fo:table-row keep-together="always">
				<fo:table-cell border-right="thin solid black" border-bottom="thin solid black" padding-top="3px"
					padding="1px" padding-left="2px" padding-right="2px">
					<fo:block>
						<xsl:value-of select="supportVesselInformationUid/@lookupLabel"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="thin solid black" border-bottom="thin solid black" padding-top="3px"
					padding="1px" padding-left="2px" padding-right="2px">
					<fo:block text-align="center">
						<xsl:value-of select="arrdt"/>
					</fo:block>
					<fo:block text-align="center">
						<xsl:value-of select="d2Utils:formatDateFromEpochMS(arrdt/@epochMS,'HH:mm')"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="thin solid black" border-bottom="thin solid black" padding-top="3px"
					padding="1px" padding-left="2px" padding-right="2px">
					<fo:block text-align="center">
						<xsl:value-of select="depdt"/>
					</fo:block>
					<fo:block text-align="center">
						<xsl:value-of select="d2Utils:formatDateFromEpochMS(depdt/@epochMS,'HH:mm')"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="thin solid black" border-bottom="thin solid black" padding-top="3px"
					padding="1px" padding-left="2px" padding-right="2px">
					<fo:block linefeed-treatment="preserve">
						<xsl:value-of select="currentstat"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-bottom="thin solid black">
					<xsl:if test="count(RigStock) &gt; 0">					
						<fo:block>
							<fo:table inline-progression-dimension="100%" table-layout="fixed" font-size="6pt">
								<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="6" column-width="proportional-column-width(15)"/>	
								<fo:table-body>	
									<fo:table-row>
										<fo:table-cell border-bottom="thin solid black" padding="1px">
											<fo:block text-align="center" font-weight="bold">
												Item
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-bottom="thin solid black" padding="1px">
											<fo:block text-align="center" font-weight="bold">
												Unit
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-bottom="thin solid black" padding="1px" display-align="center">
											<fo:block text-align="center" font-weight="bold">
												Start Amount
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-bottom="thin solid black" padding="1px" display-align="center">
											<fo:block text-align="center" font-weight="bold">
												Previous Balance
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-bottom="thin solid black" padding="1px" display-align="center">
											<fo:block text-align="center" font-weight="bold">
												Used
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-bottom="thin solid black" padding="1px" display-align="center">
											<fo:block text-align="center" font-weight="bold">
												Balance
											</fo:block>
										</fo:table-cell>
									</fo:table-row>					
									<xsl:apply-templates select="RigStock" mode="support_rigstock"/>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</xsl:if>
				</fo:table-cell>
			</fo:table-row>
	</xsl:template>
	
	<!-- Rig Stock 	-->
	<xsl:template match="RigStock" mode="support_rigstock">
	<xsl:choose>
		<xsl:when test="position() mod 2 != '0'">
			<fo:table-row>
				<fo:table-cell border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px" padding-top="3px" border-bottom="thin solid black">
					<fo:block>
					<xsl:choose>													
						<xsl:when test = "string(stockCode/@lookupLabel)=''">
							<xsl:value-of select="stockCode"/>										
						</xsl:when>													
						<xsl:otherwise>
							<xsl:value-of select="stockCode/@lookupLabel"/>
						</xsl:otherwise>
					</xsl:choose>					
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px"  padding-top="3px" border-bottom="thin solid black">
					<fo:block text-align="right">
						<xsl:value-of select="storedUnit"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px"  padding-top="3px" border-bottom="thin solid black">
					<fo:block text-align="right">
					<xsl:if test= "(amtInitial/@rawNumber != '0.00')">
						<xsl:value-of select="amtInitial"/>
					</xsl:if>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px"  padding-top="3px" border-bottom="thin solid black">
					<fo:block text-align="right">
					<xsl:if test= "(previousBalance/@rawNumber != '0.00')">
						<xsl:value-of select="previousBalance"/>
					</xsl:if>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px"  padding-top="3px" border-bottom="thin solid black">
					<fo:block text-align="right">
						<xsl:value-of select="amtUsed"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"  padding-top="3px" border-bottom="thin solid black">
					<fo:block text-align="right">
						<xsl:value-of select="d2Utils:formatNumber(dynaAttr/currentBalance,'###0.#')"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:when>
		<xsl:otherwise>
			<fo:table-row background-color="#EEEEEE">
				<fo:table-cell border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px" padding-top="3px" border-bottom="thin solid black">
					<fo:block>
					<xsl:choose>													
						<xsl:when test = "string(stockCode/@lookupLabel)=''">
							<xsl:value-of select="stockCode"/>										
						</xsl:when>													
						<xsl:otherwise>
							<xsl:value-of select="stockCode/@lookupLabel"/>
						</xsl:otherwise>
					</xsl:choose>					
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px"  padding-top="3px" border-bottom="thin solid black">
					<fo:block text-align="right">
						<xsl:value-of select="storedUnit"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px"  padding-top="3px" border-bottom="thin solid black">
					<fo:block text-align="right">
					<xsl:if test= "(amtInitial/@rawNumber != '0.00')">
						<xsl:value-of select="amtInitial"/>
					</xsl:if>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px"  padding-top="3px" border-bottom="thin solid black">
					<fo:block text-align="right">
					<xsl:if test= "(previousBalance/@rawNumber != '0.00')">
						<xsl:value-of select="previousBalance"/>
					</xsl:if>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-right="thin solid black" padding="1px" padding-left="2px" padding-right="2px"  padding-top="3px" border-bottom="thin solid black">
					<fo:block text-align="right">
						<xsl:value-of select="amtUsed"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"  padding-top="3px" border-bottom="thin solid black">
					<fo:block text-align="right">
						<xsl:value-of select="d2Utils:formatNumber(dynaAttr/currentBalance,'###0.#')"/>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:otherwise>
	</xsl:choose>
	</xsl:template>

</xsl:stylesheet>