<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Formation header START -->
	<xsl:template name="formation_header">	
		<fo:table-row>
			<fo:table-cell padding="3px" border-top="0.25px solid black"
				border-bottom="0.25px solid black" padding-left="3px" padding-right="3px">																
				<fo:block text-align="left"> Name </fo:block>
			</fo:table-cell>				
			<fo:table-cell padding="2px" border-top="0.25px solid black"
				border-bottom="0.25px solid black" padding-left="3px" padding-right="3px">	
				<fo:block text-align="left"> Top </fo:block>																
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Formation header END -->

</xsl:stylesheet>