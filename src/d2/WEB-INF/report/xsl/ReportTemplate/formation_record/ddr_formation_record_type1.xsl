<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TEMPLATE: modules/Formation -->
	<xsl:template match="modules/Formation">
	
		<xsl:variable name="current_depth" select="/root/modules/ReportDaily/ReportDaily/depthMdMsl/@rawNumber"/>
		
		<fo:block>		
			
			<xsl:if test="count(Formation[sampleTopMdMsl/@rawNumber &lt;= $current_depth and string(hideOnDdr) != 'true']) &gt; 0">
			
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt" border-bottom="thin solid black" table-omit-header-at-break="true">
		        	<fo:table-column column-width="proportional-column-width(50)"/>
					<fo:table-column column-width="proportional-column-width(50)"/>
					
					<!-- HEADER -->
					<fo:table-header>
					
						<!-- HEADER 1 -->
						<fo:table-row>
							<fo:table-cell number-columns-spanned="2" padding="2px" background-color="#DDDDDD" border="thin solid black">
								<fo:block font-size="8pt" font-weight="bold">
									Formations
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<!-- HEADER 2 -->
						<fo:table-row>						
							<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center">
									Name
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center">
									Top (<xsl:value-of select="Formation/sampleTopMdMsl/@uomSymbol"/>)
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
					</fo:table-header>
					
					<fo:table-body>									
						<xsl:apply-templates select="Formation[string(hideOnDdr) != 'true']" mode="content">
							<xsl:sort select="sampleTopMdMsl/@rawNumber" data-type="number"/>										
						</xsl:apply-templates>
					</fo:table-body>
					
				</fo:table>
			</xsl:if>
		</fo:block>
	</xsl:template>
	
	<!-- TEMPLATE: Formation -->
	<xsl:template match="Formation" mode="content">
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">		
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="formationName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="sampleTopMdMsl"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="formationName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="sampleTopMdMsl"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>