<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- casing section BEGIN -->
	<xsl:template match="modules/Formation">	
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap"
			border="0.5px solid black" space-after="6pt" space-before="6pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(16)"/>
	        <fo:table-column column-number="2" column-width="proportional-column-width(9)"/>
	        <fo:table-column column-number="3" column-width="proportional-column-width(9)"/>
	        <fo:table-column column-number="4" column-width="proportional-column-width(9)"/>
	        <fo:table-column column-number="5" column-width="proportional-column-width(9)"/>
	        <fo:table-column column-number="6" column-width="proportional-column-width(9)"/>
	        <fo:table-column column-number="7" column-width="proportional-column-width(9)"/>
	       	<fo:table-column column-number="8" column-width="proportional-column-width(30)"/>
	        			
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="8" padding="2px" border="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="center" font-weight="bold" font-size="10pt">
							Prognosis and Preliminary Correlation
						</fo:block>
					</fo:table-cell>
				</fo:table-row>	
							
				<fo:table-row text-align="center">
					<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black" number-rows-spanned="2">
						<fo:block>Top</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black" number-columns-spanned="3">
						<fo:block>
							<fo:inline>
								<fo:block>Actual Depth</fo:block>
								<fo:block>(<xsl:value-of select="/root/modules/Operation/dynaAttr/datumLabel"/>)</fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>Prognosis</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black" number-rows-spanned="2" border-bottom="0.5px solid black">
						<fo:block>
							<fo:inline>
								H/L
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black" number-rows-spanned="2" border-bottom="0.5px solid black">
						<fo:block>Pick Criteria</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" number-rows-spanned="2" border-bottom="0.5px solid black">
						<fo:block>Remarks</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row text-align="center">
					<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>MD</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>TVD</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>TVDSS</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black" border-bottom="0.5px solid black">
						<fo:block>TVDSS</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<!-- Only show formations with hideOnDgr = false & matched last formation UID-->
				<xsl:variable name="lastFormationUid" select="/root/modules/ReportDaily/ReportDaily/dynaAttr/lastFormationUid"/>
				<xsl:apply-templates select="Formation[string(hideOnDgr) != 'true' and formationUid = $lastFormationUid]"/>
			</fo:table-body>
		</fo:table>
												
	</xsl:template>

	<xsl:template match="Formation">
		<xsl:variable name="progTvdMsl" select="prognosedTopTvdMsl/@rawNumber"/>
		<xsl:variable name="tvdMsl" select="sampleTopTvdMsl/@rawNumber"/>	
		<xsl:variable name="diff_from_prog" select="$progTvdMsl - $tvdMsl"/>
		
		<fo:table-row>
			<fo:table-cell padding="2px" border-right="0.5px solid black">
				<fo:block text-align="left">
					<xsl:value-of select="formationName"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="sampleTopMdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="sampleTopTvdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="dynaAttr/tvdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="dynaAttr/progTvdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">
				<fo:block text-align="center">
					<xsl:if test="normalize-space(sampleTopTvdMsl) != 0 and normalize-space(dynaAttr/tvdMsl) != 0">
                    	<xsl:choose>                   	
                    		<xsl:when test="number($diff_from_prog)">
                    			<xsl:choose>
		                    		<xsl:when test="normalize-space($diff_from_prog) &gt; 0">
		                        		<xsl:value-of select="d2Utils:formatNumber(string($diff_from_prog),'#,##0.0')"/> H 
		                        	</xsl:when>
		                        	<xsl:when test="normalize-space($diff_from_prog) &lt; 0">
		                           		<xsl:value-of select="d2Utils:formatNumber(string(($diff_from_prog)*-1),'#,##0.0')"/> L 
		                           	</xsl:when>
		                        	<xsl:otherwise>
		                           		<xsl:value-of select="d2Utils:formatNumber(string(($diff_from_prog)*-1),'#,##0.0')"/>
		                        	</xsl:otherwise>
		                     	</xsl:choose>
                    		</xsl:when>
                    		<xsl:otherwise>
                    			N/A
                    		</xsl:otherwise>
                    	</xsl:choose>
                    </xsl:if>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-right="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="pickCriteria"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px">
				<fo:block text-align="left">
					<xsl:value-of select="formationDescription"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- formationgeolsection END -->

</xsl:stylesheet>