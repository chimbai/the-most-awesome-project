<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- formation tops section BEGIN -->
	<xsl:template match="modules/Formation">
		<fo:table width="19cm" table-layout="fixed" border="0.5px solid black" space-after="2pt" font-size="8pt">
			<fo:table-column column-width="proportional-column-width(3)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(4)"/>
			<!-- <fo:table-column column-width="proportional-column-width(3)"/> -->
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding="0.075cm" number-columns-spanned="10" text-align="center" background-color="rgb(37,87,147)">
						<fo:block font-size="10pt" font-weight="bold" color="white">Formation Tops</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row background-color="rgb(149, 179, 215)" font-weight="bold">
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm" number-rows-spanned="2" display-align="center">
						<fo:block text-align="center">
							Formation
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm" number-columns-spanned="3">
						<fo:block text-align="center">
							Prognosed
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm" number-columns-spanned="3">
						<fo:block text-align="center">
							Actual
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block text-align="center">
							Diff.
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm" number-rows-spanned="2" display-align="center">
						<fo:block text-align="center">
							Thickness
						</fo:block>
						<fo:block text-align="center">
							TVD 
						</fo:block>
						<fo:block text-align="center">
							(<xsl:value-of select="/root/modules/Formation/Formation/thicknessTvd/@uomSymbol" />)
						</fo:block>
					</fo:table-cell>
					 
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm" number-rows-spanned="2" display-align="center">
						<fo:block text-align="center">
							Pick Criteria
						</fo:block>
					</fo:table-cell>
					<!-- 
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm" number-rows-spanned="2" display-align="center">
						<fo:block text-align="center">
							Offset Well Info
						</fo:block>
					</fo:table-cell>
					 -->
				</fo:table-row>
				<fo:table-row background-color="rgb(149, 179, 215)" font-weight="bold">
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block text-align="center">
							MD<xsl:value-of select="/root/datum/currentDatumType" />
						</fo:block>
						<fo:block text-align="center">
							(<xsl:value-of select="/root/modules/Formation/Formation/prognosedTopMdMsl/@uomSymbol" />)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block text-align="center">
							TVD<xsl:value-of select="/root/datum/currentDatumType" />
						</fo:block>
						<fo:block text-align="center">
							(<xsl:value-of select="/root/modules/Formation/Formation/prognosedTopTvdMsl/@uomSymbol" />)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block text-align="center">
							TVDSS
						</fo:block>
						<fo:block text-align="center">
							(<xsl:value-of select="/root/modules/Formation/Formation/sampleTopTvdMsl/@uomSymbol" />)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block text-align="center">
							MD<xsl:value-of select="/root/datum/currentDatumType" />
						</fo:block>
						<fo:block text-align="center">
							(<xsl:value-of select="/root/modules/Formation/Formation/sampleTopMdMsl/@uomSymbol" />)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block text-align="center">
							TVD<xsl:value-of select="/root/datum/currentDatumType" />
						</fo:block>
						<fo:block text-align="center">
							(<xsl:value-of select="/root/modules/Formation/Formation/sampleTopTvdMsl/@uomSymbol" />)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block text-align="center">
							TVDSS
						</fo:block>
						<fo:block text-align="center">
							(<xsl:value-of select="/root/modules/Formation/Formation/sampleTopTvdMsl/@uomSymbol" />)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block text-align="center">
							+/- TVD
						</fo:block>
						<fo:block text-align="center">
							(<xsl:value-of select="/root/modules/Formation/Formation/sampleTopTvdMsl/@uomSymbol" />)
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:apply-templates select="Formation">
					<xsl:sort select="sequence" data-type="number"/>
				</xsl:apply-templates>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<xsl:template match="Formation">
		<xsl:if test="string(hideOnDgr) != 'true'">
			<xsl:variable name="progTvdMsl" select="prognosedTopTvdMsl/@rawNumber"/>
			<xsl:variable name="tvdMsl" select="sampleTopTvdMsl/@rawNumber"/>	
			<xsl:variable name="diff_from_prog" select="$progTvdMsl - $tvdMsl"/>
			<xsl:choose>
				<xsl:when test="position() mod 2 = '0'">
					<fo:table-row background-color="rgb(224,224,224)">
				<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
					<fo:block text-align="left">
						<xsl:value-of select="formationName"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="translate(prognosedTopMdMsl,',','')"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="translate(prognosedTopTvdMsl,',','')"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="translate(dynaAttr/progTvdMslSubsea,',','')"/>
					</fo:block>
				</fo:table-cell>
				
				
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="translate(sampleTopMdMsl,',','')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="translate(sampleTopTvdMsl,',','')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="translate(dynaAttr/tvdMslSubsea,',','')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							<xsl:if test="normalize-space(sampleTopTvdMsl) != 0 and normalize-space(dynaAttr/tvdMslSubsea) != 0">
	                    	<xsl:choose>                   	
	                    		<xsl:when test="number($diff_from_prog)">
	                    			<xsl:choose>
			                    		<xsl:when test="normalize-space($diff_from_prog) &gt; 0">
			                        		<xsl:value-of select="format-number($diff_from_prog,'###0.0')"/> High
			                        	</xsl:when>
			                        	<xsl:when test="normalize-space($diff_from_prog) &lt; 0">
			                           		<xsl:value-of select="format-number(($diff_from_prog)*-1,'###0.0')"/> Low
			                           	</xsl:when>
			                        	<xsl:otherwise>
			                           		<xsl:value-of select="format-number(($diff_from_prog)*-1,'###0.0')"/>
			                        	</xsl:otherwise>
			                     	</xsl:choose>
	                    		</xsl:when>
	                    		<xsl:otherwise>
	                    			-
	                    		</xsl:otherwise>
	                    	</xsl:choose>
	                    </xsl:if>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="translate(thicknessTvd,',','')"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="left">
							<xsl:value-of select="pickCriteria"/>
						</fo:block>
					</fo:table-cell>
					
					<!-- 
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block text-align="left">
							<xsl:value-of select="wellOffsetInfo"/>
						</fo:block>
					</fo:table-cell>
					 -->
				<!-- <xsl:otherwise>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							-
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							-
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							-
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							-
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="left">
							-
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block text-align="left">
							-
						</fo:block>
					</fo:table-cell>
				</xsl:otherwise> -->
			</fo:table-row>
				</xsl:when>
				<xsl:otherwise>
					<fo:table-row>
				<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
					<fo:block text-align="left">
						<xsl:value-of select="formationName"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="translate(prognosedTopMdMsl,',','')"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="translate(prognosedTopTvdMsl,',','')"/>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
					<fo:block text-align="center">
						<xsl:value-of select="translate(dynaAttr/progTvdMslSubsea,',','')"/>
					</fo:block>
				</fo:table-cell>
				
				
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="translate(sampleTopMdMsl,',','')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="translate(sampleTopTvdMsl,',','')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="translate(dynaAttr/tvdMslSubsea,',','')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							<xsl:if test="normalize-space(sampleTopTvdMsl) != 0 and normalize-space(dynaAttr/tvdMslSubsea) != 0">
	                    	<xsl:choose>                   	
	                    		<xsl:when test="number($diff_from_prog)">
	                    			<xsl:choose>
			                    		<xsl:when test="normalize-space($diff_from_prog) &gt; 0">
			                        		<xsl:value-of select="format-number($diff_from_prog,'###0.0')"/> High
			                        	</xsl:when>
			                        	<xsl:when test="normalize-space($diff_from_prog) &lt; 0">
			                           		<xsl:value-of select="format-number(($diff_from_prog)*-1,'###0.0')"/> Low
			                           	</xsl:when>
			                        	<xsl:otherwise>
			                           		<xsl:value-of select="format-number(($diff_from_prog)*-1,'###0.0')"/>
			                        	</xsl:otherwise>
			                     	</xsl:choose>
	                    		</xsl:when>
	                    		<xsl:otherwise>
	                    			-
	                    		</xsl:otherwise>
	                    	</xsl:choose>
	                    </xsl:if>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							<xsl:value-of select="translate(thicknessTvd,',','')"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="left">
							<xsl:value-of select="pickCriteria"/>
						</fo:block>
					</fo:table-cell>
					
					<!-- 
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block text-align="left">
							<xsl:value-of select="wellOffsetInfo"/>
						</fo:block>
					</fo:table-cell>
					 -->
				<!-- <xsl:otherwise>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							-
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							-
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							-
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="center">
							-
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" border-right="0.25px solid black">
						<fo:block text-align="left">
							-
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block text-align="left">
							-
						</fo:block>
					</fo:table-cell>
				</xsl:otherwise> -->
			</fo:table-row>
				</xsl:otherwise>
			</xsl:choose>
		
		</xsl:if>
	</xsl:template>	
	<!-- formation tops section END -->

</xsl:stylesheet>