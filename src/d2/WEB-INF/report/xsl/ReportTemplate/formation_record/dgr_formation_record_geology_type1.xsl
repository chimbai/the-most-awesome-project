<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/Formation">	
	
		<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" margin-top="2pt" border="0.85px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(8)"/>
	        <fo:table-column column-number="2" column-width="proportional-column-width(8)"/>
	        <fo:table-column column-number="3" column-width="proportional-column-width(8)"/>
	        <fo:table-column column-number="4" column-width="proportional-column-width(8)"/>
	        <fo:table-column column-number="5" column-width="proportional-column-width(8)"/>
	        <fo:table-column column-number="6" column-width="proportional-column-width(8)"/>
	        <fo:table-column column-number="7" column-width="proportional-column-width(8)"/>
	        <fo:table-column column-number="8" column-width="proportional-column-width(8)"/>
	        <fo:table-column column-number="9" column-width="proportional-column-width(8)"/>
	       	<fo:table-column column-number="10" column-width="proportional-column-width(10)"/>
	       	<fo:table-column column-number="11" column-width="proportional-column-width(8)"/>
	        <fo:table-column column-number="12" column-width="proportional-column-width(10)"/>
	        			
			<fo:table-header>
				<fo:table-row font-size="10pt">
			    	<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" font-weight="bold" number-columns-spanned="12" text-align="center" background-color="rgb(37,87,147)">
			        	<fo:block color="white">Formation Tops</fo:block>
					</fo:table-cell> 
			    </fo:table-row>
				
							
				<fo:table-row font-size="8pt" font-weight="bold" text-align="center" background-color="rgb(149, 179, 215)">
					<fo:table-cell padding="2px" border-left="0.75px" border-right="0.75px solid black" number-columns-spanned="2" >
						<fo:block>Formation</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black">
						<fo:block>
							<fo:inline>
								<fo:block>Inverted?</fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black" number-columns-spanned="3">
						<fo:block>
							<fo:inline>
								<fo:block>Prognosed</fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black" number-columns-spanned="3">
						<fo:block>
							<fo:inline>
								<fo:block>Actual</fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>
								<fo:block>Diff.</fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline> Thickness</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px">
						<fo:block>
							<fo:inline>
								<fo:block>Pick Criteria</fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>	
				</fo:table-row>
				
				
				
				<fo:table-row font-weight="bold" text-align="center" background-color="rgb(149, 179, 215)">
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black" number-columns-spanned="2">
						<fo:block></fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>
								<fo:block></fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="center" padding="3px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>MD<xsl:value-of select="/root/datum/currentDatumType"/></fo:inline>	
						</fo:block>
						<fo:block text-align="center">
							<fo:inline>(<xsl:value-of select="/root/modules/Formation/Formation/prognosedTopMdMsl/@uomSymbol"/>)</fo:inline>	
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="center" padding="3px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>TVD<xsl:value-of select="/root/datum/currentDatumType"/></fo:inline>
						</fo:block>
						<fo:block text-align="center">
							<fo:inline>(<xsl:value-of select="/root/modules/Formation/Formation/prognosedTopTvdMsl/@uomSymbol"/>)</fo:inline>	
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="center" padding="3px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>TVDSS</fo:inline>
						</fo:block>
						<fo:block text-align="center">
							<fo:inline>(<xsl:value-of select="/root/modules/Formation/Formation/dynaAttr/progTvdMslSubsea/@uomSymbol"/>)</fo:inline>	
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="center" padding="3px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>MD<xsl:value-of select="/root/datum/currentDatumType"/></fo:inline>
						</fo:block>
						<fo:block text-align="center">
							<fo:inline>(<xsl:value-of select="/root/modules/Formation/Formation/sampleTopMdMsl/@uomSymbol"/>)</fo:inline>	
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="center" padding="3px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>TVD<xsl:value-of select="/root/datum/currentDatumType"/></fo:inline>
						</fo:block>
						<fo:block text-align="center">
							<fo:inline>(<xsl:value-of select="/root/modules/Formation/Formation/sampleTopTvdMsl/@uomSymbol"/>)</fo:inline>	
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="center" padding="3px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>TVDSS</fo:inline>
						</fo:block>
						<fo:block text-align="center">
							<fo:inline>(<xsl:value-of select="/root/modules/Formation/Formation/dynaAttr/tvdMslSubsea/@uomSymbol"/>)</fo:inline>	
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell font-weight="bold" text-align="center" padding="3px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>+/- TVD</fo:inline>
						</fo:block>
						<fo:block text-align="center">
							<fo:inline></fo:inline>	
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell text-align="center" padding="2px" border-right="0.75px solid black" border-bottom="0.75px solid black">
						<fo:block>
							<fo:inline>
								<fo:block>TVD</fo:block>
							</fo:inline>
						</fo:block>
						<fo:block>
							<fo:inline>
								<fo:block>(<xsl:value-of select="/root/modules/Formation/Formation/thicknessTvd/@uomSymbol"/>)</fo:block>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-bottom="0.75px solid black">
						<fo:block>
							
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				
				
				
				
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="Formation">
					
				</xsl:apply-templates>
			</fo:table-body>
		</fo:table>
												
	</xsl:template>

	
	<xsl:template match="Formation">
	<xsl:variable name="progTvdMsl" select="d2Utils:formatNumber(string(prognosedTopTvdMsl/@rawNumber),'###0.0')"/>
	<xsl:variable name="tvdMsl" select="d2Utils:formatNumber(string(sampleTopTvdMsl/@rawNumber),'###0.0')"/>	
	<xsl:variable name="diff_from_prog" select="d2Utils:formatNumber(string($progTvdMsl - $tvdMsl),'###0.0')"/>
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
			
				<fo:table-row border-right="0.75px solid black" background-color="white">
					<fo:table-cell padding-left="2px" border-right="0.75px thin solid black" number-columns-spanned="2">
						<fo:block>
							<xsl:value-of select="formationName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="isInverted/@lookupLabel"/> 
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="prognosedTopMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="prognosedTopTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="dynaAttr/progTvdMslSubsea"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="sampleTopMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="sampleTopTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="dynaAttr/tvdMslSubsea"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px solid black" >
							<fo:block text-align="center">
					<xsl:if test="normalize-space(sampleTopTvdMsl) != 0 and normalize-space(dynaAttr/tvdMsl) != 0">
                    	<xsl:choose>                   	
                    		<xsl:when test="number($diff_from_prog)">
                    			<xsl:choose>
		                    		<xsl:when test="normalize-space($diff_from_prog) &gt; 0">
		                        		<xsl:value-of select="d2Utils:formatNumber(string($diff_from_prog),'#,##0')"/> High 
		                        	</xsl:when>
		                        	<xsl:when test="normalize-space($diff_from_prog) &lt; 0">
		                           		<xsl:value-of select="d2Utils:formatNumber(string(($diff_from_prog)*-1),'#,##0')"/> Low 
		                           	</xsl:when>
		                        	<xsl:otherwise>
		                           		<xsl:value-of select="d2Utils:formatNumber(string(($diff_from_prog)*-1),'#,##0')"/>
		                        	</xsl:otherwise>
		                     	</xsl:choose>
                    		</xsl:when>
                    		
                    	</xsl:choose>
                    </xsl:if>
							</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px solid black" padding-left="2px" text-align="center">
						<fo:block>
							<xsl:value-of select="thicknessTvd"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="left">
						<fo:block>
							<xsl:value-of select="pickCriteria"/>
						</fo:block>
					</fo:table-cell>
					
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				
				<fo:table-row border-right="0.75px solid black" background-color="#EEEEEE">
					<fo:table-cell padding-left="2px" border-right="0.75px thin solid black" number-columns-spanned="2">
						<fo:block>
							<xsl:value-of select="formationName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="isInverted/@lookupLabel"/> 
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="prognosedTopMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="prognosedTopTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="dynaAttr/progTvdMslSubsea"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="sampleTopMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="sampleTopTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px thin solid black">
						<fo:block>
							<xsl:value-of select="dynaAttr/tvdMslSubsea"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="center" border-right="0.75px solid black" >
							<fo:block text-align="center">
					<xsl:if test="normalize-space(sampleTopTvdMsl) != 0 and normalize-space(dynaAttr/tvdMsl) != 0">
                    	<xsl:choose>                   	
                    		<xsl:when test="number($diff_from_prog)">
                    			<xsl:choose>
		                    		<xsl:when test="normalize-space($diff_from_prog) &gt; 0">
		                        		<xsl:value-of select="d2Utils:formatNumber(string($diff_from_prog),'#,##0')"/> High 
		                        	</xsl:when>
		                        	<xsl:when test="normalize-space($diff_from_prog) &lt; 0">
		                           		<xsl:value-of select="d2Utils:formatNumber(string(($diff_from_prog)*-1),'#,##0')"/> Low 
		                           	</xsl:when>
		                        	<xsl:otherwise>
		                           		<xsl:value-of select="d2Utils:formatNumber(string(($diff_from_prog)*-1),'#,##0')"/>
		                        	</xsl:otherwise>
		                     	</xsl:choose>
                    		</xsl:when>
                    		
                    	</xsl:choose>
                    </xsl:if>
							</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="0.75px solid black" padding-left="2px" text-align="center">
						<fo:block>
							<xsl:value-of select="thicknessTvd"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2px" text-align="left">
						<fo:block>
							<xsl:value-of select="pickCriteria"/>
						</fo:block>
					</fo:table-cell>
					
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	
			
	
	</xsl:template>

</xsl:stylesheet>







