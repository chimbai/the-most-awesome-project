<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Formation records START -->
	<xsl:template match="Formation" mode="content">		
		<fo:table-row>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm"
				border-right="0.25px solid black">
				<fo:block text-align="left">
					<xsl:value-of select="formationName"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="sampleTopMdMsl"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="sampleTopMdMsl/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Formation records END -->
	
</xsl:stylesheet>