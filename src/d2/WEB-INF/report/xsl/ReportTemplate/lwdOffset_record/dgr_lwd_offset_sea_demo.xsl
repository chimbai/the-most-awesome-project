<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	

	<xsl:template match="modules/LwdRun/LwdSuite">
	
	<xsl:variable name="current_date" select="/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS"/>
	<xsl:variable name="day_uid" select="/root/modules/Daily/Daily/dailyUid"/>
	<xsl:variable name="in_uid" select="inDailyUid"/>
	<xsl:variable name="out_uid" select="outDailyUid"/>
	<xsl:variable name="in_date" select="/root/modules/Daily/Daily[dailyUid = $in_uid]/dayDate/@epochMS"/>
	<xsl:variable name="out_date" select="/root/modules/Daily/Daily[dailyUid = $out_uid]/dayDate/@epochMS"/>
	<xsl:variable name="current_day" select="/root/modules/Daily/Daily[dailyUid = $out_uid]/dayDate"/>
	<xsl:if test="$in_date &lt;= $current_date and $current_date &lt;= $out_date">  
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" border-right="0.5px solid black" border-bottom="0.5px solid black" margin-top="2pt" border-before-width="0.5px" 
			border-before-style="solid" border-before-width.conditionality="retain" border-after-width="0.5px" border-after-style="solid" border-after-width.conditionality="retain"
			border-left="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(45)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(45)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell text-align="center" padding="0.3px" number-columns-spanned="3">
						<fo:block font-weight="bold" text-align="center" background-color="rgb(242,242,242)" padding="2px" padding-left="-0.3px" padding-right="-0.3px">
							LWD Offset
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border-bottom="0.5px solid black" text-align="center" padding="0.3px" number-columns-spanned="3">
						<fo:block font-weight="bold" text-align="center" background-color="rgb(242,242,242)" padding="2px" padding-left="-0.3px" padding-right="-0.3px">
							(Associated BHA# <xsl:value-of select="Lwd/bharunUid/@lookupLabel"/>, Associated Bit# <xsl:value-of select="Lwd/dynaAttr/bitrunNumber"/>)
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>	
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center" >
							Name
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center" >
							Offset
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-right="0.5px solid black">
						<fo:block text-align="center" >
							Comment
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>				
				<xsl:apply-templates select="LwdTool">
				</xsl:apply-templates>
			</fo:table-body>
		</fo:table>
	</xsl:if>
	</xsl:template>

	<xsl:template match="LwdTool">
		<fo:table-row>	
			<fo:table-cell padding="2px" border-top="0.5px solid black" border-right="0.5px solid black">
				<fo:block text-align="left">
					<xsl:value-of select="toolNumber"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-top="0.5px solid black" border-right="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="distanceFromBit"/><xsl:value-of select="distanceFromBit/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px" border-top="0.5px solid black" border-right="0.5px solid black">
				<fo:block text-align="left">
					<xsl:value-of select="runSummary"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

</xsl:stylesheet>