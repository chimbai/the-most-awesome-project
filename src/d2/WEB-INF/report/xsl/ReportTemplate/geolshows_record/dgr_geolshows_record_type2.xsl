<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<!-- wells section BEGIN -->
	<xsl:template match="modules/Geolshows">
	
		<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" wrap-option="wrap" border="0.85px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(4.5)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(4.5)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(4.5)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(6.5)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(4)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(5)"/>
			
			<fo:table-column column-number="8" column-width="proportional-column-width(3.5)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(4)"/>
			<fo:table-column column-number="10" column-width="proportional-column-width(4.5)"/>
			<fo:table-column column-number="11" column-width="proportional-column-width(4)"/>
			<fo:table-column column-number="12" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="13" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="14" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="15" column-width="proportional-column-width(4)"/>
			<fo:table-column column-number="16" column-width="proportional-column-width(4)"/>
			<fo:table-column column-number="17" column-width="proportional-column-width(4.5)"/>
			<fo:table-column column-number="18" column-width="proportional-column-width(4)"/>
			<fo:table-column column-number="19" column-width="proportional-column-width(4)"/>
			<fo:table-column column-number="20" column-width="proportional-column-width(3)"/>
			<fo:table-column column-number="21" column-width="proportional-column-width(4.5)"/>
			
			<fo:table-header>
				<fo:table-row color="white" background-color="rgb(37,87,147)">
					<fo:table-cell number-columns-spanned="21" padding="2px" border-bottom="0.75px solid black">
						<fo:block text-align="center" font-weight="bold" font-size="10pt">
							Oil Shows
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
			
									<fo:table-row font-weight="bold" background-color="rgb(149, 179, 215)">
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black" number-columns-spanned="7">
											<fo:block text-align="center">
												Basic Details
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-bottom="0.75px solid black" number-columns-spanned="14">
											<fo:block text-align="center">
												Oil Stain Details
											</fo:block>
										</fo:table-cell>
			 						</fo:table-row>
									
									<fo:table-row font-size="5pt" font-weight="bold" text-align="center" background-color="rgb(149, 179, 215)">
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Depth From (ft)
											</fo:block>
											
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Depth To  (ft)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Thickness (m)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Formation
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Lithology
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												MW   Over Balance
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Increase in ROP into Show Interval
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Visual Porosity
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Show Lithology With Fluor
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												White Light: Amount of Stain
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												White Light: Stain Color
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												White Light: Cut Color
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												White Light: Amount of Residue
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Fluor Colour
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Fluor Distrib
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Fluor Intensity
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Dominant Cut Fluor
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												HC Residue Spot Dish
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												HC Odour
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black">
											<fo:block text-align="center">
												Oil Scum in Mud
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-bottom="0.75px solid black" >
											<fo:block text-align="center">
												Show Rate (Category)
											</fo:block>
										</fo:table-cell> 
									</fo:table-row>						
			</fo:table-header>
			<fo:table-body>				
				<xsl:apply-templates select="Geolshows"/>
			</fo:table-body>
		</fo:table>
												
	</xsl:template>

	<xsl:template match="Geolshows" >
	
		<fo:table-row text-align="center" font-size="6pt">
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(fromDepthMdMsl)= 'NULL' or string(fromDepthMdMsl)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="fromDepthMdMsl"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(toDepthMdMsl)= 'NULL' or string(toDepthMdMsl)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="toDepthMdMsl"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(thickness)= 'NULL' or string(thickness)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="thickness"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(formationName)= 'NULL' or string(formationName)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="formationName"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(lithology)= 'NULL' or string(lithology)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="lithology"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(mudWeightOverbalanceRate/@lookupLabel)= 'NULL' or string(mudWeightOverbalanceRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="mudWeightOverbalanceRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(ropIntoShowIncreaseRate/@lookupLabel)= 'NULL' or string(ropIntoShowIncreaseRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="ropIntoShowIncreaseRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(visualPorosityRate/@lookupLabel)= 'NULL' or string(visualPorosityRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="visualPorosityRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(fluoInShowLithologyRate/@lookupLabel)= 'NULL' or string(fluoInShowLithologyRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="fluoInShowLithologyRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(whitelightStain)= 'NULL' or string(whitelightStain)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="whitelightStain"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(showColour)= 'NULL' or string(showColour)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="showColour"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>	
			</fo:table-cell>	
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(whitelightCutColor)= 'NULL' or string(whitelightCutColor)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="whitelightCutColor"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(whitelightResidue)= 'NULL' or string(whitelightResidue)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="whitelightResidue"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(fluorColourRate/@lookupLabel)= 'NULL' or string(fluorColourRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="fluorColourRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
		
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(fluorDistributionRate/@lookupLabel)= 'NULL' or string(fluorDistributionRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="fluorDistributionRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(fluorIntensityRate/@lookupLabel)= 'NULL' or string(fluorIntensityRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="fluorIntensityRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(dominantCutTypeRate/@lookupLabel)= 'NULL' or string(dominantCutTypeRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="dominantCutTypeRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(hcResidueRate/@lookupLabel)= 'NULL' or string(hcResidueRate/@lookupLabel)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="hcResidueRate/@lookupLabel"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(showevalOdour)= 'NULL' or string(showevalOdour)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="showevalOdour"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px" border-right="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(showeval)= 'NULL' or string(showeval)= ' '">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="showeval"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
			
			<fo:table-cell padding="2px">
				<xsl:choose>													
					<xsl:when test = "string(rating)= ''">
						<fo:block text-align="center">
						-	
						</fo:block>									
					</xsl:when>													
					<xsl:otherwise>
					<fo:block text-align="center">
						<xsl:value-of select="rating"/>
					</fo:block>
					</xsl:otherwise> 
				</xsl:choose>
			</fo:table-cell>
		</fo:table-row>

	</xsl:template>

</xsl:stylesheet>