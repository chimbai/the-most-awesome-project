<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- D2_TATELY DGR OIL SHOW SECTION -->
	<xsl:template match="modules/Geolshows">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" border-right="0.5px solid black" border-bottom="0.5px solid black" margin-top="2pt" border-before-width="0.5px" 
			border-before-style="solid" border-before-width.conditionality="retain" border-after-width="0.5px" border-after-style="solid" border-after-width.conditionality="retain"
			border-left="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(1)"/>
			
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell padding="0.3px">
						<fo:block text-align="center" font-weight="bold" font-size="9pt" padding="2px" padding-left="-0.3px" padding-right="-0.3px" background-color="rgb(223,223,223)">
							Oil Shows
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
			</fo:table-header>
			<fo:table-body>				
				<xsl:apply-templates select="Geolshows"/>
			</fo:table-body>
		</fo:table>
												
	</xsl:template>

	<xsl:template match="Geolshows"> 
		<fo:table-row keep-together="always">
			<fo:table-cell>
				<fo:block>
					<fo:table table-layout="fixed">
						<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(35)"/>
						<fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
						<fo:table-column column-number="4" column-width="proportional-column-width(35)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.3px" border-bottom="0.5px solid black" number-columns-spanned="4" border-top="0.5px solid black">
									<fo:block text-align="left" padding="2px" padding-left="-0.3px" padding-right="-0.3px" background-color="rgb(223,223,223)">
										Depth <xsl:value-of select="fromDepthMdMsl"/><xsl:value-of select="fromDepthMdMsl/@uomSymbol"/>
										 - <xsl:value-of select="toDepthMdMsl"/><xsl:value-of select="toDepthMdMsl/@uomSymbol"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										Rating:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black">
									<fo:block text-align="right">
										<xsl:value-of select="rating/@lookupLabel"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										White Light Stain:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="right">
										<xsl:value-of select="whitelightStain"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										Percentage:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black">
									<fo:block text-align="right">
										<xsl:value-of select="showPercentage"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										White Light Cut:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="right">
										<xsl:value-of select="whitelightCut"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										Lithology:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black">
									<fo:block text-align="right">
										<xsl:value-of select="lithology"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										White Light Residue:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="right">
										<xsl:value-of select="whitelightResidue"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										Speed:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black">
									<fo:block text-align="right">
										<xsl:value-of select="cutFluoSpeed/@lookupLabel"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										UV Fluoresce:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="right">
										<xsl:value-of select="uvFluoresce"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										Intensity:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black">
									<fo:block text-align="right">
										<xsl:value-of select="cutFluoIntensity"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										UV Cut Fluoresce:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="right">
										<xsl:value-of select="uvCutFluoresce"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										Description:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px" border-right="0.5px solid black">
									<fo:block text-align="left" linefeed-treatment="preserve">
										<xsl:value-of select="descr"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="left">
										UV Residue:
									</fo:block>
								</fo:table-cell>
								<fo:table-cell padding="2px">
									<fo:block text-align="right">
										<xsl:value-of select="uvResidue"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
		
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	
	</xsl:template>
	
</xsl:stylesheet>