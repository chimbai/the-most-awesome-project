<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	<xsl:import href="./hse_record_header.xsl" />
	<xsl:import href="./hse_record.xsl" />
	
	<!-- hse incident or summary BEGIN -->	
	<xsl:template match="modules/HseIncident">
	<xsl:if test="count(HseIncident[string(onedayreport) != '0']) &gt; 0">
		<fo:table width="100%" table-layout="fixed" font-size="8pt" margin-top="2pt"
			border-left="0.5px solid black" border-right="0.5px solid black" border-bottom="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(40)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm"
						number-columns-spanned="5" background-color="rgb(224,224,224)" border-top="0.5px solid black"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							HSE Data
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>				
				<xsl:call-template name="hse_incident_header"/>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="HseIncident[string(onedayreport) != '0']"/>
			</fo:table-body>
		</fo:table>
	</xsl:if>
	</xsl:template>
	<!-- hse section END -->
	
</xsl:stylesheet>