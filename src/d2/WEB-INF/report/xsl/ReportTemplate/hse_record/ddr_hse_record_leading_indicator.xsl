<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	<!-- LEADING INDICATOR SECTION -->
	
	<xsl:template match="modules/LeadingIndicator/LeadingIndicator">	
		<xsl:variable name="columnCount" select="count(Indicator[string-length(normalize-space(hsePlanEventsValue)) &gt;0 ])"/>
		<fo:table width="100%" table-layout="fixed" font-size="6pt" margin-top="2pt" border="0.5px solid black" wrap-option="wrap">
			<fo:table-column column-width="proportional-column-width(2)"/>
			<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) &gt; 0]">
				<fo:table-column column-width="proportional-column-width(1)"/>
			</xsl:for-each>
							
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$columnCount} + 1">
						<fo:block font-weight="bold" font-size="10pt">Leading Indicators</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) &gt; 0]">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="shortName"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							24Hr
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) &gt; 0]">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="dailyEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Well To Date
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) &gt; 0]">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="wellToDateEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" background-color="rgb(223,223,223)">
						<fo:block>
							Planned Targets per month
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) &gt; 0]">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" background-color="rgb(223,223,223)">
							<fo:block>
								<xsl:value-of select="hsePlanEventsValue"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Month Actual
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) &gt; 0]">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="monthlyEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Year To Date
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) &gt; 0]">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="yearlyEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Comments/ Findings
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$columnCount}">
						<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) &gt; 0]">
							<xsl:if test="string-length(normalize-space(comment)) &gt; 0">
								<fo:block>
									<xsl:value-of select="comment"/>
								</fo:block>
							</xsl:if>	
						</xsl:for-each>
					</fo:table-cell>
					
				</fo:table-row>
			</fo:table-body>
		</fo:table>	
		
		<fo:table width="100%" table-layout="fixed" font-size="6pt" space-before="2pt" border="0.5px solid black" wrap-option="wrap" margin-bottom="2pt">
			<fo:table-column column-width="proportional-column-width(2)"/>
			<xsl:variable name="columnCount2" select="count(Indicator[string-length(normalize-space(hsePlanEventsValue))=0])"/>
			<xsl:variable name="columnCount3" select="$columnCount - $columnCount2"/>
					
			<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) &gt; 0]">
				<fo:table-column column-width="proportional-column-width(1)"/>
			</xsl:for-each>
			
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$columnCount} + 1">
						<fo:block font-weight="bold" font-size="10pt">Leading Indicators</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
										
					<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) = 0]">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="shortName"/>
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
					
					<xsl:call-template name="loop">
				    	<xsl:with-param name="var" select="$columnCount3"/>
				    </xsl:call-template>
										
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							24Hr
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) = 0]">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="dailyEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
					
					<xsl:call-template name="loop">
				    	<xsl:with-param name="var" select="$columnCount3"/>
				    </xsl:call-template>
				    
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Well To Date
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) = 0]">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="wellToDateEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
					
					<xsl:call-template name="loop">
				    	<xsl:with-param name="var" select="$columnCount3"/>
				    </xsl:call-template>
				    
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Month Actual
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) = 0]">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="monthlyEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
					
					<xsl:call-template name="loop">
				    	<xsl:with-param name="var" select="$columnCount3"/>
				    </xsl:call-template>
				    
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Year To Date
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) = 0]">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="yearlyEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
					
					<xsl:call-template name="loop">
				    	<xsl:with-param name="var" select="$columnCount3"/>
				    </xsl:call-template>
				    
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Comments/ Findings
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$columnCount}">
						<xsl:for-each select ="Indicator[string-length(normalize-space(hsePlanEventsValue)) = 0]">
							<xsl:if test="string-length(normalize-space(comment)) &gt; 0">
								<fo:block>
									<xsl:value-of select="comment"/>
								</fo:block>
							</xsl:if>	
						</xsl:for-each>
					</fo:table-cell>
					
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template name="loop">
		<xsl:param name="var"/>
		
		<xsl:choose>
	   		<xsl:when test="$var &gt; 0">
	   			
	   			<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
					<fo:block color="white">!</fo:block>
				</fo:table-cell>
	   			   		
		      	<xsl:call-template name="loop">
		        	<xsl:with-param name="var">
		          		<xsl:number value="number($var)-1" />
		        	</xsl:with-param>
		      	</xsl:call-template>
	    	</xsl:when>
	   	</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>