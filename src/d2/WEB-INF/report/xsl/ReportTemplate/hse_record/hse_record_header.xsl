<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	<xsl:template name="hse_incident_header">
		<fo:table-row>
			<fo:table-cell border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Events</fo:block>
			</fo:table-cell>
			<fo:table-cell border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Date Of Last</fo:block>
			</fo:table-cell>
			<fo:table-cell border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Days Since</fo:block>
			</fo:table-cell>
			<fo:table-cell border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Description</fo:block>
			</fo:table-cell>
			<fo:table-cell border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Remarks</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
</xsl:stylesheet>