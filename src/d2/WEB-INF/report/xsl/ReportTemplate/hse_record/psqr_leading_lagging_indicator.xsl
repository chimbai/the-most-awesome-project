<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">   

    <xsl:template match="modules/ProcessSafetyQuestionnaire/ProcessSafetyQuestionnaire">

        <!-- LAGGING INDICATOR VARIABLES -->
        <xsl:variable name="t1ColumnCount" select="number(boolean(string(hideLossPrimarySecondaryBarriersOnReport) != 'true'))"/>
        <xsl:variable name="t2ColumnCount" select="number(boolean(string(hideLossWellControlOnReport) != 'true'))"/>
        <xsl:variable name="t3ColumnCount" select="(number(boolean(string(hideKickOnReport) != 'true'))) + (number(boolean(string(hideLossBarrierOnReport) != 'true'))) + (number(boolean(string(hideLossMonitoringCapabilityOnReport) != 'true')))"/>
        <xsl:variable name="columnCount" select="$t1ColumnCount + $t2ColumnCount + $t3ColumnCount"/>
        <xsl:variable name="table1T">
            <xsl:choose>
                <xsl:when test="$t1ColumnCount &gt; 0 and $t2ColumnCount = 0 and $t3ColumnCount = 0">1</xsl:when>
                <xsl:when test="$t1ColumnCount = 0 and $t2ColumnCount &gt; 0 and $t3ColumnCount = 0">2</xsl:when>
                <xsl:when test="$t1ColumnCount = 0 and $t2ColumnCount = 0 and $t3ColumnCount &gt; 0">3</xsl:when>
                <xsl:when test="$t1ColumnCount &gt; 0 and $t2ColumnCount &gt; 0 and $t3ColumnCount = 0">1-2</xsl:when>
                <xsl:when test="$t1ColumnCount &gt; 0 and $t2ColumnCount = 0 and $t3ColumnCount &gt; 0">1&#38;3</xsl:when>
                <xsl:when test="$t1ColumnCount = 0 and $t2ColumnCount &gt; 0 and $t3ColumnCount &gt; 0">2-3</xsl:when>
                <xsl:otherwise>1-3</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- LAGGING INDICATOR SECTION -->

        <xsl:choose>
            <xsl:when test="$columnCount &gt; 0">
                <fo:table width="100%" table-layout="fixed" font-size="6pt" margin-top="30pt" wrap-option="wrap">
                    <fo:table-column column-width="5%"/>
                    <fo:table-header>
                        <fo:table-row>
                            <fo:table-cell border="0" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$columnCount} + 1">
                                <fo:block font-weight="bold" font-size="8pt"/>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$columnCount} + 1">
                                <fo:block font-weight="bold" font-size="8pt">
                                    <xsl:value-of select="concat('Lagging Tier ', $table1T, ' Process Safety Events')"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell text-align="center" display-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-rows-spanned="2">
                                <fo:block font-weight="bold" font-size="8pt">
                                    <xsl:value-of select="concat('T', $table1T, ' KPI')"/>
                                </fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hideLossPrimarySecondaryBarriersOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Tier 1</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossWellControlOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Tier 2</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:choose>
                                <xsl:when test="$t3ColumnCount &gt; 0">
                                    <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$t3ColumnCount}">
                                        <fo:block font-weight="bold" font-size="8pt">Tier 3</fo:block>
                                    </fo:table-cell>
                                </xsl:when>
                            </xsl:choose>
                        </fo:table-row>
                        <fo:table-row>
                            <xsl:if test="string(hideLossPrimarySecondaryBarriersOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Loss of Primary &amp; Secondary Barriers - MAE/MEE</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossWellControlOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Loss of Primary Barrier - Underground Loss of Well Control or a lesser Tier 1 Event</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideKickOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Unintentional Flow (Kick)</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossBarrierOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Loss of Barrier</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <!-- 
                            <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block font-weight="bold" font-size="8pt">Loss of Temporary/Primary Barrier</fo:block>
                            </fo:table-cell>
                             -->
                            <xsl:if test="string(hideLossMonitoringCapabilityOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Loss of Barrier Monitoring Capability</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>Query</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hideLossPrimarySecondaryBarriersOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">1</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossWellControlOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">2</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideKickOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">3a</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossBarrierOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">3b</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <!-- 
                            <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block font-weight="bold">3d</fo:block>
                            </fo:table-cell>
                             -->
                            <xsl:if test="string(hideLossMonitoringCapabilityOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">3c</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                    </fo:table-header>
                    <fo:table-body>             
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>24Hrs</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hideLossPrimarySecondaryBarriersOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="hasLossPrimarySecondaryBarriers/@lookupLabel"/>           
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossWellControlOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="hasLossWellControl/@lookupLabel"/>            
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideKickOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="hasKick/@lookupLabel"/>           
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossBarrierOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="hasLossBarrier/@lookupLabel"/>            
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <!-- 
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="hasLossPrimaryBarrier/@lookupLabel"/>         
                                </fo:block>
                            </fo:table-cell>
                            -->
                            <xsl:if test="string(hideLossMonitoringCapabilityOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="hasLossMonitoringCapability/@lookupLabel"/>           
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>Comments</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hideLossPrimarySecondaryBarriersOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" >
                                    <fo:block linefeed-treatment="preserve">
                                        <xsl:value-of select="lossPrimarySecondaryBarriersComment"/>            
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossWellControlOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block linefeed-treatment="preserve">
                                        <xsl:value-of select="lossWellControlComment"/>         
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideKickOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block linefeed-treatment="preserve">
                                        <xsl:value-of select="kickComment"/>            
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossBarrierOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block linefeed-treatment="preserve">
                                        <xsl:value-of select="lossBarrierComment"/>         
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <!-- 
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="lossPrimaryBarrierComment"/>          
                                </fo:block>
                            </fo:table-cell>
                             -->
                            <xsl:if test="string(hideLossMonitoringCapabilityOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block linefeed-treatment="preserve">
                                        <xsl:value-of select="lossMonitoringCapabilityComment"/>            
                                    </fo:block>
                                </fo:table-cell>                
                            </xsl:if>
                        </fo:table-row>               
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>Well</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hideLossPrimarySecondaryBarriersOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossPrimarySecondaryBarriersByWell"/>           
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossWellControlOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossWellControlByWell"/>            
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideKickOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasKickByWell"/>           
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossBarrierOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossBarrierByWell"/>            
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <!-- 
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossPrimaryBarrierByWell"/>         
                                </fo:block>
                            </fo:table-cell>
                             -->
                            <xsl:if test="string(hideLossMonitoringCapabilityOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossMonitoringCapabilityByWell"/>           
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>Month</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hideLossPrimarySecondaryBarriersOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossPrimarySecondaryBarriersByMonth"/>          
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossWellControlOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossWellControlByMonth"/>           
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideKickOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasKickByMonth"/>          
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossBarrierOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossBarrierByMonth"/>           
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <!-- 
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossPrimaryBarrierByMonth"/>            
                                </fo:block>
                            </fo:table-cell>
                             -->
                            <xsl:if test="string(hideLossMonitoringCapabilityOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossMonitoringCapabilityByMonth"/>          
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>Year</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hideLossPrimarySecondaryBarriersOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossPrimarySecondaryBarriersByYear"/>           
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossWellControlOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossWellControlByYear"/>            
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideKickOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasKickByYear"/>           
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideLossBarrierOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossBarrierByYear"/>            
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <!-- 
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossPrimaryBarrierByYear"/>         
                                </fo:block>
                            </fo:table-cell>
                             -->
                            <xsl:if test="string(hideLossMonitoringCapabilityOnReport) != 'true'">
                                <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block>
                                        <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasLossMonitoringCapabilityByYear"/>           
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                        </fo:table-row> 
                    </fo:table-body>
                </fo:table> 
            </xsl:when>
        </xsl:choose>

        <!-- LEADING INDICATOR VARIABLES -->
        <xsl:variable name="t4section1Count" select="number(boolean(string(hidePhaseBarrierDiagram24hrOnReport) != 'true'))"/>
        <xsl:variable name="t4section2Count" select="number(boolean(string(hideWellControlEquipmentFailedOnReport) != 'true'))"/>
        <xsl:variable name="t4section3Count" select="(number(boolean(string(hidePassCementEquipmentTestOnReport) != 'true'))) + (number(boolean(string(hidePassCementOperationOnReport) != 'true')))"/>
        <xsl:variable name="t4section4Count" select="number(boolean(string(hidePassDrillingCompletionStandardsOnReport) != 'true'))"/>
        <xsl:variable name="t4section5Count" select="(number(boolean(string(hideChangeControl24hrOnReport) != 'true'))) + (number(boolean(string(hideChangeControlApprovedOnReport) != 'true')))"/>
        <xsl:variable name="t4section6Count" select="(number(boolean(string(hideBelowProgramMudWeightOnReport) != 'true'))) + (number(boolean(string(hideHasMonitoredFluidColumnOnReport) != 'true'))) + (number(boolean(string(hideChangeControlMonitorFluidColumnOnReport) != 'true'))) + (number(boolean(string(hideConnectionGasEventOnReport) != 'true')))"/>
        <xsl:variable name="t4section7Count" select="(number(boolean(string(hidePassEquipmentTestOnReport) != 'true'))) + (number(boolean(string(hideTemporaryBarrierPassTestOnReport) != 'true')))"/>
        <xsl:variable name="columnCount2" select="$t4section1Count + $t4section2Count + $t4section3Count + $t4section4Count + $t4section5Count + $t4section6Count + $t4section7Count"/>
        <xsl:variable name="table2MarginTop">
            <xsl:choose>
                <xsl:when test="$columnCount &gt; 0">10pt</xsl:when>
                <xsl:otherwise>30pt</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- LEADING INDICATOR SECTION -->
        <xsl:choose>
            <xsl:when test="$columnCount2 &gt; 0">
                <fo:table width="100%" table-layout="fixed" font-size="6pt" margin-top="{$table2MarginTop}" wrap-option="wrap">
                    <fo:table-column column-width="5%"/>
                    <fo:table-header>
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$columnCount2} + 1">
                                <fo:block font-weight="bold" font-size="8pt">Leading Tier-4 Process Safety Metrics</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell width="10%" display-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-rows-spanned="2">
                                <fo:block font-weight="bold" font-size="8pt">T4 - KPI</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hidePhaseBarrierDiagram24hrOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-rows-spanned="2">
                                    <fo:block font-weight="bold" font-size="8pt">Phase Barrier Diagrams</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideWellControlEquipmentFailedOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Well Control Equipment</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:choose>
                                <xsl:when test="$t4section3Count &gt; 0">
                                    <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$t4section3Count}">
                                        <fo:block font-weight="bold" font-size="8pt">Cementing Operations</fo:block>
                                    </fo:table-cell>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:if test="string(hidePassDrillingCompletionStandardsOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-rows-spanned="2">
                                    <fo:block font-weight="bold" font-size="8pt">D&amp;C Standards - Deviation?</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:choose>
                                <xsl:when test="$t4section5Count &gt; 0">
                                    <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$t4section5Count}">
                                        <fo:block font-weight="bold" font-size="8pt">Change Control</fo:block>
                                    </fo:table-cell>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when test="$t4section6Count &gt; 0">
                                    <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$t4section6Count}">
                                        <fo:block text-align="center" font-weight="bold" font-size="8pt">Fluid Column</fo:block>
                                    </fo:table-cell>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when test="$t4section7Count &gt; 0">
                                    <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$t4section7Count}">
                                        <fo:block font-weight="bold" font-size="8pt">Temporary Barriers</fo:block>
                                    </fo:table-cell>
                                </xsl:when>
                            </xsl:choose>
                        </fo:table-row>
                        <fo:table-row>
                            <xsl:if test="string(hideWellControlEquipmentFailedOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Fail/Unavailable?</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementEquipmentTestOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Equipt OK</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementOperationOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Operation OK</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControl24hrOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Submitted?</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlApprovedOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Approved?</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideBelowProgramMudWeightOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Below Prog MW?</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideHasMonitoredFluidColumnOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Monitored?</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlMonitorFluidColumnOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Change control?</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideConnectionGasEventOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Connection Gas?</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassEquipmentTestOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Equipt OK</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideTemporaryBarrierPassTestOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold" font-size="8pt">Valid?</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>Query</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hidePhaseBarrierDiagram24hrOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">1</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideWellControlEquipmentFailedOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">2</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementEquipmentTestOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">3a</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementOperationOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">3b</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassDrillingCompletionStandardsOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">4</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControl24hrOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">5a</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlApprovedOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">5b</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideBelowProgramMudWeightOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">6a</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideHasMonitoredFluidColumnOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">6b</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlMonitorFluidColumnOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">6c</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideConnectionGasEventOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">6d</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassEquipmentTestOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">7a</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideTemporaryBarrierPassTestOnReport) != 'true'">
                                <fo:table-cell text-align="center" border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                    <fo:block font-weight="bold">7b</fo:block>
                                </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                    </fo:table-header>
                    <fo:table-body> 
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>24Hrs</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hidePhaseBarrierDiagram24hrOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="hasPhaseBarrierDiagram24hr/@lookupLabel"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideWellControlEquipmentFailedOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="hasWellControlEquipmentFailed/@lookupLabel"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementEquipmentTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="hasPassCementEquipmentTest/@lookupLabel"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementOperationOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="hasPassCementOperation/@lookupLabel"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassDrillingCompletionStandardsOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="hasPassDrillingCompletionStandards/@lookupLabel"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControl24hrOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="changeControl24hrAmount"/>          
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlApprovedOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="changeControlApprovedAmount"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideBelowProgramMudWeightOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="isBelowProgramMudWeight/@lookupLabel"/>           
                                </fo:block>
                            </fo:table-cell>                    
                            </xsl:if>
                            <xsl:if test="string(hideHasMonitoredFluidColumnOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="hasMonitoredFluidColumn/@lookupLabel"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlMonitorFluidColumnOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="hasChangeControlMonitorFluidColumn/@lookupLabel"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideConnectionGasEventOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="hasConnectionGasEvent/@lookupLabel"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassEquipmentTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="hasPassEquipmentTest/@lookupLabel"/>          
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideTemporaryBarrierPassTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="hasTemporaryBarrierPassTest/@lookupLabel"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>Comments</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hidePhaseBarrierDiagram24hrOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="phaseBarrierDiagram24hrAmount"/>          
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideWellControlEquipmentFailedOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="wellControlEquipmentFailedComment"/>          
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementEquipmentTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="passCementEquipmentTestComment"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementOperationOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="passCementOperationComment"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassDrillingCompletionStandardsOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="passDrillingCompletionStandardsComment"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControl24hrOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="changeControl24hrComment"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlApprovedOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="changeControlApprovedComment"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideBelowProgramMudWeightOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="belowProgramMudWeightComment"/>           
                                </fo:block>
                            </fo:table-cell>                    
                            </xsl:if>
                            <xsl:if test="string(hideHasMonitoredFluidColumnOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="monitoredFluidColumnComment"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlMonitorFluidColumnOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="changeControlMonitorFluidColumnComment"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideConnectionGasEventOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="connectionGasEventAmount"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassEquipmentTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="passEquipmentTestComment"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideTemporaryBarrierPassTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block linefeed-treatment="preserve">
                                    <xsl:value-of select="temporaryBarrierPassTestComment"/>            
                                </fo:block>
                            </fo:table-cell>            
                            </xsl:if>
                        </fo:table-row>               
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>Well</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hidePhaseBarrierDiagram24hrOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPhaseBarrierDiagram24hrByWell"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideWellControlEquipmentFailedOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasWellControlEquipmentFailedByWell"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementEquipmentTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassCementEquipmentTestByWell"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementOperationOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassCementOperationByWell"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassDrillingCompletionStandardsOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassDrillingCompletionStandardsByWell"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControl24hrOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/changeControl24hrAmountByWell"/>          
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlApprovedOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/changeControlApprovedAmountByWell"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideBelowProgramMudWeightOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/isBelowProgramMudWeightByWell"/>           
                                </fo:block>
                            </fo:table-cell>                    
                            </xsl:if>
                            <xsl:if test="string(hideHasMonitoredFluidColumnOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasMonitoredFluidColumnByWell"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlMonitorFluidColumnOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasChangeControlMonitorFluidColumnByWell"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideConnectionGasEventOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasConnectionGasEventByWell"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassEquipmentTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassEquipmentTestByWell"/>          
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideTemporaryBarrierPassTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasTemporaryBarrierPassTestByWell"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>Month</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hidePhaseBarrierDiagram24hrOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPhaseBarrierDiagram24hrByMonth"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideWellControlEquipmentFailedOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasWellControlEquipmentFailedByMonth"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementEquipmentTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassCementEquipmentTestByMonth"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementOperationOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassCementOperationByMonth"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassDrillingCompletionStandardsOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassDrillingCompletionStandardsByMonth"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControl24hrOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/changeControl24hrAmountByMonth"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlApprovedOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/changeControlApprovedAmountByMonth"/>          
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideBelowProgramMudWeightOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/isBelowProgramMudWeightByMonth"/>          
                                </fo:block>
                            </fo:table-cell>                    
                            </xsl:if>
                            <xsl:if test="string(hideHasMonitoredFluidColumnOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasMonitoredFluidColumnByMonth"/>          
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlMonitorFluidColumnOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasChangeControlMonitorFluidColumnByMonth"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideConnectionGasEventOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasConnectionGasEventByMonth"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassEquipmentTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassEquipmentTestByMonth"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideTemporaryBarrierPassTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasTemporaryBarrierPassTestByMonth"/>          
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>Year</fo:block>
                            </fo:table-cell>
                            <xsl:if test="string(hidePhaseBarrierDiagram24hrOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPhaseBarrierDiagram24hrByYear"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideWellControlEquipmentFailedOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasWellControlEquipmentFailedByYear"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementEquipmentTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassCementEquipmentTestByYear"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassCementOperationOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassCementOperationByYear"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassDrillingCompletionStandardsOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassDrillingCompletionStandardsByYear"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControl24hrOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/changeControl24hrAmountByYear"/>          
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlApprovedOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/changeControlApprovedAmountByYear"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideBelowProgramMudWeightOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/isBelowProgramMudWeightByYear"/>           
                                </fo:block>
                            </fo:table-cell>                    
                            </xsl:if>
                            <xsl:if test="string(hideHasMonitoredFluidColumnOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasMonitoredFluidColumnByYear"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideChangeControlMonitorFluidColumnOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasChangeControlMonitorFluidColumnByYear"/>            
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideConnectionGasEventOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasConnectionGasEventByYear"/>         
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hidePassEquipmentTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasPassEquipmentTestByYear"/>          
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                            <xsl:if test="string(hideTemporaryBarrierPassTestOnReport) != 'true'">
                            <fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
                                <fo:block>
                                    <xsl:value-of select="../../LeadingLaggingCummulativeData/LeadingLaggingIndicatorSummary/hasTemporaryBarrierPassTestByYear"/>           
                                </fo:block>
                            </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>     
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>