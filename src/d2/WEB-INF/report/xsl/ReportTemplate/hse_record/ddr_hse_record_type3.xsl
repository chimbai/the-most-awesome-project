<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	<!-- TEMPLATE: modules/GeneralComment -->
	<xsl:template match="modules/HseIncident">
	
		<fo:block keep-together="always">
		
			<!-- TABLE -->
			<fo:table width="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black" space-after="6pt">>
				<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(20)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(35)"/>
				
				<!-- HEADER -->
				<fo:table-header>
				
					<!-- HEADER 1 -->
					<fo:table-row>
						<fo:table-cell number-columns-spanned="5" padding="2px" background-color="#DDDDDD" border="thin solid black" display-align="center">
							<fo:block font-size="10pt" font-weight="bold">
								HSE Summary
							</fo:block>
						</fo:table-cell>					
					</fo:table-row>
					
					<!-- HEADER 2 -->
					<fo:table-row>
						<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Events
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Date Of Last
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Days Since
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Description
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Remarks
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				
				<!-- BODY -->
				<fo:table-body>
					<xsl:apply-templates select="HseIncident"/>
				</fo:table-body>
				
			</fo:table>
			
		</fo:block>
		
	</xsl:template>
	
	<!-- TEMPLATE: modules/GeneralComment -->
	<xsl:template match="HseIncident">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
			
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:choose>													
								<xsl:when test="string(incidentCategory/@lookupLabel)=''">
									<xsl:value-of select="incidentCategory"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="incidentCategory/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>					
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(hseEventdatetime/@epochMS,'dd MMM yyyy')"/>
						</fo:block>
					</fo:table-cell>			
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
						<xsl:value-of select="dynaAttr/daysLapsed"/>
						<fo:inline color="white">.</fo:inline>
						<xsl:value-of select="dynaAttr/daysLapsed/@uomLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="hseShortdescription"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="description"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			
			</xsl:when>
			<xsl:otherwise>
			
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:choose>													
								<xsl:when test="string(incidentCategory/@lookupLabel)=''">
									<xsl:value-of select="incidentCategory"/>										
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="incidentCategory/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>					
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(hseEventdatetime/@epochMS,'dd MMM yyyy')"/>
						</fo:block>
					</fo:table-cell>			
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
						<xsl:value-of select="dynaAttr/daysLapsed"/>
						<fo:inline color="white">.</fo:inline>
						<xsl:value-of select="dynaAttr/daysLapsed/@uomLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="hseShortdescription"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="description"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
</xsl:stylesheet>