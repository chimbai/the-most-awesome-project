<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	<!-- HSE Incident START -->
	<xsl:template match="HseIncident">
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:choose>													
						<xsl:when test = "string(incidentCategory/@lookupLabel)=''">
							<xsl:value-of select="incidentCategory"/> (<xsl:value-of select="numberOfIncidents"/>)
						</xsl:when>													
						<xsl:otherwise>
							<xsl:value-of select="incidentCategory/@lookupLabel"/> (<xsl:value-of select="numberOfIncidents"/>)
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(hseEventdatetime/@epochMS,'dd MMM yyyy HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="dynaAttr/daysLapsed"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:value-of select="hse_shortdescription"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:value-of select="description"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>	
	</xsl:template>
	<!-- HSE Incident END -->

</xsl:stylesheet>