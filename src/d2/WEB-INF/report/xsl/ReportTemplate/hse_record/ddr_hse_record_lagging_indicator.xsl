<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	
	<!-- LAGGING INDICATOR SECTION -->
	
	<xsl:template match="modules/LaggingIndicator/LaggingIndicator">	
		<fo:table width="100%" table-layout="fixed" font-size="6pt" margin-top="2pt" border="0.5px solid black" wrap-option="wrap">
			<xsl:variable name="columnCount" select="count(Indicator)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<xsl:for-each select ="Indicator">
				<fo:table-column column-width="proportional-column-width(1)"/>
			</xsl:for-each>
							
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$columnCount} + 1">
						<fo:block font-weight="bold" font-size="10pt">Lagging Indicators</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="shortName"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							24Hr
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="dailyEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Well To Date
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="wellToDateEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Month Actual
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="monthlyEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Year To Date
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select ="Indicator">
						<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="yearlyEventNumber"/>			
							</fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
						<fo:block>
							Comments/ Findings
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm" number-columns-spanned="{$columnCount}">
						<xsl:for-each select ="Indicator">
							<xsl:if test="string-length(normalize-space(comment)) &gt; 0">
								<fo:block>
									<xsl:value-of select="comment"/>
								</fo:block>
							</xsl:if>	
						</xsl:for-each>
					</fo:table-cell>
					
				</fo:table-row>
			</fo:table-body>
		</fo:table>	
	
	</xsl:template>

</xsl:stylesheet>