<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- IDS INTERVENTION REPORT TOOLSTRING SECTION -->
	<xsl:template match="modules/ToolString/ToolStringMaster" mode="eow_toolstring">
		<fo:table-row space-after="6pt" keep-together="always">
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block text-align="center">
					<fo:inline font-weight="bold">
					   Toolstring #<xsl:value-of select="toolStringReportNumber"/>
					</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block linefeed-treatment="preserve">
					<xsl:value-of select="comments"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
</xsl:stylesheet>