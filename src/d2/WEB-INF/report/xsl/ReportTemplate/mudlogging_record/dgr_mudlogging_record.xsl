<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/GeologistTeam/GeologistTeam">  
    <fo:table inline-progression-dimension="19.5cm" table-layout="fixed" margin-top="2pt" border="0.85px solid black">
    <fo:table-column column-number="1" column-width="proportional-column-width(25)" />
    <fo:table-column column-number="2" column-width="proportional-column-width(25)" />
    <fo:table-column column-number="3" column-width="proportional-column-width(25)" />
    <fo:table-column column-number="4" column-width="proportional-column-width(25)" />
    <fo:table-header>
    <fo:table-row font-size="10pt">
    	<fo:table-cell padding="0.05cm" border-bottom="0.5px solid black" font-weight="bold" number-columns-spanned="4" text-align="center" background-color="rgb(37,87,147)">
        	<fo:block color="white">Mud Logging Equipment/Personnel</fo:block>
		</fo:table-cell> 
    </fo:table-row>
	</fo:table-header>
	<fo:table-body>  
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm">Data Engineer - Day:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm"> <xsl:value-of select="/root/modules/GeologistTeam/GeologistTeam/dayDataEngineer"/> </fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm">Sample Catcher - Day:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm"> <xsl:value-of select="/root/modules/GeologistTeam/GeologistTeam/daySampleCatcher"/> </fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm">Data Engineer - Night:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm"> <xsl:value-of select="/root/modules/GeologistTeam/GeologistTeam/nightDataEngineer"/> </fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm">Sample Catcher - Night:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm"> <xsl:value-of select="/root/modules/GeologistTeam/GeologistTeam/nightSampleCatcher"/> </fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm">Mud Logger - Day:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm"> <xsl:value-of select="/root/modules/GeologistTeam/GeologistTeam/dayMudLogger"/> </fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm">Specialist/Other - Day:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm"> <xsl:value-of select="/root/modules/GeologistTeam/GeologistTeam/dayMudSpecialist"/> </fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm">Mud Logger - Night:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm"> <xsl:value-of select="/root/modules/GeologistTeam/GeologistTeam/nightMudLogger"/> </fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm">Specialist/Other - Night:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm"> <xsl:value-of select="/root/modules/GeologistTeam/GeologistTeam/nightMudSpecialist"/> </fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm"  font-weight="bold" border-bottom="0.75px solid black" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm">Comments:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" number-columns-spanned="3" border-bottom="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm"> <xsl:value-of select="/root/modules/GeologistTeam/GeologistTeam/comment"/> </fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	<fo:table-row font-size="8pt">
    	<fo:table-cell padding="0.05cm"  font-weight="bold" border-right="0.75px solid black" display-align="center">
			<fo:block border-width="0.5mm">Equipment Related Comments:</fo:block>
		</fo:table-cell>
		<fo:table-cell padding="0.05cm"  font-weight="bold" number-columns-spanned="3" display-align="center">
			<fo:block border-width="0.5mm"> <xsl:value-of select="/root/modules/GeologistTeam/GeologistTeam/equipmentRelatedComment"/> </fo:block>
		</fo:table-cell>
	</fo:table-row>
	
	</fo:table-body>
	</fo:table>
	</xsl:template>
</xsl:stylesheet>