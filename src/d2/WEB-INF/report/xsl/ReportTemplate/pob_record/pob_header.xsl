<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<xsl:template name="pob_header">	
		<fo:table-row>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Job Title</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Personnel</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Company</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
				<fo:block text-align="center">Pax</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

</xsl:stylesheet>