<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="PersonnelOnSite">
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="crewPosition"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="crewName"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="crewCompany/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="pax"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
</xsl:stylesheet>