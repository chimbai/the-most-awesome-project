<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- D2_OMV_GLOBAL CASING SECTION -->
	
	<xsl:template match="modules/PersonnelOnSite">			
		<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt" border="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell padding="2px" number-columns-spanned="4" border-bottom="0.5px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Personnel On Board
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
			</fo:table-header>			
			<fo:table-body font-size="8pt">
				<fo:table-row>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px">
						<fo:block text-align="center">Job Title</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px">
						<fo:block text-align="center">Personnel</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px">
						<fo:block text-align="center">Company</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px">
						<fo:block text-align="center">Pax</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:apply-templates select="PersonnelOnSite"/>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3" border-right="0.5px solid black" border-top="0.5px solid black" padding="2px" >
						<fo:block text-align="right">Total</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="2px" border-top="0.5px solid black">
						<fo:block text-align="right">
							<xsl:value-of select="sum(PersonnelOnSite/pax)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>											
	</xsl:template>

	<xsl:template match="PersonnelOnSite">
		<fo:table-row>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="left">
					<xsl:value-of select="crewPosition"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="left">
					<xsl:value-of select="crewName"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="2px">
				<fo:block text-align="left">
					<xsl:value-of select="crewCompany/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="2px">
				<fo:block text-align="right">
					<xsl:value-of select="pax"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

</xsl:stylesheet>