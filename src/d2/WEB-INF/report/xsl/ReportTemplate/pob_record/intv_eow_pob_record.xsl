<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- INTERVENTION EOW REPORT FOR IDS -->
		
	<xsl:template match="modules/PersonnelSummary">
      <fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" space-after="2pt" border="0.25px solid black">
         <fo:table-column column-width="proportional-column-width(5)"/>
         <fo:table-column column-width="proportional-column-width(4)"/>
         <fo:table-column column-width="proportional-column-width(7)"/>
         <fo:table-column column-width="proportional-column-width(3)"/>
         <fo:table-column column-width="proportional-column-width(3)"/>
         <fo:table-column column-width="proportional-column-width(2)"/>
         <fo:table-header>
            <fo:table-row font-size="9pt">
               <fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" number-columns-spanned="6" background-color="rgb(224,224,224)">
                  <fo:block>Personnel</fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-header>
         <fo:table-body width="3in" font-size="8pt">
            <fo:table-row font-weight="bold">
               <fo:table-cell border-left="0.25px solid black" border-bottom="0.25px solid black" padding="0.02cm">
                  <fo:block background-color="rgb(224,224,224)" text-align="center">Job Title</fo:block>
               </fo:table-cell>
               <fo:table-cell border-bottom="0.25px solid black" padding="0.02cm">
                  <fo:block background-color="rgb(224,224,224)" text-align="center">Name</fo:block>
               </fo:table-cell>
               <fo:table-cell border-bottom="0.25px solid black" padding="0.02cm">
                  <fo:block background-color="rgb(224,224,224)" text-align="center">Employer</fo:block>
               </fo:table-cell>
               <fo:table-cell border-bottom="0.25px solid black" padding="0.02cm">
                  <fo:block background-color="rgb(224,224,224)" text-align="center">Start Date</fo:block>
               </fo:table-cell>
               <fo:table-cell border-bottom="0.25px solid black" padding="0.02cm">
                  <fo:block background-color="rgb(224,224,224)" text-align="center">End Date</fo:block>
               </fo:table-cell>
               <fo:table-cell border-bottom="0.25px solid black" padding="0.02cm">
                  <fo:block background-color="rgb(224,224,224)" text-align="center">Days</fo:block>
               </fo:table-cell>
            </fo:table-row>
            <xsl:apply-templates select="POBSummary"/>
            
         </fo:table-body>
      </fo:table>
   </xsl:template>

	<xsl:template match="POBSummary">
		<fo:table-row keep-with-previous="always">
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="left">
	               <xsl:value-of select="crewPosition"/>
	            </fo:block>
	         </fo:table-cell>
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="left">
	               <xsl:value-of select="name"/>
	            </fo:block>
	         </fo:table-cell>
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="left">
	               <xsl:value-of select="crewCompany_raw"/>
	            </fo:block>
	         </fo:table-cell>
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="center">
	               	<xsl:value-of select="d2Utils:formatDateFromEpochMS(startDate_raw,'dd MMM yyyy')"/>
	            </fo:block>
	         </fo:table-cell>
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="center">
	            	<xsl:if test="/root/modules/ReportDaily/ReportDaily[position()=last()]/reportDatetime/@epochMS != endDate_raw">
	               		<xsl:value-of select="d2Utils:formatDateFromEpochMS(endDate_raw,'dd MMM yyyy')"/>
	               	</xsl:if>
	            </fo:block>
	         </fo:table-cell>
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="center">
	               <xsl:value-of select="total"/>
	            </fo:block>
	         </fo:table-cell>
	      </fo:table-row>
		<!-- 
		<xsl:for-each select="/root/modules/PersonnelSummary/CrewList">
			<xsl:variable name="personnelName" select="crewName"/>
			
			<fo:table-row keep-with-previous="always">
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="left">
	               <xsl:value-of select="/root/modules/PersonnelSummary/PersonnelSummary[crewName = $personnelName and crewPosition != '']/crewPosition"/>
	            </fo:block>
	         </fo:table-cell>
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="left">
	               <xsl:value-of select="$personnelName"/>
	            </fo:block>
	         </fo:table-cell>
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="left">
	               <xsl:value-of select="/root/modules/PersonnelSummary/PersonnelSummary[crewCompany != '' and crewName = $personnelName]/crewCompany"/>
	            </fo:block>
	         </fo:table-cell>
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="center">
	               <xsl:for-each select="/root/modules/PersonnelSummary/PersonnelSummary[crewName = $personnelName]">
	               		<xsl:sort data-type="text" order="descending" select="dayDate"/>
	               		<xsl:if test="position() = last()">
	               			<xsl:value-of select="dayDate"/>
	               		</xsl:if>
	               </xsl:for-each>
	            </fo:block>
	         </fo:table-cell>
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="center">
	            	<xsl:for-each select="/root/modules/PersonnelSummary/PersonnelSummary[crewName = $personnelName]">
	               		<xsl:sort data-type="text" select="dayDate"/>
	               		<xsl:if test="position() = last()">
	               			<xsl:value-of select="dayDate"/>
	               		</xsl:if>
	               </xsl:for-each>
	            </fo:block>
	         </fo:table-cell>
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block text-align="center">
	               <xsl:value-of select="count(/root/modules/PersonnelSummary/PersonnelSummary[crewName = $personnelName])"/>
	            </fo:block>
	         </fo:table-cell>
	         <fo:table-cell border-bottom="0.25px solid black" padding="0.075cm">
	            <fo:block color="white" text-align="center"> ! </fo:block>
	         </fo:table-cell>
	      </fo:table-row>
				
		</xsl:for-each>
		-->
	</xsl:template>

</xsl:stylesheet>