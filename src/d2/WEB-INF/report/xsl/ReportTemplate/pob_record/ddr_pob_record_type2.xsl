<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TEMPLATE: modules/PersonnelOnSite -->
	<xsl:template match="modules/PersonnelOnSite">
	
		<fo:block keep-together="always">
		
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" space-after="6pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(30)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
				
				<!-- HEADER -->
				<fo:table-header>
					
					<!-- HEADER 1 -->
					<fo:table-row>
						<fo:table-cell number-columns-spanned="4" padding="2px" background-color="#DDDDDD" border="thin solid black" display-align="center">
							<fo:block font-size="8pt" font-weight="bold">
								<!-- <xsl:variable name="onOffShore" select="/root/modules/Well/Well/onOffShore"/>
								<xsl:choose>
									<xsl:when test="$onOffShore = 'ON'">
										Personnel On Site
									</xsl:when>
									<xsl:otherwise>
										Personnel On Board
									</xsl:otherwise>
								</xsl:choose> -->
								Personnel On Board
							</fo:block>
						</fo:table-cell>					
					</fo:table-row>
					
					<!-- HEADER 2 -->
					<fo:table-row>
						<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Job Title
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Personnel
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Company
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="center">
								Pax
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
				</fo:table-header>	
				
				<!-- BODY -->
				<fo:table-body>
					
					<xsl:apply-templates select="PersonnelOnSite"/>
					
					<fo:table-row>
						<fo:table-cell number-columns-spanned="3" padding="2px" border="thin solid black" display-align="center">
							<fo:block text-align="right">
								Total
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black" display-align="center">
							<fo:block text-align="right">
								<xsl:value-of select="sum(PersonnelOnSite/pax)"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
				</fo:table-body>
				
			</fo:table>
			
		</fo:block>
													
	</xsl:template>

	<!-- TEMPLATE: PersonnelOnSite -->
	<xsl:template match="PersonnelOnSite">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
		
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="crewPosition"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="crewName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="crewCompany/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="pax"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			
			</xsl:when>
			<xsl:otherwise>
			
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="crewPosition"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="crewName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="crewCompany/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="pax"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			
			</xsl:otherwise>
			
		</xsl:choose>
			
	</xsl:template>

</xsl:stylesheet>