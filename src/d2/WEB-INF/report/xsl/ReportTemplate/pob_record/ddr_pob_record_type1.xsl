<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:import href="./pob_header.xsl"/>
	<xsl:import href="./pob_records.xsl"/>

	<!-- pob section BEGIN -->
	<xsl:template match="modules/PersonnelOnSite">			
		<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt"
			border-left="0.5px solid black" border-right="0.5px solid black" border-bottom="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(30)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm" padding-bottom="-0.05cm" 
						number-columns-spanned="4" background-color="rgb(224,224,224)" border-top="0.5px solid black"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Personnel On Site
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
			</fo:table-header>			
			<fo:table-body font-size="8pt">
				<xsl:call-template name="pob_header"/>
				<xsl:apply-templates select="PersonnelOnSite"/>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3" border-right="0.25px solid black" border-top="0.25px solid black" padding="0.05cm">
						<fo:block text-align="right">Total</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm" border-top="0.25px solid black">
						<fo:block text-align="right">
							<xsl:value-of select="sum(PersonnelOnSite/pax)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<!-- 
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3" padding="0.05cm">
						<fo:block text-align="right">Total Expat</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" 
						 border-left="0.25px solid black">
						<fo:block text-align="right">
							<xsl:value-of select="sum(PersonnelOnSite[nationality='expat']/pax)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3" padding="0.05cm">
						<fo:block text-align="right">Total National</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" 
						 border-left="0.25px solid black">
						<fo:block text-align="right">
							<xsl:value-of select="sum(PersonnelOnSite[nationality='local']/pax)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				 -->
			</fo:table-body>
		</fo:table>											
	</xsl:template>
	<!-- pob section END -->

</xsl:stylesheet>