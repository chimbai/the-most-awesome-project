<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils" >
	
	<xsl:template match="modules/PersonnelOnSite">			
		<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt" border-top="0.5px solid black" border-left="0.5px solid black" border-right="0.5px solid black" space-after="2pt" space-before="2pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(12)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(12)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(8)"/>
			<fo:table-column column-number="8" column-width="proportional-column-width(8)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(10)"/>
			
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell padding="0.07cm" number-columns-spanned="9">
						<fo:block text-align="center" font-weight="bold" font-size="8pt">
							Well Intervention Crew's
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>
			</fo:table-header>			
			<fo:table-body font-size="8pt">
				<fo:table-row>
					<fo:table-cell padding="0.07cm" number-columns-spanned="4" border-bottom="0.5px solid black" border-top="0.5px solid black">
						<fo:block text-align="left" font-weight="bold">Total POB Today: <xsl:value-of select="count(/root/modules/PersonnelOnSite/PersonnelOnSite)"/> PAX</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.07cm" number-columns-spanned="5" border-bottom="0.5px solid black" border-top="0.5px solid black">
						<fo:block text-align="right" font-weight="bold">Total Man-Hours for Well Intervention Crew Until Now: <xsl:value-of select="format-number(sum(/root/modules/PersonnelOnSite/dynaAttr/totalManhoursUntilNow/@rawNumber),'#.##')"/><fo:inline color="white">.</fo:inline><xsl:value-of select="/root/modules/PersonnelOnSite/PersonnelOnSite/totalWorkingHours/@uomSymbol"/></fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
						<fo:block text-align="center" font-weight="bold">No</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
						<fo:block text-align="center" font-weight="bold">Name</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
						<fo:block text-align="center" font-weight="bold">Job Title</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
						<fo:block text-align="center" font-weight="bold">Employer</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
						<fo:block text-align="center" font-weight="bold">In Offshore Date</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
						<fo:block text-align="center" font-weight="bold">Plan Out Offshore Date</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
						<fo:block text-align="center" font-weight="bold">Days on Board</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" border-right="0.5px solid black">
						<fo:block text-align="center" font-weight="bold">Shift Crew</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="0.07cm" >
						<fo:block text-align="center" font-weight="bold">Remark</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:apply-templates select="PersonnelOnSite"/>
			</fo:table-body>
		</fo:table>											
	</xsl:template>

	<xsl:template match="PersonnelOnSite">
		<fo:table-row>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" padding-bottom="0.00cm" border-bottom="0.5px solid black">
				<fo:block text-align="left">
					<xsl:value-of select="sequence"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" padding-bottom="0.00cm" border-bottom="0.5px solid black">
				<fo:block text-align="left">
					<xsl:value-of select="crewName"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding="0.07cm" padding-bottom="0.00cm" border-bottom="0.5px solid black">
				<fo:block text-align="left">
					<xsl:value-of select="crewPosition"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.07cm" padding-right="0.2cm" padding-bottom="0.00cm" border-bottom="0.5px solid black" border-right="0.5px solid black"> 
				<fo:block text-align="left">
					<xsl:value-of select="crewCompany/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.07cm" padding-right="0.2cm" padding-bottom="0.00cm" border-bottom="0.5px solid black" border-right="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(arrivedDate/@epochMS,'dd-MMM-yyyy')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.07cm" padding-right="0.2cm" padding-bottom="0.00cm" border-bottom="0.5px solid black" border-right="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(departedDate/@epochMS,'dd-MMM-yyyy')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.07cm" padding-right="0.2cm" padding-bottom="0.00cm" border-bottom="0.5px solid black" border-right="0.5px solid black">
				<fo:block text-align="center">
					<xsl:value-of select="(((/root/modules/Daily/Daily/dayDate/@epochMS - arrivedDate/@epochMS) div 1000 )div 86400) + 1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.07cm" padding-right="0.2cm" padding-bottom="0.00cm" border-bottom="0.5px solid black" border-right="0.5px solid black">
				<fo:block text-align="left">
					<xsl:value-of select="category/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.07cm" padding-right="0.2cm" padding-bottom="0.00cm" border-bottom="0.5px solid black">
				<fo:block text-align="left">
					<xsl:value-of select="comment"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

</xsl:stylesheet>