<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:import href="./mudvolume_volume_to_hand.xsl"/>
	<xsl:import href="./mudvolume_volume_losses.xsl"/>
	<xsl:import href="./mudvolume_solid_control.xsl"/>
	
	<!-- mudvol section BEGIN -->
	<xsl:template match="modules/MudVolumes">
		<xsl:apply-templates select="MudVolumes"/>
	</xsl:template>
	
	<xsl:template match="MudVolumes">
		<fo:table width="100%" table-layout="fixed" space-after="2pt" font-size="8pt" margin-top="2pt"
			border-left="0.5px solid black" border-right="0.5px solid black" border-bottom="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm" padding-bottom="-0.05cm" background-color="rgb(224,224,224)"
						border-top="0.5px solid black" border-left="0.1px solid black">
						<fo:block font-size="10pt" font-weight="bold">
							Shakers, Volumes and Losses	Data
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm" padding-bottom="-0.05cm" background-color="rgb(224,224,224)"
						border-top="0.5px solid black" border-right="0.1px solid black">
						<fo:block>
							<fo:inline font-weight="bold">Engineer :</fo:inline>
							<xsl:value-of select="engineer"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row>
					<!-- Call Voloume To Hand Template -->
					<xsl:call-template name="volumetohand"/>
					<!-- Call Losses Template -->
					<xsl:call-template name="volumelosses"/>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="2">
						<fo:table width="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt">
							<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(90)"/>
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
										<fo:block text-align="left">
											Comment:
										</fo:block>	
									</fo:table-cell>
									<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
										<fo:block text-align="left" linefeed-treatment="preserve">	
											<xsl:value-of select="comment"/>
										</fo:block>		
									</fo:table-cell>								
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
				<xsl:if test="count(SolidControlEquipment) &gt; 1">
					<xsl:call-template name="solidcontrol"/>
				</xsl:if>
			</fo:table-body>
		</fo:table>											
	</xsl:template>
	<!-- mudvol section END -->
	
</xsl:stylesheet>