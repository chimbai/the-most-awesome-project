<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- Mud Volume Records START -->
	<xsl:template match="MudVolumeDetails">
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:choose>													
						<xsl:when test = "string(label/@lookupLabel)=''">
							<xsl:value-of select="label"/>										
						</xsl:when>													
						<xsl:otherwise>
							<xsl:value-of select="label/@lookupLabel"/>
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="volume"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="volume/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Mud Volume Records END -->
	
</xsl:stylesheet>