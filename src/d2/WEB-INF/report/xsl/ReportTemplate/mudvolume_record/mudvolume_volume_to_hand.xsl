<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:import href="./mudvolume_volume_records.xsl"/>
	
	<!-- Mud Volume To Hand START -->
	<xsl:template name="volumetohand">	
		<fo:table-cell border-right="0.25px solid black">
			<fo:table width="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell border-bottom="0.25px solid black" padding-before="0.05cm"
							padding-start="0.05cm" padding-end="0.05cm" number-columns-spanned="2">
							<fo:block font-weight="bold" text-align="center">											
								Volume to Hand
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black"
							padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
							<fo:block text-align="center">Label</fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="0.25px solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
							<fo:block text-align="center">Volume</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
					<xsl:variable name="mudvolumeID" select="mudVolumesUid"/>
					<xsl:apply-templates select="MudVolumeDetails[type='vol']"/>
					<fo:table-row>									
						<fo:table-cell border-top="0.25px solid black" border-bottom="0.25px solid black" 
							border-right="0.25px solid black" padding-before="0.05cm" padding-start="0.05cm"
							padding-end="0.05cm">
							<fo:block text-align="right">
								Total Volume 
							</fo:block>				
						</fo:table-cell>
						<fo:table-cell border-top="0.25px solid black" border-bottom="0.25px solid black"
							padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
							<fo:block text-align="right">								
								<xsl:value-of select="dynaAttr/totalVolume"/>
							</fo:block>
						</fo:table-cell>																		
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:table-cell>
	</xsl:template>
	<!-- Mud Volume To Hand END -->

</xsl:stylesheet>