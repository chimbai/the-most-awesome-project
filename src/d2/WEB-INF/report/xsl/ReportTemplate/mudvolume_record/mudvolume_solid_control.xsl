<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- Solid Control END -->
	<xsl:template name="solidcontrol">		
		<fo:table-row>
			<fo:table-cell number-columns-spanned="2">						
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt">
					<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(35)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(35)"/>
					<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
					<fo:table-header>
						<fo:table-row>
							<fo:table-cell border-bottom="0.25px solid black" border-top="0.25px solid black" 
								padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm" number-columns-spanned="4">
								<fo:block font-weight="bold" text-align="center"> Solid Control Equipment </fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell border-bottom="0.25px solid black" padding-before="0.05cm"
								padding-start="0.05cm" padding-end="0.05cm">
								<fo:block text-align="center"> Equip. </fo:block>
							</fo:table-cell>
							<fo:table-cell border-bottom="0.25px solid black" padding-before="0.05cm"
								padding-start="0.05cm" padding-end="0.05cm">
								<fo:block text-align="center"> Descr. </fo:block>
							</fo:table-cell>
							<fo:table-cell border-bottom="0.25px solid black" padding-before="0.05cm"
								padding-start="0.05cm" padding-end="0.05cm">
								<fo:block text-align="center"> Mesh Size </fo:block>
							</fo:table-cell>
							<fo:table-cell border-bottom="0.25px solid black" padding-before="0.05cm"
								padding-start="0.05cm" padding-end="0.05cm">
								<fo:block text-align="center"> Hours </fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<xsl:variable name="mudvolumeID" select="mudVolumesUid"/>
						<xsl:apply-templates select="SolidControlEquipment[mudVolumesUid = $mudvolumeID]"/>
					</fo:table-body>
				</fo:table>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template match="SolidControlEquipment">
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left">
					<xsl:value-of select="name/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="left" linefeed-treatment="preserve">
					<xsl:value-of select="description"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="meshDescription"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="hoursUsed"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="hoursUsed/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Solid Control END -->
		
</xsl:stylesheet>