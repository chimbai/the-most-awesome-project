<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TEMPLATE: modules/MudVolumes -->
	<xsl:template match="modules/MudVolumes">
		<xsl:apply-templates select="MudVolumes"/>
	</xsl:template>
	
	<!-- TEMPLATE: MudVolumes -->
	<xsl:template match="MudVolumes">
	
		<fo:block keep-together="always">
		
			<!-- TABLE -->
			<fo:table inline-progression-dimension="100%" table-layout="fixed" wrap-option="wrap" space-after="2pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(25)"/>
				
				<!-- HEADER -->
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell padding="2px" background-color="#DDDDDD" display-align="center"
							border-left="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
							<fo:block font-size="8pt" font-weight="bold">
								Shakers, Volumes and Losses Data
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="2" padding="2px" background-color="#DDDDDD" display-align="center"
							border-right="thin solid black" border-top="thin solid black" border-bottom="thin solid black">
							<fo:block font-size="8pt" font-weight="bold" text-align="right">
								Engineer: <xsl:value-of select="engineer"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				
				<!-- BODY -->
				<fo:table-body>
					
					<fo:table-row>
					
						<!-- COLUMN 1: SOLID EQUIPMENT -->
						<fo:table-cell border-left="thin solid black" border-right="thin solid black">
							<xsl:variable name="mudvolumeID" select="mudVolumesUid"/>
							<xsl:choose>
								<xsl:when test="count(SolidControlEquipment[mudVolumesUid = $mudvolumeID]) = '0'">
									<fo:table width="100%" table-layout="fixed">
										<fo:table-column column-number="1" column-width="proportional-column-width(28)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(52)"/>
										<fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
										<fo:table-header>
											<fo:table-row>
												<fo:table-cell padding="2px" background-color="#DDDDDD"
													 border-bottom="thin solid black">
													<fo:block font-weight="bold" text-align="center">
														Equipment
													</fo:block>					
												</fo:table-cell>
												<fo:table-cell padding="2px" background-color="#DDDDDD"  border-bottom="thin solid black">
													<fo:block font-weight="bold" text-align="center">
														Description
													</fo:block>										
												</fo:table-cell>
												<fo:table-cell padding="2px" background-color="#DDDDDD"
													 border-bottom="thin solid black">
													<fo:block font-weight="bold" text-align="center">
														Mesh Size
													</fo:block>										
												</fo:table-cell>
											</fo:table-row>
										</fo:table-header>
										<fo:table-body>
											<xsl:variable name="mudvolumeID" select="mudVolumesUid"/>
											<xsl:apply-templates select="SolidControlEquipment[mudVolumesUid = $mudvolumeID]"/>	
										</fo:table-body>
									</fo:table>
								</xsl:when>
								<xsl:otherwise>
									<fo:table width="100%" table-layout="fixed" border-bottom="thin solid black">
										<fo:table-column column-number="1" column-width="proportional-column-width(28)"/>
										<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
										<fo:table-column column-number="3" column-width="proportional-column-width(22)"/>
										<fo:table-header>
											<fo:table-row>
												<fo:table-cell padding="2px" background-color="#DDDDDD"
													 border-bottom="thin solid black">
													<fo:block font-weight="bold" text-align="center">
														Equipment
													</fo:block>					
												</fo:table-cell>
												<fo:table-cell padding="2px" background-color="#DDDDDD"  border-bottom="thin solid black">
													<fo:block font-weight="bold" text-align="center">
														Description
													</fo:block>										
												</fo:table-cell>
												<fo:table-cell padding="2px" background-color="#DDDDDD"
													 border-bottom="thin solid black">
													<fo:block font-weight="bold" text-align="center">
														Mesh Size
													</fo:block>										
												</fo:table-cell>
											</fo:table-row>
										</fo:table-header>
										<fo:table-body>
											<xsl:variable name="mudvolumeID" select="mudVolumesUid"/>
											<xsl:apply-templates select="SolidControlEquipment[mudVolumesUid = $mudvolumeID]"/>	
										</fo:table-body>
									</fo:table>
								</xsl:otherwise>
							</xsl:choose>
						</fo:table-cell>
						
						<!-- COLUMN 2: VOLUME IN HAND -->
						<fo:table-cell border-right="thin solid black">
							<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-number="1" column-width="proportional-column-width(35)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(65)"/>
								
								<fo:table-body>
								
									<fo:table-row>
										<fo:table-cell padding="2px" padding-left="2px" padding-right="2px" border-bottom="thin solid black">
											<fo:block font-weight="bold">
												Available
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="2px" padding-left="2px" padding-right="2px" border-bottom="thin solid black">
											<fo:block text-align="right">
												<xsl:value-of select="format-number(dynaAttr/totalVolume/@rawNumber,'#,##0')"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="dynaAttr/totalVolume/@uomSymbol"/>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Active
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='vol' and label='vol_active']">
													<xsl:value-of select="sum(MudVolumeDetails[type='vol' and label='vol_active']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='vol' and label='vol_active']/volume/@uomSymbol"/>
												</xsl:if>
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Mixing
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='vol' and label='vol_mixing']">
													<xsl:value-of select="sum(MudVolumeDetails[type='vol' and label='vol_mixing']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='vol' and label='vol_mixing']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Hole
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='vol' and label='vol_hole']">
													<xsl:value-of select="sum(MudVolumeDetails[type='vol' and label='vol_hole']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='vol' and label='vol_hole']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Slug
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='vol' and label='vol_slug']">
													<xsl:value-of select="sum(MudVolumeDetails[type='vol' and label='vol_slug']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='vol' and label='vol_slug']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Reserve
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='vol' and label='vol_reserve']">
													<xsl:value-of select="sum(MudVolumeDetails[type='vol' and label='vol_reserve']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='vol' and label='vol_reserve']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Kill
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='vol' and label='vol_kill']">
													<xsl:value-of select="sum(MudVolumeDetails[type='vol' and label='vol_kill']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='vol' and label='vol_kill']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Others
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='vol' and label='vol_other']">
													<xsl:value-of select="sum(MudVolumeDetails[type='vol' and label='vol_other']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='vol' and label='vol_other']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>
								
							</fo:table>
						</fo:table-cell>
						
						<!-- COLUMN 3: LOSSES -->
						<fo:table-cell border-right="thin solid black">
							<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-number="1" column-width="proportional-column-width(55)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(45)"/>
								
								<fo:table-body>
								
									<fo:table-row>
										<fo:table-cell padding="2px" padding-left="2px" padding-right="2px" border-bottom="thin solid black">
											<fo:block font-weight="bold">
												Losses
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-bottom="thin solid black">
											<fo:block text-align="right">
												<xsl:value-of select="format-number(dynaAttr/totalLosses/@rawNumber,'#,##0')"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="dynaAttr/totalLosses/@uomSymbol"/>
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Downhole
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='loss' and label='loss_dh']">
													<xsl:value-of select="sum(MudVolumeDetails[type='loss' and label='loss_dh']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='loss' and label='loss_dh']/volume/@uomSymbol"/>
												</xsl:if>
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Surf. + Equip.
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='loss' and (label='loss_surface' or label='loss_equipment')]">
													<xsl:value-of select="sum(MudVolumeDetails[type='loss' and label='loss_surface']/volume/@rawNumber) + sum(MudVolumeDetails[type='loss' and label='loss_equipment']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='loss' and (label='loss_surface' or label='loss_equipment')]/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Dumped
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='loss' and label='loss_dumped']">
													<xsl:value-of select="sum(MudVolumeDetails[type='loss' and label='loss_dumped']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='loss' and label='loss_dumped']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												De-Gasser	
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='loss' and label='loss_degasser']">
													<xsl:value-of select="sum(MudVolumeDetails[type='loss' and label='loss_degasser']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='loss' and label='loss_degasser']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row> 
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												De-Sander
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='loss' and label='loss_desander']">
													<xsl:value-of select="sum(MudVolumeDetails[type='loss' and label='loss_desander']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='loss' and label='loss_desander']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												De-Silter
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='loss' and label='loss_desilter']">
													<xsl:value-of select="sum(MudVolumeDetails[type='loss' and label='loss_desilter']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='loss' and label='loss_desilter']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Centrifuge
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='loss' and label='loss_centrifuge']">
													<xsl:value-of select="sum(MudVolumeDetails[type='loss' and label='loss_centrifuge']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='loss' and label='loss_centrifuge']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Cuttings
											</fo:block>					
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">
												<xsl:if test="MudVolumeDetails[type='loss' and label='loss_other']">
													<xsl:value-of select="sum(MudVolumeDetails[type='loss' and label='loss_other']/volume/@rawNumber)"/>	
													<fo:inline color="white">.</fo:inline>
													<xsl:value-of select="MudVolumeDetails[type='loss' and label='loss_other']/volume/@uomSymbol"/>
												</xsl:if>	
											</fo:block>										
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>
								
							</fo:table>
						</fo:table-cell>
									
					</fo:table-row>
					
					<!-- COMMENTS -->
					<fo:table-row>
						<fo:table-cell number-columns-spanned="3" border="thin solid black">
							<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
								<fo:table-column column-number="1" column-width="proportional-column-width(8)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(92)"/>
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block font-weight="bold">
												Comment:
											</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block linefeed-treatment="preserve">
												<xsl:value-of select="comment"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>							
						</fo:table-cell>							
					</fo:table-row>
					
				</fo:table-body>
				
			</fo:table>
			
		</fo:block>
		
	</xsl:template>
	
	<!-- TEMPLATE: SolidControlEquipment -->
	<xsl:template match="SolidControlEquipment">
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"  border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="name/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="description"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" >
						<fo:block text-align="right" linefeed-treatment="preserve">
							<xsl:value-of select="meshDescription"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px"  border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="name/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="description"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" >
						<fo:block text-align="right" linefeed-treatment="preserve">
							<xsl:value-of select="meshDescription"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- TEMPLATE: MudVolumeDetails -->
	<xsl:template match="MudVolumeDetails">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="label/@lookupLabel"/>					
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="volume"/>
							<fo:inline color="white">.</fo:inline>
							<xsl:value-of select="volume/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="label/@lookupLabel"/>					
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="volume"/>
							<fo:inline color="#EEEEEE">.</fo:inline>
							<xsl:value-of select="volume/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
</xsl:stylesheet>