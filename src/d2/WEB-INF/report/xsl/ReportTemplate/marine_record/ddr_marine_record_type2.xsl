<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- NEW XSL FOR D2_TALISMAN_KL MARINE SECTION 20080925 -->
	
	<!-- TEMPLATE: modules/MarineProperties/MarineProperties -->
	<xsl:template match="modules/MarineProperties/MarineProperties">
	
		<fo:block keep-together="always">
		
			<!-- TABLE -->
			<fo:table width="100%" table-layout="fixed" wrap-option="wrap" space-after="2pt">
				<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
				
				<!-- HEADER -->
				<fo:table-header>	
					<fo:table-row>
						<fo:table-cell number-columns-spanned="2" padding="1px" background-color="#DDDDDD" display-align="center" border="thin solid black">
							<fo:block font-size="10pt" font-weight="bold">
								Marine
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>																								
				
				<!-- BODY -->
				<fo:table-body>
				
					<!-- DATA -->
					<fo:table-row font-size="8pt">
					
						<!-- COLUMN 1 -->
						<fo:table-cell border-left="thin solid black" border-right="thin solid black">
							<fo:table width="100%" table-layout="fixed">
	                           	<fo:table-column column-number="1" column-width="proportional-column-width(35)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(65)"/>
								<fo:table-body>
																	
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Report Time:
											</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">																			
												<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportDatetime/@epochMS,'HH:mm')"/>																					
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
																	
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Riser Tension:
											</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">																			
												<xsl:value-of select="riserTension"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="riserTension/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">							
											<fo:block>
												Var. Deck Load:
											</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">																			
												<xsl:value-of select="variableDeckLoad"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="variableDeckLoad/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Heave Travel:
											</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">																			
												<xsl:value-of select="heaveTravel"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="heaveTravel/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Roll Angle:
											</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">																			
												<xsl:value-of select="rollAngle"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="rollAngle/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block>
												Pitch Angle:
											</fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block text-align="right">																			
												<xsl:value-of select="pitchAngle"/>
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="pitchAngle/@uomSymbol"/>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
						
						<!-- COLUMN 2: RIG SUPPORT -->
						<fo:table-cell padding="2pt" border-right="thin solid black">
							<xsl:variable name="marinePropertiesUid" select="marinePropertiesUid"/>
	                       	<xsl:if test="count(MarineSupport[marinePropertiesUid=$marinePropertiesUid]) &gt; 0">
								<xsl:choose>
									<xsl:when test="/root/modules/RigInformation/RigInformation/rigSubType='jackup'">
										<xsl:call-template name="MarineSupport2">
											<xsl:with-param name="marinePropertiesUid" select="marinePropertiesUid"/>
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="MarineSupport">
											<xsl:with-param name="marinePropertiesUid" select="marinePropertiesUid"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:if>
						</fo:table-cell>
					</fo:table-row>
					
					<!-- COMMENTS -->
					<fo:table-row>
						<fo:table-cell number-columns-spanned="2" border="thin solid black">
							<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
								<fo:table-column column-number="1" column-width="proportional-column-width(8)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(92)"/>																																
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block font-weight="bold">
												Comment:
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
											<fo:block linefeed-treatment="preserve">
												<xsl:value-of select="comment"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>																		
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
					
		</fo:block>
		
	</xsl:template>
	
	<!-- TEMPLATE: MarineSupport2 -->
	<xsl:template name="MarineSupport">
	
		<xsl:param name="marinePropertiesUid"/>
		
		<xsl:choose>
			<xsl:when test="count(/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]) = '0'">
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
					<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
					<fo:table-header>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="2" padding="1px" background-color="#DDDDDD" border="thin solid black" display-align="center">
								<fo:block font-weight="bold" text-align="center">
									Rig Support
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" display-align="center"
								border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
								<fo:block text-align="center">
									Anchors
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center"> 
									Tension 
									(<xsl:value-of select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]/anchorTension/@uomSymbol"/>)
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<xsl:apply-templates select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]"/>
					</fo:table-body>
				</fo:table>
			</xsl:when>
			<xsl:otherwise>
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black">
					<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
					<fo:table-header>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="2" padding="1px" background-color="#DDDDDD" border="thin solid black" display-align="center">
								<fo:block font-weight="bold" text-align="center">
									Rig Support
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" display-align="center"
								border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
								<fo:block text-align="center">
									Anchors
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center"> 
									Tension 
									(<xsl:value-of select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]/anchorTension/@uomSymbol"/>)
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<xsl:apply-templates select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]"/>
					</fo:table-body>
				</fo:table>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<!-- TEMPLATE: MarineSupport2 -->
	<xsl:template name="MarineSupport2">
		<xsl:param name="marinePropertiesUid"/>
		
		<xsl:choose>
			<xsl:when test="count(/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]) = '0'">
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap">
					<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(33)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(34)"/>
							
					<fo:table-header>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="3" padding="1px" background-color="#DDDDDD" border="thin solid black" display-align="center">
								<fo:block font-weight="bold" text-align="center">
									Rig Support
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" display-align="center"
								border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
								<fo:block text-align="center">
									Legs
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center"> 
									Weight On Leg 
									(<xsl:value-of select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]/weightOnLeg/@uomSymbol"/>)
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center"> 
									Penetration
									(<xsl:value-of select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]/penetration/@uomSymbol"/>)
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<xsl:apply-templates select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]"/>
					</fo:table-body>
				</fo:table>
			</xsl:when>
			<xsl:otherwise>
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black">
					<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(33)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(34)"/>
							
					<fo:table-header>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="3" padding="1px" background-color="#DDDDDD" border="thin solid black" display-align="center">
								<fo:block font-weight="bold" text-align="center">
									Rig Support
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" display-align="center"
								border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
								<fo:block text-align="center">
									Legs
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center"> 
									Weight On Leg 
									(<xsl:value-of select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]/weightOnLeg/@uomSymbol"/>)
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black" border-bottom="thin solid black" display-align="center">
								<fo:block text-align="center"> 
									Penetration
									(<xsl:value-of select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]/penetration/@uomSymbol"/>)
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<xsl:apply-templates select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]"/>
					</fo:table-body>
				</fo:table>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- TEMPLATE: MarineSupport -->
	<xsl:template match="MarineSupport">
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<xsl:choose>
					<xsl:when test="/root/modules/RigInformation/RigInformation/rigSubType='jackup'">
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
								<fo:block text-align="center">
									<xsl:choose>													
										<xsl:when test="string(supportnum/@lookupLabel)=''">
											<xsl:value-of select="supportnum"/>										
										</xsl:when>													
										<xsl:otherwise>
											<xsl:value-of select="supportnum/@lookupLabel"/>
										</xsl:otherwise>
									</xsl:choose>					
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
								<fo:block text-align="center">
									<xsl:value-of select="weightOnLeg"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
								<fo:block text-align="center">
									<xsl:value-of select="penetration"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:when>
					<xsl:otherwise>
						<fo:table-row>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
								<fo:block text-align="center">
									<xsl:choose>													
										<xsl:when test="string(supportnum/@lookupLabel)=''">
											<xsl:value-of select="supportnum"/>										
										</xsl:when>													
										<xsl:otherwise>
											<xsl:value-of select="supportnum/@lookupLabel"/>
										</xsl:otherwise>
									</xsl:choose>					
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
								<fo:block text-align="center">
									<xsl:value-of select="anchorTension"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="/root/modules/RigInformation/RigInformation/rigSubType='jackup'">
						<fo:table-row background-color="#EEEEEE">
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
								<fo:block text-align="center">
									<xsl:choose>													
										<xsl:when test="string(supportnum/@lookupLabel)=''">
											<xsl:value-of select="supportnum"/>										
										</xsl:when>													
										<xsl:otherwise>
											<xsl:value-of select="supportnum/@lookupLabel"/>
										</xsl:otherwise>
									</xsl:choose>					
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
								<fo:block text-align="center">
									<xsl:value-of select="weightOnLeg"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
								<fo:block text-align="center">
									<xsl:value-of select="penetration"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:when>
					<xsl:otherwise>
						<fo:table-row background-color="#EEEEEE">
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
								<fo:block text-align="center">
									<xsl:choose>													
										<xsl:when test="string(supportnum/@lookupLabel)=''">
											<xsl:value-of select="supportnum"/>										
										</xsl:when>													
										<xsl:otherwise>
											<xsl:value-of select="supportnum/@lookupLabel"/>
										</xsl:otherwise>
									</xsl:choose>					
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
								<fo:block text-align="center">
									<xsl:value-of select="anchorTension"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>	
	
	<!-- TEMPLATE: modules/SupportVesselParam -->
	<xsl:template match="modules/SupportVesselParam">
		<xsl:if test="SupportVesselParam/transporttype='boat'">
			<fo:table width="100%" table-layout="fixed" border="thin solid black">
				<fo:table-column column-number="1" column-width="proportional-column-width(16)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(14)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(14)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(16)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(40)"/>
				<!--
				<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(45)"/>
				-->
				
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell number-columns-spanned="5" padding="2px" background-color="#DDDDDD"
							border-bottom="thin solid black">
							<fo:block font-size="10pt" font-weight="bold">
								Support Vessels
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>
						<fo:table-cell padding="2px" display-align="center" border-bottom="thin solid black">
							<fo:block text-align="center">
								Boats
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" display-align="center" border-bottom="thin solid black">
							<fo:block text-align="center">
								Arrived
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" display-align="center" border-bottom="thin solid black">
							<fo:block text-align="center">
								Departed
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" display-align="center" border-bottom="thin solid black">
							<fo:block text-align="center">
								Comment
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="2px" display-align="center" border-bottom="thin solid black">
							<fo:block text-align="center">
								Bulks
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				
				<fo:table-body>
					<xsl:apply-templates select="SupportVesselParam[transporttype='boat']" mode="boat">
						<xsl:sort select="sequence/@rawNumber" data-type="number"/>
					</xsl:apply-templates>
				</fo:table-body>
				
			</fo:table>	
		</xsl:if>
	</xsl:template>
	
	<!-- TEMPLATE: modules/SupportVesselParam/SupportVesselParam -->
	<xsl:template match="modules/SupportVesselParam/SupportVesselParam" mode="boat">
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="supportVesselInformationUid/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(arrdt/@epochMS,'dd MMM yyyy HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(depdt/@epochMS,'dd MMM yyyy HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="activitySummary"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block linefeed-treatment="preserve">
						
							<fo:table inline-progression-dimension="100%" table-layout="fixed" font-size="7pt" border="thin solid black">
								<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="6" column-width="proportional-column-width(15)"/>
								<fo:table-header>
									<fo:table-row>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Item
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Unit
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Start Amount
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Previus Balance
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Used
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Balance
										</fo:block>
									</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								<fo:table-body>
									<!--
									<xsl:apply-templates select="RigStock" mode="support_vessel"/>
									-->
								</fo:table-body>
							</fo:table>
							
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="supportVesselInformationUid/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(arrdt/@epochMS,'dd MMM yyyy HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(depdt/@epochMS,'dd MMM yyyy HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="activitySummary"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block linefeed-treatment="preserve">
						
							<fo:table inline-progression-dimension="100%" table-layout="fixed" font-size="7pt" border="thin solid black">
								<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="5" column-width="proportional-column-width(15)"/>
								<fo:table-column column-number="6" column-width="proportional-column-width(15)"/>
								<fo:table-header>
									<fo:table-row>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Item
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Unit
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Start Amount
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Previus Balance
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Used
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-bottom="thin solid black" padding="1px">
										<fo:block text-align="center" font-weight="bold" font-size="7">
											Balance
										</fo:block>
									</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								<fo:table-body>
									<xsl:apply-templates select="RigStock" mode="support_vessel"/>
								</fo:table-body>
							</fo:table>
							
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>