<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- D2_OMV_GLOBAL MARINE -->

	<!-- MARINE SECTION BEGIN -->
	<xsl:template match="modules/MarineProperties/MarineProperties">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" margin-top="2pt" wrap-option="wrap">
			<fo:table-column column-number="1" column-width="proportional-column-width(70)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(30)"/>
			<fo:table-body>
				
				<!-- MARINE MAIN DATA -->
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" border="0.25px solid black">
								<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								
								<fo:table-header>	
									<fo:table-row>
										<fo:table-cell number-columns-spanned="2" padding="2px" border-bottom="0.5px solid black">
											<fo:block text-align="left" font-weight="bold" font-size="9pt">
												Marine
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
																																
								<fo:table-body>
									<fo:table-row font-size="8pt">
										<fo:table-cell border-right="0.5px solid black">
											<fo:block>
					                        	<fo:table width="100%" table-layout="fixed">
					                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
													<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
													<fo:table-body>									
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Report Time: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportDatetime/@epochMS,'HH:mm')"/>																					
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Vessel Direction: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="vesselDirection"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="vesselDirection/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Riser Tension: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="riserTension"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="riserTension/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Var. Deck Load: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="variableDeckLoad"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="variableDeckLoad/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Static Offset Distance: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="staticOffsetDistance"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="staticOffsetDistance/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Static Offset Angle: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="staticOffsetAngle"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="staticOffsetAngle/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
													</fo:table-body>
												</fo:table>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
					                        	<fo:table width="100%" table-layout="fixed">
													<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
													<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
													<fo:table-body>									
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Heave Travel: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="heaveTravel"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="heaveTravel/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Heave Frequency: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="heaveFrequency"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="heaveFrequency/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Roll Angle: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="rollAngle"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="rollAngle/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Roll Frequency: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="rollFrequency"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="rollFrequency/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Pitch Angle: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="pitchAngle"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="pitchAngle/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>
														<fo:table-row>
															<fo:table-cell padding="2px">																
																<fo:block text-align="left"> Pitch Frequency: </fo:block>
															</fo:table-cell>				
															<fo:table-cell padding="2px">	
																<fo:block text-align="right">																			
																	<xsl:value-of select="pitchFrequency"/><fo:inline color="white">.</fo:inline>
																	<xsl:value-of select="pitchFrequency/@uomSymbol"/>
																</fo:block>																
															</fo:table-cell>
														</fo:table-row>									
													</fo:table-body>
												</fo:table>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									
									<fo:table-row>
										<fo:table-cell number-columns-spanned="2" border-top="0.5px solid black">
											<fo:table inline-progression-dimension="100%" table-layout="fixed" table-omit-header-at-break="true" wrap-option="wrap">
												<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
												<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>																																
												<fo:table-body>		
													<fo:table-row>
														<fo:table-cell padding="2px">
															<fo:block text-align="left" font-weight="bold"> Marine Comment: </fo:block>
														</fo:table-cell>
														<fo:table-cell padding="2px">
															<fo:block text-align="left" linefeed-treatment="preserve">
																<xsl:value-of select="comment"/>
															</fo:block>
														</fo:table-cell>
													</fo:table-row>																		
												</fo:table-body>
											</fo:table>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>			
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<!-- MARINE SUPPORT -->
							<xsl:variable name="marinePropertiesUid" select="marinePropertiesUid"/>
							<xsl:if test="count(MarineSupport) &gt; 0">
								<xsl:call-template name="MarineSupport">
									<xsl:with-param name="marinePropertiesUid" select="marinePropertiesUid"/>								
								</xsl:call-template>
							</xsl:if>
							<!-- END OF MARINE SUPPORT -->
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<!-- END OF MARINE MAIN DATA -->							
			</fo:table-body>
		</fo:table>				                                        
	</xsl:template>
		
	<xsl:template name="MarineSupport">
		<xsl:param name="marinePropertiesUid"/>
		<fo:table width="100%" table-layout="fixed" border="0.25px solid black" font-size="7pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(40)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(60)"/>			
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px" number-columns-spanned="2">
						<fo:block font-weight="bold" text-align="left" font-size="9pt"> Marine Support </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding="2px">
						<fo:block text-align="center"> Seq </fo:block>
					</fo:table-cell>										
					<fo:table-cell padding="2px">
						<fo:block text-align="center"> Anchor Tension </fo:block>
					</fo:table-cell>										
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]"/> -->
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="MarineSupport">
		<fo:table-row>
			<fo:table-cell padding="1px">
				<fo:block text-align="center">					
					<xsl:value-of select="supportnum"/>
				</fo:block>
			</fo:table-cell>						
			<fo:table-cell padding="1px">
				<fo:block text-align="center">
					<xsl:value-of select="anchorTension"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="anchorTension/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>		
		</fo:table-row>
	</xsl:template>	
	<!-- marine section END -->

</xsl:stylesheet>