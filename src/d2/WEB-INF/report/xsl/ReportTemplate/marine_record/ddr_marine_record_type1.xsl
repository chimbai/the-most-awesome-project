<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- marine section BEGIN -->
	<xsl:template match="modules/MarineProperties/MarineProperties">
		<fo:table width="100%" table-layout="fixed" font-size="8pt" margin-top="2pt"
			wrap-option="wrap" space-after="2pt" border-left="thin solid black"
			border-right="thin solid black" border-bottom="thin solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(33)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(34)"/>
			<fo:table-header>	
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3" padding="2px" background-color="#DDDDDD" border-top="thin solid black" border-bottom="thin solid black"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="8pt">
							Marine
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>																								
			<fo:table-body>
				<fo:table-row font-size="8pt">
					<fo:table-cell border-right="thin solid black"
						border-bottom="thin solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
                            	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								<fo:table-body>									
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Report Time: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">																			
												<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportDatetime/@epochMS,'HH:mm')"/>																					
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Riser Tension: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">
												<xsl:choose>
													<xsl:when test="(riserTension = '') or (riserTension = 'null') ">
													</xsl:when>
	                       							<xsl:otherwise>																			
														<xsl:value-of select="riserTension"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="riserTension/@uomSymbol"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Vessel Dir.: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">
												<xsl:choose>
													<xsl:when test="(vesselDirection = '') or (vesselDirection = 'null') ">
													</xsl:when>
	                       							<xsl:otherwise>																			
														<xsl:value-of select="vesselDirection"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="vesselDirection/@uomSymbol"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Var. Deck Load: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">
												<xsl:choose>
													<xsl:when test="(variableDeckLoad = '') or (variableDeckLoad = 'null') ">
													</xsl:when>
	                       							<xsl:otherwise>																			
														<xsl:value-of select="variableDeckLoad"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="variableDeckLoad/@uomSymbol"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="thin solid black" border-right="thin solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								<fo:table-body>	
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Roll Angle: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">	
												<xsl:choose>
													<xsl:when test="(rollAngle = '') or (rollAngle = 'null') ">
													</xsl:when>
	                       							<xsl:otherwise>																		
														<xsl:value-of select="rollAngle"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="rollAngle/@uomSymbol"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>	
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Pitch Angle: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">	
												<xsl:choose>
													<xsl:when test="(pitchAngle = '') or (pitchAngle = 'null') ">
													</xsl:when>
	                       							<xsl:otherwise>																		
														<xsl:value-of select="pitchAngle"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="pitchAngle/@uomSymbol"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>							
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Heave Travel: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">	
												<xsl:choose>
													<xsl:when test="(heaveTravel = '') or (heaveTravel = 'null') ">
													</xsl:when>
	                       							<xsl:otherwise>																			
														<xsl:value-of select="heaveTravel"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="heaveTravel/@uomSymbol"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Static Offset Angle: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">	
												<xsl:choose>
													<xsl:when test="(staticOffsetAngle = '') or (staticOffsetAngle = 'null') ">
													</xsl:when>
	                       							<xsl:otherwise>																
														<xsl:value-of select="staticOffsetAngle"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="staticOffsetAngle/@uomSymbol"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>						
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="thin solid black">
						<fo:block>
                        	<fo:table width="100%" table-layout="fixed">
								<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
								<fo:table-body>									
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Roll Freq.: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">
												<xsl:choose>
													<xsl:when test="(rollFrequency = '') or (rollFrequency = 'null') ">
													</xsl:when>
	                       							<xsl:otherwise>																				
														<xsl:value-of select="rollFrequency"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="rollFrequency/@uomSymbol"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Pitch Freq.: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">
												<xsl:choose>
													<xsl:when test="(pitchFrequency = '') or (pitchFrequency = 'null') ">
													</xsl:when>
	                       							<xsl:otherwise>																				
														<xsl:value-of select="pitchFrequency"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="pitchFrequency/@uomSymbol"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>	
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Heave Freq.: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">
												<xsl:choose>
													<xsl:when test="(heaveFrequency = '') or (heaveFrequency = 'null') ">
													</xsl:when>
	                       							<xsl:otherwise>																			
														<xsl:value-of select="heaveFrequency"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="heaveFrequency/@uomSymbol"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell padding="0.05cm">																
											<fo:block text-align="left"> Static Offset Distance: </fo:block>
										</fo:table-cell>				
										<fo:table-cell padding="0.05cm">	
											<fo:block text-align="right">	
												<xsl:choose>
													<xsl:when test="(staticOffsetDistance = '') or (staticOffsetDistance = 'null') ">
													</xsl:when>
	                       							<xsl:otherwise>																			
														<xsl:value-of select="staticOffsetDistance"/><fo:inline color="white">.</fo:inline>
														<xsl:value-of select="staticOffsetDistance/@uomSymbol"/>
													</xsl:otherwise>
												</xsl:choose>
											</fo:block>																
										</fo:table-cell>
									</fo:table-row>								
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3">
						<fo:table inline-progression-dimension="100%" table-layout="fixed"
							table-omit-header-at-break="true" wrap-option="wrap">
							<fo:table-column column-number="1" column-width="proportional-column-width(18)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(82)"/>																																
							<fo:table-body>		
								<fo:table-row>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left" font-weight="bold">Comment: </fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.05cm">
										<fo:block text-align="left" linefeed-treatment="preserve">
											<xsl:value-of select="comment"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>																		
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="3">
						<fo:block>	
							<xsl:variable name="marinePropertiesUid" select="marinePropertiesUid"/>
							<xsl:if test="count(MarineSupport) &gt; 0">
								<xsl:call-template name="MarineSupport">
									<xsl:with-param name="marinePropertiesUid" select="marinePropertiesUid"/>
								</xsl:call-template>
							</xsl:if>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>                                            
	</xsl:template>
	
	<xsl:template name="MarineSupport">
	<xsl:param name="marinePropertiesUid"/>
	
	<fo:table width="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt" space-after="5pt">
	<xsl:variable name="size" select="count(marinePropertiesUid)"/>
		<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
		<fo:table-header>
			<fo:table-row>
				<fo:table-cell border-bottom="thin solid black" border-top="thin solid black"  background-color="#DDDDDD"
					padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm" number-columns-spanned="1">
					<fo:block font-weight="bold" text-align="left"> Anchors </fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-header>
		<fo:table-body>
		
            <fo:table-row>
            	<fo:table-cell>
            		<fo:table width="100%" table-layout="fixed" wrap-option="wrap" space-after="5pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(16)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(17)"/>
						<fo:table-column column-number="3" column-width="proportional-column-width(17)"/>
						<fo:table-column column-number="4" column-width="proportional-column-width(16)"/>
						<fo:table-column column-number="5" column-width="proportional-column-width(17)"/>
						<fo:table-column column-number="6" column-width="proportional-column-width(17)"/>
						<fo:table-header>
							<fo:table-row>
								<fo:table-cell border-bottom="thin solid black" padding-before="0.05cm"
									padding-start="0.05cm" padding-end="0.05cm">
									<fo:block text-align="center"> Seq.# / Name </fo:block>
								</fo:table-cell>
								<fo:table-cell border-bottom="thin solid black" padding-before="0.05cm"
									padding-start="0.05cm" padding-end="0.05cm">
									<fo:block text-align="center"> Tension 
									<fo:inline color="white">.</fo:inline>
									(<xsl:value-of select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]/anchorTension/@uomSymbol"/>)
									</fo:block>
								</fo:table-cell>
								<fo:table-cell border-bottom="thin solid black" border-right="thin solid black"  padding-before="0.05cm"
									padding-start="0.05cm" padding-end="0.05cm" >
									<fo:block text-align="center"> Tension Max 
									<fo:inline color="white">.</fo:inline>
									(<xsl:value-of select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]/anchorTension/@uomSymbol"/>)
									</fo:block>
								</fo:table-cell>
								<fo:table-cell border-bottom="thin solid black" padding-before="0.05cm"
									padding-start="0.05cm" padding-end="0.05cm">
									<fo:block text-align="center"> Seq.# / Name </fo:block>
								</fo:table-cell>
								<fo:table-cell border-bottom="thin solid black" padding-before="0.05cm"
									padding-start="0.05cm" padding-end="0.05cm">
									<fo:block text-align="center"> Tension 
									(<xsl:value-of select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]/anchorTension/@uomSymbol"/>)
									</fo:block>
								</fo:table-cell>
								<fo:table-cell border-bottom="thin solid black" padding-before="0.05cm"
									padding-start="0.05cm" padding-end="0.05cm">
									<fo:block text-align="center"> Tension Max
									(<xsl:value-of select="/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid]/anchorTension/@uomSymbol"/>)
									</fo:block>
								</fo:table-cell>					
							</fo:table-row>
						</fo:table-header>
						<fo:table-body>
							<xsl:variable name="size" select="count(/root/modules/MarineProperties/MarineProperties/MarineSupport[marinePropertiesUid=$marinePropertiesUid])"/>
							<xsl:for-each select="/root/modules/MarineProperties/MarineProperties/MarineSupport[ceiling($size div 2) &gt;= position()]">
							<xsl:choose>
								<xsl:when test="position() mod 2 != '0'">
									<fo:table-row>
										<fo:table-cell border-right="thin solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:choose>													
													<xsl:when test = "string(supportnum/@lookupLabel)=''">
														<xsl:value-of select="supportnum"/>										
													</xsl:when>													
													<xsl:otherwise>
														<xsl:value-of select="supportnum/@lookupLabel"/>
													</xsl:otherwise>
												</xsl:choose>					
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="thin solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:value-of select="anchorTension"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="thin solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:value-of select="anchorTensionMax"/>
											</fo:block>
										</fo:table-cell>
										
										<fo:table-cell border-right="thin solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:choose>													
													<xsl:when test = "string(following::MarineSupport[ceiling($size div 2)]/supportnum/@lookupLabel)=''">
														<xsl:value-of select="following::MarineSupport[ceiling($size div 2)]/supportnum"/>										
													</xsl:when>													
													<xsl:otherwise>
														<xsl:value-of select="following::MarineSupport[ceiling($size div 2)]/supportnum/@lookupLabel"/>
													</xsl:otherwise>
												</xsl:choose>					
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="thin solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:value-of select="following::MarineSupport[ceiling($size div 2)]/anchorTension"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:value-of select="following::MarineSupport[ceiling($size div 2)]/anchorTensionMax"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:when>
								<xsl:otherwise>
									<fo:table-row background-color="#EEEEEE">
										<fo:table-cell border-right="thin solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:choose>													
													<xsl:when test = "string(supportnum/@lookupLabel)=''">
														<xsl:value-of select="supportnum"/>										
													</xsl:when>													
													<xsl:otherwise>
														<xsl:value-of select="supportnum/@lookupLabel"/>
													</xsl:otherwise>
												</xsl:choose>					
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="thin solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:value-of select="anchorTension"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-right="thin solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:value-of select="anchorTensionMax"/>
											</fo:block>
										</fo:table-cell>
										
										<fo:table-cell border-right="thin solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:choose>													
													<xsl:when test = "string(following::MarineSupport[ceiling($size div 2)]/supportnum/@lookupLabel)=''">
														<xsl:value-of select="following::MarineSupport[ceiling($size div 2)]/supportnum"/>										
													</xsl:when>													
													<xsl:otherwise>
														<xsl:value-of select="following::MarineSupport[ceiling($size div 2)]/supportnum/@lookupLabel"/>
													</xsl:otherwise>
												</xsl:choose>					
											</fo:block>
										</fo:table-cell>
										<fo:table-cell  border-right="thin solid black" padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:value-of select="following::MarineSupport[ceiling($size div 2)]/anchorTension"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell  padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
											<fo:block text-align="center">
												<xsl:value-of select="following::MarineSupport[ceiling($size div 2)]/anchorTensionMax"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</fo:table-body>
					</fo:table>
            	</fo:table-cell>
            </fo:table-row>
            
        </fo:table-body>
	</fo:table>
	</xsl:template>
	<!-- marine section END -->
</xsl:stylesheet>