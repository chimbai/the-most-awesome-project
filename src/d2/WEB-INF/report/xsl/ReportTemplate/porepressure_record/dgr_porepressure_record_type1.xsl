<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/PorePressure">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="2pt">
         <fo:table-column column-number="1" column-width="proportional-column-width(23)" />
         <fo:table-column column-number="2" column-width="proportional-column-width(77)" />
         <fo:table-header>
            <fo:table-row font-size="10pt">
               <fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" number-columns-spanned="2" text-align="center" background-color="rgb(37,87,147)">
                  <fo:block color="white">Pore Pressure / Wellbore Stability</fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-header>
         <fo:table-body>
         	<xsl:apply-templates select="PorePressure"/>
         </fo:table-body>
         </fo:table>
         </xsl:template>
         
      <xsl:template match="PorePressure">
		<fo:table-row font-size="8pt">
     		<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
            	<fo:block font-weight="bold">
                 	Estimated Pore Pressure:
            	</fo:block>
            </fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
            	<fo:block>
                	<xsl:value-of select="porePressure" /><fo:inline color="white">.</fo:inline>
                	<xsl:value-of select="porePressure/@uomSymbol"/>
            	</fo:block>
        	</fo:table-cell>
		</fo:table-row>
		<fo:table-row font-size="8pt">
     		<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
            	<fo:block font-weight="bold">
                 	Hole Condition, Cavings:
            	</fo:block>
            </fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
            	<fo:block>
                	<xsl:value-of select="holeConditionComment" />
            	</fo:block>
        	</fo:table-cell>
		</fo:table-row>
		<fo:table-row font-size="8pt">
     		<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
            	<fo:block font-weight="bold">
                 	Gas Indicators - BG, TG, CG:
            	</fo:block>
            </fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
            	<fo:block>
                	<xsl:value-of select="gasIndicators" />
            	</fo:block>
        	</fo:table-cell>
		</fo:table-row>
		<fo:table-row font-size="8pt">
     		<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
            	<fo:block font-weight="bold">
                 	Losses:
            	</fo:block>
            </fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
            	<fo:block>
                	<xsl:value-of select="lossesRemarks" />
            	</fo:block>
        	</fo:table-cell>
		</fo:table-row>
		<fo:table-row font-size="8pt">
     		<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
            	<fo:block font-weight="bold">
                 	Remarks:
            	</fo:block>
            </fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
            	<fo:block>
                	<xsl:value-of select="porePressureRemarks" />
            	</fo:block>
        	</fo:table-cell>
		</fo:table-row>
   </xsl:template>
	
</xsl:stylesheet>