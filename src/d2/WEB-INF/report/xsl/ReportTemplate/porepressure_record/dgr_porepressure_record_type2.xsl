<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:template match="modules/PorePressure">
		<fo:table inline-progression-dimension="19.5cm" table-layout="fixed" wrap-option="wrap">
        <fo:table-column column-number="1" column-width="proportional-column-width(20)" />
        <fo:table-column column-number="2" column-width="proportional-column-width(50)" />
        <fo:table-column column-number="3" column-width="proportional-column-width(8)" />
        <fo:table-column column-number="4" column-width="proportional-column-width(22)" />
       
   
         <fo:table-header>
            <fo:table-row font-size="10pt">
               <fo:table-cell padding="0.075cm" border-top="0.75px solid black" border-right="0.75px solid black" border-left="0.75px solid black" font-weight="bold" number-columns-spanned="4" text-align="center" background-color="rgb(37,87,147)">
                  <fo:block color="white">Pore Pressure</fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-header>
         <fo:table-body>
         	<xsl:apply-templates select="PorePressure"/>
         </fo:table-body>
         </fo:table>
         </xsl:template>
         
      <xsl:template match="PorePressure">
		<fo:table-row font-size="8pt">
     		<fo:table-cell padding="0.1cm" border-left="0.75px solid black" border-top="0.75px solid black" border-bottom="0.75px solid black">
            	<fo:block font-weight="bold"> 
                 	Depth:
            	</fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0.1cm" border-top="0.75px solid black" border-bottom="0.75px solid black">
            	<fo:block>
                 	<xsl:value-of select="depthTopTvdMsl"/><fo:inline color="white">.</fo:inline>
                 	<xsl:value-of select="depthTopTvdMsl/@uomSymbol"/> 
                 	 to 
                	<xsl:value-of select="translate(depthBottomTvdMsl,',','')" /><fo:inline color="white">.</fo:inline>
                	<xsl:value-of select="depthBottomTvdMsl/@uomSymbol"/> 
            	</fo:block>
            </fo:table-cell>
            
             <fo:table-cell padding="0.1cm" border-right="0.75px solid black" border-top="0.75px solid black" border-bottom="0.75px solid black" number-columns-spanned="2">
            	<fo:block font-weight="bold"> 
                 	
            	</fo:block>
            </fo:table-cell>
            
        </fo:table-row>
        <fo:table-row font-size="8pt">
     		<fo:table-cell padding="0.1cm" border-left="0.75px solid black" border-bottom="0.75px solid black" >
            	<fo:block font-weight="bold"> 
                 	LOT/FIT Last Csg Shoe:
            	</fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0.1cm" border-bottom="0.75px solid black">
            	<fo:block>
            		<xsl:if	test="translate(lotFitDensity,',','')!= ''">
            			<xsl:value-of select="translate(lotFitDensity,',','')" /><fo:inline color="white">.</fo:inline><xsl:value-of select="lotFitDensity/@uomSymbol" /> <fo:inline color="white">.</fo:inline>@
            			<xsl:value-of select="depthMdMslDuringTest"/><fo:inline color="white">.</fo:inline><xsl:value-of select="depthMdMslDuringTest/@uomSymbol" /> MD<xsl:value-of select="/root/datum/currentDatumType"/>/
            			<xsl:value-of select="depthTvdMslDuringTest"/><fo:inline color="white">.</fo:inline><xsl:value-of select="depthTvdMslDuringTest/@uomSymbol" /> TVD<xsl:value-of select="/root/datum/currentDatumType"/>
            		</xsl:if>
                 	<xsl:if	test="translate(lotFitDensity,',','')= ''">
            			 -
            		</xsl:if>
            		
      
            	</fo:block>
           </fo:table-cell>
           
           <fo:table-cell padding="0.1cm" border-bottom="0.75px solid black">
            	<fo:block font-weight="bold"> 
                 	Comment:
            	</fo:block>
            </fo:table-cell>
            
           
             <fo:table-cell padding="0.1cm" border-right="0.75px solid black" border-bottom="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(lotfitComment)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="lotfitComment"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>							
           </fo:table-cell>
            
        </fo:table-row>
        
        <fo:table-row font-size="8pt">
     		<fo:table-cell border-left="0.75px solid black" padding="0.1cm" >
            	<fo:block font-weight="bold"> 
                 	Mud Weight:
            	</fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0.1cm">
            	<fo:block>
            		<xsl:if	test="translate(mudWeight,',','')!= ''">
            			<xsl:value-of select="translate(mudWeight,',','')" /><fo:inline color="white">.</fo:inline><xsl:value-of select="mudWeight/@uomSymbol" />
            		</xsl:if>
                 	<xsl:if	test="translate(mudWeight,',','')= ''">
            			 -
            		</xsl:if>
            	</fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding="0.1cm">
            	<fo:block font-weight="bold"> 
                 	Comment:
            	</fo:block>
            </fo:table-cell>
             <fo:table-cell border-right="0.75px solid black" padding="0.1cm" >
				<xsl:choose>													
					<xsl:when test = "string(mudWeightComment)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="mudWeightComment"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>													
           </fo:table-cell>
        </fo:table-row>
        
        <fo:table-row font-size="8pt">
     		<fo:table-cell padding="0.1cm" border-left="0.75px solid black" border-bottom="0.75px solid black">
            	<fo:block font-weight="bold"> 
                 	ECD/PWD:
            	</fo:block>
            </fo:table-cell>
             <fo:table-cell padding="0.1cm" border-bottom="0.75px solid black">
            	<fo:block>
            		<xsl:if	test="translate(ecd,',','')!= ''">
            			<xsl:value-of select="translate(ecd,',','')" /><fo:inline color="white">.</fo:inline><xsl:value-of select="ecd/@uomSymbol" />
            		</xsl:if>
                 	<xsl:if	test="translate(ecd,',','')= ''">
            			-
            		</xsl:if>
            	</fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding="0.1cm" border-bottom="0.75px solid black">
            	<fo:block font-weight="bold"> 
                 	Comment:
            	</fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0.1cm"  border-right="0.75px solid black" border-bottom="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(ecdPwdComment)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="ecdPwdComment"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>			
           </fo:table-cell>
        </fo:table-row>
        
        <fo:table-row font-size="8pt">
     		<fo:table-cell border-left="0.75px solid black" padding="0.1cm">
            	<fo:block font-weight="bold"> 
                 	Min. EPP (Open Hole):
            	</fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0.1cm">
            	<fo:block>
            		<xsl:if	test="translate(estimatedPorepressureDensity,',','')!= ''">
            			<xsl:value-of select="translate(estimatedPorepressureDensity,',','')" /><fo:inline color="white">.</fo:inline><xsl:value-of select="estimatedPorepressureDensity/@uomSymbol" /><fo:inline color="white">.</fo:inline>@
            			
            			<xsl:value-of select="estimatedPorePressureDepthMdMsl"/><fo:inline color="white">.</fo:inline><xsl:value-of select="estimatedPorePressureDepthMdMsl/@uomSymbol" /> MD<xsl:value-of select="/root/datum/currentDatumType"/>/
            			<xsl:value-of select="estimatedPorePressureDepthTvdMsl"/><fo:inline color="white">.</fo:inline><xsl:value-of select="estimatedPorePressureDepthTvdMsl/@uomSymbol" />TVD<xsl:value-of select="/root/datum/currentDatumType"/>
            		</xsl:if>
                 	<xsl:if	test="translate(lotFitDensity,',','')= ''">
            			-
            		</xsl:if>
            	</fo:block>
           </fo:table-cell>
        


            
             <fo:table-cell padding="0.1cm">
            	<fo:block font-weight="bold"> 
                 	Comment:
            	</fo:block>
            </fo:table-cell>
             <fo:table-cell border-right="0.75px solid black" padding="0.1cm" >
				<xsl:choose>													
					<xsl:when test = "string(estimatedPorepressureText)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="estimatedPorepressureText"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>					
           </fo:table-cell>
        </fo:table-row>
        
         <fo:table-row font-size="8pt">
     		<fo:table-cell padding="0.1cm" border-left="0.75px solid black" border-bottom="0.75px solid black">
            	<fo:block font-weight="bold"> 
                 	Max. EPP (Open Hole):
            	</fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0.1cm" border-bottom="0.75px solid black">
            	<fo:block>
            		<xsl:if	test="translate(estimatedPorePressureMaxDensity,',','')!= ''">
            			<xsl:value-of select="translate(estimatedPorePressureMaxDensity,',','')" /><fo:inline color="white">.</fo:inline><xsl:value-of select="estimatedPorePressureMaxDensity/@uomSymbol" /><fo:inline color="white">.</fo:inline>@
            			
            			<xsl:value-of select="estimatedPorePressueMaxDepthMdMsl"/><fo:inline color="white">.</fo:inline><xsl:value-of select="estimatedPorePressueMaxDepthMdMsl/@uomSymbol" /> MD<xsl:value-of select="/root/datum/currentDatumType"/>/
            			<xsl:value-of select="estimatedPorePressueMaxDepthTvdMsl"/><fo:inline color="white">.</fo:inline><xsl:value-of select="estimatedPorePressueMaxDepthTvdMsl/@uomSymbol" />TVD<xsl:value-of select="/root/datum/currentDatumType"/>
            		</xsl:if>
                 	<xsl:if	test="translate(estimatedPorePressureMaxDensity,',','')= ''">
            			-
            		</xsl:if>
            	</fo:block>
           </fo:table-cell>
            
             <fo:table-cell padding="0.1cm" border-bottom="0.75px solid black">
            	<fo:block font-weight="bold"> 
                 	Comment:
            	</fo:block>
            </fo:table-cell>
             <fo:table-cell padding="0.1cm" border-right="0.75px solid black" border-bottom="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(maxPorePressureComment)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="maxPorePressureComment"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>
           </fo:table-cell>
        </fo:table-row>
        
        <fo:table-row font-size="8pt">
        <fo:table-cell padding="0.1cm" number-columns-spanned="4" border-left="0.75px solid black" border-right="0.75px solid black" border-bottom="0.75px solid black" background-color="rgb(149, 179, 215)">
            	<fo:block text-align="left">
                 	<fo:inline font-style="italic">Last 24 Hrs Data</fo:inline>
            	</fo:block>
            </fo:table-cell>
        </fo:table-row>  
        
         <fo:table-row font-size="8pt">
     		<fo:table-cell border-left="0.75px solid black" padding="0.1cm" >
            	<fo:block font-weight="bold"> 
                 	Background Gas Range:
            	</fo:block>
            </fo:table-cell>
            
           <fo:table-cell>
           	<fo:block>
            	<fo:table width="100%" table-layout="fixed">
                	<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
												
					<fo:table-body>
						<fo:table-row>
                        	<fo:table-cell text-align="left" padding="0.1cm" >
                            	<fo:block>
                                	<xsl:choose>													
										<xsl:when test = "string(backgroundGasRangeMin)!=''">
										Min:<xsl:value-of select="backgroundGasRangeMin"/>										
									</xsl:when>													
									<xsl:otherwise>
										-
									</xsl:otherwise>
									</xsl:choose>
                                </fo:block>
                            </fo:table-cell>
                              							
                           <fo:table-cell text-align="left" padding="0.1cm" >
                           		<fo:block>
                                 	<xsl:choose>													
										<xsl:when test = "string(backgroundGasRangeMax)!=''">
										Max:<xsl:value-of select="backgroundGasRangeMax"/>										
									</xsl:when>													
									<xsl:otherwise>
										-
									</xsl:otherwise>
									</xsl:choose>	
                                </fo:block>
                           </fo:table-cell>
                  		 </fo:table-row>
                   
                   </fo:table-body>															
				</fo:table>
             </fo:block>
           </fo:table-cell>
          
            
            <fo:table-cell padding="0.1cm">
            	<fo:block font-weight="bold"> 
                 	Comment:
            	</fo:block>
            </fo:table-cell>
            <fo:table-cell border-right="0.75px solid black" padding="0.1cm">
				<xsl:choose>													
					<xsl:when test = "string(backgroundGasComment)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="backgroundGasComment"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>
           </fo:table-cell>
        </fo:table-row>
        
         <fo:table-row font-size="8pt">
     		<fo:table-cell border-left="0.75px solid black" padding="0.1cm" >
            	<fo:block font-weight="bold"> 
                 	Max. Drilled Gas:
            	</fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0.1cm">
            	<fo:block>
            		<xsl:if	test="translate(drilledGasRangeMax,',','')!= ''">
            			<xsl:value-of select="translate(drilledGasRangeMax,',','')" /><fo:inline color="white">.</fo:inline>@
            			
            			<xsl:value-of select="drilledGasMaxDepthMdMsl"/><fo:inline color="white">.</fo:inline><xsl:value-of select="drilledGasMaxDepthMdMsl/@uomSymbol" /> MD<xsl:value-of select="/root/datum/currentDatumType"/>
            			
            		</xsl:if>
                 	<xsl:if	test="translate(drilledGasRangeMax,',','')= ''">
            			-
            		</xsl:if>
            	</fo:block>
            </fo:table-cell>
            


            <fo:table-cell padding="0.1cm">
            	<fo:block font-weight="bold"> 
                 	Comment:
            	</fo:block>
            </fo:table-cell>
             <fo:table-cell border-right="0.75px solid black" padding="0.1cm">
				<xsl:choose>													
					<xsl:when test = "string(maxDrilledGasComment)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="maxDrilledGasComment"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>
           </fo:table-cell>
        </fo:table-row>
        
        <fo:table-row font-size="8pt">
     		<fo:table-cell border-left="0.75px solid black" padding="0.1cm" >
            	<fo:block font-weight="bold"> 
                 	Trip Gas:
            	</fo:block>
            </fo:table-cell>
             <fo:table-cell padding="0.1cm">
            	<fo:block>
            		<xsl:if	test="translate(tripGasRange,',','')!= ''">
            			<xsl:value-of select="translate(tripGasRange,',','')" /><fo:inline color="white">.</fo:inline>@
            			
            			<xsl:value-of select="tripGasDepthMdMsl"/><fo:inline color="white">.</fo:inline><xsl:value-of select="tripGasDepthMdMsl/@uomSymbol" /> MD<xsl:value-of select="/root/datum/currentDatumType"/>
            			
            		</xsl:if>
                 	<xsl:if	test="translate(tripGasRange,',','')= ''">
            			-
            		</xsl:if>
            	</fo:block>
            </fo:table-cell>


            <fo:table-cell padding="0.1cm">
            	<fo:block font-weight="bold"> 
                 	Comment:
            	</fo:block>
            </fo:table-cell>
             <fo:table-cell border-right="0.75px solid black" padding="0.1cm">
				<xsl:choose>													
					<xsl:when test = "string(tripGasComment)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="tripGasComment"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>
           </fo:table-cell>
        </fo:table-row>
        
        <fo:table-row font-size="8pt">
     		<fo:table-cell border-left="0.75px solid black" padding="0.1cm" border-bottom="0.75px solid black">
            	<fo:block font-weight="bold"> 
                 	Connection Gases:
            	</fo:block>
            </fo:table-cell>
              <fo:table-cell padding="0.1cm" border-bottom="0.75px solid black">
            	<fo:block>
            		<xsl:if	test="translate(connectionGasRange,',','')!= ''">
            			<xsl:value-of select="translate(connectionGasRange,',','')" /><fo:inline color="white">.</fo:inline>@
            			
            			<xsl:value-of select="connectionGasDepthFromMdMsl"/><fo:inline color="white">.</fo:inline><xsl:value-of select="connectionGasDepthFromMdMsl/@uomSymbol" /> MD<xsl:value-of select="/root/datum/currentDatumType"/> -
            			<xsl:value-of select="connectionGasDepthToMdMsl"/><fo:inline color="white">.</fo:inline><xsl:value-of select="connectionGasDepthToMdMsl/@uomSymbol" /> MD<xsl:value-of select="/root/datum/currentDatumType"/>
            		</xsl:if>
                 	<xsl:if	test="translate(connectionGasRange,',','')= ''">
            			-
            		</xsl:if>
            	</fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding="0.1cm" border-bottom="0.75px solid black">
            	<fo:block font-weight="bold"> 
                 	Comment:
            	</fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0.1cm" border-right="0.75px solid black" border-bottom="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(connectionGasComment)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="connectionGasComment"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>
           </fo:table-cell>
        </fo:table-row>
        
          <fo:table-row font-size="8pt">
     		<fo:table-cell padding="0.1cm" border-left="0.75px solid black" border-bottom="0.75px solid black">
            	<fo:block font-weight="bold"> 
                 	Cavings:
            	</fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0.1cm" border-bottom="0.75px solid black">
            	<fo:block>
							<xsl:choose>													
								<xsl:when test = "string(cavingAmount)!=''">
									<xsl:value-of select="cavingAmount"/><fo:inline color="white">.</fo:inline><xsl:value-of select="cavingAmount/@uomSymbol" />										
								</xsl:when>													
								<xsl:otherwise>
									-<xsl:value-of select="cavingAmount/@uomSymbol" />
								</xsl:otherwise>
							</xsl:choose>							
				</fo:block>
            </fo:table-cell>

	       
            <fo:table-cell padding="0.1cm" border-bottom="0.75px solid black">
            	<fo:block font-weight="bold"> 
                 	Comment:
            	</fo:block>
            </fo:table-cell>
             <fo:table-cell padding="0.1cm" border-right="0.75px solid black" border-bottom="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(cavingsComment)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="cavingsComment"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>
           </fo:table-cell>
        </fo:table-row>
        
         <fo:table-row font-size="8pt">
     		<fo:table-cell padding="0.1cm" border-left="0.75px solid black" border-bottom="0.75px solid black">
            	<fo:block font-weight="bold"> 
                 	Hole Condition Comment:
            	</fo:block>
            </fo:table-cell>
             <fo:table-cell padding="0.1cm" border-bottom="0.75px solid black">
				<xsl:choose>													
					<xsl:when test = "string(holeConditionComment)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="holeConditionComment"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>
            </fo:table-cell>
            
            <fo:table-cell padding="0.1cm" border-right="0.75px solid black" border-bottom="0.75px solid black" number-columns-spanned="2">
            	<fo:block > 						
				</fo:block>
           </fo:table-cell>
        </fo:table-row>
        
        <fo:table-row font-size="8pt">
     		<fo:table-cell border-left="0.75px solid black" padding="0.1cm" >
            	<fo:block font-weight="bold"> 
                 	Downhole Mud Losses:
            	</fo:block>
            </fo:table-cell>
             <fo:table-cell padding="0.1cm" >
            	<fo:block>
							<xsl:choose>													
								<xsl:when test = "string(mudLossVolume)!=''">
									<xsl:value-of select="mudLossVolume"/><fo:inline color="white">.</fo:inline><xsl:value-of select="mudLossVolume/@uomSymbol" />										
								</xsl:when>													
								<xsl:otherwise>
									-
								</xsl:otherwise>
							</xsl:choose>							
				</fo:block>
            </fo:table-cell>
           
            <fo:table-cell padding="0.1cm" >
            	<fo:block font-weight="bold"> 
                 	Comment:
            	</fo:block>
            </fo:table-cell>
             <fo:table-cell border-right="0.75px solid black" padding="0.1cm">
				<xsl:choose>													
					<xsl:when test = "string(lossesRemarks)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="lossesRemarks"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>
            </fo:table-cell>
        </fo:table-row>
        
         <fo:table-row font-size="8pt">
     		<fo:table-cell border-left="0.75px solid black" border-bottom="0.75px solid black" padding="0.1cm">
            	<fo:block font-weight="bold"> 
                 	DXC Comments:
            	</fo:block>
            </fo:table-cell>
              <fo:table-cell border-right="0.75px solid black" border-bottom="0.75px solid black" padding="0.1cm" number-columns-spanned="3">
				<xsl:choose>													
					<xsl:when test = "string(description)!=''">
						<fo:block linefeed-treatment="preserve">
							<xsl:value-of select="description"/>			
						</fo:block>							
					</xsl:when>													
					<xsl:otherwise>
						<fo:block>-</fo:block>
					</xsl:otherwise>
				</xsl:choose>
            </fo:table-cell>
            
        </fo:table-row>
         <fo:table-row height="0.1cm">
              <fo:table-cell number-columns-spanned="4">
            	<fo:block>	
            						
				</fo:block>
            </fo:table-cell>
        </fo:table-row>
   </xsl:template>
	
</xsl:stylesheet>