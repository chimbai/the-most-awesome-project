<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- casing section BEGIN -->
	<xsl:template match="modules/CasingSection">
	<xsl:variable name="dailyshoedepth" select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl/@rawNumber"/>
	<xsl:if test="count(CasingSection[shoeTopMdMsl/@rawNumber &lt;= $dailyshoedepth]) &gt; 0">	
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap"
			border="0.5px solid black" space-after="6pt" space-before="6pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(14)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(28)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(28)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(30)"/>
			
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="4" padding="2px" border="0.5px solid black" background-color="rgb(223,223,223)">
						<fo:block text-align="center" font-weight="bold" font-size="10pt">
							Casing Run
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
				<fo:table-row>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px">
						<fo:block text-align="center">OD</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px">
						<fo:block text-align="center">LOT/FIT</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px">
						<fo:block text-align="center">Csg Shoe (MD/TVD)</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" padding="2px">
						<fo:block text-align="center">Remarks</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>				
				<xsl:apply-templates select="CasingSection[shoeTopMdMsl/@rawNumber &lt;= $dailyshoedepth]"/>
			</fo:table-body>
		</fo:table>
	</xsl:if>											
	</xsl:template>

	<xsl:template match="CasingSection">
		<fo:table-row>
			<fo:table-cell border-right="0.5px solid black" padding-before="2px"
				padding-start="2px" padding-end="2px">
				<fo:block text-align="left">
					<xsl:choose>													
						<xsl:when test = "string(casingOd/@lookupLabel)=''">
							<xsl:value-of select="casingOd"/>
						</xsl:when>													
						<xsl:otherwise>
							<xsl:value-of select="casingOd/@lookupLabel"/>
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding-before="2px"
				padding-start="2px" padding-end="2px">
				<fo:block text-align="center">
					<xsl:value-of select="lot"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="lot/@uomSymbol"/>
					<fo:inline> / </fo:inline>
					<xsl:value-of select="fit"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="fit/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.5px solid black" padding-before="2px"
				padding-start="2px" padding-end="2px">
				<fo:block text-align="center">
					<xsl:value-of select="shoeTopMdMsl"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="shoeTopMdMsl/@uomSymbol"/>
					<fo:inline> / </fo:inline>
					<xsl:value-of select="shoeTopTvdMsl"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="shoeTopTvdMsl/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="2px" padding-start="2px" padding-end="2px">
				<fo:block text-align="left">
					<xsl:value-of select="casingSummary"/>
				</fo:block>
			</fo:table-cell>
			
		</fo:table-row>
	</xsl:template>
	<!-- casing section END -->

</xsl:stylesheet>