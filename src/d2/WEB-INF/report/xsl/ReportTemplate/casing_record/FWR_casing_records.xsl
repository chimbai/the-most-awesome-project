<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:import href="./FWR_casing_tally_header.xsl" />
	<xsl:import href="./FWR_casing_tally_records.xsl" />
	
	<!-- FWR Casing START -->
	<xsl:template match="modules/CasingSection/CasingSection" mode="casing_section_detail">
	<xsl:variable name="selected_casingsectionuid" select="casingSectionUid"/>
	<xsl:variable name="select_dailyid" select="dailyid"/>
    <fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="3pt" table-omit-header-at-break="true">
      <fo:table-column column-number="1" column-width="proportional-column-width(1)" />
      <fo:table-body>
      	<fo:table-row>
      		<fo:table-cell>
      			<fo:block>
				    <fo:table inline-progression-dimension="19cm" table-layout="fixed" padding="0.1cm" font-size="6pt" wrap-option="wrap" table-omit-header-at-break="true" keep-together="always">
				      <fo:table-column column-number="1" column-width="proportional-column-width(1)" />
				      <fo:table-column column-number="2" column-width="proportional-column-width(1)" />
				      <fo:table-column column-number="3" column-width="proportional-column-width(1)" />
				      <fo:table-body>
					      <fo:table-row>
					      	<fo:table-cell border="0.25px solid black" padding="0.1cm">
					      		<fo:block font-weight="bold" font-size="7pt">
					      			Run Date: (#<xsl:value-of select="//root/modules/Daily/Daily[dailyUid=$select_dailyid]/dayNumber"/>)
					      			<fo:inline color="white">!</fo:inline>
					      			<xsl:value-of select="d2Utils:formatDateFromEpochMS(installStartDate/@epochMS,'dd MMM yyyy')"/>
					      		</fo:block>
					      	</fo:table-cell>
					      	<fo:table-cell border="0.25px solid black" padding="0.1cm" number-columns-spanned="2">
					      		<fo:block>
					      			Comments: <fo:inline color="white">!</fo:inline><xsl:value-of select="casingSummary"/>					      			
					      		</fo:block>
					      	</fo:table-cell>
					      </fo:table-row>
				      	<fo:table-row>
				      		<fo:table-cell>
				      			<fo:block> <!--column 1 -->
								    <fo:table width="100%" table-layout="fixed" border-top="0.25px solid black" border-bottom="0.25px solid black" border-left="0.25px solid black" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
								      <fo:table-column column-number="1" column-width="proportional-column-width(1)" />
								      <fo:table-body>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="22mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Size OD
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="casingOd/@lookupLabel"/>&#160;						                    					
					                    					</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Weight (Wet)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="weight_wet"/>&#160;
																<xsl:value-of select="weight_wet/@uomSymbol"/>						                    					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Weight (Dry)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="weight_dry"/>&#160;
																<xsl:value-of select="weight_dry/@uomSymbol"/>						                    					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Weight (Set)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="weight_landing"/>&#160;
																<xsl:value-of select="weight_landing/@uomSymbol"/>						                    					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Grade
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="grade"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Collar Type
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="collar_type"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Thread Type
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="thread_type"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Stage Tool Type
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="stage_tool_type"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Stage Tool
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="stage_tool_installed/@lookupLabel"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>								      	
								      </fo:table-body>
								    </fo:table>
				      			</fo:block>
				      		</fo:table-cell>
				      		<fo:table-cell>
				      			<fo:block> <!--column 2 -->
								    <fo:table width="100%" table-layout="fixed" border-top="0.25px solid black" border-bottom="0.25px solid black" wrap-option="wrap" space-after="3pt" table-omit-header-at-break="true">
								      <fo:table-column column-number="1" column-width="proportional-column-width(1)" />
								      <fo:table-body>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Liner
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="liner_installed/@lookupLabel"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="22mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Total # Joints Run
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="totaljoints_inhole/@rawNumber"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Total Length Casing Run
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="totallength"/>&#160;
						                    					<xsl:value-of select="totallength/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>								      	
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Float Collar Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="float_collar_depth_md"/>&#160;
						                    					<xsl:value-of select="float_collar_depth_md/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Shoe Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="shoeTopMdMsl"/>&#160;
						                    					<xsl:value-of select="shoeTopMdMsl/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Shoe Depth (TVD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="shoeTopTvdMsl"/>&#160;
					                        					<xsl:value-of select="shoeTopTvdMsl/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="32mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Landed Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="casing_landed_depth_md"/>&#160;
						                    					<xsl:value-of select="casing_landed_depth_md/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="32mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Stage Tool Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="stage_tool_depth_md"/>&#160;
																<xsl:value-of select="stage_tool_depth_md/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="32mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Liner Top Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="liner_top_depth_md"/>&#160;
							                   					<xsl:value-of select="liner_top_depth_md/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>								      	
								      </fo:table-body>
								    </fo:table>
				      			</fo:block>
				      		</fo:table-cell>
				      		<fo:table-cell>
				      			<fo:block> <!--column 3 -->
								    <fo:table width="100%" table-layout="fixed" border-top="0.25px solid black" border-bottom="0.25px solid black" wrap-option="wrap" space-after="3pt" table-omit-header-at-break="true">
								      <fo:table-column column-number="1" column-width="proportional-column-width(1)" />
								      <fo:table-body>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="22mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Hole Size
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="openHoleId"/>&#160;
						                    					<xsl:value-of select="openHoleId/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="22mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Hole Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="openHoleDepthMdMsl"/>&#160;
						                    					<xsl:value-of select="openHoleDepthMdMsl/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="22mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Hole Depth (TVD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="openHoleDepthTvdMsl"/>&#160;
						                    					<xsl:value-of select="openHoleDepthTvdMsl/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Gauge Hole Volume
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="hole_volume"/>&#160;
						                    					<xsl:value-of select="hole_volume/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>								      	
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="35mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Start Time/Date
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="d2Utils:formatDateFromEpochMS(installStartDate/@epochMS,'dd MMM yyyy HH:mm')"/>&#160;					                          					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Stop Time/Date
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="d2Utils:formatDateFromEpochMS(installCompleteDate/@epochMS,'dd MMM yyyy HH:mm')"/>&#160;					                          					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Total Time
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="total_hours"/>&#160;					                          					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Downtime
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="downtime_hours"/>&#160;					                          					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Downtime Reason
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="downtime_descr"/>&#160;					                          					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>								      	
								      </fo:table-body>
								    </fo:table>
				      			</fo:block>
				      		</fo:table-cell>
				      	</fo:table-row>
				      </fo:table-body>
				    </fo:table>
      			</fo:block>
      		</fo:table-cell>
      	</fo:table-row>
      	<fo:table-row>
        	<fo:table-cell>
	        	<xsl:choose>
		        	<xsl:when test="count(CasingTally[casingSectionUid=$selected_casingsectionuid]) &gt; 0">
		        		<fo:block>
						      <fo:table width="100%" table-layout="fixed" border="0.5px solid black" font-size="6pt">
						         <fo:table-column column-width="proportional-column-width(0.5)" />
						         <fo:table-column column-width="proportional-column-width(0.5)" />
						         <fo:table-column column-width="proportional-column-width(0.5)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1.5)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(2)" />
						         <fo:table-header>
									<xsl:call-template name="casing_tally_header"/>
						         </fo:table-header>
						         <fo:table-body>
									<xsl:apply-templates select="CasingTally[casingSectionUid=$selected_casingsectionuid]"/>
								 </fo:table-body>								
							 </fo:table>
		        		</fo:block>
		        	</xsl:when>
		        	<xsl:otherwise>
		        		<fo:block/>
		        	</xsl:otherwise>
	        	</xsl:choose>	    
        	</fo:table-cell>
   			</fo:table-row>
		</fo:table-body>
	</fo:table>
	</xsl:template>
	<!-- FWR Casing START -->
 
 </xsl:stylesheet>