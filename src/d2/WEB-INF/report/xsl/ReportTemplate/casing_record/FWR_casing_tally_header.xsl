<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<xsl:template name="casing_tally_header">
		<fo:table-row text-align="center">
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> Seq	</fo:block>
		  </fo:table-cell>
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> Unit	</fo:block>
		  </fo:table-cell>
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> Jts </fo:block>
		  </fo:table-cell>
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> OD(in) </fo:block>
		  </fo:table-cell>
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> ID(in)	</fo:block>
		  </fo:table-cell>
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> Wt </fo:block>
		  </fo:table-cell>
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> Grade </fo:block>
		  </fo:table-cell>
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> Thread </fo:block>
		  </fo:table-cell>
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> Type	</fo:block>
		  </fo:table-cell>
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> Section MD </fo:block>
		  </fo:table-cell>
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> Section TVD </fo:block>
		  </fo:table-cell>
		  <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
		  		<fo:block> Remarks </fo:block>
		  </fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
</xsl:stylesheet>