<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- TEMPLATE: modules/CasingSection -->
	<xsl:template match="modules/CasingSection">
	
		<xsl:variable name="dailyshoedepth" select="/root/modules/ReportDaily/ReportDaily/lastCsgshoeMdMsl/@rawNumber"/>
		
		<xsl:if test="count(CasingSection[shoeTopMdMsl/@rawNumber &lt;= $dailyshoedepth]) &gt; 0">
		
			<fo:block keep-together="always">
				
				<!-- TABLE -->
				<fo:table width="100%" table-layout="fixed" wrap-option="wrap" border-bottom="thin solid black" space-after="6pt">
					<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(20)"/>
					<fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
					<fo:table-column column-number="4" column-width="proportional-column-width(20)"/>
					<fo:table-column column-number="5" column-width="proportional-column-width(20)"/>
					
					<!-- HEADER -->
					<fo:table-header>
					
						<!-- HEADER 1 -->
						<fo:table-row>
							<fo:table-cell number-columns-spanned="5" padding="1px" background-color="#DDDDDD" border="thin solid black" display-align="center">
								<fo:block font-size="8pt" font-weight="bold">
									Casing
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
						<!-- HEADER 2 -->
						<fo:table-row>
							<fo:table-cell padding="2px" border-left="thin solid black" border-bottom="thin solid black">
								<fo:block text-align="center">
									OD
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									LOT
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									FIT
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									Casing Shoe (MD)
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-right="thin solid black" border-bottom="thin solid black">
								<fo:block text-align="center">
									Casing Shoe (TVD)
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						
					</fo:table-header>
					
					<!-- BODY -->
					<fo:table-body>				
						<xsl:apply-templates select="CasingSection[shoeTopMdMsl/@rawNumber &lt;= $dailyshoedepth]">
						<xsl:sort select="casingOd/@rawNumber"/>
						</xsl:apply-templates>
					</fo:table-body>
					
				</fo:table>
				
			</fo:block>
			
		</xsl:if>
													
	</xsl:template>

	<!-- TEMPLATE: CasingSection -->
	<xsl:template match="CasingSection">
	
		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
		
				<fo:table-row>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:choose>													
								<xsl:when test="string(casingOd/@lookupLabel)=''">
									<xsl:value-of select="casingOd"/>
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="casingOd/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="lot"/>
							<fo:inline color="white">.</fo:inline>
							<xsl:value-of select="lot/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="fit"/>
							<fo:inline color="white">.</fo:inline>
							<xsl:value-of select="fit/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="shoeTopMdMsl"/>
							<fo:inline color="white">.</fo:inline>
							<xsl:value-of select="shoeTopMdMsl/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="shoeTopTvdMsl"/>
							<fo:inline color="white">.</fo:inline>
							<xsl:value-of select="shoeTopTvdMsl/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
			</xsl:when>
			<xsl:otherwise>
			
				<fo:table-row background-color="#EEEEEE">
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-left="thin solid black" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:choose>													
								<xsl:when test="string(casingOd/@lookupLabel)=''">
									<xsl:value-of select="casingOd"/>
								</xsl:when>													
								<xsl:otherwise>
									<xsl:value-of select="casingOd/@lookupLabel"/>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="lot"/>
							<fo:inline color="#EEEEEE">.</fo:inline>
							<xsl:value-of select="lot/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="fit"/>
							<fo:inline color="#EEEEEE">.</fo:inline>
							<xsl:value-of select="fit/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="shoeTopMdMsl"/>
							<fo:inline color="#EEEEEE">.</fo:inline>
							<xsl:value-of select="shoeTopMdMsl/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="right">
							<xsl:value-of select="shoeTopTvdMsl"/>
							<fo:inline color="#EEEEEE">.</fo:inline>
							<xsl:value-of select="shoeTopTvdMsl/@uomSymbol"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			
			</xsl:otherwise>
			
		</xsl:choose>
		
	</xsl:template>

</xsl:stylesheet>