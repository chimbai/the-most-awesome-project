<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- Casing Tally START -->
	<xsl:template match="CasingTally">
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="runOrder"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="uom_label"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="numJoints"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="casingOd"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="casingId"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="weight"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="grade"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="csg_thread"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="type"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="sectionmd"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="sectiontvd"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="normalize-space(csg_comment)"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Casing Tally END -->
	
</xsl:stylesheet>