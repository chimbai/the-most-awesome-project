<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- casing section BEGIN -->
	<xsl:template match="modules/CasingSection">
	<xsl:variable name="dailyshoedepth" select="/root/modules/ReportDaily/ReportDaily/last_csgshoe_md/@rawNumber"/>
	<xsl:if test="count(CasingSection[shoeTopMdMsl/@rawNumber &lt;= $dailyshoedepth]) &gt; 0">	
		<fo:table width="100%" table-layout="fixed" wrap-option="wrap" margin-top="2pt"
			border-left="0.5px solid black" border-right="0.5px solid black" border-bottom="0.5px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(20)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(20)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="5" padding="0.05cm" padding-bottom="-0.05cm" border-bottom="0.25px solid black"
						background-color="rgb(224,224,224)" border-top="0.5px solid black"
						border-left="0.1px solid black" border-right="0.1px solid black">
						<fo:block text-align="left" font-weight="bold" font-size="10pt">
							Casing
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
				<fo:table-row>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">OD</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">LOT</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">FIT</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Csg Shoe MD</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.05cm">
						<fo:block text-align="center">Csg Shoe TVD</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>				
				<xsl:apply-templates select="CasingSection[shoeTopMdMsl/@rawNumber &lt;= $dailyshoedepth]"/>
			</fo:table-body>
		</fo:table>
	</xsl:if>											
	</xsl:template>

	<xsl:template match="CasingSection">
		<fo:table-row>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="center">
					<xsl:value-of select="casingOd/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="lot"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="lot/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="fit"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="fit/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="0.25px solid black" padding-before="0.05cm"
				padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="shoeTopMdMsl"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="shoeTopMdMsl/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-start="0.05cm" padding-end="0.05cm">
				<fo:block text-align="right">
					<xsl:value-of select="shoeTopTvdMsl"/><fo:inline color="white">.</fo:inline>
					<xsl:value-of select="shoeTopTvdMsl/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- casing section END -->

</xsl:stylesheet>