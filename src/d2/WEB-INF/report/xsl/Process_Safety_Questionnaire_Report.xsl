<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- IDS_DEMO Process Safety Questionnaire REPORT -->		
	
	<xsl:import href="./ReportTemplate/hse_record/psqr_leading_lagging_indicator.xsl"/> 
	<xsl:output method="xml"/> 
	<xsl:preserve-space elements="*"/>
	<xsl:template match="/">
		<fo:root> 
			<fo:layout-master-set>
				<fo:simple-page-master master-name="landscape" space-before="12pt" page-height="21cm" page-width="29cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
					<fo:region-before extent="2.2cm"/> 
					<fo:region-after extent="2cm"/> 
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="landscape" initial-page-number="1">  
				 
				<!-- REGION BEFORE - LOGO -->
				<fo:static-content flow-name="xsl-region-before"> 
					<fo:table inline-progression-dimension="27cm" table-layout="fixed" margin-top="5pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(87)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(13)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell number-columns-spanned="2" font-size="12pt" padding-bottom="10pt"> 
									<fo:block>
                                    	<fo:external-graphic content-width="scale-to-fit" width="100pt"
                                    		content-height="50%" scaling="uniform" src="url(d2://images/company_logo.jpg)"/>
                                    </fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell font-size="12pt" display-align="center">
									<fo:block font-weight="bold" text-align="left" >
			                            <fo:inline> 
			                                <xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/completeOperationName" />
			                            </fo:inline>
		                        	</fo:block>	
		                        </fo:table-cell>			
								<fo:table-cell font-size="12pt" text-align="left" font-weight="bold" display-align="before">
									<fo:block>
										<fo:block font-weight="bold">Process Safety</fo:block>
									</fo:block> 
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell font-size="12pt" display-align="center">
									<fo:block  font-weight="bold" text-align="left" >
			                            <fo:inline>
			                                <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel" />
			                            </fo:inline>
		                        	</fo:block>	
		                        </fo:table-cell>			
								<fo:table-cell font-size="12pt" text-align="left" font-weight="bold" display-align="before">
									<fo:block>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportDatetime"/>
									</fo:block> 
								</fo:table-cell> 
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<!-- END OF REGION BEFORE - LOGO -->
				
				<!-- REGION AFTER - FOOTER -->
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="26cm" table-layout="fixed" margin-top="1cm">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS, 20170822, IDS_DEMO_PSQR
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="right">
										Page <fo:page-number/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
		                        <fo:table-cell font-size="6pt" number-columns-spanned="2">
		                           <fo:block text-align="left">Printed on
		                           <xsl:variable name="printdate" select="/root/modules/Daily/Daily/dayDate"/>
		                           <xsl:value-of select="d2Utils:getSystemDate('dd MMM yyyy')"/>
		                           </fo:block>
		                        </fo:table-cell>
		                     </fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<!-- END OF REGION AFTER - FOOTER -->
				
				<!-- REGION BODY - REPORT SECTION -->
				<fo:flow flow-name="xsl-region-body" font-size="8pt">
					<fo:block>
						<fo:table inline-progression-dimension="27cm" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
							<fo:table-body>
								<!-- 4 Tier Leading & Lagging KPI SECTION -->
								<fo:table-row>
									<fo:table-cell>
										<fo:block>
											<xsl:apply-templates select="/root/modules/ProcessSafetyQuestionnaire/ProcessSafetyQuestionnaire"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
																							    
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>	
				<!-- END OF REGION BODY - REPORT SECTION -->
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
