<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">   
	<xsl:import href="./ReportTemplate/activityNpt/activityNpt.xsl" />    
     
	<xsl:output method="xml" />  
	<xsl:preserve-space elements="*" />    
	<xsl:template match="/">             
		<fo:root>        
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-top="0.5cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
					<fo:region-before extent="3cm"/> 
					<fo:region-after extent="1.5cm" margin-bottom="2cm"/> 
				</fo:simple-page-master> 
			</fo:layout-master-set> 
			<fo:page-sequence master-reference="simple" initial-page-number="1"> 
				<fo:static-content flow-name="xsl-region-before"> 
					<fo:block>
						<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-before="12pt" margin-top="1.5cm">
							<fo:table-column column-number="1" column-width="proportional-column-width(30)" />
							<fo:table-column column-number="2" column-width="proportional-column-width(65)" /> 
							<fo:table-column column-number="3" column-width="proportional-column-width(5)" />
							       
							<fo:table-body>
								<fo:table-row>    
									<fo:table-cell> 
										<fo:block> 
											<fo:external-graphic content-width="scale-to-fit" content-height="20%" scaling="non-uniform" 
													src="url(d2://images/company_logo.jpg)" />	    
										</fo:block>
									</fo:table-cell>   
									<fo:table-cell>
										<fo:block text-align="center" font-size="8pt" padding-top="2px"> 
											<fo:table inline-progression-dimension="100%" table-layout="fixed">	 
												<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
												<fo:table-body>
													<fo:table-row>  
														<fo:table-cell vertical-align="middle">  
															<fo:block margin-top="0.1cm" font-size="14pt"> 
																Activity NPT Event Detail for <xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/completeOperationName"/>
															</fo:block> 
														</fo:table-cell>	  
													</fo:table-row>	 
												</fo:table-body>   
											</fo:table>   
										</fo:block>   
									</fo:table-cell>  
									<fo:table-cell> 
										<fo:block>&#160;</fo:block>
									</fo:table-cell>
								</fo:table-row>							
							</fo:table-body>    
						</fo:table>
					</fo:block> 
					  
				</fo:static-content>   
				 
				<fo:static-content flow-name="xsl-region-after"> 
					<fo:block>
					<fo:table inline-progression-dimension="19cm" 
						table-layout="fixed" space-after="6pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)" />
						<fo:table-column column-number="2" column-width="proportional-column-width(50)" />
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS, 20170425, <xsl:value-of select="/root/UserContext/UserSelection/groupUid"/>_ACTNPTEVTDR
									</fo:block> 
								</fo:table-cell> 
								<fo:table-cell font-size="8pt">
									<fo:block text-align="right"> 
										Page <fo:page-number /> of <fo:page-number-citation ref-id="end"/>
									</fo:block>  
								</fo:table-cell>   
							</fo:table-row>    
							<fo:table-row>
								<fo:table-cell font-size="6pt" number-columns-spanned="2">
									<fo:block text-align="left">
										Printed on <xsl:value-of select="d2Utils:getLocalTimestamp('d MMMM yyyy hh:mm a (z)')" />
									</fo:block>  
								</fo:table-cell> 
							</fo:table-row>   
						</fo:table-body>   
					</fo:table>      
					</fo:block>       
				</fo:static-content>          
	  
				<fo:flow flow-name="xsl-region-body" font-size="10pt">   
					<fo:block>   
						<xsl:call-template name="summaryTable"/>     
						<xsl:if test="count(/root/modules/ActivityNptEvent/UnwantedEvent) &gt; 0">
							<xsl:apply-templates select="/root/modules/ActivityNptEvent/UnwantedEvent" /> 
						</xsl:if> 
						<fo:table inline-progression-dimension="18cm" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(100)" />
							<fo:table-body>
								<fo:table-row>		
									<fo:table-cell> 
										<fo:block id="end">&#160;</fo:block> 
									</fo:table-cell>  
								</fo:table-row>											  					
							</fo:table-body>
						</fo:table>   
					</fo:block>  
				</fo:flow>
			</fo:page-sequence> 
		</fo:root>
	</xsl:template>
	

</xsl:stylesheet>
