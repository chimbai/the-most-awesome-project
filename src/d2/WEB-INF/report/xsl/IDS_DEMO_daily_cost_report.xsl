<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
  
	<xsl:import href="./ReportTemplate/cost_record/dcr_header.xsl" />
	<xsl:import href="./ReportTemplate/well_record/dcr_well_status.xsl" />
	<xsl:import href="./ReportTemplate/hse_record/dcr_hse_metrics.xsl" />
	<xsl:import href="./ReportTemplate/well_record/dcr_well_objectives.xsl" />
	<xsl:import href="./ReportTemplate/graph/dcr_dvd_cvd.xsl" />
	<xsl:import href="./ReportTemplate/graph/dcr_time_breakdown.xsl" />
	<xsl:import href="./ReportTemplate/graph/dcr_longest_npt_bar.xsl" />
	
	<xsl:output method="xml"/>
	<xsl:preserve-space elements="*" />       
	<xsl:template match="/">            
		<fo:root>        
			<fo:layout-master-set>      
				<fo:simple-page-master master-name="simple" page-height="21cm" page-width="29.6cm" margin-left="1cm" margin-right="1.5cm"> 
					<fo:region-body margin-top="2cm" margin-bottom="1.5cm"/> 
					<fo:region-before extent="2cm"/>  
					<fo:region-after extent="1cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1" id="last-page">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table inline-progression-dimension="100%" table-layout="fixed" margin-top="5pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(25)"/> 
						<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
						<fo:table-body space-before="6pt" space-after="2pt">
								<fo:table-row>
									<fo:table-cell>
										<fo:block>
											<fo:external-graphic content-width="scale-to-fit" width="100pt" content-height="100%" scaling="uniform" src="url(d2://images/company_logo.jpg)"/>
			 							</fo:block> 
									</fo:table-cell>							
					 				<fo:table-cell>
											<fo:table inline-progression-dimension="100%" table-layout="fixed" margin-top="16pt">
												<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
												<fo:table-body space-before="6pt" space-after="4pt">
									 				<fo:table-row>
					 	 								<fo:table-cell>
															<fo:block font-size="10pt" font-weight="bold" text-align="right">
			 													Daily Cost Summary Report
															</fo:block>										
														</fo:table-cell>
												 	</fo:table-row>
												</fo:table-body>
											</fo:table>
									</fo:table-cell>    
								</fo:table-row>   
						</fo:table-body> 
					</fo:table>
				</fo:static-content> 
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="10pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>  
								<fo:table-cell font-size="6pt"> 
									<fo:block text-align="left">
										Copyright IDS 2014, IDS_COST_REPORT_SINGLE
									</fo:block>    
								</fo:table-cell>     
								<fo:table-cell font-size="8pt">  
									<fo:block text-align="right">
										Page <fo:page-number/> of <fo:page-number-citation-last ref-id="last-page"/> 
									</fo:block> 
								</fo:table-cell>  
							</fo:table-row> 
							<fo:table-row>  
		                        <fo:table-cell font-size="6pt" number-columns-spanned="2">
		                           <fo:block text-align="left">Printed on
		                           <xsl:variable name="printdate" select="/root/modules/Daily/daydate"/>
		                           <xsl:value-of select="d2Utils:getSystemDate('dd MMM yyyy')"/>
		                           </fo:block>
		                        </fo:table-cell>   
		                     </fo:table-row> 
						</fo:table-body>     
					</fo:table>    
				</fo:static-content>  
				<fo:flow flow-name="xsl-region-body" font-size="6pt">  
					<fo:block>
						<fo:table width="100%" table-layout="fixed" border="0.5px solid black" margin-top="2pt">
							<fo:table-column column-number="1" column-width="proportional-column-width(35)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(65)"/>
							<fo:table-body>  
								<fo:table-row>		     
									<fo:table-cell>     
									<!-- main column 1 -->    
										<fo:table width="100%" table-layout="fixed" margin-top="4pt"> 
											<fo:table-body> 
												<fo:table-row> 
													<fo:table-cell padding-left="10px" padding-right="6px" font-size="9pt" font-weight="bold">
										                 <fo:block> WELL: <xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/completeOperationName"/> </fo:block>
										            </fo:table-cell> 
					 							</fo:table-row>       
													      
											<!-- DAILY COST HEADER -->   
											<xsl:if test="count(/root/modules/ReportDaily/ReportDaily) &gt; 0">
											 	<fo:table-row>
													<fo:table-cell padding-bottom="4px" padding-top="4px" padding-left="10px" padding-right="10px">
														<fo:block> 
									 						<xsl:apply-templates  
																select="/root/modules/ReportDaily/ReportDaily" mode="dailycostheader"/>
					 							 		</fo:block> 
						 							</fo:table-cell>     
												</fo:table-row>      
							 	 			</xsl:if>             
													        
											<!-- WELL STATUS-->  
											 <xsl:if test="count(/root//modules/Operation/Operation) &gt; 0">
											 	<fo:table-row>
													<fo:table-cell padding-bottom="4px" padding-top="4px" padding-left="10px" padding-right="10px">
							 							<fo:block>
								 							<xsl:apply-templates   
								 								select="/root//modules/Well/Well" mode="wellstatus"/>
														</fo:block>  
													</fo:table-cell>   
												</fo:table-row>      
											</xsl:if>       
											       
											<!-- HSE METRICS-->  
											 <xsl:if test="count(/root//modules/ReportDaily/ReportDaily) &gt; 0">
												<fo:table-row>
													<fo:table-cell padding-bottom="4px" padding-top="4px" padding-left="10px" padding-right="10px">
										 				<fo:block>
	 									 					<xsl:apply-templates  
		 										 				select="/root/modules/ReportDaily/ReportDaily" mode="hsemetrics"/>
											 			</fo:block> 
													</fo:table-cell>   
							 					</fo:table-row>           
											</xsl:if>           
										 	  
					 						<!-- WELL OBJECTIVES-->   
											 <xsl:if test="count(/root/modules/Well/Well) &gt; 0">
												<fo:table-row>
													<fo:table-cell padding-bottom="4px" padding-top="4px" padding-left="10px" padding-right="10px">
							 							<fo:block>
															<xsl:apply-templates  
																select="/root/modules/Well/Well" mode="wellobjectives"/>
														</fo:block>
													</fo:table-cell>  
												</fo:table-row>   
											</xsl:if>  
											    
											</fo:table-body> 
										</fo:table>  
									</fo:table-cell> 
									<!-- main column 2 -->	
									<fo:table-cell padding-bottom="4px" padding-top="7px" padding-left="0px" padding-right="5px">
										<fo:table width="100%" table-layout="fixed" margin-top="4pt"> 
											<fo:table-body>
									
									<!-- DEPTH graph --> 
									 <xsl:if test="count(/root/modules/Activity/Activity) &gt; 0">
										<fo:table-row>
											<fo:table-cell padding-bottom="4px" padding-top="4px" padding-left="5px" padding-right="5px">
					 							<fo:block>
													<xsl:apply-templates  
														select="/root/modules/Activity" mode="depthgraph"/>
												</fo:block>
											</fo:table-cell>  
										</fo:table-row> 
									</xsl:if>
										    
									<!-- NPT breakdown --> 
									<xsl:if test="count(/root/modules/EstimatedCostReviewReportDaily) &gt; 0">
										<fo:table-row>
											<fo:table-cell>
												<fo:block>
													<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="10pt">
														<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
														<fo:table-column column-number="2" column-width="proportional-column-width(70)"/>
															<fo:table-header>
																<fo:table-cell>
										 							<fo:block > 
																		<!-- header --> 
																	</fo:block>
																</fo:table-cell> 
														 	</fo:table-header>  
															<fo:table-body> 
																<fo:table-cell padding-bottom="4px" padding-top="1px" padding-left="2px" padding-right="1px">
										 							<fo:block>
																		<xsl:apply-templates  
																			select="/root/modules/EstimatedCostReviewReportDaily" mode="piechart"/>
																	</fo:block>
																</fo:table-cell>  
																<fo:table-cell padding-bottom="4px" padding-top="1px" padding-left="2px" padding-right="1px">
																	<fo:block text-align="center">
																		<xsl:apply-templates  
																			select="/root/modules/EstimatedCostReviewReportDaily" mode="bargraph"/>
																	</fo:block>
																</fo:table-cell>
															</fo:table-body>
														</fo:table>
													</fo:block>
												</fo:table-cell>
											</fo:table-row>  
											</xsl:if>
										</fo:table-body> 
									</fo:table>
									
								</fo:table-cell>
							</fo:table-row>				
						</fo:table-body>
					</fo:table>	   
				</fo:block>	 
			</fo:flow> 
		</fo:page-sequence>
	</fo:root>
	
</xsl:template> 

</xsl:stylesheet>