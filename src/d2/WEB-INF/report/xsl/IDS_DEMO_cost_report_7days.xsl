<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">	
	<xsl:output method="xml"/>
	<xsl:preserve-space elements="*"/>
	<xsl:variable name="dailyUid" select="/root/UserContext/UserSelection/dailyUid" />
	<xsl:variable name="reportDaily" select="/root/modules/ReportDaily/ReportDaily[dailyUid=$dailyUid]" />
	<xsl:variable name="dayDate" select="$reportDaily/reportDatetime/@epochMS" />
	<xsl:variable name="primaryCurrency" select="/root/modules/CostAfeMaster/dynaAttr/primaryCurrency"/>
	
	<!-- IDS_DEMO 7DAYS COST REPORT TEMPLATE -->
	
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" page-height="21cm" page-width="29.6cm" margin-left="1cm" margin-right="1.5cm">
					<fo:region-body margin-top="2.5cm" margin-bottom="1.5cm"/>
					<fo:region-before extent="2cm"/>
					<fo:region-after extent="1cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1" id="last-page">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="12pt" margin-top="5pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(70)"/>
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="10pt">
									<fo:block>
										<fo:external-graphic content-width="scale-to-fit" width="100pt" content-height="100%" scaling="uniform" src="url(d2://images/company_logo.jpg)"/>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="6pt">
									<xsl:call-template name="basicwellheader"/>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="12pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
						<fo:table-body space-before="6pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS 2000 IDS_COST_REPORT_7DAYS
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="right">
										Page <fo:page-number /> of <fo:page-number-citation-last ref-id="last-page"/> 
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
		                        <fo:table-cell font-size="6pt" number-columns-spanned="2">
		                           <fo:block text-align="left">Printed on
		                           <xsl:variable name="printdate" select="$reportDaily/reportDatetime"/>
		                           <xsl:value-of select="d2Utils:getLocalTimestamp('d MMMM yyyy hh:mm a (z)')" />
		                           </fo:block>
		                        </fo:table-cell>
		                     </fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" font-size="7pt">
					<fo:block>
						<fo:table width="100%" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
							<fo:table-body>
								<fo:table-row keep-together="always">
									<fo:table-cell number-columns-spanned="2">
					                  <fo:block text-align="center" font-weight="bold" font-size="12pt"
					                     space-after="8pt"> 7 Days Cost Report </fo:block>
			                        </fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<xsl:apply-templates select="/root/modules/CostAccountCodes/CostAccountCodes" mode="accountCodesMain" />
										<xsl:apply-templates select="/root/modules/CostDailysheet" mode="CostDailysheetHeader" />										
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>						
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	
	<xsl:template match="modules/CostDailysheet" mode="CostDailysheetHeader">
		<xsl:variable name="accountCode_1" select="CostDailysheet/accountCode"/>
		<xsl:variable name="afeShortDescription_1" select="CostDailysheet/afeShortDescription"/>
				
		<fo:block>			
			<xsl:for-each select="/root/modules/DailyCostItems/AccountCodes/AccountCode">	
				<xsl:sort select="accountCode" order="ascending" />
				<xsl:variable name="accountCode" select="accountCode"/>
				<xsl:variable name="afeShortDescription" select="afeShortDescription"/>
				<xsl:variable name="afeTotal" select="sum(/root/modules/CostAfeMaster/CostAfeMaster/CostAfeDetail[accountCode = $accountCode and shortDescription=$afeShortDescription]/itemTotal/@rawNumber)"/>
										
				<xsl:variable name="last7daydate" select="string($dayDate - (7*24*60*60*1000))" />
        		<xsl:variable name="last6daydate" select="string($dayDate - (6*24*60*60*1000))" />
        		<xsl:variable name="last5daydate" select="string($dayDate - (5*24*60*60*1000))" />
        		<xsl:variable name="last4daydate" select="string($dayDate - (4*24*60*60*1000))" />
        		<xsl:variable name="last3daydate" select="string($dayDate - (3*24*60*60*1000))" />
        		<xsl:variable name="last2daydate" select="string($dayDate - (2*24*60*60*1000))" />
        		<xsl:variable name="last1daydate" select="string($dayDate - (1*24*60*60*1000))" />
									
				<fo:table width="100%" table-layout="fixed" keep-together="always">
					<fo:table-column column-width="proportional-column-width(9)"/>
			       	<fo:table-column column-width="proportional-column-width(2)"/>
			       	<fo:table-column column-width="proportional-column-width(2)"/>
			       						
					<fo:table-header>
						<fo:table-row>
							<fo:table-cell font-weight="bold" border-bottom="0.5px solid black" padding="0.5mm">
								<fo:block>
									<xsl:value-of select="$accountCode"/> - <xsl:value-of select="$afeShortDescription"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell font-weight="bold" border-bottom="0.5px solid black" padding="0.5mm">
								<fo:block>
		 							<xsl:value-of select="d2Utils:formatDateFromEpochMS($dayDate,'dd MMM yyyy')"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell font-weight="bold" border-bottom="0.5px solid black" text-align="right" padding="0.5mm">
								<fo:block>
									AFE:   	
									<xsl:choose>
										<xsl:when test="number($afeTotal)">
											<xsl:value-of select="d2Utils:formatNumber(string($afeTotal),'###,###,##0.00')"/>
										</xsl:when>
										<xsl:otherwise>
											0.00
										</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
						
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="3">
								<fo:block>
									<xsl:call-template name="costDetails">
										<xsl:with-param name="last6daydate" select="$last6daydate"/>
										<xsl:with-param name="last5daydate" select="$last5daydate"/>
										<xsl:with-param name="last4daydate" select="$last4daydate"/>
										<xsl:with-param name="last3daydate" select="$last3daydate"/>
										<xsl:with-param name="last2daydate" select="$last2daydate"/>
										<xsl:with-param name="last1daydate" select="$last1daydate"/>
										<xsl:with-param name="accountCode" select="$accountCode"/>
										<xsl:with-param name="afeShortDescription" select="$afeShortDescription"/>
										<xsl:with-param name="afe_total" select="$afeTotal"/>
									</xsl:call-template>									
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
				
			</xsl:for-each>
				
		</fo:block>		
	</xsl:template>
	
	<xsl:template name="costDetails">
		<xsl:param name="last6daydate"/>
		<xsl:param name="last5daydate"/>
		<xsl:param name="last4daydate"/>
		<xsl:param name="last3daydate"/>
		<xsl:param name="last2daydate"/>
		<xsl:param name="last1daydate"/>
		<xsl:param name="afeShortDescription"/>
		<xsl:param name="accountCode"/>
		<xsl:param name="afe_total"/>
		<xsl:variable name="costdaily" select="/root/modules/CostDailysheet/CostDailysheet"/>
		
		<xsl:variable name="last1daydailyUid" select="/root/modules/Daily/Daily[dayDate/@epochMS = $last1daydate]/dailyUid"/>
		<xsl:variable name="last2daydailyUid" select="/root/modules/Daily/Daily[dayDate/@epochMS = $last2daydate]/dailyUid"/>
		<xsl:variable name="last3daydailyUid" select="/root/modules/Daily/Daily[dayDate/@epochMS = $last3daydate]/dailyUid"/>
		<xsl:variable name="last4daydailyUid" select="/root/modules/Daily/Daily[dayDate/@epochMS = $last4daydate]/dailyUid"/>
		<xsl:variable name="last5daydailyUid" select="/root/modules/Daily/Daily[dayDate/@epochMS = $last5daydate]/dailyUid"/>
		<xsl:variable name="last6daydailyUid" select="/root/modules/Daily/Daily[dayDate/@epochMS = $last6daydate]/dailyUid"/>
		
		<fo:table width="100%" table-layout="fixed" border-bottom="0.5px solid black" border-left="0.5px solid black" border-right="0.5px solid black">
			<fo:table-column column-width="proportional-column-width(3)"/>
	       	<fo:table-column column-width="proportional-column-width(1)"/>
	       	<fo:table-column column-width="proportional-column-width(1)"/>
	       	<fo:table-column column-width="proportional-column-width(1)"/>
	       	<fo:table-column column-width="proportional-column-width(1)"/>
	       	<fo:table-column column-width="proportional-column-width(1)"/>
	       	<fo:table-column column-width="proportional-column-width(1)"/>
	       	<fo:table-column column-width="proportional-column-width(1)"/>
	       	<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
         
         	<fo:table-header>
         		<fo:table-row>
					<fo:table-cell padding-top="0.5mm" padding-left="0.5mm">
						<fo:block color="#aaaaaa"> Description 
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="0.5mm" padding-right="0.5mm">
						<fo:block text-align="right" color="#aaaaaa">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(string($dayDate - (6*24*60*60*1000)),'dd MMM yyyy')" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="0.5mm" padding-right="0.5mm">
						<fo:block text-align="right" color="#aaaaaa">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(string($dayDate - (5*24*60*60*1000)),'dd MMM yyyy')" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="0.5mm" padding-right="0.5mm">
						<fo:block text-align="right" color="#aaaaaa">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(string($dayDate - (4*24*60*60*1000)),'dd MMM yyyy')" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="0.5mm" padding-right="0.5mm">
						<fo:block text-align="right" color="#aaaaaa">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(string($dayDate - (3*24*60*60*1000)),'dd MMM yyyy')" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="0.5mm" padding-right="0.5mm">
						<fo:block text-align="right" color="#aaaaaa">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(string($dayDate - (2*24*60*60*1000)),'dd MMM yyyy')" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="0.5mm" padding-right="0.5mm">
						<fo:block text-align="right" color="#aaaaaa">
							<xsl:value-of select="d2Utils:formatDateFromEpochMS(string($dayDate - (24*60*60*1000)),'dd MMM yyyy')" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="0.5mm" border-left="0.5px solid black">
						<fo:block text-align="center" color="#aaaaaa"> Quantity </fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="0.5mm">
						<fo:block text-align="right" color="#aaaaaa"> 
							Item Cost
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="0.5mm">
						<fo:block text-align="right" color="#aaaaaa"> 
							Item Total 
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-top="0.5mm" padding-right="0.5mm">
						<fo:block text-align="right" color="#aaaaaa"> 
							Cost To Date (<xsl:value-of select="$primaryCurrency"/>) 
						</fo:block>
					</fo:table-cell>               
				</fo:table-row>
         	</fo:table-header>
                 
			<fo:table-body>
         		<xsl:for-each select="/root/modules/DailyCostItems/DailyCostItem[accountCode = $accountCode and afeShortDescription = $afeShortDescription]">
					<xsl:sort select="afeItemDescription" order="ascending" />
					<xsl:variable name="itemDescription" select="itemDescription"/>
					<xsl:variable name="afeItemDescription" select="afeItemDescription"/>
					<xsl:variable name="costToDate" select="costToDate"/>
									
					<fo:table-row font-size="8pt">
						<fo:table-cell padding-left="2px">																
							<fo:block text-align="left">
								<xsl:value-of select="afeItemDescription"/>
								 <xsl:if test="string-length(itemDescription) &gt; 0"> -
                             			<xsl:value-of select="itemDescription"/>
                       			</xsl:if>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-right="0.5mm">																
							<fo:block text-align="right">
								<xsl:variable name="itemTotal" select="sum($costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid = $last6daydailyUid]/itemTotal/@rawNumber)" />
								<xsl:choose>
									<xsl:when test="$last6daydailyUid  and ($itemTotal &gt; 0)">
										<xsl:value-of select="$primaryCurrency"/>
										<xsl:value-of select="d2Utils:formatNumber(string($itemTotal), '###,###,##0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<fo:block text-align="center">-</fo:block>
									</xsl:otherwise>								
								</xsl:choose>
							</fo:block>															
						</fo:table-cell>
													
						<fo:table-cell padding-right="0.5mm">																
							<fo:block text-align="right">
								<xsl:variable name="itemTotal" select="sum($costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid = $last5daydailyUid]/itemTotal/@rawNumber)" />
								<xsl:choose>
									<xsl:when test="$last5daydailyUid  and ($itemTotal &gt; 0)">
										<xsl:value-of select="$primaryCurrency"/>
										<xsl:value-of select="d2Utils:formatNumber(string($itemTotal), '###,###,##0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<fo:block text-align="center">-</fo:block>
									</xsl:otherwise>								
								</xsl:choose>
							</fo:block>															
						</fo:table-cell>
						
				       	<fo:table-cell padding-right="0.5mm">																
							<fo:block text-align="right">
								<xsl:variable name="itemTotal" select="sum($costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid = $last4daydailyUid]/itemTotal/@rawNumber)" />
								<xsl:choose>
									<xsl:when test="$last4daydailyUid and ($itemTotal &gt; 0)">
										<xsl:value-of select="$primaryCurrency"/>
										<xsl:value-of select="d2Utils:formatNumber(string($itemTotal), '###,###,##0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<fo:block text-align="center">-</fo:block>
									</xsl:otherwise>								
								</xsl:choose>
							</fo:block>															
						</fo:table-cell>
						
				       	<fo:table-cell padding-right="0.5mm">																
							<fo:block text-align="right">
								<xsl:variable name="itemTotal" select="sum($costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid = $last3daydailyUid]/itemTotal/@rawNumber)" />
								<xsl:choose>
									<xsl:when test="$last3daydailyUid and ($itemTotal &gt; 0)">
										<xsl:value-of select="$primaryCurrency"/>
										<xsl:value-of select="d2Utils:formatNumber(string($itemTotal), '###,###,##0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<fo:block text-align="center">-</fo:block>
									</xsl:otherwise>								
								</xsl:choose>
							</fo:block>															
						</fo:table-cell>
						
				       	<fo:table-cell padding-right="0.5mm">																
							<fo:block text-align="right">
								<xsl:variable name="itemTotal" select="sum($costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid = $last2daydailyUid]/itemTotal/@rawNumber)" />
								<xsl:choose>
									<xsl:when test="$last2daydailyUid and ($itemTotal &gt; 0)">
										<xsl:value-of select="$primaryCurrency"/>
										<xsl:value-of select="d2Utils:formatNumber(string($itemTotal), '###,###,##0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<fo:block text-align="center">-</fo:block>
									</xsl:otherwise>								
								</xsl:choose>
							</fo:block>															
						</fo:table-cell>
						
				       	<fo:table-cell padding-right="0.5mm">																
							<fo:block text-align="right">
								<xsl:variable name="itemTotal" select="sum($costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid = $last1daydailyUid]/itemTotal/@rawNumber)" />
								<xsl:choose>
									<xsl:when test="$last1daydailyUid and ($itemTotal &gt; 0)">
										<xsl:value-of select="$primaryCurrency"/>
										<xsl:value-of select="d2Utils:formatNumber(string($itemTotal), '###,###,##0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<fo:block text-align="center">-</fo:block>
									</xsl:otherwise>								
								</xsl:choose>
							</fo:block>															
						</fo:table-cell>
						
						<fo:table-cell border-left="0.5px solid black">	
							<fo:block text-align="center">
								<xsl:variable name="quantity" select="sum($costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid=$dailyUid]/quantity/@rawNumber)" /> 
								<xsl:choose>
									<xsl:when test="number($quantity)">
										<xsl:value-of select="d2Utils:formatNumber(string($quantity),'###,###,##0.00')"/>
									</xsl:when>
									<xsl:otherwise> - </xsl:otherwise>
								</xsl:choose>
							</fo:block>														
						</fo:table-cell>
						<fo:table-cell>	
							<fo:block text-align="right">
								<xsl:variable name="itemCost" select="$costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid=$dailyUid]/itemCost/@rawNumber" />
								<xsl:variable name="quantity" select="sum($costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid=$dailyUid]/quantity/@rawNumber)" /> 
								<xsl:choose>
									<xsl:when test="number($itemCost) and number($quantity)">
										<xsl:value-of select="$costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid=$dailyUid]/currency"/>
										<xsl:value-of select="d2Utils:formatNumber(string($itemCost),'###,###,##0.00')"/>
									</xsl:when>
									<xsl:otherwise> - </xsl:otherwise>
								</xsl:choose>
							</fo:block>														
						</fo:table-cell>
						<fo:table-cell>	
							<fo:block text-align="right">
								<xsl:variable name="quantity" select="sum($costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid=$dailyUid]/quantity/@rawNumber)" />
								<xsl:variable name="itemCost" select="$costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid=$dailyUid]/itemCost/@rawNumber" />
								<xsl:choose>
									<xsl:when test="number($itemCost) and number($quantity)">
										<xsl:value-of select="$costdaily[afeShortDescription=$afeShortDescription and afeItemDescription=$afeItemDescription and itemDescription = $itemDescription and dailyUid=$dailyUid]/currency"/>
										<xsl:value-of select="d2Utils:formatNumber(string($itemCost * $quantity),'###,###,##0.00')"/>
									</xsl:when>
									<xsl:otherwise> - </xsl:otherwise>
								</xsl:choose>
							</fo:block>														
						</fo:table-cell>
						<fo:table-cell padding-right="0.5mm">	
							<fo:block text-align="right">
								<xsl:value-of select="d2Utils:formatNumber(string($costToDate), '###,###,##0.00')"/>
							</fo:block>
						</fo:table-cell>													
					</fo:table-row>
								
				</xsl:for-each>
				
				<fo:table-row>
	               <fo:table-cell number-columns-spanned="7">
	                  <fo:block color="white"> ! </fo:block>
	               </fo:table-cell>
	               <fo:table-cell text-align="center" border-left="0.5px solid black" border-top="0.5px solid black" border-bottom="0.5px solid black">
	                  <fo:block color="white"> ! </fo:block>
	               </fo:table-cell>
	               <fo:table-cell text-align="right" border-top="0.5px solid black" border-bottom="0.5px solid black">
	                  <fo:block color="white"> ! </fo:block>
	               </fo:table-cell>
	               <fo:table-cell text-align="right" border-top="0.5px solid black" border-bottom="0.5px solid black" padding="0.5mm">
	                  <fo:block>
	                  		<xsl:value-of select="$primaryCurrency"/>
							<xsl:variable name="dailyItemTotalForEachAccountCode" select="sum($costdaily[accountCode = $accountCode and afeShortDescription=$afeShortDescription and dailyUid=$dailyUid]/itemTotal/@rawNumber)"/>				
	                  		<xsl:value-of select="d2Utils:formatNumber(string($dailyItemTotalForEachAccountCode), '###,###,##0.00')"/>
	                  </fo:block>
	               </fo:table-cell>
	               <fo:table-cell text-align="right" border-top="0.5px solid black" border-bottom="0.5px solid black" padding="0.5mm">
	                  <fo:block>
	                  		<xsl:variable name="costToDateForEachAccountcode" select="sum(/root/modules/DailyCostItems/DailyCostItem[accountCode=$accountCode and afeShortDescription=$afeShortDescription]/costToDate)"/>				
	                  		<xsl:value-of select="d2Utils:formatNumber(string($costToDateForEachAccountcode), '###,###,##0.00')"/>
	                  </fo:block>
	               </fo:table-cell>

	            </fo:table-row>
	            
	            <fo:table-row>
	            	<xsl:variable name="afeTotal" select="$afe_total"/>
	            	<xsl:variable name="totalCostToDate" select="sum(/root/modules/DailyCostItems/DailyCostItem[accountCode=$accountCode and afeShortDescription=$afeShortDescription]/costToDate)"/>				
	            	<xsl:variable name="afeDiff" select="$afeTotal - $totalCostToDate"/>
	            	
	               <fo:table-cell number-columns-spanned="9" padding="0.5mm">
	                  <fo:block color="white"> ! </fo:block>
	                  <xsl:value-of select="$afeTotal"/>
	               </fo:table-cell>
	               <fo:table-cell padding="0.5mm">
	                  <fo:block text-align="right" color="#aaaaaa"> Diff To AFE: </fo:block>
	               </fo:table-cell>
	               <fo:table-cell text-align="right" padding="0.5mm">
	                  <xsl:choose>
	                    <xsl:when test="$afeDiff &gt;= 0">
	                        <fo:block>
	                        	<xsl:value-of select="d2Utils:formatNumber(string($afeDiff),'###,###,##0.00')"/>
	                        </fo:block>
	                     </xsl:when>
	                     <xsl:otherwise>
	                        <fo:block color="red" font-weight="bold">
	                        	<xsl:value-of select="d2Utils:formatNumber(string($afeDiff),'###,###,##0.00')"/>
	                        </fo:block>
	                     </xsl:otherwise>
	                  </xsl:choose>
	               </fo:table-cell>
	            </fo:table-row>
			</fo:table-body>
		</fo:table>
		
		
	<fo:block color="white">!!</fo:block>
	</xsl:template>
	
	<xsl:template name="basicwellheader">	 	
		<xsl:variable name="welldata" select="/root/modules/Well/Well"/>
		<xsl:variable name="primary_currency" select="/root/modules/CostAfeMaster/dynaAttr/primaryCurrency"/>
		<xsl:variable name="afeTotal" select="string(sum(/root/modules/CostAfeMaster/CostAfeMaster/CostAfeDetail/itemTotal/@rawNumber))"/>
		<xsl:variable name="wellCostToDate" select="/root/modules/ReportDaily/ReportDaily[dailyUid = $dailyUid]/dynaAttr/cumcost/@rawNumber"/>
	 	<xsl:variable name="amountSpentPriorToSpud" select="sum(/root/modules/Operation/Operation/amountSpentPriorToSpud/@rawNumber)"/>
	 	<!-- <xsl:variable name="wellTotal" select="$wellCostToDate + $amountSpentPriorToSpud"/> -->
		<fo:table width="100%" table-layout="fixed" space-after="0.5cm" border-bottom="0.5px solid black">
			<fo:table-column column-width="proportional-column-width(0.5)"/>
			<fo:table-column column-width="proportional-column-width(1.5)"/>
			<fo:table-column column-width="proportional-column-width(0.8)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-body>
				<fo:table-row font-size="8pt" font-weight="bold">
					<fo:table-cell number-columns-spanned="4">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row font-size="8pt" font-weight="bold">
					<fo:table-cell padding="0.5mm">
						<fo:block> Wellname: </fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm">
						<fo:block>
						   <xsl:value-of select="$welldata/wellName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="0.5mm">
						<fo:block>
							<xsl:value-of select="$welldata/afetype"/> Total AFE: </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="0.5mm">
						<fo:block>
							<xsl:value-of select="/root/modules/CostAfeMaster/dynaAttr/primaryCurrency"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="d2Utils:formatNumber(string($afeTotal),'###,###,##0.00')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row font-size="8pt" font-weight="bold">
					<fo:table-cell padding="0.5mm">
						<fo:block> Rig: </fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm">
						<fo:block>
							<xsl:value-of select="/root/modules/RigInformation/RigInformation[rigInformationUid=$reportDaily/rigInformationUid]/rigName"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="0.5mm">
						<fo:block>Total Cost To Date: </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="0.5mm">
						<fo:block>
							<xsl:value-of select="$primary_currency"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="d2Utils:formatNumber(string($wellCostToDate),'###,###,##0.00')"/>
						</fo:block>
					</fo:table-cell>
            	</fo:table-row>
				<fo:table-row font-size="8pt" font-weight="bold">
					<fo:table-cell padding="0.5mm">
						<fo:block>Date: </fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm">
						<fo:block><xsl:value-of select="d2Utils:formatDateFromEpochMS($reportDaily/reportDatetime/@epochMS,'dd MMM yyyy')"/> (Day #<xsl:value-of
				   	   	select="$reportDaily/reportNumber"/>) </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="0.5mm">
						<fo:block>Diff (+/-): </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="0.5mm">
						<xsl:variable name="afe_actual_diff" select="string($afeTotal - $wellCostToDate)" />
						<xsl:choose>
							<xsl:when test="$afe_actual_diff &gt;= 0">
								<fo:block> 
									<xsl:value-of select="$primary_currency"/><fo:inline color="white">.</fo:inline>
									<xsl:value-of select="d2Utils:formatNumber(string($afe_actual_diff),'###,###,##0.00')"/>
								</fo:block>
							</xsl:when>
							<xsl:otherwise>
								<fo:block color="red">
									<xsl:choose>
										<xsl:when test="number($afe_actual_diff)">
											<xsl:value-of select="$primary_currency"/>
											<xsl:value-of select="d2Utils:formatNumber(string($afe_actual_diff),'###,###,##0.00')"/>
										</xsl:when>
										<xsl:otherwise>$0.00</xsl:otherwise>
									</xsl:choose>
								</fo:block>
							</xsl:otherwise>
						</xsl:choose>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row font-size="8pt" font-weight="bold">
					<fo:table-cell padding="0.5mm">
						<fo:block color="white">!</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.5mm">
					   <fo:block color="white">!</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding="0.5mm">
						<fo:block>Total Day Cost: </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" padding="0.5mm">
						<fo:block>
							<xsl:variable name="dayTotal" select="string(sum(/root/modules/CostDailysheet/CostDailysheet[dailyUid = $dailyUid]/itemTotal/@rawNumber))"/>
							<xsl:value-of select="$primary_currency"/><fo:inline color="white">.</fo:inline>
							<xsl:value-of select="d2Utils:formatNumber(string($dayTotal),'###,###,##0.00')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
</xsl:stylesheet>
