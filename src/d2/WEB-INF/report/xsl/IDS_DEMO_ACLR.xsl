<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
    <xsl:output method="xml"/>
    <xsl:preserve-space elements="*"/>
    <xsl:template match="/">
    <fo:root>
        <fo:layout-master-set>
            <fo:simple-page-master master-name="simple" page-height="21cm" page-width="29cm" margin-left="1cm" margin-right="1cm" margin-top="1cm">
             	<fo:region-body margin-top="1cm" margin-bottom="2cm"/>
               	<fo:region-before extent="1cm"/>
               	<fo:region-after extent="2cm"/>
            </fo:simple-page-master>
        </fo:layout-master-set>
        <fo:page-sequence master-reference="simple" initial-page-number="1">
        	<fo:static-content flow-name="xsl-region-before">
        		<fo:table inline-progression-dimension="27cm" table-layout="fixed" space-after="1pt">
        			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
        			<fo:table-body>
                        <fo:table-row>
							<fo:table-cell font-size="12pt" text-align="center">
                                <fo:block>ACL GROUP REPORT for <xsl:value-of select="/root/UserContext/UserSelection/groupUid"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
        			</fo:table-body>
        		</fo:table>
        	</fo:static-content>
        	<fo:static-content flow-name="xsl-region-after">
        		<fo:table inline-progression-dimension="27cm" table-layout="fixed" space-after="12pt">
        			<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
        			<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
        			<fo:table-body space-before="1pt" space-after="3pt">
        				<fo:table-row>
        					<fo:table-cell font-size="6pt">
        						<fo:block text-align="left">
        							Copyright IDS, 20141125, IDS_DEMO_ACLR
        						</fo:block>
        					</fo:table-cell>
        					<fo:table-cell font-size="8pt">
        						<fo:block text-align="right">
        							Page <fo:page-number/>
        						</fo:block>
        					</fo:table-cell>
        				</fo:table-row>
        			</fo:table-body>
        		</fo:table>
        	</fo:static-content>
        	<fo:flow flow-name="xsl-region-body" font-size="8pt">
        		<fo:block>
					<xsl:apply-templates select="/root/modules/AclList"/>
        		</fo:block>
        	</fo:flow>
        </fo:page-sequence>
        </fo:root>
        </xsl:template>
		
		
	<xsl:template match="modules/AclList">
		<fo:table inline-progression-dimension="27cm" table-layout="fixed" border="0.5px solid black" space-after="2pt" font-size="8pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(4)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(4)"/>
			<xsl:for-each select="/root/modules/AclGroup/AclGroup">	
				<fo:table-column/>
			</xsl:for-each>
       		<fo:table-header>
        		<fo:table-row font-size="7pt">
          			<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="center" background-color="rgb(224,224,224)"  display-align="after">
              			<fo:block>Module</fo:block>
          			</fo:table-cell>
          			<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="center" background-color="rgb(224,224,224)"  display-align="after">
              			<fo:block>Screen</fo:block>
          			</fo:table-cell>
          			<xsl:for-each select="/root/modules/AclGroup/AclGroup">	
						<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="center" background-color="rgb(224,224,224)">
              				<xsl:call-template name="text90">
									<xsl:with-param name="text" select="name"/>
							</xsl:call-template>
          				</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
            </fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="AclList"/>
            </fo:table-body>
        </fo:table>
    </xsl:template>
	
	<xsl:template match="AclList">
		<fo:table-row font-size="7pt">
			<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="left">
				<fo:block><xsl:value-of select="module"/></fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="left">
				<fo:block><xsl:value-of select="screen"/></fo:block>
			</fo:table-cell>
			<xsl:variable name="controller" select="controller"></xsl:variable>
        	<xsl:for-each select="/root/modules/AclGroup/AclGroup">	
        		<xsl:variable name="aclGroupUid" select="aclGroupUid"></xsl:variable>
        		<xsl:variable name="permissions" select="/root/modules/AclGroup/AclGroup/AclEntry[aclGroupUid=$aclGroupUid and beanName=$controller]/permissions"></xsl:variable>
        		<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="center">
					<fo:block>
						<xsl:choose>
							<xsl:when test="$permissions &gt; 0">
								Y
							</xsl:when>
							<xsl:otherwise>
							</xsl:otherwise>
						</xsl:choose>
					</fo:block>
				</fo:table-cell>
        	</xsl:for-each>
		</fo:table-row>
	</xsl:template>
	
	<xsl:template name="text90">
		<xsl:param name="text"/>
	   <fo:block-container 
	        reference-orientation="90" display-align="center"
	        inline-progression-dimension.minimum="5mm" 
	        inline-progression-dimension.optimum="40mm" 
	        inline-progression-dimension.maximum="auto"> 
	        <fo:block hyphenate="true"  font-weight="bold"><xsl:value-of select="$text"/></fo:block> 
		</fo:block-container> 
	</xsl:template>	
	
</xsl:stylesheet>
