<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
    
    <xsl:output method="xml" />
    <xsl:preserve-space elements="*" />
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
					<fo:region-before extent="3cm"/>
					<fo:region-after extent="1.5cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
            <fo:page-sequence master-reference="simple" initial-page-number="1">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="4pt">
                        <fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
                        <fo:table-column column-number="2" column-width="proportional-column-width(70)"/>                      
                        <fo:table-body space-before="10pt" space-after="2pt">
                           <fo:table-row>
                               <fo:table-cell font-size="12pt">
                                   <fo:block>                                    
                                       <fo:external-graphic content-width="scale-to-fit"
                                           width="100pt" content-height="50%" scaling="uniform" src="url(d2://images/company_logo.jpg)"/>                                       
                                   </fo:block>
                               </fo:table-cell>                            
                                <fo:table-cell  font-weight="bold"  font-size="12pt" text-align="left">
                                    <fo:block>
                                        Look Ahead for Well <xsl:value-of select="//modules/Well/Well/wellName"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                </fo:static-content>
                <fo:static-content flow-name="xsl-region-after">
                    <fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="12pt">
                        <fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
                        <fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
                        <fo:table-body space-before="10pt" space-after="10pt">
                            <fo:table-row>
                                <fo:table-cell font-size="10pt">
                                    <fo:block text-align="left">
                                        Copyright IDS, 2008, LOOK_AHEAD
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell font-size="8pt">
                                    <fo:block text-align="right">
                                        Page <fo:page-number/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body" font-size="8pt">
                    <fo:block>
                        7 Days Look Ahead Planning.
                        <fo:table inline-progression-dimension="19cm"  table-layout="fixed">
                            <fo:table-column column-number="1" column-width="proportional-column-width(100)" />
                            <fo:table-body>	  
                                <xsl:apply-templates select="/root/modules/OperationPlanning/OperationPlanTask"/>
                            </fo:table-body>
                        </fo:table>   
                    </fo:block>  
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template> 
    <!-- Testing -->
    <xsl:template match="/root/modules/OperationPlanning/OperationPlanTask">
        <fo:table-row keep-together="always">
            <fo:table-cell padding-bottom="2px" padding-top="2px">
                <fo:block>
                    <fo:table width="100%" table-layout="fixed" font-size="10pt" font-weight="bold" border="0.5px solid black" wrap-option="wrap" table-omit-header-at-break="true" keep-together="always" break-after="page">
                        <fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
                        <fo:table-column column-number="2" column-width="proportional-column-width(30)"/>	
                        <fo:table-header>
                       		<fo:table-row>  
                                <fo:table-cell  padding="0.1cm" border-left="0.5px solid black" border-bottom="0.5px solid black" border-right="0.5px solid black"  number-columns-spanned="2" background-color="#CCCCCC">
                                    <fo:block> Date : <xsl:value-of select="dayDate"/></fo:block>
                                </fo:table-cell>                                        
                            </fo:table-row> 
                        </fo:table-header>
                        <fo:table-body>
                             
                            <fo:table-row background-color="rgb(224,224,224)">  
                                <fo:table-cell  padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black"  border-left="0.5px solid black" color="blue">
                                    <fo:block>00:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell  padding="0.1cm" border-bottom="0.5px solid black" wrap-option="wrap">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d0000)=''">
												<xsl:value-of select="r0000"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d0000"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell  padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>01:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell  padding="0.1cm"  border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d0100)=''">
												<xsl:value-of select="r0100"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d0100"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row background-color="rgb(224,224,224)">  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>02:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d0200)=''">
												<xsl:value-of select="r0200"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d0200"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>03:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d0300)=''">
												<xsl:value-of select="r0300"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d0300"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row background-color="rgb(224,224,224)">  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>04:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
										<xsl:choose>													
											<xsl:when test = "string(d0400)=''">
												<xsl:value-of select="r0400"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d0400"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>05:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d0500)=''">
												<xsl:value-of select="r0500"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d0500"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row background-color="rgb(224,224,224)">  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>06:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d0600)=''">
												<xsl:value-of select="r0600"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d0600"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>07:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d0700)=''">
												<xsl:value-of select="r0700"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d0700"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row background-color="rgb(224,224,224)">  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>08:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d0800)=''">
												<xsl:value-of select="r0800"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d0800"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>09:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
										<xsl:choose>													
											<xsl:when test = "string(d0900)=''">
												<xsl:value-of select="r0900"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d0900"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row background-color="rgb(224,224,224)">  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>10:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
										<xsl:choose>													
											<xsl:when test = "string(d1000)=''">
												<xsl:value-of select="r1000"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d1000"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>11:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d1100)=''">
												<xsl:value-of select="r1100"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d1100"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row  background-color="rgb(224,224,224)">  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>12:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d1200)=''">
												<xsl:value-of select="r1200"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d1200"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>13:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d1300)=''">
												<xsl:value-of select="r1300"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d1300"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row  background-color="rgb(224,224,224)">  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>14:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d1400)=''">
												<xsl:value-of select="r1400"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d1400"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>15:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d1500)=''">
												<xsl:value-of select="r1500"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d1500"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row  background-color="rgb(224,224,224)">  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>16:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d1600)=''">
												<xsl:value-of select="r1600"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d1600"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>17:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d1700)=''">
												<xsl:value-of select="r1700"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d1700"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row  background-color="rgb(224,224,224)">  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>18:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d1800)=''">
												<xsl:value-of select="r1800"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d1800"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>19:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d1900)=''">
												<xsl:value-of select="r1900"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d1900"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row  background-color="rgb(224,224,224)">  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>20:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d2000)=''">
												<xsl:value-of select="r2000"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d2000"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>21:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d2100)=''">
												<xsl:value-of select="r2100"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d2100"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row  background-color="rgb(224,224,224)">  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>22:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d2200)=''">
												<xsl:value-of select="r2200"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d2200"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black" color="blue">
                                    <fo:block>23:00</fo:block>
                                </fo:table-cell>                                        
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black">
                                    <fo:block>
                                    	<xsl:choose>													
											<xsl:when test = "string(d2300)=''">
												<xsl:value-of select="r2300"/>									
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="d2300"/>
											</xsl:otherwise>
										</xsl:choose>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>  
                            <fo:table-row>  
                                <fo:table-cell padding="0.1cm" border-bottom="0.5px solid black" border-right="0.5px solid black"  number-columns-spanned="2"  font-size="10pt" font-weight="bold" background-color="#CCCCCC">
                                    <fo:block> Requirement</fo:block>
                                </fo:table-cell>                                        
                            </fo:table-row>  
                            <xsl:apply-templates select="Requirement"/>
                        </fo:table-body>  
                    </fo:table>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>               
    </xsl:template>       
    
    <xsl:template match="Requirement">
        <fo:table-row>  
            <fo:table-cell padding="0.1cm" font-size="8pt" number-columns-spanned="2" border-bottom="0.5px solid black">
                <fo:block> <xsl:value-of select="Requirement"/></fo:block>
            </fo:table-cell>                                        
        </fo:table-row>  
    </xsl:template>  
</xsl:stylesheet>