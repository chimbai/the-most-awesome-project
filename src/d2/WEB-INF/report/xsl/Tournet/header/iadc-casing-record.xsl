<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:variable name="max_make_count" select="15"/>	
	<!-- IADC Casing -->
	<xsl:template name="iadc_casing_record">
		<xsl:param name="totalTour"/>
		<xsl:variable name="row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'4.9mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'5.5mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="today" select="//root/modules/ReportDaily/ReportDaily/reportDatetime/@epochMS" xml:base=""/>
		<xsl:variable name="installdate" select="//root/modules/CasingSection/TourCasingSection[installStartDate/@epochMS &lt;= $today]"/>
		<xsl:variable name="newinstalldate">
			<xsl:for-each select="$installdate">
				<xsl:sort select="installStartDate/@epochMS" order="descending"/>
				<xsl:if test="position() = 1">
					<xsl:value-of select="installStartDate/@epochMS"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="selected_record" select="//root/modules/CasingSection/TourCasingSection[installStartDate/@epochMS &lt;= $newinstalldate]" xml:base=""/>
		<xsl:variable name="total_record" select="count(//root/modules/CasingSection/TourCasingSection[installStartDate/@epochMS &lt;= $newinstalldate]/tourCasingSectionUid)" xml:base=""/>
		<xsl:variable name="blank_record" select="3-$total_record"/>		
		<fo:table table-layout="fixed" width="100%" font-weight="bold">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-body>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="1px solid black" border-top="0.5px solid black" number-rows-spanned="4" vertical-align="middle" text-align="center" padding="2px" display-align="center">
						<fo:block font-weight="bold" vertical-align="middle">
							LAST CASING TUBING <fo:block> OR LINER </fo:block>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" border-top="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> SIZE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" border-top="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> MAKE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" border-top="0.5px solid black" text-align="center" number-columns-spanned="2" display-align="center">
						<fo:block padding="2px"> WEIGHT &amp; GRADE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" border-top="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> NO. </fo:block>
						<fo:block> JOINTS </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" border-top="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> LENGTH </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" border-top="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> RKB. TO </fo:block>
						<fo:block> CSG. HD. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" border-top="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> SET AT </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:for-each select="$selected_record">
					<xsl:sort select="installStartDate/@epochMS" order="descending"/>
					<xsl:choose>
						<xsl:when test="position() &lt;= 3">
							<fo:table-row>
						<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
						<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
							<xsl:choose>
					          <xsl:when test="position() = 3">
					          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
					          </xsl:when>
					          <xsl:otherwise>
					          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
					          </xsl:otherwise>
					        </xsl:choose>
				        	<fo:block>
								<xsl:value-of select="casingOd/@lookupLabel"/>
								<fo:inline color="white">!</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
							<xsl:choose>
					          <xsl:when test="position() = 3">
					          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
					          </xsl:when>
					          <xsl:otherwise>
					          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
					          </xsl:otherwise>
					        </xsl:choose>
				        	<fo:block>
								<xsl:value-of select="substring(make,1,$max_make_count)"/>
								<fo:inline color="white">!</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
							<xsl:choose>
					          <xsl:when test="position() = 3">
					          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
					          </xsl:when>
					          <xsl:otherwise>
					          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
					          </xsl:otherwise>
					        </xsl:choose>
				        	<fo:block>
								<xsl:value-of select="weightLandingMassPerLength"/>
								<fo:inline color="white">!</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
							<xsl:choose>
					          <xsl:when test="position() = 3">
					          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
					          </xsl:when>
					          <xsl:otherwise>
					          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
					          </xsl:otherwise>
					        </xsl:choose>
				        	<fo:block>
								<xsl:value-of select="grade"/>
								<fo:inline color="white">!</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
							<xsl:choose>
					          <xsl:when test="position() = 3">
					          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
					          </xsl:when>
					          <xsl:otherwise>
					          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
					          </xsl:otherwise>
					        </xsl:choose>
				        	<fo:block>
								<xsl:value-of select="totaljointsInhole"/>
								<fo:inline color="white">!</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
							<xsl:choose>
					          <xsl:when test="position() = 3">
					          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
					          </xsl:when>
					          <xsl:otherwise>
					          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
					          </xsl:otherwise>
					        </xsl:choose>
				        	<fo:block>
								<xsl:value-of select="totallength"/>
								<fo:inline color="white">!</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
							<xsl:choose>
					          <xsl:when test="position() = 3">
					          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
					          </xsl:when>
					          <xsl:otherwise>
					          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
					          </xsl:otherwise>
					        </xsl:choose>
				        	<fo:block>
								<xsl:value-of select="rtCsgheadLength"/>
								<fo:inline color="white">!</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
							<xsl:choose>
					          <xsl:when test="position() = 3">
					          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
					          </xsl:when>
					          <xsl:otherwise>
					          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
					          </xsl:otherwise>
					        </xsl:choose>
				        	<fo:block>
								<xsl:value-of select="casingLandedDepthMdMsl"/>
								<fo:inline color="white">!</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
				<xsl:call-template name="for-loop-casing-blank">
					<xsl:with-param name="max" select="$blank_record"/>
					<xsl:with-param name="row_height" select="$row_height"/>
				</xsl:call-template>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- IADC Casing END -->
 
	<xsl:template name="for-loop-casing-blank">
    <xsl:param name="count" select="1"/>
    <xsl:param name="row_height"/>
    <xsl:param name="max"/>
	    <xsl:if test="$max >= $count">
	       	<fo:table-row>
	       		<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
				<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
					<xsl:choose>
			          <xsl:when test="$max = $count">
			          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
			          </xsl:when>
			          <xsl:otherwise>
			          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
			          </xsl:otherwise>
			        </xsl:choose>
		        	<fo:block>
						<fo:inline color="white">!</fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
					<xsl:choose>
			          <xsl:when test="$max = $count">
			          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
			          </xsl:when>
			          <xsl:otherwise>
			          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
			          </xsl:otherwise>
			        </xsl:choose>
		        	<fo:block>
						<fo:inline color="white">!</fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
					<xsl:choose>
			          <xsl:when test="$max = $count">
			          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
			          </xsl:when>
			          <xsl:otherwise>
			          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
			          </xsl:otherwise>
			        </xsl:choose>
		        	<fo:block>
						<fo:inline color="white">!</fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
					<xsl:choose>
			          <xsl:when test="$max = $count">
			          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
			          </xsl:when>
			          <xsl:otherwise>
			          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
			          </xsl:otherwise>
			        </xsl:choose>
		        	<fo:block>
						<xsl:value-of select="grade"/>
						<fo:inline color="white">!</fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
					<xsl:choose>
			          <xsl:when test="$max = $count">
			          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
			          </xsl:when>
			          <xsl:otherwise>
			          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
			          </xsl:otherwise>
			        </xsl:choose>
		        	<fo:block>
						<fo:inline color="white">!</fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
					<xsl:choose>
			          <xsl:when test="$max = $count">
			          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
			          </xsl:when>
			          <xsl:otherwise>
			          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
			          </xsl:otherwise>
			        </xsl:choose>
		        	<fo:block>
						<fo:inline color="white">!</fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
					<xsl:choose>
			          <xsl:when test="$max = $count">
			          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
			          </xsl:when>
			          <xsl:otherwise>
			          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
			          </xsl:otherwise>
			        </xsl:choose>
		        	<fo:block>
						<fo:inline color="white">!</fo:inline>
					</fo:block>
				</fo:table-cell>
				<fo:table-cell border-left="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
					<xsl:choose>
			          <xsl:when test="$max = $count">
			          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
			          </xsl:when>
			          <xsl:otherwise>
			          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
			          </xsl:otherwise>
			        </xsl:choose>
		        	<fo:block>
						<fo:inline color="white">!</fo:inline>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
	        <xsl:call-template name="for-loop-casing-blank">
	            <xsl:with-param name="count" select="$count +1"/>
	            <xsl:with-param name="row_height" select="$row_height"/>
	            <xsl:with-param name="max" select="$max"/>
	        </xsl:call-template>
	    </xsl:if>

	</xsl:template>
</xsl:stylesheet>