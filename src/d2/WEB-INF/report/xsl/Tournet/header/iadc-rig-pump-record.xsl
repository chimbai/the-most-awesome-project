<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:variable name="content_height">4mm</xsl:variable>
	<xsl:variable name="max_manufacturer_count" select="25"/>	
	<!-- IADC Rig Pump -->
	<xsl:template name="iadc_rig_pump_record">  
		<xsl:variable name="selected_record" select="//root/modules/RigPump/TourRigPumpParam"/>
		<fo:table table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(4)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-body>
				<fo:table-row height="0mm">
					<fo:table-cell border-left="1px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> PUMP NO. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> PUMP MANUFACTURER </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> TYPE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" border-right="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> STROKE LENGTH </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[1]/unitNumber"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="substring($selected_record[1]/manufacturer,1,$max_manufacturer_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[1]/unitModel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" border-right="1px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[1]/stroke"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[2]/unitNumber"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="substring($selected_record[2]/manufacturer,1,$max_manufacturer_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[2]/unitModel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" border-right= "1px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[2]/stroke"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[3]/unitNumber"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="substring($selected_record[3]/manufacturer,1,$max_manufacturer_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[3]/unitModel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" border-right="1px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[3]/stroke"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="1px solid black" border-left="1px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[4]/unitNumber"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" border-left="0.5px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="substring($selected_record[4]/manufacturer,1,$max_manufacturer_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" border-left="0.5px solid black" border-top="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[4]/unitModel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" border-left="0.5px solid black" border-top="0.5px solid black" border-right= "1px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[4]/stroke"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
			</fo:table-body>
		</fo:table>	
	</xsl:template>
	<!-- IADC Rig Pump END -->
	
</xsl:stylesheet>