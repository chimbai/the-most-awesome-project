<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<!-- IADC Wireline -->
	<xsl:template name="iadc_wireline_record">
		<xsl:param name="totalTour"/>
		<xsl:variable name="header_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'8mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'6.5mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'4.9mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'5.5mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="selected_wireline" select="/root/modules/TourWirelineDetailRecordToDate/LatestTourWireline"/>
		<xsl:variable name="today" select="//root/modules/ReportDaily/ReportDaily/reportDatetime/@epochMS" xml:base=""/>
		<xsl:variable name="latest_tourWirelineDetail_record" select="$selected_wireline/LatestTourWirelineDetail"/>
		<fo:table table-layout="fixed" width="100%" font-size="5pt">			
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-body>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$header_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="6" border-left="0.5px solid black" border-right="1px solid black" border-top="1px solid black" padding="2px" display-align="center">
						<fo:block>
							<fo:inline font-weight="bold">WIRE LINE RECORD</fo:inline><fo:inline white-space-collapse="false">     REEL NO. </fo:inline><xsl:value-of select="$selected_wireline/reelNumber" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="0.5px solid black" border-top="0.5px solid black" padding="2px">
						<fo:block> SIZE </fo:block>
						<fo:block font-size="5pt">
							<xsl:value-of select="$selected_wireline/size" />
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-left="0.5px solid black" border-top="0.5px solid black" padding="2px">
						<fo:block> NO. LINES </fo:block>
						<fo:block font-size="5pt">
							<xsl:value-of select="$selected_wireline/lineQuantity" />
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-left="0.5px solid black" border-top="0.5px solid black" border-right="1px solid black" padding="2px">
						<fo:block> LENGTH SLIPPED </fo:block>
						<fo:block font-size="5pt">
							<xsl:value-of select="$latest_tourWirelineDetail_record/slippedLength" />
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="3" border-left="0.5px solid black" border-top="0.5px solid black" padding="2px">
						<fo:block> LENGTH CUT OFF </fo:block>
						<fo:block font-size="5pt">
							<xsl:value-of select="$latest_tourWirelineDetail_record/totalCutOffLength" />
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="3" border-left="0.5px solid black" border-top="0.5px solid black" border-right="1px solid black" padding="2px">
						<fo:block> PRESENT LENGTH </fo:block>
						<fo:block font-size="5pt">
							<xsl:value-of select="$latest_tourWirelineDetail_record/currentLength" />
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="0.5px solid black" border-top="0.5px solid black" padding="2px">
						<fo:block> WEAR OR TRIPS </fo:block>
						<fo:block> SINCE LAST CUT </fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="4" border-left="0.5px solid black" border-top="0.5px solid black"  border-right="1px solid black" padding="2px" display-align="center">
						<fo:block font-size="5pt">
							<xsl:value-of select="$latest_tourWirelineDetail_record/wearSinceLastCut" />
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="0.5px solid black" border-top="0.5px solid black" border-bottom="1px solid black" padding="2px">
						<fo:block> CUMULATIVE </fo:block>
						<fo:block> WEAR OR TRIPS </fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="4" border-left="0.5px solid black" border-top="0.5px solid black" border-bottom="1px solid black" border-right="1px solid black" padding="2px" display-align="center">
						<fo:block font-size="5pt">
							<xsl:value-of select="$latest_tourWirelineDetail_record/accumulativeDailyWear" />
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- IADC Wireline END -->
</xsl:stylesheet>