<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:import	href="./iadc-casing-record.xsl"/>
	<xsl:import	href="./iadc-wireline-record.xsl"/>
	<!-- IADC Header Page 02 -->
	
	
	<xsl:template name="iadc_header_page_02">
		<fo:table table-layout="fixed" width="100%" font-size="6pt" font-weight="bold">
			<fo:table-column column-width="proportional-column-width(3)"/>
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-body>
				<fo:table-row>             
					<fo:table-cell>
						<fo:table table-layout="fixed" width="100%" font-size="6pt">
							<fo:table-column column-width="proportional-column-width(1)"/>
							<fo:table-column column-width="proportional-column-width(1)"/>
							<fo:table-column column-width="proportional-column-width(1)"/>
							<fo:table-body>
								<fo:table-row height="6.5mm">
									<fo:table-cell border-left="1px solid black" border-top="1px solid black" padding="2px">
										<fo:block font-weight="bold"> FIELD OR DISTRICT </fo:block>
										<fo:block>
											<xsl:value-of select="//root/modules/Well/Well/field"/>
											<fo:inline color="white">!</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-left="0.5px solid black" border-top="1px solid black" padding="2px">
										<fo:block font-weight="bold"> COUNTY </fo:block>
										<fo:block>
											<xsl:value-of select="//root/modules/Well/Well/county"/>
											<fo:inline color="white">!</fo:inline>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-top="1px solid black" border-left="0.5px solid black" padding="2px">
										<fo:block font-weight="bold"> STATE/COUNTRY </fo:block>
										<fo:block>
											<xsl:value-of select="//root/modules/Well/Well/state"/><xsl:text> / </xsl:text><xsl:value-of select="//root/modules/Well/Well/country/@lookupLabel"/>
											<fo:inline color="white">!</fo:inline>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
						<xsl:call-template name="iadc_casing_record"/>
					</fo:table-cell>
					<fo:table-cell>
						<xsl:call-template name="iadc_wireline_record"/>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>       
	</xsl:template>
	<!-- IADC Header Page 02 END -->
	
</xsl:stylesheet>