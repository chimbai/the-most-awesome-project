<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<!-- IADC Header Page 03 -->
	
	
	<xsl:template name="iadc_header_page_03">
		<xsl:param name="currentRigInformationUid"/>
		<xsl:variable name="totalTour" select="count(//root/modules/RigInformation/RigInformation[rigInformationUid=$currentRigInformationUid]/RigTour)"/>
		<xsl:variable name="header_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'8mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'8mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'6.5mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'6mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<fo:table table-layout="fixed" width="100%" space-before="0px" font-size="7pt">
			<fo:table-column column-width="proportional-column-width(2)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-body>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$header_height"/></xsl:attribute>						
					<fo:table-cell border="1px solid black" border-bottom="0px" number-columns-spanned="2" padding-left="5px" display-align="center">
						<fo:block font-weight="bold" font-size="8pt">
							DRILLING CREW PAYROLL DATA
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row >
					<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-right="1px solid black" number-columns-spanned="2" padding-left="5px" display-align="center">
						<fo:block>
							<fo:inline font-weight="bold" font-size="7pt" white-space-collapse="false">DATE   </fo:inline>
							<fo:inline text-decoration="underline">
								<xsl:value-of select="//root/modules/Daily/Daily/dayDate"/>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row >
					<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-right="1px solid black" number-columns-spanned="2" padding-left="5px" display-align="center">
						<fo:block>
							<fo:inline font-weight="bold" font-size="7pt" white-space-collapse="false">WELL NAME &amp; NO.   </fo:inline>	
							<fo:inline text-decoration="underline"><xsl:value-of select="//root/modules/Operation/Operation/refLease"/> <xsl:value-of select="//root/modules/Operation/Operation/operationName"/></fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row >
					<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-right="1px solid black" number-columns-spanned="2" padding-left="5px" display-align="center">
						<fo:block>
							<fo:inline font-weight="bold" font-size="7pt" white-space-collapse="false">COMPANY   </fo:inline>
							<fo:inline text-decoration="underline">
								<xsl:choose>
									<xsl:when test="//root/modules/Operation/Operation/contractorName/@lookupLabel">
										<xsl:value-of select="//root/modules/Operation/Operation/contractorName/@lookupLabel"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="//root/modules/Operation/Operation/contractorName"/>
									</xsl:otherwise>
								</xsl:choose>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row >
					<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="1px solid black" padding-left="5px" display-align="center">
						<fo:block>
							<fo:inline font-weight="bold" font-size="7pt" white-space-collapse="false">TOOLPUSHER   </fo:inline>
							<fo:inline text-decoration="underline"><xsl:value-of select="/root/modules/ReportDaily/ReportDaily/contractor/@lookupLabel"/></fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="1px solid black" border-bottom="1px solid black" padding-left="5px" display-align="center">
						<fo:block>
							<fo:inline font-weight="bold" font-size="7pt" white-space-collapse="false">RIG NO.   </fo:inline>
							<fo:inline text-decoration="underline">
								<xsl:value-of select="//root/modules/Operation/Operation/contractorName/@lookupLabel"/>
	                                           <xsl:text> &#x20; </xsl:text>
	                                           <xsl:text> / </xsl:text>
	                                           <xsl:text> &#x20; </xsl:text>
								<xsl:value-of select="//root/modules/Operation/Operation/rigInformationUid/@lookupLabel"/>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
						
	</xsl:template>
	<!-- IADC Header Page 03 END -->
	
</xsl:stylesheet>