<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:import	href="./iadc-drill-pipe-tally-record.xsl"/>
	<xsl:import	href="./iadc-rig-pump-record.xsl"/>
	<!-- IADC Header Page 01 -->
	<xsl:variable name="general_content_font_size" select="'5pt'"/>
	<xsl:variable name="page_header_content_font_size" select="'6pt'"/>
	
	<xsl:template name="iadc_header_page_01">
		<fo:table-row>
			<xsl:attribute name="font-size"><xsl:value-of select="$page_header_content_font_size"/></xsl:attribute>
			<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-top="1px solid black" padding="0px" padding-left="2px" padding-top="1px">
				<fo:block font-weight="bold"> LEASE </fo:block>
				<fo:block>
					<xsl:attribute name="font-size"><xsl:value-of select="$general_content_font_size"/></xsl:attribute>
					<xsl:value-of select="//root/modules/Operation/Operation/refLease"/>
					<fo:inline color="white">!</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" border-top="1px solid black" padding="0px" text-align="center" padding-top="1px" padding-left="2px">
				<fo:block font-weight="bold"> WELL NO. </fo:block>
				<fo:block>
					<xsl:attribute name="font-size"><xsl:value-of select="$general_content_font_size"/></xsl:attribute>
                    <xsl:value-of select="//root/modules/Operation/Operation/operationName" />
				</fo:block>										
			</fo:table-cell>
			<fo:table-cell border-left="1px solid black" border-top="1px solid black" padding-left="2px" padding-top="1px">
				<fo:block font-weight="bold"> API WELL NUMBER </fo:block>
				<fo:block>
					<xsl:attribute name="font-size"><xsl:value-of select="$general_content_font_size"/></xsl:attribute>
					<xsl:value-of select="//root/modules/Operation/Operation/regionalWellRefNumber"/>
					<fo:inline color="white">!</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" border-top="1px solid black" padding-top="1px" text-align="center">
				<fo:block font-weight="bold"> WATER DEPTH </fo:block>
				<fo:block>
					<xsl:attribute name="font-size"><xsl:value-of select="$general_content_font_size"/></xsl:attribute>
					<xsl:value-of select="//root/modules/Well/Well/waterDepth"/>
					<fo:inline color="white">!</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" border-top="1px solid black" border-right="1px solid black" padding-top="1px" text-align="center">
				<fo:block font-weight="bold"> DATE </fo:block>
				<fo:block padding-top="1px">
					<xsl:value-of select="//root/modules/Daily/Daily/dayDate"/>
					<fo:inline color="white">!</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row height="6px">
			<xsl:attribute name="font-size"><xsl:value-of select="$page_header_content_font_size"/></xsl:attribute>
			<fo:table-cell number-columns-spanned="3" border-left="1px solid black" border-top="0.5px solid black" padding="2px">
				<fo:block font-weight="bold"> OPERATOR </fo:block>
				<fo:block font-size="6pt" padding-top="1px">
					<xsl:variable name="operatorUserUid" select="//root/modules/ReportDaily/ReportDaily/operator"/>
					<xsl:variable name="operatorUserName" select="//root/modules/Signature/Operator[userUid=$operatorUserUid]/userName"/>
					<xsl:value-of select="$operatorUserName"/>
					<fo:inline color="white">!</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-top="0.5px solid black" padding="2px">
				<fo:block font-weight="bold"> CONTRACTOR </fo:block>
				<fo:block padding-top="1px" font-size="6pt">
					<xsl:variable name="contractorUserUid" select="//root/modules/ReportDaily/ReportDaily/contractor"/>
					<xsl:variable name="contractorUserName" select="//root/modules/Signature/Contractor[userUid=$contractorUserUid]/userName"/>
					<xsl:value-of select="$contractorUserName"/>
					<fo:inline color="white">!</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" border-top="0.5px solid black" border-right="1px solid black" padding-top="2px" text-align="center">
				<fo:block font-weight="bold"> RIG NO. </fo:block>
				<fo:block padding-top="1px">
					<xsl:attribute name="font-size"><xsl:value-of select="$general_content_font_size"/></xsl:attribute>
					<xsl:value-of select="//root/modules/Operation/Operation/rigInformationUid/@lookupLabel"/>
					<fo:inline color="white">!</fo:inline>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row height="6mm">
			<xsl:variable name="operatorUserUid" select="//root/modules/ReportDaily/ReportDaily/operator"/>
			<xsl:variable name="operatorLabel" select="//root/modules/ReportDaily/ReportDaily/operator/@lookupLabel"/>
			<xsl:variable name="operatorStatus" select="//root/modules/ReportDaily/ReportDaily/operatorStatus"/>
			<xsl:variable name="isOperatorSignatureExist" select="count(//root/modules/Signature/Signature[userUid=$operatorUserUid])"/>
            <xsl:variable name="operatorSignature" select="//root/modules/Signature/Signature[userUid=$operatorUserUid]/path"/>
			<fo:table-cell number-columns-spanned="3" border-left="1px solid black" border-top="0.5px solid black" padding="2px">
				<fo:block font-weight="bold">SIGNATURE OF OPERATOR'S REPRESENTATIVE</fo:block>
				<fo:block text-align="center" vertical-align="middle">
                   	<xsl:choose>
						<xsl:when test="$operatorStatus = 'true'">    
							<xsl:choose>
                                 <xsl:when test="$isOperatorSignatureExist &gt; 0">
									<fo:inline font-weight="bold" font-size="6pt"><xsl:value-of select="$operatorLabel"/></fo:inline>
									<fo:inline color="red" font-size="6pt"> and </fo:inline>
									<fo:inline><fo:external-graphic content-width="scale-to-fit" content-height="10pt" scaling="uniform" src="{$operatorSignature}"/></fo:inline>
                                </xsl:when>
                                <xsl:otherwise>
                                	<fo:block font-weight="bold" font-size="6pt"><xsl:value-of select="$operatorLabel"/></fo:block>
                                </xsl:otherwise>
                            </xsl:choose>
                      	</xsl:when>
                      	<xsl:otherwise>
                      		<fo:block font-weight="bold" font-size="6pt">Not signed yet</fo:block>
                    	</xsl:otherwise>
                	</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<xsl:variable name="contractorUserUid" select="//root/modules/ReportDaily/ReportDaily/contractor"/>
			<xsl:variable name="contractorLabel" select="//root/modules/ReportDaily/ReportDaily/contractor/@lookupLabel"/>
			<xsl:variable name="contractorStatus" select="//root/modules/ReportDaily/ReportDaily/contractorStatus"/>
			<xsl:variable name="isContractorSignatureExist" select="count(//root/modules/Signature/Signature[userUid=$contractorUserUid])"/>
            <xsl:variable name="contractorSignature" select="//root/modules/Signature/Signature[userUid=$contractorUserUid]/path"/>
			<fo:table-cell number-columns-spanned="3" border-left="1px solid black" border-top="0.5px solid black" border-right="1px solid black" padding="2px">
				<fo:block font-weight="bold">
					SIGNATURE OF CONTRACTOR'S TOOL PUSHER
				</fo:block>
				<fo:block text-align="center" vertical-align="middle">
            	   	<xsl:choose>
						<xsl:when test="$contractorStatus = 'true'"> 
							<xsl:choose>
                               	<xsl:when test="$isContractorSignatureExist &gt; 0">
									<fo:inline font-weight="bold" font-size="6pt"><xsl:value-of select="$contractorLabel"/></fo:inline>
									<fo:inline color="red" font-size="6pt"> and </fo:inline>
									<fo:inline><fo:external-graphic content-width="scale-to-fit" content-height="10pt" scaling="uniform" src="{$contractorSignature}"/></fo:inline>
                                </xsl:when>
                               	<xsl:otherwise>
                               		<fo:block font-weight="bold" font-size="6pt"><xsl:value-of select="$contractorLabel"/></fo:block>
                              	</xsl:otherwise>
                            </xsl:choose> 
                        </xsl:when>
	                    <xsl:otherwise>
		                   	<fo:block font-weight="bold" font-size="6pt">Not signed yet</fo:block>
	                    </xsl:otherwise>
	                </xsl:choose>    
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>   
			<fo:table-cell number-columns-spanned="3">
				<xsl:call-template name="iadc_drill_pipe_tally_record"/>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="3">
				<xsl:call-template name="iadc_rig_pump_record"/>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- IADC Header Page 01 END -->
	
</xsl:stylesheet>