<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:variable name="content_height">4mm</xsl:variable>
	<xsl:variable name="max_type_thread_count" select="10"/>	
	<!-- IADC Drill Pipe Tally -->
	<xsl:template name="iadc_drill_pipe_tally_record">
	    <xsl:variable name="dailyUid" select="/root/modules/Daily/Daily/dailyUid"/>
		<xsl:variable name="tourBharunUid" select="/root/modules/BharunRec/TourBharun/TourBharunSummary[dailyUid=$dailyUid]/tourBharunUid"/>
		<xsl:variable name="selected_record" select="//root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/DrillPipeTally"/>
		<fo:table table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-body>
				<fo:table-row height="0mm">
					<fo:table-cell border-top="0.5px solid black" border-left= "1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> D.P. SIZE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> WEIGHT </fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> GRADE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> TOOL JT. O.D. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> TYPE THREAD </fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> STRING NO. </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-top="0.5px solid black" border-left= "1px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[1]/drillPipeSize"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[1]/weight"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[1]/grade"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[1]/jointOd"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="substring($selected_record[1]/threadType,1,$max_type_thread_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[1]/stringNumber"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-top="0.5px solid black" border-left= "1px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[2]/drillPipeSize"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[2]/weight"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[2]/grade"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[2]/jointOd"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="substring($selected_record[2]/threadType,1,$max_type_thread_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[2]/stringNumber"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-top="0.5px solid black" border-left= "1px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[3]/drillPipeSize"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[3]/weight"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[3]/grade"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[3]/jointOd"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="substring($selected_record[3]/threadType,1,$max_type_thread_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[3]/stringNumber"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="1px solid black" border-top="0.5px solid black" border-left= "1px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[4]/drillPipeSize"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[4]/weight"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[4]/grade"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[4]/jointOd"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="substring($selected_record[4]/threadType,1,$max_type_thread_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" border-top="0.5px solid black" border-left= "0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_record[4]/stringNumber"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- IADC Drill Pipe Tally END -->
	
</xsl:stylesheet>