<?xml version="1.0" encoding="utf-8"?>                      
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format"	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:import	href="./header/iadc-header-page-01.xsl"/>   
	<xsl:import	href="./content/iadc-time-distribution.xsl"/>             
	<xsl:import	href="./content/iadc-page-01-content.xsl"/>
	<xsl:import	href="./header/iadc-header-page-02.xsl"/>                                  
	<xsl:import	href="./content/iadc-page-02-content.xsl"/>   
	<xsl:import	href="./header/iadc-header-page-03.xsl"/>
	<xsl:import	href="./content/iadc-page-03-content.xsl"/>          
	<xsl:output method="xml" />                
	<xsl:preserve-space elements="*" />                                      
	<xsl:template match="/">                         
	<xsl:variable name="general_content_font_size" select="'7.5pt'"/>                  
	<xsl:variable name="remark_content_font_size" select="'7.5pt'"/>     
	<xsl:variable name="currentRigInformationUid" select="//root/modules/ReportDaily/ReportDaily/rigInformationUid"/>
	<xsl:variable name="selected_rigTours" select="//root/modules/RigInformation/RigInformation[rigInformationUid=$currentRigInformationUid]/RigTour"/>      
		<fo:root>                       
			<fo:layout-master-set>    
				<fo:simple-page-master master-name="IADC" page-height="14in" page-width="8.5in" margin-left="10mm" margin-right="10mm" margin-top="4mm"> <!--356mm 216mm -->
                 	<fo:region-body margin-top="9mm" margin-bottom="1mm" >
						<xsl:if test="//root/modules/ReportDaily/ReportDaily/reportStatus !='true'">
							<xsl:attribute name="background-image">url(d2://images/draft.jpg)</xsl:attribute>
							<xsl:attribute name="background-position-horizontal">center</xsl:attribute>
							<xsl:attribute name="background-position-vertical">center</xsl:attribute>
							<xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
						</xsl:if>
					</fo:region-body>    
					<fo:region-before extent="15mm"/>         
					<fo:region-after extent="20mm"/> <!--35mm -->       
				</fo:simple-page-master>        
			</fo:layout-master-set>
			<fo:page-sequence master-reference="IADC" initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">    
					<fo:block font-weight="bold">
							<fo:inline color="white">!</fo:inline>
						</fo:block>  
					<fo:table width="100%" table-layout="fixed" font-size="7pt">
						<fo:table-column column-width="proportional-column-width(1.25)"/>
						<fo:table-column column-width="proportional-column-width(0.5)"/>
						<fo:table-column column-width="proportional-column-width(1.25)"/>
						<fo:table-body space-before="6mm"> <!-- page title -->
							<fo:table-row>
								<fo:table-cell>                      
									<fo:block text-align="left">
										<fo:inline font-weight="bold">No. </fo:inline>
										<fo:inline color="#dddddd">
                                            <xsl:value-of select="//root/modules/Operation/Operation/contractorName"/>
                                            <xsl:text>_</xsl:text>
                                            <xsl:value-of select="//root/modules/Operation/Operation/rigInformationUid/@lookupLabel"/>
                                            <xsl:text>_</xsl:text>
                                            <xsl:value-of select="//root/modules/Daily/Daily/dayDate"/>
										</fo:inline>
									</fo:block>
								</fo:table-cell>
								<fo:table-cell>                               
									<fo:block text-align="center" font-weight="bold"> DAILY DRILLING
										REPORT </fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block text-align="right" font-weight="bold"> REPORT NO.:
										<xsl:value-of select="//root/modules/ReportDaily/ReportDaily/reportNumber"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>       
				</fo:static-content>                     
				<fo:static-content flow-name="xsl-region-after" font-size="5pt">
					<fo:table width="100%" table-layout="fixed" padding="8px">
						<fo:table-column column-width="proportional-column-width(1)"/>
						<fo:table-column column-width="10mm"/>
						<fo:table-column column-width="proportional-column-width(1)"/>
						<fo:table-column column-width="10mm"/>            
						<fo:table-column column-width="proportional-column-width(1)"/>
						<fo:table-body>          
							<fo:table-row>   
								<fo:table-cell padding="5px"> <!--1-->
									<fo:block font-weight="bold">     
										© 1995 International Association of Drilling Contractors
									</fo:block>           
								</fo:table-cell>               
								<fo:table-cell number-columns-spanned="4" padding="2px"> <!--2 to 5-->
									<fo:block font-size="7.5pt" font-weight="bold">
										IADC-API OFFICIAL DAILY DRILLING REPORT FORM
									</fo:block>
								</fo:table-cell>           
							</fo:table-row>                            
							<fo:table-row>                          
								<fo:table-cell>            
									<fo:block>
										<fo:inline color="white">!</fo:inline>
									</fo:block>
								</fo:table-cell>                 
								<fo:table-cell>                                                   
			      						<fo:block>                           
										<xsl:choose>   
                                              <xsl:when test="//root/modules/ReportDaily/ReportDaily/reportStatus ='true'">
                                                <fo:block>
                                                     <fo:external-graphic src="url(d2://images/IADC_logo.gif)" content-width="scale-to-fit" content-height="1cm" scaling="uniform"/>
                                                </fo:block>	
                                            </xsl:when>
                                        </xsl:choose>
									</fo:block>        
								</fo:table-cell>               
								<fo:table-cell padding="5px" display-align="center" number-columns-spanned="3">
									<fo:block font-size="7pt" font-weight="bold">FORM REPRODUCED UNDER LICENSE FROM IADC.</fo:block>
									<fo:block font-size="7pt" font-weight="bold">LICENSE EXPIRES 31 DEC</fo:block> 
									<fo:block font-size="7pt" font-weight="bold">USE BEYOND EXPIRATION DATE VIOLATES IADC COPYRIGHT.</fo:block>
								</fo:table-cell>              
							</fo:table-row>             
						</fo:table-body>                   
					</fo:table>                             
				</fo:static-content>                                   
				<xsl:choose>
				<xsl:when test="count(//root/modules/RigInformation/RigInformation[rigInformationUid=$currentRigInformationUid]/RigTour) &gt; 0">                                      
				<fo:flow flow-name="xsl-region-body" font-size="5.5pt" >
					<fo:table table-layout="fixed" width="100%" font-size="5pt" font-weight="bold" space-before="0pt">
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-column column-width="proportional-column-width(12.5)"/>
						<fo:table-column column-width="proportional-column-width(12.5)"/>
						<fo:table-column column-width="proportional-column-width(29.16)"/>
						<fo:table-column column-width="proportional-column-width(12.5)"/>
						<fo:table-column column-width="proportional-column-width(8.33)"/>
						<fo:table-body>
							<xsl:call-template name="iadc_header_page_01"/>
							<fo:table-row>         
								<fo:table-cell>        
									<xsl:call-template name="iadc_time_distribution"/>
								</fo:table-cell>        
								<fo:table-cell number-columns-spanned="5"> 
									<xsl:call-template name="iadc_page_01_content">
										<xsl:with-param name="currentRigInformationUid" select="$currentRigInformationUid"/>
										<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
									</xsl:call-template>      
								</fo:table-cell>                              
							</fo:table-row>   
						</fo:table-body>         
					</fo:table>                    
					<fo:block break-before="page"/>            
					<xsl:call-template name="iadc_header_page_02"/>
					<xsl:call-template name="iadc_page_02_content">
						<xsl:with-param name="currentRigInformationUid" select="$currentRigInformationUid"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
					<fo:block break-before="page"/>
					<xsl:call-template name="iadc_header_page_03"/>
					<xsl:call-template name="iadc_page_03_content">
						<xsl:with-param name="currentRigInformationUid" select="$currentRigInformationUid"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>              
				</fo:flow>
				</xsl:when>
				<xsl:otherwise>
					<fo:flow flow-name="xsl-region-body" font-size="5.5pt" > 
					<fo:table table-layout="fixed" width="100%" font-size="18pt" color="red" font-weight="bold" space-before="0pt">
						<fo:table-column column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>          
								<fo:table-cell>        
									<fo:block>
										"Invalid Rig selected. Please make sure that 'Rig Used' field has been filled with the correct Rig"
									</fo:block>
								</fo:table-cell>                    
							</fo:table-row>    
						</fo:table-body>             
					</fo:table>                               
					</fo:flow>
				</xsl:otherwise>
				</xsl:choose>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
