<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:variable name="bit_content_font_size" select="'5pt'"/>
    <xsl:variable name="smaller_bit_content_font_size" select="'4.5pt'"/>	
	<xsl:variable name="max_serialnumber_count" select="10"/>
	<xsl:variable name="max_iadc_count" select="10"/>
	<!-- IADC Bit Record -->
	<xsl:template name="iadc_bit_record">
		<xsl:param name="rigTourUid"/>
		<xsl:param name="totalTour"/>
		<xsl:variable name="selected_bit_summary" select="//root/modules/Bitrun/TourBitrun/TourBitSummary[rigTourUid=$rigTourUid]"/>
		<xsl:variable name="selected_bharunUid1" select="//root/modules/Bitrun/TourBitrun[tourBitrunUid=$selected_bit_summary[1]/tourBitrunUid]/tourBharunUid"/>
		<xsl:variable name="selected_bharunUid2" select="//root/modules/Bitrun/TourBitrun[tourBitrunUid=$selected_bit_summary[2]/tourBitrunUid]/tourBharunUid"/>
		
		<xsl:variable name="selected_bha_summary1" select="//root/modules/BharunRec/TourBharun/TourBharunSummary[rigTourUid=$rigTourUid and tourBharunUid=$selected_bharunUid1]"/>
		<xsl:variable name="selected_bha_summary2" select="//root/modules/BharunRec/TourBharun/TourBharunSummary[rigTourUid=$rigTourUid and tourBharunUid=$selected_bharunUid2]"/>
		
		<xsl:variable name="selected_bit1" select="//root/modules/Bitrun/TourBitrun[tourBitrunUid=$selected_bit_summary[1]/tourBitrunUid]"/>
		<xsl:variable name="selected_bit2" select="//root/modules/Bitrun/TourBitrun[tourBitrunUid=$selected_bit_summary[2]/tourBitrunUid]"/>
		<xsl:variable name="current_row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'6.5mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'5.13mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="bit_cutting_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'6.5mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'4mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="bit_cuttingheader_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'7.2mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'4mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		
		<fo:table width="100%" table-layout="fixed" border="0px solid black" font-size="5pt" font-weight="bold">
			<fo:table-column column-width="proportional-column-width(0.75)"/>
			<fo:table-column column-width="proportional-column-width(0.75)"/>
			<fo:table-column column-width="proportional-column-width(1.25)"/>
			<fo:table-column column-width="proportional-column-width(1.25)"/>
			<fo:table-body> 
				<fo:table-row height="5mm">
					<fo:table-cell number-columns-spanned="4" border-left="1px solid black" border-top="0px"  border-bottom="1px solid black" text-align="center" padding="1px" background-color="rgb(192,192,192)" display-align="center">
		                <fo:block font-size="5pt" font-weight="bold">BIT RECORD</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" display-align="center" padding-left="3px">
						<fo:block> BIT NO.</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit1/bitrunNumber"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit2/bitrunNumber"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="3px">
						<fo:block> SIZE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit1/bitDiameter/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit2/bitDiameter/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="3px">
						<fo:block> IADC CODE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="substring($selected_bit1/iadcCode,1,$max_iadc_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="substring($selected_bit2/iadcCode,1,$max_iadc_count)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="3px">
						<fo:block> MANUFACTURER </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit1/make/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit2/make/@lookupLabel"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="3px">
						<fo:block> TYPE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$smaller_bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit1/model"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$smaller_bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit2/model"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="3px">
						<fo:block> SERIAL NO. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="substring($selected_bit1/serialNumber,1,$max_serialnumber_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="substring($selected_bit2/serialNumber,1,$max_serialnumber_count)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="3px">
						<fo:block> JETS </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:for-each select="$selected_bit1/TourBitNozzle">
								<xsl:value-of select="nozzleQty"/>x<xsl:value-of select="nozzleSize"/>
								<xsl:if test="position() != last()">
										/
								</xsl:if>
							</xsl:for-each>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:for-each select="$selected_bit2/TourBitNozzle">
								<xsl:value-of select="nozzleQty"/>x<xsl:value-of select="nozzleSize"/>
								<xsl:if test="position() != last()">
										/
								</xsl:if>
							</xsl:for-each>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="3px">
						<fo:block> TFA </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$smaller_bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit1/tfa"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$smaller_bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit2/tfa"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="3px">
						<fo:block> DEPTH OUT </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit1/depthOutMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit2/depthOutMdMsl"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="3px">
						<fo:block> DEPTH IN </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit1/depthInMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bit2/depthInMdMsl"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="3px">
						<fo:block> TOTAL DRILLED </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bha_summary1/progress"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bha_summary2/progress"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="1px solid black" padding="0px" display-align="center" padding-left="3px">
						<fo:block> TOTAL HOURS </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bha_summary1/duration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"></xsl:value-of></xsl:attribute>
							<xsl:value-of select="$selected_bha_summary2/duration"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>				
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$bit_cuttingheader_height"/></xsl:attribute>
					<fo:table-cell text-align="center" number-columns-spanned="4" border-left="1px solid black"  border-bottom="0.5px solid black"  padding="0px" display-align="center">
						<fo:block font-size="4.5pt"> CUTTING STRUCTURE </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$bit_cutting_height"/></xsl:attribute>
					<fo:table-cell text-align="center" border-left="1px solid black" border-bottom="0.5px solid black" display-align="center" padding="0px">
						<fo:block font-size="4.5pt"> INNER </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black" border-bottom="0.5px solid black" display-align="center" padding="0px">
						<fo:block font-size="4.5pt"> OUTER </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black" border-bottom="0.5px solid black" display-align="center" padding="0px">
						<fo:block font-size="4.5pt"> DULL CHAR </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black" border-bottom="0.5px solid black" display-align="center" padding="0px">
						<fo:block font-size="4.5pt"> LOCATION </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$bit_cutting_height"/></xsl:attribute>
					<fo:table-cell text-align="center" border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_bit1/condFinalInner" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_bit1/condFinalOuter" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center">						
						<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"/></xsl:attribute>
						<fo:block>						
							<xsl:value-of select="$selected_bit1/condFinalDull" />
						</fo:block>						
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_bit1/condFinalLocation" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$bit_cutting_height"/></xsl:attribute>
					<fo:table-cell text-align="center"  border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-top="1px">
						<fo:block font-size="4pt"> BEARINGS/ </fo:block>
						<fo:block font-size="4pt"> SEALS </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center">
						<fo:block font-size="4.5pt"> GAUGE </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-top="1px">
						<fo:block font-size="4.5pt"> OTHER DULL CHAR </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-top="1px">
						<fo:block font-size="4.5pt"> REASON PULLED </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$bit_cutting_height"/></xsl:attribute>
					<fo:table-cell text-align="center" border-left="1px solid black"  border-bottom="1px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_bit1/condFinalBearing" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black"  border-bottom="1px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_bit1/condFinalGauge" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black"  border-bottom="1px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"/></xsl:attribute>
						<fo:block>						
							<xsl:value-of select="$selected_bit1/condFinalOther" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black"  border-bottom="1px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bit_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_bit1/condFinalReason" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>		
	</xsl:template>
	<!-- IADC Bit Record END -->
</xsl:stylesheet>

