<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	
	<!-- IADC Page 03 Content -->
	<xsl:template name="iadc_page_03_content">
		<xsl:param name="currentRigInformationUid"/>
		<xsl:param name="selected_rigTours"/>
		<xsl:param name="counter"/>
		<xsl:variable name="totalTour" select="count(//root/modules/RigInformation/RigInformation[rigInformationUid=$currentRigInformationUid]/RigTour)"/>
		<xsl:variable name="header_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'5.5mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'5.5mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'5mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'5mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="pob_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'4.9mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'4.7mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="last_row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'3.5mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'3.5mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="value">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="number($counter) * 19"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="number($counter) * 12"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable>
		
		<xsl:for-each select="$selected_rigTours">
			<xsl:variable name="rigTourUid" select="rigTourUid"/>
			<xsl:variable name="selected_pob" select="//root/modules/PersonnelOnSite/TourPersonnelOnSite[rigTourUid=$rigTourUid]"/>
			<fo:table table-layout="fixed" width="100%" font-size="7pt" font-weight="bold">
				<fo:table-column column-width="proportional-column-width(1)"/>
				<fo:table-column column-width="proportional-column-width(0.5)"/>
				<fo:table-column column-width="proportional-column-width(0.5)"/>
				<fo:table-column column-width="proportional-column-width(0.5)"/>
				<fo:table-column column-width="proportional-column-width(2.5)"/>
				<fo:table-column column-width="proportional-column-width(0.5)"/>
				<fo:table-column column-width="proportional-column-width(0.5)"/>
				<fo:table-column column-width="proportional-column-width(0.5)"/>
				<fo:table-body>
					<fo:table-row>
						<xsl:attribute name="height"><xsl:value-of select="$header_height"/></xsl:attribute>
						<fo:table-cell number-columns-spanned="4" border-bottom="0.5px solid black" border-left="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">	
	                                               
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
							<fo:block font-weight="bold">
								   FROM <xsl:value-of select="d2Utils:formatDateFromEpochMS(tourStartDateTime/@epochMS,'HHmm')" /> 
								   TO <xsl:value-of select="d2Utils:formatDateFromEpochMS(tourEndDateTime/@epochMS,'HHmm')" />
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" border-left="0.5px solid black" border-right="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
							<fo:block font-weight="bold">
								INJURED ON 
							</fo:block>
							<fo:block font-weight="bold">
								THIS TOUR?
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
						<fo:table-cell border-bottom="1px solid black" border-left="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
							<fo:block font-weight="bold"> CREW </fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="3" border-bottom="1px solid black" border-left="0.5px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
	                    	<fo:block font-weight="bold"> EMPL. ID NO. </fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="1px solid black" border-left="0.5px solid black" text-align="center" padding="2px"  background-color="rgb(192,192,192)" display-align="center">
	                    	<fo:block font-weight="bold"> NAME </fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="1px solid black" border-left="0.5px solid black" text-align="center" padding="2px"  background-color="rgb(192,192,192)" display-align="center">
	                    	<fo:block font-weight="bold"> HRS. </fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="1px solid black" border-left="0.5px solid black" text-align="center" padding="2px"  background-color="rgb(192,192,192)" display-align="center">
	                    	<fo:block font-weight="bold"> INITIAL </fo:block>
						</fo:table-cell>
						<fo:table-cell border-bottom="1px solid black" border-right="1px solid black" border-left="0.5px solid black" text-align="center" padding="2px"  background-color="rgb(192,192,192)" display-align="center">
	                    	<fo:block font-weight="bold"> YES OR NO? </fo:block>
						</fo:table-cell>
					</fo:table-row>			
					
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 1"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 2"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 3"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 4"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 5"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 6"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 7"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 8"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 9"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 10"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 11"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>					
					<xsl:call-template name="pob_row">
						<xsl:with-param name="row" select="number($value) + 12"/>
						<xsl:with-param name="selected_pob" select="$selected_pob"/>
						<xsl:with-param name="pob_height" select="$pob_height"/>
					</xsl:call-template>	
					<xsl:if test="$totalTour = '2'">
				        <xsl:call-template name="pob_row">
							<xsl:with-param name="row" select="number($value) + 13"/>
							<xsl:with-param name="selected_pob" select="$selected_pob"/>
							<xsl:with-param name="pob_height" select="$pob_height"/>
						</xsl:call-template>
						<xsl:call-template name="pob_row">
							<xsl:with-param name="row" select="number($value) + 14"/>
							<xsl:with-param name="selected_pob" select="$selected_pob"/>
							<xsl:with-param name="pob_height" select="$pob_height"/>
						</xsl:call-template>		
						<xsl:call-template name="pob_row">
							<xsl:with-param name="row" select="number($value) + 15"/>
							<xsl:with-param name="selected_pob" select="$selected_pob"/>
							<xsl:with-param name="pob_height" select="$pob_height"/>
						</xsl:call-template>	
						<xsl:call-template name="pob_row">
							<xsl:with-param name="row" select="number($value) + 16"/>
							<xsl:with-param name="selected_pob" select="$selected_pob"/>
							<xsl:with-param name="pob_height" select="$pob_height"/>
						</xsl:call-template>
						<xsl:call-template name="pob_row">
							<xsl:with-param name="row" select="number($value) + 17"/>
							<xsl:with-param name="selected_pob" select="$selected_pob"/>
							<xsl:with-param name="pob_height" select="$pob_height"/>
						</xsl:call-template>
						<xsl:call-template name="pob_row">
							<xsl:with-param name="row" select="number($value) + 18"/>
							<xsl:with-param name="selected_pob" select="$selected_pob"/>
							<xsl:with-param name="pob_height" select="$pob_height"/>
						</xsl:call-template>	
						<xsl:call-template name="pob_row">
							<xsl:with-param name="row" select="number($value) + 19"/>
							<xsl:with-param name="selected_pob" select="$selected_pob"/>
							<xsl:with-param name="pob_height" select="$pob_height"/>
						</xsl:call-template>	
			      	</xsl:if>									
					<fo:table-row>
						<xsl:attribute name="height"><xsl:value-of select="$last_row_height"/></xsl:attribute>
						<fo:table-cell number-columns-spanned="8" border-bottom="1px solid black" border-right="1px solid black" border-left="1px solid black" text-align="left" padding="2px" display-align="center">
							<fo:block> NO. OF DAYS SINCE LAST LOST TIME ACCIDENT 
							<fo:inline text-decoration="underline">
								<xsl:value-of select="//root/modules/ReportDaily/ReportDaily/daysSinceLta"/>
							</fo:inline><fo:inline color="white">!</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</xsl:for-each>
			
	</xsl:template>
	
	<xsl:template name="pob_row">
		<xsl:param name="row"/>
		<xsl:param name="selected_pob"/>
		<xsl:param name="pob_height"/>
		<fo:table-row>
			<xsl:attribute name="height"><xsl:value-of select="$pob_height"/></xsl:attribute>
			<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" text-align="left" padding="2px" display-align="center">
				<fo:block>
					<xsl:value-of select="$selected_pob[$row]/crewPosition/@lookupLabel"/>
					<fo:inline color="white">!</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="3" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="2px" display-align="center">
				<fo:block>
	                <xsl:value-of select="$selected_pob[$row]/employeeNum"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="2px" display-align="center">
				<fo:block>
	           		<xsl:value-of select="$selected_pob[$row]/crewName/@lookupLabel"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="2px" display-align="center">
				<fo:block>
					<xsl:value-of select="$selected_pob[$row]/workingHours"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="2px" display-align="center">
				<fo:block>
	           		<xsl:value-of select="$selected_pob[$row]/crewName/@pobInitial"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-bottom="0.5px solid black" border-right="1px solid black" border-left="0.5px solid black" text-align="center" padding="2px" display-align="center">
				<fo:block>
					<xsl:value-of select="$selected_pob[$row]/isInjured/@lookupLabel"/>
	            </fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
</xsl:stylesheet>