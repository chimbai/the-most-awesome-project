<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:import	href="./iadc-drilling-param-pump-record.xsl"/>
	<xsl:import	href="./iadc-deviation-record.xsl"/>
	<xsl:import	href="./iadc-activity-record.xsl"/>
	
	<!-- IADC Page 02 Content -->
	<xsl:template name="iadc_page_02_content">
		<xsl:param name="currentRigInformationUid"/>
		<xsl:param name="counter"/>
		<xsl:variable name="totalTour" select="count(//root/modules/RigInformation/RigInformation[rigInformationUid=$currentRigInformationUid]/RigTour)"/>
		<xsl:param name="selected_rigTours"/>
			<fo:table table-layout="fixed" font-weight="bold">
				<fo:table-body>
					<xsl:for-each select="$selected_rigTours">
			       		<xsl:variable name="rigTourUid" select="rigTourUid"/>
			       		<fo:table-row>
			       			<fo:table-cell>
					       		<xsl:call-template name="iadc_drill_param_pump_record"> 
									<xsl:with-param name="rigTourUid" select="$rigTourUid"/> 
									<xsl:with-param name="totalTour" select="$totalTour"/> 
								</xsl:call-template> 
								<xsl:call-template name="iadc_deviation_record">
									<xsl:with-param name="rigTourUid" select="$rigTourUid"/>
									<xsl:with-param name="totalTour" select="$totalTour"/>
								</xsl:call-template>
								<xsl:call-template name="iadc_activity_record">
									<xsl:with-param name="rigTourUid" select="$rigTourUid"/>
									<xsl:with-param name="totalTour" select="$totalTour"/>
									<xsl:with-param name="counter" select="$counter"/>
								</xsl:call-template>
							</fo:table-cell>
						</fo:table-row>
			       	</xsl:for-each>
				</fo:table-body>
			</fo:table>
</xsl:template>
</xsl:stylesheet>