<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:variable name="content_height">3.9mm</xsl:variable>
	<xsl:variable name="blank_content_height">3.9mm</xsl:variable>

	<!-- IADC Time Distribution -->
	<xsl:template name="iadc_time_distribution">
		<xsl:variable name="currentRigInformationUid" select="//root/modules/ReportDaily/ReportDaily/rigInformationUid"/>
		<xsl:variable name="selected_rigTours" select="//root/modules/RigInformation/RigInformation[rigInformationUid=$currentRigInformationUid]/RigTour"/>
		<xsl:variable name="totalTour" select="count(//root/modules/RigInformation/RigInformation[rigInformationUid=$currentRigInformationUid]/RigTour)"/>
        <xsl:variable name="total_row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'5.7mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'5mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="daywork_row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'6.5mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'6mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
        <fo:table width="100%" table-layout="fixed" font-size="5.5pt" font-weight="bold" space-before="0px">
			<fo:table-column column-width="proportional-column-width(0.8)"/>
			<fo:table-column column-width="proportional-column-width(2.8)"/> 
			<xsl:for-each select="$selected_rigTours">
				<fo:table-column column-width="proportional-column-width(0.9)"/>
			</xsl:for-each>
			<fo:table-body>
				<fo:table-row height="5mm"> <!-- No 1  for the headers -->
					<fo:table-cell number-columns-spanned="{2+$totalTour}" border="1px solid black" border-top="0px" border-bottom="1px solid black" border-right="0px solid black" text-align="center" padding="1px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block font-size="5.5pt" font-weight="bold">TIME DISTRIBUTION - HOURS</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black"  padding="1px" text-align="center" display-align="center">
						<fo:block font-weight="bold"> CODE </fo:block>
                        <fo:block font-weight="bold"> NO. </fo:block>
					</fo:table-cell>
					<fo:table-cell  border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px"  text-align="left" display-align="center">
						<fo:block font-weight="bold"> OPERATION </fo:block>
					</fo:table-cell>
					<xsl:for-each select="$selected_rigTours">
						<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center" display-align="center">
							<fo:block font-weight="bold" font-size="4.5pt"><xsl:value-of select="tourLabel"/></fo:block>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 1. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<fo:block> RIG UP AND </fo:block>
						<fo:block> TEAR DOWN </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="1"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 2. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black"  border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<fo:block> DRILL ACTUAL </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="2"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 3. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<fo:block> REAMING </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="3"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 4. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<fo:block> CORING </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="4"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 5. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<fo:block> CONDITION MUD </fo:block>
						<fo:block> &amp; CIRCULATE </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="5"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 6. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<fo:block> TRIPS </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="6"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 7. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<fo:block> LUBRICATE RIG </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="7"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 8. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<fo:block> REPAIR RIG </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="8"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 9. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> CUT OFF </fo:block>
						<fo:block> DRILLING LINE </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="9"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 10. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> DEVIATION SURVEY </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="10"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 11. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0.5px" text-align="left" display-align="center">
						<fo:block> WIRE LINE LOGS </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="11"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 12. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> RUN CASING &amp; CEMENT </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="12"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 13. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> WAIT ON CEMENT</fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="13"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 14. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> NIPPLE UP B.O.P. </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="14"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 15. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<fo:block> TEST B.O.P. </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="15"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 16. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> DRILL STEM TEST </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="16"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 17. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<fo:block> PLUG BACK </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="17"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 18. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> SQUEEZE CEMENT </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="18"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 19. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> FISHING </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="19"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 20. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black"  border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> DIR. WORK </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="20"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$blank_content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 21. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black"  border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<xsl:choose>                    
                            <xsl:when test="(not(normalize-space(//root/modules/Activity/TourActivity[iadcCode='21']/otherTaskCode)))">       
                                <fo:block font-weight="bold" padding-top="13px" font-size="10pt"> </fo:block>
                            </xsl:when>
                            <xsl:otherwise>   
                                <fo:block> <xsl:value-of select="//root/modules/Activity/TourActivity[iadcCode='21']/otherTaskCode"/> </fo:block>
                            </xsl:otherwise>
                        </xsl:choose>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="21"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$blank_content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 22. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<xsl:choose>                    
                            <xsl:when test="(not(normalize-space(//root/modules/Activity/TourActivity[iadcCode='22']/otherTaskCode)))">       
                                <fo:block font-weight="bold" padding-top="13px" font-size="10pt"> </fo:block>
                            </xsl:when>
                            <xsl:otherwise>   
                                <fo:block> <xsl:value-of select="//root/modules/Activity/TourActivity[iadcCode='22']/otherTaskCode"/> </fo:block>
                            </xsl:otherwise>
                        </xsl:choose>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="22"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$blank_content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" padding="2px" text-align="center" display-align="center">
						<fo:block> 23. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"  padding="2px" text-align="left" display-align="center">
						<xsl:choose>                    
                            <xsl:when test="(not(normalize-space(//root/modules/Activity/TourActivity[iadcCode='23']/otherTaskCode)))">       
                                <fo:block font-weight="bold" padding-top="13px" font-size="10pt"> </fo:block>
                            </xsl:when>
                            <xsl:otherwise>   
                                <fo:block> <xsl:value-of select="//root/modules/Activity/TourActivity[iadcCode='23']/otherTaskCode"/> </fo:block>
                            </xsl:otherwise>
                        </xsl:choose>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="23"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell  border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center" number-rows-spanned="8" display-align="center">
	                  	<fo:block-container padding-left="5px" reference-orientation="90" display-align="center"  width="auto" height="auto" left="10pt">
					        <fo:block text-align="center" font-weight="bold" font-size="8pt">
						  	  COMPLETION
					        </fo:block>					        
					      </fo:block-container>	
	                   
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> A. PERFORATING </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="'A'"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> B. TUBING TRIPS </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="'B'"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> C. TREATING </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="'C'"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> D. SWABBING </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="'D'"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> E. TESTING </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="'E'"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$blank_content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> F. <xsl:value-of select="//root/modules/Activity/TourActivity[iadcCode='F']/otherTaskCode"/> </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="'F'"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$blank_content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> G. <xsl:value-of select="//root/modules/Activity/TourActivity[iadcCode='G']/otherTaskCode"/> </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="'G'"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$blank_content_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" padding="2px" text-align="left" display-align="center">
						<fo:block> H. <xsl:value-of select="//root/modules/Activity/TourActivity[iadcCode='H']/otherTaskCode"/> </fo:block>
					</fo:table-cell>
					<xsl:call-template name="time_distribution_item">
						<xsl:with-param name="iadcCode" select="'H'"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					</xsl:call-template>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$total_row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black"  padding="2px" text-align="left"
						number-columns-spanned="2" display-align="center">
						<fo:block> TOTALS </fo:block>
					</fo:table-cell>
					<xsl:for-each select="$selected_rigTours">
						<xsl:variable name="rigTourUid" select="rigTourUid"/>
						<xsl:variable name="activityDuration" select="sum(//root/modules/Activity/TourActivity[rigTourUid=$rigTourUid]/activityDuration)"/>
						
						<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center" display-align="center" font-size="5pt">					
							<xsl:if test="$activityDuration > 0">
								<fo:block>
									<xsl:value-of select="format-number($activityDuration, '####.##')"	/>
								</fo:block>
		 					</xsl:if>					
						</fo:table-cell>
					</xsl:for-each>					
				</fo:table-row>
				
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$daywork_row_height"/></xsl:attribute>				
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black"  padding="2px" number-columns-spanned="{2+$totalTour}" text-align="center" display-align="center">
						<fo:block font-size="6pt"> DAYWORK TIME SUMMARY </fo:block>
						<fo:block>(OFFICE USE ONLY)</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black"  padding="2px" text-align="left" number-columns-spanned="2" display-align="center">
						<fo:block>HOURS W/CONTR. D.P.</fo:block>
					</fo:table-cell>
					<xsl:for-each select="$selected_rigTours">
						<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center">
							<fo:block/>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="left" number-columns-spanned="2" display-align="center">
						<fo:block>HOURS W/OPR D.P.</fo:block>
					</fo:table-cell>
					<xsl:for-each select="$selected_rigTours">
						<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center">
							<fo:block/>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="left" number-columns-spanned="2" display-align="center">
						<fo:block>HOURS WITHOUT D.P.</fo:block>
					</fo:table-cell>
					<xsl:for-each select="$selected_rigTours">
						<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center">
							<fo:block/>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="left" number-columns-spanned="2" display-align="center">
						<fo:block>HOURS STANDBY</fo:block>
					</fo:table-cell>
					<xsl:for-each select="$selected_rigTours">
						<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center">
							<fo:block/>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$blank_content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="left" number-columns-spanned="2">
						<fo:block font-weight="bold">
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select="$selected_rigTours">
						<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center">
							<fo:block/>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$blank_content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="left" number-columns-spanned="2">
						<fo:block font-weight="bold">
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select="$selected_rigTours">
						<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center">
							<fo:block/>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$blank_content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="left" number-columns-spanned="2">
						<fo:block font-weight="bold">
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select="$selected_rigTours">
						<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center">
							<fo:block/>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$blank_content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="left" number-columns-spanned="2">
						<fo:block font-weight="bold">
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<xsl:for-each select="$selected_rigTours">
						<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center">
							<fo:block/>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="left" number-columns-spanned="2" display-align="center">
						<fo:block>TOTAL DAYWORK</fo:block>
					</fo:table-cell>
					<xsl:for-each select="$selected_rigTours">
						<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center">
							<fo:block/>
						</fo:table-cell>
					</xsl:for-each>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="left" number-columns-spanned="2" display-align="center">
						<fo:block>NO. OF DAYS</fo:block>
						<fo:block>FROM SPUD</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center" number-columns-spanned="{$totalTour}" font-size="5pt">
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="left" number-columns-spanned="2" display-align="center">
						<fo:block> CUMULATIVE </fo:block>
						<fo:block> ROTATING HOURS </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center" number-columns-spanned="{$totalTour}" font-size="5pt">
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="2px" text-align="left" number-columns-spanned="2" display-align="center">
						<fo:block>DAILY MUD COST</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center" number-columns-spanned="{$totalTour}" display-align="center" font-size="5pt">						
						<xsl:choose>
							<xsl:when test="sum(//root/modules/MudProperties/TourMudProperties/mudCost) > 0">
								<fo:block>
								<xsl:value-of select="format-number(sum(//root/modules/MudProperties/TourMudProperties/mudCost), '####.##')"	/>
								</fo:block>
		 					</xsl:when>
		 					<xsl:otherwise>
		 						<fo:block/>
		 					</xsl:otherwise>	
	 					</xsl:choose>					
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row><xsl:attribute name="height"><xsl:value-of select="$content_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="1px solid black" padding="2px" text-align="left" number-columns-spanned="2" display-align="center">
						<fo:block>TOTAL MUD COST</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" padding="2px" text-align="center" number-columns-spanned="{$totalTour}" font-size="5pt">
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
				
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- IADC Time Distribution END -->
	
	<xsl:template name="time_distribution_item">
		<xsl:param name="iadcCode"/>
		<xsl:param name="selected_rigTours"/>
		<xsl:for-each select="$selected_rigTours">
			<xsl:variable name="rigTourUid" select="rigTourUid"/>
			<xsl:variable name="activityDuration" select="sum(//root/modules/Activity/TourActivity[rigTourUid=$rigTourUid and iadcCode=$iadcCode]/activityDuration/@rawNumber)"/>
			<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" padding="2px" text-align="center" display-align="center">
				<xsl:if test="$activityDuration > 0">
					<fo:block font-weight="bold">
						<xsl:value-of select="$activityDuration" />
					</fo:block>
				</xsl:if>
			</fo:table-cell>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>

