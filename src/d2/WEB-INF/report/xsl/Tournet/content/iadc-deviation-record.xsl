<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<!-- IADC Deviation Record -->
	<xsl:template name="iadc_deviation_record">
		<xsl:param name="rigTourUid"/>
		<xsl:param name="totalTour"/>  
		<xsl:variable name="current_header_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'6.5mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'4mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 	
		<xsl:variable name="current_row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'3.2mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'2.05mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="currDailyUid" select="//root/modules/Daily/Daily/dailyUid"/>
		<xsl:variable name="selected_deviation" select="//root/modules/SurveyReference/TourSurveyReference/TourSurveyStation[rigTourUid=$rigTourUid and dailyUid=$currDailyUid]"/>
		<fo:table table-layout="fixed" width="100%" font-size="5pt" font-weight="bold">
			<fo:table-column column-width="proportional-column-width(2.1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-body>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_header_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black"  border-bottom="1px solid black" number-rows-spanned="3" text-align="center" padding="2px" display-align="center">
						<fo:block font-weight="bold"> DEVIATION </fo:block>
						<fo:block font-weight="bold"> RECORD </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> DEPTH </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> DEV. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> DIR. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> TVD </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> HORIZ. DISP. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> DEPTH </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> DEV. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> DIR. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> TVD </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> HORIZ. DISP. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> DEPTH </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> DEV. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> DIR. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> TVD </fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="1px solid black" border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center">
						<fo:block> HORIZ. DISP. </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[1]/depthMdMsl"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[1]/inclinationAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[1]/direction"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[1]/depthTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[1]/verticalSectionExtrusion"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[2]/depthMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[2]/inclinationAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[2]/direction"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[2]/depthTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[2]/verticalSectionExtrusion"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[3]/depthMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[3]/inclinationAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[3]/direction"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[3]/depthTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="1px solid black" border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[3]/verticalSectionExtrusion"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[4]/depthMdMsl"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[4]/inclinationAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[4]/direction"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[4]/depthTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[4]/verticalSectionExtrusion"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[5]/depthMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[5]/inclinationAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[5]/direction"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[5]/depthTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[5]/verticalSectionExtrusion"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[6]/depthMdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[6]/inclinationAngle"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[6]/direction"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[6]/depthTvdMsl"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-right="1px solid black" border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
						<fo:block>
							<xsl:value-of select="$selected_deviation[6]/verticalSectionExtrusion"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	
	<!-- IADC Deviation END -->
</xsl:stylesheet>

