<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:variable name="bha_content_font_size" select="'4.5pt'"/>
	<!-- IADC Drilling Assembly -->
	<xsl:template name="iadc_drilling_assembly">
		<xsl:param name="rigTourUid"/>
		<xsl:param name="totalTour"/>
		<xsl:param name="tourNumber"/>
		<xsl:variable name="tourBharunUid" select="/root/modules/IADCTourBharunDaily/TourBharunDaily[rigTourUid=$rigTourUid][last()]/tourBharunUid"/>
		<!-- <xsl:variable name="tourBharunUid" select="/root/modules/BharunRec/TourBharun[(dailyidIn!='' or dailyidIn!=null)]/TourBharunSummary[rigTourUid=$rigTourUid]/tourBharunUid"/> -->
		<xsl:variable name="thisRigTourStart" select="/root/modules/RigInformation/RigInformation/RigTour[rigTourUid=$rigTourUid]/tourStartDateTime/@epochMS"/>
		<xsl:variable name="DayDate" select="/root/modules/Daily/Daily/dayDate/@epochMS"/>
		<xsl:variable name="bha_components" select="/root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBhaComponent[Daily/dayDate/@epochMS &lt; $DayDate or (Daily/dayDate/@epochMS = $DayDate and RigTour/tourNumber/@rawNumber &lt;= $tourNumber)]"/>
		<xsl:variable name="bha_daily_summary" select="/root/modules/BharunRec/TourBharun/TourBharunSummary[tourBharunUid=$tourBharunUid]"/>
		<xsl:variable name="current_row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'4.5mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'3.28mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 	
		<xsl:variable name="current_lastrow_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'7mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'5.43mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<fo:table width="100%" table-layout="fixed" font-size="5pt" font-weight="bold">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(6.5)"/>
			<fo:table-column column-width="proportional-column-width(2.5)"/>
			<fo:table-body> 
				<fo:table-row height="5mm">
					<fo:table-cell number-columns-spanned="3" border-left="1px solid black" border-bottom="1px solid black" text-align="center" padding="1px" background-color="rgb(192,192,192)" display-align="center">
		                <fo:block font-size="5pt" font-weight="bold">DRILLING ASSEMBLY</fo:block>
	                    <fo:block font-weight="bold" font-size="5pt">(At end of tour)</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					
					<fo:table-cell border-left="1px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block> NO. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block> ITEM </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block> LENGTH </fo:block>
					</fo:table-cell>
				</fo:table-row>

				<xsl:call-template name="bha_item">
					<xsl:with-param name="row" select="1"/>
					<xsl:with-param name="bha_components" select="$bha_components"/>
					<xsl:with-param name="row_height" select="$current_row_height"/>
					<xsl:with-param name="thisRigTourStart" select="$thisRigTourStart"/>
					<xsl:with-param name="DayDate" select="$DayDate"/>
				</xsl:call-template>
				
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-top="0.5px solid black" border-left="1px solid black" text-align="center" padding="0px" display-align="center">
					<xsl:attribute name="font-size"><xsl:value-of select="$bha_content_font_size"/></xsl:attribute>
						<fo:block font-weight="bold">
							<xsl:value-of select="//root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBharunSummary[rigTourUid=$rigTourUid]/standDpQuantity"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0px" display-align="center" padding-left="3px" padding-right="2px">
						<fo:list-block>
							<fo:list-item>
								<fo:list-item-label>
									<fo:block font-weight="bold">
										<fo:inline>STANDS</fo:inline>
										<fo:inline> D.P.</fo:inline>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<fo:block text-align="right">
										<xsl:value-of select="//root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBharunSummary[rigTourUid=$rigTourUid]/standDpSize"/>
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
						</fo:list-block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bha_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="//root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBharunSummary[rigTourUid=$rigTourUid]/standDrillPipeLength" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-top="0.5px solid black" border-left="1px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bha_content_font_size"/></xsl:attribute>
						<fo:block font-weight="bold">
							<xsl:value-of select="//root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBharunSummary[rigTourUid=$rigTourUid]/singleDpQuantity"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0px" display-align="center" padding-left="3px" padding-right="2px">
						<fo:list-block>
							<fo:list-item>
								<fo:list-item-label>
									<fo:block font-weight="bold">
				                        <fo:inline>SINGLES</fo:inline>
				                        <fo:inline> D.P.</fo:inline>
									</fo:block>
								</fo:list-item-label>
								<fo:list-item-body>
									<fo:block text-align="right">
											<xsl:value-of select="//root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBharunSummary[rigTourUid=$rigTourUid]/singleDpSize"/>
									</fo:block>
								</fo:list-item-body>
							</fo:list-item>
						</fo:list-block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bha_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="//root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBharunSummary[rigTourUid=$rigTourUid]/singleDrillPipeLength" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-top="0.5px solid black" border-left="1px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bha_content_font_size"/></xsl:attribute>
						<!-- fo:block font-weight="bold">
							<xsl:value-of select="$bha_components[21]/sequence"/>
						</fo:block-->
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0px" display-align="center" padding-left="3px">
						<fo:block>
							 KELLY DOWN <xsl:value-of select="$bha_components[21]/item"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bha_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="//root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBharunSummary[rigTourUid=$rigTourUid]/lengthKellyDown" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-top="0.5px solid black" border-left="1px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block/>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="left" padding="0px" display-align="center" padding-left="3px">
						<fo:block> 
							TOTAL 
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="0.5px solid black" border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$bha_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:variable name="totalDpLength">
							<xsl:choose>
								<xsl:when test="string(number(/root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBharunSummary[rigTourUid=$rigTourUid]/totalDpLength/@rawNumber))!='NaN'">
									<xsl:value-of select="number(/root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBharunSummary[rigTourUid=$rigTourUid]/totalDpLength/@rawNumber)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="0"/>
								</xsl:otherwise>
							</xsl:choose>
							</xsl:variable>
							<xsl:value-of select="format-number(sum(/root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBhaComponent[Daily/dayDate/@epochMS &lt; $DayDate or (Daily/dayDate/@epochMS = $DayDate and RigTour/tourNumber/@rawNumber &lt;= $tourNumber)]/jointlength/@rawNumber[.!=''])
													+ number($totalDpLength),'0.00')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_lastrow_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" text-align="left" padding-left="9px" border-bottom="1px solid black" border-left="1px solid black" display-align="center">
						<fo:block>
							WT. OF STRING
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" text-align="center" padding="0px" display-align="center" >
						<fo:block>
							<xsl:attribute name="font-size"><xsl:value-of select="$bha_content_font_size"/></xsl:attribute>
							<xsl:value-of select="//root/modules/BharunRec/TourBharun[tourBharunUid=$tourBharunUid]/TourBharunSummary[rigTourUid=$rigTourUid]/weightString" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- IADC Drilling Assembly END -->
	<xsl:template name="bha_item">
		<xsl:param name="row"/>
		<xsl:param name="bha_components"/>
		<xsl:param name="row_height"/>
		<xsl:param name="thisRigTourStart"/>
		<xsl:param name="DayDate"/>
		<xsl:variable name="max_type_count" select="30"/>

		<fo:table-row>
			<xsl:attribute name="height"><xsl:value-of select="$row_height"/></xsl:attribute>
			<fo:table-cell border-top="0.5px solid black" border-left="1px solid black"
				text-align="center" padding="opx" display-align="center">
				<xsl:attribute name="font-size"><xsl:value-of select="$bha_content_font_size"/></xsl:attribute>
				<fo:block font-weight="bold">
  					<xsl:value-of select="$bha_components[$row]/jointNumber"/>			
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-top="0.5px solid black" border-left="0.5px solid black"
				text-align="left" padding="0px" display-align="center" padding-left="2px" padding-right="2px">
				<xsl:attribute name="font-size"><xsl:value-of select="$bha_content_font_size"/></xsl:attribute>
					<fo:list-block>
						<fo:list-item>
							<fo:list-item-label>
								<fo:block font-weight="bold">
					  				<xsl:value-of select="substring($bha_components[$row]/type/@lookupLabel,1,$max_type_count)"/>		
								</fo:block>
							</fo:list-item-label>
							<fo:list-item-body>
								<fo:block text-align="right">
					  				<xsl:value-of select="$bha_components[$row]/componentOd"/>		
								</fo:block>
							</fo:list-item-body>
						</fo:list-item>
					</fo:list-block>
			</fo:table-cell>
			<fo:table-cell border-top="0.5px solid black"
				border-left="0.5px solid black"
				text-align="center" padding="0px" display-align="center">
				<xsl:attribute name="font-size"><xsl:value-of select="$bha_content_font_size"/></xsl:attribute>
				<fo:block>
	  				<xsl:value-of select="$bha_components[$row]/jointlength"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<xsl:if test="$row &lt;= 17">
			<xsl:call-template name="bha_item">
				<xsl:with-param name="bha_components" select="$bha_components"/>
				<xsl:with-param name="row" select="$row + 1"/>
				<xsl:with-param name="row_height" select="$row_height"/>
				<xsl:with-param name="thisRigTourStart" select="$thisRigTourStart"/>
				<xsl:with-param name="DayDate" select="$DayDate"/>
			</xsl:call-template>
			</xsl:if>
		</xsl:template>
</xsl:stylesheet>

