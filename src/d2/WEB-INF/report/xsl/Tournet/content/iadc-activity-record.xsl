<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<!-- IADC Activity Record -->
	
	
	<xsl:template name="iadc_activity_record">
		<xsl:param name="rigTourUid"/>
		<xsl:param name="totalTour"/>
		<xsl:param name="counter"/>
		<xsl:variable name="value" select="number($counter) * 12"/>
		
		<xsl:variable name="current_header_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'3mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'3mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 	
		<xsl:variable name="current_row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'5.9mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'3.6mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="max_activity_desc_count">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'486'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'420'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable>	
		<xsl:variable name="selected_activity" select="//root/modules/IADCActivity/Activity[rigTourUid=$rigTourUid]"/>
		<fo:table table-layout="fixed" width="100%" font-size="5pt" font-weight="bold">
			<fo:table-column column-width="proportional-column-width(6.14)"/>
			<fo:table-column column-width="proportional-column-width(6.14)"/>
			<fo:table-column column-width="proportional-column-width(5.84)"/>
			<fo:table-column column-width="proportional-column-width(5.84)"/>
			<fo:table-column column-width="proportional-column-width(58.5)"/>
            <fo:table-column column-width="proportional-column-width(17.54)"/>
			<fo:table-body>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_header_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" border-left="1px solid black"
						text-align="center" padding="0.3px" display-align="center">
						<fo:block font-weight="bold"> TIME LOG </fo:block>
					</fo:table-cell>
					<fo:table-cell number-rows-spanned="2" border-bottom="0.5px solid black" border-left="0.5px solid black"
						text-align="center" padding="0.3px" display-align="center">
						<fo:block font-weight="bold"> ELAPSED TIME </fo:block>
					</fo:table-cell>
					<fo:table-cell number-rows-spanned="2" border-bottom="0.5px solid black" border-left="0.5px solid black"
						text-align="center" padding="0.3px" display-align="center">
						<fo:block font-weight="bold"> CODE </fo:block>
						<fo:block font-weight="bold"> NO. </fo:block>
                                               
					</fo:table-cell>
					<fo:table-cell number-rows-spanned="2" number-columns-spanned="2" border-bottom="0.5px solid black" border-left="0.5px solid black" border-right="1px solid black" 
						text-align="left" padding="0.3px" display-align="center">
						<fo:block font-weight="bold"> DETAILS OF OPERATIONS IN SEQUENCE AND REMARKS </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_header_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black"
						text-align="center" padding="0.3px" display-align="center">
						<fo:block> FROM </fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"
						text-align="center" padding="0.3px" display-align="center">
						<fo:block> TO </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black"
						text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[1 + number($value)]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"
						text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[1 + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[1 + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"
						text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[1 + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black"
						text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[1 + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="1px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black"
						text-align="left" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[1 + number($value)]/remark"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black"
						text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[2]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[2  + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[2  + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[2  + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[2  + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="1px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[2 + number($value)]/remark"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[3  + number($value)]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[3  + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[3  + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[3  + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[3  + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="1px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[3 + number($value)]/remark"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[4  + number($value)]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[4  + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[4  + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[4  + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[4  + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="1px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[4 + number($value)]/remark"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[5  + number($value)]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[5  + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[5  + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[5  + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[5  + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="1px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[5 + number($value)]/remark"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[6  + number($value)]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[6  + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[6  + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[6  + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[6  + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="1px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[6 + number($value)]/remark"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[7  + number($value)]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[7  + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[7  + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[7  + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[7  + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="1px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[7 + number($value)]/remark"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[8  + number($value)]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[8  + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[8  + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[8  + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[8  + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="1px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[8 + number($value)]/remark"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[9  + number($value)]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[9  + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[9  + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[9  + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[9  + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="1px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[9 + number($value)]/remark"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[10  + number($value)]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[10  + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[10  + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[10  + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[10  + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="1px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[10 + number($value)]/remark"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[11  + number($value)]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[11  + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[11  + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[11  + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[11  + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-right="1px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[11 + number($value)]/remark"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-bottom="1px solid black" border-left="1px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[12  + number($value)]/startDatetime,'HHmm')"/>
							<fo:inline color="white">!</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:choose>
					      		<xsl:when test="d2Utils:formatDateFromEpochMS($selected_activity[12  + number($value)]/endDatetime,'HHmm')='2359'">
							       <fo:inline>2400</fo:inline>
					     		</xsl:when>
							    <xsl:otherwise>
							       <xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_activity[12  + number($value)]/endDatetime,'HHmm')"/>
							    </xsl:otherwise>
					     	</xsl:choose>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[12  + number($value)]/activityDuration"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="1px solid black" border-left="0.5px solid black" text-align="center" padding="0.3px" display-align="center" font-size="6pt">
						<fo:block>
							<xsl:value-of select="$selected_activity[12  + number($value)]/iadcCode"/>
						</fo:block>
					</fo:table-cell>
					<xsl:choose>
						<xsl:when test="$totalTour &gt; '2'">
							<fo:table-cell border-bottom="1px solid black" border-left="0.5px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
								<fo:block>
									<xsl:value-of select="$selected_activity[12 + number($value)]/remark"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0.3px" display-align="after" border-bottom="1px solid black" border-left="0.5px solid black" border-right="1px solid black">					
							<fo:block font-size="6pt" text-align="left">
								<fo:inline font-weight="bold"> DRILLER </fo:inline>  
								<xsl:variable name="drillerUserUid" select="//root/modules/ReportDaily/ReportDaily/TourDaily[rigTourUid=$rigTourUid]/driller"/>
								<xsl:variable name="drillerLabel" select="//root/modules/ReportDaily/ReportDaily/TourDaily[rigTourUid=$rigTourUid]/driller/@lookupLabel"/>
								<xsl:variable name="drillerStatus" select="//root/modules/ReportDaily/ReportDaily/TourDaily[rigTourUid=$rigTourUid]/tourStatus"/>
								<xsl:variable name="isDrillerSignatureExist" select="count(//root/modules/Signature/Signature[userUid=$drillerUserUid])"/>
		                        <xsl:variable name="drillerSignature" select="//root/modules/Signature/Signature[userUid=$drillerUserUid]/path"/>
										
	                            <xsl:choose>                       
	                                <xsl:when test="$drillerStatus !='true'">       
	                                    <fo:inline color="white">!</fo:inline>
	                                </xsl:when>
	                                <xsl:otherwise> 
		                               	<xsl:choose>
		                                	<xsl:when test="$isDrillerSignatureExist &gt;0">
		                                		&#160;&#160;&#160;
		                                		<fo:inline font-size="6pt"><xsl:value-of select="$drillerLabel"/></fo:inline>
												<fo:inline color="red" font-size="6pt"> and </fo:inline>
												<fo:inline><fo:external-graphic content-width="scale-to-fit" content-height="10pt" scaling="uniform" src="{$drillerSignature}"/></fo:inline>
							               	</xsl:when> 
		                                	<xsl:otherwise>
		                                		&#160;&#160;&#160;
		                                        <xsl:value-of select="$drillerLabel"/>
		                                	</xsl:otherwise>
	                                	</xsl:choose>
	                                </xsl:otherwise>
	                            </xsl:choose> 
							</fo:block>
						</fo:table-cell>  							
						</xsl:when>
						<xsl:otherwise>
							<fo:table-cell number-columns-spanned="2" border-bottom="1px solid black" border-left="0.5px solid black" border-right="1px solid black" text-align="left" padding="0.3px" display-align="center" font-size="6pt">
								<fo:block>
									<xsl:value-of select="$selected_activity[12  + number($value)]/remark"/>
								</fo:block>
							</fo:table-cell>
						</xsl:otherwise>
					</xsl:choose>	
                    
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- IADC Activity Record END -->
</xsl:stylesheet>

