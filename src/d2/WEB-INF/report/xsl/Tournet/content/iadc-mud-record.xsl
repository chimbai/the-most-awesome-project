<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:variable name="mud_content_font_size" select="'5pt'"/>    
	<xsl:variable name="mud_content_font_size1" select="'4.5pt'"/>    
	<xsl:variable name="max_mud_content_count" select="10"/>
	<!-- IADC Mud Record -->
	<xsl:template name="iadc_mud_record">
		<xsl:param name="rigTourUid"/>
		<xsl:param name="totalTour"/>
		<xsl:variable name="selected_mud" select="//root/modules/MudProperties/TourMudProperties[rigTourUid=$rigTourUid]"/>
		<xsl:variable name="current_row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'8mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'6mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="current_rowheader_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'6.5mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'4mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="current_rowsubheader_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'6mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'2.55mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<xsl:variable name="mc_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'4.8mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'3.88mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
				      
		<fo:table width="100%" table-layout="fixed" border="0px solid black" font-size="5pt" font-weight="bold">
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/>
			<fo:table-column column-width="proportional-column-width(1)"/> 
			<fo:table-body>
				<fo:table-row height="5mm">
					<fo:table-cell number-columns-spanned="4" border-left="1px solid black" border-top="0px" border-bottom="1px solid black" text-align="center" padding="1px" background-color="rgb(192,192,192)" display-align="center">
		                <fo:block font-size="5pt" font-weight="bold">MUD RECORD</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black"  border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="2px">
						<fo:block> TIME </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_mud[1]/reportTime/@epochMS,'HHmm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>								   
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_mud[2]/reportTime/@epochMS,'HHmm')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>								   
						<fo:block>
							<xsl:value-of select="d2Utils:formatDateFromEpochMS($selected_mud[3]/reportTime/@epochMS,'HHmm')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" display-align="center" padding-left="2px">
						<fo:block> WEIGHT </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[1]/mwdh"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
             				<xsl:value-of select="$selected_mud[2]/mwdh"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
             				<xsl:value-of select="$selected_mud[3]/mwdh"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="2px">
						<fo:block> PRESSURE GRADIENT </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center" >
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[1]/pressureGradient"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[2]/pressureGradient"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[3]/pressureGradient"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="2px">
						<fo:block> FUNNEL VISCOSITY </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
                        	<xsl:value-of select="$selected_mud[1]/mudFv"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
                        	<xsl:value-of select="$selected_mud[2]/mudFv"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
                        	<xsl:value-of select="$selected_mud[3]/mudFv"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="2px">
						<fo:block> PV/YP </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[1]/mudPv"/> / <xsl:value-of select="$selected_mud[1]/mudYp"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[2]/mudPv"/> / <xsl:value-of select="$selected_mud[2]/mudYp"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[3]/mudPv"/> / <xsl:value-of select="$selected_mud[3]/mudYp"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="2px">
						<fo:block> GEL STRENGTH </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[1]/mudGel10s"/> / <xsl:value-of select="$selected_mud[1]/mudGel10m"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[2]/mudGel10s"/> / <xsl:value-of select="$selected_mud[2]/mudGel10m"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[3]/mudGel10s"/> / <xsl:value-of select="$selected_mud[3]/mudGel10m"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="2px">
						<fo:block> FLUID LOSS </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[1]/fluidLossApiVolume"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[2]/fluidLossApiVolume"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[3]/fluidLossApiVolume"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="2px">
						<fo:block> pH </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[1]/mudPh"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[2]/mudPh"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[3]/mudPh"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="0.5px solid black" padding="0px" display-align="center" padding-left="2px">
						<fo:block> SOLIDS </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[1]/mudDissolvedSolids"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[2]/mudDissolvedSolids"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[3]/mudDissolvedSolids"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_row_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="1px solid black" padding="0px" display-align="center" padding-left="2px">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block font-weight="bold">
							H2O
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="0px" display-align="center">
						<fo:block>
							<xsl:value-of select="$selected_mud[1]/mudH2o"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[2]/mudH2o"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_mud[3]/mudH2o"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_rowheader_height"/></xsl:attribute>
					<fo:table-cell border-bottom="0.5px solid black" border-left="1px solid black" text-align="center" padding="0px" number-columns-spanned="4" display-align="center">
						<fo:block> MUD AND CHEMICALS ADDED </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_rowsubheader_height"/></xsl:attribute>
				
				
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="1px solid black" padding="0px" display-align="center">
						<fo:block> TYPE </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<fo:block> AMOUNT </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<fo:block> TYPE </fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<fo:block> AMOUNT </fo:block>
					</fo:table-cell>
				</fo:table-row>
								
				<xsl:variable name="selected_stock" select="//root/modules/FluidStock/TourRigStock[rigTourUid=$rigTourUid]"/>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$mc_height"/></xsl:attribute>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="1px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="substring($selected_stock[1]/stockCode/@lookupLabel,1,$max_mud_content_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_stock[1]/amtUsed"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="substring($selected_stock[2]/stockCode/@lookupLabel,1,$max_mud_content_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_stock[2]/amtUsed"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$mc_height"/></xsl:attribute>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="1px solid black"  padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="substring($selected_stock[3]/stockCode/@lookupLabel,1,$max_mud_content_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_stock[3]/amtUsed"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="substring($selected_stock[4]/stockCode/@lookupLabel,1,$max_mud_content_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_stock[4]/amtUsed"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$mc_height"/></xsl:attribute>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="1px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="substring($selected_stock[5]/stockCode/@lookupLabel,1,$max_mud_content_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_stock[5]/amtUsed"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="substring($selected_stock[6]/stockCode/@lookupLabel,1,$max_mud_content_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_stock[6]/amtUsed"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$mc_height"/></xsl:attribute>
					<fo:table-cell text-align="center"  border-left="1px solid black" border-bottom="1px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="substring($selected_stock[7]/stockCode/@lookupLabel,1,$max_mud_content_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center"  border-left="0.5px solid black" border-bottom="1px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_stock[7]/amtUsed"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center" border-left="0.5px solid black" border-bottom="1px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="substring($selected_stock[8]/stockCode/@lookupLabel,1,$max_mud_content_count)"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center"  border-left="0.5px solid black" border-bottom="1px solid black" padding="0px" display-align="center">
						<xsl:attribute name="font-size"><xsl:value-of select="$mud_content_font_size1"/></xsl:attribute>
						<fo:block>
							<xsl:value-of select="$selected_stock[8]/amtUsed"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>			
	</xsl:template>
	<!-- IADC Mud Record END -->
</xsl:stylesheet>

