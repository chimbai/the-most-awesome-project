<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:variable name="max_formation_count" select="20"/>	
	<!-- IADC Drilling Param & Pump Record -->
	<xsl:template name="iadc_drill_param_pump_record">
		<xsl:param name="rigTourUid"/>
		<xsl:param name="totalTour"/> 
		<xsl:variable name="current_header_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'6.5mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'4mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 	
		<xsl:variable name="current_row_height">
			<xsl:choose>
				<xsl:when test="$totalTour = '2'">
	  				<xsl:value-of select="'6.8mm'"/> 	  				
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'3.5mm'"/>
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<fo:table table-layout="fixed" width="100%" font-size="5pt" font-weight="bold">
			<fo:table-column column-width="proportional-column-width(6.66)"/>
			<fo:table-column column-width="proportional-column-width(6.66)"/>
			<fo:table-column column-width="proportional-column-width(6.66)"/>
			<fo:table-column column-width="proportional-column-width(4)"/>
			<fo:table-column column-width="proportional-column-width(19.36)"/>
			<fo:table-column column-width="proportional-column-width(6)"/>
			<fo:table-column column-width="proportional-column-width(4)"/>
			<fo:table-column column-width="proportional-column-width(6.66)"/>
			<fo:table-column column-width="proportional-column-width(4.25)"/>
			<fo:table-column column-width="proportional-column-width(4.25)"/>
			<fo:table-column column-width="proportional-column-width(4.25)"/>
			<fo:table-column column-width="proportional-column-width(4.25)"/>
			<fo:table-column column-width="proportional-column-width(4.25)"/>
			<fo:table-column column-width="proportional-column-width(4.25)"/>
			<fo:table-column column-width="proportional-column-width(4.25)"/>
			<fo:table-column column-width="proportional-column-width(4.25)"/>
			<fo:table-column column-width="proportional-column-width(6)"/>
			<fo:table-body>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_header_height"/></xsl:attribute>
					<fo:table-cell number-columns-spanned="2" border-left="1px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> DEPTH INTERVAL </fo:block>
					</fo:table-cell>
					<fo:table-cell number-rows-spanned="2" border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> DRILL..D</fo:block>
						<fo:block>REAM..R</fo:block>> <fo:block>CORE..C</fo:block>
					</fo:table-cell>
					<fo:table-cell number-rows-spanned="2" border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> CORE NO. </fo:block>
					</fo:table-cell>
					<fo:table-cell number-rows-spanned="2" border-left="0.5px solid black"  border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> FORMATION</fo:block>
						<fo:block font-size="5pt"> (SHOW CORE RECOVERY) </fo:block>
					</fo:table-cell>
					<fo:table-cell number-rows-spanned="2" border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> ROTARY TABLE SPEED </fo:block>
					</fo:table-cell>
					<fo:table-cell number-rows-spanned="2" border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> WT. ON BIT </fo:block>
					</fo:table-cell>
					<fo:table-cell number-rows-spanned="2" border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> PUMP PRESSURE </fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> PUMP NO. 1</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> PUMP NO. 2</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> PUMP NO. 3</fo:block>
					</fo:table-cell>
					<fo:table-cell number-columns-spanned="2" border-left="0.5px solid black" border-bottom="0.5px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> PUMP NO. 4</fo:block>
					</fo:table-cell>
					<fo:table-cell number-rows-spanned="2" border-right="1px solid black" border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> TOTAL PUMP OUTPUT </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<xsl:attribute name="height"><xsl:value-of select="$current_header_height"/></xsl:attribute>
					<fo:table-cell border-left="1px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> FROM </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> TO </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> LINER SIZE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> S.P.M. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> LINER SIZE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> S.P.M. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> LINER SIZE </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> S.P.M. </fo:block>
					</fo:table-cell>
					<fo:table-cell border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> LINER SIZE </fo:block>
					</fo:table-cell>
					<fo:table-cell  border-left="0.5px solid black" border-bottom="1px solid black" text-align="center" padding="2px" background-color="rgb(192,192,192)" display-align="center">
						<fo:block> S.P.M. </fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:call-template name="item-listing">
					<xsl:with-param name="position" select="1"/>
					<xsl:with-param name="rigTourUid" select="$rigTourUid"/>
					<xsl:with-param name="rowHeight" select="$current_row_height"/>
				</xsl:call-template>
				<xsl:call-template name="item-listing">
					<xsl:with-param name="position" select="2"/>
					<xsl:with-param name="rigTourUid" select="$rigTourUid"/>
					<xsl:with-param name="rowHeight" select="$current_row_height"/>
				</xsl:call-template>
				<xsl:call-template name="item-listing">
					<xsl:with-param name="position" select="3"/>
					<xsl:with-param name="rigTourUid" select="$rigTourUid"/>
					<xsl:with-param name="lastRecord" select="1"/>
					<xsl:with-param name="rowHeight" select="$current_row_height"/>
				</xsl:call-template>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template name="item-listing">
		<xsl:param name="position"/>
		<xsl:param name="rigTourUid"/>
		<xsl:param name="lastRecord" select="0"/>
		<xsl:param name="rowHeight"/>
		<xsl:variable name="selected_drill_param" select="/root/modules/DrillingParameters/TourDrillingParameters[rigTourUid=$rigTourUid]"/>
		<xsl:variable name="selected_pump1" select="/root/modules/RigPump/TourRigPumpParam[1]/TourRigSlowPumpParam[rigTourUid=$rigTourUid]"/>
		<xsl:variable name="selected_pump2" select="/root/modules/RigPump/TourRigPumpParam[2]/TourRigSlowPumpParam[rigTourUid=$rigTourUid]"/>
		<xsl:variable name="selected_pump3" select="/root/modules/RigPump/TourRigPumpParam[3]/TourRigSlowPumpParam[rigTourUid=$rigTourUid]"/>
		<xsl:variable name="selected_pump4" select="/root/modules/RigPump/TourRigPumpParam[4]/TourRigSlowPumpParam[rigTourUid=$rigTourUid]"/>
		<xsl:variable name="totaloutput" select="sum(/root/modules/RigPump/TourRigPumpParam/TourRigSlowPumpParam[rigTourUid=$rigTourUid]/flow/@rawNumber)"/>
		
		<xsl:variable name="selected_liner1" select="/root/modules/RigPump/TourRigPumpParam[1]"/>
		<xsl:variable name="selected_liner2" select="/root/modules/RigPump/TourRigPumpParam[2]"/>
		<xsl:variable name="selected_liner3" select="/root/modules/RigPump/TourRigPumpParam[3]"/>
		<xsl:variable name="selected_liner4" select="/root/modules/RigPump/TourRigPumpParam[4]"/>
		
		<fo:table-row>
			<xsl:attribute name="height"><xsl:value-of select="$rowHeight"/></xsl:attribute>
			<fo:table-cell border-left="1px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_drill_param[$position]/depthTopMdMsl"/>
					<fo:inline color="white">!</fo:inline>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_drill_param[$position]/depthMdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_drill_param[$position]/drillMode"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_drill_param[$position]/coreNumber"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="substring($selected_drill_param[$position]/formation,1,$max_formation_count)"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_drill_param[$position]/rotaryTableSpeed"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_drill_param[$position]/wobAvgForce"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_pump1[$position]/pressure"/>	
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_liner1/liner"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_pump1[$position]/rate"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_liner2/liner"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_pump2[$position]/rate"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_liner3/liner"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_pump3[$position]/rate"/>	
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_liner4/liner"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:value-of select="$selected_pump4[$position]/rate"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border-right="1px solid black" border-left="0.5px solid black" text-align="center" padding="2px" display-align="center" font-size="5pt">
				<xsl:choose>
		          <xsl:when test="$lastRecord !=1">
		          	<xsl:attribute name="border-bottom">0.5px solid black</xsl:attribute>
		          </xsl:when>
		          <xsl:otherwise>
		          	<xsl:attribute name="border-bottom">1px solid black</xsl:attribute>
		          </xsl:otherwise>
		        </xsl:choose>
				<fo:block>
					<xsl:if test="$selected_pump1[$position]/tourRigSlowPumpParamUid !='' or $selected_pump2[$position]/tourRigSlowPumpParamUid !='' or $selected_pump3[$position]/tourRigSlowPumpParamUid !='' or $selected_pump4[$position]/tourRigSlowPumpParamUid !=''">
								<xsl:value-of select="d2Utils:formatNumber(string($totaloutput),'0.00')"/>
					</xsl:if>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- IADC Drilling Param & Pump Record END -->
</xsl:stylesheet>

