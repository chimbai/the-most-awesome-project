<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:import	href="./iadc-drilling-assembly.xsl"/>
	<xsl:import	href="./iadc-bit-record.xsl"/>
	<xsl:import	href="./iadc-mud-record.xsl"/>
	<xsl:variable name="remark_content_font_size" select="'5pt'"/>
	<xsl:variable name="driller_row_height" select="'3mm'"/>
	<!-- IADC Page 01 Content -->
	<xsl:template name="iadc_page_01_content">
		<xsl:param name="currentRigInformationUid"/>
		<xsl:param name="selected_rigTours"/>
		<xsl:variable name="totalTour" select="count(//root/modules/RigInformation/RigInformation[rigInformationUid=$currentRigInformationUid]/RigTour)"/>
		<xsl:variable name="remarks_row_height"> 
			<xsl:choose> 
				<xsl:when test="$totalTour = '2'">
					<xsl:value-of select="'10.8mm'"/>		
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'5.54mm'"/>	
				</xsl:otherwise>	
			</xsl:choose>
		</xsl:variable> 
		<fo:table width="100%" table-layout="fixed" font-weight="bold">
        	<fo:table-column column-width="proportional-column-width(33.3)"/>
			<fo:table-column column-width="proportional-column-width(32)"/>
			<fo:table-column column-width="proportional-column-width(32)"/>
			<fo:table-column column-width="proportional-column-width(2.7)"/>
        	<fo:table-body>
        	<xsl:for-each select="$selected_rigTours">
        	    <xsl:variable name="rigTourUid" select="rigTourUid"/>        	
				<fo:table-row>
        			<fo:table-cell>
        				<xsl:call-template name="iadc_drilling_assembly">
							<xsl:with-param name="rigTourUid" select="$rigTourUid"/>
							<xsl:with-param name="totalTour" select="$totalTour"/>
							<xsl:with-param name="tourNumber" select="tourNumber"/>
						</xsl:call-template>
        			</fo:table-cell>
        			<fo:table-cell>
						<xsl:call-template name="iadc_bit_record">
							<xsl:with-param name="rigTourUid" select="$rigTourUid"/>
							<xsl:with-param name="totalTour" select="$totalTour"/>
						</xsl:call-template>
					</fo:table-cell>       
					<fo:table-cell>
						<xsl:call-template name="iadc_mud_record">
							<xsl:with-param name="rigTourUid" select="$rigTourUid"/>
							<xsl:with-param name="totalTour" select="$totalTour"/>
						</xsl:call-template>
					</fo:table-cell>
					<xsl:choose> 
						<xsl:when test="$totalTour = '2'">
							<fo:table-cell number-rows-spanned="3" border="1px solid black"  border-top="0px" text-align="center" display-align="center">
								<xsl:variable name="label" select="tourLabel"/>
								<xsl:call-template name="for-loop">
									<xsl:with-param name="text" select="$label"/>
								</xsl:call-template>
								<fo:block text-align="center" font-weight="bold" font-size="8pt"><fo:inline color="white">!</fo:inline></fo:block>
								<fo:block text-align="center" font-weight="bold" font-size="8pt">T</fo:block>
			                    <fo:block text-align="center" font-weight="bold" font-size="8pt">O</fo:block>
			                    <fo:block text-align="center" font-weight="bold" font-size="8pt">U</fo:block>
			                    <fo:block text-align="center" font-weight="bold" font-size="8pt">R</fo:block>
							</fo:table-cell>		
						</xsl:when>
						<xsl:otherwise>
							<fo:table-cell border="1px solid black"  border-top="0px" text-align="center" display-align="center">
								<xsl:variable name="label" select="tourLabel"/>
								<xsl:call-template name="for-loop">
									<xsl:with-param name="text" select="$label"/>
								</xsl:call-template>
								<fo:block text-align="center" font-weight="bold" font-size="8pt"><fo:inline color="white">!</fo:inline></fo:block>
								<fo:block text-align="center" font-weight="bold" font-size="8pt">T</fo:block>
			                    <fo:block text-align="center" font-weight="bold" font-size="8pt">O</fo:block>
			                    <fo:block text-align="center" font-weight="bold" font-size="8pt">U</fo:block>
			                    <fo:block text-align="center" font-weight="bold" font-size="8pt">R</fo:block>
							</fo:table-cell>
						</xsl:otherwise>	
					</xsl:choose>
        		</fo:table-row>
        		<xsl:choose> 
					<xsl:when test="$totalTour = '2'">
						<fo:table-row>    
		        			<xsl:attribute name="height"><xsl:value-of select="$remarks_row_height"/></xsl:attribute>  
		                	<fo:table-cell number-columns-spanned="3" text-align="left" padding-left="4px"  padding-top="2px" border-left="1px solid black" display-align="top">   
			                	<fo:block  font-size="5pt" font-weight="bold"> 
									REMARKS
								</fo:block> 
			                	<fo:block  font-size="5pt" font-weight="bold"  linefeed-treatment="preserve"> 
									<xsl:attribute name="font-size"><xsl:value-of select="$remark_content_font_size"/></xsl:attribute>
									<xsl:value-of select="//root/modules/ReportDaily/ReportDaily/TourDaily[rigTourUid=$rigTourUid]/tourRemark"/>
								</fo:block> 
							</fo:table-cell>
		        		</fo:table-row>
		        		<fo:table-row>    
		        			<xsl:attribute name="height"><xsl:value-of select="$driller_row_height"/></xsl:attribute>  
		                	<fo:table-cell number-columns-spanned="3" text-align="left" padding-left="4px"  padding-top="4px" border-bottom="1px solid black" border-left="1px solid black" display-align="top">   
			                	<fo:block  font-size="6pt" text-align="left">
									<fo:inline font-weight="bold"> DRILLER </fo:inline>
									    <xsl:variable name="drillerUserUid" select="//root/modules/ReportDaily/ReportDaily/TourDaily[rigTourUid=$rigTourUid]/driller"/>
										<xsl:variable name="drillerLabel" select="//root/modules/ReportDaily/ReportDaily/TourDaily[rigTourUid=$rigTourUid]/driller/@lookupLabel"/>
										<xsl:variable name="drillerStatus" select="//root/modules/ReportDaily/ReportDaily/TourDaily[rigTourUid=$rigTourUid]/tourStatus"/>
										<xsl:variable name="isDrillerSignatureExist" select="count(//root/modules/Signature/Signature[userUid=$drillerUserUid])"/>
				                        <xsl:variable name="drillerSignature" select="//root/modules/Signature/Signature[userUid=$drillerUserUid]/path"/>
												
			                            <xsl:choose>                       
			                                <xsl:when test="$drillerStatus !='true'">       
			                                    <fo:inline color="white">!</fo:inline>
			                                </xsl:when>
			                                <xsl:otherwise> 
			                                	<xsl:choose>
				                                	<xsl:when test="$isDrillerSignatureExist &gt; 0">
				                                		&#160;&#160;&#160;
				                                		<fo:inline font-size="6pt"><xsl:value-of select="$drillerLabel"/></fo:inline>
														<fo:inline color="red" font-size="6pt"> and </fo:inline>
														<fo:inline><fo:external-graphic content-width="scale-to-fit" content-height="10pt" scaling="uniform" src="{$drillerSignature}"/></fo:inline>
									               	</xsl:when> 
				                                	<xsl:otherwise>
				                                		&#160;&#160;&#160;
				                                        <xsl:value-of select="$drillerLabel"/>
				                                	</xsl:otherwise>
			                                	</xsl:choose>
			                                </xsl:otherwise>
			                            </xsl:choose> 
								</fo:block> 
							</fo:table-cell>
		        		</fo:table-row>		
					</xsl:when>	
				</xsl:choose>
        	</xsl:for-each>
        	</fo:table-body>
        </fo:table>
	</xsl:template>
	<!-- IADC Page 01 Content END -->
<xsl:template name="for-loop">
	<xsl:param name="text"/>
    <xsl:param name="count" select="1"/>
	<xsl:variable name="max" select="string-length($text)"/>
    <xsl:if test="$max &gt;= $count">
    	<xsl:variable name="character" select="substring($text,$count,1)"/>
    	<fo:block text-align="center" font-weight="bold" font-size="8pt">
    		<xsl:choose>     			
				<xsl:when test="$character = ' '">
					<fo:inline color="white">!</fo:inline>		
				</xsl:when>
				<xsl:otherwise>					
			        	<xsl:value-of select="substring($text,$count,1)"/>			        
				</xsl:otherwise>	
			</xsl:choose>       
		</fo:block>
        <xsl:call-template name="for-loop">
        	<xsl:with-param name="text" select="$text"/>
            <xsl:with-param name="count" select="$count +1"/>
        </xsl:call-template>
    </xsl:if>

</xsl:template>
</xsl:stylesheet>