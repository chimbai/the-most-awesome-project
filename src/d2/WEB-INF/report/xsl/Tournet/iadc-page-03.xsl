<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- IADC Page 03 -->
	<xsl:template name="iadc_page_03">
		<xsl:param name="currentRigInformationUid"/>
		<xsl:param name="maxPob"/>
		<xsl:param name="selected_rigTours"/>
		<xsl:param name="tourCounter"/>
		
		<xsl:choose>						
			<xsl:when test="number($tourCounter) &gt; 0">
				<xsl:variable name="rigTourUid" select="$selected_rigTours[number($tourCounter)]/rigTourUid"/>
				<xsl:variable name="currentPob" select="count(//root/modules/PersonnelOnSite/TourPersonnelOnSite[rigTourUid=$rigTourUid])"/>
				<xsl:call-template name="iadc_page_03">
					<xsl:with-param name="currentRigInformationUid" select="$currentRigInformationUid"/>
					<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
					<xsl:with-param name="maxPob">
						<xsl:choose>						
							<xsl:when test="number($currentPob) &gt; number($maxPob)">
								<xsl:value-of select="count(//root/modules/PersonnelOnSite/TourPersonnelOnSite[rigTourUid=$rigTourUid])"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of name="maxPob" select="$maxPob"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
					<!-- decrement the counter -->
					<xsl:with-param name="tourCounter" select="number($tourCounter) - 1"/>						
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				
				<xsl:variable name="totalTour" select="count($selected_rigTours)"/>
				<xsl:variable name="recordPerTour">
					<xsl:choose>
						<xsl:when test="$totalTour = '2'">
			  				<xsl:value-of select="19"/> 	  				
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="12"/>
						</xsl:otherwise>	
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="pageOverflow">
					<xsl:choose>
						<xsl:when test="ceiling(number($maxPob) div number($recordPerTour)) &gt; 0">
			  				<xsl:value-of select="ceiling(number($maxPob) div number($recordPerTour))"/> 	  				
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="number(1)"/>
						</xsl:otherwise>	
					</xsl:choose>
				</xsl:variable>
				
				<!-- IADC Page 3 Construct -->
				<xsl:if test="$pageOverflow &gt; 0">					
					<xsl:call-template name="iadc_page_03_rendering">
						<xsl:with-param name="pageOverflow" select="number($pageOverflow) - 1"/>
						<xsl:with-param name="currentRigInformationUid" select="$currentRigInformationUid"/>
						<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
						<xsl:with-param name="counter" select="number(0)"/>
					</xsl:call-template>					
				</xsl:if>
					
				<!-- END IADC Page 3 Construct -->
				
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
		
	<xsl:template name="iadc_page_03_rendering">
		<xsl:param name="currentRigInformationUid"/>
		<xsl:param name="pageOverflow"/>
		<xsl:param name="selected_rigTours"/>
		<xsl:param name="counter"/>
		
		<fo:block break-before="page"/>
		
		<xsl:call-template name="iadc_header_page_03"> 
			<xsl:with-param name="currentRigInformationUid" select="$currentRigInformationUid"/>
		</xsl:call-template>
		<xsl:call-template name="iadc_page_03_content"> 
			<xsl:with-param name="currentRigInformationUid" select="$currentRigInformationUid"/>
			<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
			<xsl:with-param name="counter" select="$counter"/>
		</xsl:call-template>
		
		<!-- Recursive template call for oveflow -->
		<xsl:if test="$pageOverflow &gt; 0">
			<xsl:call-template name="iadc_page_03_rendering">
			  <xsl:with-param name="pageOverflow" select="number($pageOverflow) - 1"/>
			  <xsl:with-param name="currentRigInformationUid" select="$currentRigInformationUid"/>
			  <xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
			  <xsl:with-param name="counter" select="number($counter) + 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!-- IADC Page 03 END -->
	
</xsl:stylesheet>