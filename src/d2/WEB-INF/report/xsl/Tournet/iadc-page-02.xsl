<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- IADC Page 02 -->
		<xsl:template name="iadc_page_02">
			<xsl:param name="currentRigInformationUid"/>
			<xsl:param name="pageOverflow"/>
			<xsl:param name="selected_rigTours"/>
			<xsl:param name="counter"/>
			
			<fo:block break-before="page"/>
			
			<xsl:call-template name="iadc_header_page_02"> 
				<xsl:with-param name="currentRigInformationUid" select="$currentRigInformationUid"/>
			</xsl:call-template>
			<xsl:call-template name="iadc_page_02_content"> 
				<xsl:with-param name="currentRigInformationUid" select="$currentRigInformationUid"/>
				<xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
				<xsl:with-param name="counter" select="$counter"/>
			</xsl:call-template>
			
			<!-- Recursive template call for oveflow -->
			<xsl:if test="$pageOverflow &gt; 0">
				<xsl:call-template name="iadc_page_02">
				  <xsl:with-param name="pageOverflow" select="number($pageOverflow) - 1"/>
				  <xsl:with-param name="currentRigInformationUid" select="$currentRigInformationUid"/>
				  <xsl:with-param name="selected_rigTours" select="$selected_rigTours"/>
				  <xsl:with-param name="counter" select="number($counter) + 1"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:template>
	
	
	<!-- IADC Page 02 END -->
	
</xsl:stylesheet>