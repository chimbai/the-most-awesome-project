<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- 21 -->
	
	<xsl:import
		href="./ReportTemplate/activity_record/ddr_activity_record_type1.xsl" />
	<xsl:import
		href="./ReportTemplate/activity_record/ddr_next_day_activity_record_type1.xsl" />
	<xsl:import
		href="./ReportTemplate/bharun_record/ddr_bharun_record_type2.xsl" />
	<xsl:import
		href="./ReportTemplate/cost_record/ddr_cost_record_type1.xsl"/>
	<xsl:import
		href="./ReportTemplate/casing_record/ddr_casing_record_type2.xsl" />
	<xsl:import
		href="./ReportTemplate/drillingparam_record/ddr_drilling_parameters_record_type2.xsl" />
	<xsl:import
		href="./ReportTemplate/formation_record/ddr_formation_record_type1.xsl" />
	<xsl:import
		href="./ReportTemplate/gencomment_record/ddr_gencomments_record_type2.xsl" />
	<xsl:import
		href="./ReportTemplate/hse_record/ddr_hse_record_type2.xsl" /> 
	<xsl:import	
		href="./ReportTemplate/marine_record/ddr_marine_record_type2.xsl" />
	<xsl:import 
		href="./ReportTemplate/supportvessel_record/ddr_supportvessel_record_type1.xsl" />
	<xsl:import
		href="./ReportTemplate/muddaily_record/ddr_muddaily_record_type2.xsl" />
	<xsl:import
		href="./ReportTemplate/mudvolume_record/ddr_mudvolume_record_type2.xsl" />
	<xsl:import
		href="./ReportTemplate/pob_record/ddr_pob_record_type2.xsl" />
	<xsl:import
		href="./ReportTemplate/rigpumps_record/ddr_rigpumps_record_type2.xsl" />
	<xsl:import
		href="./ReportTemplate/rigstock_record/ddr_rigstock_record_type2.xsl" />
	<xsl:import
		href="./ReportTemplate/surveyreference_record/ddr_surveyreference_record_type2.xsl" />
	<xsl:import
		href="./ReportTemplate/transportdaily_record/ddr_transportdaily_record_type1.xsl" />
	<xsl:import
		href="./ReportTemplate/weatherenvironment_record/ddr_weatherenvironment_record_type3.xsl" />
	<xsl:import
		href="./ReportTemplate/well_record/ddr_wells_record_type1.xsl" />
	<xsl:import 
		href="./ReportTemplate/dvd_graph_record/dvd_graph_record_type1.xsl" />		
	<xsl:import
		href="./ReportTemplate/hse_record/ddr_hse_record_lagging_indicator.xsl" />
	<xsl:import
		href="./ReportTemplate/hse_record/ddr_hse_record_leading_indicator.xsl" />

	
	<xsl:output method="xml" />
	<xsl:preserve-space elements="*" />
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
					<fo:region-before extent="3cm"/>
					<fo:region-after extent="1.5cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple"
				initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table inline-progression-dimension="19cm"
						table-layout="fixed" space-after="12pt" padding="10px">
						<fo:table-column column-number="1"
							column-width="proportional-column-width(50)" />
						<fo:table-column column-number="2"
							column-width="proportional-column-width(50)" />
						<fo:table-body space-before="10pt"
							space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="12pt">
									<fo:block>
										<fo:external-graphic
											content-width="scale-to-fit" width="90pt"
											content-height="60%" scaling="uniform"
											src="url(d2://images/company_logo.jpg)" />										
									</fo:block>
								</fo:table-cell>
								<fo:table-cell
									number-columns-spanned="2" font-size="6pt"
									display-align="center">
									<fo:block text-align="right"
										font-weight="bold">
										<fo:table
											inline-progression-dimension="19cm" table-layout="fixed"
											space-after="12pt">
											<fo:table-column
												column-number="1"
												column-width="proportional-column-width(33)" />
											<fo:table-column
												column-number="2"
												column-width="proportional-column-width(33)" />
											<fo:table-body
												space-before="6pt" space-after="6pt">
												<fo:table-row>
													<fo:table-cell
														font-size="12pt">
														<fo:block>
															<fo:inline
																text-decoration="underline">
																DRILLING
																MORNING
																REPORT #
																<xsl:value-of
																	select="/root/modules/ReportDaily/ReportDaily/reportNumber" />
															</fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell
														text-align="right" font-size="12pt" font-weight="bold"
														display-align="before">
														<fo:block>
															<fo:inline
																text-decoration="underline">
																<xsl:value-of
																	select="/root/modules/Operation/Operation/operationName" />
															</fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="19cm"
						table-layout="fixed" space-after="12pt">
						<fo:table-column column-number="1"
							column-width="proportional-column-width(50)" />
						<fo:table-column column-number="2"
							column-width="proportional-column-width(50)" />
						<fo:table-body space-before="6pt"
							space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS, 20080801, IDS_DEMO_drillg_onshore
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="8pt">
									<fo:block text-align="right">
										Page
										<fo:page-number />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell font-size="6pt" number-columns-spanned="2">
									<fo:block text-align="left">
										Printed on
										<xsl:variable name="printdate"
											select="/root/modules/Daily/Daily/dayDate" />
										<xsl:value-of
											select="d2Utils:getSystemDate('dd MMM yyyy')" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" font-size="8pt">
					<fo:block>
						<fo:table inline-progression-dimension="19cm"
							table-layout="fixed">
							<fo:table-column column-number="1"
								column-width="proportional-column-width(100)" />
							<fo:table-body>
							
								<!-- WELL DATA -->
								<fo:table-row keep-together="always">
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:block>
											<xsl:apply-templates
												select="/root/modules/Well/Well" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
																
								<!-- TODAY ACTIVITY -->
								<xsl:if test="count(/root/modules/Activity/Activity) &gt; 0">
									<fo:table-row>
										<fo:table-cell padding-bottom="2px" padding-top="2px">
											<fo:block>
												<xsl:apply-templates
													select="/root/modules/Activity" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- NEXT DAY ACTIVITY -->
								<xsl:if test="count(/root/modules/NextDayActivity/Activity) &gt; 0">
									<fo:table-row>
										<fo:table-cell padding-bottom="2px" padding-top="2px">
											<fo:block>
												<xsl:apply-templates
													select="/root/modules/NextDayActivity" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- GENERAL COMMENT -->
								<xsl:if test="count(/root/modules/GeneralComment/GeneralComment) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-bottom="2px" padding-top="2px">
											<fo:block>
												<xsl:apply-templates
													select="/root/modules/GeneralComment" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- MUD PROPERTIES -->
								<xsl:if test="count(/root/modules/MudProperties/MudProperties) &gt; 0">
									<xsl:apply-templates select="/root/modules/MudProperties" />
								</xsl:if>
								
																							
								<!-- BHA -->
								<xsl:if
									test="count(/root/modules/Bharun/Bharun) &gt; 0">
									<xsl:apply-templates
										select="/root/modules/Bharun" />
								</xsl:if>
								
								<!-- DRILLING PARAMETERS -->
								<xsl:if
									test="count(/root/modules/DrillingParameters/DrillingParameters) &gt; 0">
									<xsl:apply-templates
										select="/root/modules/DrillingParameters" />
								</xsl:if>
								
								<!-- SURVEY -->
								<xsl:if test="count(/root/modules/SurveyReference/SurveyReference/SurveyStation) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates
													select="/root/modules/SurveyReference" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- BULK STOCK -->
								<xsl:if test="count(/root/modules/RigStock/RigStock) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-top="2px" padding-bottom="2px">
											<fo:block>
												<xsl:apply-templates
													select="/root/modules/RigStock" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- PUMP -->
								<xsl:if test="count(/root/modules/RigPump/RigPump/RigPumpParam) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-top="2px" padding-bottom="2px">
											<fo:block>
												<xsl:apply-templates
													select="/root/modules/RigPump" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
																
								<!-- CASING -->
								<xsl:if test="count(/root/modules/CasingSection/CasingSection) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-top="2px" padding-bottom="2px">
											<fo:block>
												<xsl:apply-templates
													select="/root/modules/CasingSection" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- POB -->
								<xsl:if test="count(/root/modules/PersonnelOnSite/PersonnelOnSite) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-top="2px" padding-bottom="2px">
											<fo:block>
												<xsl:apply-templates
													select="/root/modules/PersonnelOnSite" />											
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- HSE DATA -->
								<xsl:if test="count(/root/modules/HseIncident/HseIncident) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-top="2px" padding-bottom="2px">
											<xsl:apply-templates
												select="/root/modules/HseIncident" />
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- MUD VOLUMES -->
								<xsl:if test="count(/root/modules/MudVolumes/MudVolumes) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-top="2px" padding-bottom="2px">
											<fo:block>
												<xsl:apply-templates
													select="/root/modules/MudVolumes" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								
								<!-- SUPPORT VESSEL -->
								<xsl:if test="count(/root/modules/SupportVesselParam/SupportVesselParam) &gt; 0">
									<fo:table-row>
										<fo:table-cell padding-top="2px" padding-bottom="2px">
											<fo:block>
												<xsl:apply-templates select="/root/modules/SupportVesselParam" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>							
								
								<!-- MARINE -->
								<!--
								<xsl:if test="count(/root/modules/MarineProperties/MarineProperties) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-top="2px" padding-bottom="2px">
											<fo:block>
												<xsl:apply-templates
													select="/root/modules/MarineProperties/MarineProperties" />												
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
								-->
								
								<!-- TRANSPORT -->
								<fo:table-row keep-together="always">
									<fo:table-cell>
										<fo:block>
											<xsl:if
												test="count(/root/modules/TransportDailyParam/TransportDailyParam) &gt; 0">
												<xsl:apply-templates
													select="/root/modules/TransportDailyParam" />
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<!-- WEATHER -->
								<xsl:if test="count(/root/modules/WeatherEnvironment/WeatherEnvironment) &gt; 0">
									<fo:table-row keep-together="always">
										<fo:table-cell padding-top="2px" padding-bottom="2px">
											<fo:block>
												<xsl:apply-templates
													select="/root/modules/WeatherEnvironment/WeatherEnvironment" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:if>
															
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

</xsl:stylesheet>
