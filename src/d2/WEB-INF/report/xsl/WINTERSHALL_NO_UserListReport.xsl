<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:output method="xml"/>
	<xsl:preserve-space elements="*"/>
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="1.5cm" margin-bottom="1.5cm"/>
					<fo:region-before extent="3cm"/>
					<fo:region-after extent="1.5cm"/> 
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">
					<fo:block>
						<xsl:call-template name="header"/>
					</fo:block>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:block>
						<xsl:call-template name="footer"/>
					</fo:block>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body">
					<xsl:choose>
						<xsl:when test="count(/root/modules/AclGroup/AclGroup) &gt; 0">					
       						<xsl:apply-templates select="/root/modules/AclGroup/AclGroup"/>
       					</xsl:when>
       					<xsl:otherwise>
       						<fo:block><fo:inline color="white">.</fo:inline></fo:block>
       					</xsl:otherwise>
       				</xsl:choose>	    	
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	
	<xsl:template name="header">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-before="3pt" space-after="3pt"
			padding-top="0.8cm">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body space-before="2pt" space-after="2pt">
				<fo:table-row>					
					<fo:table-cell font-size="10pt" text-align="center" font-weight="bold">
						<fo:block>
							USER LIST
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>				
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="footer">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="10pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body space-before="12pt" space-after="6pt">
				<fo:table-row>
					<fo:table-cell font-size="6pt" font-weight="bold">
						<fo:block text-align="left">
							<xsl:value-of select="//signedby"/>										
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell font-size="6pt">
						<fo:block text-align="left">
							<xsl:value-of select="//signtitle"/>										
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="6pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(90)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
			<fo:table-body space-before="6pt" space-after="6pt">
				<fo:table-row>
					<fo:table-cell font-size="6pt">
						<fo:block text-align="left">
							Copyright IDS 2014, IDS_USERLISTREPORT
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-size="6pt">
						<fo:block text-align="right">
							Page <fo:page-number/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell font-size="6pt" number-columns-spanned="2">
						<fo:block text-align="left">Printed on
							<xsl:value-of select="d2Utils:getSystemDate('dd MMM yyyy')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="modules/AclGroup/AclGroup">
		<xsl:variable name="aclGroupUid" select="aclGroupUid" />
		<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" padding-top="0.5cm">		
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell font-size="10pt" font-weight="bold" text-align="center" 
					border-top="thin solid black" border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black">
						<fo:block>
							Group
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-size="10pt" font-weight="bold" text-align="center" 
					border-top="thin solid black" border-bottom="thin solid black" border-right="thin solid black">
						<fo:block>
							<xsl:value-of select="name"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell number-columns-spanned="2">
						<fo:block>
							<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" border-bottom="thin solid black">		
								<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
								<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
								<fo:table-column column-number="3" column-width="proportional-column-width(40)"/>
								<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell font-size="8pt" font-weight="bold" text-align="center"  background-color="#DDDDDD"
										border-left="thin solid black" border-right="thin solid black" border-bottom="thin solid black" padding="0.1cm">
											<fo:block>
												Username
											</fo:block>
										</fo:table-cell>
										<fo:table-cell font-size="8pt" font-weight="bold" text-align="center" background-color="#DDDDDD"
										border-right="thin solid black" border-bottom="thin solid black" padding="0.1cm">
											<fo:block>
												Full Name
											</fo:block>
										</fo:table-cell>
										<fo:table-cell font-size="8pt" font-weight="bold" text-align="center" background-color="#DDDDDD"
										border-right="thin solid black" border-bottom="thin solid black" padding="0.1cm" >
											<fo:block>
												Well Operation Team
											</fo:block>
										</fo:table-cell>
										<fo:table-cell font-size="8pt" font-weight="bold" text-align="center" background-color="#DDDDDD"
										border-bottom="thin solid black" padding="0.1cm" border-right="thin solid black">
											<fo:block>
												Blocked
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								<fo:table-body>
									<xsl:choose>
										<xsl:when test="count(/root/modules/AclUser/AclGroup[aclGroupUid=$aclGroupUid]/UserAclGroup[User[userName!='idsadmin' and userName!='']]) &gt; 0">
											<xsl:apply-templates select="/root/modules/AclUser/AclGroup[aclGroupUid=$aclGroupUid]/UserAclGroup[User[userName!='idsadmin' and userName!='']]"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="UserAclGroupNoData"/>
										</xsl:otherwise>
									</xsl:choose>
									
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>	
	</xsl:template>
	
	<xsl:template match="UserAclGroup">
		<xsl:if test="User/userName != ''">
			<xsl:variable name="userUid" select="User/userUid"/>
			<xsl:choose>
				<xsl:when test="position() mod 2 != '0'">
					<fo:table-row>
						<fo:table-cell font-size="8pt" text-align="left"  padding="0.1cm"
						border-left="thin solid black" border-right="thin solid black">
							<fo:block>
								<xsl:value-of select="User/userName"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell font-size="8pt" text-align="left" padding="0.1cm"
						border-right="thin solid black" >
							<fo:block>
								<xsl:value-of select="User/fname"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell font-size="8pt" text-align="left"
						border-right="thin solid black" >
							<fo:block>
								<xsl:apply-templates select="/root/modules/OpsTeamUser/OpsTeamUser[userUid=$userUid]"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell font-size="8pt" text-align="center" padding="0.1cm"
						border-right="thin solid black">
							<fo:block>
								<xsl:value-of select="User/isblocked/@lookupLabel"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:when>
				<xsl:otherwise>
					<fo:table-row background-color="#EEEEEE">
						<fo:table-cell font-size="8pt" text-align="left"  padding="0.1cm"
						border-left="thin solid black" border-right="thin solid black">
							<fo:block>
								<xsl:value-of select="User/userName"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell font-size="8pt" text-align="left" padding="0.1cm"
						border-right="thin solid black" >
							<fo:block>
								<xsl:value-of select="User/fname"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell font-size="8pt" text-align="left"
						border-right="thin solid black" >
							<fo:block>
								<xsl:apply-templates select="/root/modules/OpsTeamUser/OpsTeamUser[userUid=$userUid]"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell font-size="8pt" text-align="center" padding="0.1cm"
						border-right="thin solid black">
							<fo:block>
								<xsl:value-of select="User/isblocked/@lookupLabel"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template name="UserAclGroupNoData"> 
		<fo:table-row>
			<fo:table-cell number-columns-spanned="4" font-size="8pt" text-align="left" padding="0.1cm"
			border-right="thin solid black" border-left="thin solid black">
				<fo:block>
					No users found in this group
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<xsl:template match="OpsTeamUser">
		<xsl:variable name="opsTeamUid" select="opsTeamUid"/>
		<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell font-size="8pt" text-align="left" padding="0.1cm">
						<fo:block>
							<xsl:value-of select="/root/modules/OpsTeam/OpsTeam[opsTeamUid=$opsTeamUid]/name"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>
