<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:import href="./ReportTemplate/cost_record/mgt_cost_record_type1.xsl"/>
	<xsl:import href="./ReportTemplate/formation_record/mgt_formation_record_geology_type1.xsl" />
	<!-- For Woodside. Enable if needed -->
	<!--
	<xsl:import href="./ReportTemplate/hse_record/ddr_hse_record_lagging_indicator.xsl" />
	-->
		
	<xsl:output method="xml"/>
	<xsl:preserve-space elements="*"/>
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
					<fo:region-before extent="3cm"/>
					<fo:region-after extent="1.5cm"/> 
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">
					<fo:block>
						<xsl:call-template name="header"/>
					</fo:block>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:block>
						<xsl:call-template name="footer"/>
					</fo:block>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" font-size="12pt">
					<xsl:choose>
						<xsl:when test="count(/root/modules/ReportDaily) &gt; 0">					
       						<xsl:apply-templates select="/root/modules/ReportDaily" mode="list"/>
       					</xsl:when>
       					<xsl:otherwise>
       						<fo:block><fo:inline color="white">.</fo:inline></fo:block>
       					</xsl:otherwise>
       				</xsl:choose>	    	
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	
	<xsl:template name="header">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-before="12pt" space-after="12pt"
			padding-top="0.3cm">
			<fo:table-column column-number="1" column-width="proportional-column-width(2)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(28)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(70)"/>
			<fo:table-body space-before="6pt" space-after="6pt">
				<fo:table-row>					
					<fo:table-cell font-size="12pt" number-columns-spanned="3" text-align="center">
						<fo:block>
							<fo:external-graphic content-width="scale-to-fit" height="1.8cm"
								width="120pt" content-height="50%" scaling="uniform"
								src="url(d2://images/company_logo.jpg)" />										
						</fo:block>
					</fo:table-cell>					
				</fo:table-row>				
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="footer">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="10pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body space-before="12pt" space-after="6pt">
				<fo:table-row>
					<fo:table-cell font-size="8pt" font-weight="bold">
						<fo:block text-align="left">
							<xsl:value-of select="//signedby"/>										
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell font-size="8pt">
						<fo:block text-align="left">
							<xsl:value-of select="//signtitle"/>										
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="6pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(90)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
			<fo:table-body space-before="6pt" space-after="6pt">
				<fo:table-row>
					<fo:table-cell font-size="8pt">
						<fo:block text-align="left">
							Copyright IDS, 20091001, CENTRALPETROLEUM_AU_DPR																	
						</fo:block>
					</fo:table-cell>
					<fo:table-cell font-size="8pt">
						<fo:block text-align="right">
							<!-- Page <fo:page-number/> -->
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell font-size="6pt" number-columns-spanned="2">
						<fo:block text-align="left">Printed on
							<xsl:variable name="printdate" select="/root/modules/Daily/Daily/dayDate"/>
							<xsl:value-of select="d2Utils:getSystemDate('dd MMM yyyy')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="modules/ReportDaily" mode="list">
		<xsl:variable name="currentwellid" select="ReportDaily/wellUid" />		
		<xsl:variable name="currentdailyid" select="ReportDaily/reportDailyUid" />
		<xsl:choose>
			<xsl:when test="position() &lt; last()" >
				<fo:block break-after="page">
					<xsl:apply-templates select="/root/modules/Well/Well[wellUid = $currentwellid]" mode="managementreport">
						<xsl:sort select="/root/modules/Well/Well[wellUid = $currentwellid]/wellName" />
						<xsl:with-param name="currentdailyid" select="$currentdailyid"/>
					</xsl:apply-templates>
				</fo:block>
			</xsl:when>
			<xsl:otherwise>
				<fo:block>
					<xsl:apply-templates select="/root/modules/Well/Well[wellUid = $currentwellid]" mode="managementreport">
						<xsl:sort select="/root/modules/Well/Well[wellUid = $currentwellid]/wellName" />
						<xsl:with-param name="currentdailyid" select="$currentdailyid"/>
					</xsl:apply-templates>
				</fo:block>
			</xsl:otherwise>
		</xsl:choose>		
	</xsl:template>
	
	<xsl:template match="modules/Well/Well" mode="managementreport">
		<xsl:variable name="currentwellid" select="wellUid" />		
		<xsl:variable name="currentoperationUid" select="/root/modules/Operation/Operation[wellUid=$currentwellid]/operationUid" />
		<xsl:variable name="currentrig" select="/root/modules/Operation/Operation[wellUid=$currentwellid]/rigInformationUid" />
		<xsl:variable name="reportDaily" select="/root/modules/ReportDaily/ReportDaily[wellUid=$currentwellid]" />	
		<xsl:variable name="currentdailyid" select="$reportDaily/dailyUid"/>	 
		<xsl:variable name="current_drillcoid" select="/root/modules/RigInformation/RigInformation[rigInformationUid=$currentrig]/rigOwner/@lookupLabel" />
		<xsl:variable name="dvd" select="concat($currentoperationUid,'_dvd')"/>
		<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-before="24pt" padding-top="0.5cm" >		
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell font-size="14pt" font-weight="bold" text-align="center">
						<fo:block color="blue">
							<xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/wellName" />						
						</fo:block>						
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell font-size="14pt" font-weight="bold" text-align="center">
						<fo:block>DAILY PARTNER REPORT</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>				
			<fo:table-body font-size="8pt" font-weight="bold">
				<fo:table-row keep-together="always">
					<fo:table-cell font-size="12pt">
						<fo:table inline-progression-dimension="19cm" space-before="2pt" table-layout="fixed" border="0.25px solid black" font-size="10pt" font-weight="bold">
							<fo:table-column column-number="1" column-width="proportional-column-width(16)"/>
							<fo:table-column column-number="2" column-width="proportional-column-width(16)"/>
							<fo:table-column column-number="3" column-width="proportional-column-width(16)"/>
							<fo:table-column column-number="4" column-width="proportional-column-width(16)"/>
							<fo:table-column column-number="5" column-width="proportional-column-width(16)"/>
							<fo:table-column column-number="6" column-width="proportional-column-width(17)"/>
							<fo:table-body font-size="8pt" font-weight="bold">
								<fo:table-row>
									<fo:table-cell text-align="left" border-bottom="0.25px solid black" padding="0.1cm" >
										<fo:block>
											Report Date:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" text-align="right" padding="0.1cm" number-columns-spanned="2">
										<fo:block color="blue">
											<xsl:value-of select="d2Utils:formatDateFromEpochMS($reportDaily/dynaAttr/daydate/@epochMS,'dd MMM yyyy')"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="left" border-bottom="0.25px solid black" padding="0.1cm" >
										<fo:block>
											Report Number:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" text-align="right" padding="0.1cm" number-columns-spanned="2">
										<fo:block color="blue">
											<xsl:value-of select="$reportDaily/reportNumber"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell text-align="left" border-bottom="0.25px solid black" padding="0.1cm" >
										<fo:block>
											Days Since Spud:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" text-align="right" padding="0.1cm" number-columns-spanned="2">
										<fo:block color="blue">											
											<xsl:value-of select="translate($reportDaily/dynaAttr/daysFromSpud,'d','')"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="left" border-bottom="0.25px solid black" padding="0.1cm" >
										<fo:block>
											Days on Well:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" text-align="right" padding="0.1cm" number-columns-spanned="2">
										<fo:block color="blue">
											<xsl:value-of select="translate($reportDaily/dynaAttr/daysOnWell,'d','')"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row >
									<fo:table-cell text-align="left" border-bottom="0.25px solid black" padding="0.1cm" >
										<fo:block>
											Drilling Co.:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-right="0.25px solid black" border-bottom="0.25px solid black" text-align="right" padding="0.1cm" >
										<fo:block color="blue">
											<xsl:value-of select="$current_drillcoid"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="left" border-bottom="0.25px solid black" padding="0.1cm" >
										<fo:block>
											Rig:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-right="0.25px solid black" border-bottom="0.25px solid black" text-align="right" padding="0.1cm" >
										<fo:block color="blue">
											<xsl:value-of select="$currentrig/@lookupLabel"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row >
									<fo:table-cell text-align="left" border-bottom="0.25px solid black" padding="0.1cm" >
										<fo:block>
											Depth (MD/TVD):
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-right="0.25px solid black" border-bottom="0.25px solid black" text-align="right" padding="0.1cm" >
										<fo:block color="blue">
											<xsl:value-of select="$reportDaily/depthMdMsl"/><fo:inline color="white">!</fo:inline>
											<xsl:value-of select="$reportDaily/depthMdMsl/@uomSymbol" /> /
											<xsl:value-of select="$reportDaily/depthTvdMsl"/><fo:inline color="white">!</fo:inline>
											<xsl:value-of select="$reportDaily/depthTvdMsl/@uomSymbol" />
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="left" border-bottom="0.25px solid black" padding="0.1cm" >
										<fo:block>
											Progress:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="right" number-columns-spanned="2" border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm" >
										<fo:block color="blue">
											<xsl:variable name="progress" select="string(sum(/root/modules/Bharun/Bharun[wellUid=$currentwellid]/BharunDailySummary[dailyUid = $currentdailyid]/progress/@rawNumber))"/>												
											<xsl:value-of select="d2Utils:formatNumber($progress,'#,##0.00')"/><fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Bharun/Bharun[wellUid=$currentwellid]/BharunDailySummary[dailyUid = $currentdailyid]/progress/@uomSymbol"/>											
											with <xsl:value-of select="$reportDaily/lastHolesize"/><fo:inline color="white">!</fo:inline>
											<xsl:value-of select="$reportDaily/lastHolesize/@uomSymbol" /> hole
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row >
									<fo:table-cell text-align="left" border-bottom="0.25px solid black" padding="0.1cm" >
										<fo:block>
											Last Casing Size:
										</fo:block>
									</fo:table-cell>
									<fo:table-cell number-columns-spanned="2" border-right="0.25px solid black" border-bottom="0.25px solid black" text-align="right" padding="0.1cm" >
										<fo:block color="blue">
											<xsl:choose>													
												<xsl:when test = "string($reportDaily/lastCsgsize/@lookupLabel)=''">
													<xsl:value-of select="$reportDaily/lastCsgsize"/>										
												</xsl:when>													
												<xsl:otherwise>
													<xsl:value-of select="$reportDaily/lastCsgsize/@lookupLabel"/>
												</xsl:otherwise>
											</xsl:choose>											
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="left" number-columns-spanned="2" border-bottom="0.25px solid black" padding="0.1cm" >
										<fo:block>
											Casing Shoe Depth (MD)
										</fo:block>
									</fo:table-cell>
									<fo:table-cell padding="0.1cm" border-right="0.25px solid black" border-bottom="0.25px solid black" text-align="right">
										<fo:block color="blue">
											<xsl:value-of select="$reportDaily/lastCsgshoeMdMsl"/><fo:inline color="white">!</fo:inline>
											<xsl:value-of select="$reportDaily/lastCsgshoeMdMsl/@uomSymbol" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell border-bottom="0.25px solid black" border-right="0.25px solid black" number-columns-spanned="6">
										<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap">
											<fo:table-column column-number="1" column-width="proportional-column-width(8)"/>
											<fo:table-column column-number="2" column-width="proportional-column-width(12)"/>
											<fo:table-column column-number="3" column-width="proportional-column-width(8)"/>
											<fo:table-column column-number="4" column-width="proportional-column-width(14)"/>
											<fo:table-column column-number="5" column-width="proportional-column-width(8)"/>
											<fo:table-column column-number="6" column-width="proportional-column-width(14)"/>
											<fo:table-column column-number="7" column-width="proportional-column-width(17)"/>
											<fo:table-column column-number="8" column-width="proportional-column-width(12)"/>
											<fo:table-header>
												<fo:table-row>
													<fo:table-cell number-columns-spanned="8" border-top="0.25px solid black" padding="0.1cm" text-align="left" font-weight="bold">
														<fo:block>
															COST DATA
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-header>
											<fo:table-body>
												<fo:table-row>
													<fo:table-cell padding="0.1cm" text-align="left">
														<fo:block>
															DAILY :
														</fo:block>
													</fo:table-cell>
													<fo:table-cell padding="0.1cm" text-align="left">
														<fo:block color="blue">
															<xsl:value-of select="$reportDaily/daycost/@uomSymbol" /><fo:inline color="white">!</fo:inline>
															<xsl:value-of select="d2Utils:formatNumber($reportDaily/daycost,'##,###')"/>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell padding="0.1cm" text-align="left">
														<fo:block>
															CUM :
														</fo:block>
													</fo:table-cell>
													<fo:table-cell padding="0.1cm" text-align="left">
														<fo:block color="blue">
															<xsl:value-of select="$reportDaily/daycost/@uomSymbol" /><fo:inline color="white">!</fo:inline>
															<xsl:value-of select="d2Utils:formatNumber($reportDaily/dynaAttr/cumcost,'##,###')"/>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell padding="0.1cm" text-align="left">
														<fo:block>
															AFE :
														</fo:block>
													</fo:table-cell>
													<fo:table-cell padding="0.1cm" text-align="left">
														<fo:block color="blue">
															<xsl:value-of select="/root/modules/Operation/Operation[wellUid=$currentwellid]/afe/@uomSymbol"/>
															<fo:inline color="white">.</fo:inline>
															<xsl:value-of select="/root/modules/Operation/Operation[wellUid=$currentwellid]/afe"/>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell padding="0.1cm" text-align="left">
														<fo:block>
															DIFF. AFE-CUM :
														</fo:block>
													</fo:table-cell>
													<fo:table-cell padding="0.1cm" text-align="right">
														<fo:block color="blue">
															<xsl:variable name="afe" select="/root/modules/Operation/Operation[wellUid=$currentwellid]/afe/@rawNumber"/>
															<xsl:variable name="cumcost" select="$reportDaily/dynaAttr/cumcost/@rawNumber"/>
															<!-- <xsl:variable name="diff_afe" select="string($afe - $cumcost)"/> -->															
															<xsl:choose>													
																<xsl:when test = "string($afe)!=''">
																	<xsl:choose>
																		<xsl:when test="string($cumcost)!=''">
																			<xsl:variable name="diff_afe" select="string($afe - $cumcost)"/>
																			<xsl:value-of select="/root/modules/Operation/Operation[wellUid=$currentwellid]/afe/@uomSymbol"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="d2Utils:formatNumber($diff_afe,'##,###')"/>																			
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:variable name="diff_afe" select="string($afe - 0)"/>
																			<xsl:value-of select="/root/modules/Operation/Operation[wellUid=$currentwellid]/afe/@uomSymbol"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="d2Utils:formatNumber($diff_afe,'##,###')"/>
																		</xsl:otherwise>
																	</xsl:choose>														
																</xsl:when>													
																<xsl:otherwise>
																	<xsl:choose>
																		<xsl:when test="string($cumcost)!=''">
																			<xsl:variable name="diff_afe" select="string(0 - $cumcost)"/>
																			<xsl:value-of select="/root/modules/Operation/Operation[wellUid=$currentwellid]/afe/@uomSymbol"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="d2Utils:formatNumber($diff_afe,'##,###')"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="/root/modules/Operation/Operation[wellUid=$currentwellid]/afe/@uomSymbol"/>
																			<fo:inline color="white">.</fo:inline>
																			<xsl:value-of select="0"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:otherwise>
															</xsl:choose>															
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table>							
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="6" padding="0.1cm">
										<fo:block>
											Details of Past 24 Hour Operations:
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell border-bottom="0.25px solid black" number-columns-spanned="6" padding="0.1cm">
										<fo:block color="blue">
											<xsl:value-of select="$reportDaily/reportPeriodSummary"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="6" padding="0.1cm">
										<fo:block>
											Operations Update @ 0600 Hours:
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell border-bottom="0.25px solid black" number-columns-spanned="6" padding="0.1cm">
										<fo:block color="blue">
											<xsl:value-of select="$reportDaily/reportCurrentStatus"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell number-columns-spanned="6" padding="0.1cm">
										<fo:block>
											Operations Forecast:
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell border-bottom="0.25px solid black" number-columns-spanned="6" padding="0.1cm">
										<fo:block color="blue">
											<xsl:value-of select="$reportDaily/reportPlanSummary"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<!-- Additional Add-in Centralpetroleum	-->	
								<!-- Requested to remove by JH
								<fo:table-row>
									<fo:table-cell number-columns-spanned="6" padding="0.1cm">
										<fo:block>
											Geology Summary:
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								-->
								<fo:table-row>
									<fo:table-cell number-columns-spanned="6" padding="0.1cm">
										<fo:block>
											Formation Summary:
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell border-bottom="0.25px solid black" number-columns-spanned="6" padding="0.1cm">
										<fo:block color="blue">
											<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily[operationUid = $currentoperationUid]/geology0600FormationSummary"/>	
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row>
									<fo:table-cell number-columns-spanned="6" padding="0.1cm">
										<fo:block>
											Lithology Summary:
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell border-bottom="0.25px solid black" number-columns-spanned="6" padding="0.1cm">
										<fo:block color="blue">
											<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily[operationUid = $currentoperationUid]/geology0600LithologySummary"/>	
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row>
									<fo:table-cell number-columns-spanned="6" padding="0.1cm">
										<fo:block>
											Geology Fwd Plan:
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell border-bottom="0.25px solid black" number-columns-spanned="6" padding="0.1cm">
										<fo:block color="blue">
											<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily[operationUid = $currentoperationUid]/geolOps"/>	
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row>
									<fo:table-cell number-columns-spanned="6" padding="0.1cm">
										<fo:block>
											Geology Comments:
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell border-bottom="0.25px solid black" number-columns-spanned="6" padding="0.1cm">
										<fo:block color="blue">
											<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily[operationUid = $currentoperationUid]/reportPeriodSummary"/>		
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
				
				<!-- Prognosis and Preliminary Correlation -->
				<!-- Request to Remove by JH
				<fo:table-row>
						<fo:table-cell>
							<fo:block>
								<xsl:variable name="lastFormationUid" select="/root/modules/ReportDaily/ReportDaily/dynaAttr/lastFormationUid"/>
								<xsl:if test="$lastFormationUid !=''">
									<xsl:apply-templates select="/root/modules/Formation" />
								</xsl:if>
							</fo:block>
						</fo:table-cell>
				</fo:table-row>
				-->
				
				<!-- For Woodside. Enable if needed -->
				<!-- 
				<fo:table-row keep-together="always">
					<fo:table-cell>
						<fo:block>													
							<xsl:apply-templates select="/root/modules/OperationCostByPhaseCode">
								<xsl:with-param name="operationUid" select="$currentoperationUid"/>
							</xsl:apply-templates>
						</fo:block>						
					</fo:table-cell>
				</fo:table-row>
			     -->
				<fo:table-row keep-together="always">
					<fo:table-cell>
						<fo:block>													
				        	<xsl:if	test="count(/root/modules/LaggingIndicator/LaggingIndicator[operationUid = $currentoperationUid]/Indicator) &gt; 0">
				        		<xsl:apply-templates select="/root/modules/LaggingIndicator/LaggingIndicator[operationUid = $currentoperationUid]"/>
				        	</xsl:if>
						</fo:block>						
					</fo:table-cell>
				</fo:table-row>	
				 				
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:external-graphic content-width="scale-to-fit" width="650pt"								
								content-height="5.5in" scaling="uniform" src="url(d2://../graph/dvd/{$dvd}.jpg)" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>			
		</fo:table>
	</xsl:template>
</xsl:stylesheet>
