<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
    <xsl:output method="xml"/>
    <xsl:preserve-space elements="*"/>
    <xsl:template match="/">
    <fo:root>
        <fo:layout-master-set>
            <fo:simple-page-master master-name="simple" page-height="29cm" page-width="21cm" margin-left="1cm" margin-right="1cm" margin-top="1cm">
             	<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
               	<fo:region-before extent="2cm"/>
               	<fo:region-after extent="2cm"/>
            </fo:simple-page-master>
        </fo:layout-master-set>
        <fo:page-sequence master-reference="simple" initial-page-number="1">
        	<fo:static-content flow-name="xsl-region-before">
        		<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="6pt">
        			<fo:table-column column-number="1" column-width="proportional-column-width(20)"/>
        			<fo:table-column column-number="2" column-width="proportional-column-width(20)"/>
        			<fo:table-column column-number="3" column-width="proportional-column-width(20)"/>
        			<fo:table-column column-number="4" column-width="proportional-column-width(20)"/>
        			<fo:table-column column-number="5" column-width="proportional-column-width(20)"/>
        			<fo:table-body space-before="6pt" space-after="2pt">
                        <fo:table-row>
							<fo:table-cell font-size="12pt">
									<fo:block>
										<fo:external-graphic content-width="scale-to-fit"
											width="100pt" content-height="50%" scaling="uniform" src="url(d2://images/logo_bj.jpg)"/>
									</fo:block>
								</fo:table-cell>
							<fo:table-cell font-size="6pt">
                                <fo:block color="white">PO</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
        			</fo:table-body>
        		</fo:table>
        	</fo:static-content>
        	<fo:static-content flow-name="xsl-region-after">
        		<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="12pt">
        			<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
        			<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
        			<fo:table-body space-before="6pt" space-after="6pt">
        				<fo:table-row>
        					<fo:table-cell font-size="6pt">
        						<fo:block text-align="left">
        							Copyright IDS, 20080602, IDS_DEMO_PO (SK)
        						</fo:block>
        					</fo:table-cell>
        					<fo:table-cell font-size="8pt">
        						<fo:block text-align="right">
        							Page <fo:page-number/>
        						</fo:block>
        					</fo:table-cell>
        				</fo:table-row>
        			</fo:table-body>
        		</fo:table>
        	</fo:static-content>
        	<fo:flow flow-name="xsl-region-body" font-size="8pt">
        		<fo:block>
					<xsl:apply-templates select="/root/modules/InventoryDocument"/>
        		</fo:block>
        	</fo:flow>
        </fo:page-sequence>
        </fo:root>
        </xsl:template>
		
		<xsl:template match="modules/InventoryDocument">
			<xsl:apply-templates select="InventoryDocument"/>
		</xsl:template>
		
        <xsl:template match="InventoryDocument">
            <fo:table inline-progression-dimension="19cm" table-layout="fixed" border="0.5px solid black" space-after="2pt" font-size="8pt">
                <fo:table-column column-number="1" column-width="proportional-column-width(4)"/>
                <fo:table-column column-number="2" column-width="proportional-column-width(8)"/>
                <fo:table-column column-number="3" column-width="proportional-column-width(4)"/>
                <fo:table-column column-number="4" column-width="proportional-column-width(8)"/>
                <fo:table-column column-number="5" column-width="proportional-column-width(4)"/>
                <fo:table-column column-number="6" column-width="proportional-column-width(8)"/>
                <fo:table-column column-number="7" column-width="proportional-column-width(4)"/>
                <fo:table-column column-number="8" column-width="proportional-column-width(8)"/>
				<fo:table-body>
                    <fo:table-row>
                        <fo:table-cell border="0.25px solid black" font-weight="bold" padding="0.1cm" display-align="center" number-columns-spanned="3" background-color="rgb(224,224,224)">
                            <fo:block text-align="left">PO Number: </fo:block>
                        </fo:table-cell>
                        <fo:table-cell border="0.25px solid black" font-weight="bold" padding="0.1cm" number-columns-spanned="5" background-color="rgb(224,224,224)">
                            <fo:block text-align="left"><xsl:value-of select="po_number"/></fo:block>
                        </fo:table-cell>
                </fo:table-row> 
                <fo:table-row>  
                        <fo:table-cell padding="0.1cm" number-columns-spanned="3">
                            <fo:block text-align="left">Date:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="0.1cm" number-columns-spanned="5">
                            <fo:block text-align="left"><xsl:value-of select="documentRaisedDate"/></fo:block>
                        </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell padding="0.1cm" display-align="center" number-columns-spanned="3">
                        <fo:block text-align="left">Send To: </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.1cm" number-columns-spanned="5" font-weight="bold">
                        <fo:block text-align="left"><xsl:value-of select="LocationToName"/></fo:block>
                    </fo:table-cell>
                </fo:table-row> 
                <fo:table-row>
                    <fo:table-cell padding="0.1cm" number-columns-spanned="3">
                        <fo:block text-align="left">Request By:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.1cm" number-columns-spanned="5" font-weight="bold">
                        <fo:block text-align="left"><xsl:value-of select="documentRaisedBy"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell padding="0.1cm" number-columns-spanned="3">
                        <fo:block text-align="left">Vendor:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.1cm" number-columns-spanned="5" font-weight="bold">
                        <fo:block text-align="left"><xsl:value-of select="deliveryCompany"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
				<fo:table-row>
                    <fo:table-cell padding="0.1cm" number-columns-spanned="3">
                        <fo:block text-align="left">Comments:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.1cm" number-columns-spanned="5" font-weight="bold">
                        <fo:block text-align="left"><xsl:value-of select="deliveryNotes"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row font-size="9pt">
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold"  number-columns-spanned="8" text-align="center" background-color="rgb(224,224,224)">
                        <fo:block>PO Details</fo:block>
                    </fo:table-cell>
                </fo:table-row>   
                <fo:table-row>
                    <fo:table-cell border-bottom="0.25px solid black" padding="0.1cm" display-align="center">
                        <fo:block text-align="left">Requested Date:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
                        <fo:block text-align="left"><xsl:value-of select="request_date"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-bottom="0.25px solid black" padding="0.1cm" display-align="center">
                        <fo:block text-align="left">Shipping Method:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm" number-columns-spanned="2">
                        <fo:block text-align="left"><xsl:value-of select="shipping_method"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-bottom="0.25px solid black" padding="0.1cm" display-align="center">
                        <fo:block text-align="left">Notes:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm" number-columns-spanned="2">
                        <fo:block text-align="left"><xsl:value-of select="shipping_notes"/></fo:block>
                    </fo:table-cell>                    
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell border-bottom="0.25px solid black" padding="0.1cm" display-align="center">
                        <fo:block text-align="left">ETA:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
                        <fo:block text-align="left"><xsl:value-of select="eta_date"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-bottom="0.25px solid black" padding="0.1cm" display-align="center">
                        <fo:block text-align="left">Required Date:</fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm" number-columns-spanned="2">
                        <fo:block text-align="left"><xsl:value-of select="required_date"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-bottom="0.25px solid black" padding="0.1cm" display-align="center">
                        <fo:block text-align="left"></fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm" number-columns-spanned="2">
                        <fo:block text-align="left"></fo:block>
                    </fo:table-cell>                    
                </fo:table-row>
                <fo:table-row font-size="9pt">
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold"  number-columns-spanned="8" text-align="center" background-color="rgb(224,224,224)">
                        <fo:block>PO Items</fo:block>
                    </fo:table-cell>
                </fo:table-row> 
                <fo:table-row font-size="9pt">
                    <fo:table-cell number-columns-spanned="8">
                    	<fo:table inline-progression-dimension="19cm" table-layout="fixed"
            				border="0.5px solid black" space-after="2pt" font-size="7pt">
            				<fo:table-column column-number="1" column-width="proportional-column-width(2)"/>
            				<fo:table-column column-number="2" column-width="proportional-column-width(12)"/>
            				<fo:table-column column-number="3" column-width="proportional-column-width(24)"/>
            				<fo:table-column column-number="4" column-width="proportional-column-width(8)"/>
            				<fo:table-column column-number="5" column-width="proportional-column-width(8)"/>
            				<fo:table-column column-number="6" column-width="proportional-column-width(8)"/>
            				<fo:table-header>
                				<fo:table-row font-size="7pt">
                    			<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="center" background-color="rgb(224,224,224)">
                        			<fo:block>Seq No.</fo:block>
                    			</fo:table-cell>
                    			<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="center" background-color="rgb(224,224,224)">
                        			<fo:block>Category</fo:block>
                    			</fo:table-cell>
                    			<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="center" background-color="rgb(224,224,224)">
                        			<fo:block>Description</fo:block>
                    			</fo:table-cell>
                    			<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="center" background-color="rgb(224,224,224)">
                        			<fo:block>Size</fo:block>
                    			</fo:table-cell>
                     			<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="center" background-color="rgb(224,224,224)">
                        			<fo:block>Qty</fo:block>
                    			</fo:table-cell>
                     			<fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" text-align="center" background-color="rgb(224,224,224)">
                        			<fo:block>Rcvd Qty</fo:block>
                    			</fo:table-cell> 
                			</fo:table-row>
            			</fo:table-header>
            			<fo:table-body>
                    		<xsl:apply-templates select="InventoryDocumentDetail"/>
            			</fo:table-body>
        			</fo:table>
                    </fo:table-cell>
                </fo:table-row>
        
                <fo:table-row font-size="9pt">
                    <fo:table-cell padding="0.075cm" border="0" font-weight="bold"  number-columns-spanned="8" text-align="center">
                        <fo:block></fo:block>
                    </fo:table-cell>
                </fo:table-row>  
                <fo:table-row font-size="7pt">
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold"  number-columns-spanned="8" text-align="center" background-color="rgb(224,224,224)">
                        <fo:block>Approval Log</fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row font-size="6pt">
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center" background-color="rgb(224,224,224)">
                        <fo:block>Action</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center" background-color="rgb(224,224,224)">
                        <fo:block>Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center" background-color="rgb(224,224,224)">
                        <fo:block>Action By</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center" background-color="rgb(224,224,224)">
                        <fo:block>Remarks</fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row font-size="6pt">
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center">
                        <fo:block>Raised</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center">
                        <fo:block text-align="center"><xsl:value-of select="documentRaisedDate"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center">
                        <fo:block text-align="center"><xsl:value-of select="documentRaisedBy"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center">
                        <fo:block text-align="center"></fo:block>
                    </fo:table-cell>
                </fo:table-row> 
                <fo:table-row font-size="6pt">
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center">
                        <fo:block>Received</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center">
                        <fo:block text-align="center"><xsl:value-of select="documentReceivedDate"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center">
                        <fo:block text-align="center"><xsl:value-of select="documentReceivedBy"/></fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="0.075cm" border="0.25px solid black" number-columns-spanned="2" text-align="center">
                        <fo:block text-align="center"><xsl:value-of select="documentReceivedNotes"/></fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>
	
    <xsl:template match="InventoryDocumentDetail">   
              <fo:table-row>
            <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
                <fo:block text-align="left"><xsl:value-of select="sequence"/></fo:block>
            </fo:table-cell>
            <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
                <fo:block text-align="left"><xsl:value-of select="category_name"/></fo:block>
            </fo:table-cell>
    	    <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
                <fo:block text-align="left"><xsl:value-of select="shortDescription"/></fo:block>
            </fo:table-cell>
            <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
                <fo:block text-align="left"><xsl:value-of select="sizeDescription"/></fo:block>
            </fo:table-cell>
            <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
                <fo:block text-align="right"><xsl:value-of select="quantityValue"/></fo:block>
            </fo:table-cell>
            <fo:table-cell border-right="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
                <fo:block text-align="right"><xsl:value-of select="received_quantity"/></fo:block>
            </fo:table-cell>
    	</fo:table-row>
    </xsl:template>
	
</xsl:stylesheet>
