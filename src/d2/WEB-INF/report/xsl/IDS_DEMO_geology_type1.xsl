<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<!-- <xsl:import
		href="./ReportTemplate/casing_record/ddr_casing_record_geol_type1.xsl" />
	<xsl:import
		href="./ReportTemplate/formation_record/ddr_formation_record_geol_type1.xsl" /> -->
	<xsl:import href="./ReportTemplate/well_record/ddr_wells_record_geol_type1.xsl" />
	<xsl:import href="./ReportTemplate/casing_record/dgr_casing_record_geology_type1.xsl" />
	<xsl:import href="./ReportTemplate/lithology_record/dgr_lithology_record_geology_type1.xsl" />
	<xsl:import href="./ReportTemplate/gasreadings_record/dgr_gasreadings_record_geology_type1.xsl" />
	<xsl:import href="./ReportTemplate/formation_record/dgr_formation_record_geology_type1.xsl" />
	
	<xsl:output method="xml" />
	<xsl:preserve-space elements="*" />
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
					<fo:region-before extent="3cm"/>
					<fo:region-after extent="1.5cm" margin-bottom="2cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">
					<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-before="12pt" padding="5px">
						<fo:table-column column-number="1" column-width="proportional-column-width(34)" />
						<fo:table-column column-number="2" column-width="proportional-column-width(33)" />
						<fo:table-column column-number="3" column-width="proportional-column-width(33)" />
						
						<fo:table-body space-before="10pt">
							<fo:table-row>
								<fo:table-cell font-size="12pt" border-top="0.5px solid black" border-bottom="0.5px solid black" border-left="0.5px solid black" padding="3px">
									<fo:block>
										<fo:external-graphic padding-left="5px" content-width="scale-to-fit" content-height="60%" scaling="uniform" src="url(d2://images/company_logo.jpg)" />
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell padding="1px" font-size="6pt" display-align="center" border-top="0.5px solid black" border-bottom="0.5px solid black" border-right="0.5px solid black">
									<fo:block font-weight="bold">
										<fo:table inline-progression-dimension="6.27cm" table-layout="fixed" space-after="6pt">
											<fo:table-column column-number="1" column-width="proportional-column-width(100)" />
											<fo:table-body space-before="6pt">
												<fo:table-row>
													<fo:table-cell font-size="10pt">
														<fo:block text-align="center">
															<fo:inline>IDS DEMO</fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell
														font-size="10pt">
														<fo:block text-align="center">
															<fo:inline>DAILY GEOLOGICAL REPORT</fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell font-size="10pt">
														<fo:block text-align="center">
															<fo:inline>( 06:00 hrs - 06:00 hrs)</fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell font-size="6pt" padding="5px" display-align="center" border-top="0.5px solid black" border-bottom="0.5px solid black" border-right="0.5px solid black">
									<fo:block text-align="right">
										<fo:table
											inline-progression-dimension="6.27cm" table-layout="fixed"
											space-after="6pt">
											<fo:table-column column-number="1" column-width="proportional-column-width(100)" />
											<fo:table-body>
												<fo:table-row>
													<fo:table-cell
														font-size="10pt">
														<fo:block text-align="left">
															<fo:inline>DATE: </fo:inline><xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/ReportDailyDGR/ReportDaily/reportDatetime/@epochMS,'dd MMM yyyy')"/>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell
														font-size="10pt">
														<fo:block text-align="left">
															<fo:inline>REPORT NO.: </fo:inline><xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportNumber" />
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
																							
												<fo:table-row>
													<fo:table-cell font-size="10pt">
														<fo:block text-align="left">
															<fo:inline>( associated DDR # </fo:inline>
															<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportNumber" />
															<fo:inline> )</fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
																
						</fo:table-body>
					</fo:table>
							
					</fo:block>
					
				</fo:static-content>
				
				<fo:static-content flow-name="xsl-region-after">
					<fo:block>
					<fo:table inline-progression-dimension="19cm"
						table-layout="fixed" space-after="6pt">
						<fo:table-column column-number="1"
							column-width="proportional-column-width(50)" />
						<fo:table-column column-number="2"
							column-width="proportional-column-width(50)" />
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS, 20080611,
										D2_IDS DEMO_GEOLOGY_TYPE1
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="8pt">
									<fo:block text-align="right">
										Page
										<fo:page-number />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell font-size="6pt" number-columns-spanned="2">
									<fo:block text-align="left">
										Printed on
										<xsl:variable name="printdate"
											select="/root/modules/Daily/Daily/dayDate" />
										<xsl:value-of
											select="d2Utils:getSystemDate('dd MMM yyyy')" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					</fo:block>
				</fo:static-content>
				
				<fo:flow flow-name="xsl-region-body" font-size="8pt">
					<fo:block>
						<fo:table inline-progression-dimension="18cm" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(100)" />
							<fo:table-body>
								
								<fo:table-row keep-together="always" padding="2px">
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:table inline-progression-dimension="19cm" table-layout="fixed" border="0.5px solid black">
											<fo:table-column column-number="1" column-width="proportional-column-width(100)" />
											<fo:table-body>
												<fo:table-row>
													<fo:table-cell text-align="center" font-size="12pt" background-color="rgb(223,223,223)">
														<fo:block font-weight="bold">
															<fo:inline text-decoration="underline">WELL: <xsl:value-of select="/root/modules/Well/Well/wellName" /></fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table>
									</fo:table-cell>
								</fo:table-row>
															
								<fo:table-row keep-together="always">
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:block>
											<xsl:apply-templates select="/root/modules/Well/Well" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-together="always">
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:block>
											<xsl:apply-templates select="/root/modules/CasingSection" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-together="always">
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:block>
											<xsl:apply-templates select="/root/modules/LithologyShows" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-together="always">
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:block>
											<xsl:apply-templates select="/root/modules/GasReadings" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-together="always">
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:block>
											<xsl:apply-templates select="/root/modules/Formation" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<fo:table-row keep-together="always" padding="2px">
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:table inline-progression-dimension="19cm" table-layout="fixed" border="0.5px solid black">
											<fo:table-column column-number="1" column-width="proportional-column-width(100)" />
											<fo:table-body>
												<fo:table-row>
													<fo:table-cell text-align="center" font-size="10pt" background-color="rgb(223,223,223)" padding="2px"
														 border-top="0.25px solid black" border-bottom="0.25px solid black">
														<fo:block font-weight="bold">
															Well Geologist
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell text-align="center" font-size="8pt" padding="2px"> 
														<fo:block>
															<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geologist"/>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table>
									</fo:table-cell>
								</fo:table-row>
																												
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

</xsl:stylesheet>
