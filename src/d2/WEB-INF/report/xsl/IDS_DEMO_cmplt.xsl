<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">

	<xsl:import
		href="./ReportTemplate/activity_record/ddr_activity_record_type1.xsl" />
	<xsl:import
		href="./ReportTemplate/activity_record/ddr_next_day_activity_record_type1.xsl" />
	<xsl:import
		href="./ReportTemplate/well_record/ddr_wells_record_type2.xsl" />
	
	<xsl:output method="xml" />
	<xsl:preserve-space elements="*" />
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
					<fo:region-before extent="3cm"/>
					<fo:region-after extent="1.5cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple"
				initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table inline-progression-dimension="19cm"
						table-layout="fixed" space-after="12pt" padding="10px">
						<fo:table-column column-number="1"
							column-width="proportional-column-width(50)" />
						<fo:table-column column-number="2"
							column-width="proportional-column-width(50)" />
						<fo:table-body space-before="10pt"
							space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="12pt">
									<fo:block>
										<fo:external-graphic
											content-width="scale-to-fit" width="150pt"
											content-height="80%" scaling="uniform"
											src="url(d2://images/company_logo.jpg)" />
										
									</fo:block>
								</fo:table-cell>
								<fo:table-cell
									number-columns-spanned="2" font-size="6pt"
									display-align="center">
									<fo:block text-align="right"
										font-weight="bold">
										<fo:table
											inline-progression-dimension="19cm" table-layout="fixed"
											space-after="12pt">
											<fo:table-column
												column-number="1"
												column-width="proportional-column-width(33)" />
											<fo:table-column
												column-number="2"
												column-width="proportional-column-width(33)" />
											<fo:table-body
												space-before="6pt" space-after="6pt">
												<fo:table-row>
													<fo:table-cell
														font-size="12pt">
														<fo:block>
															<fo:inline
																text-decoration="underline">
																COMPLETIONS MORNING	REPORT #
																<xsl:value-of
																	select="/root/modules/ReportDaily/ReportDaily/reportNumber" />
															</fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell
														text-align="right" font-size="12pt" font-weight="bold"
														display-align="before">
														<fo:block>
															<fo:inline
																text-decoration="underline">
																<xsl:value-of
																	select="/root/modules/Operation/Operation/operationName" />
															</fo:inline>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="19cm"
						table-layout="fixed" space-after="12pt">
						<fo:table-column column-number="1"
							column-width="proportional-column-width(50)" />
						<fo:table-column column-number="2"
							column-width="proportional-column-width(50)" />
						<fo:table-body space-before="6pt"
							space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS, 20080615, D2_IDE_DEMO_CMPLT
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="8pt">
									<fo:block text-align="right">
										Page
										<fo:page-number />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell font-size="6pt" number-columns-spanned="2">
									<fo:block text-align="left">
										Printed on
										<xsl:variable name="printdate"
											select="/root/modules/Daily/Daily/dayDate" />
										<xsl:value-of
											select="d2Utils:getSystemDate('dd MMM yyyy')" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" font-size="8pt">
					<fo:block>
						<fo:table inline-progression-dimension="19cm"
							table-layout="fixed">
							<fo:table-column column-number="1"
								column-width="proportional-column-width(100)" />
							<fo:table-body>
							
								<!-- WELL DATA -->
								<fo:table-row keep-together="always">
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:block>
											<xsl:apply-templates
												select="/root/modules/Well/Well" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
																
								<!-- TODAY ACTIVITY -->
								<fo:table-row>
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:block>
											<xsl:if
												test="count(/root/modules/Activity/Activity) &gt; 0">
												<xsl:apply-templates
													select="/root/modules/Activity" />
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
								<!-- NEXT DAY ACTIVITY -->
								<fo:table-row>
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:block>
											<xsl:if
												test="count(/root/modules/NextDayActivity/Activity) &gt; 0">
												<xsl:apply-templates
													select="/root/modules/NextDayActivity" />
											</xsl:if>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
																							
							</fo:table-body>
						</fo:table>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

</xsl:stylesheet>
