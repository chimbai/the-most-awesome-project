<?xml version="1.0" encoding="utf-8"?>
<!-- edited with XMLSPY v5 U (http://www.xmlspy.com) by Kenny (IDS) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	<xsl:output method="xml"/>
	<xsl:preserve-space elements="*"/>
	<xsl:template match="/">
		<fo:root font-family="Arial"> 
			<fo:layout-master-set>
				<fo:simple-page-master master-name="fwr" page-height="29cm" page-width="21cm" margin-left="1cm" margin-right="1cm" margin-top="0.5cm">
					<fo:region-body margin-top="2.5cm" margin-bottom="2cm"/>
					<fo:region-before extent="2.5cm"/>
					<fo:region-after extent="1.5cm"/>
				</fo:simple-page-master>
				<fo:simple-page-master master-name="front_page" page-height="29cm" page-width="21cm" margin-left="1cm" margin-right="1cm" margin-top="0.5cm">
					<fo:region-body margin-top="1cm" margin-bottom="1cm"/>
					<fo:region-before extent="1cm"/>
					<fo:region-after extent="1cm"/>
				</fo:simple-page-master>
				<fo:simple-page-master master-name="fwr_landscape" page-height="21cm" page-width="29cm" margin-left="1cm" margin-right="1cm" margin-top="0.5cm">
					<fo:region-body margin-top="2.5cm" margin-bottom="1cm"/>
					<fo:region-before extent="2.5cm"/>
					<fo:region-after extent="1cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<xsl:call-template name="front_page"/>
			<xsl:call-template name="dividers_part1"/>
			<xsl:apply-templates select="/root/modules/Well/Well"/>
			<xsl:call-template name="hse_section"/>
			<xsl:call-template name="dividers_part2"/>
			<xsl:call-template name="well_history"/>
			<xsl:call-template name="casing_section"/>
			<xsl:if test="/root/modules/Bharun/Bharun">
				<xsl:call-template name="bha_record"/>
			</xsl:if>
			<xsl:if test="/root/modules/MudProperties/MudProperties">
				<xsl:call-template name="mud_properties"/>
			</xsl:if>
			<xsl:if test="/root/modules/SurveyReference/SurveyReference">
				<xsl:call-template name="survey_section"/>
			</xsl:if>
			<!--<xsl:call-template name="formation_section"/>--> <!--geology formation-->
		</fo:root>
	</xsl:template>
	
	<xsl:template name="divider" >
	   <xsl:param name="node" />
	   <xsl:param name="label" />
	   <xsl:if test="$node">
	   <fo:table-row font-size="14pt">
			<fo:table-cell text-align="right" padding="0.1cm">
				<fo:block> - </fo:block>
			</fo:table-cell>
			<fo:table-cell text-align="left" padding="0.1cm">
				<fo:block>
					<xsl:value-of select="$label"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
	</xsl:template>
	
	<!-- Part 1 divider section BEGIN -->
	<xsl:template name="dividers_part1">
		<fo:page-sequence master-reference="front_page">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<!--<xsl:call-template name="header"/>-->
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="24pt">
				<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="18pt" wrap-option="wrap" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell height="26cm" width="19cm" border="1px double black" text-align="center">
									<fo:block padding-before="5cm">
										<fo:table inline-progression-dimension="19cm" table-layout="fixed" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
											<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
											<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
											<fo:table-header>
												<fo:table-row>
													<fo:table-cell number-columns-spanned="2" padding="1cm">
														<fo:block>Part 1 : Summary Data</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-header>
											<fo:table-body>											   
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/Well/Well" />
											      <xsl:with-param name="label" select="'General Well Details'" />
											   </xsl:call-template>
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="//dvd" />
											      <xsl:with-param name="label" select="'DVD'" />	
											   </xsl:call-template>
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/HseIncident/HseIncident" />
											      <xsl:with-param name="label" select="'HSE Performance'" />	
											   </xsl:call-template>
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	<!-- Part 1 divider section END -->
	
	<!-- Part 2 divider section BEGIN -->
	<xsl:template name="dividers_part2">
		<fo:page-sequence master-reference="front_page">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<!--<xsl:call-template name="header"/>-->
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="24pt">
				<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="18pt" wrap-option="wrap" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell height="26cm" width="19cm" border="1px double black" text-align="center">
									<fo:block padding-before="5cm">
										<fo:table inline-progression-dimension="19cm" table-layout="fixed" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
											<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
											<fo:table-column column-number="2" column-width="proportional-column-width(75)"/>
											<fo:table-header>
												<fo:table-row>
													<fo:table-cell number-columns-spanned="2" padding="1cm">
														<fo:block>Part 2 : Drilling Engineering</fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-header>
											<fo:table-body>											   
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/ReportDaily/ReportDaily" />
											      <xsl:with-param name="label" select="'Well History'" />
											   </xsl:call-template>
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/CasingSection/CasingSection" />
											      <xsl:with-param name="label" select="'Casing and Hole Summary'" />	
											   </xsl:call-template>
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/MudProperties/MudProperties" />
											      <xsl:with-param name="label" select="'Drilling Fluids Record'" />	
											   </xsl:call-template>
											   <xsl:call-template name="divider">	
											      <xsl:with-param name="node" select="/root/modules/SurveyReference/SurveyReference" />
											      <xsl:with-param name="label" select="'Survey Data'" />	
											   </xsl:call-template>
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	<!-- Part 2 divider section END -->
	
	<xsl:template name="front_page">
		<fo:page-sequence master-reference="front_page">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
			</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
			</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="8pt">
				<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell height="26cm" width="19cm" border="1px double black" text-align="center" display-align="center">
									<fo:block>
										<xsl:call-template name="front_detail"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	<!-- front_page section END -->
	<xsl:template name="front_detail">
		<xsl:variable name="logostatus" select="V_front.logostatus"/>
		<xsl:variable name="rigpicstatus" select="V_front.rigpicstatus"/>
		<xsl:variable name="logo" select="V_front.logo"/>
		<xsl:variable name="rigpic" select="V_front.rigpic"/>
		<xsl:variable name="authorstatus" select="V_front.authorstatus"/>
		<xsl:variable name="other_infostatus" select="V_front.other_infostatus"/>
		<xsl:variable name="fronttitle" select="'Drilling Data Appendix'"/>
		<fo:table inline-progression-dimension="19cm" color="rgb(75,75,75)" table-layout="fixed" font-size="18pt" font-weight="bold" wrap-option="wrap" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell text-align="center" display-align="center">
						<fo:block>
							<fo:external-graphic content-width="scale-to-fit"
								width="100pt" content-height="50%" scaling="uniform" src="url(d2://images/company_logo.jpg)"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell text-align="center" padding-before="1.5cm">
						<fo:block>
							<xsl:value-of select="/root/modules/Well/Well/wellName"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<!-- 
				<fo:table-row>
					<fo:table-cell text-align="center" padding-before="1cm">
						<fo:block>
							<xsl:choose>
								<xsl:when test="$authorstatus = 1">
									<xsl:value-of select="V_front.authors"/>
								</xsl:when>
								<xsl:otherwise>

								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				-->
				<fo:table-row>
					<fo:table-cell text-align="center" padding-before="1cm">
						<fo:block>
							Field - <xsl:value-of select="/root/modules/Well/Well/field"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell text-align="center" padding-before="1cm">
						<fo:block>
							<xsl:value-of select="$fronttitle"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<!-- 
				<fo:table-row>
					<fo:table-cell padding-before="0.5cm">
						<fo:block>
							<xsl:choose>
								<xsl:when test="$rigpicstatus = 1">
									<fo:external-graphic src="url({$rigpic})"/>
								</xsl:when>
								<xsl:otherwise>
									
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				-->
				<fo:table-row>
					<fo:table-cell text-align="center" padding-before="1cm" font-size="14pt">
						<fo:block>
							<xsl:value-of select="/root/modules/Well/Well/opCo"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<!--  
				<fo:table-row>
					<fo:table-cell text-align="center" padding-before="1cm" font-size="12pt">
						<fo:block>
							<xsl:choose>
								<xsl:when test="$other_infostatus = 1">
									Issued <xsl:value-of select="V_front.other_info"/>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				-->
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<!-- header_summary section BEGIN -->
	<xsl:template name="header_summary">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="15pt" font-weight="bold" wrap-option="wrap" space-before="18pt" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.2cm" border-bottom="0.25px solid black">
						<fo:block text-align="left">
							<xsl:value-of select="/root/modules/Well/Well/opCo"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.2cm" border-bottom="0.25px solid black">
						<fo:block text-align="right">
							<xsl:value-of select="/root/modules/Well/Well/wellName"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- header_summary section END -->
	<!-- footer section BEGIN -->
	<xsl:template name="footer">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="6pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(25)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(50)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.1cm">
						<fo:block text-align="left">Copyright IDS, 20080223, D2_IDE_DEMO_fwr</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<!-- footer section END -->
	
	<!-- header section BEGIN -->
	<xsl:template name="header">
		<fo:block color="rgb(75,75,75)">
		<xsl:variable name="rigId" select="/root/modules/Well/Well/rigid"/>
			<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border-bottom="0.5px solid black" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
				<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(34)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block text-align="left">Wellname : <xsl:value-of select="/root/modules/Well/Well/wellName"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block text-align="center">Drilling Co. : <xsl:value-of select="/root/modules/RigInformation/RigInformation[rigInformationUid = $rigId]/rigManager/@lookupLabel"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block text-align="right">Rig : <xsl:value-of select="$rigId"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block/>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
		<fo:block color="rgb(75,75,75)">
			<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
				<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(34)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>				
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell padding="0.1cm" >
							<fo:block>
							   <xsl:choose>
								   <xsl:when test="/root/modules/Well/Well/onOffShore = 'ON'">
		 							   RT above GL : 
		 							   <xsl:value-of select="/root/modules/Well/Well/z_airgap"/>
		 							   <xsl:value-of select="/root/modules/Well/Well/z_airgap/@uomSymbol"/>
	 							   </xsl:when>
								   <xsl:otherwise>
		 							   DFE above MSL : 
		 							   <xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl"/>
		 							   <xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl/@uomSymbol"/>
	 							   </xsl:otherwise>
 							   </xsl:choose>
 							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" >
							<fo:block>Lat : <xsl:value-of select="/root/modules/Well/Well/latDeg"/> Deg <xsl:value-of select="/root/modules/Well/Well/latMinute"/> Min <xsl:value-of select="/root/modules/Well/Well/latSecond"/> Sec</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" >
							<fo:block>Spud Date / Time : <xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/spudDate,'dd MMM yyyy HH:mm')"/></fo:block>
						</fo:table-cell>						
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block>
							   <xsl:choose>
								   <xsl:when test="/root/modules/Well/Well/onOffShore = 'ON'">
		 							   G.L. Elevation :
		 							   <xsl:value-of select="/root/modules/Well/Well/waterDepthMsl"/>
		 							   <xsl:value-of select="/root/modules/Well/Well/waterDepthMsl/@uomSymbol"/>
	 							   </xsl:when>
	 							   <xsl:otherwise>
		 							   Water Depth :
		 							   <xsl:value-of select="/root/modules/Well/Well/waterDepthMsl"/>
		 							   <xsl:value-of select="/root/modules/Well/Well/waterDepthMsl/@uomSymbol"/>
	 							   </xsl:otherwise>
 							   </xsl:choose>
 							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block>Long : <xsl:value-of select="/root/modules/Well/Well/longDeg"/> Deg <xsl:value-of select="/root/modules/Well/Well/longMinute"/> Min <xsl:value-of select="/root/modules/Well/Well/longSecond"/> Sec</fo:block>
						</fo:table-cell>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block>Release Time : <xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/rigOffHireDate,'dd MMM yyyy HH:mm')"/></fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>
	<!-- header section END -->
	
	<!-- header_landscape section BEGIN -->
	<xsl:template name="header_landscape">
	<xsl:variable name="rigId" select="/root/modules/Well/Well/rigid"/>
		<fo:block color="rgb(75,75,75)">
			<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" border-bottom="0.5px solid black" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
				<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(34)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block text-align="left">Wellname : <xsl:value-of select="/root/modules/Well/Well/wellName"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block text-align="center">Drilling Co. : <xsl:value-of select="/root/modules/RigInformation/RigInformation[rigInformationUid = $rigId]/rigManager/@lookupLabel"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block text-align="right">Rig : <xsl:value-of select="$rigId"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block/>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
		<fo:block color="rgb(75,75,75)">
			<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
				<fo:table-column column-number="1" column-width="proportional-column-width(33)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(34)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(33)"/>				
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell padding="0.1cm" >
							<fo:block>
							   <xsl:choose>
								   <xsl:when test="/root/modules/Well/Well/onOffShore = 'ON'">
		 							   RT above GL : 
		 							   <xsl:value-of select="/root/modules/Well/Well/z_airgap"/>
		 							   <xsl:value-of select="/root/modules/Well/Well/z_airgap/@uomSymbol"/>
	 							   </xsl:when>
								   <xsl:otherwise>
		 							   DFE above MSL : 
		 							   <xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl"/>
		 							   <xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl/@uomSymbol"/>
	 							   </xsl:otherwise>
 							   </xsl:choose>
 							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" >
							<fo:block>Lat : <xsl:value-of select="/root/modules/Well/Well/latDeg"/> Deg <xsl:value-of select="/root/modules/Well/Well/latMinute"/> Min <xsl:value-of select="/root/modules/Well/Well/latSecond"/> Sec</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" >
							<fo:block>Spud Date / Time : <xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/spudDate,'dd MMM yyyy HH:mm')"/></fo:block>
						</fo:table-cell>						
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block>
							   <xsl:choose>
								   <xsl:when test="/root/modules/Well/Well/onOffShore = 'ON'">
		 							   G.L. Elevation :
		 							   <xsl:value-of select="/root/modules/Well/Well/waterDepthMsl"/>
		 							   <xsl:value-of select="/root/modules/Well/Well/waterDepthMsl/@uomSymbol"/>
	 							   </xsl:when>
	 							   <xsl:otherwise>
		 							   Water Depth :
		 							   <xsl:value-of select="/root/modules/Well/Well/waterDepthMsl"/>
		 							   <xsl:value-of select="/root/modules/Well/Well/waterDepthMsl/@uomSymbol"/>
	 							   </xsl:otherwise>
 							   </xsl:choose>
 							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block>Long : <xsl:value-of select="/root/modules/Well/Well/longDeg"/> Deg <xsl:value-of select="/root/modules/Well/Well/longMinute"/> Min <xsl:value-of select="/root/modules/Well/Well/longSecond"/> Sec</fo:block>
						</fo:table-cell>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block>Release Time : <xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/rigOffHireDate,'dd MMM yyyy HH:mm')"/></fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>
	<!-- header_landscape section END -->
	
	<!-- well summary section BEGIN -->
	<xsl:template match="modules/Well/Well">
		<fo:page-sequence master-reference="fwr">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<xsl:call-template name="header_summary"/>
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="8pt">
				<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" text-align="center">
									<fo:block font-size="15pt" font-weight="bold">General Well Details</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
				<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="10pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(15)"/>
						<fo:table-column column-number="2" column-width="proportional-column-width(35)"/>
						<fo:table-column column-number="3" column-width="proportional-column-width(35)"/>
						<fo:table-column column-number="4" column-width="proportional-column-width(15)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell number-columns-spanned="2" padding="0.2cm" border-left="0.25px solid grey" border-top="0.25px solid grey" border-right="0.25px solid grey">
									<fo:block text-align="left" font-weight="bold">Well Objective : </fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell number-columns-spanned="2" padding="0.3cm" border-right="0.25px solid grey" border-left="0.25px solid grey" border-bottom="0.25px solid grey">
									<fo:block text-align="left">
										<xsl:value-of select="well_objective"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Country : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="country/@lookupLabel"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">License : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="licenseNumber"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Field : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="field"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Well : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="wellName"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Well Type : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
								      	<xsl:choose>
								      		<xsl:when test="type_intent = 'EXP'">
								      			Exploration
								      		</xsl:when>
								      		<xsl:otherwise>
								      			<xsl:value-of select="type_intent" />
								      		</xsl:otherwise>
								      	</xsl:choose>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<!--  
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Status : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="wells.status"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							-->
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Operating Company : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="opCo"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Rig : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="rigid"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm" number-columns-spanned="2">
									<fo:block text-align-last="justify">
										<fo:leader leader-pattern="rule"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Latitude : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="latDeg"/> Deg <xsl:value-of select="latMinute"/> Min <xsl:value-of select="latSecond"/> Sec</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Longitude : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="longDeg"/> Deg <xsl:value-of select="longMinute"/> Min <xsl:value-of select="longSecond"/> Sec</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Spheroid : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="spheroidType"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Datum : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="gridDatum"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Seismic Line : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="seismicLine"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Permit : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="govPermitNumber"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Projection : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="loc_utm_zone"/> Zone
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">UTM North : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="gridNs"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">UTM East : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="gridEw"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:choose>
								<xsl:when test="onOffShore = 'ON'" >
									<fo:table-row>
										<fo:table-cell>
									    </fo:table-cell>
										<fo:table-cell padding="0.075cm">
											<fo:block font-weight="bold">RT - GL : </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.075cm">
											<fo:block>
												<xsl:value-of select="z_airgap"/>
												<xsl:value-of select="z_airgap/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell>
									    </fo:table-cell>
										<fo:table-cell padding="0.075cm">
											<fo:block font-weight="bold">GL - AMSL : </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.075cm">
											<fo:block>
												<xsl:value-of select="waterDepthMsl"/>
												<xsl:value-of select="waterDepthMsl/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:when>
								<xsl:otherwise>
									<fo:table-row>
										<fo:table-cell>
									    </fo:table-cell>
										<fo:table-cell padding="0.075cm">
											<fo:block font-weight="bold">RT - MSL : </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.075cm">
											<fo:block>
												<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl"/>
												<xsl:value-of select="/root/modules/Operation/Operation/dynaAttr/rtmsl/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell>
									    </fo:table-cell>
										<fo:table-cell padding="0.075cm">
											<fo:block font-weight="bold"> Water Depth : </fo:block>
										</fo:table-cell>
										<fo:table-cell padding="0.075cm">
											<fo:block>
												<xsl:value-of select="waterDepthMsl"/>
												<xsl:value-of select="waterDepthMsl/@uomSymbol"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</xsl:otherwise>
							</xsl:choose>
							<!--<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Plug Back Depth : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block><xsl:value-of select="wells.z_pbdepth"/></fo:block>
								</fo:table-cell>
							</fo:table-row>-->
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Planned TD : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="z_targetdepth"/> MDBRT
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Driller's TD : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="z_tdmd"/> MDBRT
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm" number-columns-spanned="2">
									<fo:block text-align-last="justify">
										<fo:leader leader-pattern="rule"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>							
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Rig Move In Date / Time : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/onloc_date_onloc_time,'dd MMM yyyy HH:mm')"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Spud Date / Time : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/spudDate,'dd MMM yyyy HH:mm')"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">TD Reached Date / Time : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/td_date,'dd MMM yyyy HH:mm')"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Rig Released Date / Time : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/rigOffHireDate,'dd MMM yyyy HH:mm')"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Total Days Spud : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="wells.dayssincespud"/><!-- Not sure mapping -->
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Total Days on Operations : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="wells.daysonops"/><!-- Not sure mapping -->
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Total Days Budgeted : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="wells.planneddays"/><!-- Not sure mapping -->
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm" number-columns-spanned="2">
									<fo:block text-align-last="justify">
										<fo:leader leader-pattern="rule"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">AFE Well Cost : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="/root/modules/Operation/Operation/afe"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block font-weight="bold">Actual Well Cost : </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="0.075cm">
									<fo:block>
										<xsl:value-of select="//wells/wells.totalwellcost"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell>
							    </fo:table-cell>
								<fo:table-cell padding="0.075cm" number-columns-spanned="2">
									<fo:block text-align-last="justify">
										<fo:leader leader-pattern="rule"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>                
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	<!-- well sumamry section END -->
	
	<!-- history section BEGIN -->
	<xsl:template name="well_history">
		<fo:page-sequence master-reference="fwr">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<xsl:call-template name="header"/>
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="10pt">
				<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
									<fo:block text-align="left" font-size="12pt">Well History</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<xsl:apply-templates select="/root/modules/Well/Well" mode="well_history"/>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	
	<xsl:template match="modules/Well/Well" mode="well_history">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="left" font-size="12pt">Well: <xsl:value-of select="wellName"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<xsl:apply-templates select="/root/modules/Well/Well" mode="well_history_header"/>
	</xsl:template>
	
	<xsl:template match="modules/Well/Well" mode="well_history_header">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(12)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(73)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell padding="0.1cm" text-align="center" border-bottom="0.25px solid black">
						<fo:block>#</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" text-align="center" border-bottom="0.25px solid black">
						<fo:block>Date</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" text-align="center" border-bottom="0.25px solid black">
						<fo:block>Depth</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" text-align="center" border-bottom="0.25px solid black">
						<fo:block>24 Hour Summary</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="/root/modules/ReportDaily/ReportDaily" mode="hrsummary">
					<xsl:sort select="reportNumber" data-type="number"/>
				</xsl:apply-templates>				
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template match="modules/ReportDaily/ReportDaily" mode="hrsummary">
		<fo:table-row>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="reportNumber"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportDatetime/@epochMS,'dd MMM yyyy')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="depthMdMsl"/>
					<xsl:value-of select="depthMdMsl/@uomSymbol"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm">
				<fo:block>
					<xsl:value-of select="reportPeriodSummary"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- history section END -->
	
	<!-- casing section BEGIN -->
	<xsl:template name="casing_section">
		<fo:page-sequence master-reference="fwr">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<xsl:call-template name="header"/>
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="8pt">
				<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
									<fo:block text-align="left" font-size="12pt">Casing and Hole Summary</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<xsl:apply-templates select="/root/modules/Well/Well" mode="casing_section"/>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	
	<xsl:template match="modules/Well/Well" mode="casing_section">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="left" font-size="12pt">Well: <xsl:value-of select="wellName"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<xsl:apply-templates select="/root/modules/CasingSection/CasingSection" mode="casing_section_detail">
			<xsl:sort select="shoeTopMdMsl" data-type="number"/>
		</xsl:apply-templates>
	</xsl:template>
	
	<xsl:template match="modules/CasingSection/CasingSection" mode="casing_section_detail">
	<xsl:variable name="selected_casingsectionuid" select="casingSectionUid"/>
	<xsl:variable name="select_dailyid" select="dailyid"/>
    <fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="3pt" table-omit-header-at-break="true">
      <fo:table-column column-number="1" column-width="proportional-column-width(1)" />
      <fo:table-body>
      	<fo:table-row>
      		<fo:table-cell>
      			<fo:block>
				    <fo:table inline-progression-dimension="19cm" table-layout="fixed" padding="0.1cm" font-size="6pt" wrap-option="wrap" table-omit-header-at-break="true" keep-together="always">
				      <fo:table-column column-number="1" column-width="proportional-column-width(1)" />
				      <fo:table-column column-number="2" column-width="proportional-column-width(1)" />
				      <fo:table-column column-number="3" column-width="proportional-column-width(1)" />
				      <fo:table-body>
					      <fo:table-row>
					      	<fo:table-cell border="0.25px solid black" padding="0.1cm">
					      		<fo:block font-weight="bold" font-size="7pt">
					      			Run Date: (#<xsl:value-of select="//root/modules/Daily/Daily[dailyUid=$select_dailyid]/dayNumber"/>)
					      			<fo:inline color="white">!</fo:inline>
					      			<xsl:value-of select="d2Utils:formatDateFromEpochMS(installStartDate/@epochMS,'dd MMM yyyy')"/>
					      		</fo:block>
					      	</fo:table-cell>
					      	<fo:table-cell border="0.25px solid black" padding="0.1cm" number-columns-spanned="2">
					      		<fo:block>
					      			Comments: <fo:inline color="white">!</fo:inline><xsl:value-of select="casingSummary"/>					      			
					      		</fo:block>
					      	</fo:table-cell>
					      </fo:table-row>
				      	<fo:table-row>
				      		<fo:table-cell>
				      			<fo:block> <!--column 1 -->
								    <fo:table width="100%" table-layout="fixed" border-top="0.25px solid black" border-bottom="0.25px solid black" border-left="0.25px solid black" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
								      <fo:table-column column-number="1" column-width="proportional-column-width(1)" />
								      <fo:table-body>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="22mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Size OD
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="casingOd/@lookupLabel"/>&#160;						                    					
					                    					</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Weight (Wet)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="weight_wet"/>&#160;
																<xsl:value-of select="weight_wet/@uomSymbol"/>						                    					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Weight (Dry)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="weight_dry"/>&#160;
																<xsl:value-of select="weight_dry/@uomSymbol"/>						                    					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Weight (Set)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="weight_landing"/>&#160;
																<xsl:value-of select="weight_landing/@uomSymbol"/>						                    					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Grade
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="grade"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Collar Type
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="collar_type"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Thread Type
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="thread_type"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Stage Tool Type
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="stage_tool_type"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Stage Tool
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="stage_tool_installed/@lookupLabel"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>								      	
								      </fo:table-body>
								    </fo:table>
				      			</fo:block>
				      		</fo:table-cell>
				      		<fo:table-cell>
				      			<fo:block> <!--column 2 -->
								    <fo:table width="100%" table-layout="fixed" border-top="0.25px solid black" border-bottom="0.25px solid black" wrap-option="wrap" space-after="3pt" table-omit-header-at-break="true">
								      <fo:table-column column-number="1" column-width="proportional-column-width(1)" />
								      <fo:table-body>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Liner
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="liner_installed/@lookupLabel"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="22mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Total # Joints Run
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="totaljoints_inhole/@rawNumber"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Total Length Casing Run
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="totallength"/>&#160;
						                    					<xsl:value-of select="totallength/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>								      	
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Float Collar Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="float_collar_depth_md"/>&#160;
						                    					<xsl:value-of select="float_collar_depth_md/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Shoe Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="shoeTopMdMsl"/>&#160;
						                    					<xsl:value-of select="shoeTopMdMsl/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Shoe Depth (TVD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="shoeTopTvdMsl"/>&#160;
					                        					<xsl:value-of select="shoeTopTvdMsl/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="32mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Casing Landed Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="casing_landed_depth_md"/>&#160;
						                    					<xsl:value-of select="casing_landed_depth_md/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="32mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Stage Tool Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="stage_tool_depth_md"/>&#160;
																<xsl:value-of select="stage_tool_depth_md/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="32mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Liner Top Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="4mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="liner_top_depth_md"/>&#160;
							                   					<xsl:value-of select="liner_top_depth_md/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>								      	
								      </fo:table-body>
								    </fo:table>
				      			</fo:block>
				      		</fo:table-cell>
				      		<fo:table-cell>
				      			<fo:block> <!--column 3 -->
								    <fo:table width="100%" table-layout="fixed" border-top="0.25px solid black" border-bottom="0.25px solid black" wrap-option="wrap" space-after="3pt" table-omit-header-at-break="true">
								      <fo:table-column column-number="1" column-width="proportional-column-width(1)" />
								      <fo:table-body>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="22mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Hole Size
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="openHoleId"/>&#160;
						                    					<xsl:value-of select="openHoleId/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="22mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Hole Depth (MD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="openHoleDepthMdMsl"/>&#160;
						                    					<xsl:value-of select="openHoleDepthMdMsl/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="22mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Hole Depth (TVD)
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="openHoleDepthTvdMsl"/>&#160;
						                    					<xsl:value-of select="openHoleDepthTvdMsl/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Gauge Hole Volume
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="hole_volume"/>&#160;
						                    					<xsl:value-of select="hole_volume/@uomSymbol"/>
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>								      	
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="35mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Start Time/Date
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="d2Utils:formatDateFromEpochMS(installStartDate/@epochMS,'dd MMM yyyy HH:mm')"/>&#160;					                          					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Stop Time/Date
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="d2Utils:formatDateFromEpochMS(installCompleteDate/@epochMS,'dd MMM yyyy HH:mm')"/>&#160;					                          					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Total Time
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="total_hours"/>&#160;					                          					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Downtime
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="downtime_hours"/>&#160;					                          					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>
								      	<fo:table-row>
								      		<fo:table-cell border-right="0.25px solid black" padding="0.1cm">
												<fo:list-block provisional-distance-between-starts="30mm">
													<fo:list-item>
														<fo:list-item-label end-indent="label-end()">
															<fo:block>
																Downtime Reason
															</fo:block>
														</fo:list-item-label>
														<fo:list-item-body start-indent="body-start()" end-indent="6mm">
															<fo:block text-align="right">
																<fo:inline color="white">!</fo:inline><xsl:value-of select="downtime_descr"/>&#160;					                          					
															</fo:block>
														</fo:list-item-body>
													</fo:list-item>
												</fo:list-block>
								      		</fo:table-cell>
								      	</fo:table-row>								      	
								      </fo:table-body>
								    </fo:table>
				      			</fo:block>
				      		</fo:table-cell>
				      	</fo:table-row>
				      </fo:table-body>
				    </fo:table>
      			</fo:block>
      		</fo:table-cell>
      	</fo:table-row>
      	<fo:table-row>
        	<fo:table-cell>
	        	<xsl:choose>
		        	<xsl:when test="count(CasingTally[casingSectionUid=$selected_casingsectionuid]) &gt; 0">
		        		<fo:block>
						      <fo:table width="100%" table-layout="fixed" border="0.5px solid black" font-size="6pt">
						         <fo:table-column column-width="proportional-column-width(0.5)" />
						         <fo:table-column column-width="proportional-column-width(0.5)" />
						         <fo:table-column column-width="proportional-column-width(0.5)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1.5)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(1)" />
						         <fo:table-column column-width="proportional-column-width(2)" />
						         <fo:table-header>
						           <fo:table-row text-align="center">
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> Seq	</fo:block>
						             </fo:table-cell>
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> Unit	</fo:block>
						             </fo:table-cell>
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> Jts </fo:block>
						             </fo:table-cell>
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> OD(in) </fo:block>
						             </fo:table-cell>
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> ID(in)	</fo:block>
						             </fo:table-cell>
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> Wt </fo:block>
						             </fo:table-cell>
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> Grade </fo:block>
						             </fo:table-cell>
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> Thread </fo:block>
						             </fo:table-cell>
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> Type	</fo:block>
						             </fo:table-cell>
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> Section MD </fo:block>
						             </fo:table-cell>
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> Section TVD </fo:block>
						             </fo:table-cell>
						             <fo:table-cell padding="0.05cm" border-bottom="0.25px solid black">
						             		<fo:block> Remarks </fo:block>
						             </fo:table-cell>
						           </fo:table-row>
						         </fo:table-header>
						         <fo:table-body>
									 <xsl:apply-templates select="CasingTally[casingSectionUid=$selected_casingsectionuid]"/>
								 </fo:table-body>								
							 </fo:table>
		        		</fo:block>
		        	</xsl:when>
		        	<xsl:otherwise>
		        		<fo:block/>
		        	</xsl:otherwise>
	        	</xsl:choose>	    
        	</fo:table-cell>
   			</fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>  
  <!-- casing and hole summary end -->
  <!-- casing detail begin -->
	<xsl:template match="CasingTally">
		<fo:table-row>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="runOrder"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="uom_label"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="numJoints"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="casingOd"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="casingId"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="weight"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="grade"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="csg_thread"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="type"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="sectionmd"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black" text-align="right">
				<fo:block>
					<xsl:value-of select="sectiontvd"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.05cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="normalize-space(csg_comment)"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>  		
	<!-- casing section END -->
	
	<!-- BHA section BEGIN -->
	<xsl:template name="bha_record">
		<fo:page-sequence master-reference="fwr_landscape">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<xsl:call-template name="header_landscape"/>
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="8pt">
				<fo:block>
					<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
									<fo:block text-align="left" font-size="12pt">BHA Record</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<xsl:apply-templates select="/root/modules/Well/Well" mode="bha_record"/>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	
	<xsl:template match="modules/Well/Well" mode="bha_record">		
		<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="left" font-size="12pt">Well: <xsl:value-of select="wellName"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
						
		<xsl:apply-templates select="/root/modules/Bharun" mode="bharun_record_header"/>
		
	</xsl:template>
	
	<xsl:template match="modules/Bharun" mode="bharun_record_header">
		<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(3)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(12)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="8" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="10" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="11" column-width="proportional-column-width(6)"/>
			<fo:table-column column-number="12" column-width="proportional-column-width(31)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
						<fo:block text-align="center">#</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Date-in</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Length</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Weight (Wet)</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Weight Blw/Jar (Wet)</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">String Weight</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Pick-Up Weight</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Slack-Off Weight</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Torque Max</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Torque on Bottom</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Torque off Bottom</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="center">Description</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="Bharun" mode="bharun_record_detail">
					<xsl:sort select="bhaRunNumber" data-type="number"/>
		        </xsl:apply-templates>
			</fo:table-body>
		</fo:table>
		<!-- <xsl:apply-templates select="/root/modules/Bharun/Bharun" mode="bharun_record"/> --> <!-- still working on it -->
	</xsl:template>
	
	<xsl:template match="Bharun" mode="bharun_record_detail">
		<fo:table-row>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="bhaRunNumber"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block>
					<xsl:variable name="datein" select="datein"/>					
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Daily/Daily[dailyUid=$datein]/dayDate/@epochMS,'dd MMM yyyy')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block>
					<xsl:value-of select="bhaTotalLength"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block>
					<xsl:value-of select="weightBhaTotalWet"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block>
					<xsl:value-of select="weightbelowjarwet"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block>
					<xsl:value-of select="BharunDailySummary/weightString"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block>
					<xsl:value-of select="BharunDailySummary/weightPickup"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block>
					<xsl:value-of select="BharunDailySummary/weightSlackOff"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block>
					<xsl:value-of select="BharunDailySummary/torqueMax"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block>
					<xsl:value-of select="BharunDailySummary/torque_on_bottom_avg"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block>
					<xsl:value-of select="BharunDailySummary/torque_off_bottom_avg"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
				<fo:block>
					<xsl:value-of select="BharunDailySummary/summary"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- BHA section END -->
	
	<!-- Mud section BEGIN -->
	<xsl:template name="mud_properties">
		<fo:page-sequence master-reference="fwr_landscape">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<xsl:call-template name="header_landscape"/>
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="8pt">
				<fo:block>
					<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
									<fo:block text-align="left" font-size="12pt">Drilling Fluids Records</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<xsl:apply-templates select="/root/modules/Well/Well" mode="oil"/>
					<xsl:apply-templates select="/root/modules/Well/Well" mode="h2o"/>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	
	<xsl:template match="modules/Well/Well" mode="oil">
		 
		<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="left" font-size="12pt">OBM
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<xsl:variable name="dailyid" select="//root/modules/Daily/Daily/dailyUid"/>
		<xsl:if test="/root/modules/MudProperties/MudProperties[mudType='oil']">
			<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="6pt" border="0.5px solid black" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
				<fo:table-column column-number="1" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(7)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="5" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="9" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="10" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="11" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="12" column-width="proportional-column-width(2)"/>
				<fo:table-column column-number="13" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="14" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="15" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="16" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="17" column-width="proportional-column-width(4)"/>
				<fo:table-column column-number="18" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="19" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="20" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="21" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="22" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="23" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="24" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="25" column-width="proportional-column-width(5)"/>												
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							R# <xsl:value-of select="//muddaily/L_muddaily.daynum"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Date - Time <xsl:value-of select="//muddaily/L_muddaily.checkdt"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Type <xsl:value-of select="//muddaily/L_muddaily.mud_type_descr"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Depth <xsl:value-of select="//muddaily/L_muddaily.depth"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Oil Type	 <xsl:value-of select="//muddaily/L_muddaily.oiltype"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							TMP <xsl:value-of select="//muddaily/L_muddaily.temperature"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							MW <xsl:value-of select="//muddaily/L_muddaily.mwsurface"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							VIS <xsl:value-of select="//muddaily/L_muddaily.funnelvis"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							PV <xsl:value-of select="//muddaily/L_muddaily.pv"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							YP <xsl:value-of select="//muddaily/L_muddaily.yp"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black" number-columns-spanned="2">
							<fo:block text-align="center">
							Gel10s <xsl:value-of select="//muddaily/L_muddaily.gel10s"/> / 
							</fo:block>
							<fo:block text-align="center">
							10m <xsl:value-of select="//muddaily/L_muddaily.gel10m"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							F.L. API <xsl:value-of select="//muddaily/L_muddaily.apifl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Cake <xsl:value-of select="//muddaily/L_muddaily.hthpcake"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Sol	<xsl:value-of select="//muddaily/L_muddaily.solids"/>
							</fo:block>
						</fo:table-cell>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							%LGS <xsl:value-of select="//muddaily/L_muddaily.lgs"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							SND <xsl:value-of select="//muddaily/L_muddaily.sand"/>
							</fo:block>
						</fo:table-cell>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Oil on <xsl:value-of select="//muddaily/L_muddaily.oil"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							O/W Ratio <xsl:value-of select="//muddaily/L_muddaily.owratio"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Mp <xsl:value-of select="//muddaily/L_muddaily.mp"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Cl <xsl:value-of select="//muddaily/L_muddaily.cl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							h2o Sal <xsl:value-of select="//muddaily/L_muddaily.h2osalinity"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Excs Lime <xsl:value-of select="//muddaily/L_muddaily.excesslime"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Elec Stab <xsl:value-of select="//muddaily/L_muddaily.electricstab"/>
							</fo:block>
						</fo:table-cell>					
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block text-align="center">
							Daily Cost <xsl:value-of select="//muddaily/L_muddaily.cost"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
					<xsl:apply-templates select="/root/modules/MudProperties/MudProperties[dailyUid = $dailyid and mudType='oil']" mode="obm"/>
				</fo:table-body>
			</fo:table>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="modules/MudProperties/MudProperties" mode="obm">	
		<fo:table-row>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="reportNumber"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:variable name="dailyid" select="dailyUid"/>
					<xsl:value-of select="/root/modules/Daily/Daily[dailyUid=$dailyid]/dayDate"/> - 
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudMedium"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="depthMdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="oiltype"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudTestTemperature"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudWeight"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudFv"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudPv"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudYp"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black" number-columns-spanned="2">
				<fo:block>
					<xsl:value-of select="mudGel10s"/> / <xsl:value-of select="mudGel10m"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudApiFl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="hthpCake"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudDissolvedSolids"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="lgs"/>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="sand"/>					
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudOil"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="owratio"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mp"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudChlorideIon"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="h2osalinity"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudExcessLime"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="electricstab"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="0.1cm">
				<fo:block>
					<xsl:value-of select="mudCost"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	
	<!-- H2O -->	
	<xsl:template match="modules/Well/Well" mode="h2o">
		 
		<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="left" font-size="12pt">WBM
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		
		<xsl:variable name="dailyid" select="//root/modules/Daily/Daily/dailyUid"/>
		<xsl:if test="/root/modules/MudProperties/MudProperties[mudType='h2o']">
			<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="6pt" border="0.5px solid black" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
				<fo:table-column column-number="1" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="2" column-width="proportional-column-width(8)"/>
				<fo:table-column column-number="3" column-width="proportional-column-width(7)"/>
				<fo:table-column column-number="4" column-width="proportional-column-width(4)"/>				
				<fo:table-column column-number="5" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="6" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="7" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="8" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="9" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="10" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="11" column-width="proportional-column-width(2)"/>
				<fo:table-column column-number="12" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="13" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="14" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="15" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="16" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="17" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="18" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="19" column-width="proportional-column-width(5)"/>
				<fo:table-column column-number="20" column-width="proportional-column-width(3)"/>
				<fo:table-column column-number="21" column-width="proportional-column-width(10)"/>																
				<fo:table-header>
					<fo:table-row>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							R# <xsl:value-of select="//muddaily/L_muddaily.daynum"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Date - Time <xsl:value-of select="//muddaily/L_muddaily.checkdt"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Type <xsl:value-of select="//muddaily/L_muddaily.mud_type_descr"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Depth <xsl:value-of select="//muddaily/L_muddaily.depth"/>
							</fo:block>
						</fo:table-cell>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							TMP <xsl:value-of select="//muddaily/L_muddaily.temperature"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							MW <xsl:value-of select="//muddaily/L_muddaily.mwsurface"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							VIS <xsl:value-of select="//muddaily/L_muddaily.funnelvis"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							PV <xsl:value-of select="//muddaily/L_muddaily.pv"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							YP <xsl:value-of select="//muddaily/L_muddaily.yp"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black" number-columns-spanned="2">
							<fo:block text-align="center">
							Gel10s <xsl:value-of select="//muddaily/L_muddaily.gel10s"/> /
							</fo:block>
							<fo:block text-align="center">
							10m <xsl:value-of select="//muddaily/L_muddaily.gel10m"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							F.L. API <xsl:value-of select="//muddaily/L_muddaily.apifl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							F.L. HTHP <xsl:value-of select="//muddaily/L_muddaily.hthpcake"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Sol	<xsl:value-of select="//muddaily/L_muddaily.solids"/>
							</fo:block>
						</fo:table-cell>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Sand <xsl:value-of select="//muddaily/L_muddaily.lgs"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							MBT <xsl:value-of select="//muddaily/L_muddaily.sand"/>
							</fo:block>
						</fo:table-cell>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							PH <xsl:value-of select="//muddaily/L_muddaily.oil"/>
							</fo:block>
						</fo:table-cell>						
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Cl <xsl:value-of select="//muddaily/L_muddaily.cl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							Hard <xsl:value-of select="//muddaily/L_muddaily.h2osalinity"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" border-right="0.25px solid black">
							<fo:block text-align="center">
							KCL <xsl:value-of select="//muddaily/L_muddaily.excesslime"/>
							</fo:block>
						</fo:table-cell>											
						<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
							<fo:block text-align="center">
							Daily Cost <xsl:value-of select="//muddaily/L_muddaily.cost"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-header>
				<fo:table-body>
					<xsl:apply-templates select="/root/modules/MudProperties/MudProperties[dailyUid = $dailyid and mudType='h2o']" mode="h2o"/>
				</fo:table-body>
			</fo:table>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="modules/MudProperties/MudProperties" mode="h2o">	
		<fo:table-row>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="reportNumber"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:variable name="dailyid" select="dailyUid"/>
					<xsl:value-of select="/root/modules/Daily/Daily[dailyUid=$dailyid]/dayDate"/> -
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(reportTime/@epochMS,'HH:mm')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudMedium"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="depthMdMsl"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudTestTemperature"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudWeight"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudFv"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudPv"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudYp"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black" number-columns-spanned="2">
				<fo:block>
					<xsl:value-of select="mudGel10s"/> / <xsl:value-of select="mudGel10m"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudApiFl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="hthpFl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudDissolvedSolids"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="sand"/>					
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudMbt"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudPh"/>
				</fo:block>
			</fo:table-cell>	
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudChlorideIon"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudCa"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="0.1cm" border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="mudKcl"/>
				</fo:block>
			</fo:table-cell>				
			<fo:table-cell padding="0.1cm">
				<fo:block>
					<xsl:value-of select="mudCost"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Mud section END -->
	
	<!-- Survey section START -->
	
	<xsl:template name="survey_section">
		<fo:page-sequence master-reference="fwr">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<xsl:call-template name="header"/>
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="8pt">
				<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
									<fo:block text-align="left" font-size="12pt">Survey Data</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<xsl:if	test="count(/root/modules/SurveyReference/SurveyReference/SurveyStation) &gt; 0">
						<xsl:apply-templates select="/root/modules/SurveyReference"/>
					</xsl:if>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	
	<xsl:template match="modules/SurveyReference">
		<xsl:variable name="surveyReferenceUid" select="SurveyReference/surveyReferenceUid"/>
		<fo:table inline-progression-dimension="100%" table-layout="fixed" font-size="8pt">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block text-align="right">
							Mag Dec: <xsl:value-of select="SurveyReference/magneticDeclination"/>							
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>		                      	
		<fo:table inline-progression-dimension="100%" table-layout="fixed" font-size="8pt" border="0.25px solid black">
			<fo:table-column column-number="1" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="2" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="3" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="4" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="5" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="6" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="7" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="8" column-width="proportional-column-width(10)"/>
			<fo:table-column column-number="9" column-width="proportional-column-width(20)"/>
			<fo:table-header>				
				<fo:table-row font-size="8pt" text-align="center">
					<fo:table-cell padding="0.1cm">
						<fo:block>MD.</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm">
						<fo:block>Incl.</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm">
						<fo:block>Corr. Az</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm">
						<fo:block>TVD</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm">
						<fo:block>'V' Sect</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm">
						<fo:block>Dogleg</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm">
						<fo:block>N/S</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm">
						<fo:block>E/W</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm">
						<fo:block>Tool Type</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row font-size="8pt" text-align="center">
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block>
							(<xsl:value-of select="SurveyReference/SurveyStation/depthMdMsl/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block>
							(<xsl:value-of select="SurveyReference/SurveyStation/inclinationAngle/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block>
							(<xsl:value-of select="SurveyReference/SurveyStation/azimuthAngle/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block>
							(<xsl:value-of select="SurveyReference/SurveyStation/depthTvdMsl/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block>
							(<xsl:value-of select="SurveyReference/SurveyStation/verticalSectionExtrusion/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block>
							(<xsl:value-of select="SurveyReference/SurveyStation/dlsAngle/@uomSymbol"/> )
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block>
							(<xsl:value-of select="SurveyReference/SurveyStation/nsOffset/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block>
							(<xsl:value-of select="SurveyReference/SurveyStation/ewOffset/@uomSymbol"/>)
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
						<fo:block color="white">.</fo:block>
					</fo:table-cell>
				</fo:table-row>			
			</fo:table-header>
			<fo:table-body width="100%">
				<xsl:for-each select="SurveyReference/SurveyStation[surveyReferenceUid=$surveyReferenceUid]">
					<xsl:sort select="SurveyReference/SurveyStation/depthMdMsl" data-type="number"/>
					<fo:table-row font-size="8pt">
						<fo:table-cell border-right="0.25px solid black" padding-before="0.1cm"
							padding-start="0.1cm" padding-end="0.1cm">
							<fo:block text-align="right">
								<xsl:value-of select="depthMdMsl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.25px solid black" padding-before="0.1cm"
							padding-start="0.1cm" padding-end="0.1cm">
							<fo:block text-align="right">
								<xsl:value-of select="inclinationAngle"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.25px solid black" padding-before="0.1cm"
							padding-start="0.1cm" padding-end="0.1cm">
							<fo:block text-align="right">
								<xsl:value-of select="azimuthAngle"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.25px solid black" padding-before="0.1cm"
							padding-start="0.1cm" padding-end="0.1cm">
							<fo:block text-align="right">
								<xsl:value-of select="depthTvdMsl"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.25px solid black" padding-before="0.1cm"
							padding-start="0.1cm" padding-end="0.1cm">
							<fo:block text-align="right">
								<xsl:value-of select="verticalSectionExtrusion"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.25px solid black" padding-before="0.1cm"
							padding-start="0.1cm" padding-end="0.1cm">
							<fo:block text-align="right">
								<xsl:value-of select="dlsAngle"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.25px solid black" padding-before="0.1cm"
							padding-start="0.1cm" padding-end="0.1cm">
							<fo:block text-align="right">
								<xsl:value-of select="nsOffset"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell border-right="0.25px solid black" padding-before="0.1cm"
							padding-start="0.1cm" padding-end="0.1cm">
							<fo:block text-align="right">
								<xsl:value-of select="ewOffset"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
							<fo:block>
								<xsl:value-of select="toolType"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>											
	</xsl:template>	
	<!-- Survey section END -->
	
	<!-- Formation section BEGIN -->
	<xsl:template name="formation_section">
		<fo:page-sequence master-reference="fwr_landscape">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<xsl:call-template name="header_landscape"/>
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="10pt">
				<fo:block>
					<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
									<fo:block text-align="left" font-size="12pt">Stratigraphy</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<xsl:apply-templates select="/root/modules/Well/Well" mode="formation_section"/>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	
	<xsl:template match="modules/Well/Well" mode="formation_section">
		<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="left" font-size="12pt">Well: <xsl:value-of select="wellName"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<xsl:apply-templates select="/root/modules/Formation" mode="formation_section_header"/>
	</xsl:template>
	
	<xsl:template match="modules/Formation" mode="formation_section_header">
		<fo:table inline-progression-dimension="27cm" table-layout="fixed" font-size="8pt" border="0.5px solid black" wrap-option="wrap" space-after="12pt" table-omit-header-at-break="false">
			<fo:table-column column-width="proportional-column-width(14)"/>
			<fo:table-column column-width="proportional-column-width(8)"/>
			<fo:table-column column-width="proportional-column-width(8)"/>
			<fo:table-column column-width="proportional-column-width(8)"/>
			<fo:table-column column-width="proportional-column-width(8)"/>
			<fo:table-column column-width="proportional-column-width(8)"/>
			<fo:table-column column-width="proportional-column-width(7)"/>
			<fo:table-column column-width="proportional-column-width(7)"/>
			<fo:table-column column-width="proportional-column-width(7)"/>
			<fo:table-column column-width="proportional-column-width(25)"/>
			<fo:table-header>		
				<fo:table-row>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" text-align="center">
						<fo:block>Formation Name</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" text-align="center">
						<fo:block>Prog. TVD SS
						</fo:block>
						<fo:block>
						<xsl:value-of select="//unitsymbols/U_formation.prog_subseatvd" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" text-align="center">
						<fo:block>Prog. TVD BRT
						</fo:block>
						<fo:block>
						<xsl:value-of select="//unitsymbols/U_formation.prog_toptvd" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" text-align="center">
						<fo:block>Prog. MD BRT
						</fo:block>
						<fo:block>
						<xsl:value-of select="//unitsymbols/U_formation.prog_topmd" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" text-align="center">
						<fo:block>TVD SS
						</fo:block>
						<fo:block>
						<xsl:value-of select="//unitsymbols/U_formation.subseatvd" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" text-align="center">
						<fo:block>TVD BRT
						</fo:block>
						<fo:block>
						<xsl:value-of select="//unitsymbols/U_formation.toptvd" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" text-align="center">
						<fo:block>MD BRT
						</fo:block>
						<fo:block>
						<xsl:value-of select="//unitsymbols/U_formation.topmd" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" text-align="center">
						<fo:block>Thickness
						</fo:block>
						<fo:block>
						<xsl:value-of select="//unitsymbols/U_formation.thickness" />
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" text-align="center">
						<fo:block>Difference
						</fo:block>
					</fo:table-cell>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black" text-align="center">
						<fo:block>Pick-Criteria
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:apply-templates select="Formation" mode="formation_section_detail"/>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<xsl:template match="Formation" mode="formation_section_detail">
		<fo:table-row font-size="7pt">
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm" border-right="0.25px solid black">
				<fo:block text-align="left">
					<xsl:value-of select="formationName"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm" border-right="0.25px solid black">
				<fo:block text-align="right">
					<xsl:value-of select="prog_subseatvd"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm" border-right="0.25px solid black">
				<fo:block text-align="right">
					<xsl:value-of select="prognosedTopTvdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm" border-right="0.25px solid black">
				<fo:block text-align="right">
					<xsl:value-of select="prognosedTopMdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm" border-right="0.25px solid black">
				<fo:block text-align="right">
					<xsl:value-of select="subseatvd"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm" border-right="0.25px solid black">
				<fo:block text-align="right">
					<xsl:value-of select="sampleTopTvdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm" border-right="0.25px solid black">
				<fo:block text-align="right">
					<xsl:value-of select="sampleTopMdMsl"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm" border-right="0.25px solid black">
				<fo:block text-align="right">
					<xsl:value-of select="thickness"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm" border-right="0.25px solid black">
				<fo:block text-align="right">
					<xsl:value-of select="diff_from_prog"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm">
				<fo:block>
					<xsl:value-of select="pickCriteria"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- Formation section END -->
	
	<!-- HSE section START -->
	<!-- casing section BEGIN -->
	<xsl:template name="hse_section">
		<fo:page-sequence master-reference="fwr">
			<fo:static-content flow-name="xsl-region-before">
				<fo:block>
					<xsl:call-template name="header"/>
				</fo:block>
			</fo:static-content>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block>
					<xsl:call-template name="footer"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body" font-size="8pt">
				<fo:block>
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
						<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
									<fo:block text-align="left" font-size="12pt">Hse Performance</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					<xsl:apply-templates select="/root/modules/Well/Well" mode="hse_section"/>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	
	<xsl:template match="modules/Well/Well" mode="hse_section">
		<fo:table inline-progression-dimension="19cm" table-layout="fixed" font-size="8pt" wrap-option="wrap" space-after="6pt" table-omit-header-at-break="true">
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
						<fo:block text-align="left" font-size="12pt">Well: <xsl:value-of select="wellName"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<xsl:apply-templates select="/root/modules/HseIncident" mode="hse_section_header"/>
	</xsl:template>
	
	<xsl:template match="modules/HseIncident" mode="hse_section_header">
	<fo:table width="100%" table-layout="fixed" font-size="8pt" border="0.5px solid black"
		wrap-option="wrap" space-after="6pt" table-omit-header-at-break="false">
		<fo:table-column column-width="proportional-column-width(15)"/>
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(15)"/>		
		<fo:table-column column-width="proportional-column-width(20)"/>
		<fo:table-column column-width="proportional-column-width(30)"/>
		<fo:table-header>		
			<fo:table-row text-align="center" font-weight="bold" background-color="#CCCCCC">
				<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
					<fo:block>Rig</fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
					<fo:block>Event (# of)</fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
					<fo:block>Date - Time</fo:block>
				</fo:table-cell>				
				<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
					<fo:block>Short Description</fo:block>
				</fo:table-cell>
				<fo:table-cell padding="0.1cm" border-bottom="0.25px solid black">
					<fo:block>Extended Description</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-header>
		<fo:table-body>
		<xsl:apply-templates select="HseIncident">
			<xsl:sort select="incidentCategory"/>
			<xsl:sort select="hse_eventdate"/>
		</xsl:apply-templates>
		</fo:table-body>
	</fo:table>
	</xsl:template>
	
	<xsl:template match="HseIncident">
	<xsl:variable select="operationUid" name="currentwellid"/>
	<!--<xsl:variable select="//*/rigs_record[rigname=//wells_records/wells_record[wellid=$currentwellid]/rigid]/operator"
		name="operatorid"/>
		--><fo:table-row font-size="8pt" keep-together="always">
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm"
				border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="/root/modules/RigInformation/RigInformation/rigOwner/@lookupLabel"/> -
					<xsl:value-of select="//root/modules/Well/Well[wellUid=$currentwellid]/rigid"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm"
				border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="incidentCategory"/>(<xsl:value-of select="numberOfIncidents"/>)
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm"
				border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="d2Utils:formatDateFromEpochMS(hse_eventdate_hse_eventtime/@epochMS,'dd MMM yyyy HH:mm')"/>
				</fo:block>
			</fo:table-cell>			
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm"
				border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="hse_shortdescription"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-before="0.05cm" padding-left="0.1cm" padding-right="0.1cm"
				border-right="0.25px solid black">
				<fo:block>
					<xsl:value-of select="description"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- HSE section END -->
	
</xsl:stylesheet>