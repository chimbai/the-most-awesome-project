<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:fo="http://www.w3.org/1999/XSL/Format" 
xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
	
	<xsl:output method="xml" />  
	<xsl:preserve-space elements="*" />
	<xsl:template match="/"> 
		<fo:root> 
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/> 
					<fo:region-before extent="3cm"/>  
					<fo:region-after extent="1.5cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple" initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="12pt" padding="10px" font-weight="bold">
						<fo:table-column column-number="1" column-width="proportional-column-width(30)" />
						<fo:table-column column-number="2" column-width="proportional-column-width(40)" />
						<fo:table-column column-number="3" column-width="proportional-column-width(30)" />
						<fo:table-body space-before="10pt" space-after="6pt">
							<fo:table-row>
								<fo:table-cell number-rows-spanned="2" font-size="8pt">
									<fo:block>
										<fo:external-graphic
											content-width="scale-to-fit" width="90pt" content-height="60%" scaling="uniform"
											src="url(d2://images/company_logo.jpg)" />										
									</fo:block> 
								</fo:table-cell>
								<fo:table-cell number-rows-spanned="2" padding-top="0.7cm" text-align="center" font-size="10pt">
									<fo:block>
										AFTER ACTION REVIEW	
									</fo:block>
								</fo:table-cell>
								<fo:table-cell number-rows-spanned="2" font-size="8pt">
									<fo:block text-align="right">
										<fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="12pt">
											<fo:table-column column-number="1" column-width="proportional-column-width(100)" />
											<fo:table-body space-before="6pt" space-after="6pt">
												<fo:table-row>
													<fo:table-cell padding="0.1cm">
														<fo:block padding="0.1cm">
															Day # <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportNumber" />		
														</fo:block>
														<fo:block padding="0.1cm">
															<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportDatetime"/>		
														</fo:block>
													</fo:table-cell>  
												</fo:table-row>
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content> 
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="12pt">
						<fo:table-column column-number="1" column-width="proportional-column-width(50)" />
						<fo:table-column column-number="2" column-width="proportional-column-width(50)" />
						<fo:table-body space-before="6pt" space-after="6pt"> 
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Printed on
										<xsl:variable name="printdate"
											select="/root/modules/Daily/Daily/dayDate" />
											 <xsl:value-of select="d2Utils:getLocalTimestamp('dd MMM yyyy hh:mm a (z)')" />
											 <!-- <xsl:value-of select="d2Utils:getSystemDate('dd MMM yyyy hh:mm a (z)')" /> -->
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="right">
										Page
										<fo:page-number/>
									</fo:block>  
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>   
				<fo:flow flow-name="xsl-region-body" font-size="8pt">
					<fo:block>
						<fo:table inline-progression-dimension="100%" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(100)" />
							<fo:table-body>
							
								<!-- Daily After Action Review Data -->
								<fo:table-row>	
									<fo:table-cell padding-bottom="2px" padding-top="2px">
										<fo:block>
											<xsl:apply-templates select="/root/modules/DailyAfterActionReview/DailyAfterActionReview" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>
       			</fo:flow> 
			</fo:page-sequence>
		</fo:root>
	</xsl:template>	
    
    <xsl:template match="modules/DailyAfterActionReview/DailyAfterActionReview">
		 <fo:table inline-progression-dimension="100%" table-layout="fixed" space-after="24pt">
		 <xsl:if test="position() != last()"><xsl:attribute name="break-after">page</xsl:attribute></xsl:if>
			
			<fo:table-body> 
				<fo:table-row>
				<fo:table-cell>
						<fo:block>
							<fo:table inline-progression-dimension="100%" table-layout="fixed">
							<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
							<fo:table-body> 
							<fo:table-row>
									<fo:table-cell number-columns-spanned="2" text-align="center" font-size="10pt" font-weight="bold" padding="0.1cm">
										<fo:block>
											Well Name or Asset Name:<fo:inline color="white">!</fo:inline><xsl:value-of select="../../Well/Well/wellName"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</fo:table-body>
							</fo:table>
							<fo:table inline-progression-dimension="100%" table-layout="fixed" border="0.5px solid black">
								<fo:table-column column-number="1" column-width="proportional-column-width(25)" />
								<fo:table-column column-number="2" column-width="proportional-column-width(75)" />
								<fo:table-body font-size="8pt">
								<fo:table-row>
										<fo:table-cell background-color="#EEEEEE" border-right="0.5px solid black" border-bottom="0.5px solid black" padding="0.1cm" padding-top="0.2cm">
											<fo:block font-weight="bold">
												Operation or Activity Description
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-bottom="0.5px solid black" padding="0.1cm">
											<fo:block>
												<xsl:value-of select="workExecuted"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell background-color="#EEEEEE" border-right="0.5px solid black" border-bottom="0.5px solid black" padding="0.1cm" padding-top="0.2cm">
											<fo:block font-weight="bold">
												After Action Review Attendees
											</fo:block>
										</fo:table-cell>
										<fo:table-cell border-bottom="0.5px solid black" padding="0.1cm">
											<fo:block>
												<xsl:value-of select="attendees"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
			 							<fo:table-cell background-color="#EEEEEE" number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.01cm" padding-top="0.2cm" padding-left="0.1cm">
											<fo:block font-weight="bold">
												Work Scope
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.1cm">
											<fo:block>
												<xsl:value-of select="workCategory"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell background-color="#EEEEEE" number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.01cm" padding-top="0.2cm" padding-left="0.1cm">
											<fo:block font-weight="bold">
												Detailed Description
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.1cm">
											<fo:block>
												<xsl:value-of select="descriptionDetail"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell background-color="#EEEEEE" number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.01cm" padding-top="0.2cm" padding-left="0.1cm">
											<fo:block font-weight="bold">
												WHAT went well?
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.1cm">
											<fo:block>
												<xsl:value-of select="positiveOutcomes"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell background-color="#EEEEEE" number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.01cm" padding-top="0.2cm" padding-left="0.1cm">
											<fo:block font-weight="bold">
												WHAT did not go well?
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.1cm">
											<fo:block>
												<xsl:value-of select="negativeOutcomes"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell background-color="#EEEEEE" number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.01cm" padding-top="0.2cm" padding-left="0.1cm">
											<fo:block font-weight="bold">
												WHY the operation did not go well?
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.1cm">
											<fo:block>
												<xsl:value-of select="rootCause"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell background-color="#EEEEEE" number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.01cm" padding-top="0.2cm" padding-left="0.1cm">
											<fo:block font-weight="bold">
												HOW do we repeat the good areas of performance in the future?
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.1cm">
											<fo:block>
												<xsl:value-of select="toRepeatPractices"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell background-color="#EEEEEE" number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.01cm" padding-top="0.2cm" padding-left="0.1cm">
											<fo:block font-weight="bold">
												HOW do we avoid areas of not so good performance in the future?
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.1cm">
											<fo:block>
												<xsl:value-of select="correctiveAction"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell background-color="#EEEEEE" number-columns-spanned="2" border-bottom="0.5px solid black" padding="0.01cm" padding-top="0.2cm" padding-left="0.1cm">
											<fo:block font-weight="bold">
												OTHER opportunities for improvement or learning
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row>
										<fo:table-cell number-columns-spanned="2" padding="0.1cm">
											<fo:block>
												<xsl:value-of select="otherImprovements"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
							  </fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>	
			</fo:table-body>
		</fo:table> 
	</xsl:template>
</xsl:stylesheet>
