<?xml version='1.0'?>
<xsl:stylesheet xmlns:npd="http://www.witsml.org/schemas/131"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="html" encoding="UTF-8" indent="no"/>

    <xsl:template match="npd:drillReports">
        <xsl:apply-templates select="npd:drillReport"/>
    </xsl:template>

    <xsl:template match="npd:drillReport">
    	<table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            
    		<tbody>
    			<tr bgcolor="#2284BC">
                    <th colspan="2" style="text-align:center">NPD Report Preview Ver 1.0</th>
                </tr>
                <tr bgcolor="#1A5F88">
                    <td colspan="2" style="font-style: italic; color:white;" align="center"> Header </td>
                </tr>
                <tr>
                    <td >Well Name</td>
                    <td>
                        <xsl:value-of select="npd:nameWell"/>
                    </td>
                </tr>
                <tr>
                    <td >Wellbore Name</td>
                    <td >
                        <xsl:value-of select="npd:nameWellbore"/>
                    </td>
                </tr>
                <tr>
                    <td >Start Time</td>
                    <td >
                        <xsl:value-of select="npd:dTimStart"/>
                    </td>
                </tr>
                <tr>
                    <td >End Time</td>
                    <td >
                        <xsl:value-of select="npd:dTimEnd"/>
                    </td>
                </tr>
                <tr>
                    <td >Version Kind</td>
                    <td >
                        <xsl:value-of select="npd:versionKind"/>
                    </td>
                </tr>
                <tr>
                    <td >Date Created</td>
                    <td >
                        <xsl:value-of select="npd:createDate"/>
                    </td>
                </tr>
                                
                <xsl:if test="npd:wellAlias">	
                <tr bgcolor="#1A5F88">
                    <td colspan="2" style="font-style: italic; color:white;" align="center"> Well Alias </td>
                </tr>                      
                    <xsl:apply-templates select="npd:wellAlias"/> 
                </xsl:if>
                     
                <xsl:if test="npd:wellboreAlias">	               
                <tr bgcolor="#1A5F88">
                    <td  colspan="2" style="font-style: italic; color:white;" align="center"> Wellbore Alias </td>
                </tr>
                <tr bgcolor="#2284BC"> 
                    <td >Naming System</td>
                    <td >Alias Name</td> 
                </tr>                      
                	<xsl:apply-templates select="npd:wellboreAlias"/> 
                </xsl:if>
                	
                <xsl:if test="npd:wellboreInfo">	  
                <tr bgcolor="#1A5F88">
                    <td  colspan="2" style="font-style: italic; color:white;" align="center"> Wellbore Info </td>
                </tr>                
                	<xsl:apply-templates select="npd:wellboreInfo"/>                      
                </xsl:if>           
            </tbody>
        </table>
        
     <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            <xsl:if test="npd:statusInfo">
                <tr bgcolor="#1A5F88">
                    <td colspan="11" style="font-style: italic; color:white;" align="center">Status Info </td>
                </tr>
               
                    <xsl:apply-templates select="npd:statusInfo"/>
                
            </xsl:if>         
            </tbody>
     </table> 
     
     <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
     	<tbody>
     			<xsl:if test="npd:fluid">
                <tr bgcolor="#1A5F88">
                    <td colspan="9" style="font-style: italic; color:white;" align="center">Mud/Weight </td>
                </tr>
                <tr bgcolor="#2284BC">
                    <td >Start Time</td><td>Pressure BOP Rating</td><td>MD</td><td>Type</td><td >Mud Class</td><td>Density</td><td>PV</td><td>YP</td>
                </tr> 
                <xsl:apply-templates select="npd:fluid"/>             
         		</xsl:if>  
         </tbody>
     </table>
     
      <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
      		<tbody>
      		<xsl:if test="npd:bitRecord">
     			<tr bgcolor="#1A5F88">
                    <td colspan="4" style="font-style: italic; color:white;" align="center">Bit Data</td>
                </tr>
                <tr bgcolor="#2284BC">
                    <td >Bit Number</td><td >Bit Diameter</td><td >Manufacturer</td><td>Manufacturer Code</td>
                </tr>
                <xsl:apply-templates select="npd:bitRecord"/>      
                </xsl:if>  
          	</tbody>
       </table>
       
        
        <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            <xsl:if test="npd:porePressure">
                <tr bgcolor="#1A5F88">
                    <td colspan="9" style="font-style: italic; color:white;" align="center">Pore Pressure </td>
                </tr>
                <tr bgcolor="#2284BC">
                    <td >Kind Of Reading</td><td>Measured Depth</td><td>True Vertical Depth</td><td>Equicalent Mud Weight</td>
                </tr> 
                <xsl:apply-templates select="npd:porePressure"/>  
                </xsl:if>    
            </tbody>
        </table>
        
        <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            	<xsl:if test="npd:controlIncidentInfo">
                <tr bgcolor="#1A5F88">
                    <td colspan="16" style="font-style: italic; color:white;" align="center">Kick Info</td>
                </tr>
                <tr bgcolor="#2284BC">
                    <td >Date</td><td>Depth</td><td>Phase</td><td>Proprietary Code</td>
                    <td >Miss Minutes</td><td>Bit OD</td><td>Bit Depth</td><td>Gain Volume</td>
                    <td >Shut in Casing Pressure</td><td>Shut in Drillpipe Pressure</td><td>Kick Classification</td><td>Kill Procedure</td>
                    <td >Current Formation</td><td>DH Temp</td><td>Choke Pressure (Max)</td><td>Description</td>
                </tr> 
                <xsl:apply-templates select="npd:controlIncidentInfo"/>  
                </xsl:if>  
            </tbody>
        </table>        
        
        <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            	<xsl:if test="count(npd:surveyStation) &gt; 0">
                <tr bgcolor="#1A5F88">
                    <td colspan="9" style="font-style: italic; color:white;" align="center">Survey</td>
                </tr>
                <tr bgcolor="#2284BC">
                    <td >Measured Depth</td><td>True Vertical Depth</td><td>Azimuth</td><td>Inclination</td>
                </tr> 
                <xsl:apply-templates select="npd:surveyStation"/>    
                </xsl:if>
            </tbody>
        </table>
        
        <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            	<xsl:if test="npd:equipFailureInfo">
                <tr bgcolor="#1A5F88">
                    <td colspan="9" style="font-style: italic; color:white;" align="center">Equipment Failure</td>
                </tr>
                <tr bgcolor="#2284BC">
                    <td >Date Started</td><td>Depth</td><td>System</td><td>Downtime</td><td>Date Finished</td><td>Description</td>
                </tr> 
                <xsl:apply-templates select="npd:equipFailureInfo"/>  
                </xsl:if>  
            </tbody>
        </table>
        
        <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            	<xsl:if test="npd:coreInfo">
                <tr bgcolor="#1A5F88">
                    <td colspan="11" style="font-style: italic; color:white;" align="center">Core</td>
                </tr>
                <tr bgcolor="#2284BC">
                    <td >Date</td><td >Core Number</td><td>MD Top</td><td>MD Bottom</td><td>TVD Top</td><td>TVD Bottom</td><td>Length Recovered</td><td>% Recovered</td><td>Barrel length</td><td>Inner Barrel Type</td><td>Description</td>
                </tr> 
                <xsl:apply-templates select="npd:coreInfo"/>    
                </xsl:if>
            </tbody>
        </table>
        
        <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            	<xsl:if test="npd:lithShowInfo">
                <tr bgcolor="#1A5F88">
                    <td colspan="4" style="font-style: italic; color:white;" align="center">Lithology</td>
                </tr>
                
                <tr bgcolor="#2284BC">
                    <td >Date</td><td>MD Top</td><td>MD Bottom</td><td>Lithology</td>
                </tr> 
                <xsl:apply-templates select="npd:lithShowInfo"/> 
                </xsl:if>   
            </tbody>
        </table>   
        
        <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            <xsl:if test="npd:gasReadingInfo">
                <tr bgcolor="#1A5F88">
                    <td colspan="14" style="font-style: italic; color:white;" align="center">Gas Reading</td>
                </tr>       
                <tr bgcolor="#2284BC">             
	        		<td>Date</td><td>Reading Type</td><td>MD Top</td><td>MD Bottom</td><td>TVD Top</td><td>TVD Bottom</td> 
	            	<td>Gas High</td><td>Gas Low</td><td>Meth</td><td>Eth</td><td>Prop</td><td>Ibut</td><td>Nbut</td><td>Ipent</td>           
        		</tr>          
                <xsl:apply-templates select="npd:gasReadingInfo"/> 
            </xsl:if>    
            </tbody>
        </table>   
        
        <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            <xsl:if test="npd:stratInfo">
                <tr bgcolor="#1A5F88">
                    <td colspan="4" style="font-style: italic; color:white;" align="center">Formation</td>
                </tr>
                <tr bgcolor="#2284BC">
                    <td >Date</td><td>MD Top</td><td>TVD Top</td><td>Description</td>
                </tr> 
                <xsl:apply-templates select="npd:stratInfo"/> 
            </xsl:if>    
            </tbody>
        </table>  
        
        <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            <xsl:if test="npd:perfInfo">
                <tr bgcolor="#1A5F88">
                    <td colspan="4" style="font-style: italic; color:white;" align="center">Perforations</td>
                </tr>
                <tr bgcolor="#2284BC">
                    <td >Time Open</td><td>Time Close</td><td>Top MD</td><td>Bottom MD</td>
                </tr> 
                <xsl:apply-templates select="npd:perfInfo"/>   
            </xsl:if>  
            </tbody>
        </table> 
        
        <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            <xsl:if test="npd:formTestInfo">
                <tr bgcolor="#1A5F88">
                    <td colspan="10" style="font-style: italic; color:white;" align="center">RF Test</td>
                </tr>
                <tr bgcolor="#2284BC">
                    <td >Date</td><td>MD</td><td>TVD</td><td>Pore Pres</td><td>Good Seal</td><td>Md Sample</td><td>Dominate Component</td><td>Density HC</td><td>Volume Sample</td><td>Description</td>
                </tr> 
                <xsl:apply-templates select="npd:formTestInfo"/> 
            </xsl:if>    
            </tbody>
        </table>       
   
        <table border="1" width="90%" style="text-align:left;margin-left:auto; margin-right:auto;">
            <tbody>
            <xsl:if test="npd:activity">
                <tr bgcolor="#1A5F88">
                    <td colspan="8" style="font-style: italic; color:white;" align="center">Operation/mode </td>
                </tr>
                <tr bgcolor="#2284BC">
                    <td>Start Time </td>  <td>End Time</td><td>MD</td><td>Phase</td><td>Proprietary Code</td><td>State Detail</td><td>State</td><td>Comments</td></tr>
                <xsl:apply-templates select="npd:activity"/>
            </xsl:if>   
            </tbody>
        </table>
    </xsl:template>
    <xsl:template match="npd:wellAlias"> 
        <tr bgcolor="#2284BC"> 
            <td >Naming System</td>
            <td >Alias Name</td> 
        </tr>      
        <tr> <td>
            <xsl:value-of select="npd:namingSystem"/>
        </td>
            <td>
                <xsl:value-of select="npd:name"/>
            </td> 
        </tr>
    </xsl:template>
    
    <xsl:template match="npd:wellboreInfo"> 
    	<tr>
            <td >Spud Date</td>
            <td >
                <xsl:value-of select="npd:dTimSpud"/>
            </td>
        </tr>
        <tr>
            <td >Operator</td>
            <td >
                <xsl:value-of select="npd:operator"/>
            </td>
        </tr>
        <tr bgcolor="#1A5F88">
             <td  colspan="2" style="font-style: italic; color:white;" align="center"> Rig Alias </td>
        </tr>
        <tr bgcolor="#2284BC"> 
            <td >Naming System</td>
            <td >Alias Name</td> 
        </tr> 
        <xsl:apply-templates select="npd:rigAlias"/>   
    </xsl:template>
    
    <xsl:template match="npd:rigAlias">          
        <tr> <td>
            <xsl:value-of select="npd:namingSystem"/>
        </td>
            <td>
                <xsl:value-of select="npd:name"/>
            </td> 
        </tr>        
    </xsl:template>
    
    <xsl:template match="npd:wellboreAlias">     
          <tr> <td>
            <xsl:value-of select="npd:namingSystem"/>
        </td>
            <td>
                <xsl:value-of select="npd:name"/>
            </td> 
        </tr>        
    </xsl:template>
    
    <xsl:template match="npd:fluid">
        <tr>
        <td>
                <xsl:value-of select="npd:dTim"/>
        </td> <td>
            <xsl:variable name="bopPress" select="npd:presBopRating" />
            <xsl:value-of select='format-number($bopPress, "######.##")' />
            <xsl:value-of select="npd:presBopRating/@uom"/>
        </td>
            <td>
                <xsl:value-of select="npd:md"/><xsl:value-of select="npd:md/@uom"/>
            </td>       
            <td>
                <xsl:value-of select="npd:type"/>
            </td>
            <td>
                <xsl:value-of select="npd:mudClass"/>
            </td>
            <td>
                <xsl:variable name="dens" select="npd:density" />
	            <xsl:value-of select='format-number($dens, "######.##")' />
                <xsl:value-of select="npd:density/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:pv"/><xsl:value-of select="npd:pv/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:yp"/><xsl:value-of select="npd:yp/@uom"/>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="npd:bitRecord">
        <tr>
            <td>
                <xsl:value-of select="npd:numBit"/>
            </td> <td>
                <xsl:value-of select="npd:diaBit"/> <xsl:value-of select="npd:diaBit/@uom"/>
            </td>  <td>
                <xsl:value-of select="npd:manufacturer"/>
            </td> <td>
                <xsl:value-of select="npd:codeMfg"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="npd:activity">
       
     <xsl:choose>
         <xsl:when test="position() mod 2">
             <tr bgcolor="#E8E8E8"> 
                 <td>
                     <xsl:value-of select="npd:dTimStart"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:dTimEnd"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:md"/><xsl:value-of select="npd:md/@uom"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:phase"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:proprietaryCode"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:stateDetailActivity"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:state"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:comments"/>
                 </td>
             </tr>
         </xsl:when>
         <xsl:otherwise>
             <tr bgcolor="#FFFFFF"> 
                 <td>
                     <xsl:value-of select="npd:dTimStart"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:dTimEnd"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:md"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:phase"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:proprietaryCode"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:stateDetailActivity"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:state"/>
                 </td>
                 <td>
                     <xsl:value-of select="npd:comments"/>
                 </td>
             </tr>
         </xsl:otherwise>         
     </xsl:choose>
        
     
      
 
    </xsl:template>
    
    <xsl:template match="npd:porePressure">
        <tr bgcolor="#E8E8E8">                    
            <td>
                <xsl:value-of select="npd:readingKind"/>
            </td>
            <td>
                <xsl:value-of select="npd:md"/><xsl:value-of select="npd:md/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:tvd"/><xsl:value-of select="npd:tvd/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:equivalentMudWeight"/><xsl:value-of select="npd:equivalentMudWeight/@uom"/>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="npd:controlIncidentInfo">
                <tr bgcolor="#E8E8E8">                    
                    <td>
                        <xsl:value-of select="npd:dTim"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:mdInflow"/><xsl:value-of select="npd:mdInflow/@uom"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:phase"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:proprietaryCode"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:eTimLost "/><xsl:value-of select="npd:eTimLost/@uom"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:diaBit"/><xsl:value-of select="npd:diaBit/@uom"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:mdBit"/><xsl:value-of select="npd:mdBit/@uom"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:volMudGained"/><xsl:value-of select="npd:volMudGained/@uom"/>
                    </td>
                    <td>
                        <xsl:variable name="shutCasPress" select="npd:presShutInCasing" />
			            <xsl:value-of select='format-number($shutCasPress, "######.##")' />
                        <xsl:value-of select="npd:presShutInCasing/@uom"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:presShutInDrill"/><xsl:value-of select="npd:presShutInDrill/@uom"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:incidentType"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:killingType"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:formation"/><xsl:value-of select="npd:formation/@uom"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:tempBottom"/><xsl:value-of select="npd:tempBottom/@uom"/>
                    </td>
                    <td>
                        <xsl:variable name="chokePressMax" select="npd:presMaxChoke" />
			            <xsl:value-of select='format-number($chokePressMax, "######.##")' />
                        <xsl:value-of select="npd:presMaxChoke/@uom"/>
                    </td>
                    <td>
                        <xsl:value-of select="npd:description"/>
                    </td>
                </tr>
    </xsl:template>    
    
    <xsl:template match="npd:surveyStation">        
        <tr bgcolor="#E8E8E8">             
            <td>
                <xsl:value-of select="npd:md"/><xsl:value-of select="npd:md/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:tvd"/><xsl:value-of select="npd:tvd/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:azi"/><xsl:value-of select="npd:azi/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:incl"/><xsl:value-of select="npd:incl/@uom"/>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="npd:equipFailureInfo">        
        <tr bgcolor="#E8E8E8">             
            <td>
                <xsl:value-of select="npd:dTim"/>
            </td>
            <td>
                <xsl:value-of select="npd:md"/><xsl:value-of select="npd:md/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:equipClass"/>
            </td>
            <td>
                <xsl:value-of select="npd:eTimMissProduction"/><xsl:value-of select="npd:eTimMissProduction/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:dTimRepair"/>
            </td>
            <td>
                <xsl:value-of select="npd:description"/>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="npd:coreInfo">        
        <tr bgcolor="#E8E8E8">             
            <td>
                <xsl:value-of select="npd:dTim"/>
            </td>
            <td>
                <xsl:value-of select="npd:coreNumber"/>
            </td>
            <td>
                <xsl:value-of select="npd:mdTop"/><xsl:value-of select="npd:mdTop/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:mdBottom"/><xsl:value-of select="npd:mdBottom/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:tvdTop"/><xsl:value-of select="npd:tvdTop/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:tvdBottom"/><xsl:value-of select="npd:tvdBottom/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:lenRecovered"/><xsl:value-of select="npd:lenRecovered/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:recoverPc"/><xsl:value-of select="npd:recoverPc/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:lenBarrel"/><xsl:value-of select="npd:lenBarrel/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:innerBarrelType"/>
            </td>
            <td>
                <xsl:value-of select="npd:coreDescription"/>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="npd:lithShowInfo">        
        <tr bgcolor="#E8E8E8">             
        	<td>
                <xsl:value-of select="npd:dTim"/>
            </td>
            <td>
                <xsl:value-of select="npd:mdTop"/><xsl:value-of select="npd:mdTop/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:mdBottom"/><xsl:value-of select="npd:mdBottom/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:lithology "/>
            </td>            
        </tr>
    </xsl:template>
    
    <xsl:template match="npd:stratInfo">        
       	<tr bgcolor="#E8E8E8">             
        	<td>
                <xsl:value-of select="npd:dTim"/>
            </td>
            <td>
                <xsl:value-of select="npd:mdTop"/><xsl:value-of select="npd:mdTop/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:tvdTop"/><xsl:value-of select="npd:tvdTop/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:description"/>
            </td>            
        </tr>
    </xsl:template>
    
    <xsl:template match="npd:perfInfo">        
       	<tr bgcolor="#E8E8E8">             
        	<td>
                <xsl:value-of select="npd:dTimOpen"/>
            </td>
            <td>
                <xsl:value-of select="npd:dTimClose"/>
            </td>
            <td>
                <xsl:value-of select="npd:mdTop"/><xsl:value-of select="npd:mdTop/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:mdBottom"/><xsl:value-of select="npd:mdBottom/@uom"/>
            </td>            
        </tr>
    </xsl:template>
    
    <xsl:template match="npd:formTestInfo">        
       	<tr bgcolor="#E8E8E8">             
        	<td>
                <xsl:value-of select="npd:dTim"/>
            </td>
            <td>
                <xsl:value-of select="npd:md"/><xsl:value-of select="npd:md/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:tvd"/><xsl:value-of select="npd:tvd/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:presPore"/><xsl:value-of select="npd:presPore/@uom"/>
            </td>   
            <td>
                <xsl:value-of select="npd:goodSeal"/>
            </td>
            <td>
                <xsl:value-of select="npd:mdSample"/><xsl:value-of select="npd:mdSample/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:dominateComponent"/>
            </td> 
            <td>
                <xsl:value-of select="npd:densityHC"/><xsl:value-of select="npd:densityHC/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:volumeSample"/><xsl:value-of select="npd:volumeSample/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:description"/>
            </td>            
        </tr>
    </xsl:template>
    
    <xsl:template match="npd:gasReadingInfo">   
    	
    	      
        <tr bgcolor="#E8E8E8">             
        	<td>
                <xsl:value-of select="npd:dTim"/>
            </td>
            <td>
                <xsl:value-of select="npd:readingType"/>
            </td>
            <td>
                <xsl:value-of select="npd:mdTop"/><xsl:value-of select="npd:mdTop/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:mdBottom"/><xsl:value-of select="npd:mdBottom/@uom"/>
            </td>  
            <td>
                <xsl:value-of select="npd:tvdTop"/><xsl:value-of select="npd:tvdTop/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:tvdBottom"/><xsl:value-of select="npd:tvdBottom/@uom"/>
            </td> 
            <td>
                <xsl:value-of select="npd:gasHigh"/><xsl:value-of select="npd:gasHigh/@uom"/>
            </td>      
       		<td>
                <xsl:value-of select="npd:gasLow"/><xsl:value-of select="npd:gasLow/@uom"/>
            </td>   
            <td>
                <xsl:value-of select="npd:meth"/><xsl:value-of select="npd:meth/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:eth"/><xsl:value-of select="npd:eth/@uom"/>
            </td> 
             <td>
                <xsl:value-of select="npd:prop "/><xsl:value-of select="npd:prop/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:ibut"/><xsl:value-of select="npd:ibut/@uom"/>
            </td>   
            <td>
                <xsl:value-of select="npd:nbut"/><xsl:value-of select="npd:nbut/@uom"/>
            </td>
            <td>
                <xsl:value-of select="npd:ipent"/><xsl:value-of select="npd:ipent/@uom"/>
            </td>           
        </tr>       
    </xsl:template>
    
    <xsl:template match="npd:statusInfo">
    
        <tr bgcolor="#2284BC">
        	<td>
	           Date
	        </td>
	        
            <td>
               MD
            </td> 
            <td>
	           TVD
	        </td>
            <td>
               MD Plug Top
            </td> 
            <td>
	            Hole Diameter
	        </td>
            <td>
               MD Start Hole Diameter
            </td> 
            <td>
	            Pilot Diameter
	        </td>
            <td>
               MD Diameter Pilot Plan
            </td> 
            <td>
	            TVD Diameter Pilot Plan
	        </td>
            <td>
               MD Kick Off
            </td> 
            <td>
               TVD Kick Off
            </td> 
        </tr>
    	<tr bgcolor="#E8E8E8"> 
	    	<td>
	            <xsl:value-of select="npd:dTim"/>
	        </td>
	        
            <td>
               <xsl:value-of select="npd:md"/>
               <xsl:value-of select="npd:md/@uom"/>
            </td> 
           
            <td>
	            <xsl:value-of select="npd:tvd"/>
	            <xsl:value-of select="npd:tvd/@uom"/>
	        </td>
            <td>
               <xsl:value-of select="npd:mdPlugTop"/>
               <xsl:value-of select="npd:mdPlugTop/@uom"/>
            </td> 
            <td>
	            <xsl:value-of select="npd:diaHole"/>
	            <xsl:value-of select="npd:diaHole/@uom"/>
	        </td>
            <td>
               <xsl:value-of select="npd:mdDiaHoleStart "/>
               <xsl:value-of select="npd:mdDiaHoleStart/@uom"/>
            </td> 
            <td>
	            <xsl:value-of select="npd:diaPilot"/>
	            <xsl:value-of select="npd:diaPilot/@uom"/>
	        </td>
            <td>
               <xsl:value-of select="npd:mdDiaPilotPlan"/>
               <xsl:value-of select="npd:mdDiaPilotPlan/@uom"/>
            </td> 
            <td>
	            <xsl:value-of select="npd:tvdDiaPilotPlan"/>
	            <xsl:value-of select="npd:tvdDiaPilotPlan/@uom"/>
	        </td>
            <td>
               <xsl:value-of select="npd:mdKickoff"/>
               <xsl:value-of select="npd:mdKickoff/@uom"/>
            </td> 
            <td>
               <xsl:value-of select="npd:tvdKickoff"/>
               <xsl:value-of select="npd:tvdKickoff/@uom"/>
            </td> 
        </tr>
        <tr bgcolor="#2284BC"> 
	    	<td>
	            Strength Form
	        </td>
            <td>
               MD Strength Form
            </td> 
            <td>
	            TVD Strength Form
	        </td>
            <td>
               Last Csg Diameter
            </td> 
            <td>
	            Last Csg MD
	        </td>            
            <td>
	            Last Csg TVD
	        </td>
            <td>
               Test Type
            </td> 
            <td>
	            MD Planned
	        </td>
            <td>
               Drill Distance
            </td> 
            <td>
               elevKelly
            </td>  
        </tr>
        <tr bgcolor="#E8E8E8"> 
	    	<td>
	            <xsl:value-of select="npd:strengthForm"/>
	            <xsl:value-of select="npd:strengthForm/@uom"/>
	        </td>
            <td>
               <xsl:value-of select="npd:mdStrengthForm"/>
               <xsl:value-of select="npd:mdStrengthForm/@uom"/>
            </td> 
            <td>
	            <xsl:value-of select="npd:tvdStrengthForm"/>
	            <xsl:value-of select="npd:tvdStrengthForm/@uom"/>
	        </td>
            <td>
               <xsl:value-of select="npd:diaCsgLast"/>
               <xsl:value-of select="npd:diaCsgLast/@uom"/>
            </td> 
            <td>
	            <xsl:value-of select="npd:mdCsgLast"/>
	            <xsl:value-of select="npd:mdCsgLast/@uom"/>
	        </td>
            <td>
	            <xsl:value-of select="npd:tvdCsgLast"/>
	            <xsl:value-of select="npd:tvdCsgLast/@uom"/>
	        </td>
            <td>
               <xsl:value-of select="npd:presTestType"/>
               <xsl:value-of select="npd:presTestType/@uom"/>
            </td> 
            <td>
	            <xsl:value-of select="npd:mdPlanned"/>
	            <xsl:value-of select="npd:mdPlanned/@uom"/>
	        </td>
            <td>
               <xsl:value-of select="npd:distDrill"/>
               <xsl:value-of select="npd:distDrill/@uom"/>
            </td> 
            <td>
               <xsl:value-of select="npd:elevKelly"/>
               <xsl:value-of select="npd:elevKelly/@uom"/>
            </td> 
        </tr>
        
        <tr> 
        	<td colspan="2" bgcolor="#2284BC">
        		24 Hr Summary
        	</td>       	
        	<td colspan="9" bgcolor="#E8E8E8">
        		<xsl:value-of select="npd:sum24Hr"/>
        	</td>
        </tr>
        <tr> 
        	<td colspan="2" bgcolor="#2284BC">
        		24 Hr Forecast
        	</td>       	
        	<td colspan="9" bgcolor="#E8E8E8">
        		<xsl:value-of select="npd:forecast24Hr"/>
        	</td>
        </tr>
        <tr>
        	<td colspan="2" bgcolor="#2284BC">
        		Current Rop
        	</td>        	
        	<td colspan="9" bgcolor="#E8E8E8">
        		<xsl:value-of select="npd:ropCurrent"/>
        	</td>
        </tr>
    </xsl:template> 
    
</xsl:stylesheet>
