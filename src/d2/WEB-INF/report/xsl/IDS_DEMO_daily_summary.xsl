<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils"> 

	<xsl:output method="xml" />
	<xsl:preserve-space elements="*" /> 
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="simple" space-before="12pt" page-height="29.6cm" page-width="21cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin-top="2cm" margin-bottom="2cm"/>
					<fo:region-before extent="1.0cm"/>
					<fo:region-after extent="1.0cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="simple"
				initial-page-number="1">
				<fo:static-content flow-name="xsl-region-before">
					<fo:table inline-progression-dimension="19cm"
						table-layout="fixed" padding="10px">
						<fo:table-column column-number="1"
							column-width="proportional-column-width(100)" />
						
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell font-size="12pt">
									<fo:block>
										<fo:external-graphic
											content-width="scale-to-fit" width="150pt"
											content-height="80%" scaling="uniform"
											src="url(d2://images/company_logo.jpg)" />
										
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					<fo:table inline-progression-dimension="19cm"
						table-layout="fixed" space-after="12pt">
						<fo:table-column column-number="1"
							column-width="proportional-column-width(50)" />
						<fo:table-column column-number="2"
							column-width="proportional-column-width(50)" />
						<fo:table-body space-before="6pt"
							space-after="6pt">
							<fo:table-row>
								<fo:table-cell font-size="6pt">
									<fo:block text-align="left">
										Copyright IDS, 20080615, IDS_DEMO_daily_summary
									</fo:block>
								</fo:table-cell>
								<fo:table-cell font-size="8pt">
									<fo:block text-align="right">
										Page
										<fo:page-number />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell font-size="6pt" number-columns-spanned="2">
									<fo:block text-align="left">
										Printed on
										<xsl:variable name="printdate"
											select="/root/modules/Daily/Daily/dayDate" />
										<xsl:value-of
											select="d2Utils:getSystemDate('dd MMM yyyy')" />
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" font-size="8pt">
					<xsl:choose>
						<xsl:when test="count(/root/modules/ReportDaily) &gt; 0">					
       						<xsl:apply-templates select="/root/modules/ReportDaily" mode="list"/>
       					</xsl:when>
       					<xsl:otherwise>
       						<fo:block><fo:inline color="white">.</fo:inline></fo:block>
       					</xsl:otherwise>
       				</xsl:choose>	
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

	<xsl:template match="modules/ReportDaily" mode="list">
		<xsl:variable name="currentwellid" select="ReportDaily/wellUid" />		
		<xsl:variable name="currentdailyid" select="ReportDaily/reportDailyUid" />
		<xsl:choose>
			<xsl:when test="position() &lt; last()" >
				<fo:block break-after="page">
					<xsl:apply-templates select="/root/modules/Well/Well[wellUid = $currentwellid]" mode="dailysummaryreport">
						<xsl:sort select="/root/modules/Well/Well[wellUid = $currentwellid]/wellName" />
						<xsl:with-param name="currentdailyid" select="$currentdailyid"/>
					</xsl:apply-templates>
				</fo:block>
			</xsl:when>
			<xsl:otherwise>
				<fo:block>
					<xsl:apply-templates select="/root/modules/Well/Well[wellUid = $currentwellid]" mode="dailysummaryreport">
						<xsl:sort select="/root/modules/Well/Well[wellUid = $currentwellid]/wellName" />
						<xsl:with-param name="currentdailyid" select="$currentdailyid"/>
					</xsl:apply-templates>
				</fo:block>
			</xsl:otherwise>
		</xsl:choose>		
	</xsl:template>
	
	<xsl:template match="modules/Well/Well" mode="dailysummaryreport">
		<xsl:param name="currentdailyid"/>
		<xsl:variable name="currentwellid" select="wellUid" />		
		<xsl:variable name="currentrig" select="/root/modules/Operation/Operation[wellUid=$currentwellid]/rigInformationUid" />
		<xsl:variable name="reportDaily" select="/root/modules/ReportDaily/ReportDaily[wellUid=$currentwellid]" />		 
		<xsl:variable name="current_drillcoid" select="/root/modules/RigInformation/RigInformation[rigInformationUid=$currentrig]/rigOwner/@lookupLabel" />
		<fo:table width="100%" table-layout="fixed" font-size="8pt" wrap-option="wrap">		
			<fo:table-column column-number="1" column-width="proportional-column-width(100)"/>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell font-size="12pt" font-weight="bold" text-align="center" padding="10px">
						<fo:block><xsl:value-of select="/root/modules/Well/Well[wellUid = $currentwellid]/wellName" /> Daily Summary</fo:block>
					</fo:table-cell>
				</fo:table-row>			
					
			</fo:table-header>				
			<fo:table-body font-size="8pt" font-weight="bold">
				<fo:table-row>
	               <fo:table-cell font-size="8pt">
	                  <fo:block font-weight="bold">
	                     STATUS 
	                      <xsl:choose>
				             <xsl:when test="string-length($reportDaily/geolReportEndtime) &gt; 3">@<fo:inline color="white">!</fo:inline></xsl:when>
				             <xsl:otherwise></xsl:otherwise>
				          </xsl:choose>
				             <xsl:value-of select="d2Utils:formatDateFromEpochMS($reportDaily/geolReportEndtime/@epochMS,'HH:mm')" />
				          <xsl:choose>
				             <xsl:when test="string-length($reportDaily/dynaAttr/daydate) &gt; 3"> on </xsl:when>
				             <xsl:otherwise></xsl:otherwise>
				          </xsl:choose>
				          <xsl:value-of select="d2Utils:formatDateFromEpochMS($reportDaily/dynaAttr/daydate/@epochMS,'dd MMM yyyy')"/> :
				          <xsl:value-of select="$reportDaily/reportCurrentStatus" />
	                  </fo:block>
	               </fo:table-cell>	               
	            </fo:table-row>			
				<fo:table-row>
					<fo:table-cell font-size="8pt" font-weight="bold" text-align="left">
						<fo:block>Report # <fo:inline color="white">!</fo:inline><xsl:value-of select="/root/modules/ReportDaily/ReportDaily/reportNumber" /></fo:block>
					</fo:table-cell>
				</fo:table-row>		
				<fo:table-row>
					<fo:table-cell font-size="8pt">						
					      <fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="0pt" space-before="6pt">
					         <fo:table-column column-number="1" column-width="proportional-column-width(30)" />
					         <fo:table-column column-number="2" column-width="proportional-column-width(70)" />
					         <fo:table-body>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">WELL NAME/TYPE:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/wellName"/> / <xsl:value-of select="/root/modules/Operation/Operation/typeIntent/@lookupLabel"/>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">TARGET/COMPLETION TYPE:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="/root/modules/Operation/Operation/typeTarget"/> / <xsl:value-of select="/root/modules/Operation/Operation/typeCompletions/@lookupLabel"/>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">CONTRACTOR (OPER):</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/opCo"/>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">BLOCK:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/block"/>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">LOCATION:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black">
					                  <fo:table inline-progression-dimension="13.3cm" table-layout="fixed" space-after="0pt" space-before="0pt">
					                     <fo:table-column column-number="1" column-width="proportional-column-width(25)" />
					                     <fo:table-column column-number="2" column-width="proportional-column-width(75)" />
					                     <fo:table-body>
					                        <fo:table-row>
					                           <fo:table-cell padding="0.1cm">
					                              <fo:block font-weight="bold">Geo. Coord.</fo:block>
					                           </fo:table-cell>
					                           <fo:table-cell padding="0.1cm">
					                              <fo:block>
					                                 Latitude: <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/latDeg"/>° <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/latMinute"/>' <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/latSecond"/>" <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/latNs"/>
					                              </fo:block>
					                              <fo:block>
					                                 Longtitude: <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/longDeg"/>° <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/longMinute"/>' <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/longSecond"/>" <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/longEw"/>
					                              </fo:block>
					                           </fo:table-cell>
					                        </fo:table-row>
					                        <fo:table-row>
					                           <fo:table-cell padding="0.1cm">
					                              <fo:block font-weight="bold">UTM Coord.</fo:block>
					                           </fo:table-cell>
					                           <fo:table-cell padding="0.1cm">
					                              <fo:block>N: <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/gridNs"/></fo:block>
						                          <fo:block>E: <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/gridEw"/></fo:block>
					                           </fo:table-cell>
					                        </fo:table-row>
					                     </fo:table-body>
					                  </fo:table>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">WATER DEPTH:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/waterDepth"/>
											<xsl:if test="string-length(normalize-space(/root/modules/Well/Well[wellUid=$currentwellid]/waterDepth)) &gt; 0">
												<fo:inline color="white">.</fo:inline>
												<xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/waterDepth/@uomSymbol"/>
											</xsl:if>	
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">NEAREST WELLS/FACILITIES:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/localreferencedescr"/>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">OBJECTIVES:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black">
					                  <fo:table inline-progression-dimension="13.3cm" table-layout="fixed" space-after="0pt" space-before="0pt">
					                     <fo:table-column column-number="1" column-width="proportional-column-width(25)" />
					                     <fo:table-column column-number="2" column-width="proportional-column-width(75)" />
					                     <fo:table-body>
					                        <fo:table-row>
					                           <fo:table-cell padding="0.1cm">
					                              <fo:block font-weight="bold">Primary</fo:block>
					                           </fo:table-cell>
					                           <fo:table-cell padding="0.1cm">
					                              <fo:block>
					                                 <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/wellObjective" />
					                              </fo:block>
					                           </fo:table-cell>
					                        </fo:table-row>
					                        <fo:table-row>
					                           <fo:table-cell padding="0.1cm">
					                              <fo:block font-weight="bold">Secondary</fo:block>
					                           </fo:table-cell>
					                           <fo:table-cell padding="0.1cm">
					                              <fo:block>
					                                 <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/wellObjectiveSecondary" />
					                              </fo:block>
					                           </fo:table-cell>
					                        </fo:table-row>
					                     </fo:table-body>
					                  </fo:table>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">PROPOSED TD:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					               		<xsl:choose>													
											<xsl:when test = "string(/root/modules/Operation/Operation/proposedtddescr)=''">										
											</xsl:when>													
											<xsl:otherwise>
												  <fo:block>
								                     <xsl:value-of select="/root/modules/Operation/Operation/proposedtddescr" /><fo:inline color="white">!</fo:inline>
								                     <xsl:value-of select="/root/modules/Operation/Operation/proposedtddescr/@uomSymbol"/>
								                  </fo:block>
											</xsl:otherwise>
										</xsl:choose>	
					                    <xsl:choose>													
											<xsl:when test = "string(/root/modules/Operation/dynaAttr/datumLabel)=''">										
											</xsl:when>													
											<xsl:otherwise>
												  <fo:block>
								                     <fo:inline font-weight="bold">RT: <fo:inline color="white">!</fo:inline></fo:inline><xsl:value-of select="/root/modules/Operation/dynaAttr/datumLabel"/><fo:inline color="white">!</fo:inline>
								                     <xsl:value-of select="/root/modules/Operation/dynaAttr/datumLabel/@uomSymbol"/>
								                  </fo:block>
											</xsl:otherwise>
										</xsl:choose>					                  
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">PRE-DRILL ESTIMATED RESOURCE:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="/root/modules/Operation/Operation[wellUid=$currentwellid]/predrillestimatedresources"/>					                     
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">STRUCTURE:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/tdstructuredescr"/>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">ESTIMATED COST:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                 <fo:block>
					                     <xsl:value-of select="/root/modules/Operation/Operation[wellUid=$currentwellid]/afe/@uomSymbol"/>
										<fo:inline color="white">.</fo:inline>
										<xsl:value-of select="/root/modules/Operation/Operation[wellUid=$currentwellid]/afe"/>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">CURRENT COST:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                    <xsl:value-of select="$reportDaily/daycost/@uomSymbol" /><fo:inline color="white">!</fo:inline>
										<xsl:value-of select="d2Utils:formatNumber($reportDaily/dynaAttr/cumcost,'##,###')"/>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">RIG/TYPE:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                  	<xsl:choose>													
											<xsl:when test = "string(/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel)=''">
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid"/>										
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/rigInformationUid/@lookupLabel"/>
											</xsl:otherwise>
										</xsl:choose>							
							 				/ 							 				
							 			<xsl:choose>													
											<xsl:when test = "string(/root/modules/RigInformation/RigInformation/rigSubType/@lookupLabel)=''">
												<xsl:value-of select="/root/modules/RigInformation/RigInformation/rigSubType"/>										
											</xsl:when>													
											<xsl:otherwise>
												<xsl:value-of select="/root/modules/RigInformation/RigInformation/rigSubType/@lookupLabel"/>
											</xsl:otherwise>
										</xsl:choose>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">PROGNOSED SECTION TD MD/TVD:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                  <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/prognosedSectionMdMsl"/>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/prognosedSectionMdMsl/@uomSymbol"/>
										 / 
										 <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/prognosedSectionTvdMsl"/>
										<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/prognosedSectionTvdMsl/@uomSymbol"/>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">ESTIMATED DAYS:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/projectedDays" />
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					        </fo:table-body>
					      </fo:table>
					      
					      <fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="0pt">
					         <fo:table-column column-number="1" column-width="proportional-column-width(25)" />
					         <fo:table-column column-number="2" column-width="proportional-column-width(25)" />
					         <fo:table-column column-number="3" column-width="proportional-column-width(25)" />
					         <fo:table-column column-number="4" column-width="proportional-column-width(25)" />
					         <fo:table-body> 
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">SPUD DATE:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border-right="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="d2Utils:formatDateFromEpochMS(/root/modules/Operation/Operation/spudDate/@epochMS,'dd MMM yyyy - HH:mm')"/>
					                  </fo:block>
					               </fo:table-cell>
					               <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">DAYS SINCE SPUD:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border-right="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                  <fo:block><xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysFromSpud,'d','')"/> 
					                  (Days on well: <xsl:value-of select="translate(/root/modules/ReportDaily/ReportDaily/dynaAttr/daysOnWell,'d','')"/>)
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>					            
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">CURRENT DEPTH (MD/TVD):</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border-right="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthMdMsl/@uomSymbol"/>	
										 	/ 
										 <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl"/><fo:inline color="white">.</fo:inline>
										 <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/depthTvdMsl/@uomSymbol"/>
					                  </fo:block>
					               </fo:table-cell>
					               <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">AVG R.O.P.:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border-right="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                  <fo:block>
					                     <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/geology24hrAvgrop"/><fo:inline color="white">.</fo:inline>
										 <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/geology24hrAvgrop/@uomSymbol"/>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row> 					               
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">CURRENT MUD WEIGHT:</fo:block>
					               </fo:table-cell>
					               <fo:table-cell border-right="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                  <fo:block>
						                  <xsl:value-of select="/root/modules/MudProperties/MudProperties[wellUid=$currentwellid]/mudWeight"/><fo:inline color="white">.</fo:inline>
											<xsl:value-of select="/root/modules/MudProperties/MudProperties/mudWeight/@uomSymbol"/>
											<xsl:if test="string-length(normalize-space(/root/modules/MudProperties/MudProperties[wellUid=$currentwellid]/mudWeight)) &gt; 0">
												<xsl:variable name="mudtype" select="/root/modules/MudProperties/MudProperties[wellUid=$currentwellid]/mudType"/>
												<xsl:choose>
													<xsl:when test="$mudtype='h2o'">
														<fo:inline> (WBM)</fo:inline>
													</xsl:when>
													<xsl:when test="$mudtype='sbm'">
														<fo:inline> (SBM)</fo:inline>
													</xsl:when>
													<xsl:when test="$mudtype='oil'">
														<fo:inline> (OBM)</fo:inline>
													</xsl:when>
												</xsl:choose>
											</xsl:if>
						               </fo:block>
					               </fo:table-cell>
					               
					               <xsl:choose>
					                  <xsl:when test="(/root/modules/ReportDaily/ReportDaily/lastLot)=0 and (/root/modules/ReportDaily/ReportDaily/lastFit)!=0">
					                     <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                        <fo:block font-weight="bold">LAST F.I.T.:</fo:block>
					                     </fo:table-cell>
					                     <fo:table-cell border-right="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">   
					                        <fo:block>
					                           <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit"/>
					                        </fo:block>
					                     </fo:table-cell>
					                  </xsl:when>
					                  <xsl:when test="(/root/modules/ReportDaily/ReportDaily/lastFit)=0 and (/root/modules/ReportDaily/ReportDaily/lastLot)!=0">
					                     <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                        <fo:block font-weight="bold">LAST L.O.T.:</fo:block>
					                     </fo:table-cell>
					                     <fo:table-cell border-right="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">   
					                        <fo:block>
					                           <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot"/>
					                        </fo:block>
					                     </fo:table-cell>
					                  </xsl:when>
					                  <xsl:when test="(/root/modules/ReportDaily/ReportDaily/lastFit)=0 and (/root/modules/ReportDaily/ReportDaily/lastLot)=0">
					                     <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                        <fo:block font-weight="bold">LAST F.I.T / L.O.T.:</fo:block>
					                     </fo:table-cell>
					                     <fo:table-cell border-right="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">   
					                        <fo:block>
					                           N/A
					                        </fo:block>
					                     </fo:table-cell>
					                  </xsl:when>
					                  <xsl:otherwise>
					                     <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">
					                        <fo:block font-weight="bold">LAST F.I.T / L.O.T.:</fo:block>
					                     </fo:table-cell>
					                     <fo:table-cell border-right="0.25px solid black" border-top="0.25px solid black" border-bottom="0.25px solid black" padding="0.1cm">   
					                        <fo:block>
					                           <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastFit" /> / <xsl:value-of select="/root/modules/ReportDaily/ReportDaily/lastLot" />
					                        </fo:block>
					                     </fo:table-cell>
					                  </xsl:otherwise> 
					               </xsl:choose>  
					            </fo:table-row>  
					         </fo:table-body>
					      </fo:table>
					     
					      <xsl:apply-templates select="/root/modules/CasingSection" /> 
					      <fo:table inline-progression-dimension="19cm" table-layout="fixed" space-after="0pt">
					         <fo:table-column column-number="1" column-width="proportional-column-width(100)" />
					         <fo:table-body>
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-right="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">SUMMARY OF OPERATION UP TO 0600 HRS:</fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.1cm">
										<fo:block linefeed-treatment="preserve">
											<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportPeriodSummary"/>					                     
										</fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-right="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">CURRENT OPERATIONAL STATUS:</fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.1cm">
										<fo:block text-align="left" linefeed-treatment="preserve">
											<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/reportCurrentStatus"/>						                 													
										</fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-right="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">WELL RESULTS:</fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.1cm">
					                  <fo:block linefeed-treatment="preserve">
					                     <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/wellresultdescr" />
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-right="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">NEXT OPERATIONS:</fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.1cm">
					                  <fo:block text-align="left" linefeed-treatment="preserve">
										<xsl:value-of select="/root/modules/ReportDailyDGR/ReportDaily/geolOps"/>	
									  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-top="0.25px solid black" border-right="0.25px solid black" padding="0.1cm">
					                  <fo:block font-weight="bold">REMARKS:</fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					            <fo:table-row>
					               <fo:table-cell border-left="0.25px solid black" border-bottom="0.25px solid black" border-right="0.25px solid black" padding="0.1cm">
					                  <fo:block linefeed-treatment="preserve">
					                     <xsl:value-of select="/root/modules/Well/Well[wellUid=$currentwellid]/remarks"/>
					                  </fo:block>
					               </fo:table-cell>
					            </fo:table-row>
					         </fo:table-body>
					      </fo:table> 
					    </fo:table-cell>
					  </fo:table-row>
					</fo:table-body>
				</fo:table>
				 
   </xsl:template>
   
      <xsl:template match="/root/modules/CasingSection">  
      <fo:table inline-progression-dimension="19cm" table-layout="fixed" border="0.5px solid black">
         <fo:table-column column-number="1" column-width="proportional-column-width(15)" />
         <fo:table-column column-number="2" column-width="proportional-column-width(25)" />
         <fo:table-column column-number="3" column-width="proportional-column-width(25)" />
         <fo:table-column column-number="4" column-width="proportional-column-width(35)" />
         <fo:table-header>
            <fo:table-row font-size="9pt">
               <fo:table-cell padding="0.075cm" border="0.25px solid black" font-weight="bold" number-columns-spanned="4" text-align="center">
                  <fo:block>Casing Run</fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row>
               <fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
                  <fo:block text-align="center">OD</fo:block>
               </fo:table-cell>
               <fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
                  <fo:block text-align="center">LOT / FIT</fo:block>
               </fo:table-cell>
               <fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
                  <fo:block text-align="center">Csg Shoe (MD/TVD)</fo:block>
               </fo:table-cell>
               <fo:table-cell border-bottom="0.25px solid black" padding="0.1cm">
                  <fo:block text-align="center">Remarks</fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-header>
         <fo:table-body width="3in">
            <xsl:apply-templates select="/root/modules/CasingSection/CasingSection" />
         </fo:table-body>
      </fo:table>
   </xsl:template>
   <xsl:template match="/root/modules/CasingSection/CasingSection">
      <fo:table-row>
         <fo:table-cell border-right="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
            <fo:block text-align="left">
               <xsl:value-of select="casingOd/@lookupLabel" />
            </fo:block>
         </fo:table-cell>
			<xsl:choose>
			  <xsl:when test="(lot)=0 and (fit)!=0">
			     <fo:table-cell border-right="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
					<fo:block text-align="center">
						F.I.T. - <xsl:value-of select="fit"/>
					</fo:block>
				 </fo:table-cell>
			  </xsl:when>
			  <xsl:when test="(fit)=0 and (lot)!=0">
			     <fo:table-cell border-right="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
					<fo:block text-align="center">
						L.O.T. - <xsl:value-of select="lot"/>
					</fo:block>
				 </fo:table-cell>
			  </xsl:when>
			  <xsl:when test="(fit)=0 and (lot)=0">
			     <fo:table-cell border-right="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
					<fo:block text-align="center">
						N/A
					</fo:block>
				 </fo:table-cell>
			  </xsl:when>
			  <xsl:otherwise>
			  	<fo:table-cell border-right="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
					<fo:block text-align="center">
						<xsl:value-of select="lot"/> / <xsl:value-of select="fit"/>
					</fo:block>
				 </fo:table-cell>
			  </xsl:otherwise>
			</xsl:choose>
         <fo:table-cell border-right="0.25px solid black" padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
            <fo:block text-align="center">
            <xsl:value-of select="shoeTopMdMsl"/> / <xsl:value-of select="shoeTopTvdMsl"/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell padding-before="0.1cm" padding-start="0.1cm" padding-end="0.1cm">
            <fo:block text-align="left">
               <xsl:value-of select="casingSummary"/>
            </fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
</xsl:stylesheet>
