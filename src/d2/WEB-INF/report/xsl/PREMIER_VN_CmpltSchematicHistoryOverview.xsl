<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:d2Utils="xalan://com.idsdatanet.d2.core.report.XalanUtils">
    <xsl:output method="xml"/>
    <xsl:preserve-space elements="*"/>
    <xsl:template match="/">
    <fo:root>
        <fo:layout-master-set>
			<fo:simple-page-master master-name="landscape" space-before="12pt" page-height="21cm" page-width="29cm" margin-left="1cm" margin-right="1cm">
				<fo:region-body margin-top="2.2cm" margin-bottom="2cm"/>
				<fo:region-before extent="2.2cm"/>
				<fo:region-after extent="2.2cm"/>  
			</fo:simple-page-master>
		</fo:layout-master-set>
        
        <fo:page-sequence master-reference="landscape" initial-page-number="1">
				
			<!-- REGION BEFORE - LOGO -->
			<fo:static-content flow-name="xsl-region-before">
				<fo:table inline-progression-dimension="27cm" table-layout="fixed" margin-top="0.5cm">
					<fo:table-column column-number="1" column-width="proportional-column-width(30)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(70)"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell font-size="12pt" text-align="left">
								<fo:block>
									<xsl:variable name="theOpco" select="/root/modules/Operation/Operation/opCo"/>
									<xsl:variable name="theOpcologo"><xsl:value-of select="/root/modules/Operation/Operation/LookupCompany/logoUrl"/></xsl:variable>
										<xsl:choose>
											<xsl:when test="$theOpcologo =''">
												<fo:external-graphic content-width="scale-to-fit" width="120pt" height="1.3cm" scaling="uniform" src="url(d2://images/client-logo.jpg)" />										
											</xsl:when>
											<xsl:otherwise>
												<fo:external-graphic content-width="scale-to-fit"  width="200pt" height="1.3cm" scaling="uniform" src="url(d2://{$theOpcologo})"/>
											</xsl:otherwise>
										</xsl:choose>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell font-size="12pt" text-align="right" display-align="center" >
								<fo:block font-weight="bold">
									Completion Schematic History Overview Report 
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row  >
							<fo:table-cell font-size="10pt" text-align="right" number-columns-spanned="2"  display-align="center" border-top="thin solid black" padding-top="2px" >
								<fo:block font-weight="bold">
									 <xsl:value-of select="/root/modules/Wellbore/Wellbore/wellboreName" />
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>    
			</fo:static-content>
			<!-- END OF REGION BEFORE - LOGO -->
			
			<!-- REGION AFTER - FOOTER -->
			<fo:static-content flow-name="xsl-region-after">
				<fo:table inline-progression-dimension="27cm" table-layout="fixed"
					space-after="12pt">
					<fo:table-column column-number="1" column-width="proportional-column-width(50)"/>
					<fo:table-column column-number="2" column-width="proportional-column-width(50)"/>
					<fo:table-body space-before="6pt" space-after="6pt">
						<fo:table-row>
							<fo:table-cell font-size="6pt">
								<fo:block text-align="left">
									Copyright IDS, 20140415, PREMIER_VN_CSHHISTORY.XSL
								</fo:block>
							</fo:table-cell>
							<fo:table-cell font-size="6pt">
								<fo:block text-align="right">
									Page <fo:page-number/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
	                        <fo:table-cell font-size="6pt" number-columns-spanned="2">
	                           <fo:block text-align="left">Printed on
	                           <xsl:variable name="printdate"
								select="/root/modules/Daily/Daily/dayDate" />
								<xsl:value-of select="d2Utils:getLocalTimestamp('dd MMM yyyy hh:mm a (z)')" />
	                           </fo:block>
	                        </fo:table-cell>
	                     </fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:static-content>
			<!-- END OF REGION AFTER - FOOTER -->
        	<fo:flow flow-name="xsl-region-body" font-size="8pt">
        		<fo:block>
					<xsl:apply-templates select="/root/modules/ProductionString"/>
        		</fo:block>
        	</fo:flow>
        </fo:page-sequence>
        </fo:root>
        </xsl:template>
		
		<xsl:template match="modules/ProductionString">
		
	        <fo:block keep-together="always">
	        
	            <fo:table inline-progression-dimension="27cm" table-layout="fixed" border="thin solid black" space-after="2pt" font-size="8pt" margin-top ="0.5cm">
	                <fo:table-column column-number="1" column-width="proportional-column-width(5)"/>
	                <fo:table-column column-number="2" column-width="proportional-column-width(15)"/>
	                <fo:table-column column-number="3" column-width="proportional-column-width(15)"/>
	                <fo:table-column column-number="4" column-width="proportional-column-width(7)"/>
	                <fo:table-column column-number="5" column-width="proportional-column-width(7)"/>
	                <fo:table-column column-number="6" column-width="proportional-column-width(7)"/>
	                <fo:table-column column-number="7" column-width="proportional-column-width(7)"/>
	                <fo:table-column column-number="8" column-width="proportional-column-width(7)"/>
	                <fo:table-column column-number="9" column-width="proportional-column-width(15)"/>
	                <fo:table-column column-number="10" column-width="proportional-column-width(15)"/>
	                
	               <!-- HEADER -->
					<fo:table-header>
					
						<!-- HEADER 1 -->
						<fo:table-row background-color="rgb(223,223,223)">
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									String No.
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									Production/Injection
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									String Description
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black" >
								<fo:block text-align="center">
									String Type
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black" >
								<fo:block text-align="center">
									Version #
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black" >
								<fo:block text-align="center">
									Updates/Changes
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black" >
								<fo:block text-align="center">
									Run Date
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									Pull Date
								</fo:block>
							</fo:table-cell>	
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									Comments
								</fo:block>
							</fo:table-cell>	
							<fo:table-cell padding="2px" border-bottom="thin solid black">
								<fo:block text-align="center">
									File Attachment
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					
					<!--  BODY -->
					<fo:table-body>
						<xsl:apply-templates select="ProductionString"/>
					</fo:table-body>
					
				</fo:table>
			
			</fo:block>
												
	</xsl:template>
	<!-- TEMPLATE: ProductionString -->
	<xsl:template match="ProductionString">

		<xsl:choose>
			<xsl:when test="position() mod 2 != '0'">
				<fo:table-row>
				
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left"> 
							<xsl:value-of select="stringNo"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							 <xsl:value-of select="productionType"/>
						 </fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							 <xsl:value-of select="description"/> 

						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							 <xsl:value-of select="stringType"/> 
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							<xsl:value-of select="version"/> 
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							<xsl:value-of select="eventUpdates"/> 
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							 <xsl:value-of select="d2Utils:formatDateFromEpochMS(installDateTime/@epochMS,'dd MMM yyyy HH:mm')"/> 
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							 <xsl:value-of select="d2Utils:formatDateFromEpochMS(uninstallDateTime/@epochMS,'dd MMM yyyy HH:mm')"/>
						</fo:block>
					</fo:table-cell>
								
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							 <xsl:value-of select="remark"/> 
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px">
						<fo:block text-align="left" linefeed-treatment="preserve">
							<xsl:value-of select="dynaAttr/fileName"/>
						</fo:block>
					</fo:table-cell>
					
				</fo:table-row>
			</xsl:when>
			<xsl:otherwise>
				<fo:table-row background-color="#EEEEEE">
				
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left"> 
							<xsl:value-of select="stringNo"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							 <xsl:value-of select="productionType"/>
						 </fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							 <xsl:value-of select="description"/> 

						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							 <xsl:value-of select="stringType"/> 
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							<xsl:value-of select="version"/> 
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							<xsl:value-of select="eventUpdates"/> 
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							 <xsl:value-of select="d2Utils:formatDateFromEpochMS(installDateTime/@epochMS,'dd MMM yyyy HH:mm')"/>
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="center">
							 <xsl:value-of select="d2Utils:formatDateFromEpochMS(uninstallDateTime/@epochMS,'dd MMM yyyy HH:mm')"/> 
						</fo:block>
					</fo:table-cell>
								
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" border-right="thin solid black">
						<fo:block text-align="left">
							 <xsl:value-of select="remark"/> 
						</fo:block>
					</fo:table-cell>
					
					<fo:table-cell padding="1px" padding-left="2px" padding-right="2px" >
						<fo:block text-align="left" linefeed-treatment="preserve">
							 <xsl:value-of select="dynaAttr/fileName"/>
						</fo:block>
					</fo:table-cell>
					
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
</xsl:stylesheet>
