package com.idsdatanet.d2.ftr.customCode;


import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.CustomCode;
import com.idsdatanet.d2.core.model.CustomCodeGroup;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CustomCodeDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {

	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	public static final String INPUTONLY_PARAMETER = "inputonly";
	
	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		String selectedCustomCodeGroup = (String) commandBean.getRoot().getDynaAttr().get("customCodeGroupFilter");
		
		Object object = node.getData();
		if (object instanceof CustomCodeGroup) {
			CustomCodeGroup data = (CustomCodeGroup) object;
			data.setCustomCodeGroupUid(selectedCustomCodeGroup);
		}
		if (object instanceof CustomCode) {
			CustomCode data = (CustomCode) object;
			data.setParamType(INPUTONLY_PARAMETER);
		}
	}

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		
		if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
			String selectedCustomCodeGroup = (String) commandBean.getRoot().getDynaAttr().get("customCodeGroupFilter");
			if (StringUtils.isNotBlank(selectedCustomCodeGroup)){
				customCondition+=(customCondition.length()>0?" AND ":"")+" customCodeGroupUid =:customCodeGroupUid ";
				query.addParam("customCodeGroupUid", selectedCustomCodeGroup);
			}
			else
			{
				customCondition = " 1 = 1";
			}
		}
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


}
