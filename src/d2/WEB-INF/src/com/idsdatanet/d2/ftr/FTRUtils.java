package com.idsdatanet.d2.ftr;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.type.Type;

import com.idsdatanet.d2.common.locking.RevisionUtils;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityDescriptionMatrix;
import com.idsdatanet.d2.core.model.CustomCode;
import com.idsdatanet.d2.core.model.CustomCodeLink;
import com.idsdatanet.d2.core.model.CustomDescriptionMatrix;
import com.idsdatanet.d2.core.model.CustomFtrLink;
import com.idsdatanet.d2.core.model.CustomParameterDetail;
import com.idsdatanet.d2.core.model.CustomParameterLink;
import com.idsdatanet.d2.core.model.ModuleParameterDetail;
import com.idsdatanet.d2.core.model.ModuleParameterLink;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.UOMMapping;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.infra.uom.OperationUomContext;

/**
 * ActivityUtil to populate BHA run number based on activity depth.
 * Method to get parent wellbore operation based on current selected wellbore.
 * @author Moses, Jackson
 *
 */
public class FTRUtils {
	public static final String ACTIVTIY_REMARK_HASH_PARAMETER = "hashparameter";
	public static final String ACTIVTIY_REMARK_CLIENT_PARAMETER = "clientdefined";
	public static final String ACTIVTIY_REMARK_HOLEDEPTH_PARAMETER = "holedepth";
	public static final String ACTIVTIY_REMARK_STRINGDEPTHFROM_PARAMETER = "stringdepthfrom";
	public static final String ACTIVTIY_REMARK_STRINGDEPTHTO_PARAMETER = "stringdepthto";
	public static final String ACTIVTIY_REMARK_INPUTONLY_PARAMETER = "inputonly";
	/**=========generic D2.5 client FTR=========**/
	
	/**
	 * return  Map <String key - [TableName.FieldName],Object value - [Parameter Value]>
	 * @param commandBean
	 * @param activityUid
	 * @param sourceActivityUid
	 * @param useKey
	 * @param showAllValue
	 * @param showFormattedValue
	 * @param OperationUomContext
	 * @throws Exception 
	 */
	public static Map<String,Object> getActivityRemarkCustomParameterValues(CommandBean commandBean,String activityUid,String sourceActivityUid,String useKey,Boolean ShowAllValue,Boolean ShowFormattedValue, OperationUomContext opUomContext) throws Exception{
		
		String queryString = "from CustomParameterLink where (isDeleted is null or isDeleted = false) and activityUid = :activityUid";
		
		List<CustomParameterLink> paramList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
		if (paramList.size() >0){
			
			Map<String,Object>resultMap = new HashMap<String,Object>();
			
			String keyField = "";
			String targetFieldName="";
			String parameterKey = "";
			Object data=null;
			CustomFieldUom customFieldUom = null;
			String tableName = "";
			String fieldName = "";
			String parameterType = "";
			Class<?> hibernateClass = null;
			
			for(CustomParameterLink customParameterLink : paramList){
				
				queryString = "from CustomParameterDetail where (isDeleted is null or isDeleted = false) and customParameterDetailUid = :customParameterDetailUid";
				List<CustomParameterDetail> customParameterDetailList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "customParameterDetailUid", customParameterLink.getCustomParameterDetailUid());
				
				if(customParameterDetailList.size()>0){
					CustomParameterDetail customParameterDetail = customParameterDetailList.get(0);
					targetFieldName="";
					data=null;
					
					if (customParameterDetail.getTargetFieldName() != null) targetFieldName = customParameterDetail.getTargetFieldName();
					if (customParameterDetail.getParameterKey() != null) parameterKey = customParameterDetail.getParameterKey();
					if (customParameterLink.getParameterValue() != null) data = customParameterLink.getParameterValue();
					if (customParameterDetail.getParamType() != null) parameterType =customParameterDetail.getParamType() ;
					
					if ("targetedFieldName".equalsIgnoreCase(useKey)){
						keyField = targetFieldName;
					}else{
						keyField = customParameterLink.getCustomParameterDetailUid();
					}					
					
					if (ShowAllValue || (ACTIVTIY_REMARK_HASH_PARAMETER.equalsIgnoreCase(parameterType)||ACTIVTIY_REMARK_CLIENT_PARAMETER.equalsIgnoreCase(parameterType)||
						ACTIVTIY_REMARK_HOLEDEPTH_PARAMETER.equalsIgnoreCase(parameterType)||ACTIVTIY_REMARK_STRINGDEPTHFROM_PARAMETER.equalsIgnoreCase(parameterType)||
						ACTIVTIY_REMARK_STRINGDEPTHTO_PARAMETER.equalsIgnoreCase(parameterType))){
						tableName = "";
						fieldName = "";
						hibernateClass = null;
						String[] fieldToken = targetFieldName.split("[.]");
						if (fieldToken.length==2){
							tableName = fieldToken[0];
							fieldName = fieldToken[1];
						}
						
						if (StringUtils.isNotBlank(tableName)) {
							String tableClassName = ApplicationUtils.getConfiguredInstance().getTablePackage() + CommonUtils.beanCapitalize(tableName);
							hibernateClass = Class.forName(tableClassName);
						}				
						
						if(opUomContext!=null){
							customFieldUom = new CustomFieldUom(commandBean, hibernateClass, fieldName, opUomContext);	
							if(customFieldUom.isUOMMappingAvailable()){
								Double dblValue= null;
								if (data!=null){
									UOMMapping  uomMapping = customFieldUom.getUOMMapping();
									
									dblValue = Double.parseDouble(data.toString());
									customFieldUom.setBaseValue(dblValue);									
									dblValue = customFieldUom.getConvertedValue(uomMapping.isDatumConversion());
									
									if(ShowFormattedValue){
										customFieldUom.setUseGroupingDataFields(false);
										resultMap.put(keyField,customFieldUom.formatOutputPrecision(dblValue));
									}else{
										resultMap.put(keyField,dblValue);
									}
									
								}else{
									resultMap.put(keyField,null);
								}										
							}else{
								resultMap.put(keyField,data);
							}							
						}else{
							resultMap.put(keyField,data);
						}	
					}			
				}				
			}	
			
			return resultMap;
		} else {
			if (StringUtils.isNotBlank(sourceActivityUid)) {
				return getActivityRemarkCustomParameterValues(commandBean, sourceActivityUid, null, useKey, ShowAllValue, ShowFormattedValue, opUomContext);
			}
		}
		return null;
	}
	
	/**
	 * Method to get query result for Activity Description Matrix
	 * @param onOffShore
	 * @param operationType
	 * @param clsCode
	 * @param phsCode
	 * @param jobCode
	 * @param tskCode
	 * @param codeList
	 * @return remarkList
	 */
	public static List<ActivityDescriptionMatrix> formQueryForADM(String onOffShore, String operationType, String clsCode, String phsCode, String jobCode, String tskCode, String userCode, List<String> codeList) throws Exception {
		List<String> paramsFields = new ArrayList<String>();
		List<Object> paramsValues = new ArrayList<Object>();
		String query = "from ActivityDescriptionMatrix where (isDeleted = false or isDeleted is null) AND (customCodeGroupUid is null or customCodeGroupUid='')";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().find("Select customCodeGroupUid FROM CustomCodeGroup WHERE (isDeleted=FALSE OR isDeleted is null) AND (isActive = true or isActive is null)");
		List customCodeList = new ArrayList();
		if(list.size()>0 && list!=null)
		{
			for(Object rec:list)
			{
				customCodeList.add(rec.toString());
			}
			query = "from ActivityDescriptionMatrix where (isDeleted = false or isDeleted is null) AND (customCodeGroupUid in (:customCodeList) or customCodeGroupUid is null or customCodeGroupUid='')";
			paramsFields.add("customCodeList");
			paramsValues.add(customCodeList);
		}
		
		if (StringUtils.isNotBlank(onOffShore)) {
			query += " AND (onOffShore = :onOffShore OR (onOffShore = '' or onOffShore is null)) ";
			paramsFields.add("onOffShore");
			paramsValues.add(onOffShore);
		} else {
			query += " AND (onOffShore = '' or onOffShore is null)";
		}
		
		if (StringUtils.isNotBlank(operationType)) {
			query += " AND (operationCode = :operationCode OR (operationCode = '' or operationCode is null))";
			paramsFields.add("operationCode");
			paramsValues.add(operationType);
		} else {
			query += " AND (operationCode = '' or operationCode is null)";
		}
		
		if (StringUtils.isNotBlank(clsCode) && (codeList == null || (codeList != null && codeList.isEmpty()))) {
			query += " AND ((classShortCode = :classShortCode) OR (classShortCode = '' or classShortCode is null))";
			paramsFields.add("classShortCode");
			paramsValues.add(clsCode);
		} else if (StringUtils.isNotBlank(clsCode) && codeList != null && codeList.contains("classCode")) {
			//this is from first version, where exact match is used. second version necessitate null or empty as match all, so this is phased out.
			query += " AND (classShortCode = :classShortCode)";
			paramsFields.add("classShortCode");
			paramsValues.add(clsCode);
		} else {
			query += " AND (classShortCode = '' or classShortCode is null)";
		}
		
		if (StringUtils.isNotBlank(phsCode) && (codeList == null || (codeList != null && codeList.isEmpty()))) {
			query += " AND ((phaseShortCode = :phaseShortCode) OR (phaseShortCode = '' or phaseShortCode is null))";
			paramsFields.add("phaseShortCode");
			paramsValues.add(phsCode);
		} else if (StringUtils.isNotBlank(phsCode) && codeList != null && codeList.contains("phaseCode")) {
			//this is from first version, where exact match is used. second version necessitate null or empty as match all, so this is phased out.
			query += " AND (phaseShortCode = :phaseShortCode)";
			paramsFields.add("phaseShortCode");
			paramsValues.add(phsCode);
		} else {
			query += " AND (phaseShortCode = '' or phaseShortCode is null)";
		}
		
		if (StringUtils.isNotBlank(jobCode) && (codeList == null || (codeList != null && codeList.isEmpty()))) {
			query += " AND ((jobTypeShortCode = :jobTypeShortCode) OR (jobTypeShortCode = '' or jobTypeShortCode is null))";
			paramsFields.add("jobTypeShortCode");
			paramsValues.add(jobCode);
		} else if (StringUtils.isNotBlank(jobCode) && codeList != null && codeList.contains("jobTypeCode")) {
			//this is from first version, where exact match is used. second version necessitate null or empty as match all, so this is phased out.
			query += " AND (jobTypeShortCode = :jobTypeShortCode)";
			paramsFields.add("jobTypeShortCode");
			paramsValues.add(jobCode);
		} else {
			query += " AND (jobTypeShortCode = '' or jobTypeShortCode is null)";
		}
		
		if (StringUtils.isNotBlank(tskCode) && (codeList == null || (codeList != null && codeList.isEmpty()))) {
			query += " AND ((taskShortCode = :taskShortCode) OR (taskShortCode = '' or taskShortCode is null))";
			paramsFields.add("taskShortCode");
			paramsValues.add(tskCode);
		} else if (StringUtils.isNotBlank(tskCode) && codeList != null && codeList.contains("taskCode")) {
			//this is from first version, where exact match is used. second version necessitate null or empty as match all, so this is phased out.
			query += " AND (taskShortCode = :taskShortCode)";
			paramsFields.add("taskShortCode");
			paramsValues.add(tskCode);
		} else {
			query += " AND (taskShortCode = '' or taskShortCode is null)";
		}
		
		if (StringUtils.isNotBlank(userCode) && (codeList == null || (codeList != null && codeList.isEmpty()))) {
			query += " AND ((userShortCode = :userShortCode) OR (userShortCode = '' or userShortCode is null))";
			paramsFields.add("userShortCode");
			paramsValues.add(userCode);
		} else if (StringUtils.isNotBlank(userCode) && codeList != null && codeList.contains("userCode")) {
			//this is from first version, where exact match is used. second version necessitate null or empty as match all, so this is phased out.
			query += " AND (userShortCode = :userShortCode)";
			paramsFields.add("userShortCode");
			paramsValues.add(userCode);
		} else {
			query += " AND (userShortCode = '' or userShortCode is null)";
		}
		
		//first version, sort by condition desc
		//query += " ORDER BY condition DESC";
		
		//second version, sort by the codes (in the following order - onOffShore, operationCode, classShortCode, phaseShortCode, jobTypeShortCode, taskShortCode). DESC is to push null to the bottom
		query += " ORDER BY onOffShore DESC, operationCode DESC, classShortCode DESC, phaseShortCode DESC, jobTypeShortCode DESC, taskShortCode DESC, userShortCode DESC";
		
		List<ActivityDescriptionMatrix> remarkList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramsFields, paramsValues);
		return remarkList;
	}
	
	/**
	 * Method to get query result for Custom Description Matrix
	 * @param customDescMatrixUid
	 * @return remarkList
	 */
	public static List<CustomDescriptionMatrix> queryCDM(String customDescMatrixUid) throws Exception {
		List<String> paramsFields = new ArrayList<String>();
		List<Object> paramsValues = new ArrayList<Object>();
			
			String query = "from CustomDescriptionMatrix where (isDeleted = false or isDeleted is null) AND customDescriptionMatrixUid = :customDescMatrixUid";
			paramsFields.add("customDescMatrixUid");
			paramsValues.add(customDescMatrixUid);
			
			List<CustomDescriptionMatrix> remarkList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramsFields, paramsValues);
		
		return remarkList;
	}
	
	/**
	 * Method to get query result for Activity Description Matrix
	 * @param customDescMatrixUid
	 * @return remarkList
	 */
	public static List<ActivityDescriptionMatrix> queryADM(String activityDescMatrixUid) throws Exception {
		List<String> paramsFields = new ArrayList<String>();
		List<Object> paramsValues = new ArrayList<Object>();
			
			String query = "from ActivityDescriptionMatrix where (isDeleted = false or isDeleted is null) AND activityDescriptionMatrixUid = :activityDescMatrixUid";
			paramsFields.add("activityDescMatrixUid");
			paramsValues.add(activityDescMatrixUid);
			
			List<ActivityDescriptionMatrix> remarkList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramsFields, paramsValues);
		
		return remarkList;
	}
	
	/**
	 * Method to check if it is allowed to show Fixed Test Remark button 
	 * @param node
	 * @param request
	 */
	public static void checkAllowFTRButtonShow(CommandBeanTreeNode node, String onOffshoreRecord, String operationCode) throws Exception{
		Object object = node.getData();
		String clsCode = "";
		String phsCode = "";
		String jobCode = "";
		String tskCode = "";
		String userCode = "";
		Boolean override = false;
		Boolean rsd = false;
		String cdmUid= null;
		
		if(object instanceof Activity){
			Activity activity = (Activity) object;
			
			clsCode = activity.getClassCode();
			phsCode = activity.getPhaseCode();
			jobCode = activity.getJobTypeCode();
			tskCode = activity.getTaskCode();
			userCode = activity.getUserCode();
			node.getDynaAttr().put("FTRRecord", activity.getAdditionalRemarks());
			if(StringUtils.isBlank(activity.getAdditionalRemarks()) || StringUtils.isEmpty(activity.getAdditionalRemarks()))
			{
				node.getDynaAttr().put("FTRNoValue", "1");
			}
			
			if(activity.getCustomDescriptionMatrixUid()!=null){
				override = true;
				cdmUid = activity.getCustomDescriptionMatrixUid();
				CustomDescriptionMatrix cdm = (CustomDescriptionMatrix) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(CustomDescriptionMatrix.class, activity.getCustomDescriptionMatrixUid());
				if (cdm!=null && cdm.getForRsd()!=null){
					rsd = cdm.getForRsd();
				}
			}
		}
		
		if(object instanceof NextDayActivity){
			NextDayActivity nextDayActivity = (NextDayActivity) object;
			
			clsCode = nextDayActivity.getClassCode();
			phsCode = nextDayActivity.getPhaseCode();
			jobCode = nextDayActivity.getJobTypeCode();
			tskCode = nextDayActivity.getTaskCode();
			userCode = nextDayActivity.getUserCode();
			node.getDynaAttr().put("FTRRecord", nextDayActivity.getAdditionalRemarks());
			if(StringUtils.isBlank(nextDayActivity.getAdditionalRemarks()) || StringUtils.isEmpty(nextDayActivity.getAdditionalRemarks()))
			{
				node.getDynaAttr().put("FTRNoValue", "1");
			}
			
		}
		
		 node.getDynaAttr().put("showNoFtrSetup", null);
		 node.getDynaAttr().put("showReloadFTRButton", null);
		    
		String onOffShore = onOffshoreRecord;
		String operationType = operationCode;
		
		Object commandBean = node.getCommandBean();
		List<String> codeList = null;
		
		if(commandBean instanceof FTRCommandBean){
			FTRCommandBean activityFTRCommandBean = (FTRCommandBean) commandBean;
			codeList = activityFTRCommandBean.getCodeList();
		}
		
		List<ActivityDescriptionMatrix> remarkList = FTRUtils.formQueryForADM(onOffShore, operationType, clsCode, phsCode, jobCode, tskCode, userCode, codeList);
		String customDescriptionMatrixUid = null;
		
		if(remarkList != null && remarkList.size()>0){
			ActivityDescriptionMatrix activityDescriptionMatrix = remarkList.get(0);
			
			if (activityDescriptionMatrix!=null){
				String queryCustom = "FROM CustomDescriptionMatrix WHERE activityDescriptionMatrixUid = :activityDescriptionMatrixUid AND (isDeleted = false or isDeleted is null)";
				List<CustomDescriptionMatrix> aList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryCustom, "activityDescriptionMatrixUid", activityDescriptionMatrix.getActivityDescriptionMatrixUid());
				
				if (aList != null && aList.size() > 0) {
					CustomDescriptionMatrix customDescriptionMatrix = (CustomDescriptionMatrix) aList.get(0);
					if (customDescriptionMatrix!=null){
						customDescriptionMatrixUid = customDescriptionMatrix.getCustomDescriptionMatrixUid();
					}
				}
			}
			
			node.getDynaAttr().put("showFTRButton","1");
			override = true;
			node.getDynaAttr().put("showReloadFTRButton", "0");
		}
		else{
			node.getDynaAttr().put("showFTRButton","0");
			override = false;
			if (rsd){
				override = true;
			}
			if ((node.getDynaAttr().get("importTemplate") != null) && ("1".equals(node.getDynaAttr().get("importTemplate").toString()))) {
		        node.getDynaAttr().put("showNoFtrSetup", "1");
		      } else {
		        node.getDynaAttr().put("showNoFtrSetup", "0");
		      }
		}
		
		if ((node.getDynaAttr().get("showReloadFTRButton") != null) && ("1".equals(node.getDynaAttr().get("showReloadFTRButton").toString()))) {
		      node.getDynaAttr().put("showNoFtrSetup", "0");
		}
		
		if(override){
			node.getDynaAttr().put("showFTRButton","1");
		}
		
		node.getDynaAttr().put("customDescriptionMatrixUid", customDescriptionMatrixUid);
		if (customDescriptionMatrixUid==null && !override){
			node.getDynaAttr().put("showFTRButton","0");
		}
		if (customDescriptionMatrixUid==null && override){
			node.getDynaAttr().put("customDescriptionMatrixUid", cdmUid);
		}
		
	}
	
	public static String getCustomParameterDetailFieldType(String customParameterDetailUid,String key) throws Exception{
		
		String queryString = "Select targetFieldName from CustomParameterDetail where (isDeleted is null or isDeleted = FALSE) and customParameterDetailUid = :customParameterDetailUid and parameterKey = :parameterKey";
		
		List<Object> paramList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"customParameterDetailUid","parameterKey"},new Object[]{customParameterDetailUid,key});
		if (paramList.size() >0){
			
			if (paramList.get(0) !=null){
				String targetFieldName = paramList.get(0).toString();
				
				String[] fieldToken = targetFieldName.split("[.]");
				if (fieldToken.length==2){
					String tableName = fieldToken[0];
					String fieldName = fieldToken[1];
					
					if (StringUtils.isNotBlank(tableName)) {
						String tableClassName = ApplicationUtils.getConfiguredInstance().getTablePackage() + CommonUtils.beanCapitalize(tableName);
						Class<?> hibernateClass = Class.forName(tableClassName);
						
						Type dataType= ApplicationUtils.getConfiguredInstance().getFieldTypeFromTable(hibernateClass, fieldName);
						String typeName = dataType.getReturnedClass().getName();
						if ( (typeName.equalsIgnoreCase("java.lang.double"))|| (typeName.equalsIgnoreCase("java.lang.float"))){
							return "double";
						}else if ((typeName.equalsIgnoreCase("java.lang.integer"))|| (typeName.equalsIgnoreCase("java.lang.long"))){
							return "integer";
						}else if ((typeName.equalsIgnoreCase("java.util.date"))) {
							return "date";
						}
						
					}	
				}			
			}
		}	
		
		return "string"; 
	}
	
	public static void deleteFTRLinkRecord(String activityUid) throws Exception{
    	String queryString = "from CustomParameterLink where (isDeleted is null or isDeleted =0) and activityUid = :activityUid";
		List<CustomParameterLink> customParameterLinkLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
		if(customParameterLinkLst!=null && customParameterLinkLst.size()>0)
		{
			for (CustomParameterLink customParameterLink : customParameterLinkLst){	
				customParameterLink.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(customParameterLink);
			}
		}
		
		/*queryString = "from ModuleParameterLink where (isDeleted is null or isDeleted =0) and activity_uid = :activityUid";
		List<ModuleParameterLink> ModuleParameterLinkLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
		if(ModuleParameterLinkLst!=null && ModuleParameterLinkLst.size()>0)
		{
			for (ModuleParameterLink moduleParameterLink : ModuleParameterLinkLst){	
				moduleParameterLink.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(moduleParameterLink);
			}
		}*/
		
		queryString = "from CustomCodeLink where (isDeleted is null or isDeleted =0) and activity_uid = :activityUid";
		List<CustomCodeLink> CustomCodeLinkLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
		if(CustomCodeLinkLst!=null && CustomCodeLinkLst.size()>0)
		{
			for (CustomCodeLink customCodeLink : CustomCodeLinkLst){	
				customCodeLink.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(customCodeLink);
			}
		}
		
	}
	
	public static void deleteCustomFtrLink(String activityUid) throws Exception{
		String queryString = "from CustomFtrLink where (isDeleted is null or isDeleted = FALSE) and activityUid = :activityUid";
		List<CustomFtrLink> customFtrLinkList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
		if (customFtrLinkList != null && customFtrLinkList.size() > 0) {
			for (CustomFtrLink customFtrLink : customFtrLinkList) {
				customFtrLink.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(customFtrLink);
			}
		}
	}
	
	public static void autoRemoveTargetCustomModuleByActivityUid(UserSelectionSnapshot selection,String activityUid) throws Exception{
		if (StringUtils.isNotBlank(activityUid)){		
			
			List<String> tableList = new ArrayList<String>();
			
			//set auto created record's activityUid to null
			String queryString = "select B.targetFieldName from CustomParameterLink A,CustomParameterDetail B where A.customParameterDetailUid= B.customParameterDetailUid and " +
			                     "(A.isDeleted is null or A.isDeleted = 0) and (B.isDeleted is null or B.isDeleted = 0) and A.activityUid = :activityUid and B.paramType NOT IN ('inputonly', 'Dimension', 'Number Metric', 'Ratio Metric')";
			List<Object[]> targetFieldNameLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
			
			if (targetFieldNameLst.size()>0){
				
				String targetFieldName = "";
				String tableName = "";
				
				for (Object item : targetFieldNameLst){					
					if (item!=null){									
						targetFieldName = item.toString();
						String[] fieldToken = targetFieldName.split("[.]");
						if (fieldToken.length==2){
							// ignore second index - field
							tableName = fieldToken[0];
							if (StringUtils.isNotBlank(tableName)) {
								if (!("perforationgunconfiguration".equalsIgnoreCase(tableName) || "targetzone".equalsIgnoreCase(tableName) || "stimulationpump".equalsIgnoreCase(tableName) || "bharundailysummary".equalsIgnoreCase(tableName))) {
									if (!tableList.contains(tableName)){
										tableList.add(tableName);
										
										queryString = "from " + tableName + " where (isDeleted is null or isDeleted=0) and activityUid = :activityUid";
										List<Object> objectlst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
										if (objectlst.size()>0){
											Object object =  objectlst.get(0);
											
											//Boolean isSubmit = (Boolean) PropertyUtils.getProperty(object, "isSubmit");
											//if(isSubmit!=null && isSubmit) RevisionUtils.unSubmitRecordWithStaticRevisionLog(selection, object, "Auto update from time codes");
											
											if (tableName.toLowerCase().startsWith("ftr")) {
												PropertyUtils.setProperty(object, "isDeleted", true);
											} else {
												PropertyUtils.setProperty(object, "activityUid",null);	
											}
											ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(object);
										}
									}
								}
							}
						}							
					}				
				}	
			}
			//set is_deleted for each customParameterLink record to true
			queryString = "from CustomParameterLink where (isDeleted is null or isDeleted =0) and activity_uid = :activityUid";
			List<CustomParameterLink> customParameterLinkLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
			for (CustomParameterLink customParameterLink : customParameterLinkLst){	
				customParameterLink.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(customParameterLink);
			}
			
		}
	}

	public static int hasActivityCustomDescription(String activityUid,String customParameterDetailUid) throws Exception{
		String queryString = "Select B.customDescriptionMatrixUid from ActivityDescriptionMatrix A, CustomDescriptionMatrix B, CustomParameterDetail C, CustomParameterLink D where (A.isDeleted is null or A.isDeleted = FALSE) " + 
	                     "and (B.isDeleted is null or B.isDeleted = FALSE) and (C.isDeleted is null or C.isDeleted = FALSE) and (D.isDeleted is null or D.isDeleted = FALSE) and A.activityDescriptionMatrixUid=B.activityDescriptionMatrixUid " +
	                     "and B.customDescriptionMatrixUid=C.customDescriptionMatrixUid and C.customParameterDetailUid=D.customParameterDetailUid and D.activityUid = :activityUid group by B.customDescriptionMatrixUid";
		List<Object> idList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
		if (idList.size()>0){
			if(idList.get(0)!=null){
				String customDescriptionMatrixUid = idList.get(0).toString();
				
				queryString = "Select customParameterDetailUid from CustomParameterDetail where (isDeleted is null or isDeleted = 0) " + 
	            			  "and customDescriptionMatrixUid = :customDescriptionMatrixUid and customParameterDetailUid = :customParameterDetailUid ";
				idList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString,new String[]{"customDescriptionMatrixUid","customParameterDetailUid"},new Object[]{customDescriptionMatrixUid,customParameterDetailUid});
				if(idList.size()>0) 
					return 1;
				else
					return -1;
			}
		}		
		return 0;
	}
	
	/**
	 * add Field based on special parameter type
	 * @param tableMap
	 * @param specialParameterType
	 * @param parameterValue
	 * @throws Exception 
	 */
	public static void addFieldBasedOnSpecialParameterType(CustomFieldUom thisConverter,HashMap<String,Map> tableMap,String specialParameterType,Object baseValue) throws Exception {
		
		String tableName="Activity";
		String fieldName="";

		if (ACTIVTIY_REMARK_HOLEDEPTH_PARAMETER.equalsIgnoreCase(specialParameterType)){
			fieldName = "depthFromMdMsl";
		}else if (ACTIVTIY_REMARK_STRINGDEPTHFROM_PARAMETER.equalsIgnoreCase(specialParameterType)){
			fieldName = "stringFromDepthMdMsl";
		}else if (ACTIVTIY_REMARK_STRINGDEPTHTO_PARAMETER.equalsIgnoreCase(specialParameterType)){
			fieldName = "stringToDepthMdMsl";
		}
		Object parameterValue = baseValue;
		
		if(!fieldName.equals("")){
			
			thisConverter.setReferenceMappingField(Activity.class, fieldName);
			if(thisConverter.isUOMMappingAvailable()){
				UOMMapping  uomMapping = thisConverter.getUOMMapping();
				thisConverter.setBaseValue(Double.parseDouble(baseValue.toString()));
				parameterValue = thisConverter.getConvertedValue(uomMapping.isDatumConversion());
			}	
			
			Map<String,Object> fieldmap = null;
			if (tableMap.containsKey(tableName.toLowerCase())){
				fieldmap = tableMap.get(tableName.toLowerCase());
				fieldmap.put(fieldName, parameterValue);
			}else{
				fieldmap = new HashMap<String,Object>();
				fieldmap.put(fieldName, parameterValue);
				tableMap.put(tableName.toLowerCase(), fieldmap);
			}	
		}		
	}
	
	/**
	 * Method to update activity from activity free text remarks
	 * @param thisActivity
	 * @param fieldsMap
	 * @throws Exception
	 */
	public static void updateActivityBasedOnActivityRemarks(Activity thisActivity, Map<String,Object>fieldsMap) throws Exception{
		if (fieldsMap !=null){			
			 
			if (thisActivity!=null ){
				for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
					if (StringUtils.isNotBlank(fieldEntry.getKey())) PropertyUtils.setProperty(thisActivity, fieldEntry.getKey(), fieldEntry.getValue());
				}	
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisActivity);
			}
		}		
	}
	
/**=========custom client FTR: ndor=========**/
	
	/**
	 * FOR NOBLE - return - void
	 * @param activityUid
	 * @throws Exception 
	 */
	public static void autoRemoveTargetModuleByActivityUid(UserSelectionSnapshot selection,String activityUid) throws Exception{
		
		if (StringUtils.isNotBlank(activityUid)){		
			
			List<String> tableList = new ArrayList<String>();
			
			//set auto created record's activityUid to null
			String queryString = "select B.targetFieldName from ModuleParameterLink A,ModuleParameterDetail B where A.moduleParameterDetailUid= B.moduleParameterDetailUid and " +
			                     "(A.isDeleted is null or A.isDeleted =0) and (B.isDeleted is null or B.isDeleted =0) and A.activityUid = :activityUid and B.paramType <>'inputonly'";
			List<Object[]> targetFieldNameLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
			
			if (targetFieldNameLst.size()>0){
				
				String targetFieldName = "";
				String tableName = "";
				
				for (Object item : targetFieldNameLst){					
					if (item!=null){									
						targetFieldName = item.toString();
						String[] fieldToken = targetFieldName.split("[.]");
						if (fieldToken.length==2){
							// ignore second index - field
							tableName = fieldToken[0];						
							if (StringUtils.isNotBlank(tableName)) {
								
								if (!tableList.contains(tableName)){
									
									tableList.add(tableName);
									
									queryString = "from " + tableName + " where (isDeleted is null or isDeleted=0) and activityUid = :activityUid";
									List<Object> objectlst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
									if (objectlst.size()>0){
										Object object =  objectlst.get(0);
										
										Boolean isSubmit = (Boolean) PropertyUtils.getProperty(object, "isSubmit");
										if(isSubmit!=null && isSubmit) RevisionUtils.unSubmitRecordWithStaticRevisionLog(selection, object, "Auto update from time codes");
										
										PropertyUtils.setProperty(object, "activityUid",null);
										ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(object);
									}
								}						
							}
						}							
					}				
				}	
				
				//set is_deleted for each moduleParameterLink record to true
				queryString = "from ModuleParameterLink where (isDeleted is null or isDeleted =0) and activity_uid = :activityUid";
				List<ModuleParameterLink> ModuleParameterLinkLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
				for (ModuleParameterLink moduleParameterLink : ModuleParameterLinkLst){	
					moduleParameterLink.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(moduleParameterLink);
				}
			}			
			
		}
	}
	
	/**
	 * FOR NOBLE - return int
	 * @param activityUid
	 * @param moduleParameterDetailUid
	 * @throws Exception 
	 */
	public static int hasActivityDescription(String activityUid,String moduleParameterDetailUid) throws Exception{
		String queryString = "Select A.activityDescriptionMatrixUid from ActivityDescriptionMatrix A, ModuleParameterDetail B, ModuleParameterLink C where (A.isDeleted is null or A.isDeleted =0) " + 
		                     "and (B.isDeleted is null or B.isDeleted =0) and (C.isDeleted is null or C.isDeleted =0) and A.activityDescriptionMatrixUid=B.activityDescriptionMatrixUid " +
		                     "and B.moduleParameterDetailUid=C.moduleParameterDetailUid and C.activityUid = :activityUid group by A.activityDescriptionMatrixUid";
		List<Object> idList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
		if (idList.size()>0){
			if(idList.get(0)!=null){
				String activityDescriptionMatrixUid = idList.get(0).toString();
				
				queryString = "Select moduleParameterDetailUid from ModuleParameterDetail where (isDeleted is null or isDeleted =0) " + 
                			  "and activityDescriptionMatrixUid = :activityDescriptionMatrixUid and moduleParameterDetailUid = :moduleParameterDetailUid ";
				idList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString,new String[]{"activityDescriptionMatrixUid","moduleParameterDetailUid"},new Object[]{activityDescriptionMatrixUid,moduleParameterDetailUid});
				if(idList.size()>0) 
					return 1;
				else
					return -1;
			}
		}		
		return 0;
	}
	
	/**
	 * FOR NOBLE - return  Map <String key - [TableName.FieldName],Object value - [Parameter Value]>
	 * @param commandBean
	 * @param activityUid
	 * @param OperationUomContext
	 * @throws Exception 
	 */
	public static Map<String,Object> getActivityRemarkParameterValues(CommandBean commandBean,String activityUid,OperationUomContext opUomContext) throws Exception{
		return getActivityRemarkParameterValues(commandBean,activityUid,"targetedFieldName",false,false, opUomContext);
	}
	
	/**
	 * FOR NOBLE - return  Map <String key - [TableName.FieldName],Object value - [Parameter Value]>
	 * @param commandBean
	 * @param activityUid
	 * @param useKey
	 * @param ShowAllValue
	 * @param ShowFormattedValue
	 * @param OperationUomContext
	 * @throws Exception 
	 */
	public static Map<String,Object> getActivityRemarkParameterValues(CommandBean commandBean,String activityUid,String useKey,Boolean ShowAllValue,Boolean ShowFormattedValue, OperationUomContext opUomContext) throws Exception{
		
		String queryString = "from ModuleParameterLink where (isDeleted is null or isDeleted = false) and activityUid = :activityUid";
		
		List<ModuleParameterLink> paramList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
		if (paramList.size() >0){
			
			Map<String,Object>resultMap = new HashMap<String,Object>();
			
			String keyField = "";
			String targetFieldName="";
			String parameterKey = "";
			Object data=null;
			CustomFieldUom customFieldUom = null;
			String tableName = "";
			String fieldName = "";
			String parameterType = "";
			Class<?> hibernateClass = null;
			
			for(ModuleParameterLink moduleParameterLink : paramList){
				
				queryString = "from ModuleParameterDetail where (isDeleted is null or isDeleted = false) and moduleParameterDetailUid = :moduleParameterDetailUid";
				List<ModuleParameterDetail> moduleParameterDetailList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "moduleParameterDetailUid", moduleParameterLink.getModuleParameterDetailUid());
				
				if(moduleParameterDetailList.size()>0){
					ModuleParameterDetail moduleParameterDetail = moduleParameterDetailList.get(0);
					targetFieldName="";
					data=null;
					
					if (moduleParameterDetail.getTargetFieldName() != null) targetFieldName = moduleParameterDetail.getTargetFieldName();
					if (moduleParameterDetail.getParameterKey() != null) parameterKey = moduleParameterDetail.getParameterKey();
					if (moduleParameterLink.getParameterValue() != null) data = moduleParameterLink.getParameterValue();
					if (moduleParameterDetail.getParamType() != null) parameterType =moduleParameterDetail.getParamType() ;
					
					if ("targetedFieldName".equalsIgnoreCase(useKey)){
						keyField = targetFieldName;
					}else{
						keyField = moduleParameterLink.getModuleParameterDetailUid();
					}					
					
					if (ShowAllValue || (ACTIVTIY_REMARK_HASH_PARAMETER.equalsIgnoreCase(parameterType)||ACTIVTIY_REMARK_CLIENT_PARAMETER.equalsIgnoreCase(parameterType)||
						ACTIVTIY_REMARK_HOLEDEPTH_PARAMETER.equalsIgnoreCase(parameterType)||ACTIVTIY_REMARK_STRINGDEPTHFROM_PARAMETER.equalsIgnoreCase(parameterType)||
						ACTIVTIY_REMARK_STRINGDEPTHTO_PARAMETER.equalsIgnoreCase(parameterType))){
						tableName = "";
						fieldName = "";
						hibernateClass = null;
						String[] fieldToken = targetFieldName.split("[.]");
						if (fieldToken.length==2){
							tableName = fieldToken[0];
							fieldName = fieldToken[1];
						}
						
						if (StringUtils.isNotBlank(tableName)) {
							String tableClassName = ApplicationUtils.getConfiguredInstance().getTablePackage() + CommonUtils.beanCapitalize(tableName);
							hibernateClass = Class.forName(tableClassName);
						}				
						
						if(opUomContext!=null){
							customFieldUom = new CustomFieldUom(commandBean, hibernateClass, fieldName, opUomContext);	
							if(customFieldUom.isUOMMappingAvailable()){
								Double dblValue= null;
								if (data!=null){
									UOMMapping  uomMapping = customFieldUom.getUOMMapping();
									
									dblValue = Double.parseDouble(data.toString());
									customFieldUom.setBaseValue(dblValue);									
									dblValue = customFieldUom.getConvertedValue(uomMapping.isDatumConversion());
									
									if(ShowFormattedValue){
										customFieldUom.setUseGroupingDataFields(false);
										resultMap.put(keyField,customFieldUom.formatOutputPrecision(dblValue));
									}else{
										resultMap.put(keyField,dblValue);
									}
									
								}else{
									resultMap.put(keyField,null);
								}										
							}else{
								resultMap.put(keyField,data);
							}							
						}else{
							resultMap.put(keyField,data);
						}	
					}			
				}				
			}	
			
			return resultMap;
		}		
		return null;
	}
	
	/**
	 * FOR NOBLE - return  the data type of Module_Parameter_Detail's field>
	 * @param moduleParameterDetailUid
	 * @param key
	 * @throws Exception 
	 */
	public static String getModuleParameterDetailFieldType(String moduleParameterDetailUid,String key) throws Exception{
		
		String queryString = "Select targetFieldName from ModuleParameterDetail where (isDeleted is null or isDeleted = 0) and moduleParameterDetailUid = :moduleParameterDetailUid and parameterKey = :parameterKey";
		
		List<Object> paramList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"moduleParameterDetailUid","parameterKey"},new Object[]{moduleParameterDetailUid,key});
		if (paramList.size() >0){
			
			if (paramList.get(0) !=null){
				String targetFieldName = paramList.get(0).toString();
				
				String[] fieldToken = targetFieldName.split("[.]");
				if (fieldToken.length==2){
					String tableName = fieldToken[0];
					String fieldName = fieldToken[1];
					
					if (StringUtils.isNotBlank(tableName)) {
						String tableClassName = ApplicationUtils.getConfiguredInstance().getTablePackage() + CommonUtils.beanCapitalize(tableName);
						Class<?> hibernateClass = Class.forName(tableClassName);
						
						Type dataType= ApplicationUtils.getConfiguredInstance().getFieldTypeFromTable(hibernateClass, fieldName);
						String typeName = dataType.getReturnedClass().getName();
						if ( (typeName.equalsIgnoreCase("java.lang.double"))|| (typeName.equalsIgnoreCase("java.lang.float"))){
							return "double";
						}else if ((typeName.equalsIgnoreCase("java.lang.integer"))|| (typeName.equalsIgnoreCase("java.lang.long"))){
							return "integer";
						}
						
					}	
				}			
			}
		}	
		
		return "string"; 
	}
	
	/**
	 * Returns phase short code based on the selected UID
	 * @param phaseCodeUid
	 * @throws Exception
	 */
	public static String getPhaseCode(String phaseCodeUid) throws Exception{
		
		String phaseCode = null;
		
		String strSql = "SELECT shortCode FROM LookupPhaseCode WHERE (isDeleted is null or isDeleted = false) AND lookupPhaseCodeUid = :thisPhaseCodeUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisPhaseCodeUid", phaseCodeUid);
		
		if(lstResult.size()>0 && lstResult.get(0) != null)
			if(!StringUtils.isEmpty(lstResult.get(0).toString()))
				phaseCode = lstResult.get(0).toString();
		
		return phaseCode;
	}
	
	/**
	 * Returns task short code based on the selected UID
	 * @param taskCodeUid
	 * @throws Exception
	 */
	public static String getTaskCode(String taskCodeUid) throws Exception{
		
		String taskCode = null;
		
		String strSql = "SELECT shortCode FROM LookupTaskCode WHERE (isDeleted is null or isDeleted = false) AND lookupTaskCodeUid = :thisTaskCodeUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisTaskCodeUid", taskCodeUid);
		
		if(lstResult.size()>0 && lstResult.get(0) != null)
			if(!StringUtils.isEmpty(lstResult.get(0).toString()))
				taskCode = lstResult.get(0).toString();
		
		return taskCode;
	}
	
	/**
	 * Returns user short code based on the selected UID
	 * @param userCodeUid
	 * @throws Exception
	 */
	public static String getUserCode(String userCodeUid) throws Exception{
		
		String userCode = null;
		
		String strSql = "SELECT shortCode FROM LookupUserCode WHERE (isDeleted is null or isDeleted = false) AND lookupUserCodeUid = :thisUserCodeUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisUserCodeUid", userCodeUid);
		
		if(lstResult.size()>0 && lstResult.get(0) != null)
			if(!StringUtils.isEmpty(lstResult.get(0).toString()))
				userCode = lstResult.get(0).toString();
		
		return userCode;
	}
	
	public static CustomDescriptionMatrix getCDMList(String activityDescriptionMatrixUid, String operationUid, HashMap<String, String> mapOfCustomCodeParameter) throws Exception{
		CustomDescriptionMatrix customDescriptionMatrix = null;
		
		//there shouldn't be more than 1 custom code group for the same combination of ADM codes, 
		//i.e. if we have Activity Description Matrix with
		//onOffShore = ON
		//operationType = Drilling
		//phaseCode = A1
		//classCode = B1
		//phaseCode = C1
		//jobTypeCode = D1
		//it should not be duplicated to have custom code group A selected in 1 record 
		//and the same set of 6 codes to have custom code group B selected in another record

		String queryCustom = "FROM CustomDescriptionMatrix WHERE activityDescriptionMatrixUid = :activityDescriptionMatrixUid AND (isDeleted = false or isDeleted is null)";
		List<CustomDescriptionMatrix> aList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryCustom, "activityDescriptionMatrixUid", activityDescriptionMatrixUid);

		//there will be multiple CDM tied to the ADM (many to 1), differentiated by the differing conditions.
		//e.g. CDM 1 may have condition e.g. plugBumped = true, CDM 2 may have condition e.g. plugBumped = false.
		int matchedCDMCriteria = 0;
		if(aList != null && aList.size() > 0) {
			for (CustomDescriptionMatrix cdm : aList) {
				String condition = cdm.getCondition();
				if (condition != null && !condition.equals("")) {
					String[] conditionToken = condition.split(";");
					for(int j=0; j < conditionToken.length; j++){
						String cond = "";
						String[] temp = conditionToken[j].split("[.\\=]");

						//refine, if length = 3, format expected is table.field=cond 
						//if length = 2, format expected is field=cond (does not belong to any table in use, use CustomCode table to check)

						if (temp.length == 3) {
							String tableName = temp[0];
							String fieldName = temp[1];
							if ((temp[2].equalsIgnoreCase("null"))){
								cond = "";
							} else {
								cond = temp[2];
							}
							if (cond != null && !cond.equals("")){
								String sql ="SELECT " + fieldName + " FROM " + tableName + " WHERE (isDeleted = false or isDeleted is null) AND operationUid=:operationUid AND " + fieldName + "=:condition";
								String[] paramField = {"operationUid", "condition"};
								Object[] paramValue = {operationUid, cond};
								List resultlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,paramField,paramValue);
								if (resultlist.size()>0){
									customDescriptionMatrix = cdm;
								}
							}
						} else if (temp.length == 2){
							String fieldName = temp[0];
							if ((temp[1].equalsIgnoreCase("null"))){
								cond = "";
							} else {
								cond = temp[1];
							}
							if (cond != null){
								if (cond.equals(mapOfCustomCodeParameter.get(fieldName))) {
									matchedCDMCriteria++;
								}
							}
						}
					}
					if (!mapOfCustomCodeParameter.isEmpty() && matchedCDMCriteria == mapOfCustomCodeParameter.size()) {
						customDescriptionMatrix = cdm;
					}
				} else {
					customDescriptionMatrix = cdm;
				}
				matchedCDMCriteria = 0; //reset to 0 after evaluating each condition
			}
		}
		
		return customDescriptionMatrix;
	}
	
	public static void autoRemoveTargetCustomModuleByActivityUid(String activityUid) throws Exception{
		autoRemoveTargetCustomModuleByActivityUid(null,activityUid);
	}
	
	public static HashMap<String,Map> createLinkTables(Locale locale, UserSelectionSnapshot selection, String activityUid, Boolean checkSourceActivityUid, String activityFreeTextRemark, String customCodeParamKey, String customCodeParamValue, String customCodeParamUid, Object sourceActivityUid, Object obj) throws Exception {
		HashMap<String,Map> tableMap = new HashMap<String,Map>();
		
		boolean allowRun = true;
		String strSql ="";
		String moduleParameterDetailUid = null;
		Object parameterValue =null;
		List lstResult = null;
		CustomFieldUom thisConverter = new CustomFieldUom(locale);
		
		CustomParameterLink customParameterLink = null;
		CustomParameterDetail customParameterDetail = null;
		String customParameterLinkUidList = "";
		
		CustomFtrLink customFtrLink = null;
		
		if (activityFreeTextRemark.trim().length()>0){
			String[] tokens = activityFreeTextRemark.split("::");

			for (int i=0;i<tokens.length;i++){
				String[] params = tokens[i].split(":=");
				
				if(params.length==1) continue;
				moduleParameterDetailUid = params[0];
				parameterValue = params[1];
				
				if (allowRun){
					if (FTRUtils.hasActivityCustomDescription(activityUid, moduleParameterDetailUid)<0)
						FTRUtils.autoRemoveTargetCustomModuleByActivityUid(selection, activityUid);
					allowRun = false;
				}
				
				//get detail from customParameterDetail 
				Object baseValue =parameterValue;
				String parameterKey = "";
				String targetFieldName = "";
				String parameterType = "";
				
				strSql = "FROM CustomParameterDetail WHERE (isDeleted is null or isDeleted = false) AND customParameterDetailUid = :customParameterDetailUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "customParameterDetailUid", moduleParameterDetailUid);
				if (lstResult.size() > 0){
					customParameterDetail = (CustomParameterDetail) lstResult.get(0);
					parameterKey = customParameterDetail.getParameterKey();
					targetFieldName = customParameterDetail.getTargetFieldName();
					parameterType = customParameterDetail.getParamType();
					
					//check if record exist
					strSql = "FROM CustomParameterLink WHERE (isDeleted is null or isDeleted = false) AND activityUid = :activityUid AND customParameterDetailUid = :customParameterDetailUid";
					String[] paramsFields = {"activityUid", "customParameterDetailUid"};
					Object[] paramsValues = {activityUid, moduleParameterDetailUid};
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					if (lstResult.size() > 0){
						customParameterLink = (CustomParameterLink) lstResult.get(0);
					}else{
						customParameterLink = new CustomParameterLink();
						customParameterLink.setActivityUid(activityUid);
						customParameterLink.setCustomParameterDetailUid(moduleParameterDetailUid);
					}
					
					strSql = "FROM CustomFtrLink WHERE (isDeleted is null or isDeleted = false) AND activityUid = :activityUid";
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,  "activityUid", activityUid);
					boolean newFtrLink = true;
					if (lstResult.size() > 0) {
						newFtrLink = false;
						customFtrLink = (CustomFtrLink) lstResult.get(0);
					} else {
						customFtrLink = new CustomFtrLink();
						customFtrLink.setActivityUid(activityUid);
					}
					
					String tableName = "";
					String fieldName = "";
					Class<?> hibernateClass = null;
					
					String[] fieldToken = targetFieldName.split("[.]");
					if (fieldToken.length==2){
						tableName = fieldToken[0];
						fieldName = fieldToken[1];
					}
					if (StringUtils.isNotBlank(tableName)) {
						String tableClassName = ApplicationUtils.getConfiguredInstance().getTablePackage() + CommonUtils.beanCapitalize(tableName);
						hibernateClass = Class.forName(tableClassName);
					}
					
					thisConverter.setReferenceMappingField(hibernateClass, fieldName);
					if(thisConverter.isUOMMappingAvailable()){
						UOMMapping  uomMapping = thisConverter.getUOMMapping();
						thisConverter.setBaseValueFromUserValue(CommonUtil.getConfiguredInstance().toDouble(parameterValue.toString(), locale), uomMapping.isDatumConversion());
						baseValue = thisConverter.getBasevalue();
						parameterValue = thisConverter.getConvertedValue(uomMapping.isDatumConversion());
					}

					strSql = "FROM CustomDescriptionMatrix WHERE (isDeleted is null or isDeleted = false) AND customDescriptionMatrixUid = :customDescriptionMatrixUid";
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "customDescriptionMatrixUid", customParameterDetail.getCustomDescriptionMatrixUid());
					if (lstResult != null) {
						CustomDescriptionMatrix cdm = (CustomDescriptionMatrix) lstResult.get(0);
						boolean rCChanged = cdm.getRemarksCode() != null && !cdm.getRemarksCode().equals(customFtrLink.getRemarksCode());
						if (rCChanged) {
							customFtrLink.setRemarksCode(cdm.getRemarksCode());	
						}
						boolean aTChanged = cdm.getAnalysisType() != null && !cdm.getAnalysisType().equals(customFtrLink.getAnalysisType()); 
						if (aTChanged) {
							customFtrLink.setAnalysisType(cdm.getAnalysisType());
						}
						if (newFtrLink || rCChanged || aTChanged) {
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(customFtrLink);	
						}
					}
					customParameterLink.setCustomFtrLinkUid(customFtrLink.getCustomFtrLinkUid());
					customParameterLink.setParameterKey(parameterKey);
					customParameterLink.setParameterType(customParameterDetail.getParamType());
					customParameterLink.setTargetFieldName(customParameterDetail.getTargetFieldName());
					customParameterLink.setParameterValue(baseValue.toString());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(customParameterLink);
					
					if(customParameterLink.getCustomParameterLinkUid()!=null){
						if (customParameterLinkUidList.length()>0)
							customParameterLinkUidList = customParameterLinkUidList +",";
						customParameterLinkUidList = customParameterLinkUidList + customParameterLink.getCustomParameterLinkUid(); 
					}
					
					if (!ACTIVTIY_REMARK_INPUTONLY_PARAMETER.equalsIgnoreCase(parameterType)){
						Map<String,Object> fieldmap = null;
						if(tableName.startsWith("Ftr")){
							if (tableMap.containsKey(tableName)){
								fieldmap = tableMap.get(tableName);
								fieldmap.put(fieldName, parameterValue);
							}else{
								fieldmap = new HashMap<String,Object>();
								fieldmap.put(fieldName, parameterValue);
								tableMap.put(tableName, fieldmap);
							}
						}else{
							if (tableMap.containsKey(tableName.toLowerCase())){
								fieldmap = tableMap.get(tableName.toLowerCase());
								fieldmap.put(fieldName, parameterValue);
							}else{
								fieldmap = new HashMap<String,Object>();
								fieldmap.put(fieldName, parameterValue);
								tableMap.put(tableName.toLowerCase(), fieldmap);
							}
						}

						FTRUtils.addFieldBasedOnSpecialParameterType(thisConverter,tableMap, parameterType, baseValue);
					}
				}
			}
			
			//insert for custom code's link if found
			CustomCodeLink customCodeLink = null;
			String customCodeLinkUidList = "";
			
			List<String[]> list = parseCustomCodeSelected(customCodeParamKey, customCodeParamValue, customCodeParamUid);
			if (list != null) {
				
				String customCodeUid = "";
				String parameterKey = "";
				String strSql2 = "";
				List<CustomCode> lstResult2 = null;
				Object baseValue = null;
				
				//[0] = parameterKey, [1] = value, [2] = customCodeUid
				for (String[] strArray : list) {
					parameterKey = strArray[0];
					parameterValue = strArray[1];
					if (parameterValue != null && "null".equalsIgnoreCase(parameterValue.toString())) {
						parameterValue = "";
					}
					customCodeUid = strArray[2];
					
					//check if record exist
					strSql = "FROM CustomCodeLink WHERE (isDeleted is null or isDeleted = false) AND activityUid = :activityUid AND customCodeUid = :customCodeUid";
					String[] paramsFields = {"activityUid", "customCodeUid"};
					Object[] paramsValues = {activityUid, customCodeUid};
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					if (lstResult.size() > 0){
						customCodeLink = (CustomCodeLink) lstResult.get(0);
					}else{
						customCodeLink = new CustomCodeLink();
						customCodeLink.setActivityUid(activityUid);
						customCodeLink.setCustomCodeUid(customCodeUid);
					}
					
					strSql2 = "FROM CustomCode WHERE (isDeleted is null or isDeleted = false) AND customCodeUid = :customCodeUid";
					lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[]{"customCodeUid"}, new String[]{customCodeUid});
					
					CustomCode cc = null;
					if (lstResult2 != null && !lstResult2.isEmpty()) {
						cc = lstResult2.get(0);
					}
					
					String tableName = "";
					String fieldName = "";
					Class<?> hibernateClass = null;
					
					//check null in case this is not custom code related
					if (cc != null) {
						String[] fieldToken = cc.getTargetFieldName().split("[.]");
						if (fieldToken.length==2){
							tableName = fieldToken[0];
							fieldName = fieldToken[1];
						}
						if (StringUtils.isNotBlank(tableName)) {
							String tableClassName = ApplicationUtils.getConfiguredInstance().getTablePackage() + CommonUtils.beanCapitalize(tableName);
							hibernateClass = Class.forName(tableClassName);
						}
						thisConverter.setReferenceMappingField(hibernateClass, fieldName);
						
						if(thisConverter.isUOMMappingAvailable()){
							UOMMapping  uomMapping = thisConverter.getUOMMapping();
							thisConverter.setBaseValueFromUserValue(CommonUtil.getConfiguredInstance().toDouble(parameterValue.toString(), locale), uomMapping.isDatumConversion());
							baseValue = thisConverter.getBasevalue();
							parameterValue = thisConverter.getConvertedValue(uomMapping.isDatumConversion());
						}
					}
												
					if (customCodeLink != null) {
						customCodeLink.setParameterKey(parameterKey);
						if (baseValue != null) {
							customCodeLink.setParameterValue(baseValue.toString());
						}
						else if (parameterKey != null) {
							//e.g. lookup key no uom etc
							customCodeLink.setParameterValue(parameterValue.toString());
						}
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(customCodeLink);
						
						if (customCodeLinkUidList.length()>0) {
							customCodeLinkUidList = customCodeLinkUidList +",";
						}
						customCodeLinkUidList = customCodeLinkUidList + customCodeLink.getCustomCodeUid();
					}
				}
			}
			
			//delete the record does not belong to current selected parameter
			String strSql2 = "FROM CustomParameterLink WHERE (isDeleted is null or isDeleted = false) AND activityUid = :activityUid AND customParameterLinkUid not in (:customParameterLinkUid)";
			String[]paramsFields2 = {"activityUid", "customParameterLinkUid"};
			Object[]paramsValues2 = {activityUid,Arrays.asList(customParameterLinkUidList.split(","))};
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
			if (lstResult2 != null && lstResult2.size() > 0) {
				for(Object objResult: lstResult2){
					customParameterLink = (CustomParameterLink) objResult;
					customParameterLink.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(customParameterLink);
				}
			}
			
			//delete the record does not belong to current selected parameter
			String strSql3 = "FROM CustomCodeLink WHERE (isDeleted is null or isDeleted = false) AND activityUid = :activityUid AND customCodeUid not in (:customCodeUid)";
			String[]paramsFields3 = {"activityUid", "customCodeUid"};
			Object[]paramsValues3 = {activityUid,Arrays.asList(customCodeLinkUidList.split(","))};
			List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3);
			if (lstResult3 != null && lstResult3.size() > 0) {
				for(Object objResult: lstResult3){
					customCodeLink = (CustomCodeLink) objResult;
					customCodeLink.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(customCodeLink);
				}
			}
			
			return tableMap;
		} else {
			if (checkSourceActivityUid) {
				strSql = "FROM Activity WHERE activityUid = :activityUid";
				
				if (sourceActivityUid != null) {
					List<Activity> findActivity = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", sourceActivityUid);
					if (findActivity.size() > 0) {
						Activity activity = findActivity.get(0);
						if (activity.getAdditionalRemarks() != null) {
							if (obj instanceof Activity || obj instanceof NextDayActivity) {
								Object currentRemarks = PropertyUtils.getProperty(obj, "additionalRemarks");
								if (currentRemarks != null && currentRemarks.equals(activity.getAdditionalRemarks())) {
									//safe to clone the link tables
									strSql = "FROM CustomFtrLink WHERE activityUid = :activityUid";
									List<CustomFtrLink> findCustomFtrLink = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,  "activityUid", sourceActivityUid);
									CustomFtrLink newCustomFtrLink = null;
									if (findCustomFtrLink != null) {
										for (CustomFtrLink ftrLink : findCustomFtrLink) {
											newCustomFtrLink = new CustomFtrLink();
											PropertyUtils.copyProperties(newCustomFtrLink, ftrLink);
											newCustomFtrLink.setCustomFtrLinkUid(null);
											newCustomFtrLink.setActivityUid(activityUid);
											newCustomFtrLink.setDailyUid(selection.getDailyUid());	
											ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newCustomFtrLink);
										}
									}
									
									strSql = "FROM CustomCodeLink WHERE activityUid = :activityUid";
									List<CustomCodeLink> findCustomCodeLink = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,  "activityUid", sourceActivityUid);
									if (findCustomCodeLink != null) {
										for (CustomCodeLink codeLink : findCustomCodeLink) {
											CustomCodeLink newCustomCodeLink = new CustomCodeLink();
											PropertyUtils.copyProperties(newCustomCodeLink, codeLink);
											newCustomCodeLink.setCustomCodeLinkUid(null);
											newCustomCodeLink.setActivityUid(activityUid);
											newCustomCodeLink.setDailyUid(selection.getDailyUid());
											ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newCustomCodeLink);
										}
									}
									
									strSql = "FROM CustomParameterLink WHERE activityUid = :activityUid";
									List<CustomParameterLink> findCustomParameterLink = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,  "activityUid", sourceActivityUid);
									if (findCustomParameterLink != null) {
										for (CustomParameterLink parameterLink : findCustomParameterLink) {
											CustomParameterLink newCustomParameterLink = new CustomParameterLink();
											PropertyUtils.copyProperties(newCustomParameterLink, parameterLink);
											newCustomParameterLink.setCustomParameterLinkUid(null);
											newCustomParameterLink.setActivityUid(activityUid);
											if (newCustomFtrLink != null) {
												newCustomParameterLink.setCustomFtrLinkUid(newCustomFtrLink.getCustomFtrLinkUid());
											}
											newCustomParameterLink.setDailyUid(selection.getDailyUid());
											ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newCustomParameterLink);
										}
									}
									
									String[] tableNameList = {"FtrTripping","FtrBopEquipment","FtrDrillLineEquipment","FtrRig","FtrDrilling","FtrWorkoverCompletionSystem"};
									List<Object> ftrList = null;
									for (String tableName : tableNameList){
										String queryString = "from " + tableName +" where (isDeleted is null or isDeleted = FALSE) and activityUid = :activityUid";
										ftrList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", sourceActivityUid);
										if (ftrList != null && ftrList.size() > 0) {
											for (int i = 0; i < ftrList.size(); i++) {
												Object o = ftrList.get(i);
												String classPathName = ApplicationUtils.getConfiguredInstance().getTablePackage();
												Class ftr = Class.forName(classPathName + tableName);
												Constructor constructor = null;
												Object objFtr = null;
												if(ftr != null){
													try {
														constructor = ftr.getDeclaredConstructor();
														if(constructor != null){
															objFtr = constructor.newInstance();
														}
													}catch (InvocationTargetException x) {
											            x.printStackTrace();
											          } catch (NoSuchMethodException x) {
											            x.printStackTrace();
											          } catch (InstantiationException x) {
											            x.printStackTrace();
											          } catch (IllegalAccessException x) {
											            x.printStackTrace();
											          }
												}

												PropertyUtils.copyProperties(objFtr, o);
												Method method;
												
												try {
													  method = ftr.getMethod("set"+tableName+"Uid", String.class);
													  method.invoke(objFtr,new Object[]{null});
													  method = ftr.getMethod("setActivityUid", String.class);
													  method.invoke(objFtr,new Object[]{activityUid});
													  method = ftr.getMethod("setDailyUid", String.class);
													  method.invoke(objFtr,new Object[]{selection.getDailyUid()});
												} catch (SecurityException x) {
														x.printStackTrace();
												} catch (NoSuchMethodException x) {
														x.printStackTrace();
												} catch (IllegalArgumentException x) { 
														x.printStackTrace();
												} catch (IllegalAccessException x) { 
														x.printStackTrace();
												} catch (InvocationTargetException x) { 
														x.printStackTrace();
												}
												
												ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(objFtr);
											}
										}
									}
								}
							}
						}
					}
					return tableMap;
				}
			}
		}
		return null;
	}
	
	public static List<String[]> parseCustomCodeSelected(String parameterKey, String parameterValue, String parameterUid) {
		List<String[]> list = null;
		if (parameterKey != null && parameterValue != null && parameterUid != null) {
			list = new ArrayList<String[]>();


			String[] params = parameterKey.split(":.:.:");
			String[] paramsVal = parameterValue.split(":.:.:");
			String[] paramsUid = parameterUid.split(":.:.:");

			//construct an string array, [0] = parameterKey, [1] = value, [2] = customCodeUid
			for (int i = 0; i < params.length; i++) {
				String[] strArray = new String[3];
				strArray[0] = params[i];
				if (paramsVal.length > i) {
					strArray[1] = paramsVal[i];	
				}
				if (paramsUid.length > i) {
					strArray[2] = paramsUid[i];
				}
				list.add(strArray);
			}
		}
		return list;
	}
	
	public static HashMap<String,Map> createLinkTables(String activityUid, String activityFreeTextRemark, UserSelectionSnapshot userSelection) throws Exception {
		Locale locale = userSelection.getLocale();
		
		return createLinkTables(locale, userSelection, activityUid, false, activityFreeTextRemark, "","","", null,null);
	
	}
	
	public static HashMap<String,Map> createLinkTables(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot selection, String activityUid, Boolean checkSourceActivityUid) throws Exception {
		Locale locale = commandBean.getUserLocale();
		String activityFreeTextRemark = checkSourceActivityUid ? "" : node.getDynaAttr().get("activityFreeTextRemark").toString();
		String customCodeParamKey = null;
		String customCodeParamValue=null;
		String customCodeParamUid = null;
		Object sourceActivityUid = null;
		
		if (node.getDynaAttr().get("customCodeParamKey") != null) {
			customCodeParamKey=node.getDynaAttr().get("customCodeParamKey").toString();
		}
		if (node.getDynaAttr().get("customCodeParamValue") != null) {
			customCodeParamValue=node.getDynaAttr().get("customCodeParamValue").toString();
		}
		if (node.getDynaAttr().get("customCodeParamUid") != null) {
			customCodeParamUid=node.getDynaAttr().get("customCodeParamUid").toString();
		}
		if (checkSourceActivityUid) {
			sourceActivityUid = node.getDynaAttr().get("sourceActivityUid");	
		}
		
		Object obj = node.getData();
		return createLinkTables(locale, selection, activityUid, checkSourceActivityUid, activityFreeTextRemark, customCodeParamKey, customCodeParamValue, customCodeParamUid, sourceActivityUid, obj);
	
	}
	
	public static List<ActivityDescriptionMatrix> formQueryForNdorADM(String rigType, String phsCode, String tskCode, String usrCode) throws Exception {
		//retrieve matched free text remarks based on 4 criteria  
		List<String> paramsFields = new ArrayList<String>();
		List<Object> paramsValues = new ArrayList<Object>();
		String strSql = "from ActivityDescriptionMatrix where (isDeleted = false or isDeleted is null) " +
				"AND (rigType=:rigType or rigType is null or rigType='') ";
		paramsFields.add("rigType");
		paramsValues.add(rigType);
				
		if (StringUtils.isNotBlank(phsCode)) {
			strSql += " AND (phaseShortCode = :phaseShortCode)";
			paramsFields.add("phaseShortCode");
			paramsValues.add(phsCode);
		} else {
			strSql += " AND (phaseShortCode = '' or phaseShortCode is null)";
		}
		
		if (StringUtils.isNotBlank(tskCode)) {
			strSql += " AND (taskShortCode = :taskShortCode)";
			paramsFields.add("taskShortCode");
			paramsValues.add(tskCode);
		} else {
			strSql += " AND (taskShortCode = '' or taskShortCode is null)";
		}
		
		if (StringUtils.isNotBlank(usrCode)) {
			strSql += " AND (userShortCode = :userShortCode)";
			paramsFields.add("userShortCode");
			paramsValues.add(usrCode);
		} else {
			strSql += " AND (userShortCode = '' or userShortCode is null)";
		}
		strSql +=" ORDER BY rigType desc"; 
	 	List<ActivityDescriptionMatrix> activityRemarkList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
	 	
		return activityRemarkList;
	}
	
	public static void checkAllowNdorFTRButtonShow(CommandBean commandBean, CommandBeanTreeNode node, HttpServletRequest request) throws Exception{
		Object object = node.getData();
		String phsCode = "";
		String tskCode = "";
		String userCode = "";
		
		if(object instanceof Activity){
			Activity activity = (Activity) object;
			
			phsCode = activity.getPhaseCode();
			tskCode = activity.getTaskCode();
			userCode = activity.getUserCode();
			node.getDynaAttr().put("FTRRecord", activity.getAdditionalRemarks());
			if(StringUtils.isBlank(activity.getAdditionalRemarks()) || StringUtils.isEmpty(activity.getAdditionalRemarks()))
			{
				node.getDynaAttr().put("FTRNoValue", "1");
			}
			
		}
		
		if(object instanceof NextDayActivity){
			NextDayActivity nextDayActivity = (NextDayActivity) object;
			
			phsCode = nextDayActivity.getPhaseCode();
			tskCode = nextDayActivity.getTaskCode();
			userCode = nextDayActivity.getUserCode();
			node.getDynaAttr().put("FTRRecord", nextDayActivity.getAdditionalRemarks());
			if(StringUtils.isBlank(nextDayActivity.getAdditionalRemarks()) || StringUtils.isEmpty(nextDayActivity.getAdditionalRemarks()))
			{
				node.getDynaAttr().put("FTRNoValue", "1");
			}
			
		}
		
		String rigType = null;
		String rigInformationUid = UserSession.getInstance(request).getCurrentRigInformationUid();
		if (rigInformationUid!=null) {
			RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigInformationUid);
			if (rig!=null)  rigType = rig.getRigType();
		}
		
		List<ActivityDescriptionMatrix> remarkList = FTRUtils.formQueryForNdorADM(rigType, phsCode, tskCode, userCode);
		
		if(remarkList != null && remarkList.size()>0){
			node.getDynaAttr().put("showFTRButton","1");
			if (request.getParameterValues("auto_populate_description") != null) {
				FTRUtils.populateFieldBasedOnSpecialParameterType(commandBean, node, request);
			}
			
		}
		else{
			node.getDynaAttr().put("showFTRButton","0");
		}
	}
	
	public static void populateFieldBasedOnSpecialParameterType(CommandBean commandBean, CommandBeanTreeNode node, HttpServletRequest request) throws Exception{
		Object object = node.getData();

		if(object instanceof Activity){
			Activity activity = (Activity) object;
			
			HashMap<String,Map> moduleParameterMap = null;
			moduleParameterMap =FTRUtils.getFieldBasedOnSpecialParameterType(commandBean,node,new UserSelectionSnapshot(UserSession.getInstance(request)));	
			
			if (moduleParameterMap!=null){					
				for (Map.Entry<String, Map> entry : moduleParameterMap.entrySet()){					
					Map<String,Object> fieldsMap = entry.getValue();						
					
					if (StringUtils.isNotBlank(entry.getKey())){
						if (fieldsMap !=null){ 
							if (activity!=null){
								
								for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
									if (StringUtils.isNotBlank(fieldEntry.getKey())) PropertyUtils.setProperty(activity, fieldEntry.getKey(), fieldEntry.getValue());
								}
							}
						}
					}											
				}						
			}							
		}
		
		if(object instanceof NextDayActivity){
			NextDayActivity nextDayActivity = (NextDayActivity) object;
			
			HashMap<String,Map> moduleParameterMap = null;
			moduleParameterMap =FTRUtils.getFieldBasedOnSpecialParameterType(commandBean,node,new UserSelectionSnapshot(UserSession.getInstance(request)));	
			
			if (moduleParameterMap!=null){					
				for (Map.Entry<String, Map> entry : moduleParameterMap.entrySet()){					
					Map<String,Object> fieldsMap = entry.getValue();						
					
					if (StringUtils.isNotBlank(entry.getKey())){
						if (fieldsMap !=null){ 
							if (nextDayActivity!=null){
								
								for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
									if (StringUtils.isNotBlank(fieldEntry.getKey())) PropertyUtils.setProperty(nextDayActivity, fieldEntry.getKey(), fieldEntry.getValue());
								}
							}
						}
					}											
				}						
			}
		}
	}
	
	public static HashMap<String,Map> getFieldBasedOnSpecialParameterType(CommandBean commandBean,CommandBeanTreeNode node,UserSelectionSnapshot selection) throws Exception{	
			
		if(node.getDynaAttr().get("activityFreeTextRemark")!=null) {
			
			HashMap<String,Map> tableMap = new HashMap<String,Map>();
			
			String activityFreeTextRemark = node.getDynaAttr().get("activityFreeTextRemark").toString();
			
			String strSql ="";
			String moduleParameterDetailUid = null;				
			Object parameterValue =null;
			List lstResult = null;

			ModuleParameterDetail moduleParameterDetail = null;
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
							
			if (activityFreeTextRemark.trim().length()>0){
				String[] tokens = activityFreeTextRemark.split("::");

				for (int i=0;i<tokens.length;i++){
					String[] params = tokens[i].split(":=");
					
					if(params.length==1) continue;
					moduleParameterDetailUid = params[0];
					parameterValue = params[1];
					
					//get detail from moduleParameterDetail 
					Object baseValue =parameterValue;
					String targetFieldName = "";
					String parameterType = "";
					
					strSql = "FROM ModuleParameterDetail WHERE (isDeleted is null or isDeleted = false) AND moduleParameterDetailUid = :moduleParameterDetailUid";
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "moduleParameterDetailUid", moduleParameterDetailUid);
					if (lstResult.size() > 0){
						moduleParameterDetail = (ModuleParameterDetail) lstResult.get(0);
						targetFieldName = moduleParameterDetail.getTargetFieldName();
						parameterType = moduleParameterDetail.getParamType();															
						
						String tableName = "";
						String fieldName = "";
						Class<?> hibernateClass = null;
						
						String[] fieldToken = targetFieldName.split("[.]");
						if (fieldToken.length==2){
							tableName = fieldToken[0];
							fieldName = fieldToken[1];
						}
						if (StringUtils.isNotBlank(tableName)) {
							String tableClassName = ApplicationUtils.getConfiguredInstance().getTablePackage() + CommonUtils.beanCapitalize(tableName);
							hibernateClass = Class.forName(tableClassName);
						}
						
						thisConverter.setReferenceMappingField(hibernateClass, fieldName);
						if(thisConverter.isUOMMappingAvailable()){
							UOMMapping  uomMapping = thisConverter.getUOMMapping();
							thisConverter.setBaseValueFromUserValue(CommonUtil.getConfiguredInstance().toDouble(parameterValue.toString(), commandBean.getUserLocale()), uomMapping.isDatumConversion());
							baseValue = thisConverter.getBasevalue();
							parameterValue = thisConverter.getConvertedValue(uomMapping.isDatumConversion());
						}			
						
						
						if (!ACTIVTIY_REMARK_INPUTONLY_PARAMETER.equalsIgnoreCase(parameterType)){													
							FTRUtils.addFieldBasedOnSpecialParameterType(thisConverter,tableMap, parameterType, baseValue);
						}												
					}
				}										
									
				return tableMap;
			}
		}
		
		return null;
	}
}


