package com.idsdatanet.d2.ftr;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.activity.ActivityDataNodeListener;
import com.idsdatanet.d2.drillnet.activity.kpi.ActivityKpiUtil;
import com.idsdatanet.d2.drillnet.bharun.BharunUtils;
import com.idsdatanet.d2.drillnet.bop.BopUtil;
import com.idsdatanet.d2.drillnet.casingSection.CasingSectionUtils;
import com.idsdatanet.d2.drillnet.drillingparameters.DrillingParametersUtils;
import com.idsdatanet.d2.drillnet.leakOffTest.LeakOffTestUtil;
import com.idsdatanet.d2.drillnet.cementJob.CementJobUtils;
import com.idsdatanet.d2.pronet.stimulation.StimulationUtils;
import com.idsdatanet.d2.pronet.perforation.PerforationUtils;

/**
 * Activity Fixed Text Remark Data Node Listener for D2
 * Modified from ModuNet's ModunetActivityDataNodeListener
 *
 */
public class FTRDataNodeListener extends ActivityDataNodeListener {
	
	public static final String ACTIVITY_REMARK_HASH_PARAMETER = "hashparameter";
	public static final String ACTIVTIY_REMARK_HASH_CONSTANT = "hashconstant";
	private Boolean includeFtrActivityValidation = false;
	
	public void setIncludeFtrActivityValidation(Boolean value){
		this.includeFtrActivityValidation = value;
	}
	public void setAdditionalInternalClassCodeForNptEvent(
			List<String> additionalInternalClassCodeForNptEvent) {
		super.setAdditionalInternalClassCodeForNptEvent(additionalInternalClassCodeForNptEvent);
	}
	
	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		super.afterDataNodeLoad(commandBean, meta, node, userSelection, request);
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, userSelection.getOperationUid());
		FTRUtils.checkAllowFTRButtonShow(node,well.getOnOffShore(),operation.getOperationCode());
	}
	
	@Override
	public void beforeDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception{
		super.beforeDataNodeDelete(commandBean, node, session, status, request);
		Object object = node.getData();
		
		if(object instanceof Activity || object instanceof NextDayActivity) {
			String activityUid = (String) PropertyUtils.getProperty(object, "activityUid"); 
			FTRUtils.deleteFTRLinkRecord(activityUid);
			FTRUtils.deleteCustomFtrLink(activityUid);
			ActivityKpiUtil.deleteFTRKPI(activityUid);
		}
	}
	
	
	@Override
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		super.afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
		
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, userSelection.getOperationUid());
		FTRUtils.checkAllowFTRButtonShow(node,well.getOnOffShore(),operation.getOperationCode());
	}

	@Override
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		super.beforeDataNodeSaveOrUpdate(commandBean, node, session, status, request);
		
		if (node.getData() instanceof Activity || node.getData() instanceof NextDayActivity) {
			Object object = node.getData();
			
			// Get Well Country (In order for validation for Norway NO Wells)
			Object wellUid = PropertyUtils.getProperty(object, "wellUid");
			String wellCountry = "";
			if (wellUid != null && StringUtils.isNotBlank(wellUid.toString())) {
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellUid.toString());
				if (well != null) {
					wellCountry = well.getCountry();
				}
			}
			
			// Description Validation that ensures that EITHER FTR or Manual Description is filled at least. 
			if (this.includeFtrActivityValidation || StringUtils.equals("NO", wellCountry)) {
				Object activityDescription = PropertyUtils.getProperty(object, "activityDescription");
				Object additionalRemarks = PropertyUtils.getProperty(object, "additionalRemarks");
				if (activityDescription == null || StringUtils.isBlank(activityDescription.toString())) {
					if (additionalRemarks == null || StringUtils.isBlank(additionalRemarks.toString())) {
						status.setContinueProcess(false, true);
						status.setFieldError(node, "activityDescription", "Either Fixed Text Remark or Manual Description must be entered");
						return;
					}
				}
			}
			
			if (node.getDynaAttr().get("customDescriptionMatrixUid")!=null){
				if(StringUtils.isNotBlank(node.getDynaAttr().get("customDescriptionMatrixUid").toString())){
					PropertyUtils.setProperty(object, "customDescriptionMatrixUid", node.getDynaAttr().get("customDescriptionMatrixUid").toString());
				}else{
					PropertyUtils.setProperty(object, "customDescriptionMatrixUid", null);
				}
			}
			
		}
	}
	
	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		// TODO Auto-generated method stub
		super.afterDataNodeSaveOrUpdate(commandBean, node, session, request,
				operationPerformed);
		

		Object object = node.getData();
		
		HashMap<String,Map> moduleParameterLinkMap = null;
		if(object instanceof Activity || object instanceof NextDayActivity) {
			//update hash parameters for free text remarks into Module Parameter Link table 
			String activityUid = (String) PropertyUtils.getProperty(object, "activityUid"); 

			//return parameter with table, field name and based value for auto creation record later
			moduleParameterLinkMap = this.updateCustomParameterLink(commandBean,node,new UserSelectionSnapshot(UserSession.getInstance(request)),activityUid);

			UserSelectionSnapshot userSelection = new UserSelectionSnapshot(UserSession.getInstance(request));

			if (object instanceof Activity) {
				Activity thisActivity = (Activity) object;
				
				ActivityKpiUtil.triggerUpdateFtrDuration(commandBean,thisActivity);
				
				//For Noble - auto create records in target module based on activity remarks
				if 	(moduleParameterLinkMap!=null){					
					for (Map.Entry<String, Map> entry : moduleParameterLinkMap.entrySet()){					
						Map<String,Object> fieldsMap = entry.getValue();						
						if (StringUtils.isNotBlank(entry.getKey())) this.saveObject(commandBean, session,userSelection,entry.getKey(),thisActivity, fieldsMap);						
					}						
				}
				if (moduleParameterLinkMap == null && StringUtils.isBlank(thisActivity.getAdditionalRemarks())) {
					// deleted the moduleParameterLink record and removed targeted module record's activityUid if found in moduleParameterLink table
					//ActivityUtils.autoRemoveTargetModuleByActivityUid(userSelection,activityUid);
					FTRUtils.autoRemoveTargetCustomModuleByActivityUid(userSelection, activityUid);
				}
				
				if(node.getDynaAttr().get("FTRRecord")!=null)
				{
					if(StringUtils.isNotBlank(node.getDynaAttr().get("FTRRecord").toString()))
					{
						if((thisActivity.getAdditionalRemarks()==null) || (thisActivity.getAdditionalRemarks()==""))
						{
							FTRUtils.deleteFTRLinkRecord(activityUid);
							FTRUtils.deleteCustomFtrLink(activityUid);
							ActivityKpiUtil.deleteFTRKPI(activityUid);
						}
					}else{
						if((thisActivity.getAdditionalRemarks()==null) || (thisActivity.getAdditionalRemarks()=="")){
							FTRUtils.autoRemoveTargetCustomModuleByActivityUid(userSelection, activityUid);
						}	
					}
				}
				
			}
			if(object instanceof NextDayActivity)
			{
				NextDayActivity thisActivity = (NextDayActivity) object;
				
				ActivityKpiUtil.triggerUpdateFtrDuration(commandBean,thisActivity);
				
				//For Noble - auto create records in target module based on activity remarks
				if 	(moduleParameterLinkMap!=null){					
					for (Map.Entry<String, Map> entry : moduleParameterLinkMap.entrySet()){					
						Map<String,Object> fieldsMap = entry.getValue();						
						if (StringUtils.isNotBlank(entry.getKey())) this.saveObject(commandBean, session,userSelection,entry.getKey(),thisActivity, fieldsMap);						
					}						
				}
				
				if(node.getDynaAttr().get("FTRRecord")!=null)
				{
					if(StringUtils.isNotBlank(node.getDynaAttr().get("FTRRecord").toString()))
					{
						if((thisActivity.getAdditionalRemarks()==null) || (thisActivity.getAdditionalRemarks()==""))
						{
							FTRUtils.deleteFTRLinkRecord(activityUid);
							FTRUtils.deleteCustomFtrLink(activityUid);
							ActivityKpiUtil.deleteFTRKPI(activityUid);
						}
					}else{
						if((thisActivity.getAdditionalRemarks()==null) || (thisActivity.getAdditionalRemarks()=="")){
							FTRUtils.autoRemoveTargetCustomModuleByActivityUid(userSelection, activityUid);
						}	
					}
				}
			}
		}	
	}	
	
	/*
	 * Copy from Yesterday is not calling this but onDataNodePasteAsNew
	 */
	/*public void onDataNodeCarryForwardFromYesterday(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception{
		super.onDataNodeCarryForwardFromYesterday(commandBean, node, session);
		Object object = node.getData();
		if (object instanceof Activity || object instanceof NextDayActivity) {
			Object currentAdditionalRemarks= PropertyUtils.getProperty(object, "additionalRemarks");
			if (currentAdditionalRemarks != null) {
				if (StringUtils.isNotBlank(currentAdditionalRemarks.toString())) {
					Daily yesterday = ApplicationUtils.getConfiguredInstance().getYesterday(session.getCurrentOperationUid(), session.getCurrentDailyUid());
					
					String sql = "FROM Activity WHERE dailyUid = :dailyUid AND (isDeleted = FALSE or isDeleted is null) AND additionalRemarks = :additionalRemarks";
					String[] paramNames = {"dailyUid", "additionalRemarks"};
					String[] paramValues = {yesterday.getDailyUid(), currentAdditionalRemarks.toString()};
					List<Activity> activityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, paramValues);
					
					if (activityList != null && !activityList.isEmpty()) {
						Activity activity = activityList.get(0);
						this.populateDynaAttrSourceActivityUid(node, activity.getActivityUid());
					}
				}
				
			}
		}
	}*/
	
	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{	//on paste as new record data with initial amount will be set to null
		super.onDataNodePasteAsNew(commandBean, sourceNode, targetNode, userSession);
		Object object = targetNode.getData();
		if (object instanceof Activity || object instanceof NextDayActivity) {
			targetNode.getDynaAttr().put("editable", true);
			
			String activityUid = null;
			if (sourceNode.getData() != null && PropertyUtils.getProperty(sourceNode.getData(), "activityUid")!=null) {
				activityUid = (String) PropertyUtils.getProperty(sourceNode.getData(), "activityUid");
				this.populateDynaAttrSourceActivityUid(targetNode, activityUid);
			} else {
				//probably from copy from yesterday
				Object currentAdditionalRemarks= PropertyUtils.getProperty(object, "additionalRemarks");
				if (currentAdditionalRemarks != null) {
					if (StringUtils.isNotBlank(currentAdditionalRemarks.toString())) {
						Daily yesterday = ApplicationUtils.getConfiguredInstance().getYesterday(userSession.getCurrentOperationUid(), userSession.getCurrentDailyUid());

						String sql = "FROM Activity WHERE dailyUid = :dailyUid AND (isDeleted = FALSE or isDeleted is null) AND additionalRemarks = :additionalRemarks";
						String[] paramNames = {"dailyUid", "additionalRemarks"};
						String[] paramValues = {yesterday.getDailyUid(), currentAdditionalRemarks.toString()};
						List<Activity> activityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, paramValues);

						if (activityList != null && !activityList.isEmpty()) {
							Activity activity = activityList.get(0);
							this.populateDynaAttrSourceActivityUid(targetNode, activity.getActivityUid());
						}
					}

				}
			}
			
			//get the dynaattr and populate to target
			if (sourceNode.getDynaAttr() != null) {
				targetNode.getDynaAttr().put("showFTRButton", sourceNode.getDynaAttr().get("showFTRButton"));
			}
			
		}
	}
	
	private void populateDynaAttrSourceActivityUid(CommandBeanTreeNode targetNode, String activityUid) throws Exception {
		if (StringUtils.isNotBlank(activityUid)) {
			targetNode.getDynaAttr().put("sourceActivityUid", activityUid);
		}
	}
	
	private HashMap<String,Map> updateCustomParameterLink(CommandBean commandBean,CommandBeanTreeNode node,UserSelectionSnapshot selection,String activityUid) throws Exception{
		
		if (StringUtils.isNotBlank(activityUid)){
			Object ftrDA = node.getDynaAttr().get("activityFreeTextRemark");
			if(ftrDA != null) {
				if (StringUtils.isNotBlank(ftrDA.toString())) {
					return FTRUtils.createLinkTables(commandBean, node, selection, activityUid, false);	
				}
			}
			if (node.getDynaAttr().get("sourceActivityUid") != null) {
				//for copy paste, user may just copy and "save" an existing FTR, so we need to check against sourceActivityUid
				return FTRUtils.createLinkTables(commandBean, node, selection, activityUid, true);
				
			}
		}
		return null;
	}
	
	private void saveObject(CommandBean commandBean, UserSession session,UserSelectionSnapshot userSelection,String moduleName,Object thisActivity, Map<String, Object>fieldsMap) throws Exception{
		Activity act;
		if(thisActivity instanceof Activity){
			act = (Activity) thisActivity;
			if ("drillingparameters".equalsIgnoreCase(moduleName)){
				DrillingParametersUtils.createDrillingParametersBasedOnActivityRemarks(commandBean, session,userSelection, act, fieldsMap);
			}else if ("casingsection".equalsIgnoreCase(moduleName)){
				if("6.12".equals(act.getTaskCode())){
					CasingSectionUtils.updateCasingSetDepthByODBasedOnActivityRemarks(userSelection, fieldsMap);
				}else{
					CasingSectionUtils.createCasingSectionBasedOnActivityRemarks(userSelection,act, fieldsMap);
				}			
			}else if ("bharun".equalsIgnoreCase(moduleName) || "bharundailysummary".equalsIgnoreCase(moduleName)){
				BharunUtils.createBHABasedOnActivityRemarks(userSelection, moduleName, act, fieldsMap);
			}else if ("cementjob".equalsIgnoreCase(moduleName)){
				CementJobUtils.createCementJobBasedOnActivityRemarks(userSelection, act, fieldsMap);
			}else if ("stimulation".equalsIgnoreCase(moduleName) || "targetzone".equalsIgnoreCase(moduleName)  || "stimulationpump".equalsIgnoreCase(moduleName)){
				StimulationUtils.createStimulationBasedOnActivityRemarks(userSelection, moduleName, act, fieldsMap);
			}else if ("perforation".equalsIgnoreCase(moduleName) || "perforationgunconfiguration".equalsIgnoreCase(moduleName) ){
				PerforationUtils.createPerforationBasedOnActivityRemarks(userSelection, moduleName, act, fieldsMap);
			}else if ("bop".equalsIgnoreCase(moduleName) || "boplog".equalsIgnoreCase(moduleName) ){
				BopUtil.createBopBasedOnActivityRemarks(userSelection,moduleName, act, fieldsMap);
			}else if ("leakofftest".equalsIgnoreCase(moduleName)){
				LeakOffTestUtil.createLOTBasedOnActivityRemarks(userSelection,act, fieldsMap);
			}else if ("activity".equalsIgnoreCase(moduleName)){
				FTRUtils.updateActivityBasedOnActivityRemarks(act, fieldsMap);
			}else if (moduleName.startsWith("Ftr")){
				ActivityKpiUtil.createFTRKPIBasedOnActivityRemarks(commandBean,userSelection, moduleName, thisActivity, fieldsMap);
			}
		}else if(thisActivity instanceof NextDayActivity){
			if (moduleName.startsWith("Ftr")){
				ActivityKpiUtil.createFTRKPIBasedOnActivityRemarks(commandBean,userSelection, moduleName, thisActivity, fieldsMap);
			}
		}
	}	
}
