package com.idsdatanet.d2.ftr;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.birt.report.model.api.util.StringUtil;
import org.hibernate.type.Type;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityDescriptionMatrix;
import com.idsdatanet.d2.core.model.CustomCode;
import com.idsdatanet.d2.core.model.CustomCodeLink;
import com.idsdatanet.d2.core.model.CustomDescriptionMatrix;
import com.idsdatanet.d2.core.model.CustomParameterDetail;
import com.idsdatanet.d2.core.model.DepotWellOperationMapping;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.LogCurveDefinition;
import com.idsdatanet.d2.core.model.LogMnemonicsMapping;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.model.SchedulerTemplate;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.hibernate.tables.UomTemplate;
import com.idsdatanet.d2.core.uom.mapping.UOMManager;
import com.idsdatanet.d2.core.uom.mapping.UOMMapping;
import com.idsdatanet.d2.core.uom.mapping.UserUOMSelection;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.activity.ActivityCommandBeanListener;
import com.idsdatanet.d2.drillnet.commonLookup.CommonLookupUtil;
import com.idsdatanet.d2.infra.uom.OperationUomContext;
import com.idsdatanet.depot.core.DepotConstants;
import com.idsdatanet.depot.core.DepotContext;
import com.idsdatanet.depot.core.mapping.DepotMappingModule;
import com.idsdatanet.depot.core.mapping.MappingAggregator;
import com.idsdatanet.depot.core.query.QueryResult;
import com.idsdatanet.depot.core.soap.SoapQuery;
import com.idsdatanet.depot.core.soap.SoapQueryDelegate;
import com.idsdatanet.depot.core.util.DepotUtils;
import com.idsdatanet.depot.witsml.WitsmlManager;
import com.idsdatanet.depot.witsml.dataobject.log.LogCurveDataSource;
import com.idsdatanet.depot.witsml.dataobject.log.LogDataBean;

/**
 * D2's version of Fixed Text Remark.
 * May refer to ModunetActivityCommandBean for ModuNet's implementation.
 * Note: This is a CommandBeanListener, the word Listener is missing
 */
public class FTRCommandBean extends ActivityCommandBeanListener implements ApplicationContextAware{
	
	public static final String ACTIVITY_REMARK_CLIENT_PARAMETER = "clientdefined";
	public static final String ACTIVITY_REMARK_HASH_CONSTANT = "hashconstant";
	public static final String STATUS_WARNING = "warning";
	public static final String STATUS_ERROR = "error";
	public static final String STATUS_INFO = "information";
	
	//intended to customise which code from activity screen will be used to filter (exact match)
	private List<String> codeList = null;
	private ApplicationContext applicationContext;
	private Boolean hasFtrData = false;
	
	protected class ResponseMessage {
		String status = "information";
		String message = "";
		
		public ResponseMessage (String status, String message) {
			this.status = status;
			this.message = message;
		}
		
		public String getStatus() {
			return this.status;
		}
		
		public void setStatus(String status) {
			this.status = status;
		}
		
		public String getMessage() {
			return this.message;
		}
		
		public void setMessage(String message) {
			this.message = message;
		}
		
	}
	protected List<ResponseMessage> responseMessages = new ArrayList<ResponseMessage>();
	protected Boolean dataToFtrFieldMatch = false; //for FTR WITSML: check if there is any data value for sourceFieldName from WITSML query
	protected Boolean isDayPlus = false;
	
	public void setResponseMessage(ResponseMessage message) {
		if(this.responseMessages == null) this.responseMessages = new ArrayList<ResponseMessage>();
		this.responseMessages.add(message);
	}
	
	public List<ResponseMessage> getResponseMessage(){
		return this.responseMessages;
	}
	
	public void setApplicationContext(ApplicationContext appContext) {
		this.applicationContext = appContext;
	}
	
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}

	public List<String> getCodeList() {
		return codeList;
	}
	
	public void setCodeList(List<String> codeList){
		this.codeList = codeList;
	}

	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		super.onCustomFilterInvoked(request, response, commandBean, invocationKey);
		
		//note:
		//onCustomFilterInvoked "collectCustomParameters" - when clicking the ">" button
		//onCustomFilterInvoked "collectFreeTextRemarks" - when clicking the "Retrieve FTR" button (if CustomCode is found and defined for ADM)
		//onSubmitForServerSideProcess - when clicking the "OK" button in the popup (last step, ready to populate remarks)
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		this.addWitsmlSettings(writer, UserSession.getInstance(request));
		
		if ("collectCustomParameters".equalsIgnoreCase(invocationKey)){
			this.collectCustomParameters(writer, request, response, commandBean, null);
		}
		
		if ("collectFreeTextRemarks".equalsIgnoreCase(invocationKey)){
			this.collectFreeTextRemarks(writer, request, response, commandBean, null);
		}
		
		if ("queryWitsmlForFtr".equalsIgnoreCase(invocationKey)){
			this.queryWitsmlForFtr(writer, request, response, commandBean, false);
		}
		
		if ("queryWitsmlForFtrCustom".equalsIgnoreCase(invocationKey)){
			this.queryWitsmlForFtr(writer, request, response, commandBean, true);
		}
		
		writer.endElement();
	 	writer.close();
	}
	
	@Override
	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		
		super.afterProcessFormSubmission(commandBean, request);
		
		//Modunet use only
		/*UserSession session = UserSession.getInstance(request);
		if(timeCodeList!=null && this.type !=null){
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			
			Double totalDuration = 0.00;
			Double activityDuration = 0.00;
			for (String phaseCode : this.timeCodeList) {
				String sql = "SELECT activityDuration FROM Activity " +
						"WHERE (isDeleted is null OR isDeleted=FALSE) AND (isSimop is null OR isSimop=FALSE) " +
						"AND dailyUid=:dailyUid"  + (this.type!=null?" AND "+ this.type + "=:phaseCode":"") ;
				String[] paramsFields = {"dailyUid", "phaseCode"};
				String[] paramsValues = {session.getCurrentDailyUid(), phaseCode};
				List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
				
				if(lstResult.size()>0){
					for (Object duration : lstResult){
						if(duration!=null){
							activityDuration = (Double) duration;
							thisConverter.setReferenceMappingField(Activity.class, "activityDuration");
							thisConverter.setBaseValue(activityDuration);
							activityDuration = thisConverter.getBasevalue();
							totalDuration += activityDuration;
						}
					}
					String[] paramsFields2 = {"totalDuration", "dailyUid"};
					Object[] paramsValues2 = {totalDuration, session.getCurrentDailyUid()};
					String strSql2 = "UPDATE ReportDaily SET drillingDuration = :totalDuration WHERE dailyUid = :dailyUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
				}
			}
		}*/
		
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(targetCommandBeanTreeNode!=null){
			super.onSubmitForServerSideProcess(commandBean, request, targetCommandBeanTreeNode);
			
			FTRUtils.checkAllowFTRButtonShow(targetCommandBeanTreeNode,UserSession.getInstance(request).getCurrentWellOnOffShore(),UserSession.getInstance(request).getCurrentOperationType());
			
			String autoPopulateDescription  = (String) request.getParameter("auto_populate_description");
			String autoPopulateDescriptionNewLine;
	
			if (StringUtils.isNotBlank(autoPopulateDescription)) {
				autoPopulateDescriptionNewLine = autoPopulateDescription.replaceAll(";",System.getProperty("line.separator"));
			} else {
				autoPopulateDescriptionNewLine  = autoPopulateDescription;
			}
	
			//user selected value from the "1st popup" if there is custom code
			String customCodeParamKey = null;
			if (request.getParameterValues("custom_code_param_key") != null) {
				customCodeParamKey = request.getParameter("custom_code_param_key");
			}
			String customCodeParamValue = null;
			if (request.getParameterValues("custom_code_param_value") != null) {
				customCodeParamValue = request.getParameter("custom_code_param_value");
			}
			String customCodeParamUid = null;
			if (request.getParameterValues("custom_code_param_uid") != null) {
				customCodeParamUid = request.getParameter("custom_code_param_uid");
			}
			
			
			if(targetCommandBeanTreeNode != null && targetCommandBeanTreeNode.getData()!=null){
				if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(Activity.class) || targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(NextDayActivity.class)){
					Object thisActivity =  targetCommandBeanTreeNode.getData();
	
					//this helps to clear the FTR, upon changing the values which affects FTR condition
					String targetedField = targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField();
					if ("classCode".equals(targetedField) || "phaseCode".equals(targetedField) || "jobTypeCode".equals(targetedField) || "taskCode".equals(targetedField) || "userCode".equals(targetedField)){
						PropertyUtils.setProperty(thisActivity, "additionalRemarks", "");
					}
					//note: this will be setting empty string, if user did not click on the FTR button (>), and just change e.g. Phase/Task code..
					//upon clicking on the FTR button (>) and there is something returned from FTR, this will set the desc 
					PropertyUtils.setProperty(thisActivity, "additionalRemarks", autoPopulateDescriptionNewLine);
					if (targetCommandBeanTreeNode.getDynaAttr().get("customDescriptionMatrixUid")!= null && StringUtils.isNotBlank(targetCommandBeanTreeNode.getDynaAttr().get("customDescriptionMatrixUid").toString())){
						PropertyUtils.setProperty(thisActivity, "customDescriptionMatrixUid", targetCommandBeanTreeNode.getDynaAttr().get("customDescriptionMatrixUid"));
					}else{
						PropertyUtils.setProperty(thisActivity, "customDescriptionMatrixUid", "");
					}
					
					targetCommandBeanTreeNode.getDynaAttr().put("customCodeParamKey", customCodeParamKey);
					targetCommandBeanTreeNode.getDynaAttr().put("customCodeParamValue", customCodeParamValue);
					targetCommandBeanTreeNode.getDynaAttr().put("customCodeParamUid", customCodeParamUid);
				}
			}
		}
	}
	
	/**
	 * "Retrieve FTR" button clicked, after a custom code dropdown selected 
	 * @param request
	 * @param response
	 * @param commandBean
	 * @throws Exception
	 */
	private void collectFreeTextRemarks(SimpleXmlWriter writer, HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, Object dataObject) throws Exception{
		Map<String, String> requestParams = this.getRequestParameters(request);
		UserSession session = UserSession.getInstance(request);
		String onOffShore = session.getCurrentWellOnOffShore();
		String operationType = session.getCurrentOperationType();
		
		List<ActivityDescriptionMatrix> remarkList = FTRUtils.formQueryForADM(onOffShore, operationType, requestParams.get("classCode"), requestParams.get("phaseCode"), requestParams.get("jobCode"), requestParams.get("taskCode"), requestParams.get("userCode"), this.codeList);
		ActivityDescriptionMatrix activityDescriptionMatrix = null;
		CustomDescriptionMatrix customDescriptionMatrix = null;
		
		HashMap<String, String> mapOfCustomCodeParameter = this.addElementOutputParameterArray(writer, request);
		
		if(remarkList != null && remarkList.size()>0){
			//pick first, will search cdm if required/any, instead of searching adm again for any condition (remarks field)
			activityDescriptionMatrix = remarkList.get(0);	
			customDescriptionMatrix = FTRUtils.getCDMList(activityDescriptionMatrix.getActivityDescriptionMatrixUid(), session.getCurrentOperationUid(), mapOfCustomCodeParameter);
			if(activityDescriptionMatrix != null){
				OperationUomContext opUomContext = new OperationUomContext(false, false);
		 		Map<String, Object> customParameterLink = null;
		 		
		 		//if activity free text remark has been entered before (i.e. currently editing existing record), else (prob new record or existing record w/o free text remark)
		 		if (StringUtils.isNotBlank(requestParams.get("activityFreeTextRemark"))){
	 				customParameterLink = this.getFieldFromActivityRemarks(requestParams.get("activityFreeTextRemark"));
		 		} else {
	 				customParameterLink = FTRUtils.getActivityRemarkCustomParameterValues(commandBean, requestParams.get("activityUid"),requestParams.get("sourceActivityUid"),"parameterKey", true, true, opUomContext);
		 		}
				
		 		this.addElementFreeTextRemarks(writer, activityDescriptionMatrix, customDescriptionMatrix);

	 			if (customDescriptionMatrix != null) {
	 				String strCustomSql = "from CustomParameterDetail where (isDeleted = false or isDeleted is null) and customDescriptionMatrixUid = :customDescriptionMatrixUid order by sequence";

	 				List<CustomParameterDetail> customParameterList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strCustomSql, "customDescriptionMatrixUid", customDescriptionMatrix.getCustomDescriptionMatrixUid());
	 				if (customParameterList != null && customParameterList.size() > 0) {
	 					int index=0;
	 					for(CustomParameterDetail customParameterDetail : customParameterList) {
	 						String paramType = customParameterDetail.getParamType();
	 						customParameterDetail.getCustomParameterDetailUid();

	 						if (ACTIVITY_REMARK_CLIENT_PARAMETER.equalsIgnoreCase(paramType) || ACTIVITY_REMARK_HASH_CONSTANT.equalsIgnoreCase(paramType) ) {
	 							this.addElementConstantDetail(writer, customParameterDetail, customParameterLink);
	 						} else {
	 							String value = this.getParamValue(session, dataObject, customParameterLink, customParameterDetail);
	 							this.addElementParamDetail(writer, customParameterDetail, index, session, activityDescriptionMatrix, value);
	 							index++;
	 						}
	 					}
	 				}
	 			}
			}
		}
	}
	
	private HashMap<String, String> addElementOutputParameterArray(SimpleXmlWriter writer, HttpServletRequest request) throws Exception {
		//array of parameter (custom code) passed back from the collect custom parameters
		String[] outputParameterArray = null;
		//array of values user selected for the parameter passed back from the collect custom paramters
		String[] outputValueArray = null;
		//customcodeuid of the parameter
		String[] outputCustomCodeUidArray = null;
		if (request.getParameterValues("outputParameterArray") != null) {
			outputParameterArray = (String[]) request.getParameterValues("outputParameterArray");
		}

		if (request.getParameterValues("outputValueArray") != null) {
			outputValueArray = (String[]) request.getParameterValues("outputValueArray");
		}
		
		if (request.getParameterValues("outputCustomCodeUidArray") != null) {
			outputCustomCodeUidArray = (String[]) request.getParameterValues("outputCustomCodeUidArray");
		}
		
		HashMap<String, String> mapOfCustomCodeParameter = new HashMap<String, String>();
		if (outputParameterArray != null && outputValueArray != null && outputParameterArray.length == outputValueArray.length) {
			for (int i = 0; i < outputParameterArray.length; i++) {
				String parameter = outputParameterArray[i];
				String value = outputValueArray[i];
				mapOfCustomCodeParameter.put(parameter, value);
			}
		}
		
		//if outputParameterArray is not null, send back
		writer.startElement("outputParameterArray");
		if (outputParameterArray != null) {
			writer.addElement("skip", "false");
			for (int i = 0; i < outputParameterArray.length; i++) {
				writer.startElement("item");
				writer.addElement("index", String.valueOf(i));
				writer.addElement("label", outputParameterArray[i]);
				if (outputValueArray != null && outputValueArray.length > i) {
					writer.addElement("value", outputValueArray[i]);	
				}
				else {
					writer.addElement("value", "");
				}
				
				if (outputCustomCodeUidArray != null && outputCustomCodeUidArray.length > i) {
					writer.addElement("customCodeUid", outputCustomCodeUidArray[i]);
				}
				else {
					writer.addElement("customCodeUid", "");
				}
				writer.endElement();
			}
		}
		else {
			writer.addElement("skip", "");
		}
		writer.endElement();
		
		return mapOfCustomCodeParameter;
	}
	
	private void addElementFreeTextRemarks(SimpleXmlWriter writer, ActivityDescriptionMatrix activityDescriptionMatrix, CustomDescriptionMatrix customDescriptionMatrix) throws Exception {
		String activityDescriptionMatrixUid = "";
		if (activityDescriptionMatrix != null) {
			activityDescriptionMatrixUid = activityDescriptionMatrix.getActivityDescriptionMatrixUid();
		}
		
		String customDescriptionMatrixUid = "";
		if (customDescriptionMatrix != null) {
			customDescriptionMatrixUid = customDescriptionMatrix.getCustomDescriptionMatrixUid();
		}

 		writer.startElement("freeTextRemarks");
 		writer.addElement("activityDescriptionMatrixUid", activityDescriptionMatrixUid);
 		writer.addElement("customDescriptionMatrixUid", customDescriptionMatrixUid);
		if (customDescriptionMatrix != null) {
			writer.addElement("remarks", customDescriptionMatrix.getRemarks());
		} else {
			writer.addElement("remarks", "");
		}
		writer.endElement();
	}
	
	private void addElementConstantDetail(SimpleXmlWriter writer, CustomParameterDetail customParameterDetail, Map<String, Object> customParameterLink) throws Exception {
		writer.startElement("constantDetail");
		writer.addElement("parameterDetailUid", customParameterDetail.getCustomParameterDetailUid());

		if (customParameterLink != null) {
			if(ACTIVITY_REMARK_CLIENT_PARAMETER.equalsIgnoreCase(customParameterDetail.getParamType())){
				writer.addElement("paramValue", this.getParameterLinkFieldValue(customParameterLink, customParameterDetail.getCustomParameterDetailUid()));
			}else{
				writer.addElement("paramValue", customParameterDetail.getParameterConstant());
			}
		} else {
			writer.addElement("paramValue", customParameterDetail.getParameterConstant());
		}
		writer.endElement();
	}
	
	private void addElementParamDetail(SimpleXmlWriter writer, CustomParameterDetail customParameterDetail, Integer sequence, UserSession session, ActivityDescriptionMatrix activityDescriptionMatrix, String paramValue) throws Exception {
		String uomSymbol = "";
		String tableName  = "";
		String fieldName = "";
		String lookupKey = "";
	 	String lookupKeyID = "";
		Class<?> hibernateClass = null;
		UOMMapping mapping = null;
		String customParameterDetailUid = customParameterDetail.getCustomParameterDetailUid();
		String paramType = customParameterDetail.getParamType();
		
		int outputPrecision = 0;
		String targetedFieldName = customParameterDetail.getTargetFieldName();
		String[] fieldToken = targetedFieldName.split("[.]");
		if (fieldToken.length==2){
			tableName = fieldToken[0];
			fieldName = fieldToken[1];
		}
		if (StringUtils.isNotBlank(tableName)) {
			String tableClassName = ApplicationUtils.getConfiguredInstance().getTablePackage() + CommonUtils.beanCapitalize(tableName);
			hibernateClass = Class.forName(tableClassName);

			mapping = UOMManager.getCurrentClassPropertyMapping(hibernateClass, fieldName);
			if (mapping !=null){
				uomSymbol = mapping.getUnitSymbol();
				outputPrecision = mapping.getUnitOutputPrecision();
			}
		}

		lookupKeyID ="";
		lookupKey = customParameterDetail.getLookupKey();
		
		if(StringUtils.isNotBlank(lookupKey)){
			//for common lookup key
			lookupKey = CommonLookupUtil.checkForCommonLookupKey(lookupKey);

			Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup(lookupKey,new UserSelectionSnapshot(session), null);

			if (lookupMap!=null){	
				lookupKeyID = customParameterDetailUid;

				SimpleAttributes atts = new SimpleAttributes();
				atts.addAttribute("id", lookupKeyID);
				writer.startElement("lookup", atts);

				for (String key:lookupMap.keySet()){
					LookupItem lookupItem = lookupMap.get(key);
					writer.startElement("item");
					writer.addElement("key", lookupItem.getKey());
					writer.addElement("label", lookupItem.getValue().toString());
					writer.endElement();
				}
				writer.endElement();
			}
		}

		writer.startElement("paramDetail");
		writer.addElement("sequence", String.valueOf(sequence));
		String dataType = FTRUtils.getCustomParameterDetailFieldType(customParameterDetailUid, customParameterDetail.getParameterKey());
		writer.addElement("parameterDetailUid", customParameterDetailUid);
		writer.addElement("paramType", paramType);
		writer.addElement("dataType", dataType);
		if ("double".equals(dataType)){
			writer.addElement("precision", String.valueOf(outputPrecision));
			if(StringUtils.isNotBlank(paramValue)) {
				NumberFormat numberFormat = NumberFormat.getInstance(Locale.ENGLISH);
				Number value = numberFormat.parse(paramValue).floatValue();
				paramValue = String.format(Locale.ENGLISH, "%."+outputPrecision+"f", value);
			}
		} else {
			writer.addElement("precision", "0");
		}
		writer.addElement("paramValue", paramValue);

		writer.addElement("lookupID", lookupKeyID);
		if (StringUtils.isNotBlank(lookupKeyID)){
			writer.addElement("uomSymbol", "");
		} else {
			writer.addElement("uomSymbol", uomSymbol);
		}
		writer.addElement("prefix", customParameterDetail.getPrecedingText() != null ? customParameterDetail.getPrecedingText() : "");
		writer.addElement("suffix", customParameterDetail.getSucceedingText() != null ? customParameterDetail.getSucceedingText() : "");
		writer.addElement("placeholder", customParameterDetail.getPlaceholderText() != null ? customParameterDetail.getPlaceholderText() : "");
		writer.endElement();
	} 
	
	private void addElementCustomParameter(SimpleXmlWriter writer, HttpServletRequest request, ActivityDescriptionMatrix activityDescriptionMatrix, String activityUid, String sourceActivityUid) throws Exception {
		Boolean hasCustomCode = activityDescriptionMatrix.getHasCustomCode() != null && activityDescriptionMatrix.getHasCustomCode().booleanValue();
		if (hasCustomCode) {
			List<CustomCode> customCodeList = new ArrayList<CustomCode>();
			if (StringUtils.isNotBlank(activityDescriptionMatrix.getCustomCodeGroupUid())) {
				String queryCustomGroup = "FROM CustomCode WHERE customCodeGroupUid = :customCodeGroupUid AND (isDeleted = FALSE or isDeleted is null)";
				
				List<CustomCode> bList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryCustomGroup, "customCodeGroupUid", activityDescriptionMatrix.getCustomCodeGroupUid()); 
				
				if (bList != null && bList.size() > 0) {
					customCodeList.addAll(bList);
				}
			}
			
			SimpleAttributes atts = new SimpleAttributes();
			atts.addAttribute("size", String.valueOf(customCodeList.size()));
			writer.startElement("customParameter",atts);
			for (int i = 0; i < customCodeList.size(); i++) {
				writer.startElement("customItem");
				writer.addElement("label", customCodeList.get(i).getLabel());
				writer.addElement("sequence", String.valueOf(customCodeList.get(i).getSequence()));
				writer.addElement("parameterKey", customCodeList.get(i).getParameterKey());
				//this part of code handles the targetFieldName (i.e. check if there's any UOM info which needs to be handled by custom code)
				String uomSymbol = "";
				int outputPrecision = 0;
				String targetedFieldName = customCodeList.get(i).getTargetFieldName();
				String[] fieldToken = targetedFieldName.split("[.]");
				String tableName = "";
				String fieldName = "";
				if (fieldToken.length==2){
					tableName = fieldToken[0];
					fieldName = fieldToken[1];
				}
				Class<?> hibernateClass = null;
				UOMMapping mapping = null;
				if (StringUtils.isNotBlank(tableName)) {
					String tableClassName = ApplicationUtils.getConfiguredInstance().getTablePackage() + CommonUtils.beanCapitalize(tableName);
					hibernateClass = Class.forName(tableClassName);

					mapping = UOMManager.getCurrentClassPropertyMapping(hibernateClass, fieldName);
					if (mapping !=null){
						uomSymbol = mapping.getUnitSymbol();
						outputPrecision = mapping.getUnitOutputPrecision();
					}
				}

				//this part of code handles the lookupkey (i.e. check if this should be a lookup)
				String lookupKeyID ="";
				String lookupKey = customCodeList.get(i).getLookupKey();

				if(StringUtils.isNotBlank(lookupKey)){
					//for common lookup key
					lookupKey = CommonLookupUtil.checkForCommonLookupKey(lookupKey);

					Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup(lookupKey,new UserSelectionSnapshot(UserSession.getInstance(request)), null);

					if(lookupMap!=null){	
						lookupKeyID = customCodeList.get(i).getCustomCodeUid();

						atts = new SimpleAttributes();
						atts.addAttribute("id", lookupKeyID);
						writer.startElement("custItemLookup", atts);

						for (String key:lookupMap.keySet()){
							LookupItem lookupItem = lookupMap.get(key);
							writer.startElement("item");
							writer.addElement("key", lookupItem.getKey());
							writer.addElement("label", lookupItem.getValue().toString());
							writer.endElement();
						}
						writer.endElement();
					}
				}
				
				List<CustomCodeLink> cclList = queryForCustomCodeLink(activityUid, sourceActivityUid, customCodeList.get(i).getCustomCodeUid());
				String originalValue = "";
				if (cclList != null && !cclList.isEmpty()) {
					CustomCodeLink ccl = cclList.get(0);
					originalValue = ccl.getParameterValue();
				}
				if (originalValue != null) {
					writer.addElement("originalValue", originalValue);
				}
				else {
					writer.addElement("originalValue", "");
				}
				String dataType = getDataType(customCodeList.get(i).getTargetFieldName());
				writer.addElement("customCodeUid", customCodeList.get(i).getCustomCodeUid());
				writer.addElement("paramType", customCodeList.get(i).getParamType()!= null?customCodeList.get(i).getParamType():"");
				writer.addElement("dataType", dataType);
				if ("double".equals(dataType)) {
					writer.addElement("precision", String.valueOf(outputPrecision));
				} else {
					writer.addElement("precision", "0");
				}
				if (StringUtils.isNotBlank(lookupKeyID)) {
					writer.addElement("uomSymbol", "");
				} else {
					writer.addElement("uomSymbol", uomSymbol);
				}
				writer.addElement("lookupKey", customCodeList.get(i).getLookupKey()!=null?customCodeList.get(i).getLookupKey().toString():"");
				writer.endElement();
			}

		} else {
			SimpleAttributes atts = new SimpleAttributes();
			atts.addAttribute("size", "0");
			writer.startElement("customParameter",atts);
			writer.endElement();
		}
		writer.endElement();
	}

	private Map<String, String> getRequestParameters(HttpServletRequest request) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("activityUid", (String) request.getParameter("activityUid"));
		params.put("activityFreeTextRemark", (String) request.getParameter("activityFreeTextRemark"));
		params.put("classCode", (String) request.getParameter("classCode"));
		params.put("phaseCode", (String) request.getParameter("phaseCode"));
		params.put("jobCode", (String) request.getParameter("jobTypeCode"));
		params.put("taskCode", (String) request.getParameter("taskCode"));
		params.put("userCode", (String) request.getParameter("userCode"));
		String sourceActivityUid = "";
		if (request.getParameter("sourceActivityUid") != null) {
			sourceActivityUid = (String) request.getParameter("sourceActivityUid");	
		}
		params.put("sourceActivityUid", sourceActivityUid);
		
		String customDescriptionMatrixUid = "";
		if (request.getParameter("customDescriptionMatrixUid") != null) {
			customDescriptionMatrixUid = (String) request.getParameter("customDescriptionMatrixUid");	
		}
		params.put("customDescriptionMatrixUid", customDescriptionMatrixUid);
		return params;
	}
	/**
	 * 
	 * @param request
	 * @param response
	 * @param commandBean
	 * @throws Exception
	 */
	private void collectCustomParameters(SimpleXmlWriter writer, HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, Object dataObject) throws Exception{
		Map<String, String> requestParameters = this.getRequestParameters(request);
		UserSession session = UserSession.getInstance(request);
		String onOffShore = session.getCurrentWellOnOffShore();
		String operationType = session.getCurrentOperationType();

		Boolean customDescriptionMatrixUid= false;
		List<CustomDescriptionMatrix> aList = null;
		ActivityDescriptionMatrix activityDescriptionMatrix = null;
		CustomDescriptionMatrix customDescriptionMatrix = null;
		
		if(requestParameters.get("activityUid")!=null && StringUtils.isNotBlank(requestParameters.get("activityUid"))){
			Activity activity = (Activity) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Activity.class, requestParameters.get("activityUid"));
			
			if(activity!=null && activity.getCustomDescriptionMatrixUid()!=null){
				if (StringUtils.isNotBlank(requestParameters.get("customDescriptionMatrixUid"))){
					aList = FTRUtils.queryCDM(requestParameters.get("customDescriptionMatrixUid"));
					customDescriptionMatrixUid = true;
				}
				
			}	
		}
		
		if(!customDescriptionMatrixUid){
			List<ActivityDescriptionMatrix> remarkList = FTRUtils.formQueryForADM(onOffShore, operationType, requestParameters.get("classCode"), requestParameters.get("phaseCode"), requestParameters.get("jobCode"), requestParameters.get("taskCode"), requestParameters.get("userCode"), this.codeList);
			
			if(remarkList != null && remarkList.size()>0){
				activityDescriptionMatrix = remarkList.get(0);
				
				if (activityDescriptionMatrix!=null){
					String queryCustom = "FROM CustomDescriptionMatrix WHERE activityDescriptionMatrixUid = :activityDescriptionMatrixUid AND (isDeleted = false or isDeleted is null)";
					aList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryCustom, "activityDescriptionMatrixUid", activityDescriptionMatrix.getActivityDescriptionMatrixUid());
				}
			}
		}
		
		boolean hasCustomCode = activityDescriptionMatrix!=null && activityDescriptionMatrix.getHasCustomCode() != null && activityDescriptionMatrix.getHasCustomCode().booleanValue();
		if (hasCustomCode) {
			this.addElementCustomParameter(writer, request, activityDescriptionMatrix, requestParameters.get("activityUid"), requestParameters.get("sourceActivityUid"));
		} else {
			OperationUomContext opUomContext = new OperationUomContext(false, false);
	 		Map<String, Object> customParameterLink = null;
	 		
	 		//if activity free text remark has been entered before (i.e. currently editing existing record), else (prob new record or existing record w/o free text remark)
	 		if (StringUtils.isNotBlank(requestParameters.get("activityFreeTextRemark"))){
	 			customParameterLink = this.getFieldFromActivityRemarks(requestParameters.get("activityFreeTextRemark"));	
	 		} else {
	 			customParameterLink = FTRUtils.getActivityRemarkCustomParameterValues(commandBean, requestParameters.get("activityUid"),requestParameters.get("sourceActivityUid"),"parameterKey",true,true, opUomContext);	
	 		} 		

			if (aList != null && aList.size() > 0) {
				//correct expected behaviour = only 1 result
				customDescriptionMatrix = (CustomDescriptionMatrix) aList.get(0);
				this.addElementFreeTextRemarks(writer, activityDescriptionMatrix, customDescriptionMatrix);
				
				String strSql = "from CustomParameterDetail where (isDeleted = false or isDeleted is null) and customDescriptionMatrixUid = :customDescriptionMatrixUid order by sequence";
				List<CustomParameterDetail> customParameterList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "customDescriptionMatrixUid", customDescriptionMatrix.getCustomDescriptionMatrixUid());
				if (customParameterList != null && customParameterList.size() > 0){
					int index = 0;
					for (CustomParameterDetail customParameterDetail : customParameterList) {
						String paramType = customParameterDetail.getParamType();
						if (ACTIVITY_REMARK_CLIENT_PARAMETER.equalsIgnoreCase(paramType) || ACTIVITY_REMARK_HASH_CONSTANT.equalsIgnoreCase(paramType)) {
							this.addElementConstantDetail(writer, customParameterDetail, customParameterLink);
						} else {
 							String value = this.getParamValue(session, dataObject, customParameterLink, customParameterDetail);
							this.addElementParamDetail(writer, customParameterDetail, index, session, activityDescriptionMatrix, value);
							index++;
						}
					}
				}
			}
		}
		
		SimpleAttributes atts = new SimpleAttributes();
		atts.addAttribute("size", "0");
		writer.startElement("customParameter",atts);
		writer.endElement();
		
	}
	
	private String getParameterLinkFieldValue(Map<String,Object>parameterLink,String id){
		if (parameterLink!=null) { 
			if (parameterLink.containsKey(id)) {
				Object object = parameterLink.get(id);
				if (object!=null) return object.toString();
			}
		}
		return "";
	}
	
	private HashMap<String,Object> getFieldFromActivityRemarks(String activityFreeTextRemark) throws Exception{
		
		if (StringUtils.isNotBlank(activityFreeTextRemark)){
							
			HashMap<String,Object> tableMap = new HashMap<String,Object>();
			
			String[] tokens = activityFreeTextRemark.split("::");

			for (int i=0;i<tokens.length;i++){
				String[] params = tokens[i].split(":=");
					
				if(params.length==1) continue;
				tableMap.put(params[0], params[1]);
				
			}	
			return tableMap;
		}
		return null;
	}
	
	private String getDataType(String targetFieldName) throws Exception {
		String[] fieldToken = targetFieldName.split("[.]");
		if (fieldToken.length==2) {
			String tableName = fieldToken[0];
			String fieldName = fieldToken[1];
			
			if (StringUtils.isNotBlank(tableName)) {
				String tableClassName = ApplicationUtils.getConfiguredInstance().getTablePackage() + CommonUtils.beanCapitalize(tableName);
				Class<?> hibernateClass = Class.forName(tableClassName);
				
				Type dataType= ApplicationUtils.getConfiguredInstance().getFieldTypeFromTable(hibernateClass, fieldName);
				String typeName = dataType.getReturnedClass().getName();
				if ((typeName.equalsIgnoreCase("java.lang.double"))|| (typeName.equalsIgnoreCase("java.lang.float"))) {
					return "double";
				} else if ((typeName.equalsIgnoreCase("java.lang.integer"))|| (typeName.equalsIgnoreCase("java.lang.long"))) {
					return "integer";
				}
			}
		}
		return "string";
	}
	
	
	
	private List<CustomCodeLink> queryForCustomCodeLink(String activityUid, String sourceActivityUid, String customCodeUid) throws Exception {
		List<CustomCodeLink> customCodeList = null;
		String query = "from CustomCodeLink where (isDeleted = false or isDeleted is null) AND activityUid = :activityUid AND customCodeUid = :customCodeUid";

		if (StringUtils.isNotBlank(activityUid) && StringUtils.isNotBlank(customCodeUid)) {
			customCodeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, new String[]{"activityUid", "customCodeUid"}, new String[]{activityUid, customCodeUid});
		} 
		if (customCodeList == null && StringUtils.isNotBlank(sourceActivityUid) && StringUtils.isNotBlank(customCodeUid)) {
			//from copy paste record
			query = "from CustomCodeLink where (isDeleted = false or isDeleted is null) AND activityUid = :activityUid AND customCodeUid = :customCodeUid";
			customCodeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, new String[]{"activityUid", "customCodeUid"}, new String[]{sourceActivityUid, customCodeUid});
		}
		return customCodeList;
	}
	
	private void addWitsmlSettings(SimpleXmlWriter writer, UserSession session) throws Exception {
		String isWitsmlFtr = "false";
		String queryDwom = "FROM DepotWellOperationMapping WHERE logPurpose = 'DP_FTR' AND operationUid = :operationUid AND (isDeleted IS NULL OR isDeleted = false)";
		List<?> results = (List<?>) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryDwom, "operationUid", session.getCurrentOperationUid());
		if(results.size() > 0 ) isWitsmlFtr = "true";
		
		writer.startElement("witsmlSettings");
		writer.addElement("isWitsmlFtr", isWitsmlFtr);	
		writer.endElement();
	}
	
	public class WitsmlLogCurve {
		SchedulerTemplate template;
		LogCurveDefinition logCurveDef = null;
		DepotWellOperationMapping wellMapping = null;
		LogCurveDataSource dataSource = null;
		
		public WitsmlLogCurve(LogCurveDefinition logCurveDef, DepotWellOperationMapping dwom, LogCurveDataSource dataSource, SchedulerTemplate template) throws Exception {
			this.logCurveDef = logCurveDef;
			this.dataSource = dataSource;
			this.dataSource.setLogCurveDefinition(logCurveDef);
			
			this.wellMapping = dwom;
			this.template = template;
		}
		
		public LogCurveDefinition getLogCurveDef() {
			return logCurveDef;
		}
		
		public void setLogCurveDef(LogCurveDefinition logCurveDef) {
			this.logCurveDef = logCurveDef;
		}
		
		public DepotWellOperationMapping getWellMapping() {
			return wellMapping;
		}
		
		public void setWellMapping(DepotWellOperationMapping wellMapping) {
			this.wellMapping = wellMapping;
		}

		public LogCurveDataSource getDataSource() {
			return dataSource;
		}

		public void setDataSource(LogCurveDataSource dataSource) {
			this.dataSource = dataSource;
		}

		public SchedulerTemplate getTemplate() {
			return template;
		}

		public void setTemplate(SchedulerTemplate template) {
			this.template = template;
		}		
	}
	
	private void queryWitsmlForFtr(SimpleXmlWriter writer, HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, Boolean hasCustomCode) throws Exception{
		SchedulerTemplate schedulerTemplate = null;
		LogCurveDefinition logCurveDefinition = null;
		this.responseMessages = null;
		this.dataToFtrFieldMatch = false;
		this.isDayPlus = Boolean.valueOf(request.getParameter("isDayPlus"));
		String startDatetime = (String) request.getParameter("startDatetime");
		String endDatetime = (String) request.getParameter("endDatetime");
		Boolean calculateRopAvg = true;
		//
		Map<String, String> requestParameters = this.getRequestParameters(request);
		UserSession session = UserSession.getInstance(request);
		String onOffShore = session.getCurrentWellOnOffShore();
		String operationType = session.getCurrentOperationType();
		
		List<ActivityDescriptionMatrix> remarkList = FTRUtils.formQueryForADM(onOffShore, operationType, requestParameters.get("classCode"), requestParameters.get("phaseCode"), requestParameters.get("jobCode"), requestParameters.get("taskCode"), requestParameters.get("userCode"), this.codeList);
		ActivityDescriptionMatrix activityDescriptionMatrix = (remarkList != null && remarkList.size() > 0) ? remarkList.get(0) : null;
		CustomDescriptionMatrix customDescriptionMatrix = null;
		if(activityDescriptionMatrix!=null) {
			String queryCustom = "FROM CustomDescriptionMatrix WHERE activityDescriptionMatrixUid = :activityDescriptionMatrixUid AND (isDeleted = false or isDeleted is null)";
			List<CustomDescriptionMatrix> aList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryCustom, "activityDescriptionMatrixUid", activityDescriptionMatrix.getActivityDescriptionMatrixUid());
			if (aList != null && aList.size() > 0) {
				customDescriptionMatrix = (CustomDescriptionMatrix) aList.get(0);
			}
		}
		
		Boolean isOnBottom = false;
		if(customDescriptionMatrix != null && customDescriptionMatrix.getOnBottom() != null) {
			isOnBottom = customDescriptionMatrix.getOnBottom();
		}
		
		try {
			if((StringUtils.isBlank(startDatetime) || StringUtils.isBlank(endDatetime)) 
					|| (StringUtils.equalsIgnoreCase(startDatetime, "NaN") || StringUtils.equalsIgnoreCase(endDatetime, "NaN"))) {
				this.setResponseMessage(new ResponseMessage(FTRCommandBean.STATUS_ERROR, "Activity start/end time is not valid."));
			}else if(Long.parseLong(startDatetime) <= Long.parseLong(endDatetime)) {
				String operationUid = session.getCurrentOperationUid();
				String sql = "FROM DepotWellOperationMapping WHERE (isDeleted IS NULL OR isDeleted = false) AND operationUid = :operationUid AND logPurpose = 'DP_FTR'";
				List<DepotWellOperationMapping> results = (List<DepotWellOperationMapping>) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", operationUid);
				if(results.size() > 0) {
					this.hasFtrData = false;
					List<DrillingParameters> drillingparams = new ArrayList<DrillingParameters>();
					for(DepotWellOperationMapping dwom : results) {
						schedulerTemplate = (SchedulerTemplate) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SchedulerTemplate.class, dwom.getSchedulerTemplateUid());
						if(schedulerTemplate != null) { 
							String dataObjects = schedulerTemplate.getDataObjects();
							String[] dataObjectList = dataObjects.split("[\t]");
							
							Boolean logFound = false;
							for (String dataObject : dataObjectList) {
								if (!logFound && dataObject.startsWith(WitsmlManager.WITSML_LOG_PREFIX)) {
									logFound = true;
									String[] args = dataObject.split("[.]");
									String logCurveDefUid = args[1];
									logCurveDefinition = (LogCurveDefinition)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LogCurveDefinition.class, logCurveDefUid);
									String queryMenmonics = "FROM LogMnemonicsMapping WHERE idsFieldName = 'rop_avg' AND (isDeleted IS NULL OR isDeleted = false) AND logCurveDefinitionUid = :logCurveDefUid";
									List<LogMnemonicsMapping> lstMnemonics = (List<LogMnemonicsMapping>) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryMenmonics, "logCurveDefUid", logCurveDefUid);
									if(lstMnemonics.size() > 0) calculateRopAvg = false;
									if(logCurveDefinition != null) { 
										LogCurveDataSource dataSource = (LogCurveDataSource)this.getApplicationContext().getBean(logCurveDefinition.getLogSoapQueryId());
										DrillingParameters dp = queryWitsmlLog(new WitsmlLogCurve(logCurveDefinition, dwom, dataSource, schedulerTemplate), request, response, commandBean, session, startDatetime, endDatetime, isOnBottom);
										if(dp != null) drillingparams.add(dp);
									} 
								} 
							}
							if(!logFound) {
								this.setResponseMessage(new ResponseMessage(FTRCommandBean.STATUS_WARNING, "No Log Mnemonics settings found."));
							} 
						} else {
							
						}
					}
					if(drillingparams.size() > 0) {
						//take 1st object as template, then overwritten by following objects if properties has not null values
						Object mainDp = drillingparams.get(0);
						for(int i = 1; i < drillingparams.size(); i++) {
							DrillingParameters obj = (DrillingParameters) drillingparams.get(i);
							Map<String, Object> sourceProperties = PropertyUtils.describe(obj);
							for (Map.Entry<String, Object> entry : sourceProperties.entrySet()) 
							{
								if (PropertyUtils.isWriteable(mainDp, entry.getKey()) && entry.getValue() != null)
								{
									PropertyUtils.setProperty(mainDp, entry.getKey(), entry.getValue());
								}
							}
						}
						
						//calculate and set all as base value into object
						if(calculateRopAvg) {
							Double progress = 0.0;
							Double avgRop = 0.0;
							Double duration = 0.0;
							DrillingParameters drillingParameters = (DrillingParameters) mainDp;
							String witsmlUomTemplate = DepotUtils.getConfiguredInstance().getDefaultUomTemplateUidByDepotType(DepotConstants.WITSML);
							CustomFieldUom thisConverter = new CustomFieldUom(session.getUserLocale(), DrillingParameters.class, "duration", 0.0, witsmlUomTemplate);
							
							if(!isOnBottom) {
								//calculate duration as base value
								Long dpDuration = CommonUtil.getConfiguredInstance().calculateDuration(drillingParameters.getStartTime(), drillingParameters.getEndTime());
								if (dpDuration != null) {
									thisConverter = new CustomFieldUom(session.getUserLocale(), DrillingParameters.class, "duration", 0.0, witsmlUomTemplate);
									thisConverter.setBaseValue(dpDuration);
									PropertyUtils.setProperty(drillingParameters, "duration", (thisConverter.getConvertedValue()));
									duration = (Double) PropertyUtils.getProperty(drillingParameters, "duration");
								}
							} else {
								duration = drillingParameters.getDuration();
							}
							
							if((duration != null && duration > 0) && (drillingParameters.getDepthTopMdMsl() != null && drillingParameters.getDepthMdMsl() != null)) {
								//calculate progress as base value
								Double depthTopMdMsl = (Double) PropertyUtils.getProperty(drillingParameters, "depthTopMdMsl");
								thisConverter.setReferenceMappingField(DrillingParameters.class, "depthTopMdMsl");
								thisConverter.setBaseValueFromUserValue(depthTopMdMsl, false);
								depthTopMdMsl = thisConverter.getBasevalue();
								
								Double depthMdMsl = (Double) PropertyUtils.getProperty(drillingParameters, "depthMdMsl");
								thisConverter.setReferenceMappingField(DrillingParameters.class, "depthMdMsl");
								thisConverter.setBaseValueFromUserValue(depthMdMsl, false);
								depthMdMsl = thisConverter.getBasevalue();
								
								progress = depthMdMsl - depthTopMdMsl;
								thisConverter.setReferenceMappingField(DrillingParameters.class, "progress");
								thisConverter.setBaseValue(progress);
								drillingParameters.setProgress(thisConverter.getConvertedValue());
								
								//calculate average rop as base value
								thisConverter.setReferenceMappingField(DrillingParameters.class, "duration");
								thisConverter.setBaseValueFromUserValue(drillingParameters.getDuration());
								duration = thisConverter.getBasevalue();
								
								if (duration > 0 && progress > 0){
									avgRop = progress / duration;	
								}

								thisConverter.setReferenceMappingField(DrillingParameters.class, "ropAvg");
								thisConverter.setBaseValue(avgRop);
								drillingParameters.setRopAvg(thisConverter.getConvertedValue());
							}
						}
						
						if(hasCustomCode) this.collectFreeTextRemarks(writer, request, response, (BaseCommandBean) commandBean, mainDp);
						else this.collectCustomParameters(writer, request, response, (BaseCommandBean) commandBean, mainDp);
					} else {
						this.setResponseMessage(new ResponseMessage(FTRCommandBean.STATUS_INFO, "No data available for the specified time range."));
					}
				} else {
					this.setResponseMessage(new ResponseMessage(FTRCommandBean.STATUS_WARNING, "WITSML Well/Ops mapping not found."));
				}
			} else {
				this.setResponseMessage(new ResponseMessage(FTRCommandBean.STATUS_ERROR, "Activity start/end time is not valid."));
			}
		} catch (Exception e) {
			this.setResponseMessage(new ResponseMessage(FTRCommandBean.STATUS_ERROR, "Error: " + e.getMessage()));
		} finally {
			this.addElementResponseMessage(writer);
		}
	}
	
	private DrillingParameters queryWitsmlLog(WitsmlLogCurve witsmlLogCurve, HttpServletRequest request, HttpServletResponse response, CommandBean commandBean, UserSession session, String startDatetime, String endDatetime, Boolean onBottom) throws Exception {
		SchedulerTemplate schedulerTemplate = witsmlLogCurve.getTemplate();
		DepotWellOperationMapping dwom = witsmlLogCurve.getWellMapping();
		LogCurveDataSource dataSource = witsmlLogCurve.getDataSource();
		ImportExportServerProperties serverProperties = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, schedulerTemplate.getImportExportServerPropertiesUid());
		MappingAggregator mappingAggregator = DepotMappingModule.getConfiguredInstance().getMappingAggregator(DepotConstants.WITSML, dataSource.getMappingAggregatorId());
		
		QueryResult qResult = new QueryResult();
		qResult.setDepotType(DepotConstants.WITSML);
		qResult.setDepotVersion(mappingAggregator.getStoreVersion());
		qResult.setTransType(DepotConstants.GET_FROM_STORE);
		qResult.setClientId(DepotUtils.SCHEDULER_USER_AGENT);
		qResult.setDepotObjectId(mappingAggregator.getDataObjectId());
		
		DepotContext depotContext = new DepotContext(null, mappingAggregator.getStoreVersion(), mappingAggregator.getDefaultNamespace());
		depotContext.setWellUid(dwom.getDepotWellUid());
		depotContext.setWellboreUid(dwom.getDepotWellboreUid());
		Date dayDate = session.getCurrentDaily().getDayDate();
		if(this.isDayPlus) {
			dayDate.setDate(dayDate.getDate() + 1);
		}
		int offset = DepotUtils.getConfiguredInstance().getWellTimeZoneByWellUid(dwom.getWellUid()).getRawOffset();
		depotContext.setStartDayDateTime(xmlDatetimeWithOffset(dayDate, new Date(Long.parseLong(startDatetime)), offset));
		depotContext.setEndDayDateTime(xmlDatetimeWithOffset(dayDate, new Date(Long.parseLong(endDatetime)), offset));
		depotContext.setOnBottom(onBottom);
		
		if (dataSource instanceof SoapQuery) {
			final List<Object> records = new ArrayList<Object>();
			SoapQuery soapQuery = (SoapQuery)dataSource;
			soapQuery.setJobProperties(serverProperties, mappingAggregator, dwom, dayDate, depotContext, schedulerTemplate.getSchedulerType(), mappingAggregator.getDataObjectId());
			soapQuery.invoke(qResult, new SoapQueryDelegate() {
				public void beforeDataSave(Object logDataBean) {
					LogDataBean dataBean = (LogDataBean)logDataBean;
					DrillingParameters drillingParameters = (DrillingParameters)dataBean.getData();
					records.add(drillingParameters);
				}
				public void afterDataSave(Object drillingParameters) {
					// Do nothing
				}
				public boolean canSave() {
					return false;
				}
			});
			//reset to current main template after witsml query
			UserUOMSelection.getThreadLocalInstance().synchronizeWith(UserSession.getInstance(request));
			if(records.size() > 0) return (DrillingParameters)records.get(0);
		} else {
			commandBean.getSystemMessage().addError("Log Curve Data Source is not an instance of SoapQuery.");
			this.setResponseMessage(new ResponseMessage(FTRCommandBean.STATUS_ERROR, "Log Curve Data Source is not an instance of SoapQuery."));
		}
		return null;
	}
	
	private String xmlDatetimeWithOffset(Date date, Date time, int offset) throws Exception {
		XMLGregorianCalendar gcDate = DepotUtils.getConfiguredInstance().getXmlGregorianCalendarByDate(date);
		
		XMLGregorianCalendar gcTime = DepotUtils.getConfiguredInstance().getXmlGregorianCalendarByDate(time);
		gcDate.setHour(gcTime.getHour());
		gcDate.setMinute(gcTime.getMinute());
		gcDate.setSecond(gcTime.getSecond());
		gcDate.setMillisecond(gcTime.getMillisecond());

	    int miliseconds = offset;
	    int minutes = miliseconds / 1000 / 60;
	    int h = minutes / 60;
	    int m = minutes % 60;
	    String timezone = (miliseconds < 0 ? "" : "+") + String.format("%02d:%02d", h, m);
	    return gcDate.toString().replace("Z", timezone);
	}
	
	private String getParamValue(UserSession session, Object dataObject, Map<String, Object> customParameterLink,CustomParameterDetail customParameterDetail) throws ClassNotFoundException, Exception {
		String value = "";
		if(dataObject != null && StringUtils.isNotBlank(customParameterDetail.getSourceFieldName())) {
			String hql = "FROM UomTemplate WHERE name='WITSML' AND (isDeleted IS NULL OR isDeleted = false)";
			List <UomTemplate> uomTemplates = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
			UomTemplate witsmlTemplate = uomTemplates.get(0);
			
			String sourceFieldName = customParameterDetail.getSourceFieldName();
			String targetFieldName = customParameterDetail.getTargetFieldName();
			if(StringUtils.isNotBlank(sourceFieldName)) {
				String[] sourceToken = sourceFieldName.split("[.]");
				String[] targetToken = targetFieldName.split("[.]");
				if(StringUtil.isEqualIgnoreCase(sourceToken[0], dataObject.getClass().getSimpleName())) {
					String sourceField = sourceToken[1];
					Object val = PropertyUtils.getProperty(dataObject, sourceField);
					if(val != null && val instanceof Double) {
						CustomFieldUom targetConverter = new CustomFieldUom(session.getUserLocale(), Class.forName("com.idsdatanet.d2.core.model." + targetToken[0]), targetToken[1]);
						CustomFieldUom sourceConverter = new CustomFieldUom(session.getUserLocale(), Class.forName("com.idsdatanet.d2.core.model." + sourceToken[0]), sourceToken[1]);
						sourceConverter.setBaseValueFromUserValue(Double.parseDouble(val.toString()),false);
						targetConverter.setBaseValue(sourceConverter.getBasevalue());
						val = targetConverter.getConvertedValue(); 
						value = val.toString();
					}
					this.dataToFtrFieldMatch = true;
				}
			}
		} else {
			value = this.getParameterLinkFieldValue(customParameterLink, customParameterDetail.getCustomParameterDetailUid());
		}
		
		return value;
	}
	
	private void addElementResponseMessage(SimpleXmlWriter writer) throws Exception {
		writer.startElement("response");
		if((this.responseMessages==null || this.responseMessages.size() <=0) && !this.dataToFtrFieldMatch) {
			this.setResponseMessage(new ResponseMessage(FTRCommandBean.STATUS_INFO, "No data available for the specified time range."));
		}
		if(this.getResponseMessage() != null) {
			for(ResponseMessage message : this.responseMessages) {
				writer.startElement("message");
				writer.addElement("status", message.getStatus());
				writer.addElement("responseMessage", message.getMessage());	
				writer.endElement();
			}
		}
		writer.endElement();
	}
}
