package com.idsdatanet.d2.ftr.activityDescriptionMatrix;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.ActivityDescriptionMatrix;
import com.idsdatanet.d2.core.model.CustomDescriptionMatrix;
import com.idsdatanet.d2.core.model.CustomParameterDetail;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.commonLookup.CommonLookupUtil;

public class FTRActivityDescriptionMatrixDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {

	private List<String> inputList;
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private static final String KPI = "KPI";
	
	private Boolean isRsd = false;
	
	public void setIsRsd(Boolean value){
		this.isRsd = value;
	}
	
	public List<String> getInputList() {
		return inputList;
	}
	
	public void setInputList(List<String> inputList){
		this.inputList = inputList;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj != null && obj instanceof ActivityDescriptionMatrix){
			ActivityDescriptionMatrix activityDescriptionMatrix = (ActivityDescriptionMatrix) obj;
			
			if (StringUtils.isNotBlank(activityDescriptionMatrix.getCustomCodeGroupUid())) {
				activityDescriptionMatrix.setHasCustomCode(Boolean.TRUE);
			} else {
				activityDescriptionMatrix.setHasCustomCode(Boolean.FALSE);
			}
			
			String onOffShore = activityDescriptionMatrix.getOnOffShore();
			String operationCode = activityDescriptionMatrix.getOperationCode();
			String classCode = activityDescriptionMatrix.getClassShortCode();
			String phaseCode = activityDescriptionMatrix.getPhaseShortCode();
			String jobTypeCode = activityDescriptionMatrix.getJobTypeShortCode();
			String taskCode = activityDescriptionMatrix.getTaskShortCode();
			String condition = activityDescriptionMatrix.getCondition();
			String matrixUid = activityDescriptionMatrix.getActivityDescriptionMatrixUid();
			
			if (this.inputList != null) {
				List<String> paramNames = new ArrayList<String>();
				List<Object> values = new ArrayList<Object>();
				String queryString = "from ActivityDescriptionMatrix where (isDeleted = false or isDeleted is null) ";

				if (this.inputList.contains("onOffShore")) {
					queryString += " AND (onOffShore = :onOffShore)";
					paramNames.add("onOffShore");
					values.add(onOffShore);
				} else {
					queryString += " AND (onOffShore = '' or onOffShore is null)";
				}

				if (this.inputList.contains("operationCode")) {
					queryString += " AND (operationCode = :operationCode)";
					paramNames.add("operationCode");
					values.add(operationCode);
				} else {
					queryString += " AND (operationCode = '' or operationCode is null)";
				}

				if (this.inputList.contains("classShortCode")) {
					queryString += " AND (classShortCode = :classShortCode)";
					paramNames.add("classShortCode");
					values.add(classCode);
				} else {
					queryString += " AND (classShortCode = '' or classShortCode is null)";
				}

				if (this.inputList.contains("phaseCode")) {
					queryString += " AND (phaseShortCode = :phaseShortCode)";
					paramNames.add("phaseShortCode");
					values.add(phaseCode);
				} else {
					queryString += " AND (phaseShortCode = '' or phaseShortCode is null)";
				}

				if (this.inputList.contains("jobTypeCode")) {
					queryString += " AND (jobTypeShortCode = :jobTypeShortCode)";
					paramNames.add("jobTypeShortCode");
					values.add(jobTypeCode);
				} else {
					queryString += " AND (jobTypeShortCode = '' or jobTypeShortCode is null)";
				}

				if (this.inputList.contains("taskCode")) {
					queryString += " AND (taskShortCode = :taskShortCode)";
					paramNames.add("taskShortCode");
					values.add(taskCode);
				} else {
					queryString += " AND (taskShortCode = '' or taskShortCode is null)";
				}

				if (this.inputList.contains("condition")) {
					queryString += " AND (condition = :condition)";
					paramNames.add("condition");
					values.add(condition);
				} else {
					queryString += " AND (condition = '' or condition is null)";
				}



				if (StringUtils.isNotBlank(activityDescriptionMatrix.getActivityDescriptionMatrixUid())){
					queryString += " and activityDescriptionMatrixUid <> :activityDescriptionMatrixUid";
					if (this.inputList.contains("onOffShore")) {
						paramNames.add("onOffShore");
						values.add(onOffShore);
					}
					if (this.inputList.contains("operationCode")) {
						paramNames.add("operationCode");
						values.add(operationCode);
					}
					if (this.inputList.contains("classShortCode")) {
						paramNames.add("classShortCode");
						values.add(classCode);
					}
					if (this.inputList.contains("phaseCode")) {
						paramNames.add("phaseShortCode");
						values.add(phaseCode);
					}
					if (this.inputList.contains("jobTypeCode")) {
						paramNames.add("jobTypeShortCode");
						values.add(jobTypeCode);
					}
					if (this.inputList.contains("taskCode")) {
						paramNames.add("taskShortCode");
						values.add(taskCode);
					}
					if (this.inputList.contains("condition")) {
						paramNames.add("condition");
						values.add(condition);
					}

					paramNames.add("activityDescriptionMatrixUid");
					values.add(matrixUid);
				}
				List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, values);

				if(result.size()>0){
					status.addWarning("You have entered invalid data or have not filled in a required field. Please correct the error and try again");
					status.setContinueProcess(false, true);
				}
			}
			Boolean isUnique = validateUniqueCombinationCodes(activityDescriptionMatrix);
			if (isUnique != null && !isUnique) {
				status.addWarning("Duplicate combination of Activity Description Matrix codes is not allowed");
				status.setContinueProcess(false, true);
			}
			
			Boolean checkValidCDM = checkIfCustomGroupHasNoCondition(node);
			if (checkValidCDM != null && !checkValidCDM) {
				status.addWarning("Activity Description Matrix with Custom Code Group need to have Custom Codes Combination to differentiate the descriptions");
				status.setContinueProcess(false, true);
			}
			
		} else if (obj != null && obj instanceof CustomDescriptionMatrix) {
			Boolean checkTargetFieldKeyFilled = checkIfTargetFieldKeyIsFilled(node);
			if (checkTargetFieldKeyFilled != null && !checkTargetFieldKeyFilled) {
				status.addWarning("Target Field Name must be filled for Description Matrix when Analyze By is not 'KPI'");
				status.setContinueProcess(false, true);
			}
			
			Boolean checkCodeIsFilled = checkIfCodeIsFilled(node);
			if (checkCodeIsFilled != null && !checkCodeIsFilled) {
				status.addWarning("Code field is mandatory when Analyze By field is not blank");
				status.setContinueProcess(false, true);
			}
			
			Boolean checkValidCDM = checkIfCustomGroupHasNoCondition(node);
			if (checkValidCDM != null && !checkValidCDM) {
				status.addWarning("Activity Description Matrix with Custom Code Group need to have Custom Codes Combination to differentiate the descriptions");
				status.setContinueProcess(false, true);
			}

			String[] checkCodeIsUnique = checkIfCodeIsUnique(node);
			if (checkCodeIsUnique != null) {
				String error = "";
				if (checkCodeIsUnique.length == 3) {
					error = "Code " + checkCodeIsUnique[0] + " is already used for " + checkCodeIsUnique[1];
					if (StringUtils.isNotBlank(checkCodeIsUnique[2])) {
						error += " and Custom Codes Combination " + checkCodeIsUnique[2];
					}
				}
				status.addWarning("Code field must be unique for the same FTR description." + System.lineSeparator() + error);
				status.setContinueProcess(false, true);
			}
			if (commandBean.getBeanName().equals("activityDescriptionMatrixRsdCommandBean")) {
				CustomDescriptionMatrix customDescriptionMatrix = (CustomDescriptionMatrix) obj;
				customDescriptionMatrix.setForRsd(true);
			}
			
		} else if (obj != null && obj instanceof CustomParameterDetail){
			CustomParameterDetail customParameterDetail = (CustomParameterDetail) obj;
			if ("clientdefined".equalsIgnoreCase(customParameterDetail.getParamType()) || ("hashconstant".equalsIgnoreCase(customParameterDetail.getParamType()))){
				customParameterDetail.setLookupKey("");
			}else{
				customParameterDetail.setParameterConstant("");
			}
			
			if (customParameterDetail.getTargetFieldName()!= null){
				String targetFieldName = customParameterDetail.getTargetFieldName();
				String lookupKey = customParameterDetail.getLookupKey();
				String errMsg = this.validateTableField(targetFieldName); 
				errMsg += this.validateLookupKey(commandBean, session, lookupKey);
				if (StringUtils.isNotBlank(errMsg)){
					status.addError(errMsg);
					status.setContinueProcess(false, true);
				}
			}
		}
	}
	
	private String validateLookupKey(CommandBean commandBean,UserSession session, String lookupKey){
		String msg = "";
		
		if (StringUtils.isNotBlank(lookupKey)){
 			 
			 try {
				Map<String, LookupItem> lookup=null;
				//for common lookup key
				lookupKey = CommonLookupUtil.checkForCommonLookupKey(lookupKey);
				lookup = LookupManager.getConfiguredInstance().getLookup(lookupKey, new UserSelectionSnapshot(session), null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				msg = "Invalid lookup key :" + lookupKey + "\n";
			}
		}
		return msg;
	}
	
	private String validateTableField(String tableField){
		String msg = "";
		
		if (tableField.trim().length()>0){
			int pos = tableField.indexOf(".");
			if (pos > 0){
				String tableName = tableField.substring(0, pos);
				String fieldName = tableField.substring(pos+1);
				
				String condition = " where " + fieldName + " is null";
				String sql = "select " + fieldName + " from " + tableName + condition;
				try {
					//ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					msg ="Invalid " + tableField;
				}
				
			}else{
				msg = "Require [table].[field] format for targeted field name : " + tableField;
			}
		}
		return msg;
	}
	
	private Boolean validateUniqueCombinationCodes(ActivityDescriptionMatrix adm) throws Exception{
		Boolean isUnique = null;
		List<String> paramNames = new ArrayList<String>();
		List<Object> values = new ArrayList<Object>();
		
		int hasCriteria = 0;
		
		String queryString = "SELECT count(*) FROM ActivityDescriptionMatrix WHERE (isDeleted = false or isDeleted is null)";
		
		queryString += adm.getOnOffShore() != null ? " AND onOffShore = :onOffShore" : " AND (onOffShore is null OR onOffShore = '')";
		queryString += adm.getOperationCode() != null ? " AND operationCode = :operationCode" : " AND (operationCode is null OR operationCode = '')";
		queryString += adm.getClassShortCode() != null ? " AND classShortCode = :classShortCode" : " AND (classShortCode is null OR classShortCode = '')";
		queryString += adm.getPhaseShortCode() != null ? " AND phaseShortCode = :phaseShortCode" : " AND (phaseShortCode is null OR phaseShortCode = '')";
		queryString += adm.getJobTypeShortCode() != null ? " AND jobTypeShortCode = :jobTypeShortCode" : " AND (jobTypeShortCode is null OR jobTypeShortCode = '')";
		queryString += adm.getTaskShortCode() != null ? " AND taskShortCode = :taskShortCode" : " AND (taskShortCode is null OR taskShortCode = '')";
		queryString += adm.getUserShortCode() != null ? " AND userShortCode = :userShortCode" : " AND (userShortCode is null OR userShortCode = '')";
		if (adm.getActivityDescriptionMatrixUid() != null && StringUtils.isNotBlank(adm.getActivityDescriptionMatrixUid())){
			//exclude own record previously created when validating
			queryString += " AND activityDescriptionMatrixUid <> :activityDescriptionMatrixUid";
		}
		
		if (adm.getOnOffShore() != null) {
			paramNames.add("onOffShore");
			values.add(adm.getOnOffShore());
			hasCriteria++;
		}
		if (adm.getOperationCode() != null) {
			paramNames.add("operationCode");
			values.add(adm.getOperationCode());
			hasCriteria++;
		}
		if (adm.getClassShortCode() != null) {
			paramNames.add("classShortCode");
			values.add(adm.getClassShortCode());
			hasCriteria++;
		}
		if (adm.getPhaseShortCode() != null) {
			paramNames.add("phaseShortCode");
			values.add(adm.getPhaseShortCode());
			hasCriteria++;
		}
		if (adm.getJobTypeShortCode() != null) {
			paramNames.add("jobTypeShortCode");
			values.add(adm.getJobTypeShortCode());
			hasCriteria++;
		}
		if (adm.getTaskShortCode() != null) {
			paramNames.add("taskShortCode");
			values.add(adm.getTaskShortCode());
			hasCriteria++;
		}
		if (adm.getUserShortCode() != null) {
			paramNames.add("userShortCode");
			values.add(adm.getUserShortCode());
			hasCriteria++;
		}
		if (adm.getActivityDescriptionMatrixUid() != null && StringUtils.isNotBlank(adm.getActivityDescriptionMatrixUid())){
			paramNames.add("activityDescriptionMatrixUid");
			values.add(adm.getActivityDescriptionMatrixUid());
			hasCriteria++;
		}

		List result = null;
		
		if (hasCriteria > 0) {
			result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, values);
		}
		else {
			result = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);
		}
		
		if (result != null && result.size() > 0) {
			Long count = Long.parseLong(result.get(0).toString());
			if (count > 0) {
				isUnique = false;
			}
		}
		
		return isUnique;
	}
	
	/*
	 * node expected - ActivityDescriptionMatrix instance
	 */
	private Boolean checkIfNoCustomGroupHasMultipleDescription(CommandBeanTreeNode node) throws Exception{
		Boolean isValid = true;
		
		if (node.getData() != null && node.getData() instanceof ActivityDescriptionMatrix) {
			ActivityDescriptionMatrix adm = (ActivityDescriptionMatrix) node.getData();
			Map map = null;
			
			if (StringUtils.isBlank(adm.getCustomCodeGroupUid())) {
				if (node.getChild("CustomDescriptionMatrix") != null) {
					map = node.getChild("CustomDescriptionMatrix");
					if (map.size() > 1) {
						isValid = false;
					}
				}
			}
		}
		
		return isValid;
	}
	
	/*
	 * node expected - ActivityDescriptionMatrix instance
	 */
	private Boolean checkIfCustomGroupHasNoCondition(CommandBeanTreeNode node) throws Exception {
		Boolean isValid = true;
		
		if (node.getData() != null && node.getData() instanceof ActivityDescriptionMatrix) {
			ActivityDescriptionMatrix adm = (ActivityDescriptionMatrix) node.getData();
			Map<String, CommandBeanTreeNode> map = null;
			
			
			if (StringUtils.isNotBlank(adm.getCustomCodeGroupUid())) {
				if (node.getChild("CustomDescriptionMatrix") != null) {
					map = node.getChild("CustomDescriptionMatrix");
					CustomDescriptionMatrix cdm = null;
					CommandBeanTreeNode childNode = null;
					for (Map.Entry<String, CommandBeanTreeNode> entry : map.entrySet()) {
						childNode = entry.getValue();
						if (childNode.getData() != null && childNode.getData() instanceof CustomDescriptionMatrix) {
							cdm = (CustomDescriptionMatrix) childNode.getData();
							if (StringUtils.isBlank(cdm.getCondition())) {
								isValid = false;
								return isValid;
							}
						}
					}
				}
			}
		} else if (node.getData() != null && node.getData() instanceof CustomDescriptionMatrix) {
			CustomDescriptionMatrix cdm = (CustomDescriptionMatrix) node.getData();
			
			String sql = "FROM ActivityDescriptionMatrix WHERE (isDeleted = FALSE or isDeleted is null) AND activityDescriptionMatrixUid = :activityDescriptionMatrixUid";
			
			//note this fails to trigger the checking when it is a new CDM being created
			if (StringUtils.isNotBlank(cdm.getActivityDescriptionMatrixUid())) {

				List<ActivityDescriptionMatrix> admList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "activityDescriptionMatrixUid", cdm.getActivityDescriptionMatrixUid());
				if (admList != null && admList.size() > 0) {
					ActivityDescriptionMatrix adm = admList.get(0);
					if (StringUtils.isNotBlank(adm.getCustomCodeGroupUid())) {
						if (StringUtils.isBlank(cdm.getCondition())) {
							isValid = false;
							return isValid;
						}
					}
				}

			}
		}
		
		return isValid;
	}
	
	/*
	 * node expected - CustomDescriptionMatrix instance
	 */
	private Boolean checkIfTargetFieldKeyIsFilled(CommandBeanTreeNode node) throws Exception {
		Boolean isValid = true;
		
		Map<String, CommandBeanTreeNode> secondMap = null;
		CustomDescriptionMatrix cdm = null;
		
		if (node.getData() != null && node.getData() instanceof CustomDescriptionMatrix) {
			cdm = (CustomDescriptionMatrix) node.getData();
			CustomParameterDetail cpd = null;
			CommandBeanTreeNode childNode = null;
			if (!KPI.equals(cdm.getAnalysisType())) {
				secondMap = node.getChild("CustomParameterDetail");

				for (Map.Entry<String, CommandBeanTreeNode> childEntry : secondMap.entrySet()) {
					childNode = childEntry.getValue();

					if (childNode.getData() != null && childNode.getData() instanceof CustomParameterDetail) {
						cpd = (CustomParameterDetail) childNode.getData();

						if (StringUtils.isBlank(cpd.getTargetFieldName())) {
							isValid = false;
							return isValid;
						}
					}
				}
			}
		}
		
		return isValid;
	}
	
	/*
	 * node expected - CustomDescriptionMatrix instance
	 */
	private Boolean checkIfCodeIsFilled(CommandBeanTreeNode node) throws Exception {
		Boolean isValid = true;
		
		CustomDescriptionMatrix cdm = null;
		
		if (node.getData() != null && node.getData() instanceof CustomDescriptionMatrix) {
			cdm = (CustomDescriptionMatrix) node.getData();
			if (StringUtils.isNotBlank(cdm.getAnalysisType()) && StringUtils.isBlank(cdm.getRemarksCode())) {
				isValid = false;
				return isValid;
			}
		}
		
		return isValid;
	}
	
	/*
	 * node expected - CustomDescriptionMatrix instance
	 */
	private String[] checkIfCodeIsUnique(CommandBeanTreeNode node) throws Exception {
		String[] isInvalid = null;
		
		CustomDescriptionMatrix cdm = null;
		
		if (node.getData() != null && node.getData() instanceof CustomDescriptionMatrix) {
			cdm = (CustomDescriptionMatrix) node.getData();
			
			if (StringUtils.isNotBlank(cdm.getRemarksCode())) {
				String sql = "FROM CustomDescriptionMatrix WHERE (isDeleted = FALSE or isDeleted is null) AND (remarksCode is not null and remarksCode <> '')";
				if (this.isRsd){
					sql += " AND (forRsd = TRUE) ";
				}else{
					sql += " AND (forRsd = FALSE OR forRsd is null) ";
				}
				List<CustomDescriptionMatrix> checkDBList = null;
				List<String> paramNames = new ArrayList<String>();
				List<Object> paramValues = new ArrayList<Object>();
				if (StringUtils.isNotBlank(cdm.getCustomDescriptionMatrixUid())) {
					sql += " AND customDescriptionMatrixUid <> :customDescriptionMatrixUid";
					paramNames.add("customDescriptionMatrixUid");
					paramValues.add(cdm.getCustomDescriptionMatrixUid());
					checkDBList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, paramValues);
				}
				else {
					checkDBList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
				}
				
				if (checkDBList != null && checkDBList.size() > 0) {
					for (CustomDescriptionMatrix cdmFound : checkDBList ) {
						if (StringUtils.isNotBlank(cdm.getRemarksCode()) && cdm.getRemarksCode().equals(cdmFound.getRemarksCode()) && !cdm.getRemarks().equals(cdmFound.getRemarks())
								&& (StringUtils.isBlank(cdm.getCondition()) || (StringUtils.isNotBlank(cdm.getCondition()) && cdm.getCondition().equals(cdmFound.getCondition())))) {
							isInvalid = new String[]{cdmFound.getRemarksCode(),cdmFound.getRemarks(),cdmFound.getCondition()};
							return isInvalid;
						} else if (StringUtils.isNotBlank(cdm.getRemarksCode()) && !cdm.getRemarksCode().equals(cdmFound.getRemarksCode()) && cdm.getRemarks().equals(cdmFound.getRemarks())
								&& (StringUtils.isBlank(cdm.getCondition()) || (StringUtils.isNotBlank(cdm.getCondition()) && cdm.getCondition().equals(cdmFound.getCondition())))) {
							isInvalid = new String[]{cdmFound.getRemarksCode(),cdmFound.getRemarks(),cdmFound.getCondition()};
							return isInvalid;
						}else if (StringUtils.isNotBlank(cdm.getRemarksCode()) && cdm.getRemarksCode().equals(cdmFound.getRemarksCode()) && cdm.getRemarks().equals(cdmFound.getRemarks())
								&& (StringUtils.isBlank(cdm.getCondition()) || (StringUtils.isNotBlank(cdm.getCondition()) && cdm.getCondition().equals(cdmFound.getCondition())))) {
							isInvalid = new String[]{cdmFound.getRemarksCode(),cdmFound.getRemarks(),cdmFound.getCondition()};
							return isInvalid;
						}
					}
					
				}
			}

		}
		
		return isInvalid;
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		
		if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
			String selectedOperationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
			if (StringUtils.isNotBlank(selectedOperationCode)){
				customCondition+=(customCondition.length()>0?" AND ":"")+" operationCode =:operationCode ";
				query.addParam("operationCode", selectedOperationCode);
			}
			else
			{
				customCondition = " 1 = 1";
			}
		}
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		//return null;
		
		if (meta.getTableClass().equals(ActivityDescriptionMatrix.class)) {
			if (commandBean.getRoot().getDynaAttr().get("sortBy") != null && StringUtils.isNotBlank((String)commandBean.getRoot().getDynaAttr().get("sortBy"))) {
				return (String) commandBean.getRoot().getDynaAttr().get("sortBy");
			} else {
				return "onOffShore DESC, operationCode DESC, classShortCode DESC, phaseShortCode DESC, jobTypeShortCode DESC, taskShortCode DESC";
			}
		}
		return null;
	}

}
