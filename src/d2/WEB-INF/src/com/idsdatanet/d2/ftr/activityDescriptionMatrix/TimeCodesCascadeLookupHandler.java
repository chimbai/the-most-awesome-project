package com.idsdatanet.d2.ftr.activityDescriptionMatrix;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class TimeCodesCascadeLookupHandler implements CascadeLookupHandler {

	String table = null;
	String parent = null;
	
	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String rigType = (String) commandBean.getRoot().getDynaAttr().get("rigType");
		
		String queryString = "FROM " + table + " WHERE (isDeleted=false or isDeleted is null) " +
				"and (rigType=:rigType or rigType='' or rigType is null) " +
				"ORDER BY sysPaddedCode";
		List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "rigType", rigType);
		for (Object rec : list) {
			String shortCode = (String) PropertyUtils.getProperty(rec, "shortCode");
			String name = (String) PropertyUtils.getProperty(rec, "name");
			Boolean isActive = (Boolean) PropertyUtils.getProperty(rec, "isActive");
			LookupItem item = new LookupItem(shortCode, shortCode+". "+name);
			item.setActive(isActive);
			result.put(shortCode, item);
		}
		return result;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		String queryString ="SELECT " + parent+ " FROM "+table+" WHERE (isDeleted IS NULL OR isDeleted=FALSE) GROUP BY "+parent+" ORDER BY "+parent;
		List<String> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);
		for (String type : rs) {
			CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(type);
			cascadeLookupSet.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, type, null)));
			result.add(cascadeLookupSet);				
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
		
	}

	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, String key, LookupCache lookupCache)
			throws Exception {
		
		String rigType = (String) commandBean.getRoot().getDynaAttr().get("rigType");
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String sql ="FROM "+table+" WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
				"AND (rigType=:rigType or rigType='' or rigType is null) AND "+parent+"=:key ORDER BY sysPaddedCode";
		List<Object> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"rigType", "key"}, new Object[]{rigType, key});
		for (Object rec : rs) {
			String shortCode = (String) PropertyUtils.getProperty(rec, "shortCode");
			String name = (String) PropertyUtils.getProperty(rec, "name");
			Boolean isActive = (Boolean) PropertyUtils.getProperty(rec, "isActive");
			LookupItem item = new LookupItem(shortCode, shortCode+". "+name);
			item.setActive(isActive);
			result.put(shortCode, item);
		}
		return result;
	}

	
}
