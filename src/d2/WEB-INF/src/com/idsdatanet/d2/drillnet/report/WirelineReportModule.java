package com.idsdatanet.d2.drillnet.report;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.WirelineRun;
import com.idsdatanet.d2.core.model.WirelineSuite;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WirelineReportModule extends DefaultReportModule {
	private boolean selectionBySuiteAndRun = false;
	
	private class WirelineLookupHandler implements LookupHandler {
		private Map<String, LookupItem> lookup = null;
		
		WirelineLookupHandler(){
		}		
				
		public Map<String, LookupItem> getLookup(CommandBean commandBean,
				CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
				HttpServletRequest request, LookupCache lookupCache)
				throws Exception {
			
				this.lookup=new LinkedHashMap<String, LookupItem>();
				try {
					String queryString = "FROM WirelineSuite WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid";
					List<WirelineSuite> suite;
					
						suite = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid",userSelection.getOperationUid());
					
					if (suite.size()>0)
						for (WirelineSuite wireline :suite)
						{
							this.lookup.put(wireline.getWirelineSuiteUid(), new LookupItem(wireline.getWirelineSuiteUid(), wireline.getSuiteNumber()));
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}					
				
				return this.lookup;
			}
	}
	
	private class WirelineSuiteAndRunLookupHandler implements LookupHandler {
		private Map<String, LookupItem> lookup = null;		
		
		WirelineSuiteAndRunLookupHandler(){
		}		
				
		public Map<String, LookupItem> getLookup(CommandBean commandBean,
				CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
				HttpServletRequest request, LookupCache lookupCache)
				throws Exception {
			
				this.lookup=new LinkedHashMap<String, LookupItem>();
				LookupItem item = new LookupItem("ALL", "ALL");
				
				try {
					String queryString = "FROM WirelineSuite WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid";
					List<WirelineSuite> suite;
					
					suite = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid",userSelection.getOperationUid());
					
					if (suite.size()>0)
						this.lookup.put("ALL", item);
						for (WirelineSuite wireline :suite)
						{
							String queryString2 = "FROM WirelineRun WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid and wirelineSuiteUid = :wirelineSuiteUid order by runNumber";
							List<WirelineRun> wirelineRun;
							wirelineRun = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString2, new String[] {"operationUid", "wirelineSuiteUid"}, new Object[] {userSelection.getOperationUid(), wireline.getWirelineSuiteUid()});
							
							if(wirelineRun.size() > 0){
								for(WirelineRun run : wirelineRun){
									this.lookup.put(wireline.getWirelineSuiteUid() + ":" + run.getWirelineRunUid(), new LookupItem(wireline.getWirelineSuiteUid()  + ":" + run.getWirelineRunUid(),"Suite " + wireline.getSuiteNumber() + " - Run " + run.getRunNumber()));
								}								
							}else{
								this.lookup.put(wireline.getWirelineSuiteUid(), new LookupItem(wireline.getWirelineSuiteUid(), "Suite " + wireline.getSuiteNumber() + " (No Run)"));
							}
							
							
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}					
				
				return this.lookup;
			}
	}
	
	//override the report generating function, if got multiple day selected construct the List of UserSelectionSnapShot and generate report	
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		commandBean.getSystemMessage().clear();
		String wirelineSuiteUid = "";
		String wirelineRunUid = "";
		String selectionOption = (String) commandBean.getRoot().getDynaAttr().get("suiteNumber");
		
		UserSelectionSnapshot userSelection = UserSelectionSnapshot.getInstanceForCustomDaily(session, session.getCurrentDailyUid());
		userSelection.setCustomProperty("suiteNumber", selectionOption);
		
		if (commandBean.getRoot().getDynaAttr().get("suiteNumber") != null) {

			if(this.selectionBySuiteAndRun){
				String[] suiteAndRun = selectionOption.split(":");
				if(StringUtils.isNotBlank(suiteAndRun[0])) wirelineSuiteUid = suiteAndRun[0];
				if(suiteAndRun.length > 1){
					if(StringUtils.isNotBlank(suiteAndRun[1])) wirelineRunUid = suiteAndRun[1];					
				}
				if ("ALL".equals(commandBean.getRoot().getDynaAttr().get("suiteNumber"))) userSelection.setCustomProperty("suiteNo", "All Suite");
			}else{
				wirelineSuiteUid = selectionOption;
			}
			
			//report naming should be "operation name + suite # + hole size" 
			//declaration			
			String holeSizeUnit = "";
			String[] paramsFields = {"thisWirelineSuiteUid"};
			Object[] paramsValues = new Object[1]; 
			paramsValues[0] = wirelineSuiteUid;
			
			//get hole size unit
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelineSuite.class, "holeSize");	
			if (thisConverter.getUomSymbol().toString()!= null) holeSizeUnit = thisConverter.getUomSymbol().toString();

			//query statement on the suite number and hole size from the selected wireline suite
			String strSql1 = "SELECT suiteNumber, holeSize FROM WirelineSuite WHERE (isDeleted = false or isDeleted is null) AND wirelineSuiteUid = :thisWirelineSuiteUid";			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields, paramsValues);			
			
			//check if got result from the query
			if (lstResult.size() > 0){
				Object[] thisResult = (Object[]) lstResult.get(0);
				//get the suite number
				if (thisResult[0] != null) {				
					if (StringUtils.isNotBlank(thisResult[0].toString())){						
						userSelection.setCustomProperty("suiteNo", "Suite #" + thisResult[0].toString());
					}
				}
				//get the suite hole size
				if (thisResult[1] != null) {				
					if (StringUtils.isNotBlank(thisResult[1].toString())){						
						userSelection.setCustomProperty("holeSize", thisResult[1].toString() + holeSizeUnit);
					}
				}
			}
			
			String[] paramsFields2 = {"thisWirelineSuiteUid","wirelineRunUid"};
			Object[] paramsValues2 = {wirelineSuiteUid, wirelineRunUid}; 
			
			
			if(this.selectionBySuiteAndRun && StringUtils.isNotBlank(wirelineRunUid)){
				String strSql2 = "SELECT runNumber FROM WirelineRun WHERE (isDeleted = false or isDeleted is null) AND wirelineSuiteUid = :thisWirelineSuiteUid AND wirelineRunUid = :wirelineRunUid";			
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				if (lstResult2.size() > 0){
					String thisResult = (String) lstResult2.get(0);
					if(StringUtils.isNotBlank(thisResult)){				
						
						userSelection.setCustomProperty("runNo", "Run #" + thisResult);
					}
				}				
			}
		}else{
			userSelection.setCustomProperty("suiteNo", "All Suite");
		}
		
		return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));
		
	}
	
	//override
	protected ReportJobParams submitReportJobWithEmail(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		return submitReportJobWithGenerateAndSent(session, request, commandBean);
	}
	
	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception{
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		
		String suiteNumber = (String) userContext.getUserSelection().getCustomProperties().get("suiteNumber");
		String wirelineRunNumber = "";
		String wirelineSuiteNumber = "";
		if(StringUtils.isNotBlank(suiteNumber)){
			if(suiteNumber.contains(":")){
				String[] suiteAndRun = suiteNumber.split(":");
				if(StringUtils.isNotBlank(suiteAndRun[0])) wirelineSuiteNumber = suiteAndRun[0];
				if(StringUtils.isNotBlank(suiteAndRun[1])) wirelineRunNumber = suiteAndRun[1];
				
			}else{
				wirelineSuiteNumber = suiteNumber;
			}
		}
		if(super.getOperationRequired() && operation == null) throw new Exception("Operation must be selected");
		
		
		if(operation == null) throw new Exception("Unable to generate output file name");
		
		return (operation == null ? "" : operation.getOperationName()) + " " + (StringUtils.isNotBlank(wirelineSuiteNumber) ? "" : wirelineSuiteNumber) + " " + (StringUtils.isNotBlank(wirelineRunNumber) ? "" : wirelineRunNumber) + (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
	}	
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		String operation_uid = userContext.getUserSelection().getOperationUid();
		if(super.getOperationRequired() && StringUtils.isBlank(operation_uid)){
			throw new Exception("Operation must be selected");
		}	
		
		String suiteNumber = (String) userContext.getUserSelection().getCustomProperties().get("suiteNumber");		
		
		String wirelineRunNumber = "";
		String wirelineSuiteNumber = "";
		if(StringUtils.isNotBlank(suiteNumber)){
			if(suiteNumber.contains(":")){
				String[] suiteAndRun = suiteNumber.split(":");
				if(StringUtils.isNotBlank(suiteAndRun[0])) wirelineSuiteNumber = suiteAndRun[0];
				if(StringUtils.isNotBlank(suiteAndRun[1])) wirelineRunNumber = suiteAndRun[1];
				
			}else{
				wirelineSuiteNumber = suiteNumber;
			}
		}		
		return this.getReportType(userContext.getUserSelection()) + 
			(StringUtils.isBlank(operation_uid) ? "" : "/" + operation_uid) + 
			(StringUtils.isBlank(wirelineSuiteNumber) ? "" : "/" + wirelineSuiteNumber) + 
			(StringUtils.isBlank(wirelineRunNumber) ? "" : "/" + wirelineRunNumber) + 
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize));
		
	}
	
	public void setSelectionBySuiteAndRun(boolean isTrue){
		this.selectionBySuiteAndRun = isTrue;
	}
	
	public void init(CommandBean commandBean){
		super.init(commandBean);
		
		if(commandBean instanceof BaseCommandBean){
			Map lookup = new HashMap();
			if(this.selectionBySuiteAndRun){
				lookup.put("@suiteNumber", new WirelineSuiteAndRunLookupHandler());
			}else{
				lookup.put("@suiteNumber", new WirelineLookupHandler());
			}
			((BaseCommandBean) commandBean).setLookup(lookup);
							
		}
	}	
}
