package com.idsdatanet.d2.drillnet.coiledTubing;


import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.model.CoiledTubing;
import com.idsdatanet.d2.core.model.CoiledTubingDetails;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CoiledTubingDataNodeListener extends EmptyDataNodeListener {
	
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		Object object = parent.getData();
		if (object instanceof CoiledTubing)
		{
			CoiledTubing coiledTubing = (CoiledTubing) object;
			CoiledTubingUtils.setCoiledTubingStartDate(coiledTubing);
			CoiledTubingUtils.setCoiledTubingEndDate(coiledTubing);
			CoiledTubingUtils.setCoiledTubingFootage(coiledTubing);
		}
		
		if (object instanceof CoiledTubingDetails)
		{
			CoiledTubingDetails coiledTubingDtails = (CoiledTubingDetails) object;
			CoiledTubingUtils.setCoiledTubingDetailsTotalLength(coiledTubingDtails);
		}
	}

}
