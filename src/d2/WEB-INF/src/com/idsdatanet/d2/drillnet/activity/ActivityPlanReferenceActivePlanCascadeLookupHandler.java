package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ActivityPlanReferenceActivePlanCascadeLookupHandler implements CascadeLookupHandler {

	@Override
	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache)
			throws Exception {
		// TODO Auto-generated method stub
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String sql = "SELECT a.operationPlanPhaseUid, a.sequence, a.phaseName FROM OperationPlanPhase a, OperationPlanMaster b " +
				"WHERE b.dvdPlanStatus = '1' " + 
				"AND (a.operationPlanMasterUid = b.operationPlanMasterUid) " +
				"AND (a.isDeleted IS NULL or a.isDeleted = FALSE) " +
				"AND (b.isDeleted IS NULL or b.isDeleted = FALSE) " +
				"AND a.operationUid = :thisOperationUid " +
				"AND a.phaseCode = :phaseCode " +
				"ORDER BY a.sequence";
		List<Object[]> listPlanPhaseResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,new String[] {"thisOperationUid", "phaseCode"}, new String[] {userSelection.getOperationUid(),key});	
		if(listPlanPhaseResults.size()>0) {
			for(Object[] listPlanPhaseResult:listPlanPhaseResults) {
				if(listPlanPhaseResult[0] != null && listPlanPhaseResult[1] != null && listPlanPhaseResult[2] != null) {
					String planReferencePlusSequence = listPlanPhaseResult[1].toString() + " " + listPlanPhaseResult[2].toString();
					LookupItem lookupItem = new LookupItem(listPlanPhaseResult[0].toString(),planReferencePlusSequence);
					result.put(listPlanPhaseResult[2].toString(),lookupItem);
				}
			}
		}
		
		return result;
	}
	

	@Override
	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		UserSession session = UserSession.getInstance(request);
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		String sql ="SELECT shortCode from LookupPhaseCode where (isDeleted = false or isDeleted is null)" +
				" and (onOffShore=:thisOnOffShore or onOffShore='' or onOffShore is null) and " +
				"(operationCode =:thisOperationCode or operationCode ='' or operationCode is null) ";
		List<Object> listPhaseCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"thisOnOffShore","thisOperationCode"}, new String[] {session.getCurrentWellOnOffShore(),userSelection.getOperationType()});
		if(listPhaseCode.size()>0) {
			for(Object listPhaseCodeResult:listPhaseCode) {
				String phaseCode = listPhaseCodeResult.toString();
				CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(phaseCode);
				cascadeLookupSet.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, phaseCode, null)));
				result.add(cascadeLookupSet);
			}
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);
		return result_in_array;
	}

	
	@Override
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
