package com.idsdatanet.d2.drillnet.activity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CompanyServiceProvidedLookupHandler implements LookupHandler {

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {

		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();

		String strsql = "SELECT distinct a.lookupCompanyUid, a.companyName FROM LookupCompany a, LookupCompanyService b, " +
				"CommonLookup c WHERE b.code = c.shortCode " +
				"AND a.lookupCompanyUid = b.lookupCompanyUid " +
				"AND (a.isDeleted IS NULL OR a.isDeleted = '') " +
				"AND (b.isDeleted IS NULL OR b.isDeleted = '') " +
				"AND (c.isDeleted IS NULL OR c.isDeleted = '') " +
				"AND c.lookupTypeSelection = 'unwantedEvent.contractorType' " +
				"ORDER BY a.companyName";
 		
 		List<Object[]> results = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strsql);
		if (results.size() > 0) {
			for (Object[] companyResult : results) {
				if (companyResult[0] != null && companyResult[1] != null) {
					result.put(companyResult[0].toString(), new LookupItem(companyResult[0].toString(), companyResult[1].toString()));
				}
			}
		}
		return result;

		
	}


}
