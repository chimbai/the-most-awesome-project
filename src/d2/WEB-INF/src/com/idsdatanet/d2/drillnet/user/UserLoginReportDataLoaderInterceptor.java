package com.idsdatanet.d2.drillnet.user;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;

public class UserLoginReportDataLoaderInterceptor implements DataLoaderInterceptor {

	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "(isDeleted IS NULL OR isDeleted = '') AND userUid IN (SELECT DISTINCT userUid FROM UserAclGroup WHERE (isDeleted IS NULL OR isDeleted = ''))";
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
}
