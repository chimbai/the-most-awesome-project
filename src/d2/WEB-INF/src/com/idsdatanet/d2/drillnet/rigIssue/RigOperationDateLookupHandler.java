package com.idsdatanet.d2.drillnet.rigIssue;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.menu.MenuDaySelectionItem;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class RigOperationDateLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot selectionSnapshot, HttpServletRequest request, LookupCache cache) throws Exception {
		Map<String, LookupItem> result = cache.getFromCache(this.getClass().getName());
		if(result != null) return result;
		
		UserSession session = null;

		if (commandBean.getRoot().getDynaAttr().get("operationUid").toString()!=null)
		{
			List<MenuDaySelectionItem> list = MenuManager.getConfiguredInstance().getDaysSelection(selectionSnapshot.getOperationType(), commandBean.getRoot().getDynaAttr().get("operationUid").toString(), session);
			
			result = new LinkedHashMap<String, LookupItem>();
			for(MenuDaySelectionItem item: list){
				result.put(item.getDailyUid(), new LookupItem(item.getDailyUid(), item.getLabel()));
			}
			cache.saveToCache(this.getClass().getName(), result);
		}
				
		return result;
	}
}
