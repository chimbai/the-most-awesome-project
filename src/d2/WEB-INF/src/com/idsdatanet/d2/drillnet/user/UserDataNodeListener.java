package com.idsdatanet.d2.drillnet.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.JobResponse;
import com.idsdatanet.d2.core.job.JobServer;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.AclGroup;
import com.idsdatanet.d2.core.model.MobileSubsLog;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.service.Manager;
import com.idsdatanet.d2.core.util.JSONResponse;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.multiselect.MultiSelect;
import com.idsdatanet.d2.core.web.security.RandPass;
import com.idsdatanet.d2.core.web.webservice.SimpleJsonResponse;
import com.idsdatanet.d2.core.web.webservice.SimpleXmlResponse;
import com.idsdatanet.d2.infra.system.encode.MD5;
import com.idsdatanet.d2.infra.system.security.DefaultPasswordEncoder;
import com.idsdatanet.d2.infra.system.security.ldap.LdapEmail;
import com.idsdatanet.d2.infra.system.security.totp.TOTPConfig;
import com.idsdatanet.depot.idsbridge.provider.user.IDSBridgeUserProvider;
import com.idsdatanet.depot.idsbridge.provider.user.IDSBridgeUserResponse;
import com.idsdatanet.depot.idsbridge.response.Message;

import net.sf.json.JSONObject;

public class UserDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener {
	
	private Log logger = LogFactory.getLog(this.getClass());
	
	private MailEngine mailEngine = null;
	private String idsEmail = null;
	private IDSBridgeUserProvider idsBridgeUserProvider = null;
	private TOTPConfig totpConfig = null;
	
	public void setTotpConfig(TOTPConfig value) {
		this.totpConfig = value;
	}
	
	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}
	
	public void setLdapIdsEmail(LdapEmail value) {
		if(value!=null && value.getLdapIdsEmail()!=null)	this.idsEmail = value.getLdapIdsEmail();
	}
	
	public void setIdsBridgeUserProvider(IDSBridgeUserProvider idsBridgeUserProvider) {
		this.idsBridgeUserProvider = idsBridgeUserProvider;
	}
	
	public static boolean isValid(String passwordhere, String confirmhere, List<String> errorList) {

	    Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
	    Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
	    Pattern lowerCasePatten = Pattern.compile("[a-z ]");
	    Pattern digitCasePatten = Pattern.compile("[0-9 ]");
	    Pattern spaceCasePatten = Pattern.compile("\\s\\s");
	    errorList.clear();

	    boolean flag=true;

	    if (passwordhere.contains(" ")) {
	        errorList.add("Password cannot contains a space");
	        flag=false;
	    }
	    if (passwordhere.length() < 8) {
	        errorList.add("Password lenght must have at least 8 character");
	        flag=false;
	    }
	    if (!specailCharPatten.matcher(passwordhere).find()) {
	        errorList.add("Password must have at least one special character");
	        flag=false;
	    }
	    if (!UpperCasePatten.matcher(passwordhere).find()) {
	        errorList.add("Password must have at least one uppercase character");
	        flag=false;
	    }
	    if (!lowerCasePatten.matcher(passwordhere).find()) {
	        errorList.add("Password must have at least one lowercase character");
	        flag=false;
	    }
	    if (!digitCasePatten.matcher(passwordhere).find()) {
	        errorList.add("Password must have at least one digit character");
	        flag=false;
	    }

	    return flag;

	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		if (!(node.getData() instanceof User)) return;
		User user = (User) node.getData();
		
		if (StringUtils.isNotBlank(user.getPassword())) {
			List<String> errorList = new ArrayList<String>();
			Boolean validpassword = this.isValid(user.getPassword(), null, errorList);
			if (BooleanUtils.isFalse(validpassword)) {
				status.setContinueProcess(false, true);
				status.setFieldError(node, "password", errorList.get(0));
				return;
			}		
		}

		if(node.getInfo().isNewlyCreated()){
			user.setCreatedBy(session.getUserFirstName());
			user.setCreatedDate(new Date());
		}else{
			this.addMobileSubsLogForExistingUser(user);
		}
		
		if(StringUtils.isNotBlank(user.getUserName())){
			CharSequence prefix ="@ids";
			boolean containPrefix = user.getUserName().contains(prefix);
			if (containPrefix){
				status.setContinueProcess(false, true);
				status.setFieldError(node, "userName", "Prefix with @ids not allowed");
			}
		}
		if (!BooleanUtils.isTrue(ApplicationConfig.getConfiguredInstance().getUserPasswordVerification()) || BooleanUtils.isTrue(user.getIsSystemUser())) {
			//ignore and do not override password field if userVerification=true as it is hidden from screen
			if(StringUtils.isBlank(user.getPassword())) {
				String oldPassword = (String) node.getDynaAttr().get("oldpassword");
				user.setPassword(oldPassword);
			} else {
				if(StringUtils.isNotBlank(user.getPassword())) {
					node.getDynaAttr().put("password", user.getPassword());
					user.setPassword(DefaultPasswordEncoder.encodePassword(user.getPassword()));
					user.setPasswordChangedDate(new Date());
					user.setVerificationCode(null);
				}
			}
		}
		
		if (BooleanUtils.isNotTrue(user.getIsTemporaryUser())) {
			user.setAccountExpiryDateTime(null);
		}
		
		
		Boolean isIdsUser = false;
		if(session.getCurrentUser()!=null){
			User currentUser = session.getCurrentUser();
			if (currentUser!= null){
				if(currentUser.getIdsUser()!=null){
					if(currentUser.getIdsUser()){
						isIdsUser = true;
					}
				}	
			}
		}
		
		if(isIdsUser){
			Boolean sendEmail = false;
			String aclGroupList = "";
			MultiSelect multiSelect = node.getMultiSelect();
			
			if (multiSelect!=null) {
				List<String> aclGroupUid = multiSelect.getValues().get("@aclGroups");
				if (aclGroupUid!=null) {
					Integer count = 0;
					for (String group : aclGroupUid) {
						AclGroup acl = (AclGroup) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(AclGroup.class, group);
						
						if(acl!=null){			
							List<Object[]> listGroup = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select aclGroupUid From UserAclGroup where (isDeleted = false or isDeleted is null) and userUid =:userUid and aclGroupUid =:aclGroupUid", new String[] {"userUid","aclGroupUid"}, new Object[]{user.getUserUid(),group});
							if (listGroup != null && !listGroup.isEmpty()) {
								if(!sendEmail){
									sendEmail = false;
								}
							}else{
								sendEmail = true;									
							}
							
							aclGroupList += ((count>0)?","+acl.getName():acl.getName()) ;								
						}
						count++;
					}
					
					List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From UserAclGroup where (isDeleted = false or isDeleted is null) and userUid =:userUid", new String[] {"userUid"}, new Object[]{user.getUserUid()});
					if(!(count==list.size())){
						sendEmail = true;
					}
				}
			}
			node.getDynaAttr().put("updateAclGroup", sendEmail);
			node.getDynaAttr().put("aclGroupName", aclGroupList);
		}		
		
		if("1".equals(node.getDynaAttr().get("totpReset"))) {
			user.setTotpKey(null);
			user.setTotpLastVerifiedCode(null);
			user.setTotpResetDatetime(new Date());
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		if (!(node.getData() instanceof User)) return;
		
		Boolean isIdsUser = false;
		if(session.getCurrentUser()!=null){
			User currentUser = session.getCurrentUser();
			if (currentUser!= null){
				if(currentUser.getIdsUser()!=null){
					if(currentUser.getIdsUser()){
						isIdsUser = true;
					}
				}	
			}
		}
		
		User user = (User) node.getData();
		String email = user.getEmail();
		//notification on LDAP user creating new user to be refined later according to SA
		/*if(isIdsUser){
			email = null;
			if(idsEmail!=null){
				email = idsEmail;
			}
			
		}*/
		
		if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
			//if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_ACCOUNT_CREATED_EMAIL_NOTIFICATION)) || isIdsUser) {
			if (BooleanUtils.isTrue(ApplicationConfig.getConfiguredInstance().getUserPasswordVerification())) {
				
				String password = (String) node.getDynaAttr().get("password");
				if(StringUtils.isBlank(password)){
					password= "";
				}
				
				String aclGroupList = "";
				MultiSelect multiSelect = node.getMultiSelect();
				
				if (multiSelect!=null) {
					List<String> aclGroupUid = multiSelect.getValues().get("@aclGroups");
					if (aclGroupUid!=null) {
						Integer count = 0;
						for (String group : aclGroupUid) {
							AclGroup acl = (AclGroup) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(AclGroup.class, group);
							
							if(acl!=null){
								aclGroupList += ((count>0)?","+acl.getName():acl.getName()) ;
							}
							count++;
						}
					}
				}
				
				String activationCode = MD5.encode(RandPass.randomString(8));
				user.setActivationCode(activationCode);
				user.setActivationCodeDateTime(new Date());
				Manager daoManager = ApplicationUtils.getConfiguredInstance().getDaoManager();
				daoManager.saveObject(user);
				
				String url =  session.getClientAbsoluteBaseUrl() + "reset.html?activationCode=" + activationCode;
				if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_ACCOUNT_CREATED_EMAIL_NOTIFICATION))) {
					this.sendAccountCreatedNotification(mailEngine, 
							email, 
							user.getFname(), 
							user.getUserName(), 
							new String(password), 
							url, 
							"IDS DataNet Login Information", 
							"core/accountCreated.ftl",
							aclGroupList,true);
				}
			}
			node.getDynaAttr().remove("password");
			
			this.addMobileSubsLogForNewUser(user);
		}else{
			if(isIdsUser){
				email = null;
				if(idsEmail!=null){
					email = idsEmail;
				}
				
				String password = (String) node.getDynaAttr().get("password");
				if(StringUtils.isBlank(password)){
					password= "";
				}
				
				Boolean sendEmail = false;
				Boolean update = (Boolean) node.getDynaAttr().get("updateAclGroup");
				if(update!=null && update){
					sendEmail = true;
				}
				String aclGroupList = "";
				String aclName = (String) node.getDynaAttr().get("aclGroupName");
				if(StringUtils.isNotBlank(aclName)){
					aclGroupList = aclName;
				}
				
				if(sendEmail){
					this.sendAccountCreatedNotification(mailEngine, 
							email, 
							user.getFname(), 
							user.getUserName(), 
							new String(password), 
							session.getClientBaseUrl(), 
							"IDS DataNet Login Information", 
							"core/accountUpdated.ftl",
							aclGroupList,false);
				}
				
			}
			
			
		}
		//User user = (User) node.getData();
		String allowCreate = (String) node.getDynaAttr().get("allowCreate");
		String allowApprove = (String) node.getDynaAttr().get("allowApprove");
		String allowSend = (String) node.getDynaAttr().get("allowSend");
		String allowReceive = (String) node.getDynaAttr().get("allowReceive");	
		this.refreshCache(commandBean, node, session);
	}
	
	@Override
	public void afterDataNodeDelete(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		if (node.getData() instanceof User) {
			this.refreshCache(commandBean, node, session);
		}
	}
	
	private void refreshCache(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception {
		if (node.getData() instanceof User) {
			
			List<String> changedRowPrimaryKeys = new ArrayList<String>();
			
			changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(session.getCurrentRigInformationUid());
			D2ApplicationEvent.publishTableChangedEvent(RigInformation.class, changedRowPrimaryKeys);
			
			changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(session.getCurrentWellUid());
			D2ApplicationEvent.publishTableChangedEvent(Well.class, changedRowPrimaryKeys);
			
			changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(session.getCurrentWellboreUid());
			D2ApplicationEvent.publishTableChangedEvent(Wellbore.class, changedRowPrimaryKeys);
			
			changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(session.getCurrentOperationUid());
			D2ApplicationEvent.publishTableChangedEvent(Operation.class, changedRowPrimaryKeys);
			
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
			
			MenuManager.getConfiguredInstance().refreshCachedData();
			
			//kwong - disable this to prevent page refresh which cannot select back previous selected record
			//if (commandBean.getFlexClientControl() != null) {
				//commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			//}
		}
	}


	private void sendAccountCreatedNotification(final MailEngine mailEngine, 
			final String email, final String fname, final String userName, final String password, final String url, final String subject, final String templateFilename, final String aclGroup, final Boolean status) {
		
		try {
			JobContext job = JobServer.getConfiguredInstance().createJob(new D2Job() {
	    		public void run(UserContext userContext, JobResponse jobResponse) throws Exception {
	    			Map<String, Object> model = new HashMap<String, Object>();
					model.put("fname", fname);
					model.put("userName", userName);
					model.put("password", password);
					model.put("url", url);
					model.put("acl", aclGroup);
					if(status){
						model.put("status", "new");
					}else{
						model.put("status", "");
					}
					
					
					mailEngine.sendMail(StringUtils.isNotBlank(email) ? email : ApplicationConfig.getConfiguredInstance().getSupportEmail(), 
							ApplicationConfig.getConfiguredInstance().getSupportEmail(), 
							ApplicationConfig.getConfiguredInstance().getSupportEmail(), 
							subject, 
							mailEngine.generateContentFromTemplate(templateFilename, model));
	        	}
	        	public void dispose(){
	        	}
	    	}, UserContext.getUserContext());
			job.setTimeout(60000); // 1 minute
			JobServer.getConfiguredInstance().submitJob(job);
		} catch (Exception e) {
			this.logger.error(e.getMessage(), e);
		}
	}

	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		if (!(node.getData() instanceof User)) return;
		User user = (User) node.getData();
		node.getDynaAttr().put("oldpassword", user.getPassword());
		
		if (UserUtils.hasSignature(user.getUserUid()))	
			node.getDynaAttr().put("hasSignature","1");			
	}
	
	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof User) {
			if (BooleanUtils.isTrue(ApplicationConfig.getConfiguredInstance().getActivateRigAccesssControl())) {
				if (StringUtils.isBlank(ApplicationConfig.getConfiguredInstance().getNbbDeployedRig())) {
					node.getDynaAttr().put("showUserRigAccess", "1");
				}
			}
			if (BooleanUtils.isTrue(ApplicationConfig.getConfiguredInstance().getUserPasswordVerification())) {
				node.getDynaAttr().put("hidePassword", "1");
			}
			User user = (User) node.getData();
			user.setSendAccountAccessNotificationEmail(Boolean.TRUE); //default value
		}
	}

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		
		Map<String,CommandBeanTreeNode> nodes = root.getChild(User.class.getSimpleName());
		if(nodes != null) {
			// hide 'Super Administrator Group' in the selection, unless the current user belongs to that group
			String hideAclGroupUid = null;
			if (request != null) {
				UserSession session = UserSession.getInstance(request);
				if (session != null) {
					List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select aclGroupUid from AclGroup where (isDeleted = false or isDeleted is null) and name = :name", "name", "Super Administrator Group");
					if (list != null && !list.isEmpty()) {
						if (!session.getCombinedAclGroups().getAclGroupUids().contains(list.get(0))) {
							hideAclGroupUid = list.get(0);
						}
					}
				}
			}
			String hidePassword = null;
			if (BooleanUtils.isTrue(ApplicationConfig.getConfiguredInstance().getUserPasswordVerification())) {
				hidePassword = "1";
			}
			boolean totpEnabled = false;
			if(this.totpConfig != null) {
				totpEnabled = this.totpConfig.getTotpOption() != TOTPConfig.TOTP_OPTION_DISABLED;
			}
			for(CommandBeanTreeNode node: nodes.values()) {
				if(hideAclGroupUid != null) node.getDynaAttr().put("hideAclGroupUid", hideAclGroupUid);
				if(hidePassword != null) node.getDynaAttr().put("hidePassword", hidePassword);
				if(totpEnabled) {
					node.getDynaAttr().put("totpEnabled", "1");
					node.getDynaAttr().put("totpRegistered", isTotpKeyRegistered(node) ? "1" : "0");
				}
			}
		}
	}

	private static boolean isTotpKeyRegistered(CommandBeanTreeNode node) throws Exception {
		if(node.getData() instanceof User) {
			return StringUtils.isNotBlank(((User) node.getData()).getTotpKey());
		}else {
			return false;
		}
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("uploadSignature".equalsIgnoreCase(invocationKey))
		{
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			try{
				FileItem file_item = null;
		        String userUid = request.getParameter("userUid");
	
		        if(request instanceof MultipartHttpServletRequest){
					MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					if(! multipartRequest.getFileMap().isEmpty()){
						file_item = ((CommonsMultipartFile) multipartRequest.getFileMap().values().iterator().next()).getFileItem();
					}
				}else{
					FileItemFactory factory = new DiskFileItemFactory();
			        ServletFileUpload upload = new ServletFileUpload(factory);
			        List items = upload.parseRequest(request);        
			        
			        for(Iterator i = items.iterator(); i.hasNext(); ){
			            FileItem item = (FileItem) i.next();
			            if(! item.isFormField()){
			                file_item = item;
			                break;
			            }
			        }		
				}
		        byte[] image = file_item.get();
		        
		        byte[] encoded = Base64.encodeBase64(image);
		        
				UserUtils.removeSignature(userUid);
				UserUtils.addSignature(userUid, new String(encoded, "ASCII"));
		       
		        writer.addElement("status", "1");
			}catch(Exception e)
			{
				writer.addElement("responseErrorMsg", e.getMessage());
				e.printStackTrace();
			}finally{
				writer.endElement();
				writer.close();
			}
		} else if ("removeSignature".equalsIgnoreCase(invocationKey)) {
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			try{
				String userUid = request.getParameter("userUid");
				UserUtils.removeSignature(userUid);
				writer.addElement("status", "1");
			}catch(Exception e)
			{
				writer.addElement("responseErrorMsg", e.getMessage());
			}finally{
				writer.endElement();
				writer.close();
			}
		} else if ("requestAPIKey".equalsIgnoreCase(invocationKey)) {
			SimpleXmlResponse writer = SimpleJsonResponse.getSimpleJsonResponse(response);
			writer.startElement("root");
			boolean success = false;
			try {
				String userUid = request.getParameter("userUid");
				IDSBridgeUserResponse userResponse = this.idsBridgeUserProvider.registerUser(userUid);
				success = userResponse.isSuccess();
				if (success) {
					String api_key = userResponse.getAPIKey();
					User user = (User)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(User.class, userUid);
					user.setIdsBridgeApiKey(api_key);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(user);
					writer.addElement("idsbridge_api_key", api_key);
				} else {
					writer.addElement("error", "Failed to register user for API Key from IDSBridge with error: " + userResponse.getMessagesWithType(Message.TYPE_ERROR));
				}
			} catch(Exception e) {
				e.printStackTrace();
				writer.addElement("error", e.getMessage());
			} finally {
				writer.addElement("success", (success? "true":"false"));
				writer.endElement();
				writer.close();
			}
		} else if ("revokeAPIKey".equalsIgnoreCase(invocationKey)) {
			SimpleXmlResponse writer = SimpleJsonResponse.getSimpleJsonResponse(response);
			writer.startElement("root");
			boolean success = false;
			try {
				String userUid = request.getParameter("userUid");
				IDSBridgeUserResponse userResponse = this.idsBridgeUserProvider.revokeUserAPIKey(userUid);
				success = userResponse.isSuccess();
				if (success) {
					User user = (User)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(User.class, userUid);
					user.setIdsBridgeApiKey(null); // reset
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(user);
				} else {
					writer.addElement("error", "Failed to revoke API Key from IDSBridge with error: " + userResponse.getMessagesWithType(Message.TYPE_ERROR));
				}
			} catch(Exception e) {
				e.printStackTrace();
				writer.addElement("error", e.getMessage());
			} finally {
				writer.addElement("success", (success? "true":"false"));
				writer.endElement();
				writer.close();
			}
		}
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * return true if same, false if different
	 * @param firstObj
	 * @param secondObj
	 * @return
	 */
	private boolean compareBoolean(Boolean firstObj, Boolean secondObj) {
		if (firstObj == null && secondObj != null) {
			return false;
		} else if (firstObj != null && secondObj == null) {
			return false;
		} else if (firstObj == null && secondObj == null) {
			return true;
		} else {
			return (firstObj.booleanValue() == secondObj.booleanValue());
		}
	}
	
	private void addMobileSubsLogForExistingUser(User user) throws Exception{
		User savedUser = ApplicationUtils.getConfiguredInstance().getCachedUser(user.getUserUid());
		if(user.getIsMobileUser() != null && user.getIsMobileUser()) {
			if(savedUser.getIsMobileUser() == null || !savedUser.getIsMobileUser()) {
				String hql = "from MobileSubsLog where userUid=:thisUserUid and (isDeleted is null or isDeleted = 0) and (sysDeleted is null or sysDeleted = 0) order by lastEditDatetime desc";
				List<MobileSubsLog> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "thisUserUid", user.getUserUid());
				MobileSubsLog mobileSubs = new MobileSubsLog();
				mobileSubs.setUserUid(user.getUserUid());
				mobileSubs.setIsActivated(true);
				if (result != null && result.size() > 0) {
					MobileSubsLog latestSubsLog = result.get(0);
					mobileSubs.setCounter(latestSubsLog.getCounter() + 1);
				}else{
					mobileSubs.setCounter(1);
				}
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mobileSubs);
			}
		}else{
			if(savedUser.getIsMobileUser() != null && savedUser.getIsMobileUser()) {
				String hql = "from MobileSubsLog where userUid=:thisUserUid and (isDeleted is null or isDeleted = 0) and (sysDeleted is null or sysDeleted = 0) order by lastEditDatetime desc";
				List<MobileSubsLog> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "thisUserUid", user.getUserUid());
				MobileSubsLog mobileSubs = new MobileSubsLog();
				mobileSubs.setUserUid(user.getUserUid());
				mobileSubs.setIsActivated(false);
				if(result != null && result.size() > 0) {
					MobileSubsLog latestSubsLog = result.get(0);
					mobileSubs.setCounter(latestSubsLog.getCounter());
				}else{
					mobileSubs.setCounter(1);
				}
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mobileSubs);
			}
		}
	}
	
	private void addMobileSubsLogForNewUser(User user) throws Exception {
		if(user.getIsMobileUser() != null && user.getIsMobileUser()) {
			MobileSubsLog mobileSubs = new MobileSubsLog();
			mobileSubs.setUserUid(user.getUserUid());
			mobileSubs.setIsActivated(true);
			mobileSubs.setCounter(1);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mobileSubs);
		}
	}
}
