package com.idsdatanet.d2.drillnet.kick;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class KickActivityLinkLookupHandler implements LookupHandler{
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		UserSession session = UserSession.getInstance(request);
		String dailyUid = session.getCurrentDailyUid();
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		List<Activity> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Activity WHERE (isDeleted=false or isDeleted is null) AND dailyUid=:dailyUid and (submode1='kick' or submode2='kick' or submode3='kick') and dayPlus ='0' order by startDatetime, endDatetime", "dailyUid", dailyUid);
		for (Activity rec : list) {
			String key = rec.getActivityUid();
			String endDateTime = df.format(rec.getEndDatetime());
			if (("23:59").equalsIgnoreCase(endDateTime)) {
				endDateTime = "24:00";
			}
			String value = df.format(rec.getStartDatetime()) + "-" + endDateTime + " -- " + rec.getActivityDescription();
			LookupItem item = new LookupItem(key, value);
			result.put(key, item);
		}
		return result;
	}
}
