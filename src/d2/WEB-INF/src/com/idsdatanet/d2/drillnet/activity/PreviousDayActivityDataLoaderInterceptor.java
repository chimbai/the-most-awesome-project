package com.idsdatanet.d2.drillnet.activity;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class PreviousDayActivityDataLoaderInterceptor implements DataLoaderInterceptor {
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		
		if (NextDayActivity.class.equals(meta.getTableClass())) {
			conditionClause = "(dailyUid = session.dailyUid AND dayPlus = '1')" ;
			Daily prevDaily =ApplicationUtils.getConfiguredInstance().getYesterday(userSelection.getOperationUid(), userSelection.getDailyUid());

			if (prevDaily != null){
				
				query.addParam("prevDailyUid", prevDaily.getDailyUid());
				conditionClause = conditionClause + " or (dailyUid = :prevDailyUid and dayPlus='1')";
			} 
		}
		
		return conditionClause;
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

}
