package com.idsdatanet.d2.drillnet.report;

public class ScheduledManagementReportJob extends ScheduledAbstractReportJob {
	protected int getReportParamsReportType(){
		return ScheduledReportGenerationJobParams.REPORT_TYPE_MANAGEMENT_REPORT;
	}
}
