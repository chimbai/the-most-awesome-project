package com.idsdatanet.d2.drillnet.activity;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityDataLoaderInterceptor implements DataLoaderInterceptor {
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if (request != null) {
			if (Activity.class.equals(meta.getTableClass())) {
				return conditionClause + (StringUtils.isNotBlank(conditionClause) ? " AND " : "") + "((dayPlus IS NULL) OR dayPlus = '0')";
			}
		} else {
			if (Activity.class.equals(meta.getTableClass())) {
				Daily tomorrow = ApplicationUtils.getConfiguredInstance().getTomorrow(userSelection.getOperationUid(), userSelection.getDailyUid());
				if (tomorrow != null) {
					List<ReportDaily> drillnetReportDailyList = ActivityUtils.getDrillnetReportDaily(tomorrow.getDailyUid());
					if (!drillnetReportDailyList.isEmpty()) {
						return conditionClause + (StringUtils.isNotBlank(conditionClause) ? " AND " : "") + "((dayPlus IS NULL) OR dayPlus = '0')";
					}
				}
			}
			if (NextDayActivity.class.equals(meta.getTableClass())) {
				return "1 = 2";
			}
		}
		return conditionClause;
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

}
