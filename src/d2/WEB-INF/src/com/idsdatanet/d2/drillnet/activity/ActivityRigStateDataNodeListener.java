package com.idsdatanet.d2.drillnet.activity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.ActivityLessonTicketLink;
import com.idsdatanet.d2.core.model.ActivityRsd;
import com.idsdatanet.d2.core.model.ActivityUnwantedEventLink;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.keyPerformanceIndicator.KeyPerformanceIndicatorUtils;
import com.idsdatanet.d2.drillnet.reportDaily.ReportDailyUtils;
import com.idsdatanet.d2.safenet.unwantedEvent.UnwantedEventFlexUtils;

public class ActivityRigStateDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	
	private static final String DYN_ATTR_ACTIVITY_DATE = "activityDate";
	private Boolean displayCompanyRCMessageOnSystemMessage = false;
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		Calendar activityDate = Calendar.getInstance();
		
		if(object instanceof ActivityRsd) {
			
			String activityRsdUid = (String) PropertyUtils.getProperty(object, "activityRsdUid");
			if ("".equals(activityRsdUid)) {
				PropertyUtils.setProperty(object, "activityRsdUid", null);
			}

			Date thisActivityDate = null;
			if(node.getDynaAttr().get("activityDate") != null)
			{
				if(CommonDateParser.parse(node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE).toString()) != null)
				{
					thisActivityDate = CommonDateParser.parse(node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE).toString());
				}
				else
				{
					thisActivityDate = (Date) node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE);
				}
			}else{
				
				if (session.getCurrentDaily()!=null)
					thisActivityDate = session.getCurrentDaily().getDayDate();
					
			}
			
			//GET ACTIVITY DATE TO ASSIGN DAILYUID
			if(thisActivityDate != null && (commandBean.getOperatingMode() != BaseCommandBean.OPERATING_MODE_DEPOT))
			{
				activityDate.setTime(thisActivityDate);
				
				String thisOperationUid = PropertyUtils.getProperty(object, "operationUid").toString();
				Daily thisDaily = ApplicationUtils.getConfiguredInstance().getDailyByOperationAndDate(thisOperationUid, activityDate.getTime());
				if(thisDaily != null)
				{
					PropertyUtils.setProperty(object, "dailyUid", thisDaily.getDailyUid());
				}
				else
				{						
					Daily newDaily = CommonUtil.getConfiguredInstance().createNewDaily(activityDate.getTime(), thisOperationUid, session, request);
					PropertyUtils.setProperty(object, "dailyUid", newDaily.getDailyUid());
					
					//CREATE REPORT_DAILY
					ReportDaily newReportDaily = new ReportDaily();
					newReportDaily.setOperationUid(thisOperationUid);
					newReportDaily.setDailyUid(newDaily.getDailyUid().toString());
					newReportDaily.setReportDatetime(activityDate.getTime());
					newReportDaily.setReportType(CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(thisOperationUid));
					newReportDaily.setReportNumber("-");
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newReportDaily);
				}
				

				//STAMP STARTDATETIME & ENDDATETIME WITH CURRENT ACTIVITY DATE
				if(PropertyUtils.getProperty(object, "startDatetime") != null)
				{
					Calendar startDatetime = Calendar.getInstance();
					startDatetime.setTime((Date) PropertyUtils.getProperty(object, "startDatetime"));
					startDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
					startDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
					startDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
					PropertyUtils.setProperty(object, "startDatetime", startDatetime.getTime());
				}
													
				Calendar endDatetime = Calendar.getInstance();
				endDatetime.setTime((Date) PropertyUtils.getProperty(object, "endDatetime"));
				endDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
				endDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
				endDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
				PropertyUtils.setProperty(object, "endDatetime", endDatetime.getTime());
				
				D2ApplicationEvent.refresh();
				
			}
			
			
			List<Date> endDatetimeList = new ArrayList<Date>();
			
			// get parent node and loop through child to get each end datetime
			CommandBeanTreeNode parentNode = node.getParent();
			for (Iterator i = parentNode.getList().values().iterator(); i.hasNext();) {
				Map items = (Map) i.next();
				
				for (Iterator j = items.values().iterator(); j.hasNext();) {
					CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
					if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(object.getClass())) {
						Boolean isDeleted = (Boolean) PropertyUtils.getProperty(childNode.getData(), "isDeleted"); // shouldn't need this, temporary fix for Flex client
						if (isDeleted == null || !isDeleted.booleanValue()) {
							endDatetimeList.add((Date) PropertyUtils.getProperty(childNode.getData(), "endDatetime"));
						}
					}
				}
			}
			Collections.sort(endDatetimeList, new ActivityUtils.ReversedDateComparator());
			commandBean.getRoot().getDynaAttr().put("endDatetime", endDatetimeList);
			
			Date start = (Date) PropertyUtils.getProperty(object, "startDatetime");
			Date end = (Date) PropertyUtils.getProperty(object, "endDatetime");
			
			// if user didn't enter start time, find the first smaller than the current end time.
			if(start == null) {
				DateFormat df = new SimpleDateFormat("kkmmss");
				
				boolean found = false;
				for(Date endDatetime : endDatetimeList) {
					Calendar currentEndDatetime = Calendar.getInstance();
					currentEndDatetime.setTime(end);
					
					Calendar cachedEndDatetime = Calendar.getInstance();
					cachedEndDatetime.setTime(endDatetime);
					
					if(df.format(cachedEndDatetime.getTime()).compareTo(df.format(currentEndDatetime.getTime()))==-1) {
						
						if(thisActivityDate!=null){
							activityDate.setTime(thisActivityDate);
						}
						cachedEndDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
						cachedEndDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
						cachedEndDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
						
												
						PropertyUtils.setProperty(object, "startDatetime", cachedEndDatetime.getTime());
						found = true;
						break;
					}
				}
				// if no matches, set to 00:00
				if(!found) {
					Calendar zero = Calendar.getInstance();
					zero.clear();
					zero.set(Calendar.HOUR_OF_DAY, 0);
					zero.set(Calendar.MINUTE, 0);
					zero.set(Calendar.SECOND, 0);
					zero.set(Calendar.MILLISECOND, 0);
					
					
					if(thisActivityDate!=null){
						activityDate.setTime(thisActivityDate);
					}
					zero.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
					zero.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
					zero.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
					
										
					PropertyUtils.setProperty(object, "startDatetime", zero.getTime());
				}
				start = (Date) PropertyUtils.getProperty(object, "startDatetime");
			}
			
			// check if start time greater than end time.
			if(start != null && end != null){
				if(start.getTime() > end.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "startDatetime", "Start time can not be greater than end time.");
					return;
				}
				
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityRsd.class, "activityDuration");
				
				thisConverter.setBaseValue(thisDuration);
				
				PropertyUtils.setProperty(object, "activityDuration", (thisConverter.getConvertedValue()));
				
				/** PORT FROM TNP: Tour Stamping Feature **/
				String rigTourUid = CommonUtil.getConfiguredInstance().tourAssignmentBasedOnTime(session, start, false);
				if (StringUtils.isNotBlank(rigTourUid)) {
					PropertyUtils.setProperty(object, "rigTourUid", rigTourUid);
				}
				/** PORT FROM TNP: Tour Stamping Feature **/
				
			}
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if(! node.getInfo().isTemplateNode() && (object instanceof ActivityRsd)) {
			String strInputMode  = (String) commandBean.getRoot().getDynaAttr().get("inputMode");
			if(StringUtils.isBlank(strInputMode))
			{
				commandBean.getRoot().getDynaAttr().put("inputMode", "all");
			}
			
			if("all".equalsIgnoreCase(commandBean.getRoot().getDynaAttr().get("inputMode").toString()))
			{
				node.getDynaAttr().put("editable", true);
			}
			else
			{
				node.getDynaAttr().put("editable", false);
			}
								
			this.populateDynAttrActivityDate(node, userSelection, true);
			
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		String newActivityRecordInputMode = null;
		if (request != null) {
			newActivityRecordInputMode = request.getParameter("newActivityRecordInputMode");
		}
		
		if (obj instanceof ActivityRsd) {
			if (meta.getTableClass() == ActivityRsd.class && "1".equals(newActivityRecordInputMode)) {
				commandBean.getRoot().addCustomNewChildNodeForInput(new ActivityRsd());
			}
		}
		
		afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		
		if (meta.getTableClass().equals(ActivityRsd.class)) {
									
			//BY DEFAULT, LOAD DATA BY OPERATION
			String strSql = "FROM ActivityRsd WHERE operationUid = :operationUid AND groupUid = :groupUid AND (isDeleted IS NULL OR isDeleted = '') ORDER BY startDatetime";
			String[] paramNames =  {"operationUid", "groupUid"};
			Object[] paramValues = {userSelection.getOperationUid(), userSelection.getGroupUid()};
						
			List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			List<Object> output_maps = new ArrayList<Object>();
			
			for(Object objResult: items){
				ActivityRsd activity = (ActivityRsd) objResult;
				output_maps.add(activity);
			}
						
			return output_maps;
		}
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}
	
	private void populateDynAttrActivityDate(CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Boolean getDailyFromNode) throws Exception
	{
		String dailyUid = userSelection.getDailyUid();
		Date activityDate = null;
		if (getDailyFromNode && PropertyUtils.getProperty(node.getData(), "dailyUid")!=null)
		{
			dailyUid = (String) PropertyUtils.getProperty(node.getData(), "dailyUid");
		}
		if (StringUtils.isNotBlank(dailyUid))
		{
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
			if (daily!=null && daily.getDayDate()!=null)
				activityDate = daily.getDayDate();
		}
		node.getDynaAttr().put(DYN_ATTR_ACTIVITY_DATE, activityDate);
	}
	


}