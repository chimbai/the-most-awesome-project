package com.idsdatanet.d2.drillnet.rushmore;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RushmoreWells;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class RushmoreWDPRDataNodeListener extends EmptyDataNodeListener {

	private String dprVersion = null;
	private Boolean _setHiddenFieldsToNull = false;
	
	public void setHiddenFieldsToNull(Boolean value) {
		_setHiddenFieldsToNull = value;
	}
	
	public void setDprVersion(String version) {
		this.dprVersion = version;
	}
	
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
		
		Object object = node.getData();
		if (object instanceof RushmoreWells) {
			node.getDynaAttr().put("rushmoreDprVersion", this.dprVersion);
		}
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof RushmoreWells) {
			RushmoreWells thisRushmoreWell = (RushmoreWells) object;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			CustomFieldUom thisConverter;
			thisConverter = new CustomFieldUom(commandBean, RushmoreWells.class, "finalDrillBitOd1");
			
			//set default value
			
			RushmoreDPRUtils.defaultPopulateProperties(thisRushmoreWell, userSelection.getOperationUid(),thisConverter,node,userSelection, true,null);
			RushmoreDPRUtils.populateDefaultMultiSelect(node, userSelection.getOperationUid());
			
			
			thisConverter.setReferenceMappingField(MudProperties.class, "mudWeight");
			
			String mudUnit = thisConverter.getUOMMapping().getUnitID();
			
			if (mudUnit.equals("SpecificGravity")){
				thisRushmoreWell.setMudWeightUnits("sg");
			} else if (mudUnit.equals("KilogramsPerCubicMetre")){
				thisRushmoreWell.setMudWeightUnits("kgm3");
			} else if (mudUnit.equals("PoundsMassPerUKGallon")){
				thisRushmoreWell.setMudWeightUnits("ppg");
			} else if (mudUnit.equals("PoundsMassPerUSGallon")){
				thisRushmoreWell.setMudWeightUnits("ppg");
			} else if (mudUnit.equals("PoundsMassPerCubicFoot")){
				thisRushmoreWell.setMudWeightUnits("pf3"); 			// get Mud weight unit
			}
			
			
			thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
			String costUnit = thisConverter.getUOMMapping().getUnitID();
			
			if (costUnit.equals("USD")){
				thisRushmoreWell.setCostCurrency("USD");
			} else if (costUnit.equals("NOK")){
				thisRushmoreWell.setCostCurrency("NOK");
			} else if (costUnit.equals("GBP")){
				thisRushmoreWell.setCostCurrency("GBP");
			} else if (costUnit.equals("EUR")){
				thisRushmoreWell.setCostCurrency("EUR");					
			} else if (costUnit.equals("AUD")){
				thisRushmoreWell.setCostCurrency("AUD");		// get Cost Currency
			}

			thisConverter.setReferenceMappingField(ReportDaily.class, "depthMdMsl");
			String activityDepthUnit = thisConverter.getUOMMapping().getUnitID();
			
			if (activityDepthUnit.equals("Metre")){
				thisRushmoreWell.setUnitsOfMeasure("M");
			} else if (activityDepthUnit.equals("Feet")){
				thisRushmoreWell.setUnitsOfMeasure("F");		// get Units of Measure value
			}
			
			RushmoreDPRUtils.populateInterruptCodes(node, thisRushmoreWell, thisConverter, userSelection.getOperationUid(), qp);
		}	// End of rushmore_well
	}
	
	

	@Override
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		Object object = node.getData();
		Map<String, List<String>> values = node.getMultiSelect().getValues();
		
		if (object instanceof RushmoreWells) {
			RushmoreWells thisRushmoreWell = (RushmoreWells) object;
			if (values!=null && values.containsKey("multiLateralJunctionType")) {
				List<String> mLJTResult = values.get("multiLateralJunctionType");
				if(mLJTResult!=null)
				{
					if ((mLJTResult.contains("6")) && (mLJTResult.contains("6s")))
					{
						status.addError("For Multi-lateral junction type, 'Pressure integrity at junction - casing seal' OR " +
											"'Downhole splitter - large main bore with 2 smaller wellbores of equal size', only ONE value can be selected.");
						status.setContinueProcess(false, true);
						return;
					}
				}
				
			}
			
			RushmoreDPRUtils.beforeSaveOrUpdate(thisRushmoreWell, _setHiddenFieldsToNull, session.getCurrentWellOnOffShore());
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof RushmoreWells) {
			node.getDynaAttr().put("rushmoreDprVersion", this.dprVersion);
		}
	}
}
