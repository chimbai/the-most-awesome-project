package com.idsdatanet.d2.drillnet.lookupIncidentCategory;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.LookupIncidentCategory;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LookupIncidentCategoryDataNodeListener extends EmptyDataNodeListener{
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		
		Object object = node.getData();
		if(object instanceof com.idsdatanet.d2.core.model.LookupIncidentCategory){
			D2ApplicationEvent.refresh();
		}
	}
	
	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof LookupIncidentCategory) {
			LookupIncidentCategory rec = (LookupIncidentCategory) node.getData();
			String operationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCode"); 
			rec.setOperationCode(operationCode);
		}
	}
}
