package com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.dailyReport;

import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.drillnet.report.DefaultReportModule;
import com.idsdatanet.d2.drillnet.report.ReportAutoEmail;
import com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.ReportService;

public class AbstractReportService implements ReportService,InitializingBean {

	private String emailListKey = null;
	private String emailSubject = null;
	private String emailBodyTemplateFile = null;
	private boolean sendEmailAfterReportGeneration = true;
	private boolean opsTeamBased = false;
	private ReportAutoEmail reportAutoEmail = null;
	private DefaultReportModule defaultReportModule = null;
	private String generateReportUserName = "idsadmin";
	
	public String getGenerateReportUserName() {
		return generateReportUserName;
	}

	public void setGenerateReportUserName(String generateReportUserName) {
		this.generateReportUserName = generateReportUserName;
	}

	public void setSendEmailAfterReportGeneration(boolean value){
		this.sendEmailAfterReportGeneration = value;
	}
	
	public void setEmailDistributionListKey(String value){
		this.emailListKey = value;
	}
	
	public void setEmailBodyTemplateFile(String value){
		this.emailBodyTemplateFile = value;
	}
	
	public void setEmailSubject(String value){
		this.emailSubject = value;
	}
	
	public void setOpsTeamBased(boolean opsTeamBased) {
		this.opsTeamBased = opsTeamBased;
	}
	
	public ReportAutoEmail getReportAutoEmail()
	{
		return this.reportAutoEmail;
	}
	
	public void afterPropertiesSet()
	{
		this.reportAutoEmail = new ReportAutoEmail();
		reportAutoEmail.setEmailListKey(this.emailListKey);
		reportAutoEmail.setEmailSubject(this.emailSubject);
		reportAutoEmail.setEmailContentTemplateFile(this.emailBodyTemplateFile);
		reportAutoEmail.setSendEmailAfterReportGeneration(this.sendEmailAfterReportGeneration);
		reportAutoEmail.setOpsTeamBased(this.opsTeamBased);
	}

	public void setDefaultReportModule(DefaultReportModule defaultReportModule) {
		this.defaultReportModule = defaultReportModule;
	}

	public DefaultReportModule getDefaultReportModule() {
		return defaultReportModule;
	}

	synchronized public void run() throws Exception {
		this.defaultReportModule.generateDefaultReportOnCurrentSystemDatetime(this.generateReportUserName, reportAutoEmail);
	}
}
