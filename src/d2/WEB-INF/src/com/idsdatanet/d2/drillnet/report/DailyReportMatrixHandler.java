package com.idsdatanet.d2.drillnet.report;

import java.util.List;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.matrix.DefaultMatrixHandler;
import com.idsdatanet.d2.drillnet.matrix.Matrix;
import com.idsdatanet.d2.drillnet.matrix.MatrixHandler;

public class DailyReportMatrixHandler extends DefaultMatrixHandler{

	private String reportType = null; 
	
	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public void process(Matrix matrix, UserSelectionSnapshot userSelection) throws Exception {
		String sql = "select distinct dailyUid from ReportFiles where (isDeleted = false or isDeleted is null) and reportOperationUid = :operationUid and reportType=:reportType";
		List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"operationUid","reportType"}, new Object[]{userSelection.getOperationUid(), this.reportType});
		if(list.size() > 0) {
			for(String dailyUid : list) {
				matrix.addData(dailyUid,"N/A");
			}
		}
	}

}
