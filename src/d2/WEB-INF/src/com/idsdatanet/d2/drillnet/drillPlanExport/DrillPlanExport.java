package com.idsdatanet.d2.drillnet.drillPlanExport;

public class DrillPlanExport {
	private String id = null;
	private String wells = null;
	
	public DrillPlanExport() {
		
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setWells(String value) {
		this.wells = value;
	}
	
	public String getWells() {
		return this.wells;
	}
}
