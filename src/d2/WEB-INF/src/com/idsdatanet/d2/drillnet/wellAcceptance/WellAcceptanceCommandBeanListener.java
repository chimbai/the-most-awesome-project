package com.idsdatanet.d2.drillnet.wellAcceptance;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.WellAcceptance;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class WellAcceptanceCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {

	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{

		if(targetCommandBeanTreeNode != null){
			
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(WellAcceptance.class)){
				WellAcceptance wellAcceptance = (WellAcceptance) targetCommandBeanTreeNode.getData();
				
				if ("wellName".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())) {
					if (wellAcceptance.getWellName()!=null){
						
						Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellAcceptance.getWellName());
						//Well well = getWellByName(wellAcceptance.getWellName());
						
						if (well!=null){
							wellAcceptance.setTotalDepthMdMsl(getLastWellbore(well.getWellUid()));
							wellAcceptance.setWellUid(well.getWellUid());
							
							Operation op = getLastOperation(well.getWellUid());
							
							if (op!=null){
								wellAcceptance.setEndDateTime(op.getRigOffHireDate());
								wellAcceptance.setSpudDateTime(op.getSpudDate());
								wellAcceptance.setOperationUid(op.getOperationUid());
								wellAcceptance.setWellboreUid(op.getWellboreUid());
							}
							
						}	
					}						
				}
			}
		}
	}
	
	private Well getWellByName(String wellName) throws Exception{
		
		String strSql = "FROM Well WHERE (isDeleted is null or isDeleted = false) AND wellName = :wellName";
		String[] paramsFields = {"wellName"};
		Object[] paramsValues = {wellName};
		
		List<Well> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			Well well = (Well) lstResult.get(0);
			
			return well;
		}
		
		return null;
	}

	private Double getLastWellbore(String wellUid) throws Exception{
		
		Double finalTdMdMsl = null;
		
		String strSql = "FROM Wellbore WHERE (isDeleted is null or isDeleted = false) AND wellUid = :wellUid ORDER BY finalTdMdMsl DESC";
		String[] paramsFields = {"wellUid"};
		Object[] paramsValues = {wellUid};
		
		List<Wellbore> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			Wellbore wellbore = (Wellbore) lstResult.get(0);

			if (wellbore.getFinalTdMdMsl() != null) {
				return wellbore.getFinalTdMdMsl();
			}
		}
		
		return finalTdMdMsl;
	}
	
	private Operation getLastOperation(String wellUid) throws Exception{
		
		String strSql = "FROM Operation WHERE (isDeleted is null or isDeleted = false) AND wellUid = :wellUid ORDER BY rigOffHireDate DESC";
		String[] paramsFields = {"wellUid"};
		Object[] paramsValues = {wellUid};
		
		List<Operation> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			Operation op = (Operation) lstResult.get(0);
			
			return op;
		}
		
		return null;
	}
	
}
