package com.idsdatanet.d2.drillnet.formation;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.*;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.*;

import java.util.*;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;

public class DDRReportListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	private static final String TVDSS_REFERENCE_FIELD = "sampleTopTvdMsl";
	private static final String MDSS_REFERENCE_FIELD = "sampleTopMdMsl";
	private static final String PROG_TVD_MSL_REFERENCE_FIELD = "prognosedTopTvdMsl";
	private static final String DIFF_FROM_PROG_REFERENCE_FIELD = "diffFromProg";
	
	private static final String HIGH = "High";
	private static final String LOW = "Low";
	
	private String viewType;
	private boolean operationBased = false;
	private boolean wellboreBased = false;
	private String noBasin = null;
	
	public void setViewType(String value){
		this.viewType = value;
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return query;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(meta.getTableClass().equals(Formation.class)){
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null){
				return conditionClause.replace("{_custom_condition_}", "");
			}
			
			if (StringUtils.isNotBlank(GroupWidePreference.getValue(userSelection.getGroupUid(),GroupWidePreference.GWP_FORMATION_SHOW_LAST)))
			{
				query.rowsToFetch = Integer.parseInt(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_FORMATION_SHOW_LAST).toString());				
			}
			else
			{
				query.rowsToFetch = 4;
			}			
			int rowcounted = query.rowsToFetch;
			String SqlCondition = "";
			SqlCondition = conditionClause.toString();
			String gwpSorting = FormationUtils.sortPreference(userSelection, viewType);
			
			String thisFilter = "";
			//GET TODAY DEPTH FROM DAILY REPORT JUST TO FETCH THOSE RECORDS THAT LESS THAN LAST MIDNIGHT DEPTH RECORDED
			ReportDaily thisReportDaily = ApplicationUtils.getConfiguredInstance().getReportDailyByOperationType(daily.getDailyUid(), userSelection.getOperationType());
			String strReportType = null;
			if (thisReportDaily!=null) strReportType = thisReportDaily.getReportType();
			if(StringUtils.isBlank(strReportType)) return conditionClause.replace("{_custom_condition_}", "");
						
			String strSql = "SELECT rd.depthMdMsl as midNightDepth FROM ReportDaily rd WHERE (rd.isDeleted = false or rd.isDeleted is null) and rd.dailyUid = :thisDailyUid AND rd.operationUid = :thisOperationUid AND reportType = :thisReportType";
			
			String[] paramsFields = {"thisDailyUid", "thisOperationUid", "thisReportType"};
			Object[] paramsValues = {daily.getDailyUid(), daily.getOperationUid(), strReportType};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);			
			
			QueryProperties qp = new QueryProperties();
			qp.setRowsToFetch(0, rowcounted);

			if (conditionClause.indexOf("{_custom_condition_}") != -1 && !lstResult.isEmpty())
			{
				Object a = (Object) lstResult.get(0);
				Double midNightDepth = 0.00;
				if (a != null) midNightDepth = Double.parseDouble(a.toString());
				query.addParam("thisDepth", midNightDepth);
				thisFilter = "and sampleTopMdMsl <= :thisDepth";
				
				String gwpSortingdesc = "";
				for (String str:gwpSorting.split(","))
				{
					gwpSortingdesc += str + " DESC,";
				}
				if(StringUtils.isNotBlank(gwpSortingdesc)) gwpSortingdesc = StringUtils.substring(gwpSortingdesc, 0, -1);
				
				if (this.wellboreBased)
					SqlCondition = SqlCondition.replace("session.wellboreUid", ":thisWellboreUid");
				else if(this.operationBased)
					SqlCondition = SqlCondition.replace("session.operationUid", ":thisOperationUid");
				else
					SqlCondition = SqlCondition.replace("session.wellUid", ":thisWellUid");
				SqlCondition = SqlCondition.replace("{_custom_condition_}", thisFilter);
				List lstResult2 = null;
				
				String strSql2 = "SELECT formationUid FROM Formation WHERE (isDeleted = false or isDeleted is null) and " + SqlCondition + " ORDER BY " + gwpSortingdesc;
				if (this.wellboreBased){
					String[] paramsFields2 = {"thisWellboreUid","thisDepth"};
					Object[] paramsValues2 = {daily.getWellboreUid(),midNightDepth};
					lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
				}
				else if(this.operationBased){
					String[] paramsFields2 = {"thisOperationUid","thisDepth"};
					Object[] paramsValues2 = {daily.getOperationUid(),midNightDepth};
					lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
				}else{
					String[] paramsFields2 = {"thisWellUid","thisDepth"};
					Object[] paramsValues2 = {daily.getWellUid(),midNightDepth};
					lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
				}
				String formationUidList = "";
				if (lstResult2.size() > 0){
					for(Object objResult: lstResult2){
						Object thisFormationUid = (Object) objResult;
						if(thisFormationUid != null) formationUidList = formationUidList + "'" + thisFormationUid.toString() + "',";
					}
					if(StringUtils.isNotBlank(formationUidList)) formationUidList = StringUtils.substring(formationUidList, 0, -1);
					
					thisFilter += " and formationUid IN (" + formationUidList + ")";
				}		
			}
			else
			{
				query.rowsToFetch = 0;
			}
			return conditionClause.replace("{_custom_condition_}", thisFilter);
			
		}else{
			return null;
		}
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		String gwpSorting = FormationUtils.sortPreference(userSelection, viewType);
		return gwpSorting;
	}
	
	public void setOperationBased(boolean operationBased) {
		this.operationBased = operationBased;
	}

	public boolean isOperationBased() {
		return operationBased;
	}
	
	public void setWellboreBased(boolean wellboreBased) {
		this.wellboreBased = wellboreBased;
	}

	public boolean isWellboreBased() {
		return wellboreBased;
	}

	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof Formation) {
			Formation thisFormation = (Formation) object;
				
			int invert = (this.isInvert(userSelection.getGroupUid()) ? -1 : 1);
			
			Double prognosedTvd = null;
			Double actualTvd = null;
			if (thisFormation.getPrognosedTopTvdMsl() != null) {
				// - depth to datum reference point (without offset to msl)
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, PROG_TVD_MSL_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getPrognosedTopTvdMsl(), false);
				thisConverter.removeDatumOffsetToUserReferencePoint();
				prognosedTvd = thisConverter.getBasevalue()* invert;
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@progTvdMsl", thisConverter.getUOMMapping(false));
				}
				
				node.getDynaAttr().put("progTvdMsl", thisConverter.getConvertedValue() * invert);
				
				// - depth to subsea
				thisConverter = new CustomFieldUom(commandBean, Formation.class, PROG_TVD_MSL_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getPrognosedTopTvdMsl(), false);
				thisConverter.removeDatumOffset();
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@progTvdMslSubsea", thisConverter.getUOMMapping(false));
				}
				
				node.getDynaAttr().put("progTvdMslSubsea", thisConverter.getConvertedValue() * invert);
			}
			
			if (thisFormation.getSampleTopTvdMsl() != null) {	
				// - depth to datum reference point (without offset to msl)
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, TVDSS_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getSampleTopTvdMsl(), false);
				thisConverter.removeDatumOffsetToUserReferencePoint();
				actualTvd = thisConverter.getBasevalue()* invert;
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@tvdMsl", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("tvdMsl", thisConverter.getConvertedValue() * invert);
				
				// - depth to subsea
				thisConverter = new CustomFieldUom(commandBean, Formation.class, TVDSS_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getSampleTopTvdMsl(), false);
				thisConverter.removeDatumOffset();
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@tvdMslSubsea", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("tvdMslSubsea", thisConverter.getConvertedValue() * invert);
			}
			
			if (prognosedTvd!=null && actualTvd!=null) {
				Double different = prognosedTvd - actualTvd;
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, PROG_TVD_MSL_REFERENCE_FIELD);
				thisConverter.setBaseValue(different);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@diffTvd", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("diffTvd", thisConverter.getConvertedValue());
				
				//for GeoNet's absolute Diff Tvd and High/Low Status
				thisConverter = new CustomFieldUom(commandBean, Formation.class, DIFF_FROM_PROG_REFERENCE_FIELD);
				thisConverter.setBaseValue(Math.abs(different));
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@absDiffTvd", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("absDiffTvd", thisConverter.getConvertedValue());
				
				//check comparison
				if (different.compareTo(Double.valueOf(0)) < 0) {
					node.getDynaAttr().put("diffHighLowStatus", LOW);
				}
				else if (different.compareTo(Double.valueOf(0)) > 0) {
					node.getDynaAttr().put("diffHighLowStatus", HIGH);
				}

			}
						
			if (thisFormation.getSampleTopMdMsl() != null) {	
				// - depth to datum reference point (without offset to msl)
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, MDSS_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getSampleTopMdMsl(), false);
				thisConverter.removeDatumOffsetToUserReferencePoint();
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@mdMsl", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("mdMsl", thisConverter.getConvertedValue() * invert);
				
				// - depth to subsea
				thisConverter = new CustomFieldUom(commandBean, Formation.class, MDSS_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getSampleTopMdMsl(), false);
				thisConverter.removeDatumOffset();
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@mdMslSubsea", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("mdMslSubsea", thisConverter.getConvertedValue() * invert);
			}
			
			checkIfBasinSelected(node, userSelection.getWellUid(), node.getInfo().isFirstNodeInList()); // only check once per list (to improve performance)
			
			//get the eub formation code for formation name
			if (thisFormation.getFormationName() != null && userSelection.getWellUid() != null) {	
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
				String basinUid = "";
				basinUid = well.getBasin();
				String eub = CommonUtil.getConfiguredInstance().getEubCode(thisFormation.getFormationName(), basinUid);
				if (eub != null)
				{
					node.getDynaAttr().put("eubFormationCode", eub);
				}	
			}	
		}
	}

	private boolean isInvert(String groupUid) throws Exception {
		return "negative".equals(GroupWidePreference.getValue(groupUid, "structureformationgeol"));	
	}
	private void checkIfBasinSelected (CommandBeanTreeNode node, String wellUid, boolean doChecking) throws Exception {
		if(doChecking){
			this.noBasin = null;
			if (StringUtils.isNotBlank(wellUid)) {
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellUid);
				if (StringUtils.isBlank(well.getBasin())) {
					this.noBasin = "1";
				}
			}
		}

		if(this.noBasin != null){
			node.getDynaAttr().put("noBasin", this.noBasin);
		}
	}
}
