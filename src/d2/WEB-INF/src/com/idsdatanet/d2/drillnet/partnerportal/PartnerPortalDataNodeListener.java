package com.idsdatanet.d2.drillnet.partnerportal;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class PartnerPortalDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		String dynaAttrOperationUid = (String) commandBean.getRoot().getDynaAttr().get("selectOperationUid");
		
		if (StringUtils.isNotBlank(dynaAttrOperationUid)) {
			customCondition = "operationUid = :customFilterOperationUid";
			query.addParam("customFilterOperationUid", dynaAttrOperationUid);
		} else {
			customCondition = "operationUid = :customFilterOperationUid";
			query.addParam("customFilterOperationUid", "NO LOAD");
		}
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean arg0, UserSelectionSnapshot arg1, HttpServletRequest arg2, TreeModelDataDefinitionMeta arg3, DataDefinitionHQLQuery arg4, CommandBeanTreeNode arg5) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	/*
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		Object object = node.getData();

		if(object instanceof Operation) {
			
			Operation thisOperation = (Operation) object;
			
			
			}
		}
	}*/

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
}
