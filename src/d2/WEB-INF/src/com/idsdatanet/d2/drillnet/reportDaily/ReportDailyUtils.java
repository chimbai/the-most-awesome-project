package com.idsdatanet.d2.drillnet.reportDaily;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityLessonTicketLink;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CasingSummary;
import com.idsdatanet.d2.core.model.CustomCodeLink;
import com.idsdatanet.d2.core.model.CustomFtrLink;
import com.idsdatanet.d2.core.model.CustomParameterLink;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DailyShiftInfo;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.TourDaily;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.activity.ActivityUtils;
import com.idsdatanet.d2.drillnet.bharun.BharunUtils;
import com.idsdatanet.d2.drillnet.user.UserUtils;

public class ReportDailyUtils {
	public static void getDrillerInfo (CommandBeanTreeNode node, String operationUid, String groupUid, Date dayDate, CommandBean commandBean, boolean autoCalculateDepthTvdSs) throws Exception {
		Object object = node.getData();
		if (object instanceof ReportDaily) {
			ReportDaily reportDaily = (ReportDaily) object;
			String dailyUid ="";
			if (dayDate instanceof java.util.Date){
				
				//get dailyUid with Date enter by user
				String[] paramsFields = {"userDate", "thisOperationUid"};
				Object[] paramsValues = new Object[2]; paramsValues[0] = dayDate; paramsValues[1] = operationUid;
				String strSql = "SELECT dailyUid FROM Daily WHERE (isDeleted = false or isDeleted is null) and dayDate = :userDate AND operationUid = :thisOperationUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				if (!lstResult.isEmpty())
				{
					Object a = (Object) lstResult.get(0);
					if (a != null) {
						dailyUid = a.toString();					
					}
				}					
			}
			
			if (StringUtils.isBlank(dailyUid)) return;
			
			//Operation currentOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
			String defaultReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);

			String[] paramsFields2 = {"thisOperationUid", "thisDailyUid", "reportType"};
	 		Object[] paramsValues2 = new Object[3];
			paramsValues2[0] = operationUid;
			paramsValues2[1] = dailyUid;
			paramsValues2[2] = defaultReportType;
			
			String strSql2 = "select reportDailyUid FROM ReportDaily where operationUid = :thisOperationUid and dailyUid = :thisDailyUid and reportType = :reportType and (isDeleted = false or isDeleted is null)";
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2,paramsValues2);
			
			String ddrReportDailyUid = null;
			if (!lstResult2.isEmpty()) {
				Object b = (Object) lstResult2.get(0);
				if (b != null) ddrReportDailyUid = b.toString();
				
				ReportDaily ddrReportDaily = (ReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ReportDaily.class, ddrReportDailyUid);
				Map<String, Object> reportDailyFieldValues = PropertyUtils.describe(ddrReportDaily);
				for (Map.Entry entry : reportDailyFieldValues.entrySet()) {
					setDynaAttr(node, "ddr." + entry.getKey(), entry.getValue());
				}
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "depth_tvd_msl");
				if ("1".equals(GroupWidePreference.getValue(groupUid, GroupWidePreference.GWP_AUTO_CALCULATE_DAILY_DEPTH_TVDSS))) {	
				 	
					if (ddrReportDaily.getDepthTvdMsl() != null){
						thisConverter.setBaseValueFromUserValue(ddrReportDaily.getDepthTvdMsl(),true);
						Double tvdss = thisConverter.getBasevalue();
						thisConverter.setReferenceMappingField(ReportDaily.class, "subseaTvd");
						thisConverter.setBaseValue(tvdss);
						reportDaily.setSubseaTvd(thisConverter.getConvertedValue());
					}
				}
				
				//For negative output
				if (autoCalculateDepthTvdSs) {	
				 	
					if (ddrReportDaily.getDepthTvdMsl() != null){
						thisConverter.setBaseValueFromUserValue(ddrReportDaily.getDepthTvdMsl(),true);
						Double tvdss = thisConverter.getBasevalue();
						thisConverter.setReferenceMappingField(ReportDaily.class, "subseaTvd");
						thisConverter.setBaseValue(tvdss);
						reportDaily.setSubseaTvd(thisConverter.getConvertedValue());
						reportDaily.setSubseaTvd(reportDaily.getSubseaTvd()*-1);
					}
				}
				
				//GET DRILLER'S DRILLING CUMCOST, COMPLETION CUM COST, TANGIBLE CUM COST AND TOTAL CUM COST
				String[] paramsFields3 = {"todayDate", "operationUid", "reportType"};
				Object[] paramsValues3 = new Object[3]; 
				paramsValues3[0] = dayDate; 
				paramsValues3[1] = operationUid;
				
				//ALWAYS SET REPORT TYPE TO DDR TO GET DRILLER'S INFO FOR DGR
				//paramsValues3[2] = "DDR";
				paramsValues3[2] = defaultReportType;
				
				//GET Operation.amountSpentPriorToSpud, TO BE ADDED TO CUMULATIVE DAILY DRILLING COST
				//Double amountSpendPriorToSpud = 0.00;
		
				//if (currentOperation.getAmountSpentPriorToSpud() != null) amountSpendPriorToSpud = currentOperation.getAmountSpentPriorToSpud();
				
				//CUMULATIVE DRILLING COST = cumcost + ammountSpentPriorToSpud + cumcostadjust
				/*String strSql3 = "select sum(daycost) as cumcost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3);
				Object a = (Object) lstResult.get(0);
				Double cumCost = 0.00;
				if (a != null) cumCost = Double.parseDouble(a.toString());*/
				Double cumCost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(dailyUid, defaultReportType);
				
				/*String strSql4 = "select sum(costadjust) as cumcostadjust from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields3, paramsValues3);
				Object a = (Object) lstResult.get(0);
				Double cumcostadjust = 0.00;
				if (a != null) cumcostadjust = Double.parseDouble(a.toString());*/
				
				//cumCost = cumCost + amountSpendPriorToSpud + cumcostadjust;
				//cumCost = cumCost + cumcostadjust;
								
				//CUMULATIVE COMPLETION COST
				String strSql5 = "select sum(daycompletioncost) as cumcompletioncost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramsFields3, paramsValues3);
				Object a = (Object) lstResult.get(0);
				Double cumcompletioncost = null;
				if (a != null) cumcompletioncost = Double.parseDouble(a.toString());
				
				//CUMULATIVE TANGIBLE COST
				String strSql6 = "select sum(daytangiblecost) as cumtangiblecost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramsFields3, paramsValues3);
				a = (Object) lstResult.get(0);
				Double cumtangiblecost = null;
				if (a != null) cumtangiblecost = Double.parseDouble(a.toString());
				
				//TOTAL DAILY COST = daycost + daycompletioncost + daytangiblecost
				Double totalcost = null;
				if(ddrReportDaily.getDaycost() != null || ddrReportDaily.getDaycompletioncost() != null || ddrReportDaily.getDaytangiblecost() != null) totalcost = 0.0;
				if (ddrReportDaily.getDaycost() != null) totalcost = totalcost + ddrReportDaily.getDaycost();
				if (ddrReportDaily.getDaycompletioncost() != null)totalcost =  totalcost + ddrReportDaily.getDaycompletioncost();
				if (ddrReportDaily.getDaytangiblecost() != null)totalcost =  totalcost + ddrReportDaily.getDaytangiblecost();
				
				//CUMULATIVE TOTAL COST = cumcost(including amountSpentPriorToSpud and cumcostadjust) + cumcompletioncost + cumtangiblecost
				Double cumtotalcost = null;
				if ((cumCost!=null) || (cumcompletioncost !=null) || (cumtangiblecost !=null)) {
					cumtotalcost = 0.0;
					if (cumCost!=null) cumtotalcost += cumCost;
					if (cumcompletioncost!=null) cumtotalcost += cumcompletioncost;
					if (cumtangiblecost!=null) cumtotalcost += cumtangiblecost;
				}
								
				//ASSIGN UNIT TO THE VALUE BASED ON ReportDaily.daycost
				
				thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
				if (cumCost!=null) {
					thisConverter.setBaseValue(cumCost);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@ddr.cumcost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("ddr.cumcost", thisConverter.getConvertedValue());
				}
				
				if (cumcompletioncost!=null) {
					thisConverter.setBaseValue(cumcompletioncost);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@ddr.cumcompletioncost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("ddr.cumcompletioncost", thisConverter.getConvertedValue());
				}
				
				if (totalcost!=null) {
					thisConverter.setBaseValue(totalcost);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@ddr.totalcost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("ddr.totalcost", thisConverter.getConvertedValue());
				}
				
				if (cumtotalcost!=null) {
					thisConverter.setBaseValue(cumtotalcost);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@ddr.cumtotalcost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("ddr.cumtotalcost", thisConverter.getConvertedValue());
				}
				
				if (cumtangiblecost!=null) {
					thisConverter.setBaseValue(cumtangiblecost);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@ddr.cumtangiblecost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("ddr.cumtangiblecost", thisConverter.getConvertedValue());
				}							
				showCustomUOM(node, groupUid);
			}
		}
	}
	
	private static void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		}
	}
	
	private static void showCustomUOM(CommandBeanTreeNode node, String groupUid) throws Exception {
		Object object = node.getData();
		if (object instanceof ReportDaily) {
			CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
			// set units for standard fields in Well.class
			for (Field field : ReportDaily.class.getDeclaredFields()) {	
				thisConverter.setReferenceMappingField(ReportDaily.class, field.getName());
				if(thisConverter.getUOMMapping() != null){
					node.setCustomUOM("@ddr." + field.getName(), thisConverter.getUOMMapping());
				}
			}
			thisConverter.dispose();
		}
	}
	
	public static void autoPopulateBitDiameter(ReportDaily reportDaily, String defaultReportType, CustomFieldUom thisConverter) throws Exception {
		
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);
		
		thisConverter.setReferenceMappingField(ReportDaily.class, "currentBitDiameter");
		String strSql6 = "select bit.bitDiameter from Bharun bha, BharunDailySummary bds, Bitrun bit where bds.dailyUid =:dailyUid and (bds.isDeleted is null or bds.isDeleted ='') and bha.operationUid=:operationUid and bds.bharunUid=bha.bharunUid and (bha.isDeleted is null or bha.isDeleted ='') and bit.bharunUid=bha.bharunUid and (bit.isDeleted is null or bit.isDeleted ='')";
		String[] paramsFields6 = {"dailyUid","operationUid"};
		Object[] paramsValues6 = {reportDaily.getDailyUid(),reportDaily.getOperationUid()};
		List lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramsFields6, paramsValues6, qp);
		Boolean isNotSameDateOut = false;
		Boolean isNotSameDateIn = false;
		Date date1=null;
		Date date2 = null;
				
		//check selected day have bit record or not.If no bit record, then the bit diameter value no save into database. 
		if(lstResult6!=null && lstResult6.size()>0)
			{
				//The selected day have bit record. Retrieve all bit record which does not have date out.
				String strSql7 = "select bitDiameter from Bitrun where (isDeleted is null or isDeleted ='') and operationUid=:operationUid and bitrunUid in (select bit.bitrunUid from Bharun bha, BharunDailySummary bds, Bitrun bit where bds.dailyUid =:dailyUid and (bds.isDeleted is null or bds.isDeleted ='') and bha.operationUid=:operationUid and bds.bharunUid=bha.bharunUid and (bha.isDeleted is null or bha.isDeleted ='') and bit.bharunUid=bha.bharunUid and (bit.isDeleted is null or bit.isDeleted ='')) and (dailyUidOut='' or dailyUidOut is null)";
				String[] paramsFields7 = {"dailyUid","operationUid"};
				Object[] paramsValues7 = {reportDaily.getDailyUid(),reportDaily.getOperationUid()};
				List lstResult7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7, paramsFields7, paramsValues7, qp);
					
					if(lstResult7!=null && lstResult7.size()>0)
					{
						//Have bit record with no date out. Check on date in for those bit with no date out. 
						String strSql4 = "select bit.bitDiameter, rd.reportDatetime from Bitrun bit, ReportDaily rd where rd.dailyUid=bit.dailyUidIn and (rd.isDeleted is null or rd.isDeleted='') and (bit.isDeleted is null or bit.isDeleted ='') and bit.operationUid=:operationUid and bit.bitrunUid in (select bit.bitrunUid from Bharun bha, BharunDailySummary bds, Bitrun bit where bds.dailyUid =:dailyUid and (bds.isDeleted is null or bds.isDeleted ='') and bha.operationUid=:operationUid and bds.bharunUid=bha.bharunUid and (bha.isDeleted is null or bha.isDeleted ='') and bit.bharunUid=bha.bharunUid and (bit.isDeleted is null or bit.isDeleted ='')) and (bit.dailyUidOut='' or bit.dailyUidOut is null) group by bit.bitDiameter, rd.reportDatetime order by rd.reportDatetime desc";
						String[] paramsFields4 = {"dailyUid","operationUid"};
						Object[] paramsValues4 = {reportDaily.getDailyUid(),reportDaily.getOperationUid()};
						List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4, qp);
					    List lstResult8 = lstResult4;
						if(lstResult4!=null && lstResult4.size()>0)
						{
							//Have bit record with date in. Saved the latest date in into database.

							for(Object objBit: lstResult4)
								{
									Object[] BitRecord = (Object[]) objBit;
									if (BitRecord[1] != null){
										date1= (Date) BitRecord[1];
										
										for (Object objBitSecond: lstResult8)
										{
											Object[] BitRecordSecond = (Object[]) objBitSecond;
											date2 = (Date) BitRecordSecond[1];
											
											if(date1.compareTo(date2)!=0)
											{
												isNotSameDateIn = true;
											}
										}
									}
								}
								
								if(isNotSameDateIn)
								{
									Object[] BitRecord = (Object[]) lstResult4.get(0);
									if(BitRecord[0]!=null)
									{
										thisConverter.setBaseValue(Double.parseDouble(BitRecord[0].toString()));
										reportDaily.setCurrentBitDiameter(thisConverter.getConvertedValue());
										String strSql = "UPDATE ReportDaily SET currentBitDiameter =:bitDiameter WHERE reportDailyUid = :thisReportDailyUid";
										ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bitDiameter", "thisReportDailyUid"}, new Object[] {thisConverter.getConvertedValue(), reportDaily.getReportDailyUid()});
									}
								}
								else
								{
									Double bitDiameter = null;
									Object c = (Object) lstResult6.get(lstResult6.size() - 1);
									if(c!=null)
									{
										bitDiameter = Double.parseDouble(c.toString());
									}
									if(bitDiameter!=null)
									{
										thisConverter.setBaseValue(bitDiameter);
										reportDaily.setCurrentBitDiameter(thisConverter.getConvertedValue());
										String strSql = "UPDATE ReportDaily SET currentBitDiameter =:bitDiameter WHERE reportDailyUid = :thisReportDailyUid";
										ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bitDiameter", "thisReportDailyUid"}, new Object[] {thisConverter.getConvertedValue(), reportDaily.getReportDailyUid()});
									}
									else
									{
										reportDaily.setCurrentBitDiameter(null);
										String strSql = "UPDATE ReportDaily SET currentBitDiameter =:bitDiameter WHERE reportDailyUid = :thisReportDailyUid";
										ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bitDiameter", "thisReportDailyUid"}, new Object[] {null, reportDaily.getReportDailyUid()});
									}
								}
						}
						else
						{
							// The bit record does not have date in value. Save the last record into database.
							
							Double bitDiameter = null;
							Object a = (Object) lstResult6.get(lstResult6.size() - 1);
							if(a!=null)
							{
								bitDiameter = Double.parseDouble(a.toString());
							}
							
							if(bitDiameter!=null)
							{
								thisConverter.setBaseValue(bitDiameter);
								reportDaily.setCurrentBitDiameter(thisConverter.getConvertedValue());
								String strSql = "UPDATE ReportDaily SET currentBitDiameter =:bitDiameter WHERE reportDailyUid = :thisReportDailyUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bitDiameter", "thisReportDailyUid"}, new Object[] {thisConverter.getConvertedValue(), reportDaily.getReportDailyUid()});
							}
							else
							{
								reportDaily.setCurrentBitDiameter(null);
								String strSql = "UPDATE ReportDaily SET currentBitDiameter =:bitDiameter WHERE reportDailyUid = :thisReportDailyUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bitDiameter", "thisReportDailyUid"}, new Object[] {null, reportDaily.getReportDailyUid()});
							}
						}
					}
					// does not have bit record which does not have date out. 
					else
					{	
						String strSql8 = "select bit.bitDiameter from Bitrun bit, ReportDaily rd where rd.dailyUid=bit.dailyUidOut and (rd.isDeleted is null or rd.isDeleted='') and (bit.isDeleted is null or bit.isDeleted ='') and bit.operationUid=:operationUid and bit.bitrunUid in (select bit.bitrunUid from Bharun bha, BharunDailySummary bds, Bitrun bit where bds.dailyUid =:dailyUid and (bds.isDeleted is null or bds.isDeleted ='') and bha.operationUid=:operationUid and bds.bharunUid=bha.bharunUid and (bha.isDeleted is null or bha.isDeleted ='') and bit.bharunUid=bha.bharunUid and (bit.isDeleted is null or bit.isDeleted ='')) group by bit.bitDiameter";
						String[] paramsFields8 = {"dailyUid","operationUid"};
						Object[] paramsValues8 = {reportDaily.getDailyUid(),reportDaily.getOperationUid()};
						List lstResult8 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql8, paramsFields8, paramsValues8, qp);
						
						if(lstResult8!=null && lstResult8.size()>0)
						{
							//Have bit record which have date out value.Check on the date out see is same or not.
							
							String strSql4 = "select bit.bitDiameter, rd.reportDatetime from Bitrun bit, ReportDaily rd where rd.dailyUid=bit.dailyUidOut and (rd.isDeleted is null or rd.isDeleted='') and (bit.isDeleted is null or bit.isDeleted ='') and bit.operationUid=:operationUid and bit.bitrunUid in (select bit.bitrunUid from Bharun bha, BharunDailySummary bds, Bitrun bit where bds.dailyUid =:dailyUid and (bds.isDeleted is null or bds.isDeleted ='') and bha.operationUid=:operationUid and bds.bharunUid=bha.bharunUid and (bha.isDeleted is null or bha.isDeleted ='') and bit.bharunUid=bha.bharunUid and (bit.isDeleted is null or bit.isDeleted ='')) group by bit.bitDiameter, rd.reportDatetime order by rd.reportDatetime desc";
							String[] paramsFields4 = {"dailyUid","operationUid"};
							Object[] paramsValues4 = {reportDaily.getDailyUid(),reportDaily.getOperationUid()};
							List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4, qp);
							List lstResult9 = lstResult4;
							if(lstResult4!=null && lstResult4.size()>0)
							{
								for(Object objBit: lstResult9)
								{
									Object[] BitRecord = (Object[]) objBit;
									if (BitRecord[1] != null){
										date1= (Date) BitRecord[1];
										
										for (Object objBitSecond: lstResult4)
										{
											Object[] BitRecordSecond = (Object[]) objBitSecond;
											date2 = (Date) BitRecordSecond[1];
											
											if(date1.compareTo(date2)!=0)
											{
												isNotSameDateOut = true;
											}
										}
									}
								}
								
								if(isNotSameDateOut)
								{
									Object[] BitRecord = (Object[]) lstResult4.get(0);
									
									if(BitRecord[0]!=null)
									{
										
										thisConverter.setBaseValue(Double.parseDouble(BitRecord[0].toString()));
										reportDaily.setCurrentBitDiameter(thisConverter.getConvertedValue());
										String strSql = "UPDATE ReportDaily SET currentBitDiameter =:bitDiameter WHERE reportDailyUid = :thisReportDailyUid";
										ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bitDiameter", "thisReportDailyUid"}, new Object[] {thisConverter.getConvertedValue(), reportDaily.getReportDailyUid()});
									}
								}
								//The bit record have same date out
								else
								{
									//check whether the bit record have date in or not. 
									String strSql1 = "select bit.bitDiameter, rd.reportDatetime from Bitrun bit, ReportDaily rd where rd.dailyUid=bit.dailyUidIn and (rd.isDeleted is null or rd.isDeleted='') and (bit.isDeleted is null or bit.isDeleted ='') and bit.operationUid=:operationUid and bit.bitrunUid in (select bit.bitrunUid from Bharun bha, BharunDailySummary bds, Bitrun bit where bds.dailyUid =:dailyUid and (bds.isDeleted is null or bds.isDeleted ='') and bha.operationUid=:operationUid and bds.bharunUid=bha.bharunUid and (bha.isDeleted is null or bha.isDeleted ='') and bit.bharunUid=bha.bharunUid and (bit.isDeleted is null or bit.isDeleted ='')) group by bit.bitDiameter, rd.reportDatetime order by rd.reportDatetime desc";
									String[] paramsFields1 = {"dailyUid","operationUid"};
									Object[] paramsValues1 = {reportDaily.getDailyUid(),reportDaily.getOperationUid()};
									List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
									List lstResult2 = lstResult1;
									if(lstResult1!=null && lstResult1.size()>0)
									{
										//The bit record have date in value.
										
										for(Object objBit: lstResult2)
										{
											Object[] BitRecord = (Object[]) objBit;
											if (BitRecord[1] != null){
												date1= (Date) BitRecord[1];
												
												for (Object objBitSecond: lstResult1)
												{
													Object[] BitRecordSecond = (Object[]) objBitSecond;
													date2 = (Date) BitRecordSecond[1];
													
													if(date1.compareTo(date2)!=0)
													{
														isNotSameDateIn = true;
													}
												}
											}
											
										}
										if(isNotSameDateIn)
										{
											Object[] BitRecord = (Object[]) lstResult1.get(0);
											if(BitRecord[0]!=null)
											{
												
												thisConverter.setBaseValue(Double.parseDouble(BitRecord[0].toString()));
												reportDaily.setCurrentBitDiameter(thisConverter.getConvertedValue());
												String strSql = "UPDATE ReportDaily SET currentBitDiameter =:bitDiameter WHERE reportDailyUid = :thisReportDailyUid";
												ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bitDiameter", "thisReportDailyUid"}, new Object[] {thisConverter.getConvertedValue(), reportDaily.getReportDailyUid()});
											}
										}
										else
										{
											Double bitDiameter = null;
											Object a = (Object) lstResult6.get(lstResult6.size() - 1);
											if(a!=null)
											{
												bitDiameter = Double.parseDouble(a.toString());
											}
											if(bitDiameter!=null)
											{
												thisConverter.setBaseValue(bitDiameter);
												reportDaily.setCurrentBitDiameter(thisConverter.getConvertedValue());
												String strSql = "UPDATE ReportDaily SET currentBitDiameter =:bitDiameter WHERE reportDailyUid = :thisReportDailyUid";
												ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bitDiameter", "thisReportDailyUid"}, new Object[] {thisConverter.getConvertedValue(), reportDaily.getReportDailyUid()});
											}
											else
											{
												reportDaily.setCurrentBitDiameter(null);
												String strSql = "UPDATE ReportDaily SET currentBitDiameter =:bitDiameter WHERE reportDailyUid = :thisReportDailyUid";
												ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bitDiameter", "thisReportDailyUid"}, new Object[] {null, reportDaily.getReportDailyUid()});
											}
										}
										
									}
									else
									{
										Double bitDiameter = null;
										Object a = (Object) lstResult6.get(lstResult6.size() - 1);
										
										if(a!=null)
										{
											bitDiameter = Double.parseDouble(a.toString());
										}
										if(bitDiameter!=null)
										{
											thisConverter.setBaseValue(bitDiameter);
											String strSql = "UPDATE ReportDaily SET currentBitDiameter =:bitDiameter WHERE reportDailyUid = :thisReportDailyUid";
											ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bitDiameter", "thisReportDailyUid"}, new Object[] {thisConverter.getConvertedValue(), reportDaily.getReportDailyUid()});
										}
										else
										{
											String strSql = "UPDATE ReportDaily SET currentBitDiameter =:bitDiameter WHERE reportDailyUid = :thisReportDailyUid";
											ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bitDiameter", "thisReportDailyUid"}, new Object[] {null, reportDaily.getReportDailyUid()});
										}
									}
								}
							}
						}
					}
				}
				else
				{
					reportDaily.setCurrentBitDiameter(null);
				}
		 	
		
	}
	
	public static void getOperationAfeAmt(CommandBeanTreeNode node, String operationUid) throws Exception {
		
		//get afe, secondary afe and cum. afe from operation table				
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
		Double afe = operation.getAfe();
		Double suppAfe = operation.getSecondaryafeAmt();
		Double cumAfe = null; 
	
		CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(), Operation.class, "afe");
		
		if (afe!=null) {
			thisConverter.setBaseValue(afe);
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@afe", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("afe", thisConverter.getConvertedValue());
			
			if (cumAfe==null) cumAfe = 0.0;
			cumAfe += afe;
		}
		
		if (suppAfe!=null) {
			thisConverter.setBaseValue(suppAfe);
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@secondaryafeAmt", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("secondaryafeAmt", thisConverter.getConvertedValue());

			if (cumAfe==null) cumAfe = 0.0;
			cumAfe += suppAfe;
		}
		
		if (cumAfe!=null) {
			thisConverter.setBaseValue(cumAfe);
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@cumafeAmt", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("cumafeAmt", thisConverter.getConvertedValue());
		}
	}
	
	public static void calculateDrillingAheadAvgRop(CommandBeanTreeNode node, String operationUid, String dailyUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		List reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid", "dailyUid", dailyUid, qp);
		ReportDaily reportDaily = null;
		
		if(reportDailyList != null && reportDailyList.size() > 0) {
			reportDaily = (ReportDaily) reportDailyList.get(0);
		} else {
			return;
		}
		
		//CustomFieldUom thisConverter = new CustomFieldUom((Locale) null, BharunDailySummary.class, "progress");
		
		//calculate average ROP for Drilling Ahead task - 20100111-0509-asim
		Double dailyDrillingAheadAvgRop = 0.0;
		
		Double dailyTotalBharunProgress = BharunUtils.dailyTotalProgress(operationUid, dailyUid);
		//thisConverter.setBaseValue(dailyTotalBharunProgress);
		//dailyTotalBharunProgress = thisConverter.getConvertedValue();
				
		Double totalDrillingAheadHours = 0.0;
		String strSql = "SELECT SUM(activityDuration) FROM Activity WHERE (isDeleted = false OR isDeleted is null)" +
						" AND operationUid =:operationUid AND dailyUid =:dailyUid" +
						" AND taskCode = 'D'";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"operationUid", "dailyUid"}, new Object[]{operationUid, dailyUid}, qp);
		Object a = (Object) lstResult.get(0);
		if (a != null) totalDrillingAheadHours = Double.parseDouble(a.toString());
		//thisConverter.setReferenceMappingField(DrillingParameters.class, "duration");
		//totalDrillingAheadHours = thisConverter.getConvertedValue();
		
		if (totalDrillingAheadHours > 0) {
			dailyDrillingAheadAvgRop = dailyTotalBharunProgress / totalDrillingAheadHours;
		}
		
		reportDaily.setDrillingAheadAvgRop(dailyDrillingAheadAvgRop);
		
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportDaily, qp);
	}
	
	/**
	 * Method to validate user password
	 * @param node current node either Report Daily or Daily Tour
	 * @param commandBean
	 * @param request
	 * @throws Exception
	 */
	public static Boolean validatePassword(HttpServletRequest request) throws Exception
	{
		String userUid = request.getParameter("userUid");
		String password = request.getParameter("password");
		if (userUid!=null && password!=null)
			return BooleanUtils.isTrue(UserUtils.validateUser(userUid, password, UserSession.getInstance(request)));
		return false;
	}
	
	/**
	 * Method to set the lock status
	 * @param node
	 * @param commandBean
	 * @param request
	 * @throws Exception
	 * @return no return
	 */
	public static void setStatus(CommandBeanTreeNode node, CommandBean commandBean, HttpServletRequest request) throws Exception
	{
		String targetField = node.getInfo().getSubmitForServerSideProcessTargetField();
		if(ReportDaily.class.equals(node.getDataDefinition().getTableClass())){
			ReportDaily rd = (ReportDaily)node.getData();
			if("operatorStatus".equalsIgnoreCase(targetField)){
				Boolean isLocked = !BooleanUtils.isTrue(rd.getOperatorStatus());
				Boolean isOtherLocked = BooleanUtils.isTrue(rd.getContractorStatus());
				Boolean needUpdateReportStatus = (isLocked && isOtherLocked)!= BooleanUtils.isTrue(rd.getReportStatus());
				String reportStatus = " , reportStatus="+!BooleanUtils.isTrue(rd.getReportStatus());
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE ReportDaily set operatorStatus=:operatorStatus, lastEditDatetime=:lastEditDatetime "+(needUpdateReportStatus?reportStatus:"")+" WHERE reportDailyUid=:reportDailyUid", new String[]{"reportDailyUid","operatorStatus","lastEditDatetime"}, new Object[]{rd.getReportDailyUid(),isLocked, new Date()});
				commandBean.getFlexClientControl().setReloadParentHtmlPage();
			}else if ("contractorStatus".equalsIgnoreCase(targetField))
			{
				Boolean isLocked = !BooleanUtils.isTrue(rd.getContractorStatus());
				Boolean isOtherLocked = BooleanUtils.isTrue(rd.getOperatorStatus());
				Boolean needUpdateReportStatus = (isLocked && isOtherLocked)!= BooleanUtils.isTrue(rd.getReportStatus());
				String reportStatus = " , reportStatus="+!BooleanUtils.isTrue(rd.getReportStatus());
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE ReportDaily set contractorStatus=:contractorStatus, lastEditDatetime=:lastEditDatetime "+(needUpdateReportStatus?reportStatus:"")+" WHERE reportDailyUid=:reportDailyUid", new String[]{"reportDailyUid","contractorStatus","lastEditDatetime"}, new Object[]{rd.getReportDailyUid(),isLocked, new Date()});
				commandBean.getFlexClientControl().setReloadParentHtmlPage();
			}
		}else if (TourDaily.class.equals(node.getDataDefinition().getTableClass())){
			TourDaily td = (TourDaily)node.getData();
			if("tourStatus".equalsIgnoreCase(targetField)){
				Boolean isLocked = !BooleanUtils.isTrue(td.getTourStatus());
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE TourDaily set tourStatus=:tourStatus, lastEditDatetime=:lastEditDatetime WHERE tourDailyUid=:tourDailyUid", new String[]{"tourDailyUid","tourStatus","lastEditDatetime"}, new Object[]{td.getTourDailyUid(),isLocked, new Date()});
				commandBean.getFlexClientControl().setReloadParentHtmlPage();
			} 
		}
	}
	
	/**
	 * method to set is daily lock flag to the current node's dynamic attributes
	 * @param node
	 * @throws Exception
	 * @return no return
	 */
	public static void setDailyLockFlag(CommandBeanTreeNode node) throws Exception
	{
		CommandBeanTreeNode nodeRD = getReportDailyFromParent(node);
		if (nodeRD.getData() instanceof ReportDaily)
		{
			ReportDaily rd = (ReportDaily) nodeRD.getData();
			if (BooleanUtils.isTrue(rd.getReportStatus()))
			{
				node.getDynaAttr().put("isDayLocked", "1");
			}
		}
	}
	/**
	 * Method to get Report Daily from Parent node
	 * @param node
	 * @return Node object that is report daily
	 * @throws Exception
	 */
	public static CommandBeanTreeNode getReportDailyFromParent(CommandBeanTreeNode node) throws Exception{
		if (node.getData() instanceof ReportDaily)
			return node;
		else
		{
			if (node.getParent()!=null)
			{
				return getReportDailyFromParent(node.getParent());
			}
		}
		return null;
	}
	
	/**
	 * Method to revert next day activity when a daily record is deleted - also restore relation for unwanted event, lesson ticket
	 * @param yesterdayDailyUid
	 * @param todayDailyUid
	 * @param session 
	 * @throws Exception
	 */
	public static void revertNextDayActivityWhenDeleteDaily(String yesterdayDailyUid, String todayDailyUid, UserSession session) throws Exception
	{
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
		
		if (StringUtils.isNotBlank(yesterdayDailyUid))
		{
			String[] paramsFields5 = {"thisTodayDailyUid"};
			String[] paramsValues5 = {todayDailyUid};
			String strSql5 = "FROM Activity WHERE (isDeleted = false or isDeleted is null) and dailyUid = :thisTodayDailyUid";
			List <Activity>result5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramsFields5, paramsValues5);
			
			if (result5.size()>0)
			{
				for(Activity todayActivity : result5){
					String todayActivityUid = todayActivity.getActivityUid();
					String[] paramsFields6 = {"todayActivityUid"};
					String[] paramsValues6 = {todayActivityUid};
					String strSql6 = "FROM Activity WHERE (isDeleted = false or isDeleted is null) and carriedForwardActivityUid = :todayActivityUid";
					List <Activity>result6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramsFields6, paramsValues6);
					if (result6.size() > 0) {
						String yesterdayActivityUid = result6.get(0).getActivityUid();
						
						//Unwanted event section checking
						String[] paramsFields7 = {"todayActivityUid"};
						String[] paramsValues7 = {todayActivityUid};
						String strSql7 = "FROM UnwantedEvent WHERE (isDeleted = false or isDeleted is null) and activityUid = :todayActivityUid AND type='UE' ";
						List <UnwantedEvent>result7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7, paramsFields7, paramsValues7);
						if (result7.size()>0){
							for(UnwantedEvent todayUnwantedEvent : result7){
								String todayUnwantedEventUid = todayUnwantedEvent.getUnwantedEventUid();
								String[] paramsFields8 = {"yesterdayActivityUid", "todayUnwantedEventUid"};
								String[] paramsValues8 = {yesterdayActivityUid, todayUnwantedEventUid};
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update UnwantedEvent set activityUid = :yesterdayActivityUid where unwantedEventUid = :todayUnwantedEventUid", paramsFields8, paramsValues8);
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update ActivityUnwantedEventLink set activityUid = :yesterdayActivityUid where unwantedEventUid = :todayUnwantedEventUid", paramsFields8, paramsValues8);
							}
						}
						
						if (todayActivity.getNptEventUid() != null) {
							String[] paramsFields10 = {"unwantedEventUid"};
							String[] paramsValues10 = {todayActivity.getNptEventUid()};
							String strSql10 = "FROM UnwantedEvent WHERE (isDeleted = false or isDeleted is null) and unwantedEventUid = :unwantedEventUid AND type='nptEvent' ";
							List <UnwantedEvent> result10 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql10,  paramsFields10, paramsValues10);
							if (result10 != null && result10.size() > 0) {
								UnwantedEvent ueNpt = result10.get(0);
								ueNpt.setDailyUid(yesterdayDailyUid);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ueNpt);
								
								String[] paramsFieldsNE = {"nptEventUid", "yesterdayActivityUid"};
								String[] paramsValuesNE = {todayActivity.getNptEventUid(), yesterdayActivityUid};
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update Activity set nptEventUid = :nptEventUid where activityUid = :yesterdayActivityUid", paramsFieldsNE, paramsValuesNE);
								
								todayActivity.setNptEventUid(null);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(todayActivity);
							}	
						}
						
						//check Lesson Ticket
						String strSql9 = "FROM ActivityLessonTicketLink WHERE (isDeleted = false or isDeleted is null) AND activityUid = :todayActivityUid";
						List <ActivityLessonTicketLink>result9 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql9, paramsFields7, paramsValues7);
						String strSq20 = "";
						
						Daily yesterday = ApplicationUtils.getConfiguredInstance().getCachedDaily(yesterdayDailyUid);
						String prefixPattern = GroupWidePreference.getValue(session.getCurrentGroupUid(), "lessonTicketNumberPrefixPattern");
						String getPrefixName = CommonUtil.getConfiguredInstance().formatDisplayPrefix(prefixPattern, userSelection, session.getCurrentOperation(), yesterday, null);
						
						if(result9.size()>0){
							for(ActivityLessonTicketLink todayActivityLessonTicketLink : result9){
								todayActivityLessonTicketLink.setIsDeleted(true);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(todayActivityLessonTicketLink);
								
								strSq20 = "FROM ActivityLessonTicketLink WHERE isDeleted = true AND sysDeleted = 3 AND activityUid = :yesterdayActivityUid";
								List <ActivityLessonTicketLink> result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSq20, new String[]{"yesterdayActivityUid"}, new String[]{yesterdayActivityUid});
								if (result2.size() > 0) {
									for (ActivityLessonTicketLink yesterdayActivityLessonTicketLink : result2)  {
										yesterdayActivityLessonTicketLink.setIsDeleted(null);
										yesterdayActivityLessonTicketLink.setSysDeleted(null);
										ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(yesterdayActivityLessonTicketLink);
										
										String sql21 = "FROM LessonTicket WHERE lessonTicketUid = :lessonTicketUid AND (isDeleted = FALSE or isDeleted IS NULL)";
										List<LessonTicket> result3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql21, new String[]{"lessonTicketUid"}, new String[]{yesterdayActivityLessonTicketLink.getLessonTicketUid()});
										if (result3 != null && !result3.isEmpty()) {
											LessonTicket lt = (LessonTicket) result3.get(0);
											lt.setLessonTicketNumberPrefix(getPrefixName);
											lt.setRp2Date(yesterday.getDayDate());
											ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lt);
										}
									}
								}
							}
						}
						
						
						//check on FTR link tables, custom code link, custom ftr link, custom paramter link
						String strSql = "FROM CustomFtrLink WHERE (isDeleted = false or isDeleted is null) and activityUid = :todayActivityUid";
						List <CustomFtrLink> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields7, paramsValues7);
						String strSql2 = "";
						if (result.size() > 0) {
							for (CustomFtrLink todayCustomFtrLink : result) {
								todayCustomFtrLink.setIsDeleted(true);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(todayCustomFtrLink);
								
								strSql2 = "FROM CustomFtrLink WHERE isDeleted = true and sysDeleted = 3 and activityUid = :yesterdayActivityUid";
								List <CustomFtrLink> result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[]{"yesterdayActivityUid"}, new String[]{yesterdayActivityUid});
								if (result2.size() > 0) {
									for (CustomFtrLink yesterdayCustomFtrLink : result2)  {
										yesterdayCustomFtrLink.setIsDeleted(null);
										yesterdayCustomFtrLink.setSysDeleted(null);
										ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(yesterdayCustomFtrLink);
									}
								}
							}
						}
						
						strSql = "FROM CustomParameterLink WHERE (isDeleted = false or isDeleted is null) and activityUid = :todayActivityUid";
						List <CustomParameterLink> resultCPL = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields7, paramsValues7);
						if (resultCPL.size() > 0) {
							for (CustomParameterLink todayCustomParameterLink : resultCPL) {
								todayCustomParameterLink.setIsDeleted(true);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(todayCustomParameterLink);
								
								strSql2 = "FROM CustomParameterLink WHERE isDeleted = true and sysDeleted = 3 and activityUid = :yesterdayActivityUid";
								List <CustomParameterLink> result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[]{"yesterdayActivityUid"}, new String[]{yesterdayActivityUid});
								if (result2.size() > 0) {
									for (CustomParameterLink yesterdayCustomParameterLink : result2)  {
										yesterdayCustomParameterLink.setIsDeleted(null);
										yesterdayCustomParameterLink.setSysDeleted(null);
										ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(yesterdayCustomParameterLink);
									}
								}
							}
						}
						
						strSql = "FROM CustomCodeLink WHERE (isDeleted = false or isDeleted is null) and activityUid = :todayActivityUid";
						List <CustomCodeLink> resultCCL = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields7, paramsValues7);
						if (resultCCL.size() > 0) {
							for (CustomCodeLink todayCustomCodeLink : resultCCL) {
								todayCustomCodeLink.setIsDeleted(true);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(todayCustomCodeLink);
								
								strSql2 = "FROM CustomCodeLink WHERE isDeleted = true and sysDeleted = 3 and activityUid = :yesterdayActivityUid";
								List <CustomCodeLink> result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[]{"yesterdayActivityUid"}, new String[]{yesterdayActivityUid});
								if (result2.size() > 0) {
									for (CustomCodeLink yesterdayCustomCodeLink : result2)  {
										yesterdayCustomCodeLink.setIsDeleted(null);
										yesterdayCustomCodeLink.setSysDeleted(null);
										ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(yesterdayCustomCodeLink);
									}
								}
							}
						}
						
						String[] tableNameList = {"FtrTripping","FtrBopEquipment","FtrDrillLineEquipment","FtrRig","FtrDrilling","FtrWorkoverCompletionSystem"};
						List<Object> ftrList = null;
						List<Object> ftrList2 = null;
						for (String tableName : tableNameList){
							String queryString = "from " + tableName +" where (isDeleted is null or isDeleted = FALSE) and activityUid = :todayActivityUid";
							ftrList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramsFields7, paramsValues7);
							if (ftrList != null && ftrList.size() > 0) {
								for (int i = 0; i < ftrList.size(); i++) {
									Object o = ftrList.get(i);
									PropertyUtils.setProperty(o,"isDeleted", true);
									ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(o);
									
									strSql2 = "from " + tableName +" where isDeleted = true and sysDeleted = 3 and activityUid = :yesterdayActivityUid";
									ftrList2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[]{"yesterdayActivityUid"}, new String[]{yesterdayActivityUid});
									if (ftrList2.size() > 0) {
										for (int x = 0; x < ftrList2.size(); x++)  {
											Object o2 = ftrList2.get(x);
											PropertyUtils.setProperty(o2,"isDeleted", null);
											PropertyUtils.setProperty(o2,"sysDeleted", null);
											ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(o2);
										}
									}
								}
							}
						}
					}
					//check HSE Incident
					//check Equipment Failure
					//check Kick
					
					String[] paramsFields15 = {"thisTodayActivityUid", "thisYesterdayDailyUid"};
					String[] paramsValues15 = {todayActivityUid, yesterdayDailyUid};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update Activity set carriedForwardActivityUid = null, sysDeleted='7' where carriedForwardActivityUid = :thisTodayActivityUid and dailyUid = :thisYesterdayDailyUid", paramsFields15, paramsValues15);
				}
			}
			
			String[] paramsFields16 = {"thisYesterdayDailyUid"};
			String[] paramsValues16 = {yesterdayDailyUid};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update Activity set carriedForwardActivityUid = null, sysDeleted='7' where dailyUid = :thisYesterdayDailyUid and (isDeleted = false or isDeleted is null) and dayPlus=1", paramsFields16, paramsValues16);
		}		
	}
	
	/**
	 * Method to clean BHA records after a daily is deleted
	 * @param todayDailyUid
	 * @throws Exception
	 */
	public static void cleanBHADailyWhenDeleteDaily(String todayDailyUid) throws Exception
	{
		if (StringUtils.isNotBlank(todayDailyUid))
		{
			// Checking BHA - JWONG BharunDailySummary
			String[] paramsFields1 = {"thisDailyUid"};
			String[] paramsValues1 = {todayDailyUid};
			String strSql2 = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and dailyUid = :thisDailyUid";
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields1, paramsValues1);
			if (result.size()>0)
			{
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update BharunDailySummary set isDeleted = true where dailyUid = :dailyUid", new String[] {"dailyUid"}, new Object[] {todayDailyUid});
				for(Iterator i = result.iterator(); i.hasNext(); ){
					Object obj_array = (Object) i.next();
					BharunDailySummary bharunDailySummary = (BharunDailySummary) obj_array;
					
					String[] paramsFields3 = {"thisBharunUid"};
					String[] paramsValues3 = {bharunDailySummary.getBharunUid()};
					String strSql4 = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and bharunUid = :thisBharunUid";
					List result4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields3, paramsValues3);
					if (result4.isEmpty())
					{
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update Bharun set isDeleted = true  where bharunUid = :bharunUid", new String[] {"bharunUid"}, new Object[] {bharunDailySummary.getBharunUid()});
						String[] paramsFields2 = {"thisBharunUid"};
						String[] paramsValues2 = {bharunDailySummary.getBharunUid()};
						String strSql3 = "FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid = :thisBharunUid";
						List result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields2, paramsValues2);
						if (result2.size()>0)
						{
							ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update Bitrun set isDeleted = true  where bharunUid = :bharunUid", new String[] {"bharunUid"}, new Object[] {bharunDailySummary.getBharunUid()});
						}
					}
				}
			}
		}
	}
	
	public static void autoLookupCasingSectionData(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception {
		Object object = node.getData();		
		if(object instanceof ReportDaily) {
			ReportDaily thisReport = (ReportDaily) object;
			thisReport.setSysReportDailySubclass(ModuleConstants.MODULE_KEY_WELL);
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_CASING_SECTON_DATA))) {
				Object objResult = CommonUtil.getConfiguredInstance().lastCasingSection(session.getCurrentGroupUid(), session.getCurrentWellboreUid(), session.getCurrentDailyUid());
				if (objResult != null) {
					if (objResult instanceof CasingSection) {
						CasingSection thisCasingSection = (CasingSection) objResult;
						
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "last_csgshoe_md_msl");						
						if (thisCasingSection.getShoeTopMdMsl() != null) {
							thisConverter.setBaseValue(thisCasingSection.getShoeTopMdMsl());
							thisReport.setLastCsgshoeMdMsl(thisConverter.getConvertedValue());
						}
						if (thisCasingSection.getShoeTopTvdMsl() != null) {
							thisConverter.setReferenceMappingField(ReportDaily.class, "last_csgshoe_tvd_msl");							
							thisConverter.setBaseValue(thisCasingSection.getShoeTopTvdMsl());
							thisReport.setLastCsgshoeTvdMsl(thisConverter.getConvertedValue());
						}
						
						//due to casing size drop down must be in m and 6 decimal so no need to get raw value
						if (thisCasingSection.getCasingOd() != null) {
							thisConverter.setReferenceMappingField(ReportDaily.class, "last_csgsize");
							thisConverter.setBaseValue(thisCasingSection.getCasingOd());
							thisReport.setLastCsgsize(thisConverter.getConvertedValue());
						}
					}
				}
			} else {
				if ("2".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_CASING_SECTON_DATA))) {
					Object objResult = CommonUtil.getConfiguredInstance().lastCasingSection(session.getCurrentGroupUid(), session.getCurrentWellboreUid(), session.getCurrentDailyUid());
					if (objResult != null) {
						if (objResult instanceof CasingSection) {
							CasingSection thisCasingSection = (CasingSection) objResult;

							CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "last_csgshoe_md_msl");
							if (thisCasingSection.getShoeTopMdMsl() != null) {
								thisConverter.setBaseValue(thisCasingSection.getShoeTopMdMsl());
								thisReport.setLastCsgshoeMdMsl(thisConverter.getConvertedValue());
							}
							if (thisCasingSection.getShoeTopTvdMsl() != null) {
								thisConverter.setReferenceMappingField(ReportDaily.class, "last_csgshoe_tvd_msl");
								thisConverter.setBaseValue(thisCasingSection.getShoeTopTvdMsl());
								thisReport.setLastCsgshoeTvdMsl(thisConverter.getConvertedValue());
							}
							if (thisCasingSection.getCasingSectionUid() != null) {
								// based on last casing section record's UID, filter casing summary list to get those latest casing size
								String lastCsgSumCsgSize = CasingSummaryCasingSize(thisCasingSection.getCasingSectionUid(),session, thisConverter);	
								if (lastCsgSumCsgSize != null && StringUtils.isNotBlank(lastCsgSumCsgSize)){
									thisReport.setLastCsgSizeDescription(lastCsgSumCsgSize);	
								}															
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * lookup specific casing section's casing summary's concatenated casing sizes which available in the run.
	 * @param casingSectionUid
	 * @param session
	 * @param thisConverter
	 * @return String
	 * @throws Exception
	 */
	public static String CasingSummaryCasingSize(String casingSectionUid, UserSession session, CustomFieldUom thisConverter) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double casingSize = 0.00;
		String casingSizeLabel = "";
		int count = 0;
		String strSql = "SELECT casingOd FROM CasingSummary WHERE (isDeleted = false or isDeleted is null) and (casingOd is not null and casingOd != '') and casingSectionUid = :casingSectionUid ORDER BY casingOd DESC";
		String[] paramsFields2 = {"casingSectionUid"};
		Object[] paramsValues2 = {casingSectionUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
		
		if (lstResult.size() > 0){
			for(Object objResult: lstResult){
				if (objResult != null && StringUtils.isNotBlank(objResult.toString())) casingSize = Double.parseDouble(objResult.toString());
				thisConverter.setReferenceMappingField(CasingSummary.class, "casingOd");
				thisConverter.setBaseValue(casingSize);
				casingSize = thisConverter.getConvertedValue();
				if (casingSize > 0){
					if (count == 0){
						casingSizeLabel = getCasingSizeLookupLabel(casingSize, session);					
					}else{
						casingSizeLabel = casingSizeLabel + " x " + getCasingSizeLookupLabel(casingSize, session);
					}
					count ++;
				}												
			}
		}		
		return casingSizeLabel;
	}
	
	/**
	 * populate the lookup label for necessary casing size's code based on lookup xml 
	 * @param casingOd
	 * @param session
	 * @return String
	 * @throws Exception
	 */
	private static String getCasingSizeLookupLabel(Double casingOd, UserSession session) throws Exception {		
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://casingsection.casing_size?key=code&value=label", new UserSelectionSnapshot(session), null);
		if (lookupMap != null) {
			DecimalFormat formatter = new DecimalFormat("#0.000000");			
			LookupItem lookupItem = lookupMap.get(formatter.format(casingOd));
			if (lookupItem != null) {
				return (String) lookupItem.getValue();
			}
		}		
		return casingOd.toString();
	}
	
	/**
	 * Auto populate next casing record's size, grade & weight in report daily based on GWP setting 
	 * @param commandBean
	 * @param node
	 * @param session 
	 * @throws Exception
	 */
	public static void autoLookupNextCasingSectionData(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception {		
		Object object = node.getData();		
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);

		if (object instanceof ReportDaily) {
			ReportDaily reportDaily = (ReportDaily) object;
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "lastCsgsize");			

			String strExcludeLinerConds = "";
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_LAST_CASING_SECION_EXCLUDING_LINER_TYPE))) strExcludeLinerConds += " and sectionName not in ('liner') ";
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_NEXT_CASING_SECTON_DATA))) {
				Double currentCasingSize = null;
				if (reportDaily.getLastCsgsize()!=null) {
					thisConverter.setReferenceMappingField(ReportDaily.class, "lastCsgsize");
					thisConverter.setBaseValueFromUserValue(reportDaily.getLastCsgsize());
					currentCasingSize = thisConverter.getBasevalue();
				}				
				reportDaily.setNextCsgsize(null);
				List<CasingSection> casingList = null;
				if (currentCasingSize!=null) {
					String strSql = "FROM CasingSection WHERE (isDeleted=false or isDeleted is null) " + strExcludeLinerConds + " and casingOd<:currentCasingSize and operationUid=:operationUid ORDER BY casingOd DESC";
					casingList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"currentCasingSize","operationUid"}, new Object[]{currentCasingSize, reportDaily.getOperationUid()}, qp);
				} else {
					String strSql = "FROM CasingSection WHERE (isDeleted=false or isDeleted is null) " + strExcludeLinerConds + " and operationUid=:operationUid ORDER BY casingOd DESC";
					casingList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"operationUid"}, new Object[]{reportDaily.getOperationUid()}, qp);
				}
				if (casingList.size()>0) {
					CasingSection thisCasingSection = casingList.get(0);
					if (thisCasingSection.getCasingOd() != null) {
						thisConverter.setReferenceMappingField(ReportDaily.class, "nextCsgsize");
						thisConverter.setBaseValue(thisCasingSection.getCasingOd());
						reportDaily.setNextCsgsize(thisConverter.getConvertedValue());
					}
					reportDaily.setNextCsggrade(thisCasingSection.getGrade());
					
					if (thisCasingSection.getCasingWeight()!=null) {
						thisConverter.setReferenceMappingField(ReportDaily.class, "nextCsgWeight");
						thisConverter.setBaseValue(thisCasingSection.getCasingWeight());
						reportDaily.setNextCsgWeight(thisConverter.getConvertedValue());
					}
				}
			} else if ("2".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_NEXT_CASING_SECTON_DATA))) {
				Object objResult = CommonUtil.getConfiguredInstance().lastCasingSection(session.getCurrentGroupUid(), session.getCurrentWellboreUid(), session.getCurrentDailyUid());
				if (objResult != null) {
					if (objResult instanceof CasingSection) {
						CasingSection lastCasingSection = (CasingSection) objResult;
						Date lastInstallStartDate = lastCasingSection.getInstallStartDate();
						List<CasingSection> casingList = null;
						if (lastInstallStartDate!=null) {
							String strSql = "FROM CasingSection WHERE (isDeleted=false or isDeleted is null) " + strExcludeLinerConds + " and (installStartDate != '' and installStartDate is not null) and installStartDate>:lastInstallStartDate and operationUid=:operationUid ORDER BY installStartDate ASC";
							casingList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"lastInstallStartDate","operationUid"}, new Object[]{lastInstallStartDate, reportDaily.getOperationUid()}, qp);
						}		
						if (casingList.size()>0) {
							CasingSection thisCasingSection = casingList.get(0);
							if (thisCasingSection.getCasingSectionUid() != null) {
								// based on queried casing section record's UID, filter casing summary list to get those latest casing size, grade, weight
								String nextCsgSumCsgSize = CasingSummaryCasingSize(thisCasingSection.getCasingSectionUid(),session, thisConverter);
								String nextCsgSumCsgWeight = CasingSummaryCasingWeight(thisCasingSection.getCasingSectionUid(),session, thisConverter);
								String nextCsgSumCsgGrade = CasingSummaryCasingGrade(thisCasingSection.getCasingSectionUid(),session);

								if (nextCsgSumCsgSize != null && StringUtils.isNotBlank(nextCsgSumCsgSize)) {
									reportDaily.setNextCsgSizeDescription(nextCsgSumCsgSize);
								}
								if (nextCsgSumCsgWeight != null && StringUtils.isNotBlank(nextCsgSumCsgWeight)) {
									reportDaily.setNextCsgWeightDescription(nextCsgSumCsgWeight);
								}
								if (nextCsgSumCsgGrade != null && StringUtils.isNotBlank(nextCsgSumCsgGrade)) {
									reportDaily.setNextCsggrade(nextCsgSumCsgGrade);
								}
							}
						}
					}
				} else {
					//Handle case where there is no last casing section info retrieved, will get the first record from the list sorted by installStartDate ASC
					List<CasingSection> casingList = null;
					String strSql = "FROM CasingSection WHERE (isDeleted=false or isDeleted is null) " + strExcludeLinerConds + " and (installStartDate != '' and installStartDate is not null) and operationUid=:operationUid ORDER BY installStartDate ASC";
					casingList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"operationUid"}, new Object[]{reportDaily.getOperationUid()}, qp);
					if (casingList.size()>0) {
						CasingSection thisCasingSection = casingList.get(0);
						if (thisCasingSection.getCasingSectionUid() != null) {
							// based on queried casing section record's UID, filter casing summary list to get those latest casing size, grade, weight
							String nextCsgSumCsgSize = CasingSummaryCasingSize(thisCasingSection.getCasingSectionUid(),session, thisConverter);
							String nextCsgSumCsgWeight = CasingSummaryCasingWeight(thisCasingSection.getCasingSectionUid(),session, thisConverter);
							String nextCsgSumCsgGrade = CasingSummaryCasingGrade(thisCasingSection.getCasingSectionUid(),session);

							if (nextCsgSumCsgSize != null && StringUtils.isNotBlank(nextCsgSumCsgSize)) {
								reportDaily.setNextCsgSizeDescription(nextCsgSumCsgSize);
							}
							if (nextCsgSumCsgWeight != null && StringUtils.isNotBlank(nextCsgSumCsgWeight)) {
								reportDaily.setNextCsgWeightDescription(nextCsgSumCsgWeight);
							}
							if (nextCsgSumCsgGrade != null && StringUtils.isNotBlank(nextCsgSumCsgGrade)) {
								reportDaily.setNextCsggrade(nextCsgSumCsgGrade);
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * lookup specific casing section's casing summary's concatenated casing weights which available in the run.
	 * @param casingSectionUid
	 * @param session
	 * @param thisConverter
	 * @return String
	 * @throws Exception
	 */
	public static String CasingSummaryCasingWeight(String casingSectionUid, UserSession session, CustomFieldUom thisConverter) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double casingWeight = 0.00;
		String casingWeightDesc = "";
		int count = 0;
		String strSql = "SELECT casingWeight FROM CasingSummary WHERE (isDeleted = false or isDeleted is null) and (casingWeight is not null and casingWeight >=0 ) and casingSectionUid = :casingSectionUid ORDER BY casingOd DESC";
		String[] paramsFields2 = {"casingSectionUid"};
		Object[] paramsValues2 = {casingSectionUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
		
		if (lstResult.size() > 0){
			for(Object objResult: lstResult){
				if (objResult != null && StringUtils.isNotBlank(objResult.toString())) casingWeight = Double.parseDouble(objResult.toString());
				if (casingWeight > 0){
					thisConverter.setReferenceMappingField(CasingSummary.class, "casingWeight");
					thisConverter.setBaseValue(casingWeight);
					String strCasingWeight = Double.toString(thisConverter.getConvertedValue());
					
					if (count == 0){						
						casingWeightDesc = strCasingWeight + thisConverter.getUomSymbol();
					}else{
						casingWeightDesc = casingWeightDesc + " x " + strCasingWeight + thisConverter.getUomSymbol();
					}
					count ++;
				}												
			}
		}		
		return casingWeightDesc;
	}
	
	/**
	 * lookup specific casing section's casing summary's concatenated casing grade which available in the run.
	 * @param casingSectionUid
	 * @param session 
	 * @return String
	 * @throws Exception
	 */
	public static String CasingSummaryCasingGrade(String casingSectionUid, UserSession session) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String casingGrade = "";
		String casingGradeDesc = "";
		int count = 0;
		String strSql = "SELECT grade FROM CasingSummary WHERE (isDeleted = false or isDeleted is null) and (grade is not null and grade !='' ) and casingSectionUid = :casingSectionUid ORDER BY casingOd DESC";
		String[] paramsFields2 = {"casingSectionUid"};
		Object[] paramsValues2 = {casingSectionUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
		
		if (lstResult.size() > 0){
			for(Object objResult: lstResult){
				if (objResult != null && StringUtils.isNotBlank(objResult.toString())) casingGrade = objResult.toString();
				if (StringUtils.isNotBlank(casingGrade)){					
					if (count == 0){						
						casingGradeDesc = casingGrade;
					}else{
						casingGradeDesc = casingGradeDesc + " x " + casingGrade;
					}
					count ++;
				}												
			}
		}		
		return casingGradeDesc;
	}
	
	/**
	 * get cumulative unplanned future cost to date
	 * @param todayDate
	 * @param operationUid
	 * @param reportType
	 * @return Double
	 * @throws Exception
	 */
	public static Double getCumulativeUnplannedFutureCost(Date todayDate, String operationUid, String reportType) throws Exception {
		Double cumufc = null;
		
		String strSql = "select sum(unplannedFutureCost) as cumufc from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,  new String[]{"todayDate", "operationUid", "reportType"}, new Object[]{todayDate, operationUid, reportType});
		Object result = (Object) lstResult.get(0);
		
		if (result != null) cumufc = Double.parseDouble(result.toString());
	
		return cumufc;
	}
	
	/**
	 * check if operation type contain in the operation list 
	 * @param excludeOperationType
	 * @param operationCode
	 * @return
	 * @throws Exception
	 */
	public static boolean showCopyDataFilters(String groupUid, String operationCode) throws Exception {	
		String operationList = GroupWidePreference.getValue(groupUid, "excludeCopyDataFiltersByOperationTypes") ;
		if (operationList != null && !"".equals(operationList)){			
			for (String ops : operationList.split(";"))
			{
				if (ops.equalsIgnoreCase(operationCode)){
					return true;				
				}
			}
		}
		return false;
	}

	public static void autoPopulateDaySinceSpud(String currentDailyUid, CommandBean commandbean) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql3 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND dailyUid = :thisDailyUid";
		List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, "thisDailyUid", currentDailyUid);
		
		if(!reportDailyList.isEmpty())
		{
			Date currentDayDate =  reportDailyList.get(0).getReportDatetime();
			Calendar cal = Calendar.getInstance();
			cal.setTime(currentDayDate); 
			cal.add(Calendar.DATE, 1);
			currentDayDate = cal.getTime();
			
			String currentReportDailyUid = reportDailyList.get(0).getReportDailyUid();
			String wellUid = reportDailyList.get(0).getWellUid();
			Double totalDuration = 0.0;
			
			String[] paramFields1 = {"thisCurrentDate","thisWellUid"};
			Object[] paramValue1 = {currentDayDate, wellUid};
			String strSqlTest1 = "FROM Operation WHERE startDate < :thisCurrentDate AND (isDeleted = false or isDeleted is null) AND wellUid = :thisWellUid";
			List<Operation> getOpsList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlTest1, paramFields1, paramValue1);
			
			if(!getOpsList.isEmpty())
			{
				for(Operation ops : getOpsList)
				{
					Date spudDate = ops.getSpudDate();
					String opsUid = ops.getOperationUid();
					
					String[] paramFields2 = {"thisOpsUid","thisCurrentDate"};
					Object[] paramValues2 = {opsUid,currentDayDate};
					String strSql4 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOpsUid AND reportDatetime < :thisCurrentDate AND reportType <> 'DGR'";
					List<ReportDaily> rDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramFields2, paramValues2);
					
					if(!rDailyList.isEmpty())
					{
						for(ReportDaily rptDaily : rDailyList)
						{
							String dailyUid = rptDaily.getDailyUid();
							
							if(spudDate == null)
							{
								String strSql5 = "SELECT SUM(activityDuration) FROM Activity " +
								"WHERE (isDeleted = false or isDeleted is null) " + 
								"AND (isSimop=false or isSimop is null) " +
								"AND (isOffline=false or isOffline is null) " +
								"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
								"AND dailyUid = :thisDailyUid";
								
								List sumOfDuration = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, "thisDailyUid", dailyUid);
								
								if (sumOfDuration.get(0) != null && !StringUtils.isBlank(sumOfDuration.get(0).toString()))
									totalDuration = totalDuration + Double.parseDouble(sumOfDuration.get(0).toString());
								
							}else
							{
								String strSql5 = "FROM Activity " +
								"WHERE (isDeleted = false or isDeleted is null) " + 
								"AND (isSimop=false or isSimop is null) " +
								"AND (isOffline=false or isOffline is null) " +
								"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
								"AND dailyUid = :thisDailyUid";
								
								List<Activity> activityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, "thisDailyUid", dailyUid, qp);
								
								if(!activityList.isEmpty())
								{
									for(Activity act : activityList)
									{
										Double dura = 0.0;
										
										if(act.getStartDatetime().before(spudDate) && act.getEndDatetime().after(spudDate))
										{
											Long duration = act.getEndDatetime().getTime() - spudDate.getTime();
											duration = TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS);
											dura = Double.parseDouble(duration.toString());
										}
										else if (act.getStartDatetime().after(spudDate) || act.getStartDatetime().equals(spudDate))
											dura = Double.parseDouble(act.getActivityDuration().toString());
										
										totalDuration += dura;
									}	
								}
							}
						}
					}
				}
			}
			
			ReportDaily rptDaily = (ReportDaily)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ReportDaily.class, currentReportDailyUid);
			CustomFieldUom converter = new CustomFieldUom(commandbean, ReportDaily.class, "daysFromSpudWell");
			if (converter.isUOMMappingAvailable()){
				converter.setBaseValue(totalDuration);
	 		}
			rptDaily.setDaysFromSpudWell(converter.getConvertedValue());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rptDaily);
		}
	}
	public static void updateCumCostFromPrevOperation(String currentReportDailyUid, CommandBean commandbean,  UserSession session) throws Exception{
		
		Operation thisOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(session.getCurrentOperationUid());
		if(thisOperation == null) return;
		
		ReportDaily thisRptDaily = (ReportDaily)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ReportDaily.class, currentReportDailyUid);
		if(thisRptDaily == null) return;
		
		Date todayDate = thisRptDaily.getReportDatetime();
		if(todayDate == null) return;
		
		String strSqlOps = "SELECT operationUid from Operation WHERE (isDeleted = false or isDeleted is null) AND wellUid = :thisWellUid";
		List<String> opsUid = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlOps, "thisWellUid", thisOperation.getWellUid());
		String[] paramsFieldsOps2 = {"operationUid", "thisDate"};
		Object[] paramsValuesOps2 = {opsUid, todayDate};
		
		String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND reportDatetime >= :thisDate and (operationUid in (:operationUid))";
		List<ReportDaily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldsOps2, paramsValuesOps2);
		
		if(!lstResult.isEmpty())
		{
			for(ReportDaily rpt : lstResult)
			{
				Date rptDate = rpt.getReportDatetime();
				Object[] paramsValuesOps3 = {opsUid, rptDate};
				
				//calculate cumcost
				strSql = "select sum(daycost) as cumcost from ReportDaily where (isDeleted = false or isDeleted is null) and reportDatetime <= :thisDate and (operationUid in (:operationUid))";
				Double cumCostFromPrevOps = null;
				List sumResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldsOps2, paramsValuesOps3);
				Object a = (Object) sumResult.get(0);
				
				if (a != null) cumCostFromPrevOps = Double.parseDouble(a.toString());
				
				strSql = "select sum(costadjust) as cumcostadjust from ReportDaily where (isDeleted = false or isDeleted is null) and reportDatetime <= :thisDate and (operationUid in (:operationUid))";
				List lstResultAdjust = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldsOps2, paramsValuesOps3);
				Object adjust = (Object) lstResultAdjust.get(0);
				Double cumcostadjust = null;
				if (adjust != null) cumcostadjust = Double.parseDouble(adjust.toString());
				
				if(cumcostadjust != null){
					if(cumCostFromPrevOps != null) cumCostFromPrevOps = cumCostFromPrevOps + cumcostadjust;
					else cumCostFromPrevOps = 0.0 + cumcostadjust;
				}
				
				//calculate cumtangiblecost
				strSql = "select sum(daytangiblecost) as cumtangiblecost from ReportDaily where (isDeleted = false or isDeleted is null) and reportDatetime <= :thisDate and (operationUid in (:operationUid))";
				List cumTangiCost = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldsOps2, paramsValuesOps3);
				a = (Object) cumTangiCost.get(0);
				Double cumTangibleFromPrevOps = null;
				if (a != null) cumTangibleFromPrevOps = Double.parseDouble(a.toString());
				
				ReportDaily rptDaily = (ReportDaily)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ReportDaily.class, rpt.getReportDailyUid());
				
				CustomFieldUom converter = new CustomFieldUom(commandbean, ReportDaily.class, "cumCostFromPrevOperation");
				if (converter.isUOMMappingAvailable() && cumCostFromPrevOps != null){
					converter.setBaseValue(cumCostFromPrevOps);
					rptDaily.setCumCostFromPrevOperation(converter.getConvertedValue());
		 		}
				
				
				converter.setReferenceMappingField(ReportDaily.class, "cumTangibleCostFromPrevOperation");
				if (converter.isUOMMappingAvailable() && cumTangibleFromPrevOps != null){
					converter.setBaseValue(cumTangibleFromPrevOps);
					rptDaily.setCumTangibleCostFromPrevOperation(converter.getConvertedValue());
		 		}
						
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rptDaily);
			}
		}
	}
	
	public static void updateRigInformationUid(ReportDaily rdparam, String reportType) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		if (rdparam != null) {
			String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND dailyUid = :thisDailyUid AND report_type = :thisReportType";
			String[] paramsFields = {"thisDailyUid", "thisReportType"};
			Object[] paramsValues = {rdparam.getDailyUid(), reportType};
			List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			
			if (reportDailyList != null && reportDailyList.size() > 0) {
				for (int i = 0; i < reportDailyList.size(); i++) {
					ReportDaily rd = reportDailyList.get(i);
					if ("DGR".equals(rdparam.getReportType()) && "DDR".equals(reportType)) {
						//check both report daily and update only if the rig information uid is different 
						if ((rdparam.getRigInformationUid() != null && !rdparam.getRigInformationUid().equals(rd.getRigInformationUid()))
						   || (rd.getRigInformationUid() == null && rdparam.getRigInformationUid() != null)
						   || (rd.getRigInformationUid() != null && rdparam.getRigInformationUid() == null)) {
							rdparam.setRigInformationUid(rd.getRigInformationUid());
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rdparam, qp);
						}
					}
					else {
						//check both report daily and update only if the rig information uid is different 
						if ((rd.getRigInformationUid() != null && !rd.getRigInformationUid().equals(rdparam.getRigInformationUid()))
						   || (rd.getRigInformationUid() == null && rdparam.getRigInformationUid() != null)
						   || (rd.getRigInformationUid() != null && rdparam.getRigInformationUid() == null)) {
							rd.setRigInformationUid(rdparam.getRigInformationUid());
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rd, qp);
						}
					}
				}
			}
		}

	}
	
	/**
	 * Calculate dryhole duration/cost
	 * @param day
	 * @param spudDate
	 * @param dryHoleEndDate
	 * @throws Exception
	 */
	public static void setReportDailyDryHole(ReportDaily day, Date spudDate, Date dryHoleEndDate) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Calendar spudDateCal = Calendar.getInstance();
		spudDateCal.setTime(spudDate);
		spudDateCal.set(Calendar.HOUR_OF_DAY, 0);
		spudDateCal.set(Calendar.MINUTE, 0);
		spudDateCal.set(Calendar.SECOND, 0);
		spudDateCal.set(Calendar.MILLISECOND, 0);
		
		Calendar dryHoleEndCal = Calendar.getInstance();
		dryHoleEndCal.setTime(dryHoleEndDate);
		dryHoleEndCal.set(Calendar.HOUR_OF_DAY, 0);
		dryHoleEndCal.set(Calendar.MINUTE, 0);
		dryHoleEndCal.set(Calendar.SECOND, 0);
		dryHoleEndCal.set(Calendar.MILLISECOND, 0);
		
		if (day.getReportDatetime().getTime() >= spudDateCal.getTime().getTime() && day.getReportDatetime().getTime() <= dryHoleEndCal.getTime().getTime()) {
			Double dryHoleDuration = null;
			Double dryHoleNptDuration = null;
			Double totalActivityDuration = CommonUtil.getConfiguredInstance().calculateActivityDurationByDaily(day.getDailyUid());
			Double costPerSecond = null;
			Double dryHoleCost = null;
			dryHoleDuration = CommonUtil.getConfiguredInstance().durationFromDryHolePerDay(day.getOperationUid(), day.getDailyUid(), day.getGroupUid(), spudDate, dryHoleEndDate, false);
			dryHoleNptDuration = CommonUtil.getConfiguredInstance().durationFromDryHolePerDay(day.getOperationUid(), day.getDailyUid(), day.getGroupUid(), spudDate, dryHoleEndDate, true);
			
			Double totalcost = null;
			if (day.getDaycost() != null || day.getDaycompletioncost() != null || day.getDaytangiblecost() != null || day.getOtherCost()!= null || day.getDaytestcost()!=null)  totalcost = 0.0;
			if (day.getDaycost() != null) totalcost = totalcost + day.getDaycost();
			if (day.getDaycompletioncost() != null) totalcost =  totalcost + day.getDaycompletioncost();
			if (day.getDaytangiblecost() != null) totalcost =  totalcost + day.getDaytangiblecost();
			if (day.getOtherCost() != null) totalcost =  totalcost + day.getOtherCost();
			if (day.getDaytestcost()!=null) totalcost = totalcost + day.getDaytestcost();
			
			if (totalcost != null && totalActivityDuration > 0.00)
				costPerSecond = totalcost / totalActivityDuration;
			
			if (costPerSecond != null && dryHoleDuration != null)
				dryHoleCost = dryHoleDuration * costPerSecond;
			
			String strSql = null;
			strSql = "UPDATE ReportDaily SET dryHoleDuration=:dryHoleDuration, dryHoleNptDuration=:dryHoleNptDuration, costPerSecond=:costPerSecond, dryHoleCost=:dryHoleCost WHERE reportType <> 'DGR' and operationUid =:operationUid and dailyUid =:dailyUid";
			String[] paramsFields = {"dryHoleDuration", "dryHoleNptDuration", "costPerSecond", "dryHoleCost", "operationUid", "dailyUid"};
			Object[] paramsValues = {dryHoleDuration, dryHoleNptDuration, costPerSecond, dryHoleCost, day.getOperationUid(), day.getDailyUid()};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
	}
	
	/**
	 * Calculate spud to td duration
	 * @param day
	 * @param spudDate
	 * @param dryHoleEndDate
	 * @throws Exception
	 */
	public static void setReportDailySpudToTdDuration(ReportDaily day, Date spudDate, Date tdDateTdTime) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Calendar spudDateCal = Calendar.getInstance();
		spudDateCal.setTime(spudDate);
		spudDateCal.set(Calendar.HOUR_OF_DAY, 0);
		spudDateCal.set(Calendar.MINUTE, 0);
		spudDateCal.set(Calendar.SECOND, 0);
		spudDateCal.set(Calendar.MILLISECOND, 0);
		
		Calendar tdDateTdTimeCal = Calendar.getInstance();
		tdDateTdTimeCal.setTime(tdDateTdTime);
		tdDateTdTimeCal.set(Calendar.HOUR_OF_DAY, 0);
		tdDateTdTimeCal.set(Calendar.MINUTE, 0);
		tdDateTdTimeCal.set(Calendar.SECOND, 0);
		tdDateTdTimeCal.set(Calendar.MILLISECOND, 0);
		
		if (day.getReportDatetime().getTime() >= spudDateCal.getTime().getTime() && day.getReportDatetime().getTime() <= tdDateTdTimeCal.getTime().getTime()) {
			Double spudToTdDuration = null;
			spudToTdDuration = CommonUtil.getConfiguredInstance().spudToTdDurationDaily(day.getOperationUid(), day.getDailyUid(), day.getGroupUid(), spudDate, tdDateTdTime);
			
			String strSql = null;
			strSql = "UPDATE ReportDaily SET spudToTdDuration=:spudToTdDuration WHERE reportType <> 'DGR' and operationUid =:operationUid and dailyUid =:dailyUid";
			String[] paramsFields = {"spudToTdDuration", "operationUid", "dailyUid"};
			Object[] paramsValues = {spudToTdDuration, day.getOperationUid(), day.getDailyUid()};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
	}
	
	/**
	 * If GWP `autoCalcDaily24hrProgress` is 1 (enabled), dynamic attribute `geology24hrProgress` is taken 
	 * from DDR's report_daily.progress field, else, it takes from report_daily.geology_24hr_progress field
	 * @param commandBean
	 * @param node
	 * @param userSelection
	 * @param session
	 * @throws Exception
	 */
	public static void getProgressFromDdrDaily(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception {
		Object object = node.getData();
		ReportDaily thisReportDaily = null;
		if (object instanceof ReportDaily) {
			thisReportDaily = (ReportDaily) object;
			if (StringUtils.equals(thisReportDaily.getReportType(),"DGR")) {
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "geology_24hr_progress");
				Double progress = null;
				if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DAILY_24HR_PROGRESS))) {
					if (StringUtils.equals("DDR",thisReportDaily.getReportType())) { return; }
					String dailyUid = thisReportDaily.getDailyUid();
					String strSql = "FROM ReportDaily WHERE reportType='DDR' AND dailyUid=:dailyUid AND isDeleted IS NULL";
					String[] paramsFields = {"dailyUid"};
					Object[] paramsValues = {dailyUid};
					QueryProperties qp = new QueryProperties();
					qp.setUomConversionEnabled(false);
					List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
					if (!reportDailyList.isEmpty()) {
						ReportDaily ddrReportDaily = reportDailyList.get(0);
						progress = ddrReportDaily.getProgress(); 
					}
				} else {
					progress = thisReportDaily.getGeology24hrProgress();
				}
				if (progress != null) {
					thisConverter.setBaseValue(progress);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@geology24hrProgress", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("geology24hrProgress", thisConverter.getConvertedValue());
				}
			}
		}
	}
	
	public static double recalculateTotalShiftDuration(CommandBean commandBean, String dailyUid,String operationUid) throws Exception {
		double totalShiftDuration = 0;
		List<DailyShiftInfo> list = null;
		
		if(StringUtils.isNotBlank(dailyUid) && StringUtils.isNotBlank(operationUid)){
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM DailyShiftInfo WHERE (isDeleted='' OR isDeleted is null) AND dailyUid = :dailyUid AND operationUid = :operationUid", new String[] {"dailyUid","operationUid"}, new Object[] {dailyUid,operationUid} );
			
			if(list.size() > 0 && list != null){
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
				
				for(DailyShiftInfo shift : list){
	
					if (shift != null && shift.getStartDatetime() != null && shift.getEndDatetime() != null) {
						Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(shift.getStartDatetime(), shift.getEndDatetime());				
						thisConverter.setBaseValue(thisDuration);				
						
						totalShiftDuration += thisConverter.getConvertedValue();
					}
				}
			}
		}
		
		return totalShiftDuration;
	}

	public static Boolean isShiftTimeOverlapped(CommandBean commandBean,String dailyUid,String operationUid) throws Exception {
		Date activityFirstStartDateTime = null;
		Date activityLastStartDateTime = null;
		Double activityActualDuration = 0.0;
		Double shiftDuration = 0.0;
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
		List<DailyShiftInfo> list = null;
		
		if(StringUtils.isNotBlank(dailyUid) && StringUtils.isNotBlank(operationUid)){
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM DailyShiftInfo WHERE (isDeleted='' OR isDeleted is null) AND dailyUid = :dailyUid AND operationUid = :operationUid order by startDatetime", new String[] {"dailyUid","operationUid"}, new Object[] {dailyUid,operationUid} );
			
			if(list.size() > 0 && list != null){
				for(DailyShiftInfo shift : list) {
					
					if (shift.getStartDatetime() != null && shift.getEndDatetime() != null) {						
						if (activityFirstStartDateTime == null) activityFirstStartDateTime = shift.getStartDatetime();					
							
						activityLastStartDateTime = shift.getEndDatetime();
						if (shift != null) {
							Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(shift.getStartDatetime(), shift.getEndDatetime());				
							thisConverter.setBaseValue(thisDuration);				
							
							shiftDuration += thisConverter.getConvertedValue();
						}
					}
				}
			}
		}
		thisConverter.setBaseValueFromUserValue(shiftDuration);
		shiftDuration=thisConverter.getBasevalue();
		int decimalPlaces = 1;
		BigDecimal bd = new BigDecimal(shiftDuration);
		bd = bd.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
		shiftDuration = bd.doubleValue();
	
		if (activityFirstStartDateTime != null && activityLastStartDateTime !=null) {	
			activityActualDuration = ActivityUtils.getActivityDurationInSecond(activityFirstStartDateTime, activityLastStartDateTime, thisConverter);
			
			if (activityActualDuration.compareTo(shiftDuration) != 0) {
				return true;
			}
		}
	
	
		return false;
	}
	
	public static boolean isOmvCountrySpecific(String wellUid, String operationUid, String forwardShiftReportType, String country) throws Exception{
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
		
		if (operation!=null && forwardShiftReportType.equals(operation.getOperationCode())){
			Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellUid);
			
			if (well!=null && StringUtils.isNotBlank(well.getCountry()) && country.equals(well.getCountry())){
				return true;
			}
		}
		return false;
	}
}
