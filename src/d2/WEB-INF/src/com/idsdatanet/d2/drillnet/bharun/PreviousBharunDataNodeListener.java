package com.idsdatanet.d2.drillnet.bharun;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class PreviousBharunDataNodeListener implements DataLoaderInterceptor{

	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String currentClass		= meta.getTableClass().getSimpleName();
		String customCondition 	= "";
		
		if("Bharun".equals(currentClass) || "BharunDailySummary".equals(currentClass)) {
			String bharunUid = "";
			String bharunDailySummaryUid = "";
			String dailyUid = userSelection.getDailyUid();
			if (dailyUid!=null) {
				Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid); 
				if (daily!=null) {
					String queryString = "FROM Bharun b, BharunDailySummary bd, Daily d " +
							"WHERE (b.isDeleted=false or b.isDeleted is null) " +
							"AND (bd.isDeleted=false or bd.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND b.bharunUid=bd.bharunUid " +
							"AND bd.dailyUid=d.dailyUid " +
							"AND d.dayDate<:dayDate " +
							"AND b.operationUid=:operationUid " +
							"ORDER BY d.dayDate DESC, b.depthInMdMsl DESC";
					List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"dayDate", "operationUid"}, new Object[] {daily.getDayDate(), userSelection.getOperationUid()});
					for (Object[] rec : list) {
						Bharun bharun = (Bharun) rec[0];
						BharunDailySummary bharunDailySummary = (BharunDailySummary) rec[1];
						
						queryString = "FROM BharunDailySummary WHERE (isDeleted=false or isDeleted is null) AND bharunUid=:bharunUid and dailyUid=:dailyUid";
						List<BharunDailySummary> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"bharunUid", "dailyUid"}, new Object[] {bharun.getBharunUid(), dailyUid});
						if (list2.size()>0) continue;
						bharunUid = bharun.getBharunUid();
						bharunDailySummaryUid = bharunDailySummary.getBharunDailySummaryUid();
						break;
					}
				}
			}
			
			if ("Bharun".equals(currentClass)) {
				customCondition = "(isDeleted = false or isDeleted is null) and bharunUid = '" + bharunUid + "'";
			} else if ("BharunDailySummary".equals(currentClass)) {
				customCondition = "(isDeleted = false or isDeleted is null) and bharunDailySummaryUid = '" + bharunDailySummaryUid + "'";
			}
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		return null;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
