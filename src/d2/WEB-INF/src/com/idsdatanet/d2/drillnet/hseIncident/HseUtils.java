package com.idsdatanet.d2.drillnet.hseIncident;

import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class HseUtils {


	public static Integer getNumberOfIncidentsWellToDate(String operationUid, String dailyUid, String incidentCategory) throws Exception{
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		Date todayDate = daily.getDayDate();
		
		String strSql = "select sum(h.numberOfIncidents) as sumNumber from HseIncident h, Daily d WHERE " +
				"(h.isDeleted = false or h.isDeleted is null) AND " +
				"(d.isDeleted = false or d.isDeleted is null) AND " +
				"h.dailyUid = d.dailyUid and " +
				"d.dayDate <= :todayDate and " +
				"h.incidentCategory = :incidentCategory and " +
				"d.operationUid = :operationUid ";
		String[] paramsFields = {"todayDate", "incidentCategory", "operationUid"};
		Object[] paramsValues = new Object[3]; 
		paramsValues[0] = todayDate; 
		paramsValues[1] = incidentCategory; 
		paramsValues[2] = operationUid;
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		Integer SumTotal = 0;
		if (lstResult.size()>0){
			Object a = (Object) lstResult.get(0);
			if (a != null) SumTotal = Integer.parseInt(a.toString());
			
		}
		return SumTotal;
	}

	//function to get total num of incidents based on rig (OP-4861 d2_choc_my)
	public static Integer getNumberOfIncidentsRigToDate(String rigInformationUid, String dailyUid, String incidentCategory) throws Exception{
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		Date todayDate = daily.getDayDate();

		String strSql = "select sum(h.numberOfIncidents) as sumNumber from HseIncident h, Daily d WHERE " +
				"(h.isDeleted = false or h.isDeleted is null) AND " +
				"(d.isDeleted = false or d.isDeleted is null) AND " +
				"h.dailyUid = d.dailyUid and " +
				"d.dayDate <= :todayDate and " +
				"h.incidentCategory = :incidentCategory and " +
				"h.rigInformationUid = :rigInformationUid ";
		String[] paramsFields = {"todayDate", "incidentCategory", "rigInformationUid"};
		Object[] paramsValues = new Object[3]; 
		paramsValues[0] = todayDate; 
		paramsValues[1] = incidentCategory; 
		paramsValues[2] = rigInformationUid;
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		Integer SumTotal = 0;
		if (lstResult.size()>0){
			Object a = (Object) lstResult.get(0);
			if (a != null) SumTotal = Integer.parseInt(a.toString());
			
		}
		return SumTotal;
	}
	
	public static void populateAndUpdateLastLTI(String dailyUid, String rigInformationUid, String groupUid, boolean save, ReportDaily currentReportDaily) throws Exception{
		//auto lookup last LTI for current rig
		if ("1".equals(GroupWidePreference.getValue(groupUid, GroupWidePreference.GWP_AUTO_LOOKUP_LAST_LTI))) {
			HseIncident lastLTI = CommonUtil.getConfiguredInstance().lastLTI(rigInformationUid, dailyUid);
			
			if(currentReportDaily != null) {
				if (lastLTI != null) {
					currentReportDaily.setLastLtiEventname(lastLTI.getIncidentCategory());
					currentReportDaily.setLastLtiDate(lastLTI.getHseEventdatetime());
				}else {
					currentReportDaily.setLastLtiEventname(null);
					currentReportDaily.setLastLtiDate(null);
				}
				if(save) {
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(currentReportDaily);
				}
				
			}else if(dailyUid != null) {
				String hql = "From ReportDaily where (isDeleted is null or isDeleted = 0) and dailyUid = :dailyUid";
				List<?> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "dailyUid", dailyUid);
				if(result != null && result.size() > 0) {
					for(Object obj : result) {
						ReportDaily reportDaily = (ReportDaily) obj;
						if (lastLTI != null) {
							reportDaily.setLastLtiEventname(lastLTI.getIncidentCategory());
							reportDaily.setLastLtiDate(lastLTI.getHseEventdatetime());
						}else {
							reportDaily.setLastLtiEventname(null);
							reportDaily.setLastLtiDate(null);
						}

						if(save) {
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportDaily);
						}
					}
				}
			}
		}
	}
	
}


