package com.idsdatanet.d2.drillnet.kickTolerance;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.model.KickTolerance;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class KickToleranceUtils {

	public static void getRelatedInfo(CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof KickTolerance) {
			//GET VALUES FROM REPORTDAILY TABLE
			String[] paramsFields1 = {"thisOperationUid", "thisDailyUid"};
	 		Object[] paramsValues1 = new Object[2];
			paramsValues1[0] = userSelection.getOperationUid();
			paramsValues1[1] = userSelection.getDailyUid();
			
			String strSql1 = "select reportDailyUid FROM ReportDaily where operationUid = :thisOperationUid and dailyUid = :thisDailyUid and reportType ='DDR' and (isDeleted = false or isDeleted is null)";
			List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1,paramsValues1);
			
			String ddrReportDailyUid = null;
			if (!lstResult1.isEmpty()) {
				Object b = (Object) lstResult1.get(0);
				if (b != null) ddrReportDailyUid = b.toString();
				
				ReportDaily ddrReportDaily = (ReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ReportDaily.class, ddrReportDailyUid);
				Map<String, Object> reportDailyFieldValues = PropertyUtils.describe(ddrReportDaily);
				for (Map.Entry entry : reportDailyFieldValues.entrySet()) {
					setDynaAttr(node, "ddr_" + entry.getKey(), entry.getValue());
				}
			}
		}
	}
	
	private static void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		}
	}
}
