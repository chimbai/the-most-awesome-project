package com.idsdatanet.d2.drillnet.report;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.web.mvc.AbstractDownloadHandler;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;

public class ReportTemplateDownloadHandler extends AbstractDownloadHandler {
	private Log log = LogFactory.getLog(getClass());
	
	public void process(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String reportTemplateName = request.getParameter("templateName");
		if(StringUtils.isBlank(reportTemplateName)){
			response.getWriter().print("Report template not specified");
			return;
		}
		
		File output_file = null;
		
		reportTemplateName = ApplicationConfig.getConfiguredInstance().getServerRootPath() + reportTemplateName;
		
		try{
			output_file = new File(reportTemplateName);
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		if(output_file == null){
			response.getWriter().print("Unable to create a reference to template file");
			return;
		}
		
		if(! output_file.exists()){
			response.getWriter().print("Report template file does not exist");
			return;
		}
		
		String download_filename = output_file.getName();
		if(StringUtils.isNotBlank(download_filename)){
			String file_ext = this.getFileExtension(output_file);
			if(StringUtils.isNotBlank(file_ext)){
				if(! download_filename.endsWith(file_ext)) download_filename += file_ext;
			}
		}
		
		this.outputAsDownloadAttachment(output_file, download_filename, request, response);
	}
	
	private String getFileExtension(File file){
		String name = file.getName();
		int i = name.lastIndexOf(".");
		if(i == -1){
			return null;
		}else{
			return name.substring(i);
		}
	}
}
