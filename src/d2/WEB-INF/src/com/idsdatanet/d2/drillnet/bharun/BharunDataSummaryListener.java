package com.idsdatanet.d2.drillnet.bharun;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.EmptyDataSummary;
import com.idsdatanet.d2.core.web.mvc.Summary;
import com.idsdatanet.d2.core.web.mvc.SummaryInfo;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class BharunDataSummaryListener extends EmptyDataSummary {
	private List<Summary> data = null;
	
	public void generate(HttpServletRequest request) throws Exception {
		this.data = new ArrayList<Summary>();
		UserSession session = UserSession.getInstance(request);
		String operation = session.getCurrentOperationUid();
		List<String> bharunNumberList = new ArrayList<String>();
		
		String strSql1 = "select bharun.bhaRunNumber, bharun.bharunUid FROM Bharun bharun, BharunDailySummary bharunDailySummary, Daily daily WHERE (bharun.isDeleted = false or bharun.isDeleted is null) and (bharunDailySummary.isDeleted = false or bharunDailySummary.isDeleted is null) and (daily.isDeleted = false or daily.isDeleted is null) and bharun.bharunUid = bharunDailySummary.bharunUid and daily.dailyUid = bharunDailySummary.dailyUid and bharun.operationUid= :thisOperation group by bharun.bhaRunNumber, bharun.bharunUid, daily.dayDate ORDER BY daily.dayDate, bharun.bhaRunNumber";
		String[] paramsFields1 = {"thisOperation"};
		String[] paramsValues1 = {operation};
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);

		if (lstResult.size() > 0){
			for (Object[] objBha : lstResult){
				String strBhanum = (String) objBha[0];
				String bharunUid = (String) objBha[1];
				
				// eliminate redundant bharun number
				if(bharunNumberList.contains(strBhanum)) continue;
				bharunNumberList.add(strBhanum);
				
				String strSql2 = "SELECT a.dailyUid FROM BharunDailySummary a, Daily b WHERE (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and a.dailyUid = b.dailyUid AND a.bharunUid= :thisBharunUid ORDER BY b.dayDate";
				String[] paramsFields2 = {"thisBharunUid"};
				String[] paramsValues2 = {bharunUid};
				List<String> lstResultDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);

				if (lstResult.size() > 0){
					Summary summary = new Summary("BHA #" + strBhanum);
					
					for (String dailyUid : lstResultDaily){
						Daily thisDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
						
						ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), thisDaily.getDailyUid());
						String reportNumber = "-";
						if (reportDaily!=null) {
							reportNumber = reportDaily.getReportNumber(); 
							if (!session.withinAccessScope(reportDaily)) {
								continue;
							}
						}

						summary.addInfo(new SummaryInfo("Day #" + reportNumber, thisDaily.getDailyUid()));
						
						/*List reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid", "dailyUid", thisDaily.getDailyUid());
						String reportNumber = "1";
						if(reportDailyList != null && reportDailyList.size() > 0) {
							ReportDaily reportDaily = (ReportDaily) reportDailyList.get(0);
							reportNumber = reportDaily.getReportNumber();
							if (!session.withinAccessScope(reportDaily)) {
								continue;
							}
						}
						summary.addInfo(new SummaryInfo("Day #" + reportNumber, thisDaily.getDailyUid()));*/
					}
					
					if (summary.getInfo().size() > 0) {
						this.data.add(summary);
					}
					//summary.dispose();
				}
			}
		}
		
	}
	
	public List<Summary> getData() {
		return this.data;
	}
}
