package com.idsdatanet.d2.drillnet.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import com.idsdatanet.d2.core.report.Attachments;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ReportJobParams {
	private String local_output_file = null;	
	private Stack<UserSelectionSnapshot> multiCopiesJobData = null;
	private Map<String,Attachments> runtimeContext=null;
	private ReportAutoEmail autoReportEmail = null;
	private boolean isUserSessionReportJob = true;
	private ReportDataGenerator additionalReportDataGenerator = null;
	private String paperSize = null;
	private String language = null;
	private List<ReportJobResult> reportJobResult = null;
	private boolean threadWaiting = false;
	private String queryString = null;
	private String queryUid = null;
	private boolean isJoinReport = false;
	private Map<String,Object> emailContentMapping= new HashMap<String,Object>();
	private String lookupCompanyUid = null;
	private String rigInformationUid = null;
	
	public String getQueryUid() {
		return queryUid;
	}

	public void setQueryUid(String queryUid) {
		this.queryUid = queryUid;
	}

	/** Variable used to tell if the current job uses multiple wells*/
	private boolean multiWellJob = false;
	
	/** Variable used to contain the list of operations to apply the bean
	 *  generators to */
	private List<UserSelectionSnapshot> multiWellList;
	
	public ReportJobResult addReportJobResult(){
		if(this.reportJobResult == null) this.reportJobResult = new ArrayList<ReportJobResult>();
		ReportJobResult result = new ReportJobResult();
		this.reportJobResult.add(result);
		return result;
	}

	public ReportJobResult getLastReportJobResult(){
		if(this.reportJobResult != null && this.reportJobResult.size() > 0){
			return this.reportJobResult.get(this.reportJobResult.size() - 1);
		}else{
			return null;
		}
	}
	
	public List<ReportJobResult> getReportJobResult(){
		return this.reportJobResult;
	}
	
	public boolean isThreadWaiting(){
		return this.threadWaiting;
	}
	
	synchronized public void waitOnThisObject() throws Exception {
		if(this.threadWaiting) throw new Exception("Already another thread waiting on this object");
		this.threadWaiting = true;
		this.wait();
	}
	
	public void notifyWaitingThreadOnThisObject(){
		if(this.threadWaiting){
			synchronized(this){
				this.threadWaiting = false;
				this.notifyAll();
			}
		}
	}
	
	public boolean isMultiWellJob() {
		return multiWellJob;
	}

	public List<UserSelectionSnapshot> getMultiWellList() {
		return multiWellList;
	}

	public void setMultiWellList(List<UserSelectionSnapshot> multiWellList) {
		this.multiWellList = multiWellList;
	}

	public void setMultiWellJob(boolean multiWellJob) {
		this.multiWellJob = multiWellJob;
	}

	public void resetForNewJob(){
		this.local_output_file = null;
	}
	
	public void dispose(){
		local_output_file = null;	
		multiCopiesJobData = null;
		runtimeContext=null;
		autoReportEmail = null;
		additionalReportDataGenerator = null;
		paperSize = null;
		language = null;
		reportJobResult = null;
		emailContentMapping = null;
		lookupCompanyUid = null;
		rigInformationUid = null;
	}

	public void setLocalOutputFile(String value){
		this.local_output_file = value;
	}
	
	public String getLocalOutputFile(){
		return this.local_output_file;
	}
	
	public void setMultiCopiesJob(List<UserSelectionSnapshot> list){
		this.multiCopiesJobData = new Stack<UserSelectionSnapshot>();
		this.multiCopiesJobData.addAll(list);
	}
	
	public UserSelectionSnapshot getNextMultiCopiesJob(){
		if(this.multiCopiesJobData == null) return null;
		if(this.multiCopiesJobData.isEmpty()) return null;
		return this.multiCopiesJobData.pop();
	}
	
	public boolean isMultiCopiesJobEnd()
	{
		if(this.multiCopiesJobData == null){
			return false;
		}else{
			return this.multiCopiesJobData.size()==0;
		}
	}
	public ReportAutoEmail getReportAutoEmail(){
		return this.autoReportEmail;
	}
	
	public void setReportAutoEmail(ReportAutoEmail value){
		this.autoReportEmail = value;
	}
	
	public boolean isUserSessionReportJob(){
		return this.isUserSessionReportJob;
	}
	
	public void setUserSessionReportJob(boolean value){
		this.isUserSessionReportJob = value;
	}
	
	public void setAdditionalReportDataGenerator(ReportDataGenerator value){
		this.additionalReportDataGenerator = value;
	}
	
	public ReportDataGenerator getAdditionalReportDataGenerator(){
		return this.additionalReportDataGenerator;
	}
	
	public void setPaperSize(String value){
		this.paperSize = value;
	}
	
	public String getPaperSize(){
		return this.paperSize;
	}
	
	public void setLanguage(String value){
		this.language = value;
	}
	
	public String getLanguage(){
		return this.language;
	}

	public void setRuntimeContext(Map<String,Attachments> runtimeContext) {
		this.runtimeContext = runtimeContext;
	}

	public Map<String,Attachments> getRuntimeContext() {
		return runtimeContext;
	}
	
	public String getQueryString() {
		return queryString;
	}

	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	public void setJoinReport(boolean isJoinReport) {
		this.isJoinReport = isJoinReport;
	}

	public boolean isJoinReport() {
		return isJoinReport;
	}
	public void putEmailContentMapping (String key, Object value)
	{
		this.emailContentMapping.put(key,value);
	}	
	
	public void setEmailContentMapping (Map<String,Object> emailContentMapping)
	{
		this.emailContentMapping = emailContentMapping;
	}
	
	public Map<String,Object> getEmailContentMapping()
	{
		return this.emailContentMapping;
	}

	public String getLookupCompanyUid() {
		return lookupCompanyUid;
	}

	public void setLookupCompanyUid(String lookupCompanyUid) {
		this.lookupCompanyUid = lookupCompanyUid;
	}

	public String getRigInformationUid() {
		return rigInformationUid;
	}

	public void setRigInformationUid(String rigInformationUid) {
		this.rigInformationUid = rigInformationUid;
	}
	
	

}
