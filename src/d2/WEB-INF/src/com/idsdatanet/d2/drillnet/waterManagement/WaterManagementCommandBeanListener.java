package com.idsdatanet.d2.drillnet.waterManagement;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.WellFluid;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WaterManagementCommandBeanListener extends EmptyCommandBeanListener {
//this is to populate the unit to show in screen for water out of site section
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);		

		for (Field field : WellFluid.class.getDeclaredFields()) {
			thisConverter.setReferenceMappingField(WellFluid.class, field.getName());
			if (thisConverter.isUOMMappingAvailable()) {
				commandBean.setCustomUOM(WaterOutOfSite.class, field.getName(), thisConverter.getUOMMapping());
			}
		}
	
	}

}
