package com.idsdatanet.d2.drillnet.drillingparameters;

import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DrillingParametersUtils {

	/**
	 * Method to assign BharunUid to existing drilling paramaters
	 * @param commandBean
	 * @param session
	 * @param drillingParameters
	 * @throws Exception
	 */
	
	public static void drillingParamertersBhaAssignment(CommandBean commandBean, UserSession session, DrillingParameters drillingParameters) throws Exception {
		Double topDepth = drillingParameters.getDepthTopMdMsl();
		Double bottomDepth = drillingParameters.getDepthMdMsl();
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		if (topDepth!=null && bottomDepth!=null) {
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "depthTopMdMsl");
			if (thisConverter.isUOMMappingAvailable()) {
				thisConverter.setBaseValueFromUserValue(topDepth);
				topDepth = thisConverter.getBasevalue();
			}
			thisConverter.setReferenceMappingField(DrillingParameters.class, "depthMdMsl");
			if (thisConverter.isUOMMappingAvailable()) {
				thisConverter.setBaseValueFromUserValue(bottomDepth);
				bottomDepth = thisConverter.getBasevalue();
			}
			
			String queryString = "FROM Bharun a, BharunDailySummary b " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (b.isDeleted=false or b.isDeleted is null) " +
					"AND a.bharunUid=b.bharunUid " +
					"AND b.dailyUid=:dailyUid " +
					"AND a.depthInMdMsl<=:topDepth " +
					"AND a.depthOutMdMsl>=:bottomDepth " +
					"ORDER BY a.depthInMdMsl, a.depthOutMdMsl ";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
					new String[]{"dailyUid","topDepth","bottomDepth"}, new Object[] {drillingParameters.getDailyUid(),topDepth, bottomDepth}, qp);
			if (list.size()>0) {
				Object[] object = list.get(0);
				Bharun bharun = (Bharun) object[0];
				drillingParameters.setBharunUid(bharun.getBharunUid());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(drillingParameters);
			}
		}
	}
	
	/**
	 * Method to create or update  Drilling Parameters from Activity based on pre-defined free text remarks
	 * @param commandBean
	 * @param session
	 * @param thisActivity
	 * @param fieldsMap
	 * @throws Exception
	 */
	public static void createDrillingParametersBasedOnActivityRemarks(CommandBean commandBean, UserSession session,UserSelectionSnapshot userSelection,Activity thisActivity, Map<String,Object>fieldsMap) throws Exception{
		if (fieldsMap !=null){
			DrillingParameters drillingParameters = null;
			Bharun bharun = null;
			BharunDailySummary bharunDailySummary = null;
			String country = "";
			
			String[] paramsFields = {"operationUid","activityUid"};
			Object[] paramsValues = {thisActivity.getOperationUid(),thisActivity.getActivityUid()};
			String strSql = "FROM DrillingParameters WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND activityUid=:activityUid";
			List<DrillingParameters> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			// OMV WKO - Get Bharun based on activity to populate bharunUid into DP
			String[] paramsFields2 = {"operationUid","activityUid"};
			Object[] paramsValues2 = {thisActivity.getOperationUid(),thisActivity.getActivityUid()};
			String strSql2 = "FROM Bharun WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND activityUid=:activityUid";
			List<Bharun> ls2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
			
			if (ls2.size()!=0) {
				bharun = (Bharun)ls2.get(0);
			}
			
			if (ls.size()==0){
				drillingParameters = new DrillingParameters();
				drillingParameters.setBharunUid("");
				drillingParameters.setActivityUid(thisActivity.getActivityUid());
				
				if (bharun.getBharunUid() != null || bharun.getBharunUid() != "") {
					drillingParameters.setBharunUid(bharun.getBharunUid());
				}	
			} else{
				drillingParameters = ls.get(0);			
			}
			 
			if (drillingParameters!=null ){
				// OMV WKO - Check for country = Austria(AT) if client = OMV
				if (DrillingParametersDataNodeListener.populateBhaDailySummaryTorqueAvg) {
					Well currentWell = session.getCurrentWell();
					if (currentWell != null) {
						country = currentWell.getCountry();
					}
				}
				
				// OMV WKO - Get BharunDailySummary to populate torqueAvg value
				if (DrillingParametersDataNodeListener.populateBhaDailySummaryTorqueAvg && session.getCurrentOperationType().equals("WKO") && country.equals("AT")) {
					String[] paramsFields3 = {"bharunUid","dailyUid"};
					Object[] paramsValues3 = {bharun.getBharunUid(),thisActivity.getDailyUid()};
					String strSql3 = "FROM BharunDailySummary WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND bharunUid=:bharunUid AND dailyUid=:dailyUid";
					List<BharunDailySummary> ls3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3);
					
					if (ls3.size()!=0) {
						bharunDailySummary = (BharunDailySummary)ls3.get(0);
					}
				}
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
				
				//populate activity value to drilling parameters record 
				Double duration = thisActivity.getActivityDuration();
				if (duration!=null ){
					thisConverter.setReferenceMappingField(Activity.class, "activityDuration");
					if (thisConverter.isUOMMappingAvailable()) {
						thisConverter.setBaseValueFromUserValue(duration);
						Double baseValue = thisConverter.getBasevalue();
						
						thisConverter.setReferenceMappingField(DrillingParameters.class, "duration");
						if (thisConverter.isUOMMappingAvailable()) {
							thisConverter.setBaseValue(baseValue);
							duration = thisConverter.getConvertedValue();
						}
					}					
				}								
				drillingParameters.setStartTime(thisActivity.getStartDatetime());
				drillingParameters.setEndTime(thisActivity.getEndDatetime());	
				drillingParameters.setDuration(duration);
				
				if (DrillingParametersDataNodeListener.populateBhaDailySummaryTorqueAvg && session.getCurrentOperationType().equals("WKO") && country.equals("AT")) {
					if (bharun != null) {
						bharun.setTimeIn(thisActivity.getStartDatetime());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharun);
					}
				}
				
				//populate dynamic mapping field from remarks  
				for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
					if (StringUtils.isNotBlank(fieldEntry.getKey())) {
						PropertyUtils.setProperty(drillingParameters, fieldEntry.getKey(), fieldEntry.getValue());
						
						// OMV WKO - When populating into torqueAvg in DP, populates into BharunDailySummary as well
						if (DrillingParametersDataNodeListener.populateBhaDailySummaryTorqueAvg && session.getCurrentOperationType().equals("WKO") && country.equals("AT")) {
							if(bharunDailySummary != null) {
								if (StringUtils.equals("torqueAvg", fieldEntry.getKey())) {
									Double torqueAvg = (Double)fieldEntry.getValue();
									bharunDailySummary.setTorqueAvg(torqueAvg);
									ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharunDailySummary);
								}
							}
						}
					}		
				}
				
				
				Boolean isSubmit = drillingParameters.getIsSubmit();
				//if(isSubmit!=null && isSubmit) RevisionUtils.unSubmitRecordWithStaticRevisionLog(userSelection, drillingParameters, "Auto update from time codes");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(drillingParameters);
				
				//assign BharunUid
				DrillingParametersUtils.drillingParamertersBhaAssignment(commandBean, session, drillingParameters);
			}
		}		
	}
	
}
