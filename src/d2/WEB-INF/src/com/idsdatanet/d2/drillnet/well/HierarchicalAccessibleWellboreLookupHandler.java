package com.idsdatanet.d2.drillnet.well;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.cache.Wellbores;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Well;

public class HierarchicalAccessibleWellboreLookupHandler implements LookupHandler {
	
	private int nameMaxLength = 30;
	
	public void setNameMaxLength(int nameMaxLength) {
		this.nameMaxLength = nameMaxLength;
	}
	
	public boolean onlyReturnAccessibleWellbores() {
		return true;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> results = new LinkedHashMap<String, LookupItem>();
		List<LookupItem> lookupItems = new ArrayList<LookupItem>();
		
		if (request != null) {
			
			Collection<_Well> wellList = ApplicationUtils.getConfiguredInstance().getCachedAllWells().values();
			
			//get all the accessible wellbores
			Wellbores accessibleWellbores = UserSession.getInstance(request).getCachedAllAccessibleWellbores();
			
			if (wellList != null && wellList.size() > 0) {
				for (_Well well : wellList) {
					String wellName	= well.getWellName();
					List<Wellbore> wellboreList = ApplicationUtils.getConfiguredInstance().getWellboresByWell(well.getWellUid());
					if (wellboreList != null && wellboreList.size() > 0) {
						for (Wellbore wellbore : wellboreList) {
							//check if wellbore is accessible or not
							if (!onlyReturnAccessibleWellbores() || accessibleWellbores.containsKey(wellbore.getWellboreUid())){
								String hierarchicalWellbore = ApplicationUtils.getConfiguredInstance().getCascadeParentWellboreName(wellbore.getWellboreUid(), MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES);
								hierarchicalWellbore = hierarchicalWellbore.substring(0, hierarchicalWellbore.lastIndexOf(MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES));
								String label = joinString(wellName + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + hierarchicalWellbore, MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES);
								lookupItems.add(new LookupItem(wellbore.getWellboreUid(), label));
							}
						}
					}
				}
			}
		}
		
		Collections.sort(lookupItems, new LookupItemComparator());
		for (LookupItem lookupItem : lookupItems) {
			results.put(lookupItem.getKey(), lookupItem);
		}
		return results;
	}
	
	private class LookupItemComparator implements Comparator<LookupItem> {
		
		public int compare(LookupItem v1, LookupItem v2) {
			if (v1 == null || v1.getValue() == null || v2 == null || v2.getValue() == null || StringUtils.equals((String)v1.getValue(), (String)v2.getValue())) {
				return 0;
			}
			return WellNameUtil.getPaddedStr((String)v1.getValue()).compareTo(WellNameUtil.getPaddedStr((String)v2.getValue()));
		}
	}
	
	/**
	 * Merge the same occurance of well, wellbore and operation name in a given input
	 * @param input
	 * @param delim
	 * @return
	 */
	private String joinString(String input, String delim) {
		if(input.indexOf(delim) != -1) {
			String[] values = input.split(delim);
			if(values.length > 1) {
				String pivot = values[0];
				StringBuilder out = new StringBuilder(getShortName(pivot));
				for(int i=1; i < values.length; i++) {
					if(! StringUtils.equals(pivot.trim(), values[i].trim())) {
						out.append(delim + getShortName(values[i]));
						pivot = values[i];
					}
				}
				return out.toString();
			} else {
				return input;
			}
		} else {
			return input;
		}
	}
	
	private String getShortName(String name) {
		if (name.length() > nameMaxLength) {
			int midLength = nameMaxLength / 2;
			return name.substring(0, midLength - 3) + "..." + name.substring(name.length() - midLength);
		}
		return name;
	}
}
