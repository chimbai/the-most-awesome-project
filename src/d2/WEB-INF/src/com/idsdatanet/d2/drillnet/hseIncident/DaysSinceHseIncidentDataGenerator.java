package com.idsdatanet.d2.drillnet.hseIncident;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class DaysSinceHseIncidentDataGenerator implements ReportDataGenerator {

	private List<String> hseIncidentMarkers;
	
	public void setHseIncidentMarkers(List<String> hseIncidentMarkers){		
		if(hseIncidentMarkers != null){
			this.hseIncidentMarkers = new ArrayList<String>();
			for(String value: hseIncidentMarkers){
				this.hseIncidentMarkers.add(value);
			}
		}
	}
	
	public List<String> getHseIncidentMarkers() {
		return this.hseIncidentMarkers;
	}

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
		if(this.getHseIncidentMarkers() != null)
		{
			for(String value:this.hseIncidentMarkers)
			{
				//get the current date from the user selection
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
				if (daily == null) return;
				Date todayDate = daily.getDayDate();
				String currentRigInformationUid;
				if (userContext.getUserSelection().getRigInformationUid() != null){
					currentRigInformationUid = userContext.getUserSelection().getRigInformationUid().toString();
				
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime(todayDate);
					thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
					thisCalendar.set(Calendar.MINUTE, 59);
					thisCalendar.set(Calendar.SECOND , 59);
					thisCalendar.set(Calendar.MILLISECOND , 59);
					
					String strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) and rigInformationUid = :currentRigInformationUid AND hseEventdatetime <= :currentDate AND incidentCategory = :hseIncidentMarkers and (supportVesselInformationUid is null or supportVesselInformationUid = '') ORDER BY hseEventdatetime DESC";
					String[] paramsFields2 = {"currentRigInformationUid", "currentDate","hseIncidentMarkers"};
					Object[] paramsValues2 = {currentRigInformationUid, thisCalendar.getTime(), value.toString()};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);		
					
					if (lstResult.size() > 0){
						HseIncident hseIncident = (HseIncident) lstResult.get(0);
						
						CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "daysSinceLta");
	
						String incidentCategory = hseIncident.getIncidentCategory();
						Long eventDateTime = hseIncident.getHseEventdatetime().getTime();
	
						double daysSinceHseIncident = CommonUtil.getConfiguredInstance().calculateDaysSinceHseIncident(daily.getDayDate(), hseIncident.getHseEventdatetime(), userContext.getUserSelection().getGroupUid());
						thisConverter.setBaseValue(daysSinceHseIncident);
						Double daysSinceIncident = thisConverter.getConvertedValue();					
						
						ReportDataNode thisReportNode = reportDataNode.addChild("DaysSinceHseIncident");
						thisReportNode.addProperty("incidentCategory", incidentCategory);
						thisReportNode.addProperty("daysSinceHseIncident", daysSinceIncident.toString());					
						thisReportNode.addProperty("hseEventDateTime", hseIncident.getHseEventdatetime().toString());					
						thisReportNode.addProperty("hseEventDateTimeEpochMS", eventDateTime.toString());
						thisReportNode.addProperty("currentRig", currentRigInformationUid);
					}
				}
			}
		}		
	}
}