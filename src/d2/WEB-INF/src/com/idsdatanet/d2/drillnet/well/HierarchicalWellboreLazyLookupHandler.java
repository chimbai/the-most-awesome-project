package com.idsdatanet.d2.drillnet.well;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.flex.FlexClientAdaptorForCommandBean;

import java.util.Collections;

public class HierarchicalWellboreLazyLookupHandler implements LookupHandler {
	
	private static final String LAZY_LOOKUP_START_KEY = "_start_with_key";
	
	private Boolean inStringSearch = false;
	
	public void setInStringSearch(Boolean value) {
		this.inStringSearch = value;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> results = new LinkedHashMap<String, LookupItem>();
		List<LookupItem> lookupItems = new ArrayList<LookupItem>();
		QueryProperties qp = new QueryProperties();
		qp.setRowsToFetch(0, FlexClientAdaptorForCommandBean.LAZY_LOAD_LOOKUP_ROW_TO_FETCH * 3);
		List<Object[]> list = null;
		
		String queryString = "FROM Well w, Wellbore wb " +
			"WHERE (w.isDeleted=false or w.isDeleted is null) " +
			"AND (wb.isDeleted=false or wb.isDeleted is null) " +
			"AND w.wellUid = wb.wellUid ";
		
		if (request != null) {
			String startKey = request.getParameter(LAZY_LOOKUP_START_KEY);
			
			if (startKey!=null) {
				String[] keywords = startKey.split(MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES);
				
				String wellConditions = "";
				int counter = 0;
				int arrayLength = (keywords.length * 2);
				String[] paramNames = new String[arrayLength];
				Object[] paramValues = new Object[arrayLength];
				for (String keyword : keywords) {
					wellConditions += ("".equals(wellConditions)?"":" or ") + "w.wellName like :wellName_" + counter;
					paramNames[counter] = "wellName_" + counter;
					paramValues[counter] = (inStringSearch?"%":"") + keyword.trim() + "%";
					counter++;
				}
				for (String keyword : keywords) {
					wellConditions += ("".equals(wellConditions)?"":" or ") + "wb.wellboreName like :wellboreName_" + counter;
					paramNames[counter] = "wellboreName_" + counter;
					paramValues[counter] = (inStringSearch?"%":"") + keyword.trim() + "%";
					counter++;
				}
				if (!"".equals(wellConditions)) queryString += " and (" + wellConditions + ") ";
				queryString += " order by w.wellName, wb.wellboreName"; 
				
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
				
				counter = 0; 
				for (Object[] rec : list) {
					Well well = (Well) rec[0];
					Wellbore wellbore = (Wellbore) rec[1];
					String hierarchicalWellbore = ApplicationUtils.getConfiguredInstance().getCascadeParentWellboreName(wellbore.getWellboreUid(), MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES);
					hierarchicalWellbore = hierarchicalWellbore.substring(0, hierarchicalWellbore.lastIndexOf(MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES));
					String label = joinString(well.getWellName() + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + hierarchicalWellbore, MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES);
					
					if (this.inStringSearch) {
						if (label.toLowerCase().indexOf(startKey.toLowerCase())>=0) {
							lookupItems.add(new LookupItem(wellbore.getWellboreUid(), label));
							counter++;
						}
					} else {
						if (label.toLowerCase().indexOf(startKey.toLowerCase())==0) {
							lookupItems.add(new LookupItem(wellbore.getWellboreUid(), label));
							counter++;
						}
					}
					if (FlexClientAdaptorForCommandBean.LAZY_LOAD_LOOKUP_ROW_TO_FETCH <= counter) break;
				}
				
			} else {
				if (node!=null) {
					String parentWellboreUid = null;
					if (node.getData() instanceof Wellbore) { //for wellboreCommandBean
						parentWellboreUid = ((Wellbore) node.getData()).getParentWellboreUid(); 
					}else if (node.getData() instanceof Operation){
						parentWellboreUid = ((Operation) node.getData()).getWellboreUid();
					} else {
						if (node.getDynaAttr()!=null) { //for wellOperationCommandBean
							if (node.getDynaAttr().containsKey("parentWellboreUid")) {
								parentWellboreUid = (String) node.getDynaAttr().get("parentWellboreUid");
							}
						}
					}
					if (parentWellboreUid!=null) {
						queryString += " AND wb.wellboreUid=:wellboreUid";
						list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "wellboreUid", parentWellboreUid);
						
						for (Object[] rec : list) {
							Well well = (Well) rec[0];
							Wellbore wellbore = (Wellbore) rec[1];
							String hierarchicalWellbore = ApplicationUtils.getConfiguredInstance().getCascadeParentWellboreName(wellbore.getWellboreUid(), MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES);
							hierarchicalWellbore = hierarchicalWellbore.substring(0, hierarchicalWellbore.lastIndexOf(MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES));
							String label = joinString(well.getWellName() + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + hierarchicalWellbore, MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES);
							lookupItems.add(new LookupItem(wellbore.getWellboreUid(), label));
						}
					}
				}
				
			}
		}
		
		Collections.sort(lookupItems, new LookupItemComparator());
		for (LookupItem lookupItem : lookupItems) {
			results.put(lookupItem.getKey(), lookupItem);
		}
		return results;
	}
	
	
	
	
	private class LookupItemComparator implements Comparator<LookupItem> {
		
		public int compare(LookupItem v1, LookupItem v2) {
			if (v1 == null || v1.getValue() == null || v2 == null || v2.getValue() == null || StringUtils.equals((String)v1.getValue(), (String)v2.getValue())) {
				return 0;
			}
			return WellNameUtil.getPaddedStr((String)v1.getValue()).compareTo(WellNameUtil.getPaddedStr((String)v2.getValue()));
		}
	}
	
	/**
	 * Merge the same occurance of well, wellbore and operation name in a given input
	 * @param input
	 * @param delim
	 * @return
	 */
	private String joinString(String input, String delim) {
		if(input.indexOf(delim) != -1) {
			String[] values = input.split(delim);
			if(values.length > 1) {
				String pivot = values[0];
				StringBuilder out = new StringBuilder(getShortName(pivot));
				for(int i=1; i < values.length; i++) {
					if(! StringUtils.equals(pivot.trim(), values[i].trim())) {
						out.append(delim + getShortName(values[i]));
						pivot = values[i];
					}
				}
				return out.toString();
			} else {
				return input;
			}
		} else {
			return input;
		}
	}
	
	/**
	 * Return a string of 30 characters only.
	 * @param name
	 * @return
	 */
	private String getShortName(String name) {
		if(name.length() > 30) {
			return name.substring(0, 30) + "...";
		} else {
			return name;
		}
	}

}
