package com.idsdatanet.d2.drillnet.surveyreference;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.templateimport.DefaultTemplateImportHandler;
import com.idsdatanet.d2.core.web.mvc.templateimport.EmptySlot;

public class SurveyStationTemplateImportHandler extends DefaultTemplateImportHandler{
	
	@Override
	protected void setupAdditionalBindingValues(int line,
			List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode) {
		
		UserSelectionSnapshot userSelection = null;
		List<Daily> dailyList = null;
		
		try{
			userSelection = new UserSelectionSnapshot(commandBean.getCurrentUserSession());
			dailyList = ApplicationUtils.getConfiguredInstance().getDaysInOperation(userSelection.getOperationUid());
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		if (bindingValue != null) {
			if (bindingValue.get(0) instanceof EmptySlot) {
				
			} else {
				try {
					
					String fieldName = this.getPropertyNameFromField(targetMeta, "dailyUid");
					
					Date surveyDate = null;
					if(bindingProperties.indexOf(fieldName) < 0) return;
					Object fieldValue = bindingValue.get(bindingProperties.indexOf(fieldName));
					if (fieldValue!=null) surveyDate = CommonDateParser.parse(fieldValue.toString());
					
					if (dailyList!=null || dailyList.size() > 0) {
						if(surveyDate!=null) {
							for(Daily d:dailyList){
								if(DateUtils.isSameDay(d.getDayDate(), surveyDate)) {
									bindingValue.set(bindingProperties.indexOf(fieldName), d.getDailyUid());
								}
							}
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
