package com.idsdatanet.d2.drillnet.report;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.BooleanUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.service.Manager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.depot.core.util.DepotUtils;

public class RsdJrsReportModule extends JRSReportModule{

	private String depotWitsmlQueryTemplate;
	private String depotWitsmlTags;
	private String depotWitsmlName;
	
	public String getDepotWitsmlQueryTemplate() {
		return depotWitsmlQueryTemplate;
	}

	public void setDepotWitsmlQueryTemplate(String depotWitsmlQueryTemplate) {
		this.depotWitsmlQueryTemplate = depotWitsmlQueryTemplate;
	}

	public String getDepotWitsmlTags() {
		return depotWitsmlTags;
	}

	public void setDepotWitsmlTags(String depotWitsmlTags) {
		this.depotWitsmlTags = depotWitsmlTags;
	}

	public String getDepotWitsmlName() {
		return depotWitsmlName;
	}

	public void setDepotWitsmlName(String depotWitsmlName) {
		this.depotWitsmlName = depotWitsmlName;
	}
	
	@Override
	public String getOutputFileExtension() throws Exception {	
		return this.getJrsReportConfiguration().getJrExecutionRequest().getOutputFormat();
	}
	
	private ReportJobParams popReportJobData(JobContext job){
		ReportJobParams reportJobData = this.reportJobs.get(job);
		if(reportJobData != null) this.reportJobs.remove(job);
		return reportJobData;
	}
	
	synchronized public void jobEnded(JobContext job) throws Exception{
		ReportJobParams reportJobData = this.popReportJobData(job);
		if(reportJobData == null) return;
		
		if(job.getJobResponse().isSuccess()){
			ReportFiles reportFiles = null;
			boolean newFile = false;
			
			List<Object> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportFiles where (isDeleted = false or isDeleted is null) and reportFile = :reportFile", "reportFile", reportJobData.getLocalOutputFile());
			if(rs.size() > 0){
				reportFiles = (ReportFiles) rs.get(0);
			}else{
				reportFiles = new ReportFiles();
				newFile = true;
			}
			
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(job.getUserContext().getUserSelection().getDailyUid());
			if(daily != null){
				reportFiles.setReportDayDate(daily.getDayDate());
			}
			
			ReportDaily reportDaily = this.getRelatedReportDaily(job.getUserContext().getUserSelection().getDailyUid(), this.getDailyType());
			if(reportDaily != null){
				reportFiles.setReportDayNumber(reportDaily.getReportNumber());
			}else{
				reportDaily = ApplicationUtils.getConfiguredInstance().getReportDailyByOperationType(job.getUserContext().getUserSelection().getDailyUid(), job.getUserContext().getUserSelection().getOperationType());
				if (reportDaily != null) 
				{
					reportFiles.setReportDayNumber(reportDaily.getReportNumber());
				}
			}
					
			Operation reportOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(job.getUserContext().getUserSelection().getOperationUid());
			
			String opCo = null;
			if(reportOperation!=null){
				opCo = reportOperation.getOpCo();
			}
			
			String report_type = this.getReportType(job.getUserContext().getUserSelection());
			ReportTypes report_type_record = ReportUtils.getConfiguredInstance().getReportType(job.getUserContext().getUserSelection().getGroupUid(), report_type, reportJobData.getPaperSize(), opCo);
			
			reportFiles.setDateGenerated(new Date());
			reportFiles.setReportUserUid(job.getUserContext().getUserSelection().getUserUid());
			reportFiles.setReportOperationUid(job.getUserContext().getUserSelection().getOperationUid());
			reportFiles.setDailyUid(job.getUserContext().getUserSelection().getDailyUid());
			reportFiles.setReportFile(reportJobData.getLocalOutputFile());
			reportFiles.setReportType(report_type);
			reportFiles.setGroupUid(job.getUserContext().getUserSelection().getGroupUid());
			
			if(newFile){
				if (reportOperation == null || !BooleanUtils.isTrue(reportOperation.getTightHole())) { // tight hole should override GWP
					reportFiles.setIsPrivate(! "1".equals(GroupWidePreference.getValue(job.getUserContext().getUserSelection().getGroupUid(), GroupWidePreference.GWP_NEWLY_GENERATED_REPORT_DEFAULT_PUBLIC)));
				}
			}
			
			this.beforeSaveReportEntry(job, reportFiles);
			
			reportFiles.setDisplayName(this.getReportDisplayNameOnCreation(reportFiles, report_type_record, reportOperation, daily, reportDaily, job.getUserContext().getUserSelection(), reportJobData));

			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportFiles);
			
			//RSD convert jasper pdf to image
			if("pdf".equalsIgnoreCase(this.getJrsReportConfiguration().getJrExecutionRequest().getOutputFormat()) && this.getJrsReportConfiguration().getJrExecutionRequest().getPdfToImage().TRUE) {
				this.convertPdfToImage(this.getReportOutputFile(reportFiles), job);
			}
		}
	}
	
	protected byte[] getBytesFromFile(File file) throws IOException {
		try (InputStream is = new FileInputStream(file)) {
			long length = file.length();
	
			if (length > Integer.MAX_VALUE) {
				// File too large
			}
			byte[] bytes = new byte[(int)length];
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length-offset)) >= 0) {
				offset += numRead;
			}
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file " + file.getName());
			}
			return bytes;
		}
	}
	
	/*
	 * Convert pdf to image and encode it to Base64 string
	 */
	private void convertPdfToImage(File output_file, JobContext job) throws Exception {
		
		Manager dbManager = ApplicationUtils.getConfiguredInstance().getDaoManager();
		Well well = (Well) dbManager.getObject(Well.class, job.getUserContext().getUserSelection().getWellUid());
		Wellbore wellbore = (Wellbore) dbManager.getObject(Wellbore.class, job.getUserContext().getUserSelection().getWellboreUid());
		
		File rsdImageParentFolder = new File(new File(ApplicationConfig.getConfiguredInstance().getServerRootPath() + "WEB-INF"), "rsdImage");
		if(rsdImageParentFolder != null){
			if(! rsdImageParentFolder.exists()) rsdImageParentFolder.mkdirs();
		}
		if(output_file.exists()) {
			PDDocument document = PDDocument.load(output_file);
			PDFRenderer pdfRenderer = new PDFRenderer(document);
			String imageOutputName = rsdImageParentFolder + File.separator + ReportUtils.replaceInvalidCharactersInFileName(depotWitsmlTags + well.getWellName()) + ".png";
			for (int page = 0; page < document.getNumberOfPages(); ++page) {
				BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 150, ImageType.RGB);
				ImageIOUtil.writeImage(bim, imageOutputName, 150);
			}
			document.close();
			
			byte[] fileInBytes = this.getBytesFromFile(new File(imageOutputName));
			String base64Image = Base64.encodeBase64String(fileInBytes);
			
			this.logContentToXml(well, wellbore, base64Image, imageOutputName, rsdImageParentFolder);
			
			File imageFile = new File (imageOutputName);
			if(imageFile.exists()) imageFile.delete();
		}
	}
	/*
	 * Log content to witsml attachment schema
	 */
	private void logContentToXml(Well well, Wellbore wellbore, String base64Image, String imageOutputName, File rsdImageParentFolder) throws Exception {
		
		String sql = "SELECT d.depotWellUid, d.depotWellboreUid, d.depotWellName, d.depotWellboreName, d.operationUid FROM DepotWellOperationMapping d, SchedulerTemplate s WHERE "
				+ "d.schedulerTemplateUid=s.schedulerTemplateUid and d.wellUid=:wellUid and d.wellboreUid=:wellboreUid "
				+ "and (d.isDeleted is null or d.isDeleted=false) and (s.isDeleted is null or s.isDeleted=false) and s.schedulerName='KDI_Attachment'";
		String[] paramsFields = {"wellUid", "wellboreUid"};		
		Object[] paramsValues = {well.getWellUid(), wellbore.getWellboreUid()};
		
		String depotWellUid = null;
		String depotWellboreUid = null;
		String depotWellName = null;
		String depotWellboreName = null;
		String operationUid = null;
		List<Object[]> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		if (results != null && results.size() > 0) {
			for (Object[] result : results) {
				depotWellUid = result[0].toString();
				depotWellboreUid = result[1].toString();
				depotWellName = result[2].toString();
				depotWellboreName = result[3].toString();
				operationUid = result[4].toString();
			}
			Date now = new Date();
			GregorianCalendar gregCal = new GregorianCalendar();
			gregCal.setTime(now);
			XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregCal);
			
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("uidWell", depotWellUid);
			data.put("uidWellbore", depotWellboreUid);
			data.put("uid", DepotUtils.randomUUIDString());
			data.put("nameWell", depotWellName);
			data.put("nameWellbore", depotWellboreName);
			data.put("nameFile", new File(imageOutputName).getName());
			data.put("content", base64Image);
			data.put("dTimCreation", xmlGregCal.toString());
			data.put("dTimLastChange", xmlGregCal.toString());
			data.put("priv_customData", this.depotWitsmlTags);
			data.put("name", this.depotWitsmlName);
			
			String kdiSnapshot = DepotUtils.getConfiguredInstance().generateContent(this.depotWitsmlQueryTemplate, data);
			File f = new File(rsdImageParentFolder + File.separator + depotWitsmlTags + operationUid + ".xml");
			FileOutputStream fos = new FileOutputStream(f);
			byte[] contentInBytes = kdiSnapshot.getBytes();

			fos.write(contentInBytes);
			fos.flush();
			fos.close();
		}
	}
}
