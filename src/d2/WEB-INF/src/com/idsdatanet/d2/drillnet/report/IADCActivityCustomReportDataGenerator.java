package com.idsdatanet.d2.drillnet.report;

import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.RigTour;
import com.idsdatanet.d2.core.model.TourActivity;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * @author Thien Fung Sian
 *
 */
public class IADCActivityCustomReportDataGenerator implements ReportDataGenerator{
	
	private String fontName = "Arial";
	private int fontSize = 6;
	private Double widthInCm = 9.75;
	Map<String, Double> thisCounter = null;
	private int numberOfTour = 0;

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub		
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		
		/* Get the activity records */
		List<TourActivity> activities = getTimeCodesForIadc( userContext.getUserSelection());
		this.getNumberOfTour(userContext.getUserSelection());
		thisCounter = new HashMap<String, Double>();
		
		CustomFieldUom thisConverter = null;
		if (userContext.getUserSelection() != null) { 
			thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
		}
		
		int groupingId = 0;
		
		if( activities.size() > 0 ){
			for( TourActivity activity : activities ){
				String remark = "";
				groupingId++;
				
				if( activity.getActivityDescription() != null ) remark += activity.getActivityDescription();
				
				if( StringUtils.isNotBlank( remark ) ){
					List<String> contents = ReportUtils.getWrappedText(remark, createFont(), getWidthInPixel());
					int rowCount = 0;
					for( String row : contents){
						rowCount++;
						//System.out.println(row);
						ReportDataNode thisReportNode = reportDataNode.addChild("Activity");
						thisReportNode.addProperty("activityUid", ( activity.getTourActivityUid() != null ? activity.getTourActivityUid() : ""));
						thisReportNode.addProperty("iadcCode", ( activity.getIadcCode() != null && rowCount <= 1 ? activity.getIadcCode() : ""));
						thisReportNode.addProperty("startDatetime", ( activity.getStartDatetime() != null && rowCount <= 1 ? String.valueOf( activity.getStartDatetime().getTime() ) : null));
						thisReportNode.addProperty("endDatetime", ( activity.getEndDatetime() != null && rowCount <= 1 ? String.valueOf( activity.getEndDatetime().getTime() ) : null));
						thisReportNode.addProperty("activityDuration", ( rowCount <= 1 ? this.formatData(thisConverter, activity.getActivityDuration(), Activity.class, "activityDuration") : null ));
						thisReportNode.addProperty("remark", row);
						thisReportNode.addProperty("rigTourUid",  (activity.getRigTourUid() != null ? activity.getRigTourUid() : ""));
						thisReportNode.addProperty("rigTourNum",  (activity.getRigTourUid() != null ? getRigTourNum(activity.getRigTourUid()) : ""));
						thisReportNode.addProperty("groupId", String.valueOf(groupingId));
						if( activity.getRigTourUid() != null ) trackRecordCounter( activity.getRigTourUid() );
					}					
				}else{
					ReportDataNode thisReportNode = reportDataNode.addChild("Activity");
					thisReportNode.addProperty("activityUid", ( activity.getTourActivityUid() != null ? activity.getTourActivityUid() : ""));
					thisReportNode.addProperty("iadcCode", ( activity.getIadcCode() != null ? activity.getIadcCode() : ""));
					thisReportNode.addProperty("startDatetime", ( activity.getStartDatetime() != null ? String.valueOf( activity.getStartDatetime().getTime() ) : null));
					thisReportNode.addProperty("endDatetime", ( activity.getEndDatetime() != null ? String.valueOf( activity.getEndDatetime().getTime() ) : null));
					thisReportNode.addProperty("activityDuration", this.formatData(thisConverter, activity.getActivityDuration(), Activity.class, "activityDuration"));
					thisReportNode.addProperty("remark", "");
					thisReportNode.addProperty("rigTourUid",  (activity.getRigTourUid() != null? activity.getRigTourUid() : ""));
					thisReportNode.addProperty("rigTourNum",  (activity.getRigTourUid() != null? getRigTourNum(activity.getRigTourUid()) : ""));
					thisReportNode.addProperty("groupId", String.valueOf(groupingId));
					if( activity.getRigTourUid() != null ) trackRecordCounter( activity.getRigTourUid() );
				}			
			}
		}
			
		// Activity Summmry for handling overflow in IADC Report
		ReportDataNode thisReportNode2 = reportDataNode.addChild("ActivitySummary");
		Double rowsPerTour = 12.0; //Default 12 rows for 2 Tour IADC.
		int maxOverflow = 1;
		/*if( numberOfTour == 3 ){
			rowsPerTour = 10.0;
		}*/
		for( Entry<String, Double> entry : thisCounter.entrySet()){
			Double record = entry.getValue();
			int overflow = (int) Math.ceil( record / rowsPerTour );
			if( overflow > maxOverflow ) maxOverflow = overflow;
		}
		thisReportNode2.addProperty( "overflow", String.valueOf( maxOverflow ) );
		thisReportNode2.addProperty( "numberOfTour", String.valueOf( numberOfTour ) );
	}

	private void getNumberOfTour( UserSelectionSnapshot userSelection ) throws Exception{
		String queryString = "FROM RigTour WHERE (isDeleted=false or isDeleted is null) and rigInformationUid=:rigInformationUid ORDER BY sequence, tourStartDateTime";
		List<RigTour> rigTourList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "rigInformationUid", userSelection.getRigInformationUid());

		if( rigTourList.size() > 0 ) numberOfTour = rigTourList.size();
		
	}

	/**
	 * Method to increment activity record count for each tour.
	 * @param rigTourUid
	 * @throws Exception
	 */
	private void trackRecordCounter(String rigTourUid) throws Exception {
		// TODO Auto-generated method stub
		if( !thisCounter.containsKey( rigTourUid ) ){
			thisCounter.put(rigTourUid, 1.0);
		}else{
			Double count = thisCounter.get(rigTourUid);
			count++;
			thisCounter.remove(rigTourUid);
			thisCounter.put(rigTourUid, count.doubleValue());
		}
	}

	/**
	 * Method to get Tour Number based on rigTourUid.
	 * @param rigTourUid
	 * @return Tour Number
	 * @throws Exception
	 */
	private String getRigTourNum(String rigTourUid) throws Exception {
		// TODO Auto-generated method stub
		String queryString = "Select tourNumber From RigTour Where rigTourUid = :rigTourUid And (isDeleted = false or isDeleted is null)";
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "rigTourUid", rigTourUid);
		if ( result.size() > 0 ){
			if( result.get(0) !=  null ) return String.valueOf(result.get(0));
		}		
		return "";
	}

	/**
	 * Method to get time codes (activity records) for IADC Tour based on tour start/end time.
	 * @param userSelection
	 * @return a list of Activity object.
	 * @throws Exception
	 */
	private List<TourActivity> getTimeCodesForIadc( UserSelectionSnapshot userSelection) throws Exception {
		List<TourActivity> result = new ArrayList();
		
		String strSql = "FROM TourActivity WHERE dailyUid = :dailyUid AND (isSimop='0' OR isSimop = '' OR isSimop is NULL) AND (isOffline='0' OR isOffline = '' OR isOffline is NULL) AND (isDeleted = false or isDeleted is null)ORDER BY startDatetime";
		result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", userSelection.getDailyUid());
		
		return result;
	}

	private int getWidthInPixel() throws Exception{
		return (int) Math.ceil( widthInCm * 37.795275591 );
	}
	
	private Font createFont() throws Exception {
		// TODO Auto-generated method stub
		return new Font(fontName, Font.PLAIN, fontSize);
	}

	/* Setter and Getter */
	public void setFontName(String fontName) {
		this.fontName = fontName;
	}

	public String getFontName() {
		return fontName;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setWidthInCm(Double widthInCm) {
		this.widthInCm = widthInCm;
	}

	public Double getWidthInCm() {
		return widthInCm;
	}
	
	private String formatData(CustomFieldUom thisConverter, Double value, Class className, String fieldName) throws Exception {
		if (value!=null) {
			if (thisConverter!=null) {
				thisConverter.setReferenceMappingField(className, fieldName);
				if (thisConverter.isUOMMappingAvailable()) {
					return thisConverter.formatOutputPrecision(value);
				}				
			}
			return String.valueOf(value);
		}		
		return "";
	}
}
