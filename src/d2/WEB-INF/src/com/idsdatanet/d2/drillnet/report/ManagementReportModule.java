package com.idsdatanet.d2.drillnet.report;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.report.MultipleUserSelectionSnapshot;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ManagementReportModule extends DefaultReportModule {
	private static final String MGT_REPORT_DATE = "mgt_report_date";
	private static final String MGT_REPORT_COMBINED_REPORT = "mgt_report_combined_report";
	
	private static final String FIELD_SELECTED_DATE = "mgtSelectedDate";
	private static final String FIELD_SELECTED_OPERATIONS = "mgtSelectedOperations";
	private static final String FIELD_DISTINCT_REPORT = "mgtDistinctReport";
	private static final String LATEST_REPORT_FLAG = "latestReport";
	//private static final String MAX_ALLOWABLE_DAYS = "maxAllowableDays";
	private static final String FIELD_SELECTED_OPERATION_TYPE = "mgtSelectedOperationType";
	private static final String FIELD_COMMENT = "mgmtRptComment";
	private boolean useGroupFilter = false;
	
	private SimpleDateFormat formatter = null;
	private Map<String,String> operationTypeForReportName = null;
	
	private Integer daysLimit = 7;
	
	public void setDaysLimit(Integer value) {
		this.daysLimit = value;
	}
	
	public Integer getDaysLimit() {
		return this.daysLimit;
	}
	
	public void setUseGroupFilter (boolean useGroupFilter) {
		this.useGroupFilter = useGroupFilter;
	}
	
	public void setReportNameDateFormat(String format){
		this.formatter = new SimpleDateFormat(format);
	}
	
	public void setOperationTypeForReportName(Map<String,String> values){
		this.operationTypeForReportName = values;
	}
	
	private Map getSelectedOperations(CommandBean commandBean) throws Exception {
		Object selected_operations = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_OPERATIONS);
		if(selected_operations == null){
			return null;
		}
		if(StringUtils.isBlank(selected_operations.toString())){
			return null;
		}
		
		String[] id_list = selected_operations.toString().split(",");
		Map result = new HashMap();
		for(String id: id_list){
			result.put(id.trim(), null);
		}
		return result;
	}

	private boolean isEmptyString(Object value){
		if(value == null) return true;
		return StringUtils.isBlank(value.toString());
	}

	protected String getReportDisplayNameOnCreation(ReportFiles reportFile, ReportTypes reportType, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) throws Exception {
		String file_name_pattern = null;
		if(reportType != null){
			file_name_pattern = reportType.getReportFilenamePattern();
		}
		
		if(StringUtils.isBlank(file_name_pattern)){
			return null;
		}

		// look for <distinct></distinct> or <combined></combined>, both must be specified if used
		if(file_name_pattern.indexOf("<distinct>") != -1){
			if(this.isCombinedReport(userSelection)){
				file_name_pattern = this.getFileNamePattern(file_name_pattern, "combined");
			}else{
				file_name_pattern = this.getFileNamePattern(file_name_pattern, "distinct");
			}
		}
		
		return this.formatDisplayName(file_name_pattern, reportFile, operation, daily, reportDaily, userSelection, reportJobParams);
	}
	
	private String getFileNamePattern(String pattern, String subType){
		int start = pattern.indexOf("<" + subType + ">");
		if(start == -1) return pattern;
		int end = pattern.indexOf("</" + subType + ">");
		if(end == -1) return pattern;
		return pattern.substring(start + subType.length() + 2, end);
	}

	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		boolean useLocalizedCutoffTimeLogic = false;
		Date reportDate = null;
		Date reportDateTo = null;
		Object dateParam = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_DATE);
		Boolean isLatest = false;
		
		if(isEmptyString(dateParam)) dateParam = LATEST_REPORT_FLAG;
		
		if(LATEST_REPORT_FLAG.equals(dateParam)){
			reportDate = new Date();
			useLocalizedCutoffTimeLogic = true;
			isLatest = true;
		}else{
			if (dateParam.toString().indexOf(",") > -1) {
				String[] dateRange = dateParam.toString().split(",");
				reportDate = CommonDateParser.parse(dateRange[0].toString());
				reportDateTo = CommonDateParser.parse(dateRange[1].toString());
				
			}else {
				reportDate = CommonDateParser.parse(dateParam.toString());
			}
			useLocalizedCutoffTimeLogic = false;
			isLatest = false;
			
		}  
		
		if (!isLatest) {
			if (reportDate !=null & reportDateTo !=null){	
				if (reportDateTo.before(reportDate)) {
					//throw new Exception("From date cannot be earlier than To date. Please re-select.");
					commandBean.getSystemMessage().addError("TO date must not be earlier than FROM date. Please re-select.", true);
					return null;
					
				}else {
					//Integer dayLimit = Integer.parseInt(commandBean.getRoot().getDynaAttr().get(MAX_ALLOWABLE_DAYS).toString());

					Long totalDays = (reportDateTo.getTime() - reportDate.getTime()) / 86400000;
					
					if (totalDays > this.getDaysLimit()) {
						//throw new Exception();
						commandBean.getSystemMessage().addError("Max allowable duration is " + this.getDaysLimit() +" days", true);
						return null;
					}
				}		
	
			}
		}

		boolean distinct_report = "true".equals(commandBean.getRoot().getDynaAttr().get(FIELD_DISTINCT_REPORT));
		
		
		Map selected_operations = this.getSelectedOperations(commandBean);
		if(selected_operations != null){
			List<UserSelectionSnapshot> userSelectionList = new ArrayList<UserSelectionSnapshot>();

			if (reportDate !=null & reportDateTo !=null){	
				// one day multi operation
				
				for (Date date: this.getDateListFromRange(reportDate, reportDateTo)){
					
					List<Daily> daily_list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From Daily where " +
						"(isDeleted is null or isDeleted = false) AND dayDate =:date order by dayDate, operationUid", "date", date);
					
					List<Daily> filtered_list = new ArrayList<Daily>();
					
					for(Daily daily: daily_list){
						if(selected_operations.containsKey(daily.getOperationUid())){
							filtered_list.add(daily);
							reportDate = daily.getDayDate();
						}
					}
					
					if(daily_list.size() > 0){
					
						if (filtered_list.size() > 0) {
							if (distinct_report) {
								List <UserSelectionSnapshot> currDailySelection = this.createDistinctMultipleDaysUserSelection(session.getUserName(), filtered_list, null);
								userSelectionList.addAll(currDailySelection);				
							}else {					
								UserSelectionSnapshot currDailySelection = this.createCombinedMultipleDaysUserSelection(session.getUserName(), filtered_list);
								userSelectionList.add(currDailySelection);
							}
						}
					}
				}
				
				if(distinct_report){
					return this.submitReportJob(userSelectionList, this.getParametersForReportSubmission(session, request, commandBean));				
				}else{
					return this.submitReportJob(userSelectionList, this.getParametersForReportSubmission(session, request, commandBean));
					
				}
				
				
			}else {
				
				List<Daily> filtered_list = new ArrayList<Daily>();
				
				List<Daily> daily_list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic);
				for(Daily daily: daily_list){
					if(selected_operations.containsKey(daily.getOperationUid())){
						filtered_list.add(daily);
						reportDate = daily.getDayDate();
					}
				}
				
				if(distinct_report){					
					return this.submitReportJob(this.createDistinctMultipleDaysUserSelection(session.getUserName(), filtered_list, null, commandBean), this.getParametersForReportSubmission(session, request, commandBean));
				}else{					
					return this.submitReportJob(this.createCombinedMultipleDaysUserSelection(session.getUserName(), filtered_list, commandBean), this.getParametersForReportSubmission(session, request, commandBean));
				}
			}
			

			//boolean distinct_report = "true".equals(commandBean.getRoot().getDynaAttr().get(FIELD_DISTINCT_REPORT));
			
			
		}else{
			throw new Exception("No data fit the selected criteria therefore unable to generate report.");
		}
	}

	private List<UserSelectionSnapshot> createMultipleDaysUserSelection(String userLogin, List<Daily> list) throws Exception {
		List<UserSelectionSnapshot> result = new ArrayList<UserSelectionSnapshot>();
		for(Daily daily: list){
			result.add(UserSelectionSnapshot.getInstanceForCustomDaily(userLogin, daily.getDailyUid()));
		}
		return result;
	}

	private Object createMultipleDaysUserSelectionFromScheduler(ManagementReportScheduleParams scheduleParam, boolean excludeTightHoleOperation) throws Exception {
		Date reportDate = new Date();
		boolean useLocalizedCutoffTimeLogic = true;
		List<Daily> list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic, excludeTightHoleOperation, scheduleParam.getOperationTypes());
		if(list.size() > 0){
			if(! scheduleParam.isDistinctReport()){
				UserSelectionSnapshot userSelection = this.createCombinedMultipleDaysUserSelection(scheduleParam.getReportUserUid(), list);
				if (scheduleParam.getOperationTypes()!=null && scheduleParam.getOperationTypes().size()>0) {
					userSelection.setCustomProperty(FIELD_SELECTED_OPERATION_TYPE, StringUtils.join(scheduleParam.getOperationTypes(),","));
				}
				return userSelection;
			}else{
				return this.createDistinctMultipleDaysUserSelection(scheduleParam.getReportUserUid(), list, scheduleParam.getOperationTypes());
			}
		}else{
			return null;
		}
	}

	private UserSelectionSnapshot createCombinedMultipleDaysUserSelection(String login, List<Daily> days) throws Exception {
		MultipleUserSelectionSnapshot userSelection = new MultipleUserSelectionSnapshot(UserSelectionSnapshot.getInstanceForCustomLogin(login));
		userSelection.addCopies(this.createMultipleDaysUserSelection(login, days));
		
		Date reportDate = null;
		for(Daily daily: days){
			if(reportDate == null){
				reportDate = daily.getDayDate();
			}else if(reportDate.compareTo(daily.getDayDate()) < 0){
				reportDate = daily.getDayDate();
			}
		}
		
		userSelection.setCustomProperty(MGT_REPORT_DATE, reportDate);
		userSelection.setCustomProperty(MGT_REPORT_COMBINED_REPORT, new Boolean(true));
		return userSelection;
	}
	
	private UserSelectionSnapshot createCombinedMultipleDaysUserSelection(String login, List<Daily> days, CommandBean commandBean) throws Exception {
		MultipleUserSelectionSnapshot userSelection = new MultipleUserSelectionSnapshot(UserSelectionSnapshot.getInstanceForCustomLogin(login));
		userSelection.addCopies(this.createMultipleDaysUserSelection(login, days));
		
		Date reportDate = null;
		for(Daily daily: days){
			if(reportDate == null){
				reportDate = daily.getDayDate();
			}else if(reportDate.compareTo(daily.getDayDate()) < 0){
				reportDate = daily.getDayDate();
			}
		}
		

		if(commandBean.getRoot().getDynaAttr().get(FIELD_COMMENT) != null) {
			String comment = commandBean.getRoot().getDynaAttr().get(FIELD_COMMENT).toString();
			userSelection.setCustomProperty(FIELD_COMMENT, comment);
		}else {
			String comment = "";
			userSelection.setCustomProperty(FIELD_COMMENT, comment);
		}
		

		userSelection.setCustomProperty(MGT_REPORT_DATE, reportDate);
		userSelection.setCustomProperty(MGT_REPORT_COMBINED_REPORT, new Boolean(true));
		return userSelection;
	}
	
	protected String parseDisplayNameParam(String key, ReportFiles reportFiles, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) {
		if(FILE_NAME_PATTERN_DATE.equalsIgnoreCase(key) || key.startsWith(FILE_NAME_PATTERN_DATE+FILE_NAME_PATTERN_DATE_SEPARATOR)){
			try{
				if (FILE_NAME_PATTERN_DATE.equalsIgnoreCase(key))
				{
					if(userSelection.getCustomProperties() != null)
					{
						return null2EmptyString(ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(userSelection.getCustomProperties().get(MGT_REPORT_DATE)));
					}
					else
					{
						return null2EmptyString(ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(new Date()));
					}
				}else if (key.startsWith(FILE_NAME_PATTERN_DATE+FILE_NAME_PATTERN_DATE_SEPARATOR))
				{
					if(userSelection.getCustomProperties() != null)
					{
						String strFormat = key.substring(FILE_NAME_PATTERN_DATE.length()+FILE_NAME_PATTERN_DATE_SEPARATOR.length());
						DateFormat dateFormat = new SimpleDateFormat(strFormat);
						return null2EmptyString(dateFormat.format(userSelection.getCustomProperties().get(MGT_REPORT_DATE)));
					}
					else
					{
						String strFormat = key.substring(FILE_NAME_PATTERN_DATE.length()+FILE_NAME_PATTERN_DATE_SEPARATOR.length());
						DateFormat dateFormat = new SimpleDateFormat(strFormat);
						return null2EmptyString(dateFormat.format(new Date()));
					}
				}else{
					return "";
				}
			}catch(Exception e){
				Log logger = LogFactory.getLog(this.getClass());
				logger.error(e.getMessage(), e);
				return null;
			}
		}else if("MgtReportOperationType".equalsIgnoreCase(key)){
			if(operation != null){
				if(this.operationTypeForReportName != null){
					if(this.operationTypeForReportName.containsKey(operation.getOperationCode())){
						return this.operationTypeForReportName.get(operation.getOperationCode());
					}else{
						return operation.getOperationCode();
					}
				}else{
					return operation.getOperationCode();
				}
			}else{
				return "";
			}
		}else {
			return super.parseDisplayNameParam(key, reportFiles, operation, daily, reportDaily, userSelection, reportJobParams);
		}
	}

	private List<UserSelectionSnapshot> createDistinctMultipleDaysUserSelection(String login, List<Daily> days, List<String> operationTypes) throws Exception {
		List<UserSelectionSnapshot> result = this.createMultipleDaysUserSelection(login, days);
		for(UserSelectionSnapshot userSelection : result){
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			userSelection.setCustomProperty(MGT_REPORT_DATE, (daily != null ? daily.getDayDate() : new Date()));
			userSelection.setCustomProperty(MGT_REPORT_COMBINED_REPORT, new Boolean(false));
			if (operationTypes!=null && operationTypes.size()>0) {
				userSelection.setCustomProperty(FIELD_SELECTED_OPERATION_TYPE, StringUtils.join(operationTypes,","));
			}
		}
		return result;
	}
	
	private List<UserSelectionSnapshot> createDistinctMultipleDaysUserSelection(String login, List<Daily> days, List<String> operationTypes, CommandBean commandBean) throws Exception {
		List<UserSelectionSnapshot> result = this.createMultipleDaysUserSelection(login, days);
		for(UserSelectionSnapshot userSelection : result){
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(commandBean.getRoot().getDynaAttr().get(FIELD_COMMENT) != null) {
				String comment = commandBean.getRoot().getDynaAttr().get(FIELD_COMMENT).toString();
				userSelection.setCustomProperty(FIELD_COMMENT, comment);
			}else {
				String comment = "";
				userSelection.setCustomProperty(FIELD_COMMENT, comment);
			}
			
			userSelection.setCustomProperty(MGT_REPORT_DATE, (daily != null ? daily.getDayDate() : new Date()));
			userSelection.setCustomProperty(MGT_REPORT_COMBINED_REPORT, new Boolean(false));
			if (operationTypes!=null && operationTypes.size()>0) {
				userSelection.setCustomProperty(FIELD_SELECTED_OPERATION_TYPE, StringUtils.join(operationTypes,","));
			}
		}
		return result;
	}

	public ReportJobParams generateManagementReportOnCurrentSystemDatetime(String loginUser, ReportAutoEmail reportAutoEmail) throws Exception {
		ManagementReportScheduleParams params = new ManagementReportScheduleParams();
		params.setDistinctReport(false);
		params.setReportUserUid(loginUser);
		return this.generateManagementReportOnCurrentSystemDatetime(params, reportAutoEmail);
	}
	
	public ReportJobParams generateManagementReportOnCurrentSystemDatetime(ManagementReportScheduleParams scheduleParam, ReportAutoEmail reportAutoEmail) throws Exception {
		ReportJobParams reportJobParams = null;
		// return null if no day selected
		Object userSelection = this.createMultipleDaysUserSelectionFromScheduler(scheduleParam, true);
		if(userSelection == null){
			// no day selected, don't generate report (and send empty email);
			return null;
		}

		//Get the current date
		Date reportDate = new Date();
		boolean useLocalizedCutoffTimeLogic = true;
		//Retrieve all the daily records based on the current date
		List <Daily> list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic, true, scheduleParam.getOperationTypes());
		//Get the current daily day date
		Date currentReportDate = null;
		for(Daily daily: list){
			if(currentReportDate == null){
				currentReportDate = daily.getDayDate();
			}else if(currentReportDate.compareTo(daily.getDayDate()) < 0){
				currentReportDate = daily.getDayDate();
			}
		}
		//Get the operationUid and operationName of those operations that have current daily day date
		List OperationNameList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select DISTINCT o.operationUid, o.operationName " +
				"From Operation o, Daily d WHERE (o.isDeleted is null or o.isDeleted = false) AND " +
				"(d.isDeleted is null or d.isDeleted = false) AND " +
				((scheduleParam.getOperationTypes()!=null && scheduleParam.getOperationTypes().size()>0)?" o.operationCode in ('"+ StringUtils.join(scheduleParam.getOperationTypes().toArray(), "','") +"') AND ":"") + 
				"(o.operationUid=d.operationUid) AND (o.tightHole is null or o.tightHole = false) AND " +
				"d.dayDate =:reportDate", new String[] {"reportDate"}, new Object[]{currentReportDate});	
		
		//Map the operationName key with it corresponding value
		Map <String, Object> mapOperationNameList = new HashMap <String, Object>();
		Map <String, Object> mapWellNameList = new HashMap <String, Object>();
		
		if (OperationNameList.size() > 0){
			for(Iterator i = OperationNameList.iterator(); i.hasNext(); ){
				Map <String, Object> operationItem = new HashMap <String, Object>();
				Object[] obj_array = (Object[]) i.next();
				if (obj_array[1] != null && StringUtils.isNotBlank(obj_array[1].toString())){
						operationItem.put("operationName", obj_array[1].toString());
					}
						
				//Map the operationUid key with the operationName corresponding value for each operation 
				if(operationItem.size() > 0)
				{
					if (obj_array[0] != null && StringUtils.isNotBlank(obj_array[0].toString())){
						
						Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, obj_array[0].toString());
						Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, operation.getWellUid());			
						Map <String, Object> wellItem = new HashMap <String, Object>();
						
						wellItem.put("wellName", well.getWellName());
						mapOperationNameList.put(obj_array[0].toString(), operationItem);
						mapWellNameList.put(well.getWellUid(), wellItem);
					}
				
				}
			}
		}
		
		String emailSubject = reportAutoEmail.getEmailSubject();
		if(!StringUtils.isBlank(emailSubject)) {
			if(emailSubject.contains("<REPORT DATE>")) {
				String formattedReportDate = new SimpleDateFormat("dd.MM.yy").format(currentReportDate);
				reportAutoEmail.setEmailSubject(emailSubject.replace("<REPORT DATE>", formattedReportDate));
			}
		}
		
		ReportJobParams params = new ReportJobParams();
		params.setUserSessionReportJob(false);
		params.setReportAutoEmail(reportAutoEmail);
		params.setPaperSize(super.defaultPaperSize);	
		//Put in the email content mapping value in the params
		params.putEmailContentMapping("operationNameList", mapOperationNameList);
		params.putEmailContentMapping("wellNameList", mapWellNameList);
		if(! scheduleParam.isDistinctReport()){
			reportJobParams= this.submitReportJob((UserSelectionSnapshot) userSelection, params);
		}else{
			reportJobParams = this.submitReportJob((List<UserSelectionSnapshot>) userSelection, params);
		}
		return reportJobParams;
	}

	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		return this.getOutputFileInLocalPath(userContext, paperSize, "en");
	}
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize, String language) throws Exception{
		String report_type = this.getReportType(userContext.getUserSelection());
		if(this.isCombinedReport(userContext.getUserSelection())){
			String operation_types = (String) userContext.getUserSelection().getCustomProperty(FIELD_SELECTED_OPERATION_TYPE);
			operation_types = StringUtils.isNotBlank(operation_types)?operation_types.replaceAll(",", "_") + " ":"";
			String name = report_type + " " + operation_types + (userContext.getUserSelection().getCustomProperty(MGT_REPORT_DATE) != null? this.formatter.format(userContext.getUserSelection().getCustomProperty(MGT_REPORT_DATE)):this.formatter.format(new Date())) + 
				(StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
			
			return report_type + "/" + ReportUtils.replaceInvalidCharactersInFileName(name);
		}else{
			Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
			if(operation == null) throw new Exception("Operation must be selected");
			String name = operation.getOperationName() + " " + (userContext.getUserSelection().getCustomProperty(MGT_REPORT_DATE) != null? this.formatter.format(userContext.getUserSelection().getCustomProperty(MGT_REPORT_DATE)):this.formatter.format(new Date())) + 
				(StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
			
			return report_type + "/" + operation.getOperationUid() + "/" + ReportUtils.replaceInvalidCharactersInFileName(name);
		}
	}
	
	private boolean isCombinedReport(UserSelectionSnapshot userSelection){
		Object value = userSelection.getCustomProperty(MGT_REPORT_COMBINED_REPORT);
		if(value instanceof Boolean) return (Boolean) value;
		if(value == null) return true;
		return false;
	}

	protected void beforeSaveReportEntry(JobContext jobContext, ReportFiles report) throws Exception{
		if(this.isCombinedReport(jobContext.getUserContext().getUserSelection())){
			/*
			String delimiter = "";
			StringBuilder sb = new StringBuilder();
			MultipleUserSelectionSnapshot multipleUserSelection = (MultipleUserSelectionSnapshot) jobContext.getUserContext().getUserSelection();
			for (UserSelectionSnapshot userSelection : multipleUserSelection.getCopies()) {
				sb.append(delimiter);
				sb.append(userSelection.getOperationUid());
				delimiter = "\t";
			}
			if (sb.length() > 0) {
				report.setReportOperationUid(sb.toString());
			} else {
				report.setReportOperationUid(null);
			}
			*/
			report.setReportOperationUid(null);
			report.setReportDayDate((Date) jobContext.getUserContext().getUserSelection().getCustomProperty(MGT_REPORT_DATE));
		}
	}
	/*
	public List<Object> loadReportOutputFiles(UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		List<Object> accessibleReportFiles = new ArrayList<Object>();
		UserSession userSession = UserSession.getInstance(request);
		
		for (Object obj : super.loadReportOutputFiles(userSelection, request)) {
			ReportOutputFile reportOutputFile = (ReportOutputFile) obj;
			if (StringUtils.isNotBlank(reportOutputFile.getReportOperationUid())) {
				boolean forbidden = false;
				for (String operationUid : reportOutputFile.getReportOperationUid().split("\t")) {
					if (!userSession.getCachedAllAccessibleOperations().containsKey(operationUid)) {
						forbidden = true;
						break;
					}
				}
				if (!forbidden) {
					accessibleReportFiles.add(obj);
				}
			} else {
				// the operationUid of old combined mgt report is null
				accessibleReportFiles.add(obj);
			}
		}
		return accessibleReportFiles;
	}
	*/
	protected String getReportDisplayName(ReportFiles reportFile, Operation operation, UserSelectionSnapshot userSelection) throws Exception{
		if(StringUtils.isNotBlank(reportFile.getDisplayName())) return reportFile.getDisplayName();
		if(operation == null){
			//change file name to auto follow report type
			return this.getReportType() + " " + this.formatter.format(reportFile.getReportDayDate());
		}else{
			return super.getReportDisplayName(reportFile, operation, userSelection);
		}
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("flexMgtReportLoadOperationList".equals(invocationKey)) {
			boolean useLocalizedCutoffTimeLogic = false;
			Date reportDate = null;
			Date reportDateTo = null;
			
			String userSessionGroupUid = UserSession.getInstance(request).getCurrentGroupUid();
			String value = request.getParameter("reportDate");
			String opTypes = request.getParameter("operationType");
			String[] operationCodes = {};
			if (StringUtils.isNotBlank(opTypes)) operationCodes = opTypes.split(",");
			
			if(LATEST_REPORT_FLAG.equals(value)){
				reportDate = new Date();
				useLocalizedCutoffTimeLogic = true;
			}else{
				
				if (value.indexOf(",") > -1) {
					String[] dateRange = value.split(",");
					reportDate = CommonDateParser.parse(dateRange[0]);
					reportDateTo = CommonDateParser.parse(dateRange[1]);
					
				}else{
					reportDate = CommonDateParser.parse(value);
				}
				
				useLocalizedCutoffTimeLogic = false;
			}
			
			if (reportDate !=null && reportDateTo !=null) {
				List<String> list = this.getOperationFromDateRange(reportDate, reportDateTo, operationCodes);
				
				SimpleXmlWriter writer = new SimpleXmlWriter(response);
				writer.startElement("root");
				
				for (String operation : list)
				{
					String operationUid = String.valueOf(operation);
					boolean includeOperation = ApplicationUtils.getConfiguredInstance().getCachedAllOperations().containsKey(operationUid);
					if(this.useGroupFilter){
						Operation o = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
						includeOperation = userSessionGroupUid.equalsIgnoreCase(o.getGroupUid());
					}
	
					if (includeOperation)
					{
						writer.startElement("Operation");
						writer.addElement("operationUid", operationUid);
						String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(userSessionGroupUid, operationUid);
						writer.addElement("operationName", operationName);
						writer.endElement();
					
					}
				}
				writer.endElement();
				writer.close();
				
			}else {
				List<Daily> list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic);
			
				//get all the user allowed to access operation
				Operations accessibleOperations = UserSession.getInstance(request).getCachedAllAccessibleOperations();
				
				SimpleXmlWriter writer = new SimpleXmlWriter(response);
				writer.startElement("root");
				for(Daily rec:list){
					Operation o = ApplicationUtils.getConfiguredInstance().getCachedOperation(rec.getOperationUid());
					if (o==null) continue;
					
					if (operationCodes.length>0) { //skip operation if the operation code not in selected list 
						Boolean skip = true;
						for (String opCode : operationCodes) {
							if (opCode.equals(o.getOperationCode())) {
								skip = false;
								break;
							}
						}
						if (skip) continue;
					}
					
					//check if operation is accessible or not
					boolean includeOperation = accessibleOperations.containsKey(rec.getOperationUid());
					if(this.useGroupFilter){
						includeOperation = userSessionGroupUid.equalsIgnoreCase(o.getGroupUid());
					}
					if (includeOperation){
						writer.startElement("Operation");
						writer.addElement("operationUid", rec.getOperationUid());
						String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(userSessionGroupUid, rec.getOperationUid());
						writer.addElement("operationName", operationName);
						writer.endElement();
						
					}				
				}
				writer.endElement();
				writer.close();
				
			}
			
			
			
		} else {
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);
		}
	}
	private List<Date> getDateListFromRange(Date start, Date end)	
	{
		List<Date> dates = new ArrayList<Date>();
		Date count = start;
		while (count.before(end) || count.equals(end))
		{
			dates.add(count);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(count);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			count = calendar.getTime();
		}
		return dates;
	}
	
	private List <String> getOperationFromDateRange(Date start, Date end, String[] operationCodes) throws Exception{
		List<String> list = null;
		
		if (start !=null && end !=null) {
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select DISTINCT o.operationUid " +
					"From Operation o, Daily d WHERE (o.isDeleted is null or o.isDeleted = false) AND " +
					"(d.isDeleted is null or d.isDeleted = false) AND " +
					(operationCodes.length>0?" operationCode in ('"+ StringUtils.join(operationCodes, "','") +"') AND ":"") + 
					"(o.operationUid=d.operationUid) AND (o.tightHole is null or o.tightHole = false) AND " +
					"d.dayDate >=:startDate AND d.dayDate<=:endDate", new String[] {"startDate","endDate"}, new Object[]{start, end});
		}
		
		return list;

	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		super.afterDataLoaded(commandBean, root, userSelection, request);
		commandBean.getRoot().getDynaAttr().put("daysLimit", this.getDaysLimit());
		
		if (userSelection!=null && userSelection.getOperationType()!=null){
			commandBean.getRoot().getDynaAttr().put("currentOperationType", userSelection.getOperationType());
		}
	}

}
