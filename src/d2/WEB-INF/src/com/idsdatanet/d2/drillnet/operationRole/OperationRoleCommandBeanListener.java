package com.idsdatanet.d2.drillnet.operationRole;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.OperationRole;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class OperationRoleCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener{
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(OperationRole.class)){
				if("pobMasterUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					OperationRole thisOperationRole = (OperationRole) targetCommandBeanTreeNode.getData();
					
					//Get the designation from pob_master table
					String pobMasterUid = thisOperationRole.getPobMasterUid();
					if (StringUtils.isNotBlank(pobMasterUid)) {
						List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select designation, companyName, officePhone, mobilePhone, homePhone, faxNo, emailAddress from PobMaster where (isDeleted = false or isDeleted is null) and pobMasterUid = :pobMasterUid", "pobMasterUid", pobMasterUid);
						if (lstResult.size() > 0) 
							{
								Object[] thisResult = (Object[]) lstResult.get(0);
								if(thisResult != null)
								{	
									if (thisResult[0] != null) thisOperationRole.setRole(thisResult[0].toString());
									if (thisResult[1] != null) targetCommandBeanTreeNode.getDynaAttr().put("companyName", thisResult[1].toString());
									if (thisResult[2] != null) targetCommandBeanTreeNode.getDynaAttr().put("officePhone", thisResult[2].toString());
									if (thisResult[3] != null) targetCommandBeanTreeNode.getDynaAttr().put("mobilePhone", thisResult[3].toString());
									if (thisResult[4] != null) targetCommandBeanTreeNode.getDynaAttr().put("homePhone", thisResult[4].toString());
									if (thisResult[5] != null) targetCommandBeanTreeNode.getDynaAttr().put("faxNo", thisResult[5].toString());
									if (thisResult[6] != null) targetCommandBeanTreeNode.getDynaAttr().put("emailAddress", thisResult[6].toString());
								}
								
							}
					}
				}
			}
		}
	}
}
