package com.idsdatanet.d2.drillnet.rigpump;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class RigPumpCommandBeanListener extends EmptyCommandBeanListener  {
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		
		if (userSelection.getDailyUid()!=null)
		{
		commandBean.getSystemMessage().addInfo("Rig Pump information can also be added/edited on the Rig Information screen.");
		}
	}
}