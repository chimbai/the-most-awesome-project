package com.idsdatanet.d2.drillnet.commonLookup;

import java.net.URLEncoder;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.CommonLookupDepot;
import com.idsdatanet.d2.core.model.CommonLookupType;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class CommonLookupUtil {

	public static void updateCommonLookupDepotString(String commonLookupUid) throws Exception
	{
		CommonLookup cl = (CommonLookup) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(CommonLookup.class, commonLookupUid);
		if (cl!=null)
		{
			List<CommonLookupDepot> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From CommonLookupDepot WHERE (isDeleted is null or isDeleted = false) and commonLookupUid=:commonLookupUid", "commonLookupUid", commonLookupUid);
			String depotString = null;
			if (result.size()>0){ 
				for (CommonLookupDepot cld : result)
				{
					if (StringUtils.isNotBlank(cld.getDepotType()) && StringUtils.isNotBlank(cld.getDepotValue()))
					{
						if (depotString == null)
							depotString = "?"+cld.getDepotType()+"="+cld.getDepotValue();
						else
							depotString += "&"+cld.getDepotType()+"="+cld.getDepotValue();
					}
				}
				depotString = depotString.replace(" ", "%20");
			}
			cl.setDepotString(depotString);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cl);
		}
	}
	
	//get common lookup based on lookup key 
	public static String checkForCommonLookupKey(String lookupKey) throws Exception{		
		if(isCommonLookupType(lookupKey)){
			//baseCommonLookupDefinition
			String table = "CommonLookup";
			String key = "shortCode";
			String value = "lookupLabel";
			String order = "lookupLabel";
			if ("casingsection.casing_size".equals(lookupKey) || "holeSize".equals(lookupKey) || "casingsection.casing_holesize".equals(lookupKey) || "linerSize".equals(lookupKey)){
				order = "shortCode";
			}
			String parent = "parentReferenceKey";
			String cache = "false";
			String depotstringfield = "depotString";
			String delimeter = null;
			String format = null;
			
			String filter = " lookupTypeSelection='"+lookupKey+"'";
			String lookup_uri = "db://" + urlEncode(table) + "?key=" + urlEncode(key) + "&value=" + urlEncode(value) + "&cache=" + urlEncode(cache) + "&order=" + urlEncode(order) + "&filter=" + urlEncode(filter) + "&parent=" + urlEncode(parent) + "&delimeter=" + urlEncode(delimeter) + "&format=" + urlEncode(format) + "&depotstring=" + urlEncode(depotstringfield);
			return lookup_uri ;	
			
		}else{	//normal xml lookup
			return lookupKey;
		}		
		
	}
	
	//check is valid defined common lookup
	public static Boolean isCommonLookupType(String lookupTypeSelection) throws Exception{
		String queryString = "From CommonLookupType where (isDeleted is null or isDeleted = 0) and lookupTypeSelection =:lookupTypeSelection ";
    	List <CommonLookupType> lstSummary = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"lookupTypeSelection"}, new Object[] {lookupTypeSelection});
		
    	if(lstSummary.size()>0){
    		return true;			
    	} 
		
		return false;
	}
	
	protected static String urlEncode(String value) throws Exception {
		if(value == null) return "";
		return URLEncoder.encode(value, "UTF-8");
	}
	
}
