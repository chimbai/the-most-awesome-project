package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.report.ReportOption;

public class ExtendedNPTReportDataGenerator extends NPTReportDataGenerator{

	
	private Boolean useRootCause= false;
	
	protected void generateActualData(UserContext userContext, Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		List<NPTSummary> includedList = this.shouldIncludeWell(userContext, startDate, endDate);
		this.includeQueryData(includedList, reportDataNode, userContext.getUserSelection(), startDate, endDate);
	}
	
	private List<NPTSummary> generatedata(UserContext userContext, Date startDate, Date endDate, String includeWellUid) throws Exception
	{

		String[] paramNames = {"startDate","endDate",BHIQueryUtils.getQueryParamName(getUseCountry()),"nptSubject","companyName","wellUid"};
		Object[] values = {startDate,endDate,BHIQueryUtils.getQueryParamValue(userContext, getUseCountry()),BHIQueryUtils.getReportOptionValue(userContext,BHIQueryUtils.CONSTANT_NPT_CATEGORY),BHIQueryUtils.getReportOptionValue(userContext,BHIQueryUtils.CONSTANT_COMPANY),includeWellUid};
		String sql = "SELECT DISTINCT w.wellUid,w.wellName, wb.wellboreUid, wb.wellboreName, a.operationUid, w.country, a.lookupCompanyUid, d.dailyUid, d.dayDate, "+(this.getUseRootCause()?"a.rootCauseCode":"a.nptSubject")+", "+BHIQueryUtils.getQuerySQL(getUseCountry())+", sum(a.activityDuration) as duration"+
			" FROM Well w,Wellbore wb, Operation o, Activity a, Daily d "+ 
			" Where w.wellUid=wb.wellUid and wb.wellboreUid=o.wellboreUid"+
			" and d.operationUid=o.operationUid"+ 
			" and a.dailyUid=d.dailyUid"+
			" and (a.isDeleted is null or a.isDeleted = False)"+
			" and (d.isDeleted is null or d.isDeleted = False)"+
			" and (w.isDeleted is null or w.isDeleted = false) and (wb.isDeleted is null or wb.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false)"+
			" and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) "+
			" and (a.internalClassCode='TP' or a.internalClassCode='TU')"+ 
			" and (a.startDatetime>=:startDate and a.endDatetime<=:endDate)"+ 
			" and (a.lookupCompanyUid in (:companyName))"+
			" and ("+(this.getUseRootCause()?"a.rootCauseCode":"a.nptSubject")+" in (:nptSubject))"+
			BHIQueryUtils.getQueryFilter(getUseCountry())+
			" and w.wellUid=:wellUid"+
			" group by o.operationUid,"+BHIQueryUtils.getQuerySQL(getUseCountry())+", d.dailyUid ,a.lookupCompanyUid, "+(this.getUseRootCause()?"a.rootCauseCode":"a.nptSubject")+
			" order by o.operationUid,"+BHIQueryUtils.getQuerySQL(getUseCountry())+", d.dailyUid ,a.lookupCompanyUid,  "+(this.getUseRootCause()?"a.rootCauseCode":"a.nptSubject");
		
		List<NPTSummary> includedList = new ArrayList<NPTSummary>();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, values);
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
		for (Object[] item :result)
		{		
			NPTSummary npt = new NPTSummary((String)item[0],(String)item[4], (String)item[5], (String)item[6], null,null,(String)item[7],(Date)item[8]);
			npt.setNptSubject((String)item[9]);
			BHIQueryUtils.setCountryOrArea(npt, getUseCountry(), (String)item[10]);
			
			npt.setWellName((String)item[1]);
			npt.setWellboreUid((String)item[2]);
			npt.setWellboreName((String)item[3]);
			npt.setDuration(Double.parseDouble(item[11].toString()));
			thisConverter.setBaseValue(npt.getDuration());
			npt.setDuration(thisConverter.getConvertedValue());
			
			includedList.add(npt);
		}
		return includedList;
		
	}
	
	private List<NPTSummary> shouldIncludeWell(UserContext userContext, Date startDate, Date endDate) throws Exception
	{
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		ReportOption wells = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL):null;
		List<NPTSummary> result = new ArrayList<NPTSummary>();
		for (String wellUid : wells.getValues())
		{
			result.addAll(this.generatedata(userContext, startDate, endDate,wellUid));
		}
		return result;
	}

	private void includeQueryData(List<NPTSummary> nptSummary, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception
	{
		ReportOption options = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_QUERIES)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_QUERIES):null;
		if (options.getValues().contains("Q1"))
			this.includeNPTHours(nptSummary, node, userSelection, start, end);
		if (options.getValues().contains("Q2"))
			this.includeNPTHoursPerCategory(nptSummary, node, userSelection, start, end);
		if (options.getValues().contains("Q3"))
			this.includeNPTHoursPerArea(nptSummary, node, userSelection, start, end);
		if (options.getValues().contains("Q4")|| options.getValues().contains("Q5") )
			this.includeNPTHoursPerCategoryPerArea(nptSummary, node, userSelection, start, end);
	}
	
	private void includeNPTHoursPerCategoryPerArea(
			List<NPTSummary> nptSummary, ReportDataNode node,
			UserSelectionSnapshot userSelection, Date start, Date end) throws Exception {
		ReportDataNode nodeChild = node.addChild("NPTHoursPerCategoryPerArea"); 
		ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;		
		ReportOption subs = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_NPT_CATEGORY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_NPT_CATEGORY):null;
		ReportOption ops = this.getCountryOrAreaOption(userSelection);
		
		if (ops!=null){
			for (String op : ops.getValues()){
				if (subs!=null){
					for (String sub : subs.getValues()){
						if (cos!=null){
							for (String coName : cos.getValues()){				
								Double duration =0.00;
								
								for (NPTSummary npt : nptSummary)
								{
									if (npt.getNptSubject().equalsIgnoreCase(sub) && npt.getLookupCompanyUid().equalsIgnoreCase(coName)&& (op.equalsIgnoreCase((this.getUseCountry()?npt.getCountry():npt.getOperationArea()))))
										duration += npt.getDuration();
								}
								ReportDataNode catChild = nodeChild.addChild("data");
								catChild.addProperty("lookupCompanyUid", coName);
								catChild.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(coName).toString());
								this.writeCountryOrAreaProperty(catChild, ops, op);
								catChild.addProperty("nptSubject", sub);
								catChild.addProperty("nptSubject_lookupLabel",subs.getValue(sub).toString());
								catChild.addProperty("duration", duration.toString());
							}
						}
					}
				}
			}
		}
	}
	
	private ReportOption getCountryOrAreaOption(UserSelectionSnapshot userSelection)
	{
		if (this.getUseCountry())
			return userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COUNTRY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COUNTRY):null;
		return userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_OPERATION_AREA)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_OPERATION_AREA):null;
	}
	private void writeCountryOrAreaProperty(ReportDataNode node, ReportOption ops, String op)
	{
		if (this.getUseCountry())
		{
			node.addProperty("country", op);
			if (ops.getValue(op)!=null)
				node.addProperty("country_lookupLabel",ops.getValue(op).toString());
			else
				node.addProperty("country_lookupLabel",op);
		}else{
			node.addProperty("operationArea", op);
			if (ops.getValue(op)!=null)
				node.addProperty("operationArea_lookupLabel",ops.getValue(op).toString());
			else
				node.addProperty("operationArea_lookupLabel",op);
		}
	}
	
	private void includeNPTHoursPerArea(List<NPTSummary> nptSummary, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception {
		ReportDataNode nodeChild = node.addChild("NPTHoursPerArea"); 
		ReportOption cats = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY):null;
		ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;
		ReportOption ops = this.getCountryOrAreaOption(userSelection);
		
		if (cats!=null){
			for (String cat : cats.getValues()){
				ReportDataNode child = nodeChild.addChild(cats.getValue(cat).toString().replace(" ", "_"));
				if (ops!=null){
					for (String op : ops.getValues()){
						if (cos!=null){
							for (String coName : cos.getValues()){
								List<Date> dates = DateRangeUtils.getDateList(start, end, false, cat);
								for (Date date: dates){
									Double duration =0.00;
									String formatLabel = DateRangeUtils.formatDateLabel(date, cat,true);
									for (NPTSummary npt : nptSummary)
									{
										String formatLabelData = DateRangeUtils.formatDateLabel(npt.getDayDate(), cat,true);
										if (npt.getLookupCompanyUid().equalsIgnoreCase(coName)&& (op.equalsIgnoreCase((this.getUseCountry()?npt.getCountry():npt.getOperationArea()))) && formatLabel.equalsIgnoreCase(formatLabelData))
											duration += npt.getDuration();
									}
									ReportDataNode catChild = child.addChild("data");
									catChild.addProperty("lookupCompanyUid", coName);
									catChild.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(coName).toString());
									this.writeCountryOrAreaProperty(catChild, ops, op);
									catChild.addProperty("label", formatLabel);
									catChild.addProperty("duration", duration.toString());
								}
							}
						}
					}
				}
			}
		}
	}
	private void includeNPTHoursPerCategory(List<NPTSummary> nptSummary, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception {
		ReportDataNode nodeChild = node.addChild("NPTHoursPerCategory"); 
		ReportOption cats = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY):null;
		ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;
		ReportOption subs = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_NPT_CATEGORY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_NPT_CATEGORY):null;
		
		if (cats!=null){
			for (String cat : cats.getValues()){
				ReportDataNode child = nodeChild.addChild(cats.getValue(cat).toString().replace(" ", "_"));
				if (subs!=null){
					for (String sub : subs.getValues()){
						if (cos!=null){
							for (String coName : cos.getValues()){
								List<Date> dates = DateRangeUtils.getDateList(start, end, false, cat);
								for (Date date: dates){
									Double duration =0.00;
									String formatLabel = DateRangeUtils.formatDateLabel(date, cat,true);
									for (NPTSummary npt : nptSummary)
									{
										String formatLabelData = DateRangeUtils.formatDateLabel(npt.getDayDate(), cat,true);
										if (npt.getLookupCompanyUid().equalsIgnoreCase(coName)&& npt.getNptSubject().equalsIgnoreCase(sub) && formatLabel.equalsIgnoreCase(formatLabelData))
											duration += npt.getDuration();
									}
									ReportDataNode catChild = child.addChild("data");
									catChild.addProperty("lookupCompanyUid", coName);
									catChild.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(coName).toString());
									catChild.addProperty("nptSubject", sub);
									catChild.addProperty("nptSubject_lookupLabel",subs.getValue(sub).toString());
									catChild.addProperty("label", formatLabel);
									catChild.addProperty("duration", duration.toString());
								}
							}
						}
					}
				}
			}
		}
	}
	private void includeNPTHours(List<NPTSummary> nptSummary, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception {	
		
		ReportDataNode nodeChild = node.addChild("NPTHours"); 
		ReportOption cats = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY):null;
		ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;
		
		if (cats!=null){
			for (String cat : cats.getValues()){
				ReportDataNode child = nodeChild.addChild(cats.getValue(cat).toString().replace(" ", "_"));
				if (cos!=null){
					for (String coName : cos.getValues()){
						List<Date> dates = DateRangeUtils.getDateList(start, end, false, cat);
						for (Date date: dates){
							Double duration =0.00;
							String formatLabel = DateRangeUtils.formatDateLabel(date, cat,true);
							for (NPTSummary npt : nptSummary)
							{
								String formatLabelData = DateRangeUtils.formatDateLabel(npt.getDayDate(), cat,true);
								if (npt.getLookupCompanyUid().equalsIgnoreCase(coName) && formatLabel.equalsIgnoreCase(formatLabelData))
									duration += npt.getDuration();
							}
							ReportDataNode catChild = child.addChild("data");
							catChild.addProperty("lookupCompanyUid", coName);
							catChild.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(coName).toString());
							catChild.addProperty("label", formatLabel);
							catChild.addProperty("duration", duration.toString());
						}
					}
				}
			}
		}
	}
	public void setUseRootCause(Boolean useRootCause) {
		this.useRootCause = useRootCause;
	}
	public Boolean getUseRootCause() {
		return useRootCause;
	}

}
