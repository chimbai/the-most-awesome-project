package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.report.ReportOption;

public class NPTLessonLearnedReportDataGenerator extends NPTReportDataGenerator{


	
	protected void generateActualData(UserContext userContext, Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		List<NPTLessonTicket> includedList = this.shouldIncludeWell(userContext, startDate, endDate);
		this.includeQueryData(includedList, reportDataNode, userContext.getUserSelection(), startDate, endDate);
	}
	
	private List<NPTLessonTicket> generatedata(UserContext userContext, Date startDate, Date endDate, String includeWellUid) throws Exception
	{

		String[] paramNames = {"startDate","endDate","companyNPTClassCode","otherNPTClassCode","field","wellUid"};
		Object[] values = {startDate,endDate,this.getCompanyNPTClassCode(),this.getOtherNPTClassCode(),BHIQueryUtils.getReportOptionValue(userContext,BHIQueryUtils.CONSTANT_FIELD),includeWellUid};
		String sql = "SELECT w.wellUid,w.wellName, o.operationName, w.field, w.country, o.opCo, a.sections, t.lessonTitle, t.descrEventRisk, t.rootCauseDescription, t.descrLesson, a.nptMainCategory, a.nptSubCategory, t.eventProbability, t.eventImpact, t.descrPostevent, t.descrContigency, a.incidentLevel"+		
			" FROM Well w,Wellbore wb, Operation o, Activity a, Daily d, ActivityLessonTicketLink l, LessonTicket t"+ 
			" Where w.wellUid=wb.wellUid and wb.wellboreUid=o.wellboreUid"+
			" and d.operationUid=o.operationUid"+ 
			" and a.dailyUid=d.dailyUid"+
			" and a.activityUid=l.activityUid"+
			" and t.lessonTicketUid=l.lessonTicketUid"+		
			" and (a.isDeleted is null or a.isDeleted = False)"+
			" and (d.isDeleted is null or d.isDeleted = False)"+
			" and (w.isDeleted is null or w.isDeleted = false) and (wb.isDeleted is null or wb.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false)"+
			" and (l.isDeleted is null or l.isDeleted = False)"+			
			" and (t.isDeleted is null or t.isDeleted = False)"+	
			" and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) "+
			" and (a.classCode=:companyNPTClassCode or a.classCode=:otherNPTClassCode)"+ 
			" and (a.startDatetime>=:startDate and a.endDatetime<=:endDate)"+ 						
			" and w.wellUid=:wellUid"+
			" and (w.field in (:field))"+			
			" order by w.field, w.wellUid, o.operationUid, a.startDatetime";
		
		List<NPTLessonTicket> includedList = new ArrayList<NPTLessonTicket>();		
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, values);
		
		for (Object[] item :result)
		{
			NPTLessonTicket npt = new NPTLessonTicket((String)item[0],null, (String)item[4], null, null,null);			
			npt.setWellName((String)item[1]);
			npt.setOperationName((String)item[2]);		
			npt.setField((String)item[3]);	
			npt.setOpCo((String)item[5]);	
			npt.setSections((String)item[6]);
			npt.setLessonTitle(String.valueOf(item[7]));				
			npt.setDescrEventRisk((String)item[8]);
			npt.setRootCauseDescription((String)item[9]);
			npt.setDescrLesson((String)item[10]);
			
			String nptMainCategory = this.getNptMainCategoryName((String) item[11], userContext);
			String nptSubCategory = this.getNptSubCategoryName((String) item[12], userContext);
			
			npt.setNptMainCategory(nptMainCategory);
			npt.setNptSubCategory(nptSubCategory);
			npt.setEventProbability((String)item[13]);
			npt.setEventImpact((String)item[14]);
			npt.setDescrPostevent((String)item[15]);
			npt.setDescrContigency((String)item[16]);
			npt.setIncidentLevel(String.valueOf(item[17]));			
			
			includedList.add(npt);
		}
		return includedList;
		
	}
	
	private List<NPTLessonTicket> shouldIncludeWell(UserContext userContext, Date startDate, Date endDate) throws Exception
	{
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		ReportOption wells = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL):null;
		List<NPTLessonTicket> result = new ArrayList<NPTLessonTicket>();
		for (String wellUid : wells.getValues())
		{
			result.addAll(this.generatedata(userContext, startDate, endDate,wellUid));
		}
		return result;
	}

	private void includeQueryData(List<NPTLessonTicket> NPTLessonTicket, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception
	{			
		ReportDataNode nodeChild = node.addChild("NPTLessonTicket");		
		
		for (NPTLessonTicket npt : NPTLessonTicket)
		{	
			ReportDataNode catChild = nodeChild.addChild("data");
			catChild.addProperty("welluid", npt.getWellUid());
			catChild.addProperty("wellname", npt.getWellName());
			catChild.addProperty("operationname", npt.getOperationName());
			catChild.addProperty("field",npt.getField());	
			catChild.addProperty("opCo",npt.getOpCo());
			catChild.addProperty("country",npt.getCountry());
			catChild.addProperty("sections",npt.getSections());
			catChild.addProperty("lessonTitle",npt.getLessonTitle());
			catChild.addProperty("descrEventRisk",npt.getDescrEventRisk());
			catChild.addProperty("rootCauseDescription",npt.getRootCauseDescription());
			catChild.addProperty("descrLesson",npt.getDescrLesson());
			catChild.addProperty("nptMainCategory",npt.getNptMainCategory());
			catChild.addProperty("nptSubCategory",npt.getNptSubCategory());
			catChild.addProperty("eventProbability",npt.getEventProbability());
			catChild.addProperty("eventImpact",npt.getEventImpact());
			catChild.addProperty("descrPostevent",npt.getDescrPostevent());
			catChild.addProperty("descrContigency",npt.getDescrContigency());
			catChild.addProperty("incidentLevel",npt.getIncidentLevel());
						
		}
		
	}
	
	private String getNptMainCategoryName(String nptMainCategory, UserContext userContext) throws Exception {
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://Activity.nptMainCategory?key=code&value=label", userSelection, null);
		if (lookupMap != null) {
			LookupItem lookupItem = lookupMap.get(nptMainCategory);
			if (lookupItem != null) {
				return (String) lookupItem.getValue();
			}
		}
		
		return nptMainCategory;
	}
	
	private String getNptSubCategoryName(String nptSubCategory, UserContext userContext) throws Exception {
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		CascadeLookupSet[] cascadeLookupNptSubCategory = LookupManager.getConfiguredInstance().getCompleteCascadeLookupSet("xml://Activity.nptSubCategory?key=code&amp;value=label", userSelection);
		
		for(CascadeLookupSet cascadeLookup : cascadeLookupNptSubCategory){
			Map<String, LookupItem> lookupList = cascadeLookup.getLookup();
			if (lookupList != null) {
				LookupItem lookupItem = lookupList.get(nptSubCategory);
				if (lookupItem != null) {
					return (String) lookupItem.getValue();
				}
			}					
		}		
		return nptSubCategory;
	}
}
