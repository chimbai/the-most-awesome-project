package com.idsdatanet.d2.drillnet.leakOffTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.HoleSection;
import com.idsdatanet.d2.core.uom.hibernate.tables.UomTemplateMapping;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LeakOffTestHoleSectionLookupHandler implements LookupHandler {

	@Override
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode treeNode, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache cache) throws Exception {
		String precision = "";
		String strSql = "SELECT outputPrecision FROM UomTemplateMapping WHERE uomTemplateUid =:uomTemplateUid AND unitMappingUid='hole_section_hole_size_LengthMeasure'";
		List precisionResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "uomTemplateUid", userSelection.getUomTemplateUid());
		precision = precisionResult.get(0).toString();
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String hql = "from HoleSection where (isDeleted is null or isDeleted = 0) and wellboreUid=:wellboreUid";
		List<HoleSection> holeSections = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "wellboreUid", userSelection.getWellboreUid());
		
		if (holeSections != null && holeSections.size() > 0) {
			List<String> uids = new ArrayList<String>();
			for (HoleSection holeSection : holeSections) {
				Double holeSize = holeSection.getHoleSize();
				if (holeSize != null) {
					String temp = String.format("%."+precision+"f",holeSize);
					uids.add(temp);
				}
			}
			Map<String, String> holeSizeLookups = new HashMap<String, String>();
			hql = "from CommonLookup where (isDeleted is null or isDeleted = 0) and lookupTypeSelection='casingsection.casing_holesize' and shortCode in (:holeSizes)";
			List<CommonLookup> result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "holeSizes", uids);
			for (CommonLookup commonLookup : result2) {
				holeSizeLookups.put(commonLookup.getShortCode(), commonLookup.getLookupLabel());
			}
			for (HoleSection holeSection : holeSections) {
				String key = holeSection.getHoleSectionUid();
				String label = holeSection.getSectionName();
				Double holeSize = holeSection.getHoleSize();
				if (holeSize != null) {
					String temp = String.format("%."+precision+"f",holeSize);
					label += " (" + holeSizeLookups.get(temp) + ")";
				}
				result.put(key, new LookupItem(key, label));
			}
		}
		return result;
	}

}
