package com.idsdatanet.d2.drillnet.report;



import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.support.ApplicationObjectSupport;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.operationPlanMaster.DefaultDynamicLookaheadDVDPlanImpl;
import com.idsdatanet.d2.drillnet.operationPlanMaster.DynamicLookaheadDVDPlan;

public class EstimateFinalDurationDataGenerator extends ApplicationObjectSupport implements ReportDataGenerator {

	@Override
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		
		DynamicLookaheadDVDPlan dvdPlanHandler = new DefaultDynamicLookaheadDVDPlanImpl();
		CustomFieldUom converter = new CustomFieldUom(userContext.getUserSelection().getLocale());
		converter.setReferenceMappingField(OperationPlanPhase.class, "p10_duration");
		OperationPlanPhase currentActualPhase = null;
		Double cumP10DurationAtPhase = 0.0;
		Double cumP10DurationTotal = 0.0;
		Double positiveP10MinusActualAtPhase = 0.0;
		Double estimatedFinalDuration = 0.0;
		Double cumActualDurationAtPhase = 0.0;
		
		String strSql = "SELECT a.planReference from Activity a, Daily d " +
				"WHERE a.operationUid =:operationUid " +
				"AND d.dailyUid = :dailyUid " +
				"AND a.dailyUid = d.dailyUid " +
				"AND (a.isDeleted = false or a.isDeleted is null) " + 
				"AND (d.isDeleted = false or d.isDeleted is null) " + 
				"AND (a.dayPlus is null or a.dayPlus = 0) " +
				"AND (a.isSimop=false or a.isSimop is null) " +
				"AND (a.isOffline=false or a.isOffline is null) " +
				"AND (a.carriedForwardActivityUid is null OR a.carriedForwardActivityUid='') " +
				"AND (a.planReference is not null and a.planReference <> '') " +
				"ORDER BY a.startDatetime DESC";
		
		List<String> result1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"operationUid", "dailyUid"}, new Object[] {userContext.getUserSelection().getOperationUid(), userContext.getUserSelection().getDailyUid()});
		
		if(result1 != null && result1.size() > 0) {
			String currentPlanPhase = result1.get(0);
			if(currentPlanPhase != null && StringUtils.isNotBlank((String) currentPlanPhase)) {
				currentActualPhase = (OperationPlanPhase) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanPhase.class, currentPlanPhase);
			}
		}
		
		if(currentActualPhase != null) {
			strSql = "FROM OperationPlanMaster m, OperationPlanPhase p "
					+ "WHERE (m.isDeleted is null or m.isDeleted = false) "
					+ "AND (p.isDeleted is null or p.isDeleted = false) "
					+ "AND (m.dvdPlanStatus=true and m.dvdPlanStatus is not null) "
					+ "AND m.operationUid = :operationUid "
					+ "AND p.operationPlanMasterUid = m.operationPlanMasterUid "
					+ "ORDER BY p.sequence";
			
			List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"operationUid"}, new Object[] {userContext.getUserSelection().getOperationUid()});
			
			if(result != null && result.size() > 0) {
				for(Object[] item : result) {
					OperationPlanPhase phase = (OperationPlanPhase) item[1];
					Double p10 = (phase.getP10Duration() == null ? 0.0 : phase.getP10Duration());
					cumP10DurationAtPhase +=  p10; 
					
					Double actualDuration = dvdPlanHandler.calculateActualDuration(phase, phase.getOperationUid());
					converter.setBaseValue(actualDuration);
					
					if((p10 - converter.getConvertedValue()) > 0) {
						positiveP10MinusActualAtPhase = p10 - converter.getConvertedValue();
					}else {
						positiveP10MinusActualAtPhase = 0.0;
					}
					
					cumActualDurationAtPhase += converter.getConvertedValue();
					
					if(phase.getOperationPlanPhaseUid().equals(currentActualPhase.getOperationPlanPhaseUid())) {
						break;
					}
				}
				
				for(Object[] item : result) {
					OperationPlanPhase phase = (OperationPlanPhase) item[1];
					cumP10DurationTotal +=  (phase.getP10Duration() == null ? 0.0 : phase.getP10Duration());
				}
			}
		}
		
		estimatedFinalDuration = cumActualDurationAtPhase
				+ positiveP10MinusActualAtPhase 
				+ (cumP10DurationTotal - cumP10DurationAtPhase);
		
		reportDataNode.addChild("estimatedFinalDuration");
		reportDataNode.addProperty("cumP10DurationAtPhase", Double.toString(cumP10DurationAtPhase));
		reportDataNode.addProperty("cumP10DurationTotal", Double.toString(cumP10DurationTotal));
		reportDataNode.addProperty("cumActualDurationAtPhase", Double.toString(cumActualDurationAtPhase));
		reportDataNode.addProperty("positiveP10MinusActualAtPhase", Double.toString(positiveP10MinusActualAtPhase));
		reportDataNode.addProperty("estimatedFinalDuration", Double.toString(estimatedFinalDuration));
		reportDataNode.addProperty("cumP10DurationAtPhaseUom", converter.getUomSymbol());
		reportDataNode.addProperty("cumP10DurationTotalUom", converter.getUomSymbol());
		reportDataNode.addProperty("cumActualDurationAtPhaseUom", converter.getUomSymbol());
		reportDataNode.addProperty("positiveP10MinusActualAtPhaseUom", converter.getUomSymbol());
		reportDataNode.addProperty("estimatedFinalDurationUom", converter.getUomSymbol());
		
	}

	@Override
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	
	

}
