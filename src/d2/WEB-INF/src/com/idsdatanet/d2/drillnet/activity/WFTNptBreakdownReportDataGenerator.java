package com.idsdatanet.d2.drillnet.activity;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;


public class WFTNptBreakdownReportDataGenerator implements ReportDataGenerator{
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		QueryProperties qp = new QueryProperties();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		
		DateFormat df = new SimpleDateFormat("dd-MMM-YY");
		
		String strSql = "SELECT startDatetime, phaseCode, rootCauseCode, SUM(activityDuration)/3600.00 AS duration, activityDescription, nptEventUid, planReference, lookupCompanyUid " +
				"FROM Activity " +
				"WHERE isDeleted IS NULL " +
				"AND (dayPlus IS NULL OR dayPlus=0) " +
				"AND (isSimop <> 1 OR isSimop IS NULL) " +
				"AND (isOffline <> 1 OR isOffline IS NULL) " +
				"AND (internalClassCode = 'TP' OR internalClassCode = 'TU') " +
				"AND operationUid = :currentOperationUid " +
				"GROUP BY planReference, rootCauseCode";
		String[] paramsFields = {"currentOperationUid"};
		Object[] paramsValues = {currentOperationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues,qp);
		
		if (lstResult.size() > 0){
			for(Object objResult: lstResult){
				Object[] objDuration = (Object[]) objResult;
				ReportDataNode thisReportNode = reportDataNode.addChild("NptActivity");
				String startDatetime = "";
				String phaseCode = "";
				String nptCode = "";
				String nptDesc = "";
				Double activityDuration = 0.0;
				String activityDescription = "";
				String nptEventUid = "";
				String planReference = "";
				String sequence = "";
				Double holeSize = 0.0;
				String phaseName = "";
				String rootCauseCode = "";
				String lookupCompanyUid = "";
				String dynamicPhaseType = "";
				String companyName = "";
				String holeSection = "";
				
				if (objDuration[5] != null && StringUtils.isNotBlank(objDuration[5].toString())){
					nptEventUid = objDuration[5].toString();
					String eventRef = "";
					
					String strSql2 = "SELECT unwantedEventUid, eventRef, nptDuration, eventStatus, eventTitle " +
							"FROM UnwantedEvent " +
							"WHERE isDeleted IS NULL " +
							"AND unwantedEventUid = :nptEventUid ";
					String[] paramsFields2 = {"nptEventUid"};
					Object[] paramsValues2 = {nptEventUid};
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
					
					if (lstResult2.size() > 0){
						thisReportNode.addProperty("nptEventUid", nptEventUid.toString());
						thisReportNode.addProperty("nptEventRef", eventRef.toString());
					}
				}
				else {
					thisReportNode.addProperty("nptEventUid", "");
					thisReportNode.addProperty("nptEventRef", "-");
				}
				
				if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())){
					startDatetime = (String) df.format(objDuration[0]);
				}
				if (objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())){ phaseCode = objDuration[1].toString();}
				if (objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())){
					nptCode = StringUtils.substring(objDuration[2].toString(),0,4);
					nptDesc = objDuration[2].toString().substring(objDuration[2].toString().lastIndexOf("-") + 1);
					rootCauseCode = objDuration[2].toString();
				}
				if (objDuration[3] != null && StringUtils.isNotBlank(objDuration[3].toString())){ activityDuration = Double.parseDouble(objDuration[3].toString());}
				if (objDuration[4] != null && StringUtils.isNotBlank(objDuration[4].toString())){ activityDescription = objDuration[4].toString();}
				
				if (objDuration[6] != null && StringUtils.isNotBlank(objDuration[6].toString())){
					planReference = objDuration[6].toString();
					String strSql3 = "SELECT sequence, holeSize, phaseName, dynamicPhaseType " +
							"FROM OperationPlanPhase " +
							"WHERE isDeleted IS NULL " +
							"AND operationPlanPhaseUid = :planReference ";
					String[] paramsFields3 = {"planReference"};
					Object[] paramsValues3 = {planReference};
					List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3,qp);
					
					if (lstResult3.size() > 0){
						for(Object objRes: lstResult3){
							Object[] objList = (Object[]) objRes;
							if (objList[0] != null && StringUtils.isNotBlank(objList[0].toString())){ sequence = objList[0].toString();}
							if (objList[1] != null && StringUtils.isNotBlank(objList[1].toString())){ holeSize = Double.parseDouble(objList[1].toString());}
							if (objList[2] != null && StringUtils.isNotBlank(objList[2].toString())){ phaseName = objList[2].toString();}
							if (objList[3] != null && StringUtils.isNotBlank(objList[3].toString())){ dynamicPhaseType = objList[3].toString();}
							
							ReportDataNode reportNode = thisReportNode.addChild("holeSize");
							//add dummy record for general listing 
							reportNode.addProperty("size", holeSize.toString() );
							reportNode.addProperty("holeSizeLabel", this.getHoleSizeLabel(this.formatHoleSize(holeSize), userContext));
							if (objList[3].equals("CMPLT") ){
								if (objList[3].toString().equals("CMPLT")){
									holeSection = "Completion";
									reportNode.addProperty("holeSection", holeSection.toString());
								}
								else
									holeSection = this.getHoleSizeLabel(this.formatHoleSize(holeSize), userContext) + " Section";
									reportNode.addProperty("holeSection", holeSection.toString());
							}
						}
					}
				}
				else {
					sequence = "";
					holeSize = 0.0;
					phaseName = "";
				}
				
				if (objDuration[7] != null && StringUtils.isNotBlank(objDuration[7].toString())){
					lookupCompanyUid = objDuration[7].toString();
					String strSql4 = "SELECT lookupCompanyUid, companyName, address " +
							"FROM LookupCompany " +
							"WHERE isDeleted IS NULL " +
							"AND lookupCompanyUid = :lookupCompanyUid ";
					String[] paramsFields4 = {"lookupCompanyUid"};
					Object[] paramsValues4 = {lookupCompanyUid};
					List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4,qp);
					
					if (lstResult4.size() > 0){
						for(Object objResults: lstResult4){
							Object[] objLists = (Object[]) objResults;
							companyName = objLists[1].toString();				
							
						}
					}
				}
				
				thisReportNode.addProperty("startDatetime", startDatetime.toString());
				thisReportNode.addProperty("phaseCode", phaseCode.toString());
				thisReportNode.addProperty("nptCode", nptCode.toString());
				thisReportNode.addProperty("nptDesc", nptDesc.toString());
				thisReportNode.addProperty("activityDuration", activityDuration.toString());
				thisReportNode.addProperty("activityDescription", activityDescription.toString());
				thisReportNode.addProperty("sequence", sequence.toString());
				thisReportNode.addProperty("phaseName", phaseName.toString());
				thisReportNode.addProperty("rootCauseCode", rootCauseCode.toString());
				thisReportNode.addProperty("companyName", companyName.toString());
				thisReportNode.addProperty("dynamicPhaseType", dynamicPhaseType.toString());
			}
					
		}
	}
	
	private String getHoleSizeLabel(String holeSizeValue, UserContext userContext) throws Exception {
		String retValue = "";
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://activity.holeSize?key=code&amp;value=label", userSelection, null);
		if (lookupMap !=null && StringUtils.isNotBlank(lookupMap.toString())  && lookupMap !=null){
			 LookupItem lookup = lookupMap.get(holeSizeValue.toString());
			if (lookup !=null)	retValue = lookup.getValue().toString();					
		} 
		
		return retValue;
	}
	
	private String formatHoleSize(Double holeSize) {
		if (holeSize==null) return "";
		DecimalFormat nf = new DecimalFormat();
		nf.setMaximumFractionDigits(6);
		nf.setMinimumFractionDigits(6);
		return nf.format(holeSize);
	}
}