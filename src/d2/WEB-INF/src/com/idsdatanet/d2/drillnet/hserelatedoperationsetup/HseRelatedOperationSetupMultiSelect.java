package com.idsdatanet.d2.drillnet.hserelatedoperationsetup;

import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.multiselect.StandardTreeNodeMultiSelect;

public class HseRelatedOperationSetupMultiSelect extends StandardTreeNodeMultiSelect{

	public List<String> loadMultiSelectKeys(CommandBeanTreeNode node) throws Exception {
		String parent_id = BeanUtils.getProperty(node.getData(), this.parentIdField);
		return this.daoManager.findByNamedParam("select " + this.childLookupField + " from " + this.childTableClass.getName() + " where (isDeleted = false or isDeleted is null) and " + this.childIdField + " = :parent_id ", new String[] {"parent_id"}, new Object[] {parent_id});
	}

}
