package com.idsdatanet.d2.drillnet.startNewDayUtils;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.web.mvc.*;
import com.idsdatanet.d2.core.model.*;
import java.util.*;
import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;

public class BharunDailySummaryDataLoaderInterceptor implements DataLoaderInterceptor {
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return query;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(meta.getTableClass().equals(BharunDailySummary.class)){
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null){
				return conditionClause.replace("{_custom_condition_}", "");
			}
						
			String thisFilter = "";
			thisFilter = "dailyUid = session.dailyUid";
						
			if("0".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "allowCopyBHAWhenGotDateOut"))){
				String strInCondition = "";
				String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND dailyUid = :thisDailyUid";
				List lstTmpResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisDailyUid", userSelection.getDailyUid());
				if (lstTmpResult.size() > 0) {
					for(Object objResult: lstTmpResult)
					{
						BharunDailySummary thisBhaDailySummary = (BharunDailySummary) objResult;
						String thisBharunUid = thisBhaDailySummary.getBharunUid().toString();
						
						String strSql2 = "FROM Bharun WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :thisBharunUid";
						List lstTmpResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "thisBharunUid", thisBharunUid);
						if (lstTmpResult2.size() > 0) 
						{
							Bharun thisBharun = (Bharun) lstTmpResult2.get(0);
							//CREATE SQL CONDITION TO LOAD ALL BHA WITH NO DATERUN, BASED ON GWP SETTING
							if (thisBharun.getDailyidOut() == null || thisBharun.getDailyidOut().equalsIgnoreCase("")) 
							{
								strInCondition = strInCondition + "'" + thisBharun.getBharunUid().toString() + "',"; 
								
							}
											
						}
					}
				}
				strInCondition = StringUtils.substring(strInCondition, 0, -1);
				thisFilter = thisFilter + " AND bharunUid IN (" + strInCondition + ")";
				
			}		
			return conditionClause.replace("{_custom_condition_}", thisFilter);
			
		}else{
			return null;
		}
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
}