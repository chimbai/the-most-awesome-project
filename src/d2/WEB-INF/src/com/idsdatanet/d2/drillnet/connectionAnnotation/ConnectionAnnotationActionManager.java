package com.idsdatanet.d2.drillnet.connectionAnnotation;

import java.util.List;


import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Annotations;
import com.idsdatanet.d2.core.model.RigStateRaw;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;

public class ConnectionAnnotationActionManager implements ActionManager {
	@Override
	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if (node.getData() instanceof RigStateRaw) {
			if (action.equals(Action.SAVE))
				return new SaveAnnotationsActionHandler();
			return null;
		}
		return null;
	}
	
	private class SaveAnnotationsActionHandler implements ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			Object obj = node.getData();
			
			if (obj instanceof RigStateRaw) {
				RigStateRaw rsr = (RigStateRaw) node.getData();
				Annotations ann;
				

				String strSql = "FROM Annotations WHERE rigStateRawUid=:rigStateRawUid AND (isDeleted = FALSE OR isDeleted IS NULL)";
				List<Annotations> antResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigStateRawUid", rsr.getRigStateRawUid());

				if (antResult.size() > 0) {
					ann = (Annotations) antResult.get(0);
				} else {
					ann = new Annotations();
					ann.setRigStateRawUid(rsr.getRigStateRawUid());
					ann.setDailyUid(userSession.getCurrentDailyUid());
				}
				
				Object threshold = node.getDynaAttr().get("threshold");
				ann.setThreshold(threshold == null ? "" : threshold.toString());
				
				Object rootCause = node.getDynaAttr().get("rootCause");
				ann.setRootCause(rootCause == null ? "" : rootCause.toString());
				
				Object description = node.getDynaAttr().get("description");
				ann.setDescription(description == null ? "" : description.toString());
				
				if (StringUtils.isBlank(ann.getThreshold()) && StringUtils.isBlank(ann.getRootCause()) && StringUtils.isBlank(ann.getDescription()))
					ann.setIsDeleted(true);
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ann);
			}
			
			return new ActionHandlerResponse(true, false, null, BaseCommandBean.OPERATION_PERFORMED_SAVE_EXISTING);
		}
	}
}
