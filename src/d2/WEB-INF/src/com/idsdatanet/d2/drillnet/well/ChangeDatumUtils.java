package com.idsdatanet.d2.drillnet.well;

import java.text.NumberFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.NumberUtils;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ChangeDatumUtils {
	public static void doSaveOpsDatum(BaseCommandBean commandBean, HttpServletRequest request, HttpServletResponse response) throws Exception {
		commandBean.incomingRequestStart(request); // FIXME: this is needed because CustomFieldUom.format(...) uses currentUserSession/currentUserSelectionSnapshot in command bean, need to call afterCompletion later		
		String operationUid = request.getParameter("operationUid");
		String defaultDatumUid = request.getParameter("defaultDatumUid");
		String reportingDatumOffset = request.getParameter("reportingDatumOffset");
		String datumType = request.getParameter("datumType");
		String datumReferencePoint = request.getParameter("datumReferencePoint");
		String offsetMsl = request.getParameter("offsetMsl");
		String entryType = request.getParameter("entryType");		
		if ("null".equals(entryType)) entryType = null; //catch "null" string from flex
		String slantAngle = request.getParameter("slantAngle");
		if ("null".equals(slantAngle)) slantAngle = null;//catch "null" string from flex
		Boolean isGlReference = new Boolean(request.getParameter("isGlReference"));

		Double dblOffsetMsl = toDouble(offsetMsl, request);
		Double dblReportingDatumOffset = toDouble(reportingDatumOffset, request);
		Double dblSlantAngle = toDouble(slantAngle, request);
		Double dblSurfaceSlantLength = 0.0;
		if (StringUtils.isBlank(entryType) || entryType == null) entryType = "0";
		
		//If is reference from GL, datum type set to empty
		if(isGlReference) {
			if ("--".equalsIgnoreCase(datumType)) {
				datumType = "";
			}
		}
		
		//if (dblReportingDatumOffset > 0 && dblSlantAngle > 0) {
		if (StringUtils.isNotBlank(reportingDatumOffset) && StringUtils.isNotBlank(slantAngle) && "1".equals(entryType)) {
			Double reportingDatumOffsetRaw = 0.0;
			Double slantAngleRaw = 0.0;
			
			CustomFieldUom thisConverterField = new CustomFieldUom(commandBean, OpsDatum.class, "reportingDatumOffset");
			thisConverterField.setBaseValueFromUserValue(dblReportingDatumOffset);
			reportingDatumOffsetRaw = thisConverterField.getBasevalue();
			
			thisConverterField.setReferenceMappingField(OpsDatum.class, "slantAngle");
			thisConverterField.setBaseValueFromUserValue(dblSlantAngle);
			slantAngleRaw = thisConverterField.getBasevalue();
			
			//check angle unit is it in degree or radian, if in degree need to convert to radian for calculation
			
			double convertionFactor = 1.0;
			double thisSlantAngle;
			
			if ("Degree".equals(thisConverterField.getUomLabel())) convertionFactor = 1 / 57.2957549575;
			else if ("Gradian".equals(thisConverterField.getUomLabel())) convertionFactor = 0.015707963;
			
			thisSlantAngle = slantAngleRaw * convertionFactor;
			dblSurfaceSlantLength = reportingDatumOffsetRaw / Math.cos(thisSlantAngle);
			
			thisConverterField.setReferenceMappingField(OpsDatum.class, "surfaceSlantLength");			
			
			thisConverterField.setBaseValue(dblSurfaceSlantLength);
			dblSurfaceSlantLength = thisConverterField.getConvertedValue();
			
		}
		
		String datumName = CustomFieldUom.format(commandBean, dblReportingDatumOffset, OpsDatum.class, "reportingDatumOffset").replace(" ", "") + " " + datumType;
		OpsDatum opsDatum = null;
		
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
		if (operation != null) {
			//if (StringUtils.isNotBlank(reportingDatumOffset) && StringUtils.isNotBlank(datumType)) {
			  if (StringUtils.isNotBlank(reportingDatumOffset)) {
				// check if the entered datum already exists
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OpsDatum where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and wellboreUid = :wellboreUid and wellUid = :wellUid and offsetMsl = :offsetMsl and datumCode = :datumCode and datumReferencePoint = :datumReferencePoint and reportingDatumOffset = :reportingDatumOffset and entryType = :entryType and slantAngle = :slantAngle and isGlReference = :isGlReference", 
						new String[] {"operationUid", "wellboreUid", "wellUid", "offsetMsl", "datumCode", "datumReferencePoint", "reportingDatumOffset", "entryType", "slantAngle", "isGlReference"}, 
						new Object[] {operation.getOperationUid(), operation.getWellboreUid(), operation.getWellUid(), dblOffsetMsl, datumType, datumReferencePoint, dblReportingDatumOffset, entryType, dblSlantAngle, isGlReference});
				if (list != null && list.size() > 0) {
					opsDatum = (OpsDatum) list.get(0);
				} else {
					// does not exist, create one					
					opsDatum = new OpsDatum();
					opsDatum.setOperationUid(operation.getOperationUid());
					opsDatum.setWellboreUid(operation.getWellboreUid());
					opsDatum.setWellUid(operation.getWellUid());
					opsDatum.setRigInformationUid(operation.getRigInformationUid());
					opsDatum.setDatumName(datumName);
					opsDatum.setIsReference("1");
					opsDatum.setDatumCode(datumType);
					opsDatum.setOffsetMsl(dblOffsetMsl);
					opsDatum.setDatumReferencePoint(datumReferencePoint);
					opsDatum.setReportingDatumOffset(dblReportingDatumOffset);
					opsDatum.setEntryType(entryType);
					opsDatum.setSlantAngle(dblSlantAngle);
					opsDatum.setSurfaceSlantLength(dblSurfaceSlantLength);
					opsDatum.setIsGlReference(isGlReference);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(opsDatum);
				}
				defaultDatumUid = opsDatum.getOpsDatumUid();
			}
			
			if (StringUtils.isNotBlank(defaultDatumUid)) {
				// set as default datum of the operation
				if (DefaultDatumLookupHandler.MSL.equals(defaultDatumUid)) {
					operation.setDefaultDatumUid(null);
				} else {
					operation.setDefaultDatumUid(defaultDatumUid);
				}
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(operation);
				
				DatumManager.loadDatum();
				UserSession.getInstance(request).setCurrentUOMDatumUid(operation.getDefaultDatumUid(), true);
			}	
		}
		
		commandBean.afterCompletion(request, response); // FIXME
	}
	
	private static double toDouble(String text, HttpServletRequest request) {
		if (StringUtils.isNotBlank(text)) {
			try {
				return (Double) NumberUtils.parseNumber(text, Double.class, NumberFormat.getInstance(request.getLocale()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
}
