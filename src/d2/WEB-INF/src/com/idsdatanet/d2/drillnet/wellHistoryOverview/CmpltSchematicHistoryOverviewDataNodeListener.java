package com.idsdatanet.d2.drillnet.wellHistoryOverview;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.infra.filemanager.FileManagerCommandBean;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.FileManagerFiles;
import com.idsdatanet.d2.core.model.ProductionString;
import com.idsdatanet.d2.core.web.filemanager.FileManagerUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CmpltSchematicHistoryOverviewDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{
		
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		node.getDynaAttr().put("screenOption", (String) commandBean.getRoot().getDynaAttr().get("ScreenOutput"));	
		
		if((obj instanceof String) && meta.getTableClass() == ProductionString.class){
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ProductionString WHERE productionStringUid=:productionStringUid ORDER BY installDateTime", "productionStringUid", obj.toString());
			obj = lstResult.get(0);
			node.setData(obj);
			
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
		
		if (obj instanceof ProductionString) {
			ProductionString productionString = (ProductionString) obj;
			String componentUid = productionString.getProductionStringUid();
			
			if (StringUtils.isNotBlank(componentUid)) {
				ProductionString productionstring = (ProductionString) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ProductionString.class, componentUid);
				
				Map<String,LookupItem> productionTypeLookup= LookupManager.getConfiguredInstance().getLookup("xml://productionString.stringtype?key=code&amp;value=label", userSelection, null);
				
				   if (productionstring !=null) {
					node.getDynaAttr().put("stringNo", productionstring.getStringNo());
					
					/*
					 * Only enable it for the productionType which was in lookup type
					 * 
					String productionType = "";
					
					if (productionstring.getProductionType() != null){
						LookupItem pt = productionTypeLookup.get(productionstring.getProductionType());
						if (pt != null)
						{
							productionType = pt.getValue().toString();
						}
					}
					node.getDynaAttr().put("productionType",productionType);
					*/
					node.getDynaAttr().put("productionType", productionstring.getProductionType());
					node.getDynaAttr().put("description", productionstring.getDescription());
					node.getDynaAttr().put("stringType",productionstring.getStringType());
					node.getDynaAttr().put("version", productionstring.getVersion());
					node.getDynaAttr().put("eventUpdates", productionstring.getEventUpdates());
					node.getDynaAttr().put("remark", productionstring.getRemark());
							

					if (productionstring.getInstallDateTime()!=null) node.getDynaAttr().put("installDateTime",sdf.format(productionstring.getInstallDateTime()));
					if (productionstring.getUninstallDateTime()!=null) node.getDynaAttr().put("uninstallDateTime",sdf.format(productionstring.getUninstallDateTime()));
						
					
					//load file manager file list 
					String filemanagerlist = "";
					String filename = "";
					List<FileManagerFiles> fileLst = FileManagerUtils.getFlexNodeFilesAttachment("productionStringController", obj);
					
					for(FileManagerFiles filemanagerfiles : fileLst){
						File actual_file = FileManagerUtils.getAttachedFile(filemanagerfiles);
						
						if (filemanagerfiles.getIsPrivate()!=true && actual_file.exists())
						{
						
							String fileUid = filemanagerfiles.getFileManagerFilesUid();
							String uploadedDate = filemanagerfiles.getUploadedDatetime().toString();
							String fileName = filemanagerfiles.getFileName();
							
							if (StringUtils.isBlank(filemanagerlist)) {
								filemanagerlist = "fileuid=" + fileUid + "&filename=" + fileName + "&date=" + uploadedDate;
							}else {
								filemanagerlist = filemanagerlist + ",fileuid=" + fileUid + "&filename=" + fileName + "&date=" + uploadedDate;
							}
							
							if (StringUtils.isBlank(filename)) {
								filename = fileName;
							}else {
								filename = filename + "\n"+ fileName;
							}
							
							
						}
							
					}
					
					node.getDynaAttr().put("fileManagerList", filemanagerlist);
					node.getDynaAttr().put("fileName", filename);
				}
				
			}
			
			
			}
			

	}

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
