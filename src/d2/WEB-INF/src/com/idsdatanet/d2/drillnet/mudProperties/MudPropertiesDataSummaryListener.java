package com.idsdatanet.d2.drillnet.mudProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.EmptyDataSummary;
import com.idsdatanet.d2.core.web.mvc.Summary;
import com.idsdatanet.d2.core.web.mvc.SummaryInfo;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class MudPropertiesDataSummaryListener extends EmptyDataSummary {
	private List<Summary> data = null;
	
	public void generate(HttpServletRequest request) throws Exception {
		this.data = new ArrayList<Summary>();
		UserSession session = UserSession.getInstance(request);
		String operation = session.getCurrentOperationUid();
		List<String> mudTypeList = new ArrayList<String>();
		
		String strSql1 = "select mud.mudType, daily.dayDate FROM MudProperties mud, Daily daily WHERE (mud.isDeleted = false or mud.isDeleted is null) " +
					"and (daily.isDeleted = false or daily.isDeleted is null) and mud.dailyUid = daily.dailyUid and mud.operationUid= :thisOperation " +
					"group by mud.mudType, daily.dayDate ORDER BY daily.dayDate, mud.mudType";
		String[] paramsFields1 = {"thisOperation"};
		String[] paramsValues1 = {operation};
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);

		if (lstResult.size() > 0){
			for (Object[] objMud : lstResult){
				String strMudType = (String) objMud[0];
				//String mudPropertiesUid = (String) objMud[1];
	
				// eliminate redundant mudType number
				
				if(mudTypeList.contains(strMudType)) continue;
				mudTypeList.add(strMudType);
				
				String strSql2 = "SELECT a.dailyUid, b.dayDate FROM MudProperties a, Daily b WHERE (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and " +
								"a.dailyUid = b.dailyUid AND a.mudType= :mudtype AND a.operationUid =:operationUid group by a.dailyUid, b.dayDate ORDER BY b.dayDate";
				String[] paramsFields2 = {"mudtype", "operationUid"};
				String[] paramsValues2 = {strMudType, operation};
				List<Object[]> lstResultDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);

				if (lstResult.size() > 0){
					String mudTypeLabel = this.getMudTypeLabel(strMudType, request);
					
					if (StringUtils.isBlank(strMudType)) mudTypeLabel = "[no mud type selected]";
					Summary summary = new Summary(mudTypeLabel);
					
					for (Object[] dailyRec : lstResultDaily){
						String dailyUid = (String) dailyRec[0];
						Daily thisDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
						
						ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), thisDaily.getDailyUid());
						String reportNumber = "-";
						if (reportDaily!=null) {
							reportNumber = reportDaily.getReportNumber(); 
							if (!session.withinAccessScope(reportDaily)) {
								continue;
							}
						}

						summary.addInfo(new SummaryInfo("Day #" + reportNumber, thisDaily.getDailyUid()));
						
						/*List reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid", "dailyUid", thisDaily.getDailyUid());
						String reportNumber = "-";
						if(reportDailyList != null && reportDailyList.size() > 0) {
							ReportDaily reportDaily = (ReportDaily) reportDailyList.get(0);
							reportNumber = reportDaily.getReportNumber();
							if (!session.withinAccessScope(reportDaily)) {
								continue;
							}
						}
						summary.addInfo(new SummaryInfo("Day #" + reportNumber, thisDaily.getDailyUid()));*/
					}
					
					if (summary.getInfo().size() > 0) {
						this.data.add(summary);
					}
					//summary.dispose();
				}
			}
		}
		
	}
	
	public List<Summary> getData() {
		return this.data;
	}
	
	private String getMudTypeLabel(String mudType, HttpServletRequest request) throws Exception {
		UserSession session = UserSession.getInstance(request);
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://mud_properties.mud_type?key=code&value=label", userSelection, null);
		if (lookupMap != null) {
			LookupItem lookupItem = lookupMap.get(mudType);
			if (lookupItem != null) {
				return (String) lookupItem.getValue();
			}
		}
		
		return mudType;
	}

	

}
