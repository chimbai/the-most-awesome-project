package com.idsdatanet.d2.drillnet.transportDailyParam;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.ExcelImportTemplateManager;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.TransportDailyParam;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.witsml.wellit.transportdaily.WellITPobHeliRoutes;
import com.idsdatanet.depot.witsml.wellit.transportdaily.WellITTransportDailyScreenImport;

public class TransportDailyParamCommandBeanListener extends EmptyCommandBeanListener  {
	
	private ExcelImportTemplateManager excelImportTemplateManager = null;
	
	public ExcelImportTemplateManager getExcelImportTemplateManager() {
        return excelImportTemplateManager;
    }

    public void setExcelImportTemplateManager(ExcelImportTemplateManager excelImportTemplateManager) {
        this.excelImportTemplateManager = excelImportTemplateManager;
    }
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		String action = request.getParameter("action");
		if (StringUtils.equals(ExcelImportTemplateManager.IMPORT_TEMPLATE_EXCEL, action)) {
            this.excelImportTemplateManager.importExcelFile((BaseCommandBean) commandBean, this, request, request.getParameter(ExcelImportTemplateManager.KEY_TEMPLATE), request.getParameter(ExcelImportTemplateManager.KEY_FILE), "TransportDailyParam");
        }else if (StringUtils.equals("importWellIT",action)) {
        	this.importWellIT(request,commandBean,targetCommandBeanTreeNode);
        }else if(TransportDailyParam.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())){
			if("transportType".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				this.transportTypeChanged(targetCommandBeanTreeNode, request);
			}
			if("supportVesselInformationUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				this.supportVesselInformationChanged(targetCommandBeanTreeNode, request);
			}
		}
	}
	
	@Override
    public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
        if (ExcelImportTemplateManager.IMPORT_TEMPLATE_EXCEL.equalsIgnoreCase(invocationKey)) {
            this.excelImportTemplateManager.uploadFile(request, response, ExcelImportTemplateManager.KEY_FILE);
        }
        return;
    }
	
	//To Load TransportType While Changing
	private void transportTypeChanged(CommandBeanTreeNode node, HttpServletRequest request) throws Exception {
		String transportType = (String) PropertyUtils.getProperty(node.getData(), "transportType");	
		Object object = node.getData();
		if (object instanceof TransportDailyParam) {
			TransportDailyParam transportDailyParam = (TransportDailyParam)object;
			
			if (transportType.equalsIgnoreCase("boat")){	
				String supportVesselInformationUid = (String) PropertyUtils.getProperty(node.getData(), "supportVesselInformationUid");
				if (StringUtils.isBlank(supportVesselInformationUid)){
					transportDailyParam.setLookupCompanyUid(null);
				}else{
					String[] paramsFields = {"supportVesselInformationUid"};
					Object[] paramsValues = {supportVesselInformationUid}; 
					
					String strSql = "select operator from SupportVesselInformation where (isDeleted = false or isDeleted is null) and supportVesselInformationUid = :supportVesselInformationUid";
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					if (lstResult.size()>0)
					{
						Object a = lstResult.get(0);
						String operator = (String) a;
						transportDailyParam.setLookupCompanyUid(operator);
					}else{
						transportDailyParam.setLookupCompanyUid(null);
					}
				}
			}
		}
	}
	
	//To Load Support Vessel Information Detail
	private void supportVesselInformationChanged(CommandBeanTreeNode node, HttpServletRequest request) throws Exception {
		String supportVesselInformationUid = (String) PropertyUtils.getProperty(node.getData(), "supportVesselInformationUid");	
		
		String[] paramsFields = {"supportVesselInformationUid"};
		Object[] paramsValues = {supportVesselInformationUid}; 
		
		String strSql = "select operator from SupportVesselInformation where (isDeleted = false or isDeleted is null) and supportVesselInformationUid = :supportVesselInformationUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		Object object = node.getData();
		if (object instanceof TransportDailyParam) {
			TransportDailyParam transportDailyParam = (TransportDailyParam)object;
			if (lstResult.size()>0)
			{
				Object a = lstResult.get(0);
				String operator = (String) a;
				transportDailyParam.setLookupCompanyUid(operator);
			}else{
				transportDailyParam.setLookupCompanyUid(null);
			}
		}
	}
	
	private void importWellIT(HttpServletRequest request, CommandBean commandBean,CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
    	Object dataList = new WellITTransportDailyScreenImport().getDataFromWellIT(request,commandBean);
    	this.importPobHeliRoute(request,(ArrayList<Object>)dataList,commandBean,targetCommandBeanTreeNode);
	}
    
    private void importPobHeliRoute(HttpServletRequest request, ArrayList<Object> dataList,CommandBean commandBean,CommandBeanTreeNode targetCommandBeanTreeNode) {
    	try {
    		commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
        	CommandBeanTreeNodeImpl targetNode = (CommandBeanTreeNodeImpl) commandBean.getRoot();
    		UserSession userSession = UserSession.getInstance(request);
    		String strSql = "FROM LookupCompany WHERE (isDeleted IS NULL or isDeleted = 0) AND company_name = 'CHC Helicopters'";
			List<LookupCompany> lookupCompanyList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			String lookupCompanyUid = null;
			if(lookupCompanyList.size() > 0) {
				LookupCompany lookupCompany = lookupCompanyList.get(0);
				lookupCompanyUid = lookupCompany.getLookupCompanyUid();
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        	for(Object o : dataList) {
        		if (o instanceof WellITPobHeliRoutes) {
        			WellITPobHeliRoutes data = (WellITPobHeliRoutes) o;
	        		TransportDailyParam transportDaily = new TransportDailyParam();
	        		transportDaily.setTransportType("helicopter");
	        		transportDaily.setTransportRefNumber(data.getRouteName());
	        		transportDaily.setPaxIn(data.getPaxOut());
	        		transportDaily.setPaxOut(data.getPaxIn());
	        		transportDaily.setComments(data.getRouteDescription());
	        		transportDaily.setLookupCompanyUid(lookupCompanyUid);
	        		Date fromDate = null;
	        		Date toDate = null;
	        		if(data.getFromDate() != null) {
	        			if(!StringUtils.isEmpty(data.getFromDate())) {
	        				fromDate = sdf.parse(data.getFromDate());
	        			}
	        		}
	        		if(data.getToDate() != null) {
	        			if(!StringUtils.isEmpty(data.getToDate())) {
	        				toDate = sdf.parse(data.getToDate());
	        			}
	        		}
	        		transportDaily.setArrivalTime(fromDate);
	        		transportDaily.setDepartureTime(toDate);
	        		CommandBeanTreeNodeImpl node = (CommandBeanTreeNodeImpl) targetNode.addCustomNewChildNodeForInput(transportDaily);
	        		node.getAtts().setEditMode(true);
	                node.setNewlyCreated(true);
	                node.setDirty(true);
        		}
        	}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
}