package com.idsdatanet.d2.drillnet.tightholesetup;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.log.CommandLoggingContext;
import com.idsdatanet.d2.core.log.CommandLoggingControl;
import com.idsdatanet.d2.core.model.TightHoleUser;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.operation.OperationUtils;

public class TightHoleSetupUtils extends BlazeRemoteClassSupport {
	
	public String getUserList() {
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			String readOnly = "";
			List <Object []> userData = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT u.userUid, u.readOnly FROM User u WHERE (u.isblocked = false OR u.isblocked IS NULL) AND (u.isDeleted = false OR u.isDeleted IS NULL) AND u.userUid =:userUid", "userUid", getCurrentUserSession().getCurrentUser().getUserUid());
			if (userData.size() > 0 && userData!=null)
			{
				    
				for(Object[] obj: userData)
				{
					if(obj[1]!=null)
					{
					    readOnly = obj[1].toString();
						writer.startElement("ReadOnly");
						writer.addElement("isReadOnly", readOnly);
						writer.endElement();
					}
				}
					
			}
		
			List<Object[]> objs = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT u.userUid, u.fname, u.userName FROM User u WHERE (u.isblocked = false OR u.isblocked IS NULL) AND (u.isDeleted = false OR u.isDeleted IS NULL) AND u.userName <> 'idsadmin' and (idsUser = false or idsUser is null) ORDER BY u.fname");
			Collections.sort(objs, new UserComparator());
			for (Object[] obj : objs) {
				writer.startElement("User");
				writer.addElement("userUid", (String) obj[0]);
				writer.addElement("fname", StringUtils.defaultIfEmpty((String) obj[1], (String) obj[2]));
				writer.endElement();
			}
			writer.endElement();
			writer.close();
			return bytes.toString("utf-8");
		} catch (Exception e) {
			return "<root/>";
		}
	}
	
	public String getTightHoleUserList() {
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			List<Object[]> objs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT t.userUid, u.fname, u.userName FROM TightHoleUser t, User u WHERE (t.isDeleted = false OR t.isDeleted IS NULL) AND (u.isDeleted = false OR u.isDeleted IS NULL) AND t.userUid = u.userUid AND t.operationUid = :operationUid AND u.userName <> 'idsadmin' and (u.idsUser = false or u.idsUser is null) ", "operationUid", getCurrentUserSession().getCurrentOperationUid());
			Collections.sort(objs, new UserComparator());
			for (Object[] obj : objs) {
				writer.startElement("TightHoleUser");
				writer.addElement("userUid", (String) obj[0]);
				writer.addElement("fname", StringUtils.defaultIfEmpty((String) obj[1], (String) obj[2]));
				writer.endElement();
			}
			writer.endElement();
			writer.close();
			return bytes.toString("utf-8");
		} catch (Exception e) {
			return "<root/>";
		}
	}
	
	private class UserComparator implements Comparator<Object[]> {
		public int compare(Object[] o1, Object[] o2) {
			if (StringUtils.isNotBlank((String) o1[1]) && StringUtils.isNotBlank((String) o2[1])) {
				return ((String) o1[1]).toLowerCase().compareTo(((String) o2[1]).toLowerCase());
			} else if (StringUtils.isBlank((String) o1[1]) && StringUtils.isNotBlank((String) o2[1])) {
				return ((String) o1[2]).toLowerCase().compareTo(((String) o2[1]).toLowerCase());
			} else if (StringUtils.isNotBlank((String) o1[1]) && StringUtils.isBlank((String) o2[1])) {
				return ((String) o1[2]).toLowerCase().compareTo(((String) o2[2]).toLowerCase());
			} else {
				return 0;
			}
		}
	}
	
	public void saveTightHoleUserList(List<String> userUidList) throws Exception {
		
		CommandLoggingControl commandLoggingControl = CommandLoggingContext.getConfiguredInstance().joinOrStartLoggingCommand();
		try {
		
			List<String> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT DISTINCT t.userUid FROM TightHoleUser t, User u WHERE (t.isDeleted = false OR t.isDeleted IS NULL) AND (u.isDeleted = false OR u.isDeleted IS NULL) AND t.userUid = u.userUid AND t.operationUid = :operationUid AND u.userName <> 'idsadmin'", "operationUid", getCurrentUserSession().getCurrentOperationUid());
			for (String userUid : rs) {
				if (userUidList.contains(userUid)) {
					userUidList.remove(userUid);
				} else {
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE TightHoleUser t SET t.isDeleted = true, t.lastEditDatetime = :lastEditDatetime, t.lastEditUserUid = :lastEditUserUid WHERE t.userUid = :userUid AND t.operationUid = :operationUid", new String[] {"lastEditDatetime", "lastEditUserUid", "userUid", "operationUid"}, new Object[] {new Date(), getCurrentUserSession().getUserUid(), userUid, getCurrentUserSession().getCurrentOperationUid()});
				}
			}
			for (String userUid : userUidList) {
				TightHoleUser t = new TightHoleUser();
				t.setGroupUid(getCurrentUserSession().getCurrentGroupUid());
				t.setWellUid(getCurrentUserSession().getCurrentWellUid());
				t.setWellboreUid(getCurrentUserSession().getCurrentWellboreUid());
				t.setOperationUid(getCurrentUserSession().getCurrentOperationUid());
				t.setUserUid(userUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(t);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			commandLoggingControl.endLoggingCommand();
		}
	}
	
	public String getWWOHyperlinkRedirection() {
		try {
			return OperationUtils.getWWOHyperlinkRedirection(getCurrentUserSession().getCurrentGroupUid());			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
}
