package com.idsdatanet.d2.drillnet.hseIncident;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class HseIncidentDataNodeListener extends EmptyDataNodeListener {
	
	private Integer defaultEventNo = null;
	
//	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
//		
//		Object object = node.getData();
//
//		if(object instanceof HseIncident) {
//			String thisEventDate = (String) node.getDynaField().getValues().get("hse_eventdate");
//			String thisEventTime = (String) node.getDynaField().getValues().get("hse_eventtime");
//			
//			if(StringUtils.isNotBlank(thisEventDate) && StringUtils.isNotBlank(userSelection.getDailyUid())){
//				
//				if(StringUtils.isNotBlank(thisEventTime)) {
//					
//				}
//				else {
//					thisEventTime = "00:00";
//				}
//				
//				Date dtEventDate 		= CommonDateParser.parse(thisEventDate + " " + thisEventTime);
//				Daily today 			= ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
//				Date todayDate 			= today.getDayDate();
//				long daysLapsed 		= 0;
//				String strdaysLapsed 	= "";
//				
//				if(todayDate != null && dtEventDate != null) {
//					daysLapsed = ((todayDate.getTime() - dtEventDate.getTime()) / (24 * 60 * 60 * 1000 )) + 1;
//					NumberFormat formatter = new DecimalFormat("#0.00");
//					strdaysLapsed = formatter.format(daysLapsed);
//				}
//	
//				node.getDynaAttr().put("daysLapsed", strdaysLapsed);
//			}
//			
//			
//		}
//		
//	}
	
	public void setDefaultEventNo(Integer value){
		this.defaultEventNo = value;
	}
	
	
	
	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed) throws Exception {
		
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
		Object data = node.getData();
		
		if(data instanceof HseIncident) {
			HseUtils.populateAndUpdateLastLTI(userSelection.getDailyUid(), userSelection.getRigInformationUid(), userSelection.getGroupUid(), true, null);
		}
	}

	@Override
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed) throws Exception {
		
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
		Object data = node.getData();
		
		if(data instanceof HseIncident) {
			HseUtils.populateAndUpdateLastLTI(userSelection.getDailyUid(), userSelection.getRigInformationUid(), userSelection.getGroupUid(), true, null);
		}
	}

	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		Object object = node.getData();
		if (object instanceof HseIncident) {
			HseIncident hseIncident = (HseIncident) object;
			Date eventDateTime = hseIncident.getHseEventdatetime();
			//if (eventDateTime != null && StringUtils.isNotBlank(userSelection.getDailyUid())) {
			if (eventDateTime != null && StringUtils.isNotBlank(hseIncident.getDailyUid())) {
				//Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(hseIncident.getDailyUid());
				if (daily != null) {
					Date dayDate = daily.getDayDate();
					if (dayDate != null) {	
						double daysLapsed = CommonUtil.getConfiguredInstance().calculateDaysSinceHseIncident(dayDate, eventDateTime, userSelection.getGroupUid());
						
						//assign unit to the value base on daily days_since_lta unit
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "days_since_lta");
						thisConverter.setBaseValue(daysLapsed);	
						if (thisConverter.isUOMMappingAvailable()) {
							node.setCustomUOM("@daysLapsed", thisConverter.getUOMMapping());   
						}
						node.getDynaAttr().put("daysLapsed", thisConverter.getConvertedValue());
					}
				}
			}
			this.includeNextEventDate(node);
			
			String dailyUid = hseIncident.getDailyUid();
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(hseIncident.getOperationUid());
			String query = "from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and dailyUid=:dailyUid";
			String[] paramNames = {"reportType", "dailyUid"};
			Object[] paramValues = {reportType, dailyUid};
			List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramNames, paramValues);
			if (reportDailyList.size()>0) {
				ReportDaily reportDaily = reportDailyList.get(0);
				node.getDynaAttr().put("enteredBy", reportDaily.getSupervisor());
			}
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, HseIncident.class, "numberOfIncidents");
			Integer welltodate = HseUtils.getNumberOfIncidentsWellToDate(hseIncident.getOperationUid(), hseIncident.getDailyUid(),hseIncident.getIncidentCategory());
			thisConverter.setBaseValue(welltodate);
			node.getDynaAttr().put("numberOfIncidentsWellToDate", thisConverter.getConvertedValue());
			
			//for DEA GE HSE Incident
			node.getDynaAttr().put("cumNumOfIncidents", thisConverter.getConvertedValue());

			//new field for total num of incidents based on rig (OP-4861 d2_choc_my)
			Integer rigtodate = HseUtils.getNumberOfIncidentsRigToDate(hseIncident.getRigInformationUid(), hseIncident.getDailyUid(), hseIncident.getIncidentCategory());
			thisConverter.setBaseValue(rigtodate);
			node.getDynaAttr().put("cumNumOfIncidentsRigToDate", thisConverter.getConvertedValue());
			
		    thisConverter.setReferenceMappingField(HseIncident.class, "intvToolLength");
		    if (thisConverter.isUOMMappingAvailable()){
		     commandBean.setCustomUOM(DailyObservation.class,"intvToolLength", thisConverter.getUOMMapping());
		    }
		    
		    if (hseIncident.getIncidentCategory()!=null){
		    	List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select internalCode from LookupIncidentCategory where (isDeleted = false or isDeleted is null) and hseCategory = :incidentCategory", "incidentCategory", hseIncident.getIncidentCategory());
				if (lstResult.size() > 0){
					if (lstResult.get(0) !=null) {
						node.getDynaAttr().put("lookupIncidentCategoryInternalCode", lstResult.get(0).toString());
					}
				}
		    }
		}
	}
	
	
	public void includeNextEventDate(CommandBeanTreeNode node) throws Exception
	{
		HseIncident hseIncident = (HseIncident) node.getData();
		if(hseIncident.getHseEventdatetime()!=null && !StringUtils.isBlank(hseIncident.getFrequency()))
		{
			String frequency = hseIncident.getFrequency();
			if (frequency.length()>1)
			{
				String[] tokenList={"d","m","y"};
				String token=frequency.substring(0,1);
				String tokenVal=frequency.substring(1,frequency.length());
				if (Arrays.asList(tokenList).contains(token) && NumberUtils.isNumber(tokenVal) && Integer.parseInt(tokenVal)>0)
				{
					Date nextEventDate=dateAdd(hseIncident.getHseEventdatetime(),token,Integer.parseInt(tokenVal));
					node.getDynaAttr().put("nextHseEventdatetime",nextEventDate);
				}
			}
		}
	}
	
	public Date dateAdd(Date date1,String token,int tokenVal){
		if(token.equalsIgnoreCase("d")){
			return DateUtils.addDays(date1, tokenVal);
		}else if(token.equalsIgnoreCase("m")){
			return DateUtils.addMonths(date1, tokenVal);
		}else if(token.equalsIgnoreCase("y")){	
			return DateUtils.addYears(date1, tokenVal);
		}
		return date1;
	}	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof HseIncident) {
			HseIncident hseIncident = (HseIncident) object;
			
			if (commandBean.getOperatingMode() != BaseCommandBean.OPERATING_MODE_DEPOT) 
				hseIncident.setRigInformationUid(session.getCurrentRigInformationUid());
			
			if (hseIncident.getNumberOfIncidents() == null){
				if(defaultEventNo != null){
					hseIncident.setNumberOfIncidents(defaultEventNo);
				}else{
					hseIncident.setNumberOfIncidents(1);
				}
			}else if(hseIncident.getNumberOfIncidents() < 0){
				status.setFieldError(node, "numberOfIncidents", "The record is not saved if the value is less than 0. ");
				status.setContinueProcess(false, true);
				return;	
			}
			
            // 47331 - incidentCategory Perfect Day validation (max 1 for Day)
            if (!this.validateUniqueStringField(node, status, "incidentCategory", "Perfect Day", "There can only be ONE Perfect Day record in a day.")) {
                return;
            }
            
            // 13197 - incidentCategory Safe Day validation (max 1 for Day)
            if (!this.validateUniqueStringField(node, status, "incidentCategory", "Safe Day", "There can only be ONE Safe Day record in a day.")) {
                return;
            }

            if (StringUtils.isNotBlank(hseIncident.getIncidentCategory())) {
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select isLosttime from LookupIncidentCategory where (isDeleted = false or isDeleted is null) and hseCategory = :incidentCategory", "incidentCategory", hseIncident.getIncidentCategory());
				if (lstResult.size() > 0){
					if (lstResult.get(0) !=null) {
						hseIncident.setIsLostTimeIncident(Boolean.parseBoolean(lstResult.get(0).toString()));
					}
				}	
			}
            
            //Ticket: 20030 - check if start time greater than end time
			if (hseIncident.getHseEventEnddatetime() != null &&  hseIncident.getHseEventdatetime() != null) {
				if(hseIncident.getHseEventdatetime().getTime() > hseIncident.getHseEventEnddatetime().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "hseEventdatetime", "HSE event start time can not be greater than HSE event end time.");
					return;
				}
			}
		}
		
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof HseIncident) {
			HseIncident hseIncident = (HseIncident) object;
			//auto select rig
			if (StringUtils.isNotBlank(userSelection.getRigInformationUid())) {
				hseIncident.setRigInformationUid(userSelection.getRigInformationUid());
			}
		}
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
		
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
	//	Object object = node.getData();
	//	if (object instanceof HseIncident) {
	//		HseIncident hseIncident = (HseIncident) object;
	//		hseIncident.setNumberOfIncidents(1);
	//	}
		
		Object object = node.getData();
		
		if (object instanceof HseIncident) {
			
			HseIncident hseIncident = (HseIncident) object;
			if (userSelection.getDailyUid() != null){
				Daily today = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());		
				if(today!=null)
				{
					Date todayDate = today.getDayDate();
					Date defaultDateTime = DateUtils.addHours(todayDate, 0);				
						
					hseIncident.setHseEventdatetime(defaultDateTime);		
				}
				ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), userSelection.getDailyUid());
				if (reportDaily!=null) {
					node.getDynaAttr().put("enteredBy", reportDaily.getSupervisor());
				}
			}
		}		
	}
	public void onDataNodeCarryForwardFromYesterday(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception{
		
		Object object = node.getData();
		
		if (object instanceof HseIncident) {
			HseIncident hseIncident = (HseIncident) object;
			if("0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "retainIncidentDateToWhenCarryOver"))){
				Daily today = ApplicationUtils.getConfiguredInstance().getCachedDaily(session.getCurrentDailyUid());			
				if(today!=null)
				{
					Date todayDate = today.getDayDate();
					Date defaultDateTime = DateUtils.addHours(todayDate, 0);
					hseIncident.setHseEventdatetime(defaultDateTime);
				}
			}
			if("0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "carryForwardHseNumberOfIncidents")))
				hseIncident.setNumberOfIncidents(0);
		}
	}	

    public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception {
		Object object = targetNode.getData();
		if (object instanceof HseIncident) {
		   HseIncident hseIncident = (HseIncident) object;
		   if("0".equals(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), "retainIncidentDateToWhenCarryOver"))){
			    Daily today = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSession.getCurrentDailyUid());   
			    if(today!=null)
			    {
				     Date todayDate = today.getDayDate();
				     Date defaultDateTime = DateUtils.addHours(todayDate, 0);
				     hseIncident.setHseEventdatetime(defaultDateTime);
			    }
		   }
		   if("0".equals(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), "carryForwardHseNumberOfIncidents")))
				hseIncident.setNumberOfIncidents(0);
		}
    }

    private boolean validateUniqueStringField(CommandBeanTreeNode sourceNode, DataNodeProcessStatus status, String fieldName, String uniqueValue, String errorMsg) throws Exception {
        CommandBeanTreeNode parentNode = sourceNode.getParent();
        Object object = sourceNode.getData();
        int counter = 0;

        for (Iterator i = parentNode.getList().values().iterator(); i.hasNext();) {
            Map items = (Map) i.next();
            for (Iterator j = items.values().iterator(); j.hasNext();){
                CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
                if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(object.getClass())) {
                    String fieldValue = PropertyUtils.getProperty(childNode.getData(), fieldName).toString();
                    if (StringUtils.equals(uniqueValue, fieldValue)) {
                        counter++;
                        if (counter > 1) {
                            break;
                        }
                    }
                }
            }
        }
        if (counter > 1) {
            if (StringUtils.equals(PropertyUtils.getProperty(object, fieldName).toString(), uniqueValue)) {
                status.setFieldError(sourceNode, fieldName, errorMsg);
                status.setContinueProcess(false, true);
            } else {
                status.setContinueProcess(false);
            }
        }
        return counter <= 1;
    }
}
