package com.idsdatanet.d2.drillnet.reportDaily;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * OSL Cuztomized Cost Calculation
 * @author Jack
 *
 */
public class OSLDailyCostReportGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		
	}

	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//NumberFormat formatter = new DecimalFormat("#0.00");
		
		Date rigOnHireStartDate = null;
		Date onlocDateOnlocTime = null;
		Date spudStartDate = null;
		Date dryHoleDate = null;
		Date completionStartDate = null;
		Date completionEndDate = null;
		Date pnaStartDate = null;
		Date pnaEndDate = null;
		Date wellTestingStartDate = null;
		Date wellTestingEndDate = null;
		//Date wkoStartDate = null;
		Date rigOffHireStartDate = null;
		Double preAfeRigMove = 0.0;
		Double preAfeDrilling = 0.0;
		Double preAfeCompletion = 0.0;
		Double preAfePna = 0.0;
		String sidetrackAfeNumber = null;
		String rigMoveAfeNumber = null;
		String completionAfeNumber = null;
		String pnaAfeNumber = null;
		String wellTestingAfeNumber = null;
		String drillingAfeNumber = null;
		String currentAfeNumber = null;
		
		Double afebudgetMobdemob = 0.0;
		Double afebudgetDrilling = 0.0;
		Double afebudgetCompletion = 0.0;
		Double afebudgetTesting = 0.0;
		Double afebudgetIntervention = 0.0;
		Double currentAfeBudget = 0.0;
		
		String startDate = null;
		String startDateEpochMS = null;
		String endDate = null;
		String endDateEpochMS = null;
		
		Double dailyRigMoveCost = 0.0;
		Double dailyDrllgCost = 0.0;
		Double dailyCmpltCost = 0.0;
		Double dailyPNACost = 0.0;
		Double dailyWellTestingCost = 0.0;
		Double dailyWkoCost = 0.0;
		
		Double cumcostRigmove = 0.0;
		Double cumcostDrilling = 0.0;
		Double cumcostCompletion = 0.0;
		Double cumcostPna = 0.0;
		Double cumcostWellTesting = 0.0;
		Double cumcostWko = 0.0;
		String currencyUomSymbol = "";
		
		String currentDailyUid = userContext.getUserSelection().getDailyUid();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		
		if (currentDailyUid == null || currentOperationUid == null) return;
		
		Daily currentDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (currentDaily==null) return;
		Date todayDate = currentDaily.getDayDate();
		
		if (reportType==null) {
			reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(currentOperationUid);
		}
		
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "daycost");
		if (thisConverter.isUOMMappingAvailable()) {	
			currencyUomSymbol=thisConverter.getUomSymbol();
		}
		
		Operation currentOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(currentOperationUid);
		if (currentOperation==null) return;
		
		if (currentOperation.getRigOnHireDate() != null) rigOnHireStartDate = currentOperation.getRigOnHireDate(); //Start RM
		if (currentOperation.getOnlocDateOnlocTime() != null) onlocDateOnlocTime = currentOperation.getOnlocDateOnlocTime(); //End RM
		if (currentOperation.getSpudDate() != null) spudStartDate = currentOperation.getSpudDate(); //END RM - START DRLLG
		if (currentOperation.getDryHoleEndDateTime() != null) dryHoleDate = currentOperation.getDryHoleEndDateTime(); //END DRLLG
		
		if (currentOperation.getCompletionStartDateTime() != null) completionStartDate = currentOperation.getCompletionStartDateTime(); //START CMPLT
		if (currentOperation.getCompletionEndDateTime() != null) completionEndDate = currentOperation.getCompletionEndDateTime(); //END CMPLT
		
		if (currentOperation.getPnaStartDateTime() != null) pnaStartDate = currentOperation.getPnaStartDateTime(); //START PNA
		if (currentOperation.getPnaEndDateTime() != null) pnaEndDate = currentOperation.getPnaEndDateTime(); //END PNA
		
		if (currentOperation.getWellTestingStartDateTime() != null) wellTestingStartDate = currentOperation.getWellTestingStartDateTime(); //START WELL TESTING
		if (currentOperation.getWellTestingEndDateTime() != null) wellTestingEndDate = currentOperation.getWellTestingEndDateTime(); //END WELL TESTING
		
		if (currentOperation.getRigOffHireDate() != null) rigOffHireStartDate = currentOperation.getRigOffHireDate();
		
		//if (currentOperation.getStartDate() != null) wkoStartDate = currentOperation.getStartDate();
		
		if (currentOperation.getPrevAfeRigMove() != null) preAfeRigMove = currentOperation.getPrevAfeRigMove();
		if (currentOperation.getPrevAfeDrilling() != null) preAfeDrilling = currentOperation.getPrevAfeDrilling();
		if (currentOperation.getPrevAfeCompletion() != null) preAfeCompletion = currentOperation.getPrevAfeCompletion();
		if (currentOperation.getPrevAfePna() != null) preAfePna = currentOperation.getPrevAfePna();
		if (currentOperation.getSidetrackAfeNumber() != null) sidetrackAfeNumber = currentOperation.getSidetrackAfeNumber();
		if (currentOperation.getRigMoveAfeNumber() != null) rigMoveAfeNumber = currentOperation.getRigMoveAfeNumber();
		if (currentOperation.getDrillingAfeNumber() != null) drillingAfeNumber = currentOperation.getDrillingAfeNumber();
		if (currentOperation.getCompletionAfeNumber() != null) completionAfeNumber = currentOperation.getCompletionAfeNumber();
		if (currentOperation.getPnaAfeNumber() != null) pnaAfeNumber = currentOperation.getPnaAfeNumber();
		if (currentOperation.getWellTestingAfeNumber() != null) wellTestingAfeNumber = currentOperation.getWellTestingAfeNumber();
		if (currentOperation.getAfebudgetMobdemob() != null) afebudgetMobdemob = currentOperation.getAfebudgetMobdemob();
		if (currentOperation.getAfebudgetDrilling() != null) afebudgetDrilling = currentOperation.getAfebudgetDrilling();
		if (currentOperation.getAfebudgetCompletion() != null) afebudgetCompletion = currentOperation.getAfebudgetCompletion();
		if (currentOperation.getAfebudgetTesting() != null) afebudgetTesting = currentOperation.getAfebudgetTesting();
		if (currentOperation.getAfebudgetIntervention() != null) afebudgetIntervention = currentOperation.getAfebudgetIntervention();
		if (currentOperation.getAfeNumber() != null) currentAfeNumber = currentOperation.getAfeNumber();
		if (currentOperation.getAfe() != null) currentAfeBudget = currentOperation.getAfe();
		
		/*-------------------------------------------SPUD DATE------------------------------------------*/
		Calendar thisSpudDateTimeObj = Calendar.getInstance();
		Calendar thisSpudDateObj = Calendar.getInstance();
		
		if (spudStartDate!=null)
		{
			thisSpudDateTimeObj.setTime(spudStartDate);
			
			thisSpudDateObj.set(Calendar.YEAR, thisSpudDateTimeObj.get(Calendar.YEAR));
			thisSpudDateObj.set(Calendar.MONTH, thisSpudDateTimeObj.get(Calendar.MONTH));
			thisSpudDateObj.set(Calendar.DAY_OF_MONTH, thisSpudDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisSpudDateObj.set(Calendar.HOUR_OF_DAY, 0);
			thisSpudDateObj.set(Calendar.MINUTE, 0);
			thisSpudDateObj.set(Calendar.SECOND, 0);
			thisSpudDateObj.set(Calendar.MILLISECOND, 0);
		}
		
		/*-------------------------------------------RIG ON HIRE DATE------------------------------------------*/
		Calendar thisRigOnHireDateTimeObj = Calendar.getInstance();
		Calendar thisRigOnHireObj = Calendar.getInstance();
		
		if (rigOnHireStartDate!=null)
		{
			thisRigOnHireDateTimeObj.setTime(rigOnHireStartDate);
		
			thisRigOnHireObj.set(Calendar.YEAR, thisRigOnHireDateTimeObj.get(Calendar.YEAR));
			thisRigOnHireObj.set(Calendar.MONTH, thisRigOnHireDateTimeObj.get(Calendar.MONTH));
			thisRigOnHireObj.set(Calendar.DAY_OF_MONTH, thisRigOnHireDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisRigOnHireObj.set(Calendar.HOUR_OF_DAY, 0);
			thisRigOnHireObj.set(Calendar.MINUTE, 0);
			thisRigOnHireObj.set(Calendar.SECOND, 0);
			thisRigOnHireObj.set(Calendar.MILLISECOND, 0);
		}
		
		/*-------------------------------------------On Loc Date On Loc Time------------------------------------------*/
		Calendar thisOnlocDateOnlocTimeObj = Calendar.getInstance();
		Calendar thisOnlocDateOnlocObj = Calendar.getInstance();
		
		if (onlocDateOnlocTime!=null)
		{
			thisOnlocDateOnlocTimeObj.setTime(onlocDateOnlocTime);
		
			thisOnlocDateOnlocObj.set(Calendar.YEAR, thisOnlocDateOnlocTimeObj.get(Calendar.YEAR));
			thisOnlocDateOnlocObj.set(Calendar.MONTH, thisOnlocDateOnlocTimeObj.get(Calendar.MONTH));
			thisOnlocDateOnlocObj.set(Calendar.DAY_OF_MONTH, thisOnlocDateOnlocTimeObj.get(Calendar.DAY_OF_MONTH));
			thisOnlocDateOnlocObj.set(Calendar.HOUR_OF_DAY, 0);
			thisOnlocDateOnlocObj.set(Calendar.MINUTE, 0);
			thisOnlocDateOnlocObj.set(Calendar.SECOND, 0);
			thisOnlocDateOnlocObj.set(Calendar.MILLISECOND, 0);
		}
		
		/*-------------------------------------------DRY HOLE DATE------------------------------------------*/		
		Calendar thisDryHoleDateTimeObj = Calendar.getInstance();
		Calendar thisDryHoleObj = Calendar.getInstance();		
		if (dryHoleDate!=null)
		{
			thisDryHoleDateTimeObj.setTime(dryHoleDate);
			
			thisDryHoleObj.set(Calendar.YEAR, thisDryHoleDateTimeObj.get(Calendar.YEAR));
			thisDryHoleObj.set(Calendar.MONTH, thisDryHoleDateTimeObj.get(Calendar.MONTH));
			thisDryHoleObj.set(Calendar.DAY_OF_MONTH, thisDryHoleDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisDryHoleObj.set(Calendar.HOUR_OF_DAY, 0);
			thisDryHoleObj.set(Calendar.MINUTE, 0);
			thisDryHoleObj.set(Calendar.SECOND, 0);
			thisDryHoleObj.set(Calendar.MILLISECOND, 0);
		}
		
		/*-------------------------------------------RIG RELEASE DATE------------------------------------------*/		
		Calendar thisRigOffDateTimeObj = Calendar.getInstance();
		Calendar thisRigOffObj = Calendar.getInstance();		
		if (rigOffHireStartDate!=null)
		{
			thisRigOffDateTimeObj.setTime(rigOffHireStartDate);
			
			thisRigOffObj.set(Calendar.YEAR, thisRigOffDateTimeObj.get(Calendar.YEAR));
			thisRigOffObj.set(Calendar.MONTH, thisRigOffDateTimeObj.get(Calendar.MONTH));
			thisRigOffObj.set(Calendar.DAY_OF_MONTH, thisRigOffDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisRigOffObj.set(Calendar.HOUR_OF_DAY, 0);
			thisRigOffObj.set(Calendar.MINUTE, 0);
			thisRigOffObj.set(Calendar.SECOND, 0);
			thisRigOffObj.set(Calendar.MILLISECOND, 0);
		}
		
		/*-------------------------------------------COMPLETION START DATE------------------------------------------*/		
		Calendar thisCmpltStartDateTimeObj = Calendar.getInstance();
		Calendar thisCmpltStartObj = Calendar.getInstance();		
		if (completionStartDate!=null)
		{
			thisCmpltStartDateTimeObj.setTime(completionStartDate);
			
			thisCmpltStartObj.set(Calendar.YEAR, thisCmpltStartDateTimeObj.get(Calendar.YEAR));
			thisCmpltStartObj.set(Calendar.MONTH, thisCmpltStartDateTimeObj.get(Calendar.MONTH));
			thisCmpltStartObj.set(Calendar.DAY_OF_MONTH, thisCmpltStartDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisCmpltStartObj.set(Calendar.HOUR_OF_DAY, 0);
			thisCmpltStartObj.set(Calendar.MINUTE, 0);
			thisCmpltStartObj.set(Calendar.SECOND, 0);
			thisCmpltStartObj.set(Calendar.MILLISECOND, 0);
		}
		
		/*-------------------------------------------COMPLETION END DATE------------------------------------------*/		
		Calendar thisCmpltEndDateTimeObj = Calendar.getInstance();
		Calendar thisCmpltEndObj = Calendar.getInstance();		
		if (completionEndDate!=null)
		{
			thisCmpltEndDateTimeObj.setTime(completionEndDate);
			
			thisCmpltEndObj.set(Calendar.YEAR, thisCmpltEndDateTimeObj.get(Calendar.YEAR));
			thisCmpltEndObj.set(Calendar.MONTH, thisCmpltEndDateTimeObj.get(Calendar.MONTH));
			thisCmpltEndObj.set(Calendar.DAY_OF_MONTH, thisCmpltEndDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisCmpltEndObj.set(Calendar.HOUR_OF_DAY, 0);
			thisCmpltEndObj.set(Calendar.MINUTE, 0);
			thisCmpltEndObj.set(Calendar.SECOND, 0);
			thisCmpltEndObj.set(Calendar.MILLISECOND, 0);
		}
		
		/*-------------------------------------------PNA START DATE------------------------------------------*/		
		Calendar thisPNAStartDateTimeObj = Calendar.getInstance();
		Calendar thisPNAStartObj = Calendar.getInstance();		
		if (pnaStartDate!=null)
		{
			thisPNAStartDateTimeObj.setTime(pnaStartDate);
			
			thisPNAStartObj.set(Calendar.YEAR, thisPNAStartDateTimeObj.get(Calendar.YEAR));
			thisPNAStartObj.set(Calendar.MONTH, thisPNAStartDateTimeObj.get(Calendar.MONTH));
			thisPNAStartObj.set(Calendar.DAY_OF_MONTH, thisPNAStartDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisPNAStartObj.set(Calendar.HOUR_OF_DAY, 0);
			thisPNAStartObj.set(Calendar.MINUTE, 0);
			thisPNAStartObj.set(Calendar.SECOND, 0);
			thisPNAStartObj.set(Calendar.MILLISECOND, 0);
		}
		
		/*-------------------------------------------PNA END DATE------------------------------------------*/		
		Calendar thisPNAEndDateTimeObj = Calendar.getInstance();
		Calendar thisPNAEndObj = Calendar.getInstance();		
		if (pnaEndDate!=null)
		{
			thisPNAEndDateTimeObj.setTime(pnaEndDate);
			
			thisPNAEndObj.set(Calendar.YEAR, thisPNAEndDateTimeObj.get(Calendar.YEAR));
			thisPNAEndObj.set(Calendar.MONTH, thisPNAEndDateTimeObj.get(Calendar.MONTH));
			thisPNAEndObj.set(Calendar.DAY_OF_MONTH, thisPNAEndDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisPNAEndObj.set(Calendar.HOUR_OF_DAY, 0);
			thisPNAEndObj.set(Calendar.MINUTE, 0);
			thisPNAEndObj.set(Calendar.SECOND, 0);
			thisPNAEndObj.set(Calendar.MILLISECOND, 0);
		}
		
		/*-------------------------------------------WELL TESTING START DATE------------------------------------------*/		
		Calendar thisWellTestingStartDateTimeObj = Calendar.getInstance();
		Calendar thisWellTestingStartObj = Calendar.getInstance();		
		if (wellTestingStartDate!=null)
		{
			thisWellTestingStartDateTimeObj.setTime(wellTestingStartDate);
			
			thisWellTestingStartObj.set(Calendar.YEAR, thisWellTestingStartDateTimeObj.get(Calendar.YEAR));
			thisWellTestingStartObj.set(Calendar.MONTH, thisWellTestingStartDateTimeObj.get(Calendar.MONTH));
			thisWellTestingStartObj.set(Calendar.DAY_OF_MONTH, thisWellTestingStartDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisWellTestingStartObj.set(Calendar.HOUR_OF_DAY, 0);
			thisWellTestingStartObj.set(Calendar.MINUTE, 0);
			thisWellTestingStartObj.set(Calendar.SECOND, 0);
			thisWellTestingStartObj.set(Calendar.MILLISECOND, 0);
		}
		
		/*-------------------------------------------WELL TESTING END DATE------------------------------------------*/		
		Calendar thisWellTestingEndDateTimeObj = Calendar.getInstance();
		Calendar thisWellTestingEndObj = Calendar.getInstance();		
		if (wellTestingEndDate!=null)
		{
			thisWellTestingEndDateTimeObj.setTime(wellTestingEndDate);
			
			thisWellTestingEndObj.set(Calendar.YEAR, thisWellTestingEndDateTimeObj.get(Calendar.YEAR));
			thisWellTestingEndObj.set(Calendar.MONTH, thisWellTestingEndDateTimeObj.get(Calendar.MONTH));
			thisWellTestingEndObj.set(Calendar.DAY_OF_MONTH, thisWellTestingEndDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisWellTestingEndObj.set(Calendar.HOUR_OF_DAY, 0);
			thisWellTestingEndObj.set(Calendar.MINUTE, 0);
			thisWellTestingEndObj.set(Calendar.SECOND, 0);
			thisWellTestingEndObj.set(Calendar.MILLISECOND, 0);
		}
		
		/*-------------------------------------------WKO START DATE------------------------------------------		
		Calendar thisWkoDateTimeObj = Calendar.getInstance();
		Calendar thisWkoObj = Calendar.getInstance();		
		if (wkoStartDate!=null)
		{
			thisWkoDateTimeObj.setTime(wkoStartDate);
			
			thisWkoObj.set(Calendar.YEAR, thisWkoDateTimeObj.get(Calendar.YEAR));
			thisWkoObj.set(Calendar.MONTH, thisWkoDateTimeObj.get(Calendar.MONTH));
			thisWkoObj.set(Calendar.DAY_OF_MONTH, thisWkoDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisWkoObj.set(Calendar.HOUR_OF_DAY, 0);
			thisWkoObj.set(Calendar.MINUTE, 0);
			thisWkoObj.set(Calendar.SECOND, 0);
			thisWkoObj.set(Calendar.MILLISECOND, 0);
		}	
		*/
		/*-------------------------------------------WKO END DATE------------------------------------------		
		Calendar thisWkoEndDateTimeObj = Calendar.getInstance();
		Calendar thisWkoEndObj = Calendar.getInstance();		
		if (wkoStartDate!=null)
		{
			thisWkoEndDateTimeObj.setTime(thisWkoEndDateTimeObj.getTime());
			
			thisWkoEndObj.set(Calendar.YEAR, thisWkoEndDateTimeObj.get(Calendar.YEAR));
			thisWkoEndObj.set(Calendar.MONTH, thisWkoEndDateTimeObj.get(Calendar.MONTH));
			thisWkoEndObj.set(Calendar.DAY_OF_MONTH, thisWkoEndDateTimeObj.get(Calendar.DAY_OF_MONTH+1));
			thisWkoEndObj.set(Calendar.HOUR_OF_DAY, 0);
			thisWkoEndObj.set(Calendar.MINUTE, 0);
			thisWkoEndObj.set(Calendar.SECOND, 0);
			thisWkoEndObj.set(Calendar.MILLISECOND, 0);
		}
		*/
		/*-------------------------------------------REPORT TIME------------------------------------------*/
		Calendar thisReportTimeObj = Calendar.getInstance();
		thisReportTimeObj.setTimeInMillis(0);	
		thisReportTimeObj.set(Calendar.HOUR_OF_DAY, 0);
		thisReportTimeObj.set(Calendar.MINUTE, 0);
		
		
		/**
		 * Rig Move Cost logic
		 * From RigOnHireDate until SpudDate counted as Rig Move
		 */
		/*------------------------------------------ RM Phase ------------------------------------------*/
		//DAILY RM COST
		if (StringUtils.isNotBlank(rigMoveAfeNumber)) {
			Date rigMobEndDateTime = null;
			Calendar thisRigMobEndDateTimeObj = Calendar.getInstance();
			Calendar thisRigMobEndDateObj = Calendar.getInstance();
			
			if(StringUtils.equalsIgnoreCase(reportType, "CMPLT") || StringUtils.equalsIgnoreCase(reportType, "WKO")){
				// to get Operation.completionStartDate as Rig Mobalization End Date Time
				if (completionStartDate!=null){
					rigMobEndDateTime = completionStartDate;
					thisRigMobEndDateTimeObj.setTime(completionStartDate);
				}
			}else{
				// to get Operation.spudDate as Rig Mobalization End Date Time
				if(spudStartDate!=null){
					rigMobEndDateTime = spudStartDate;
					thisRigMobEndDateTimeObj.setTime(spudStartDate);
				}
			}
			
			thisRigMobEndDateObj.set(Calendar.YEAR, thisRigMobEndDateTimeObj.get(Calendar.YEAR));
			thisRigMobEndDateObj.set(Calendar.MONTH, thisRigMobEndDateTimeObj.get(Calendar.MONTH));
			thisRigMobEndDateObj.set(Calendar.DAY_OF_MONTH, thisRigMobEndDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisRigMobEndDateObj.set(Calendar.HOUR_OF_DAY, 0);
			thisRigMobEndDateObj.set(Calendar.MINUTE, 0);
			thisRigMobEndDateObj.set(Calendar.SECOND, 0);
			thisRigMobEndDateObj.set(Calendar.MILLISECOND, 0);
			
			if ((rigOnHireStartDate != null) && (thisRigOnHireObj.getTime().compareTo(todayDate) <= 0) && (thisRigMobEndDateObj.getTime().compareTo(todayDate) >= 0 || (rigMobEndDateTime == null))) {
				if (thisRigMobEndDateObj.getTime().compareTo(todayDate) == 0) {
					dailyRigMoveCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisRigMobEndDateObj.getTime(), rigMobEndDateTime, todayDate, thisReportTimeObj.getTime());
				} else if ((thisRigOnHireObj.getTime().compareTo(todayDate) >= 0 || (rigOnHireStartDate == null))) {
					dailyRigMoveCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, thisRigOnHireObj.getTime(), rigOnHireStartDate);
				} else {
					dailyRigMoveCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, todayDate, thisReportTimeObj.getTime());
				}
			}
			
			//CUM RIG MOVE COST
			/*if (StringUtils.isNotBlank(sidetrackAfeNumber)) {
				if (StringUtils.isNotBlank(preAfeRigMove.toString()))
				{
					cumcostRigmove = preAfeRigMove;
				}
			} else {
				cumcostRigmove = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisSpudDateObj.getTime(), spudStartDate, thisRigOnHireObj.getTime(), rigOnHireStartDate);
			}*/
		
			cumcostRigmove = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisRigMobEndDateObj.getTime(), rigMobEndDateTime, thisRigOnHireObj.getTime(), rigOnHireStartDate);
			cumcostRigmove = cumcostRigmove + preAfeRigMove;
			
			//RM Phase WRITE TO XML
			Long rigOnHireEpochMS = (rigOnHireStartDate!=null)?rigOnHireStartDate.getTime():0;
			Long rigMobEndEpochMS = (rigMobEndDateTime!=null)?rigMobEndDateTime.getTime():0;
			startDate = (rigOnHireStartDate!=null)?rigOnHireStartDate.toString():"";
			startDateEpochMS = (rigOnHireStartDate!=null)?rigOnHireEpochMS.toString():"";
			endDate = (rigMobEndDateTime!=null)?rigMobEndDateTime.toString():"";
			endDateEpochMS = (rigMobEndDateTime!=null)?rigMobEndEpochMS.toString():"";
			this.createReportNode(reportDataNode, currencyUomSymbol, rigMoveAfeNumber, afebudgetMobdemob, startDate, startDateEpochMS, endDate, endDateEpochMS, dailyRigMoveCost, cumcostRigmove, "RM");
			
		}
		
		/*------------------------------------------ END RM Phase ------------------------------------------*/
		
		/**
		 * Drilling Cost logic
		 * From SpudDate until DryholeDate counted as Drilling
		 */
		/*------------------------------------------ START DRLLG Phase ------------------------------------------*/
		//DAILY DRILLING COST
		if (StringUtils.isNotBlank(drillingAfeNumber)) {
			if ((spudStartDate != null) && (thisSpudDateObj.getTime().compareTo(todayDate) <= 0) && (thisDryHoleObj.getTime().compareTo(todayDate) >= 0 || (dryHoleDate == null))) {
				if (thisDryHoleObj.getTime().compareTo(todayDate) == 0) {
					dailyDrllgCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisDryHoleObj.getTime(), dryHoleDate, todayDate, thisReportTimeObj.getTime());
				} else if ((thisSpudDateObj.getTime().compareTo(todayDate) >= 0 || (spudStartDate == null))) {
					dailyDrllgCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, thisSpudDateObj.getTime(), spudStartDate);
				} else {
					dailyDrllgCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, todayDate, thisReportTimeObj.getTime());
				}
			}
		
			//CUM DRILLING COST
			/*if (StringUtils.isNotBlank(sidetrackAfeNumber)) {
				if (StringUtils.isNotBlank(preAfeDrilling.toString())) {
					cumcostDrilling = preAfeDrilling;
				}
			} else {
				cumcostDrilling = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisDryHoleObj.getTime(), dryHoleDate, thisSpudDateObj.getTime(), spudStartDate);
			}*/
		
		
			cumcostDrilling = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisDryHoleObj.getTime(), dryHoleDate, thisSpudDateObj.getTime(), spudStartDate);
			cumcostDrilling = cumcostDrilling + preAfeDrilling;
			
			//DRLLG WRITE TO XML
			Long spudStartEpochMS = (spudStartDate!=null)?spudStartDate.getTime():0;
			Long dryHoleStartEpochMS = (dryHoleDate!=null)?dryHoleDate.getTime():0;
			startDate = (spudStartDate!=null)?spudStartDate.toString():"";
			startDateEpochMS = (spudStartDate!=null)?spudStartEpochMS.toString():"";
			endDate = (dryHoleDate!=null)?dryHoleDate.toString():"";
			endDateEpochMS = (dryHoleDate!=null)?dryHoleStartEpochMS.toString():"";		
			this.createReportNode(reportDataNode, currencyUomSymbol, drillingAfeNumber, afebudgetDrilling, startDate, startDateEpochMS, endDate, endDateEpochMS, dailyDrllgCost, cumcostDrilling, "DRLLG");
		}
		
		
		/*------------------------------------------ END DRLLG Phase ------------------------------------------*/
		
		/**
		 * Completion Cost Logic
		 * From Completion Start date until Completion End Date
		 */
		/*------------------------------------------ Start CMPLT Phase ------------------------------------------*/
		//DAILY CMPLT COST
		if (StringUtils.isNotBlank(completionAfeNumber) && !StringUtils.equalsIgnoreCase(reportType, "WKO")) {
			if ((completionStartDate != null) && (thisCmpltStartObj.getTime().compareTo(todayDate) <= 0) && (thisCmpltEndObj.getTime().compareTo(todayDate) >= 0 || (completionEndDate == null))) {
				if (thisCmpltEndObj.getTime().compareTo(todayDate) == 0) {
					dailyCmpltCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisCmpltEndObj.getTime(), completionEndDate, todayDate, thisReportTimeObj.getTime());
				} else if ((thisCmpltStartObj.getTime().compareTo(todayDate) >= 0 || (completionStartDate == null))) {
					dailyCmpltCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, thisCmpltStartObj.getTime(), completionStartDate);
				} else {
					dailyCmpltCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, todayDate, thisReportTimeObj.getTime());
				}
			}
		
			//CUM COMPLETION COST
			cumcostCompletion = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisCmpltEndObj.getTime(), completionEndDate, thisCmpltStartObj.getTime(), completionStartDate);
			cumcostCompletion = cumcostCompletion + preAfeCompletion;
			
			//CMPLT WRITE TO XML
			Long completionStartEpochMS = (completionStartDate!=null)?completionStartDate.getTime():0;
			Long completionEndEpochMS = (completionEndDate!=null)?completionEndDate.getTime():0;
			startDate = (completionStartDate!=null)?completionStartDate.toString():"";
			startDateEpochMS = (completionStartDate!=null)?completionStartEpochMS.toString():"";
			endDate = (completionEndDate!=null)?completionEndDate.toString():"";
			endDateEpochMS = (completionEndDate!=null)?completionEndEpochMS.toString():"";
			this.createReportNode(reportDataNode, currencyUomSymbol, completionAfeNumber, afebudgetCompletion, startDate, startDateEpochMS, endDate, endDateEpochMS, dailyCmpltCost, cumcostCompletion, "CMPLT");
		}
		
		/*------------------------------------------ END CMPLT Phase ------------------------------------------*/
		
		/**
		 * Plug and Abandon Cost Logic
		 * From PNA Start date until PNA End Date
		 */
		/*------------------------------------------ Start PNA Phase ------------------------------------------*/
		//DAILY PNA COST
		if (StringUtils.isNotBlank(pnaAfeNumber)) {
			if ((pnaStartDate != null) && (thisPNAStartObj.getTime().compareTo(todayDate) <= 0) && (thisPNAEndObj.getTime().compareTo(todayDate) >= 0 || (pnaEndDate == null))) {
				if (thisPNAEndObj.getTime().compareTo(todayDate) == 0) {
					dailyPNACost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisPNAEndObj.getTime(), pnaEndDate, todayDate, thisReportTimeObj.getTime());
				} else if ((thisPNAStartObj.getTime().compareTo(todayDate) >= 0 || (pnaStartDate == null))) {
					dailyPNACost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, thisPNAStartObj.getTime(), pnaStartDate);
				} else {
					dailyPNACost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, todayDate, thisReportTimeObj.getTime());
				}
			}
			
			//CUM PNA COST
		
			cumcostPna = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisPNAEndObj.getTime(), pnaEndDate, thisPNAStartObj.getTime(), pnaStartDate);
			cumcostPna = cumcostPna + preAfePna;
			
			//PNA WRITE TO XML
			Long pnaStartEpochMS = (pnaStartDate!=null)?pnaStartDate.getTime():0;
			Long pnaEndEpochMS = (pnaEndDate!=null)?pnaEndDate.getTime():0;
			startDate = (pnaStartDate!=null)?pnaStartDate.toString():"";
			startDateEpochMS = (pnaStartDate!=null)?pnaStartEpochMS.toString():"";
			endDate = (pnaEndDate!=null)?pnaEndDate.toString():"";
			endDateEpochMS = (pnaEndDate!=null)?pnaEndEpochMS.toString():"";
			this.createReportNode(reportDataNode, currencyUomSymbol, pnaAfeNumber, afebudgetIntervention, startDate, startDateEpochMS, endDate, endDateEpochMS, dailyPNACost, cumcostPna, "PNA");
		}
		
		
		/*------------------------------------------ END PNA Phase ------------------------------------------*/
		
		/**
		 * Well Testing Cost Logic
		 * From Well Testing Start date until Well Testing End Date
		 */
		/*------------------------------------------ Start WELL TESTING Phase ------------------------------------------*/
		//WELL TESTING Phase
		if (StringUtils.isNotBlank(wellTestingAfeNumber)) {
			if ((wellTestingStartDate != null) && (thisWellTestingStartObj.getTime().compareTo(todayDate) <= 0) && (thisWellTestingEndObj.getTime().compareTo(todayDate) >= 0 || (wellTestingEndDate == null))) {
				if (thisWellTestingEndObj.getTime().compareTo(todayDate) == 0) {
					dailyWellTestingCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisWellTestingEndObj.getTime(), wellTestingEndDate, todayDate, thisReportTimeObj.getTime());
				} else if ((thisWellTestingStartObj.getTime().compareTo(todayDate) >= 0 || (wellTestingStartDate == null))) {
					dailyWellTestingCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, thisWellTestingStartObj.getTime(), wellTestingStartDate);
				}
				else {
					dailyWellTestingCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, todayDate, thisReportTimeObj.getTime());
				}
			}
			//CUM WELL TESTING COST
		
			cumcostWellTesting = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisWellTestingEndObj.getTime(), wellTestingEndDate, thisWellTestingStartObj.getTime(), wellTestingStartDate);
			
			//WT Write to XML
			Long wellTestingStartEpochMS = (wellTestingStartDate!=null)?wellTestingStartDate.getTime():0;
			Long wellTestingEndEpochMS = (wellTestingEndDate!=null)?wellTestingEndDate.getTime():0;
			startDate = (wellTestingStartDate!=null)?wellTestingStartDate.toString():"";
			startDateEpochMS = (wellTestingStartDate!=null)?wellTestingStartEpochMS.toString():"";
			endDate = (wellTestingEndDate!=null)?wellTestingEndDate.toString():"";
			endDateEpochMS = (wellTestingEndDate!=null)?wellTestingEndEpochMS.toString():"";
			this.createReportNode(reportDataNode, currencyUomSymbol, wellTestingAfeNumber, afebudgetTesting, startDate, startDateEpochMS, endDate, endDateEpochMS, dailyWellTestingCost, cumcostWellTesting, "WellTesting");
		}
		/*------------------------------------------ END WELL TESTING Phase ------------------------------------------*/
		/*------------------------------------------ Start WKO Phase ------------------------------------------*/
		//WKO Phase
		if (StringUtils.isNotBlank(completionAfeNumber) && !StringUtils.equalsIgnoreCase(reportType, "CMPLT")) {
			if ((completionStartDate != null) && (thisCmpltStartObj.getTime().compareTo(todayDate) <= 0)) {
				if ((thisCmpltStartObj.getTime().compareTo(todayDate) >= 0 || (completionStartDate == null))) {
					dailyWkoCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, thisCmpltStartObj.getTime(), completionStartDate);
				}
				else {
					dailyWkoCost = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, null, null, todayDate, thisReportTimeObj.getTime());
				}
			}
			//CUM WKO COST
			cumcostWko = this.calculateCostByPhase(userContext, currentOperationUid, currentDailyUid, thisCmpltEndObj.getTime(), completionEndDate, thisCmpltStartObj.getTime(), completionStartDate);
			
			//WT Write to XML
			Long wkoStartEpochMS = (completionStartDate!=null)?completionStartDate.getTime():0;
			startDate = (completionStartDate!=null)?completionStartDate.toString():"";
			startDateEpochMS = (completionStartDate!=null)?wkoStartEpochMS.toString():"";
			endDate = "";
			endDateEpochMS = "";
			this.createReportNode(reportDataNode, currencyUomSymbol, completionAfeNumber, afebudgetCompletion, startDate, startDateEpochMS, endDate, endDateEpochMS, dailyWkoCost, cumcostWko, "WKO");
		}
	}
	/*------------------------------------------ END WKO Phase ------------------------------------------*/
	
	
	/**
	 * Method to form common reportDataNode
	 * @param reportDataNode
	 * @param uom
	 * @param afeNumber
	 * @param afeBudget
	 * @param startDate
	 * @param startEpochMS
	 * @param endDate
	 * @param endEpochMS
	 * @param daily
	 * @param cum
	 * @param phase
	 * @return
	 * @throws Exception
	 */
	private ReportDataNode createReportNode(ReportDataNode reportDataNode, String uom, String afeNumber, Double afeBudget, 
			String startDate, String startEpochMS, String endDate, String endEpochMS, Double daily, Double cum, String phase) throws Exception {
		
		NumberFormat formatter = new DecimalFormat("#0.00");
		
		ReportDataNode thisReportNode = reportDataNode.addChild("DailyCostByPhase");
		thisReportNode.addProperty("currencyUomSymbol", uom);
		thisReportNode.addProperty("startDate", startDate);
		thisReportNode.addProperty("startDateEpochMS", startEpochMS);
		thisReportNode.addProperty("endDate", endDate);
		thisReportNode.addProperty("endDateEpochMS", endEpochMS);
		thisReportNode.addProperty("dailyCost", formatter.format(daily));
		thisReportNode.addProperty("cumCost", formatter.format(cum));
		thisReportNode.addProperty("phase", phase);
		thisReportNode.addProperty("afeNumber", afeNumber);
		thisReportNode.addProperty("afeBudget", formatter.format(afeBudget));
		
		return thisReportNode;
		
	}
	
	/**
	 * method to calculate cost by each phase based on date
	 * @param userContext
	 * @param operationUid
	 * @param dailyUid
	 * @param endDate
	 * @param endTime
	 * @param startDate
	 * @param startTime
	 * @return
	 * @throws Exception
	 */
	private Double calculateCostByPhase(UserContext userContext, String operationUid, String dailyUid, Date endDate, Date endTime, Date startDate, Date startTime) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double dayCost = 0.0;
		
		Daily currentDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		if (currentDaily==null) return null;
		Date todayDate = currentDaily.getDayDate();
		
		if (endDate != null) {
			if (todayDate.after(endDate)) todayDate = endDate;
		}
		
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		
		List lstResult2 = null;
		if (startDate != null) {
			Double totalCost = 0.0;
			
			String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid AND reportType = :thisReportType AND reportDatetime >= :thisSpudDate AND reportDatetime <= :thisDayDate";
			String[] paramsFields = {"thisOperationUid", "thisReportType", "thisSpudDate", "thisDayDate"};
			Object[] paramsValues= {operationUid, reportType, startDate, todayDate};
			List<ReportDaily> listReportDaily= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			for (ReportDaily rd:listReportDaily)
			{
				String selectedDailyUid = rd.getDailyUid();
				Double selectedDaycost = 0.0;
				Double totalHrs = 0.0;
				Date selectedReportDatetime = rd.getReportDatetime();
				
				Calendar thisReportDateTimeObj = Calendar.getInstance();
				Calendar thisReportDateObj = Calendar.getInstance();
				
				thisReportDateTimeObj.setTime(selectedReportDatetime);
				
				thisReportDateObj.set(Calendar.YEAR, thisReportDateTimeObj.get(Calendar.YEAR));
				thisReportDateObj.set(Calendar.MONTH, thisReportDateTimeObj.get(Calendar.MONTH));
				thisReportDateObj.set(Calendar.DAY_OF_MONTH, thisReportDateTimeObj.get(Calendar.DAY_OF_MONTH));
				thisReportDateObj.set(Calendar.HOUR_OF_DAY, 0);
				thisReportDateObj.set(Calendar.MINUTE, 0);
				thisReportDateObj.set(Calendar.SECOND, 0);
				thisReportDateObj.set(Calendar.MILLISECOND, 0);
				
				if (rd.getDaycost()!=null) selectedDaycost = rd.getDaycost();
				
				//GET TOTAL ACTIVITY DURATION OF THE DAY
				String strSql1 = "SELECT SUM(activityDuration) FROM Activity " +
					"WHERE (isDeleted = false or isDeleted is null) " +
					"and (dayPlus IS NULL OR dayPlus=0) " +
					"AND dailyUid = :thisSelectedDailyUid " +
					"AND operationUid = :thisOperationUid " +
					"AND (isSimop=false or isSimop is null) " +
					"AND (isOffline=false or isOffline is null)";
				String[] paramsFields1 = {"thisOperationUid", "thisSelectedDailyUid"};
				Object[] paramsValues1 = {operationUid, selectedDailyUid};
				
				List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
				Object a = (Object) lstResult1.get(0);
				if (a != null) totalHrs = Double.parseDouble(a.toString());
				
				//GET TOTAL DURATION OF ACTIVITY THAT IS CATEGORIZED AS DRILLING
				//BY DEFAULT, IT WILL CALCULATION THE WHOLE DAY				
				String strSql4 = "SELECT SUM(activityDuration) FROM Activity " +
					"WHERE (isDeleted = false or isDeleted is null) " +
					"and (dayPlus IS NULL OR dayPlus=0) " +
					"AND dailyUid = :thisSelectedDailyUid " +
					"AND operationUid = :thisOperationUid " +
					"AND (isSimop=false or isSimop is null) " +
					"AND (isOffline=false or isOffline is null)";
				String[] paramsFields4 = {"thisOperationUid", "thisSelectedDailyUid"};
				Object[] paramsValues4 = {operationUid, selectedDailyUid};
				lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4, qp);
				
				if(startTime!=null) {
					if (thisReportDateObj.getTime().compareTo(startDate) == 0) {
						String strSql2 = "SELECT SUM(activityDuration) FROM Activity " +
							"WHERE (isDeleted = false or isDeleted is null) " +
							"and (dayPlus IS NULL OR dayPlus=0) " +
							"AND dailyUid = :thisSelectedDailyUid " +
							"AND TIME(startDatetime) >= TIME(:startTime) " +
							"AND operationUid = :thisOperationUid " +
							"AND (isSimop=false or isSimop is null) " +
							"AND (isOffline=false or isOffline is null)";
						String[] paramsFields2 = {"thisOperationUid", "thisSelectedDailyUid", "startTime"};
						Object[] paramsValues2 = {operationUid, selectedDailyUid, startTime};
						lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
					}
				}
				
				if(endTime!=null) {
					if (thisReportDateObj.getTime().compareTo(endDate) == 0) {
						String strSql3 = "SELECT SUM(activityDuration) FROM Activity " +
							"WHERE (isDeleted = false or isDeleted is null) " +
							"and (dayPlus IS NULL OR dayPlus=0) " +
							"AND dailyUid = :thisSelectedDailyUid " +
							"AND TIME(endDatetime) <= TIME(:endTime) " +
							"AND operationUid = :thisOperationUid " +
							"AND (isSimop=false or isSimop is null) " +
							"AND (isOffline=false or isOffline is null)";
						String[] paramsFields3 = {"thisOperationUid", "thisSelectedDailyUid", "endTime"};
						Object[] paramsValues3 = {operationUid, selectedDailyUid, endTime};
						lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3, qp);
					}
				}
				
				Double drllgHours = 0.00;
				
				if (lstResult2.size() > 0) {
					if (lstResult2.get(0) != null) {					
						if (lstResult2.get(0).toString() != null) drllgHours = Double.parseDouble(lstResult2.get(0).toString());
					}
				}
				
				//GET DAY COST
				if ((selectedDaycost > 0) && drllgHours > 0) {					
					totalCost = totalCost + (selectedDaycost*((drllgHours/3600)/(totalHrs/3600)));
				}
			}
			dayCost = totalCost;
		} else 
		{
			dayCost = 0.0;
		}
		
		return dayCost;
	}
}