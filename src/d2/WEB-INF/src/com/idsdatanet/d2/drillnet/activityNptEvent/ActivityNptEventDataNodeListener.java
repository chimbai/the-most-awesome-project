package com.idsdatanet.d2.drillnet.activityNptEvent;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ActivityNptEventDataNodeListener extends EmptyDataNodeListener{

	private Map<String, ReportDaily> reportDailyMap = new HashMap<String, ReportDaily>();
	private boolean checkSeverityDurationException = false;
	
	public boolean getCheckSeverityDurationException() {
		return checkSeverityDurationException;
	}
	
	public void setCheckSeverityDurationException(boolean checkSeverityDurationException){
		this.checkSeverityDurationException = checkSeverityDurationException;
	}
	
	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof UnwantedEvent) {
			UnwantedEvent unwantedEvent = (UnwantedEvent) node.getData();
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			String queryString = "select min(a.startDatetime), min(a.depthMdMsl), sum(a.activityDuration), min(a.depthFromMdMsl) From Activity a, Daily d WHERE (a.isDeleted=false or a.isDeleted is null) and (d.isDeleted=false or d.isDeleted is null) " +
								"and a.dailyUid=d.dailyUid and a.nptEventUid=:nptEventUid " +
								"AND (a.dayPlus IS NULL OR a.dayPlus=0)";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "nptEventUid", unwantedEvent.getUnwantedEventUid(), qp);
			if (list.size()>0) {
				Object[] rec = list.get(0);
				
				Date startDate = (Date) rec[0];
				node.getDynaAttr().put("eventDateTime", startDate);
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "depthMdMsl");

				Double startDepth = (Double) rec[1];
				if(startDepth!=null) {
					if (thisConverter.isUOMMappingAvailable()) {
						thisConverter.setBaseValue(startDepth);
						node.getDynaAttr().put("eventStartDepth", thisConverter.getConvertedValue());
						node.setCustomUOM("@eventStartDepth", thisConverter.getUOMMapping());
						node.getDynaAttr().put("eventStartDepth_text", thisConverter.getFormattedValue());
					}
				}
				
				//startDepth_premierindo TicketNo:-20327
				thisConverter.setReferenceMappingField(Activity.class, "depthFromMdMsl");

				Double startDepth1 = (Double) rec[3];
				if(startDepth1!=null) {
					if (thisConverter.isUOMMappingAvailable()) {
						thisConverter.setBaseValue(startDepth1);
						node.getDynaAttr().put("eventStartDepth1", thisConverter.getConvertedValue());
						node.setCustomUOM("@eventStartDepth1", thisConverter.getUOMMapping());
						node.getDynaAttr().put("eventStartDepth_premierindo", thisConverter.getFormattedValue());
					}
				}
				
				if(!checkSeverityDurationException){
					thisConverter.setReferenceMappingField(UnwantedEvent.class, "nptDuration");
					Double duration = (Double) rec[2];
					if (thisConverter.isUOMMappingAvailable() && duration!=null) {
						thisConverter.setBaseValue(duration);
						unwantedEvent.setNptDuration(thisConverter.getConvertedValue());
						
						if (duration<= 86400.0) { //24 hours
							unwantedEvent.setEventSeverity("anomaly");
						} else if (duration<= (86400.0 * 2)) { //48 hours
							unwantedEvent.setEventSeverity("significant");
						} else if (duration<= (86400.0 * 7)) { //1 week
							unwantedEvent.setEventSeverity("serious");
						} else if (duration<= (86400.0 * 30)) { //1 month
							unwantedEvent.setEventSeverity("severe");
						} else { //1 month++
							unwantedEvent.setEventSeverity("disastrous");
						}
					}
				}
				
				queryString = "From Activity WHERE (isDeleted=false or isDeleted is null) and nptEventUid=:nptEventUid";
				List<Activity> activitylist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "nptEventUid", unwantedEvent.getUnwantedEventUid());
				Double totalCost = 0.0;
				if (activitylist.size()>0) {
					for (Activity act : activitylist){
						Double actCost = this.calculateActivityCost(act.getActivityUid());
						totalCost += actCost;
					}
				}
				thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
				thisConverter.setBaseValue(totalCost);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@totalCost", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalCost", thisConverter.getConvertedValue());
			}
			
		} else if (node.getData() instanceof Activity) {
			Activity activity = (Activity) node.getData();
			Double activityCost = this.calculateActivityCost(activity.getActivityUid());
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "daycost");
			thisConverter.setBaseValue(activityCost);
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@activityCost", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("activityCost", thisConverter.getConvertedValue());
			
			ReportDaily reportDaily = null;
			if (reportDailyMap.containsKey(activity.getDailyUid())) {
				reportDaily = reportDailyMap.get(activity.getDailyUid());
			} else {
				String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(userSelection.getOperationUid());
				if (reportType!=null) {
					List<ReportDaily> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From ReportDaily WHERE (isDeleted=false or isDeleted is null) and reportType=:reportType and dailyUid=:dailyUid", 
							new String[]{"reportType","dailyUid"}, new Object[]{reportType, activity.getDailyUid()});
					if (list.size()>0) {
						reportDaily = list.get(0);
						reportDailyMap.put(activity.getDailyUid(), reportDaily);
					}
				}
			}

			if (reportDaily!=null) {
				node.getDynaAttr().put("reportNumber", reportDaily.getReportNumber());
				node.getDynaAttr().put("reportDatetime", reportDaily.getReportDatetime());
			}
		}
	}

	@Override
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof UnwantedEvent) {
			UnwantedEvent unwantedEvent = (UnwantedEvent) node.getData();
			unwantedEvent.setType("nptEvent");
			unwantedEvent.setNptDuration(null);
			unwantedEvent.setEventSeverity(null);
		}
	}

	@Override
	public void afterDataNodeDelete(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		if (node.getData() instanceof UnwantedEvent) {
			UnwantedEvent unwantedEvent = (UnwantedEvent) node.getData();
			
			//set Activity.nptEventUid to null for deleted NPT events
			String queryString = "From Activity WHERE (isDeleted=false or isDeleted is null) and nptEventUid=:nptEventUid";
			List<Activity> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "nptEventUid", unwantedEvent.getUnwantedEventUid());
			for (Activity activity : list) {
				activity.setNptEventUid(null);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(activity);
			}
			
		}
	}
	
	public Double calculateActivityCost(String activityUid) throws Exception {
		Double totalCost = 0.0;
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Activity activity = null;
		String strSql = "FROM Activity WHERE (isDeleted=false or isDeleted is null) " +
				"AND (isSimop=false or isSimop is null) " +
				"AND (isOffline=false or isOffline is null) " +
				"AND (dayPlus IS NULL OR dayPlus=0) " +
				"AND activityUid=:activityUid";
		List<Activity> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", activityUid, qp);
		if (list.size()>0) activity = list.get(0);
		if (activity==null) return 0.0;
		
		Double duration = activity.getActivityDuration();
		Double totalDuration = 0.0;
		strSql = "SELECT SUM(activityDuration) as activityDuration FROM Activity " +
				"WHERE (isDeleted=false or isDeleted is null) " +
				"AND dailyUid=:dailyUid " +
				"AND (isSimop=false or isSimop is null) " +
				"AND (isOffline=false or isOffline is null) " +
				"AND (dayPlus IS NULL OR dayPlus=0) ";
		List<Double> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", activity.getDailyUid(), qp);
		if (rs.size()>0) {
			if (rs.get(0)!=null) totalDuration = rs.get(0); 
		}
		
		Double dayCost = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(activity.getDailyUid());
		if (dayCost==null) {
			String query = "from ReportDaily where (isDeleted = false or isDeleted is null) and reportType <> 'DGR' and dailyUid=:dailyUid";
			String[] paramNames = {"dailyUid"};
			Object[] paramValues = {activity.getDailyUid()};
			List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramNames, paramValues);
			if (reportDailyList.size()>0){
				dayCost = reportDailyList.get(0).getDaycost();
			}
		}
		if (dayCost==null) dayCost = 0.0;
		totalCost = dayCost * duration / totalDuration;

		return totalCost;
	}

}
