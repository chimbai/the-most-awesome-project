package com.idsdatanet.d2.drillnet.activity;

import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class OperationActivityDurationByTaskCodeToNextDay0600ReportDataGenerator implements ReportDataGenerator{

	@SuppressWarnings("deprecation")
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, 
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false); 
		
		// get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		
//		
//		Calendar thisCalendar = Calendar.getInstance();
//		thisCalendar.clear();
//		thisCalendar.set(Calendar.DAY_OF_MONTH, todayDate.getDate()+1);
//		thisCalendar.set(Calendar.HOUR, 6);
//		thisCalendar.set(Calendar.MINUTE, 0);
//		thisCalendar.set(Calendar.SECOND, 0);
//		thisCalendar.set(Calendar.MILLISECOND, 0);
//		 
//		todayDate = thisCalendar.getTime(); 
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		String todayDateInString = null;
//		if(todayDate!=null){
//			todayDateInString = df.format((todayDate));
//			Date date1 = df.parse(todayDateInString);
//		}
		
		String strSql = "SELECT a.taskCode, SUM(a.activityDuration) as totalDuration, a.classCode, a.internalClassCode " +
				"FROM Daily d, Activity a " +
				"WHERE (d.isDeleted = false or d.isDeleted is null) " +
				"and (a.isDeleted = false or a.isDeleted is null) " +
				"and a.dailyUid = d.dailyUid " +
				"AND d.dayDate <= :userDate " + 
				"AND NOT(a.taskCode is null or a.taskCode = '') " +
				"AND d.operationUid = :thisOperationUid " +
				"AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid = '') " +
//				"AND (a.dayPlus IS NULL OR a.dayPlus=0) " +
				"AND (a.isSimop <> 1 OR a.isSimop IS NULL) " +
				"AND (a.isOffline <> 1 OR a.isOffline IS NULL) " +
				"GROUP BY a.taskCode, a.classCode, a.internalClassCode";
		
		String[] paramsFields = {"userDate", "thisOperationUid"};
		Object[] paramsValues = {todayDate, userContext.getUserSelection().getOperationUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			this.createProperties(userContext, reportDataNode, lstResult);
		}		
		
		// select tommorow date until 0600 for start_datetime
		Daily tomorrow = ApplicationUtils.getConfiguredInstance().getTomorrow(userContext.getUserSelection().getOperationUid(), userContext.getUserSelection().getDailyUid());
		
		if (tomorrow != null){
			Date tmrDate = tomorrow.getDayDate();
			String tmrDailyUid = tomorrow.getDailyUid();
			
			tmrDate.setHours(6);
			
			String strSql33 = "SELECT a.taskCode, SUM(a.activityDuration) as totalDuration, a.classCode, a.internalClassCode " +
					"FROM Daily d, Activity a " +
					"WHERE (d.isDeleted = false or d.isDeleted is null) " +
					"and (a.isDeleted = false or a.isDeleted is null) " +
					"and d.dailyUid=:dailyUid " +
					"and a.dailyUid = d.dailyUid " +
					"AND a.startDatetime < :userTmrDate " + 
					"AND NOT(a.taskCode is null or a.taskCode = '') " +
					"AND d.operationUid = :thisOperationUid " +
					"AND (a.dayPlus IS NULL OR a.dayPlus=0) " +
					"AND (a.isSimop <> 1 OR a.isSimop IS NULL) " +
					"AND (a.isOffline <> 1 OR a.isOffline IS NULL) " +
					"GROUP BY a.taskCode, a.classCode, a.internalClassCode";
			
			String[] paramsFields33 = {"userTmrDate", "dailyUid", "thisOperationUid"};
			Object[] paramsValues33 = {tmrDate, tmrDailyUid, userContext.getUserSelection().getOperationUid()};
			
			List lstTmrResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql33, paramsFields33, paramsValues33, qp);
			if (lstTmrResult.size() > 0){
				this.createProperties(userContext, reportDataNode, lstTmrResult);
			}
		}
	}

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 
	 * @param userContext
	 * @param reportDataNode
	 * @param resullt
	 * @throws Exception
	 */
	private void createProperties(UserContext userContext, ReportDataNode reportDataNode, List resullt) throws Exception {
		//unit converter to convert + format value
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
		
		for(Object objResult: resullt){
			//due to result is in array, cast the result to array first and read 1 by 1
			Object[] objDuration = (Object[]) objResult;
			
			//default class to program, cause some record might have no code
			String taskCode = "N/A";
			Double totalDuration = 0.0;				
			String taskName = "N/A";
			String classCode = "N/A";
			String internalClassCode = "N/A";
			
			if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())) {
				taskCode = objDuration[0].toString();
				String strSql2 = "SELECT name FROM LookupTaskCode WHERE (isDeleted is null OR isDeleted = false) AND shortCode = :thisTaskCode AND (operationCode = :thisOperationType OR operationCode = '')";
				String[] paramsFields2 = {"thisTaskCode", "thisOperationType"};
				Object[] paramsValues2 = {taskCode, userContext.getUserSelection().getOperationType().toString()};
				
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				if (lstResult2.size()>0)
				{
					Object lookupTaskCodeResult = (Object) lstResult2.get(0);
					if(lookupTaskCodeResult != null) taskName = lookupTaskCodeResult.toString();
				}
				
			}
			
			if (objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())){
				totalDuration = Double.parseDouble(objDuration[1].toString());
				
				//assign unit to the value base on activity.activityDuration, this will do auto convert to user display unit + format
				thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
				thisConverter.setBaseValue(totalDuration);
				totalDuration = thisConverter.getConvertedValue();					
			}
			
			if (objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())) classCode = objDuration[2].toString();
			if (objDuration[3] != null && StringUtils.isNotBlank(objDuration[3].toString())) internalClassCode = objDuration[3].toString();
			
			 
			
			ReportDataNode thisReportNode = reportDataNode.addChild("code");
			thisReportNode.addProperty("name", taskCode);
			thisReportNode.addProperty("duration", totalDuration.toString());	
			thisReportNode.addProperty("taskName", taskName);
			thisReportNode.addProperty("internalClassCode", internalClassCode);
			thisReportNode.addProperty("classCode",classCode);
		}	
	}

}
