package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ServiceCompanyLookupHandler implements LookupHandler {
	
	private List<String> serviceCode;
		
	
	public void setServiceCode(List<String> serviceCode)
	{
		if(serviceCode != null){
			this.serviceCode = new ArrayList<String>();
			for(String value: serviceCode){
				this.serviceCode.add(value.trim().toUpperCase());
			}
		}
	}
	
	public List<String> getServiceCode() {
		return this.serviceCode;
	}
		
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		if(this.getServiceCode() != null)
		{
			//GET SERVICECODE
			String strInCondition = "";
			
			for(String value:this.serviceCode)
			{
				strInCondition = strInCondition + "'" + value.toString() + "',";
			}
			
			strInCondition = StringUtils.substring(strInCondition, 0, -1);
			strInCondition = StringUtils.upperCase(strInCondition);
						
			String sql = "SELECT distinct a.lookupCompanyUid, a.companyName FROM LookupCompany a, LookupCompanyService b WHERE a.lookupCompanyUid = b.lookupCompanyUid AND (a.isDeleted IS NULL OR a.isDeleted = '') AND (b.isDeleted IS NULL OR b.isDeleted = '') AND b.code IN (" + strInCondition + ") ORDER BY a.companyName";
			List<Object[]> listCompanyResults = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
			if (listCompanyResults.size() > 0) {
				for (Object[] listCompanyResult : listCompanyResults) {
					if (listCompanyResult[0] != null && listCompanyResult[1] != null) {
						result.put(listCompanyResult[0].toString(), new LookupItem(listCompanyResult[0].toString(), listCompanyResult[1].toString()));
					}
				}
			}
			
		}
				
		return result;
	}

}

