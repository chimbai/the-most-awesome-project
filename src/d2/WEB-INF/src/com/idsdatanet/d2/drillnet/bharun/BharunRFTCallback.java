package com.idsdatanet.d2.drillnet.bharun;

import com.idsdatanet.d2.core.stt.DefaultSTTActionCallback;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class BharunRFTCallback extends DefaultSTTActionCallback {
	
	public void afterRefreshFromTownCompleted(UserSelectionSnapshot userSelection) {
		try {
			BharunUtils.updateCumulativeCircHour(userSelection.getDailyUid(),userSelection.getOperationUid());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}