package com.idsdatanet.d2.drillnet.campaignHistoryOverview;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.operationinformationcenter.DDRReportList;

public class CampaignHistoryOverviewDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private static final String CUSTOM_FROM_MARKER = "{_custom_from_}";
		
	public void afterDataNodeLoad(CommandBean commandBean,TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		
		ReportDaily reportdaily = (ReportDaily) object;
		if (reportdaily != null) {
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, reportdaily.getOperationUid());
				
			if (operation != null) {
				if (StringUtils.isNotBlank(operation.getOperationCampaignUid())) {
					node.getDynaAttr().put("operationCampaignUid", operation.getOperationCampaignUid());
				}
				
				String completeOperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), operation.getOperationUid());
				if (StringUtils.isNotBlank(completeOperationName)) {
					node.getDynaAttr().put("operationName", completeOperationName);
				}
				
				String fileMangerList = CommonUtil.getConfiguredInstance().loadFileManagerFileListForOIC(operation.getOperationUid(), null);
				if (StringUtils.isNotBlank(fileMangerList)) {
					node.getDynaAttr().put("fileManagerList", fileMangerList);
				}else {
					node.getDynaAttr().put("noFilesUploaded", "1");
				}
							
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
				
				//LOAD LATEST DAILY REPORT
				String thisReportType = "DDR";
				String[] paramsFields = {"dailyUid", "thisOperation", "reportDailyReportType", "reportFilesReportType"};
				String[] paramsValues = {reportdaily.getDailyUid(), operation.getOperationUid(), thisReportType, thisReportType};
				List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rd.dailyUid =:dailyUid and rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rd.reportType=:reportDailyReportType and rf.reportType=:reportFilesReportType ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues);
				
				if(items.size() < 1)
				{
					Map<String, String> reportTypePriorityWhenDrllgNotExist = CommonUtils.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist();
					String operationCode = operation.getOperationCode();
					String strReportTypes = reportTypePriorityWhenDrllgNotExist.get(operationCode);
					if (strReportTypes != null) {
						String[] reportTypes = strReportTypes.split("[,]");
						for (String reportType : reportTypes) {
							String[] paramsValues2 = {reportdaily.getDailyUid(), operation.getOperationUid(), reportType, reportType};
							thisReportType = reportType;
							items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rd.dailyUid =:dailyUid and rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rd.reportType=:reportDailyReportType and rf.reportType=:reportFilesReportType ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues2);
							
							if(items.size() > 0)
							{
								break;
							}
						}
					}
				}
				
				for(Iterator i = items.iterator(); i.hasNext(); ){
					Object[] obj_array = (Object[]) i.next();
					DDRReportList ddr = new DDRReportList();
					ReportFiles thisReportFile = (ReportFiles) obj_array[0];
					ddr.setData(thisReportFile, operation.getOperationName());
								
					File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());
					
					if(report_file.exists()){
						node.getDynaAttr().put("dirReportDate", sdf.format(thisReportFile.getReportDayDate()));
						node.getDynaAttr().put("dirReportFilesUid", thisReportFile.getReportFilesUid());
						node.getDynaAttr().put("dirReportDisplayName", thisReportFile.getDisplayName());
						node.getDynaAttr().put("dirFileExtension", this.getFileExtension(thisReportFile.getReportFile()));
						node.getDynaAttr().put("dirReportType", reportdaily.getReportType());
					}
				}
				
				//LOAD LATEST TUBING STRING REPORT
				List items2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles WHERE dailyUid =:dailyUid and (isDeleted = false or isDeleted is null) and reportType = 'TSR' order by reportDayDate DESC", "dailyUid", reportdaily.getDailyUid());
				if(items2.size() > 0){
				
					ReportFiles thisReportFile = (ReportFiles) items2.get(0);
					File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());
					
					if(report_file.exists()){
						node.getDynaAttr().put("tsrReportDate", sdf.format(thisReportFile.getReportDayDate()));
						node.getDynaAttr().put("tsrReportFilesUid", thisReportFile.getReportFilesUid());
						node.getDynaAttr().put("tsrReportDisplayName", thisReportFile.getDisplayName());
						node.getDynaAttr().put("tsrFileExtension", this.getFileExtension(thisReportFile.getReportFile()));
						node.getDynaAttr().put("tsrReportType", "TSR");
					}
				}
			}
		}
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		if(StringUtils.isNotBlank(fromClause) && fromClause.indexOf(CUSTOM_FROM_MARKER) < 0) return null;
		
		String currentClass	= meta.getTableClass().getSimpleName();
		String customFrom = "";
		
		if(currentClass.equals("ReportDaily")) {
			
			customFrom = "ReportDaily reportDaily, Operation operation, Daily daily";
			return fromClause.replace(CUSTOM_FROM_MARKER, customFrom);
		}
		return null;
	}

	

	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, 
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String currentClass		= meta.getTableClass().getSimpleName();
		String customCondition 	= "";
		String selectedOperationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
		
		if(currentClass.equals("ReportDaily")) {
			customCondition = "(reportDaily.isDeleted = false or reportDaily.isDeleted is null) and (operation.isDeleted = false or operation.isDeleted is null) and (daily.isDeleted = false or daily.isDeleted is null) " +
					"and reportDaily.operationUid = operation.operationUid and daily.operationUid = operation.operationUid " +
					"and reportDaily.dailyUid = daily.dailyUid and reportDaily.reportType <> 'DGR'";
			
			if (StringUtils.isNotBlank(selectedOperationCode)){
				customCondition += "AND operation.operationCode =:operationCode";
				query.addParam("operationCode", selectedOperationCode);
			}
			
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		return null;
	}
	
	private String getFileExtension(String reportFile){
		int i = reportFile.lastIndexOf(".");
		if(i != -1){
			return reportFile.substring(i + 1);
		}else{
			return null;
		}
	}

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		
		if ("ReportDaily".equals(meta.getTableClass().getSimpleName())) {
			String orderBy = null;
			if (StringUtils.isBlank(orderBy)) {
				orderBy = "reportDaily.reportDatetime DESC, operation.operationName ASC";
			} 
			return orderBy; 
		}
		return null;
	}
}
