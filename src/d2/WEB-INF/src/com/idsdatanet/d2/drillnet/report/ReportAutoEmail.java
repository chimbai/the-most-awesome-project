package com.idsdatanet.d2.drillnet.report;

public class ReportAutoEmail {
	private String subject = null;
	private String contentTemplateFile = null;
	private String content= null;
	private String emailListKey = null;
	private String emailList = null;
	private boolean useEmailList = false;
	private boolean sendEmailAfterReportGeneration = false;
	private boolean opsTeamBased = false;
	private String language = "en";
	
	public String getEmailSubject(){
		return this.subject;
	}
	
	public String getEmailContentTemplateFile(){
		return this.contentTemplateFile;
	}
	
	public String getEmailListKey(){
		return this.emailListKey;
	}
	
	public String getEmailList(){
		return this.emailList;
	}
	
	public boolean isUseEmailList(){
		return this.useEmailList;
	}
	
	public boolean isSendEmailAfterReportGeneration(){
		return this.sendEmailAfterReportGeneration;
	}
	
	public boolean isOpsTeamBased() {
		return opsTeamBased;
	}

	public void setEmailSubject(String subject){
		this.subject = subject;
	}
	
	public void setEmailContentTemplateFile(String templateFile){
		this.contentTemplateFile = templateFile;
	}
	
	public void setEmailListKey(String key){
		this.emailListKey = key;
	}
	
	public void setEmailList(String key){
		this.emailList = key;
	}
	
	public void setUseEmailList(boolean value){
		this.useEmailList = value;
	}
	
	public void setSendEmailAfterReportGeneration(boolean value){
		this.sendEmailAfterReportGeneration = value;
	}

	public void setOpsTeamBased(boolean opsTeamBased) {
		this.opsTeamBased = opsTeamBased;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguage() {
		return language;
	}
}
