package com.idsdatanet.d2.drillnet.daily;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.time.DateUtils;



import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DailyDataNodeListener extends EmptyDataNodeListener {
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		
		Object object = node.getData();
		
		if(object instanceof com.idsdatanet.d2.core.model.Daily){
			if(operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW){
				session.setCurrentDailyUid(((Daily) node.getData()).getDailyUid());
			}
		}
		
		//Daily thisDay = (Daily) object;
		//CommonUtil.getConfiguredInstance().carryOverNextDayActivity(thisDay);
	}

	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		session.setCurrentDailyUid(null);
	}
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof Daily) {
			
			//variable declaration
			Daily daily = (Daily) obj;
			
			Date tomorrowDayDate=DateUtils.addDays(daily.getDayDate(), 1); 
			node.getDynaAttr().put("tomorrowDayDate", ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(tomorrowDayDate));
			
		}
		
	}
}
