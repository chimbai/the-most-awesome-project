package com.idsdatanet.d2.drillnet.surveyreference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.idsdatanet.d2.core.model.SurveyReference;
import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class SurveyReferenceUtils {
	
	/**
	 * Use on screen
	 */
	public static void calculateMinimumCurvatureMethod(CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		
		// Get SurveyReference
		SurveyReference thisSurveyRef = (SurveyReference) targetCommandBeanTreeNode.getData();
		
		// Getting Screen UOM unit
		CustomFieldUom thisConverter = new CustomFieldUom(targetCommandBeanTreeNode.getCommandBean(), SurveyStation.class, "depthMdMsl");
		
		// Get SurveyStation
		Map<String, CommandBeanTreeNode> thisObjMap = targetCommandBeanTreeNode.getList().get(SurveyStation.class.getSimpleName());
		if(thisObjMap != null) {
			
			// Prepare SurveyStation
			List<CommandBeanTreeNode> sorted_survey_station = new ArrayList(thisObjMap.values());
			Collections.sort(sorted_survey_station, new SurveyStationCommandBeanTreeNodeComparator());
			
			List<SurveyStation> filtered_survey_station = new ArrayList();

			//Filter
			for(CommandBeanTreeNode thisStationNode: sorted_survey_station){
				Object objSurveyStation = thisStationNode.getData();
				if(objSurveyStation instanceof SurveyStation) {
					if (Action.DELETE.equals(thisStationNode.getAtts().getAction())) continue;
					SurveyStation thisSurveyStation = (SurveyStation) objSurveyStation;
					if (thisSurveyStation.getDepthMdMsl() == null || thisSurveyStation.getInclinationAngle() == null || thisSurveyStation.getAzimuthAngle() == null) continue;
					if (thisSurveyRef.getCalculateAftertiebackmd() != null && thisSurveyRef.getTieBackMdMsl() != null){
						if ("1".equals(thisSurveyRef.getCalculateAftertiebackmd()) && thisSurveyRef.getTieBackMdMsl() > thisSurveyStation.getDepthMdMsl()) {
							continue;
						}
					}

					thisStationNode.getAtts().setAction(Action.SAVE);
					thisStationNode.setDirty(true);
					filtered_survey_station.add(thisSurveyStation);
				}
			}
			
			SurveyReferenceUtils.calculateMinimumCurvatureMethodFunction(thisSurveyRef, filtered_survey_station, thisConverter);
		}	
	}
	
	public static void calculateMinimumCurvatureMethodFunction(SurveyReference thisSurveyRef, List<SurveyStation> surveyStationList, CustomFieldUom thisConverter) throws Exception {
		SurveyReferenceUtils.calculateMinimumCurvatureMethodFunction(thisSurveyRef, surveyStationList, thisConverter, false);
	}
	
	/**
	 * @param thisSurveyRef - Parent node of the surveyStationList
	 * @param surveyStationList - List<SurveyStation>, assumed to be presorted in ascending order according to depthMdMsl
	 * @param thisConverter - UOM
	 * @throws Exception
	 */
	public static void calculateMinimumCurvatureMethodFunction(SurveyReference thisSurveyRef, List<SurveyStation> surveyStationList, CustomFieldUom thisConverter, boolean firstStationTieOn) throws Exception {
		SurveyStation prevSurveyStation = null;
		
		// Initiate Default values 
		double ref_magDec = (thisSurveyRef.getMagneticDeclination() != null) ? thisSurveyRef.getMagneticDeclination() : 0.0;
		ref_magDec = ref_magDec * 3.142857 / 180; // Set ref_magDec to radius
					
		if (thisSurveyRef.getBuildDropWalk() == null) {
			if ("Metre".equals(thisConverter.getUomLabel())) {
				thisSurveyRef.setBuildDropWalk(new Double(30.0));
			} else if ("Feet".equals(thisConverter.getUomLabel())) {
				thisSurveyRef.setBuildDropWalk(new Double(100.0));
			}
		}
		double ref_buildDropWalk = thisSurveyRef.getBuildDropWalk();
		
		if (thisSurveyRef.getVerticalSectionAngle() == null) {
			thisSurveyRef.setVerticalSectionAngle(0.0);
		}
		double ref_verticalSectionAngle = thisSurveyRef.getVerticalSectionAngle();
		
		double ref_northing = (thisSurveyRef.getNorthing() != null) ? thisSurveyRef.getNorthing() : 0.0;
		double ref_easting = (thisSurveyRef.getEasting() != null) ? thisSurveyRef.getEasting() : 0.0;
		
		// Initialise Calculated Values (With defaults)
		double dlsAngle = 0.0;
		double rf = 1.0;
		double course = 0.0;
		double first_northing = 0.0;
		double first_easting = 0.0;
		double tmp_northing = 0.0;
		double tmp_easting = 0.0;
		double thisInclAngle, lastInclAngle, thisAziAngle, lastAziAngle;
		
		// Initialise Misc. Variables
		int counter = 1;
		double s1, s2, s3;
		
		double convertionFactor = 1.0;
		thisConverter.setReferenceMappingField(SurveyStation.class, "inclinationAngle");
		if ("Degree".equals(thisConverter.getUomLabel())) convertionFactor = 1 / 57.2957549575;
		else if ("Gradian".equals(thisConverter.getUomLabel())) convertionFactor = 0.015707963;
		
		//check vertical section angle in run level is it in degree or not, if NO need to change it to degree for calc
		thisConverter.setReferenceMappingField(SurveyReference.class,"verticalSectionAngle");
		if ("Radian".equals(thisConverter.getUomLabel())) ref_verticalSectionAngle = ref_verticalSectionAngle * 57.2957549575;
		else if ("Gradian".equals(thisConverter.getUomLabel())) ref_verticalSectionAngle = ref_verticalSectionAngle * 0.9;	
		
		if(firstStationTieOn) {
			prevSurveyStation = surveyStationList.get(0);
			surveyStationList = surveyStationList.subList(1, surveyStationList.size());
			
			first_northing = prevSurveyStation.getNsOffset();
			first_easting = prevSurveyStation.getEwOffset();
		}
		
		for(SurveyStation thisSurveyStation : surveyStationList) {
			
			// First Node
			if(prevSurveyStation == null) {
				if (thisSurveyStation.getInclinationAngle() == null) thisSurveyStation.setInclinationAngle(0.0);
				if (thisSurveyStation.getAzimuthAngle() == null) thisSurveyStation.setAzimuthAngle(0.0);
				if (thisSurveyStation.getDepthMdMsl() == null) thisSurveyStation.setDepthMdMsl(0.0);
				if (thisSurveyStation.getDepthTvdMsl() == null) thisSurveyStation.setDepthTvdMsl(0.0);
				if (thisSurveyStation.getNsOffset() == null) thisSurveyStation.setNsOffset(0.0);
				if (thisSurveyStation.getEwOffset() == null) thisSurveyStation.setEwOffset(0.0);
				prevSurveyStation = thisSurveyStation;
				
				first_northing = thisSurveyStation.getNsOffset();
				first_easting = thisSurveyStation.getEwOffset();
			} else {
				//if unit not in Radian need to convert to radian base on a factor
				thisInclAngle = thisSurveyStation.getInclinationAngle() * convertionFactor;
				lastInclAngle = prevSurveyStation.getInclinationAngle() * convertionFactor;
				thisAziAngle = thisSurveyStation.getAzimuthAngle() * convertionFactor;
				lastAziAngle = prevSurveyStation.getAzimuthAngle() * convertionFactor;
				
				//get RF value for use in later calc
				s1 = Math.cos(thisInclAngle - lastInclAngle);
				s2 = Math.sin(thisInclAngle) *  Math.sin(lastInclAngle);
				s3 = 1 - Math.cos(thisAziAngle - lastAziAngle);
				dlsAngle = Math.acos(s1 - (s2 * s3));
				
				if (dlsAngle == 0.0) rf = 1.0;
				else rf = 2 / dlsAngle * Math.tan(dlsAngle/2);
				
				//get current station temp TVD value
				if (thisSurveyStation.getDepthMdMsl() > 0) course = thisSurveyStation.getDepthMdMsl() - prevSurveyStation.getDepthMdMsl();
				s1 = course / 2;
				s2 = Math.cos(thisInclAngle) + Math.cos(lastInclAngle);
				s3 = (s1 * s2 * rf);
				if (thisSurveyStation.getDepthMdMsl() > 0) thisSurveyStation.setDepthTvdMsl(prevSurveyStation.getDepthTvdMsl() + s3);
				else thisSurveyStation.setDepthTvdMsl(0.0);
				
				//get current station DogLeg value
				if (course > 0) thisSurveyStation.setDlsAngle((dlsAngle / course) * (ref_buildDropWalk * 180 / 3.142857));
				else thisSurveyStation.setDlsAngle(0.0);
				
				//get northing easting value, when it is second node need to use the first northing and easting to calc else use previous
				s1 = course / 2;
				s2 = Math.sin(lastInclAngle) * Math.cos(lastAziAngle);
				s3 = Math.sin(thisInclAngle) * Math.cos(thisAziAngle);
				tmp_northing = s1 * (s2 + s3) * rf;
				
				s1 = course / 2;
				s2 = Math.sin(lastInclAngle) * Math.sin(lastAziAngle);
				s3 = Math.sin(thisInclAngle) * Math.sin(thisAziAngle);
				tmp_easting = s1 * (s2 + s3) * rf;
				
				if (thisSurveyStation.getDepthMdMsl() > 0) {
					if (counter == 2) {
						thisSurveyStation.setNsOffset(first_northing + tmp_northing);
						thisSurveyStation.setEwOffset(first_easting + tmp_easting);
					}
					else {
						thisSurveyStation.setNsOffset(prevSurveyStation.getNsOffset() + tmp_northing);
						thisSurveyStation.setEwOffset(prevSurveyStation.getEwOffset() + tmp_easting);
					}
				}							
				else {
					thisSurveyStation.setNsOffset(0.0);
					thisSurveyStation.setEwOffset(0.0);
				}
				
				//get vertical section value
				if (thisSurveyStation.getDepthMdMsl() > 0) {
					s1 = (thisSurveyStation.getNsOffset() - ref_northing) * Math.cos(3.142857 / 180 * ref_verticalSectionAngle);
					s2 = (thisSurveyStation.getEwOffset() - ref_easting) * Math.sin(3.142857 / 180 * ref_verticalSectionAngle);
					thisSurveyStation.setVerticalSectionExtrusion(s1 + s2);								
				} 
				else {
					thisSurveyStation.setVerticalSectionExtrusion(0.0);
				}
				
				//get closure value = departure value
				s1 = Math.pow(thisSurveyStation.getNsOffset(), 2);
				s2 = Math.pow(thisSurveyStation.getEwOffset(), 2);
				s3 = Math.pow(s1 + s2, 0.5);
				thisSurveyStation.setDeparture(s3);
				
				//get direction value
				if (thisSurveyStation.getEwOffset() == 0) s1 = 0.0;
				else s1 = Math.atan(thisSurveyStation.getNsOffset()/thisSurveyStation.getEwOffset());
				
				//change from radian to degree
				s2 = s1 * 57.2957549575;
				if (thisSurveyStation.getEwOffset() >= 0) thisSurveyStation.setDirection(90 - s2);
				else thisSurveyStation.setDirection(270 - s2);
				
				prevSurveyStation = thisSurveyStation;
			}
			counter++;
		}
	}
}

class SurveyStationCommandBeanTreeNodeComparator implements Comparator<CommandBeanTreeNode> {
	public int compare(CommandBeanTreeNode o1, CommandBeanTreeNode o2) {
		try {
			Object in1 = o1.getData();
			Object in2 = o2.getData();
			
			SurveyStation f1 = (SurveyStation) in1;
			SurveyStation f2 = (SurveyStation) in2;
			
			if(f1.getDepthMdMsl() != null && f2.getDepthMdMsl() != null) {
				return f1.getDepthMdMsl().compareTo(f2.getDepthMdMsl());
			} else if(f1.getDepthMdMsl() != null) {
				return 1;
			} else if(f2.getDepthMdMsl() != null) {
				return -1;
			} else {
				return 0;
			}
		} catch(Exception e) {
			return 0;
		}
	}
}