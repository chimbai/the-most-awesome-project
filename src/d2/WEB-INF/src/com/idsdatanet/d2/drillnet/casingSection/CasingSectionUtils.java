package com.idsdatanet.d2.drillnet.casingSection;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CasingTally;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.reportDaily.ReportDailyCasingUtils;
import com.idsdatanet.d2.drillnet.reportDaily.ReportDailyUtils;

public class CasingSectionUtils {
	/**
	 * calculate Depth From and Depth To based on casing tally joint length
	 * @param nodes
	 * @param session
	 * @param parent
	 * @throws Exception
	 */
	public static void calculateDepthFromAndDepthTo(String casingsectionuid, Collection<CommandBeanTreeNode> nodes, UserSession session) throws Exception
	{
		String[] paramsFields1 = {"casingSectionUid"};
		Object[] paramsValues1 = {casingsectionuid};
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String strSql1 = "FROM CasingTally WHERE (isDeleted = false or isDeleted is null) and casingSectionUid = :casingSectionUid order by runOrder";
		List <CasingTally> lstResult1= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
			if(lstResult1.size() > 0 && lstResult1!=null)
			{
				Double from = 0.0;
				Double to = 0.0;
				
				//query for getting the Stickup Length value
				String[] paramsFields = {"casingSectionUid"};
				Object[] paramsValues = {casingsectionuid};
				qp.setDatumConversionEnabled(true);
				String strSql = "SELECT stickUpLengthFromMdMsl FROM CasingSection WHERE (isDeleted = false or isDeleted is null) and casingSectionUid = :casingSectionUid";
				List lstResult= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
					
					//return result of the Stickup 
				CustomFieldUom thisConverterField = new CustomFieldUom(session.getUserLocale(), CasingSection.class, "stickUpLengthFromMdMsl");
				Object ResultStickuplength = (Object) lstResult.get(0);
				if (ResultStickuplength != null) 
				{
					//get the Stickuplength value in base value
					thisConverterField.setBaseValueFromUserValue(Double.parseDouble(ResultStickuplength.toString()));
					from = Double.parseDouble(ResultStickuplength.toString());
				}	
				for(CasingTally obj:lstResult1)
				{
					
						
					CasingTally casingTallyRecord=(CasingTally)obj;
					Double csgLength = (Double) PropertyUtils.getProperty(obj, "csgLength");
						//calculate Depth From and Depth To based on casing tally joint length
						//variable declaration
					
					Double csgLengthCopy = 0.0;
					
					//check if joint length is empty
					if (csgLength != null)
						{	
							//get a copy value of joint length in base value
							csgLengthCopy = csgLength;
							thisConverterField.setReferenceMappingField(CasingTally.class, "csgLength");
							thisConverterField.setBaseValueFromUserValue(csgLengthCopy);
							csgLengthCopy = thisConverterField.getBasevalue();
						}
						//depth from and depth to calculation method
						if (csgLengthCopy < 0) 
						{
							from = from + csgLengthCopy;
							to = from - csgLengthCopy;
						}
						else 
						{
							to = from + csgLengthCopy;
						}
						Boolean isModified = false;
						//set the depth from value with screen unit 
						thisConverterField.setReferenceMappingField(CasingTally.class, "jointintervalFrom");
						thisConverterField.setBaseValue(from); 
						from = thisConverterField.getConvertedValue();
						if (!from.equals(casingTallyRecord.getJointintervalFrom()))
						{
							casingTallyRecord.setJointintervalFrom(from);
							isModified = true;
						}
						//carry forward the depth to to next row depth from value
						from = to;
						
						//set the depth to value with screen unit 
						thisConverterField.setReferenceMappingField(CasingTally.class, "jointintervalTo");
						thisConverterField.setBaseValue(to); 
						to = thisConverterField.getConvertedValue();
						if (!to.equals(casingTallyRecord.getJointintervalTo()))
						{
							casingTallyRecord.setJointintervalTo(to);
							isModified = true;
						}
						
						
						//update Depth From and Depth To in database
						if (isModified)
						{	
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(casingTallyRecord);
						}
						
					
				}
			}
		
	}
	/**
	 * auto lookup casing section data & update to each report daily's last_csgshoe_md_msl, last_csgshoe_tvd_msl, last_csgsize / last_csg_size_description, last_lot, last_fit except those records' reportType = DGR
	 * this function only will be called if GWP_AUTO_LOOKUP_CASING_SECTON_DATA = 1 or 2
	 * @param commandBean
	 * @param session
	 * @throws Exception
	 */
	public static void UpdateReportDailyCasing(CommandBean commandBean, UserSession session) throws Exception
	{
		String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) " +
		"and wellboreUid = :thisWellboreUid AND reportType <> 'DGR'";
		String[] paramsFields2 = {"thisWellboreUid"};
		String[] paramsValues2 = {session.getCurrentWellboreUid()};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
		
		if (lstResult.size() > 0){
			for(Object objReportdaily: lstResult){
				ReportDaily thisReportDaily = (ReportDaily) objReportdaily;
				Object objResult = CommonUtil.getConfiguredInstance().lastCasingSection(session.getCurrentGroupUid(), session.getCurrentWellboreUid(), thisReportDaily.getDailyUid());
				
				if (objResult != null){
					if (objResult instanceof CasingSection){
						CasingSection thisCasingSection = (CasingSection) objResult;
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "last_csgshoe_md_msl");

						if (thisCasingSection.getShoeTopMdMsl() != null){
							thisConverter.setBaseValue(thisCasingSection.getShoeTopMdMsl());
							thisReportDaily.setLastCsgshoeMdMsl(thisConverter.getConvertedValue());
						} else {
							thisReportDaily.setLastCsgshoeMdMsl(null);
						}
						
						if (thisCasingSection.getShoeTopTvdMsl() != null){
							thisConverter.setReferenceMappingField(ReportDaily.class, "last_csgshoe_tvd_msl");
							thisConverter.setBaseValue(thisCasingSection.getShoeTopTvdMsl());
							thisReportDaily.setLastCsgshoeTvdMsl(thisConverter.getConvertedValue());
						} else {
							thisReportDaily.setLastCsgshoeTvdMsl(null);
						}
						
						if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_CASING_SECTON_DATA))){
							//due to casing size drop down must be in m and 6 decimal so no need to get raw value
							if (thisCasingSection.getCasingOd() != null){
								thisConverter.setReferenceMappingField(ReportDaily.class, "last_csgsize");
								thisConverter.setBaseValue(thisCasingSection.getCasingOd());
								thisReportDaily.setLastCsgsize(thisConverter.getConvertedValue());
							} else {
								thisReportDaily.setLastCsgsize(null);
							}							
						}else{
							if (thisCasingSection.getCasingSectionUid() != null){
								// based on last casing section record's UID, filter casing summary list to get those latest casing size
								String lastCsgSumCsgSize = ReportDailyUtils.CasingSummaryCasingSize(thisCasingSection.getCasingSectionUid(),session, thisConverter);	
								if (lastCsgSumCsgSize != null && StringUtils.isNotBlank(lastCsgSumCsgSize)){
									thisReportDaily.setLastCsgSizeDescription(lastCsgSumCsgSize);	
								}else {
									thisReportDaily.setLastCsgSizeDescription(null);
								}																
							}
						}
						// autoLookuplotfit was here
					}
				}else{
					thisReportDaily.setLastCsgshoeMdMsl(null);
					thisReportDaily.setLastCsgshoeTvdMsl(null);
					if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_CASING_SECTON_DATA))){
						thisReportDaily.setLastCsgsize(null);
					}else{
						thisReportDaily.setLastCsgSizeDescription(null);
					}

					if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_FIT_LOT))){		
						thisReportDaily.setLastLot(null);
						thisReportDaily.setLastFit(null);	
					}
				}
			
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReportDaily);
			}
		}
	}
	
	/**
	 * Method to create or update casing section from Activity based on pre-defined free text remarks
	 * @param thisActivity
	 * @param fieldsMap
	 * @throws Exception
	 */
	public static void createCasingSectionBasedOnActivityRemarks(UserSelectionSnapshot userSelection,Activity thisActivity, Map<String,Object>fieldsMap) throws Exception{
		
		if (fieldsMap !=null){
			
			String[] paramsFields = {"operationUid","activityUid"};
			Object[] paramsValues = {thisActivity.getOperationUid(),thisActivity.getActivityUid()};
			String strSql = "FROM CasingSection WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND activityUid=:activityUid";
			List<CasingSection> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			CasingSection casingSection = null;
			if(ls.size()==0){
				if(thisActivity.getStartDatetime()!=null){
					casingSection = new CasingSection();
					casingSection.setActivityUid(thisActivity.getActivityUid());
					casingSection.setSectionName("");
				}
			}else{
				casingSection = ls.get(0);			
			}
			 
			if (casingSection!=null ){
				casingSection.setInstallStartDate(thisActivity.getStartDatetime());
				for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
					if (StringUtils.isNotBlank(fieldEntry.getKey())) PropertyUtils.setProperty(casingSection, fieldEntry.getKey(), fieldEntry.getValue());
				}
				Boolean isSubmit = casingSection.getIsSubmit();
				//if(isSubmit!=null && isSubmit)RevisionUtils.unSubmitRecordWithStaticRevisionLog(userSelection, casingSection, "Auto update from time codes");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(casingSection);
			}
		}		
	}
	
	public static void updateCasingSetDepthByODBasedOnActivityRemarks(UserSelectionSnapshot userSelection, Map<String,Object>fieldsMap) throws Exception{
		if (fieldsMap !=null){
			
			Double casingOd = null;
			if (fieldsMap.containsKey("casingOd")){
				casingOd = (fieldsMap.get("casingOd")==null?null:(Double) fieldsMap.get("casingOd"));
				
				if(casingOd!=null){
					String[] paramsFields = {"operationUid","casingOd"};
					Object[] paramsValues = {userSelection.getOperationUid(),casingOd};
					String strSql = "FROM CasingSection WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND casingOd=:casingOd";
					List<CasingSection> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					for(CasingSection casingSection:ls){					
						
						for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
							if (StringUtils.isNotBlank(fieldEntry.getKey())) {
								
								if(fieldEntry.getKey().equalsIgnoreCase("casingLandedDepthMdMsl")){
									if(casingSection.getCasingLandedDepthMdMsl()==null && fieldEntry.getValue()!=null){
										casingSection.setCasingLandedDepthMdMsl((Double)fieldEntry.getValue());
									}								
								}else{
									PropertyUtils.setProperty(casingSection, fieldEntry.getKey(), fieldEntry.getValue());
								}												
							}
						}
						Boolean isSubmit = casingSection.getIsSubmit();
						//if(isSubmit!=null && isSubmit)RevisionUtils.unSubmitRecordWithStaticRevisionLog(userSelection, casingSection, "Auto update from time codes");
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(casingSection);
					}
				}
				
			}		
			
		}	
		
	}	
	
	public static void UpdateLeakOffTestCasingShoeDepth(CasingSection thisCasingSection, CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception
	{	
		Double shoeDepthMdMsl = 0.00;
		Double shoeTvdMsl = 0.00;
		Double casingOd = null;
		String wellboreUid = null;
		
		if (thisCasingSection.getCasingOd() != null) casingOd = thisCasingSection.getCasingOd();
		if (thisCasingSection.getShoeTopMdMsl() != null) shoeDepthMdMsl = thisCasingSection.getShoeTopMdMsl();
		if (thisCasingSection.getShoeTopTvdMsl() != null) shoeTvdMsl = thisCasingSection.getShoeTopTvdMsl();
		if (thisCasingSection.getWellboreUid() != null) wellboreUid = thisCasingSection.getWellboreUid();
		
		CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(), CasingSection.class, "shoeTopMdMsl");
		thisConverter.setBaseValueFromUserValue(shoeDepthMdMsl, false);
		shoeDepthMdMsl = thisConverter.getConvertedValue();
		
		thisConverter.setReferenceMappingField(CasingSection.class, "shoeTopTvdMsl");
		thisConverter.setBaseValueFromUserValue(shoeTvdMsl, false);
		shoeTvdMsl = thisConverter.getConvertedValue();
		
		if (casingOd != null && StringUtils.isNotBlank(wellboreUid)) {
			
			String[] paramsFields = {"thisCasingOd", "thisWellboreUid"};
			Object[] paramsValues = {casingOd, thisCasingSection.getWellboreUid()};
			
			String queryString = "FROM LeakOffTest WHERE (isDeleted = false or isDeleted is null) AND casingSize = :thisCasingOd AND wellboreUid=:thisWellboreUid";
			List<LeakOffTest> lst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramsFields, paramsValues);
			if (lst.size()>0){
				for(LeakOffTest leakOffTest : lst){
					leakOffTest.setCasingSize(casingOd);
					leakOffTest.setShoeDepthMdMsl(shoeDepthMdMsl);
					leakOffTest.setShoeTvdMsl(shoeTvdMsl);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject( leakOffTest );
				}
			}
		}
	}
	
	/**
	 * Auto Populates LOT/FIT Fields in all ReportDaily object in the Wellbore of the CasingSection if
	 * GWP 'autoPopulateLOTFIT' is enabled
	 * @param commandBean
	 * @param session
	 */
	public static void autoLookupReportDailyLOTFIT(CommandBean commandBean, UserSession session) throws Exception {
		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_FIT_LOT))) {
			String strSql = "FROM ReportDaily WHERE (isDeleted = false OR isDeleted IS NULL AND wellboreUid = :thisWellboreUid)";
			String[] paramField = {"thisWellboreUid"};
			String[] paramValue = {session.getCurrentWellboreUid()};
			List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramField, paramValue);
			
			if (!reportDailyList.isEmpty()) {
				for (ReportDaily thisReportDaily : reportDailyList) {
					CasingSection thisCasingSection = (CasingSection) CommonUtil.getConfiguredInstance().lastCasingSection(session.getCurrentGroupUid(), session.getCurrentWellboreUid(), thisReportDaily.getDailyUid());
					if (thisCasingSection != null ) {
						ReportDailyCasingUtils.populateLOTFIT(commandBean, thisReportDaily, thisCasingSection);
					} else {		
						thisReportDaily.setLastLot(null);
						thisReportDaily.setLastFit(null);	
					}
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReportDaily);
				}
			}
		}
	}
}
