package com.idsdatanet.d2.drillnet.aclSetup;

import java.util.List;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.MenuItem;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class AclListDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
	// TODO Auto-generated method stub

		
		
		String strSql = "FROM MenuItem WHERE (isDeleted = false or isDeleted IS NULL) and parent='' ORDER by sequence";

		List<MenuItem> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
		if (lstResult.size()>0){
			for (MenuItem mi : lstResult){
				this.generateMenuData(userContext, reportDataNode, mi, mi.getLabel());
			}
		}
	}		
	
	private void generateMenuData(UserContext userContext, ReportDataNode reportDataNode, MenuItem menuItem, String moduleLabel) throws Exception{
		String strSqlchild = "FROM MenuItem WHERE (isDeleted = false or isDeleted IS NULL)  " +
			"AND parent=:parentUid AND beanName!='' " +
			"ORDER by sequence";
			String[] paramsFields = {"parentUid"};
			Object[] paramsValues = {menuItem.getMenuItemUid()};

		List<MenuItem> lstResultChild = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlchild, paramsFields, paramsValues);
		if (lstResultChild.size()>0){
			for (MenuItem mi : lstResultChild){
				ReportDataNode thisReportNode = reportDataNode.addChild("AclList");
				thisReportNode.addProperty("module", moduleLabel);
				thisReportNode.addProperty("screen", mi.getLabel());
				thisReportNode.addProperty("controller", mi.getBeanName());
			}
		}
		
		String strSqlchild2 = "FROM MenuItem WHERE (isDeleted = false or isDeleted IS NULL)  " +
		"AND parent=:parentUid AND beanName='' " +
		"ORDER by sequence";
		String[] paramsFields2 = {"parentUid"};
		Object[] paramsValues2 = {menuItem.getMenuItemUid()};
		List<MenuItem> lstResultChild2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlchild2, paramsFields2, paramsValues2);
		if (lstResultChild2.size()>0){
			for (MenuItem mi : lstResultChild2){
				String label = moduleLabel + " > " + mi.getLabel();
				this.generateMenuData(userContext, reportDataNode, mi, label);
			}
		}
	}
}
