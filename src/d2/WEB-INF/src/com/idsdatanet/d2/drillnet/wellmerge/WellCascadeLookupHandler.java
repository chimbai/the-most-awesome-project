package com.idsdatanet.d2.drillnet.wellmerge;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WellCascadeLookupHandler implements CascadeLookupHandler {

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		List<Well> wellList = WellMergeUtils.getWellList();
		if (wellList != null && wellList.size() > 0) {
			Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
			for(Well well : wellList) {
				String wellUid = well.getWellUid();
				String wellName = well.getWellName();
				wellName += " [" + wellUid + "]";
				LookupItem item = new LookupItem(wellUid, wellName);
				result.put(wellName, item);
			}
			return result;
		}
		return null;
	}
	
	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		return null;
	}

	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		return null;
	}

}
