package com.idsdatanet.d2.drillnet.activity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityPlanReferenceLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
						
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		LinkedHashMap<String, String> phaseCode = new LinkedHashMap<String, String>();
		LinkedHashMap<String, String> taskCode = new LinkedHashMap<String, String>();
				
		//GET PHASE CODE
		String strSql = "FROM LookupPhaseCode WHERE (isDeleted=false or isDeleted is null)";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
		
		for (Object obj : list)
		{
			LookupPhaseCode thisLookupPhaseCode = (LookupPhaseCode) obj;
			phaseCode.put(thisLookupPhaseCode.getShortCode(), thisLookupPhaseCode.getName());
		}
		
		//GET TASK CODE 
		String strSql2 = "FROM LookupTaskCode WHERE (isDeleted=false or isDeleted is null)";
		List list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql2);
		
		for (Object obj : list2)
		{
			LookupTaskCode thisLookupTaskCode = (LookupTaskCode) obj;
			taskCode.put(thisLookupTaskCode.getShortCode(), thisLookupTaskCode.getName());
		}
			
		//GET PHASE AND TASK CODE FROM OperationPlanTask
		String strSql3 = "FROM OperationPlanTask WHERE (isDeleted=false or isDeleted is null) AND operationUid = :thisOperationUid order by phaseCode";
		List list3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, "thisOperationUid", userSelection.getOperationUid());
			
		for (Object obj : list3) {
			OperationPlanTask thisOperationPlanTask = (OperationPlanTask) obj;
			String key = thisOperationPlanTask.getPhaseCode() + "," + thisOperationPlanTask.getTaskCode();
			String value = phaseCode.get(thisOperationPlanTask.getPhaseCode()) + " > " + taskCode.get(thisOperationPlanTask.getTaskCode());
					
			LookupItem item = new LookupItem(key, value);
			result.put(key, item);
		}
		return result;
	}
		
}
