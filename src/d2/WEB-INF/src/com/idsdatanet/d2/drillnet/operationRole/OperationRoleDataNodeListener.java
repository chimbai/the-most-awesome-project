package com.idsdatanet.d2.drillnet.operationRole;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.model.OperationRole;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class OperationRoleDataNodeListener extends EmptyDataNodeListener{
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof OperationRole) {
			OperationRole thisOperationRole = (OperationRole) object;

			
			if (thisOperationRole.getPobMasterUid() != null) {	
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select designation, companyName, officePhone, mobilePhone, homePhone, faxNo, emailAddress from PobMaster where (isDeleted = false or isDeleted is null) and pobMasterUid = :pobMasterUid", "pobMasterUid", thisOperationRole.getPobMasterUid());
				if (lstResult.size() > 0) 
					{
						Object[] thisResult = (Object[]) lstResult.get(0);
						if(thisResult != null)
						{	
							if (thisResult[0] != null) thisOperationRole.setRole(thisResult[0].toString());
							if (thisResult[1] != null) node.getDynaAttr().put("companyName", thisResult[1].toString());
							if (thisResult[2] != null) node.getDynaAttr().put("officePhone", thisResult[2].toString());
							if (thisResult[3] != null) node.getDynaAttr().put("mobilePhone", thisResult[3].toString());
							if (thisResult[4] != null) node.getDynaAttr().put("homePhone", thisResult[4].toString());
							if (thisResult[5] != null) node.getDynaAttr().put("faxNo", thisResult[5].toString());
							if (thisResult[6] != null) node.getDynaAttr().put("emailAddress", thisResult[6].toString());
						}
						
					}			
			}	
		}
	}
	
}
