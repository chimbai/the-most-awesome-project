package com.idsdatanet.d2.drillnet.activityCodeSyncLogger;

import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

import java.io.File;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.log.BaseCommandLogger;
import com.idsdatanet.d2.core.log.CommandLogging;
import com.idsdatanet.d2.core.log.ThreadLocalCommandLogger;

import edu.emory.mathcs.backport.java.util.Collections;


public class ActivityCodeSyncLogger extends BaseCommandLogger implements ThreadLocalCommandLogger {
	
	private Boolean isLoged = false;
	public static Boolean isEnabled = null;
	
	public void setIsEnabled(Boolean value){
		this.isEnabled = value;
	}
	
	private static ThreadLocal<ActivityCodeSyncLogger> threadLocal = new ThreadLocal<ActivityCodeSyncLogger>();
	
    public CommandLogging getThreadLocalInstance(boolean autoCreate) {
    	ActivityCodeSyncLogger logger = threadLocal.get();
		if (logger == null && autoCreate) {
			logger = new ActivityCodeSyncLogger();
			threadLocal.set(logger);
		}
		return logger;
	}
    
	public CommandLogging getThreadLocalInstance() {
		return this.getThreadLocalInstance(false);
	}

	public void removeThreadLocal() {
		threadLocal.remove();
	}

	@Override
	public boolean canBeginLogging(Object context) throws Exception {
		//return EDMSyncEngine.getConfiguredInstance().isEnabled();
		Boolean toLog = false;
		
		if (!isEnabled)
			return false;
		
		deletePreviousLogs();

		if (context instanceof BaseCommandBean) {
			BaseCommandBean commandBean = (BaseCommandBean)context;
			if ("lookupClassCodeController".equals(commandBean.getControllerBeanName())) toLog = true;
			else if ("lookupPhaseCodeController".equals(commandBean.getControllerBeanName())) toLog = true;
			else if ("lookupTaskCodeController".equals(commandBean.getControllerBeanName())) toLog = true;
			else if ("lookupRootCauseCodeController".equals(commandBean.getControllerBeanName())) toLog = true;
		}
		
		//to add another filter (type)
		String sql = "SELECT count(*) FROM ImportExportServerProperties WHERE (isDeleted = false or isDeleted is null) AND dataTransferType = 'actCodeSync'";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		if(!list.isEmpty() && toLog == true)
		{
			Long size = (Long) list.get(0);
			if (size <= 0)
				return false;
		}

		return toLog;
	}

	private void deletePreviousLogs() throws Exception{
		File dir;
		try {
			dir = new File(getRootPath());
		
		if (dir != null && dir.isDirectory()) {
			String[] files = dir.list();
			if (files != null && files.length > 0) {
				for(String fileList : files)
				{
					if (fileList != null && fileList.endsWith(".act")) {
						File f = new File(getRootPath(), fileList);
						f.delete();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean canLogInstanceOf(Object obj) throws Exception {
		
		if (obj instanceof com.idsdatanet.d2.core.model.LookupClassCode || 
				obj instanceof com.idsdatanet.d2.core.model.LookupPhaseCode ||
				obj instanceof com.idsdatanet.d2.core.model.LookupTaskCode ||
				obj instanceof com.idsdatanet.d2.core.model.LookupRootCauseCode){
			isLoged = true;
			return true;
		}
		
		return false;
	}

	@Override
	public void loggingBegan(Object context, ObjectOutputStream objectOutput) throws Exception {
		//do nothing
	}

	@Override
	public void loggingEnded(boolean hasCommandToSend) {
		
		if(isLoged)
		{
			String filename = null;
			File dir;
			try {
				dir = new File(getRootPath());
			
			if (dir != null && dir.isDirectory()) {
				String[] files = dir.list();
				if (files != null && files.length > 0) {
					Arrays.sort(files, Collections.reverseOrder());
					filename = files[0];
					if (filename != null && filename.endsWith(".act")) {
						File f = new File(getRootPath(), filename);
						ActivityCodeSyncMapping.getMap(f);						
					}
				}
			}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(filename != null && StringUtils.isNotBlank(filename))
			{
				try {
					File f = new File(getRootPath(), filename);
					getRootPath();
					f.delete();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private String getRootPath() throws Exception{
		return ApplicationConfig.getConfiguredInstance().getServerRootPath() + "WEB-INF/activityCodeSync/";
		
	}

	@Override
	public String getLoggingFilePath() throws Exception {
		return (getRootPath() + (new Date()).getTime() + ".act");
	}
}