package com.idsdatanet.d2.drillnet.mudProperties;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.MudRheology;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class OkeaMudPropertiesReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		UserSelectionSnapshot userSelection = userContext.getUserSelection();

		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		if (daily == null) return;
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, daily.getOperationUid());
		
		if (operation!=null){
			ReportDataNode child = reportDataNode.addChild("DDR");
			
			//Mud Parameters
			if (daily.getDailyUid()!=null){
				Integer maxRheology = 0;
				ArrayList<String> mudPropertiesList = new ArrayList<String>();
				
				String strSql = "FROM MudProperties WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid AND (isPlan = FALSE OR isPlan IS NULL) ORDER BY reportTime";
				String[] paramsField = {"operationUid", "dailyUid"};
				Object[] paramsValue = {daily.getOperationUid(), daily.getDailyUid()};
				List<MudProperties> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if (result.size() > 0){
					child = reportDataNode.addChild("MudProperties");
					
					Integer count = 1;
					Integer diff = 0;
					Integer column = 1;
					Double cumCuttings = 0.0;
					Double cumDhLosses = 0.0;
					
					if (result.size()>3){
						diff = result.size() - 3;
					}
					Map<String,LookupItem> mudTypeLookup = LookupManager.getConfiguredInstance().getLookup("xml://mud_properties.mud_type?key=code&amp;value=label", userContext.getUserSelection(), null);
					DateFormat dateFormatTime = new SimpleDateFormat("HH:mm");
					
					for (MudProperties mud : result){
						if (mud.getMetalCuttingsMass()!=null) cumCuttings = cumCuttings + mud.getMetalCuttingsMass();
						if (mud.getDhloss()!=null) cumDhLosses = cumDhLosses + mud.getDhloss();
						
						if (count ==1 || count == diff+2 || count == diff+3){
							
							if (mud.getReportTime()!=null) {
								if (dateFormatTime.format(mud.getReportTime()).equals("23:59")){
									child.addProperty("reportTime"+"_"+column, "24:00");
								}else{
									child.addProperty("reportTime"+"_"+column, dateFormatTime.format(mud.getReportTime()));
								}
							}
							
							if (mud.getMudType()!=null) {
								child.addProperty("mudType"+"_"+column, getLookupValue(mudTypeLookup, mud.getMudType()));
								child.addProperty("mudTypeCode"+"_"+column, mud.getMudType());
							}
							if (mud.getOtherlabel1()!=null) child.addProperty("otherlabel1"+"_"+column, getCommonLookup("MudProperties.otherlabel1", mud.getOtherlabel1()));
							if (mud.getMudMedium()!=null) child.addProperty("mudMedium"+"_"+column, mud.getMudMedium().toString());
							if (mud.getMudCost()!=null) child.addProperty("mudCost"+"_"+column, mud.getMudCost().toString());
							if (mud.getMetalCuttingsMass()!=null) child.addProperty("metalCuttingsMass"+"_"+column, mud.getMetalCuttingsMass().toString());
							if (mud.getMudEngineerSummary()!=null) child.addProperty("mudEngineerSummary"+"_"+column, mud.getMudEngineerSummary().toString());
							if (mud.getOiltype()!=null) child.addProperty("oiltype"+"_"+column, mud.getOiltype().toString());
							if (mud.getSampleTakenFrom()!=null) child.addProperty("sampleTakenFrom"+"_"+column, mud.getSampleTakenFrom().toString());
							if (mud.getMwdh()!=null) child.addProperty("mwdh"+"_"+column, this.formatDataWithUom(thisConverter, mud.getMwdh(), MudProperties.class, "mwdh", false, false));	
							if (mud.getDepthMdMsl()!=null) child.addProperty("depthMdMsl"+"_"+column, mud.getDepthMdMsl().toString());
							if (mud.getMudTestTemperature()!=null) child.addProperty("mudTestTemperature"+"_"+column, mud.getMudTestTemperature().toString());
							if (mud.getMudWeight()!=null) child.addProperty("mudWeight"+"_"+column, this.formatDataWithUom(thisConverter, mud.getMudWeight(), MudProperties.class, "mudWeight", false, false));				
							if (mud.getMudFv()!=null) child.addProperty("mudFv"+"_"+column, mud.getMudFv().toString());
							if (mud.getMudPv()!=null) child.addProperty("mudPv"+"_"+column, mud.getMudPv().toString());
							if (mud.getMudYp()!=null) child.addProperty("mudYp"+"_"+column, mud.getMudYp().toString());
							if (mud.getBopPressureRating()!=null) child.addProperty("bopPressureRating"+"_"+column, this.formatDataWithUom(thisConverter, mud.getBopPressureRating(), MudProperties.class, "bopPressureRating", false, false));	
							if (mud.getMudGel10s()!=null) child.addProperty("mudGel10s"+"_"+column, mud.getMudGel10s().toString());
							if (mud.getMudGel10m()!=null) child.addProperty("mudGel10m"+"_"+column, mud.getMudGel10m().toString());
							if (mud.getMudEngineer()!=null) child.addProperty("mudEngineer"+"_"+column, mud.getMudEngineer().toString());
							if (mud.getMudspecialist()!=null) child.addProperty("mudspecialist"+"_"+column, mud.getMudspecialist().toString());
							if (mud.getMudExcessLimeConcentration()!=null) child.addProperty("mudExcessLimeConcentration"+"_"+column, this.formatDataWithUom(thisConverter, mud.getMudExcessLimeConcentration(), MudProperties.class, "mudExcessLimeConcentration", false, false));	
							if (mud.getH2osalinity()!=null) child.addProperty("h2osalinity"+"_"+column, mud.getH2osalinity().toString());
							if (mud.getMudChlorideIonDensity()!=null) child.addProperty("mudChlorideIonDensity"+"_"+column, mud.getMudChlorideIonDensity().toString());
							if (mud.getMudChlorideIonConcentration()!=null) child.addProperty("mudChlorideIonConcentration"+"_"+column, mud.getMudChlorideIonConcentration().toString());
							if (mud.getMudCaDensity()!=null) child.addProperty("mudCaDensity"+"_"+column, mud.getMudCaDensity().toString());
							if (mud.getMudMbtDensity()!=null) child.addProperty("mudMbtDensity"+"_"+column, mud.getMudMbtDensity().toString());
							if (mud.getMudMbt()!=null) child.addProperty("mudMbt"+"_"+column, mud.getMudMbt().toString());
							if (mud.getGlycol()!=null) child.addProperty("glycol"+"_"+column, mud.getGlycol().toString());
							if (mud.getKplus()!=null) child.addProperty("kplus"+"_"+column, mud.getKplus().toString());
							if (mud.getKplusDensity()!=null) child.addProperty("kplusDensity"+"_"+column, mud.getKplusDensity().toString());
							if (mud.getKplusConcentration()!=null) child.addProperty("kplusConcentration"+"_"+column, this.formatDataWithUom(thisConverter, mud.getKplusConcentration(), MudProperties.class, "kplusConcentration", false, false));
							if (mud.getMudKclConcentration()!=null) child.addProperty("mudKclConcentration"+"_"+column, mud.getMudKclConcentration().toString());
							if (mud.getMudPm()!=null) child.addProperty("mudPm"+"_"+column, mud.getMudPm().toString());
							if (mud.getMudPf()!=null) child.addProperty("mudPf"+"_"+column, this.formatDataWithUom(thisConverter, mud.getMudPf(), MudProperties.class, "mudPf", false, false));
							if (mud.getMudMf()!=null) child.addProperty("mudMf"+"_"+column, this.formatDataWithUom(thisConverter, mud.getMudMf(), MudProperties.class, "mudMf", false, false));
							if (mud.getMudExcessLimeDensity()!=null) child.addProperty("mudExcessLimeDensity"+"_"+column, mud.getMudExcessLimeDensity().toString());
							if (mud.getMudPh()!=null) child.addProperty("mudPh"+"_"+column, mud.getMudPh().toString());
							if (mud.getMudPhpaDensity()!=null) child.addProperty("mudPhpaDensity"+"_"+column, mud.getMudPhpaDensity().toString());
							if (mud.getOwratio()!=null) child.addProperty("owratio"+"_"+column, mud.getOwratio().toString());
							if (mud.getElectricstab()!=null) child.addProperty("electricstab"+"_"+column, mud.getElectricstab().toString());
							if (mud.getCaclWp()!=null) child.addProperty("caclWp"+"_"+column, mud.getCaclWp().toString());
							if (mud.getCalciumChlorideDensity()!=null) child.addProperty("calciumChlorideDensity"+"_"+column, mud.getCalciumChlorideDensity().toString());
							if (mud.getMp()!=null) child.addProperty("mp"+"_"+column, mud.getMp().toString());
							if (mud.getMudApiFl()!=null) child.addProperty("mudApiFl"+"_"+column, mud.getMudApiFl().toString());
							if (mud.getMudApiCake()!=null) child.addProperty("mudApiCake"+"_"+column, mud.getMudApiCake().toString());
							if (mud.getHthpFl()!=null) child.addProperty("hthpFl"+"_"+column, mud.getHthpFl().toString());
							if (mud.getHthpCake()!=null) child.addProperty("hthpCake"+"_"+column, this.formatDataWithUom(thisConverter, mud.getHthpCake(), MudProperties.class, "hthpCake", false, false));
							if (mud.getHthpTestTemperature()!=null) child.addProperty("hthpTestTemperature"+"_"+column, mud.getHthpTestTemperature().toString());
							if (mud.getHthpTestPressure()!=null) child.addProperty("hthpTestPressure"+"_"+column, mud.getHthpTestPressure().toString());
							if (mud.getDhgain()!=null) child.addProperty("dhgain"+"_"+column, mud.getDhgain().toString());
							if (mud.getDhloss()!=null) child.addProperty("dhloss"+"_"+column, mud.getDhloss().toString());
							if (mud.getMudDissolvedSolidsConcentration()!=null) child.addProperty("mudDissolvedSolidsConcentration"+"_"+column, mud.getMudDissolvedSolidsConcentration().toString());
							if (mud.getDrilledSolids()!=null) child.addProperty("drilledSolids"+"_"+column, mud.getDrilledSolids().toString());
							if (mud.getLgsWtDensity()!=null) child.addProperty("lgsWtDensity"+"_"+column, mud.getLgsWtDensity().toString());
							if (mud.getMudOil()!=null) child.addProperty("mudOil"+"_"+column, mud.getMudOil().toString());
							if (mud.getMudSand()!=null) child.addProperty("mudSand"+"_"+column, mud.getMudSand().toString());
							if (mud.getMudH2o()!=null) child.addProperty("mudH2o"+"_"+column, mud.getMudH2o().toString());
							if (mud.getOilcuttings()!=null) child.addProperty("oilcuttings"+"_"+column, mud.getOilcuttings().toString());
							if (mud.getBarite()!=null) child.addProperty("barite"+"_"+column, mud.getBarite().toString());
							
							child.addProperty("cumCuttings"+"_"+column, cumCuttings.toString());
							child.addProperty("cumDhLosses"+"_"+column, cumDhLosses.toString());
							
							List<MudRheology> data = this.getRheology(mud.getMudPropertiesUid());
							if (data!=null){
								if (data.size()> maxRheology) {
									maxRheology = data.size();
								}						
							}
							
							if (mud.getMudPropertiesUid()!=null) mudPropertiesList.add(mud.getMudPropertiesUid());
							
							column++;
															
						}
						
						count++;
					}
				}
				
				Map<String,LookupItem> rpmLookup = LookupManager.getConfiguredInstance().getLookup("xml://mud_properties.rpm_reading?key=code&amp;value=label", userContext.getUserSelection(), null);
				for (Integer i=0;i<3;i++){
					Integer rheoList = 0;
					
					if (mudPropertiesList.size()> 0 && i+1 <= mudPropertiesList.size()){
						String mudPropertiesUid = mudPropertiesList.get(i);
						
						if (mudPropertiesUid!=null && StringUtils.isNotBlank(mudPropertiesUid)){
							List<MudRheology> data = this.getRheology(mudPropertiesUid);
							
							if (data!=null && data.size()>0){
								for (MudRheology rheology : data){
									child = reportDataNode.addChild("MudRheology");
									child.addProperty("column", i.toString());
									child.addProperty("mudPropertiesUid", mudPropertiesUid);
									
									if (rheology.getRpm()!=null && rpmLookup!=null){
										child.addProperty("rpm",rheology.getRpm().toString());
										child.addProperty("rpm_lookup", getLookupValue(rpmLookup, rheology.getRpm()));
									}
									if (rheology.getRheologyReading()!=null)  child.addProperty("rheologyReading",rheology.getRheologyReading().toString());
									
									rheoList++;
								}
							}
							
									
							if (rheoList<maxRheology){
								for (Integer blank=rheoList+1;blank<maxRheology+1;blank++){
									child = reportDataNode.addChild("MudRheology");
									child.addProperty("column", i.toString());
									child.addProperty("mudPropertiesUid", mudPropertiesUid);
									child.addProperty("rpm","");
									child.addProperty("rheologyReading","");
								}
							}
						}
					}else{
						for (Integer blank=0;blank<maxRheology;blank++){
							child = reportDataNode.addChild("MudRheology");
							child.addProperty("column", i.toString());
							child.addProperty("mudPropertiesUid", "x");
							child.addProperty("rpm","");
							child.addProperty("rheologyReading","");
						}
					}
					
					
					
				}
				
				
			}
		}
		
	}
	
	private String getLookupValue(Map<String,LookupItem> lookupList, Object lookupvalue) throws Exception {
		String retValue = "";
		
		if (lookupvalue !=null && StringUtils.isNotBlank(lookupvalue.toString())  && lookupList !=null){
			 LookupItem lookup = lookupList.get(lookupvalue.toString());
			if (lookup !=null)	retValue = lookup.getValue().toString();					
		} 
		
		return retValue;
	}
	
	private Double getConvertedUOM(CustomFieldUom thisConverter, Double value, String unit, Boolean offset) throws Exception{
		if (thisConverter!=null){
			thisConverter.setBaseValueFromUserValue(value);
			thisConverter.changeUOMUnit(unit);
			if (offset) thisConverter.addDatumOffset();
			
			return thisConverter.getConvertedValue();
		}

		return null;
	}
	
	
	private String getCommonLookup(String lookupTypeSelection, String shortCode) throws Exception{
		String lookupLabel = "";
		
		String strSql = "FROM CommonLookup Where (isDeleted is null or isDeleted = false) and lookupTypeSelection=:lookupTypeSelection AND shortCode=:shortCode ";
		String[] paramsField = {"lookupTypeSelection", "shortCode"};
		Object[] paramsValue = {lookupTypeSelection, shortCode};
		List<CommonLookup> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		
		if (result.size() > 0){
			for (CommonLookup cl : result){
				lookupLabel = cl.getLookupLabel();
			}
		}
		return lookupLabel;
	}
	
	private String formatDataWithUom(CustomFieldUom thisConverter, Double value, Class className, String fieldName, Boolean uom, Boolean space) throws Exception {
		if (value!=null) {
			if (thisConverter!=null) {
				thisConverter.setReferenceMappingField(className, fieldName);
				if (thisConverter.isUOMMappingAvailable()) {
					return thisConverter.formatOutputPrecision(value) + (space?" ":"") + (uom?thisConverter.getUomSymbol():"");
				}				
			}
			return String.valueOf(value);
		}		
		return "";
	}

	private List<MudRheology> getRheology(String mudPropertiesUid) throws Exception{
		String strSql = "FROM MudRheology WHERE (isDeleted = false or isDeleted is null) AND mudPropertiesUid=:mudPropertiesUid ORDER BY rpm ";
		String[] paramsField = {"mudPropertiesUid"};
		Object[] paramsValue = {mudPropertiesUid};
		List<MudRheology> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		
		if (result.size() > 0){
			return (List<MudRheology>) result;
		}
		
		return null;
	}
}
