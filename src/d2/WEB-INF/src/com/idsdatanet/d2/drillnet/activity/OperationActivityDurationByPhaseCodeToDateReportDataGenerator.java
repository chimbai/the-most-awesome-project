package com.idsdatanet.d2.drillnet.activity;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.query.generators.MSSQLDatabaseManager;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @author dlee
 * This class help to mine data for activity duration for the operation up to the selected date group by phase code
 * for daily reporting use
 * 
 * configuration is set in ddr.xml
 */

public class OperationActivityDurationByPhaseCodeToDateReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		String prevPhase = null;
		String prevPhaseName = null;
		
		String phaseCode = "N/A";
		String phaseName = "N/A";
		Double totalDuration = 0.0;
		Double runningTotalDuration = 0.0;
		Double currentRunningTotalDuration = 0.0;
		Double currentRunningTotalDurationDay = 0.0;
		Double minDepth = 0.0;
		Double maxDepth = 0.0;
		String minDepthDisplay = "";
		String maxDepthDisplay = "";
		Date startDate = null;
		Date endDate = null;
		long startDateVal = 0;
		long endDateVal = 0;
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		// get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if(daily == null) return;
		Date todayDate = daily.getDayDate();
		
		String strSql = "SELECT a.phaseCode, SUM(a.activityDuration) AS totalDuration, MIN(a.depthMdMsl) AS minDepth, MAX(a.depthMdMsl) AS maxDepth, d.dayDate, min(a.startDatetime) " + 
				"FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) " + 
				"AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
				"NOT(a.phaseCode IS NULL OR a.phaseCode = '') AND (a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " +
				"GROUP BY a.phaseCode, d.dayDate ORDER BY min(a.startDatetime)";
		
		String[] paramsFields = {"userDate", "thisOperationUid"};
		Object[] paramsValues = {todayDate, userContext.getUserSelection().getOperationUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if(lstResult.size() > 0) {
			//unit converter to convert + format value
			CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
			
			for(Object objResult: lstResult) {
				Object[] objDuration = (Object[]) objResult;
				
				if(objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())) {
					phaseCode = objDuration[0].toString();
					String strSql2 = "SELECT name FROM LookupPhaseCode WHERE (isDeleted is null OR isDeleted = false) AND shortCode = :thisPhaseCode AND (operationCode = :thisOperationType OR operationCode = '')";
					String[] paramsFields2 = {"thisPhaseCode", "thisOperationType"};
					Object[] paramsValues2 = {phaseCode, userContext.getUserSelection().getOperationType().toString()};
					
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				     if(lstResult2.size()>0) {
				      Object lookupPhaseCodeResult = (Object) lstResult2.get(0);
				      if(lookupPhaseCodeResult != null) phaseName = lookupPhaseCodeResult.toString();
				     }
				}
				
				// first phase
				if(prevPhase == null) {
					prevPhase = phaseCode;
					prevPhaseName = phaseName;
					
					if(objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())) {
						minDepth = Double.parseDouble(objDuration[2].toString());
						
						//assign unit to the value base on activity.depthMdMsl, this will do auto convert to user display unit + format
						thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");
						thisConverter.setBaseValue(minDepth);
						minDepth = thisConverter.getConvertedValue();
						minDepthDisplay = thisConverter.getFormattedValue();
					}
					
					if(objDuration[4] != null && StringUtils.isNotBlank(objDuration[4].toString())) {
						startDate = (Date) objDuration[4];
						endDate = startDate;
						
						startDateVal = startDate.getTime();
						endDateVal = startDateVal;
					}
				} else {
					if(phaseCode.equalsIgnoreCase(prevPhase)) {
						if(objDuration[4] != null && StringUtils.isNotBlank(objDuration[4].toString())) {
							endDate = (Date) objDuration[4];
							endDateVal = endDate.getTime();
						}
					} else {
						addToReportDataNode(reportDataNode, prevPhase, prevPhaseName, totalDuration, currentRunningTotalDuration, currentRunningTotalDurationDay, 
								minDepth, minDepthDisplay, maxDepth, maxDepthDisplay, startDateVal, startDate, endDateVal, endDate);
						
						prevPhase = phaseCode;
						prevPhaseName = phaseName;
						totalDuration = 0.0;
						minDepth = 0.0;
						maxDepth = 0.0;
						minDepthDisplay = "";
						maxDepthDisplay = "";
						startDate = null;
						endDate = null;
						startDateVal = 0;
						endDateVal = 0;
						
						if(objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())) {
							minDepth = Double.parseDouble(objDuration[2].toString());
							
							//assign unit to the value base on activity.depthMdMsl, this will do auto convert to user display unit + format
							thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");
							thisConverter.setBaseValue(minDepth);
							minDepth = thisConverter.getConvertedValue();
							minDepthDisplay = thisConverter.getFormattedValue();
						}
						
						if(objDuration[4] != null && StringUtils.isNotBlank(objDuration[4].toString())) {
							startDate = (Date) objDuration[4];
							endDate = startDate;
							
							startDateVal = startDate.getTime();
							endDateVal = startDateVal;
						}
					}
				}
				
				/* common functions */
				if(objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())) {
					totalDuration = (totalDuration*3600) + Double.parseDouble(objDuration[1].toString());
					
					//need to add to running before the value get converted, so handling all in raw unit -> seconds
					runningTotalDuration = runningTotalDuration + Double.parseDouble(objDuration[1].toString());
					
					//assign unit to the value base on activity.activityDuration, this will do auto convert to user display unit + format
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(totalDuration);
					totalDuration = thisConverter.getConvertedValue();					
				}
				
				//for running total calculation need to be outside the test cause the total is always needed in every record
				//assign unit to current running total
				currentRunningTotalDuration = runningTotalDuration;
				
				//do another value for day base, do this before the value got converted, cause here is always seconds
				currentRunningTotalDurationDay = currentRunningTotalDuration / 86400;
				
				thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
				thisConverter.setBaseValue(currentRunningTotalDuration);
				currentRunningTotalDuration = thisConverter.getConvertedValue();
				
				if(objDuration[3] != null && StringUtils.isNotBlank(objDuration[3].toString())) {
					maxDepth = Double.parseDouble(objDuration[3].toString());
					
					//assign unit to the value base on activity.depthMdMsl, this will do auto convert to user display unit + format
					thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");
					thisConverter.setBaseValue(maxDepth);
					maxDepth = thisConverter.getConvertedValue();
					maxDepthDisplay = thisConverter.getFormattedValue();
				}
			}
			
			// last phase
			if(prevPhase != null) {
				addToReportDataNode(reportDataNode, phaseCode, phaseName, totalDuration, currentRunningTotalDuration, currentRunningTotalDurationDay, 
						minDepth, minDepthDisplay, maxDepth, maxDepthDisplay, startDateVal, startDate, endDateVal, endDate);
			}
		}		
	}

	private void addToReportDataNode(ReportDataNode reportDataNode, String phase, String phaseName,
			Double totalDuration, Double currentRunningTotalDuration, Double currentRunningTotalDurationDay,
			Double minDepth, String minDepthDisplay, Double maxDepth, String maxDepthDisplay, long startDateVal,
			Date startDate, long endDateVal, Date endDate) {
		
		//In BIRT Report - If raw value needed for calculation, need to manually add property (eg. minDepthVal_raw)
		ReportDataNode thisReportNode = reportDataNode.addChild("code");
		thisReportNode.addProperty("name", phase);
		thisReportNode.addProperty("phaseName", phaseName);
		thisReportNode.addProperty("duration", totalDuration.toString());	
		thisReportNode.addProperty("runningTotalDuration", currentRunningTotalDuration.toString());
		thisReportNode.addProperty("runningTotalDurationDay", currentRunningTotalDurationDay.toString());				
		thisReportNode.addProperty("minDepthVal", minDepth.toString());
		thisReportNode.addProperty("minDepthDisplay", minDepthDisplay);
		thisReportNode.addProperty("maxDepthVal", maxDepth.toString());
		thisReportNode.addProperty("maxDepthDisplay", maxDepthDisplay);
		thisReportNode.addProperty("startDate", String.valueOf(startDateVal));
		thisReportNode.addProperty("startDateDisplay", startDate.toString());
		thisReportNode.addProperty("endDate", String.valueOf(endDateVal));
		thisReportNode.addProperty("endDateDisplay", endDate.toString());
	}
}
