package com.idsdatanet.d2.drillnet.reportDaily;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.startnewday.StartNewDaySetting;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.common.util.dynamicAttribute.DynamicAttributeProcessor;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.dynamicfield.DynamicFieldUtil;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.BopLog;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CostAfeMaster;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DailyShiftInfo;
import com.idsdatanet.d2.core.model.DepotWellOperationMapping;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.model.GeneralComment;
import com.idsdatanet.d2.core.model.HoleSection;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.model.SafetyTicket;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.UOMManager;
import com.idsdatanet.d2.core.uom.mapping.UOMMapping;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.costnet.CostNetConstants;
import com.idsdatanet.d2.drillnet.activity.ActivityUtils;
import com.idsdatanet.d2.drillnet.bharun.BharunUtils;
import com.idsdatanet.d2.drillnet.hseIncident.HseUtils;
import com.idsdatanet.d2.drillnet.leakOffTest.LeakOffTestUtil;
import com.idsdatanet.d2.drillnet.mudProperties.MudPropertiesUtil;
import com.idsdatanet.d2.drillnet.rigUtilization.RigUtilizationUtils;
import com.idsdatanet.d2.drillnet.wellexplorer.WellExplorerConstant;
import com.idsdatanet.d2.pronet.slickline.SlicklineUtil;
import com.idsdatanet.d2.pronet.wellProductionTest.WellProductionTestUtil;
import com.idsdatanet.d2.safenet.environmentDischarges.EnvironmentDischargesUtil;
import com.idsdatanet.depot.edm.sync.EDMSyncEvent;
import com.idsdatanet.depot.hip.core.OMVGDBEvent;
import com.idsdatanet.depot.edm.util.EdmUtils;
import com.idsdatanet.depot.witsml.rts.externalactivity.event.ExternalActivityDailyEvent;

public class ReportDailyDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	private static final String QC_NOT_DONE = "0";
	private static final String RIG_SIDE_QC_DONE = "1";
	private Boolean isChangeDate = false;
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private String defaultReportType;
	private String reportGenerated;
	private String daysFromSpudByDateCalcType;
	private Boolean autoUpdateFlowbackData = false;
	private boolean wellboreBased = false;
	private Boolean ignore24HourChecking = false;
	private Boolean showSurveyByDayDepth = false;
	private Boolean createdNewDay = false;
	private Boolean updateExistingDaily = false;
	private boolean autoPopulateLastHoleSize = false;
	private boolean autoPopulatePrevHoleSize = false;
	private boolean autoPopulateQCflagFromDDR = false;
	private boolean autoCalculateDepthTvdSs = false;
	private String previousDailyUid = "";
	private boolean isDailyReportEndDateOnNextDay = true;
	private String customDailyReportStartTime = null;
	private String customDailyReportEndTime = null;
	private String customCutOffTime = null;
	private String forwardShiftReportType;
	private String shiftCountry = "AT";
	
	private Map<String, String> dynamicAttributeMap = null;
	
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	public boolean isDailyReportEndDateOnNextDay() {
		return isDailyReportEndDateOnNextDay;
	}

	public void setDailyReportEndDateOnNextDay(boolean isDailyReportEndDateOnNextDay) {
		this.isDailyReportEndDateOnNextDay = isDailyReportEndDateOnNextDay;
	}

	public String getCustomDailyReportStartTime() {
		return customDailyReportStartTime;
	}

	public void setCustomDailyReportStartTime(String customDailyReportStartTime) {
		this.customDailyReportStartTime = customDailyReportStartTime;
	}

	public String getCustomDailyReportEndTime() {
		return customDailyReportEndTime;
	}

	public void setCustomDailyReportEndTime(String customDailyReportEndTime) {
		this.customDailyReportEndTime = customDailyReportEndTime;
	}

	public String getCustomCutOffTime() {
		return customCutOffTime;
	}

	public void setCustomCutOffTime(String customCutOffTime) {
		this.customCutOffTime = customCutOffTime;
	}

	public void setAutoPopulateLastHoleSize(boolean value){
		this.autoPopulateLastHoleSize = value;
	}
	
	public void setAutoPopulatePrevHoleSize(boolean value){
		this.autoPopulatePrevHoleSize = value;
	}
	
	public void setAutoCalculateDepthTvdSs(boolean value){
		this.autoCalculateDepthTvdSs = value;
	}

	public void setAutoPopulateQCflagFromDDR(boolean autoPopulateQCflagFromDDR) {
		this.autoPopulateQCflagFromDDR = autoPopulateQCflagFromDDR;
	}
	
	public void setDaysFromSpudByDateCalcType(String value){
		this.daysFromSpudByDateCalcType = value;
	}
	
	public void setReportType(String value){
		this.defaultReportType = value;
	}
	
	public void setForwardShiftReportType(String value){
		this.forwardShiftReportType = value;
	}

	public void setShiftCountry(String value){
		this.shiftCountry = value;
	}
	
	public void setReportGenerated(String value){
		this.reportGenerated = value;
	}
	
	public void setAutoUpdateFlowbackData(Boolean value) {
		this.autoUpdateFlowbackData = value;
	}
	
	private Boolean isChangeDate(String dailyUid, Date newDate) throws Exception {
		Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
		if (daily != null) 
			return !DateUtils.isSameDay(daily.getDayDate(), newDate);
		return false;
	}
	
	public void setWellboreBased(boolean wellboreBased) {
		this.wellboreBased = wellboreBased;
	}

	public boolean isWellboreBased() {
		return wellboreBased;
	}
	
	public Boolean getShowSurveyByDayDepth() {
		return showSurveyByDayDepth;
	}
	
	public void setShowSurveyByDayDepth(Boolean showSurveyByDayDepth) {
		this.showSurveyByDayDepth = showSurveyByDayDepth;
	}
	
	public Map<String, String> getDynamicAttributeMap() {
		return dynamicAttributeMap;
	}

	public void setDynamicAttributeMap(Map<String, String> dynamicAttributeMap) {
		this.dynamicAttributeMap = dynamicAttributeMap;
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof ReportDaily) {
			ReportDaily thisReport = (ReportDaily) object;
			
			//21790
			if(thisReport.getPlannedDaysCompleted() != null )
			{
				CustomFieldUom thisConverter1 = new CustomFieldUom(commandBean, ReportDaily.class, "PlannedDaysCompleted");
				thisReport.setPlannedDaysCompleted(Double.parseDouble(thisConverter1.formatOutputPrecision((thisReport.getPlannedDaysCompleted())))); 
			}
			
			String strSql4 = "FROM GeneralComment WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid AND dailyUid = :thisdailyUid";
			String[] paramsFields4 = {"thisdailyUid","thisOperationUid"};
			Object[] paramsValues4 = {thisReport.getDailyUid(),thisReport.getOperationUid()};
			List<GeneralComment> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4);
			if (!lstResult2.isEmpty()) {
				for(GeneralComment res:lstResult2) {
					String category = res.getCategory();
					if (StringUtils.isNotBlank(category)) {
						if (category.equals("spOpsSummary")) {
							node.getDynaAttr().put("operationSummary", res.getComments());
						}
						
						if (category.equals("sp24hrsSummary")) {
							node.getDynaAttr().put("last24hrsDrillScene", res.getComments());
						}
						
						if (category.equals("sp24hrsComm")) {
							node.getDynaAttr().put("last24hrsCommunications", res.getComments());
						}
					}
				} 
			}
			
			//Ticket - 18136		
			String strSqlBop = "SELECT hseEventdatetime FROM HseIncident WHERE hseEventdatetime <= : thisReportDatetime AND (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid AND dailyUid = :thisdailyUid AND incidentCategory = : thisIncidentCategory ORDER BY hse_eventdatetime DESC";
			String[] paramsFieldsBop = {"thisReportDatetime","thisdailyUid","thisOperationUid","thisIncidentCategory"};
			Object[] paramsValuesBop = {thisReport.getReportDatetime() ,thisReport.getDailyUid(),thisReport.getOperationUid(),"BOP Test/Drill"};
			List lstResultBop = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlBop, paramsFieldsBop, paramsValuesBop);
			
			if (lstResultBop.size() > 0 && lstResultBop != null) {
				Date hseDate = (Date) lstResultBop.get(0);
				double usecLapsed = thisReport.getReportDatetime().getTime() - hseDate.getTime(); 
				double daysSinceLastbopHseInc = (usecLapsed / 86400) / 1000; //to get days
				node.getDynaAttr().put("daysSinceLastbopHseInc",(int) daysSinceLastbopHseInc);
			}
			else {
				double daysSinceLastbopHseInc = 0;
				node.getDynaAttr().put("daysSinceLastbopHseInc",(int) daysSinceLastbopHseInc);
			}//end 18136
			
			//Ticket - 22086
			sumDailyHseObservationCards(node,thisReport);
		}
		
		String wellExplorerOperationUid = request == null ? null : request.getParameter(WellExplorerConstant.OPERATION_UID);
		String currentDailyUid = null;
		UserSession session = null;
		if (request != null) {
			session = UserSession.getInstance(request);
		}
		
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);
		
		String nodeobject = "ReportDaily";
		if (node.getData().equals(nodeobject)) {
			ReportDailyUtils.setDailyLockFlag(node);
		}
		
		double totalActivityDuration = ActivityUtils.recalculateTotalDuration(commandBean,userSelection.getDailyUid(),userSelection.getOperationUid());
		
		String operationHour = "24";
		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "act_show_warning_if_not_24"))) {	
			String newActivityRecordInputMode = null;
			
			if (!("1".equals(newActivityRecordInputMode))) {
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
				
				thisConverter.setBaseValue(86400);
				if (StringUtils.isNotBlank(operationHour)) {
					Double totalHoursInSecond = Double.parseDouble(operationHour);
					if (!totalHoursInSecond.isNaN()) {
						totalHoursInSecond = totalHoursInSecond * 3600.0;
						thisConverter.setBaseValue(totalHoursInSecond);
					}
				}
				
				if (object instanceof ReportDaily) {
				    ReportDaily reportDaily = (ReportDaily) object;
				    if (!StringUtils.equals("PRD", reportDaily.getReportType())) { // Don't display for PRD Operation Type
        				if (!thisConverter.getFormattedValue(totalActivityDuration).equals(thisConverter.getFormattedValue()) && !this.ignore24HourChecking && (StringUtils.isNotBlank(this.defaultReportType) && !StringUtils.equals(this.defaultReportType, "DGR"))) {
        					commandBean.getSystemMessage().addInfo("Total hours for activities in the day (" + thisConverter.getFormattedValue(totalActivityDuration) + ") is not " + operationHour + " hours.");
        				}
				    }
				}
			}
		}
		if (object instanceof ReportDaily) {			
			ReportDaily reportDaily = (ReportDaily) object;
			Double dayCost = reportDaily.getDaycost();
			CustomFieldUom converter = new CustomFieldUom(commandBean, ReportDaily.class, "daycost");
			
			if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CARRY_DAY_COST_FROM_PREV_OP))) {	
				if (converter.isUOMMappingAvailable()) {
					node.setCustomUOM("@dayCostFromPrevOpLastDay", converter.getUOMMapping());
					
					if (dayCost!=null) {
						converter.setBaseValue(dayCost);
						node.getDynaAttr().put("dayCostFromPrevOpLastDay", converter.getConvertedValue());
					} else node.getDynaAttr().put("dayCostFromPrevOpLastDay", "");
				}
				
				Double dayTangibleCost = reportDaily.getDaytangiblecost();
				converter.setReferenceMappingField(ReportDaily.class, "daytangiblecost");
				if (converter.isUOMMappingAvailable()) {
					node.setCustomUOM("@dayTangibleCostFromPrevOpLastDay", converter.getUOMMapping());
					if (dayTangibleCost!=null) {	
						converter.setBaseValue(dayTangibleCost);
						node.getDynaAttr().put("dayTangibleCostFromPrevOpLastDay", converter.getConvertedValue());
					} else node.getDynaAttr().put("dayTangibleCostFromPrevOpLastDay", "");
				}
				
				Double costAdjust = reportDaily.getCostadjust();
				converter.setReferenceMappingField(ReportDaily.class, "costadjust");
				if (converter.isUOMMappingAvailable()) {
					node.setCustomUOM("@costAdjustFromPrevOpLastDay", converter.getUOMMapping());
					if (costAdjust!=null) {	
						converter.setBaseValue(costAdjust);
						node.getDynaAttr().put("costAdjustFromPrevOpLastDay", converter.getConvertedValue());
					} else node.getDynaAttr().put("costAdjustFromPrevOpLastDay", "");
				}
				
				Double completionCost = reportDaily.getDaycompletioncost();
				converter.setReferenceMappingField(ReportDaily.class, "daycompletioncost");
				if (converter.isUOMMappingAvailable()) {
					node.setCustomUOM("@completionCostFromPrevOpLastDay", converter.getUOMMapping());
					if (completionCost!=null) {	
						converter.setBaseValue(completionCost);
						node.getDynaAttr().put("completionCostFromPrevOpLastDay", converter.getConvertedValue());
					} else node.getDynaAttr().put("completionCostFromPrevOpLastDay", "");
				}
				
				Double otherCost = reportDaily.getOtherCost();
				converter.setReferenceMappingField(ReportDaily.class, "otherCost");
				if (converter.isUOMMappingAvailable()) {
					node.setCustomUOM("@otherCostFromPrevOpLastDay", converter.getUOMMapping());
					if (otherCost!=null) {
						converter.setBaseValue(otherCost);
						node.getDynaAttr().put("otherCostFromPrevOpLastDay", converter.getConvertedValue());
					} else node.getDynaAttr().put("otherCostFromPrevOpLastDay", "");
				}
				
				Double testCost = reportDaily.getDaytestcost();
				converter.setReferenceMappingField(ReportDaily.class, "daytestcost");
				if (converter.isUOMMappingAvailable()) {
					node.setCustomUOM("@testCostFromPrevOpLastDay", converter.getUOMMapping());
					if (testCost!=null) {
						converter.setBaseValue(testCost);
						node.getDynaAttr().put("testCostFromPrevOpLastDay", converter.getConvertedValue());
					} else node.getDynaAttr().put("testCostFromPrevOpLastDay", "");
				}
				
				String commentCost = reportDaily.getCommentCost();
				if (commentCost!=null) {
					node.getDynaAttr().put("commentCostFromPrevOpLastDay", commentCost);
				}
			}
			
			//need to have calc base on each day
			currentDailyUid = reportDaily.getDailyUid();
			
			Double daycostFromCostNet = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(currentDailyUid);
			if (daycostFromCostNet!=null) {
				reportDaily.setDaycost(daycostFromCostNet);
				node.getDynaAttr().put("allowEditDayCost", -1);
			}
			
			//get operationHour from Operation
			String[] paramNames = {"operationUid"};
			Object[] paramValues = {reportDaily.getOperationUid()};
			String strSql5 = "SELECT operationHour FROM Operation WHERE operationUid = :operationUid";
			List<Object> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramNames, paramValues);
			Object b = lstResult5.get(0);
			if (b != null) {
				node.getDynaAttr().put("operationHour", b.toString());
			}
			
			// get the current date from the session
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
			if (daily == null) return;
			Date todayDate = daily.getDayDate();
			node.getDynaAttr().put("daydate", todayDate);
			
			// set dynamic date and dynamic end time		
			if ("DGR".equals(this.defaultReportType)) {
				Date nextDate = DateUtils.addDays(todayDate, 1);
				node.getDynaAttr().put("dgrReportDateTo", nextDate);

				if(reportDaily.getDailyReportStartDatetime() != null) {
					Date startTime = reportDaily.getDailyReportStartDatetime();
					Date endTime = DateUtils.addHours(startTime, 24);
					node.getDynaAttr().put("dailyReportEndDatetime", endTime);
				}			
			}
			
			if (session != null) {
				if (BooleanUtils.isTrue(session.getCombinedAclGroups().getDisableSettingQcFlag())) {
					node.getDynaAttr().put("disableSettingQcFlag", "true");
				}
			}
			
			String operationUid = reportDaily.getOperationUid();
			String wellboreUid = reportDaily.getWellboreUid();
			String wellUid = reportDaily.getWellUid();
			
			// run here only if this is not a request from well explorer. purpose for this is to skip all below 
			// to speed up loading time for well explorer screen that only require a few fields that does not
			// include the following fields.
			if (StringUtils.isBlank(wellExplorerOperationUid)) {
				
				// for FWR, the report type cannot specific from the fwr.xml, so need to use priority to get the report type.
				if (("FWR".equalsIgnoreCase(this.reportGenerated)) || ("MGT".equalsIgnoreCase(this.reportGenerated))) {
					this.defaultReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
				}
			
				String[] paramsFields = {"todayDate", "operationUid", "reportType"};
				Object[] paramsValues = new Object[3]; 
				paramsValues[0] = todayDate; 
				paramsValues[1] = operationUid;
				paramsValues[2] = this.defaultReportType;
				
				Operation thisOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
				
				Double cumCost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(currentDailyUid, reportDaily.getReportType());
				
				String strSql = "select sum(daycompletioncost) as cumcompletioncost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				Object a = lstResult.get(0);
				Double cumcompletioncost = null;
				if (a != null) cumcompletioncost = Double.parseDouble(a.toString());
				
				//calculate sum of costs
				node.getDynaAttr().put("isGWPcarryCostOn", "false");
				if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CARRY_DAY_COST_FROM_PREV_OP))) {
					String[] paramsFieldsOps = {"thisStartDate", "thisWellUid"};
					Object[] paramsValuesOps = {thisOperation.getStartDate(), thisOperation.getWellUid()};
					String strSqlOps = "SELECT operationUid from Operation WHERE (isDeleted = false or isDeleted is null) AND startDate <= :thisStartDate AND wellUid = :thisWellUid";
					List<String> opsUid = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlOps, paramsFieldsOps, paramsValuesOps);
					String[] paramsFieldsOps2 = {"operationUid", "thisDate"};
					Object[] paramsValuesOps2 = {opsUid, todayDate};
					
					Double cumcompletioncostFromPrevOps = null;
					Double cumothercostFromPrevOps = null;
					Double cumtestcostFromPrevOps = null;
					
					if (!opsUid.isEmpty()) {
						strSql = "select sum(daycompletioncost) as cumcompletioncost from ReportDaily where (isDeleted = false or isDeleted is null) and reportDatetime <= :thisDate and (operationUid in (:operationUid))";
						lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldsOps2, paramsValuesOps2);
						a = lstResult.get(0);
						if (a != null) cumcompletioncostFromPrevOps = Double.parseDouble(a.toString());
						
						strSql = "select sum(otherCost) as cumothercost from ReportDaily where (isDeleted = false or isDeleted is null) and reportDatetime <= :thisDate and (operationUid in (:operationUid))";
						lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldsOps2, paramsValuesOps2);
						a = lstResult.get(0);
						if (a != null) cumothercostFromPrevOps = Double.parseDouble(a.toString());
						
						strSql = "select sum(daytestcost) as cumtestcost from ReportDaily where (isDeleted = false or isDeleted is null) and reportDatetime <= :thisDate and (operationUid in (:operationUid))";
						lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldsOps2, paramsValuesOps2);
						a = lstResult.get(0);
						if (a != null) {
							cumtestcostFromPrevOps = Double.parseDouble(a.toString());					
						}
						reportDaily.setCumtestcost(cumtestcostFromPrevOps);
					}
					
					Double cumtotalcostFromPrevOps = null;
					if ((reportDaily.getCumCostFromPrevOperation()!=null) || (reportDaily.getCumTangibleCostFromPrevOperation() !=null) || (cumcompletioncostFromPrevOps !=null) || (cumothercostFromPrevOps !=null)) {
						cumtotalcostFromPrevOps = 0.0;
						if (reportDaily.getCumCostFromPrevOperation()!=null) cumtotalcostFromPrevOps += reportDaily.getCumCostFromPrevOperation();
						if (reportDaily.getCumTangibleCostFromPrevOperation()!=null) cumtotalcostFromPrevOps += reportDaily.getCumTangibleCostFromPrevOperation();
						if (cumcompletioncostFromPrevOps!=null) cumtotalcostFromPrevOps += cumcompletioncostFromPrevOps;
						if (cumothercostFromPrevOps!=null) cumtotalcostFromPrevOps += cumothercostFromPrevOps;
						if (cumtestcostFromPrevOps!=null) cumtotalcostFromPrevOps += cumtestcostFromPrevOps;
					}
					
					if (cumcompletioncostFromPrevOps!=null) {
						if (converter.isUOMMappingAvailable()) {
							converter.setReferenceMappingField(ReportDaily.class, "daycompletioncost");
							converter.setBaseValue(cumcompletioncostFromPrevOps);
							node.setCustomUOM("@cumCompletionCostFromPrevOpLastDay", converter.getUOMMapping());
						}
						node.getDynaAttr().put("cumCompletionCostFromPrevOpLastDay", converter.getConvertedValue());
					}
					
					if (cumothercostFromPrevOps!=null) {
						if (converter.isUOMMappingAvailable()) {
							converter.setReferenceMappingField(ReportDaily.class, "otherCost");
							converter.setBaseValue(cumothercostFromPrevOps);
							node.setCustomUOM("@cumOtherCostFromPrevOpLastDay", converter.getUOMMapping());
						}
						node.getDynaAttr().put("cumOtherCostFromPrevOpLastDay", converter.getConvertedValue());
					}
					
					if (cumtotalcostFromPrevOps!=null) {
						converter.setBaseValue(cumtotalcostFromPrevOps);
						if (converter.isUOMMappingAvailable()) {
							node.setCustomUOM("@cumtotalcostFromPrevOpLastDay", converter.getUOMMapping());
						}
						node.getDynaAttr().put("cumtotalcostFromPrevOpLastDay", converter.getConvertedValue());
					}
					node.getDynaAttr().put("isGWPcarryCostOn", "true");
				}
			
				//calculate cum. tangible cost
				strSql = "select sum(daytangiblecost) as cumtangiblecost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				a = lstResult.get(0);
				Double cumtangiblecost = null;
				if (a != null) cumtangiblecost = Double.parseDouble(a.toString());
			
				strSql = "select sum(otherCost) as cumothercost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				a = lstResult.get(0);
				Double cumothercost = null;
				if (a != null) cumothercost = Double.parseDouble(a.toString());
				
				strSql = "select sum(daytestcost) as cumtestcost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				a = lstResult.get(0);
				Double cumtestcost = null;
				if (a != null) {
					cumtestcost = Double.parseDouble(a.toString());
				}
				if ("0".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CARRY_DAY_COST_FROM_PREV_OP)))
					reportDaily.setCumtestcost(cumtestcost);
				
				Double totalcost = null;
				if (reportDaily.getDaycost() != null || reportDaily.getDaycompletioncost() != null || reportDaily.getDaytangiblecost() != null || reportDaily.getOtherCost()!= null || reportDaily.getDaytestcost()!=null)  totalcost = 0.0;
				if (reportDaily.getDaycost() != null) totalcost = totalcost + reportDaily.getDaycost();
				if (reportDaily.getDaycompletioncost() != null)totalcost =  totalcost + reportDaily.getDaycompletioncost();
				if (reportDaily.getDaytangiblecost() != null)totalcost =  totalcost + reportDaily.getDaytangiblecost();
				if (reportDaily.getOtherCost() != null)totalcost =  totalcost + reportDaily.getOtherCost();
				if (reportDaily.getDaytestcost()!=null) totalcost = totalcost + reportDaily.getDaytestcost();
				
				Double cumtotalcost = null;
				if ((cumCost!=null) || (cumcompletioncost !=null) || (cumtangiblecost !=null) || (cumothercost !=null)) {
					cumtotalcost = 0.0;
					if (cumCost!=null) cumtotalcost += cumCost;
					if (cumcompletioncost!=null) cumtotalcost += cumcompletioncost;
					if (cumtangiblecost!=null) cumtotalcost += cumtangiblecost;
					if (cumothercost!=null) cumtotalcost += cumothercost;
					if (cumtestcost!=null) cumtotalcost += cumtestcost;
				}
				
				//assign unit to the value base on daily cost unit
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "daycost");
				
				if (cumCost!=null) {
					thisConverter.setBaseValue(cumCost);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@cumcost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumcost", thisConverter.getConvertedValue());
				}
				
				if (cumcompletioncost!=null) {
					thisConverter.setBaseValue(cumcompletioncost);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@cumcompletioncost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumcompletioncost", thisConverter.getConvertedValue());
				}
				
				if (totalcost!=null) {
					thisConverter.setBaseValue(totalcost);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@totalcost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("totalcost", thisConverter.getConvertedValue());
				}
				
				if (cumtotalcost!=null) {
					thisConverter.setBaseValue(cumtotalcost);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@cumtotalcost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumtotalcost", thisConverter.getConvertedValue());
				}
				
				if (cumtangiblecost!=null) {
					thisConverter.setBaseValue(cumtangiblecost);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@cumtangiblecost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumtangiblecost", thisConverter.getConvertedValue());
				}
				
				if (cumothercost!=null) {
					thisConverter.setBaseValue(cumothercost);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@cumothercost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumothercost", thisConverter.getConvertedValue());
				}
				
				ReportDailyUtils.getOperationAfeAmt(node, userSelection.getOperationUid());
				
				Double fluidCostToDate = CommonUtil.getConfiguredInstance().calculateCummulativeRigStockCost(operationUid, "fluids", daily);
				if (fluidCostToDate!=null) {
					thisConverter.setReferenceMappingField(RigStock.class, "itemCost");
					node.getDynaAttr().put("fluidsTotalCost", fluidCostToDate);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@fluidsTotalCost", thisConverter.getUOMMapping());
					}
				}			
				
				//calculate cum. actual vessel cost = Actual Daily Vessel Cost * total activity duration to date in day
				Double cumActualVesselCost = null;				
				cumActualVesselCost = CommonUtil.getConfiguredInstance().CalculatecumActualVesselCost(reportDaily.getOperationUid(),reportDaily.getDailyUid());
						
				//assign unit to the value base on daily cost unit
				thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "actualVesselCost");
				
				if (cumActualVesselCost!=null) {
					thisConverter.setBaseValue(cumActualVesselCost);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@cumActualVesselCost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumActualVesselCost", thisConverter.getConvertedValue());
				}
				
				//get the value for days on well = sum of activity hour + days spend prior to spud
				thisConverter.setReferenceMappingField(Operation.class, "days_spent_prior_to_spud");
				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysOnOperation(operationUid, daily.getDailyUid()));
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysOnWell", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysOnWell", thisConverter.getConvertedValue());
				
				//get the value for day curve
				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().dayCurve(daily.getWellUid(),daily.getWellboreUid(),operationUid, daily.getDailyUid()));
				if(thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@dayCurve", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("dayCurve", thisConverter.getConvertedValue());
				
				//get the value for days from rig move based on mob. start date
				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysFromRigMoveForOperation(operationUid, daily.getDailyUid()));
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysFromRigMove", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysFromRigMove", thisConverter.getConvertedValue());
				
				//get the value for days from rig move based on rig acceptance date
				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysFromRigAcceptenceForOperation(operationUid, daily.getDailyUid()));
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysFromRigAcceptance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysFromRigAcceptance", thisConverter.getConvertedValue());
				
				//Sidetrack
				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysOnOperationSiderack(operationUid, daily.getDailyUid()));
								
				Wellbore currentWellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(wellboreUid);
				if (currentWellbore != null) {
					node.getDynaAttr().put("wellboreType", currentWellbore.getWellboreType());
				}
	
				reportDaily.setDaysSinceSidetrack(thisConverter.getConvertedValue());
				String strSql2 = "UPDATE ReportDaily SET daysSinceSidetrack =:daysSinceSidetrack WHERE reportDailyUid = :thisReportDailyUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, new String[] {"daysSinceSidetrack", "thisReportDailyUid"}, new Object[] {thisConverter.getBasevalue(), reportDaily.getReportDailyUid()});
				
				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysFromSpudOnOperation(operationUid, daily.getDailyUid()));
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysFromSpud", thisConverter.getUOMMapping());
				}
				if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CALCULATE_DAYS_SINCE_SPUD_WELL))) {
					node.getDynaAttr().put("daysFromSpud", "");
				} else { 
					node.getDynaAttr().put("daysFromSpud", thisConverter.getConvertedValue());
				}
				
				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysFromSpudOnOperationByDate(operationUid, daily.getDailyUid(), this.daysFromSpudByDateCalcType));
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysFromSpud_SinceOpsSpudDate", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysFromSpud_SinceOpsSpudDate", thisConverter.getConvertedValue());
				
				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysOnLocationForOperation(operationUid, daily.getDailyUid()));
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysOnLocation", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysOnLocation", thisConverter.getConvertedValue());

				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysFromReEntryOperation(operationUid, daily.getDailyUid(), userSelection.getGroupUid()));				
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysFromReEntry", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysFromReEntry", thisConverter.getConvertedValue());
				
				Double drllgdays = CommonUtil.getConfiguredInstance().daysOnOperationByType(wellboreUid, daily.getDailyUid(),"DRLLG");
				if (drllgdays==null) drllgdays=0.0;
				thisConverter.setBaseValue(drllgdays);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysOnDrllg", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysOnDrllg",thisConverter.getConvertedValue());
				
				Double cmpltdays = CommonUtil.getConfiguredInstance().daysOnOperationByType(wellboreUid, daily.getDailyUid(),"CMPLT");
				if (cmpltdays==null) cmpltdays=0.0;
				thisConverter.setBaseValue(cmpltdays);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysOnCmplt", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysOnCmplt",thisConverter.getConvertedValue());
				
				Double daysOnDrllgByWell = CommonUtil.getConfiguredInstance().daysOnOperationByTypeByWell(wellUid, daily.getDailyUid(),"DRLLG");
				if (daysOnDrllgByWell==null) daysOnDrllgByWell=0.0;
				thisConverter.setBaseValue(daysOnDrllgByWell);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysOnDrllgByWell", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysOnDrllgByWell",thisConverter.getConvertedValue());
				
				Double daysOnCmpltByWell = CommonUtil.getConfiguredInstance().daysOnOperationByTypeByWell(wellUid, daily.getDailyUid(),"CMPLT");
				if (daysOnCmpltByWell==null) daysOnCmpltByWell=0.0;
				thisConverter.setBaseValue(daysOnCmpltByWell);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysOnCmpltByWell", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysOnCmpltByWell",thisConverter.getConvertedValue());
											
				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysFromDryHole(operationUid, daily.getDailyUid(), userSelection.getGroupUid()));				
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysFromDryHole", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysFromDryHole", thisConverter.getConvertedValue());
				
				//the gwp is for control to either use the new method of calculation for days since last lti or the default calculation
				if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_DAYS_SINCE_LAST_LTI_BASED_ON_CONTRACT))) {					
					String groupUid = userSelection.getGroupUid();
					String rigInformationUid = userSelection.getRigInformationUid();
					
					if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
						Date reportDate = reportDaily.getReportDatetime();
						String queryString = "FROM RigInformation r, RigContract rc " +
								"WHERE (r.isDeleted=false or r.isDeleted is null) " +
								"AND (rc.isDeleted=false or rc.isDeleted is null) " +
								"AND r.rigInformationUid=rc.rigInformationUid " +
								"AND r.rigInformationUid=:rigInformationUid " +
								"AND rc.contractStartDatetime<=:reportDate " +
								"AND rc.contractEndDatetime>=:reportDate " +
								"AND r.groupUid=:groupUid ";
						List contractList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"groupUid", "rigInformationUid","reportDate"}, new Object[] {groupUid, rigInformationUid, reportDate});
						if (contractList.isEmpty()) {
							commandBean.getSystemMessage().addInfo("Day not used in calculation for rig utilisation days.");
						}
					}					
				} else {
					//If gwp is turn off the days since lta value will be auto populated by using default calculation.
					if ("0".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_MANUAL_INCREMENT_DAYS_SINCE_LTI_EVENT))) {
						if (reportDaily.getLastLtiDate() != null) {
							double daysSinceLta = CommonUtil.getConfiguredInstance().calculateDaysSinceHseIncident(todayDate, reportDaily.getLastLtiDate(), userSelection.getGroupUid());
							thisConverter.setReferenceMappingField(ReportDaily.class, "days_since_lta");
							thisConverter.setBaseValue(daysSinceLta);
							reportDaily.setDaysSinceLta(thisConverter.getConvertedValue());
						} else {
							reportDaily.setDaysSinceLta(null);
						}
					}
				}
				
				//auto lookup last BOP for current rig on data node load (not input template)
				if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_LAST_BOP))) {
					BopLog thisBopLog = CommonUtil.getConfiguredInstance().lastBopDate(userSelection.getRigInformationUid(), currentDailyUid);					
					if (thisBopLog != null) {	
						reportDaily.setLastbopDate(thisBopLog.getTestingDatetime());
						reportDaily.setNextBopDate(thisBopLog.getNextTestDatetime());
					} else {//15395 - if remove date is entered
						
						//set the calendar
						Calendar thisCalendar = Calendar.getInstance();
						thisCalendar.setTime(todayDate);
						thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
						thisCalendar.set(Calendar.MINUTE, 59);
						thisCalendar.set(Calendar.SECOND , 59);
						thisCalendar.set(Calendar.MILLISECOND , 59);
						
						String strSql6 = "SELECT bl FROM Bop b, BopLog bl WHERE (b.bopUid= bl.bopUid) and (b.isDeleted = false or b.isDeleted is null) and (bl.isDeleted = false or bl.isDeleted is null) and b.removeDate is NOT null and b.rigInformationUid = :currentRigInformationUid AND bl.testingDatetime <= :currentDate ORDER BY bl.testingDatetime DESC";
						String[] paramsFields6 = {"currentRigInformationUid", "currentDate"};
						Object[] paramsValues6 = {userSelection.getRigInformationUid(), thisCalendar.getTime()};		
								
						List lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramsFields6, paramsValues6);
						
						if (lstResult6.size() > 0){	
							BopLog thisBopLogRemove = (BopLog) lstResult6.get(0);
							reportDaily.setLastbopDate(thisBopLogRemove.getTestingDatetime());
							reportDaily.setNextBopDate(thisBopLogRemove.getNextTestDatetime());
						}
						else {
							reportDaily.setLastbopDate(null);
							reportDaily.setNextBopDate(null);
						}
					}
				}

				//auto lookup casing section data
				if (session != null) {
					ReportDailyUtils.autoLookupCasingSectionData(commandBean, node, session);
					ReportDailyUtils.autoLookupNextCasingSectionData(commandBean, node, session);
					ReportDailyCasingUtils.autoLookupLOTFIT(commandBean, node, session);
				} 
				
				//calculate cum. deffered prod
				strSql = "select sum(daydeferredprod) as cumdeferredprod from ReportDaily where reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid and (isDeleted=false or isDeleted is null)";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				a = lstResult.get(0);
				Double cumdeferredprod = null;
				if (a != null) cumdeferredprod = Double.parseDouble(a.toString());
				
				if (cumdeferredprod!=null) {
					thisConverter.setReferenceMappingField(ReportDaily.class, "daydeferredprod");
					thisConverter.setBaseValue(cumdeferredprod);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@cumdeferredprod", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumdeferredprod", thisConverter.getConvertedValue());
				}
				
				//calculate pob
				String[] paramsFields2 = {"thisOperationUid", "thisDailyUid"};
		 		Object[] paramsValues2 = new Object[2];
				paramsValues2[0] = operationUid;
				paramsValues2[1] = currentDailyUid;
				
				strSql = "select sum(pax) as totalpax from PersonnelOnSite where operationUid = :thisOperationUid and dailyUid = :thisDailyUid and (isDeleted=false or isDeleted is null)";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
				a = lstResult.get(0);
				Double totalpax = null;
				if (a != null) totalpax = Double.parseDouble(a.toString());
				node.getDynaAttr().put("comment_pob", totalpax);
				
				if (("DGR".equalsIgnoreCase(this.defaultReportType)) || ("CMPLT".equalsIgnoreCase(this.defaultReportType))) {
					ReportDailyUtils.getDrillerInfo(node, operationUid, userSelection.getGroupUid(), todayDate, commandBean, autoCalculateDepthTvdSs);
				}
				
				// AUTO POPULATE QC FLAG
				if ("DGR".equals(this.defaultReportType) && autoPopulateQCflagFromDDR) {
					if (!StringUtils.isEmpty(currentDailyUid)) {
						String strSql3 = "SELECT qcFlag FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND dailyUid = :selectedDailyUid AND reportType = 'DDR'";
						List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, new String[] {"selectedDailyUid"}, new Object[] {currentDailyUid});
						if (!lstResult3.isEmpty()) {
							Object a3 = lstResult3.get(0);
							node.getDynaAttr().put("ddrQcFlag", a3.toString());
						} else {
							node.getDynaAttr().put("ddrQcFlag", "");
						}
					}
				}
				
				//GET ORIGINAL AFE AMOUNT, SUPPLIMENTARY AFE AMOUNT AND CUMULATIVE AFE AMOUNT
				if (thisOperation != null) {
					Double originalAfe = null;
					if (thisOperation.getAfe() != null) {
						originalAfe = thisOperation.getAfe();
					}
					List<Object[]> afeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationAfe o, CostAfeMaster c where (o.isDeleted=false or o.isDeleted is null) and (c.isDeleted=false or c.isDeleted is null) and (o.afeType=:afeType) and o.costAfeMasterUid=c.costAfeMasterUid and o.operationUid=:operationUid", new String[] {"afeType", "operationUid"}, new Object[] {CostNetConstants.MASTER_AFE, thisOperation.getOperationUid()});
					if (afeList.size() > 0) {
						String afeNumber = "";
						Double afeTotal = null;
						for (Object[] rec : afeList) {
							CostAfeMaster costAfeMaster = (CostAfeMaster) rec[1];
							afeNumber += ("".equals(afeNumber)?"":"\n") + costAfeMaster.getAfeNumber();
							Double thisAfeTotal = CommonUtil.getConfiguredInstance().calculateAfeTotal(costAfeMaster.getCostAfeMasterUid(), thisOperation.getOperationUid());
							if (thisAfeTotal != null) {
								if (afeTotal == null) afeTotal = 0.0;
								afeTotal += thisAfeTotal;
							}
						}
						thisOperation.setAfeNumber(afeNumber);
						originalAfe = afeTotal;
					}
					
					if (originalAfe != null) {
						thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
						thisConverter.setBaseValue(originalAfe);
						if (thisConverter.isUOMMappingAvailable()) {
							node.setCustomUOM("@afe", thisConverter.getUOMMapping());
						}
						node.getDynaAttr().put("afe", thisConverter.getConvertedValue());
					} else {
                        node.getDynaAttr().put("afe", originalAfe);             
                    }     
					Double remainingOnAFE = null;
					if (originalAfe==null && cumtotalcost==null) {
						node.getDynaAttr().put("remainingOnAFE", remainingOnAFE);
					} else {
						if (originalAfe==null) originalAfe = 0.0;
						if (cumtotalcost==null) cumtotalcost = 0.0;
						remainingOnAFE = originalAfe - cumtotalcost;
						thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
						thisConverter.setBaseValue(remainingOnAFE);
						node.setCustomUOM("@remainingOnAFE", thisConverter.getUOMMapping());
						node.getDynaAttr().put("remainingOnAFE", thisConverter.getConvertedValue());
					}
					
					Double supplementaryAfe = null;
					if (thisOperation.getSecondaryafeAmt() != null) {
						supplementaryAfe = thisOperation.getSecondaryafeAmt();
					}
					afeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationAfe o, CostAfeMaster c where (o.isDeleted=false or o.isDeleted is null) and (c.isDeleted=false or c.isDeleted is null) and (o.afeType=:afeType or o.afeType='' or o.afeType is null) and o.costAfeMasterUid=c.costAfeMasterUid and o.operationUid=:operationUid", new String[] {"afeType", "operationUid"}, new Object[] {CostNetConstants.SUPPLEMENTARY_AFE, thisOperation.getOperationUid()});
					if (afeList.size() > 0) {
						String afeNumber = "";
						Double afeTotal = null;
						for (Object[] rec : afeList) {
							CostAfeMaster costAfeMaster = (CostAfeMaster) rec[1];
							afeNumber += ("".equals(afeNumber)?"":"\n") + costAfeMaster.getAfeNumber();
							Double thisAfeTotal = CommonUtil.getConfiguredInstance().calculateAfeTotal(costAfeMaster.getCostAfeMasterUid(), thisOperation.getOperationUid());
							if (thisAfeTotal != null) {
								if (afeTotal == null) afeTotal = 0.0;
								afeTotal += thisAfeTotal;
							}
						}
						thisOperation.setSecondaryafeRef(afeNumber);
						supplementaryAfe = afeTotal;
					}
					if (supplementaryAfe != null) {
						thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
						thisConverter.setBaseValue(supplementaryAfe);
						if (thisConverter.isUOMMappingAvailable()) {
							node.setCustomUOM("@secondaryafeAmt", thisConverter.getUOMMapping());
						}
						node.getDynaAttr().put("secondaryafeAmt", thisConverter.getConvertedValue());
					} else {
						node.getDynaAttr().put("secondaryafeAmt", supplementaryAfe);
					}
					Double totalafe = null;
					if (originalAfe != null || supplementaryAfe!=null) totalafe = 0.0;
					if (originalAfe != null) totalafe += originalAfe;
					if (supplementaryAfe != null) totalafe += supplementaryAfe;
					if (totalafe != null) {
						thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
						thisConverter.setBaseValue(totalafe);
						if (thisConverter.isUOMMappingAvailable()) {
							node.setCustomUOM("@cumafeAmt", thisConverter.getUOMMapping());
						}
						node.getDynaAttr().put("cumafeAmt", thisConverter.getConvertedValue());
					}
					node.getDynaAttr().put("afeNumber", thisOperation.getAfeNumber());
					node.getDynaAttr().put("secondaryafeRef", thisOperation.getSecondaryafeRef());
				}
				
				//calculate cumulative Safety Observation
				//query statement for getting the sum of stopday from the report daily screen
				strSql = "select sum(stopday) as cumstopday from ReportDaily where reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid and (isDeleted=false or isDeleted is null)";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				//query return and set to cumstopday
				a = lstResult.get(0);
				Double cumstopday = null;
				if (a != null) cumstopday = Double.parseDouble(a.toString());
				
				//get the field mapping and assign to cumstopday
				if (cumstopday != null) {
					thisConverter.setReferenceMappingField(ReportDaily.class, "stopday");
					thisConverter.setBaseValue(cumstopday);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@cumstopday", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumstopday", thisConverter.getConvertedValue());
				}

				//query statement for getting the sum of hseobservationcard from the report daily screen from previous well. This is filtered by selected Rig on Daily screen. 
				//Ticket no: 20423
				//Based on ticket 24667, also need to filter by not including deleted days 
				String paramsField[] = {"todayDate", "operationUid", "reportType", "rigInformationUid"};
				Object paramsValue[] = {todayDate, operationUid , this.defaultReportType, userSelection.getRigInformationUid()};
				strSql = "select sum(cumObservationCard) as prevcumhseobservationcard from ReportDaily where reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid and rigInformationUid =:rigInformationUid and (isDeleted = false or isDeleted is null)";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				//query return and set to cumhsecard
				a = lstResult.get(0);
				Integer cumhsecard = null;
				if (a != null) cumhsecard = Integer.parseInt(a.toString());
				
				//get the field mapping and assign to cumhsecard
				if (thisOperation.getPrevCumHseObservationCard() != null && cumhsecard != null) {
					cumhsecard = cumhsecard + thisOperation.getPrevCumHseObservationCard();
				}
				if (thisOperation.getPrevCumHseObservationCard() != null && cumhsecard == null) {
					cumhsecard = thisOperation.getPrevCumHseObservationCard();
				}
				
				if (cumhsecard != null) {
					thisConverter.setReferenceMappingField(ReportDaily.class, "cumObservationCard");
					thisConverter.setBaseValue(cumhsecard);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@prevcumhseobservationcard", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("prevcumhseobservationcard", thisConverter.getConvertedValue());
				}

				//query statement for getting the sum of hseobservationcard from the report daily screen
				//Based on ticket 24667, also need to filter by not including deleted days 
				strSql = "select sum(hseObservationCard) as cumhseobservationcard from ReportDaily where reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid and (isDeleted = false or isDeleted is null)";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				//query return and set to cumhseobservationcard
				a = lstResult.get(0);
				Double cumhseobservationcard = null;
				if (a != null) cumhseobservationcard = Double.parseDouble(a.toString());
				
				//get the field mapping and assign to cumhseobservationcard
				if (cumhseobservationcard != null) {
					thisConverter.setReferenceMappingField(ReportDaily.class, "hseObservationCard");
					thisConverter.setBaseValue(cumhseobservationcard);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@cumhseobservationcard", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumhseobservationcard", thisConverter.getConvertedValue());
				}
				
				//calculate the Days Since Last BOP test
				//ticket - 15395
				//check Last BOP Date is empty/null
				if (reportDaily.getLastbopDate() != null) {
					double daysSinceLastbop = CommonUtil.getConfiguredInstance().calculateDaysSinceLastBOP(todayDate, reportDaily.getLastbopDate());

					//get the field mapping from the day since lta and assign to days since last bop
					thisConverter.setReferenceMappingField(ReportDaily.class, "days_since_lta"); 
					thisConverter.setBaseValue(daysSinceLastbop);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@daysSinceLastbop", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("daysSinceLastbop", thisConverter.getConvertedValue());
				}//15395
				
				// Getting wellbore.planned_td_md_msl and wellbore.planned_td_tvd_msl - jwong
				String sql = "FROM Wellbore WHERE (isDeleted = false or isDeleted is null) AND wellboreUid = :thisWellboreUid";
				List<Wellbore> thisWellbore = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "thisWellboreUid", thisOperation.getWellboreUid(),qp);
			
				if (thisWellbore.size() > 0) {
					Double PlannedTdMdMsl = null;
					if (thisWellbore.get(0).getPlannedTdMdMsl() != null) {
						PlannedTdMdMsl = thisWellbore.get(0).getPlannedTdMdMsl();
						thisConverter.setReferenceMappingField(Wellbore.class, "plannedTdMdMsl");
						thisConverter.setBaseValue(PlannedTdMdMsl);
						if (thisConverter.isUOMMappingAvailable()) {
							node.setCustomUOM("@plannedTdMdMsl", thisConverter.getUOMMapping());
						}
						node.getDynaAttr().put("plannedTdMdMsl", thisConverter.getConvertedValue());
					}				
					Double PlannedTdTvdMsl = null;
					if (thisWellbore.get(0).getPlannedTdTvdMsl() != null) {
						PlannedTdTvdMsl = thisWellbore.get(0).getPlannedTdTvdMsl();
						thisConverter.setReferenceMappingField(Wellbore.class, "plannedTdTvdMsl");
						thisConverter.setBaseValue(PlannedTdTvdMsl);
						if (thisConverter.isUOMMappingAvailable()) {
							node.setCustomUOM("@plannedTdTvdMsl", thisConverter.getUOMMapping());
						}
						node.getDynaAttr().put("plannedTdTvdMsl", thisConverter.getConvertedValue());
					}
				}
				
				//Calculate cumulative downtime
				Double cumOpsDowntimeDuration = CommonUtil.getConfiguredInstance().calculateCumulativeDowntime(currentDailyUid, reportDaily.getReportType());
				if (cumOpsDowntimeDuration != null && thisConverter.isUOMMappingAvailable()) {
					thisConverter.setReferenceMappingField(ReportDaily.class, "cumOpsDowntimeDuration");
					thisConverter.setBaseValue(cumOpsDowntimeDuration);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@cumOpsDowntimeDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumOpsDowntimeDuration", thisConverter.getConvertedValue());
				}
				
			}
			
			String[] paramsField1 = { "todayDate", "operationUid" };
		    Object[] paramsValue1 = new Object[2];
		    paramsValue1[0] = todayDate;
		    paramsValue1[1] = operationUid;
		    
		    String strSql6 = "select sum(currentDrillLineWork) as cumCurrentDrillLineWork from ReportDaily where (isDeleted = false or isDeleted is null) and reportDatetime <= :todayDate and operationUid = :operationUid";
		    List<Object> lstResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramsField1, paramsValue1);
		    
		    Object objecta = lstResults.get(0);
		    Double cumCurrentDrillLineWork = null;
		    
		    if (objecta != null)
		    	cumCurrentDrillLineWork = Double.valueOf(Double.parseDouble(objecta.toString())); 
		    
		    CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, ReportDaily.class, "currentDrillLineWork");
		    
		    if (cumCurrentDrillLineWork != null && thisConverter2.isUOMMappingAvailable()) {
		    	thisConverter2.setBaseValue(cumCurrentDrillLineWork.doubleValue());
		    	if (thisConverter2.isUOMMappingAvailable())
		    		node.setCustomUOM("@cumCurrentDrillLineWork", thisConverter2.getUOMMapping()); 
		    	
		    	node.getDynaAttr().put("cumCurrentDrillLineWork", Double.valueOf(thisConverter2.getConvertedValue()));
		    } 
			
			//Stop/start cards from HSE screen Ticket:20090831-1034-wchin
			
			Calendar thisCalendar = Calendar.getInstance();
			thisCalendar.setTime(todayDate);
			thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
			thisCalendar.set(Calendar.MINUTE, 59);
			thisCalendar.set(Calendar.SECOND , 59);
			thisCalendar.set(Calendar.MILLISECOND , 59);
			
			String paramsFields[] = {"todayDate", "currentDatetime", "rigInformationUid"};
			Object paramsValues[] = {todayDate, thisCalendar.getTime(), userSelection.getRigInformationUid()};
					
			String strHseSql = "select hseShortdescription from HseIncident where incidentCategory='Stop/Start Cards' and (hseEventdatetime >= :todayDate and hseEventdatetime <=:currentDatetime) " +
					"and rigInformationUid=:rigInformationUid and (isDeleted=false or isDeleted is null) and (supportVesselInformationUid is null or supportVesselInformationUid = '') order by hseEventdatetime DESC";
			List lstHseResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strHseSql, paramsFields, paramsValues);
			String hseShortDescription = null;
			if (!lstHseResult.isEmpty()) {
				Object a = lstHseResult.get(0);
				if (a != null) hseShortDescription = a.toString();
			}
			node.getDynaAttr().put("numberOfStopStartCards", hseShortDescription);
			
			// Cost per Depth
			Double cost = reportDaily.getDaycost();
			if (cost != null) {
				if (cost > 0.0) {
					String queryString = "select min(depthTopMdMsl), max(depthMdMsl) FROM DrillingParameters WHERE (isDeleted=false or isDeleted is null) AND dailyUid=:dailyUid";
					List<Double[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", reportDaily.getDailyUid());
					if (result.size() > 0) {
						Object[] rec = result.get(0);
						if (rec[0]!=null && rec[1]!=null) {
							Double diff = Double.parseDouble(rec[1].toString()) - Double.parseDouble(rec[0].toString());
							if (diff > 0) {
								CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "daycost");
								String output = thisConverter.getFormattedValue(cost / diff, false);
								thisConverter.setReferenceMappingField(DrillingParameters.class, "depthTopMdMsl");
								if (thisConverter.isUOMMappingAvailable()) {
									output += "/"+thisConverter.getUomSymbol();
								}
								node.getDynaAttr().put("costPerDepth", output);
							}
						}
					}
				}
			}
			
			// auto lookup depth from drilling parameter
			Double startDepth = 0.0;
			Double endDepth = 0.0;			
			String strDPSql = "FROM DrillingParameters WHERE (isDeleted=false or isDeleted is null) AND dailyUid = :thisDailyUid";
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strDPSql, "thisDailyUid", currentDailyUid);			
			if (list.size() > 0) {
				String strDpSql1 = "SELECT MIN(depthTopMdMsl), MAX(depthMdMsl) FROM DrillingParameters WHERE (isDeleted=false or isDeleted is null) AND dailyUid = :thisDailyUid";
				List<Double[]> dpResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strDpSql1, "thisDailyUid", currentDailyUid, qp);
				
				Object[] rec = dpResult1.get(0);				
				if (rec[0] != null) startDepth = Double.parseDouble(rec[0].toString());
				if (rec[1] != null) endDepth = Double.parseDouble(rec[1].toString());				
			} else {	
				String paramsFields1[] = {"todayDate", "thisOperationUid"};
				Object paramsValues1[] = {todayDate, operationUid};
				
				String strDpSql2 = "SELECT MAX(dp.depthMdMsl) FROM DrillingParameters dp, Daily d WHERE (dp.isDeleted=false or dp.isDeleted is null) AND (d.isDeleted=false or d.isDeleted is null) AND dp.dailyUid = d.dailyUid AND dp.operationUid = :thisOperationUid AND d.dayDate <= :todayDate";
				List dpResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strDpSql2, paramsFields1, paramsValues1, qp);
				
				Object rec = dpResult2.get(0);					
				if (rec != null) {
					startDepth = Double.parseDouble(rec.toString());
					endDepth = Double.parseDouble(rec.toString());
				}
			}
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "depthTopMdMsl");
			thisConverter.setBaseValue(startDepth);
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@startDepth", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("startDepth", thisConverter.getConvertedValue());
			
			thisConverter.setReferenceMappingField(DrillingParameters.class, "depthMdMsl");
			thisConverter.setBaseValue(endDepth);
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@endDepth", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("endDepth", thisConverter.getConvertedValue());
			
			//calculate daily drilling hours from drilling parameters
			Double dailyDrillingHours = CommonUtil.getConfiguredInstance().calcTotalValueFromDrilParamByDaily("duration", currentDailyUid);
			
			thisConverter.setReferenceMappingField(DrillingParameters.class, "duration");
			thisConverter.setBaseValue(dailyDrillingHours);
			node.getDynaAttr().put("dailyDrillingHours", thisConverter.getConvertedValue());
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@dailyDrillingHours", thisConverter.getUOMMapping());
			}
			
			//calculate daily depth progress from drilling parameters
			Double dailyDepthProgress = CommonUtil.getConfiguredInstance().calcTotalValueFromDrilParamByDaily("progress", currentDailyUid);
			
			thisConverter.setReferenceMappingField(DrillingParameters.class, "progress");
			thisConverter.setBaseValue(dailyDepthProgress);
			node.getDynaAttr().put("dailyDepthProgress", thisConverter.getConvertedValue());
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@dailyDepthProgress", thisConverter.getUOMMapping());
			}
			
			Double avgROP = 0.00;
			if ((dailyDrillingHours > 0 && dailyDepthProgress > 0)) {
				avgROP = dailyDepthProgress / dailyDrillingHours;
			}
			
			thisConverter.setReferenceMappingField(DrillingParameters.class, "ropAvg");
			thisConverter.setBaseValue(avgROP);
			node.getDynaAttr().put("avgRop", thisConverter.getConvertedValue());
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@avgRop", thisConverter.getUOMMapping());
			}
			
			//calculate to date POB hours - 20091202-0957-jwong
			node.getDynaAttr().put("totalManHoursToDate", this.getPOBHoursToDate(userSelection.getGroupUid(), operationUid, todayDate));

			//get cumulative DT/NPT hours to date - 20100111-0509-asim
			if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CALC_CUM_NPT_HOURS))) {
				Double cumNPTHrs = CommonUtil.getConfiguredInstance().getCumNPTHrsToDate(operationUid, currentDailyUid);
				thisConverter.setReferenceMappingField(DrillingParameters.class, "duration");
				thisConverter.setBaseValue(cumNPTHrs);
				node.getDynaAttr().put("cumNPTHrs", thisConverter.getConvertedValue());
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@cumNPTHrs", thisConverter.getUOMMapping());
				}
			}
			
		 	// To retrieve yesterdays report daily in order to calculate the progress from 0600 to 0600
		 	//Temporary calculate in afterdatanodeload, will move to before / after save
		 	Double geology24hrProgress = null;
		 	Double driller24hrProgress = null;
		 	String previousDailyUid =null;
		 	
		 	Daily previousDaily = ApplicationUtils.getConfiguredInstance().getYesterday(operationUid, currentDailyUid);
		 	if (previousDaily != null) {
		 		previousDailyUid = previousDaily.getDailyUid();
		 	}
		 	
		 	if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_GEO_24HR_PROGRESS))) {
	 			geology24hrProgress = CommonUtil.getConfiguredInstance().getGeology24HrsProgress(operationUid, previousDailyUid, reportDaily, commandBean);			 			
	 			reportDaily.setGeology24hrProgress(geology24hrProgress);

	 			String strSql = "UPDATE ReportDaily SET geology24hrProgress =:geology24hrProgress WHERE reportDailyUid = :thisReportDailyUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"geology24hrProgress", "thisReportDailyUid"}, new Object[] {geology24hrProgress, reportDaily.getReportDailyUid()});		 	
		 	}
	 				 	
	 		if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DAILY_24HR_PROGRESS))) {
	 			driller24hrProgress= CommonUtil.getConfiguredInstance().getDriller24HrsProgress(operationUid, previousDailyUid, reportDaily, commandBean);
	 			reportDaily.setProgress(driller24hrProgress);
	 			
	 			String strSql = "UPDATE ReportDaily SET progress =:driller24hrProgress WHERE reportDailyUid = :thisReportDailyUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"driller24hrProgress", "thisReportDailyUid"}, new Object[] {driller24hrProgress, reportDaily.getReportDailyUid()});
		 	}
	 		
	 		// Get plannedDuration from Operation						
			String strOperationSql = "SELECT plannedDuration FROM Operation WHERE (isDeleted=false or isDeleted is null) AND operationUid = :operationUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strOperationSql, "operationUid", operationUid, qp);			
			if (!lstResult.isEmpty()) {
				Object a = lstResult.get(0);
				Double totalDuration = null;
				if (a != null) totalDuration = Double.parseDouble(a.toString());
		 		
		 		if (totalDuration != null) {
					thisConverter.setReferenceMappingField(Operation.class, "plannedDuration");
					thisConverter.setBaseValue(totalDuration);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@plannedDVDDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("plannedDVDDuration", thisConverter.getConvertedValue());
				}
			}
	 		
	 		if (!"1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_FIT_LOT))) {
				if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_FIT_LOT_FROM_LOT))) {					
					Object objResult = CommonUtil.getConfiguredInstance().lastCasingSection(userSelection.getGroupUid(), wellboreUid, currentDailyUid);
					Double lastCasingSize = 0.00;
					if (objResult != null) {
						if (objResult instanceof CasingSection) {
							CasingSection thisCasingSection = (CasingSection) objResult;
							
							//due to casing size drop down must be in m and 6 decimal so no need to get raw value
							if (thisCasingSection.getCasingOd() != null) {
								thisConverter.setReferenceMappingField(ReportDaily.class, "last_csgsize");
								thisConverter.setBaseValue(thisCasingSection.getCasingOd());
								lastCasingSize = thisConverter.getBasevalue();
							}
						}
					}
					LeakOffTestUtil.populateLOTFIT(commandBean, object, userSelection, "LOT","lastLot",lastCasingSize);
					LeakOffTestUtil.populateLOTFIT(commandBean, object, userSelection, "FIT","lastFit",lastCasingSize);
				} else {
					if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_LATEST_LOT_OR_FIT))) {
						Object objResult = CommonUtil.getConfiguredInstance().lastCasingSection(userSelection.getGroupUid(), wellboreUid, currentDailyUid);
						Double lastCasingSize = 0.00;
						if (objResult != null) {
							if (objResult instanceof CasingSection) {
								CasingSection thisCasingSection = (CasingSection) objResult;
								
								//due to casing size drop down must be in m and 6 decimal so no need to get raw value
								if (thisCasingSection.getCasingOd() != null) {
									thisConverter.setReferenceMappingField(ReportDaily.class, "last_csgsize");
									thisConverter.setBaseValue(thisCasingSection.getCasingOd());
									lastCasingSize = thisConverter.getBasevalue();
								}
							}
						}
						LeakOffTestUtil.populateLOTFIT(commandBean, object, userSelection, "LOT","lastLot",lastCasingSize);
						LeakOffTestUtil.populateLOTFIT(commandBean, object, userSelection, "FIT","lastFit",lastCasingSize);
						LeakOffTestUtil.populateLotFitData(thisConverter, node, userSelection,lastCasingSize);
					}
				}
			}
	 		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_LATEST_LOT_OR_FIT))) {
	 			//this is to get the last latest mud weight value from leak off test screen
	 			LeakOffTestUtil.populateLatestMudWeight(commandBean, object, node, operationUid, currentDailyUid);
	 			//this is to get the test type value from leak off test screen for checking on whether to show the last LOT or last FIT field in daily screen 
	 			LeakOffTestUtil.populateTestTypeDynaAttr(node, operationUid, currentDailyUid);
	 		}
	 		
	 		Double cumufc = ReportDailyUtils.getCumulativeUnplannedFutureCost(todayDate, operationUid, CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid));
	 		if (cumufc != null) {
	 			thisConverter.setReferenceMappingField(ReportDaily.class, "unplannedFutureCost");
				thisConverter.setBaseValue(cumufc);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@cumUnplannedFutureCost", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumUnplannedFutureCost", thisConverter.getConvertedValue());
			}
	 		ReportDailyUtils.autoPopulateBitDiameter(reportDaily, this.defaultReportType, thisConverter);

			// Get the mud weight from Mud Properties screen
	 		QueryProperties qp1 = new QueryProperties();		
			qp1.setUomConversionEnabled(false);
			CustomFieldUom thisConverter1 = new CustomFieldUom(commandBean, MudProperties.class, "mudWeight");
			
			String strSql1 = "select mp.mudWeight FROM MudProperties mp, Daily d WHERE (mp.isDeleted = false or mp.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and mp.operationUid=:currentOperationUid and mp.dailyUid=:currentDailyUid and d.dailyUid=mp.dailyUid order by mp.reportTime desc";
			String[] paramsFields1 = {"currentOperationUid","currentDailyUid"};
			Object[] paramsValues1 = new Object[2]; paramsValues1[0] = operationUid; paramsValues1[1] = currentDailyUid;
			List mpMudWeight = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1, qp1);

			if (!mpMudWeight.isEmpty()) {
				Object a = mpMudWeight.get(0);
				Double mudWeight = null;
				if (a != null) mudWeight = Double.parseDouble(a.toString());
		 		
		 		if (mudWeight != null) {
					thisConverter1.setReferenceMappingField(MudProperties.class, "mudWeight");
					thisConverter1.setBaseValue(mudWeight);
					if (thisConverter1.isUOMMappingAvailable()) {
						node.setCustomUOM("@mudWeight", thisConverter1.getUOMMapping());
					}
					node.getDynaAttr().put("mudWeight", thisConverter1.getConvertedValue());
				}
			}
			
			//31560
			if ("DDR".equalsIgnoreCase(defaultReportType)) {
				node.getDynaAttr().put("ddrExist", "1");
			} else {
				String strSql2 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND dailyUid = :thisDailyUid AND report_type = 'DDR'";
				String[] paramsFields2 = {"thisDailyUid"};
				String[] paramsValues2 = {reportDaily.getDailyUid()};
				List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				if (reportDailyList != null && reportDailyList.size() > 0) {
					node.getDynaAttr().put("ddrExist", "1");
				} else {
					node.getDynaAttr().put("ddrExist", "0");
				}
			}
			
			//Ticket: 44712
			Map lastSurveyData = new HashMap<String, Object>();
			if (showSurveyByDayDepth) {
				lastSurveyData = CommonUtil.getConfiguredInstance().calculateLastSurveyDepthAndAngleByDayDepth(reportDaily.getOperationUid(), reportDaily.getDailyUid(), userSelection.getDailyUid(), userSelection.getLocale());
			} else {
				lastSurveyData = CommonUtil.getConfiguredInstance().calculateLastSurveyDepthAndAngle(reportDaily.getOperationUid(), reportDaily.getDailyUid(), commandBean);
			}
			//Collect last survey data
			node.getDynaAttr().put("SurveyStation.depthMdMsl", lastSurveyData.get("depthMdMsl"));
			node.getDynaAttr().put("SurveyStation.depthTvdMsl", lastSurveyData.get("depthTvdMsl"));
			node.getDynaAttr().put("SurveyStation.inclinationAngle", lastSurveyData.get("inclinationAngle"));
			node.getDynaAttr().put("SurveyStation.azimuthAngle", lastSurveyData.get("azimuthAngle"));
			node.setCustomUOM("@SurveyStation.depthMdMsl", (UOMMapping) lastSurveyData.get("uomDepthMdMsl"));
			node.setCustomUOM("@SurveyStation.depthTvdMsl", (UOMMapping) lastSurveyData.get("uomDepthTvdMsl"));
			node.setCustomUOM("@SurveyStation.inclinationAngle", (UOMMapping) lastSurveyData.get("uomInclinationAngle"));
			node.setCustomUOM("@SurveyStation.azimuthAngle", (UOMMapping) lastSurveyData.get("uomAzimuthAngle"));
			
			// Dynamic Attribute Processor - Load (Meant to connect multiple tables into '1 table' on screen seamlessly)
			DynamicAttributeProcessor.loadDynamicAttributes(commandBean, node, object, this.dynamicAttributeMap);
			
			ReportDailyUtils.getProgressFromDdrDaily(commandBean, node, userSelection);
			
			// Rig Information standard fields jwong (Ported by jhwong 20200320)
			if (StringUtils.isNotBlank(reportDaily.getRigInformationUid())) {
				RigInformation rigInformation = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, reportDaily.getRigInformationUid());
				if (rigInformation != null) {
					Map<String, Object> RigInformationFieldValues = PropertyUtils.describe(rigInformation);
					for (Map.Entry<String, Object> entry : RigInformationFieldValues.entrySet()) {
						node.getDynaAttr().put("rigInformation." + entry.getKey(), entry.getValue());
					}
				}
			}

			Well thisWell = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, reportDaily.getWellUid());
			node.getDynaAttr().put("well.safetyPermitRef", thisWell.getSafetyPermitRef());
			
			Double totalcummudcost = 0.00;
			thisConverter.setReferenceMappingField(MudProperties.class, "mudCost");
			totalcummudcost = MudPropertiesUtil.calcCumMudCost(false, null, daily.getDayDate(), operationUid);
			thisConverter.setBaseValue(totalcummudcost);
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@totalcummudcost", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("totalcummudcost", thisConverter.getConvertedValue());
			
			
			if (object instanceof ReportDaily) {			
				if (forwardShiftReportType!=null && StringUtils.isNotBlank(forwardShiftReportType)){
					
					if (ReportDailyUtils.isOmvCountrySpecific(daily.getWellUid(), daily.getOperationUid(), forwardShiftReportType, shiftCountry)){
						Map<String, CommandBeanTreeNode> shiftNodes = node.getChild(DailyShiftInfo.class.getSimpleName());
						
						if(shiftNodes == null || shiftNodes.isEmpty()) {
							commandBean.getSystemMessage().addError("Shift record is required to save Daily record.");					
						}
					}
				}
			
				//if (daily.getDayDate()!=null){
				//	DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
				//	node.getDynaAttr().put("maxDateToUse", df.format(daily.getDayDate()));
				//}
			}
		}
	}

	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		MenuManager.getConfiguredInstance().refreshCachedData();
		String thisReportType = "";
		ReportDaily thisReportdaily = null;
		
		Object newObject = node.getData();
		if (newObject instanceof ReportDaily) {
			thisReportdaily = (ReportDaily) newObject;
			thisReportType = thisReportdaily.getReportType();
		}

		if (commandBean.getFlexClientControl() != null) {
			commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		}
			
		Object object = node.getData();
		if (object instanceof ReportDaily) {
			ReportDaily thisReport = (ReportDaily) object;
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_CARRY_DAY_COST_FROM_PREV_OP))) {
				ReportDailyUtils.updateCumCostFromPrevOperation(thisReport.getReportDailyUid(),commandBean,session);
			}
			
			if (this.isChangeDate) {
				CommonUtil.getConfiguredInstance().updateActivityDate(thisReport);
				//update start date and end date on slickline screen - 20110303-0128-scwong
				SlicklineUtil.updateSlicklineDate(thisReport);
				
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);
				String strSql3 = "select distinct(b.bharunUid) from Bharun b, BharunDailySummary bds, Daily d, DownholeTools dt where (dt.isDeleted = false or dt.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
				"and b.operationUid=:operationUid and bds.dailyUid=:dailyUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid and bds.bharunDailySummaryUid=dt.bharunDailySummaryUid ";
				
				List <Object> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3,
						new String[] {"operationUid", "dailyUid"},
						new Object[] {session.getCurrentOperationUid(), thisReport.getDailyUid()}, qp);
				
				if (lstResult2 != null && lstResult2.size() > 0) {
					Daily currDaily=ApplicationUtils.getConfiguredInstance().getCachedDaily(session.getCurrentDailyUid());
					String toolSerialNumber="";
					String tool="";
					String bharunUid="";
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
					if (currDaily.getDayDate() != null) {
						String strSql = "From Daily where (isDeleted = false or isDeleted is null) " +
						"and operationUid=:operationUid order by dayDate";
						
						List <Daily> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
								new String[] {"operationUid"},
								new Object[] {session.getCurrentOperationUid()}, qp);
						
						if (lstResult4 != null && lstResult4.size() > 0) {
							for (Object record:lstResult2) {
								if (record != null) {
									bharunUid = record.toString();
								}
								String strSql8 = "Select dt.toolSerialNumber, dt.tool FROM BharunDailySummary bds,DownholeTools dt, Daily d where bds.bharunDailySummaryUid=dt.bharunDailySummaryUid and bds.dailyUid=:dailyUid and bds.bharunUid=:bharunUid and (dt.isDeleted=false or dt.isDeleted is null) and (bds.isDeleted=false or bds.isDeleted is null) and (d.isDeleted=false or d.isDeleted is null) and bds.dailyUid=d.dailyUid and d.operationUid=:operationUid order by d.dayDate";
								List <Object[]> lstResult8 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql8, new String[] {"operationUid","dailyUid","bharunUid"}, new Object[] {session.getCurrentOperationUid(),session.getCurrentDailyUid(),bharunUid});
								if (lstResult8 != null && lstResult8.size() > 0) {
									for (Object[] result : lstResult8) {
										if (result[0] != null) {
											toolSerialNumber = result[0].toString();
										}
										if (result[1] != null) {
											tool = result[1].toString();
										}
										for (Daily dailyrecord : lstResult4) {
											BharunUtils.recalculateCumulativeCircHour(dailyrecord.getDayDate(), toolSerialNumber, tool, session.getCurrentOperationUid(), thisConverter, bharunUid);
										}
									}
								}
							}
						}
					}
				}
			}
			
			if (this.updateExistingDaily) {
				if (StringUtils.isNotBlank(previousDailyUid)) {
					CommonUtil.getConfiguredInstance().deleteExistingDvdSummary("daily", previousDailyUid);
					CommonUtil.getConfiguredInstance().createDvdSummary(session.getCurrentOperationUid(), session.getCurrentDailyUid());
					this.updateExistingDaily = false;
				}
			} else if (this.createdNewDay) {
				CommonUtil.getConfiguredInstance().createDvdSummary(session.getCurrentOperationUid(), session.getCurrentDailyUid());
			}
			
			// EFC Vessel Cost - 20100321-1223-ekhoo 
		 	CommonUtil.getConfiguredInstance().UpdateReportDailyEFC(thisReport.getOperationUid(),thisReport.getDailyUid());
		 	CommonUtil.getConfiguredInstance().calculateCostIncurred(thisReport.getReportDatetime(), thisReport.getDaycost(),"lwd_tool");
		 	CommonUtil.getConfiguredInstance().calculateCostIncurred(thisReport.getReportDatetime(), thisReport.getDaycost(),"wireline_tool");
		 	
			//refresh start/end date in OFS
			List<String> changedRowPrimaryKeys = new ArrayList<>();
			changedRowPrimaryKeys.add(thisReport.getOperationUid());
			D2ApplicationEvent.publishTableChangedEvent(Operation.class, changedRowPrimaryKeys);
			
			if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_DEPOT) {
				session.setCurrentDailyUid(thisReport.getDailyUid());
			}
			
			if (!thisReportType.equals("DGR")) {
				if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
					Daily selectedDay = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisReport.getDailyUid());
					if (selectedDay == null) {
						selectedDay = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, thisReport.getDailyUid());
					}
					CommonUtil.getConfiguredInstance().carryOverNextDayActivity(selectedDay, session);
				}
		 	}
			
			String thisOperationUid = "";
			if (thisReport.getOperationUid() != null) {
				thisOperationUid = thisReport.getOperationUid();
			}
			if (showSurveyByDayDepth) {
				CommonUtil.getConfiguredInstance().setLastSurveyDepthByDayDepth(thisOperationUid, thisReport.getDailyUid(), session);
			} else {
				CommonUtil.getConfiguredInstance().setLastSurveyDepth(thisOperationUid, thisReport.getDailyUid(), commandBean);
			}
			
			//auto update flow back data on well production test table when there is any changes on report daily screen (ticket: 20110524-1800-ysim)
			if (this.autoUpdateFlowbackData) {
				WellProductionTestUtil.updateFlowBackData(thisReport, session);
			}
			
			//the gwp is for control to either use the new method of calculation for days since last lti or the default calculation
			if ("0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_MANUAL_INCREMENT_DAYS_SINCE_LTI_EVENT))) {
				if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_DAYS_SINCE_LAST_LTI_BASED_ON_CONTRACT))) {
					RigUtilizationUtils.updateAllReportDailyDaysSinceLastLti(session.getCurrentGroupUid(), session.getCurrentRigInformationUid(), thisReport.getDailyUid());
					// calculate Days Since Last LTI  with using rig contract
					/*CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "daysSinceLta");
					Double daysSinceLastLti = RigUtilizationUtils.calculateDaysSinceLastLTI(session.getCurrentGroupUid(), session.getCurrentRigInformationUid(), thisReport.getDailyUid());
					if (daysSinceLastLti!=null && thisConverter.isUOMMappingAvailable()) { 
						thisConverter.setBaseValue(daysSinceLastLti);
						daysSinceLastLti = thisConverter.getConvertedValue();
					}
					thisReport.setDaysSinceLta(daysSinceLastLti);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);*/
				}
			}
			
			//change mud type when edit/update DGR if mud properties data changed.
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_MUD_TYPE))) {
				String reportType = "DGR";
				if (thisReport.getDailyUid() != null) {
					String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid AND reportType = :thisReportType";
					String[] paramsFields = {"selectedOperationUid", "thisReportType"};
					String[] paramsValues = {thisReport.getOperationUid(), reportType};
					List<ReportDaily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					if (lstResult.size() > 0) {
						for (ReportDaily selectedDDRReportDaily : lstResult) {
							String strSql1 = "FROM MudProperties WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid and dailyUid = :selectedDailyUid order by reportNumber desc";
							String[] paramsFields1 = {"selectedOperationUid", "selectedDailyUid"};
							String[] paramsValues1 = {selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid()};
							List<MudProperties> lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
							if (lstResult1.size() > 0) {
								MudProperties mudData = lstResult1.get(0);
								String strSql2 = "UPDATE ReportDaily SET mudType =:mudType WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
								String[] paramsFields2 = {"mudType", "operationUid", "dailyUid", "reportType"};
								Object[] paramsValues2 = {mudData.getMudType(), mudData.getOperationUid(), mudData.getDailyUid(), reportType};
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields2, paramsValues2);
							}
						}
					}
				}
			}
			
			//change formation @ midnight value when edit/update midnight@depth(MD) of DDR. (DDR.midnight@depth >= Formation.actual_top_depth)
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_FORMATION_NAME))) {
				String reportType = "DGR";
				Operation thisOperation = UserSession.getInstance(request).getCurrentOperation();		
				if (thisOperation != null) {
					if (thisReport.getDailyUid() != null) {
						String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid AND reportType = :thisReportType";
						String[] paramsFields = {"selectedOperationUid", "thisReportType"};
						String[] paramsValues = {thisReport.getOperationUid(), reportType};
						List<ReportDaily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
						//check if geology day exists and update if it exists
						if (lstResult.size() > 0) {
							for (ReportDaily selectedDDRReportDaily : lstResult) {
								Double depthMdMsl = null;
								List<ReportDaily> lstResult1 = null;
								if (this.isWellboreBased()) { // wellbore-based
									String strSql1 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid AND wellboreUid =:selectedWellboreUid AND dailyUid =:dailyUid AND reportType = 'DDR'";
									String[] paramsFields1 = {"selectedOperationUid","selectedWellboreUid" , "dailyUid"};
									String[] paramsValues1 = {selectedDDRReportDaily.getOperationUid(),session.getCurrentWellboreUid() , selectedDDRReportDaily.getDailyUid()};
									lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
								} else { // well-based
									String strSql1 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid AND dailyUid =:dailyUid AND reportType = 'DDR'";
									String[] paramsFields1 = {"selectedOperationUid",  "dailyUid"};
									String[] paramsValues1 = {selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid()};
									lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
							    }
								if (lstResult1.size() > 0) {
									ReportDaily Depth = lstResult1.get(0);
									if (Depth.getDepthMdMsl() != null) {
										depthMdMsl = Depth.getDepthMdMsl();
										List<Formation> lstResult2 = null;
										if (this.wellboreBased) { // wellbore-based
											String strSql2 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) and  wellboreUid = :selectedWellboreUid and sampleTopMdMsl <=:depthMdMsl order by sampleTopMdMsl desc";
											String[] paramsFields2 = {"selectedWellboreUid",  "depthMdMsl"};
											Object[] paramsValues2 = {session.getCurrentWellboreUid(), depthMdMsl};
											lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
									    } else { // well-based
											String strSql2 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid and sampleTopMdMsl <=:depthMdMsl order by sampleTopMdMsl desc";
											String[] paramsFields2 = {"selectedOperationUid",  "depthMdMsl"};
											Object[] paramsValues2 = {selectedDDRReportDaily.getOperationUid(), depthMdMsl};
											lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
									    }
										if (depthMdMsl != null && depthMdMsl != 0.0) {
											if (lstResult2.size() > 0) {
												Formation formationdata = lstResult2.get(0);
												depthMdMsl = Depth.getDepthMdMsl();
												if (formationdata.getSampleTopMdMsl() <= depthMdMsl) {
													if (this.wellboreBased) {
														String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null)  AND wellboreUid =:wellboreUid AND dailyUid =:dailyUid AND reportType =:reportType";
														String[] paramsFields3 = {"formationName", "wellboreUid", "dailyUid", "reportType"};
														String[] paramsValues3 = {formationdata.getFormationName(), formationdata.getWellboreUid(), selectedDDRReportDaily.getDailyUid(), reportType};
														ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
												    } else {
														String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
														String[] paramsFields3 = {"formationName", "operationUid", "dailyUid", "reportType"};
														String[] paramsValues3 = {formationdata.getFormationName(), formationdata.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
														ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
												    }
												}
											} else {
												String formationName = null;
												if (this.wellboreBased) {
													String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND wellboreUid =:wellboreUid AND dailyUid =:dailyUid AND reportType =:reportType";
													String[] paramsFields3 = {"formationName","wellboreUid", "dailyUid", "reportType"};
													String[] paramsValues3 = {formationName, selectedDDRReportDaily.getWellboreUid(), selectedDDRReportDaily.getDailyUid(), reportType};
													ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);	
											    } else {
													String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
													String[] paramsFields3 = {"formationName", "operationUid", "dailyUid", "reportType"};
													String[] paramsValues3 = {formationName, selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
													ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
											    }
											}
										}
							        } else {
										if (Depth.getDepthMdMsl() == null) {
											String formationName = null;
											if (this.wellboreBased) {
												String strSql4 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null)  AND wellboreUid =:wellboreUid AND dailyUid =:dailyUid AND reportType =:reportType";
												String[] paramsFields4 = {"formationName", "wellboreUid", "dailyUid", "reportType"};
												String[] paramsValues4 = {formationName,selectedDDRReportDaily.getWellboreUid(), selectedDDRReportDaily.getDailyUid(), reportType};
												ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql4, paramsFields4, paramsValues4);
										    } else {
												String strSql4 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
												String[] paramsFields4 = {"formationName", "operationUid", "dailyUid", "reportType"};
												String[] paramsValues4 = {formationName, selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
												ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql4, paramsFields4, paramsValues4);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			//31560 update rig information uid in DGR from DDR
			if ("DDR".equals(defaultReportType)) {
				ReportDailyUtils.updateRigInformationUid(thisReport, "DGR");
			} else if ("DGR".equals(defaultReportType)) {
				ReportDailyUtils.updateRigInformationUid(thisReport, "DDR");
			}
		
			Operation o = ApplicationUtils.getConfiguredInstance().getCachedOperation(session.getCurrentOperationUid());
			if (o != null && o.getSpudDate() != null && o.getTdDateTdTime() != null) {
				ReportDailyUtils.setReportDailySpudToTdDuration(thisReport, o.getSpudDate(), o.getTdDateTdTime());
			}
			// Dynamic Attribute Processor - Saving
			DynamicAttributeProcessor.saveDynamicAttributes(commandBean, node, object, this.dynamicAttributeMap);
			
			ExternalActivityDailyEvent event = new ExternalActivityDailyEvent(thisReportdaily);
			this.applicationEventPublisher.publishEvent(event);
			
			// 20616 OMV HIP
			OMVGDBEvent GDBEvent = new OMVGDBEvent(thisReportdaily);
			this.applicationEventPublisher.publishEvent(GDBEvent);
	    }
	}
	
	public void beforeDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception{
		Object obj = node.getData();
		if (obj instanceof ReportDaily) {
			ReportDaily today = (ReportDaily) obj;
			Daily yesterday = ApplicationUtils.getConfiguredInstance().getYesterday(today.getOperationUid(), today.getDailyUid());
			
			if (yesterday != null) {
				Date todayDate = today.getReportDatetime();
				Calendar thisCalendar = Calendar.getInstance();
				thisCalendar.setTime(todayDate);
				thisCalendar.add(Calendar.DATE, -1);
				thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
				thisCalendar.set(Calendar.MINUTE, 0);
				thisCalendar.set(Calendar.SECOND , 0);
				thisCalendar.set(Calendar.MILLISECOND , 0);
				
				Date previousDate = thisCalendar.getTime();
				
				thisCalendar.setTime(yesterday.getDayDate());
				thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
				thisCalendar.set(Calendar.MINUTE, 0);
				thisCalendar.set(Calendar.SECOND , 0);
				thisCalendar.set(Calendar.MILLISECOND , 0);
				
				Date yesterdayDate = thisCalendar.getTime();
				
				String yesterdayDailyUid = yesterday.getDailyUid();
				
				if (yesterdayDate != null && yesterdayDate.compareTo(previousDate) == 0) {
					node.getDynaAttr().put("previousDailyUid", yesterdayDailyUid);
				}
			}
			
			EnvironmentDischargesUtil.calculateTotalForWellAfterDayChanged(commandBean, node, session);
			
			// Delete record of the day in EdmSync
			EdmUtils.deleteRecordFromEdmSync("ReportDaily", today.getReportDailyUid(), "DM_DAILY");
			// Delete record of the day in EdmImport
			EdmUtils.deleteRecordFromEdmImport(today.getOperationUid(), today.getDailyUid());
		}
	
		// Delete DVD Summary records of the day
		CommonUtil.getConfiguredInstance().deleteExistingDvdSummary("daily", session.getCurrentDailyUid());
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		if (object instanceof ReportDaily) {
		
			//check if got any other report daily object tie with this dailyUid or not, if no, then delete the daily object
			ReportDaily thisReport = (ReportDaily) object;
			
			String thisPreviousDailyUid = (String) node.getDynaAttr().get("previousDailyUid");
			String thisOperationUid  = thisReport.getOperationUid();
			
			String[] paramsFields2 = {"thisPreviousDailyUid" , "thisOperationUid"};
			String[] paramsValues2 = {thisPreviousDailyUid , thisOperationUid};
			String strSql2 = "SELECT activityUid FROM Activity WHERE (isDeleted = false or isDeleted is null) and dailyUid = :thisPreviousDailyUid and operationUid = :thisOperationUid and dayPlus = '1'";
			List<String> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
			if (lstResult3.size() > 0) {
				for (String thisActivityUid : lstResult3){
					String[] paramsFields4 = {"thisLinkedActivityUid"};
					String[] paramsValues4 = {thisActivityUid};
					String strSql4 = "SELECT lessonTicketUid FROM ActivityLessonTicketLink WHERE isDeleted = '1' and activityUid = :thisLinkedActivityUid";
					List<String> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4);
					
					if (!lstResult5.isEmpty()) {
						for (String thisLessonTicketUid : lstResult5) {
							LessonTicket ll = (LessonTicket) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LessonTicket.class, thisLessonTicketUid);
							if (ll != null) {
								ll.setIsDeleted(true);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ll);
							}
						}
						
					}
				}
			}
			
			String thisDailyUid  = thisReport.getDailyUid();
			String[] paramsFields7 = {"thisDailyUid" , "thisOperationUid"};
			String[] paramsValues7 = {thisDailyUid , thisOperationUid};
			String strSql7 = "FROM LessonTicket WHERE (isDeleted = false or isDeleted is null) and dailyUid = :thisDailyUid and operationUid = :thisOperationUid";
			List<LessonTicket> lstResult9 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7, paramsFields7, paramsValues7);
			if (!lstResult9.isEmpty()) {
				for (LessonTicket lt : lstResult9){
					lt.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lt);
				}
			
			}
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_CARRY_DAY_COST_FROM_PREV_OP))){
				ReportDailyUtils.updateCumCostFromPrevOperation(thisReport.getReportDailyUid(),commandBean,session);
			}
			
			Daily currDaily=ApplicationUtils.getConfiguredInstance().getCachedDaily(session.getCurrentDailyUid());
			String[] paramsFields = {"thisDailyUid", "thisReportDailyUid"};
			String[] paramsValues = {thisReport.getDailyUid(), thisReport.getReportDailyUid()};
			String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :thisDailyUid AND reportDailyUid <> :thisReportDailyUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.isEmpty()) {
				String thisYesterdayDailyUid = (String) node.getDynaAttr().get("previousDailyUid");
				String thisTodayDailyUid = thisReport.getDailyUid();

				Object obj = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, thisTodayDailyUid);
				if (obj!=null) {
					((Daily)obj).setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(obj);
				}
				//ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update Daily set isDeleted = true where dailyUid = :dailyUid", new String[] {"dailyUid"}, new Object[] {thisTodayDailyUid});

				ApplicationUtils.getConfiguredInstance().refreshCachedData();
				String operationUid = thisReport.getOperationUid();
				CommonUtil.getConfiguredInstance().setStartAndLastOperationDate(operationUid);
				
				//check NextDayActivity to revert data when a daily is deleted
				ReportDailyUtils.revertNextDayActivityWhenDeleteDaily(thisYesterdayDailyUid, thisTodayDailyUid, session);
				
				//clean BHA daily data after daily record is deleted
				ReportDailyUtils.cleanBHADailyWhenDeleteDaily(thisTodayDailyUid);
			}
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_DAYS_SINCE_LAST_LTI_BASED_ON_CONTRACT))) {
				//update all days since last LTI since today onward
				RigUtilizationUtils.updateAllReportDailyDaysSinceLastLti(session.getCurrentGroupUid(), session.getCurrentRigInformationUid(), thisReport.getReportDatetime(), null);
			}
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			String strSql3 = "select distinct(b.bharunUid) from Bharun b, BharunDailySummary bds, Daily d, DownholeTools dt where " +
			"b.operationUid=:operationUid and bds.dailyUid=:dailyUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid and bds.bharunDailySummaryUid=dt.bharunDailySummaryUid ";
			
			List <Object> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3,
					new String[] {"operationUid", "dailyUid"},
					new Object[] {session.getCurrentOperationUid(), thisReport.getDailyUid()}, qp);
			
			if (lstResult2 != null && lstResult2.size() > 0) {
				String toolSerialNumber="";
				String tool="";
				String bharunUid="";
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
				if (currDaily != null){
					if (currDaily.getDayDate() != null) {
						String strSql1 = "From Daily where (isDeleted = false or isDeleted is null) " +
						"and operationUid=:operationUid order by dayDate";
						
						List <Daily> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1,
								new String[] {"operationUid"},
								new Object[] {session.getCurrentOperationUid()}, qp);
						
						if (lstResult4 != null && lstResult4.size() > 0) {
							for (Object record : lstResult2) {
								if (record != null) {
									bharunUid = record.toString();
								}
								String strSql8 = "Select dt.toolSerialNumber, dt.tool FROM BharunDailySummary bds,DownholeTools dt, Daily d where bds.bharunDailySummaryUid=dt.bharunDailySummaryUid and bds.dailyUid=:dailyUid and bds.bharunUid=:bharunUid and bds.dailyUid=d.dailyUid and d.operationUid=:operationUid order by d.dayDate";
								List <Object[]> lstResult8 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql8, new String[] {"operationUid","dailyUid","bharunUid"}, new Object[] {session.getCurrentOperationUid(),session.getCurrentDailyUid(),bharunUid});
								if (lstResult8 != null && lstResult8.size() > 0) {
									for (Object[] result : lstResult8) {
										if (result[0] != null) {
											toolSerialNumber = result[0].toString();
										}
										if (result[1] != null) {
											tool = result[1].toString();
										}
										for (Daily dailyrecord : lstResult4) {
											BharunUtils.recalculateCumulativeCircHour(dailyrecord.getDayDate(), toolSerialNumber, tool, session.getCurrentOperationUid(), thisConverter, bharunUid);
										}
									}
								}
							}
						}
					}
				}
			}
			//refresh start/end date in OFS
			List<String> changedRowPrimaryKeys = new ArrayList<>();
			changedRowPrimaryKeys.add(thisReport.getOperationUid());
			D2ApplicationEvent.publishTableChangedEvent(Operation.class, changedRowPrimaryKeys);
			MenuManager.getConfiguredInstance().refreshCachedData();
			session.setCurrentDailyUid(null);
			if (commandBean.getFlexClientControl() != null) {
				commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			}
			//18761 update all bha total weights
			//update mud properties to deleted
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_WEIGHT_DRY_WET_BELOW_JAR_CALCULATION))) {
				CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(),MudProperties.class, "mudWeight");
				if (session != null && session.getCurrentDailyUid() != null) {
					MudPropertiesUtil.updateBHATotalWeights(thisReport.getOperationUid(), session.getCurrentDailyUid(), thisConverter);	
				} else {
					MudPropertiesUtil.updateBHATotalWeights(thisReport.getOperationUid(), null, thisConverter);
				}	
			}
		}
		
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof ReportDaily) {
			ReportDaily thisReport = (ReportDaily) object;
//			ReportDaily thisReport = event.getReportDaily();
			
			//21790
			if(thisReport.getPlannedDaysCompleted() != null )
			{
				CustomFieldUom thisConverter1 = new CustomFieldUom(commandBean, ReportDaily.class, "PlannedDaysCompleted");
				thisReport.setPlannedDaysCompleted(Double.parseDouble(thisConverter1.formatOutputPrecision((thisReport.getPlannedDaysCompleted()))));
			}
			
//			if (forwardShiftReportType!=null && StringUtils.isNotBlank(forwardShiftReportType)){
//
//				if (ReportDailyUtils.isOmvCountrySpecific(thisReport.getWellUid(), thisReport.getOperationUid(), forwardShiftReportType, shiftCountry)){
//					 
//					String dailyUid = CommonUtil.getConfiguredInstance().getLastDailyUidInOperationByReportType(thisReport.getOperationUid(), forwardShiftReportType);
//					
//					String strSql = "FROM DailyShiftInfo WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid ORDER BY startDatetime";
//					String[] paramsField = {"dailyUid"};
//					Object[] paramsValue = {dailyUid};
//					List<DailyShiftInfo> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
//					HashMap<Integer, DailyShiftInfo> dailyShiftInfosByUid = new HashMap<Integer, DailyShiftInfo>();
//					
//					if(result.size()>0){
//						// first put all the previous day DailyShiftInfos in a hashmap by uid
//						for(DailyShiftInfo shift : result) {
//							dailyShiftInfosByUid.put(shift.getSequence(), shift);
//						}
//						// then create a DailyShiftInfo population list. Use to populate the DailyShiftInfo form for saving later.
//						List<DailyShiftInfo> populateDSI = new ArrayList<DailyShiftInfo>();
//						
//						// get the edited nodes
//						Map<String, CommandBeanTreeNode> shiftNodes = node.getChild(DailyShiftInfo.class.getSimpleName());
//						
//						// either append a new DailyShiftInfo to the population list 
//						// or override the DailyShiftInfo in the hashmap if they were edited directly
//						for (Entry<String, CommandBeanTreeNode> entry : shiftNodes.entrySet()) {
//							CommandBeanTreeNode nodes = entry.getValue();
//							DailyShiftInfo newDailyShiftInfo = (DailyShiftInfo) nodes.getData();
//							// if an existing one (previous DailyShiftInfo) was edit, override
//							if(dailyShiftInfosByUid.containsKey(newDailyShiftInfo.getSequence())) {
//								dailyShiftInfosByUid.put(newDailyShiftInfo.getSequence(), newDailyShiftInfo);
//							} else {
//								// else just add directly to the population list if a new one
//								populateDSI.add(newDailyShiftInfo);
//							}
//						}
//						// add all the previous day DailyShiftInfos (untouched or dirty) in the population list
//						populateDSI.addAll(dailyShiftInfosByUid.values());
//						
//						
//						
//						// populate the DailyShiftInfo form with the previous day DailyShiftInfo + plus any new/edited DailyShiftInfo
//						for (DailyShiftInfo shift : populateDSI) {
//							DailyShiftInfo newDailyShiftInfo = new DailyShiftInfo();
//							newDailyShiftInfo.setSequence(shift.getSequence());
//							newDailyShiftInfo.setShiftLabel(shift.getShiftLabel());
//							newDailyShiftInfo.setStartDatetime(shift.getStartDatetime());
//							newDailyShiftInfo.setEndDatetime(shift.getEndDatetime());
//							newDailyShiftInfo.setPaxNoContractor(shift.getPaxNoContractor());
//							newDailyShiftInfo.setPaxNoOperator(shift.getPaxNoOperator());
//							newDailyShiftInfo.setPaxNoServices(shift.getPaxNoServices());
//							node.addCustomNewChildNodeForInput(newDailyShiftInfo);
//						}
//						
////							int count = 1;
//							
////							for (DailyShiftInfo shift : result){
////								if (count==1){
//							
////								}else{
////									DailyShiftInfo newDailyShiftInfo = new DailyShiftInfo();
////									newDailyShiftInfo.setSequence(shift.getSequence());
////									newDailyShiftInfo.setShiftLabel(shift.getShiftLabel());
////									newDailyShiftInfo.setStartDatetime(shift.getStartDatetime());
////									newDailyShiftInfo.setEndDatetime(shift.getEndDatetime());
////									newDailyShiftInfo.setPaxNoContractor(shift.getPaxNoContractor());
////									newDailyShiftInfo.setPaxNoOperator(shift.getPaxNoOperator());
////									newDailyShiftInfo.setPaxNoServices(shift.getPaxNoServices());
////									node.addCustomNewChildNodeForInput(newDailyShiftInfo);
////								}
////								count++;
////							}
//					}
//					
//					Map<String, CommandBeanTreeNode> shiftNodes = node.getChild(DailyShiftInfo.class.getSimpleName());
//					
//					if(shiftNodes == null || shiftNodes.isEmpty()) {
//						commandBean.getSystemMessage().addError("Shift record is required to save Daily record.");
//						status.setContinueProcess(false, true);
//						return;
//					}
//				}
//			}
			// EDM Sync - Publish EDM Sync Event 
			EDMSyncEvent event = new EDMSyncEvent(thisReport);
			this.applicationEventPublisher.publishEvent(event);
			
			thisReport.setSysReportDailySubclass(ModuleConstants.MODULE_KEY_WELL);
			
			if (node.getDynaAttr().get("operationSummary") != null) {
				String operationSummary = node.getDynaAttr().get("operationSummary").toString();
				String strSql = "FROM GeneralComment WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid AND dailyUid = :thisdailyUid AND category = :thisCategory";
				String[] paramsFields = {"thisdailyUid","thisOperationUid","thisCategory"};
				Object[] paramsValues = {thisReport.getDailyUid(),thisReport.getOperationUid(),"spOpsSummary"};
				List<GeneralComment> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				if (!lstResult2.isEmpty()) {
					for(GeneralComment res:lstResult2) {
						res.setComments(operationSummary);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(res);
					}
				} else {
					GeneralComment generalComment = new GeneralComment();
					generalComment.setCategory("spOpsSummary");
					generalComment.setComments(operationSummary);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(generalComment);
				}
			}
			
			if (node.getDynaAttr().get("last24hrsDrillScene") != null) {
				String operationSummary = node.getDynaAttr().get("last24hrsDrillScene").toString();
				String strSql = "FROM GeneralComment WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid AND dailyUid = :thisdailyUid AND category = :thisCategory";
				String[] paramsFields = {"thisdailyUid","thisOperationUid","thisCategory"};
				Object[] paramsValues = {thisReport.getDailyUid(),thisReport.getOperationUid(),"sp24hrsSummary"};
				List<GeneralComment> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				if (!lstResult2.isEmpty()) {
					for(GeneralComment res:lstResult2) {
						res.setComments(operationSummary);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(res);
					}
				} else {
					GeneralComment generalComment = new GeneralComment();
					generalComment.setCategory("sp24hrsSummary");
					generalComment.setComments(operationSummary);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(generalComment);
				}
			}
			
			if (node.getDynaAttr().get("last24hrsCommunications") != null) {
				String operationSummary = node.getDynaAttr().get("last24hrsCommunications").toString();
				String strSql = "FROM GeneralComment WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid AND dailyUid = :thisdailyUid AND category = :thisCategory";
				String[] paramsFields = {"thisdailyUid","thisOperationUid","thisCategory"};
				Object[] paramsValues = {thisReport.getDailyUid(),thisReport.getOperationUid(),"sp24hrsComm"};
				List<GeneralComment> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				if (!lstResult2.isEmpty()) {
					for(GeneralComment res:lstResult2) {
						res.setComments(operationSummary);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(res);
					}
				} else {
					GeneralComment generalComment = new GeneralComment();
					generalComment.setCategory("sp24hrsComm");
					generalComment.setComments(operationSummary);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(generalComment);
				}
			}
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_CARRY_DAY_COST_FROM_PREV_OP)))	{
				if (thisReport != null) {
					thisReport.isPropertyModified("daycost");
				 	 
			 		Double dayCostFromPrevOpLastDay = null;
			 		Double dayTangibleCostFromPrevOpLastDay = null;
			 		Double costAdjustFromPrevOpLastDay = null;
			 		Double completionCostFromPrevOpLastDay = null;
			 		Double testCostPrevOpLastDay = null;
			 		Double otherCostPrevOpLastDay = null;
			 		String commentCostFromPrevOpLastDay = null;

		 			if (node.getDynaAttr().get("dayCostFromPrevOpLastDay") != null && StringUtils.isNotBlank(node.getDynaAttr().get("dayCostFromPrevOpLastDay").toString()))
		 				dayCostFromPrevOpLastDay = CommonUtil.getConfiguredInstance().toDouble(node.getDynaAttr().get("dayCostFromPrevOpLastDay").toString(), request.getLocale());

		 			if (node.getDynaAttr().get("dayTangibleCostFromPrevOpLastDay") != null && StringUtils.isNotBlank(node.getDynaAttr().get("dayTangibleCostFromPrevOpLastDay").toString()))
						dayTangibleCostFromPrevOpLastDay = CommonUtil.getConfiguredInstance().toDouble(node.getDynaAttr().get("dayTangibleCostFromPrevOpLastDay").toString(), request.getLocale());

					if (node.getDynaAttr().get("costAdjustFromPrevOpLastDay") != null && StringUtils.isNotBlank(node.getDynaAttr().get("costAdjustFromPrevOpLastDay").toString()))
						costAdjustFromPrevOpLastDay = CommonUtil.getConfiguredInstance().toDouble(node.getDynaAttr().get("costAdjustFromPrevOpLastDay").toString(), request.getLocale());
					
					if (node.getDynaAttr().get("completionCostFromPrevOpLastDay") != null && StringUtils.isNotBlank(node.getDynaAttr().get("completionCostFromPrevOpLastDay").toString()))
						completionCostFromPrevOpLastDay = CommonUtil.getConfiguredInstance().toDouble(node.getDynaAttr().get("completionCostFromPrevOpLastDay").toString(), request.getLocale());
					
					if (node.getDynaAttr().get("testCostFromPrevOpLastDay") != null && StringUtils.isNotBlank(node.getDynaAttr().get("testCostFromPrevOpLastDay").toString()))
						testCostPrevOpLastDay = CommonUtil.getConfiguredInstance().toDouble(node.getDynaAttr().get("testCostFromPrevOpLastDay").toString(), request.getLocale());
					
					if (node.getDynaAttr().get("otherCostFromPrevOpLastDay") != null && StringUtils.isNotBlank(node.getDynaAttr().get("otherCostFromPrevOpLastDay").toString()))
						otherCostPrevOpLastDay = CommonUtil.getConfiguredInstance().toDouble(node.getDynaAttr().get("otherCostFromPrevOpLastDay").toString(), request.getLocale());
					
					if (node.getDynaAttr().get("commentCostFromPrevOpLastDay") != null && StringUtils.isNotBlank(node.getDynaAttr().get("commentCostFromPrevOpLastDay").toString()))
						commentCostFromPrevOpLastDay = node.getDynaAttr().get("commentCostFromPrevOpLastDay").toString();
					
					thisReport.setDaycost(dayCostFromPrevOpLastDay);
		 			thisReport.setDaytangiblecost(dayTangibleCostFromPrevOpLastDay);
		 			thisReport.setCostadjust(costAdjustFromPrevOpLastDay);
		 			thisReport.setCommentCost(commentCostFromPrevOpLastDay);
		 			thisReport.setDaycompletioncost(completionCostFromPrevOpLastDay);
		 			thisReport.setDaytestcost(testCostPrevOpLastDay);
		 			thisReport.setOtherCost(otherCostPrevOpLastDay);
				}
	 		}
			String onlyAllowSettingQcFlagTo = session.getCombinedAclGroups().getOnlyAllowSettingQcFlagTo();
			if (StringUtils.isNotBlank(onlyAllowSettingQcFlagTo)) {
				boolean validate = true;
				if (StringUtils.isNotBlank(thisReport.getReportDailyUid())) {
					// when updating existing record
					List<ReportDaily> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and reportDailyUid = :reportDailyUid", "reportDailyUid", thisReport.getReportDailyUid());
					if (results != null && !results.isEmpty()) {
						if (StringUtils.isBlank(results.get(0).getQcFlag())) {
							validate = true;
						} else if (results.get(0).getQcFlag().equals(thisReport.getQcFlag())) {
							// if user did not try to change qc flag
							validate = false;
						} else {
							validate = true;
						}
					}
				}
				if (validate) {
					if (QC_NOT_DONE.equals(onlyAllowSettingQcFlagTo)) {
						if (StringUtils.isNotBlank(thisReport.getQcFlag()) && !QC_NOT_DONE.equals(thisReport.getQcFlag())) {
							status.addError("You are only allowed to set QC Status to 'QC Not Done'");
							status.setContinueProcess(false, true);
							return;
						}
					} else if (RIG_SIDE_QC_DONE.equals(onlyAllowSettingQcFlagTo)) {
						if (StringUtils.isNotBlank(thisReport.getQcFlag()) && !QC_NOT_DONE.equals(thisReport.getQcFlag()) && !RIG_SIDE_QC_DONE.equals(thisReport.getQcFlag())) {
							status.addError("You are only allowed to set QC Status to 'QC Not Done' or 'Rig Side QC Done'");
							status.setContinueProcess(false, true);
							return;
						}
					}
				}
			}
			
			Date userDate = null;
			if (node.getDynaAttr().get("daydate") instanceof java.util.Date) {
				userDate = (Date) node.getDynaAttr().get("daydate");
			} else {
				String daydate = (String) node.getDynaAttr().get("daydate");
				if (StringUtils.isNotBlank(daydate)) {
					if (((BaseCommandBean)commandBean).getUseClientMVCRenderer())
						userDate = new Date(Long.parseLong(daydate));
					else
						userDate = CommonDateParser.parse(daydate);
				}
			}
			this.isChangeDate = (thisReport.getDailyUid() == null || userDate == null) ? false : this.isChangeDate(thisReport.getDailyUid(),userDate);	
			
			if (this.isChangeDate) {
				EnvironmentDischargesUtil.calculateTotalForWellAfterDayChanged(commandBean, node, session);
			}
			
			String thisOperationUid = thisReport.getOperationUid();
			String selectedRigUid = thisReport.getRigInformationUid();
			previousDailyUid = thisReport.getDailyUid();
			
			if (userDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
				
				//get dailyUid with Date enter by user
				String[] paramsFields = {"userDate", "thisOperationUid"};
				Object[] paramsValues = new Object[2]; paramsValues[0] = userDate; paramsValues[1] = thisOperationUid;
				String strSql = "SELECT dailyUid FROM Daily WHERE (isDeleted = false or isDeleted is null) and dayDate = :userDate AND operationUid = :thisOperationUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				String  thisdailyUid = "";
				if (!lstResult.isEmpty()) {
					Object a = lstResult.get(0);
					if (a != null) {
						thisdailyUid = a.toString();
						session.setCurrentDailyUid(thisdailyUid);
					}
				}
					
				//check if create new record
			 	if (!ApplicationUtils.getConfiguredInstance().isIdentifierValueExists(object)) {		
			 		//if the daily object already exist, test and use it
				 	if (!("".equals(thisdailyUid))) {
				 		//set dailyUid to the report 
						thisReport.setReportDatetime(userDate);
				 		thisReport.setDailyUid(thisdailyUid);
				 		String[] paramsFields2 = {"thisdailyUid", "thisReportType"};
				 		Object[] paramsValues2 = new Object[2];
						paramsValues2[0] = thisdailyUid; 
						paramsValues2[1] = thisReport.getReportType();
						
						strSql = "SELECT reportDailyUid FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :thisdailyUid AND reportType=:thisReportType";
						lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
						if (!lstResult.isEmpty()) {
							status.setDynamicAttributeError(node, "daydate", "Report Daily date " + formatter.format(userDate) + " already exist.");
					 		status.setContinueProcess(false, true);
					 		return;
					 	}
				 	} else {
				 		// create new day				 						 		
						Daily thisDaily = CommonUtil.getConfiguredInstance().createDay(userDate, thisOperationUid, session, request, commandBean.getSystemMessage());
						createdNewDay = true;
						thisReport.setDailyUid(thisDaily.getDailyUid());
						thisReport.setReportDatetime(thisDaily.getDayDate());
				 	}				 	
			 	} else { //if now is updating a existing record	
					//if the date for the enter date exist, if not go to create the daily object
					if (!("".equals(thisdailyUid))) {
						//check if the daily report record already exist for this date, if not change the date of the daily id to the one just enter = edit date for a day
						String[] paramsFields2 = {"thisdailyUid", "thisreportDailyUid", "thisReportType"};
				 		Object[] paramsValues2 = new Object[3];
						paramsValues2[0] = thisdailyUid; 
						paramsValues2[1] = thisReport.getReportDailyUid();
						paramsValues2[2] = thisReport.getReportType();
						strSql = "SELECT reportDailyUid FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :thisdailyUid AND reportDailyUid <> :thisreportDailyUid AND reportType=:thisReportType";
						lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
						String  reportDailyUid = "";
						if (!lstResult.isEmpty()) {
							Object b = lstResult.get(0);
							if (b != null) reportDailyUid = b.toString();
						}
						
						if (!("".equals(reportDailyUid))) {
				 			status.setDynamicAttributeError(node, "daydate", "Report Daily date " + formatter.format(userDate) + " already exist.");
				 			status.setContinueProcess(false, true);
				 			return;
				 		} else {
				 			//need to check if any reportDaily object use this daily object or not if not can update the daily object date
				 			String[] paramsFields3 = {"thisdailyUid", "thisreportDailyUid"};
					 		Object[] paramsValues3 = new Object[2];
							paramsValues3[0] = thisdailyUid; 
							paramsValues3[1] = thisReport.getReportDailyUid();
				 			strSql = "SELECT reportDailyUid FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :thisdailyUid AND reportDailyUid <> :thisreportDailyUid";
							lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields3, paramsValues3);
							if (!lstResult.isEmpty()) {
								//data maintenance to set all related object to tie to this new dailyUid
				 				CommonUtil.getConfiguredInstance().changeDailyUid(thisReport.getDailyUid(), thisdailyUid, thisReport.getReportDailyUid(), thisReport.getReportType(), thisOperationUid);
				 				// update bharun dailyidIn and dailyidOut date for bharun that has previous dailyUid selected
				 				CommonUtil.getConfiguredInstance().UpdateBharunDateInDateOut(thisReport.getDailyUid(), thisdailyUid, thisReport.getReportType());
				 				
								//set the report object to the new date
				 				thisReport.setReportDatetime(userDate);
				 				thisReport.setDailyUid(thisdailyUid);	
							} else {
								//update daily record to new date - change date on daily object	
					 			String strSql1 = "FROM Daily WHERE (isDeleted = false or isDeleted is null) and dailyUid =:thisDailyUid";
					 			String[] paramsFields1 = {"thisDailyUid"};
					 			Object[] paramsValues1 = {thisReport.getDailyUid()};
					 			
					 			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
					 			
					 			if (lstResult.size() > 0) {
					 				//set the report object to the new date
					 				thisReport.setReportDatetime(userDate);
					 				updateExistingDaily = true;
					 				
					 				Daily thisDay = (Daily) lstResult.get(0);
					 				thisDay.setDayDate(userDate);
									ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisDay);
									D2ApplicationEvent.refresh();
									
									CommonUtil.getConfiguredInstance().setStartAndLastOperationDate(thisOperationUid);
					 			}
							}
				 		}
				 	} else {			 		
						Daily thisDaily = CommonUtil.getConfiguredInstance().createDay(userDate, thisOperationUid, session, request, commandBean.getSystemMessage(), null, null, null, null);
						updateExistingDaily = true;
						
				 		//data maintenance to set all related object to tie to this new dailyUid
		 				CommonUtil.getConfiguredInstance().changeDailyUid(thisReport.getDailyUid(), thisDaily.getDailyUid(), thisReport.getReportDailyUid(), thisReport.getReportType(), thisOperationUid);
		 				// update bharun dailyidIn and dailyidOut date for bharun that has previous dailyUid selected
		 				CommonUtil.getConfiguredInstance().UpdateBharunDateInDateOut(thisReport.getDailyUid(), thisDaily.getDailyUid(), thisReport.getReportType());
		 				
				 		thisReport.setDailyUid(thisDaily.getDailyUid());
						thisReport.setReportDatetime(thisDaily.getDayDate());
				 	}	
				}
			 	
			 	//copy from yesterday
			 	String[] selected_carry_forward = request.getParameterValues("selected_carry_forward");
	 			if (selected_carry_forward == null) {
	 				if (node.getDynaAttr().get("selected_carry_forward") instanceof String) {
	 					selected_carry_forward = new String[] { node.getDynaAttr().get("selected_carry_forward").toString() };
	 				} else {
	 					selected_carry_forward = (String[]) node.getDynaAttr().get("selected_carry_forward");
	 				}
	 			}
	 		
	 			if (selected_carry_forward != null) {
		 			String[] selected_carry_from_dailyUid = request.getParameterValues("selected_carry_from_dailyUid");
		 			String[] selected_carry_from_rigInformationUid = request.getParameterValues("selected_carry_from_rigInformationUid");
		 			Daily thisDay = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisReport.getDailyUid());
		 			//CommonUtil.getConfiguredInstance().carryFromYesterday(thisDay, request, commandBean.getSystemMessage(), selected_carry_from_dailyUid, selected_carry_from_rigInformationUid, selected_carry_forward, selectedRigUid);
		 			CommonUtil.getConfiguredInstance().carryFromLastDay(thisDay, request, commandBean.getSystemMessage(), selected_carry_from_dailyUid, selected_carry_from_rigInformationUid, selected_carry_forward, selectedRigUid);
		 			thisReport.setCopyFromYesterdayUsed(true);
	 			}
			 	
			 	//for first day, update geology 24 hrs progress if changing 0600 depth for ddr
			 	Daily previousDaily = ApplicationUtils.getConfiguredInstance().getYesterday(thisOperationUid, session.getCurrentDailyUid());
			 	if (previousDaily == null) {
			 		if (("DDR".equalsIgnoreCase(thisReport.getReportType()) && ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_GEO_24HR_PROGRESS))))) {
			 			String[] paramsFields2 = {"operationUid", "dailyUid"};
				 		Object[] paramsValues2 = new Object[2];
						paramsValues2[0] = thisOperationUid; 
						paramsValues2[1] = thisReport.getDailyUid();
						
					 	List<ReportDaily> dgrReportDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid= :operationUid " +
					 			"and dailyUid= :dailyUid and reportType='DGR'", paramsFields2, paramsValues2);
					 	
				 		if (dgrReportDaily.size() > 0) {
				 			for (ReportDaily currentDGR : dgrReportDaily) {
				 				if (thisReport.getDepth0600MdMsl()!=null) {
				 					if (currentDGR.getDepth0600MdMsl() !=null) {
				 						currentDGR.setGeology24hrProgress(currentDGR.getDepth0600MdMsl() - thisReport.getDepth0600MdMsl());
				 					}
				 				}
				 				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(currentDGR);	
				 			}
				 		}
			 		}
			 	}
			 	
		 		// Update Tomorrow Report Daily progress If Change Current Day Depth0600Md or Midnight Depth
		 		Daily tomorrowDaily = ApplicationUtils.getConfiguredInstance().getTomorrow(thisOperationUid, session.getCurrentDailyUid());

			 	if (tomorrowDaily != null) {
			 		
			 		Double tGeology24hrProgress = null;
				 	Double tDriller24hrProgress = null;
				 	
				 	String[] paramsFields2 = {"operationUid", "dailyUid","reportType"};
			 		Object[] paramsValues2 = new Object[3];
					paramsValues2[0] = thisOperationUid; 
					paramsValues2[1] = tomorrowDaily.getDailyUid();
					paramsValues2[2] = thisReport.getReportType();
				 	List<ReportDaily> tomorrowReportDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid= :operationUid " +
				 			"and dailyUid= :dailyUid and reportType= :reportType", paramsFields2, paramsValues2);
				 	
			 		if (tomorrowReportDaily.size() > 0) {
				 		for (ReportDaily tomorrowRd : tomorrowReportDaily) {
				 			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_GEO_24HR_PROGRESS))) {
					 			if (thisReport.getDepth0600MdMsl()!=null) {
									if (tomorrowRd.getDepth0600MdMsl() !=null ) {
										tGeology24hrProgress = tomorrowRd.getDepth0600MdMsl() - thisReport.getDepth0600MdMsl();
									}
								}
					 			tomorrowRd.setGeology24hrProgress(tGeology24hrProgress);
					 			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(tomorrowRd);
				 			}
				 			
				 			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DAILY_24HR_PROGRESS))) {
					 			if (thisReport.getDepthMdMsl()!=null) {
									if (tomorrowRd.getDepthMdMsl() !=null ) {
										tDriller24hrProgress = tomorrowRd.getDepthMdMsl() - thisReport.getDepthMdMsl();
									}
								}
					 			tomorrowRd.setProgress(tDriller24hrProgress);
					 			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(tomorrowRd);
				 			}
				 		}
			 		}
			 	} 			

			 	//Synchronise the depth0600MdMsl and depth0600TvdMsl field between ddr and dgr records
			 	if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_UPDATE_0600_DEPTH))) {				 	
				 	String[] paramsFields2 = {"operationUid", "dailyUid"};
			 		Object[] paramsValues2 = new Object[2];
					paramsValues2[0] = thisOperationUid; 
					paramsValues2[1] = thisReport.getDailyUid();
				 	List<ReportDaily> reportDailies = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid= :operationUid and dailyUid= :dailyUid", paramsFields2, paramsValues2);
				 	if (reportDailies.size() > 0) {
				 		for (ReportDaily rd1 : reportDailies) {
				 			if (!(thisReport.getReportType().equals(rd1.getReportType()))) {
				 				rd1.setDepth0600MdMsl(thisReport.getDepth0600MdMsl());
				 				rd1.setDepth0600TvdMsl(thisReport.getDepth0600TvdMsl());
				 				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rd1);
				 			}
				 		}
				 	}
			 	}
			 	
				//calculate the Days since Last Recordable incident
			 	if (!"0".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoCalculateDaysSinceLastRecordableIncident"))) {
					if (thisReport.getLastRecordableIncidentDateTime()!= null) {
						//calculate the days since last Recordable Incident
						double day = CommonUtil.getConfiguredInstance().calculateDaysSinceLastRecordableIncident(userDate, thisReport.getLastRecordableIncidentDateTime(),session.getCurrentGroupUid());
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "daysSinceLastRecordableIncident");	
						thisConverter.setBaseValue(day);
						thisReport.setDaysSinceLastRecordableIncident(thisConverter.getConvertedValue());
					}
			 	}	
			 	
			 	//Calculate Days To Bop Test
			 	Date lastBopDate = thisReport.getLastbopDate();
			 	
			 	if (lastBopDate != null && StringUtils.isNotBlank(lastBopDate.toString())) {
			 		String bopTestDueEveryXdays = GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_BOP_TEST_DUE_EVERY_X_DAYS);
			 		if (bopTestDueEveryXdays == null || StringUtils.isBlank(bopTestDueEveryXdays)) {
			 			bopTestDueEveryXdays = "0";
			 		}
		 			Date nextBopDate = DateUtils.addDays(lastBopDate, Integer.parseInt(bopTestDueEveryXdays));			
		 		
		 			if (userDate.compareTo(lastBopDate) != -1) {
		 				double daysToBopTest = CommonUtil.getConfiguredInstance().calculateDaysToBopTest(userDate, nextBopDate, session.getCurrentGroupUid());
		 				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "daysToBopTest");	
		 				thisConverter.setBaseValue(daysToBopTest);
		 				thisReport.setDaysToBopTest(thisConverter.getConvertedValue());
		 			} else {
		 				thisReport.setDaysToBopTest(null);
		 			}
			 	} else {
			 		thisReport.setDaysToBopTest(null);
			 	}
			 	
				//auto lookup casing section data
			 	ReportDailyUtils.autoLookupCasingSectionData(commandBean, node, session);
			 	
			 	//Calculate total Production~ 
			 	if (thisReport.getOilProductionVolume()!=null || thisReport.getWaterProduction()!=null) {
			 		double oilProdc = 0.0;
			 		double waterProdc = 0.0;
			 		double totalProdc = 0.0;
			 		double oilCut = 0.0;
			 		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "oilProductionVolume");
			 		if (thisReport.getOilProductionVolume()!=null) {
				 		thisConverter.setBaseValueFromUserValue(thisReport.getOilProductionVolume());
				 		oilProdc = thisConverter.getBasevalue();
			 		}
			 		
			 		thisConverter.setReferenceMappingField(ReportDaily.class, "waterProduction");
			 		if (thisReport.getWaterProduction()!=null) {
				 		thisConverter.setBaseValueFromUserValue(thisReport.getWaterProduction());
				 		waterProdc = thisConverter.getBasevalue();
			 		}
			 		
			 		thisConverter.setReferenceMappingField(ReportDaily.class, "totalOilWaterProd");
			 		totalProdc = oilProdc + waterProdc;
			 		thisConverter.setBaseValue(totalProdc);
			 		
			 		thisReport.setTotalOilWaterProd(thisConverter.getConvertedValue());
			 		if (totalProdc!=0.0) oilCut = oilProdc / totalProdc * 100;
			 		thisReport.setOilCut(oilCut);
			 	} else {
			 		thisReport.setTotalOilWaterProd(null);
			 		thisReport.setOilCut(null);
			 	}
			 	// The Pump checking
			 	if (StringUtils.isEmpty(thisReport.getProductionPumpParamUid())) {
			 		thisReport.setSpm(null);
			 		thisReport.setStrokeLength(null);
			 		thisReport.setVariableFrequencyDriveCurrent(null);
			 		thisReport.setVariableFrequencyDriveMotorTorque(null);
			 	}
			 	
			 	//do check, if report_type <> dgr
			 	String reportType= thisReport.getReportType();
			 	if (!"DGR".equals(reportType)) {
				 	//update safety_ticket rigInformationUid accordingly
				 	String[] paramsFields2 = {"thisdailyUid", "thisOperationUid"};
			 		Object[] paramsValues2 = new Object[2];
					paramsValues2[0] = thisdailyUid; 
					paramsValues2[1] = thisReport.getOperationUid();					
					strSql = "FROM SafetyTicket WHERE (isDeleted = false or isDeleted is null) and dailyUid = :thisdailyUid AND operationUid=:thisOperationUid";
					List<SafetyTicket> lstSafetyResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
					if (lstSafetyResult.size() > 0) {
						for (SafetyTicket recSet : lstSafetyResult) {
							recSet.setRigInformationUid(thisReport.getRigInformationUid());
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(recSet);
						}
					}
			 	}
			 	
			 	//UPDATE monthly currency key in daily
			 	if (session.getCurrentOperation() != null) {
			 		UOMMapping mapping = UOMManager.getCurrentClassPropertyMapping(ReportDaily.class, "daycost", false, session.getCurrentOperation().getUomTemplateUid());
					CommonUtil.getConfiguredInstance().updateDailyMonthlyCurrencyKey(thisReport.getDailyUid(), mapping);
			 	}
			 	
			 	if (autoPopulateLastHoleSize) {
					String strSql2 = "FROM Activity WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :selectedDailyUid AND dayPlus = 0 AND (holeSize is not null OR holeSize != '') ORDER BY endDatetime DESC";
					String[] paramsFields2 = {"selectedOperationUid","selectedDailyUid"};
					Object[] paramsValues2 = {thisReport.getOperationUid(),thisReport.getDailyUid()};
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
					if (!lstResult2.isEmpty()) {
						Activity lastActivity = (Activity) lstResult2.get(0);
						thisReport.setLastHolesize(lastActivity.getHoleSize());
						if(ApplicationUtils.getConfiguredInstance().isIdentifierValueExists(object)) {
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
						}
					} else {
						thisReport.setLastHolesize(null);
						if(ApplicationUtils.getConfiguredInstance().isIdentifierValueExists(object)) {
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
						}
					}
				}
				
				if (autoPopulatePrevHoleSize) {
					Calendar thisCalender = Calendar.getInstance();
					thisCalender.setTime(thisReport.getReportDatetime());
					thisCalender.add(Calendar.DATE, 1);
					thisCalender.add(Calendar.SECOND, -1);
					Date nextDate = thisCalender.getTime();
					
					String strSql2 = "FROM HoleSection WHERE (isDeleted = false or isDeleted is null) AND wellboreUid = :selectedWellboreUid AND dateTimeOut < :reportDayDate AND (dateTimeIn is not null) AND (dateTimeOut is not null) ORDER BY dateTimeOut DESC";
					String[] paramsFields2 = {"selectedWellboreUid","reportDayDate"};
					Object[] paramsValues2 = {thisReport.getWellboreUid(),nextDate};
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
					if (!lstResult2.isEmpty()) {
						HoleSection temp = (HoleSection) lstResult2.get(0);
						double min = 0.0;
						double max = 0.0;
						if (temp.getHoleSize() != null) {
							min = temp.getHoleSize();
							max = temp.getHoleSize();
						}
						for (Object hss : lstResult2) {
							HoleSection hs = (HoleSection) hss;
							if (hs.getHoleSize() != null) {
								if (hs.getHoleSize() < min) {
									min = hs.getHoleSize();
								}
								if (hs.getHoleSize() > max) {
									max = hs.getHoleSize();
								}
							}
						}
						if (nextDate.after(temp.getDateTimeOut())) {
							if (min != 0.0) {
								thisReport.setPrevHolesize(min);
							} else {
								thisReport.setPrevHolesize(null);
							}
						} else if (nextDate.equals(temp.getDateTimeOut())) {
							if (max != 0.0) {
								thisReport.setPrevHolesize(max);
							} else {
								thisReport.setPrevHolesize(null);
							}
						}
						if(ApplicationUtils.getConfiguredInstance().isIdentifierValueExists(object)) {
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
						}
					} else {
						thisReport.setPrevHolesize(null);
						if(ApplicationUtils.getConfiguredInstance().isIdentifierValueExists(object)) {
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
						}
					}
				}
				
				HseUtils.populateAndUpdateLastLTI(thisReport.getDailyUid(), new UserSelectionSnapshot(session).getRigInformationUid(), thisReport.getGroupUid(), false, thisReport);
			}
		}
		if (object instanceof DailyShiftInfo) {
			if (forwardShiftReportType!=null && StringUtils.isNotBlank(forwardShiftReportType)){
				DailyShiftInfo shiftInfo = (DailyShiftInfo) object;
				
				if (shiftInfo.getDailyUid()!=null){
					Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, shiftInfo.getDailyUid());
					
					if (daily!=null){
						Calendar dailyDayDate = Calendar.getInstance();
						dailyDayDate.setTime(daily.getDayDate());
						
						if(PropertyUtils.getProperty(object, "startDatetime") != null){
							Calendar reportTime = Calendar.getInstance();
							reportTime.setTime((Date) PropertyUtils.getProperty(object, "startDatetime"));
							reportTime.set(Calendar.YEAR, dailyDayDate.get(Calendar.YEAR));
							reportTime.set(Calendar.MONTH, dailyDayDate.get(Calendar.MONTH));
							reportTime.set(Calendar.DAY_OF_MONTH, dailyDayDate.get(Calendar.DAY_OF_MONTH));
							PropertyUtils.setProperty(object, "startDatetime", reportTime.getTime());
						}
						
						if(PropertyUtils.getProperty(object, "endDatetime") != null){
							Calendar reportTime = Calendar.getInstance();
							reportTime.setTime((Date) PropertyUtils.getProperty(object, "endDatetime"));
							reportTime.set(Calendar.YEAR, dailyDayDate.get(Calendar.YEAR));
							reportTime.set(Calendar.MONTH, dailyDayDate.get(Calendar.MONTH));
							reportTime.set(Calendar.DAY_OF_MONTH, dailyDayDate.get(Calendar.DAY_OF_MONTH));
							PropertyUtils.setProperty(object, "endDatetime", reportTime.getTime());
						}
					}
				}
				
				if (ReportDailyUtils.isOmvCountrySpecific(shiftInfo.getWellUid(), shiftInfo.getOperationUid(), forwardShiftReportType, shiftCountry)){
					if(shiftInfo.getStartDatetime() != null && shiftInfo.getEndDatetime() != null){
						
						if(shiftInfo.getStartDatetime().getTime() > shiftInfo.getEndDatetime().getTime()){
							status.setContinueProcess(false, true);
							status.setFieldError(node, "startDatetime", "Start Time cannot be after End Time.");
							return;
						}
					}
				}	
			}	
		}
		
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		CustomFieldUom convert = new CustomFieldUom(commandBean, ReportDaily.class, "daycost");
		
		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CARRY_DAY_COST_FROM_PREV_OP))) {
			if (convert.isUOMMappingAvailable()) {
				convert.setReferenceMappingField(ReportDaily.class, "daycost");
				node.setCustomUOM("@dayCostFromPrevOpLastDay", convert.getUOMMapping());
				convert.setReferenceMappingField(ReportDaily.class, "daytangiblecost");
				node.setCustomUOM("@dayTangibleCostFromPrevOpLastDay", convert.getUOMMapping());
				convert.setReferenceMappingField(ReportDaily.class, "costadjust");
				node.setCustomUOM("@costAdjustFromPrevOpLastDay", convert.getUOMMapping());
				convert.setReferenceMappingField(ReportDaily.class, "daycompletioncost");
				node.setCustomUOM("@completionCostFromPrevOpLastDay", convert.getUOMMapping());
				convert.setReferenceMappingField(ReportDaily.class, "otherCost");
				node.setCustomUOM("@otherCostFromPrevOpLastDay", convert.getUOMMapping());
				convert.setReferenceMappingField(ReportDaily.class, "daytestcost");
				node.setCustomUOM("@testCostFromPrevOpLastDay", convert.getUOMMapping());
			}
			node.getDynaAttr().put("dayCostFromPrevOpLastDay", "");
			node.getDynaAttr().put("dayTangibleCostFromPrevOpLastDay", "");
			node.getDynaAttr().put("costAdjustFromPrevOpLastDay", "");
			node.getDynaAttr().put("completionCostFromPrevOpLastDay", "");
			node.getDynaAttr().put("otherCostFromPrevOpLastDay", "");
			node.getDynaAttr().put("testCostFromPrevOpLastDay", "");
		}
		
		Object object = node.getData();
		
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);
		
		if (object instanceof ReportDaily) {
			ReportDaily reportDaily = (ReportDaily) object;
			reportDaily.setReportType(this.defaultReportType);
			
			if (forwardShiftReportType!=null && StringUtils.isNotBlank(forwardShiftReportType)){
				String dailyUid = CommonUtil.getConfiguredInstance().getLastDailyUidInOperationByReportType(userSelection.getOperationUid(), forwardShiftReportType);
				
				String strSql = "FROM DailyShiftInfo WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid ORDER BY startDatetime";
				String[] paramsField = {"dailyUid"};
				Object[] paramsValue = {dailyUid};
				List<DailyShiftInfo> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result.size()>0){
					
					Map<String, CommandBeanTreeNode> shiftNodes = node.getChild(DailyShiftInfo.class.getSimpleName());
					if(shiftNodes == null || shiftNodes.isEmpty()) {
						
						for (DailyShiftInfo shift : result){
							DailyShiftInfo newDailyShiftInfo = new DailyShiftInfo();
							newDailyShiftInfo.setSequence(shift.getSequence());
							newDailyShiftInfo.setShiftLabel(shift.getShiftLabel());
							newDailyShiftInfo.setStartDatetime(shift.getStartDatetime());
							newDailyShiftInfo.setEndDatetime(shift.getEndDatetime());
							newDailyShiftInfo.setPaxNoContractor(shift.getPaxNoContractor());
							newDailyShiftInfo.setPaxNoOperator(shift.getPaxNoOperator());
							newDailyShiftInfo.setPaxNoServices(shift.getPaxNoServices());
							node.addCustomNewChildNodeForInput(newDailyShiftInfo);
						}
					}else{	
						int count = 1;
						
						for (DailyShiftInfo shift : result){
							if (count==1){
								for (Entry<String, CommandBeanTreeNode> entry : shiftNodes.entrySet()) {
									CommandBeanTreeNode nodes = entry.getValue();
									DailyShiftInfo newDailyShiftInfo = (DailyShiftInfo) nodes.getData();
									newDailyShiftInfo.setSequence(shift.getSequence());
									newDailyShiftInfo.setShiftLabel(shift.getShiftLabel());
									newDailyShiftInfo.setStartDatetime(shift.getStartDatetime());
									newDailyShiftInfo.setEndDatetime(shift.getEndDatetime());
									newDailyShiftInfo.setPaxNoContractor(shift.getPaxNoContractor());
									newDailyShiftInfo.setPaxNoOperator(shift.getPaxNoOperator());
									newDailyShiftInfo.setPaxNoServices(shift.getPaxNoServices());
								}
							}else{
								DailyShiftInfo newDailyShiftInfo = new DailyShiftInfo();
								newDailyShiftInfo.setSequence(shift.getSequence());
								newDailyShiftInfo.setShiftLabel(shift.getShiftLabel());
								newDailyShiftInfo.setStartDatetime(shift.getStartDatetime());
								newDailyShiftInfo.setEndDatetime(shift.getEndDatetime());
								newDailyShiftInfo.setPaxNoContractor(shift.getPaxNoContractor());
								newDailyShiftInfo.setPaxNoOperator(shift.getPaxNoOperator());
								newDailyShiftInfo.setPaxNoServices(shift.getPaxNoServices());
								node.addCustomNewChildNodeForInput(newDailyShiftInfo);
							}
							count++;
						}
					}
				}		
			}
	
			// default date
			Operation thisOperation = UserSession.getInstance(request).getCurrentOperation();
			if (thisOperation != null) {
				//auto fill data base on the current selected day
				//String dailyUid = ApplicationUtils.getConfiguredInstance().getLastDailyUidInOperation(userSelection.getOperationUid());
				String dailyUid = CommonUtil.getConfiguredInstance().getLastDailyUidInOperationByReportType(userSelection.getOperationUid(), this.defaultReportType);
				String dailyUidWellbore = CommonUtil.getConfiguredInstance().getLastDailyUidInWellboreByReportType(userSelection.getWellboreUid(), this.defaultReportType);
				Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
				Daily selectedDailyWellbore = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUidWellbore);
				Date newReportDayDate = CommonUtil.getConfiguredInstance().getTodayReportDate(thisOperation.getOperationUid(), userSelection.getDailyUid(), this.defaultReportType, userSelection);				
				if (selectedDaily != null) {
					String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :selectedDailyUid AND reportType = :thisReportType";
					String[] paramsFields = {"selectedDailyUid", "thisReportType"};
					String[] paramsValues = {selectedDaily.getDailyUid(), this.defaultReportType};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, QueryProperties.useNewSession());
					
					if (!lstResult.isEmpty()) {
						ReportDaily selectedReportDaily =  (ReportDaily) lstResult.get(0);
						
						String reportNumber = selectedReportDaily.getReportNumber();
						String operationTypeReportNumber = selectedReportDaily.getOperationTypeReportNumber();
						try {
							Integer newReportNumber = Integer.parseInt(reportNumber);
							newReportNumber++;
							reportNumber = newReportNumber.toString();
						} catch (Exception e) {
							reportNumber += "a";
						}
						if (operationTypeReportNumber != null) {
							try {
								Integer newOperationTypeReportNumber = Integer.parseInt(operationTypeReportNumber);
								newOperationTypeReportNumber++;
								operationTypeReportNumber = newOperationTypeReportNumber.toString();
							} catch (Exception e) {
								operationTypeReportNumber += "a";
							}
						} else {
							operationTypeReportNumber = "1";
						}
						selectedReportDaily.setReportNumber(reportNumber);
						selectedReportDaily.setOperationTypeReportNumber(operationTypeReportNumber);
						if (autoPopulateLastHoleSize) {
							selectedReportDaily.setLastHolesize(null);
						}
						
						if (autoPopulatePrevHoleSize) {
							selectedReportDaily.setPrevHolesize(null);
						}
						
						//get  the dynamic fields values
						Map<String, Object> thisDynamicFieldsValue = DynamicFieldUtil.getFieldValues(userSelection.getGroupUid(), selectedReportDaily);
						
						//set all Uid to null, so will get new Uid when saving
						this.setNullToPropertiesOnStartNewDay(selectedReportDaily, userSelection);
						
						//20057 - auto lookup last BOP for current rig
						if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_LAST_BOP))) {
							getLastBOPLogNewInputMode(userSelection,selectedReportDaily);
						}
						//20057 - auto lookup last LTI fix for addnew
						if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_LAST_LTI))) {
							populateAndUpdateLastLTINewInputMode(userSelection, selectedReportDaily);
						}
						
						if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_PREV_DAY_ACTIVITY_DEPTH))) {
							String prevDailyUid = userSelection.getDailyUid();
							if (StringUtils.isNotBlank(prevDailyUid)) {
								Daily prevDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(prevDailyUid);
								if (prevDaily != null) {
									QueryProperties qp2 = new QueryProperties();
									qp2.setUomConversionEnabled(false);
									String strSql4 = "FROM Activity WHERE (isDeleted = false OR isDeleted IS NULL) AND dailyUid = :selectedDailyUid AND depthMdMsl IS NOT NULL " +
											"AND (isSimop = false OR isSimop IS NULL) AND (isOffline = false OR isOffline IS NULL) ORDER BY dayPlus DESC, endDatetime DESC, startDatetime DESC ";
									String[] paramsFields4 = {"selectedDailyUid"};
									String[] paramsValues4 = {prevDaily.getDailyUid()};
									List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4,qp2);
									CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "depthMdMsl");	
									if (lstResult4.size() > 0) {
										Activity activity = (Activity) lstResult4.get(0);
										thisConverter.setBaseValue(activity.getDepthMdMsl());
										selectedReportDaily.setDepthMdMsl(thisConverter.getConvertedValue());
									} else {
										selectedReportDaily.setDepthMdMsl(null);
									}
								}
							}
						}
						//un-set QC flag when start new day
						selectedReportDaily.setQcFlag(QC_NOT_DONE);
						
						//SET DEFAULT DGR START AND END TIME
						if ("DGR".equals(this.defaultReportType)) {
							CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
							ReportDailyUtils.autoPopulateBitDiameter(selectedReportDaily, this.defaultReportType, thisConverter);
						}
						
						// AUTO POPULATE QC FLAG
						if ("DGR".equals(this.defaultReportType) && autoPopulateQCflagFromDDR) {
							if (newReportDayDate != null) {
								String strSql3 = "SELECT qcFlag FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND reportDatetime = :selectedDayDate AND reportType = 'DDR'";
								String[] paramsFields3 = {"selectedDayDate","selectedOperationUid"};
								Object[] paramsValues3 = {newReportDayDate,thisOperation.getOperationUid()};
								List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3);
								if (!lstResult3.isEmpty()) {
									Object a3 = lstResult3.get(0);
									node.getDynaAttr().put("ddrQcFlag", a3.toString());
								} else {
									node.getDynaAttr().put("ddrQcFlag", "");
								}
							}
						}
						
						//AUTO UPDATE 0600 DEPTH
						if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_UPDATE_0600_DEPTH))) {
							//IF CREATING NEW DGR REPORT, IT WILL GET THE DEPTH0600 FROM DDR
							if ("DGR".equals(this.defaultReportType)) {							
								if (newReportDayDate != null) {
									String strSql2 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND reportDatetime = :selectedDayDate AND reportType = 'DDR'";
									String[] paramsFields2 = {"selectedDayDate","selectedOperationUid"};
									Object[] paramsValues2 = {newReportDayDate,thisOperation.getOperationUid()};
									List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
									
									if (!lstResult2.isEmpty()) {
										ReportDaily selectedDDRReportDaily = (ReportDaily) lstResult2.get(0);
										CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
										thisConverter.setReferenceMappingField(ReportDaily.class, "depth0600MdMsl");
										if (selectedDDRReportDaily.getDepth0600MdMsl() != null) {											
											thisConverter.setBaseValue(selectedDDRReportDaily.getDepth0600MdMsl());
											selectedReportDaily.setDepth0600MdMsl(thisConverter.getConvertedValue());
										} else if (selectedDDRReportDaily.getDepthMdMsl() != null) {											
											thisConverter.setBaseValue(selectedDDRReportDaily.getDepthMdMsl());
											selectedReportDaily.setDepth0600MdMsl(thisConverter.getConvertedValue());
										}
										
										thisConverter.setReferenceMappingField(ReportDaily.class, "depth0600TvdMsl");
										if (selectedDDRReportDaily.getDepth0600TvdMsl() != null) {
											thisConverter.setBaseValue(selectedDDRReportDaily.getDepth0600TvdMsl());
											selectedReportDaily.setDepth0600TvdMsl(thisConverter.getConvertedValue());
										} else if (selectedDDRReportDaily.getDepthTvdMsl() != null) {
											thisConverter.setBaseValue(selectedDDRReportDaily.getDepthTvdMsl());
											selectedReportDaily.setDepth0600TvdMsl(thisConverter.getConvertedValue());
										}										 
									}
								}
							}
							//IF CREATING NEW DDR REPORT, IT WILL GET THE DEPTH0600 FROM DGR
							if ("DDR".equals(this.defaultReportType)) {						
								if (newReportDayDate != null) {
									String strSql3 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND reportDatetime = :selectedDayDate AND reportType = 'DGR'";
									String[] paramsFields3 = {"selectedDayDate","selectedOperationUid"};
									Object[] paramsValues3 = {newReportDayDate,thisOperation.getOperationUid()};
									List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3, qp);
									
									if (!lstResult3.isEmpty()) {
										ReportDaily selectedDGRReportDaily = (ReportDaily) lstResult3.get(0);
										CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
										thisConverter.setReferenceMappingField(ReportDaily.class, "depth0600MdMsl");
										if (selectedDGRReportDaily.getDepth0600MdMsl() != null) {
											thisConverter.setBaseValue(selectedDGRReportDaily.getDepth0600MdMsl());
											selectedReportDaily.setDepth0600MdMsl(thisConverter.getConvertedValue());
										}
										
										thisConverter.setReferenceMappingField(ReportDaily.class, "depth0600TvdMsl");
										if (selectedDGRReportDaily.getDepth0600TvdMsl() != null) {
											thisConverter.setBaseValue(selectedDGRReportDaily.getDepth0600TvdMsl());
											selectedReportDaily.setDepth0600TvdMsl(thisConverter.getConvertedValue());
										}										 
									}
								}
							}
						}
						
						//AUTO POPULATE MUD TYPE
						if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_MUD_TYPE))) {
							//IF CREATING NEW DGR, it will auto populate mud type from DDR mud properties.
							if ("DGR".equals(this.defaultReportType)) {		
								String mudType = null;
								String[] paramNames5 = {"dailyUid","selectedOperationUid"};
								Object[] paramValues5 = {userSelection.getDailyUid(),thisOperation.getOperationUid()};
								String strSql5 = "FROM MudProperties WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :dailyUid order by reportNumber desc";
								List lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramNames5, paramValues5);
								if (lstResult5.size() > 0) {
									MudProperties b = (MudProperties) lstResult5.get(0);
									if (b.getMudType() != null){
										selectedReportDaily.setMudType(b.getMudType());
									}
								} else {
									selectedReportDaily.setMudType(mudType);
								}
							}
						} else {
							if ("DGR".equals(this.defaultReportType)) {
								String mudType = null;
								selectedReportDaily.setMudType(mudType);
							}
						}
						
						//auto populate formation name
						if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_FORMATION_NAME))) {
							//IF CREATING NEW DGR REPORT, IT WILL GET THE FORMATION NAME FROM DGR FORMATION DATA SCREEN
							if ("DGR".equals(this.defaultReportType)) {		
								String FormationName = null;
								List lstResult6 = null;
								if (this.wellboreBased) {
									String[] paramNames6 = {"dailyUid","selectedOperationUid","selectedWellboreUid"};
									Object[] paramValues6 = {userSelection.getDailyUid(),thisOperation.getOperationUid(),thisOperation.getWellboreUid()};
									String strSql6 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND wellboreUid  = :selectedWellboreUid AND dailyUid = :dailyUid";
									lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramNames6, paramValues6);
							    } else {
									String[] paramNames6 = {"dailyUid","selectedOperationUid"};
									Object[] paramValues6 = {userSelection.getDailyUid(),thisOperation.getOperationUid()};
									String strSql6 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :dailyUid";
									lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramNames6, paramValues6);
							    }
								if (lstResult6.size() > 0) {
									ReportDaily selectedDDRReportDaily = (ReportDaily) lstResult6.get(0);
									List lstResult7 = null;
									if (this.wellboreBased) {
										String[] paramNames7 = {"midnightDepth","selectedOperationUid","selectedWellboreUid"};
										Object[] paramValues7 = {selectedDDRReportDaily.getDepthMdMsl(),thisOperation.getOperationUid(),thisOperation.getWellboreUid()};
										String strSql7 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) AND  operationUid = :selectedOperationUid AND wellboreUid  = :selectedWellboreUid AND sampleTopMdMsl <= :midnightDepth order by sampleTopMdMsl desc";
										lstResult7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7, paramNames7, paramValues7);
								    } else {
										String[] paramNames7 = {"midnightDepth","selectedOperationUid"};
										Object[] paramValues7 = {selectedDDRReportDaily.getDepthMdMsl(),thisOperation.getOperationUid()};
										String strSql7 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND sampleTopMdMsl <= :midnightDepth order by sampleTopMdMsl desc";
										lstResult7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7, paramNames7, paramValues7);
								    }
									
									if (lstResult7.size() > 0) {
										Formation c = (Formation) lstResult7.get(0);
										if (c.getFormationName() != null) {
											selectedReportDaily.setFormation(c.getFormationName());
										}
									} else {
										selectedReportDaily.setFormation(FormationName);
									}
								}
							}
						} else {
							if("DGR".equals(this.defaultReportType)) {		
								String FormationName = null;
								selectedReportDaily.setFormation(FormationName);
							}
						}
						
						//17935 auto set GeoNet's Daily Morning Update Day Date to newly created report day date + 1
						//From Specs: Criteria for GWP will be to only enable for client where "Date Time" field under Morning Update without Time selector. Only enable for those with date selector. Check before enabling the GWP.
						if ("DGR".equals(this.defaultReportType) && newReportDayDate != null) {
							selectedReportDaily.setCutOffDateTime(this.getCutOffDateTime(userSelection, GroupWidePreference.GWP_AUTO_POPULATE_MORNING_UPDATE_DATE, newReportDayDate, this.customCutOffTime));
							selectedReportDaily.setDailyReportStartDatetime(this.getDailyReportStartDateTime(userSelection, GroupWidePreference.GWP_DEFAULT_REPORT_START_TIME, newReportDayDate, this.customDailyReportStartTime));
							selectedReportDaily.setDailyReportEndDatetime(this.getDailyReportEndDateTime(userSelection, GroupWidePreference.GWP_DEFAULT_REPORT_END_TIME, newReportDayDate, this.customDailyReportEndTime, this.isDailyReportEndDateOnNextDay));
						}
						
						//31560
						//to take note here that the node is set to another object (selectedReportDaily) instead of the original object reportDaily, so set accordingly
						if ("DGR".equalsIgnoreCase(defaultReportType)) {
							Date newDayDate = CommonUtil.getConfiguredInstance().getTodayReportDate(thisOperation.getOperationUid(), userSelection.getDailyUid(), this.defaultReportType, userSelection);
							if (newDayDate != null) {
								String strSql2 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND reportDatetime = :selectedDayDate AND reportType = 'DDR'";
								String[] paramsFields2 = {"selectedDayDate","selectedOperationUid"};
								Object[] paramsValues2 = {newDayDate,thisOperation.getOperationUid()};
								List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
								if (!lstResult2.isEmpty()) {
									ReportDaily selectedDDRReportDaily = (ReportDaily) lstResult2.get(0);
									selectedReportDaily.setRigInformationUid(selectedDDRReportDaily.getRigInformationUid());
									node.getDynaAttr().put("ddrExist", "1");
								} else {
									node.getDynaAttr().put("ddrExist", "0");
								}
							} else {
								node.getDynaAttr().put("ddrExist", "0");
							}
						} else if ("DDR".equalsIgnoreCase(defaultReportType)) {
							node.getDynaAttr().put("ddrExist", "1");
						}
						node.setData(selectedReportDaily);
						
						//loop dynamic fields and set value
						for (String fieldName : thisDynamicFieldsValue.keySet()) {
							node.getDynaField().getValues().put(fieldName, thisDynamicFieldsValue.get(fieldName));
						}
						
						if (selectedReportDaily.getDaysSinceLta() != null) {
							selectedReportDaily.setDaysSinceLta(selectedReportDaily.getDaysSinceLta() + 1);
						}
						
						if (selectedReportDaily.getDaysSinceLastRecordableIncident() != null) {
							selectedReportDaily.setDaysSinceLastRecordableIncident(selectedReportDaily.getDaysSinceLastRecordableIncident() + 1);
						}
						
						if (selectedReportDaily.getDaysSinceLastMedicalIncident() != null) {
							selectedReportDaily.setDaysSinceLastMedicalIncident(selectedReportDaily.getDaysSinceLastMedicalIncident() + 1);
						}	

						if (selectedReportDaily.getEfcOtherCost() == null) {
							selectedReportDaily.setEfcOtherCost(thisOperation.getAfeOtherCost());
						}
												
						selectedReportDaily.setActualVesselCost(thisOperation.getOperationVesselHourlyCost());

						if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CARRY_DAY_COST_FROM_PREV_OP))) {
							String strSqlDaily = "FROM ReportDaily WHERE (isDeleted Is Null OR isDeleted = false) AND reportType != 'DGR' AND dailyUid = :selectedDailyUid AND operationUid = :operationUid";
							String[] paramFieldsDaily = {"selectedDailyUid","operationUid"};
							Object[] paramValuesDaily = {selectedDaily.getDailyUid(), thisOperation.getOperationUid()};
							List<ReportDaily> DailyListCurrentOp = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlDaily,paramFieldsDaily,paramValuesDaily,qp);
				 			 
							if (!DailyListCurrentOp.isEmpty()) { 
					 			Double lastDayCost = DailyListCurrentOp.get(0).getDaycost();
					 			node.getDynaAttr().put("dayCostFromPrevOpLastDay", lastDayCost);
					 			Double lastDayTangibleCost = DailyListCurrentOp.get(0).getDaytangiblecost();
					 			node.getDynaAttr().put("dayTangibleCostFromPrevOpLastDay", lastDayTangibleCost);
					 			Double lastDayAdjustCost = DailyListCurrentOp.get(0).getCostadjust();
					 			node.getDynaAttr().put("costAdjustFromPrevOpLastDay", lastDayAdjustCost);
					 			String lastDayCommentCost = DailyListCurrentOp.get(0).getCommentCost();
					 			node.getDynaAttr().put("commentCostFromPrevOpLastDay", lastDayCommentCost);
					 			Double lastDayCompletionCost = DailyListCurrentOp.get(0).getDaycompletioncost();
					 			node.getDynaAttr().put("completionCostFromPrevOpLastDay", lastDayCompletionCost);
					 			Double lastDayOtherCost = DailyListCurrentOp.get(0).getOtherCost();
					 			node.getDynaAttr().put("otherCostFromPrevOpLastDay", lastDayOtherCost);
					 			Double lastDayTestCost = DailyListCurrentOp.get(0).getDaytestcost();
					 			node.getDynaAttr().put("testCostFromPrevOpLastDay", lastDayTestCost);
							}
						}
					} else {
						reportDaily.setReportNumber("1");
						if (selectedDailyWellbore != null) {
							String strSqlOperationType = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :selectedDailyUid AND reportType = :thisReportType";
							String[] paramsFieldsOperationType = {"selectedDailyUid", "thisReportType"};
							String[] paramsValuesOperationType = {selectedDailyWellbore.getDailyUid(), this.defaultReportType};
							List lstResultOperationType = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlOperationType, paramsFieldsOperationType, paramsValuesOperationType, QueryProperties.useNewSession());
							if (lstResultOperationType != null) {
								ReportDaily lastOperationDaily =  (ReportDaily) lstResultOperationType.get(0);
								String operationTypeReportNumber = lastOperationDaily.getOperationTypeReportNumber();
								if (operationTypeReportNumber != null) {
									try {
										Integer newOperationTypeReportNumber = Integer.parseInt(operationTypeReportNumber);
										newOperationTypeReportNumber++;
										operationTypeReportNumber = newOperationTypeReportNumber.toString();
									} catch (Exception e) {
										operationTypeReportNumber += "a";
									}
									reportDaily.setOperationTypeReportNumber(operationTypeReportNumber);
								} else {
									reportDaily.setOperationTypeReportNumber("1");
								}
							} else {
								reportDaily.setOperationTypeReportNumber("1");
							}
						} else {
							reportDaily.setOperationTypeReportNumber("1");
						}
						reportDaily.setQcFlag(QC_NOT_DONE);
					}
				} else {
					reportDaily.setReportNumber("1");
					if (selectedDailyWellbore != null) {
						String strSqlOperationType = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :selectedDailyUid AND reportType = :thisReportType";
						String[] paramsFieldsOperationType = {"selectedDailyUid", "thisReportType"};
						String[] paramsValuesOperationType = {selectedDailyWellbore.getDailyUid(), this.defaultReportType};
						List lstResultOperationType = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlOperationType, paramsFieldsOperationType, paramsValuesOperationType, QueryProperties.useNewSession());
						if (lstResultOperationType != null) {
							ReportDaily lastOperationDaily =  (ReportDaily) lstResultOperationType.get(0);
							String operationTypeReportNumber = lastOperationDaily.getOperationTypeReportNumber();
							if (operationTypeReportNumber != null) {
								try {
									Integer newOperationTypeReportNumber = Integer.parseInt(operationTypeReportNumber);
									newOperationTypeReportNumber++;
									operationTypeReportNumber = newOperationTypeReportNumber.toString();
								} catch (Exception e) {
									operationTypeReportNumber += "a";
								}
								reportDaily.setOperationTypeReportNumber(operationTypeReportNumber);
							} else {
								reportDaily.setOperationTypeReportNumber("1");
							}
						} else {
							reportDaily.setOperationTypeReportNumber("1");
						}
					} else {
						reportDaily.setOperationTypeReportNumber("1");
					}
					reportDaily.setQcFlag(QC_NOT_DONE);
					
					if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CARRY_DAY_COST_FROM_PREV_OP))) {
						List<ReportDaily> rptDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From ReportDaily " +
								"WHERE (isDeleted Is Null OR isDeleted = false) " +
								"AND reportType != 'DGR' " +
			  	 				"AND operationUid = :operationUid",
			  	 				"operationUid",thisOperation.getOperationUid());
				 		
						// no day exist
						if (rptDailyList.isEmpty()) {
							Well thisWell = ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid());
							String strSqlOps  ="FROM Operation WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND wellUid = :wellUid AND operationUid != :operationUid AND startDate < :newReportDayDate ORDER BY startDate DESC";
							String[] paramFieldsOps = {"wellUid", "operationUid", "newReportDayDate"};
							Object[] paramObjectsOps = {thisWell.getWellUid(), thisOperation.getOperationUid(),thisOperation.getStartDate()};
							List<Operation> opsList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlOps,paramFieldsOps,paramObjectsOps,qp);
						 
							if (!opsList.isEmpty()) {
								Operation lastOps = opsList.get(0);
								String strSqlReportDaily  ="FROM ReportDaily WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND operationUid = :thisOperationUid ORDER BY reportDatetime DESC";
								List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlReportDaily,"thisOperationUid",lastOps.getOperationUid(),qp);
								
								ReportDaily selectedLastReportDaily = null;
								if (!reportDailyList.isEmpty()) {
									selectedLastReportDaily = reportDailyList.get(0); 
								}
					 			if (selectedLastReportDaily != null) {
						 			Double lastReportDailyDayCost = selectedLastReportDaily.getDaycost();
									node.getDynaAttr().put("dayCostFromPrevOpLastDay", lastReportDailyDayCost); 
									Double lastReportDailyTangibleCost = selectedLastReportDaily.getDaytangiblecost();
						 			node.getDynaAttr().put("dayTangibleCostFromPrevOpLastDay", lastReportDailyTangibleCost);
									Double lastReportDailyCostAdjust = selectedLastReportDaily.getCostadjust();
							  		node.getDynaAttr().put("costAdjustFromPrevOpLastDay", lastReportDailyCostAdjust);
									String lastReportDailyCommentCost = selectedLastReportDaily.getCommentCost();
									node.getDynaAttr().put("commentCostFromPrevOpLastDay", lastReportDailyCommentCost);
									Double lastReportDailyCompletionCost = selectedLastReportDaily.getDaycompletioncost();
							  		node.getDynaAttr().put("completionCostFromPrevOpLastDay", lastReportDailyCompletionCost);
							  		Double lastReportDailyOtherCost = selectedLastReportDaily.getOtherCost();
							  		node.getDynaAttr().put("otherCostFromPrevOpLastDay", lastReportDailyOtherCost);
							  		Double lastReportDailyTestCost = selectedLastReportDaily.getDaytestcost();
							  		node.getDynaAttr().put("testCostFromPrevOpLastDay", lastReportDailyTestCost);
					 			}
							} 
						}
					}
					
					//Auto increment the day since lti value by 1 when add new day
					if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_MANUAL_INCREMENT_DAYS_SINCE_LTI_EVENT))) {
						reportDaily.setDaysSinceLta(1.0);
					}
					if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_UPDATE_0600_DEPTH))) {
						if ("DGR".equals(this.defaultReportType)) {							
							if (newReportDayDate != null) {
								String strSql2 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and reportDatetime = :selectedDayDate AND reportType = 'DDR'";							
								String[] paramsFields2 = {"selectedDayDate"};
								Object[] paramsValues2 = {newReportDayDate};
								List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
								
								if (!lstResult2.isEmpty()) {
									ReportDaily selectedDDRReportDaily = (ReportDaily) lstResult2.get(0);
									if (selectedDDRReportDaily.getDepth0600MdMsl() != null) {
										reportDaily.setDepth0600MdMsl(selectedDDRReportDaily.getDepth0600MdMsl());
									} else if (selectedDDRReportDaily.getDepthMdMsl() != null) {
										reportDaily.setDepth0600MdMsl(selectedDDRReportDaily.getDepthMdMsl());									
									}
									if (selectedDDRReportDaily.getDepth0600TvdMsl() != null) {
										reportDaily.setDepth0600TvdMsl(selectedDDRReportDaily.getDepth0600TvdMsl());
									} else if (selectedDDRReportDaily.getDepthTvdMsl() != null) {
										reportDaily.setDepth0600TvdMsl(selectedDDRReportDaily.getDepthTvdMsl());									
									}
								}
							}
						}
					}
					
					if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_MUD_TYPE))) {
						//IF CREATING NEW DGR, it will auto populate mud type from DDR mud properties.
						if ("DGR".equals(this.defaultReportType)) {		
							String mudType = null;
							String[] paramNames5 = {"dailyUid","selectedOperationUid"};
							Object[] paramValues5 = {userSelection.getDailyUid(),thisOperation.getOperationUid()};
							String strSql5 = "FROM MudProperties WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :dailyUid order by reportNumber";
							List lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramNames5, paramValues5);
							if (lstResult5.size() > 0) {
								MudProperties b = (MudProperties) lstResult5.get(0);
								if (b.getMudType() != null) {
									reportDaily.setMudType(b.getMudType());
								}
							} else {
								reportDaily.setMudType(mudType);
							}
						}
					} else {
						if ("DGR".equals(this.defaultReportType)) {		
							String mudType = null;
							reportDaily.setMudType(mudType);
						}
					}
					if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_FORMATION_NAME))) {
						//IF CREATING NEW DGR REPORT, IT WILL GET THE FORMATION NAME FROM DGR FORMATION DATA SCREEN
						if ("DGR".equals(this.defaultReportType)) {	
							String FormationName = null;
							Double depthMdMsl = null;
							List<ReportDaily> lstResult6 =  null;
							if (this.wellboreBased) {
								String[] paramNames6 = {"dailyUid","selectedOperationUid","selectedWellboreUid"};
								Object[] paramValues6 = {userSelection.getDailyUid(),thisOperation.getOperationUid(), thisOperation.getWellboreUid()};
								String strSql6 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND wellboreUid = :selectedWellboreUid AND dailyUid = :dailyUid AND reportType = 'DDR'";
								lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramNames6, paramValues6);
						    } else {
								String[] paramNames6 = {"dailyUid","selectedOperationUid"};
								Object[] paramValues6 = {userSelection.getDailyUid(),thisOperation.getOperationUid()};
								String strSql6 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :dailyUid AND reportType = 'DDR'";
								lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramNames6, paramValues6);
							}
							
							if (lstResult6.size() > 0) {
								ReportDaily selectedDDRReportDaily = lstResult6.get(0);
								if (selectedDDRReportDaily.getDepthMdMsl() != null) {
									depthMdMsl = Double.parseDouble(selectedDDRReportDaily.getDepthMdMsl().toString());
									List lstResult7 = null;
									if (this.wellboreBased) {
										String[] paramNames7 = {"midnightDepth","selectedOperationUid","selectedWellboreUid"};
										Object[] paramValues7 = {depthMdMsl,thisOperation.getOperationUid(),thisOperation.getWellboreUid()};
										String strSql7 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND wellboreUid = :selectedWellboreUid AND sampleTopMdMsl <= :midnightDepth order by sampleTopMdMsl desc";
										lstResult7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7, paramNames7, paramValues7);
								    } else {
										String[] paramNames7 = {"midnightDepth","selectedOperationUid"};
										Object[] paramValues7 = {depthMdMsl,thisOperation.getOperationUid()};
										String strSql7 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND sampleTopMdMsl <= :midnightDepth order by sampleTopMdMsl desc";
										lstResult7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7, paramNames7, paramValues7);
								    }
									if (lstResult7.size() > 0) {
										Formation c = (Formation) lstResult7.get(0);
										if (c.getFormationName() != null) {
											reportDaily.setFormation(c.getFormationName());
										}
									} else {
										reportDaily.setFormation(FormationName);
									}
								}
							}
						}
					} else {
						if ("DGR".equals(this.defaultReportType)) {
							String FormationName = null;
							reportDaily.setFormation(FormationName);
						}
					}
				}
				
				//31560
				if ("DGR".equalsIgnoreCase(reportDaily.getReportType())) {
					Date newDayDate = CommonUtil.getConfiguredInstance().getTodayReportDate(thisOperation.getOperationUid(), userSelection.getDailyUid(), this.defaultReportType, userSelection);
					if (newDayDate != null) {
						String strSql2 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND reportDatetime = :selectedDayDate AND reportType = 'DDR'";
						String[] paramsFields2 = {"selectedDayDate","selectedOperationUid"};
						Object[] paramsValues2 = {newDayDate,thisOperation.getOperationUid()};
						List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
						if (!lstResult2.isEmpty()) {
							ReportDaily selectedDDRReportDaily = (ReportDaily) lstResult2.get(0);
							reportDaily.setRigInformationUid(selectedDDRReportDaily.getRigInformationUid());
							node.getDynaAttr().put("ddrExist", "1");
						} else {
							node.getDynaAttr().put("ddrExist", "0");
						}
					} else {
						node.getDynaAttr().put("ddrExist", "0");
					}
				} else if ("DDR".equalsIgnoreCase(reportDaily.getReportType())) {
					node.getDynaAttr().put("ddrExist", "1");
				}
				
				//17935 auto set GeoNet's Daily Morning Update Day Date to newly created report day date + 1
				//From Specs: Criteria for GWP will be to only enable for client where "Date Time" field under Morning Update without Time selector. Only enable for those with date selector. Check before enabling the GWP.
				if ("DGR".equals(this.defaultReportType) && newReportDayDate != null) {
					reportDaily.setCutOffDateTime(this.getCutOffDateTime(userSelection, GroupWidePreference.GWP_AUTO_POPULATE_MORNING_UPDATE_DATE, newReportDayDate, this.customCutOffTime));
					reportDaily.setDailyReportStartDatetime(this.getDailyReportStartDateTime(userSelection, GroupWidePreference.GWP_DEFAULT_REPORT_START_TIME, newReportDayDate, this.customDailyReportStartTime));
					reportDaily.setDailyReportEndDatetime(this.getDailyReportEndDateTime(userSelection, GroupWidePreference.GWP_DEFAULT_REPORT_END_TIME, newReportDayDate, this.customDailyReportEndTime, this.isDailyReportEndDateOnNextDay));
				}

				//get date for the newly created record
				Date newDayDate = CommonUtil.getConfiguredInstance().getTodayReportDate(thisOperation.getOperationUid(), userSelection.getDailyUid(), this.defaultReportType, userSelection);
				node.getDynaAttr().put("daydate", newDayDate);

				//31560
				if (!"DGR".equalsIgnoreCase(reportDaily.getReportType())) {
					// default rig used
					String rigInformationUid = ApplicationUtils.getConfiguredInstance().getRigInformationUid(thisOperation.getOperationUid(), null);
					if (rigInformationUid != null) {
						//node.getDynaField().getValues().put("rigid", rigInformationUid);
						reportDaily.setRigInformationUid(rigInformationUid);
					}									
				}
				
				if (("DGR".equalsIgnoreCase(this.defaultReportType)) || ("CMPLT".equalsIgnoreCase(this.defaultReportType))) {
					ReportDailyUtils.getDrillerInfo(node, userSelection.getOperationUid(), userSelection.getGroupUid(), (Date) node.getDynaAttr().get("daydate"), commandBean, autoCalculateDepthTvdSs);
				}
			
				ReportDailyUtils.getOperationAfeAmt(node, userSelection.getOperationUid());
				
				if (reportDaily.getEfcOtherCost() == null) {
					reportDaily.setEfcOtherCost(thisOperation.getAfeOtherCost());
				}
				
				reportDaily.setActualVesselCost(thisOperation.getOperationVesselHourlyCost());
				
				// Show dynamic label for existing field: "24hr. Summary" based on selected operation hours 
				// get operationHour from Operation
				if (thisOperation.getOperationHour()!=null) {
					node.getDynaAttr().put("operationHour", thisOperation.getOperationHour());
				}
				
				Boolean excludeCopyDataFilters = ReportDailyUtils.showCopyDataFilters(thisOperation.getGroupUid(), thisOperation.getOperationCode()) ;
		 		node.getDynaAttr().put("excludeCopyDataFilters", excludeCopyDataFilters);
			}
		}
	}

		
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		String wellExplorerOperationUid	= null;
		if (request != null) wellExplorerOperationUid = request.getParameter(WellExplorerConstant.OPERATION_UID);
		
		if (StringUtils.isNotBlank(wellExplorerOperationUid)) {
			customCondition = "operationUid = :customFilterOperationUid";
			query.addParam("customFilterOperationUid", wellExplorerOperationUid);
		} else {
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(userSelection.getOperationUid());
			//customCondition = "dailyUid = :customFilterDailyUid AND reportType = 'DDR'";
			
			//Generate FWR, load current operation and report daily with reportType=(reportTypePriority)
			if ("FWR".equalsIgnoreCase(this.reportGenerated)) {				
				customCondition = "operationUid = :customFilterOperationUid AND reportType = :reportType";
				query.addParam("customFilterOperationUid", userSelection.getOperationUid());
				query.addParam("reportType", reportType);			
			} else {				
				customCondition = "dailyUid = :customFilterDailyUid AND reportType = :reportType";
				query.addParam("customFilterDailyUid", userSelection.getDailyUid());
				query.addParam("reportType", reportType);				
			}
		}
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean arg0, UserSelectionSnapshot arg1, HttpServletRequest arg2, TreeModelDataDefinitionMeta arg3, DataDefinitionHQLQuery arg4, CommandBeanTreeNode arg5) throws Exception {
		return null;
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
	private Date stampTimeToDate(Date date, String timeStamp) throws Exception {
		if (date != null && StringUtils.isNotBlank(timeStamp)) {
			String[] timeArray = timeStamp.split(":");
			int len = timeArray.length;
			for (int i = 0; i < len; i++) {
				String time = timeArray[i];
				if (i == 0) {
					int hour = Integer.parseInt(time == null ? "0" : time);
					if (hour == 24) {
						date = DateUtils.addHours(date, 23);
						date = DateUtils.addMinutes(date, 59);
						date = DateUtils.addSeconds(date, 59);
						break;
					} else {
						date = DateUtils.addHours(date, hour);
					}
				}
				if (i == 1) {
					int min = Integer.parseInt(time == null ? "0" : time);
					date = DateUtils.addMinutes(date, min);
				}
			}
		}
		return date;
	}
	
	private Date getCutOffDateTime(UserSelectionSnapshot userSelection, String gwpName, Date dayDate, String customTimeStamp) throws Exception{
		Date cutOffDateTime = null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(dayDate);
		cal.add(Calendar.DAY_OF_YEAR, 1);

		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), gwpName))) {
			cutOffDateTime =  cal.getTime();
		}
		
		Well currentWell = ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid());
		if (currentWell != null) {
			String country = currentWell.getCountry();
			if (StringUtils.isNotBlank(country) && country.equalsIgnoreCase("NO")) {
				cutOffDateTime = this.stampTimeToDate(cal.getTime(), customTimeStamp);
			}
		}
		return cutOffDateTime;
	}
	
	private Date getDailyReportEndDateTime(UserSelectionSnapshot userSelection, String gwpName, Date daydate, String customTimeStamp, Boolean isNextDay) throws Exception{
		Date endDate = daydate;
		if (endDate != null) {
			String country = null;
			Well currentWell = ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid());
			
			if (currentWell != null) {
				country = currentWell.getCountry();
			}
			
			if (StringUtils.isNotBlank(country) && country.equalsIgnoreCase("NO")) {
				endDate = this.stampTimeToDate(endDate, customTimeStamp);
				if (isNextDay) {
					endDate = DateUtils.addDays(endDate, 1);
				}
			} else {
				endDate = DateUtils.addDays(endDate, 1);
				endDate = this.stampTimeToDate(endDate, GroupWidePreference.getValue(userSelection.getGroupUid(), gwpName));
			}
		}
		return endDate;
	}
	
	
	private Date getDailyReportStartDateTime(UserSelectionSnapshot userSelection, String gwpName, Date daydate, String customTimeStamp) throws Exception{
		Date defaultDateTime = daydate;
		try {
			String country = null;
			Well currentWell = ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid());
			if (currentWell != null) {
				country = currentWell.getCountry();
			}
			
			if (StringUtils.isNotBlank(country) && country.equalsIgnoreCase("NO")) {
				defaultDateTime = this.stampTimeToDate(defaultDateTime, customTimeStamp);
			} else {
				defaultDateTime = this.stampTimeToDate(defaultDateTime, GroupWidePreference.getValue(userSelection.getGroupUid(), gwpName));
			}

			if (defaultDateTime != null) {
				return defaultDateTime;
			}
		} catch (Exception e) {
		}
		return defaultDateTime;
	}
	
	private void setNullToPropertiesOnStartNewDay(ReportDaily reportDaily, UserSelectionSnapshot userSelection) throws Exception {
		StartNewDaySetting sett = StartNewDaySetting.getConfiguredInstance();
		if (sett.getPropertyToSetNullOnStartNewDay() != null && sett.getPropertyToSetNullOnStartNewDay().size() > 0) {
			for (String prop : sett.getPropertyToSetNullOnStartNewDay()) {
				if (PropertyUtils.isWriteable(reportDaily, prop))
					PropertyUtils.setProperty(reportDaily, prop, null);
			}
		}
		if (sett.getPropertyToSetNullWhenGWPOnAndOnStartNewDay() != null && sett.getPropertyToSetNullWhenGWPOnAndOnStartNewDay().size() > 0) {
			for (Map.Entry<String, String> entry : sett.getPropertyToSetNullWhenGWPOnAndOnStartNewDay().entrySet()) {
				String gwpKey = entry.getKey();
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), gwpKey))) {
					String props = entry.getValue();
					if (StringUtils.isNotBlank(props)) {
						if (props.contains(",")) {
							for (String prop : props.split(",")) {
								if (PropertyUtils.isWriteable(reportDaily, prop))
									PropertyUtils.setProperty(reportDaily, prop, null);
							}
						}
					} else {
						if (PropertyUtils.isWriteable(reportDaily, props))
							PropertyUtils.setProperty(reportDaily, props, null);
					}
				}
			}
		}
	}
	
	private double getPOBHoursToDate(String groupUid, String operationUid, Date userDate) throws Exception {
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
				
		Double totalManHours = 0.0;
		
		String strSql = "SELECT SUM(p.pax) FROM Daily d, PersonnelOnSite p " +
		"WHERE (d.isDeleted = false or d.isDeleted is null) " +
		"and (p.isDeleted = false or p.isDeleted is null) " +
		"and p.dailyUid = d.dailyUid " +
		"AND d.dayDate <= :userDate " +
		"AND d.operationUid = :thisOperationUid ";
		
		String[] paramsFields = {"userDate", "thisOperationUid"};
		Object[] paramsValues = {userDate, operationUid};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Object a = lstResult.get(0);
		if (a != null) {
			Double totalPax = 0.0;
			totalPax = Double.parseDouble(a.toString());
			Double defaultHours = Double.parseDouble(GroupWidePreference.getValue(groupUid, "pobDefaultWorkinghours"));
			return (totalPax * defaultHours) / 24;
		}
		return totalManHours;		
	}
	
	 public static void autoLookupLastLinerSection(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception {
        Object object = node.getData();
        if (object instanceof ReportDaily) {
            Object objResult;
            ReportDaily thisReport = (ReportDaily)object;
            thisReport.setSysReportDailySubclass("well");
            if (RIG_SIDE_QC_DONE.equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoLookupLastLinerSection")) && (objResult = CommonUtil.getConfiguredInstance().lastLinerSection(session.getCurrentGroupUid(), session.getCurrentWellboreUid(), session.getCurrentDailyUid())) != null && objResult instanceof CasingSection) {
                CasingSection thisCasingSection = (CasingSection)objResult;
                CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "last_liner_od");
                if (thisCasingSection.getCasingOd() != null) {
                    thisConverter.setBaseValue(thisCasingSection.getCasingOd().doubleValue());
                    thisReport.setLastLinerOd(Double.valueOf(thisConverter.getConvertedValue()));
                }
                if (thisCasingSection.getLinerTopDepthMdMsl() != null) {
                    thisConverter.setReferenceMappingField(ReportDaily.class, "last_liner_top_md_msl");
                    thisConverter.setBaseValue(thisCasingSection.getLinerTopDepthMdMsl().doubleValue());
                    thisReport.setLastLinerTopMdMsl(Double.valueOf(thisConverter.getConvertedValue()));
                }
                if (thisCasingSection.getLinerTopDepthTvdMsl() != null) {
                    thisConverter.setReferenceMappingField(ReportDaily.class, "last_liner_top_tvd_msl");
                    thisConverter.setBaseValue(thisCasingSection.getLinerTopDepthTvdMsl().doubleValue());
                    thisReport.setLastLinerTopTvdMsl(Double.valueOf(thisConverter.getConvertedValue()));
                }
                if (thisCasingSection.getShoeTopMdMsl() != null) {
                    thisConverter.setReferenceMappingField(ReportDaily.class, "last_liner_md_msl");
                    thisConverter.setBaseValue(thisCasingSection.getShoeTopMdMsl().doubleValue());
                    thisReport.setLastLinerMdMsl(Double.valueOf(thisConverter.getConvertedValue()));
                }
                if (thisCasingSection.getShoeTopMdMsl() != null) {
                    thisConverter.setReferenceMappingField(ReportDaily.class, "last_liner_tvd_msl");
                    thisConverter.setBaseValue(thisCasingSection.getShoeTopTvdMsl().doubleValue());
                    thisReport.setLastLinerTvdMsl(Double.valueOf(thisConverter.getConvertedValue()));
                }
            }
        }
	 }
	 
	public static void sumDailyHseObservationCards(CommandBeanTreeNode node, ReportDaily reportDaily) throws Exception {
		//22086 - Sum of daily safe and unsafe observations cards from HSE Incident	
		String query = "SELECT SUM(numberOfIncidents) FROM HseIncident "
				+ "WHERE (incidentCategory='Un-safe Observations Cards' OR incidentCategory='Safe Observations Cards') "
				+ "AND dailyUid=:dailyUid "
				+ "AND (isDeleted = 0 OR isDeleted IS NULL)";
		List queryresult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, "dailyUid", reportDaily.getDailyUid());
		
		if (queryresult.size() > 0) {
			node.getDynaAttr().put("sumDailyHseObservationCards", queryresult.get(0));
		}
	 }
	
	public static void getLastBOPLogNewInputMode(UserSelectionSnapshot userSelection, ReportDaily selectedReportDaily) throws Exception {
		//20057 - auto lookup last BOP for current rig for new input mode
		if(selectedReportDaily.getReportDatetime() != null) {
			Date newDate = DateUtils.addDays(selectedReportDaily.getReportDatetime(), 1); //current date + 1 day for new report daily
		
			String strSql6 = "SELECT bl FROM Bop b, BopLog bl WHERE (b.bopUid= bl.bopUid) and (b.isDeleted = false or b.isDeleted is null) and (bl.isDeleted = false or bl.isDeleted is null) and b.rigInformationUid = :currentRigInformationUid AND bl.testingDatetime <= :newcurrentDate ORDER BY bl.testingDatetime DESC";
			String[] paramsFields6 = {"currentRigInformationUid", "newcurrentDate"};
			Object[] paramsValues6 = {userSelection.getRigInformationUid(), newDate};			
			List lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramsFields6, paramsValues6);
			
			if (lstResult6.size() > 0){	
				BopLog thisBopLog = (BopLog) lstResult6.get(0);
				if (thisBopLog != null) {
					selectedReportDaily.setLastbopDate(thisBopLog.getTestingDatetime());
					selectedReportDaily.setNextBopDate(thisBopLog.getNextTestDatetime());
				}
			}
			else {
				selectedReportDaily.setLastbopDate(null);
				selectedReportDaily.setNextBopDate(null);
			}
		}
		//Days Since Last BOP auto populate on edit/add
		/*
		 * if(selectedReportDaily.getLastbopDate() != null) { //ticket - 15395 //check
		 * Last BOP Date is empty/null double daysSinceLastbop =
		 * CommonUtil.getConfiguredInstance().calculateDaysSinceLastBOP(newDate,
		 * selectedReportDaily.getLastbopDate()); //increment +1 because its next day's
		 * CustomFieldUom thisConverter = new CustomFieldUom(commandBean,
		 * ReportDaily.class, "days_since_lta"); //get the field mapping from the day
		 * since lta and assign to days since last bop
		 * thisConverter.setBaseValue(daysSinceLastbop); if
		 * (thisConverter.isUOMMappingAvailable()) {
		 * node.setCustomUOM("@daysSinceLastbop", thisConverter.getUOMMapping()); }
		 * node.getDynaAttr().put("daysSinceLastbop",
		 * thisConverter.getConvertedValue()); }
		 */
	}
	
	public static void populateAndUpdateLastLTINewInputMode(UserSelectionSnapshot userSelection, ReportDaily selectedReportDaily) throws Exception{
		//auto lookup last LTI for current rig for new first day
		if(selectedReportDaily.getReportDatetime() != null) {
			Date newDate = DateUtils.addDays(selectedReportDaily.getReportDatetime(), 1); //current date + 1 day for new report daily
		
			String includeHistoricalName = "";
			if(userSelection.getRigInformationUid()!=null){		
				String strSql = "select historicalName from RigInformation where (isDeleted = false or isDeleted is null) and rigInformationUid = :rigInformationUid";
				List<String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigInformationUid", userSelection.getRigInformationUid());
				if (lstResult.size()>0) {
					String historicalName = lstResult.get(0);
					if (StringUtils.isNotBlank(historicalName)) {
						includeHistoricalName = " OR rigInformationUid='" + historicalName + "'";
					}			
				}
			}
			
			String strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) and (rigInformationUid = :currentRigInformationUid" + includeHistoricalName + ") AND hseEventdatetime <= :currentDate AND isLostTimeIncident = 1 and (supportVesselInformationUid is null or supportVesselInformationUid = '') ORDER BY hseEventdatetime DESC";
			String[] paramsFields2 = {"currentRigInformationUid", "currentDate"};
			Object[] paramsValues2 = {userSelection.getRigInformationUid(), newDate};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
			
			if (lstResult.size() > 0){
				HseIncident lastLTI = (HseIncident) lstResult.get(0);
				selectedReportDaily.setLastLtiEventname(lastLTI.getIncidentCategory());
				selectedReportDaily.setLastLtiDate(lastLTI.getHseEventdatetime());
			}
		}
	}
}
