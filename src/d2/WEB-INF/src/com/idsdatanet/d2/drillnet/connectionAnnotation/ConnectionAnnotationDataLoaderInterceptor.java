package com.idsdatanet.d2.drillnet.connectionAnnotation;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ConnectionAnnotationDataLoaderInterceptor implements DataLoaderInterceptor {
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		if("RigStateRaw".equals(meta.getTableClass().getSimpleName())) {
			String customCondition = "operationUid=:operationUid AND (startTime>=:startDate AND endTime<:endDate) AND internalRigState IN ";
			String connectionRigStateFilter  = (String) commandBean.getRoot().getDynaAttr().get("connectionRigStateFilter");
			
			Daily todayDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			
			if (todayDaily == null) return "NULL IS NULL";
			Date startDate = todayDaily.getDayDate();
			Date endDate = DateUtils.addDays(startDate, 1);
			
			//NOT ALLOW THE FOLLOWING FIELD TO BE BLANK, SET DEFAULT FILTERING
			if (StringUtils.isBlank(connectionRigStateFilter)) {
				connectionRigStateFilter = "connection_type";
				commandBean.getRoot().getDynaAttr().put("connectionRigStateFilter", connectionRigStateFilter);
			}
			
			if ("connection_type".equalsIgnoreCase(connectionRigStateFilter)) {
				customCondition += "('22','28','31','38')";
			} else {
				customCondition += "('40','36','41')";
			}
			
			query.addParam("operationUid", userSelection.getOperationUid());
			query.addParam("startDate", startDate);
			query.addParam("endDate", endDate);
			
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		return null;
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
}
