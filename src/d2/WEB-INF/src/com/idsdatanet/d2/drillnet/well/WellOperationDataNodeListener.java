package com.idsdatanet.d2.drillnet.well;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dynamicfield.DynamicFieldUtil;
import com.idsdatanet.d2.core.dynamicfield.DynamicFieldsImpl;
import com.idsdatanet.d2.core.model.Basin;
import com.idsdatanet.d2.core.model.Campaign;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DynamicFieldMeta;
import com.idsdatanet.d2.core.model.GloryHole;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.Platform;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.TargetZone;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.stt.RFTActionHandler;
import com.idsdatanet.d2.core.stt.STTActionHandler;
import com.idsdatanet.d2.core.stt.STTComConfig;
import com.idsdatanet.d2.core.stt.STTJobConfig;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;
import com.idsdatanet.d2.drillnet.formation.FormationUtils;
import com.idsdatanet.d2.drillnet.operation.OperationUtils;

/**
 * 
 * @author zjong
 *
 */
public class WellOperationDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor, com.idsdatanet.d2.core.web.mvc.ActionManager {
	

	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private String tournetDailyRedirect = null;
	
	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		Object object = node.getData();
		if (object instanceof Operation) {
			Operation operation = (Operation)object;
			setDynaAttr(node, "wellName", operation.getOperationName());
			
			if (isDrilling(operation)) {
				Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
				setDynaAttr(node, "parentWellboreUid", wellbore.getParentWellboreUid());
			} else {
				setDynaAttr(node, "parentWellboreUid", operation.getWellboreUid());
			}
			
			// well standard fields
			Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, operation.getWellUid());
			Map<String, Object> wellFieldValues = PropertyUtils.describe(well);
			for (Map.Entry entry : wellFieldValues.entrySet()) {
				setDynaAttr(node, "well." + entry.getKey(), entry.getValue());
				if ("gloryHoleUid".equals(entry.getKey())) {
					String gloryHoleUid = (String) entry.getValue();
					if (StringUtils.isNotBlank(gloryHoleUid)) {
						GloryHole gloryHole = (GloryHole) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(GloryHole.class, gloryHoleUid);
						if (gloryHole!=null) {
							Map<String, Object> gloryHoleFieldValues = PropertyUtils.describe(gloryHole);
							for (Map.Entry gloryHoleFieldEntry : gloryHoleFieldValues.entrySet()) {
								String fieldName = gloryHoleFieldEntry.getKey().toString();
								CustomFieldUom thisConverter = new CustomFieldUom(commandBean, GloryHole.class, fieldName);
								if (thisConverter.isUOMMappingAvailable()){
									if (gloryHoleFieldEntry.getValue()!=null) {
										thisConverter.setBaseValue((Double)gloryHoleFieldEntry.getValue());
										setDynaAttr(node, "gloryHole." + fieldName, thisConverter.getFormattedValue());
									}
								} else {
									setDynaAttr(node, "gloryHole." + fieldName, gloryHoleFieldEntry.getValue());
								}
							}
						}
					}
				}
			}
			
			// well dynamic fields
			Map<String, Object> wellDynamicFieldValues = DynamicFieldUtil.getFieldValues(userSelection.getGroupUid(), well);
			Map<String, DynamicFieldMeta> wellDynamicFieldMetaMap = DynamicFieldUtil.getConfiguredInstance().getDynamicFieldMeta(Well.class, userSelection.getGroupUid());
			for (Iterator i = wellDynamicFieldMetaMap.keySet().iterator(); i.hasNext(); ) {
				String fieldName = (String) i.next();
				if (wellDynamicFieldValues.containsKey(fieldName)) {
					setDynaAttr(node, "well." + fieldName, wellDynamicFieldValues.get(fieldName));
				}
			}
			
			// wellbore standard fields
			Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
			Map<String, Object> wellboreFieldValues = PropertyUtils.describe(wellbore);
			for (Map.Entry entry : wellboreFieldValues.entrySet()) {
				setDynaAttr(node, "wellbore." + entry.getKey(), entry.getValue());
			}
			
			// wellbore dynamic fields
			Map<String, Object> wellboreDynamicFieldValues = DynamicFieldUtil.getFieldValues(userSelection.getGroupUid(), wellbore);
			Map<String, DynamicFieldMeta> wellboreDynamicFieldMetaMap = DynamicFieldUtil.getConfiguredInstance().getDynamicFieldMeta(Wellbore.class, userSelection.getGroupUid());
			for (Iterator i = wellboreDynamicFieldMetaMap.keySet().iterator(); i.hasNext(); ) {
				String fieldName = (String) i.next();
				if (wellboreDynamicFieldValues.containsKey(fieldName)) {
					setDynaAttr(node, "wellbore." + fieldName, wellboreDynamicFieldValues.get(fieldName));
				}
			}
			
			// Rig Information standard fields jwong
			if(StringUtils.isNotBlank(operation.getRigInformationUid())){
				RigInformation rigInformation = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, operation.getRigInformationUid());
				if(rigInformation != null){
					Map<String, Object> RigInformationFieldValues = PropertyUtils.describe(rigInformation);
					for (Map.Entry entry : RigInformationFieldValues.entrySet()) {
						setDynaAttr(node, "rigInformation." + entry.getKey(), entry.getValue());
					}
				}
			}
			
			OperationUtils.setDvdPlanName(node, operation);
			
			//Check LookAhead Plan Data
			String strSql = "from OperationPlanTask where (isDeleted = false or isDeleted is null) and operationUid = :thisOperationUid";
			String[] paramsFields2 = {"thisOperationUid"};
			Object[] paramsValues2 = {operation.getOperationUid()};
			
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
			
			if (lstResult2.size() > 0){
				String lookaheadPlanName = operation.getOperationName() + " Lookahead Plan";
				setDynaAttr(node, "lookaheadPlan", lookaheadPlanName);
			}
			
			/*List<OperationPlanMaster> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationPlanMaster WHERE (isDeleted = false OR isDeleted is null) and dvdPlanStatus=true AND operationUid = :operationUid", "operationUid", operation.getOperationUid());
			if (!results.isEmpty()) {
				OperationPlanMaster operationPlanMaster = results.get(0);
				setDynaAttr(node, "planname", operationPlanMaster.getDvdPlanName());
			}*/
	
			//showCustomUOM(node, userSelection.getGroupUid());
			
			OperationUtils.setDatumRelatedContentOnLoad(node, userSelection);
			OperationUtils.setDefaultDatumReferencePoint(node);
			
			OperationUtils.setAfeAllowEditFlag(node);
			
			OperationUtils.setAllowChangeOperationType(node);
			
			// opsTeams is defined as a required field, but while in edit mode,
			// the field is hidden and the value will not be used, so this is to
			// trick the validator that it has a value
			setDynaAttr(node, "opsTeams", "_empty_");			
			
			
			OperationUtils.calculateCostRelatedContent(node, false,false);
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Operation.class, "plannedDuration");
			
			Double operationBudget = null;
			Double vesselHourlyRate = operation.getOperationVesselHourlyCost();
			Double plannedDuration = operation.getPlannedDuration();
			if (vesselHourlyRate!=null && plannedDuration!=null) {
				thisConverter.setReferenceMappingField(Operation.class, "plannedDuration");
				thisConverter.setBaseValueFromUserValue(plannedDuration);
				Double plannedDay = thisConverter.getConvertedValue();
				operation.setPlannedVesselCost(plannedDay * vesselHourlyRate);
				operationBudget = plannedDay * vesselHourlyRate;
			} else {
				operation.setPlannedVesselCost(null);
			}
			
			if (operation.getAfeOtherCost()!=null) {
				if (operationBudget==null) operationBudget = 0.0;
				operationBudget += operation.getAfeOtherCost();
			}
			if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoCalcSubseaOperationBudget"))) {
				operation.setOperationBudget(operationBudget);
			}
			
			OperationUtils.setLockOperationFlag(node);
			
			OperationUtils.setCanManageTightHoleUserFlag(node, request);
			//The setCanSetTightHoleFlag function is for getting the canSetTightHole value from user screen so that this value can be used for checking in well operation screen 
			//for determine whether the Is Tight Hole field is editable or not
			OperationUtils.setCanSetTightHoleFlag(node, request);
			
			if (operation.getRigInformationUid()!=null){
				List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select comment from RigInformation where (isDeleted=false or isDeleted is null) and rigInformationUid=:rigInformationUid", "rigInformationUid", operation.getRigInformationUid());
				if (rs != null && !rs.isEmpty()) {
					try {
						if (rs.get(0) != null) {
								setDynaAttr(node, "rigType", rs.get(0).toString());
						}
					} catch (Exception e) {
					}
				}
			}	
			
			OperationUtils.setActualCosts(commandBean, node);
		}
	}
	
	
	
	private boolean isDrilling(Operation operation) {
		return operation.getOperationCode().startsWith(Constant.DRLLG); // eg. DRLLG, DRLLG_NEWCODE (woodside_au_off)
	}
	
	private boolean isNotDrilling(Operation operation) {
		return !isDrilling(operation);
	}
	
	@Override
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {

		Object object = node.getData();
		if (object instanceof Operation) {
			Operation operation = (Operation) object;
			
			if (operation.getOperationCode() != null){
				String opCode = operation.getOperationCode();
				String relatedOpCode = OperationUtils.getMobilizationRelatedOpCode(opCode);
				if (relatedOpCode !=null){
					node.getDynaAttr().put("reportingDatumOffset", null);
				}
			}
			
			String parentWellboreUid = getDynaAttr(node, "parentWellboreUid");
			String rigName = getDynaAttr(node, "rigName");
			String basinName = getDynaAttr(node, "name");
			String platformName = getDynaAttr(node, "platformName");
			String reportingDatumOffset = getDynaAttr(node, "reportingDatumOffset");
			String datumType = getDynaAttr(node, "datumType");
			String campaignName = getDynaAttr(node, "campaignName");
			String offsetMsl = getDynaAttr(node, "offsetMsl");
			String entryType = (String) node.getDynaAttr().get("entryType");
			String slantAngle = (String) node.getDynaAttr().get("slantAngle");
			String uwi = getDynaAttr(node, "well.uwi");
			String wellUid = getDynaAttr(node, "well.wellUid");
			
			Boolean isGlReference = Boolean.parseBoolean(CommonUtils.null2EmptyString(node.getDynaAttr().get("isGlReference")));
			
			Double dblSlantAngle = CommonUtil.getConfiguredInstance().toDouble(slantAngle, commandBean.getUserLocale());
			Double dblSurfaceSlantLength = 0.0;
			
			operation.setSysOperationSubclass("well");
			
			if (StringUtils.isNotBlank(reportingDatumOffset) && StringUtils.isNotBlank(slantAngle) && "1".equals(entryType)) {
				Double reportingDatumOffsetRaw = 0.0;
				Double slantAngleRaw = 0.0;
				Double dblReportingDatumOffset = CommonUtil.getConfiguredInstance().toDouble(reportingDatumOffset, commandBean.getUserLocale());
				
				CustomFieldUom thisConverterField = new CustomFieldUom(commandBean, OpsDatum.class, "reportingDatumOffset");
				thisConverterField.setBaseValueFromUserValue(dblReportingDatumOffset);
				reportingDatumOffsetRaw = thisConverterField.getBasevalue();
				
				thisConverterField.setReferenceMappingField(OpsDatum.class, "slantAngle");
				thisConverterField.setBaseValueFromUserValue(dblSlantAngle);
				slantAngleRaw = thisConverterField.getBasevalue();
				
				//check angle unit is it in degree or radian, if in degree need to convert to radian for calculation
				
				double convertionFactor = 1.0;
				double thisSlantAngle;
				
				if ("Degree".equals(thisConverterField.getUomLabel())) convertionFactor = 1 / 57.2957549575;
				else if ("Gradian".equals(thisConverterField.getUomLabel())) convertionFactor = 0.015707963;
				
				thisSlantAngle = slantAngleRaw * convertionFactor;
				dblSurfaceSlantLength = reportingDatumOffsetRaw / Math.cos(thisSlantAngle);
				
				thisConverterField.setReferenceMappingField(OpsDatum.class, "surfaceSlantLength");			
				
				thisConverterField.setBaseValue(dblSurfaceSlantLength);
				dblSurfaceSlantLength = thisConverterField.getConvertedValue();
				
			}
			
			if (StringUtils.isBlank(operation.getOperationUid())) {
				operation.setOperationUid(null); // flex client will set operationUid to empty string after submitForServerSideProcess because it is being implicitly declared by the filter, so set it back to null here
				operation.setIsLocked(null); // this operation could be copied from a locked operation, but a new operation should not be locked
			}
			
			// validate as much as possible here, if failed, return immediately, before anything get created below
			if (StringUtils.isNotBlank(rigName)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from RigInformation where (isDeleted = false or isDeleted is null) and rigName = :rigName", "rigName", rigName);
				if (list != null && list.size() > 0) {
					status.setDynamicAttributeError(node, "rigName", "The rig '" + rigName + "' already exists in database");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (StringUtils.isNotBlank(campaignName)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Campaign where (isDeleted = false or isDeleted is null) and campaignName = :campaignName", "campaignName", campaignName);
				if (list != null && list.size() > 0) {
					status.setDynamicAttributeError(node, "campaignName", "The campaign '" + campaignName + "' already exists in database");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (StringUtils.isNotBlank(basinName)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Basin where (isDeleted = false or isDeleted is null) and name = :name", "name", basinName);
				if (list != null && list.size() > 0) {
					status.setDynamicAttributeError(node, "name", "The basin '" + basinName + "' already exists in database");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (StringUtils.isNotBlank(platformName)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Platform where (isDeleted = false or isDeleted is null) and platformName = :platformName", "platformName", platformName);
				if (list != null && list.size() > 0) {
					status.setDynamicAttributeError(node, "platformName", "The platform '" + platformName + "' already exists in database");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (StringUtils.isNotBlank(uwi)) {
				List<Well> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Well where (isDeleted = false or isDeleted is null) and uwi = :uwi", "uwi", uwi);
				if (list != null && list.size() > 0) {
					for (Well w: list){
						if  (!w.getWellUid().equals(wellUid)){
							status.setDynamicAttributeError(node, "well.uwi", "The UWI '" + uwi + "' already exists in database");
							status.setContinueProcess(false, true);
							return;
						}
					}
				}
			}
			if (StringUtils.isNotBlank(operation.getOperationUid())) { // when editing existing record
				if (isNotDrilling(operation) && StringUtils.isBlank(parentWellboreUid)) {
					// if this is not a drilling operation, parent well must be selected
					String operationCodelabel = OperationUtils.getOperationCodeLabel(commandBean, node, operation.getOperationCode());
					status.addError(operationCodelabel + " operation requires an <strong>associated borehole</strong> to be selected.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (DefaultDatumLookupHandler.MSL.equals(operation.getDefaultDatumUid())) {
				operation.setDefaultDatumUid(null);
				operation.setzRtToGround(null);
			}
			
			if (StringUtils.isBlank(operation.getOperationUid())) {
				// when adding a new operation
				if (StringUtils.isNotBlank(reportingDatumOffset)) {
					// and user choose to create a new datum
					CustomFieldUom datumConverter = new CustomFieldUom(commandBean, OpsDatum.class, "reportingDatumOffset");
					datumConverter.setBaseValueFromUserValue(CommonUtil.getConfiguredInstance().toDouble(reportingDatumOffset, commandBean.getUserLocale()), false);
					Double dblReportingDatumOffset = datumConverter.getBasevalue();

					datumConverter.setReferenceMappingField(OpsDatum.class, "offsetMsl");
					datumConverter.setBaseValueFromUserValue(CommonUtil.getConfiguredInstance().toDouble(offsetMsl, commandBean.getUserLocale()), false);
					Double dblOffsetMsl = datumConverter.getBasevalue();
					
					datumConverter.setReferenceMappingField(OpsDatum.class, "slantAngle");
					datumConverter.setBaseValueFromUserValue(CommonUtil.getConfiguredInstance().toDouble(slantAngle, commandBean.getUserLocale()), false);
					dblSlantAngle = datumConverter.getBasevalue();

					commandBean.overrideThreadLocalCurrentSelectedDatum(isGlReference,datumType, dblReportingDatumOffset, dblOffsetMsl, dblSlantAngle, dblSurfaceSlantLength, entryType);
				} else {
					// MSL
					commandBean.overrideThreadLocalCurrentSelectedDatum(isGlReference,datumType, 0, 0, 0, 0, entryType);
				}
			}
			
			// check whether to create new or update when performing depot AddToStore
			boolean isDepotCreateNew = false;
			if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_DEPOT && StringUtils.isNotBlank(operation.getOperationUid())) {
				Operation prevOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operation.getOperationUid());
				if (prevOperation == null) {
					isDepotCreateNew = true;
				}
			}
			
			if (isDepotCreateNew || StringUtils.isBlank(operation.getOperationUid())) {
				// create new
				if (isDrilling(operation) || StringUtils.isBlank(parentWellboreUid)) { // user will be warned if it is not drilling and parentWellboreUid being left blank
					// is drilling
					if (StringUtils.isBlank(parentWellboreUid)) {
						// create well, i.e. initial
						doCreateWell(commandBean, node, session, status, request);
					} else {
						// create wellbore, i.e. sidetrack
						doCreateWellbore(commandBean, node, session, status, request);
					}
				} else {
					// not drilling, create operation, e.g. workover
					doSaveOperation(commandBean, node, session, status, request);
				}
				
				if (BooleanUtils.isTrue(operation.getTightHole())) {
					commandBean.getSystemMessage().addInfo("You have declared this well to be 'tight-hole'. Please check destinations for any auto-generated emails and review list of people authorised to access the IDS site and the partner portal (if active). If you have any concerns about data confidentiality during the 'tight-hole' period, please contact IDS support", true, true);
				}
			} else {
				// update existing
				Operation prevOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operation.getOperationUid());
				if (isDrilling(prevOperation)) {
					// this was drilling
					Wellbore prevWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, prevOperation.getWellboreUid());
					if (StringUtils.isBlank(prevWellbore.getParentWellboreUid())) {
						// this was a well
						if (isNotDrilling(operation)) {
							// becomes an operation, e.g. workover
							doConvertWell2Operation(commandBean, node, session, status, request);
						} else if (StringUtils.isNotBlank(parentWellboreUid)) {
							// becomes a wellbore
							doConvertWell2Wellbore(commandBean, node, session, status, request);
						} else {
							// update well name, onOffShore, wellbore name and operation name
							doUpdateWell(commandBean, node, session, status, request);
						}
					} else {
						// this was a wellbore
						if (isNotDrilling(operation)) {
							// becomes an operation, e.g. workover
							doConvertWellbore2Operation(commandBean, node, session, status, request);
						} else if (StringUtils.isBlank(parentWellboreUid)) {
							// becomes a well
							doConvertWellbore2Well(commandBean, node, session, status, request);
						} else {
							// update wellbore name and operation name
							doUpdateWellbore(commandBean, node, session, status, request);
						}
					}
					
				} else {
					// this was an operation
					if (isDrilling(operation)) {
						// becomes drilling
						if (StringUtils.isBlank(parentWellboreUid)) {
							// becomes a well
							doConvertOperation2Well(commandBean, node, session, status, request);
						} else {
							// becomes a wellbore
							doConvertOperation2Wellbore(commandBean, node, session, status, request);
						}
					} else {
						// update self
						doSaveOperation(commandBean, node, session, status, request);
					}
				}
				
				// if default datum is changed, set the current datum in session to reflect the change
				String prevDefaultDatumUid = StringUtils.defaultString(prevOperation.getDefaultDatumUid());
				String currDefaultDatumUid = StringUtils.defaultString(operation.getDefaultDatumUid());
				if (!prevDefaultDatumUid.equals(currDefaultDatumUid)) {
					DatumManager.loadDatum();
					session.setCurrentUOMDatumUid(operation.getDefaultDatumUid(), true);
				}
				
				// the operation has just been set to become a tight hole
				if (BooleanUtils.isNotTrue(prevOperation.getTightHole()) && BooleanUtils.isTrue(operation.getTightHole())) {
					commandBean.getSystemMessage().addInfo("You have declared this well to be 'tight-hole'. Please check destinations for any auto-generated emails and review list of people authorised to access the IDS site and the partner portal (if active). If you have any concerns about data confidentiality during the 'tight-hole' period, please contact IDS support", true, true);
				}
			}
			
			// create this if the above passed
			if (StringUtils.isNotBlank(rigName)) {
				RigInformation thisRigInformation = CommonUtil.getConfiguredInstance().createRigInformation(rigName);
				commandBean.getSystemMessage().addInfo("A new rig has been created with minimal information from the Well Operation screen. Please make sure that the Rig Information module is reviewed and relevant details are added in.", true, true);
				operation.setRigInformationUid(thisRigInformation.getRigInformationUid());
			}
			if (StringUtils.isNotBlank(campaignName)) {
				Campaign campaign = CommonUtil.getConfiguredInstance().createCampaignOperation(campaignName);
				commandBean.getSystemMessage().addInfo("A new campaign has been created with minimal information from the Well Operation screen. Please make sure that the Campaign module is reviewed and relevant details are added in.", true, true);
				operation.setOperationCampaignUid(campaign.getCampaignUid());
			}
		}
		
		if (status.isProceed() && object instanceof TargetZone) {
			TargetZone thisTargetZone = (TargetZone) object;
			Operation operation = (Operation) node.getParent().getData();
			thisTargetZone.setWellUid(operation.getWellUid());
			thisTargetZone.setWellboreUid(operation.getWellboreUid());
		}
	}
	
	private void doConvertOperation2Wellbore(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Operation operation = (Operation) node.getData();
		String wellName = getDynaAttr(node, "wellName");
		String parentWellboreUid = getDynaAttr(node, "parentWellboreUid");
		
		Wellbore parentWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, parentWellboreUid);
		
		// create new wellbore
		//Wellbore wellbore = CommonUtil.getConfiguredInstance().createWellbore(wellName, parentWellbore.getWellUid(), parentWellbore.getWellboreUid(), session);
		Wellbore wellbore = new Wellbore();
		wellbore.setWellUid(parentWellbore.getWellUid());
		wellbore.setParentWellboreUid(parentWellbore.getWellboreUid());
		wellbore.setWellboreName(wellName);
		this.saveWellbore(wellbore, commandBean, node, session, request);
		//commandBean.getSystemMessage().addInfo("A new wellbore has been created with minimal information from the Well screen. Please make sure that the Wellbore module is reviewed and relevant details are added in.");
		
		// below may cause an unreferenced wellbore and well, so check and clean
		deleteWellboreIfNoOperationAssociated(operation.getWellboreUid(), operation.getOperationUid());
		
		// update self, this may result in an unreferenced wellbore
		operation.setWellUid(parentWellbore.getWellUid());
		operation.setWellboreUid(wellbore.getWellboreUid());
		operation.setOperationName(wellName);
		
		// touch well
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, operation.getWellUid());
		this.saveWell(well, commandBean, node, session, request);
	}
	
	private void doConvertOperation2Well(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Operation operation = (Operation) node.getData();
		String wellName = getDynaAttr(node, "wellName");
		//String wellOnOffShore = getDynaAttr(node, "onOffShore");
		
		// create new well and wellbore
		//Well well = CommonUtil.getConfiguredInstance().createWell(wellName, wellOnOffShore, session);
		Well well = new Well();
		well.setWellName(wellName);
		this.saveWell(well, commandBean, node, session, request);
		//commandBean.getSystemMessage().addInfo("A new well has been created with minimal information from the Well screen. Please make sure that the Well module is reviewed and relevant details are added in.");
		
		//Wellbore wellbore = CommonUtil.getConfiguredInstance().createWellbore(wellName, well.getWellUid(), null, session);
		Wellbore wellbore = new Wellbore();
		wellbore.setWellUid(well.getWellUid());
		wellbore.setParentWellboreUid(null);
		wellbore.setWellboreName(wellName);
		this.saveWellbore(wellbore, commandBean, node, session, request);
		//commandBean.getSystemMessage().addInfo("A new wellbore has been created with minimal information from the Well screen. Please make sure that the Wellbore module is reviewed and relevant details are added in.");
		
		// below may cause an unreferenced wellbore and well, so check and clean
		deleteWellboreIfNoOperationAssociated(operation.getWellboreUid(), operation.getOperationUid());
		
		// update self, this will result in unreferenced well and wellbore
		operation.setWellUid(well.getWellUid());
		operation.setWellboreUid(wellbore.getWellboreUid());
		operation.setOperationName(wellName);
	}
	
	private void doUpdateWellbore(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Operation operation = (Operation) node.getData();
		String wellName = getDynaAttr(node, "wellName");
		String parentWellboreUid = getDynaAttr(node, "parentWellboreUid");
		
		Wellbore parentWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, parentWellboreUid);
		
		Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
		
		// update associated daily's wellUid //jwong
		String strSql = "UPDATE Daily SET wellUid =:thisWellUid " +
		 "WHERE operationUid =:thisOperationUid";
		String[] paramsFields = {"thisWellUid", "thisOperationUid"};
		Object[] paramsValues = {parentWellbore.getWellUid(), operation.getOperationUid()};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
		
		// below may cause an unreferenced well, so check and clean
		deleteWellIfNoWellboreAssociated(wellbore.getWellUid(), wellbore.getWellboreUid());
		
		wellbore.setWellUid(parentWellbore.getWellUid());
		wellbore.setParentWellboreUid(parentWellbore.getWellboreUid());
		wellbore.setWellboreName(wellName);
		if (ApplicationUtils.getConfiguredInstance().isWellboreParentCyclicReference(wellbore)) {
			status.addError("This parent wellbore seems to be part of this wellbore's child. Please choose another parent wellbore.");
			status.setContinueProcess(false, true);
			return;
		}
		// save wellbore
		this.saveWellbore(wellbore, commandBean, node, session, request);
		
		// update current operation name
		operation.setWellUid(parentWellbore.getWellUid());
		operation.setOperationName(wellName);
		
		// touch well
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, operation.getWellUid());
		this.saveWell(well, commandBean, node, session, request);
		
		// update wellUid of all child wellbores and associated operations
		doCascadeUpdateWellUid(wellbore, parentWellbore.getWellUid(), session);
			
	}
		
	private void doConvertWellbore2Well(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Operation operation = (Operation) node.getData();
		String wellName = getDynaAttr(node, "wellName");
		//String wellOnOffShore = getDynaAttr(node, "onOffShore");
		
		//Well well = CommonUtil.getConfiguredInstance().createWell(wellName, wellOnOffShore, session);
		Well well = new Well();
		well.setWellName(wellName);
		this.saveWell(well, commandBean, node, session, request);
		//commandBean.getSystemMessage().addInfo("A new well has been created with minimal information from the Well screen. Please make sure that the Well module is reviewed and relevant details are added in.");
		
		// update current wellbore, this will result in an unreferenced well
		Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
		
		// below may cause an unreferenced well, so check and clean
		deleteWellIfNoWellboreAssociated(wellbore.getWellUid(), wellbore.getWellboreUid());
		
		wellbore.setWellUid(well.getWellUid());
		wellbore.setParentWellboreUid(null);
		wellbore.setWellboreName(wellName);
		if (ApplicationUtils.getConfiguredInstance().isWellboreParentCyclicReference(wellbore)) {
			status.addError("This parent wellbore seems to be part of this wellbore's child. Please choose another parent wellbore.");
			status.setContinueProcess(false, true);
			return;
		}
		this.saveWellbore(wellbore, commandBean, node, session, request);
		
		// update current operation name
		operation.setWellUid(well.getWellUid());
		operation.setOperationName(wellName);
		
		// update wellUid of all child wellbores and associated operations
		doCascadeUpdateWellUid(wellbore, well.getWellUid(), session);
	}
	
	private void doCascadeUpdateWellUid(Wellbore wellbore, String newWellUid,
			UserSession session) throws Exception {
		
		// update associated operation's wellUid
		List operationList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid", "wellboreUid", wellbore.getWellboreUid());
		for (Object object : operationList) {
			Operation childOperation = (Operation) object;
			childOperation.setWellUid(newWellUid);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(childOperation);
			
		}
		
		// update child wellbores' wellUid
		List wellboreList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where (isDeleted = false or isDeleted is null) and parentWellboreUid = :parentWellboreUid", "parentWellboreUid", wellbore.getWellboreUid());
		for (Object object : wellboreList) {
			Wellbore childWellbore = (Wellbore) object;
			childWellbore.setWellUid(newWellUid);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(childWellbore);
			doCascadeUpdateWellUid(childWellbore, newWellUid, session);
		}
	}
	
	private void doConvertWellbore2Operation(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		doConvertWell2Operation(commandBean, node, session, status, request);
	}
	
	private void doUpdateWell(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Operation operation = (Operation) node.getData();
		String wellName = getDynaAttr(node, "wellName");
		
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, operation.getWellUid());
		well.setWellName(wellName);
		this.saveWell(well, commandBean, node, session, request);
		
		Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
		wellbore.setWellboreName(wellName);
		this.saveWellbore(wellbore, commandBean, node, session, request);
		
		operation.setOperationName(wellName);
	}
	
	private void doConvertWell2Wellbore(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		doUpdateWellbore(commandBean, node, session, status, request);
	}
	
	private void doConvertWell2Operation(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Operation operation = (Operation) node.getData();
		
		// check if there is any child wellbore associated to this well
		List wellboreList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where (isDeleted = false or isDeleted is null) and parentWellboreUid = :parentWellboreUid", "parentWellboreUid", operation.getWellboreUid());
		if (wellboreList.size() > 0) {
			String operationCodelabel = OperationUtils.getOperationCodeLabel(commandBean, node, operation.getOperationCode());
			status.addError("Cannot change operation type to <strong>" + operationCodelabel + "</strong>, because there is at least one child well associated with this well.");
			status.setContinueProcess(false, true);
		} else {
			// this will result in unreferenced well and wellbore
			doSaveOperation(commandBean, node, session, status, request);
		}
	}
	
	private void doCreateWell(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
		Operation operation = (Operation) node.getData();
		String wellName = getDynaAttr(node, "wellName");
		
		Well well = new Well();
		well.setWellName(wellName);
		this.saveWell(well, commandBean, node, session, request);
		//commandBean.getSystemMessage().addInfo("A new well has been created with minimal information from the Well screen. Please make sure that the Well module is reviewed and relevant details are added in.");
		
		Wellbore wellbore = new Wellbore();
		wellbore.setWellUid(well.getWellUid());
		wellbore.setWellboreName(wellName);
		this.saveWellbore(wellbore, commandBean, node, session, request);
		//commandBean.getSystemMessage().addInfo("A new wellbore has been created with minimal information from the Well screen. Please make sure that the Wellbore module is reviewed and relevant details are added in.");
		
		operation.setWellUid(well.getWellUid());
		operation.setWellboreUid(wellbore.getWellboreUid());
		operation.setOperationName(wellName);
	}
	
	private void doCreateWellbore(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
		Operation operation = (Operation) node.getData();
		String wellName = getDynaAttr(node, "wellName");
		String parentWellboreUid = getDynaAttr(node, "parentWellboreUid");
		
		Wellbore parentWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, parentWellboreUid);
		
		//Wellbore wellbore = CommonUtil.getConfiguredInstance().createWellbore(wellName, parentWellbore.getWellUid(), parentWellboreUid, session);
		Wellbore wellbore = new Wellbore();
		wellbore.setWellboreName(wellName);
		wellbore.setWellUid(parentWellbore.getWellUid());
		wellbore.setParentWellboreUid(parentWellboreUid);
		this.saveWellbore(wellbore, commandBean, node, session, request);
		//commandBean.getSystemMessage().addInfo("A new wellbore has been created with minimal information from the Well screen. Please make sure that the Wellbore module is reviewed and relevant details are added in.");
		
		operation.setWellUid(parentWellbore.getWellUid());
		operation.setWellboreUid(wellbore.getWellboreUid());
		operation.setOperationName(wellName);
		
		// touch well
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, operation.getWellUid());
		this.saveWell(well, commandBean, node, session, request);
	}
	
	private void doSaveOperation(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
		Operation operation = (Operation) node.getData();
		String wellName = getDynaAttr(node, "wellName");
		String parentWellboreUid = getDynaAttr(node, "parentWellboreUid");
		
		Wellbore parentWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, parentWellboreUid);
		
		if (!parentWellboreUid.equals(operation.getWellboreUid())) {
			// the below might cause a well and wellbore to become unreferenced, so clean it up
			deleteWellboreIfNoOperationAssociated(operation.getWellboreUid(), operation.getOperationUid());
		}
		
		operation.setWellUid(parentWellbore.getWellUid());
		operation.setWellboreUid(parentWellbore.getWellboreUid());
		operation.setOperationName(wellName);
		
		// touch well
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, operation.getWellUid());
		this.saveWell(well, commandBean, node, session, request);
		
		// touch wellbore
		Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
		this.saveWellbore(wellbore, commandBean, node, session, request);
	}
	
	private void saveWellbore(Wellbore wellbore, CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request) throws Exception {
		
		if (wellbore != null) {
			Operation operation = (Operation) node.getData();
			
			// standard fields
			for (String dynaAttrName : CommonUtil.getConfiguredInstance().getDynaAttrNames(commandBean, node, request, "@wellbore.", "Operation")) {
				CommonUtil.getConfiguredInstance().setProperty(commandBean.getUserLocale(), wellbore, dynaAttrName.substring(10), node.getDynaAttr().get(dynaAttrName.substring(1)));
			}
			wellbore.setWellboreFinalPurpose(operation.getWellboreFinalPurpose());
			
			if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_DEPOT) {
				UserSession _session = UserSession.getInstance(request);
				ApplicationUtils.getConfiguredInstance().setCommonFields(wellbore, _session, commandBean.getInfo().getExcludedFieldsForAutoUpdate());
				if (wellbore.getWellboreUid() != null) {
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveImportedObjectWithActiveSTTCommandLogging(wellbore);
				} else {
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(wellbore);
				}
			} else {
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(wellbore);
			}
			
			// dynamic fields
			DynamicFieldsImpl dynamicFields = new DynamicFieldsImpl(wellbore, session.getCurrentGroupUid());
			Map<String, DynamicFieldMeta> dynamicFieldMetaMap = DynamicFieldUtil.getConfiguredInstance().getDynamicFieldMeta(Wellbore.class, session.getCurrentGroupUid());
			for (Iterator i = dynamicFieldMetaMap.keySet().iterator(); i.hasNext(); ) {
				String fieldName = (String) i.next();
				if (node.getDynaAttr().containsKey("wellbore." + fieldName)) {
					dynamicFields.getValues().put(fieldName, node.getDynaAttr().get("wellbore." + fieldName));
				}
			}
			dynamicFields.save();
		}
	}
	
	private void saveWell(Well well, CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request) throws Exception {
		
		if (well != null) {
			// for 'create new' widgets
			String basinName = getDynaAttr(node, "name");
			String platformName = getDynaAttr(node, "platformName");
			
			if (!node.isImported()) node.getMultiSelect().beforeParentNodeSave(request); // skip this when current request is WITSML. Currently WITSML does not support multiselect yet.
			
			// standard fields
			for (String dynaAttrName : CommonUtil.getConfiguredInstance().getDynaAttrNames(commandBean, node, request, "@well.", "Operation")) {
				CommonUtil.getConfiguredInstance().setProperty(commandBean.getUserLocale(), well, dynaAttrName.substring(6), node.getDynaAttr().get(dynaAttrName.substring(1)));
			}
			
			if (StringUtils.isNotBlank(platformName)) {
				Platform thisPlatform = CommonUtil.getConfiguredInstance().createPlatform(platformName);
				well.setPlatformUid(thisPlatform.getPlatformUid());
				commandBean.getSystemMessage().addInfo("A new platform has been created with minimal information from the Well screen. Please make sure that the Platform module is reviewed and relevant details are added in.", true, true);
			}
			
			if (StringUtils.isNotBlank(basinName)) {
				Basin thisBasin = CommonUtil.getConfiguredInstance().createBasin(basinName);
				commandBean.getSystemMessage().addInfo("A new basin has been created with minimal information from the Well screen. Please make sure that the Basin module is reviewed and relevant details are added in.", true, true);
				well.setBasin(thisBasin.getBasinUid());
			}
			
			if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_DEPOT) {
				UserSession _session = UserSession.getInstance(request);
				ApplicationUtils.getConfiguredInstance().setCommonFields(well, _session, commandBean.getInfo().getExcludedFieldsForAutoUpdate());
				if (well.getWellUid() != null) {
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveImportedObjectWithActiveSTTCommandLogging(well);
				} else {
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(well);
				}
			} else {
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(well);
			}
			// dynamic fields
			DynamicFieldsImpl dynamicFields = new DynamicFieldsImpl(well, session.getCurrentGroupUid());
			Map<String, DynamicFieldMeta> dynamicFieldMetaMap = DynamicFieldUtil.getConfiguredInstance().getDynamicFieldMeta(Well.class, session.getCurrentGroupUid());
			for (Iterator i = dynamicFieldMetaMap.keySet().iterator(); i.hasNext(); ) {
				String fieldName = (String) i.next();
				if (node.getDynaAttr().containsKey("well." + fieldName)) {
					dynamicFields.getValues().put(fieldName, node.getDynaAttr().get("well." + fieldName));
				}
			}
			dynamicFields.save();
		}
	}
	
	static private String getDynaAttr(CommandBeanTreeNode node, String dynaAttrName) throws Exception {
		Object dynaAttrValue = node.getDynaAttr().get(dynaAttrName);
		if (dynaAttrValue != null) {
			return dynaAttrValue.toString();
		}
		return null;
	}
	
	private void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		}
	}
	
	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		Object object = node.getData();
		if (object instanceof Operation) {
			Operation operation = (Operation) object;
			
			OperationUtils.setTightHoleRelatedContentAfterSaveOperation(operation, session);
			
			// put this newly created operation into the user selected ops teams, only the ops team that the current user has access to will show in the multiselect
			if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {				
				OperationUtils.updateOpTeams(node);
			}
			
			// refresh
			List<String> changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(operation.getOperationUid());
			D2ApplicationEvent.publishTableChangedEvent(Operation.class, changedRowPrimaryKeys);
			
			changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(operation.getWellboreUid());
			D2ApplicationEvent.publishTableChangedEvent(Wellbore.class, changedRowPrimaryKeys);
			
			changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(operation.getWellUid());
			D2ApplicationEvent.publishTableChangedEvent(Well.class, changedRowPrimaryKeys);
			
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
			
			// add the current well to the recent wells list, this must be done before the session.setCurrentOperationUid(...) call below
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_WELL_QUERY_FILTER_ENABLED))) {
				if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
					String queryFilterUid = session.getSystemSelectionFilter().addRecentObject(session.getUserUid(), operation.getWellUid(), session.getCurrentView());
					session.getSystemSelectionFilter().setQueryFilterUid(queryFilterUid, false);
					
					User currentUser = ApplicationUtils.getConfiguredInstance().getCachedUser(session.getUserUid());
					currentUser.setWellQueryFilterUid(queryFilterUid);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(currentUser);
				}
			}
			
			// remember the selected day
			Daily selectedDaily = session.getCurrentDaily();
			
			// this will always reset the selected day
			session.setCurrentOperationUid(operation.getOperationUid());
			
			if (selectedDaily != null) {
				// if the selected operation does not change, put back the selected day
				if (operation.getOperationUid().equals(selectedDaily.getOperationUid())) {
					session.setCurrentDailyUid(selectedDaily.getDailyUid());
				}
			}
			session.setCurrentRigOnOffShoreByWellUid(operation.getWellUid());
			
			// redirect to report daily
			if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				OperationUtils.redirectToReportDaily(session, commandBean, operation, "welloperation.html", tournetDailyRedirect);
			}
			
			//update first day progress when editing kickoff depth
			OperationUtils.updateFirstDayProgress(commandBean, operation, session);
			
			
			// if we just updated an existing record, reload the page for the well and datum drop down to reflect the change
			if (operationPerformed != BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				if (commandBean.getFlexClientControl() != null) {
					commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
				}
			}
			
			OperationUtils.updateDatumRelatedContentAfterSave(node, session, request);
			//Calculate planned duration from operation plan phase 
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoSumP50DurationToOperation")))
			{
				CommonUtil.getConfiguredInstance().calcPlannedDurationFromOperationPlanPhase(session.getCurrentOperationUid());
			}
			
			
			// EFC Vessel Cost - 20100321-1223-ekhoo 
			if (operation.getOperationVesselHourlyCost() != null)
			{
				String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) " +
							"and operationUid = :thisOperationUid AND reportType <> 'DGR'";
				String[] paramsFields2 = {"thisOperationUid"};
				String[] paramsValues2 = {operation.getOperationUid()};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
				
				if (lstResult.size() > 0){
					for(Object objReportdaily: lstResult){
						ReportDaily thisReportDaily = (ReportDaily) objReportdaily;
						CommonUtil.getConfiguredInstance().UpdateReportDailyEFC(operation.getOperationUid(),thisReportDaily.getDailyUid());
					}
				}
			}else{
				String strSql1 = "UPDATE ReportDaily SET efcVesselCost =null ," +
				 "efcTotalCost = null " +
				 "WHERE operationUid =:thisOperationUid";
				String[] paramsFields = {"thisOperationUid"};
				Object[] paramsValues = {operation.getOperationUid()};
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields, paramsValues);
			}
			
			OperationUtils.saveActualCosts(node);
			
			// auto populate PrognosedTopTvdMsl and SampleTopTvdMsl from prognosedTopMdMsl and sampleTopMdMsl when Wellbore.wellboreShape = 'Vertical'
			FormationUtils.autoPopulateTvdFromMd(operation.getWellboreUid(),(String)node.getDynaAttr().get("wellbore.wellboreShape"), session, commandBean);
		}
	}
	
	
	
	@Override
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		Object object = node.getData();
		if (object instanceof Operation) {
			/*
			 * to copy data from current well
			if(StringUtils.isNotBlank(userSelection.getOperationUid())){
				WellOperationCommandBeanListener.copyDataFromOperation(node, userSelection.getOperationUid());
			}
			*/
			
			Operation operation = (Operation) object;
			operation.setWellUid(null);
			operation.setWellboreUid(null);
			OperationUtils.setDefaultOperationStartDate(node, userSelection);
			operation.setWellboreFinalPurpose("DEV");
			//comment this off as the field is a mandatory field so that user will be more aware of the type they are selecting
			//operation.setOperationCode("DRLLG");
			OperationUtils.populateUomTemplateForNewOperation(node, userSelection);
			OperationUtils.setCanSetTightHoleFlag(node, request);
			operation.setTightHole(false);
			
			//set datum to null, and force user to pick rather auto and lead to data error in future
			OperationUtils.setDefaultDatumRelatedValueForNewOperation(node, userSelection);
			setDynaAttr(node, "editable", true);
			Basin defaultBasin = CommonUtil.getConfiguredInstance().getDefaultBasin();
			if (defaultBasin !=null)
				setDynaAttr(node, "well.basin", defaultBasin.getBasinUid());
			//This checking is for Add New Blank Button
			//If user no access to any ops team, ops team list will not show
			//If accessToOpsTeamOnly is true, then required field to show
			//If accessToOpsTeamOnly is null/false, then field with non-required to show 			
			
			OperationUtils.setAccessToOpsTeamOnlyFlag(node, userSelection);
			
			setDynaAttr(node, "well.onOffShore", GroupWidePreference.getValue(userSelection.getGroupUid(), "defaultOnOffShore"));
			
			OperationUtils.setDefaultDatumReferencePoint(node);
			OperationUtils.setActualCosts(commandBean, node);
			
			//set operation name prefix
			
			Double offset =0.0;	
			
			if (node.getDynaAttr().get("well.tzGmtOffset") !=null || node.getDynaAttr().get("well.tzGmtOffset") !=null) {
				offset = Double.parseDouble(node.getDynaAttr().get("well.tzGmtOffset").toString());
			}
			String operationPrefix = OperationUtils.getOperationNamePrefix(userSelection, operation, offset);
			setDynaAttr(node, "wellName", operationPrefix);
		}

	}

	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {		
		
		afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition 	= "(isDeleted = false or isDeleted is null)";

		String dynaAttrWellUid = (String) commandBean.getRoot().getDynaAttr().get("wellUid");
		if(StringUtils.isNotBlank(dynaAttrWellUid)){
			if(! com.idsdatanet.d2.drillnet.constant.UI.SELECT_OPTION_ALL.equals(dynaAttrWellUid)){
				query.addParam("customFilterWellUid", commandBean.getRoot().getDynaAttr().get("wellUid"));
				customCondition += " and wellUid = :customFilterWellUid";
			}
		}else{
			customCondition += " and operationUid = :customFilterOperationUid";
			query.addParam("customFilterOperationUid", userSelection.getOperationUid()); // use userSelection whenever possible (instead of UserSession)
		}
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	@Override
	public void afterDataNodeDelete(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		Object obj = node.getData();
		if (obj instanceof Operation) {
			Operation operation = (Operation) obj;
			
			OperationUtils.deleteOperationFromOpTeam(operation);
			
			List<String> changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(operation.getOperationUid());
			D2ApplicationEvent.publishTableChangedEvent(Operation.class, changedRowPrimaryKeys);
			
			changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(operation.getWellboreUid());
			D2ApplicationEvent.publishTableChangedEvent(Wellbore.class, changedRowPrimaryKeys);
			
			changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(operation.getWellUid());
			D2ApplicationEvent.publishTableChangedEvent(Well.class, changedRowPrimaryKeys);
			
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
			session.setCurrentOperationUid(null);
			session.setCurrentDailyUid(null);
			if (commandBean.getFlexClientControl() != null) {
				commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			}
		}
	}
	
	private DeleteHandler deleteHandler = new DeleteHandler();
	
	private class DeleteHandler implements com.idsdatanet.d2.core.web.mvc.ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			Operation operation = (Operation) node.getData();
			String parentWellboreUid = getDynaAttr(node, "parentWellboreUid");
			
			if (isDrilling(operation)) {
				if (StringUtils.isBlank(parentWellboreUid)) {
					// delete well
					//ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWell(operation.getWellUid());
					Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(operation.getWellUid());
					ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWell(well);
				} else {
					// delete wellbore
					Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(operation.getWellboreUid());
					deleteWellbore(wellbore);
				}
			} else {
				// delete operation
				deleteOperation(operation);
			}
			return new ActionHandlerResponse(true, false, null, BaseCommandBean.OPERATION_PERFORMED_DELETE);
		}
	}
	
	private void deleteOperation(Operation operation) throws Exception {
		if (operation != null) {
			// mark operation deleted
			ApplicationUtils.getConfiguredInstance().setDeleteFlagOnOperation(operation);
			
			// delete wellbore if no operation associated
			deleteWellboreIfNoOperationAssociated(operation.getWellboreUid());
		}
	}
	
	private void deleteWellboreIfNoOperationAssociated(String wellboreUid) throws Exception {
		if (StringUtils.isNotBlank(wellboreUid)) {
			List otherOperations = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid", "wellboreUid", wellboreUid);
			if (otherOperations.isEmpty()) {
				// no other operation associated with this wellbore, delete it
				Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(wellboreUid);
				deleteWellbore(wellbore);
			}
		}
	}
	
	private void deleteWellbore(Wellbore wellbore) throws Exception {
		if (wellbore != null) {
			// mark wellbore deleted
			//ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWellbore(wellbore.getWellboreUid());
			ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWellbore(wellbore);
			
			// delete well if no wellbore associated
			deleteWellIfNoWellboreAssociated(wellbore.getWellUid());
		}
	}
	
	private void deleteWellIfNoWellboreAssociated(String wellUid) throws Exception {
		if (StringUtils.isNotBlank(wellUid)) {
			List otherWellbores = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where (isDeleted = false or isDeleted is null) and wellUid = :wellUid", "wellUid", wellUid);
			if (otherWellbores.isEmpty()) {
				// no other wellbore associated with this well, mark well as deleted
				ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWell(wellUid);
			}
		}
	}
	
	private void deleteWellboreIfNoOperationAssociated(String wellboreUid, String operationUid) throws Exception {
		if (StringUtils.isNotBlank(wellboreUid) && StringUtils.isNotBlank(operationUid)) {
			// check whether this wellbore has other operation associated
			List otherOperations = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid and operationUid <> :operationUid", new String[] { "wellboreUid", "operationUid" }, new Object[] { wellboreUid, operationUid });
			if (otherOperations.isEmpty()) {
				// no other operation associated with this wellbore, delete it
				Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(wellboreUid);
				deleteWellbore(wellbore);
			}
		}
	}
	
	private void deleteWellIfNoWellboreAssociated(String wellUid, String wellboreUid) throws Exception {
		if (StringUtils.isNotBlank(wellUid) && StringUtils.isNotBlank(wellboreUid)) {
			List otherWellbores = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where (isDeleted = false or isDeleted is null) and wellUid = :wellUid and wellboreUid <> :wellboreUid", new String[] { "wellUid", "wellboreUid" }, new Object[] { wellUid, wellboreUid });
			if (otherWellbores.isEmpty()) {
				// no other wellbore associated with this well, mark well as deleted
				ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWell(wellUid);
			}
		}
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
	/*
	 * ActionManager for send to town and refresh from town
	 */
	private STTComConfig comConfig;
	private List<STTJobConfig> jobConfigs;
	
	public void setComConfig(STTComConfig comConfig) {
		this.comConfig = comConfig;
	}

	public void setJobConfigs(List<STTJobConfig> jobConfigs) {
		this.jobConfigs = jobConfigs;
	}
	
	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if (node.getData() instanceof Operation) {
			if (com.idsdatanet.d2.core.web.mvc.Action.DELETE.equals(action)) {
				return this.deleteHandler;
			}
		}
		if (com.idsdatanet.d2.core.web.mvc.Action.SEND_TO_TOWN.equals(action)) {
			return new STTActionHandler(comConfig, jobConfigs);
		} else if (com.idsdatanet.d2.core.web.mvc.Action.REFRESH_FROM_TOWN.equals(action)) {
			return new RFTActionHandler(comConfig, jobConfigs);
		}
		if ("toggleLockOperation".equals(action)) {
			return this.lockOperationHandler;
		}
		return null;
	}
	
	private LockOperationHandler lockOperationHandler = new LockOperationHandler();
	
	private class LockOperationHandler implements com.idsdatanet.d2.core.web.mvc.ActionHandler {
		
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			Operation currentOperation = userSession.getCurrentOperation();
			if (currentOperation != null) {
				currentOperation.setIsLocked(!BooleanUtils.isTrue(currentOperation.getIsLocked()));
				currentOperation.setLockedByUserUid(userSession.getCurrentUser().getUserUid());
				currentOperation.setLockedDatetime(new Date());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(currentOperation);
								
				if (commandBean.getFlexClientControl() != null) {
					commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
				}
			}
			return new ActionHandlerResponse(true);
		}
	}
	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{	//on paste as new record data with initial amount will be set to null
		
		Object object = targetNode.getData();
		if (object instanceof Operation) {
			setDynaAttr(targetNode, "editable", true);
			setDynaAttr(targetNode, "allowEditOperationType", 0);
		}
	}

	public void setTournetDailyRedirect(String tournetDailyRedirect) {
		this.tournetDailyRedirect = tournetDailyRedirect;
	}

	public String getTournetDailyRedirect() {
		return tournetDailyRedirect;
	}
}
