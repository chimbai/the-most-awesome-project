package com.idsdatanet.d2.drillnet.lookupCompany;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ServiceProviderLookupHandler implements LookupHandler {
		
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {

		String serviceCode = "NCR_SVCCO";
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		String sql = "select lc.lookupCompanyUid, lc.companyName from LookupCompany lc, LookupCompanyService lcs " + 
					 "where (lc.isDeleted = false or lc.isDeleted is null) and (lcs.isDeleted = false or lcs.isDeleted is null) " + 
					 "and lc.lookupCompanyUid=lcs.lookupCompanyUid and lcs.code=:code order by lc.companyName asc";
		
		List<Object[]> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "code",serviceCode);
		if (results.size() > 0) {
			for (Object[] companyResult : results) {
				if (companyResult[0] != null && companyResult[1] != null) {
					result.put(companyResult[0].toString(), new LookupItem(companyResult[0].toString(), companyResult[1].toString()));
				}
			}
		}
		return result;
	}

}

