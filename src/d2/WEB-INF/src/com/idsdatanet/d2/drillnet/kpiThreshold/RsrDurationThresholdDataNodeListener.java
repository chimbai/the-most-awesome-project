package com.idsdatanet.d2.drillnet.kpiThreshold;

import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.model.KpiThreshold;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class RsrDurationThresholdDataNodeListener extends EmptyDataNodeListener  {
	

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if (obj instanceof KpiThreshold)
		{
			KpiThreshold kpi = (KpiThreshold) obj;
			if (kpi.getTopDuration() <= kpi.getBottomDuration()) {
				status.setFieldError(node, "topDuration", "Cannot be smaller than or same as Duration Bottom");
				status.setContinueProcess(false, true);
			}	
		}
	}
	
}
