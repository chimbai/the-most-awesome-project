package com.idsdatanet.d2.drillnet.activity;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.stt.STTDecisionListener;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class ActivitySTTDecisionListener implements STTDecisionListener {
	
	public boolean ignore(Object data) {
		
		if(data instanceof Activity || data instanceof NextDayActivity) {
			try {
				String activityUid = (String) PropertyUtils.getProperty(data, "activityUid");
				
				Activity dbRecord = (Activity) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Activity.class, activityUid);
				
				if (dbRecord != null) {
					
					String dbCarriedForwardActivityUid = dbRecord.getCarriedForwardActivityUid();
					String sourceCarriedForwardActivityUid = (String) PropertyUtils.getProperty(data, "carriedForwardActivityUid"); 			
					
					if (StringUtils.isNotBlank(dbCarriedForwardActivityUid) && StringUtils.isBlank(sourceCarriedForwardActivityUid)){
						// if db value is not empty and source value is empty; 
						//set the carried forward Uid to the source so when replace will maintain the Uid relationship
						PropertyUtils.setProperty(data, "carriedForwardActivityUid", dbCarriedForwardActivityUid);
						
						return false;
					}	
				}
			} catch (Exception e) {}
		}
				
		//by default this record should allow to be copied
		return false;
	}

}
