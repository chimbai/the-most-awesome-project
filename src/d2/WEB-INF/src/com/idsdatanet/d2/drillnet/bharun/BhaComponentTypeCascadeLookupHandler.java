package com.idsdatanet.d2.drillnet.bharun;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.LookupBhaComponent;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class BhaComponentTypeCascadeLookupHandler implements CascadeLookupHandler {	
	
	private String defaultUri = null;
	private Object parentLookupURI = null;
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public void setDefaultUri(String defaultUri) {
		this.defaultUri = defaultUri;
	}
	
	public String getDefaultUri() {
		return this.defaultUri;
	}
	
	public void setParentLookupURI(Object parentLookupURI) {
		this.parentLookupURI = parentLookupURI;
	}

	public Object getParentLookupURI() {
		return parentLookupURI;
	}

	private class BhaComponentComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				LookupItem in1 = (LookupItem) o1[1];
				LookupItem in2 = (LookupItem) o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(in1.getValue().toString().toLowerCase());
				String f2 = WellNameUtil.getPaddedStr(in2.getValue().toString().toLowerCase());
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}

	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, String key, LookupCache lookupCache)
			throws Exception {
		// TODO Auto-generated method stub
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		Map<String, LookupItem> lookupList = new HashMap<String, LookupItem>();
		List<Object[]> mapList = new ArrayList();
		// This is to get the bha component lookup items from the lookup.xml
		if (this.defaultUri!=null) {
			CascadeLookupSet[] cascadeLookupSet = LookupManager.getConfiguredInstance().getCompleteCascadeLookupSet(this.defaultUri, userSelection);
			if(cascadeLookupSet!=null)
			{
				for(CascadeLookupSet cascadeLookup : cascadeLookupSet){
					//This for checking the parent key with the group name assigned to the lookup items in lookup.xml
					if(cascadeLookup.getKey().equalsIgnoreCase(key))
					{
						lookupList.putAll(cascadeLookup.getLookup());
					}
				}
			}
			
			for (Map.Entry<String, LookupItem> lookupItem: lookupList.entrySet()) {
				mapList.add(new Object[] {lookupItem.getKey(), lookupItem.getValue()});
			}
			
		}
        
		//This is to get the bha component lookup items from the lookup bha component table.
		String sql = "from LookupBhaComponent where (isDeleted = false or isDeleted is null) and componentType=:componentType order by type";
		List<LookupBhaComponent> lookupBhaComponentList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "componentType", key);
		if (lookupBhaComponentList.size() > 0) {
			for (LookupBhaComponent lookupBhaComponent : lookupBhaComponentList) {
				if (lookupBhaComponent.getLookupBhaComponentUid() != null && lookupBhaComponent.getType() != null) {
					LookupItem lookupItem = new LookupItem(lookupBhaComponent.getLookupBhaComponentUid(), lookupBhaComponent.getType());
					lookupItem.setActive(lookupBhaComponent.getIsActive());
					// supply witsml lookup
					if (StringUtils.isNotBlank(lookupBhaComponent.getWitsmlLookupKey())) {
						lookupItem.addDepotValue("witsml", lookupBhaComponent.getWitsmlLookupKey());
					}
					// supply image key lookup
					if (StringUtils.isNotBlank(lookupBhaComponent.getImageKey()))
					{
						lookupItem.addDepotValue("schematic", lookupBhaComponent.getImageKey());
					}
					mapList.add(new Object[] {lookupBhaComponent.getLookupBhaComponentUid(), lookupItem});
				}
			}
		}
		
		//Sorting by BHA component
		Collections.sort(mapList, new BhaComponentComparator());
		for(Iterator i = mapList.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			LookupItem item = (LookupItem) obj_array[1];
			if (!result.containsKey(item.getKey())) {
				result.put(item.getKey(), item);
			}
		}
		
		return result;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		//To get the parent field lookup items
		Map<String,LookupItem> parent = LookupManager.getConfiguredInstance().getLookup(this.parentLookupURI.toString(), userSelection, null);
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		for (Map.Entry<String, LookupItem> entry : parent.entrySet())
		{
			//To get the child lookup items based on the parent key that assigned to them and add into the cascade lookup set. 
			CascadeLookupSet set = new CascadeLookupSet(entry.getKey());
			set.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, entry.getKey(), null)));
			result.add(set);
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
	}
}
