package com.idsdatanet.d2.drillnet.rigpump;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.model.RigPump;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class RigPumpDataNodeAllowedAction implements DataNodeAllowedAction {
	
	private Boolean isWellControl = false;
	
	public void setIsWellControl(Boolean isWellControl){
		this.isWellControl = isWellControl;
	}
	
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		if(StringUtils.equals(targetClass, null)) {
			Object obj = node.getData();
			
			if (obj instanceof RigPump) {
				if(StringUtils.equals(action, "delete"))  return false;
				if (this.isWellControl){
					if(StringUtils.equals(action, "edit"))  return false;
				}
			}
			if (obj instanceof Mud && this.isWellControl) {
				if(StringUtils.equals(action, "delete"))  return false;
				if(StringUtils.equals(action, "edit"))  return false;
			}
		}
		return true;
	}
}
