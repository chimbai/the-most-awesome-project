package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class OperationActivityDurationBreakdownByPhaseCodeReportDataGenerator implements ReportDataGenerator {
		private List<String> nptCode;
		private List<String> phaseCode;
		
		private Double cumP10Duration = 0.0;
		private Double cumP50Duration  = 0.0;
		private Double cumP90Duration  = 0.0;
		private Double cumTLDuration  = 0.0;
		private Double cumPmeanDuration  = 0.0;
		private Double cumActualDuration  = 0.0;
		private Double cumNptDuration  = 0.0;
	
		public void disposeOnDataGeneratorExit() {
			// TODO Auto-generated method stub	
		}
		
		public void setNptCode(List<String> nptCode)
		{
			if(nptCode != null){
				this.nptCode = new ArrayList<String>();
				for(String value: nptCode){
					this.nptCode.add(value.trim().toUpperCase());
				}
			}
		}
		
		public List<String> getNptCode() {
			return this.nptCode;
		}
		
		public void setPhaseCode(List<String> phaseCode)
		{
			if(phaseCode != null){
				this.phaseCode = new ArrayList<String>();
				for(String value: phaseCode){
					this.phaseCode.add(value.trim().toUpperCase());
				}
			}
		}
		
		public List<String> getPhaseCode() {
			return this.phaseCode;
		}
		
		public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		// get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		
		Date todayDate = daily.getDayDate();
		String nptClassCodeList = "";
		String phaseCodeList = "";
		CustomFieldUom durationConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activityDuration");
		
		if(this.getNptCode() != null){
			for(String value:this.nptCode){
				nptClassCodeList = nptClassCodeList + "'" + value.toString() + "',";
			}
			
			nptClassCodeList = StringUtils.substring(nptClassCodeList, 0, -1);
			nptClassCodeList = StringUtils.upperCase(nptClassCodeList);
		}
		
		if(this.getPhaseCode() != null){
			for(String value:this.phaseCode){
				phaseCodeList = phaseCodeList + "'" + value.toString() + "',";
			}
			
			phaseCodeList = StringUtils.substring(phaseCodeList, 0, -1);
			phaseCodeList = StringUtils.upperCase(phaseCodeList);
		}
			
		//Collect Planned, Actual and NPT duration of each phase
		String currentOperationPlanMasterUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(userContext.getUserSelection().getOperationUid());
		String phaseName = "N/A";
		String phaseCode = "N/A";
		
		for(String shortCode:this.phaseCode){
			phaseName = getPhaseName(shortCode,userContext.getUserSelection().getOperationType(),qp);
				
			queryPlannedDurationPerPhase(shortCode,phaseCodeList,currentOperationPlanMasterUid,durationConverter,qp);
			queryActualDurationPerPhase(userContext.getUserSelection().getOperationUid(),todayDate,shortCode,phaseCodeList,durationConverter,qp);
			queryNPTDurationPerPhase(userContext.getUserSelection().getOperationUid(),todayDate,shortCode,phaseCodeList,nptClassCodeList,durationConverter,qp);
			
			ReportDataNode thisReportNode = reportDataNode.addChild("phaseBreakdown");
			thisReportNode.addProperty("code",shortCode);
			thisReportNode.addProperty("name",phaseName);
			thisReportNode.addProperty("cumP10Duration",this.getCumP10Duration().toString());
			thisReportNode.addProperty("cumP50Duration",this.getCumP50Duration().toString());
			thisReportNode.addProperty("cumP90Duration",this.getCumP90Duration().toString());
			thisReportNode.addProperty("cumTLDuration",this.getCumTLDuration().toString());
			thisReportNode.addProperty("cumPmeanDuration",this.getCumPmeanDuration().toString());
			thisReportNode.addProperty("cumActualDuration",this.getCumActualDuration().toString());
			thisReportNode.addProperty("cumNptDuration",this.getCumNptDuration().toString());
			if(durationConverter.isUOMMappingAvailable() ){
				thisReportNode.addProperty("durationUomSymbol", durationConverter.getUomSymbol());
				thisReportNode.addProperty("durationUomLabel", durationConverter.getUomLabel());
			}
		}
			
		//For Other phase
		phaseCode = "N/A";
		phaseName = "Other";
						
		//Query Planned duration
		queryPlannedDurationPerPhase(phaseCode,phaseCodeList,currentOperationPlanMasterUid,durationConverter,qp);
		queryActualDurationPerPhase(userContext.getUserSelection().getOperationUid(),todayDate,phaseCode,phaseCodeList,durationConverter,qp);
		queryNPTDurationPerPhase(userContext.getUserSelection().getOperationUid(),todayDate,phaseCode,phaseCodeList,nptClassCodeList,durationConverter,qp);
		
		ReportDataNode thisReportNode = reportDataNode.addChild("phaseBreakdown");
		thisReportNode.addProperty("code",phaseCode);
		thisReportNode.addProperty("name",phaseName);
		thisReportNode.addProperty("cumP10Duration",this.getCumP10Duration().toString());
		thisReportNode.addProperty("cumP50Duration",this.getCumP50Duration().toString());
		thisReportNode.addProperty("cumP90Duration",this.getCumP90Duration().toString());
		thisReportNode.addProperty("cumTLDuration",this.getCumTLDuration().toString());
		thisReportNode.addProperty("cumPmeanDuration",this.getCumPmeanDuration().toString());
		thisReportNode.addProperty("cumActualDuration",this.getCumActualDuration().toString());
		thisReportNode.addProperty("cumNptDuration",this.getCumNptDuration().toString());
		if(durationConverter.isUOMMappingAvailable() ){
			thisReportNode.addProperty("durationUomSymbol", durationConverter.getUomSymbol());
			thisReportNode.addProperty("durationUomLabel", durationConverter.getUomLabel());
		}
	}	
		
	private String getPhaseName(String code, String operationType, QueryProperties qp) throws Exception{
		String phaseName = "N/A";
		
		//Query phase name
		String strSql = "SELECT name FROM LookupPhaseCode " +
				 "WHERE (isDeleted IS FALSE OR isDeleted IS NULL) " +
				 "AND shortCode = :shortCode " +
				 "AND (operationCode = :thisOperationType OR operationCode = '')";
		String[] paramsFields = {"shortCode","thisOperationType"};
		Object[] paramsValues = {code, operationType};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			Object lookupPhaseCodeResult = (Object) lstResult.get(0);
			
			if(lookupPhaseCodeResult != null){
				phaseName = lookupPhaseCodeResult.toString();
			}
		}
		
		return phaseName;
	}
	
	private void queryPlannedDurationPerPhase(String phaseCode, String phaseCodeList, String operationPlanMasterUid, CustomFieldUom durationConverter, QueryProperties qp) throws Exception{
		String strSql = "";
		
		if(!StringUtils.equals(phaseCode,"N/A")){
			strSql = "SELECT phaseCode, SUM(p10Duration) as cumP10Duration, SUM(p50Duration) as cumP50Duration, SUM(p90Duration) as cumP90Duration, SUM(tlDuration) as cumTLDuration, SUM(pmeanDuration) as cumPMeanDuration FROM OperationPlanPhase " +
					 "WHERE (isDeleted = FALSE OR isDeleted IS NULL) " +
					 "AND operationPlanMasterUid = :operationPlanMasterUid " +
					 "AND phaseCode = :phaseCode";
		}
		else{
			strSql = "SELECT phaseCode, SUM(p10Duration) as cumP10Duration, SUM(p50Duration) as cumP50Duration, SUM(p90Duration) as cumP90Duration, SUM(tlDuration) as cumTLDuration, SUM(pmeanDuration) as cumPMeanDuration FROM OperationPlanPhase " +
					 "WHERE (isDeleted = FALSE OR isDeleted IS NULL) " +
					 "AND operationPlanMasterUid = :operationPlanMasterUid " +
					 "AND phaseCode NOT IN (" + phaseCodeList + ") " +
					 "GROUP BY phaseCode";
		}
		
		List lstResult = null;
		
		if(!StringUtils.equals(phaseCode,"N/A")){
			String[] paramsFields = {"operationPlanMasterUid","phaseCode"};
			Object[] paramsValues = {operationPlanMasterUid,phaseCode};
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
		else{
			String[] paramsFields = {"operationPlanMasterUid"};
			Object[] paramsValues = {operationPlanMasterUid};
		    lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
		
		if(lstResult != null && lstResult.size() > 0){
			cumP10Duration = 0.0;
			cumP50Duration  = 0.0;
			cumP90Duration = 0.0;
			cumTLDuration  = 0.0;
			cumPmeanDuration  = 0.0;
			
			for(Object objResult: lstResult){
				Object[] obj = (Object[]) objResult;
				
				Double p10Duration = 0.0;
				Double p50Duration  = 0.0;
				Double p90Duration = 0.0;
				Double tlDuration  = 0.0;
				Double pmeanDuration  = 0.0;
				
				if(obj[1]!=null && StringUtils.isNotBlank(obj[1].toString())) p10Duration = Double.parseDouble(obj[1].toString());
				if(obj[2]!=null && StringUtils.isNotBlank(obj[2].toString())) p50Duration = Double.parseDouble(obj[2].toString());
				if(obj[3]!=null && StringUtils.isNotBlank(obj[3].toString())) p90Duration = Double.parseDouble(obj[3].toString());
				if(obj[4]!=null && StringUtils.isNotBlank(obj[4].toString())) tlDuration = Double.parseDouble(obj[4].toString());
				if(obj[5]!=null && StringUtils.isNotBlank(obj[5].toString())) pmeanDuration = Double.parseDouble(obj[5].toString());
				
				cumP10Duration += p10Duration;
				cumP50Duration += p50Duration;
				cumP90Duration += p90Duration;
				cumTLDuration += tlDuration;
				cumPmeanDuration += pmeanDuration;
			}
			
			if(cumP10Duration != null){
				durationConverter.setBaseValue(cumP10Duration);
				this.setCumP10Duration(durationConverter.getConvertedValue());
			}
			
			if(cumP50Duration != null){
				durationConverter.setBaseValue(cumP50Duration);
				this.setCumP50Duration(durationConverter.getConvertedValue());
			}
			
			if(cumP90Duration != null){
				durationConverter.setBaseValue(cumP90Duration);
				this.setCumP90Duration(durationConverter.getConvertedValue());
			}
			
			if(cumTLDuration != null){
				durationConverter.setBaseValue(cumTLDuration);
				this.setCumTLDuration(durationConverter.getConvertedValue());
			}
			
			if(cumPmeanDuration != null){
				durationConverter.setBaseValue(cumPmeanDuration);
				this.setCumPmeanDuration(durationConverter.getConvertedValue());
			}
		}
	}
	
	private void queryActualDurationPerPhase(String operationUid, Date todayDate, String phaseCode, String phaseCodeList, CustomFieldUom durationConverter, QueryProperties qp) throws Exception{
		String strSql = "";
		
		if(!StringUtils.equals(phaseCode,"N/A")){
			strSql = "SELECT a.phaseCode, SUM(a.activityDuration) as totalDuration FROM Activity a, Daily d " +
				     "WHERE (a.isDeleted IS FALSE OR a.isDeleted IS NULL) " +
				     "AND (d.isDeleted IS FALSE OR d.isDeleted IS NULL) " +
		 		     "AND a.dailyUid = d.dailyUid " +
				     "AND a.operationUid = :thisOperationUid " +
				     "AND d.dayDate <= :userDate " +
				     "AND a.phaseCode = :phaseCode " +
				     "AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL))";
		}
		else{
			strSql = "SELECT a.phaseCode, SUM(a.activityDuration) as totalDuration FROM Activity a, Daily d " +
				     "WHERE (a.isDeleted IS FALSE OR a.isDeleted IS NULL) " +
				     "AND (d.isDeleted IS FALSE OR d.isDeleted IS NULL) " +
		 		     "AND a.dailyUid = d.dailyUid " +
				     "AND a.operationUid = :thisOperationUid " +
				     "AND d.dayDate <= :userDate " +
				     "AND a.phaseCode NOT IN (" + phaseCodeList + ") " +
				     "AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " +
				     "GROUP BY a.phaseCode";
		}
		
		List lstResult = null;
		
		if(!StringUtils.equals(phaseCode,"N/A")){
			String[] paramsFields = {"thisOperationUid","userDate","phaseCode"};
			Object[] paramsValues = {operationUid,todayDate,phaseCode};
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
		else{
			String[] paramsFields = {"thisOperationUid","userDate"};
			Object[] paramsValues = {operationUid,todayDate};
		    lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
		
		if(lstResult != null && lstResult.size() > 0){
			cumActualDuration = 0.0; 
			
			for(Object objResult: lstResult){
				Object[] obj = (Object[]) objResult;
				Double duration = 0.0;
				
				if(obj[1]!=null && StringUtils.isNotBlank(obj[1].toString())) duration = Double.parseDouble(obj[1].toString());
				
				cumActualDuration += duration;
			}
			
			if(cumActualDuration != null){
				durationConverter.setBaseValue(cumActualDuration);
				this.setCumActualDuration(durationConverter.getConvertedValue());
			}
		}
	}
	
	private void queryNPTDurationPerPhase(String operationUid, Date todayDate, String phaseCode, String phaseCodeList, String nptClassCodeList, CustomFieldUom durationConverter, QueryProperties qp) throws Exception{
		String strSql = "";
		
		if(!StringUtils.equals(phaseCode,"N/A")){
			strSql = "SELECT a.phaseCode, SUM(a.activityDuration) as totalDuration FROM Activity a, Daily d " +
		 		     "WHERE (a.isDeleted IS FALSE OR a.isDeleted IS NULL) " +
		 		     "AND (d.isDeleted IS FALSE OR d.isDeleted IS NULL) " +
		 		     "AND a.dailyUid = d.dailyUid " +
		 		     "AND a.operationUid = :thisOperationUid " +
		 		     "AND d.dayDate <= :userDate " +
		 		     "AND a.phaseCode = :phaseCode " +
		 		     "AND a.classCode IN (" + nptClassCodeList + ") " +
		 		     "AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL))";
		}
		else{
			strSql = "SELECT a.phaseCode, SUM(a.activityDuration) as totalDuration FROM Activity a, Daily d " +
		 		     "WHERE (a.isDeleted IS FALSE OR a.isDeleted IS NULL) " +
		 		     "AND (d.isDeleted IS FALSE OR d.isDeleted IS NULL) " +
		 		     "AND a.dailyUid = d.dailyUid " +
		 		     "AND a.operationUid = :thisOperationUid " +
		 		     "AND d.dayDate <= :userDate " +
		 		     "AND a.phaseCode NOT IN (" + phaseCodeList + ") " +
		 		     "AND a.classCode IN (" + nptClassCodeList + ") " +
		 		     "AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " +
		 		     "GROUP BY a.phaseCode";
		}
		
		List lstResult = null;
		
		if(!StringUtils.equals(phaseCode,"N/A")){
			String[] paramsFields = {"thisOperationUid","userDate","phaseCode"};
			Object[] paramsValues = {operationUid,todayDate,phaseCode};
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
		else{
			String[] paramsFields = {"thisOperationUid","userDate"};
			Object[] paramsValues = {operationUid,todayDate};
		    lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
		
		if(lstResult != null && lstResult.size() > 0){
			cumNptDuration = 0.0; 
			
			for(Object objResult: lstResult){
				Object[] obj = (Object[]) objResult;
				Double duration = 0.0;
				
				if(obj[1]!=null && StringUtils.isNotBlank(obj[1].toString())) duration = Double.parseDouble(obj[1].toString());
				
				cumNptDuration += duration;
			}
			
			if(cumNptDuration != null){
				durationConverter.setBaseValue(cumNptDuration);
				this.setCumNptDuration(durationConverter.getConvertedValue());
			}
		}
	}

	public void setCumP10Duration(Double cumP10Duration) {
		this.cumP10Duration = cumP10Duration;
	}

	public Double getCumP10Duration() {
		return cumP10Duration;
	}

	public void setCumP50Duration(Double cumP50Duration) {
		this.cumP50Duration = cumP50Duration;
	}

	public Double getCumP50Duration() {
		return cumP50Duration;
	}

	public void setCumP90Duration(Double cumP90Duration) {
		this.cumP90Duration = cumP90Duration;
	}

	public Double getCumP90Duration() {
		return cumP90Duration;
	}

	public void setCumTLDuration(Double cumTLDuration) {
		this.cumTLDuration = cumTLDuration;
	}

	public Double getCumTLDuration() {
		return cumTLDuration;
	}

	public void setCumPmeanDuration(Double cumPmeanDuration) {
		this.cumPmeanDuration = cumPmeanDuration;
	}

	public Double getCumPmeanDuration() {
		return cumPmeanDuration;
	}

	public void setCumActualDuration(Double cumActualDuration) {
		this.cumActualDuration = cumActualDuration;
	}

	public Double getCumActualDuration() {
		return cumActualDuration;
	}

	public void setCumNptDuration(Double cumNptDuration) {
		this.cumNptDuration = cumNptDuration;
	}

	public Double getCumNptDuration() {
		return cumNptDuration;
	}
}