package com.idsdatanet.d2.drillnet.mudProperties;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.MudRheology;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class MudPropertiesReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		
		if (StringUtils.isNotBlank(currentOperationUid)) {

			String strSql = "Select distinct(holeSize) from MudProperties where (isDeleted = false or isDeleted is null) and operationUid =:operationUid order by holeSize DESC" ;
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,  new String[] {"operationUid"}, new Object[] {currentOperationUid});
		
			if (lstResult.size() > 0){
				for(Object a : lstResult)
				{
					
					if (a !=null){ //if there is holesize
						
						Double holeSize = Double.parseDouble(a.toString());
						ReportDataNode thisReportNode = reportDataNode.addChild("MudPropertiesHoleSize");
						thisReportNode.addProperty("holeSize", this.nullToEmptyString(this.formatHoleSize(holeSize)));
						thisReportNode.addProperty("holeSizeLabel", this.getHoleSizeLabel(this.formatHoleSize(holeSize), userContext));
						
						
						String strSql2 = "Select dailyUid, reportNumber, reportDatetime From ReportDaily where (isDeleted = false or isDeleted is null) and operationUid =:operationUid and reportType !='DGR' order by reportDatetime" ;
						List<Object[]> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2,  new String[] {"operationUid"}, new Object[] {currentOperationUid});
						if (lstResult2.size() > 0){
							for (Object[] rptDaily : lstResult2){
								String dailyUid = rptDaily[0].toString();
								
								String strSql3 = "From MudProperties where (isDeleted = false or isDeleted is null) and dailyUid =:dailyUid and holeSize=:holeSize" ;
								List<MudProperties> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3,  new String[] {"dailyUid", "holeSize"}, new Object[] {dailyUid, a});
							
								if (lstResult3.size() > 0){
									MudProperties mud = lstResult3.get(0);
									
									ReportDataNode thisReportNode2 = thisReportNode.addChild("MudProperties");
									thisReportNode2.addProperty("reportNumber", this.nullToEmptyString(rptDaily[1]));
									thisReportNode2.addProperty("dayDate", this.nullToEmptyString(rptDaily[2]));
									thisReportNode2.addProperty("operationUid", mud.getOperationUid());
									
									String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(mud.getGroupUid(), mud.getOperationUid());
									thisReportNode2.addProperty("operationName", operationName);
									
									thisReportNode2.addProperty("dailyUid", this.nullToEmptyString(mud.getDailyUid()));
									
									CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), MudProperties.class, "mwdh");
									thisReportNode2.addProperty("mudWeight", this.nullToEmptyString(mud.getMwdh()));
									thisReportNode2.addProperty("mudWeightUomSymbol", this.nullToEmptyString(thisConverter.getUomSymbol()));
									
									thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), MudProperties.class, "mudPv");
									thisReportNode2.addProperty("mudPv", this.nullToEmptyString(mud.getMudPv()));
									thisReportNode2.addProperty("mudPvUomSymbol", this.nullToEmptyString(thisConverter.getUomSymbol()));
									
									thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), MudProperties.class, "mudYp");
									thisReportNode2.addProperty("mudYp", this.nullToEmptyString(mud.getMudYp()));
									thisReportNode2.addProperty("mudYpUomSymbol", this.nullToEmptyString(thisConverter.getUomSymbol()));
									
									thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), MudProperties.class, "mudApiFl");
									thisReportNode2.addProperty("mudApiFl", this.nullToEmptyString(mud.getMudApiFl()));
									thisReportNode2.addProperty("mudApiFlUomSymbol", this.nullToEmptyString(thisConverter.getUomSymbol()));
									
									thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), MudProperties.class, "kclLossConcentration");
									thisReportNode2.addProperty("kclLossConcentration", this.nullToEmptyString(mud.getKclLossConcentration()));
									thisReportNode2.addProperty("kclLossConcentrationUomSymbol", this.nullToEmptyString(thisConverter.getUomSymbol()));
									
									thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), MudProperties.class, "mudKclConcentration");
									thisReportNode2.addProperty("mudKclConcentration", this.nullToEmptyString(mud.getMudKclConcentration()));
									thisReportNode2.addProperty("mudKclConcentrationUomSymbol", this.nullToEmptyString(thisConverter.getUomSymbol()));
									
									
									String strSql4 = "From MudRheology where (isDeleted = false or isDeleted is null) and rpm = 6 and mudPropertiesUid=:mudPropertiesUid";
									List<MudRheology> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4,  new String[] {"mudPropertiesUid"}, new Object[] {mud.getMudPropertiesUid()});
	
									if (lstResult4.size() > 0){
										for(MudRheology rh : lstResult4){
											thisReportNode2.addProperty("rheology6rpm", this.nullToEmptyString(rh.getRheologyReading()));
										}
									}
									
	
									thisReportNode2.addProperty("holeSize", this.nullToEmptyString(this.formatHoleSize(mud.getHoleSize())));
	
								}
								
							}
						}
					}

				}
			}
			
		}	
	}
	
	private String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}
	
	private String getHoleSizeLabel(String holeSizeValue, UserContext userContext) throws Exception {
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://casingsection.casing_holesize?key=code&amp;value=label", userSelection, null);
		if (lookupMap != null) {
			LookupItem lookupItem = lookupMap.get(holeSizeValue);
			if (lookupItem != null) {
				return (String) lookupItem.getValue();
			}
		}
		
		return holeSizeValue;
	}
	
	private String formatHoleSize(Double holeSize) {
		if (holeSize==null) return "";
		DecimalFormat nf = new DecimalFormat();
		nf.setMaximumFractionDigits(6);
		nf.setMinimumFractionDigits(6);
		return nf.format(holeSize);
	}
}
