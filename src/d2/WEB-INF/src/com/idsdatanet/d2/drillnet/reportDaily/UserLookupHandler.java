package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class UserLookupHandler implements LookupHandler {	
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		String sql = "select a.userUid, a.isDeleted, a.fname from User a order by a.fname";
		
		List<Object[]> userResults = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		if (userResults.size() > 0) {
			for (Object[] userResult : userResults) {
				if (userResult[0] != null && userResult[2] != null) {
					LookupItem lookupItem = null;
					if (userResult[1] != null && Boolean.TRUE.equals(Boolean.valueOf(userResult[1].toString()))) {
						lookupItem = new LookupItem(userResult[0].toString(), userResult[2].toString());
						lookupItem.setActive(Boolean.FALSE);
					}
					else {
						lookupItem = new LookupItem(userResult[0].toString(), userResult[2].toString());
					}
					result.put(userResult[0].toString(), lookupItem);
				}
			}
		}
		return result;
	}
}