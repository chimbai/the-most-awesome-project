package com.idsdatanet.d2.drillnet.lookupCompany;

import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.model.*;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import java.util.*;
import org.apache.commons.lang.StringUtils;

public class LookupCompanyDataNodeListener extends EmptyDataNodeListener {
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof LookupCompany) {
			LookupCompany myCompany = (LookupCompany)object;
			String myCompanyName = myCompany.getCompanyName();
			
			if(myCompanyName != null ){
				
				String strSql = "FROM LookupCompany WHERE (isDeleted = false or isDeleted is null) and lookupCompanyUid <> :lookupCompanyUid AND companyName = :companyName";
				String[] paramsFields = {"lookupCompanyUid", "companyName"};
				String[] paramsValues = {(myCompany.getLookupCompanyUid()==null?"":myCompany.getLookupCompanyUid()), myCompanyName};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (lstResult.size() > 0){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "companyName", "This Company Name already exist.");
				}
			}
			myCompany.setIsNewCompany(null);
			
			String companyStatus = myCompany.getStatus();
			//Ticket 17090: To auto-populate isActive field based on company status
			myCompany.setIsActive(true);
			
			if(companyStatus != null && StringUtils.isNotBlank(companyStatus)){
				if ("B".equals(companyStatus) || "I".equals(companyStatus)){
					myCompany.setIsActive(false);
				}
			}

		}
	}
}
