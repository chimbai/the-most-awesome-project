package com.idsdatanet.d2.drillnet.activity;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityMarDataNodeListener extends EmptyDataNodeListener {

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		User user = (User) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(User.class, userSelection.getUserUid());
		commandBean.getRoot().getDynaAttr().put("username",user.getUserName()); 
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		User user = (User) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(User.class, userSelection.getUserUid());
		commandBean.getRoot().getDynaAttr().put("username",user.getUserName()); 
	}
}