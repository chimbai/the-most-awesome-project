package com.idsdatanet.d2.drillnet.wellExternalCatalog;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.WellExternalCatalog;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * Load handler to override the collection of WECs. This is used in the case the filters at the top of the WellExternalCatalog screen.
 * Essentially upon entering and clicking "Filter", a submitCurrentNodeAndReload happens in wellexternalcatalog.js. 
 * Then the filter parameters are fetch and used to build the full query + filters. 
 * If no filters it just uses the default fetch all query + pagination page.
 * 17 Aug 2022
 */
public class WellExternalCatalogDataNodeLoadHandler extends EmptyDataNodeListener implements DataNodeLoadHandler  {
	private static final String NOT_IMPORTED = "NOT_IMPORTED";
	private static final String PENDING_IMPORT = "PENDING_IMPORT";
	private static final String IMPORTING = "IMPORTING";
	private static final String IMPORTED = "IMPORTED";
	private static final String IMPORTED_WW = "IMPORTED_WW";
	private String[] nonImportedStatuses = { NOT_IMPORTED, PENDING_IMPORT, IMPORTING };
	private String[] importedStatuses = { IMPORTED, IMPORTED_WW };
	private Boolean selectedWellUwiBefore = false;
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		
		if (meta.getTableClass().equals(WellExternalCatalog.class)) {
			QueryProperties qp = new QueryProperties();
			// grab all the filter values from the dynaAttrs
			String server = (String) commandBean.getRoot().getDynaAttr().get("server");
			String wellName = (String) commandBean.getRoot().getDynaAttr().get("wellName");
			String uwi = (String) commandBean.getRoot().getDynaAttr().get("uwi");
			String country = (String) commandBean.getRoot().getDynaAttr().get("country");
			String status = (String) commandBean.getRoot().getDynaAttr().get("status");
			String onOffShore = (String) commandBean.getRoot().getDynaAttr().get("onOffShore");
			String uwbi = (String) commandBean.getRoot().getDynaAttr().get("uwbi");
			String field = (String) commandBean.getRoot().getDynaAttr().get("field");
			
			// If you selected wellName or uwi prior, this will help to remove both since we added both.
			if (this.selectedWellUwiBefore && (StringUtils.isBlank(wellName) || StringUtils.isBlank(uwi))) {
				commandBean.getRoot().getDynaAttr().put("wellName", "");
				commandBean.getRoot().getDynaAttr().put("uwi", "");
				wellName = null;
				uwi = null;
				this.selectedWellUwiBefore = false;
			}
			
			// begin the query statement building process
			String hql = "From WellExternalCatalog WHERE (isDeleted IS NULL OR isDeleted = 0)";
			List<String> paramNames= new ArrayList<String>();
			List<Object> paramValues= new ArrayList<Object>();
			
			if (StringUtils.isNotBlank(server)) {
				hql = hql + " AND importExportServerPropertiesUid=:server";
				paramNames.add("server");
				paramValues.add(server);
			}
			
			// for wellName and uwi lookups, we can pre-populate the lookup with the corresponding wellName and UWI since they are linked
			// essentially wellName A will have uwi B, hence both will be populated if you selected either one
			if (StringUtils.isNotBlank(wellName)) {
				hql = hql + " AND wellName=:wellName";
				paramNames.add("wellName");
				paramValues.add(wellName);
				String quickHql = "SELECT uwi FROM WellExternalCatalog WHERE (isDeleted=0 OR isDeleted IS NULL) AND wellName=:wellName";
				List<?> referencedUwi = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(quickHql,"wellName",wellName);
				commandBean.getRoot().getDynaAttr().put("uwi", referencedUwi.get(0));
				this.selectedWellUwiBefore = true;
			}
			if (StringUtils.isNotBlank(uwi)) {
				hql = hql + " AND uwi=:uwi";
				paramNames.add("uwi");
				paramValues.add(uwi);
				String quickHql = "SELECT wellName FROM WellExternalCatalog WHERE (isDeleted=0 OR isDeleted IS NULL) AND uwi=:uwi";
				List<?> referencedUwiWellName = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(quickHql,"uwi",uwi);
				commandBean.getRoot().getDynaAttr().put("wellName", referencedUwiWellName.get(0));
				this.selectedWellUwiBefore = true;
			}
			
			if (StringUtils.isNotBlank(country)) {
				hql = hql + " AND country=:country";
				paramNames.add("country");
				paramValues.add(country);
			}
			
			if (StringUtils.isNotBlank(status)) {
				String[] statusesToFilter = null;
				// if you pick "Yes" or "No" it will fetch a list of specific statuses
				if(status.equals(NOT_IMPORTED) || status.equals(IMPORTED)) {
					if(status.equals(NOT_IMPORTED)) 
						statusesToFilter = this.nonImportedStatuses;
					if(status.equals(IMPORTED))
						statusesToFilter = this.importedStatuses;
					
					hql = hql + " AND (status IN (";
					String delimiter = "";
					for(String statusToFilter : statusesToFilter) {
						hql += delimiter+":"+statusToFilter;
						paramNames.add(statusToFilter);
						paramValues.add(statusToFilter);
						delimiter = ",";
					}
					hql += ") ";
					if(status.equals(NOT_IMPORTED)) 
						hql += "OR status IS NULL ";
					hql += ") ";
				// else if the others, we will just add the status to the hql
				} else {
					hql += " AND status=:status";
					paramNames.add("status");
					paramValues.add(status);
				}
			}
			
			if (StringUtils.isNotBlank(onOffShore)) {
				hql = hql + " AND onOffShore=:onOffShore";
				paramNames.add("onOffShore");
				paramValues.add(onOffShore);
			}
			
			if (StringUtils.isNotBlank(uwbi)) {
				hql = hql + " AND uwbi=:uwbi";
				paramNames.add("uwbi");
				paramValues.add(uwbi);
			}
			
			if (StringUtils.isNotBlank(field)) {
				hql = hql + " AND field=:field";
				paramNames.add("field");
				paramValues.add(field);
			}
			// finally order by wellName
			hql = hql + " ORDER BY wellName";
			
			if (pagination!=null && pagination.isEnabled()) {
				qp.setRowsToFetch(pagination.getCurrentPage() * pagination.getRowsPerPage(), pagination.getRowsPerPage());
			}
			// fetch
			List<?> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, paramNames, paramValues, qp);
			List<Object> output_maps = new ArrayList<Object>();
			// add to the output maps the fetched WECs
			for(Object obj: results){
				WellExternalCatalog wec = (WellExternalCatalog) obj;
				output_maps.add(wec);
			}
			
			Long totalRows = new Long(-1);
			List<Object> results2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, paramNames, paramValues);
			
			if (results2.size()>0) {
				totalRows = Long.valueOf(results2.size());
			}
			
			if (pagination!=null && pagination.isEnabled()) {
				commandBean.getInfo().getPagination().setTotalRows(totalRows.intValue());
				commandBean.getInfo().getPagination().setRowsPerPage(pagination.getRowsPerPage());
				commandBean.getInfo().getPagination().setCurrentPage(pagination.getCurrentPage());
				commandBean.getInfo().getPagination().setEnabled(pagination.isEnabled());
			}
			
			return output_maps;
		}
		return null;
	}
}
