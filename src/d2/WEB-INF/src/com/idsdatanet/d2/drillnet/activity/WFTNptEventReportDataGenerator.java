package com.idsdatanet.d2.drillnet.activity;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


public class WFTNptEventReportDataGenerator implements ReportDataGenerator{
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		QueryProperties qp = new QueryProperties();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		
		String strSql = "SELECT SUM(activityDuration/3600.0) AS duration, operationUid, wellUid " +
				"FROM Activity " +
				"WHERE isDeleted IS NULL " +
				"AND (dayPlus IS NULL OR dayPlus=0) " +
				"AND (isSimop <> 1 OR isSimop IS NULL) " +
				"AND (isOffline <> 1 OR isOffline IS NULL) " +
				"AND operationUid = :currentOperationUid ";
		String[] paramsFields = {"currentOperationUid"};
		Object[] paramsValues = {currentOperationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues,qp);
		
		if (lstResult.size() > 0){
			for(Object objResult: lstResult){
				Object[] objDuration = (Object[]) objResult;
				Double totalDuration = 0.0;
				String operationUid = "";
				String wellUid = "";
				ReportDataNode thisReportNode = reportDataNode.addChild("Activity");
				if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())){ totalDuration = Double.parseDouble(objDuration[0].toString());}
				if (objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())){ operationUid = objDuration[1].toString();}
				if (objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())){ wellUid = objDuration[2].toString();}
				thisReportNode.addProperty("sumActivityDuration", totalDuration.toString());
				thisReportNode.addProperty("operationUid", operationUid.toString());
				thisReportNode.addProperty("wellUid", wellUid.toString());
				
				String stringSql = "SELECT SUM(activityDuration/3600.0) AS duration, operationUid, wellUid " +
						"FROM Activity " +
						"WHERE isDeleted IS NULL " +
						"AND (dayPlus IS NULL OR dayPlus=0) " +
						"AND (isSimop <> 1 OR isSimop IS NULL) " +
						"AND (isOffline <> 1 OR isOffline IS NULL) " +
						"AND operationUid = :currentOperationUid " +
						"AND (internalClassCode = 'TP' OR internalClassCode = 'TU') " +
						"AND rootCauseCode IS NOT NULL ";
				String[] paramsField = {"currentOperationUid"};
				Object[] paramsValue = {currentOperationUid};
				List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(stringSql, paramsField, paramsValue,qp);
				
				if (result.size() > 0){
					for(Object objResults: result){
						Object[] objDurations = (Object[]) objResults;
						Double nptDuration = 0.0;
						if (objDurations[0] != null && StringUtils.isNotBlank(objDurations[0].toString())){ nptDuration = Double.parseDouble(objDurations[0].toString());}
						thisReportNode.addProperty("sumNptDuration", nptDuration.toString());
					}
				}
			}
		}
		
		String strSql1 = "SELECT rootCauseCode, SUM(activityDuration/3600.0) AS duration " +
				"FROM Activity " +
				"WHERE isDeleted IS NULL " +
				"AND (dayPlus IS NULL OR dayPlus=0) " +
				"AND (isSimop <> 1 OR isSimop IS NULL) " +
				"AND (isOffline <> 1 OR isOffline IS NULL) " +
				"AND operationUid = :currentOperationUid " +
				"AND (internalClassCode = 'TP' OR internalClassCode = 'TU') " +
				"AND rootCauseCode IS NOT NULL " +
				"GROUP BY rootCauseCode";
		String[] paramsFields1 = {"currentOperationUid"};
		Object[] paramsValues1 = {currentOperationUid};
		List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1,qp);
		
		if (lstResult1.size() > 0){
			for(Object objResult1: lstResult1){
				Object[] objLength = (Object[]) objResult1;
				String rootCauseCode = "";
				Double duration = 0.0;
				String nptCode = "";
				ReportDataNode thisReportNode = reportDataNode.addChild("NptEvent");
				if (objLength[0] != null && StringUtils.isNotBlank(objLength[0].toString())){
					nptCode = StringUtils.substring(objLength[0].toString(),0,4);
					
					String strSql2 = "SELECT lookupLabel " +
							"FROM CommonLookup " +
							"WHERE isDeleted IS NULL " +
							"AND (shortCode = :nptCode) " +
							"AND (lookupTypeSelection = 'rooNptCode')";
					String[] paramsFields2 = {"nptCode"};
					Object[] paramsValues2 = {nptCode};
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
					if (lstResult2.size()>0)
					{
						Object lookupRCCodeResult = (Object) lstResult2.get(0);
						if(lookupRCCodeResult != null) rootCauseCode = lookupRCCodeResult.toString();
						thisReportNode.addProperty("code", nptCode.toString());
						thisReportNode.addProperty("label", rootCauseCode.toString());
					}
				}
				if (objLength[1] != null && StringUtils.isNotBlank(objLength[1].toString())){ duration = Double.parseDouble(objLength[1].toString());}		

				
				thisReportNode.addProperty("duration", duration.toString());
			}
		}
	}
}