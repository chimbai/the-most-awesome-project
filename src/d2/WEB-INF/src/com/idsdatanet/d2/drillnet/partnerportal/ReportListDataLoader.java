package com.idsdatanet.d2.drillnet.partnerportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.filemanager.FileManagerUtils;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.FileManagerFiles;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ReportListDataLoader implements DataNodeLoadHandler{
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		Object object = node.getData();
		if(object instanceof Operation){
			Operation thisOperation = (Operation) object;
		
			if(meta.getTableClass().equals(PublicDDRReportList.class)){
				String[] paramsFields = {"thisOperation"};
				String[] paramsValues = {thisOperation.getOperationUid()};
				List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles WHERE (isDeleted = false or isDeleted is null) and reportOperationUid=:thisOperation AND reportType='DDR' AND isPrivate = false ORDER BY reportDayDate DESC", paramsFields, paramsValues);;
				List<Object> output_maps = new ArrayList<Object>();
								
				Map<String,String> local_operation_name_cache = new HashMap<String,String>();
				for(Iterator i = items.iterator(); i.hasNext(); ){
					Object obj_array = (Object) i.next();
					PublicDDRReportList ddr = new PublicDDRReportList();
					ddr.setData((ReportFiles) obj_array, CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), thisOperation.getOperationUid(), local_operation_name_cache));
					output_maps.add(ddr);					
				}
				
				return output_maps;
			}
			else if(meta.getTableClass().equals(PublicDGRReportList.class))
			{
				String[] paramsFields = {"thisOperation"};
				String[] paramsValues = {thisOperation.getOperationUid()};
				List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles WHERE (isDeleted = false or isDeleted is null) and reportOperationUid=:thisOperation AND reportType='DGR' AND isPrivate = false ORDER BY reportDayDate DESC", paramsFields, paramsValues);;
				List<Object> output_maps = new ArrayList<Object>();
				
				Map<String,String> local_operation_name_cache = new HashMap<String,String>();
				for(Iterator i = items.iterator(); i.hasNext(); ){
					Object obj_array = (Object) i.next();
					PublicDGRReportList dgr = new PublicDGRReportList();
					dgr.setData((ReportFiles) obj_array, CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), thisOperation.getOperationUid(), local_operation_name_cache));
					output_maps.add(dgr);					
				}
				return output_maps;
			}
			else if(meta.getTableClass().equals(PublicUploadedFileList.class))
			{
				List<FileManagerFiles> items = FileManagerUtils.getPublicUploadedFilesForPartnerPortal(thisOperation.getOperationUid());
				List<Object> output_maps = new ArrayList<Object>();
				
				for(FileManagerFiles f : items){
					PublicUploadedFileList filemanager = new PublicUploadedFileList();
					filemanager.setData(f);
					output_maps.add(filemanager);					
				}
				return output_maps;
			}
			else if(meta.getTableClass().equals(PublicOtherReportList.class))
			{
				String[] paramsFields = {"thisOperation"};
				String[] paramsValues = {thisOperation.getOperationUid()};
				List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles WHERE (isDeleted = false or isDeleted is null) and reportOperationUid=:thisOperation AND (reportType <> 'DGR' AND reportType <> 'DDR' AND reportType <> 'MGT' AND reportType <> 'MGT_GEO') AND isPrivate = false ORDER BY reportDayDate DESC", paramsFields, paramsValues);
				List<Object> output_maps = new ArrayList<Object>();
				
				Map<String,String> local_operation_name_cache = new HashMap<String,String>();
				for(Iterator i = items.iterator(); i.hasNext(); ){
					Object obj_array = (Object) i.next();
					PublicOtherReportList othReport = new PublicOtherReportList();
					othReport.setData((ReportFiles) obj_array, CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), thisOperation.getOperationUid(), local_operation_name_cache));
					output_maps.add(othReport);					
				}
				return output_maps;
			}
			else{
				return null;
			}
		}
		else return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		if(meta.getTableClass().equals(Operation.class)) return false;
		else return true;
	}

}
