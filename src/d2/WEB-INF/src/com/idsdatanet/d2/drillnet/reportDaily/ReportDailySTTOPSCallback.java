package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.stt.STTTownSideCallback;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ReportDailySTTOPSCallback implements STTTownSideCallback {
	
	public void afterSendToTownCompleted(UserSelectionSnapshot userSelection) {
		try {
			String strSql = "FROM Daily WHERE (isDeleted = false or isDeleted is null) AND operationUid=:thisOperationUid";
			String[] paramsFields = {"thisOperationUid"};
			Object[] paramsValues = {userSelection.getOperationUid()};
			List<Daily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			for (Daily daily : lstResult){
				CommonUtil.getConfiguredInstance().updateDailyCostFromCostNet(daily.getDailyUid());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
