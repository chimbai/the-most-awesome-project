package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class FirstNptPhaseNptDurationBreakdownByRootCauseCodeReportDataGenerator implements ReportDataGenerator {
		private List<String> nptCode;
		private List<String> phaseCode;
	
		public void disposeOnDataGeneratorExit() {
			// TODO Auto-generated method stub	
		}
		
		public void setNptCode(List<String> nptCode)
		{
			if(nptCode != null){
				this.nptCode = new ArrayList<String>();
				for(String value: nptCode){
					this.nptCode.add(value.trim().toUpperCase());
				}
			}
		}
		
		public List<String> getNptCode() {
			return this.nptCode;
		}
		
		public void setPhaseCode(List<String> phaseCode)
		{
			if(phaseCode != null){
				this.phaseCode = new ArrayList<String>();
				for(String value: phaseCode){
					this.phaseCode.add(value.trim().toUpperCase());
				}
			}
		}
		
		public List<String> getPhaseCode() {
			return this.phaseCode;
		}
		
		public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		// get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		
		Date todayDate = daily.getDayDate();
		String nptClassCodeList = "";
		String phaseCodeList = "";
		String strSql = "";
		String firstNptPhase = "";
		CustomFieldUom durationConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activityDuration");
		
		if(this.getNptCode() != null){
			for(String value:this.nptCode){
				nptClassCodeList = nptClassCodeList + "'" + value.toString() + "',";
			}
			
			nptClassCodeList = StringUtils.substring(nptClassCodeList, 0, -1);
			nptClassCodeList = StringUtils.upperCase(nptClassCodeList);
		}
		
		if(this.getPhaseCode() != null){
			for(String value:this.phaseCode){
				phaseCodeList = phaseCodeList + "'" + value.toString() + "',";
			}
			
			phaseCodeList = StringUtils.substring(phaseCodeList, 0, -1);
			phaseCodeList = StringUtils.upperCase(phaseCodeList);
		}
			
		// Query 1st NPT Phase Code of today's Activities
		strSql = "FROM Activity " +
				 "WHERE (isDeleted IS FALSE OR isDeleted IS NULL) " +
				 "AND classCode IN (" + nptClassCodeList + ") " +
				 "AND dailyUid = :dailyUid " +
				 "AND ((dayPlus IS NULL OR dayPlus=0) AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL)) " +
				 "ORDER BY dayPlus, startDatetime";
		List currentDayActivityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", daily.getDailyUid(),qp);
			
		if(currentDayActivityList != null && currentDayActivityList.size() > 0){
			Activity firstNptActivity = (Activity)currentDayActivityList.get(0);
			if(firstNptActivity != null){
				firstNptPhase = firstNptActivity.getPhaseCode();
			}
		}
			
		if(StringUtils.isNotBlank(firstNptPhase)){
			String phaseName = "N/A";
			String phaseCode = "N/A";
			
			for(String value:this.phaseCode){
				if(StringUtils.equals(firstNptPhase, value)){
					phaseCode = value;
					break;
				}
			}
			
			if(!StringUtils.equals(phaseCode,"N/A")){
				phaseName = getPhaseName(firstNptPhase,userContext.getUserSelection().getOperationType(),qp);
			}
			else{
				phaseName = "Other";
			}
				
			ReportDataNode thisReportNode = reportDataNode.addChild("currentNptPhase");
			thisReportNode.addProperty("shortCode",phaseCode);
			thisReportNode.addProperty("name",phaseName.trim().toUpperCase());
					
			if(!StringUtils.equals(phaseCode,"N/A")){
				strSql = "SELECT a.phaseCode, a.rootCauseCode, lrcc.name, SUM(a.activityDuration) as totalDuration FROM Activity a, Daily d, LookupRootCauseCode lrcc " +
			 		     "WHERE (a.isDeleted IS FALSE OR a.isDeleted IS NULL) " +
			 		     "AND (d.isDeleted IS FALSE OR d.isDeleted IS NULL) " +
			 		     "AND (lrcc.isDeleted IS FALSE OR lrcc.isDeleted IS NULL) " +
			 		     "AND a.dailyUid = d.dailyUid " +
			 		     "AND a.operationUid = :thisOperationUid " +
			 		     "AND d.dayDate <= :userDate " +
			 		     "AND a.phaseCode = :phaseCode " +
			 		     "AND a.classCode IN (" + nptClassCodeList + ") " +
			 		     "AND a.rootCauseCode = lrcc.shortCode " +
			 		     "AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " +
			 		     "GROUP BY a.rootCauseCode";
			}
			else{
				strSql = "SELECT a.phaseCode, a.rootCauseCode, lrcc.name, SUM(a.activityDuration) as totalDuration FROM Activity a, Daily d, LookupRootCauseCode lrcc " +
			 		     "WHERE (a.isDeleted IS FALSE OR a.isDeleted IS NULL) " +
			 		     "AND (d.isDeleted IS FALSE OR d.isDeleted IS NULL) " +
			 		     "AND (lrcc.isDeleted IS FALSE OR lrcc.isDeleted IS NULL) " +
			 		     "AND a.dailyUid = d.dailyUid " +
			 		     "AND a.operationUid = :thisOperationUid " +
			 		     "AND d.dayDate <= :userDate " +
			 		     "AND a.phaseCode NOT IN (" + phaseCodeList + ") " +
			 		     "AND a.classCode IN (" + nptClassCodeList + ") " +
			 		     "AND a.rootCauseCode = lrcc.shortCode " +
			 		     "AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " +
			 		     "GROUP BY a.rootCauseCode";
			}
			
			List lstResult = null;
			
			if(!StringUtils.equals(phaseCode,"N/A")){
				String[] paramsFields = {"thisOperationUid","userDate","phaseCode"};
				Object[] paramsValues = {userContext.getUserSelection().getOperationUid(),todayDate,phaseCode};
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			}
			else{
				String[] paramsFields = {"thisOperationUid","userDate"};
				Object[] paramsValues = {userContext.getUserSelection().getOperationUid(),todayDate};
			    lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			}
			
			if(lstResult != null && lstResult.size() > 0){
				Double totalNptDuration = 0.0;
				
				for(Object objResult: lstResult){
					Object[] obj = (Object[]) objResult;
					
					if(obj[3]!=null && StringUtils.isNotBlank(obj[3].toString())) totalNptDuration += Double.parseDouble(obj[3].toString());
				}
				
				if(totalNptDuration != null){
					durationConverter.setBaseValue(totalNptDuration);
					totalNptDuration = durationConverter.getConvertedValue();
				}
				
				for(Object objResult: lstResult){
					Object[] obj = (Object[]) objResult;
					
					String shortCode = "N/A";
					String rootCauseCode = "N/A";
					String rootCauseName = "N/A";
					Double duration = 0.0;
					
					if(obj[0]!=null && StringUtils.isNotBlank(obj[0].toString())) shortCode = obj[0].toString();
					if(obj[1]!=null && StringUtils.isNotBlank(obj[1].toString())) rootCauseCode = obj[1].toString();
					if(obj[2]!=null && StringUtils.isNotBlank(obj[2].toString())) rootCauseName = obj[2].toString();
					if(obj[3]!=null && StringUtils.isNotBlank(obj[3].toString())) duration = Double.parseDouble(obj[3].toString());
					
					if(duration != null){
						durationConverter.setBaseValue(duration);
						duration = durationConverter.getConvertedValue();
					}
					
					thisReportNode = reportDataNode.addChild("currentPhaseNptBreakdown");
					thisReportNode.addProperty("phaseCode",shortCode);
					thisReportNode.addProperty("rootCauseCode",rootCauseCode);
					thisReportNode.addProperty("series",rootCauseName + " (" + rootCauseCode + ")");
					thisReportNode.addProperty("duration", duration.toString());
					thisReportNode.addProperty("totalDuration", totalNptDuration.toString());
				}
			}
		}
	}	
		
	private String getPhaseName(String code, String operationType, QueryProperties qp) throws Exception{
		String phaseName = "N/A";
		
		//Query phase name
		String strSql = "SELECT name FROM LookupPhaseCode " +
				 "WHERE (isDeleted IS FALSE OR isDeleted IS NULL) " +
				 "AND shortCode = :shortCode " +
				 "AND (operationCode = :thisOperationType OR operationCode = '')";
		String[] paramsFields = {"shortCode","thisOperationType"};
		Object[] paramsValues = {code, operationType};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			Object lookupPhaseCodeResult = (Object) lstResult.get(0);
			
			if(lookupPhaseCodeResult != null){
				phaseName = lookupPhaseCodeResult.toString();
			}
		}
		
		return phaseName;
	}
}