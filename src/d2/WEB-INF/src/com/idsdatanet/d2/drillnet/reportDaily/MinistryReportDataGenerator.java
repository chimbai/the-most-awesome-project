package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class MinistryReportDataGenerator implements ReportDataGenerator {
	
	private String casingOdLookup = null;
	private String casingHoleSizeLookup = null;
	
	public void setCasingOdLookup(String value) {
		casingOdLookup = value;
	}
	
	public String getCasingOdLookup() {
		return casingOdLookup;
	}
	
	public void setCasingHoleSizeLookup(String value) {
		casingHoleSizeLookup = value;
	}
	
	public String getCasingHoleSizeLookup() {
		return casingHoleSizeLookup;
	}

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double depthMdMsl = 0.00;
		Double depthTvdMsl = 0.00;
		Double currentHoleSize = 0.00;
		String currentDepthMdMsl = "";
		String currentDepthMdMslUom = "";
		String currentDepthTvdMsl = "";
		String currentDepthTvdMslUom = "";
		String thisReportType = null;
		
		String thisWellboreUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getWellboreUid().toString())) thisWellboreUid = userContext.getUserSelection().getWellboreUid().toString();		
		
		String thisOperationUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getOperationUid().toString())) thisOperationUid = userContext.getUserSelection().getOperationUid().toString();
		
		String thisDailyUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getDailyUid().toString())) thisDailyUid = userContext.getUserSelection().getDailyUid().toString();
		
		if (thisReportType==null) thisReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(thisOperationUid);
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "depthMdMsl");
		
		//Get Midnight depth and last casing shoe md
		String strSql = "FROM ReportDaily WHERE (isDeleted is null or isDeleted = false) and operationUid =:thisOperationUid and reportType =:thisReportType and dailyUid =:thisDailyUid";
		String[] paramsFields = {"thisOperationUid", "thisReportType", "thisDailyUid"};
		Object[] paramsValues = {thisOperationUid, thisReportType, thisDailyUid};
		
		List<ReportDaily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if (lstResult.size() > 0) {
			ReportDaily thisReportDaily = lstResult.get(0);
			
			if(thisReportDaily != null){
				
				if (thisReportDaily.getDepthMdMsl() != null) {
					thisConverter.setBaseValue(thisReportDaily.getDepthMdMsl());
					depthMdMsl = thisConverter.getConvertedValue();
					currentDepthMdMsl = depthMdMsl.toString();
					currentDepthMdMslUom = thisConverter.getUomSymbol();
				}
				
				if (thisReportDaily.getDepthTvdMsl() != null) {
					thisConverter.setReferenceMappingField(ReportDaily.class, "depthTvdMsl");
					thisConverter.setBaseValue(thisReportDaily.getDepthTvdMsl());
					depthTvdMsl = thisConverter.getConvertedValue();
					currentDepthTvdMsl = depthTvdMsl.toString();
					currentDepthTvdMslUom = thisConverter.getUomSymbol();
				}
				
				if (thisReportDaily.getLastHolesize() != null) {
					thisConverter.setReferenceMappingField(ReportDaily.class, "lastHolesize");
					thisConverter.setBaseValue(thisReportDaily.getLastHolesize());
					currentHoleSize = thisConverter.getBasevalue();
				}
			}
		}
		
		//QUERY BIT
		/*Double currentBitDiameter = null;
		
		String strSql3 = "SELECT b.bitDiameter, b.secOd FROM Bitrun b, Bharun br WHERE br.bharunUid = b.bharunUid AND (b.isDeleted = false OR b.isDeleted is null) AND (br.isDeleted = false OR br.isDeleted is null) AND br.depthOutMdMsl <= :thisDepthMdMsl AND b.operationUid = :thisOperationUid AND (b.primaryfunction <> 'ream' OR b.primaryfunction = '' OR b.primaryfunction is null) ORDER BY b.bitDiameter DESC";
		String[] paramsFields3 = {"thisOperationUid","thisDepthMdMsl"};
		Object[] paramsValues3 = {thisOperationUid,depthMdMsl};
		
		List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3, qp);
		*/	
		//QUERY CASING OD FROM CASING SECTION
		String strSql2 = "FROM CasingSection WHERE wellboreUid = :thisWellboreUid AND (isDeleted = false OR isDeleted is null) GROUP BY casingOd ORDER BY casingOd DESC, shoeTopMdMsl";
		String[] paramsFields2 = {"thisWellboreUid"};
		Object[] paramsValues2 = {thisWellboreUid};
		
		List<CasingSection> casingList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
		
		//LOOP CASING SECTION
		for(int i = 0;i<=casingList.size()-1;i++){
			
			Double currentCasingOd = null;
			Double currentOpenHoleId = null;
			Double currentShoeTopMdMsl = null;
			Double currentShoeTopTvdMsl = null;
			Double currentPlannedShoeTopMdMsl = null;
			Double currentPlannedShoeTopTvdMsl = null;
			Double openHoleDepthMdMsl = null;
			Double fit = null;
			Double lot = null;
			
			String sequence = "";
			String casingOd = "";
			String openHoleId = "";
			String openHoleIdNumOnly = "";
			String openHoleIdNumOnlyUom = "";
			String shoeTopMdMsl = "";
			String shoeTopMdMslUom = "";
			String shoeTopTvdMsl = "";
			String shoeTopTvdMslUom = "";
			String plannedShoeTopMdMsl = "";
			String plannedShoeTopMdMslUom = "";
			String plannedShoeTopTvdMsl = "";
			String plannedShoeTopTvdMslUom = "";
			String drivenBit = "";
			String finalOpenHoleDepthMdMsl = "";
			String finalOpenHoleDepthMdMslUom = "";
			
			ReportDataNode thisReportNode = reportDataNode.addChild("MinistryBorehole");
			
			String fitlotcommon = "";
			String fitLotCommonNumOnly = "";
			String fitLotCommonNumOnlyUom = "";
			String checkFitLot = "";
			
			CasingSection currentCasing = casingList.get(i);
			sequence = (i+1)+"";
			
			if (currentCasing.getFit() != null && StringUtils.isNotBlank(currentCasing.getFit().toString())) fit = currentCasing.getFit();
			if (currentCasing.getLot() != null && StringUtils.isNotBlank(currentCasing.getLot().toString())) lot = currentCasing.getLot();			
			
			if (fit != null) {				
				if (fit != 0) {
					thisConverter.setReferenceMappingField(CasingSection.class, "fit");
					thisConverter.setBaseValue(fit);
					fitlotcommon = thisConverter.getFormattedValue() + "(F)";
					
					//OP-4967 -> output fit without uom precision
					fit = thisConverter.getConvertedValue();
					fitLotCommonNumOnly = fit.toString();
					checkFitLot = "(F)";
					fitLotCommonNumOnlyUom = thisConverter.getUomSymbol();
					
				} else if (lot != null) {
					if (lot != 0) {
						thisConverter.setReferenceMappingField(CasingSection.class, "lot");
						thisConverter.setBaseValue(lot);
						fitlotcommon = thisConverter.getFormattedValue() + "(L)";
						
						//OP-4967 -> output lot without uom precision
						lot = thisConverter.getConvertedValue();
						fitLotCommonNumOnly = lot.toString();
						checkFitLot = "(L)";
						fitLotCommonNumOnlyUom = thisConverter.getUomSymbol();
					}
				}				
			} else if (lot != null) {				
				if (lot != 0) {
					thisConverter.setReferenceMappingField(CasingSection.class, "lot");
					thisConverter.setBaseValue(lot);
					fitlotcommon = thisConverter.getFormattedValue() + "(L)";
					
					//OP-4967 -> output lot without uom precision
					lot = thisConverter.getConvertedValue();
					fitLotCommonNumOnly = lot.toString();
					checkFitLot = "(L)";
					fitLotCommonNumOnlyUom = thisConverter.getUomSymbol();
					
				} else if (fit != null) {
					if (fit != 0) {
						thisConverter.setReferenceMappingField(CasingSection.class, "fit");
						thisConverter.setBaseValue(fit);
						fitlotcommon = thisConverter.getFormattedValue() + "(F)";
						
						//OP-4967 -> output fit without uom precision
						fit = thisConverter.getConvertedValue();
						fitLotCommonNumOnly = fit.toString();
						checkFitLot = "(F)";
						fitLotCommonNumOnlyUom = thisConverter.getUomSymbol();
					}
				}				
			} else {				
				fitlotcommon = "";	
				fitLotCommonNumOnly = "";	
			}
			
			//Output casingOd to uom precision, it will need to lookup label from lookup.xml
			if (currentCasing.getCasingOd() != null && StringUtils.isNotBlank(currentCasing.getCasingOd().toString())) {
				currentCasingOd = currentCasing.getCasingOd();
				
				thisConverter.setReferenceMappingField(CasingSection.class, "casingOd");
				thisConverter.setBaseValue(currentCasingOd);
				casingOd = thisConverter.formatOutputPrecision();
				
				if (this.casingOdLookup!=null) {
					Map<String, LookupItem> casingLookup = LookupManager.getConfiguredInstance().getLookup(this.casingOdLookup, userContext.getUserSelection(), new LookupCache());
					if (casingLookup.containsKey(casingOd.toString())) {
						LookupItem item = casingLookup.get(casingOd.toString());
						casingOd = item.getValue().toString();
					} else {
						casingOd = casingOd.toString();
					}
				} else {
					casingOd = casingOd.toString();
				}
			}
			
			//Output casingOd to uom precision, it will need to lookup label from lookup.xml
			if (currentCasing.getOpenHoleId() != null && StringUtils.isNotBlank(currentCasing.getOpenHoleId().toString())) { 
				currentOpenHoleId = currentCasing.getOpenHoleId();
				thisConverter.setReferenceMappingField(CasingSection.class, "openHoleId");
				thisConverter.setBaseValue(currentOpenHoleId);
				openHoleId = thisConverter.formatOutputPrecision();
				
				//OP-4967 -> output openHoleId without uom precision
				currentOpenHoleId = thisConverter.getConvertedValue();
				openHoleIdNumOnly = currentOpenHoleId.toString();
				openHoleIdNumOnlyUom = thisConverter.getUomSymbol();
			
				if (this.casingHoleSizeLookup!=null) {
					Map<String, LookupItem> casingHoleSizeLookup = LookupManager.getConfiguredInstance().getLookup(this.casingHoleSizeLookup, userContext.getUserSelection(), new LookupCache());
					if (casingHoleSizeLookup.containsKey(openHoleId.toString())) {
						LookupItem item = casingHoleSizeLookup.get(openHoleId.toString());
						openHoleId = item.getValue().toString();
					} else {
						openHoleId = openHoleId.toString();
					}
				} else {
					openHoleId = openHoleId.toString();
				}
			}
			
			//Get PlannedShoeTop From Casing
			if (currentCasing.getPlannedShoeTopMdMsl() != null && StringUtils.isNotBlank(currentCasing.getPlannedShoeTopMdMsl().toString())) {
				currentPlannedShoeTopMdMsl = currentCasing.getPlannedShoeTopMdMsl();
				
				thisConverter.setReferenceMappingField(CasingSection.class, "plannedShoeTopMdMsl");
				thisConverter.setBaseValue(currentPlannedShoeTopMdMsl);
				currentPlannedShoeTopMdMsl = thisConverter.getConvertedValue();
				plannedShoeTopMdMsl = currentPlannedShoeTopMdMsl.toString();
				plannedShoeTopMdMslUom = thisConverter.getUomSymbol();
			}

			//Get PlannedShoeTopTvdMsl From Casing
			if (currentCasing.getPlannedShoeTopTvdMsl() != null && StringUtils.isNotBlank(currentCasing.getPlannedShoeTopTvdMsl().toString())) {
				currentPlannedShoeTopTvdMsl = currentCasing.getPlannedShoeTopTvdMsl();
				
				thisConverter.setReferenceMappingField(CasingSection.class, "plannedShoeTopTvdMsl");
				thisConverter.setBaseValue(currentPlannedShoeTopTvdMsl);
				currentPlannedShoeTopTvdMsl = thisConverter.getConvertedValue();
				plannedShoeTopTvdMsl = currentPlannedShoeTopTvdMsl.toString();
				plannedShoeTopTvdMslUom = thisConverter.getUomSymbol();
			}
			
			//Get ShoeTop From Casing
			if (currentCasing.getShoeTopMdMsl() != null && StringUtils.isNotBlank(currentCasing.getShoeTopMdMsl().toString())) { 
				currentShoeTopMdMsl = currentCasing.getShoeTopMdMsl();
			
				thisConverter.setReferenceMappingField(CasingSection.class, "shoeTopMdMsl");
				thisConverter.setBaseValue(currentShoeTopMdMsl);
				currentShoeTopMdMsl = thisConverter.getConvertedValue();
				shoeTopMdMsl = currentShoeTopMdMsl.toString();
				shoeTopMdMslUom = thisConverter.getUomSymbol();
			}

			//Get ShoeTopTvdMsl From Casing
			if (currentCasing.getShoeTopTvdMsl() != null && StringUtils.isNotBlank(currentCasing.getShoeTopTvdMsl().toString())) { 
				currentShoeTopTvdMsl = currentCasing.getShoeTopTvdMsl();
			
				thisConverter.setReferenceMappingField(CasingSection.class, "shoeTopTvdMsl");
				thisConverter.setBaseValue(currentShoeTopTvdMsl);
				currentShoeTopTvdMsl = thisConverter.getConvertedValue();
				shoeTopTvdMsl = currentShoeTopTvdMsl.toString();
				shoeTopTvdMslUom = thisConverter.getUomSymbol();
			}
			
			//If casing od greater than hole size
			if (currentCasingOd != null && currentOpenHoleId != null) {
				if (currentCasingOd >= currentOpenHoleId) drivenBit = "DRIVEN";
			}
			/*if (currentCasingOd != null) {
				if(lstResult3.size() > 0) {
					for (Object result : lstResult3) {
						Object[] thisBitResult = (Object[]) result;
						
						if (thisBitResult[0] != null && StringUtils.isNotBlank(thisBitResult[0].toString())) {
							currentBitDiameter = Double.parseDouble(thisBitResult[0].toString());
							//if casing od greater or equal than bit diameter then set it to DRIVEN
							if (currentBitDiameter != null) {
								if (currentCasingOd > currentBitDiameter) {
									drivenBit = "DRIVEN";
								}
							}
						}
					}
				}
			}*/
			
			//Get Final Depth of each Casing
			if (currentCasing.getOpenHoleDepthMdMsl() != null && StringUtils.isNotBlank(currentCasing.getOpenHoleDepthMdMsl().toString())) {
				openHoleDepthMdMsl = currentCasing.getOpenHoleDepthMdMsl();
				
				thisConverter.setReferenceMappingField(CasingSection.class, "openHoleDepthMdMsl");
				thisConverter.setBaseValue(openHoleDepthMdMsl);
				openHoleDepthMdMsl =thisConverter.getConvertedValue();
				finalOpenHoleDepthMdMsl = openHoleDepthMdMsl.toString();
				finalOpenHoleDepthMdMslUom = thisConverter.getUomSymbol();
			}
			
			//Get Midnight Depth
			if (currentHoleSize != null && currentOpenHoleId != null) {
				if (currentOpenHoleId < currentHoleSize) {
					currentDepthMdMsl = "";
					currentDepthTvdMsl = "";
				}
			}
			
			thisReportNode.addProperty("sequence", sequence);
			thisReportNode.addProperty("plannedShoeTopMdMsl", plannedShoeTopMdMsl.toString());
			thisReportNode.addProperty("plannedShoeTopMdMslUom", plannedShoeTopMdMslUom.toString());
			thisReportNode.addProperty("plannedShoeTopTvdMsl", plannedShoeTopTvdMsl.toString());
			thisReportNode.addProperty("plannedShoeTopTvdMslUom", plannedShoeTopTvdMslUom.toString());

			thisReportNode.addProperty("shoeTopMdMsl", shoeTopMdMsl.toString());
			thisReportNode.addProperty("shoeTopMdMslUom", shoeTopMdMslUom.toString());
			thisReportNode.addProperty("shoeTopTvdMsl", shoeTopTvdMsl.toString());
			thisReportNode.addProperty("shoeTopTvdMslUom", shoeTopTvdMslUom.toString());

			thisReportNode.addProperty("fitLotCommon", fitlotcommon);
			thisReportNode.addProperty("fitLotCommonNumOnly", fitLotCommonNumOnly);
			thisReportNode.addProperty("checkFitLot", checkFitLot);
			thisReportNode.addProperty("fitLotCommonNumOnlyUom", fitLotCommonNumOnlyUom);
			thisReportNode.addProperty("openHoleId", openHoleId.toString());
			thisReportNode.addProperty("openHoleIdNumOnly", openHoleIdNumOnly.toString());
			thisReportNode.addProperty("openHoleIdNumOnlyUom", openHoleIdNumOnlyUom.toString());
			thisReportNode.addProperty("casingOd", casingOd.toString());
			thisReportNode.addProperty("drivenBit", drivenBit.toString());
			thisReportNode.addProperty("finalOpenHoleDepthMdMsl", finalOpenHoleDepthMdMsl.toString());
			thisReportNode.addProperty("finalOpenHoleDepthMdMslUom", finalOpenHoleDepthMdMslUom.toString());
			thisReportNode.addProperty("currentDepthMdMsl", currentDepthMdMsl.toString());
			thisReportNode.addProperty("currentDepthMdMslUom", currentDepthMdMslUom.toString());
			thisReportNode.addProperty("currentDepthTvdMsl", currentDepthTvdMsl.toString());
			thisReportNode.addProperty("currentDepthTvdMslUom", currentDepthTvdMslUom.toString());
			
		}
	}
}
