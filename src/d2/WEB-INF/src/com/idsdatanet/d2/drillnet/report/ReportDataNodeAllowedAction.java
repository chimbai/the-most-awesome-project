package com.idsdatanet.d2.drillnet.report;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ReportDataNodeAllowedAction implements DataNodeAllowedAction {
	
	private String defaultReportType;
	private String reportGenerated;
	
	public void setReportType(String value){
		this.defaultReportType = value;
	}
	
	public void setReportGenerated(String value){
		this.reportGenerated = value;
	}
	
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		if("generateReport".equals(action)){
			String strSql = null;
			String strSql2;
			String[] multiReportType;
			String operationUid = session.getCurrentOperationUid();
			//if generate fwr, get the respective report type
			if ("FWR".equalsIgnoreCase(reportGenerated) || ("MGT".equalsIgnoreCase(reportGenerated))) {
				this.defaultReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
			}
			
			if (StringUtils.isNotBlank(this.defaultReportType)) {
				if ("MULTIWELL".equals(this.defaultReportType)){
					return true;
				} else {
					multiReportType = this.defaultReportType.split(",");
					for (int i=0; i<multiReportType.length; i++)
					{
						strSql2 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid =:operationUid AND reportType='" + multiReportType[i] + "'";
						List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "operationUid", session.getCurrentOperationUid());
						
						if (lstResult.size() > 0){
							return true;
						}
					}
				}
			} else {
				strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid =:operationUid AND reportType='DGR'";
			}
			
			if (StringUtils.isNotBlank(strSql))
			{
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", session.getCurrentOperationUid());
				
				if (lstResult.size() > 0){
					return true;
				}else{
					return false;
				}
			}
		} else if (action.equals(Action.DELETE) || action.equals(Action.EDIT)) {
			Object data = node.getData();
			if (data instanceof ReportOutputFile) {
				ReportOutputFile reportOutputFile = (ReportOutputFile) data;
				if (reportOutputFile.isWithoutActualReport()) {
					return false;
				}
			}
		}
		return true;
	}

}
