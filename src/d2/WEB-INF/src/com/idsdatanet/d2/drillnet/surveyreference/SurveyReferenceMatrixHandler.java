package com.idsdatanet.d2.drillnet.surveyreference;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.matrix.DefaultMatrixHandler;
import com.idsdatanet.d2.drillnet.matrix.Matrix;
import com.idsdatanet.d2.drillnet.matrix.MatrixHandler;
/**
 * 
 * @author RYAU
 *
 */
public class SurveyReferenceMatrixHandler extends DefaultMatrixHandler {
	
	public void process(Matrix matrix, UserSelectionSnapshot userSelection) throws Exception {
		boolean f = false;
		try {
			SurveyStation.class.getDeclaredField("isIncomplete");
			f = true;
		} catch (NoSuchFieldException e) {
			f = false;
		}
		String sql = "";
		if(f) {
			sql = "select surveyStation.dailyUid,surveyStation.isIncomplete from SurveyReference surveyReference, SurveyStation surveyStation where (surveyReference.isDeleted = false or surveyReference.isDeleted is null) and (surveyStation.isDeleted = false or surveyStation.isDeleted is null) and surveyReference.surveyReferenceUid=surveyStation.surveyReferenceUid and surveyReference.operationUid = :operationUid";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", userSelection.getOperationUid());
			
			if(list != null && list.size() > 0) {
				Set<String> linkedHashSet = new LinkedHashSet<String>();
				for(Object[] daily : list) {
					String dailyUid = null;
					if(daily[0] != null) {
						dailyUid = (String) daily[0];
					}
					linkedHashSet.add(dailyUid);
				}
				for(String id: linkedHashSet) {
					String dailyUid = null;
					String isIncomplete = null;
					for(Object[] daily : list) {
						if(daily[0] != null) {
							dailyUid = (String) daily[0];
							if(id.equals(dailyUid)){
								if(daily[1] != null) {
									if(isIncomplete != "true") isIncomplete = String.valueOf(daily[1]);
								}
							}
						}
					}
					matrix.addData(id,isIncomplete);
				}
			}
		}else{
			sql = "select distinct surveyStation.dailyUid from SurveyReference surveyReference, SurveyStation surveyStation where (surveyReference.isDeleted = false or surveyReference.isDeleted is null) and (surveyStation.isDeleted = false or surveyStation.isDeleted is null) and surveyReference.surveyReferenceUid=surveyStation.surveyReferenceUid and surveyReference.operationUid = :operationUid";
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", userSelection.getOperationUid());
			
			if(list.size() > 0) {
				for(String dailyUid : list) {
					matrix.addData(dailyUid,"N/A");
				}
			}
		}

	}
}
