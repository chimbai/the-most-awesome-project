package com.idsdatanet.d2.drillnet.drillingparameters;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.bharun.BharunUtils;
import com.idsdatanet.d2.drillnet.casingSection.CasingTallySummary;

public class DrillingParametersDataNodeListener extends EmptyDataNodeListener {
	
	//18115
//	private boolean torquecalculation = false;
	private boolean calwWobRop = false;
	public static boolean populateBhaDailySummaryTorqueAvg = false;
	
//	public void settorquecalculation(Boolean torquecalculation){
//		this.torquecalculation = torquecalculation;
//	}
	
	//p2
	public void setcalwWobRop(Boolean calwWobRop){
		this.calwWobRop = calwWobRop;
	}
	
	// OMV WKO - Ticket #19100
	public void setpopulateBhaDailySummaryTorqueAvg(Boolean populateBhaDailySummaryTorqueAvg){
		this.populateBhaDailySummaryTorqueAvg = populateBhaDailySummaryTorqueAvg;
	}

	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof DrillingParameters){
			
			DrillingParameters thisDrillParam = (DrillingParameters) object;
			
			
			this.includeBitRPM( node,  commandBean);
			
			String bitrunNumber = null;
			String thisOperationUid = userSelection.getOperationUid();
			
			String strSql = "SELECT distinct bitrun.bitrunNumber FROM Bitrun bitrun, BharunDailySummary bharunDailySummary WHERE (bitrun.isDeleted = false or bitrun.isDeleted is null) AND " + 
					"(bharunDailySummary.isDeleted = false or bharunDailySummary.isDeleted is null) " +
					"AND bitrun.bharunUid = :thisBharunUid AND bitrun.operationUid = :operationUid AND bitrun.bharunUid = bharunDailySummary.bharunUid";			
			String[] paramsFields2 = {"thisBharunUid", "operationUid"};
			String[] paramsValues2 = {thisDrillParam.getBharunUid(), thisOperationUid};
			
			List<String> Result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);			
			if (Result.size() > 0){
				
				for(String strBitrunNumber : Result){
					if(StringUtils.isNotBlank(strBitrunNumber)){
						if(bitrunNumber==null){
							bitrunNumber = strBitrunNumber.toString();
						}
						else{
							bitrunNumber = bitrunNumber + " , " + strBitrunNumber.toString();
						}
					}
				}				
			}
			node.getDynaAttr().put("bitrunNumber", bitrunNumber);
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "wobAvg");
			List lstResult;
			
			//get AVG(wobAvg) from drilling parameters by divide the total wobAvg with same bharun# that wobAvg not empty or 0  
			strSql = "SELECT AVG(a.wobAvg) as avgWob FROM DrillingParameters a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.wobAvg > 0";
			String[] paramsFields = {"BharunUid", "DailyUid"};
			String[] paramsValues = {thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid()};
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			//set query value to avgWob for return result not equal to null
			Object wobResult = (Object) lstResult.get(0);
			double avgWob = 0.00;
			if (wobResult != null){
				avgWob = Double.parseDouble(wobResult.toString());
			}
			
			//assign raw value to avgTotalWob
			thisConverter.setBaseValue(avgWob);
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@avgTotalWob", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("avgTotalWob", thisConverter.getConvertedValue());
			
			//get AVG(surfaceRpmAvg) from drilling parameters by divide the total surfaceRpmAvg with same bharun# that surfaceRpmAvg not empty or 0 
			strSql = "SELECT AVG(a.surfaceRpmAvg) as avgSurfaceRpm FROM DrillingParameters a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.surfaceRpmAvg > 0";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			//set query value to avgSurfaceRpm for return result not equal to null
			Object surfaceRpmResult = (Object) lstResult.get(0);
			double avgSurfaceRpm = 0.00;
			if (surfaceRpmResult != null){
				avgSurfaceRpm = Double.parseDouble(surfaceRpmResult.toString());
			}
			
			//assign raw value to avgTotalSurfaceRpm
			thisConverter.setReferenceMappingField(DrillingParameters.class, "surfaceRpmAvg");
			thisConverter.setBaseValue(avgSurfaceRpm);
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@avgTotalSurfaceRpm", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("avgTotalSurfaceRpm", thisConverter.getConvertedValue());
			
			//get AVG(dhRpmAvg) from drilling parameters by divide the total dhRpmAvg with same bharun# that dhRpmAvg not empty or 0  
			strSql = "SELECT AVG(a.dhRpmAvg) as avgDhRpm FROM DrillingParameters a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.dhRpmAvg > 0";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			//set query value to avgDhRpm for return result not equal to null
			Object dhRpmResult = (Object) lstResult.get(0);
			double avgDhRpm = 0.00;
			if (dhRpmResult != null){
				avgDhRpm = Double.parseDouble(dhRpmResult.toString());
			}
			
			//assign raw value to avgTotalDhRpm
			thisConverter.setReferenceMappingField(DrillingParameters.class, "dhRpmAvg");
			thisConverter.setBaseValue(avgDhRpm);
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@avgTotalDhRpm", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("avgTotalDhRpm", thisConverter.getConvertedValue());
			
			//get AVG(pressureAvg) from drilling parameters by divide the total pressureAvg with same bharun# that pressureAvg not empty or 0  
			strSql = "SELECT AVG(a.pressureAvg) as avgPressure FROM DrillingParameters a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.pressureAvg > 0";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			//set query value to avgPressure for return result not equal to null
			Object pressureResult = (Object) lstResult.get(0);
			double avgPressure = 0.00;
			if (pressureResult != null){
				avgPressure = Double.parseDouble(pressureResult.toString());
			}
			
			//assign raw value to avgTotalPressure
			thisConverter.setReferenceMappingField(DrillingParameters.class, "pressureAvg");
			thisConverter.setBaseValue(avgPressure);
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@avgTotalPressure", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("avgTotalPressure", thisConverter.getConvertedValue());
			
			//get AVG(flowAvg) from drilling parameters by divide the total flowAvg with same bharun# that flowAvg not empty or 0 
			strSql = "SELECT AVG(a.flowAvg) as avgFlow FROM DrillingParameters a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.flowAvg > 0";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			//set query value to avgFlow for return result not equal to null
			Object flowResult = (Object) lstResult.get(0);
			double avgFlow = 0.00;
			if (flowResult != null){
				avgFlow = Double.parseDouble(flowResult.toString());
			}
			
			//assign raw value to avgTotalFlow
			thisConverter.setReferenceMappingField(DrillingParameters.class, "flowAvg");
			thisConverter.setBaseValue(avgFlow);
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@avgTotalFlow", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("avgTotalFlow", thisConverter.getConvertedValue());
			
			//18818 To autopopulate Drilling Parameters' iadc duration and krevs from bha
			getBHAIADCDurationAndKrevs(node, commandBean);

			// get the current dailyUid and operationUid from the record
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisDrillParam.getDailyUid());
			if(daily != null) {
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);
				
				Date currentDate = daily.getDayDate();
				String operationUid = daily.getOperationUid();
				if (thisDrillParam.getEndTime()!=null) {
					strSql = "SELECT SUM(dp.duration), SUM(dp.progress), SUM(dp.pumpDuration) as pumpTotalDuration FROM DrillingParameters dp, Daily d " +
							"WHERE (dp.isDeleted = false or dp.isDeleted is null) " +
							"AND (d.isDeleted = false or d.isDeleted is null) " +
							"AND d.dailyUid = dp.dailyUid " +
							"AND (d.dayDate < :todayDate or (d.dayDate = :todayDate and (dp.endTime is null or dp.endTime<=:endTime))) " +
							"AND d.operationUid = :operationUid " +
							"AND dp.bharunUid = :bharunUid " +
							"AND (dp.excludeFromNewHole=false or dp.excludeFromNewHole is null) ";
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"todayDate", "operationUid", "bharunUid", "endTime"}, new Object[] {currentDate, operationUid, thisDrillParam.getBharunUid(), thisDrillParam.getEndTime()}, qp);
				} else {
					strSql = "SELECT SUM(dp.duration), SUM(dp.progress), SUM(dp.pumpDuration)as pumpTotalDuration FROM DrillingParameters dp, Daily d " +
							"WHERE (dp.isDeleted = false or dp.isDeleted is null) " +
							"AND (d.isDeleted = false or d.isDeleted is null) " +
							"AND d.dailyUid = dp.dailyUid " +
							"AND d.dayDate <=:todayDate " +
							"AND d.operationUid = :operationUid " +
							"AND dp.bharunUid = :bharunUid " +
							"AND (dp.excludeFromNewHole=false or dp.excludeFromNewHole is null) ";
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"todayDate", "operationUid", "bharunUid"}, new Object[] {currentDate, operationUid, thisDrillParam.getBharunUid()}, qp);
				}
				Object[] thisResult = (Object[]) lstResult.get(0);
				thisConverter.setReferenceMappingField(DrillingParameters.class, "duration");
				
				//CALCULATE TOTAL TIME ON BOTTOM
				Double totalDuration = 0.00;
				if (thisResult[0] != null) {
					totalDuration = Double.parseDouble(thisResult[0].toString());
				}
				thisConverter.setBaseValue(totalDuration);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalDuration", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalDuration", thisConverter.getConvertedValue());
				
				//CALCULATE TOTAL BIT PROGRESS
				thisConverter.setReferenceMappingField(DrillingParameters.class, "progress");
				Double totalProgress = 0.00;
				if (thisResult[1] != null) {
					totalProgress = Double.parseDouble(thisResult[1].toString());
				}
				thisConverter.setBaseValue(totalProgress);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalProgress", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalProgress", thisConverter.getConvertedValue());
	
				//CALCULATE AVERAGE ROP
				thisConverter.setReferenceMappingField(DrillingParameters.class, "ropAvg");
				Double avgrop = 0.00;
				if (thisResult[0] != null && thisResult[1] != null) {
					if (totalDuration!=0) avgrop = totalProgress / totalDuration;
				}
				thisConverter.setBaseValue(avgrop);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@avgROP", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("avgROP", thisConverter.getConvertedValue());
				
				//CALCULATE TOTAL PUMP DURATION
				
				thisConverter.setReferenceMappingField(DrillingParameters.class, "pumpDuration");
				Double pumpTotalDuration = 0.00;
				
				if (thisResult[2] != null) {
					
					pumpTotalDuration = Double.parseDouble(thisResult[2].toString());
				}
				thisConverter.setBaseValue(pumpTotalDuration);
				pumpTotalDuration = pumpTotalDuration * 3600.0;
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@pumpTotalDuration", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("pumpTotalDuration", thisConverter.getConvertedValue());
				
			}
			
			//p2
			double iadc_hrs = 0.00;
			double krevs = 0.00;
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			String[] paramsFields1 = {"operationUid", "bharunUid"};
			Object[] paramsValues1 =  {thisDrillParam.getOperationUid(), thisDrillParam.getBharunUid()};
			String strSql2 = "SELECT iadcDuration, krevs "
					+ " FROM Bitrun WHERE (isDeleted = false or isDeleted is null)"
					+ " and operationUid =:operationUid and bharunUid = :bharunUid";
							
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields1, paramsValues1, qp);
			
			if(lstResult2.size() > 0 && lstResult2 != null) {
				
				Object[] a = (Object[]) lstResult2.get(0);
				
				if (a[0] != null) {
					iadc_hrs = Double.parseDouble(a[0].toString());
				}
				
				thisConverter.setReferenceMappingField(Bitrun.class, "iadcDuration");
				thisConverter.setBaseValue(iadc_hrs);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@iadc_hrs", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("iadc_hrs", thisConverter.getConvertedValue());
				
				if (a[1] != null) {
					krevs = Double.parseDouble(a[1].toString());
				}
				
				thisConverter.setReferenceMappingField(Bitrun.class, "krevs");
				thisConverter.setBaseValue(krevs);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@krevs", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("krevs", thisConverter.getConvertedValue());
				
			}
			
			Double cumRpmMin = null;
			if (thisDrillParam.getSurfaceRpmMin()!=null){
				cumRpmMin = thisDrillParam.getSurfaceRpmMin();
			}
			if (thisDrillParam.getDhRpmMin()!=null){
				cumRpmMin = (cumRpmMin!=null)?cumRpmMin + thisDrillParam.getDhRpmMin():thisDrillParam.getDhRpmMin();
			}
			
			if (cumRpmMin!=null){
				thisConverter.setReferenceMappingField(DrillingParameters.class, "surfaceRpmMin");
				thisConverter.setBaseValueFromUserValue(cumRpmMin);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumRpmMin", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumRpmMin", thisConverter.getConvertedValue());
			}
			
			Double cumRpmAvg = null;
			if (thisDrillParam.getSurfaceRpmAvg()!=null){
				cumRpmAvg = thisDrillParam.getSurfaceRpmAvg();
			}
			if (thisDrillParam.getDhRpmAvg()!=null){
				cumRpmAvg = (cumRpmAvg!=null)?cumRpmAvg + thisDrillParam.getDhRpmAvg():thisDrillParam.getDhRpmAvg();
			}
			
			if (cumRpmAvg!=null){
				thisConverter.setReferenceMappingField(DrillingParameters.class, "surfaceRpmAvg");
				thisConverter.setBaseValueFromUserValue(cumRpmAvg);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumRpmAvg", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumRpmAvg", thisConverter.getConvertedValue());
			}
			
			Double cumRpmMax = null;
			if (thisDrillParam.getSurfaceRpmMax()!=null){
				cumRpmMax = thisDrillParam.getSurfaceRpmMax();
			}
			if (thisDrillParam.getDhRpmMax()!=null){
				cumRpmMax = (cumRpmMax!=null)?cumRpmMax + thisDrillParam.getDhRpmMax():thisDrillParam.getDhRpmMax();
			}
			
			if (cumRpmMax!=null){
				thisConverter.setReferenceMappingField(DrillingParameters.class, "surfaceRpmMax");
				thisConverter.setBaseValueFromUserValue(cumRpmMax);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumRpmMax", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumRpmMax", thisConverter.getConvertedValue());
			}
			
		}
	}
	
	
	private void setDynAttr(CommandBean commandBean, CommandBeanTreeNode node, Class objectType, String field, String unitField, Double fieldValue) throws Exception
	{
		if (fieldValue !=null && objectType !=null && field !=null && unitField !=null && objectType!=null)
		{
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			thisConverter.setReferenceMappingField(objectType, unitField);
			thisConverter.setBaseValue(fieldValue);
			if (thisConverter.isUOMMappingAvailable()) {
				commandBean.setCustomUOM(CasingTallySummary.class, "@"+field, thisConverter.getUOMMapping());
				
			}
			node.getDynaAttr().put(field, thisConverter.getConvertedValue());
		}
	}
	
	
	private void includeBitRPM(CommandBeanTreeNode node, CommandBean commandBean) throws Exception
	{
		DrillingParameters param=(DrillingParameters)node.getData();
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		
		
		Double dhRpmAvg=param.getDhRpmAvg();
		Double surfaceRpmAvg=param.getSurfaceRpmAvg();
		
		if (dhRpmAvg!=null && surfaceRpmAvg!=null)
		{
			thisConverter.setReferenceMappingField(DrillingParameters.class, "dhRpmAvg");
			if (thisConverter.isUOMMappingAvailable())
			{
				thisConverter.setBaseValueFromUserValue(dhRpmAvg);
				dhRpmAvg=thisConverter.getBasevalue();
			}
			
			thisConverter.setReferenceMappingField(DrillingParameters.class, "surfaceRpmAvg");
			if (thisConverter.isUOMMappingAvailable())
			{
				thisConverter.setBaseValueFromUserValue(surfaceRpmAvg);
				surfaceRpmAvg=thisConverter.getBasevalue();
			}
			Double bitRpm=surfaceRpmAvg + dhRpmAvg;
			if (thisConverter.isUOMMappingAvailable())
			{
				thisConverter.setBaseValue(bitRpm);
				bitRpm=thisConverter.getConvertedValue();
				node.setCustomUOM("@bitRpm", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("bitRpm", bitRpm);
		}
	}
	
	public void afterDataNodeProcessed(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, String action, int operationPerformed, boolean errorOccurred) throws Exception{
		Object obj = node.getData();
		
		if(obj instanceof DrillingParameters){
			DrillingParameters thisDrillParam = (DrillingParameters)obj;
			
			if (!"0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "calc_annvel"))) {	
				BharunUtils.calculateAV(session.getCurrentGroupUid(),thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid(), commandBean.getSystemMessage());
			}
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "calchsi"))) {
				BharunUtils.saveHSI(thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid(), commandBean.getSystemMessage());
			}
			
			//27353 Checked that none of current active clients enable HSI field on Drilling Parameter screen, comment out this part to avoid confusion of double HSI calculation
			/*if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "calchsi"))) {
				

				Double calcHSI = this.calculateHSI(commandBean, thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid(), thisDrillParam.getFlowAvg());
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "hsi");
				thisConverter.setBaseValue(calcHSI);
				thisDrillParam.setHsi(thisConverter.getConvertedValue());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisDrillParam);
			}*/
			
			//set Bit Daily value in bharun_daily_summary when save drilling parameters
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "copyCumulativeValueFromDrillParam"))) {				
				CommonUtil.getConfiguredInstance().copyValueFromDrilParam(thisDrillParam.getDailyUid());
				
				//re-calculate and save the bit depth drilled in & out and bit hours in & out 
				CommonUtil.getConfiguredInstance().calcCumulativeBitProgressDuration(thisDrillParam.getDailyUid(), thisDrillParam.getOperationUid());
			}
						
			//set %HHP at bit in bharun_daily_summary when save drilling parameters
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PERCENT_HHPB))) {
				CustomFieldUom thisConverter=new CustomFieldUom(commandBean,DrillingParameters.class, "flowAvg");
				BharunUtils.savePercentHHPb(thisConverter, thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid(), commandBean.getSystemMessage());
			}
			
			//set Impact Force in bharun_daily_summary when save drilling parameters
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_IMPACT_FORCE))) {
				CustomFieldUom thisConverter=new CustomFieldUom(commandBean,DrillingParameters.class, "flowAvg");
				BharunUtils.saveImpactForce(thisConverter, thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid(), commandBean.getSystemMessage());
			}			
			//set Jet Nozzle Pressure Loss (Pb) in bharun_daily_summary when save drilling parameters
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_JET_NOZZLE_PRESSURE_LOSS))) {
				CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
				BharunUtils.saveJetNozzlePressureLoss(thisConverter, thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid(), commandBean.getSystemMessage(), session.getCurrentGroupUid(), session.getCurrentWellboreUid(), session.getCurrentOperationUid());
			}
			
			// call method to populate System Hydraulic Horse Power if GQP turned on
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_SYS_HYDRAULIC_HORSE_POWER))) {
				CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
				BharunUtils.saveSysHydraulicHorsepower(thisConverter, thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid(), commandBean.getSystemMessage(), session.getCurrentWellboreUid());
			}
			
			//set Bit Hydraulic Horse power (HHPb) in bharun_daily_summary when save drilling parameters
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_BIT_HYDRAULIC_HORSE_POWER))) {
				CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
				BharunUtils.saveBitHydraulicHorsepower(thisConverter, thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid(), commandBean.getSystemMessage());
			}
			
			// call method to populate Bottom Hole Force (Impact Force) if GWP turned on
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_BIT_IMPACT_FORCE))) {
				CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
				BharunUtils.saveBitImpactForce(thisConverter, thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid(), commandBean.getSystemMessage(), session.getCurrentGroupUid(), session.getCurrentWellboreUid());
			}
		}
	}
	
	// 27353 Comment out duplicate HSI calculation as current HSI calculation at Bharun screen gone through logic in CommonUtils.java and none active client is using HSI field on Drilling Parameter screen
	/* private Double calculateHSI(CommandBean commandBean, String BharunUid, String DailyUid, Double AverageFlow) throws Exception  {
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		//get average flow rate from drilling parameters
		
		Double flowAvg = 0.00;
		
		flowAvg = AverageFlow;

		if (flowAvg != null){
			thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "flowAvg");
			thisConverter.setBaseValueFromUserValue(flowAvg);
			flowAvg = thisConverter.getBasevalue();
			//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
			flowAvg = flowAvg / 0.0000630902;
		}else{
			commandBean.getSystemMessage().addWarning("HSI Calculation: Average Flow (Drilling Parameter) value empty -> HSI = 0.0");
			return 0.0;
		}

		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		//get bit diameter
		String strSql = "SELECT bitDiameter, bitrunUid FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", BharunUid, qp);
		
		if (lstResult.size()<=0){
			commandBean.getSystemMessage().addWarning("HSI Calculation: No Bit run associated to this BHA -> HSI = 0.0");
			return 0.0;
		}
		
		Object[] thisBitResult = (Object[]) lstResult.get(0);
		//get bitrunUid for acquire bit nozzle
		String bitrunUid = thisBitResult[1].toString();
		
		Double bitDiameter = 0.00;
		if (thisBitResult[0] != null){
			bitDiameter = Double.parseDouble(thisBitResult[0].toString());
			
			//convert to inch for calculation the IDS base is m		
			bitDiameter = bitDiameter / 0.0254;
		}else{
			commandBean.getSystemMessage().addWarning("HSI Calculation: Bit Diameter value empty -> HSI = 0.0");
			return 0.0;
		}
		
		//get MW 
		strSql = "SELECT AVG(mudWeight) as avgMW FROM MudProperties WHERE (isDeleted = false or isDeleted is null) and dailyUid =:DailyUid";
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "DailyUid", DailyUid, qp);
		Object thisMWResult = (Object) lstResult.get(0);
		Double mudWeight = 0.00;
		if (thisMWResult != null){
			mudWeight = Double.parseDouble(thisMWResult.toString());
			
			//convert to PoundsMassPerUKGallon for calculation the IDS base is KilogramsPerCubicMetre		
			mudWeight = mudWeight / 99.77633;
		}else{
			commandBean.getSystemMessage().addWarning("HSI Calculation: Mud Weight (Mud Properties) value empty -> HSI = 0.0");
			return 0.0;
		}
		
		//get bit nozzle
		strSql = "SELECT nozzleQty, nozzleSize FROM BitNozzle WHERE (isDeleted = false or isDeleted is null) and bitrunUid =:bitrunUid";
		List<Object[]> lstNozzleResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bitrunUid", bitrunUid, qp);
		Double nozzleQty = 0.00;
		Double nozzleSize = 0.00;
		Double nozzleCalc = 0.00;
		Double nozzleTotal = 0.00;
		
		if (lstNozzleResult.size() > 0){
			for(Object[] thisNozzleResult: lstNozzleResult){
				if (thisNozzleResult[0] != null) nozzleQty = Double.parseDouble(thisNozzleResult[0].toString());
				if (thisNozzleResult[1] != null) nozzleSize = Double.parseDouble(thisNozzleResult[1].toString());
				
				// need to convert to 32nd of inch for calculation the base is m 
				nozzleSize = nozzleSize / 0.0007937;
				
				nozzleCalc = nozzleQty * (nozzleSize * nozzleSize);
				nozzleTotal = nozzleTotal + nozzleCalc;
			}
		}
		else {
			commandBean.getSystemMessage().addWarning("HSI Calculation: Bit Nozzle value empty -> HSI = 0.0");
			return 0.0;
		}

		//finally calculate the HSI
		Double pbitcalc = (156.482 * flowAvg * flowAvg * mudWeight) / (nozzleTotal * nozzleTotal);
		Double calcHSI = (flowAvg * pbitcalc) / (1346 * bitDiameter * bitDiameter);
		
		//the base in IDS = WattPerSquareMetre, so need to convert from HSI to WattPerSquareMetre, make sure all calculation result is in IDS base 
		calcHSI = calcHSI * 1155837.16;
		
		return calcHSI;
	}*/
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof DrillingParameters) {
			DrillingParameters thisDrillParam = (DrillingParameters) object;
			
			if (thisDrillParam.getDepthTopMdMsl()!=null && thisDrillParam.getDepthMdMsl()!=null) {
				if (thisDrillParam.getDepthTopMdMsl() > thisDrillParam.getDepthMdMsl()) {
					status.setFieldError(node, "depthMdMsl", "Bottom Depth must greater than Top Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getFlowMin()!=null && thisDrillParam.getFlowAvg()!=null) {
				if (thisDrillParam.getFlowMin() > thisDrillParam.getFlowAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "flowAvg", "Flow Avg must be greater than Flow Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getFlowMin()!=null && thisDrillParam.getFlowMax()!=null) {
				if (thisDrillParam.getFlowMin() > thisDrillParam.getFlowMax()) {
					status.setFieldError(node, "flowMax", "Flow Max must be greater than Flow Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getFlowAvg()!=null && thisDrillParam.getFlowMax()!=null) {
				if (thisDrillParam.getFlowAvg() > thisDrillParam.getFlowMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "flowMax", "Flow Max must be greater than Flow Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (thisDrillParam.getSurfaceRpmMin()!=null && thisDrillParam.getSurfaceRpmAvg()!=null) {
				if (thisDrillParam.getSurfaceRpmMin() > thisDrillParam.getSurfaceRpmAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "surfaceRpmAvg", "Surface RPM Avg must be greater than Surface RPM Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getSurfaceRpmMin()!=null && thisDrillParam.getSurfaceRpmMax()!=null) {
				if (thisDrillParam.getSurfaceRpmMin() > thisDrillParam.getSurfaceRpmMax()) {
					status.setFieldError(node, "surfaceRpmMax", "Surface RPM Max must greater than Surface RPM Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getSurfaceRpmAvg()!=null && thisDrillParam.getSurfaceRpmMax()!=null) {
				if (thisDrillParam.getSurfaceRpmAvg() > thisDrillParam.getSurfaceRpmMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "surfaceRpmMax", "Surface RPM Max must greater than Surface RPM Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (thisDrillParam.getDhRpmMin()!=null && thisDrillParam.getDhRpmAvg()!=null) {
				if (thisDrillParam.getDhRpmMin() > thisDrillParam.getDhRpmAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "dhRpmAvg", "Downhole RPM Avg must greater than Downhole RPM Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getDhRpmMin()!=null && thisDrillParam.getDhRpmMax()!=null) {
				if (thisDrillParam.getDhRpmMin() > thisDrillParam.getDhRpmMax()) {
					status.setFieldError(node, "dhRpmMax", "Downhole RPM Max must greater than Downhole RPM Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getDhRpmAvg()!=null && thisDrillParam.getDhRpmMax()!=null) {
				if (thisDrillParam.getDhRpmAvg() > thisDrillParam.getDhRpmMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "dhRpmMax", "Downhole RPM Max must greater than Downhole RPM Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}

			if (thisDrillParam.getPressureMin()!=null && thisDrillParam.getPressureAvg()!=null) {
				if (thisDrillParam.getPressureMin() > thisDrillParam.getPressureAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "pressureAvg", "Pressure Avg must greater than Pressure Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getPressureMin()!=null && thisDrillParam.getPressureMax()!=null) {
				if (thisDrillParam.getPressureMin() > thisDrillParam.getPressureMax()) {
					status.setFieldError(node, "pressureMax", "Pressure Max must greater than Pressure Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getPressureAvg()!=null && thisDrillParam.getPressureMax()!=null) {
				if (thisDrillParam.getPressureAvg() > thisDrillParam.getPressureMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "pressureMax", "Pressure Max must greater than Pressure Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}

			if (thisDrillParam.getTorqueMin()!=null && thisDrillParam.getTorqueAvg()!=null) {
				if (thisDrillParam.getTorqueMin() > thisDrillParam.getTorqueAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "torqueAvg", "Torque Avg must greater than Torque Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getTorqueMin()!=null && thisDrillParam.getTorqueMax()!=null) {
				if (thisDrillParam.getTorqueMin() > thisDrillParam.getTorqueMax()) {
					status.setFieldError(node, "torqueMax", "Torque Max must greater than Torque Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getTorqueAvg()!=null && thisDrillParam.getTorqueMax()!=null) {
				if (thisDrillParam.getTorqueAvg() > thisDrillParam.getTorqueMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "torqueMax", "Torque Max must greater than Torque Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (thisDrillParam.getWobMin()!=null && thisDrillParam.getWobAvg()!=null) {
				if (thisDrillParam.getWobMin() > thisDrillParam.getWobAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "wobAvg", "WOB Avg must greater than WOB Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getWobMin()!=null && thisDrillParam.getWobMax()!=null) {
				if (thisDrillParam.getWobMin() > thisDrillParam.getWobMax()) {
					status.setFieldError(node, "wobMax", "WOB Max must greater than WOB Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getWobAvg()!=null && thisDrillParam.getWobMax()!=null) {
				if (thisDrillParam.getWobAvg() > thisDrillParam.getWobMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "wobMax", "WOB Max must greater than WOB Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getWobMinForce()!=null && thisDrillParam.getWobAvgForce()!=null) {
				if (thisDrillParam.getWobMinForce() > thisDrillParam.getWobAvgForce() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "wobAvgForce", "WOB Avg must greater than WOB Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getWobMinForce()!=null && thisDrillParam.getWobMaxForce()!=null) {
				if (thisDrillParam.getWobMinForce() > thisDrillParam.getWobMaxForce()) {
					status.setFieldError(node, "wobMaxForce", "WOB Max must greater than WOB Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getWobAvgForce()!=null && thisDrillParam.getWobMaxForce()!=null) {
				if (thisDrillParam.getWobAvgForce() > thisDrillParam.getWobMaxForce() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "wobMaxForce", "WOB Max must greater than WOB Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getRopMin()!=null && thisDrillParam.getRopAvg()!=null) {
				if (thisDrillParam.getRopMin() > thisDrillParam.getRopAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "ropAvg", "ROP Avg must greater than ROP Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getRopMin()!=null && thisDrillParam.getRopMax()!=null) {
				if (thisDrillParam.getRopMin() > thisDrillParam.getRopMax()) {
					status.setFieldError(node, "ropMax", "ROP Max must greater than ROP Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getRopAvg()!=null && thisDrillParam.getRopMax()!=null) {
				if (thisDrillParam.getRopAvg() > thisDrillParam.getRopMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "ropMax", "ROP Max must greater than ROP Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}

			if (thisDrillParam.getInstantRopMin()!=null && thisDrillParam.getInstantRopAvg()!=null) {
				if (thisDrillParam.getInstantRopMin() > thisDrillParam.getInstantRopAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "instantRopAvg", "Instantaneous ROP Avg must greater than Instantaneous ROP Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getInstantRopMin()!=null && thisDrillParam.getInstantRopMax()!=null) {
				if (thisDrillParam.getInstantRopMin() > thisDrillParam.getInstantRopMax()) {
					status.setFieldError(node, "instantRopMax", "Instantaneous ROP Max must greater than Instantaneous ROP Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getInstantRopAvg()!=null && thisDrillParam.getInstantRopMax()!=null) {
				if (thisDrillParam.getInstantRopAvg() > thisDrillParam.getInstantRopMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "instantRopMax", "Instantaneous ROP Max must greater than Instantaneous ROP Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (thisDrillParam.getSpmMin()!=null && thisDrillParam.getSpmAvg()!=null) {
				if (thisDrillParam.getSpmMin() > thisDrillParam.getSpmAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "spmAvg", "SPM Avg must greater than SPM Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getSpmMin()!=null && thisDrillParam.getSpmMax()!=null) {
				if (thisDrillParam.getSpmMin() > thisDrillParam.getSpmMax()) {
					status.setFieldError(node, "spmMax", "SPM Max must greater than SPM Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getSpmAvg()!=null && thisDrillParam.getSpmMax()!=null) {
				if (thisDrillParam.getSpmAvg() > thisDrillParam.getSpmMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "spmMax", "SPM Max must greater than SPM Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			//STANDPIPE PRESSURE
			if (thisDrillParam.getStandpipePressureMin()!=null && thisDrillParam.getStandpipePressure()!=null) {
				if (thisDrillParam.getStandpipePressureMin() > thisDrillParam.getStandpipePressure() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "standpipePressure", "Standpipe Pressure Avg must greater than Standpipe Pressure Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getStandpipePressureMin()!=null && thisDrillParam.getStandpipePressureMax()!=null) {
				if (thisDrillParam.getStandpipePressureMin() > thisDrillParam.getStandpipePressureMax()) {
					status.setFieldError(node, "standpipePressureMax", "Standpipe Pressure Max must greater than Standpipe Pressure Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getStandpipePressure()!=null && thisDrillParam.getStandpipePressureMax()!=null) {
				if (thisDrillParam.getStandpipePressure() > thisDrillParam.getStandpipePressureMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "standpipePressureMax", "Standpipe Pressure Max must greater than Standpipe Pressure Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			//CIRCULATING PRESSURE
			if (thisDrillParam.getCircPressureMin()!=null && thisDrillParam.getCircPressureAvg()!=null) {
				if (thisDrillParam.getCircPressureMin() > thisDrillParam.getCircPressureAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "circPressureAvg", "Circulating Pressure Avg must greater than Circulating Pressure Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getCircPressureMin()!=null && thisDrillParam.getCircPressureMax()!=null) {
				if (thisDrillParam.getCircPressureMin() > thisDrillParam.getCircPressureMax()) {
					status.setFieldError(node, "circPressureMax", "Circulating Pressure Max must greater than Circulating Pressure Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getCircPressureAvg()!=null && thisDrillParam.getCircPressureMax()!=null) {
				if (thisDrillParam.getCircPressureAvg() > thisDrillParam.getCircPressureMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "circPressureMax", "Circulating Pressure Max must greater than Circulating Pressure Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}

			// FLOW AT BIT
			if (thisDrillParam.getBitFlowMin()!=null && thisDrillParam.getBitFlowAvg()!=null) {
				if (thisDrillParam.getBitFlowMin() > thisDrillParam.getBitFlowAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "bitFlowAvg", "Flow at Bit Avg must greater than Flow at Bit Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getBitFlowMin()!=null && thisDrillParam.getBitFlowMax()!=null) {
				if (thisDrillParam.getBitFlowMin() > thisDrillParam.getBitFlowMax()) {
					status.setFieldError(node, "bitFlowMax", "Flow at Bit Max must greater than Flow at Bit Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getBitFlowAvg()!=null && thisDrillParam.getBitFlowMax()!=null) {
				if (thisDrillParam.getBitFlowAvg() > thisDrillParam.getBitFlowMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "bitFlowMax", "Flow at Bit Max must greater than Flow at Bit Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			// FOAM FLOW RATE
			if (thisDrillParam.getFoamFlowMin()!=null && thisDrillParam.getFoamFlowAvg()!=null) {
				if (thisDrillParam.getFoamFlowMin() > thisDrillParam.getFoamFlowAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "foamFlowAvg", "Foam Flow Rate Avg must greater than Foam Flow Rate Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getFoamFlowMin()!=null && thisDrillParam.getFoamFlowMax()!=null) {
				if (thisDrillParam.getFoamFlowMin() > thisDrillParam.getFoamFlowMax()) {
					status.setFieldError(node, "foamFlowMax", "Foam Flow Rate Max must greater than Foam Flow Rate Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getFoamFlowAvg()!=null && thisDrillParam.getFoamFlowMax()!=null) {
				if (thisDrillParam.getFoamFlowAvg() > thisDrillParam.getFoamFlowMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "foamFlowMax", "Foam Flow Rate Max must greater than Foam Flow Rate Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			// AIR FLOW RATE
			if (thisDrillParam.getAirFlowMin()!=null && thisDrillParam.getAirFlowAvg()!=null) {
				if (thisDrillParam.getAirFlowMin() > thisDrillParam.getAirFlowAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "airFlowAvg", "Air Flow Rate Avg must greater than Air Flow Rate Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getAirFlowMin()!=null && thisDrillParam.getAirFlowMax()!=null) {
				if (thisDrillParam.getAirFlowMin() > thisDrillParam.getAirFlowMax()) {
					status.setFieldError(node, "airFlowMax", "Air Flow Rate Max must greater than Air Flow Rate Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getAirFlowAvg()!=null && thisDrillParam.getAirFlowMax()!=null) {
				if (thisDrillParam.getAirFlowAvg() > thisDrillParam.getAirFlowMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "airFlowMax", "Air Flow Rate Max must greater than Air Flow Rate Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			// WELLHEAD PRESSURE
			if (thisDrillParam.getWellheadPressureMin()!=null && thisDrillParam.getWellheadPressureAvg()!=null) {
				if (thisDrillParam.getWellheadPressureMin() > thisDrillParam.getWellheadPressureAvg() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "wellheadPressureAvg", "Wellhead Pressure Avg must greater than Wellhead Pressure Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getWellheadPressureMin()!=null && thisDrillParam.getWellheadPressureMax()!=null) {
				if (thisDrillParam.getWellheadPressureMin() > thisDrillParam.getWellheadPressureMax()) {
					status.setFieldError(node, "wellheadPressureMax", "Wellhead Pressure Max must greater than Wellhead Pressure Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (thisDrillParam.getWellheadPressureAvg()!=null && thisDrillParam.getWellheadPressureMax()!=null) {
				if (thisDrillParam.getWellheadPressureAvg() > thisDrillParam.getWellheadPressureMax() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))) {
					status.setFieldError(node, "wellheadPressureMax", "Wellhead Pressure Max must greater than Wellhead Pressure Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			//MAX DEPTH > MIN DEPTH (flow)
			if (thisDrillParam.getFlowMinDepth()!=null && thisDrillParam.getFlowMaxDepth()!=null) {
				if (thisDrillParam.getFlowMinDepth() > thisDrillParam.getFlowMaxDepth()) {
					status.setFieldError(node, "flowMaxDepth", "Flow Max Depth must be greater than Flow Min Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			//MAX DEPTH > MIN DEPTH (surfaceRpm)
			if (thisDrillParam.getSurfaceRpmMinDepth()!=null && thisDrillParam.getSurfaceRpmMaxDepth()!=null) {
				if (thisDrillParam.getSurfaceRpmMinDepth() > thisDrillParam.getSurfaceRpmMaxDepth()) {
					status.setFieldError(node, "surfaceRpmMaxDepth", "Surface RPM Max Depth must greater than Surface RPM Min Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			//MAX DEPTH > MIN DEPTH (dhRpm)
			if (thisDrillParam.getDhRpmMinDepth()!=null && thisDrillParam.getDhRpmMaxDepth()!=null) {
				if (thisDrillParam.getDhRpmMinDepth() > thisDrillParam.getDhRpmMaxDepth()) {
					status.setFieldError(node, "dhRpmMaxDepth", "Downhole RPM Max Depth must greater than Downhole RPM Min Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			//MAX DEPTH > MIN DEPTH (standpipePressure)
			if (thisDrillParam.getStandpipePressureMinDepth()!=null && thisDrillParam.getStandpipePressureMaxDepth()!=null) {
				if (thisDrillParam.getStandpipePressureMinDepth() > thisDrillParam.getStandpipePressureMaxDepth()) {
					status.setFieldError(node, "standpipePressureMaxDepth", "Standpipe Pressure Max Depth must greater than Standpipe Pressure Min Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			//MAX DEPTH > MIN DEPTH (torque)
			if (thisDrillParam.getTorqueMinDepth()!=null && thisDrillParam.getTorqueMaxDepth()!=null) {
				if (thisDrillParam.getTorqueMinDepth() > thisDrillParam.getTorqueMaxDepth()) {
					status.setFieldError(node, "torqueMaxDepth", "Torque Max Depth must greater than Torque Min Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			//MAX DEPTH > MIN DEPTH (wob)
			if (thisDrillParam.getWobMinDepth()!=null && thisDrillParam.getWobMaxDepth()!=null) {
				if (thisDrillParam.getWobMinDepth() > thisDrillParam.getWobMaxDepth()) {
					status.setFieldError(node, "wobMaxDepth", "WOB Max Depth must greater than WOB Min Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			//MAX DEPTH > MIN DEPTH (rop)
			if (thisDrillParam.getRopMinDepth()!=null && thisDrillParam.getRopMaxDepth()!=null) {
				if (thisDrillParam.getRopMinDepth() > thisDrillParam.getRopMaxDepth()) {
					status.setFieldError(node, "ropMaxDepth", "ROP Max Depth must greater than ROP Min Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			//MAX DEPTH > MIN DEPTH (spm)
			if (thisDrillParam.getSpmMinDepth()!=null && thisDrillParam.getSpmMaxDepth()!=null) {
				if (thisDrillParam.getSpmMinDepth() > thisDrillParam.getSpmMaxDepth()) {
					status.setFieldError(node, "spmMaxDepth", "SPM Max Depth must greater than SPM Min Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			//18115
//			String welluid = thisDrillParam.getWellUid();
//			String country = null;
//			Well currentWell = ApplicationUtils.getConfiguredInstance().getCachedWell(welluid);
//			if (currentWell != null) {
//				country = currentWell.getCountry();
//			}
//			
//			if(session.getCurrentOperationType().equals("WKO") && country.equals("AT")){
//			
//				Double torqueavg = 0.0;
//				Double torquemin = 0.0;
//				Double torquemax = 0.0;
//				
//				CustomFieldUom thisConverter1 = new CustomFieldUom(commandBean, DrillingParameters.class, "torqueMax");
//				CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, DrillingParameters.class, "torqueMin");
//				CustomFieldUom thisConverter3 = new CustomFieldUom(commandBean, DrillingParameters.class, "torqueAvg");
//				
//				QueryProperties qp = new QueryProperties();
//				qp.setUomConversionEnabled(true);
//				
//				String strSql1 = "SELECT bharunUid FROM DrillingParameters " 
//						+ " WHERE drillingParametersUid=:drillingParametersUid AND dailyUid = :dailyUid and(isDeleted is null or isDeleted = false) ";
//				String[] paramsFields1 = { "drillingParametersUid", "dailyUid"};
//				Object[] paramsValues1 = {thisDrillParam.getDrillingParametersUid(), thisDrillParam.getDailyUid()};
//				List <BharunDailySummary> lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//						strSql1, paramsFields1, paramsValues1);
//				
//				if (lstResult1 !=null && lstResult1.size()>0) {
//					
//					Object previousbharunUid = (Object) lstResult1.get(0);
//					String bharunUid = (String)previousbharunUid;
//					
//					//check start time then get the remaining torque
//					String strSql2 = "SELECT a.torqueMax, a.torqueMin, a.torqueAvg FROM DrillingParameters a WHERE "
//							+ "(a.isDeleted = false or a.isDeleted is null) AND a.bharunUid = :BharunUid AND a.dailyUid = :dailyUid "
//							+ "AND a.startTime IS NOT NULL AND a.startTime != :StartTime order by a.startTime DESC";
//					String[] paramsField2 = {"dailyUid","BharunUid", "StartTime"};
//					Object[] paramsValues2 = {thisDrillParam.getDailyUid() ,bharunUid, thisDrillParam.getStartTime()};
//					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsField2, 
//							paramsValues2, qp);
//					
//					if (lstResult2 != null && lstResult2.size() > 0) {
//
//						Object[] torque = (Object[]) lstResult2.get(0);
//						
//						if(torque[0] != null) torquemax = Double.parseDouble(torque[0].toString()); 
//						if(torque[1] != null) torquemin = Double.parseDouble(torque[1].toString());
//						if(torque[2] != null) torqueavg = Double.parseDouble(torque[2].toString());
//						
//						String strSql3 = "FROM BharunDailySummary " 
//								+ " WHERE bharunUid =:bharunUid AND dailyUid = :dailyUid and(isDeleted is null or isDeleted = false) ";
//						String[] paramsFields3 = { "bharunUid", "dailyUid"};
//						Object[] paramsValues3 = {bharunUid, thisDrillParam.getDailyUid()};
//						List <BharunDailySummary> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//								strSql3, paramsFields3, paramsValues3);
//						
//						thisConverter1.setBaseValue(torquemax);
//						thisConverter2.setBaseValue(torquemin);
//						thisConverter3.setBaseValue(torqueavg);
//					
//						if(lstResult3!=null && lstResult3.size()>0) {
//							BharunDailySummary thisBharunDailySummary = lstResult3.get(0);
//							
//							thisBharunDailySummary.setTorqueAvg(thisConverter3.getBasevalue());
//							thisBharunDailySummary.setTorqueMax(thisConverter1.getBasevalue());
//							thisBharunDailySummary.setTorqueMin(thisConverter2.getBasevalue());
//							
//							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBharunDailySummary, qp);
//						}
//						
//					//if start time is null then set to 0
//					} else {
//						
//						String strSql4 = "FROM BharunDailySummary " 
//								+ " WHERE bharunUid =:bharunUid AND dailyUid = :dailyUid and(isDeleted is null or isDeleted = false) ";
//						String[] paramsFields4 = { "bharunUid", "dailyUid"};
//						Object[] paramsValues4 = {bharunUid, thisDrillParam.getDailyUid()};
//						List <BharunDailySummary> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//								strSql4, paramsFields4, paramsValues4);
//						
//						thisConverter1.setBaseValue(torquemax);
//						thisConverter2.setBaseValue(torquemin);
//						thisConverter3.setBaseValue(torqueavg);
//					
//						if(lstResult4!=null && lstResult4.size()>0) {
//							BharunDailySummary thisBharunDailySummary = lstResult4.get(0);
//							
//							thisBharunDailySummary.setTorqueAvg(thisConverter3.getBasevalue());
//							thisBharunDailySummary.setTorqueMax(thisConverter1.getBasevalue());
//							thisBharunDailySummary.setTorqueMin(thisConverter2.getBasevalue());
//							
//							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBharunDailySummary, qp);
//						}
//					}
//				}
//			}
		
			
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))){
				
				
				//auto calculate the average
				if (thisDrillParam.getFlowMax() != null && thisDrillParam.getFlowMin() != null) thisDrillParam.setFlowAvg((thisDrillParam.getFlowMax() + thisDrillParam.getFlowMin())/2);
				else 
				{
					if (thisDrillParam.getFlowMax() != null)thisDrillParam.setFlowAvg(thisDrillParam.getFlowMax());
					else if (thisDrillParam.getFlowMin() != null)thisDrillParam.setFlowAvg(thisDrillParam.getFlowMin());
					else thisDrillParam.setFlowAvg(null);
				}
				
				if (thisDrillParam.getSurfaceRpmMax() != null && thisDrillParam.getSurfaceRpmMin() != null) thisDrillParam.setSurfaceRpmAvg((thisDrillParam.getSurfaceRpmMax() + thisDrillParam.getSurfaceRpmMin())/2);
				else
				{
					if (thisDrillParam.getSurfaceRpmMax() != null)thisDrillParam.setSurfaceRpmAvg(thisDrillParam.getSurfaceRpmMax());
					else if (thisDrillParam.getSurfaceRpmMin() != null)thisDrillParam.setSurfaceRpmAvg(thisDrillParam.getSurfaceRpmMin());
					else thisDrillParam.setSurfaceRpmAvg(null);
				}

				if (thisDrillParam.getDhRpmMax() != null && thisDrillParam.getDhRpmMin() != null) thisDrillParam.setDhRpmAvg((thisDrillParam.getDhRpmMax() + thisDrillParam.getDhRpmMin())/2);
				else
				{
					if (thisDrillParam.getDhRpmMax() != null)thisDrillParam.setDhRpmAvg(thisDrillParam.getDhRpmMax());
					else if (thisDrillParam.getDhRpmMin() != null)thisDrillParam.setDhRpmAvg(thisDrillParam.getDhRpmMin());
					else thisDrillParam.setDhRpmAvg(null);
				}
				
				if (thisDrillParam.getPressureMax() != null && thisDrillParam.getPressureMin() != null) thisDrillParam.setPressureAvg((thisDrillParam.getPressureMax() + thisDrillParam.getPressureMin())/2);
				else
				{
					if (thisDrillParam.getPressureMax() != null)thisDrillParam.setPressureAvg(thisDrillParam.getPressureMax());
					else if (thisDrillParam.getPressureMin() != null)thisDrillParam.setPressureAvg(thisDrillParam.getPressureMin());
					else thisDrillParam.setPressureAvg(null);
				}
				
				if (thisDrillParam.getSpmMax() != null && thisDrillParam.getSpmMin() != null) thisDrillParam.setSpmAvg((thisDrillParam.getSpmMax() + thisDrillParam.getSpmMin())/2);
				else
				{
					if (thisDrillParam.getSpmMax() != null)thisDrillParam.setSpmAvg(thisDrillParam.getSpmMax());
					else if (thisDrillParam.getSpmMin() != null)thisDrillParam.setSpmAvg(thisDrillParam.getSpmMin());
					else thisDrillParam.setSpmAvg(null);
				}
				
				if (thisDrillParam.getTorqueMax() != null && thisDrillParam.getTorqueMin() != null) thisDrillParam.setTorqueAvg((thisDrillParam.getTorqueMax() + thisDrillParam.getTorqueMin())/2);
				else
				{
					if (thisDrillParam.getTorqueMax() != null)thisDrillParam.setTorqueAvg(thisDrillParam.getTorqueMax());
					else if (thisDrillParam.getTorqueMin() != null)thisDrillParam.setTorqueAvg(thisDrillParam.getTorqueMin());
					else thisDrillParam.setTorqueAvg(null);
				}
				
				if (thisDrillParam.getWobMax() != null && thisDrillParam.getWobMin() != null) thisDrillParam.setWobAvg((thisDrillParam.getWobMax() + thisDrillParam.getWobMin())/2);
				else
				{
					if (thisDrillParam.getWobMax() != null)thisDrillParam.setWobAvg(thisDrillParam.getWobMax());
					else if (thisDrillParam.getWobMin() != null)thisDrillParam.setWobAvg(thisDrillParam.getWobMin());
					else thisDrillParam.setWobAvg(null);
				}
				
				if (thisDrillParam.getWobMaxForce() != null && thisDrillParam.getWobMinForce() != null) thisDrillParam.setWobAvgForce((thisDrillParam.getWobMaxForce() + thisDrillParam.getWobMinForce())/2);
				else
				{
					if (thisDrillParam.getWobMaxForce() != null)thisDrillParam.setWobAvgForce(thisDrillParam.getWobMaxForce());
					else if (thisDrillParam.getWobMinForce() != null)thisDrillParam.setWobAvgForce(thisDrillParam.getWobMinForce());
					else thisDrillParam.setWobAvgForce(null);
				}
				
				if (thisDrillParam.getInstantRopMax() != null && thisDrillParam.getInstantRopMin() != null) thisDrillParam.setInstantRopAvg((thisDrillParam.getInstantRopMax() + thisDrillParam.getInstantRopMin())/2);
				else
				{
					if (thisDrillParam.getInstantRopMax() != null)thisDrillParam.setInstantRopAvg(thisDrillParam.getInstantRopMax());
					else if (thisDrillParam.getInstantRopMin() != null)thisDrillParam.setInstantRopAvg(thisDrillParam.getInstantRopMin());
					else thisDrillParam.setInstantRopAvg(null);
				}

				//STANDPIPE PRESSURE
				if (thisDrillParam.getStandpipePressureMax() != null && thisDrillParam.getStandpipePressureMin() != null) thisDrillParam.setStandpipePressure((thisDrillParam.getStandpipePressureMax() + thisDrillParam.getStandpipePressureMin())/2);
				else
				{
					if (thisDrillParam.getStandpipePressureMax() != null)thisDrillParam.setStandpipePressure(thisDrillParam.getStandpipePressureMax());
					else if (thisDrillParam.getStandpipePressureMin() != null)thisDrillParam.setStandpipePressure(thisDrillParam.getStandpipePressureMin());
					else thisDrillParam.setStandpipePressure(null);
				}
				
				//CIRCULATING PRESSURE
				if (thisDrillParam.getCircPressureMax() != null && thisDrillParam.getCircPressureMin() != null) thisDrillParam.setCircPressureAvg((thisDrillParam.getCircPressureMax() + thisDrillParam.getCircPressureMin())/2);
				else
				{
					if (thisDrillParam.getCircPressureMax() != null)thisDrillParam.setCircPressureAvg(thisDrillParam.getCircPressureMax());
					else if (thisDrillParam.getCircPressureMin() != null)thisDrillParam.setCircPressureAvg(thisDrillParam.getCircPressureMin());
					else thisDrillParam.setCircPressureAvg(null);
				}
				
				// FLOW AT BIT
				if (thisDrillParam.getBitFlowMax() != null && thisDrillParam.getBitFlowMin() != null) thisDrillParam.setBitFlowAvg((thisDrillParam.getBitFlowMax() + thisDrillParam.getBitFlowMin())/2);
				else
				{
					if (thisDrillParam.getBitFlowMax() != null)thisDrillParam.setBitFlowAvg(thisDrillParam.getBitFlowMax());
					else if (thisDrillParam.getBitFlowMin() != null)thisDrillParam.setBitFlowAvg(thisDrillParam.getBitFlowMin());
					else thisDrillParam.setBitFlowAvg(null);
				}
				
				// FOAM FLOW RATE
				if (thisDrillParam.getFoamFlowMax() != null && thisDrillParam.getFoamFlowMin() != null) thisDrillParam.setFoamFlowAvg((thisDrillParam.getFoamFlowMax() + thisDrillParam.getFoamFlowMin())/2);
				else
				{
					if (thisDrillParam.getFoamFlowMax() != null)thisDrillParam.setFoamFlowAvg(thisDrillParam.getFoamFlowMax());
					else if (thisDrillParam.getFoamFlowMin() != null)thisDrillParam.setFoamFlowAvg(thisDrillParam.getFoamFlowMin());
					else thisDrillParam.setFoamFlowAvg(null);
				}
				
				// AIR FLOW RATE
				if (thisDrillParam.getAirFlowMax() != null && thisDrillParam.getAirFlowMin() != null) thisDrillParam.setAirFlowAvg((thisDrillParam.getAirFlowMax() + thisDrillParam.getAirFlowMin())/2);
				else
				{
					if (thisDrillParam.getAirFlowMax() != null)thisDrillParam.setAirFlowAvg(thisDrillParam.getAirFlowMax());
					else if (thisDrillParam.getAirFlowMin() != null)thisDrillParam.setAirFlowAvg(thisDrillParam.getAirFlowMin());
					else thisDrillParam.setAirFlowAvg(null);
				}
				
				// WELLHEAD PRESSURE
				if (thisDrillParam.getWellheadPressureMax() != null && thisDrillParam.getWellheadPressureMin() != null) thisDrillParam.setWellheadPressureAvg((thisDrillParam.getWellheadPressureMax() + thisDrillParam.getWellheadPressureMin())/2);
				else
				{
					if (thisDrillParam.getWellheadPressureMax() != null)thisDrillParam.setWellheadPressureAvg(thisDrillParam.getWellheadPressureMax());
					else if (thisDrillParam.getWellheadPressureMin() != null)thisDrillParam.setWellheadPressureAvg(thisDrillParam.getWellheadPressureMin());
					else thisDrillParam.setWellheadPressureAvg(null);
				}
			}
						
			
			//TICKET 20090421-1009-kwildridge - shift Bit Performance fields from BHA Daily
			Date startTime = (Date) PropertyUtils.getProperty(object, "startTime");
			Date endTime = (Date) PropertyUtils.getProperty(object, "endTime");
			
			if(startTime != null && endTime != null){
				//ADDED CHECKING IF END TIME EARLIER THAN START TIME
				if(startTime.getTime() > endTime.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "endTime", "End time cannot be earlier than start time.");
					return;
				}
				
				//CALCULATE DURATION
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(startTime, endTime);
				
				/*
				long endT = endTime.getTime();
				if (endT == 86399000) { // if it is 23:59:59
					endT = 86400000; // make it 24:00:00 just for duration calculation
				}
				
				Long duration = (endT - startTime.getTime()) / 1000;
				*/
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "duration");
				thisConverter.setBaseValue(thisDuration);				
				PropertyUtils.setProperty(object, "duration", (thisConverter.getConvertedValue()));
			}
			
			//CALCULATE PROGRESS
			Double topDepth = (Double) thisDrillParam.getDepthTopMdMsl();
			Double bottomDepth = (Double) thisDrillParam.getDepthMdMsl();
			
			if(topDepth != null && bottomDepth != null){

				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "depthTopMdMsl");
				thisConverter.setBaseValueFromUserValue(topDepth);
				topDepth = thisConverter.getBasevalue();
				
				thisConverter.setReferenceMappingField(DrillingParameters.class, "depthMdMsl");
				thisConverter.setBaseValueFromUserValue(bottomDepth);
				bottomDepth = thisConverter.getBasevalue();
				
				double progress = bottomDepth - topDepth;
				thisConverter.setReferenceMappingField(DrillingParameters.class, "progress");
				thisConverter.setBaseValue(progress);
				thisDrillParam.setProgress(thisConverter.getConvertedValue());
			} 
			else 
			{
				thisDrillParam.setProgress(null);
			}
			
			//GWP Setting to control 1 DrillParam per day per Bharun
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "allowOneDrillParamPerDayPerBHA"))) {
				if (thisDrillParam.getBharunUid() != null || StringUtils.isBlank(thisDrillParam.getBharunUid())) {
					String thisDrillingParamUid = thisDrillParam.getDrillingParametersUid();
					String bharunUid = thisDrillParam.getBharunUid();
					String dailyUid = thisDrillParam.getDailyUid();
					
					String strSql = "";
					
					boolean duplicated = false;
					
					if (thisDrillingParamUid != null){
						strSql = "FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :bharunUid AND dailyUid = :dailyUid AND drillingParametersUid <> :drillingParametersUid";
						String[] paramsFields = {"bharunUid","dailyUid", "drillingParametersUid"};
						String[] paramsValues = {bharunUid,dailyUid, thisDrillingParamUid};
						List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
						if (lstResult.size() > 0) duplicated = true;						
					}
					else{						
						strSql = "FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :bharunUid AND dailyUid = :dailyUid";
						String[] paramsFields = {"bharunUid","dailyUid"};
						String[] paramsValues = {bharunUid,dailyUid};						
						List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
						if (lstResult.size() > 0) duplicated = true;						
					}
							
					if (duplicated == true)
					{
						status.setContinueProcess(false);
						commandBean.getSystemMessage().addError("Only 1 Drilling Parameters is allow per day per BHA run");
						return;
					}
				}
			}
			
			//p2
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(true);
			
			double ropavg = 0.00;
			double wobAvg = 0.00;
			
			String strsql = "SELECT bharunUid FROM DrillingParameters WHERE drillingParametersUid =: drillingParametersUid AND"
					+ " operationUid =: operationUid AND (is_deleted IS NULL OR is_deleted IS FALSE)";
			String[] paramsFields = {"drillingParametersUid", "operationUid"};
			Object[] paramsValues = {thisDrillParam.getDrillingParametersUid(), thisDrillParam.getOperationUid()};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
					strsql, paramsFields, paramsValues);
			
			if (lstResult.size() > 0 && lstResult != null) {
				
				Object bhaUid = (Object)lstResult.get(0);
				String bharunUid = bhaUid.toString();
				
				String strSql1 = "SELECT ropAvg, wobAvg FROM DrillingParameters " 
						+ " WHERE bharunUid =:bharunUid AND operationUid =: operationUid and(isDeleted is null or isDeleted = false)"
						+ " AND startTime IS NOT NULL AND startTime != :StartTime ORDER BY startTime DESC";
				String[] paramsFields1 = { "bharunUid", "operationUid", "StartTime"};
				Object[] paramsValues1 = {bharunUid, thisDrillParam.getOperationUid(), thisDrillParam.getStartTime()};
				List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
						strSql1, paramsFields1, paramsValues1);
				
				
				
				if (lstResult1 !=null && lstResult1.size() > 0) {
					
					Object[] avg = (Object[]) lstResult1.get(0);
					
					if (avg[0] != null) ropavg = Double.parseDouble(avg[0].toString());
					if (avg[1] != null) wobAvg = Double.parseDouble(avg[1].toString());
					CustomFieldUom thisConverter1 = new CustomFieldUom(commandBean, DrillingParameters.class, "wobAvg");
					CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, DrillingParameters.class, "ropAvg");
					thisConverter1.setBaseValue(wobAvg);
					thisConverter2.setBaseValue(ropavg);
					
					String strSql2 = "FROM Bitrun " 
							+ " WHERE bharunUid =:bharunUid and(isDeleted is null or isDeleted = false) ";
					String[] paramsFields2 = { "bharunUid"};
					Object[] paramsValues2 = {bharunUid};
					List <Bitrun> lstResult2= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
							strSql2, paramsFields2, paramsValues2, qp);
					
					if(lstResult2!=null && lstResult2.size()>0) {
						Bitrun bitrun = lstResult2.get(0);
						bitrun.setRop(thisConverter2.getBasevalue());
						bitrun.setWobAvg(thisConverter1.getBasevalue());
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bitrun, qp);
					}
				} else {
					
					String strSql2 = "FROM Bitrun " 
							+ " WHERE bharunUid =:bharunUid and(isDeleted is null or isDeleted = false) ";
					String[] paramsFields2 = { "bharunUid"};
					Object[] paramsValues2 = {bharunUid};
					List <Bitrun> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
							strSql2, paramsFields2, paramsValues2, qp);
					
					if(lstResult2!=null && lstResult2.size()>0) {
						Bitrun bitrun = lstResult2.get(0);
						bitrun.setRop(ropavg);
						bitrun.setWobAvg(wobAvg);
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bitrun, qp);
					}
				}
			}
		}
	}
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		//CALCULATE ROP
		Object obj = node.getData();
		if (obj instanceof DrillingParameters) {
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_DRILLING_PARAMETERS_REDAING))){
				DrillingParameters thisDrillParam = (DrillingParameters) obj;
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
				if (thisDrillParam.getDuration() != null && thisDrillParam.getProgress() != null) {
				
					Double duration = 0.0;
					Double progress = 0.0;
					Double avgRop = 0.0;
		
					thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "duration");
					thisConverter.setBaseValueFromUserValue(thisDrillParam.getDuration());
					duration = thisConverter.getBasevalue();
					
					thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "progress");
					thisConverter.setBaseValueFromUserValue(thisDrillParam.getProgress());
					progress = thisConverter.getBasevalue();
					
					if (duration > 0 && progress > 0){
						avgRop = progress / duration;	
					}
					
					thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "ropAvg");
					thisConverter.setBaseValue(avgRop);
					thisDrillParam.setRopAvg(thisConverter.getConvertedValue());
				}else {
					if (thisDrillParam.getRopMax() != null && thisDrillParam.getRopMin() != null) thisDrillParam.setRopAvg((thisDrillParam.getRopMax() + thisDrillParam.getRopMin())/2);
					else
					{
						if (thisDrillParam.getRopMax() != null)thisDrillParam.setRopAvg(thisDrillParam.getRopMax());
						else if (thisDrillParam.getRopMin() != null)thisDrillParam.setRopAvg(thisDrillParam.getRopMin());
						else thisDrillParam.setRopAvg(null);
					}
				}
	
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisDrillParam);
			}
			
			//18818 To autopopulate Drilling Parameters' iadc duration and krevs from bha
			getBHAIADCDurationAndKrevs(node, commandBean);
			
			//calculate and save BHA run -> Jet Velocity
			this.updateBharunJetVelocity(node);
			
		}
		
		//18115
//		if(torquecalculation) {
//			DrillingParameters thisDrillParam = (DrillingParameters) obj;
//			String welluid = thisDrillParam.getWellUid();
//			String country = null;
//			Well currentWell = ApplicationUtils.getConfiguredInstance().getCachedWell(welluid);
//			if (currentWell != null) {
//				country = currentWell.getCountry();
//			}
//			
//			if(session.getCurrentOperationType().equals("WKO") && country.equals("AT")){
//				
//				QueryProperties qp = new QueryProperties();
//				qp.setUomConversionEnabled(true);
//				Double torqueAvg = 0.0;
//				Double torqueMax = 0.0;
//				Double torqueMin = 0.0;
//				
//				CustomFieldUom thisConverter1 = new CustomFieldUom(commandBean, DrillingParameters.class, "torqueMax");
//				CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, DrillingParameters.class, "torqueMin");
//				CustomFieldUom thisConverter3 = new CustomFieldUom(commandBean, DrillingParameters.class, "torqueAvg");
//				
//				//get latest start time
//				String strSqllst = "SELECT a.startTime FROM DrillingParameters a WHERE "
//						+ "(a.isDeleted = false or a.isDeleted is null) AND a.bharunUid = :BharunUid AND a.dailyUid = :dailyUid order"
//						+ " by a.startTime desc";
//				String[] paramsFieldlst = {"dailyUid","BharunUid"};
//				String[] paramsValueslst = {thisDrillParam.getDailyUid() ,thisDrillParam.getBharunUid()};
//				List lstResultlst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqllst, paramsFieldlst, 
//						paramsValueslst, qp);
//				
//				Date lateststartTime = null;
//				
//				Object lst = (Object) lstResultlst.get(0);
//				lateststartTime = (Date)lst;
//				
//				//get start time
//				String strSqlst = "SELECT a.startTime FROM DrillingParameters a WHERE "
//						+ "(a.isDeleted = false or a.isDeleted is null) AND a.bharunUid = :BharunUid AND a.dailyUid = :dailyUid "
//						;
//				String[] paramsFieldsst = {"dailyUid","BharunUid"};
//				String[] paramsValuesst = {thisDrillParam.getDailyUid() ,thisDrillParam.getBharunUid()};
//				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlst, paramsFieldsst, 
//						paramsValuesst, qp);
//				
//				Date startTime = null;
//				
//				if(lstResult.size() > 0 && lstResult != null) {
//					
//					for(int i = 0; i < lstResult.size() ; i ++) {
//						
//						Object st = (Object) lstResult.get(i);
//						startTime = (Date)st;
//					
//						if(startTime != null) {
//							
//							if(startTime.getTime() == lateststartTime.getTime()) {
//								
//								String strSql2 = "SELECT a.torqueMax, a.torqueAvg, a.torqueMin FROM DrillingParameters a WHERE "
//										+ "(a.isDeleted = false or a.isDeleted is null) AND a.bharunUid = :BharunUid AND a.dailyUid = :dailyUid "
//										+ "AND a.startTime IS NOT NULL ORDER BY a.startTime DESC";
//								String[] paramsFields2 = {"dailyUid","BharunUid"};
//								String[] paramsValues2 = {thisDrillParam.getDailyUid() ,thisDrillParam.getBharunUid()};
//								List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, 
//										paramsValues2, qp);
//								
//								Object[] queryresult = (Object[]) lstResult1.get(0);
//								
//									if(queryresult!= null) {
//										if(queryresult[0] != null) {
//											torqueMax = Double.parseDouble(queryresult[0].toString());
//										}
//										
//										if(queryresult[1] != null) {
//											torqueAvg = Double.parseDouble(queryresult[1].toString());
//										}
//										
//										if(queryresult[2] != null) {
//											torqueMin = Double.parseDouble(queryresult[2].toString());
//										}
//										
//										thisConverter1.setBaseValue(torqueMax);
//										thisConverter2.setBaseValue(torqueMin);
//										thisConverter3.setBaseValue(torqueAvg);
//										
//										String strSql1 = "FROM BharunDailySummary " 
//												+ " WHERE bharunUid =:bharunUid AND dailyUid = :dailyUid and(isDeleted is null or isDeleted = false) ";
//										String[] paramsFields1 = { "bharunUid", "dailyUid"};
//										Object[] paramsValues1 = {thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid()};
//										List <BharunDailySummary> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//												strSql1, paramsFields1, paramsValues1, qp);
//										
//										if(lstResult5!=null && lstResult5.size()>0) {
//											BharunDailySummary thisBharunDailySummary = lstResult5.get(0);
//											
//											thisBharunDailySummary.setTorqueAvg(thisConverter3.getBasevalue());
//											thisBharunDailySummary.setTorqueMax(thisConverter1.getBasevalue());
//											thisBharunDailySummary.setTorqueMin(thisConverter2.getBasevalue());
//											
//											ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBharunDailySummary, qp);
//											break;
//										}
//									}
//								}
//							}else {
//								
//								String strSql1 = "FROM BharunDailySummary " 
//										+ " WHERE bharunUid =:bharunUid AND dailyUid = :dailyUid and(isDeleted is null or isDeleted = false) ";
//								String[] paramsFields1 = { "bharunUid", "dailyUid"};
//								Object[] paramsValues1 = {thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid()};
//								List <BharunDailySummary> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//										strSql1, paramsFields1, paramsValues1);
//								
//								Double torqueavg = 0.0;
//								Double torquemin = 0.0;
//								Double torquemax = 0.0;
//								
//								thisConverter1.setBaseValue(torquemax);
//								thisConverter2.setBaseValue(torquemin);
//								thisConverter3.setBaseValue(torqueavg);
//							
//								if(lstResult5!=null && lstResult5.size()>0) {
//									BharunDailySummary thisBharunDailySummary = lstResult5.get(0);
//									
//									thisBharunDailySummary.setTorqueAvg(thisConverter3.getBasevalue());
//									thisBharunDailySummary.setTorqueMax(thisConverter1.getBasevalue());
//									thisBharunDailySummary.setTorqueMin(thisConverter2.getBasevalue());
//									
//									ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBharunDailySummary, qp);
//								}
//							}
//						}
//					}
//				}
//			}
		
		//p2
		if(calwWobRop) {
			DrillingParameters thisDrillParam = (DrillingParameters) obj;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(true);
			
			String strSql3 = "SELECT wobAvg, ropAvg, startTime FROM DrillingParameters WHERE "
					+ "(isDeleted = false or isDeleted is null) AND bharunUid = :BharunUid AND startTime IS NOT NULL order by startTime desc";
			String[] paramsFields2 = {"BharunUid"};
			String[] paramsValues2 = {thisDrillParam.getBharunUid()};
			List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields2, 
					paramsValues2, qp);
			
			double ropavg = 0.00;
			double wobAvg = 0.00;
			CustomFieldUom thisConverter1 = new CustomFieldUom(commandBean, DrillingParameters.class, "wobAvg");
			CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, DrillingParameters.class, "ropAvg");
		
			if(lstResult1!=null && lstResult1.size()>0) {
				
				Object[] queryResult = (Object[]) lstResult1.get(0);
				
				if(queryResult[2] != null) {
					//calculate wobAvg
					if(queryResult[0] != null) {
						wobAvg = Double.parseDouble(queryResult[0].toString());
					}
					thisConverter1.setBaseValue(wobAvg);
					
					//calculate ropAvg
					if(queryResult[1] != null) {
						ropavg = Double.parseDouble(queryResult[1].toString());
					}
					thisConverter2.setBaseValue(ropavg);
				} else {
					thisConverter1.setBaseValue(0.00);
					thisConverter2.setBaseValue(0.00);
				}
	
				String strSql1 = "FROM Bitrun " 
						+ " WHERE bharunUid =:bharunUid and(isDeleted is null or isDeleted = false) ";
				String[] paramsFields1 = { "bharunUid"};
				Object[] paramsValues1 = {thisDrillParam.getBharunUid()};
				List <Bitrun> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
						strSql1, paramsFields1, paramsValues1, qp);
				
				if(lstResult5!=null && lstResult5.size()>0) {
					Bitrun bitrun = lstResult5.get(0);
					
					bitrun.setRop(thisConverter2.getBasevalue());
					bitrun.setWobAvg(thisConverter1.getBasevalue());
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bitrun, qp);
				}
			}
		}	
	}
	
	
	private void getBHAIADCDurationAndKrevs(CommandBeanTreeNode node, CommandBean commandBean) throws Exception {
		if (node.getData() instanceof DrillingParameters) {
			DrillingParameters thisDrillParam = (DrillingParameters) node.getData();
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			String strSql = "SELECT s.IADCDuration, s.krevs FROM BharunDailySummary s, Bharun b WHERE s.bharunUid = b.bharunUid AND s.bharunUid = :BharunUid AND s.dailyUid = :DailyUid AND (s.isDeleted = false or s.isDeleted is null) AND (b.isDeleted = false or b.isDeleted is null)";
			String[] paramsFields = {"BharunUid", "DailyUid"};
			String[] paramsValues = {thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid()};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);

			Double iadcDuration = 0.00;
			Double krevs = 0.00;

			if (lstResult.size() > 0) {
				Object[] queryResult = (Object[]) lstResult.get(0);
				if (queryResult[0] != null) {
					iadcDuration = Double.parseDouble(queryResult[0].toString());
				}
				if (queryResult[1] != null) {
					krevs = Double.parseDouble(queryResult[1].toString());
				}
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BharunDailySummary.class, "IADCDuration");
				thisConverter.setBaseValue(iadcDuration);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@bharunIadcDuration", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("bharunIadcDuration", thisConverter.getConvertedValue());
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "krevs");
				thisConverter.setBaseValue(krevs);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@bharunKrevs", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("bharunKrevs", thisConverter.getConvertedValue());
			}
		}
	}
	
	private void updateBharunJetVelocity(CommandBeanTreeNode node) throws Exception {
		if (node.getData() instanceof DrillingParameters) {
			DrillingParameters thisDrillParam = (DrillingParameters) node.getData();
			String dailyUid = thisDrillParam.getDailyUid();
			String bharunUid = thisDrillParam.getBharunUid();
			Double jetvelocity = CommonUtil.getConfiguredInstance().calculateJetVelocity(bharunUid, dailyUid);
			CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "jetvelocity");
			
			String strSql = "FROM BharunDailySummary WHERE (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid and dailyUid=:dailyUid";
			List<BharunDailySummary> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"bharunUid", "dailyUid"}, new Object[] {bharunUid, dailyUid});
			for (BharunDailySummary bhaDailySummary : list) {
				if (jetvelocity!=null) {
					thisConverter.setBaseValue(jetvelocity);
					bhaDailySummary.setJetvelocity(thisConverter.getConvertedValue());
				} else {
					bhaDailySummary.setJetvelocity(null);
				}
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bhaDailySummary);
			}
		}
	}

	@Override
	public void afterDataNodeDelete(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		//calculate and save BHA run -> Jet Velocity
		this.updateBharunJetVelocity(node);
		
		//18115
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Object obj=node.getData();
		
		if (obj instanceof DrillingParameters)
		{
			DrillingParameters thisDrillParam = (DrillingParameters) obj;
			
//			Double torqueAvg = 0.0;
//			Double torqueMax = 0.0;
//			Double torqueMin = 0.0;
//			
//			CustomFieldUom thisConverter1 = new CustomFieldUom(commandBean, DrillingParameters.class, "torqueMax");
//			CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, DrillingParameters.class, "torqueMin");
//			CustomFieldUom thisConverter3 = new CustomFieldUom(commandBean, DrillingParameters.class, "torqueAvg");
//			
//			//get start time
//			String strSqlst = "SELECT a.startTime FROM DrillingParameters a WHERE "
//					+ "(a.isDeleted = false or a.isDeleted is null) AND a.bharunUid = :BharunUid AND a.dailyUid = :dailyUid "
//					+ " and a.startTime IS NOT NULL ORDER BY a.startTime DESC"
//					;
//			String[] paramsFieldsst = {"dailyUid","BharunUid"};
//			String[] paramsValuesst = {thisDrillParam.getDailyUid() ,thisDrillParam.getBharunUid()};
//			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlst, paramsFieldsst, 
//					paramsValuesst, qp);
//			
//			Date startTime = null;
//			
//			if(lstResult.size() > 0 && lstResult != null) {
//				
//							
//				String strSql2 = "SELECT a.torqueMax, a.torqueAvg, a.torqueMin FROM DrillingParameters a WHERE "
//						+ "(a.isDeleted = false or a.isDeleted is null) AND a.bharunUid = :BharunUid AND a.dailyUid = :dailyUid "
//						+ "AND a.startTime IS NOT NULL ORDER BY a.startTime DESC";
//				String[] paramsFields2 = {"dailyUid","BharunUid"};
//				String[] paramsValues2 = {thisDrillParam.getDailyUid() ,thisDrillParam.getBharunUid()};
//				List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, 
//						paramsValues2, qp);
//				
//				Object[] queryresult = (Object[]) lstResult1.get(0);
//				
//					if(queryresult!= null) {
//						if(queryresult[0] != null) {
//							torqueMax = Double.parseDouble(queryresult[0].toString());
//						}
//						
//						if(queryresult[1] != null) {
//							torqueAvg = Double.parseDouble(queryresult[1].toString());
//						}
//						
//						if(queryresult[2] != null) {
//							torqueMin = Double.parseDouble(queryresult[2].toString());
//						}
//						
//						thisConverter1.setBaseValue(torqueMax);
//						thisConverter2.setBaseValue(torqueMin);
//						thisConverter3.setBaseValue(torqueAvg);
//		
//						String strSql1 = "FROM BharunDailySummary " 
//								+ " WHERE bharunUid =:bharunUid AND dailyUid = :dailyUid and(isDeleted is null or isDeleted = false) ";
//						String[] paramsFields1 = { "bharunUid", "dailyUid"};
//						Object[] paramsValues1 = {thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid()};
//						List <BharunDailySummary> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//								strSql1, paramsFields1, paramsValues1, qp);
//						
//						if(lstResult5!=null && lstResult5.size()>0) {
//							BharunDailySummary thisBharunDailySummary = lstResult5.get(0);
//							thisBharunDailySummary.setTorqueAvg(thisConverter3.getBasevalue());
//							thisBharunDailySummary.setTorqueMax(thisConverter1.getBasevalue());
//							thisBharunDailySummary.setTorqueMin(thisConverter2.getBasevalue());
//							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBharunDailySummary, qp);
//						}
//					}
//				}else {
//
//					String strSql4 = "FROM BharunDailySummary " 
//							+ " WHERE bharunUid =:bharunUid AND dailyUid = :dailyUid and(isDeleted is null or isDeleted = false) ";
//					String[] paramsFields4 = { "bharunUid", "dailyUid"};
//					Object[] paramsValues4 = {thisDrillParam.getBharunUid(), thisDrillParam.getDailyUid()};
//					List <BharunDailySummary> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//							strSql4, paramsFields4, paramsValues4, qp);
//					
//					thisConverter1.setBaseValue(torqueMax);
//					thisConverter2.setBaseValue(torqueMin);
//					thisConverter3.setBaseValue(torqueAvg);
//				
//					if(lstResult4!=null && lstResult4.size()>0) {
//						BharunDailySummary thisBharunDailySummary = lstResult4.get(0);
//						thisBharunDailySummary.setTorqueAvg(thisConverter3.getBasevalue());
//						thisBharunDailySummary.setTorqueMax(thisConverter1.getBasevalue());
//						thisBharunDailySummary.setTorqueMin(thisConverter2.getBasevalue());
//						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBharunDailySummary, qp);
//					}
//				}
//			
				//p2
				double ropavg = 0.00;
				double wobAvg = 0.00;
				
				CustomFieldUom thisConverter4 = new CustomFieldUom(commandBean, DrillingParameters.class, "wobAvg");
				CustomFieldUom thisConverter5 = new CustomFieldUom(commandBean, DrillingParameters.class, "ropAvg");
				
				String strSql3 = "SELECT wobAvg, ropAvg FROM DrillingParameters WHERE "
						+ "(isDeleted = false or isDeleted is null) AND bharunUid = :BharunUid AND startTime IS NOT NULL order by startTime desc";
				String[] paramsFields3 = {"BharunUid"};
				String[] paramsValues3 = {thisDrillParam.getBharunUid()};
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, 
						paramsValues3, qp);
					
				if(lstResult2 != null && lstResult2.size() > 0){
					
					Object[] queryResult = (Object[]) lstResult2.get(0);
					
					//calculate wobAvg
					if(queryResult[0] != null) {
						wobAvg = Double.parseDouble(queryResult[0].toString());
					}
					thisConverter4.setBaseValue(wobAvg);
					
					//calculate ropavg
					if(queryResult[1] != null) {
						ropavg = Double.parseDouble(queryResult[1].toString());
					}
					thisConverter5.setBaseValue(ropavg);
					
					String strSql1 = "FROM Bitrun " 
							+ " WHERE bharunUid =:bharunUid and(isDeleted is null or isDeleted = false) ";
					String[] paramsFields1 = { "bharunUid"};
					Object[] paramsValues1 = {thisDrillParam.getBharunUid()};
					List <Bitrun> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
							strSql1, paramsFields1, paramsValues1, qp);
					
					if(lstResult5!=null && lstResult5.size()>0) {
						Bitrun bitrun = lstResult5.get(0);
						bitrun.setRop(thisConverter5.getBasevalue());
						bitrun.setWobAvg(thisConverter4.getBasevalue());
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bitrun, qp);
					}
				} else {
					
					String strSql1 = "FROM Bitrun " 
							+ " WHERE bharunUid =:bharunUid and(isDeleted is null or isDeleted = false) ";
					String[] paramsFields1 = { "bharunUid"};
					Object[] paramsValues1 = {thisDrillParam.getBharunUid()};
					List <Bitrun> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
							strSql1, paramsFields1, paramsValues1, qp);
					
					thisConverter4.setBaseValue(wobAvg);
					thisConverter5.setBaseValue(ropavg);
					
					if(lstResult5!=null && lstResult5.size()>0) {
						Bitrun bitrun = lstResult5.get(0);
						bitrun.setRop(thisConverter5.getBasevalue());
						bitrun.setWobAvg(thisConverter4.getBasevalue());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bitrun, qp);
					}
				}	
			}		
		}		
			
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof DrillingParameters) {
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_MIDNIGHT_DEPTH_TO_PARAM)))
				this.includeMidnightDepth(node, commandBean, UserSession.getInstance(request));
		}
	}
	

	private void includeMidnightDepth(CommandBeanTreeNode node, CommandBean commandBean, UserSession session) throws Exception
	{
		Object object = node.getData();
		if (object instanceof DrillingParameters)
		{
			DrillingParameters param = (DrillingParameters)object;
			String dailyUidToday = session.getCurrentDailyUid();
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean,ReportDaily.class,"depthMdMsl");
			CustomFieldUom thisConverterParam = new CustomFieldUom(commandBean,DrillingParameters.class,"depthMdMsl");
			
			
			ReportDaily today = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(session, dailyUidToday);
			if (today!=null && today.getDepthMdMsl()!=null)
			{
				if (thisConverter.isUOMMappingAvailable())
				{
					thisConverter.setBaseValueFromUserValue(today.getDepthMdMsl(),false);
					if (thisConverterParam.isUOMMappingAvailable())
					{
						thisConverterParam.setBaseValue(thisConverter.getBasevalue());
					}
					param.setDepthMdMsl(thisConverterParam.getConvertedValue());
				}
				
			}
			Daily dailyYesterday = ApplicationUtils.getConfiguredInstance().getYesterday(session.getCurrentOperationUid(), dailyUidToday);
			if (dailyYesterday!=null)
			{
				ReportDaily yesterday = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(session, dailyYesterday.getDailyUid());
				if (yesterday!=null && yesterday.getDepthMdMsl()!=null)
				{
					if (thisConverter.isUOMMappingAvailable())
					{
						thisConverter.setBaseValueFromUserValue(yesterday.getDepthMdMsl(),false);
						
						thisConverterParam.setReferenceMappingField(DrillingParameters.class, "depthTopMdMsl");
						if (thisConverterParam.isUOMMappingAvailable())
						{
							thisConverterParam.setBaseValue(thisConverter.getBasevalue());
						}
						param.setDepthTopMdMsl(thisConverterParam.getConvertedValue());
					}
				}
			}
		}
	}
}
