package com.idsdatanet.d2.drillnet.supportVesselParam;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.ExcelImportTemplateManager;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.witsml.wellit.supportvessel.WellITSupportVesselParamScreenImport;

public class SupportVesselParamCommandBeanListener extends EmptyCommandBeanListener {
	
	private ExcelImportTemplateManager excelImportTemplateManager = null;

    public ExcelImportTemplateManager getExcelImportTemplateManager() {
        return excelImportTemplateManager;
    }

    public void setExcelImportTemplateManager(ExcelImportTemplateManager excelImportTemplateManager) {
        this.excelImportTemplateManager = excelImportTemplateManager;
    }
    
	@Override
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if (ExcelImportTemplateManager.IMPORT_TEMPLATE_EXCEL.equalsIgnoreCase(invocationKey)) {
            this.excelImportTemplateManager.uploadFile(request, response, ExcelImportTemplateManager.KEY_FILE);
        } else if (StringUtils.equals("importWellIT", invocationKey)) {
			// Get JSON
			Object jsonOutput = WellITSupportVesselParamScreenImport.importWellIT(request, commandBean);
			this.writeResponse(response, jsonOutput);
		} else if (StringUtils.equals("getImportedVesselList", invocationKey)) {
			UserSession userSession = UserSession.getInstance(request);
	    	Daily daily = userSession.getCurrentDaily();
	    	String hql = "SELECT DISTINCT importParameters FROM SupportVesselParam WHERE dailyUid=:dailyUid AND (isDeleted is null or isDeleted = 0) AND importParameters IS NOT NULL";
	    	List<String> vesselList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"dailyUid"}, new Object[] {daily.getDailyUid()});
			
	    	String result = "";
	    	StringBuilder sb = new StringBuilder();
	    	if (!vesselList.isEmpty()) {
	    		for (String s : vesselList) {
	    			String[] strArr = s.split("_");
	    			if (strArr.length == 3) {
	    				sb.append(strArr[2]).append(",");
	    			}
	    		}
	    		result = sb.deleteCharAt(sb.length() - 1).toString();
	    	}
			this.writeResponse(response, result);
		}
		return;
	}
	
	@Override
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		String action = request.getParameter("action");
		if (StringUtils.equals(ExcelImportTemplateManager.IMPORT_TEMPLATE_EXCEL, action)) {
            this.excelImportTemplateManager.importExcelFile((BaseCommandBean) commandBean, this, request, request.getParameter(ExcelImportTemplateManager.KEY_TEMPLATE), request.getParameter(ExcelImportTemplateManager.KEY_FILE), "SupportVesselParam");
        } else if (StringUtils.equals("importWellIT",action)) {
			String vesselIds = request.getParameter("vesselIds");
			String jsonString = request.getParameter("json");
			WellITSupportVesselParamScreenImport.importWellIT(request, commandBean, jsonString, vesselIds.split(","));
        }
		SupportVesselParamUtils.stockCodeChanged(request, targetCommandBeanTreeNode);
	}

	private void writeResponse(HttpServletResponse response, Object output) throws Exception {
		response.getWriter().write(output.toString());
	}
	
}
