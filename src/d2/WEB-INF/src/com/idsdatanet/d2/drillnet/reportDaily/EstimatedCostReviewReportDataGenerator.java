package com.idsdatanet.d2.drillnet.reportDaily;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * Time Breakdown with Estimated Cost Review Calculation
 * for cost sheet daily reporting use
 *
 */
public class EstimatedCostReviewReportDataGenerator  implements ReportDataGenerator {

	
private String defaultReportType;
	
	public void setReportType(String value){
		this.defaultReportType = value;
	}

	public void disposeOnDataGeneratorExit() {
		
	}

	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);	
		 
		//create header report node
		ReportDataNode thisReportNode = reportDataNode.addChild("DailyCostData");
		//ReportDataNode thisReportNode2 = thisReportNode.addChild("ReportDaily");
		
		//get current operationUid
		String operationUid = userContext.getUserSelection().getOperationUid();
		Operation thisOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
		String thisOperationCode = thisOperation.getOperationCode();

		//get current date
		String currentDailyUid = userContext.getUserSelection().getDailyUid();
		Daily currentDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (currentDaily==null) return;
		Date todayDate = currentDaily.getDayDate();
		
		if (reportType==null) {
			reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		}
		if(this.defaultReportType != null){
			reportType = this.defaultReportType;
		}
		DateFormat df = new SimpleDateFormat("dd MMM yyyy");
		thisReportNode.addProperty("currentDate", df.format(todayDate).toString());
		thisReportNode.addProperty("operationUid", operationUid);
		
		
		String selectReportDailySql = "SELECT reportDatetime, reportType FROM ReportDaily " +
				"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
				"AND dailyUid=:dailyUid " +
				"AND operationUid=:operationUid";
		String[] selectReportDailyParamsFields = {"dailyUid", "operationUid"};
		Object[] selectReportDailyParamsValues = {currentDailyUid, operationUid};
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(selectReportDailySql, selectReportDailyParamsFields, selectReportDailyParamsValues, qp);
		
		if(!lstResult.isEmpty()){
			Date thisReportDatetime = (Date) lstResult.get(0)[0];
			CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "days_since_lta");
			CustomFieldUom durationConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
			int numDays = 0;
			double cumCost = 0.0;
			
			if(CommonUtil.getConfiguredInstance().calculateCummulativeCost(currentDailyUid, lstResult.get(0)[1].toString()) != null)
				cumCost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(currentDailyUid, lstResult.get(0)[1].toString());
			
			String reportDailySql = "SELECT dailyUid FROM ReportDaily " +
					"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
					"AND operationUid=:operationUid " +
					"AND reportDatetime <=:thisReportDatetime " +
					"ORDER BY reportDatetime ASC";
			String[] reportDailyParamsFields = {"thisReportDatetime", "operationUid"};
			Object[] reportDailyParamsValues = {thisReportDatetime, operationUid};
			
			List<String> dailyLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(reportDailySql, reportDailyParamsFields, reportDailyParamsValues, qp);
			if (!dailyLst.isEmpty())
			{
				numDays = dailyLst.size();
				
				String activitySql = "SELECT classCode, sum(activityDuration) " +
				"FROM Activity " +
				"WHERE (isDeleted is null or isDeleted=false) " +
				"AND (isSimop=false or isSimop is null) " +
				"AND (isOffline=false or isOffline is null) " +
				"AND (dayPlus IS NULL OR dayPlus=0) " +
				"AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') " + 
				"AND (dailyUid in (:thisDailyUid)) " +
				"GROUP BY classCode";
				
				List<Object[]> classCodeLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(activitySql, "thisDailyUid", dailyLst, qp);
				
				if (!classCodeLst.isEmpty())
				{
					for (Object[] obj : classCodeLst)
					{
						ReportDataNode thisReportNode2 = thisReportNode.addChild("ClassCode");
						String clsCode = (String) obj[0];
						Double duration = (Double) obj[1];
						Double estCost = 0.0;
						
						if (thisConverter !=null)
						{
							thisConverter.setBaseValue(duration);
							duration = thisConverter.getConvertedValue();
							duration = Double.parseDouble(durationConverter.formatOutputPrecision(duration).replace(",", ""));
						}
						
						String queryString2 = "SELECT name FROM LookupClassCode WHERE (isDeleted=false or isDeleted is null) and shortCode=:thisShortCode";
						List<String> classCodeName = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString2, "thisShortCode", clsCode);
						if(classCodeName.isEmpty())
							clsCode = "NULL";
						else
							clsCode = classCodeName.get(0) + " (" + clsCode + ")";
						
						if(cumCost > 0)
							estCost = (duration / numDays) * cumCost;
						
						thisReportNode2.addProperty("name", clsCode);
						thisReportNode2.addProperty("duration", duration.toString());
						thisReportNode2.addProperty("estCost", estCost.toString());
					}
				}
				
			}
		}	
	}
}
			