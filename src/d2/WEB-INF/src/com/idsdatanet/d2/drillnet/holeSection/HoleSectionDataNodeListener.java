package com.idsdatanet.d2.drillnet.holeSection;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.HoleSection;
import com.idsdatanet.d2.core.model.HoleSectionKpi;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class HoleSectionDataNodeListener extends EmptyDataNodeListener{
	
	private boolean autoPopulatePrevHoleSize = false;
	private String holeSectionKpiUunit = "holesection_kpi.uom"; 
	
	public void setAutoPopulatePrevHoleSize(boolean value){
		this.autoPopulatePrevHoleSize = value;
	}
	
	public void setHoleSectionKpiUunit(String value){
		this.holeSectionKpiUunit = value;
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		autoPopulatePrevHoleSizeOnReportDaily(node,session,request);
		Object obj = node.getData();
		if (obj instanceof HoleSection) {
			HoleSection holeSection = (HoleSection) obj;
			String importParameter = holeSection.getImportParameters();
			if(StringUtils.isNotBlank(importParameter)) {
				String[] ip = importParameter.split("[.]");
				if(ip.length == 3) {
					String wbGeometryUid = ip[2];
					String hql = "UPDATE HoleSection SET isDeleted=true WHERE wellboreUid=:wellboreUid AND  (isDeleted IS NULL OR isDeleted = false) AND (importParameters IS NOT NULL AND importParameters != '') AND importParameters NOT LIKE '%." + wbGeometryUid + "'";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(hql, new String[] {"wellboreUid"}, new Object[] { holeSection.getWellboreUid() });
				}
			}
		}
		
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		autoPopulatePrevHoleSizeOnReportDaily(node,session,request);
	}
	
	private void autoPopulatePrevHoleSizeOnReportDaily(CommandBeanTreeNode node, UserSession session, HttpServletRequest request) throws Exception{
		Object object = node.getData();
		if (object instanceof HoleSection) {
			if(autoPopulatePrevHoleSize){
				UserSelectionSnapshot selection = new UserSelectionSnapshot(UserSession.getInstance(request));
				String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND reportType<>'DGR'";
				String[] paramsFields = {"selectedOperationUid"};
				Object[] paramsValues = {selection.getOperationUid()};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				ReportDaily thisReport = null;
				if(!lstResult.isEmpty()){
					for(int i = 0;i < lstResult.size();i++){
						thisReport = (ReportDaily) lstResult.get(i);
						if(thisReport != null){
							Calendar thisCalender = Calendar.getInstance();
							thisCalender.setTime(thisReport.getReportDatetime());
							thisCalender.add(Calendar.DATE, 1);
							thisCalender.add(Calendar.SECOND, -1);
							Date nextDate = thisCalender.getTime();
							
							String strSql2 = "FROM HoleSection WHERE (isDeleted = false or isDeleted is null) AND wellboreUid = :selectedWellboreUid AND dateTimeOut < :reportDayDate AND (dateTimeIn is not null) AND (dateTimeOut is not null) ORDER BY dateTimeOut DESC";
							String[] paramsFields2 = {"selectedWellboreUid","reportDayDate"};
							Object[] paramsValues2 = {thisReport.getWellboreUid(),nextDate};
							List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
							if (!lstResult2.isEmpty())
							{
								HoleSection temp = (HoleSection) lstResult2.get(0);
								double min = 0.0;
								double max = 0.0;
								if(temp.getHoleSize() != null){
									min = temp.getHoleSize();
									max = temp.getHoleSize();
								}
								for(Object hss : lstResult2){
									HoleSection hs = (HoleSection) hss;
									if(hs.getHoleSize() != null){
										if(hs.getHoleSize() < min){
											min = hs.getHoleSize();
										}
										if (hs.getHoleSize() > max){
											max = hs.getHoleSize();
										}
									}
								}
								if(nextDate.after(temp.getDateTimeOut())){
									if(min != 0.0){
										thisReport.setPrevHolesize(min);
									}else{
										thisReport.setPrevHolesize(null);
									}
								}else if(nextDate.equals(temp.getDateTimeOut())){
									if(max != 0.0){
										thisReport.setPrevHolesize(max);
									}else{
										thisReport.setPrevHolesize(null);
									}
								}
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
							}else{
								thisReport.setPrevHolesize(null);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
							}
						}
					}
				}
			}
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof HoleSection){
			HoleSection holeSection = (HoleSection)object;
			
			if (holeSection!=null){
				if(holeSection.getDateTimeIn() != null && holeSection.getDateTimeOut() != null){
					if(holeSection.getDateTimeIn().getTime() > holeSection.getDateTimeOut().getTime()){
						
						status.setContinueProcess(false, true);
						status.setFieldError(node, "dateTimeIn", "Date In can not be greater than Date out.");
						return;
					}
				}
			}
		}
		
		if (object instanceof HoleSectionKpi) {
			KpiUtils.handleConversion(commandBean, object, holeSectionKpiUunit);
		}
	}
}