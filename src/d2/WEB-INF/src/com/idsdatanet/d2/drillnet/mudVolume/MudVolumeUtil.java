package com.idsdatanet.d2.drillnet.mudVolume;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.MudVolumes;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * All common utility related to Mud Volume.
 * @author lling
 *
 */

public class MudVolumeUtil {
	
	/**
	 * Common Method for Cumulative losses volumes from first day until selected day (d2 wko)
	 * @param operationUid
	 * @param dailyUid
	 * @param type
	 * @param label
	 * @throws Exception
	 */
	public static double calculateCumulativeLossesVolumeFor(String operationUid, String dailyUid, String type, String label) throws Exception
	{
		Double totalVolume = 0.00;
		if (operationUid!=null && dailyUid!=null){
			Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
			
			if (selectedDaily!=null && selectedDaily.getDayDate()!=null){
				String[] paramsFields = {"operationUid", "userDate"};
				Object[] paramsValues = new Object[2]; paramsValues[0] = operationUid; paramsValues[1]= selectedDaily.getDayDate();
				String strSql = "select sum(mvd.volume) as totalVolume from MudVolumeDetails mvd where (mvd.isDeleted = false or mvd.isDeleted is null) and mvd.type = '"+type+"'"+(label!=null?"and mvd.label='"+label+"'":"")+" and mvd.mudVolumesUid IN (select mv.mudVolumesUid from Daily daily, MudVolumes mv where (mv.isDeleted = false or mv.isDeleted is null) and (daily.isDeleted = false or daily.isDeleted is null) and mv.dailyUid = daily.dailyUid and mv.operationUid =:operationUid and daily.dayDate <= :userDate) ";
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				Object a = (Object) lstResult.get(0);
				
				if (a != null) totalVolume = Double.parseDouble(a.toString());
				
			}
		}
		return totalVolume;
	}
	
	//18115
	public static Double calculateCumulativeLossesVolume(String operationUid, String dailyUid, String type, MudVolumes mudVolume) throws Exception
	{
		Double previousdaytotalcumlosses = 0.00;
		Double cumLossesnotnull = 0.00;
		Double totalNullLosses = 0.00;
		Double thisRecordCumLosses = 0.00;
		if (operationUid!=null && dailyUid!=null){
			Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
			
			if (selectedDaily!=null && selectedDaily.getDayDate()!=null){
				
				//get report datetime
				
				Date reportDateTime = null;
				String[] paramsFields = {"dailyUid", "operationUid"};
				Object[] paramsValues =  {dailyUid, operationUid};
				String strSql = "SELECT reportDatetime FROM ReportDaily WHERE dailyUid = :dailyUid AND " + 
						"operationUid = :operationUid and (isDeleted is null or isDeleted is false)";
				List listRsult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, 
						paramsValues);
				
				Object rdt = (Object) listRsult.get(0);
				
				if (rdt != null) reportDateTime = (Date)rdt;


				//get all previous day total cum losses
				String[] paramsFields1 = {"operationUid", "reportDatetime"};
				Object[] paramsValues1 =  {operationUid, reportDateTime };
				String strSql1 = "SELECT sum(volume) from MudVolumeDetails where mudVolumesUid in (SELECT "
						+ "mudVolumesUid FROM MudVolumes WHERE dailyUid in (select dailyUid from ReportDaily where "
						+ " reportDatetime < :reportDatetime AND " +  
					"operationUid = :operationUid) AND (isDeleted is null or isDeleted is false)) and (isDeleted is null or isDeleted is false) and type = '"+type+"'"; 
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, 
						paramsValues1);
				
				if(lstResult != null && lstResult.size() > 0) {
					if(lstResult.get(0) != null)
						previousdaytotalcumlosses = (Double) lstResult.get(0);
				}
				
				//previousDayTotalCumLosses
				//if checktime != null, get all record with checktime <= this record,  this record = this totalLosses + previousDayTotalCumLosses
				//elseif checktime == null, get sum of all not null records for today ,get sum of all null record for today, add both into previousDayTotalCumLosses.
				
				//get checktime
				Date checktime = mudVolume.getCheckTime();
				if(checktime != null) {
					String[] paramsFields5 = {"operationUid", "checkTime", "dailyUid"};
					Object[] paramsValues5 =  {operationUid,checktime, dailyUid};
					String strSql5 = "select SUM(volume) from MudVolumeDetails where mudVolumesUid in "
							+ "(select mudVolumesUid from MudVolumes where operationUid = :operationUid AND dailyUid = :dailyUid"
							+ " and checkTime <= :checkTime ) AND " 
							+ "(isDeleted = false or isDeleted is null) "
							+ "and type = '"+type+"'" ; 
					
					List lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramsFields5, 
							paramsValues5);
					if(lstResult5 != null && lstResult5.size() > 0) {
						if(lstResult5.get(0) != null)
							cumLossesnotnull = (Double) lstResult5.get(0);	
					}
					thisRecordCumLosses = cumLossesnotnull + previousdaytotalcumlosses;
				}else{
					String[] paramsFields5 = {"operationUid", "dailyUid"};
					Object[] paramsValues5 =  {operationUid, dailyUid};
					String strSql5 = "select SUM(volume) from MudVolumeDetails where mudVolumesUid in "
							+ "(select mudVolumesUid from MudVolumes where operationUid = :operationUid AND dailyUid = :dailyUid"
							+ " and checkTime IS NOT NULL ) AND " 
							+ "(isDeleted = false or isDeleted is null) "
							+ "and type = '"+type+"'" ; 
					
					List lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramsFields5, 
							paramsValues5);
					if(lstResult5 != null && lstResult5.size() > 0) {
						if(lstResult5.get(0) != null)
						cumLossesnotnull = (Double) lstResult5.get(0);
					}
					
					strSql5 = "select SUM(volume) from MudVolumeDetails where mudVolumesUid in "
							+ "(select mudVolumesUid from MudVolumes where operationUid = :operationUid AND dailyUid = :dailyUid"
							+ " and checkTime IS NULL ) AND " 
							+ "(isDeleted = false or isDeleted is null) "
							+ "and type = '"+type+"'" ;
					lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramsFields5, 
							paramsValues5);
					if(lstResult5 != null && lstResult5.size() > 0) {
						if(lstResult5.get(0) != null)
						totalNullLosses = (Double) lstResult5.get(0);						
					}
					thisRecordCumLosses = cumLossesnotnull + totalNullLosses + previousdaytotalcumlosses;
				}
				return thisRecordCumLosses;
			}
		}
		
		return null;
	}
	
	/* 18115  WKO P1 Changes */
	public static double calculateCumulativeLossesVolumeFor(String operationUid, String dailyUid, String type) throws Exception
	{
		return MudVolumeUtil.calculateCumulativeLossesVolumeFor(operationUid, dailyUid, type, null);
	}
	
	public static void calculateVolumePumpedandLosses(String dailyUid, String operationUid, CustomFieldUom thisConverter, String fieldname, String cumFieldName, String filter) throws Exception
	{		
		QueryProperties qp=new QueryProperties();
		qp.setUomConversionEnabled(false);
				
		//query out dayDate, mudVolumesUid, checkTime, dailyUid from mud volumes screen
		String sql = "select d.dayDate, mv.mudVolumesUid, mv."+ filter + ", mv.dailyUid from Daily d, MudVolumes mv where d.dailyUid=mv.dailyUid and (mv.isDeleted = '' or mv.isDeleted is null) and (d.isDeleted = '' or d.isDeleted is null) and mv.operationUid=:operationUid order by d.dayDate, mv." + filter;
										
 		List <Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,
				new String[] {"operationUid"},
				new Object[] {operationUid}, qp);
 		 		 		
 		if (lstResult.size() > 0){
			for (Object[] result : lstResult){
				
				Double totalVolume = 0.00;
				Object[] obj = (Object[]) result;
				
				String currentDailyUid = null;
				//get currentDailyUid 
				if (obj[3] != null && StringUtils.isNotBlank(obj[3].toString())){
					currentDailyUid = ( String ) obj[3];
				}
				
				Daily currentDay = ApplicationUtils.getConfiguredInstance().getCachedDaily( currentDailyUid );
	 			Date currentRecordDate = currentDay.getDayDate();
				
				Date checkTime = null;
				Integer sequence = null;
				String extraFilter = "";
				ArrayList pF = new ArrayList(); 
				ArrayList pV = new ArrayList(); 
				
				pF.add("operationUid");
				pV.add(operationUid);
				pF.add("currentRecordDate");
				pV.add(currentRecordDate);
				
				if ("checkTime".equals(filter)){
					if (obj[2] != null && StringUtils.isNotBlank(obj[2].toString())){
						checkTime = ( Date ) obj[2];
						pF.add("currentCheckTime");
						pV.add(checkTime);
						extraFilter = " and mv.checkTime <= :currentCheckTime ";
					}
				}
				if ("sequence".equals(filter)){
					if (obj[2] != null && StringUtils.isNotBlank(obj[2].toString())){
						sequence =Integer.parseInt( obj[2].toString());
						pF.add("sequence");
						pV.add(sequence);
						extraFilter = " and mv.sequence <= :sequence ";
					}
				}
				
				String[] paramsFields = (String[]) pF.toArray(new String [pF.size()]);
				Object[] paramsValues = pV.toArray();
				
				//get sum value of pumpedVolume where the mud volume day date is equal to the date from query1
				String Strsql = "select sum(mv." + fieldname + ") as cumVolume from Daily d, MudVolumes mv where d.dailyUid=mv.dailyUid and (mv.isDeleted = '' or mv.isDeleted is null) and (d.isDeleted = '' or d.isDeleted is null) and d.dayDate<:currentRecordDate and mv.operationUid=:operationUid";
				
				List lst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(Strsql,
						new String[] {"operationUid", "currentRecordDate"},
						new Object[] {operationUid, currentRecordDate}, qp);	
				
				if (lst.size()>0) {
					if(lst.get(0)!=null){
						totalVolume = Double.parseDouble(lst.get(0).toString());
					}
				}
				//get sum value of pumpedVolume where the mud volume day date is equal to the date from Strsql2
				String Strsql2 = "select sum(mv." + fieldname + ") as cumVolume from Daily d, MudVolumes mv where d.dailyUid=mv.dailyUid and (mv.isDeleted = '' or mv.isDeleted is null) and (d.isDeleted = '' or d.isDeleted is null) and d.dayDate=:currentRecordDate " + extraFilter + " and mv.operationUid=:operationUid";
			
				List lst2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(Strsql2, paramsFields, paramsValues, qp);	
				
				if (lst2.size()>0) {
					if(lst2.get(0)!=null){
						totalVolume += Double.parseDouble(lst2.get(0).toString());
					}
				}
				
				String mudVolumesUid = result[1].toString();
				//get base value for cumPumpedVolume and cumVolumeLosses 
				thisConverter.setBaseValueFromUserValue(totalVolume);
				totalVolume= thisConverter.getConvertedValue();
				//update the result value to selected mud volumes record 							
				//update the result to each of the record
				
				String str = "FROM MudVolumes WHERE (isDeleted = false or isDeleted is null) and mudVolumesUid =:mudVolumesUid";		
				
				List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(str, "mudVolumesUid", mudVolumesUid, qp);
				
				if (lstResult3.size() > 0){					
					MudVolumes mudVolumes = (MudVolumes) lstResult3.get(0);
					/*cumFieldName is the field for update 
					  set totalVolume to the cumFieldName field on the object 
					*/
					PropertyUtils.setProperty(mudVolumes, cumFieldName, totalVolume);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mudVolumes, qp);
				}
			}
 		}
 	}
		
	 /**
	 * Common Method for auto calculated cumulative volumes value (d2 santos csm)
	 * @param mudVolumesUid
	 * @param operationUid
	 * @param thisConverter
	 * @param fieldname
	 * @param type
	 * @param label
	 * @throws Exception
	 * */ 
	public static void calcCumulativeVolumeType(String mudVolumesUid, String operationUid, CustomFieldUom thisConverter, String fieldname, String type, String label) throws Exception { 
	{
			//variable declaration
			Double totalVolume = 0.00;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			String mudPropertiesUid="";
					
			if(mudVolumesUid!=null)
			{
				//get mud properties UID for current operation from sql1 
				String sql1 = "select mudPropertiesUid from MudVolumes where operationUid=:operationUid and mudVolumesUid=:mudVolumesUid and (isDeleted = false or isDeleted is null)";
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql1, new String[]{"operationUid","mudVolumesUid"}, new Object[] {operationUid, mudVolumesUid});
				
				if (list.size() > 0 ){
				    if(list.get(0)!=null)
				      mudPropertiesUid = list.get(0).toString();
				}
			}
			//get sum value based on the mud volume details type and label
			String[] paramsFields = {"operationUid", "mudPropertiesUid"};
			Object[] paramsValues = new Object[2]; paramsValues[0] = operationUid;  paramsValues[1] = mudPropertiesUid;
			String strSql = "select sum(mvd.volume) as totalVolume from MudVolumeDetails mvd, MudVolumes mv where (mvd.isDeleted = false or mvd.isDeleted is null) and (mv.isDeleted = false or mv.isDeleted is null) and mvd.type = '"+type+"'"+(label!=null?"and mvd.label='"+label+"'":"")+" and mvd.mudVolumesUid=mv.mudVolumesUid and mv.operationUid =:operationUid and mv.mudPropertiesUid=:mudPropertiesUid ";
					
			List lstResultVolume = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
								
			if (lstResultVolume.size()>0) {
				if (lstResultVolume.get(0)!=null)
				{
					//sum value from query 
					totalVolume = Double.parseDouble(lstResultVolume.get(0).toString());
				}
			}
						
			//get base value for totalVolume 
			thisConverter.setBaseValueFromUserValue(totalVolume);
			totalVolume= thisConverter.getConvertedValue();
			//update the result value to selected mud properties record 							
			String str = "FROM MudProperties WHERE (isDeleted = false or isDeleted is null) and mudPropertiesUid =:mudPropertiesUid";		
			
			List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(str, "mudPropertiesUid", mudPropertiesUid, qp);
			
			if (lstResult3.size() > 0){					
				MudProperties mudProperties = (MudProperties) lstResult3.get(0);
				/*fieldname is the field for update 
				  set totalVolume to the fieldname field on the object 
				*/
				PropertyUtils.setProperty(mudProperties, fieldname, totalVolume);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mudProperties, qp);
			}
		}
	}
	
	/**
	 * Common Method to calculate total volume for each mud volume record (based on type)
	 * @param operationUid
	 * @param dailyUid
	 * @param mudVolumesUid
	 * @param type
	 * @throws Exception
	 */
	
	public static double calculateTotalVolumeFor(String operationUid, String dailyUid, String mudVolumesUid, String type) throws Exception
	{
		return MudVolumeUtil.calculateTotalVolumeFor(operationUid, dailyUid, mudVolumesUid, type, null);
	}
	/**
	 * Common Method to calculate total volume for each mud volume record (based on type and label)
	 * @param operationUid
	 * @param dailyUid
	 * @param mudVolumesUid
	 * @param type
	 * @param label
	 * @return totalVolume
	 * @throws Exception
	 */

	public static double calculateTotalVolumeFor(String operationUid, String dailyUid, String mudVolumesUid, String type, String label) throws Exception
	{
		String[] paramsFields = {"dailyUid", "operationUid", "mudVolumeUid"};
		Object[] paramsValues = new Object[3]; paramsValues[0] = dailyUid; paramsValues[1] = operationUid; paramsValues[2] = mudVolumesUid;
		String strSql = "select sum(mvd.volume) as totalVolume from MudVolumeDetails mvd, MudVolumes mv where (mvd.isDeleted = false or mvd.isDeleted is null) and (mv.isDeleted = false or mv.isDeleted is null) and mvd.type = '"+type+"'"+(label!=null?" and mvd.label='"+label+"'":"")+" and mvd.mudVolumesUid = mv.mudVolumesUid and mv.dailyUid = :dailyUid and mv.operationUid = :operationUid and mv.mudVolumesUid =:mudVolumeUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		Object a = (Object) lstResult.get(0);
		Double totalVolume = 0.00;
		if (a != null) totalVolume = Double.parseDouble(a.toString());
		return totalVolume;
	}
	
	/**
	 * Method to calculate total volume loses and update the cum_volume_losses from the day it was edit until end of the operation day 
	 * @param operationUid
	 * @param dailyUid
	 * @param commandBean
	 */
	
	public static void updateCumLossesVolumeToDateForOperation(String operationUid, String dailyUid) throws Exception
	{
		QueryProperties qp=new QueryProperties();
		qp.setUomConversionEnabled(false);
		Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);		
		String strSql = "from Daily where (isDeleted = false or isDeleted is null) and dayDate>= :dayDate and operationUid=:operationUid order by dayDate asc";
		List <Daily> lstResult= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"dayDate","operationUid"}, new Object[]{selectedDaily.getDayDate(),operationUid});
		if (lstResult.size() > 0){	
			for (Daily getDaily : lstResult) {
				String[] paramsFields = {"dailyUid", "operationUid"};
				Object[] paramsValues = new Object[2]; paramsValues[0] = getDaily.getDailyUid(); paramsValues[1] = operationUid;
				String queryMud = "FROM MudVolumes where (isDeleted = false or isDeleted is null) and dailyUid=:dailyUid and operationUid=:operationUid";
				// retrieve data from DB based on base value instead of on-screen value (ticket: 19198)
				List<MudVolumes> mudList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryMud, paramsFields, paramsValues, qp);
				if (mudList.size() > 0){	
					for (MudVolumes getMud : mudList) {
						Double cumVolumeLosses = MudVolumeUtil.calculateCumulativeLossesVolumeFor(operationUid, getDaily.getDailyUid(), "loss", null);
	
						getMud.setCumVolumeLosses(cumVolumeLosses);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(getMud,qp);
					}
					}
		}
		
	}
	}
}
