package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.Date;

public class BHAFootageSummary {
	
	private String wellUid;
	private String operationUid;
	private String country;
	private String operationArea;
	private String operationCode;
	private String bharunUid;
	private Date timeIn;
	private Date timeOut;
	private String toolCategory;
	private Double toolSize;
	private String dailyUid;
	private Date dayDate;
	private String section;
	private String directionalCompany;
	private Double progress;
	private Double duration;
	private Boolean failure;
	private Double reliability;
	private String wellboreUid;
	private String wellboreName;
	private String wellName;
	private Boolean isCompetitor;
	public BHAFootageSummary(String wellUid, String operationUid,
			String country, String operationArea, String bharunUid,
			Date timeIn, Date timeOut, String toolCategory,
			Double toolSize, String dailyUid, Date dayDate,String section,
			String directionalCompany, Double progress) {
		super();
		this.wellUid = wellUid;
		this.operationUid = operationUid;
		this.country = country;
		this.operationArea = operationArea;
		this.bharunUid = bharunUid;
		this.timeIn = timeIn;
		this.timeOut = timeOut;
		this.toolCategory = toolCategory;
		this.toolSize = toolSize;
		this.dailyUid = dailyUid;
		this.dayDate = dayDate;
		this.directionalCompany = directionalCompany;
		this.progress = progress;
		this.section = section;
	}
	
	public void setWellUid(String wellUid) {
		this.wellUid = wellUid;
	}
	public String getWellUid() {
		return wellUid;
	}
	public void setOperationUid(String operationUid) {
		this.operationUid = operationUid;
	}
	public String getOperationUid() {
		return operationUid;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry() {
		return country;
	}
	public void setOperationArea(String operationArea) {
		this.operationArea = operationArea;
	}
	public String getOperationArea() {
		return operationArea;
	}
	public void setBharunUid(String bharunUid) {
		this.bharunUid = bharunUid;
	}
	public String getBharunUid() {
		return bharunUid;
	}
	public void setTimeIn(Date timeIn) {
		this.timeIn = timeIn;
	}
	public Date getTimeIn() {
		return timeIn;
	}
	public void setDayDate(Date dayDate) {
		this.dayDate = dayDate;
	}
	public Date getDayDate() {
		return dayDate;
	}
	public void setProgress(Double progress) {
		this.progress = progress;
	}
	public Double getProgress() {
		return progress;
	}
	public void setTimeOut(Date timeOut) {
		this.timeOut = timeOut;
	}
	public Date getTimeOut() {
		return timeOut;
	}
	public void setToolCategory(String toolCategory) {
		this.toolCategory = toolCategory;
	}
	public String getToolCategory() {
		return toolCategory;
	}
	public void setToolSize(Double toolSize) {
		this.toolSize = toolSize;
	}
	public Double getToolSize() {
		return toolSize;
	}
	public void setDailyUid(String dailyUid) {
		this.dailyUid = dailyUid;
	}
	public String getDailyUid() {
		return dailyUid;
	}
	public void setDirectionalCompany(String directionalCompany) {
		this.directionalCompany = directionalCompany;
	}
	public String getDirectionalCompany() {
		return directionalCompany;
	}

	public void setDuration(Double duration) {
		this.duration = duration;
	}

	public Double getDuration() {
		return duration;
	}

	public void setReliability(Double reliability) {
		this.reliability = reliability;
	}

	public Double getReliability() {
		return reliability;
	}

	public void setFailure(Boolean failure) {
		this.failure = failure;
	}

	public Boolean getFailure() {
		return failure;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getSection() {
		return section;
	}

	public void setWellboreUid(String wellboreUid) {
		this.wellboreUid = wellboreUid;
	}

	public String getWellboreUid() {
		return wellboreUid;
	}

	public void setWellboreName(String wellboreName) {
		this.wellboreName = wellboreName;
	}

	public String getWellboreName() {
		return wellboreName;
	}

	public void setWellName(String wellName) {
		this.wellName = wellName;
	}

	public String getWellName() {
		return wellName;
	}

	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

	public String getOperationCode() {
		return operationCode;
	}

	public void setIsCompetitor(Boolean isCompetitor) {
		this.isCompetitor = isCompetitor;
	}

	public Boolean getIsCompetitor() {
		return isCompetitor;
	}
	
	
}
