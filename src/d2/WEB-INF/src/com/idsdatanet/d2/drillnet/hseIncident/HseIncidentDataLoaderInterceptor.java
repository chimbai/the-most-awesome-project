package com.idsdatanet.d2.drillnet.hseIncident;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class HseIncidentDataLoaderInterceptor implements DataLoaderInterceptor {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private static final String CUSTOM_FROM_MARKER = "{_custom_from_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		if(StringUtils.isNotBlank(fromClause) && fromClause.indexOf(CUSTOM_FROM_MARKER) < 0) return null;
		
		String currentClass	= meta.getTableClass().getSimpleName();
		String customFrom = "";
		
		if(currentClass.equals("LeadingIndicator") || currentClass.equals("LaggingIndicator") || currentClass.equals("OtherIndicator") || currentClass.equals("DailyObservation")) {
			customFrom = "HseIncident";			
			return fromClause.replace(CUSTOM_FROM_MARKER, customFrom);
		}
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String currentClass		= meta.getTableClass().getSimpleName();
		String customCondition 	= "";
		String eventType = "";
		
		if(currentClass.equals("LeadingIndicator")) eventType = "leading";
		if(currentClass.equals("LaggingIndicator")) eventType = "lagging";
		if(currentClass.equals("OtherIndicator")) eventType = "other";
		if(currentClass.equals("DailyObservation")) eventType = "observation";
		
		if(currentClass.equals("LeadingIndicator") || currentClass.equals("LaggingIndicator") || currentClass.equals("OtherIndicator") || currentClass.equals("DailyObservation")) {
			customCondition = "(isDeleted = false or isDeleted is null) and dailyUid=:currentDailyUid and group_uid=:groupUid ";
			customCondition += "and systemStatus=:eventType";
			query.addParam("currentDailyUid", userSelection.getDailyUid());
			query.addParam("groupUid", userSelection.getGroupUid());
			query.addParam("eventType", eventType);
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		return null;
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

}
