package com.idsdatanet.d2.drillnet.matrix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.menu.MenuItemEx;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.ControllerBeanNameHandlerMapping;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;
import com.idsdatanet.d2.core.web.security.BaseAclPermission;
/**
 * Main matrix manager to process and generate Data Population Matrix.
 * @author RYAU
 *
 */
public class MatrixManager extends DefaultBeanSupport implements InitializingBean {
	private static final String MAIN_MENU = "mainmenu";
	private static final String URL_MAPPING_BEAN_NAME = "urlMapping";
	private List<Map<String, Object>> matrixDefinitionList = null;
	private DefaultMatrixHandler defaultMatrixHandler = null;
	private Map<String, Object> customMatrixHandler = null;
	private String matrixType = null;
	private LinkedHashMap<String, List<Matrix>> result = null;
		
	/**
	 * Set Matrix Data Definition
	 * @param matrixDefinition
	 */
	public void setMatrixDefinition(List<Map<String, Object>> matrixDefinition) {
		this.matrixDefinitionList = matrixDefinition;
	}
	/**
	 * Set Default Matrix Handler
	 * @param defaultMatrixHandler
	 */
	public void setDefaultHandler(DefaultMatrixHandler defaultMatrixHandler) {
		this.defaultMatrixHandler = defaultMatrixHandler;
	}
	
	/**
	 * Set Custom Matrix Handler
	 * @param defaultMatrixHandler
	 */
	public void setCustomHandler(Map<String, Object> customMatrixHandler) {
		this.customMatrixHandler = customMatrixHandler;
	}
	
	/**
	 * Set Matrix Type - "DDR", "DGR" ...
	 * @param matrixType
	 */
	public void setMatrixType(String matrixType) {
		this.matrixType = matrixType;
	}
	/**
	 * Main method to process Matrix Data
	 * @param userSelection
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, List<Matrix>> processMatrix(UserSelectionSnapshot userSelection, UserSession session) throws Exception {
		result = new LinkedHashMap();
		if(this.matrixDefinitionList != null) {
			this.processMatrixDefinitionFromList(userSelection, session, this.matrixDefinitionList);
		} else {
			this.processMatrixDefinitionFromMenuItems(userSelection, session, MenuManager.getConfiguredInstance().getMenu(session), null);
		}
		return result;
	}
	
	/**
	 * Main method to process Matrix Data Definition from XML
	 * @param userSelection
	 * @param session
	 * @param matrixDefinitionList
	 * @return
	 * @throws Exception
	 */
	private void processMatrixDefinitionFromList(UserSelectionSnapshot userSelection, UserSession session, List<Map<String, Object>> matrixDefinitionList) throws Exception {
		for(Map definition : matrixDefinitionList) {
			MatrixDataDefinitionMeta meta = new MatrixDataDefinitionMeta(definition);
			this.populateMatrix(userSelection, session, meta);
		}
	}
	
	/**
	 * Main method to process Matrix Data Definition from Menu Items
	 * @param userSelection
	 * @param session
	 * @param menuItemMap
	 * @param parent
	 * @return
	 * @throws Exception
	 */
	private void processMatrixDefinitionFromMenuItems(UserSelectionSnapshot userSelection, UserSession session, Map<String, List<MenuItemEx>> menuItemMap, String parent) throws Exception {
		if (StringUtils.isBlank(parent)) {
			MatrixDataDefinitionMeta meta = new MatrixDataDefinitionMeta(new HashMap<String, Object>());
			meta.setTableName("ReportDaily");
			meta.setBeanName("reportDailyController");
			meta.setTitle("Qc-Status");
			this.populateMatrix(userSelection, session, meta);
			parent = MAIN_MENU;
		}
		if (menuItemMap.containsKey(parent)) {
			List<MenuItemEx> menuItems = menuItemMap.get(parent);
			for (MenuItemEx menuItem : menuItems) {
				if (StringUtils.isNotBlank(menuItem.getDataMatrixSectionName())) {
					MatrixDataDefinitionMeta meta = new MatrixDataDefinitionMeta(session, menuItem, this.getApplicationContext());
					if (customMatrixHandler.containsKey(meta.getBeanName())) {
						Object handler = customMatrixHandler.get(meta.getBeanName());
						if (handler instanceof MatrixHandler) {
							meta.setCustomHandler((MatrixHandler) handler);
						}
						this.populateMatrix(userSelection, session, meta);
					} else if (meta.hasSummaryTable()) {
						this.populateMatrix(userSelection, session, meta);
					}
					
				}
				this.processMatrixDefinitionFromMenuItems(userSelection, session, menuItemMap, menuItem.getMenuItemUid()); //do recursive 
			}
		}
	}
	
	/**
	 * Main method to process Matrix from MatrixDataDefinitionMeta
	 * @param userSelection
	 * @param session
	 * @param meta
	 * @return
	 * @throws Exception
	 */
	private void populateMatrix(UserSelectionSnapshot userSelection, UserSession session, MatrixDataDefinitionMeta meta) throws Exception {
		Matrix matrix = new Matrix(meta);
		String section = meta.getSection() != null? meta.getSection() : "";
		
		if(!AclManager.getConfiguredInstance().getAccess(BaseAclPermission.ACL_KEY_READ, meta.getBeanName(), session)) {
			return;
		}
		if(meta.getCustomHandler() == null) {
			defaultMatrixHandler.process(matrix, userSelection);
		} else {
			meta.getCustomHandler().process(matrix, userSelection);
		}
		
		// check for availability of url
		if(StringUtils.isBlank(matrix.getMetaDefinition().getUrl())) {
			matrix.setUrl(this.getControllerBeanNameHandlerMapping().getPathMapping(matrix.getMetaDefinition().getBeanName()));
		}
		
		List<Matrix> matrixList = result.get(section);
		if (matrixList == null) {
			matrixList = new ArrayList<Matrix>();
		}
		matrixList.add(matrix);
		result.put(section, matrixList);
	}
	
	/**
	 * Return a list of ReportDaily according to the reportType set from matrixType property and current operationUid.
	 * @param userSelection
	 * @return
	 * @throws Exception
	 */
	public List<ReportDaily> getMatrixDays(UserSelectionSnapshot userSelection) throws Exception {
		
		//BY DEFAULT, GET MATRIX DAYS FOR DDR REPORT TYPE
		String[] paramNames = {"reportType", "operationUid"};
		String[] paramValues = {matrixType, userSelection.getOperationUid()}; 
		List<ReportDaily> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and reportType = :reportType and operationUid = :operationUid order by reportDatetime, reportNumber", paramNames, paramValues);
		
		//IF NO MATRIX DAY IS FOUND, GET DAY FROM OTHER REPORT TYPES
		if(list.size() < 1)
		{
			Map<String, String> reportTypePriorityWhenDrllgNotExist = CommonUtils.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist();
			String operationType = userSelection.getOperationType();
			
			String strReportTypes = reportTypePriorityWhenDrllgNotExist.get(operationType);
			if (strReportTypes != null) {
				String[] reportTypes = strReportTypes.split("[,]");
				for (String reportType : reportTypes) {
					String[] paramValues2 = {reportType, userSelection.getOperationUid()};
					list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and reportType = :reportType and operationUid = :operationUid order by reportDatetime, reportNumber", paramNames, paramValues2);
				
					if(list.size() > 0)
					{
						return list;
					}
				}
			}
		}
			
		if(list.size() > 0) {
			return list;
		}
		return null;
	}
	
	/**
	 * Return a ControllerBeanNameHandlerMapping to retrieve current beanName's url
	 * @return
	 */
	private ControllerBeanNameHandlerMapping getControllerBeanNameHandlerMapping() {
		return (ControllerBeanNameHandlerMapping) this.getApplicationContext().getBean(URL_MAPPING_BEAN_NAME);
	}
	
	public void afterPropertiesSet() throws Exception {
		if(defaultMatrixHandler == null) throw new Exception("a default matrix handler must be set in " + this.getClass().getName());
		if(matrixType == null) throw new Exception("type of matrix must be set in " + this.getClass().getName() + ". Possible type are 'DDR', 'DGR' and so on.");
	}
}
