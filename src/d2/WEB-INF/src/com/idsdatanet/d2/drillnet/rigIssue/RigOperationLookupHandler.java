package com.idsdatanet.d2.drillnet.rigIssue;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class RigOperationLookupHandler implements LookupHandler {	
		
	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();	
		
		if (commandBean.getRoot().getDynaAttr().get("rigInformationUid").toString()!=null)
		{
			String sql = "select operationUid, operationName from Operation where (isDeleted = false or isDeleted is null) and rigInformationUid=:rigInformationUid order by operationName";
			
			List<Object []> rigOperationResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "rigInformationUid", commandBean.getRoot().getDynaAttr().get("rigInformationUid").toString());
			if (rigOperationResults.size() > 0) {
				for (Object[] rigOperationResult : rigOperationResults) {
					if (rigOperationResult[0] != null && rigOperationResult[1] != null) {					
						result.put(rigOperationResult[0].toString(), new LookupItem(rigOperationResult[0].toString(), rigOperationResult[1].toString()));
					}
				}
			}
			else
			{
				commandBean.getRoot().getDynaAttr().put("operationUid", null);
			}
		}
		return result;
	}
}

