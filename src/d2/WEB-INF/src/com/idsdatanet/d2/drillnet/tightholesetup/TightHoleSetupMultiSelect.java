package com.idsdatanet.d2.drillnet.tightholesetup;

import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.multiselect.StandardTreeNodeMultiSelect;

/**
 * To hide idsadmin from being modified by the screen as per the original D2 behaviour.
 * 
 * @author mhchai
 */
public class TightHoleSetupMultiSelect extends StandardTreeNodeMultiSelect{
	/**
	 * Load multi-select field's values for the specified data node
	 */
	public List<String> loadMultiSelectKeys(CommandBeanTreeNode node) throws Exception {
		String parent_id = BeanUtils.getProperty(node.getData(), this.parentIdField);
		return this.daoManager.findByNamedParam("select " + this.childLookupField + " from " + this.childTableClass.getName() + " where (isDeleted = false or isDeleted is null) and " + this.childIdField + " = :parent_id and " + this.childLookupField + " not in (select u." + this.childLookupField + " from User u where (u.isDeleted = false or u.isDeleted is null) and u.userName = 'idsadmin') and " + this.childLookupField + " not in (select u." + this.childLookupField + " from User u where (u.isDeleted = false or u.isDeleted is null) and u.idsUser = true)", new String[] {"parent_id"}, new Object[] {parent_id});
	}

}
