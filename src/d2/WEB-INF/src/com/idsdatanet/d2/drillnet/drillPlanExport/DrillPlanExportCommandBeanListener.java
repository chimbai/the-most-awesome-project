package com.idsdatanet.d2.drillnet.drillPlanExport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math.util.MathUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.LookupClassCode;
import com.idsdatanet.d2.core.model.LookupRootCauseCode;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.service.Manager;
import com.idsdatanet.d2.core.uom.UOMConverter;
import com.idsdatanet.d2.core.uom.exceptions.UOMConverterException;
import com.idsdatanet.d2.core.uom.hibernate.tables.UomTemplate;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.uom.mapping.UOMManager;
import com.idsdatanet.d2.core.web.helper.FileDownloadWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class DrillPlanExportCommandBeanListener implements DataNodeLoadHandler, CommandBeanListener {
	private static class ZipOutput{
		final OutputStream outputStream;
		final ZipOutputStream zipOut;
		
		ZipOutput(OutputStream out){
			this.outputStream = out;
			this.zipOut = new ZipOutputStream(out);
		}
		
		ZipOutput zip(String name, CSV csv) throws IOException {
			this.zipOut.putNextEntry(new ZipEntry(name));
			this.zipOut.write(csv.toCSV(true).getBytes("utf-8"));
			return this;
		}
		
		void close() throws IOException {
			this.zipOut.close();
			this.outputStream.close();
		}
	}
	
	private static class CSV{
		private StringBuffer sbValues = null;
		private boolean firstField = true;
		private StringBuffer sbHeaders = new StringBuffer();
		
		CSV(String... headers){
			for(int i=0; i < headers.length; ++i) {
				if(i > 0) this.sbHeaders.append(",");
				this.sbHeaders.append(headers[i]);
			}
		}
		
		String toCSV(boolean includeHeader) {
			return (includeHeader && this.sbHeaders.length() > 0 ? this.sbHeaders.toString() + "\n" : "") + (sbValues != null ? sbValues.toString() : "");
		}
		
		void addLine() {
			if(this.sbValues == null) {
				this.sbValues = new StringBuffer();
			}else {
				sbValues.append("\n");
			}
			this.firstField = true;
		}
		
		void addValue(String value) {
			if(!this.firstField) {
				this.sbValues.append(",");
			}else {
				this.firstField = false;
			}
			this.sbValues.append(this.addCSVQuote(value));
		}

		private String addCSVQuote(String value) {
			if(value == null) return "";
			if(value.contains("\"")) {
				return "\"" + value.replace("\"", "\"\"") + "\"";
			}
			if(value.contains(" ") || value.contains(",")) {
				return "\"" + value + "\"";
			}
			return value;
		}
	}

	private static class ActivityLookup{
		private final Map<String,String> classCodeLookup = new HashMap<String,String>();
		private final Map<String,String> rootCauseCodeLookup = new HashMap<String,String>();
		
		ActivityLookup(Operation operation, Manager daoManager){
			loadClassCodeLookup(daoManager, operation);
			loadRootCauseCodeLookup(daoManager, operation);
		}
		
		@SuppressWarnings("unchecked")
		void loadRootCauseCodeLookup(Manager daoManager, Operation operation){
			List<LookupRootCauseCode> rs = daoManager.findByNamedParam("from LookupRootCauseCode where (isDeleted = false or isDeleted is null) and (operationCode = :operationCode OR operationCode = '' OR operationCode is null)", "operationCode", operation.getOperationCode());
			for(LookupRootCauseCode item: rs) {
				this.rootCauseCodeLookup.put(toLowercase(item.getShortCode()), item.getName());
			}
		}
		
		@SuppressWarnings("unchecked")
		void loadClassCodeLookup(Manager daoManager, Operation operation){
			List<LookupClassCode> rs = daoManager.findByNamedParam("from LookupClassCode where (isDeleted = false or isDeleted is null) and (operationCode = :operationCode OR operationCode = '' OR operationCode is null)", "operationCode", operation.getOperationCode());
			for(LookupClassCode item: rs) {
				this.classCodeLookup.put(toLowercase(item.getShortCode()), item.getInternalCode());
			}
		}
		
		String toLowercase(String value) {
			return value == null ? "" : value.toLowerCase();
		}
		
		String getTimeClassification(Activity activity) {
			String value = this.classCodeLookup.get(toLowercase(activity.getClassCode()));
			if("TP".equalsIgnoreCase(value) || "TU".equalsIgnoreCase(value)) {
				return "Non Productive";
			}else if("P".equalsIgnoreCase(value) || "U".equalsIgnoreCase(value)) {
				return "Productive";
			}else {
				return null;
			}
		}
		
		String getRootCauseCode(Activity activity) {
			return this.rootCauseCodeLookup.get(toLowercase(activity.getRootCauseCode()));
		}
	}

	private byte[] reportOutputBytes = null;
	private String uomTemplateName = null;
	private boolean useCustomUomConversion = true;
	private static final Log logger = LogFactory.getLog(DrillPlanExport.class);
	private static final String LENGTH_METER = "LengthMeasure.Metre";
	private static final String LENGTH_INCH = "LengthMeasure.Inch";
	private static final String ANGLE_DEGREE = "AngleMeasure.Degree";
	private static final String DOUBLE_PRECISION = "0.00";
	
	public void setUseCustomUomConversion(boolean value) {
		this.useCustomUomConversion = value;
	}
	
	public void setUomTemplateName(String value) {
		this.uomTemplateName = value;
	}
	
	@Override
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		
		DrillPlanExport drillPlanExport = new DrillPlanExport();
		drillPlanExport.setId("1");
		List<Object> list = new ArrayList<Object>();
		list.add(drillPlanExport);
		return list;
	}

	@Override
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return meta.getTableClass().equals(DrillPlanExport.class);
	}

	@Override
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
	}

	@Override
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
	}

	@Override
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
	}

	@Override
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
	}

	@Override
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
	}

	@Override
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response,
			BaseCommandBean commandBean, String invocationKey) throws Exception {
		
		if("generate_report".equals(invocationKey)) {
			JSONArray selectedWells = JSONArray.fromObject(request.getParameter("selectedWells"));
			List<String> wells = new ArrayList<String>();
			for(int i=0; i < selectedWells.size(); ++i) {
				wells.add(selectedWells.getString(i));
			}
			Map<String,String> result = new HashMap<String,String>();
			try {
				generateReport(wells, response, getUomTemplateUid(request));
				result.put("status","ok");	
			}catch(Exception ex) {
				logger.error("Error generating drillplan export csv", ex);
				result.put("error", "Error generating CSV, please contact support for further action");
			}
			outputJson(result, response);
		}else if("downloadReport".equals(invocationKey)) {
			ByteArrayInputStream in = new ByteArrayInputStream(this.reportOutputBytes);
			FileDownloadWriter.writeFileOutputFromStream(request, response, in, true, null, "application/zip", this.reportOutputBytes.length);
			this.reportOutputBytes = null;
		}
	}

	@Override
	public void init(CommandBean commandBean) throws Exception {
	}
	
	private String getUomTemplateUid(HttpServletRequest request) throws Exception {
		if(this.useCustomUomConversion) {
			return null;
		}
		if(StringUtils.isNotBlank(this.uomTemplateName)) {
			for(UomTemplate item: UOMManager.getUomTemplates()) {
				if(this.uomTemplateName.equalsIgnoreCase(item.getName())) {
					return item.getUomTemplateUid();
				}
			}
			throw new Exception("UOM Template not found: " + this.uomTemplateName);
		}else {
			return UserSession.getInstance(request).getCurrentUOMTemplateUid();
		}
	}

	@SuppressWarnings("rawtypes")
	private static void outputJson(Map data, HttpServletResponse response) throws IOException{
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		response.getWriter().write(JSONObject.fromObject(data).toString());
	}

	@SuppressWarnings("unchecked")
	private void generateReport(List<String> wells, HttpServletResponse response, String uomTemplateUid) throws Exception {
		Manager daoManager = ApplicationUtils.getConfiguredInstance().getDaoManager();
		List<Operation> operations = daoManager.findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and wellUid in (:wells) and operationCode = :operationCode", new String[] {"wells", "operationCode"}, new Object[] {wells, "DRLLG"});
		CSV csvOperation = collectOperationDetails(operations, daoManager);
		CSV csvCasing = new CSV("WellGUID","WellName","Type","TopDepthMD","BottomDepthMD","OutsideDiameter","UPDATE_DATE");
		CSV csvSection = new CSV("WellGUID","WellName","Diameter","TopDepthMD","BottomDepthMD","UPDATE_DATE");
		collectCasingDetails(operations, csvCasing, csvSection, uomTemplateUid, daoManager);
		CSV csvRig = collectRigDetails(operations, uomTemplateUid, daoManager);
		CSV csvSurvey = collectSurveyDetails(operations, uomTemplateUid, daoManager);
		CSV csvActivity = collectActivityDetails(operations, uomTemplateUid, daoManager);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		ZipOutput zip = new ZipOutput(out);
		zip.zip("Well.csv", csvOperation)
			.zip("Casing.csv", csvCasing)
			.zip("Section.csv", csvSection)
			.zip("Rig.csv", csvRig)
			.zip("Survey.csv", csvSurvey)
			.zip("Activity.csv", csvActivity)
			.close();
	
		this.reportOutputBytes = out.toByteArray();
	}

	private CSV collectActivityDetails(List<Operation> operations, String uomTemplateUid, Manager daoManager) throws Exception {
		CSV csv = new CSV("WellGUID","WellName","WellboreName","WellboreDiameter","StartDate","EndDate","ActivityStartDepthMD","ActivityEndDepthMD","TopDepthMD","BottomDepthMD","TopDepthTVD","BottomDepthTVD","ActivityCodeL1","ActivityCodeL2","ActivityCodeL3","ActivityCodeL4","ActivityCodeL5","ActivityCodeL6","Description","TimeClassification","NPTDetail","NPTReason","UPDATE_DATE");
		for(Operation operation: operations) {
			collectActivityDetails(operation, csv, uomTemplateUid, daoManager);
		}
		return csv;
	}

	@SuppressWarnings("unchecked")
	private void collectActivityDetails(Operation operation, CSV csv, String uomTemplateUid, Manager daoManager) throws Exception {
		Wellbore wellbore = (Wellbore) daoManager.getObject(Wellbore.class, operation.getWellboreUid());
		ActivityLookup activityLookup = new ActivityLookup(operation, daoManager);
		List<Activity> rs = daoManager.findByNamedParam("from Activity where (isDeleted = false or isDeleted is null) and (isOffline = false or isOffline is null) and (isSimop = false or isSimop is null) and dayPlus != 1 and dailyUid in (select dailyUid from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid = :rdOperationUid) and operationUid = :operationUid order by startDatetime", new String[] {"rdOperationUid", "operationUid"}, new String[] {operation.getOperationUid(), operation.getOperationUid()}, getQueryProperties(uomTemplateUid, operation));
		for(Activity activity: rs) {
			csv.addLine();
			csv.addValue(operation.getOperationUid());
			csv.addValue(operation.getOperationName());
			csv.addValue(wellbore.getWellboreName());
			csv.addValue(convertFromBase(activity.getHoleSize(), LENGTH_INCH, DOUBLE_PRECISION));
			csv.addValue(format(activity.getStartDatetime(),"MM/dd/yyyy HH:mm"));
			csv.addValue(format(activity.getEndDatetime(),"MM/dd/yyyy HH:mm"));
			csv.addValue(null); //ActivityStartDepthMD
			csv.addValue(null); //ActivityEndDepthMD
			csv.addValue(convertFromBase(activity.getDepthFromMdMsl(), LENGTH_METER, DOUBLE_PRECISION));
			csv.addValue(convertFromBase(activity.getDepthMdMsl(), LENGTH_METER, DOUBLE_PRECISION));
			csv.addValue(null); //TopDepthTVD
			csv.addValue(null); //BottomDepthTVD
			csv.addValue(activity.getJobTypeCode());
			csv.addValue(activity.getTaskCode());
			csv.addValue(null); //ActivityCodeL3
			csv.addValue(null); //ActivityCodeL4
			csv.addValue(null); //ActivityCodeL5
			csv.addValue(null); //ActivityCodeL6
			csv.addValue(concat(activity.getAdditionalRemarks(), activity.getActivityDescription()));
			csv.addValue(activityLookup.getTimeClassification(activity));
			csv.addValue(activityLookup.getRootCauseCode(activity)); //NPTDetail
			csv.addValue(null); //NPTReason
			csv.addValue(null); //UPDATE_DATE
		}
	}
	
	private static String concat(String v1, String v2) {
		boolean v1Blank = StringUtils.isBlank(v1);
		boolean v2Blank = StringUtils.isBlank(v2);
		if(!v1Blank && !v2Blank) {
			return v1.trim() + "\n\n" + v2.trim();
		}else if(v1Blank && v2Blank) {
			return null;
		}else if(!v1Blank) {
			return v1.trim();
		}else {
			return v2.trim();
		}
	}
	
	private CSV collectSurveyDetails(List<Operation> operations, String uomTemplateUid, Manager daoManager) throws Exception {
		CSV csv = new CSV("WellGUID","WellName","MD","Inclination","Azimuth","TVD","NS","EW","UPDATE_DATE");
		for(Operation operation: operations) {
			collectSurveyDetails(operation, csv, uomTemplateUid, daoManager);
		}
		return csv;
	}
	
	@SuppressWarnings("unchecked")
	private void collectSurveyDetails(Operation operation, CSV csv, String uomTemplateUid, Manager daoManager) throws Exception {
		List<String> rsSurveyReference = daoManager.findByNamedParam("select surveyReferenceUid from SurveyReference where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid", "wellboreUid", operation.getWellboreUid());
		if(!rsSurveyReference.isEmpty()) {
			List<SurveyStation> rs = daoManager.findByNamedParam("from SurveyStation where (isDeleted = false or isDeleted is null) and surveyReferenceUid = :surveyReferenceUid order by depthMdMsl", "surveyReferenceUid", rsSurveyReference.get(0), getQueryProperties(uomTemplateUid, operation));
			for(SurveyStation surveyStation: rs) {
				csv.addLine();
				csv.addValue(operation.getOperationUid());
				
				String operationName = null;
				if (surveyStation.getOperationUid()!=null){
					Operation op = (Operation) daoManager.getObject(Operation.class, surveyStation.getOperationUid());
					if (op!=null && StringUtils.isNotBlank(op.getOperationName())){
						operationName = op.getOperationName();				
					}
				}
				csv.addValue(operationName);
				
				csv.addValue(convertFromBase(surveyStation.getDepthMdMsl(), LENGTH_METER, DOUBLE_PRECISION));
				csv.addValue(convertFromBase(surveyStation.getInclinationAngle(), ANGLE_DEGREE, DOUBLE_PRECISION));
				csv.addValue(convertFromBase(surveyStation.getAzimuthAngle(), ANGLE_DEGREE, DOUBLE_PRECISION));
				csv.addValue(convertFromBase(surveyStation.getDepthTvdMsl(), LENGTH_METER, DOUBLE_PRECISION));
				csv.addValue(convertFromBase(surveyStation.getNsOffset(), LENGTH_METER, DOUBLE_PRECISION));
				csv.addValue(convertFromBase(surveyStation.getEwOffset(), LENGTH_METER, DOUBLE_PRECISION));
				csv.addValue(null); //UPDATE_DATE
			}
		}
	}

	private static CSV collectRigDetails(List<Operation> operations, String uomTemplateUid, Manager daoManager) throws Exception {
		CSV csv = new CSV("WellGUID","Name","Type","WellName","UPDATE_DATE");
		for(Operation operation: operations) {
			collectRigDetails(operation, csv, uomTemplateUid, daoManager);
		}
		return csv;
	}
	
	@SuppressWarnings("unchecked")
	private static void collectRigDetails(Operation operation, CSV csv, String uomTemplateUid, Manager daoManager) throws Exception {
		List<RigInformation> rs = daoManager.findByNamedParam("from RigInformation where (isDeleted = false or isDeleted is null) and rigInformationUid = :rigInformationUid", "rigInformationUid", operation.getRigInformationUid());
		if(!rs.isEmpty()) {
			RigInformation rigInformation = rs.get(0);
			csv.addLine();
			csv.addValue(operation.getOperationUid());
			csv.addValue(rigInformation.getRigName());
			csv.addValue(getRigSubType(rigInformation, daoManager));
			csv.addValue(operation.getOperationName());
			csv.addValue(null);
		}
	}
	
	@SuppressWarnings("unchecked")
	private static String getRigSubType(RigInformation rigInformation, Manager daoManager) {
		List<String> rs = daoManager.findByNamedParam("select lookupLabel from CommonLookup where (isDeleted = false or isDeleted is null) and (isActive = true or isActive is null) and lookupTypeSelection = :lookupTypeSelection and shortCode = :shortCode", new String[] {"lookupTypeSelection","shortCode"}, new String[] {"rigInformation.rigSubType", rigInformation.getRigSubType()});
		if(!rs.isEmpty()) {
			return rs.get(0);
		}else {
			return null;
		}
	}

	private void collectCasingDetails(List<Operation> operations, CSV casingCSV, CSV sectionCSV, String uomTemplateUid, Manager daoManager) throws Exception {
		for(Operation operation: operations) {
			collectCasingDetails(operation, casingCSV, sectionCSV, uomTemplateUid, daoManager);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void collectCasingDetails(Operation operation, CSV casingCSV, CSV sectionCSV, String uomTemplateUid, Manager daoManager) throws Exception {
		List<CasingSection> casingSections = daoManager.findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid order by installStartDate, topDepthMdMsl", "wellboreUid", operation.getWellboreUid(), getQueryProperties(uomTemplateUid, operation));
		CasingSection previousCasing = null;
		for(CasingSection casing: casingSections) {
			casingCSV.addLine();
			casingCSV.addValue(operation.getOperationUid());
			
			String wellName = null;
			if (casing.getWellUid()!=null){
				Well well = (Well) daoManager.getObject(Well.class, casing.getWellUid());
				if (well!=null && StringUtils.isNotBlank(well.getWellName())) {
					wellName = well.getWellName();
				}
			}
			casingCSV.addValue(wellName);
			
			casingCSV.addValue(getSectionName(casing));
			casingCSV.addValue(getCasingTopDepthMd(casing));
			casingCSV.addValue(convertFromBase(casing.getShoeTopMdMsl(), LENGTH_METER, DOUBLE_PRECISION));
			casingCSV.addValue(convertFromBase(casing.getCasingOd(), LENGTH_INCH, DOUBLE_PRECISION));
			casingCSV.addValue(null); //UPDATE_DATE

			Double previousDepth = Double.valueOf(0);
			if(previousCasing != null) {
				previousDepth = previousCasing.getOpenHoleDepthMdMsl();
			}

			sectionCSV.addLine();
			sectionCSV.addValue(operation.getOperationUid());
			
			String wellboreName = null;
			if(casing.getWellboreUid()!=null){
				Wellbore wellbore = (Wellbore) daoManager.getObject(Wellbore.class, casing.getWellboreUid());
				if (wellbore!=null && StringUtils.isNotBlank(wellbore.getWellboreName())){
					wellboreName = wellbore.getWellboreName();
				}
			}
			sectionCSV.addValue(wellboreName);
			
			sectionCSV.addValue(convertFromBase(casing.getOpenHoleId(), LENGTH_INCH, DOUBLE_PRECISION));
			sectionCSV.addValue(convertFromBase(previousDepth, LENGTH_METER, DOUBLE_PRECISION));
			sectionCSV.addValue(convertFromBase(casing.getOpenHoleDepthMdMsl(), LENGTH_METER, DOUBLE_PRECISION));
			sectionCSV.addValue(null); //UPDATE_DATE
			
			previousCasing = casing;
		}
	}

	private CSV collectOperationDetails(List<Operation> operations, Manager daoManager) throws UOMConverterException {
		CSV csv = new CSV("WellGUID","Name","UWI","SpudDate","Field","Area","Country","State","County","OffshoreFlag","GroundElevation","KBElevation","Longitude","Latitude","OriginalCRS","DataSource","UPDATE_DATE");
		for(Operation operation: operations) {
			collectOperationDetails(operation, csv, daoManager);
		}
		return csv;
	}
	
	private void collectOperationDetails(Operation operation, CSV csv, Manager daoManager) throws UOMConverterException {
		Well well = (Well) daoManager.getObject(Well.class, operation.getWellUid());
		csv.addLine();
		csv.addValue(operation.getOperationUid());
		csv.addValue(operation.getOperationName());
		csv.addValue(well.getUniqueWellId());
		csv.addValue(format(operation.getSpudDate(), "MM/dd/yyyy"));
		csv.addValue(well.getField());
		csv.addValue(null); //Area
		csv.addValue(getCommonLookup(daoManager, "country", well.getCountry(), null));
		csv.addValue(getCommonLookup(daoManager, "subdivision", well.getState(), well.getCountry()));
		csv.addValue(null); //County
		csv.addValue("ON".equalsIgnoreCase(well.getOnOffShore()) ? "0" : "1");
		csv.addValue("ON".equalsIgnoreCase(well.getOnOffShore()) ? convertFromBase(well.getGlHeightMsl(), LENGTH_METER, DOUBLE_PRECISION): null);

		if (operation.getDefaultDatumUid()!=null){
			OpsDatum opsDatum = (OpsDatum) daoManager.getObject(OpsDatum.class, operation.getDefaultDatumUid());
			if (opsDatum!=null && "RKB".equals(opsDatum.getDatumCode())){
				Double reportingDatumOffset = opsDatum.getReportingDatumOffset();
				Double offsetMsl = opsDatum.getOffsetMsl();
				csv.addValue(convertFromBase(reportingDatumOffset + offsetMsl, LENGTH_METER, DOUBLE_PRECISION));
			}else{
				csv.addValue(null);
			}
		}else{
			csv.addValue(null);
		}
		
		csv.addValue(convertDMS2DecimalDegree(convertFromBase(well.getLongDeg(),ANGLE_DEGREE), well.getLongMinute(), well.getLongSecond(), "W".equalsIgnoreCase(well.getLongEw())));
		csv.addValue(convertDMS2DecimalDegree(convertFromBase(well.getLatDeg(),ANGLE_DEGREE), well.getLatMinute(), well.getLatSecond(), "S".equalsIgnoreCase(well.getLatNs())));
		csv.addValue(null); //OriginalCRS
		csv.addValue(null); //DataSource
		csv.addValue(null); //UPDATE_DATE
	}

	@SuppressWarnings("unchecked")
	private static String getCommonLookup(Manager daoManager, String lookupType, String key, String parentKey) {
		if(StringUtils.isBlank(key)) {
			return key;
		}
		List<String> params = new ArrayList<String>();
		List<Object> values = new ArrayList<Object>();
		params.add("lookupType"); values.add(lookupType);
		params.add("shortCode"); values.add(key);
		if(parentKey != null) {
			params.add("parentKey"); values.add(parentKey);
		}
		List<String> rs = daoManager.findByNamedParam("select lookupLabel from CommonLookup where (isDeleted = false or isDeleted is null) and lookupTypeSelection = :lookupType and shortCode = :shortCode" + (parentKey != null ? " and parentReferenceKey = :parentKey" : ""), params, values);
		if(!rs.isEmpty()) {
			return rs.get(0);
		}else {
			return key;
		}
	}
	
	private QueryProperties getQueryProperties(String uomTemplateUid, Operation operation) throws Exception {
		QueryProperties qp = new QueryProperties();
		if(this.useCustomUomConversion) {
			qp.setUomConversionEnabled(false);
		}else {
			qp.setUomTemplateUid(uomTemplateUid);
		}
		qp.setUomDatumUid(getOperationDefaultDatum(operation));
		return qp;
	}
	
	private static String getOperationDefaultDatum(Operation operation) throws Exception {
		if(operation == null) return null;
		if(StringUtils.isNotBlank(operation.getDefaultDatumUid())){
			if(DatumManager.isValidDatumId(operation.getOperationUid(), operation.getDefaultDatumUid())){
				return operation.getDefaultDatumUid();
			}
		}
		return null;
	}

	private Double convertFromBase(Double value, String key) throws UOMConverterException {
		if(value == null) return null;
		return this.useCustomUomConversion ? UOMConverter.convertFromBase(value, key) : value;
	}
	
	private String convertFromBase(Double value, String key, String format) throws UOMConverterException {
		if(value == null) {
			return null;
		}
		return format(convertFromBase(value, key), format);
	}
	
	private static String convertDMS2DecimalDegree(Double degree, Double minute, Double second, boolean negative) {
		if(degree == null || minute == null || second == null) {
			return "";
		}
		return format((degree + (minute/60.0) + (second/3600.0)) * (negative ? -1 : 1), "#.######");
	}

	private static String format(Date date, String format) {
		if(date == null) return null;
		SimpleDateFormat df = new SimpleDateFormat(format);
		return df.format(date);
	}

	private static String format(Double value, String format) {
		if(value == null) return null;
		DecimalFormat df = new DecimalFormat(format);
		return df.format(MathUtils.round(value, df.getMaximumFractionDigits()));
	}
	
	private static boolean eq(String v1, String v2) {
		if(v1 == null && v2 == null) return true;
		if(v1 != null && v2 != null) return v1.equalsIgnoreCase(v2);
		return false;
	}

	private String getCasingTopDepthMd(CasingSection casing) throws UOMConverterException {
		if(eq(casing.getSectionName(), "liner")) {
			return convertFromBase(casing.getLinerTopDepthMdMsl(), LENGTH_METER, DOUBLE_PRECISION);
		}else {
			return format(Double.valueOf(0), DOUBLE_PRECISION);
		}
	}
	
	private static String getSectionName(CasingSection casingSection) {
		String name = casingSection.getSectionName();
		if(eq(name, "surface") || eq(name, "complete") || eq(name, "interm") || eq(name, "product")) {
			return "Casing";
		}else if(eq(name, "conductor")) {
			return "Conductor"; 
		}else if(eq(name, "liner")) {
			return "Liner";
		}else {
			return name;
		}
	}
}
