package com.idsdatanet.d2.drillnet.tightholesetup;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * 
 * @author mhchai
 */
public class TightHoleUserListLookupHandler implements LookupHandler {
	
	private String orderBy;
	
	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String sql = "select userUid, userName, fname from User where (isDeleted = false or isDeleted is null) and (isblocked = false OR isblocked IS NULL) AND userName NOT IN ('idsadmin') and (idsUser = false or idsUser is null) ORDER BY :order";
		List<Object[]> listUserResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"order"}, new String[]{orderBy});
		if (listUserResults.size() > 0) {
			for (Object[] listUserResult : listUserResults) {
				if (listUserResult[0] != null && listUserResult[1] != null) {
					String fname="";
					if (listUserResult[2] != null && listUserResult[2].toString().length() > 0) {
						fname = listUserResult[2].toString();
					} else {
						fname = listUserResult[1].toString();
					}
					
					LookupItem lookupItem = new LookupItem(listUserResult[0].toString(), fname);
					result.put(listUserResult[0].toString(), lookupItem);
				}
			}
		}
		return result;
	}

}

