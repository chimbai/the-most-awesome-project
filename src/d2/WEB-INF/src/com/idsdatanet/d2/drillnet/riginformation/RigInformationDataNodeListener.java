package com.idsdatanet.d2.drillnet.riginformation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.RigCompressors;
import com.idsdatanet.d2.core.model.RigContract;
import com.idsdatanet.d2.core.model.RigEngine;
import com.idsdatanet.d2.core.model.RigGenerators;
import com.idsdatanet.d2.core.model.RigHydrTongs;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RigMobileWinch;
import com.idsdatanet.d2.core.model.RigPowerUnitHydrTongs;
import com.idsdatanet.d2.core.model.RigPowerUnitRotaryTable;
import com.idsdatanet.d2.core.model.RigPreventorControlUnit;
import com.idsdatanet.d2.core.model.RigPump;
import com.idsdatanet.d2.core.model.RigRotaryTable;
import com.idsdatanet.d2.core.model.RigTour;
import com.idsdatanet.d2.core.model.TourWireline;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.drillLine.DrillLineUtils;
import com.idsdatanet.d2.drillnet.marineProperties.MarinePropertiesUtil;
import com.idsdatanet.d2.drillnet.rigUtilization.RigUtilizationUtils;
import com.idsdatanet.d2.tournet.wireline.TourWirelineUtils;

public class RigInformationDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
				
		String customCondition 	= "";
		String currentClass	= meta.getTableClass().getSimpleName();
		String selectedStatus  = "";
		selectedStatus = (String) commandBean.getRoot().getDynaAttr().get("filterStatus");
			
		if(currentClass.equals("RigInformation")) {			
			if(StringUtils.isNotBlank(selectedStatus)){
				if("1".equals(selectedStatus)) customCondition = "status = true";
				if("0".equals(selectedStatus)) customCondition = "status = false";
			}
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		
		return null;
	}
		
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		String beanName = commandBean.getBeanName();
		if (StringUtils.equals(beanName, "alt5_rigInformationCommandBean")) {
			Object obj = node.getData();
			if (obj instanceof RigInformation) {
				RigInformation thisRigInformation = (RigInformation) node.getData();
				thisRigInformation.setRigSubType("workover");
			}
		}
	
	}
	
	public void beforeDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception{
		Object obj = node.getData();
		if (obj instanceof RigTour)
		{
			if (RigInformationUtils.isRigTourUsed((RigTour)obj) && RigInformationUtils.isLastRigTour((RigTour)obj))
			{
				status.addWarning("The selected Rig Tour to be deleted are used by an Operation and are the last tour, so Deletion isn't allowed");
				status.setContinueProcess(false);
			}
		}
		if (obj instanceof RigInformation)
		{
			if (RigInformationUtils.isRigUsed((RigInformation)obj) )
			{
				status.addWarning("There is existing Operation related to selected Rig. The Rig cannot be deleted");
				status.setContinueProcess(false);
			}
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		
		if (node.getData() instanceof RigInformation) {
			RigInformation thisRigInformation = (RigInformation) node.getData();
			
			if(thisRigInformation.getIsoffshore()!=null && thisRigInformation.getIsoffshore()){
				// do nothing
			}else{
				thisRigInformation.setNumsupport(null);
				thisRigInformation.setNumsupportSecondary(null);
				thisRigInformation.setDeckloadmax(null);
				thisRigInformation.setNumlifeboats(null);
				thisRigInformation.setNumhelipad(null);
			}
		}
		
		if (node.getData() instanceof RigContract) {
			RigContract rigContract = (RigContract) node.getData();
			double duration = 0.0;
			if (rigContract.getContractStartDatetime()!=null && rigContract.getContractEndDatetime()!=null) {
				duration = RigUtilizationUtils.calculateDaysOnContract(rigContract.getContractStartDatetime(), rigContract.getContractEndDatetime());
			}
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RigContract.class, "contract_duration");
			thisConverter.setBaseValue(duration);
			rigContract.setContractDuration(thisConverter.getConvertedValue());
		}
		if (node.getData() instanceof RigPump){
			RigPump rigPump = (RigPump)node.getData();
			
			if(rigPump.getLookupRigEquipmentListUid()!=null) {
				String equipmentLookupUid = rigPump.getLookupRigEquipmentListUid();
				String companyLookupUid = rigPump.getManufacturerUid();
				List <String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select equipmentNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentLookupUid", "equipmentLookupUid", equipmentLookupUid);
				if (lstResult.size() > 0) {
					String equipmentNumber = lstResult.get(0);
    				if (equipmentNumber != null){
    					rigPump.setUnitNumber(equipmentNumber);
    				} else {
    					rigPump.setUnitNumber(null);
    				}
				}
				
				List <String> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select companyName from LookupCompany where (isDeleted = false or isDeleted is null) and lookupCompanyUid = :companyLookupUid", "companyLookupUid", companyLookupUid);
				if (lstResult2.size() > 0) {
					String companyName = lstResult2.get(0);
    				if (companyName != null){
    					rigPump.setManufacturer(companyName);
    				} else {
    					rigPump.setManufacturer(null);
    				}
				}

			}
			
			if(rigPump.getInstallDate()!= null && rigPump.getRemoveDate()!= null){
				if(rigPump.getInstallDate().getTime() > rigPump.getRemoveDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "installDate", "Install date cannot be greater than Remove date.");
					return;
				}
			}
		}
		if (node.getData() instanceof RigEngine){
			RigEngine rigEngine = (RigEngine)node.getData();
			
			if(rigEngine.getInstalledDate()!= null && rigEngine.getRemovedDate()!= null){
				if(rigEngine.getInstalledDate().getTime() > rigEngine.getRemovedDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "installedDate", "Install date cannot be greater than Remove date.");
					return;
				}
			}
		}
		if (node.getData() instanceof RigMobileWinch){
			RigMobileWinch rigMobileWinch = (RigMobileWinch)node.getData();
			
			if(rigMobileWinch.getInstalledDate()!= null && rigMobileWinch.getRemovedDate()!= null){
				if(rigMobileWinch.getInstalledDate().getTime() > rigMobileWinch.getRemovedDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "installedDate", "Install date cannot be greater than Remove date.");
					return;
				}
			}
		}
		if (node.getData() instanceof RigPreventorControlUnit){
			RigPreventorControlUnit rigPreventorControlUnit = (RigPreventorControlUnit)node.getData();
			
			if(rigPreventorControlUnit.getInstalledDate()!= null && rigPreventorControlUnit.getRemovedDate()!= null){
				if(rigPreventorControlUnit.getInstalledDate().getTime() > rigPreventorControlUnit.getRemovedDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "installedDate", "Install date cannot be greater than Remove date.");
					return;
				}
			}
		}
		if (node.getData() instanceof RigPowerUnitHydrTongs){
			RigPowerUnitHydrTongs rigPowerUnitHydrTongs = (RigPowerUnitHydrTongs)node.getData();
			
			if(rigPowerUnitHydrTongs.getInstalledDate()!= null && rigPowerUnitHydrTongs.getRemovedDate()!= null){
				if(rigPowerUnitHydrTongs.getInstalledDate().getTime() > rigPowerUnitHydrTongs.getRemovedDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "installedDate", "Install date cannot be greater than Remove date.");
					return;
				}
			}
		}
		if (node.getData() instanceof RigHydrTongs){
			RigHydrTongs rigHydrTongs = (RigHydrTongs)node.getData();
			
			if(rigHydrTongs.getInstalledDate()!= null && rigHydrTongs.getRemovedDate()!= null){
				if(rigHydrTongs.getInstalledDate().getTime() > rigHydrTongs.getRemovedDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "installedDate", "Install date cannot be greater than Remove date.");
					return;
				}
			}
		}
		if (node.getData() instanceof RigPowerUnitRotaryTable){
			RigPowerUnitRotaryTable rigPowerUnitRotaryTable = (RigPowerUnitRotaryTable)node.getData();
			
			if(rigPowerUnitRotaryTable.getInstalledDate()!= null && rigPowerUnitRotaryTable.getRemovedDate()!= null){
				if(rigPowerUnitRotaryTable.getInstalledDate().getTime() > rigPowerUnitRotaryTable.getRemovedDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "installedDate", "Install date cannot be greater than Remove date.");
					return;
				}
			}
		}
		if (node.getData() instanceof RigRotaryTable){
			RigRotaryTable rigRotaryTable = (RigRotaryTable)node.getData();
			
			if(rigRotaryTable.getInstalledDate()!= null && rigRotaryTable.getRemovedDate()!= null){
				if(rigRotaryTable.getInstalledDate().getTime() > rigRotaryTable.getRemovedDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "installedDate", "Install date cannot be greater than Remove date.");
					return;
				}
			}
		}
		if (node.getData() instanceof RigGenerators){
			RigGenerators rigGenerators = (RigGenerators)node.getData();
			
			if(rigGenerators.getInstalledDate()!= null && rigGenerators.getRemovedDate()!= null){
				if(rigGenerators.getInstalledDate().getTime() > rigGenerators.getRemovedDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "installedDate", "Install date cannot be greater than Remove date.");
					return;
				}
			}
			
			if(rigGenerators.getLookupRigEquipmentListUid()!=null) {
				String equipmentLookupUid = rigGenerators.getLookupRigEquipmentListUid();
				List <String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select equipmentNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentLookupUid", "equipmentLookupUid", equipmentLookupUid);
				if (lstResult.size() > 0) {
					String equipmentNumber = lstResult.get(0);
    				if (equipmentNumber != null){
    					rigGenerators.setGeneratorNumber(equipmentNumber);
    				} else {
    					rigGenerators.setGeneratorNumber(null);
    				}
				}
			}
		}
		
		if (node.getData() instanceof RigCompressors){
			RigCompressors rigCompressors = (RigCompressors)node.getData();
			
			if(rigCompressors.getLookupRigEquipmentListUid()!=null) {
				String equipmentLookupUid = rigCompressors.getLookupRigEquipmentListUid();
				List <String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select equipmentNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentLookupUid", "equipmentLookupUid", equipmentLookupUid);
				if (lstResult.size() > 0) {
					String equipmentNumber = lstResult.get(0);
    				if (equipmentNumber != null){
    					rigCompressors.setCompressorNumber(equipmentNumber);
    				} else {
    					rigCompressors.setCompressorNumber(null);
    				}
				}
			}
		}
		
		if (node.getData() instanceof RigTour) {
			RigTour rigTour = (RigTour) node.getData();
			//ticket no: 23668
//			if(node.getInfo().isNewlyCreated()){
//				RigTourMaster master = (RigTourMaster) node.getParent().getData();
//				rigTour.setRigInformationUid(master.getRigInformationUid());
//			}
			Long tourHour = RigInformationUtils.GetRigTourHour(rigTour.getTourStartDateTime(), rigTour.getTourEndDateTime());
			if(tourHour != null) {
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RigTour.class, "tourHrs");
				thisConverter.setBaseValue(tourHour);
				rigTour.setTourHrs(thisConverter.getConvertedValue());
			}
		}
		
		if(node.getData() instanceof TourWireline){
			RigInformation thisRigInformation = (RigInformation) node.getParent().getData();
			String rigUid = thisRigInformation.getRigInformationUid();
			if(node.getInfo().isNewlyCreated() && !TourWirelineUtils.isAllowAddWireline(rigUid)){
				status.addError("New wireline reel cannot be saved until previous wireline reel uninstalled date has been input.");
				status.setContinueProcess(false);
			}
		}
	}

	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		if (node.getData() instanceof RigInformation) {
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_VARIABLE_DECK_MARGIN))){
				//calculate Marine Properties' Variable Deck Load Margin = Max Variable Deck Load Current Variable Deck Load
				MarinePropertiesUtil.calculateAllVariableDeckLoadMarginFromRigInformationSaveOrUpdate(commandBean, session, (RigInformation)node.getData());
				this.refreshCache(node); //refresh cache data
			}
		} else if (node.getData() instanceof TourWireline) {
			TourWireline tourWireline = (TourWireline) node.getData();
			DrillLineUtils.calculateSlipAndCut(tourWireline, new CustomFieldUom(commandBean));
		}
	}

	
	public void afterDataNodeDelete(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		if (node.getData() instanceof RigInformation) {
			this.refreshCache(node); //refresh cache data
		}else if (node.getData() instanceof TourWireline) {
			TourWireline tourWireline = (TourWireline) node.getData();
			DrillLineUtils.calculateSlipAndCut(tourWireline, new CustomFieldUom(commandBean));
		}
	}
	
	private void refreshCache(CommandBeanTreeNode node) throws Exception {
		if (node.getData() instanceof RigInformation) {
			RigInformation rig = (RigInformation) node.getData();
			List<String> changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(rig.getRigInformationUid());
			D2ApplicationEvent.publishTableChangedEvent(RigInformation.class, changedRowPrimaryKeys);
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
		}
	}

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		//Ticket: 43607 - Optimize Rig Information Load Time
		/*if (obj instanceof RigTour)
		{
			if (RigInformationUtils.isRigTourUsed((RigTour)obj) )
			{
				node.getDynaAttr().put("isRigTourUsed", "1");
			}
		}
		if (obj instanceof RigInformation)
		{
			if (RigInformationUtils.isRigUsed((RigInformation)obj) )
			{
				node.getDynaAttr().put("isRigUsed", "1");

			}
		}else */
		if (obj instanceof TourWireline) {
			//NOBLE - 20120524-0829-scwong - TourWireline By Date
			if (node.getParent()!=null && node.getParent().getData() instanceof RigInformation) {
				RigInformation rig = (RigInformation) node.getParent().getData();
				node.getDynaAttr().put("lineQuantity", rig.getLineQuantity());
				CustomFieldUom converter = new CustomFieldUom(commandBean, RigInformation.class, "lineQuantity");
				if (converter.isUOMMappingAvailable()) {
					node.setCustomUOM("@lineQuantity", converter.getUOMMapping());
				}
			}
			
		}
	}

	@Override
	public void afterChildClassNodesProcessed(String className,
			ChildClassNodesProcessStatus status, UserSession userSession,
			CommandBeanTreeNode parent) throws Exception {
		
		if("1".equals(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), GroupWidePreference.GWP_DAYS_SINCE_LAST_LTI_BASED_ON_CONTRACT))){
			//update all Days Since LTI in report daily field
			if ("RigContract".equals(className) && status.isAnyNodeSavedOrDeleted()) {
				if (parent.getData() instanceof RigInformation) {
					RigInformation rig = (RigInformation) parent.getData();
					RigUtilizationUtils.updateAllReportDailyDaysSinceLastLti(userSession.getCurrentGroupUid(), rig.getRigInformationUid(), null, null);
				}
			}
		}		
	}
}
