package com.idsdatanet.d2.drillnet.personnelonsite;

import java.util.Date;

import com.idsdatanet.d2.core.model.PersonnelOnSite;

public class ImportPOB extends PersonnelOnSite implements java.io.Serializable {
	private Date dayDate;
	
	public ImportPOB() {
	}
	
	public Date getDayDate() {
		return this.dayDate;
	}

	public void setDayDate(Date dayDate) {
		this.dayDate = dayDate;
	}
}
