package com.idsdatanet.d2.drillnet.wellHistoryOverview;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.WellHeadSection;
import com.idsdatanet.d2.core.model.WellHeadDetail;
import com.idsdatanet.d2.core.model.WellHeadDetailLog;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class WellHeadHistoryOverviewDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
				
		Map<String,LookupItem> statusLookup= LookupManager.getConfiguredInstance().getLookup("xml://WellHeadDetailLog.status?key=code&amp;value=label", userContext.getUserSelection(), null);
		
		/*String sql = "select a.wellHeadUid,a.wellUid,a.wellboreUid,a.operationUid,a.referenceNo,b.operationName from WellHead a,Operation b where a.operationUid = b.operationUid and (a.isDeleted is null or a.isDeleted = false) " +
					 "and (b.isDeleted is null or b.isDeleted = false) and a.wellboreUid =:wellboreUid order by a.installDateTime desc,b.sysOperationStartDatetime desc,a.operationUid,a.referenceNo";
		*/
		
		String sql = "select a.wellHeadUid,a.wellUid,a.wellboreUid,a.operationUid,a.referenceNo,b.operationName from WellHead a,Operation b where a.operationUid = b.operationUid and (a.isDeleted is null or a.isDeleted = false) " +
		 "and (b.isDeleted is null or b.isDeleted = false) and a.wellboreUid =:wellboreUid order by a.installDateTime desc,a.operationUid,a.referenceNo";
		
		List<Object[]> lstData = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql ,"wellboreUid", userContext.getUserSelection().getWellboreUid());
		
		if (lstData.size()>0){
			ReportDataNode WellHeadNode = reportDataNode.addChild("WellHead");
			ReportDataNode WellHeadSectionNode = reportDataNode.addChild("WellHeadSection");
			ReportDataNode WellHeadDetailNode = reportDataNode.addChild("WellHeadDetail");
			ReportDataNode currentDataNode = null;
			
			CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
						
			for(Object[] item : lstData){
				
				String wellHeadUid = item[0].toString();
				
				currentDataNode = WellHeadNode.addChild("ReportInfo");
				currentDataNode.addProperty("wellHeadUid", wellHeadUid);
				currentDataNode.addProperty("wellUid", item[1].toString());
				currentDataNode.addProperty("wellboreUid", item[2].toString());
				currentDataNode.addProperty("operationUid", item[3].toString());
				currentDataNode.addProperty("referenceNo", item[4].toString());
				currentDataNode.addProperty("operationName", item[5].toString());
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM WellHeadSection WHERE wellHeadUid=:wellHeadUid order by sectionCode", "wellHeadUid", wellHeadUid);
								
				for(Object data : lstResult){
									
					WellHeadSection wellHeadSection = (WellHeadSection) data;
					
					String wellHeadSectionUid  = wellHeadSection.getWellHeadSectionUid();
					currentDataNode = WellHeadSectionNode.addChild("ReportInfo");
					currentDataNode.addProperty("sectionDesc", wellHeadSection.getDescription());
					currentDataNode.addProperty("sectionCode", wellHeadSection.getSectionCode() + " - " + wellHeadSection.getDescription());
					currentDataNode.addProperty("wellHeadSectionUid", wellHeadSectionUid);
					currentDataNode.addProperty("wellHeadUid", wellHeadSection.getWellHeadUid());
				
					if (StringUtils.isNotBlank(wellHeadSectionUid)) {
												
						List lstWellHeadDetail = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM WellHeadDetail WHERE wellHeadSectionUid=:wellHeadSectionUid order by sequence", "wellHeadSectionUid", wellHeadSectionUid);
						
						Integer autoSeqNumber = 0;
						
						for(Object WellHeadDetaildata : lstWellHeadDetail){
							WellHeadDetail wellHeadDetail = (WellHeadDetail) WellHeadDetaildata;
							
							currentDataNode = WellHeadDetailNode.addChild("ReportInfo");
							String wellHeadDetailUid =wellHeadDetail.getWellHeadDetailUid();
							
							autoSeqNumber = autoSeqNumber + 1;
							
							currentDataNode.addProperty("wellHeadSectionUid", wellHeadDetail.getWellHeadSectionUid() );
							currentDataNode.addProperty("wellHeadDetailUid", wellHeadDetailUid);
							currentDataNode.addProperty("autoSeqNumber", autoSeqNumber.toString());
							currentDataNode.addProperty("type", wellHeadDetail.getDetailType());
							currentDataNode.addProperty("manufacturer", wellHeadDetail.getManufacturer());
							currentDataNode.addProperty("serialNo", wellHeadDetail.getSerialNo());
							currentDataNode.addProperty("partNo", wellHeadDetail.getPartNo());
							currentDataNode.addProperty("comment", wellHeadDetail.getComment());								
							currentDataNode.addProperty("status", getLookupValue(statusLookup,wellHeadDetail.getStatus()));	
							
							if (wellHeadDetail.getCheckDateTime()!=null)
							{
								currentDataNode.addProperty("checkDateTime", sdf.format(wellHeadDetail.getCheckDateTime()));
							}
							generateUomField(currentDataNode,thisConverter,"topPressureRating", wellHeadDetail.getTopPressureRating());
							generateUomField(currentDataNode,thisConverter,"size", wellHeadDetail.getSize());
							
							List lstWellHeadDetailLogDetail = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM WellHeadDetailLog WHERE wellHeadDetailUid=:wellHeadDetailUid order by serviceDateTime DESC", "wellHeadDetailUid", wellHeadDetailUid);
													
							for(Object WellHeadDetaiLogldata : lstWellHeadDetailLogDetail){
								WellHeadDetailLog wellHeadDetailLog = (WellHeadDetailLog) WellHeadDetaiLogldata;
								if (wellHeadDetailLog.getServiceDateTime()!=null)
								{
									currentDataNode.addProperty("checkDateTime", sdf.format(wellHeadDetailLog.getServiceDateTime()));
								}
								currentDataNode.addProperty("serviceRemark", wellHeadDetailLog.getServiceRemark());	
								currentDataNode.addProperty("description", wellHeadDetailLog.getDescription());	
								currentDataNode.addProperty("wellHeadDetailLogUid", wellHeadDetailLog.getWellHeadDetailLogUid());	
								break;
							}
						}						
					
					}
																		
				}
				break;
			}
		}
	}

	private void generateUomField(ReportDataNode node,CustomFieldUom thisConverter,String DataField, Object Data ) throws Exception{
		
		String rawValue = "";
		String dataUomSymbol = "";
		String formattedvalue = "";
		
		thisConverter.setReferenceMappingField(WellHeadDetail.class, DataField);
		if (thisConverter.isUOMMappingAvailable()) dataUomSymbol = thisConverter.getUomSymbol().toString();
		
		if (Data != null)
		{
			if (thisConverter.isUOMMappingAvailable()){
				thisConverter.setBaseValueFromUserValue(Double.parseDouble(Data.toString()));
				formattedvalue = thisConverter.getFormattedValue();	
				rawValue = formattedvalue.replace(dataUomSymbol, "").trim() ;
			}
			else{
				rawValue = Data.toString();
			}			
		}				
		
		node.addProperty(DataField, rawValue);
		node.addProperty(DataField + "UomSymbol", dataUomSymbol);		
		if (StringUtils.isNotBlank(formattedvalue))
			node.addProperty(DataField + "WithUomSymbol", formattedvalue);
		else
			node.addProperty(DataField + "WithUomSymbol", rawValue);
	}
		
		private String getLookupValue(Map<String,LookupItem> lookupList, Object lookupvalue) throws Exception {
			String retValue = "";
			
			if (lookupvalue !=null && StringUtils.isNotBlank(lookupvalue.toString())  && lookupList !=null){
				 LookupItem lookup = lookupList.get(lookupvalue.toString());
				if (lookup !=null)	retValue = lookup.getValue().toString();					
			} 
			
			return retValue;
		}
}
