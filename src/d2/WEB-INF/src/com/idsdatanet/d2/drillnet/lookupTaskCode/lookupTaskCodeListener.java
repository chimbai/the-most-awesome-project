package com.idsdatanet.d2.drillnet.lookupTaskCode;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class lookupTaskCodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor{

	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		String selectedOperationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
		String selectedPhaseCode = (String) commandBean.getRoot().getDynaAttr().get("phaseCodeFilter");
		String selectedOperationalUnit = (String) commandBean.getRoot().getDynaAttr().get("operationalUnitFilter");
		String selectedCommonFilter = (String) commandBean.getRoot().getDynaAttr().get("commonFilter");
		String selectedIsActiveFilter = (String) commandBean.getRoot().getDynaAttr().get("isActiveFilter");
		
		Boolean isActiveFilterFilterBoolean = null;
		if (StringUtils.isNotBlank(selectedIsActiveFilter)) {
			if (StringUtils.equals("1", selectedIsActiveFilter)) {
				isActiveFilterFilterBoolean = true;
			} else if (StringUtils.equals("0", selectedIsActiveFilter)) {
				isActiveFilterFilterBoolean = false;
			} 
		}
		
		Object object = node.getData();
		if (object instanceof LookupTaskCode) {
			LookupTaskCode data = (LookupTaskCode) object;
			data.setOperationCode(selectedOperationCode);
			data.setPhaseShortCode(selectedPhaseCode);
			data.setOperationalUnit(selectedOperationalUnit);
			data.setCommonFilter(selectedCommonFilter);
			data.setIsActive(isActiveFilterFilterBoolean);
		}
	}

	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof LookupTaskCode) {
			LookupTaskCode lookupTaskCode = (LookupTaskCode) object;
			lookupTaskCode.setSysPaddedCode(WellNameUtil.getPaddedStr(lookupTaskCode.getShortCode()));
		}
		
	}
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {

		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		
		if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
			String selectedOperationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
			String selectedInternalCode = (String) commandBean.getRoot().getDynaAttr().get("internalCodeFilter");
			String selectedPhaseCode = (String) commandBean.getRoot().getDynaAttr().get("phaseCodeFilter");
			String selectedOperationalUnit = (String) commandBean.getRoot().getDynaAttr().get("operationalUnitFilter");
			String selectedCommonFilter = (String) commandBean.getRoot().getDynaAttr().get("commonFilter");
			String selectedIsActiveFilter = (String) commandBean.getRoot().getDynaAttr().get("isActiveFilter");
			String phaseCodeFilter = (String) commandBean.getRoot().getDynaAttr().get("filterPhaseCode");
			String jobTypeCodeFilter = (String) commandBean.getRoot().getDynaAttr().get("filterJobCode");
			
			if (StringUtils.isNotBlank(selectedOperationCode)){
				customCondition+=(customCondition.length()>0?" AND ":"")+" operationCode =:operationCode ";
				query.addParam("operationCode", selectedOperationCode);
			}
			else
			{
				customCondition = " 1 = 1";
			}
			if (StringUtils.isNotBlank(selectedInternalCode)){
				customCondition+=(customCondition.length()>0?" AND ":"")+" internalCode =:internalCode ";
				query.addParam("internalCode", selectedInternalCode);
			}
			if (StringUtils.isNotBlank(selectedOperationalUnit)){
				customCondition+=(customCondition.length()>0?" AND ":"")+" operationalUnit =:operationalUnit ";
				query.addParam("operationalUnit", selectedOperationalUnit);
			}
			if(StringUtils.isNotBlank(selectedCommonFilter)){
				customCondition+=(customCondition.length()>0?" AND ":"")+" commonFilter =:commonFilter ";
				query.addParam("commonFilter", selectedCommonFilter);
			}
			if(StringUtils.isNotBlank(selectedPhaseCode)){
				customCondition+=(customCondition.length()>0?" AND ":"")+" phaseShortCode =:phaseShortCode ";
				query.addParam("phaseShortCode", selectedPhaseCode);
			}
			if (StringUtils.isNotBlank(selectedIsActiveFilter)) {
				if (StringUtils.equals("1", selectedIsActiveFilter)) {
					customCondition+=(customCondition.length()>0?" AND ":"")+" (isActive IS TRUE OR isActive IS NULL) ";
				} else if (StringUtils.equals("0", selectedIsActiveFilter)) {
					customCondition+=(customCondition.length()>0?" AND ":"")+" isActive IS FALSE ";
				} 
			}
			if (StringUtils.isNotBlank(jobTypeCodeFilter)) {
				String taskCodeUid = null;
				List<String> taskCodeJobArr = new ArrayList<String>();
				if (StringUtils.equals("Show only Active", jobTypeCodeFilter)) {
					String strSql = "FROM LookupTaskCode WHERE (isDeleted = FALSE OR isDeleted IS NULL) " +
						"and (isActive IS TRUE OR isActive IS NULL) " +
						"and jobTypeShortCode in (select shortCode from LookupJobType WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (isActive IS TRUE OR isActive IS NULL))";
					
					String strSql2 = "SELECT shortCode, operationCode FROM LookupJobType WHERE (isDeleted = FALSE OR isDeleted IS NULL) and (isActive IS TRUE OR isActive IS NULL)";
				
					List<LookupTaskCode> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
					List<Object[]> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql2);
					
					if(lstResult.size() > 0 && lstResult2.size() > 0) {
						for(LookupTaskCode task:lstResult) {
							String jobTypeShortCode = task.getJobTypeShortCode();
							String operationCode = task.getOperationCode();
								for(Object[] res: lstResult2) {
									Object[] obj = (Object[]) res;
									String shortCode = obj[0].toString();
									String opCode = obj[1].toString();
									if(jobTypeShortCode.equals(shortCode)) {
										if(operationCode.equals(opCode)) {
											taskCodeUid = task.getLookupTaskCodeUid();
											taskCodeJobArr.add(taskCodeUid);
										}
									}
								}
						}
					}
					
					if (!taskCodeJobArr.isEmpty()) {
						customCondition+=(customCondition.length()>0?" AND ":"")+" lookupTaskCodeUid in (:lookupTaskCodeUid)";
						query.addParam("lookupTaskCodeUid", taskCodeJobArr);
					} else {
						customCondition+=(customCondition.length()>0?" AND ":"")+" lookupTaskCodeUid IS NULL";
					}
					
				}
			}
			
			if (StringUtils.isNotBlank(phaseCodeFilter)) {
				String taskCodeUid = null;
				List<String> taskCodePhaseArr = new ArrayList<String>();
				if (StringUtils.equals("Show only Active", phaseCodeFilter)) {
					String strSql = "FROM LookupTaskCode WHERE (isDeleted = FALSE OR isDeleted IS NULL) " +
							"and (isActive IS TRUE OR isActive IS NULL) " +
							"and phaseShortCode in (select shortCode from LookupPhaseCode WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (isActive IS TRUE OR isActive IS NULL))";
						
						String strSql2 = "SELECT shortCode, operationCode FROM LookupPhaseCode WHERE (isDeleted = FALSE OR isDeleted IS NULL) and (isActive IS TRUE OR isActive IS NULL)";
					
						List<LookupTaskCode> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
						List<Object[]> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql2);
						
						if(lstResult.size() > 0 && lstResult2.size() > 0) {
							for(LookupTaskCode task:lstResult) {
								String phaseShortCode = task.getPhaseShortCode();
								String operationCode = task.getOperationCode();
									for(Object[] res: lstResult2) {
										Object[] obj = (Object[]) res;
										String shortCode = obj[0].toString();
										String opCode = obj[1].toString();
										if(phaseShortCode.equals(shortCode)) {
											if(operationCode.equals(opCode)) {
												taskCodeUid = task.getLookupTaskCodeUid();
												taskCodePhaseArr.add(taskCodeUid);
											}
										}
									}
							}
						}
					
					if(!taskCodePhaseArr.isEmpty()) {
						customCondition+=(customCondition.length()>0?" AND ":"")+" lookupTaskCodeUid in (:lookupTaskCodeUid)";
						query.addParam("lookupTaskCodeUid", taskCodePhaseArr);
					} else {
						customCondition+=(customCondition.length()>0?" AND ":"")+" lookupTaskCodeUid IS NULL";
					}
					
				}
			}
		}
		
		if (userSelection.getCustomProperty("reportOperationCodesFilter")!=null) {
			List<String> reportOperationCodesFilter = (List<String>) userSelection.getCustomProperty("reportOperationCodesFilter");
			if (reportOperationCodesFilter.size()>0) {
				customCondition+=(customCondition.length()>0?" AND ":"")+" (operationCode in (:reportOperationCodesFilter) or operationCode='' or operationCode is null) ";
				query.addParam("reportOperationCodesFilter", reportOperationCodesFilter);
			}
		}
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {

		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {

		return null;
	}
	
}
