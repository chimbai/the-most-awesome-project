package com.idsdatanet.d2.drillnet.activityNptEvent;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CompanyServiceProvidedCascadeLookupHandler implements CascadeLookupHandler {

	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String sql = "SELECT distinct a.lookupCompanyUid, a.companyName, a.isActive FROM LookupCompany a, LookupCompanyService b " +
				"WHERE a.lookupCompanyUid = b.lookupCompanyUid " +
				"AND (a.isDeleted IS NULL OR a.isDeleted = '') " +
				"AND (b.isDeleted IS NULL OR b.isDeleted = '') " +
				"AND b.code =:code ORDER BY a.companyName";
		List<Object[]> listCompanyResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "code", key);
		if (listCompanyResults.size() > 0) {
			for (Object[] listCompanyResult : listCompanyResults) {
				if (listCompanyResult[0] != null && listCompanyResult[1] != null) {
					Boolean isActive =null; 
					if (listCompanyResult[2] != null){
						isActive = Boolean.parseBoolean(listCompanyResult[2].toString());
					}
					
					LookupItem lookupItem = new LookupItem(listCompanyResult[0].toString(), listCompanyResult[1].toString());
					lookupItem.setActive(isActive);
					
					result.put(listCompanyResult[0].toString(),lookupItem);
				}
			}
		}

		return result;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://unwantedEvent.contractorType?key=code&amp;value=label", userSelection, null);
		
		for (Map.Entry<String, LookupItem> item : lookupMap.entrySet()) {
			String serviceCode = (String) item.getValue().getKey();
			CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(serviceCode);
			cascadeLookupSet.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, serviceCode, null)));
			result.add(cascadeLookupSet);				
	
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		return null;
	}


}
