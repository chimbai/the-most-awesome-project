package com.idsdatanet.d2.drillnet.opsDatum;

import java.util.List;

import com.idsdatanet.d2.core.stt.STTDataGeneratorConfig;
import com.idsdatanet.d2.core.stt.active.ActiveSTTObjectListener;
import com.idsdatanet.d2.core.stt.active.ActiveSTTRequestParam;
import com.idsdatanet.d2.core.stt.active.DaoCommand;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;

public class OpsDatumActiveSTTListener implements ActiveSTTObjectListener{

	public void afterPerformDaoCommand(DaoCommand daoCommand, ActiveSTTRequestParam params) throws Exception {
		// TODO Auto-generated method stub
		DatumManager.loadDatum();
	}

	public void afterPerformRFT(Object object) throws Exception {
		// TODO Auto-generated method stub
		DatumManager.loadDatum();		
	}

	public void beforePerformDaoCommand(DaoCommand daoCommand, ActiveSTTRequestParam params) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforePerformRFT(Object object) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterCollectionPerformDaoCommand(
			STTDataGeneratorConfig dataGeneratorConfig,
			List<DaoCommand> daoCommands, ActiveSTTRequestParam params) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
