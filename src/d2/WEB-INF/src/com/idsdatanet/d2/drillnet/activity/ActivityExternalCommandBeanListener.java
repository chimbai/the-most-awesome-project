package com.idsdatanet.d2.drillnet.activity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.lookup.CascadeLookupMeta;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.ActivityExt;
import com.idsdatanet.d2.core.model.DepotWellOperationMapping;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RigTour;
import com.idsdatanet.d2.core.model.SysLogActivityExtTransaction;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.core.webservice.dto.DepotDTO;
import com.idsdatanet.depot.witsml.rts.externalactivity.ExternalActivityManager;

import net.sf.json.JSONObject;


public class ActivityExternalCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {
	private static final String REFRESH_DATA = "refresh_data";
	private static final String CHECK_STATUS = "check_status";
	private Boolean ignore24HourChecking = false;
	private Boolean showSystemMessageForActivity = true;
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		double totalActivityDuration = this.recalculateTotalDuration(commandBean);
		String operationHour = "24";
		if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "act_show_warning_if_not_24")))
		{	
			String newActivityRecordInputMode = null;
			if (request != null) {
				newActivityRecordInputMode = request.getParameter("newActivityRecordInputMode");
				UserSession session = UserSession.getInstance(request);
				if (session.getCurrentOperation()!=null) {
					operationHour = ActivityUtils.getOperationHour(session.getCurrentOperation());
				}
			}
			
			if (!("1".equals(newActivityRecordInputMode))){
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityExt.class, "activityDuration");
				thisConverter.setBaseValue(86400);
				if (StringUtils.isNotBlank(operationHour)) {
					Double totalHoursInSecond = Double.parseDouble(operationHour);
					if (!totalHoursInSecond.isNaN()) {
						totalHoursInSecond = totalHoursInSecond * 3600.0;
						thisConverter.setBaseValue(totalHoursInSecond);
					}
				}
				
				if (!thisConverter.getFormattedValue(totalActivityDuration).equals(thisConverter.getFormattedValue()) && !this.ignore24HourChecking){
					commandBean.getSystemMessage().addInfo("Total hours for activities in the day (" + thisConverter.getFormattedValue(totalActivityDuration) + ") is not " + operationHour + " hours.");
				}
			}
			
			if (isTimeOverlapped(commandBean)) {
				commandBean.getSystemMessage().addInfo("There is time overlapping / gaps between activities.");
			}
		}

		//SET DEFAULT INPUT MODE TO "ALL" IF NOT SELECTED
		String strInputMode  = (String) commandBean.getRoot().getDynaAttr().get("inputMode");
		if(StringUtils.isBlank(strInputMode))
		{
			commandBean.getRoot().getDynaAttr().put("inputMode", "all");
		}
		
		UserSession session = UserSession.getInstance(request);
		if (StringUtils.isNotBlank(session.getCurrentRigInformationUid())) {
			RigInformation rig = (RigInformation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, session.getCurrentRigInformationUid());
			List<RigTour> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from RigTour where rigInformationUid=:rigInformationUid and (isDeleted is null or isDeleted=0)", "rigInformationUid", session.getCurrentRigInformationUid());
			if (result == null || result.size() == 0) {
				commandBean.getSystemMessage().addError("There is no Rig Tour assigned to current Rig: " + rig.getRigName());
			}
		} else {
			commandBean.getSystemMessage().addError("Current operation has no rig assigned.");
		}
		List dwoms = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from DepotWellOperationMapping where (isDeleted is null or isDeleted=0) and wellUid=:wellUid and wellboreUid=:wellboreUid and (rigStateDefinitionUid IS NOT NULL AND rigStateDefinitionUid != '')", new String[] {"wellUid", "wellboreUid"}, new String[] {session.getCurrentWellUid(), session.getCurrentWellboreUid()});
		if (dwoms != null && dwoms.size() > 1) {
			commandBean.getSystemMessage().addError("There are more than one DepotWellOperationMapping being used in the current Wellbore. Please make sure there is only one mapping per wellbore.");
		}
	}
	
	private double recalculateTotalDuration(CommandBean commandBean) throws Exception {
		double totalActivityDuration = 0;
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityExt.class, "activityDuration");
		
		for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ActivityExt").values()) {
			ActivityExt activity = (ActivityExt) node.getData();
			
			if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {
				totalActivityDuration += activity.getActivityDuration();
			}
		}
		if (thisConverter.isUOMMappingAvailable()){
			commandBean.getRoot().setCustomUOM("@activityDuration", thisConverter.getUOMMapping());
		}
		commandBean.getRoot().getDynaAttr().put("activityDuration", totalActivityDuration);
		
		return totalActivityDuration;
	}
	
	private Boolean isTimeOverlapped(CommandBean commandBean) throws Exception {
		Date activityFirstStartDateTime = null;
		Date activityLastStartDateTime = null;
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityExt.class, "activityDuration");
		Double activityDuration = 0.0;
		Double activityActualDuration = 0.0;
		for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ActivityExt").values()) {
			ActivityExt activity = (ActivityExt) node.getData();
			if (activity.getIsDeleted() == null || !activity.getIsDeleted()) { 
				if (activity.getStartDatetime() != null && activity.getEndDatetime() != null) {						
					if (activityFirstStartDateTime == null) activityFirstStartDateTime = activity.getStartDatetime();	
					activityLastStartDateTime = activity.getEndDatetime();
					if (activity != null && activity.getActivityDuration() != null) {
						activityDuration += activity.getActivityDuration();
					}
				}
			}
		}
		
		thisConverter.setBaseValueFromUserValue(activityDuration);
		activityDuration=thisConverter.getBasevalue();
		int decimalPlaces = 1;
		BigDecimal bd = new BigDecimal(activityDuration);
		bd = bd.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
		activityDuration = bd.doubleValue();
		
		if (activityFirstStartDateTime != null && activityLastStartDateTime !=null) {	
			activityActualDuration = ActivityUtils.getActivityDurationInSecond(activityFirstStartDateTime, activityLastStartDateTime, thisConverter);
			if (activityActualDuration.compareTo(activityDuration) != 0) {
				return true;
			}
		}
		
		return false;
	}

	private Map<String,LookupItem> getLookup(BaseCommandBean commandBean, HttpServletRequest request, String className, String field) throws Exception
	{
		UserSelectionSnapshot selection = new UserSelectionSnapshot(UserSession.getInstance(request));
		Object handler = null;
		if (commandBean.isCascadeLookup(className, field))
		{
			CascadeLookupMeta meta = commandBean.getCascadeLookupMeta(className, field);
			if (meta!=null)
			handler = meta.getLookupUriOrHandler();
		}
		if (commandBean.isLookup(className, field))
		{
			handler = commandBean.getLookup().get(className+"."+field);
		}
		if (handler !=null)
		{
			if (handler instanceof LookupHandler)
				return ((LookupHandler)handler).getLookup(commandBean, commandBean.getRoot(), selection, request, null);
			if (handler instanceof String)
				return LookupManager.getConfiguredInstance().getLookup(handler.toString(), selection, null);
		}
		return null;
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		DepotDTO depotDTO = new DepotDTO();
		try {
			DepotWellOperationMapping dwom = ExternalActivityManager.getConfiguredInstance().getCurrentWellOpMappedWithDWOM(request);
			if (StringUtils.equals(REFRESH_DATA, invocationKey)) {
				if (dwom != null) {
					ExternalActivityManager.getConfiguredInstance().refreshData(depotDTO, dwom, request);
				} else {
					depotDTO.addErrorMessage("You have not map current Well Operation.");
				}
			} else if (StringUtils.equals(CHECK_STATUS, invocationKey)) {
				if (dwom != null) {
					UserSession userSession = UserSession.getInstance(request);
					SysLogActivityExtTransaction log = ExternalActivityManager.getConfiguredInstance().checkStatus(userSession);
					if (log != null) {
						depotDTO.setResult(log.getTransactionStatus());
					} else {
						depotDTO.setResult(ExternalActivityManager.STATUS_NOT_AVAILABLE);
					}
				}
			}
		} finally {
			JSONObject responseOutput = new JSONObject();
			responseOutput.put("response", depotDTO);
			response.getWriter().write(responseOutput.toString());
		}
	}
	
	public void setIgnore24HourChecking(Boolean ignore24HourChecking) {
		this.ignore24HourChecking = ignore24HourChecking;
	}

	public Boolean getIgnore24HourChecking() {
		return ignore24HourChecking;
	}

	public void setShowSystemMessageForActivity(Boolean showSystemMessageForActivity) {
		this.showSystemMessageForActivity = showSystemMessageForActivity;
	}

	public Boolean getShowSystemMessageForActivity() {
		return showSystemMessageForActivity;
	}	
}
