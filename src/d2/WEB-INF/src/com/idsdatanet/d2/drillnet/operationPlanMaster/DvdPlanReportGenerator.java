package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class DvdPlanReportGenerator implements ReportDataGenerator{
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		QueryProperties qp = new QueryProperties();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		
		
		String strSql = "SELECT operationPlanPhaseUid, sequence, holeSize, phaseCode, phaseName,p10Duration, p50Duration, p90Duration,tlDuration, depthMdMsl, dynamicPhaseType, phaseSummary " +
				"FROM OperationPlanPhase " +
				"WHERE isDeleted IS NULL " +
				"AND operationUid = :currentOperationUid ";
		String[] paramsFields = {"currentOperationUid"};
		Object[] paramsValues = {currentOperationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues,qp);
		
		if (lstResult.size() > 0){
			for(Object objResult: lstResult){
				Object[] objDuration = (Object[]) objResult;
				ReportDataNode thisReportNode = reportDataNode.addChild("operationPlanPhase");
				String operationPlanPhaseUid = "";
				Double holeSize = 0.0;
				String sequence = "";
				String phaseCode = "";
				String phaseName = "";
				String tlDuration = "";
				String p10Duration = "";
				String p50Duration = "";
				String p90Duration = "";
				String actualDuration = "";
				String nptDuration = "";
				String depthMdMsl = "";
				String dynamicPhaseType = "";
				String phaseSummary = "";
				String actualDepth = "";
				String totalDuration = "";
				String totalP50Duration = "";
				String holeSection = "";
				String nptTotalDuration = "";
				
				if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())){
					operationPlanPhaseUid = objDuration[0].toString();
					String strSql2 = "SELECT MAX(depthMdMsl), SUM(activityDuration)/3600.00 as duration " +
							"FROM Activity " +
							"WHERE isDeleted IS NULL " +
							"AND (dayPlus IS NULL or dayPlus = 0) " +
							"AND (isSimop = FALSE or isSimop IS NULL) " +
							"AND (isOffline = FALSE or isOffline IS NULL) " +
							"AND planReference = :operationPlanPhaseUid";
					String[] paramsFields2 = {"operationPlanPhaseUid"};
					Object[] paramsValues2 = {operationPlanPhaseUid};
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
					
					for(Object objResults: lstResult2){
						Object[] objDur = (Object[]) objResults;
						
						if (objDur[0] != null && StringUtils.isNotBlank(objDur[0].toString())){ 
							actualDepth = objDur[0].toString();
						}
						else{
							actualDepth = "0.00";
						}
						
						if (objDur[1] != null && StringUtils.isNotBlank(objDur[1].toString())){ 
							actualDuration = objDur[1].toString();
						}
						else{
							actualDuration = "0.00";
						}
						
						if (lstResult2.size() > 0){
							thisReportNode.addProperty("operationPlanPhaseUid", operationPlanPhaseUid.toString());
							thisReportNode.addProperty("actualDepth", actualDepth.toString());
							thisReportNode.addProperty("actualDuration", actualDuration.toString());
						}
					}
				}
				else {
					thisReportNode.addProperty("operationPlanPhaseUid", "");
					thisReportNode.addProperty("actualDuration", "0.00");
				}
				
				if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())){
					operationPlanPhaseUid = objDuration[0].toString();
					String strSql3 = "SELECT planReference, SUM(activityDuration)/3600.00 as duration " +
							"FROM Activity " +
							"WHERE isDeleted IS NULL " +
							"AND (dayPlus IS NULL or dayPlus = 0) " +
							"AND (isSimop = FALSE or isSimop IS NULL) " +
							"AND (isOffline = FALSE or isOffline IS NULL) " +
							"AND classCode <> 'P' " +
							"AND planReference = :operationPlanPhaseUid";
					String[] paramsFields3 = {"operationPlanPhaseUid"};
					Object[] paramsValues3 = {operationPlanPhaseUid};
					List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3,qp);
					
					for(Object objRes: lstResult3){
						Object[] objLength = (Object[]) objRes;
						
						if (objLength[1] != null && StringUtils.isNotBlank(objLength[1].toString())){ 
							nptDuration = objLength[1].toString();
						}
						else{
							nptDuration = "0.00";
						}
						
						if (lstResult3.size() > 0){
							thisReportNode.addProperty("nptDuration", nptDuration.toString());
						}
					}
				}
				else {
					thisReportNode.addProperty("nptDuration", "0.00");
				}
				
				if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())){
					operationPlanPhaseUid = objDuration[0].toString();
					String strSql4 = "SELECT MAX(depthMdMsl), SUM(activityDuration)/3600.00 as duration " +
							"FROM Activity " +
							"WHERE isDeleted IS NULL " +
							"AND (dayPlus IS NULL or dayPlus = 0) " +
							"AND (isSimop = FALSE or isSimop IS NULL) " +
							"AND (isOffline = FALSE or isOffline IS NULL) " +
							"AND operationUid = :currentOperationUid";
					String[] paramsFields4 = {"currentOperationUid"};
					Object[] paramsValues4 = {currentOperationUid};
					List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4,qp);
					
					for(Object objResults: lstResult4){
						Object[] objD = (Object[]) objResults;						
						if (objD[1] != null && StringUtils.isNotBlank(objD[1].toString())){ 
							totalDuration = objD[1].toString();
						}
						else{
							totalDuration = "0.00";
						}
						
						if (lstResult4.size() > 0){
							thisReportNode.addProperty("totalDuration", totalDuration.toString());
						}
					}
				}
				
				if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())){
					operationPlanPhaseUid = objDuration[0].toString();
					String strSql5 = "SELECT MAX(depthMdMsl), SUM(activityDuration)/3600.00 as duration " +
							"FROM Activity " +
							"WHERE isDeleted IS NULL " +
							"AND (internalClassCode = 'TP' OR internalClassCode = 'TU') " +
							"AND (dayPlus IS NULL or dayPlus = 0) " +
							"AND (isSimop = FALSE or isSimop IS NULL) " +
							"AND (isOffline = FALSE or isOffline IS NULL) " +
							"AND operationUid = :currentOperationUid";
					String[] paramsFields5 = {"currentOperationUid"};
					Object[] paramsValues5 = {currentOperationUid};
					List lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramsFields5, paramsValues5,qp);
					
					for(Object objR: lstResult5){
						Object[] objLong = (Object[]) objR;						
						if (objLong[1] != null && StringUtils.isNotBlank(objLong[1].toString())){ 
							nptTotalDuration = objLong[1].toString();
						}
						else{
							nptTotalDuration = "0.00";
						}
						
						if (lstResult5.size() > 0){
							thisReportNode.addProperty("nptTotalDuration", nptTotalDuration.toString());
						}
					}
				}
				
				if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())){
					operationPlanPhaseUid = objDuration[0].toString();
					String strSql5 = "SELECT operationPlanPhaseUid, SUM(p50Duration)/3600.00 as duration " +
							"FROM OperationPlanPhase " +
							"WHERE isDeleted IS NULL " +
							"AND operationUid = :currentOperationUid";
					String[] paramsFields5 = {"currentOperationUid"};
					Object[] paramsValues5 = {currentOperationUid};
					List lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramsFields5, paramsValues5,qp);
					
					for(Object objResults: lstResult5){
						Object[] objL = (Object[]) objResults;						
						if (objL[1] != null && StringUtils.isNotBlank(objL[1].toString())){ 
							totalP50Duration = objL[1].toString();
						}
						else{
							totalP50Duration = "0.00";
						}
						
						if (lstResult5.size() > 0){
							thisReportNode.addProperty("totalP50Duration", totalP50Duration.toString());
						}
					}
				}
				
				if (objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())){ sequence = objDuration[1].toString();}
				if (objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())){ holeSize = Double.parseDouble(objDuration[2].toString());}
				if (objDuration[3] != null && StringUtils.isNotBlank(objDuration[3].toString())){ phaseCode = objDuration[3].toString();}
				if (objDuration[4] != null && StringUtils.isNotBlank(objDuration[4].toString())){ phaseName = objDuration[4].toString();}
				if (objDuration[5] != null && StringUtils.isNotBlank(objDuration[5].toString())){
					p10Duration = objDuration[5].toString();
				}
				else {
					p10Duration = "0.00";
				}
				if (objDuration[6] != null && StringUtils.isNotBlank(objDuration[6].toString())){
					p50Duration = objDuration[6].toString();
				}
				else {
					p50Duration = "0.00";
				}
				if (objDuration[7] != null && StringUtils.isNotBlank(objDuration[7].toString())){
					p90Duration = objDuration[7].toString();
				}
				else {
					p90Duration = "0.00";
				}
				if (objDuration[8] != null && StringUtils.isNotBlank(objDuration[8].toString())){
					tlDuration = objDuration[8].toString();
				}
				else {
					tlDuration = "0.00";
				}
				if (objDuration[9] != null && StringUtils.isNotBlank(objDuration[9].toString())){
					depthMdMsl = objDuration[9].toString();
				}
				else {
					tlDuration = "0.00";
				}
				
				if (objDuration[10] != null && StringUtils.isNotBlank(objDuration[10].toString())){
					if (objDuration[10].toString().equals("CMPLT")){
						holeSection = "Completion";
					}
					else
						holeSection = this.getHoleSizeLabel(this.formatHoleSize(holeSize), userContext) + " Section";
				}
				
				if (objDuration[10] != null && StringUtils.isNotBlank(objDuration[10].toString())){ dynamicPhaseType = objDuration[10].toString();}
				if (objDuration[11] != null && StringUtils.isNotBlank(objDuration[11].toString())){ phaseSummary = objDuration[11].toString();}
				
				thisReportNode.addProperty("holeSize", holeSize.toString());
				thisReportNode.addProperty("holeSection", holeSection.toString());
				thisReportNode.addProperty("sequence", sequence.toString());
				thisReportNode.addProperty("phaseCode", phaseCode.toString());
				thisReportNode.addProperty("phaseName", phaseName.toString());
				thisReportNode.addProperty("tlDuration", tlDuration.toString());
				thisReportNode.addProperty("p10Duration", p10Duration.toString());
				thisReportNode.addProperty("p50Duration", p50Duration.toString());
				thisReportNode.addProperty("p90Duration", p90Duration.toString());
				thisReportNode.addProperty("depthMdMsl", depthMdMsl.toString());
				thisReportNode.addProperty("dynamicPhaseType", dynamicPhaseType.toString());
				thisReportNode.addProperty("phaseSummary", phaseSummary.toString());
			}
					
		}
	}
	
	private String getHoleSizeLabel(String holeSizeValue, UserContext userContext) throws Exception {
		String retValue = "";
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://activity.holeSize?key=code&amp;value=label", userSelection, null);
		if (lookupMap !=null && StringUtils.isNotBlank(lookupMap.toString())  && lookupMap !=null){
			 LookupItem lookup = lookupMap.get(holeSizeValue.toString());
			if (lookup !=null)	retValue = lookup.getValue().toString();					
		} 
		
		return retValue;
	}
	
	private String formatHoleSize(Double holeSize) {
		if (holeSize==null) return "";
		DecimalFormat nf = new DecimalFormat();
		nf.setMaximumFractionDigits(6);
		nf.setMinimumFractionDigits(6);
		return nf.format(holeSize);
	}

}