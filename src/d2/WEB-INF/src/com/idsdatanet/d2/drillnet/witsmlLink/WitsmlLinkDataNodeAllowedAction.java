package com.idsdatanet.d2.drillnet.witsmlLink;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;
/**
 * @author RYAU
 *
 */
public class WitsmlLinkDataNodeAllowedAction implements DataNodeAllowedAction {
	
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		
		if(targetClass.equals("WitsmlLink") && action.equals("edit")) {
			return false;
		}
		return true;
	}
}
