package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.Date;

public class NPTSummary implements java.io.Serializable {
	
	private String wellUid;
	private String operationUid;
	private String operationType;
	private String country;
	private String lookupCompanyUid;
	private String classCode;
	private String taskCode;
	private Double duration;
	private String dailyUid;
	private Date dayDate;
	private String operationArea;
	private String nptSubject;
	private String wellName;
	private String wellboreUid;
	private String wellboreName;
	private String field;
	private String operationName;
	private String phaseCode;
	private String sections;
	private String nptSubCategory;
	private String nptMainCategory;	
	private String activityDescription;
	private Date startDatetime;
	private String rigInformationUid;
	private String incidentLevel;
	private Double depthMdMsl;
	private String depthMdMslUom;
	private String incidentNumber;
	private String activityUid;
	
	public void setDuration(Double duration) {
		this.duration = duration;
	}
	public Double getDuration() {
		return duration;
	}
	public NPTSummary(String wellUid, String operationUid, String country,
			String lookupCompanyUid, String classCode, Double duration, String dailyUid, Date dayDate) {
		this.wellUid = wellUid;
		this.operationUid = operationUid;
		this.country = country;
		this.lookupCompanyUid = lookupCompanyUid;
		this.classCode = classCode;
		this.duration = duration;
		this.dailyUid = dailyUid;
		this.dayDate = dayDate;
	}
	public void setLookupCompanyUid(String lookupCompanyUid) {
		this.lookupCompanyUid = lookupCompanyUid;
	}
	public String getLookupCompanyUid() {
		return lookupCompanyUid;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry() {
		return country;
	}
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	public String getClassCode() {
		return classCode;
	}
	public void setWellUid(String wellUid) {
		this.wellUid = wellUid;
	}
	public String getWellUid() {
		return wellUid;
	}
	public void setOperationUid(String operationUid) {
		this.operationUid = operationUid;
	}
	public String getOperationUid() {
		return operationUid;
	}
	public void setDailyUid(String dailyUid) {
		this.dailyUid = dailyUid;
	}
	public String getDailyUid() {
		return dailyUid;
	}
	public void setDayDate(Date dayDate) {
		this.dayDate = dayDate;
	}
	public Date getDayDate() {
		return dayDate;
	}
	public void setNptSubject(String nptSubject) {
		this.nptSubject = nptSubject;
	}
	public String getNptSubject() {
		return nptSubject;
	}
	public void setOperationArea(String operationArea) {
		this.operationArea = operationArea;
	}
	public String getOperationArea() {
		return operationArea;
	}
	public void setWellName(String wellName) {
		this.wellName = wellName;
	}
	public String getWellName() {
		return wellName;
	}
	public void setWellboreUid(String wellboreUid) {
		this.wellboreUid = wellboreUid;
	}
	public String getWellboreUid() {
		return wellboreUid;
	}
	public void setWellboreName(String wellboreName) {
		this.wellboreName = wellboreName;
	}
	public String getWellboreName() {
		return wellboreName;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getField() {
		return field;
	}
	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}
	public String getOperationName() {
		return operationName;
	}
	public void setPhaseCode(String phaseCode) {
		this.phaseCode = phaseCode;
	}
	public String getPhaseCode() {
		return phaseCode;
	}
	public void setSections(String sections) {
		this.sections = sections;
	}
	public String getSections() {
		return sections;
	}
	public void setNptSubCategory(String nptSubCategory) {
		this.nptSubCategory = nptSubCategory;
	}
	public String getNptSubCategory() {
		return nptSubCategory;
	}
	public void setNptMainCategory(String nptMainCategory) {
		this.nptMainCategory = nptMainCategory;
	}
	public String getNptMainCategory() {
		return nptMainCategory;
	}
	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}
	public String getActivityDescription() {
		return activityDescription;
	}

	public void setStartDatetime(Date startDatetime2) {
		this.startDatetime = startDatetime2;
	}
	public Date getStartDatetime() {
		return startDatetime;
	}
	public void setRigInformationUid(String rigInformationUid) {
		this.rigInformationUid = rigInformationUid;
	}
	public String getRigInformationUid() {
		return rigInformationUid;
	}
	public void setIncidentLevel(String incidentLevel) {
		this.incidentLevel = incidentLevel;
	}
	public String getIncidentLevel() {
		return incidentLevel;
	}
	public void setDepthMdMsl(Double depthMdMsl) {
		this.depthMdMsl = depthMdMsl;
	}
	public Double getDepthMdMsl() {
		return depthMdMsl;
	}
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	public String getOperationType() {
		return operationType;
	}
	public void setDepthMdMslUom(String depthMdMslUom) {
		this.depthMdMslUom = depthMdMslUom;
	}
	public String getDepthMdMslUom() {
		return depthMdMslUom;
	}
	public void setIncidentNumber(String incidentNumber) {
		this.incidentNumber = incidentNumber;
	}
	public String getIncidentNumber() {
		return incidentNumber;
	}
	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}
	public String getTaskCode() {
		return taskCode;
	}
	public void setActivityUid(String activityUid) {
		this.activityUid = activityUid;
	}
	public String getActivityUid() {
		return activityUid;
	}
}
