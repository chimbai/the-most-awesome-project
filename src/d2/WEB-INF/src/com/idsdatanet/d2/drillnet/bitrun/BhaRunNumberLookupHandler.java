package com.idsdatanet.d2.drillnet.bitrun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.bharun.BharunUtils;

public class BhaRunNumberLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		String operationUid = userSelection.getOperationUid();
		String dailyUid = userSelection.getDailyUid();

		List<Object[]> listBharunResults = new ArrayList<Object[]>();
		
		if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "allowCopyBHAWhenGotDateOut"))){
			listBharunResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select bharunUid, bhaRunNumber from Bharun where " +
					"(isDeleted = false or isDeleted is null) and operationUid = :operationUid " +
					"order by bhaRunNumber", "operationUid", operationUid);
		
		}else {
			String strInCondition = BharunUtils.continueBharunNumberCondition(operationUid, dailyUid);
			if (StringUtils.isNotBlank(strInCondition)) {
			
				//String sql = "select a.bharunUid, a.bhaRunNumber from Bharun a, BharunDailySummary b where (a.isDeleted = false or a.isDeleted is null) " +
				//		"and (b.isDeleted = false or b.isDeleted is null) and a.bharunUid = b.bharunUid and b.dailyUid IN (" + strInCondition +  ") order by a.bhaRunNumber";
				
				listBharunResults = ApplicationUtils.getConfiguredInstance().getDaoManager().find("select a.bharunUid, a.bhaRunNumber from Bharun a, BharunDailySummary b where (a.isDeleted = false or a.isDeleted is null) " +
								"and (b.isDeleted = false or b.isDeleted is null) and a.bharunUid = b.bharunUid and b.bharunUid " +
								"IN (" + strInCondition +  ") order by a.bhaRunNumber");
				
			}
		}

		if (listBharunResults.size() > 0) {
			Collections.sort(listBharunResults, new BharunNumberComparator());
			for (Object[] listBharunResult : listBharunResults) {
				if (listBharunResult[0] != null && listBharunResult[1] != null) {

					result.put(listBharunResult[0].toString(), new LookupItem(listBharunResult[0].toString(), listBharunResult[1].toString()));
				}
			}
		}

		return result;
	}
	
	private class BharunNumberComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Object in1 = o1[1];
				Object in2 = o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(in1.toString());
				String f2 = WellNameUtil.getPaddedStr(in2.toString());
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}

}
