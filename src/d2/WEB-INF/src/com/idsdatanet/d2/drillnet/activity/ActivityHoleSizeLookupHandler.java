package com.idsdatanet.d2.drillnet.activity;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.HoleSection;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityHoleSizeLookupHandler implements LookupHandler{

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {

		LinkedHashMap<String, LookupItem> holeSizeLookup = new LinkedHashMap<String, LookupItem>();
		Map<String, LookupItem>holeSizeList = getLookupList(userSelection, "xml://casingsection.casing_holesize?key=code&amp;value=label");
		CustomFieldUom holeSizeConverter = new CustomFieldUom(this.getLocale(userSelection), HoleSection.class, "holeSize");

		if (userSelection.getWellboreUid()!=null)
		{
			String strSql = "FROM HoleSection WHERE (isDeleted=false or isDeleted is null) AND wellboreUid = :thisWellboreUid order by holeSize" ;
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisWellboreUid", userSelection.getWellboreUid());
			
			for (Object obj : list)
			{
				HoleSection thisHoleSection = (HoleSection) obj;
				String holeSize = "";
				if (holeSizeList != null && thisHoleSection.getHoleSize()!=null && StringUtils.isNotBlank(thisHoleSection.getSectionName())) {
					holeSize = holeSizeConverter.formatOutputPrecision(0.000000);			
					holeSize = holeSizeConverter.formatOutputPrecision(thisHoleSection.getHoleSize());
					LookupItem lookupItem = holeSizeList.get(holeSize);
					if (lookupItem != null) {
						holeSize = thisHoleSection.getSectionName().toString() + " (" + lookupItem.getValue().toString() + ")";
						
						LookupItem item = new LookupItem(thisHoleSection.getHoleSectionUid(),holeSize);
						holeSizeLookup.put(thisHoleSection.getHoleSectionUid(), item);
					}
				}		
			}
		}
		return holeSizeLookup;
		
	}
	
	private Map<String, LookupItem> getLookupList(UserSelectionSnapshot userSelection, String uri) {
		Map<String, LookupItem> result = new HashMap<String, LookupItem>(); 
		try {
			result = LookupManager.getConfiguredInstance().getLookup(uri, userSelection, null);
		} catch (Exception e) {
			System.out.println("lookup not found : " + uri);
		}
		return result;
	}
	
	private Locale getLocale(UserSelectionSnapshot userSelection) throws Exception {
		return userSelection.getLocale();
	}
}
