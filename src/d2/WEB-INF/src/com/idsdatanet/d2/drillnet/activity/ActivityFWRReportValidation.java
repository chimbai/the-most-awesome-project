package com.idsdatanet.d2.drillnet.activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.validation.CommandBeanReportValidator;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityFWRReportValidation implements CommandBeanReportValidator {
	private class DayTotal implements Comparable<DayTotal> {
		Date dayDate = null;
		String dayNumber = null;
		double total = 0;
		
		public int compareTo(DayTotal o){
			if(this.dayDate == null && o.dayDate == null) return 0;
			if(this.dayDate == null) return -1;
			if(o.dayDate == null) return 1;
			return this.dayDate.compareTo(o.dayDate);
		}
	}

	private double null2Zero(Double value){
		if(value == null) return 0;
		return value;
	}
	
	private String null2EmptyString(String value){
		if(value == null) return "";
		return value;
	}
	
	public void validate(CommandBean commandBean, ReportValidation reportValidation, String reportType, UserSelectionSnapshot userSelection) throws Exception {
		if(commandBean.getRoot() == null) return;
		Map<String, CommandBeanTreeNode> activity_list = commandBean.getRoot().getList().get(Activity.class.getSimpleName());
		if(activity_list == null) return;
		
		Map<String, DayTotal> days = new HashMap<String, DayTotal>();
		DayTotal total = null;
		
		for(CommandBeanTreeNode activity_node: activity_list.values()){
			if(! activity_node.getInfo().isTemplateNode()){
				Activity activity = (Activity) activity_node.getData();
				total = days.get(activity.getDailyUid());
				if(total == null){
					total = new DayTotal();
					days.put(activity.getDailyUid(), total);
				}
				if (activity.getDayPlus() != null) {
					if ("0".equals(activity.getDayPlus().toString())) total.total += null2Zero(activity.getActivityDuration());
				}
			}
		}

		if(days.size() == 0) return;
		
		DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getInstance(commandBean.getUserLocale());
		decimalFormat.applyPattern("##0.0");
		
		List<DayTotal> error_days = new ArrayList<DayTotal>();
		
		String dailyType=CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(userSelection.getOperationUid());
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
		Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		String operationHour = "24";
		if (operation !=null){
			if(StringUtils.isNotBlank(operation.getOperationHour())){
				operationHour = operation.getOperationHour();
			}
		}
		
		if (StringUtils.isNotBlank(operationHour)){
			operationHour = thisConverter.formatOutputPrecision(Double.parseDouble(operationHour));
		}
		
		for(String dailyuid: days.keySet()){
			total = days.get(dailyuid);
			thisConverter.setBaseValueFromUserValue(total.total);
			thisConverter.changeUOMUnit("Hour");
			String totalduration = thisConverter.formatOutputPrecision();
			if(totalduration.compareTo(operationHour) !=0 ){
				List<ReportDaily> reportdaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid =:dailyUid AND reportType='" + dailyType + "'", "dailyUid", dailyuid);
				if (reportdaily.size()>0)
				{
					ReportDaily day=reportdaily.get(0);
					total.dayDate = day.getReportDatetime();
					total.dayNumber = null2EmptyString(day.getReportNumber()) + " (" + totalduration + " " + thisConverter.getUomSymbol() + ")";
					error_days.add(total);
				}
			}
		}
		
		if(error_days.size() == 0) return;
		
		Collections.sort(error_days);
		
		String days_list = null;
		
		if(error_days.size() == 1){
			days_list = "#" + error_days.get(0).dayNumber;
		}else{
			for(int i=0; i < error_days.size() - 1; ++i){
				if(i==0){
					days_list = "#" + error_days.get(i).dayNumber;
				}else{
					days_list += ", #" + error_days.get(i).dayNumber;
				}
			}
			days_list += " and #" + error_days.get(error_days.size()-1).dayNumber;
		}
		
		reportValidation.addError("Total activity duration is not " + operationHour + " hours on Day " + days_list + ".");
	}
}
