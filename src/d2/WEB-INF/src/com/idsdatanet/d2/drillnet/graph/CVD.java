package com.idsdatanet.d2.drillnet.graph;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.graph.BaseGraph;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CVD extends BaseGraph {
	private final static Integer ACTUAL_COST_DEPTH 	  = 0;
	private final static Integer PLANNED_COST_DEPTH   = 1;
	private Map<String, XYDataset> xyDatasetMap   	  = new HashMap<String, XYDataset>();
	private List<Object[]> operationPlanPhaseLists 	  = null;
	private List dailyLists 			 		  	  = null;
	private QueryProperties queryProperties 		  = new QueryProperties();
	private NumberFormat formatter 					  = new DecimalFormat("#.###");
	private Boolean showActualDataToDate = false;
	private String currentOperationPlanMasterUid = null;
	
	public void setShowActualDataToDate(Boolean showActualDataToDate) {
		this.showActualDataToDate = showActualDataToDate;
	}
	
	// default constructor
	public CVD() {}
	
	/**
	 * Main method that must be implemented in order to paint graph.
	 * @param request A HttpServletRequest object from BaseGraphController. Use to get any parameter from HTTP.
	 * @param response A HttpServletResponse object from BaseGraphController. Basically use for outputstream purpose.
	 */
	@Override
	public JFreeChart paint() throws Exception {
		String groupUid = null;
		Locale locale = null;
		this.queryProperties.setUomConversionEnabled(false);
		
		if (this.getCurrentHttpRequest() != null) {
			UserSession session = UserSession.getInstance(this.getCurrentHttpRequest());
			groupUid = session.getCurrentGroupUid();
			locale = session.getUserLocale();
		} else {
			groupUid = this.getCurrentUserContext().getUserSelection().getGroupUid();
			locale = this.getCurrentUserContext().getUserSelection().getLocale();
		}
		this.formatter = (DecimalFormat) DecimalFormat.getInstance(locale);
		
		if(this.getCurrentUserContext() != null) {
			String operationUid = this.getCurrentUserContext().getUserSelection().getOperationUid();
			this.currentOperationPlanMasterUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
			
			String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, operationUid);
			if(StringUtils.isNotBlank(operationName)) {
				setTitle(operationName);
			}
					
			this.operationPlanPhaseLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select operationUid, p10Duration, p50Duration, p90Duration, tlDuration, depthMdMsl, phaseBudget, operationPlanPhaseUid, p10Cost, p90Cost from OperationPlanPhase where (isDeleted = false or isDeleted is null) and operationPlanMasterUid = :operationPlanMasterUid order by sequence,phaseSequence", "operationPlanMasterUid", this.currentOperationPlanMasterUid, this.queryProperties);
			this.dailyLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and reportType=:reportType order by reportDatetime", new String[] {"operationUid", "reportType"}, new Object[] {operationUid, reportType}, this.queryProperties);
		}
		
		// start drawing ...
		CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale());
		
		thisConverter.setReferenceMappingField(Activity.class, "depthMdMsl");
		String depth = "Depth (" + thisConverter.getUomSymbol() + ")";
		thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
		String cost ="Cost (" + thisConverter.getUomSymbol() + ")";
	       
		XYDataset dataset = createActualCostDepthDataset();
		JFreeChart chart = ChartFactory.createXYLineChart(
			getTitle(),      			// chart title
            cost,                  		// x axis label
            depth,                    	// y axis label
            dataset,                  	// data
            PlotOrientation.VERTICAL,
            true,                     	// include legend
            true,                     	// tooltips
            false                     	// urls
		);
        
		// main settings
        XYPlot plot = chart.getXYPlot();
        plot.setOrientation(PlotOrientation.VERTICAL);
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.lightGray);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.getRangeAxis().setFixedDimension(15.0);
        XYItemRenderer renderer = plot.getRenderer();
        renderer.setSeriesPaint(0, Color.black);
        
        this.createPlannedCostAndDepthDataset(groupUid);         
        XYDataset planDataset = (XYDataset) this.xyDatasetMap.get("plannedCostDepth");
    	plot.setDataset(PLANNED_COST_DEPTH, planDataset);
    	plot.mapDatasetToRangeAxis(PLANNED_COST_DEPTH, ACTUAL_COST_DEPTH);
        XYItemRenderer planRenderer = new StandardXYItemRenderer();
        planRenderer.setSeriesPaint(0, Color.green);
        plot.setRenderer(PLANNED_COST_DEPTH, planRenderer); 
        
        if (isTitleEnabled()) chart.addSubtitle(new TextTitle("Cost Vs Depth"));
        chart.setBackgroundPaint(Color.lightGray);
        chart.getXYPlot().getRangeAxis(ACTUAL_COST_DEPTH).setInverted(true);
        ((NumberAxis) chart.getXYPlot().getRangeAxis()).setNumberFormatOverride(formatter);
        ((NumberAxis) chart.getXYPlot().getDomainAxis()).setNumberFormatOverride(formatter);
        
        return chart;
	}
	
	
	
	/**
	 * Create a dataset for JFreeChart to draw a Actual Day Depth line
	 * @return
	 * @throws Exception
	 */
	
	private XYDataset createActualCostDepthDataset() throws Exception {
		XYSeries actualCostDepthSeries = new XYSeries("Actual Cost/Depth");
	
		if(this.dailyLists != null && dailyLists.size() > 0) {		
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(), Activity.class, "depthMdMsl");
			String wellboreUid = null;
			String currentDailyUid = null;
			
			if (this.getCurrentUserContext() != null) {
				UserContext userContext = this.getCurrentUserContext();
				wellboreUid = userContext.getUserSelection().getWellboreUid();
				currentDailyUid = userContext.getUserSelection().getDailyUid();

				if (StringUtils.isNotBlank(wellboreUid)) {
					Wellbore wellbore 	= (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, wellboreUid);
					
					if (StringUtils.isBlank(wellbore.getParentWellboreUid())) {
						actualCostDepthSeries.add(0.0, 0.0);				
					}else {
						Double kickOffMd = 0.0;
						if(wellbore.getKickoffMdMsl() != null)thisConverter.setBaseValue(wellbore.getKickoffMdMsl());
						kickOffMd = thisConverter.getConvertedValue();
						actualCostDepthSeries.add(0.0, kickOffMd);					
					}
					
					String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(userContext.getUserSelection().getOperationUid());
					
					for(Iterator i=dailyLists.iterator(); i.hasNext();) {
						ReportDaily reportDaily 		 = (ReportDaily)i.next();			
						String activityQuery = "from Activity where (isDeleted = false or isDeleted is null) and dailyUid=:dailyUid and operationUid = :operationUid AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') AND (isOffline=0 or isOffline is null) AND (isSimop=0 or isSimop is null) order by endDatetime";
						String[] paramNames  = {"dailyUid", "operationUid"};
						String[] paramValues = {reportDaily.getDailyUid(), userContext.getUserSelection().getOperationUid()};
						List activityLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(activityQuery, paramNames, paramValues, this.queryProperties);

						Double dayDepth = null;
						Double dayCost = null;
						
						if(activityLists != null && activityLists.size() > 0) {
							for(Iterator it=activityLists.iterator(); it.hasNext();) { 
								Activity activity 	= (Activity)it.next();								
								dayDepth = null;					
								if (activity.getDepthMdMsl() !=null) {
									thisConverter.setBaseValue(activity.getDepthMdMsl());
									dayDepth = thisConverter.getConvertedValue();
								}	
							}
							dayCost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(reportDaily.getDailyUid(), reportType);
							if (dayCost!=null && dayDepth!=null)
							actualCostDepthSeries.add(dayCost, dayDepth);
						}
						
						if (reportDaily.getDailyUid().equals(currentDailyUid) && showActualDataToDate) break;
					}
				}
			}
		}
		
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(actualCostDepthSeries);
		
		return dataset;
	}
	
	private void createPlannedCostAndDepthDataset(String groupUid) throws Exception {
		XYSeries plannedCostDepthSeries = new XYSeries("Planned Cost/Depth");
        CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(), Activity.class, "depthMdMsl");
		String wellboreUid = null;
		
		if (this.getCurrentUserContext() != null) {
			wellboreUid = this.getCurrentUserContext().getUserSelection().getWellboreUid();
		}
		
		if (StringUtils.isNotBlank(wellboreUid)) {
			Wellbore wellbore 	= (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, wellboreUid);
			
			if (StringUtils.isBlank(wellbore.getParentWellboreUid())) {
				plannedCostDepthSeries.add(0.0, 0.0);
			}else {
				Double kickOffMd = 0.0;
				if(wellbore.getKickoffMdMsl() != null)thisConverter.setBaseValue(wellbore.getKickoffMdMsl());
				kickOffMd = thisConverter.getConvertedValue();
				plannedCostDepthSeries.add(0.0, kickOffMd);
			}
			
			Double cumDuration = 0.0;
			if(this.operationPlanPhaseLists != null && operationPlanPhaseLists.size() > 0) {
				for (Object[] recp : this.operationPlanPhaseLists) {
					Double Depth = null;
					Double Duration = null;
					if (recp[5]!=null)
						Depth =  Double.parseDouble(recp[5].toString());
					if (recp[6]!=null)
						Duration = Double.parseDouble(recp[6].toString());		
					if (Depth != null && Duration != null){
						thisConverter.setBaseValue(Depth);
						Depth = thisConverter.getConvertedValue();
						cumDuration +=Duration;
						plannedCostDepthSeries.add(cumDuration, Depth);
					}									
				}
			}
			
			XYSeriesCollection plannedCostDepthDataset = new XYSeriesCollection();
			plannedCostDepthDataset.addSeries(plannedCostDepthSeries);
			this.xyDatasetMap.put("plannedCostDepth", plannedCostDepthDataset);
		}
	}
	
	public void dispose() {
		this.xyDatasetMap.clear();
		this.operationPlanPhaseLists = null;
		this.dailyLists = null;
	}
}
