package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class SelectiveCompanyLookupHandler implements LookupHandler{
	private Boolean useNameAsKey = false;
	public Boolean getUseNameAsKey() {
		return useNameAsKey;
	}
	public void setUseNameAsKey(Boolean useNameAsKey) {
		this.useNameAsKey = useNameAsKey;
	}
	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		List<Object[]> allCompany = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT lookupCompanyUid, companyName from LookupCompany Where (isDeleted is null or isDeleted = false) order by companyName");
		List<String> actCompany = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT DISTINCT lookupCompanyUid From Activity Where (isDeleted is null or isDeleted = false)");
		List<String> bhaCompany = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT DISTINCT directionalCompany From Bharun Where (isDeleted is null or isDeleted = false)");
		Map<String,LookupItem> lookupList = new LinkedHashMap<String,LookupItem>();
		if (allCompany.size()>0)
		{
			for(Object[] item:allCompany)
			{
				String lookupCompanyUid=useNameAsKey?item[1].toString():item[0].toString();
				String companyName = item[1].toString();
				
				if (actCompany.contains(lookupCompanyUid) || bhaCompany.contains(lookupCompanyUid))
					lookupList.put(lookupCompanyUid, new LookupItem(lookupCompanyUid,companyName));
			}
		}
		return lookupList;
	}

}
