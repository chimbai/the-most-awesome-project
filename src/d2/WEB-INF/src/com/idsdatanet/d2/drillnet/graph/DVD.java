package com.idsdatanet.d2.drillnet.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.swing.JPanel;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.TextAnchor;

import bsh.StringUtil;

import com.idsdatanet.d2.infra.uom.OperationUomAndDatumSelector;
import com.idsdatanet.d2.infra.uom.OperationUomContext;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.graph.BaseGraph;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LookupClassCode;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationDvdAnnotation;
import com.idsdatanet.d2.core.model.OperationPlanMaster;
import com.idsdatanet.d2.core.model.OperationPlanSetting;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

/**
 * This class paint a "Day Vs. Depth" graph. It has to implement <code>Painter</code> 
 * in order for GraphManager to process the <code>paint(request, outputstream)</code> method.
 * @author RYAU
 *
 */
public class DVD extends BaseGraph {
	private final static Integer ACTUAL_DAY_DEPTH 	  = 0;
	private final static Integer PLANNED_10_DAY_DEPTH = 1;
	private final static Integer PLANNED_50_DAY_DEPTH = 2;
	private final static Integer PLANNED_90_DAY_DEPTH = 3;
	private final static Integer PLANNED_COST 		  = 4;
	private final static Integer ACTUAL_COST 		  = 5;
	private final static Integer PLANNED_TL 		  = 6;
	private final static Integer PLANNED_10_Cost 	  = 7;
	private final static Integer PLANNED_90_Cost      = 8;
	private final static Integer PLANNED_MEAN_DAY_DEPTH = 9;
	private Map<String, XYDataset> xyDatasetMap   	  = new HashMap<String, XYDataset>();
	private Map<String, Boolean> dataAvailabilityMap  = new HashMap<String, Boolean>();
	private List<Object[]> operationPlanPhaseLists 		  	  = null;
	private List dailyLists 			 		  	  = null;
	private List activityLists			 		  	  = null;
	private List annotationLists					  = null;
	private QueryProperties queryProperties 		  = new QueryProperties();
	private NumberFormat formatter 					  = new DecimalFormat("#.###");
	private Double maxDepth							  = 0.0;
	private Double maxDuration						  = 0.0;
	
	private Boolean showActualCost = true;
	private Boolean showPlannedCost = true;
	private Boolean showAnnotation = false;
	private Boolean showActualDataToDate = false;
	private Boolean showProductiveTimeDepth = false;
	private Boolean showP10P90Cost = false;
	
	private String currentOperationPlanMasterUid = null;
	private OperationPlanSetting operationPlanSetting = null;
	private Color TEAL = new Color(5, 237, 255);
	private Color customYellow = new Color(242, 199, 0);
	private Color customeOrange = new Color(245, 155, 0);
	private Color[] colors = {
			Color.BLACK, 
			Color.BLUE,
			Color.GRAY,
			Color.GREEN, 
			//Color.ORANGE,
			customeOrange,
			Color.RED,
			//Color.YELLOW,
			customYellow,
			TEAL			
		};
	private CustomFieldUom durationConverter = null;
	private OperationUomContext operationUomContext = null; 
	
	public void setShowP10P90Cost(Boolean showP10P90Cost) {
		this.showP10P90Cost = showP10P90Cost;
	}
	public Boolean getShowP10P90Cost() {
		return this.showP10P90Cost;
	}
	
	public void setShowProductiveTimeDepth(Boolean showProductiveTimeDepth) {
		this.showProductiveTimeDepth = showProductiveTimeDepth;
	}
	public Boolean getShowProductiveTimeDepth() {
		return this.showProductiveTimeDepth;
	}
	
	public void setShowActualCost(Boolean showActualCost) {
		this.showActualCost = showActualCost;
	}
	public Boolean getShowActualCost() {
		return this.showActualCost;
	}
	public Boolean getShowPlannedCost() {
		return this.showPlannedCost;
	}
	public void setShowPlannedCost(Boolean showPlannedCost) {
		this.showPlannedCost = showPlannedCost;
	}
	public void setShowActualDataToDate(Boolean showActualDataToDate) {
		this.showActualDataToDate = showActualDataToDate;
	}
	
	public Boolean getShowAnnotation() {
		return this.showAnnotation;
	}
	public void setShowAnnotation(Boolean showAnnotation) {
		this.showAnnotation = showAnnotation;
	}
	
	// dashed line
	private BasicStroke dashesStroke = new java.awt.BasicStroke(1f,
		        BasicStroke.CAP_SQUARE,
		        BasicStroke.JOIN_MITER,
		        10.0f,
		        new float[] {2f, 3f, 2f, 3f},
		        0f
		    );
	
	// longdashed line
	private BasicStroke longdashesStroke = new java.awt.BasicStroke(1f,
		        BasicStroke.CAP_SQUARE,
		        BasicStroke.JOIN_MITER,
		        10.0f,
		        new float[] {3f, 3f, 3f, 3f},
		        0f
		    );
	
	// long dashed line
	private BasicStroke longDashesDotStroke  = new java.awt.BasicStroke(1f,
		        BasicStroke.CAP_SQUARE,
		        BasicStroke.JOIN_MITER,
		        10.0f,
		        new float[] {7f, 3f, 2f, 3f},
		        0f
		    );
	
	// 2 dashed line + 1 long dashed line 
	private BasicStroke dotDashStroke = new java.awt.BasicStroke(1f,
		        BasicStroke.CAP_SQUARE,
		        BasicStroke.JOIN_MITER,
		        10.0f,
		        new float[] {1f, 3f, 1f, 3f, 6f, 3f},
		        0f
		    );
	
	// dotted line
	private BasicStroke dotStroke = new java.awt.BasicStroke(1f,
		        BasicStroke.CAP_SQUARE,
		        BasicStroke.JOIN_MITER,
		        10.0f,
		        new float[] {1f, 3f, 1f, 3f},
		        0f
		    );
	
	// solid line
	private BasicStroke solidStroke = new java.awt.BasicStroke(1f,
		        BasicStroke.CAP_SQUARE,
		        BasicStroke.JOIN_MITER,
		        10.0f,
		        new float[] {1f},
		        0f
		    );
	
	private BasicStroke[] BStroke = {
			dotDashStroke, // 2 dashed line + 1 long dashed line 
			solidStroke, // Solid Line
			longdashesStroke, // Fine Dashed
			dotStroke, // Fine Dotted	
	};
	
	private CustomFieldUom getDurationConverter() throws Exception {
		return new CustomFieldUom(this.getLocale(), Operation.class, "daysSpentPriorToSpud", this.operationUomContext);
	}
	
	// default constructor
	public DVD() {}
	
	@Override
	public boolean isAllowedAlwaysPaint() {
		HttpServletRequest currentHttpRequest = this.getCurrentHttpRequest();
		
		if (currentHttpRequest != null) {
			String isFromOIC = (String) currentHttpRequest.getParameter("fromOIC");
			
			if (StringUtils.isNotBlank(isFromOIC) && StringUtils.equals(isFromOIC, "true")) {
				return anyUpdatedData();
			}
		}
		
		return true;
	}
	
	public boolean anyUpdatedData() {
		try {
			Date now = new Date();
			now = DateUtils.truncate(now, Calendar.DAY_OF_MONTH);
			Date queryDate = DateUtils.addDays(now, -2);
			String operationUid = null;
			
			if (this.getCurrentUserContext() != null) {
				operationUid = this.getCurrentUserContext().getUserSelection().getOperationUid();
				this.currentOperationPlanMasterUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
			}
			
			if (StringUtils.isNotBlank(operationUid)) {
				String[] paramNames = {"operationUid", "queryDate"};
				Object[] paramValues = {operationUid, queryDate};
				
				// check if activity has any updated data
				List resultList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Activity where operationUid =:operationUid and lastEditDatetime >:queryDate", paramNames, paramValues);
				if (resultList != null && resultList.size() > 0) return true;
				
				// check if operation plan phase has any updated data
				resultList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanPhase where operationUid =:operationUid and lastEditDatetime >:queryDate", paramNames, paramValues);
				if (resultList != null && resultList.size() > 0) return true;
				
				// check if operation plan task has any updated data
				resultList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanTask where operationUid =:operationUid and lastEditDatetime >:queryDate", paramNames, paramValues);
				if (resultList != null && resultList.size() > 0) return true;
			}
		} catch(Exception e) {}
		return false;
	}
	
	/**
	 * Main method that must be implemented in order to paint graph.
	 * @param request A HttpServletRequest object from BaseGraphController. Use to get any parameter from HTTP.
	 * @param response A HttpServletResponse object from BaseGraphController. Basically use for outputstream purpose.
	 */
	@Override
	public JFreeChart paint() throws Exception {
		String groupUid = null;
		Locale locale = null;
		Boolean isShowLegend = true;
		this.queryProperties.setUomConversionEnabled(false);
		this.queryProperties.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		operationUomContext = new OperationUomContext(true, true);
		operationUomContext.setOperationUid(this.getCurrentUserContext().getUserSelection().getOperationUid());
        durationConverter = this.getDurationConverter();
		
		if (this.getCurrentHttpRequest() != null) {
			UserSession session = UserSession.getInstance(this.getCurrentHttpRequest());
			groupUid = session.getCurrentGroupUid();
			locale = session.getUserLocale();
			
			String StrLegend = (String) this.getCurrentHttpRequest().getParameter("showLegend");
			
			if (StringUtils.isNotBlank(StrLegend) && StringUtils.equals(StrLegend, "false")) {
				isShowLegend = false;
			}
		
		} else {
			groupUid = this.getCurrentUserContext().getUserSelection().getGroupUid();
			locale = this.getCurrentUserContext().getUserSelection().getLocale();
		}
		this.formatter = (DecimalFormat) DecimalFormat.getInstance(locale);
		
        List OperationPlanSettingList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanSetting where (isDeleted = false or isDeleted is null) and groupUid =:groupUid", "groupUid", groupUid);
		if (OperationPlanSettingList.size() > 0)
        	this.operationPlanSetting = (OperationPlanSetting) OperationPlanSettingList.get(0);
        
		if(this.getCurrentUserContext() != null) {
			String operationUid = this.getCurrentUserContext().getUserSelection().getOperationUid();
			this.currentOperationPlanMasterUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
			
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
			
			String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, operationUid);
			
			if(StringUtils.isNotBlank(operationName)) {
				if (this.operationPlanSetting != null)
					if (StringUtils.isNotBlank(this.operationPlanSetting.getDvdTitle()))
						setTitle(this.operationPlanSetting.getDvdTitle());
					else setTitle(operationName);
				else setTitle(operationName);
			}
			
			//this.setFileName(operationUid + "_" + "dvd.jpg");
			if (this.showActualDataToDate) {
				String fileName = this.getFileName();
				String dailyUid = this.getCurrentUserContext().getUserSelection().getDailyUid();
				if (dailyUid!=null) {
					fileName = fileName.replaceAll(operationUid + "_", "");
					if (!(fileName.indexOf(dailyUid + "_")>=0)) {
						fileName = dailyUid + "_" + fileName;
						this.setFileName(fileName);
					}
				}
			}
			
			this.operationPlanPhaseLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select operationUid, p10Duration, p50Duration, p90Duration, tlDuration, pmeanDuration, depthMdMsl, phaseBudget, operationPlanPhaseUid, p10Cost, p90Cost from OperationPlanPhase where (isDeleted = false or isDeleted is null) and operationPlanMasterUid = :operationPlanMasterUid order by sequence,phaseSequence", "operationPlanMasterUid", this.currentOperationPlanMasterUid, this.queryProperties);
			this.dailyLists 			 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and reportType=:reportType order by reportDatetime", new String[] {"operationUid", "reportType"}, new Object[] {operationUid, reportType}, this.queryProperties);
		}
		
		// start drawing ...
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		operationUomContext.setOperationUid(this.getCurrentUserContext().getUserSelection().getOperationUid());
		CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(),Activity.class, "depthMdMsl",operationUomContext);
		
		String depth = "Depth (" + thisConverter.getUomSymbol() + ")";
		
		XYDataset dataset = createActualDayDepthDataset(this.getCurrentUserContext().getUserSelection().getOperationUid(), null, null);
		JFreeChart chart = ChartFactory.createXYLineChart(
			getTitle(),      			// chart title
            "Days (" + durationConverter.getUomSymbol() + ")",                  // x axis label
            depth,                    	// y axis label
            dataset,                  	// data
            PlotOrientation.VERTICAL,
            isShowLegend,               // include legend
            true,                     	// tooltips
            false                     	// urls
		);
		
		// Operation Plan Setting
        Boolean dvdHideP10 = false;
        Boolean dvdHideP50 = false;
        Boolean dvdHideP90 = false;
        Boolean dvdHideTL = true;
        Boolean dvdHidePCost = false;
        Boolean dvdHideActualDepth = false;
        Boolean dvdHideProductiveDepth = true;
        Boolean dvdHideActualCost = false;
        Boolean dvdHidePmean = false;
        
        if (this.operationPlanSetting!=null)
        {
        	dvdHideP10 = this.operationPlanSetting.getHideP10Curve();
        	dvdHideP50 = this.operationPlanSetting.getHideP50Curve();
        	dvdHideP90 = this.operationPlanSetting.getHideP90Curve();
        	dvdHideTL = this.operationPlanSetting.getHideTechnicalLimit();
        	if (BooleanUtils.isNotFalse(dvdHideTL)) dvdHideTL = true;	
        	dvdHidePCost = this.operationPlanSetting.getHidePlannedCost();
        	dvdHidePmean = this.operationPlanSetting.getHidePmeanCurve();
        	
        	dvdHideActualDepth = this.operationPlanSetting.getHideActualDayDepth();
        	dvdHideProductiveDepth = this.operationPlanSetting.getHideProdTimeDepth();
        	dvdHideActualCost = this.operationPlanSetting.getHideActualCost();
        }
        
		// main settings
        XYPlot plot = chart.getXYPlot();
        plot.setOrientation(PlotOrientation.VERTICAL);
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.lightGray);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.getRangeAxis().setFixedDimension(15.0);
        XYItemRenderer renderer = plot.getRenderer();
        if (this.operationPlanSetting!=null)
        {
        	if (StringUtils.isNotBlank(this.operationPlanSetting.getActualDayDepthLineColor())){
        		renderer.setSeriesPaint(0, this.colors[(int) Double.parseDouble(this.operationPlanSetting.getActualDayDepthLineColor())]);
        	}else renderer.setSeriesPaint(0, Color.BLACK); //actual day/depth - solid black
        	
        	if (StringUtils.isNotBlank(this.operationPlanSetting.getActualDayDepthLineStyle())){
        		renderer.setSeriesStroke(0, this.BStroke[(int) Double.parseDouble(this.operationPlanSetting.getActualDayDepthLineStyle())]);
	       	}
        }else renderer.setSeriesPaint(0, Color.BLACK); //actual day/depth - solid black
        
        if (this.showProductiveTimeDepth && BooleanUtils.isNotTrue(dvdHideProductiveDepth)){
        	if (this.operationPlanSetting!=null)
            {
            	if (StringUtils.isNotBlank(this.operationPlanSetting.getProdTimeDepthLineColor())){
            		renderer.setSeriesPaint(1, this.colors[(int) Double.parseDouble(this.operationPlanSetting.getProdTimeDepthLineColor())]);
            	}else renderer.setSeriesPaint(1, new Color(51, 102, 0)); // Productive Time/Depth -- Solid -- #336600
            	
            	if (StringUtils.isNotBlank(this.operationPlanSetting.getProdTimeDepthLineStyle())){
            		renderer.setSeriesStroke(1, this.BStroke[(int) Double.parseDouble(this.operationPlanSetting.getProdTimeDepthLineStyle())]);
    	       	}
            }else renderer.setSeriesPaint(1, new Color(51, 102, 0)); // Productive Time/Depth -- Solid -- #336600
        }
        
        // Create all p10, p50, p90 and pmean curve lines. 
        this.createPlannedCostAndDepthDataset(groupUid);
        
        if(this.dataAvailabilityMap.size() > 0){
	        // planned P10 Curve settings
	        if (BooleanUtils.isNotTrue(dvdHideP10) && (Boolean) this.dataAvailabilityMap.get("p10")) {
		        XYDataset plannedP10Dataset = (XYDataset) this.xyDatasetMap.get("p10");
		        plot.setDataset(PLANNED_10_DAY_DEPTH, plannedP10Dataset);
		        plot.mapDatasetToRangeAxis(PLANNED_10_DAY_DEPTH, ACTUAL_DAY_DEPTH);
		        XYItemRenderer plannedP10Renderer = new StandardXYItemRenderer();
		        if (this.operationPlanSetting!=null){
		        	if (StringUtils.isNotBlank(this.operationPlanSetting.getP10CurveLineColor())){
			        	plannedP10Renderer.setSeriesPaint(0, this.colors[(int) Double.parseDouble(this.operationPlanSetting.getP10CurveLineColor())]);
		        	}else{
		        		plannedP10Renderer.setSeriesPaint(0, new Color(153, 153, 204)); //Planned/P10 Curve -- Dashes -- #9999cc
		        	}
			       	if (StringUtils.isNotBlank(this.operationPlanSetting.getP10CurveLineStyle())){
						plannedP10Renderer.setSeriesStroke(0, this.BStroke[(int) Double.parseDouble(this.operationPlanSetting.getP10CurveLineStyle())]);
			       	}else plannedP10Renderer.setSeriesStroke(0, dashesStroke);
		        }else{
		        	plannedP10Renderer.setSeriesPaint(0, new Color(153, 153, 204)); //Planned/P10 Curve -- Dashes -- #9999cc
		        	plannedP10Renderer.setSeriesStroke(0, dashesStroke);
		        }
		        plot.setRenderer(PLANNED_10_DAY_DEPTH, plannedP10Renderer);
	        }
	        
	        // planned P50 Curve settings
	        if (BooleanUtils.isNotTrue(dvdHideP50) && (Boolean) this.dataAvailabilityMap.get("p50")) {
	        	XYDataset plannedP50Dataset = (XYDataset) this.xyDatasetMap.get("p50");
	        	plot.setDataset(PLANNED_50_DAY_DEPTH, plannedP50Dataset);
	        	plot.mapDatasetToRangeAxis(PLANNED_50_DAY_DEPTH, ACTUAL_DAY_DEPTH);
		        XYItemRenderer plannedP50Renderer = new StandardXYItemRenderer();
		        if (this.operationPlanSetting!=null){
		        	if (StringUtils.isNotBlank(this.operationPlanSetting.getP50CurveLineColor())){
		        		plannedP50Renderer.setSeriesPaint(0, this.colors[(int) Double.parseDouble(this.operationPlanSetting.getP50CurveLineColor())]);
		        	}else{
		        		plannedP50Renderer.setSeriesPaint(0, Color.BLUE); // Planned/P50 Curve -- Dashes -- Dark Blue
		        	}
			       	if (StringUtils.isNotBlank(this.operationPlanSetting.getP50CurveLineStyle())){
			       		plannedP50Renderer.setSeriesStroke(0, this.BStroke[(int) Double.parseDouble(this.operationPlanSetting.getP50CurveLineStyle())]);
			       	}else plannedP50Renderer.setSeriesStroke(0, dashesStroke);
		        }else{
		        	plannedP50Renderer.setSeriesPaint(0, Color.BLUE); // Planned/P50 Curve -- Dashes -- Dark Blue
			        plannedP50Renderer.setSeriesStroke(0, dashesStroke);
		        }
		        plot.setRenderer(PLANNED_50_DAY_DEPTH, plannedP50Renderer);
	        }
	        
	        // planned P90 Curve settings
	        if (BooleanUtils.isNotTrue(dvdHideP90) && (Boolean) this.dataAvailabilityMap.get("p90")) {
	        	XYDataset plannedP90Dataset = (XYDataset) this.xyDatasetMap.get("p90");
	        	plot.setDataset(PLANNED_90_DAY_DEPTH, plannedP90Dataset);
	        	plot.mapDatasetToRangeAxis(PLANNED_90_DAY_DEPTH, ACTUAL_DAY_DEPTH);
		        XYItemRenderer plannedP90Renderer = new StandardXYItemRenderer();
		        if (this.operationPlanSetting!=null){
		        	if (StringUtils.isNotBlank(this.operationPlanSetting.getP90CurveLineColor())){
		        		plannedP90Renderer.setSeriesPaint(0, this.colors[(int) Double.parseDouble(this.operationPlanSetting.getP90CurveLineColor())]);
		        	}else{
		        		plannedP90Renderer.setSeriesPaint(0, Color.YELLOW); // Planned/P90 Curve -- Dashes -- Yellow
		        	}
			       	if (StringUtils.isNotBlank(this.operationPlanSetting.getP90CurveLineStyle())){
			       		plannedP90Renderer.setSeriesStroke(0, this.BStroke[(int) Double.parseDouble(this.operationPlanSetting.getP90CurveLineStyle())]);
			       	}else plannedP90Renderer.setSeriesStroke(0, dashesStroke);
		        }else{
		        	plannedP90Renderer.setSeriesPaint(0, Color.YELLOW); // Planned/P90 Curve -- Dashes -- Yellow
			        plannedP90Renderer.setSeriesStroke(0, dashesStroke);
		        }
		        plot.setRenderer(PLANNED_90_DAY_DEPTH, plannedP90Renderer);
	        }
	        
	        // planned Pmean Curve settings
	        if (BooleanUtils.isNotTrue(dvdHidePmean) && (Boolean) this.dataAvailabilityMap.get("pmean")) {
	        	XYDataset plannedPmeanDataset = (XYDataset) this.xyDatasetMap.get("pmean");
	        	plot.setDataset(PLANNED_MEAN_DAY_DEPTH, plannedPmeanDataset);
	        	plot.mapDatasetToRangeAxis(PLANNED_MEAN_DAY_DEPTH, ACTUAL_DAY_DEPTH);
		        XYItemRenderer plannedPmeanRenderer = new StandardXYItemRenderer();
		        if (this.operationPlanSetting!=null){
		        	if (StringUtils.isNotBlank(this.operationPlanSetting.getPmeanCurveLineColor())){
		        		plannedPmeanRenderer.setSeriesPaint(0, this.colors[(int) Double.parseDouble(this.operationPlanSetting.getPmeanCurveLineColor())]);
		        	}else{
		        		plannedPmeanRenderer.setSeriesPaint(0, TEAL); // Planned/Pmean Curve -- Dashes -- Teal
		        	}
			       	if (StringUtils.isNotBlank(this.operationPlanSetting.getPmeanCurveLineStyle())){
			       		plannedPmeanRenderer.setSeriesStroke(0, this.BStroke[(int) Double.parseDouble(this.operationPlanSetting.getPmeanCurveLineStyle())]);
			       	}else plannedPmeanRenderer.setSeriesStroke(0, dashesStroke);
		        }else{
		        	plannedPmeanRenderer.setSeriesPaint(0, TEAL); // Planned/Pmean Curve -- Dashes -- Teal
			        plannedPmeanRenderer.setSeriesStroke(0, dashesStroke);
		        }
		        plot.setRenderer(PLANNED_MEAN_DAY_DEPTH, plannedPmeanRenderer);
	        }
        }
        
        // Technical Limit Curve settings
        if (BooleanUtils.isNotTrue(dvdHideTL)) {
        	XYDataset TLDataset = (XYDataset) this.xyDatasetMap.get("TL");
        	plot.setDataset(PLANNED_TL, TLDataset);
        	plot.mapDatasetToRangeAxis(PLANNED_TL, ACTUAL_DAY_DEPTH);
	        XYItemRenderer TLRenderer = new StandardXYItemRenderer();
	        if (this.operationPlanSetting!=null){
	        	if (StringUtils.isNotBlank(this.operationPlanSetting.getTechnicalLimitLineColor())){
	        		TLRenderer.setSeriesPaint(0, this.colors[(int) Double.parseDouble(this.operationPlanSetting.getTechnicalLimitLineColor())]);
	        	}else{
	        		TLRenderer.setSeriesPaint(0, new Color(204, 102, 0)); // Technical Limit Curve -- Dashes -- #CC6600
	        	}
		       	if (StringUtils.isNotBlank(this.operationPlanSetting.getTechnicalLimitLineStyle())){
		       		TLRenderer.setSeriesStroke(0, this.BStroke[(int) Double.parseDouble(this.operationPlanSetting.getTechnicalLimitLineStyle())]);
		       	}else TLRenderer.setSeriesStroke(0, dashesStroke);
	        }else{
	        	TLRenderer.setSeriesPaint(0, new Color(204, 102, 0)); // Technical Limit Curve -- Dashes -- #CC6600
		        TLRenderer.setSeriesStroke(0, dashesStroke);
	        }
	        plot.setRenderer(PLANNED_TL, TLRenderer);
        }
        
        // planned Cost settings
        if (this.showPlannedCost && BooleanUtils.isNotTrue(dvdHidePCost)) {
	        XYDataset plannedCostDataset = this.xyDatasetMap.get("pCost");
	        plot.setDataset(PLANNED_COST, plannedCostDataset);
	        plot.mapDatasetToRangeAxis(PLANNED_COST, ACTUAL_COST);
	        XYItemRenderer plannedCostRenderer = new StandardXYItemRenderer();
	        if (this.operationPlanSetting!=null){
	        	if (StringUtils.isNotBlank(this.operationPlanSetting.getPlannedCostLineColor())){
	        		plannedCostRenderer.setSeriesPaint(0, this.colors[(int) Double.parseDouble(this.operationPlanSetting.getPlannedCostLineColor())]);
	        	}else{
	        		plannedCostRenderer.setSeriesPaint(0, new Color(51, 153, 153)); //Planned Cost Curve -- Long Dashes Dot -- #339999 
	        	}
		       	if (StringUtils.isNotBlank(this.operationPlanSetting.getPlannedCostLineStyle())){
		       		plannedCostRenderer.setSeriesStroke(0, this.BStroke[(int) Double.parseDouble(this.operationPlanSetting.getPlannedCostLineStyle())]);
		       	}else plannedCostRenderer.setSeriesStroke(0, dashesStroke);
	        }else{
		        plannedCostRenderer.setSeriesPaint(0, new Color(51, 153, 153)); //Planned Cost Curve -- Long Dashes Dot -- #339999 
		        plannedCostRenderer.setSeriesStroke(0, longDashesDotStroke);
	        }
	        plot.setRenderer(PLANNED_COST, plannedCostRenderer);
        }
        
        // Right Axis settings
        if ((this.showPlannedCost && BooleanUtils.isNotTrue(dvdHidePCost)) || (this.showActualCost && BooleanUtils.isNotTrue(dvdHideActualCost))) {
	        CustomFieldUom costConverter = new CustomFieldUom(this.getLocale(),ReportDaily.class, "daycost",operationUomContext);
	        NumberAxis costaxis = new NumberAxis("Cost (" + costConverter.getUomSymbol()+ ")");
	        costaxis.setNumberFormatOverride(formatter);
	        costaxis.setLabelPaint(Color.red);
	        costaxis.setTickLabelPaint(Color.red);
	        plot.setRangeAxis(ACTUAL_COST, costaxis);
        }
        if (this.showActualCost && BooleanUtils.isNotTrue(dvdHideActualCost)) {
	        XYDataset actualCostDataset = this.createActualCostDataset(this.getCurrentUserContext().getUserSelection().getOperationUid(), null);
	        plot.setDataset(ACTUAL_COST, actualCostDataset);
	        plot.mapDatasetToRangeAxis(ACTUAL_COST, ACTUAL_COST);
	        XYItemRenderer costRenderer = new StandardXYItemRenderer();
	        if (this.operationPlanSetting!=null){
	        	if (StringUtils.isNotBlank(this.operationPlanSetting.getActualCostLineColor())){
	        		costRenderer.setSeriesPaint(0, this.colors[(int) Double.parseDouble(this.operationPlanSetting.getActualCostLineColor())]);
	        	}else{
	        		costRenderer.setSeriesPaint(0, new Color(153, 0, 0)); //Actual Cost -- Solid -- #990000
	        	}
		       	if (StringUtils.isNotBlank(this.operationPlanSetting.getActualCostLineStyle())){
		       		costRenderer.setSeriesStroke(0, this.BStroke[(int) Double.parseDouble(this.operationPlanSetting.getActualCostLineStyle())]);
		       	}else costRenderer.setSeriesStroke(0, solidStroke);
	        }else{
	        	costRenderer.setSeriesPaint(0, new Color(153, 0, 0)); //Actual Cost -- Solid -- #990000 
		        costRenderer.setSeriesStroke(0, solidStroke);
	        }
	        plot.setRenderer(ACTUAL_COST, costRenderer);
        }
        if (this.showP10P90Cost) {
	        XYDataset p10CostDataset = this.createP10CostDataset(this.getCurrentUserContext().getUserSelection().getOperationUid(), null);
	        plot.setDataset(PLANNED_10_Cost, p10CostDataset);
	        plot.mapDatasetToRangeAxis(PLANNED_10_Cost, ACTUAL_COST);
	        XYItemRenderer p10costRenderer = new StandardXYItemRenderer();
	        p10costRenderer.setSeriesPaint(0, new Color(0, 153, 0));
	        plot.setRenderer(PLANNED_10_Cost, p10costRenderer);
        }
        if (this.showP10P90Cost) {
	        XYDataset p90CostDataset = this.createP90CostDataset(this.getCurrentUserContext().getUserSelection().getOperationUid(), null);
	        plot.setDataset(PLANNED_90_Cost, p90CostDataset);
	        plot.mapDatasetToRangeAxis(PLANNED_90_Cost, ACTUAL_COST);
	        XYItemRenderer p90costRenderer = new StandardXYItemRenderer();
	        p90costRenderer.setSeriesPaint(0, new Color(0, 0, 153));
	        plot.setRenderer(PLANNED_90_Cost, p90costRenderer);
        }
        
        if (this.showAnnotation) {
        	this.loadAnnotationList();
        	
        	if (this.annotationLists!=null) {
        		for (Object x : this.annotationLists) {
                	Double offY = this.maxDepth * 0.02;
                	Double offX = this.maxDuration * 0.01;
        			Object[] rec = (Object[]) x; 
        			String annotationReferenceNumber = rec[4].toString();
        			if (StringUtils.isNotBlank(annotationReferenceNumber)){
	        			if (annotationReferenceNumber.length()>30) {
	    					int fromIndex = 15;
	    					if (annotationReferenceNumber.indexOf(" ", fromIndex) > 30) {
	    						fromIndex = annotationReferenceNumber.indexOf(" ", fromIndex);
	    					} else {
	    						while (fromIndex >= 0 && annotationReferenceNumber.indexOf(" ", fromIndex + 1)<30) {
	    							fromIndex = annotationReferenceNumber.indexOf(" ", fromIndex + 1);
	    						}
	    					}
	    					
	    					if (fromIndex > annotationReferenceNumber.length()) fromIndex = annotationReferenceNumber.length();
	    					if (fromIndex >0) {
		    					if (!annotationReferenceNumber.equals(annotationReferenceNumber.substring(0, fromIndex))) {
		    						annotationReferenceNumber = annotationReferenceNumber.substring(0, fromIndex) + "...";
		    					}
	    					}
	        			}
	        			String[] arrAnnotationReferenceNumber = annotationReferenceNumber.split("\n");
	        			
	        			XYTextAnnotation annotation = new XYTextAnnotation("",0,0);
	        			JPanel j = new JPanel(); //create fake graphic to get FontMetrics
	        			FontMetrics metrics = j.getFontMetrics(annotation.getFont());
	        			Integer length = 0;
	        			for (String singleString: arrAnnotationReferenceNumber) {
	        				if (metrics.stringWidth(singleString)> length) {
	        					length = metrics.stringWidth(singleString); 
	        				}
	        			}
	        			
	        			Double rowHeight = 0.035;
	        			offY = offY + ((arrAnnotationReferenceNumber.length - 1) * rowHeight * this.maxDepth);
	        			if ((Double) rec[1] < offY + (rowHeight * this.maxDepth)) offY = (Double) rec[1] - (rowHeight * this.maxDepth);
	        			
	        			for (String singleString: arrAnnotationReferenceNumber) {
	        				
	        				for (;metrics.stringWidth(singleString)<length;) {
	        					singleString += " ";
	        				}
	        				
		        			annotation = new XYTextAnnotation(singleString, ((Double) rec[0]) + offX, ((Double) rec[1]) - offY);
		        			annotation.setTextAnchor(TextAnchor.BOTTOM_LEFT);
		        			String commentType = (String) rec[2];
		        			if (commentType==null) commentType = "";
		        			annotation.setOutlineVisible(true);
		        			annotation.setOutlineVisible(false);
		        			
		        			if (commentType.equals("Alert")) {
		        				annotation.setBackgroundPaint(Color.RED);
		        			} else if (commentType.equals("Attention")) {
		        				annotation.setBackgroundPaint(Color.YELLOW);
		        			} else {
		        				annotation.setBackgroundPaint(Color.LIGHT_GRAY);
		        			}
		        			plot.addAnnotation(annotation);
		        			offY = offY - (rowHeight * this.maxDepth); 
	        			}
        			}
        		}
        	}
        }
        if (isTitleEnabled()) {
        	if (this.operationPlanSetting!=null)
        		if (StringUtils.isNotBlank(this.operationPlanSetting.getDvdSubtitle()))
        			chart.addSubtitle(new TextTitle(this.operationPlanSetting.getDvdSubtitle()));
        		else chart.addSubtitle(new TextTitle("Progress Vs. Planned"));
        	else chart.addSubtitle(new TextTitle("Progress Vs. Planned"));
		}
        chart.setBackgroundPaint(Color.lightGray);
        chart.getXYPlot().getRangeAxis(ACTUAL_DAY_DEPTH).setInverted(true);
        ((NumberAxis) chart.getXYPlot().getRangeAxis()).setNumberFormatOverride(formatter);
        ((NumberAxis) chart.getXYPlot().getDomainAxis()).setNumberFormatOverride(formatter);
        
        if (this.operationPlanSetting!=null){
        	if (BooleanUtils.isFalse((this.operationPlanSetting.getStartDepthIsZero()))){
        		((NumberAxis) chart.getXYPlot().getRangeAxis(ACTUAL_DAY_DEPTH)).setAutoRangeIncludesZero(false);
        	}
        }
        
        return chart;
	}
	
	/**
	 * Create a dataset for JFreeChart to draw a Planned Phase - P10 Curve line
	 * @return XYDataset
	 * @throws Exception
	 */
	private void createPlannedCostAndDepthDataset(String groupUid) throws Exception {
		XYSeries plannedP10CurveSeries = new XYSeries("Planned/P10 Curve");
		XYSeries plannedP50CurveSeries = new XYSeries("Planned/P50 Curve");
		XYSeries plannedP90CurveSeries = new XYSeries("Planned/P90 Curve");
		XYSeries TLCurveSeries = new XYSeries("Technical Limit Curve");
		XYSeries plannedCostCurveSeries = new XYSeries("Planned Cost Curve");
		XYSeries plannedPmeanCurveSeries = new XYSeries("Planned/PMean Curve");
		
		Boolean dvdHideP10 = false;
		Boolean dvdHideP50 = false;
		Boolean dvdHideP90 = false;
		Boolean dvdHideTL = true;
		Boolean dvdHidePmean = false;
		
		if (this.operationPlanSetting != null){
			if (StringUtils.isNotBlank(this.operationPlanSetting.getP10CurveLabel())) plannedP10CurveSeries.setKey(this.operationPlanSetting.getP10CurveLabel());
			if (StringUtils.isNotBlank(this.operationPlanSetting.getP50CurveLabel())) plannedP50CurveSeries.setKey(this.operationPlanSetting.getP50CurveLabel());
			if (StringUtils.isNotBlank(this.operationPlanSetting.getP90CurveLabel())) plannedP90CurveSeries.setKey(this.operationPlanSetting.getP90CurveLabel());
			if (StringUtils.isNotBlank(this.operationPlanSetting.getTechnicalLimitLabel())) TLCurveSeries.setKey(this.operationPlanSetting.getTechnicalLimitLabel());
			if (StringUtils.isNotBlank(this.operationPlanSetting.getPlannedCostLabel())) plannedCostCurveSeries.setKey(this.operationPlanSetting.getPlannedCostLabel());
			if (StringUtils.isNotBlank(this.operationPlanSetting.getPmeanCurveLabel())) plannedPmeanCurveSeries.setKey(this.operationPlanSetting.getPmeanCurveLabel());
			
			dvdHideP10 = this.operationPlanSetting.getHideP10Curve();
			dvdHideP50 = this.operationPlanSetting.getHideP50Curve();
			dvdHideP90 = this.operationPlanSetting.getHideP90Curve();
			dvdHideTL = this.operationPlanSetting.getHideTechnicalLimit();
			dvdHidePmean = this.operationPlanSetting.getHidePmeanCurve();
		}
        
		Boolean p10IsNull = true;
		Boolean p50IsNull = true;
		Boolean p90IsNull = true;
		Boolean pmeanIsNull = true;

        operationUomContext.setOperationUid(this.getCurrentUserContext().getUserSelection().getOperationUid());
        CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(), Activity.class, "depthMdMsl", operationUomContext);
		this.durationConverter = this.getDurationConverter();

		String wellboreUid = null;
		
		if (this.getCurrentUserContext() != null) {
			wellboreUid = this.getCurrentUserContext().getUserSelection().getWellboreUid();
		}
		
		String parentOperationUid = null;
		String dvdUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(this.getCurrentUserContext().getUserSelection().getOperationUid());
		if (dvdUid!=null) {
			OperationPlanMaster dvd = (OperationPlanMaster) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanMaster.class, dvdUid);
			parentOperationUid = dvd.getDvdParentOperationUid();
		}
		
		if (StringUtils.isNotBlank(wellboreUid)) {
			Wellbore wellbore 	= (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, wellboreUid);
			
			String sql = "SELECT kickoffMdMsl FROM Wellbore WHERE (isDeleted = false or isDeleted is null) AND wellboreUid=:thisWellboreUid";
			List thisWellbore  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "thisWellboreUid", wellboreUid, this.queryProperties);
			
			Double kickOffMd = 0.0;
			Double wellboreKickOffMd = null;
			if(thisWellbore != null && thisWellbore.size() > 0) {
				if(thisWellbore.get(0) != null) wellboreKickOffMd = (Double) thisWellbore.get(0);
			}
			
			if (StringUtils.isBlank(wellbore.getParentWellboreUid())) {
				plannedCostCurveSeries.add(0.0, 0.0);
				plannedP10CurveSeries.add(0.0, 0.0);
				plannedP50CurveSeries.add(0.0, 0.0);
				plannedP90CurveSeries.add(0.0, 0.0);
				TLCurveSeries.add(0.0, 0.0);
				plannedPmeanCurveSeries.add(0.0, 0.0);
			}else {
				
				if(wellboreKickOffMd != null) {
					thisConverter.setBaseValue(wellboreKickOffMd);
					kickOffMd = thisConverter.getConvertedValue(true);
				}
				if (StringUtils.isNotBlank(parentOperationUid)) {
					plannedCostCurveSeries.add(0.0, 0.0);
					plannedP10CurveSeries.add(0.0, 0.0);
					plannedP50CurveSeries.add(0.0, 0.0);
					plannedP90CurveSeries.add(0.0, 0.0);
					TLCurveSeries.add(0.0, 0.0);
					plannedPmeanCurveSeries.add(0.0, 0.0);
				} else {
					plannedCostCurveSeries.add(0.0, kickOffMd);
					plannedP10CurveSeries.add(0.0, kickOffMd);
					plannedP50CurveSeries.add(0.0, kickOffMd);
					plannedP90CurveSeries.add(0.0, kickOffMd);
					TLCurveSeries.add(0.0, kickOffMd);
					plannedPmeanCurveSeries.add(0.0, kickOffMd);
				}
				
			}
			
			Double cumCost = 0.0;
			Double cumP10Duration = 0.0;
			Double cumP50Duration = 0.0;
			Double cumP90Duration = 0.0;
			Double cumTLDuration = 0.0;
			Double cumPmeanDuration = 0.0;
			
			if(this.operationPlanPhaseLists != null && operationPlanPhaseLists.size() > 0) {
				for (Object[] recp : this.operationPlanPhaseLists) {
					Double p10Duration  = 0.0;
					Double p50Duration  = 0.0;
					Double p90Duration  = 0.0;
					Double TLDuration  = 0.0;
					Double pmeanDuration = 0.0;
					Double depthMd 		= 0.0;
					Double cost			= 0.0;
					String OperationPlanPhaseUid = recp[8].toString();
					
					if(OperationPlanPhaseUid != null) {
						String query 				 = "Select operationUid, p10Duration, p50Duration, p90Duration, tlDuration, depthMdMsl, taskBudget " + 
													    "from OperationPlanTask where (isDeleted = false or isDeleted is null) and operationPlanPhaseUid = :operationPlanPhaseUid";
						List<Object[]> operationPlanTaskLists  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, "operationPlanPhaseUid", OperationPlanPhaseUid, this.queryProperties);
						// if plan task is found, then ignore the plan phase

						if(operationPlanTaskLists != null && operationPlanTaskLists.size() > 0) {
							for (Object[] rec : operationPlanTaskLists) {
								Double Depth = null;
								Double Budget = null;
								if (rec[1]!=null)
								{
									p10Duration = Double.parseDouble(rec[1].toString());
									if (p10IsNull) p10IsNull = false;
								}
								if (rec[2]!=null)
								{
									p50Duration = Double.parseDouble(rec[2].toString());
									if (p50IsNull) p50IsNull = false;
								}
								if (rec[3]!=null)
								{
									p90Duration = Double.parseDouble(rec[3].toString());
									if (p90IsNull) p90IsNull = false;
								}
								if (rec[4]!=null)
								{
									TLDuration = Double.parseDouble(rec[4].toString());
								}
								if (rec[5]!=null)
								{
									Depth = Double.parseDouble(rec[5].toString());
								}
								if (rec[6]!=null)
								{
									Budget = Double.parseDouble(rec[6].toString());
								}
								
								if (Depth!=null){
									thisConverter.setBaseValue(Depth);
									depthMd = thisConverter.getConvertedValue();
								}
								if (Budget!= null) {
									cost = Budget;
								}
								
								cumCost += cost;
								
								if(p10Duration != null && depthMd != null) {
									durationConverter.setBaseValue(p10Duration);
									p10Duration = durationConverter.getConvertedValue();
									cumP10Duration += p10Duration;
									plannedP10CurveSeries.add(cumP10Duration, depthMd);
									if (BooleanUtils.isNotTrue(dvdHideP10)) {
										if (depthMd > this.maxDepth) this.maxDepth = depthMd;
										if (cumP10Duration > this.maxDuration) this.maxDuration = cumP10Duration;
									}
								}
								if(p50Duration != null) {
									durationConverter.setBaseValue(p50Duration);
									p50Duration = durationConverter.getConvertedValue();
									cumP50Duration += p50Duration;
									
									if(depthMd != null) {
										plannedP50CurveSeries.add(cumP50Duration, depthMd);
										if (BooleanUtils.isNotTrue(dvdHideP50)) {
											if (depthMd > this.maxDepth) this.maxDepth = depthMd;
											if (cumP50Duration > this.maxDuration) this.maxDuration = cumP50Duration;
										}
									}
									
									if(cumCost != null) {
										plannedCostCurveSeries.add(cumP50Duration, cumCost);
									}
								}

								if(p90Duration != null && depthMd != null) {
									durationConverter.setBaseValue(p90Duration);
									p90Duration = durationConverter.getConvertedValue();
									cumP90Duration += p90Duration;
									
									plannedP90CurveSeries.add(cumP90Duration, depthMd);
									if (BooleanUtils.isNotTrue(dvdHideP90)) {
										if (depthMd > this.maxDepth) this.maxDepth = depthMd;
										if (cumP90Duration > this.maxDuration) this.maxDuration = cumP90Duration;
									}
								}
								
								if(TLDuration != null && depthMd != null) {
									durationConverter.setBaseValue(TLDuration);
									TLDuration = durationConverter.getConvertedValue();
									cumTLDuration += TLDuration;
									
									TLCurveSeries.add(cumTLDuration, depthMd);
									if (BooleanUtils.isNotTrue(dvdHideTL)) {
										if (depthMd > this.maxDepth) this.maxDepth = depthMd;
										if (cumTLDuration > this.maxDuration) this.maxDuration = cumTLDuration;
									}
								}
							}
						} else {
							p10Duration = null;
							p50Duration = null;
							p90Duration = null;
							TLDuration	= null;
							pmeanDuration = null;
							Double Depth = null;
							Double Budget = null;
							if (recp[1]!= null){
								p10Duration = Double.parseDouble(recp[1].toString());
								if (p10IsNull) p10IsNull = false;
							}
							if (recp[2]!= null){
								p50Duration = Double.parseDouble(recp[2].toString());
								if (p50IsNull) p50IsNull = false;
							}
							if (recp[3]!= null){
								p90Duration = Double.parseDouble(recp[3].toString());
								if (p90IsNull) p90IsNull = false;
							}
							if (recp[4]!= null){
								TLDuration = Double.parseDouble(recp[4].toString());
							}
							if (recp[5]!= null){
								pmeanDuration = Double.parseDouble(recp[5].toString());
								if (pmeanIsNull) pmeanIsNull = false;
							}
							if (recp[6]!= null){
								Depth = Double.parseDouble(recp[6].toString());
							}
							if (recp[7]!= null){
								Budget = Double.parseDouble(recp[7].toString());
							}

							if (Budget != null) {
								cost = Budget;
							}
							
							if (Depth !=null){
								thisConverter.setBaseValue(Depth);
								depthMd  = thisConverter.getConvertedValue();
							}
					
							cumCost = cumCost + cost;
							
							if(p10Duration != null && depthMd != null) {
								durationConverter.setBaseValue(p10Duration);
								p10Duration = durationConverter.getConvertedValue();
								cumP10Duration += p10Duration;
								
								plannedP10CurveSeries.add(cumP10Duration, depthMd);
								if (BooleanUtils.isNotTrue(dvdHideP10)) {
									if (depthMd > this.maxDepth) this.maxDepth = depthMd;
									if (cumP10Duration > this.maxDuration) this.maxDuration = cumP10Duration;
								}
							}
							if(p50Duration != null) {
								durationConverter.setBaseValue(p50Duration);
								p50Duration = durationConverter.getConvertedValue();
								cumP50Duration += p50Duration;
								
								if(depthMd != null) {
									plannedP50CurveSeries.add(cumP50Duration, depthMd);
									if (BooleanUtils.isNotTrue(dvdHideP50)) {
										if (depthMd > this.maxDepth) this.maxDepth = depthMd;
										if (cumP50Duration > this.maxDuration) this.maxDuration = cumP50Duration;
									}
								}
								
								if(cumCost != null) {
									plannedCostCurveSeries.add(cumP50Duration, cumCost);
								}
							}
							if(p90Duration != null && depthMd != null) {
								durationConverter.setBaseValue(p90Duration);
								p90Duration = durationConverter.getConvertedValue();
								cumP90Duration += p90Duration;
								
								plannedP90CurveSeries.add(cumP90Duration, depthMd);
								if (BooleanUtils.isNotTrue(dvdHideP90)) {
									if (depthMd > this.maxDepth) this.maxDepth = depthMd;
									if (cumP90Duration > this.maxDuration) this.maxDuration = cumP90Duration;
								}
							}
							if(TLDuration != null && depthMd != null) {
								durationConverter.setBaseValue(TLDuration);
								TLDuration = durationConverter.getConvertedValue();
								cumTLDuration += TLDuration;
								
								TLCurveSeries.add(cumTLDuration, depthMd);
								if (BooleanUtils.isNotTrue(dvdHideTL)) {
									if (depthMd > this.maxDepth) this.maxDepth = depthMd;
									if (cumTLDuration > this.maxDuration) this.maxDuration = cumTLDuration;
								}
							}
																				
							if(pmeanDuration != null) {
								durationConverter.setBaseValue(pmeanDuration);
								pmeanDuration = durationConverter.getConvertedValue();
								cumPmeanDuration += pmeanDuration;
								
								if(depthMd != null) {
									plannedPmeanCurveSeries.add(cumPmeanDuration, depthMd);
									if (BooleanUtils.isNotTrue(dvdHidePmean)) {
										if (depthMd > this.maxDepth) this.maxDepth = depthMd;
										if (cumPmeanDuration > this.maxDuration) this.maxDuration = cumPmeanDuration;
									}
								}
								
								if(cumCost != null) {
									plannedCostCurveSeries.add(cumPmeanDuration, cumCost);
								}
							}
						}
						
					}
				}
			}
			
			XYSeriesCollection p10Dataset = new XYSeriesCollection();
			p10Dataset.addSeries(plannedP10CurveSeries);
			
			XYSeriesCollection p50Dataset = new XYSeriesCollection();
			p50Dataset.addSeries(plannedP50CurveSeries);
			
			XYSeriesCollection p90Dataset = new XYSeriesCollection();
			p90Dataset.addSeries(plannedP90CurveSeries);
			
			XYSeriesCollection TLDataset = new XYSeriesCollection();
			TLDataset.addSeries(TLCurveSeries);
			
			XYSeriesCollection pCostDataset = new XYSeriesCollection();
			pCostDataset.addSeries(plannedCostCurveSeries);
			
			XYSeriesCollection pmeanDataset = new XYSeriesCollection();
			pmeanDataset.addSeries(plannedPmeanCurveSeries);
			
			this.xyDatasetMap.put("p10", p10Dataset);
			this.xyDatasetMap.put("p50", p50Dataset);
			this.xyDatasetMap.put("p90", p90Dataset);
			this.xyDatasetMap.put("pCost", pCostDataset);
			this.xyDatasetMap.put("TL", TLDataset);
			this.xyDatasetMap.put("pmean", pmeanDataset);
			
			this.dataAvailabilityMap.put("p10", !p10IsNull);
			this.dataAvailabilityMap.put("p50", !p50IsNull);
			this.dataAvailabilityMap.put("p90", !p90IsNull);
			this.dataAvailabilityMap.put("pmean", !pmeanIsNull);
		}
	}
	
	/**
	 * Create a dataset for JFreeChart to draw a Actual Day Depth line
	 * @return
	 * @throws Exception
	 */
	Double global_duration;
	private XYDataset createActualDayDepthDataset(String operationUid, XYSeries actualSeries, XYSeries productiveSeries) throws Exception {
		if(actualSeries==null){
			actualSeries = new XYSeries("Actual Day/Depth");
			if (this.operationPlanSetting != null)
				if (StringUtils.isNotBlank(this.operationPlanSetting.getActualDayDepthLabel()))
					actualSeries.setKey(this.operationPlanSetting.getActualDayDepthLabel());
		}
		if(productiveSeries==null){
			productiveSeries = new XYSeries("Productive Time/Depth");
			if (this.operationPlanSetting != null)
				if (StringUtils.isNotBlank(this.operationPlanSetting.getProdTimeDepthLabel()))
					productiveSeries.setKey(this.operationPlanSetting.getProdTimeDepthLabel());
		}
		if (global_duration==null) {
			global_duration = 0.0;
		}
		String parentDvdOperationUid=null;
		Boolean isChild = false;
	
		Boolean dvdHideProductiveDepth = true;
		if (this.operationPlanSetting != null){
			dvdHideProductiveDepth = this.operationPlanSetting.getHideProdTimeDepth();
			if (BooleanUtils.isNotFalse(dvdHideProductiveDepth)) dvdHideProductiveDepth = true;	
		}
		
		String dvdUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
		OperationPlanMaster dvd = null;
		if (dvdUid!=null) {
			dvd = (OperationPlanMaster) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanMaster.class, dvdUid);
			
			if (dvd!=null) {
				parentDvdOperationUid = dvd.getDvdParentOperationUid(); 
				if (parentDvdOperationUid!=null && StringUtils.isNotBlank(parentDvdOperationUid)) {
					isChild = true;
					createActualDayDepthDataset(parentDvdOperationUid, actualSeries,  productiveSeries);
				}else{
					
				}
				
			}
			
		}
		this.operationUomContext.setOperationUid(operationUid);
		this.durationConverter = this.getDurationConverter();
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		this.dailyLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid =:operationUid and reportType=:reportType order by reportDatetime", new String[] {"operationUid", "reportType"}, new Object[] {operationUid, reportType}, this.queryProperties);
				
		if(this.dailyLists != null && dailyLists.size() > 0) {
			Double productive_duration = 0.0;
			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(), Activity.class, "depthMdMsl", operationUomContext);
			
			String wellboreUid = null;
			String currentDailyUid = null;
			String thisOperationUid = operationUid;
			if (dvd!=null) {
				thisOperationUid = dvd.getOperationUid();
			}
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, thisOperationUid);
			if (this.getCurrentUserContext() != null) {
				UserContext userContext = this.getCurrentUserContext();
				wellboreUid = operation.getWellboreUid();
				
				currentDailyUid = userContext.getUserSelection().getDailyUid();

				if (StringUtils.isNotBlank(wellboreUid)) {
					Wellbore wellbore 	= (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, wellboreUid);
					
					String sql = "SELECT kickoffMdMsl FROM Wellbore WHERE (isDeleted = false or isDeleted is null) AND wellboreUid=:thisWellboreUid";
					List thisWellbore  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "thisWellboreUid", wellboreUid, this.queryProperties);
					
					Double kickOffMd = 0.0;
					Double wellboreKickOffMd = null;
					if(thisWellbore != null && thisWellbore.size() > 0) {
						if(thisWellbore.get(0) != null) wellboreKickOffMd = (Double) thisWellbore.get(0);
					}
					
					if (!isChild) {

						if (StringUtils.isBlank(wellbore.getParentWellboreUid())) {
							actualSeries.add(0.0, 0.0);
							if (this.showProductiveTimeDepth && BooleanUtils.isNotTrue(dvdHideProductiveDepth)){
								productiveSeries.add(0.0, 0.0);
							}					
						}else{
							if(wellboreKickOffMd != null) {
								thisConverter.setBaseValue(wellboreKickOffMd);
								kickOffMd = thisConverter.getConvertedValue(true);
							}
							
							if (actualSeries.isEmpty()) {
								actualSeries.add(0.0, kickOffMd);
							} else {
								actualSeries.add(0.0, 0.0);
							}
							if (this.showProductiveTimeDepth && BooleanUtils.isNotTrue(dvdHideProductiveDepth)){
								if (productiveSeries.isEmpty()) {
									productiveSeries.add(0.0, kickOffMd);
								} else {
									productiveSeries.add(0.0, 0.0);
								}
							}
						
							
						}
					}

					Map<String, String> classCode = new HashMap<String, String>();					
					String lookupClassCodeQuery = "from LookupClassCode where (isDeleted = false or isDeleted is null)";
					List lookupClassCodeLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(lookupClassCodeQuery, this.queryProperties);
					if (lookupClassCodeLists !=null && lookupClassCodeLists.size() > 0){
						for(Iterator it=lookupClassCodeLists.iterator(); it.hasNext();) {
							LookupClassCode lookupClassCode = (LookupClassCode)it.next();
							classCode.put(lookupClassCode.getShortCode(), lookupClassCode.getInternalCode());
							
						}
					}
								
					for(Iterator i=dailyLists.iterator(); i.hasNext();) {
						ReportDaily reportDaily  = (ReportDaily)i.next();
						
						String strSql = "SELECT operationUid, activityDuration, depthMdMsl, classCode FROM DvdSummary " +
										"WHERE (isDeleted = FALSE OR isDeleted IS NULL) " +
										"AND dailyUid = :dailyUid " +
										"AND operationUid = :operationUid " +
										"ORDER BY endDatetime";
						String[] paramNames  = {"dailyUid", "operationUid"};
						String[] paramValues = {reportDaily.getDailyUid(), reportDaily.getOperationUid()};
						List<Object[]> summaryLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues, this.queryProperties);
		
						if(summaryLists != null && summaryLists.size() > 0) {
							
							for (Object[] rec : summaryLists) {
								Double duration = null;
								Double depth = null;
								String clsCode = null;
								if (rec[1]!=null){
									duration= Double.parseDouble(rec[1].toString());
								}
								if (rec[2]!=null){
									depth	= Double.parseDouble(rec[2].toString());
								}
								if (rec[3]!=null){
									clsCode	= rec[3].toString();
								}
								Double dayDepth = 0.0;
								
								if (depth !=null) {
									thisConverter.setBaseValue(depth);
									dayDepth = thisConverter.getConvertedValue();
								}
								
								if(duration != null && dayDepth != null) {
									durationConverter.setBaseValue(duration);
									global_duration += durationConverter.getConvertedValue();									
									actualSeries.add(global_duration, dayDepth);		
																		
									String internalCode = classCode.get(clsCode);
									if (this.showProductiveTimeDepth && BooleanUtils.isNotTrue(dvdHideProductiveDepth)){
										if (!("TU".equals(internalCode) || "TP".equals(internalCode))){
											productive_duration += durationConverter.getConvertedValue();
											productiveSeries.add(productive_duration, dayDepth);
										}		
									}
																								
									if (dayDepth>this.maxDepth) this.maxDepth = dayDepth;
									if (global_duration > this.maxDuration) this.maxDuration = global_duration;
									if (this.showProductiveTimeDepth && BooleanUtils.isNotTrue(dvdHideProductiveDepth)){
										if (productive_duration > this.maxDuration) this.maxDuration = productive_duration;
									}
									
								}
								
							}
						}
						
						if (reportDaily.getDailyUid().equals(currentDailyUid) && showActualDataToDate) break;
					}
				}
			}
		}
		
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(actualSeries);
		
		if (this.showProductiveTimeDepth && BooleanUtils.isNotTrue(dvdHideProductiveDepth)){
			dataset.addSeries(productiveSeries);
		}
		
		return dataset;
	}
	
	/**
	 * Create a dataset for JFreeChart to draw Actual Cost line
	 * @return XYDataset
	 * @throws Exception
	 */
	Double global_duration_cost;
	Double global_cost;
	private XYDataset createActualCostDataset(String operationUid, XYSeries actualCostSeries) throws Exception {
		if (actualCostSeries==null) { 
			actualCostSeries = new XYSeries("Actual Cost"); 
			if (this.operationPlanSetting != null)
				if (StringUtils.isNotBlank(this.operationPlanSetting.getActualCostLabel()))
					actualCostSeries.setKey(this.operationPlanSetting.getActualCostLabel());
		}
		if(global_duration_cost==null) {
			global_duration_cost = 0.0; 
		}
		if(global_cost==null) {
			global_cost=0.0; 
		}
		
		String parentDvdOperationUid=null;
		Boolean isChild = false;

		String dvdUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
		if (dvdUid!=null) {
			OperationPlanMaster dvd = (OperationPlanMaster) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanMaster.class, dvdUid);
			
			if (dvd!=null) {
				parentDvdOperationUid = dvd.getDvdParentOperationUid(); 
				if (parentDvdOperationUid!=null && StringUtils.isNotBlank(parentDvdOperationUid)) {
					isChild = true;
					createActualCostDataset(parentDvdOperationUid, actualCostSeries);
				}
				
			}
			
		}
		this.operationUomContext.setOperationUid(operationUid);
		this.durationConverter = this.getDurationConverter();
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		this.dailyLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid =:operationUid and reportType=:reportType order by reportDatetime", new String[] {"operationUid", "reportType"}, new Object[] {operationUid, reportType}, this.queryProperties);
		
		if(this.dailyLists != null && this.dailyLists.size() > 0) {

			if(!isChild)
			actualCostSeries.add(0.0, 0.0);

			if (this.getCurrentUserContext() != null) {
				String currentDailyUid = this.getCurrentUserContext().getUserSelection().getDailyUid();

				//Get AmountSpentPriorToSpud to include in dayCost
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
				if (operation!=null) {
					if (operation.getAmountSpentPriorToSpud()!=null) global_cost = operation.getAmountSpentPriorToSpud();
					if (operation.getAmountSpentPriorToOperation()!=null) global_cost = operation.getAmountSpentPriorToOperation();
				}

				for(Iterator i = this.dailyLists.iterator(); i.hasNext();) {
					ReportDaily reportDaily	= (ReportDaily)i.next();
					String[] paramNames = {"operationUid", "dailyUid"};
					String[] paramValues = {reportDaily.getOperationUid(), reportDaily.getDailyUid()};
					this.activityLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Activity where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and dailyUid = :dailyUid AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') AND (isOffline=0 or isOffline is null) AND (isSimop=0 or isSimop is null) order by dayPlus, endDatetime", paramNames, paramValues, this.queryProperties);

					//String[] dynamicFieldParamNames  = {"fieldUid", "fieldKey"};
					//String[] dynamicFieldParamValues = {"report_daily.daycost", reportDaily.getDailyUid()};
					//List dayCostList 					 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select fieldValueDouble from DynamicField where (isDeleted = false or isDeleted is null) and fieldUid = :fieldUid and fieldKey = :fieldKey", dynamicFieldParamNames, dynamicFieldParamValues, this.queryProperties);

					//Get dayCost from Costnet, if costnet is null then get dayCost from reportDaily
					Double dayCost = 0.0;
					Double dayCostFromCostNet = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(reportDaily.getDailyUid());
					if(dayCostFromCostNet == null) {
						paramNames = new String[] {"operationUid", "dailyUid", "reportType"};
						paramValues = new String[] {reportDaily.getOperationUid(), reportDaily.getDailyUid(), CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(reportDaily.getOperationUid())};
						List dayCostList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT daycost FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid and dailyUid = :dailyUid and reportType=:reportType", paramNames, paramValues, this.queryProperties);
						if(dayCostList != null && dayCostList.size() > 0) {
							if(dayCostList.get(0) != null) dayCost = (Double) dayCostList.get(0);
						}
					} else {
						dayCost = dayCostFromCostNet;
					}

					Double dayTotalDuration = 0.0;
					if(activityLists != null && activityLists.size() > 0) {
						//get today total duration first for cost apportion
						for(Iterator it = activityLists.iterator(); it.hasNext();) {
							Activity activity = (Activity) it.next();
							Double duration = activity.getActivityDuration();
							if(duration != null) dayTotalDuration += duration;
						}

						// TO-DO - might change from dynamic field to daily object.
						for(Iterator it = activityLists.iterator(); it.hasNext();) {
							Activity activity = (Activity) it.next();
							Double duration = activity.getActivityDuration();

							if(duration != null) {
								durationConverter.setBaseValue(duration);
								global_duration_cost += durationConverter.getConvertedValue();
								global_cost += (dayCost * (duration/dayTotalDuration));
								actualCostSeries.add(global_duration_cost, global_cost);
							}
						}
					}
					if (reportDaily.getDailyUid().equals(currentDailyUid) && this.showActualDataToDate) break;
				}
			}
		}
		
		
	
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(actualCostSeries);
	
		return dataset;
	}
	
	Double global_p10_duration_cost;
	Double global_p10_cost;
	
	private XYDataset createP10CostDataset(String operationUid, XYSeries p10CostSeries) throws Exception {
		this.operationUomContext.setOperationUid(operationUid);
		this.durationConverter = this.getDurationConverter();

		if (p10CostSeries==null) { 
			p10CostSeries = new XYSeries("Planned/P10 Cost"); 
		}
		if(global_p10_duration_cost==null) {
			global_p10_duration_cost = 0.0; 
		}
		if(global_p10_cost==null) {
			global_p10_cost=0.0; 
		}

		if(this.operationPlanPhaseLists != null && operationPlanPhaseLists.size() > 0) {
			p10CostSeries.add(0.0, 0.0);
			for (Object[] recp : this.operationPlanPhaseLists) {
				Double p10Duration  = 0.0;
				Double p10cost = 0.0;

				if (recp[1]!=null)
					p10Duration = Double.parseDouble(recp[1].toString());
				if (recp[9]!=null)
					p10cost = Double.parseDouble(recp[9].toString());
				
				if(p10Duration != null && p10cost !=null) {
					durationConverter.setBaseValue(p10Duration);
					global_p10_duration_cost += durationConverter.getConvertedValue();
					global_p10_cost += p10cost;
					p10CostSeries.add(global_p10_duration_cost, global_p10_cost);
				}
			}
		}
	
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(p10CostSeries);
	
		return dataset;
	}
	
	Double global_p90_duration_cost;
	Double global_p90_cost;
	
	private XYDataset createP90CostDataset(String operationUid, XYSeries p90CostSeries) throws Exception {
		this.operationUomContext.setOperationUid(operationUid);
		this.durationConverter = this.getDurationConverter();

		if (p90CostSeries==null) { 
			p90CostSeries = new XYSeries("Planned/P90 Cost"); 
		}
		if(global_p90_duration_cost==null) {
			global_p90_duration_cost = 0.0; 
		}
		if(global_p90_cost==null) {
			global_p90_cost=0.0; 
		}

		if(this.operationPlanPhaseLists != null && operationPlanPhaseLists.size() > 0) {
			p90CostSeries.add(0.0, 0.0);
			
			for (Object[] recp : this.operationPlanPhaseLists) {
				Double p90Duration  = 0.0;
				Double p90cost = 0.0;
				
				if (recp[3]!=null)
					p90Duration = Double.parseDouble(recp[3].toString());
				if (recp[10]!=null)
					p90cost = Double.parseDouble(recp[10].toString());
				
				if(p90Duration != null && p90cost !=null) {
					durationConverter.setBaseValue(p90Duration);
					global_p90_duration_cost += durationConverter.getConvertedValue();
					global_p90_cost += p90cost;
					p90CostSeries.add(global_p90_duration_cost, global_p90_cost);
				}
			}
		}
	
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(p90CostSeries);
	
		return dataset;
	}
	
	private void loadAnnotationList() throws Exception {
		String queryString = "FROM OperationDvdAnnotation WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid";
		CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale());
		
		if (this.getCurrentUserContext() != null) {
			String operationUid = this.getCurrentUserContext().getUserSelection().getOperationUid();
			this.operationUomContext.setOperationUid(operationUid);
			this.durationConverter = this.getDurationConverter();

			List<OperationDvdAnnotation> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid, this.queryProperties);
			this.annotationLists = new ArrayList();
			for (OperationDvdAnnotation rec : rs) {
				String dailyUid = rec.getDailyUid();
				if (dailyUid==null) continue;
				if (rec.getDepthOccur()==null) continue;
				if (rec.getComments()==null) continue;
				if (rec.getAnnotationReferenceNumber()==null) continue; 
				
				Date dayDate = null;
				Date timeOccur = rec.getTimeOccur();
				Double duration = 0.0;
				
				queryString = "FROM Daily where (isDeleted=false or isDeleted is null) and dailyUid=:dailyUid";
				List<Daily> dailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", rec.getDailyUid(), this.queryProperties);
				if (dailyList.size()>0) {
					dayDate = dailyList.get(0).getDayDate();
				}
				
				if (dayDate!=null) { //previous day
					queryString = "SELECT sum(a.activityDuration) from Activity a, Daily d " +
							"where (a.isDeleted=false or a.isDeleted is null) " +
							"and (d.isDeleted=false or d.isDeleted is null) " +
							"and a.dailyUid=d.dailyUid " +
							"and d.dayDate<:dayDate " + 
							"and a.operationUid=:operationUid " +
							"AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null)";
					List<Double> durationList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
							new String[] {"dayDate","operationUid"}, 
							new Object[] {dayDate, operationUid}, 
							this.queryProperties);
					if (durationList.size()>0) {
						Double result = durationList.get(0);
						if (result!=null) duration += durationList.get(0);
					}
				}
				
				if (timeOccur!=null && dailyUid!=null) {
					queryString = "SELECT sum(activityDuration) from Activity " +
							"where (isDeleted=false or isDeleted is null) " +
							"and dailyUid=:dailyUid " +
							"and startDatetime<=:timeOccur " +
							"AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') " +
							"AND (isSimop=false or isSimop is null) " +
							"AND (isOffline=false or isOffline is null)";
					List<Double> durationList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
							new String[] {"dailyUid","timeOccur"}, 
							new Object[] {dailyUid, timeOccur}, 
							this.queryProperties);
					if (durationList.size()>0) {
						Double result = durationList.get(0);
						if (result!=null) duration += durationList.get(0);
					}
				}
				
				Object[] e = new Object[5];
				//do calculation
				durationConverter.setBaseValue(duration);
				e[0] = durationConverter.getConvertedValue();
				thisConverter.setReferenceMappingField(Activity.class, "depthMdMsl", operationUomContext.getUomTemplateUid());
				thisConverter.setBaseValue(rec.getDepthOccur());
				e[1] = thisConverter.getConvertedValue();
				e[2] = rec.getCommentType();
				e[3] = rec.getComments();
				e[4] = rec.getAnnotationReferenceNumber();
				this.annotationLists.add(e);
			}
		}
	}
	
	public void dispose() {
		this.xyDatasetMap.clear();
		this.operationPlanPhaseLists = null;
		this.dailyLists 			 = null;
		this.activityLists			 = null;
	}
}
