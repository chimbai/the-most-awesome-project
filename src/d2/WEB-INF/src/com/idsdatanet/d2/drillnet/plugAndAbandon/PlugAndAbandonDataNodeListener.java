package com.idsdatanet.d2.drillnet.plugAndAbandon;

//import class
import javax.servlet.http.HttpServletRequest; 
import com.idsdatanet.d2.core.model.*;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.uom.CustomFieldUom;

public class PlugAndAbandonDataNodeListener extends EmptyDataNodeListener {
	
	//before save or update event handler
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		//load the data
		Object object = node.getData();
		
		if (object instanceof PlugAndAbandonDetail) {
			
			//object for plug and abandon detail 
			PlugAndAbandonDetail plugandabandondetail = (PlugAndAbandonDetail) object; 
			
			//variable declaration
			Double length = 0.00; 
			Double depthTopMdMsl = null;
			Double depthBottomMdMsl = null;
			
			//reference mapping to depthTopMdMsl
			CustomFieldUom thisConverterField = new CustomFieldUom(commandBean, PlugAndAbandonDetail.class, "depthTopMdMsl");

			//get DepthTopMdMsl/DepthBottomMdMsl from screen when not null
			if (plugandabandondetail.getDepthTopMdMsl() != null) depthTopMdMsl = Double.parseDouble(plugandabandondetail.getDepthTopMdMsl().toString());
			if (plugandabandondetail.getDepthBottomMdMsl() != null) depthBottomMdMsl = Double.parseDouble(plugandabandondetail.getDepthBottomMdMsl().toString());
			
			//check if both DepthTopMdMsl and DepthBottomMdMsl not null
			if (depthTopMdMsl != null & depthBottomMdMsl != null) 
			{
				//to get the raw value from the user value as the calculation is based on base value (exclude adopt unit for the selected field)
				//set the base value from use value for depthTopMdMsl
				thisConverterField.setBaseValueFromUserValue(depthTopMdMsl); 
				//get the base value
				depthTopMdMsl = thisConverterField.getBasevalue();
			
				//change the reference mapping to depthBottomMdMsl
				thisConverterField.setReferenceMappingField(PlugAndAbandonDetail.class, "depthBottomMdMsl");
				//set the base value from use value for depthTopMdMsl
				thisConverterField.setBaseValueFromUserValue(depthBottomMdMsl); 
				//get the base value
				depthBottomMdMsl = thisConverterField.getBasevalue();

				//length calculation as length is calculated based on the different between depthBottomMdMsl and depthTopMdMsl
				//absolute value for the length
				length = Math.abs(depthBottomMdMsl - depthTopMdMsl);
				
				//convert the value to the screen display unit for display
				//change the reference mapping to length
				thisConverterField.setReferenceMappingField(PlugAndAbandonDetail.class, "length");
				//set the length to base value
				thisConverterField.setBaseValue(length); 
				//get the converted value for the length
				length = thisConverterField.getConvertedValue();
				
				//assign the value to length on screen
				plugandabandondetail.setLength(length); 
			
			}
			//check if depthBottomMdMsl or depthTopMdMsl is a null value
			else 
			{
				//set length to null
				length = null; 
				//assign the value to length on screen
				plugandabandondetail.setLength(length);
			}
			
			
		}

	}

}
