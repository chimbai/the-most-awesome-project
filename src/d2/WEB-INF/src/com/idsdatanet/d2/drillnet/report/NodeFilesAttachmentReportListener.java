package com.idsdatanet.d2.drillnet.report;

import java.text.DateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.model.FileManagerFiles;
import com.idsdatanet.d2.core.util.ClassUtils;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.filemanager.FileManagerUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanReportDataListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class NodeFilesAttachmentReportListener extends EmptyCommandBeanReportDataListener{
	private String className = null;
	private String controllerName = null;
	private List<String> outputFields = null;
	private static final Log logger = LogFactory.getLog(NodeFilesAttachmentReportListener.class);
			
	public void setOutputFields(List<String> values) {
		this.outputFields = values;
	}
	
	public void setControllerName(String value) {
		this.controllerName = value;
	}
	
	public void setClassName(String value) {
		this.className = value;
	}
	
	public void afterWriteDataNode(CommandBeanTreeNode node, SimpleXmlWriter xmlWriter, UserSelectionSnapshot userSelection) throws Exception{
		if(node.getData() != null && node.getData().getClass().getSimpleName().equals(this.className)){
			List<FileManagerFiles> list = FileManagerUtils.getFlexNodeFilesAttachment(this.controllerName, node.getData());
			for(FileManagerFiles item: list) {
				xmlWriter.startElement("NodeFilesAttachment");
				this.outputAttachmentDetails(item, xmlWriter);
				xmlWriter.endElement();
			}
		}
	}
	
	private static String formatDate(Object value) throws Exception {
		if(value == null) return null;
		DateFormat df = ApplicationConfig.getConfiguredInstance().getReportDateFormater();
		if(df == null) return value.toString();
		try{
			return df.format(value);
		}catch(Exception e){
			logger.error("Error format date", e);
			return value.toString();
		}
	}
	
	private static String formatValue(Object value) throws Exception {
		if(value == null) {
			return "";
		}
		if(java.util.Date.class.isInstance(value)) {
			return formatDate(value);
		}
		return value.toString();
	}
	
	private void outputAttachmentDetails(FileManagerFiles data, SimpleXmlWriter xmlWriter) throws Exception {
		Map<String, Object> dataDescribed = ClassUtils.collectCommonBeanProperties(data);
		dataDescribed.remove("class"); // this is common property inheritted from Object
		Set<String> keys = (this.outputFields != null)? new HashSet<String>(this.outputFields) : dataDescribed.keySet();
		for(String key: keys) {
			xmlWriter.addElement(key, formatValue(dataDescribed.get(key)));
		}
	}
}
