package com.idsdatanet.d2.drillnet.activity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.core.lookup.CascadeLookupMeta;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.templateimport.DefaultTemplateImportHandler;
import com.idsdatanet.d2.core.web.mvc.templateimport.EmptySlot;


public class BHIActivityTemplateImportHandler extends DefaultTemplateImportHandler {
	
	private static final String ACTIVITY_DATE = "@activityDate";
	private static final String ACTIVITY_START_DATE = "startDatetime";
	private static final String ACTIVITY_END_DATE = "endDatetime";
	private Map<String,String> fieldToUri = null;
	
	private Date previousActivityDate = null;
	private Boolean isNextDay = false;
	
	@Override
	protected void setupAdditionalBindingValues(int line, List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode){
		
		if (bindingValue !=null) {
			
			if (bindingValue.get(0) instanceof EmptySlot) {
				
			}else {
				Date activityDate = null;
				Date startTime = null;
				Date endTime = null;
				DateFormat df = null;
				try {
					 df = new SimpleDateFormat(ApplicationConfig.getConfiguredInstance().getApplicationDateFormat());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					this.processNPTLookup(bindingProperties, bindingValue, targetMeta, commandBean);
					this.processLookupMapping(bindingProperties, bindingValue, targetMeta);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (bindingProperties.contains(this.getPropertyNameFromField(targetMeta,ACTIVITY_DATE)) && bindingProperties.contains(this.getPropertyNameFromField(targetMeta,ACTIVITY_START_DATE)) && bindingProperties.contains(this.getPropertyNameFromField(targetMeta,ACTIVITY_END_DATE)))
				{
					activityDate = CommonDateParser.parse((String) bindingValue.get(bindingProperties.indexOf(this.getPropertyNameFromField(targetMeta,ACTIVITY_DATE))));
					startTime = CommonDateParser.parse((String)  bindingValue.get(bindingProperties.indexOf(this.getPropertyNameFromField(targetMeta,ACTIVITY_START_DATE))));
					endTime =CommonDateParser.parse((String) bindingValue.get(bindingProperties.indexOf(this.getPropertyNameFromField(targetMeta,ACTIVITY_END_DATE))));
					if ((previousActivityDate!=null && !previousActivityDate.equals(activityDate))){
						previousActivityDate = activityDate;
						isNextDay = false;
					}else{
						if (isNextDay)
						{
							bindingValue.set(bindingProperties.indexOf(this.getPropertyNameFromField(targetMeta,ACTIVITY_DATE)), df.format(DateUtils.addDays(activityDate, 1)));
						}
					}
					if (endTime.before(startTime))
					{
						Calendar cal = Calendar.getInstance();
						cal.setTime(endTime);
						cal.set(Calendar.HOUR_OF_DAY, 23);
						cal.set(Calendar.MINUTE, 59);
						cal.set(Calendar.SECOND, 59);
						try {
							this.addNextDayActivity(bindingProperties, bindingValue, targetMeta, commandBean, parentNode);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						bindingValue.set(bindingProperties.indexOf(this.getPropertyNameFromField(targetMeta,ACTIVITY_END_DATE)), "23:59:59");
						this.previousActivityDate = activityDate;
						this.isNextDay = true;
					}
				}
			}
		}
	}
	
	private void processNPTLookup(List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean) throws Exception
	{
		String propertyName = this.getPropertyNameFromField(targetMeta, "nptMainCategory");
		int index = bindingProperties.indexOf(propertyName);
		String value = bindingValue.get(index).toString();
		
		if (StringUtils.isNotBlank(value))
		{
			CascadeLookupMeta meta = commandBean.getCascadeLookupMeta("Activity", "nptMainCategory");
			if (meta!=null)
			{
				String catUri = meta.getLookupUriOrHandler().toString();
				Map<String,LookupItem> catLookups = LookupManager.getConfiguredInstance().getLookup(catUri, new UserSelectionSnapshot(), null);
				for (Map.Entry<String, LookupItem> entry : catLookups.entrySet())
				{
					if (entry.getValue().getValue().toString().equalsIgnoreCase(value))
					{
						bindingValue.set(index, entry.getKey());
						String subPropertyName = this.getPropertyNameFromField(targetMeta, "nptSubCategory");
						int subIndex = bindingProperties.indexOf(subPropertyName);
						String subValue = bindingValue.get(subIndex).toString();
						if (StringUtils.isNotBlank(subValue))
						{
							CascadeLookupSet[] lookupSet = commandBean.getCompleteCascadeLookupSet("Activity","nptSubCategory");
							for (CascadeLookupSet set : lookupSet)
							{
								if (entry.getKey().equalsIgnoreCase(set.getKey()))
								{
									for (Map.Entry<String, LookupItem> sublookup : set.getLookup().entrySet())
									{
										if (sublookup.getValue().getValue().toString().equalsIgnoreCase(subValue))
											bindingValue.set(subIndex, sublookup.getKey());
									}
								}
							}
						}
					}
				}
				
			}
		}
	}
	
	private void processLookupMapping(List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta) throws Exception
	{
		if (fieldToUri!=null)
		{
			for (Map.Entry<String, String> entry : fieldToUri.entrySet())
			{
				String field = entry.getKey();
				String uri = entry.getValue();
				Map<String,LookupItem> lookups = LookupManager.getConfiguredInstance().getLookup(uri, new UserSelectionSnapshot(), null);
				String propertyName = this.getPropertyNameFromField(targetMeta, field);
				int index = bindingProperties.indexOf(propertyName);
				String value = bindingValue.get(index).toString();
				for (Map.Entry<String, LookupItem> eLookup : lookups.entrySet())
				{
					if (eLookup.getValue().getValue().toString().equalsIgnoreCase(value))
						bindingValue.set(index, eLookup.getKey());
				}
			}
		}
	}
	
	private void addNextDayActivity(List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode) throws Exception
	{
		DateFormat df = null;
		try {
			 df = new SimpleDateFormat(ApplicationConfig.getConfiguredInstance().getApplicationDateFormat());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Date activityDate = null;
		Date startTime = null;
		Date endTime = null;
		activityDate = CommonDateParser.parse((String) bindingValue.get(bindingProperties.indexOf(this.getPropertyNameFromField(targetMeta,ACTIVITY_DATE))));
		startTime = CommonDateParser.parse((String)  bindingValue.get(bindingProperties.indexOf(this.getPropertyNameFromField(targetMeta,ACTIVITY_START_DATE))));
		endTime =CommonDateParser.parse((String) bindingValue.get(bindingProperties.indexOf(this.getPropertyNameFromField(targetMeta,ACTIVITY_END_DATE))));
			
			activityDate = DateUtils.addDays(activityDate, 1);	
			
			List newBindingValue = new ArrayList(bindingValue);	
			newBindingValue.set(bindingProperties.indexOf(this.getPropertyNameFromField(targetMeta,ACTIVITY_DATE)), df.format(activityDate));
			df = new SimpleDateFormat("kk:mm");
			newBindingValue.set(bindingProperties.indexOf(this.getPropertyNameFromField(targetMeta,ACTIVITY_START_DATE)),"00:00");
			newBindingValue.set(bindingProperties.indexOf(this.getPropertyNameFromField(targetMeta,ACTIVITY_END_DATE)), df.format(endTime));
			
			CommandBeanTreeNode currentNode = commandBean.addNewTemplateImportNode(parentNode, targetMeta);
			
			this.bindImportDataToNewNode(-1, currentNode, bindingProperties, newBindingValue, commandBean.getDataBinder());
		
	}

	public void setFieldToUri(Map<String,String> fieldToUri) {
		this.fieldToUri = fieldToUri;
	}

	public Map<String,String> getFieldToUri() {
		return fieldToUri;
	}
}
