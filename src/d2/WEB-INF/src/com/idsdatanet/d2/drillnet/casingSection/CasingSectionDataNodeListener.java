package com.idsdatanet.d2.drillnet.casingSection;

import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CasingSummary;
import com.idsdatanet.d2.core.model.CasingTally;
import com.idsdatanet.d2.core.model.CementJob;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;
import com.idsdatanet.d2.drillnet.bharun.BharunUtils;
import com.idsdatanet.d2.drillnet.leakOffTest.LeakOffTestUtil;

public class CasingSectionDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler, DataNodeAllowedAction, ActionManager, CommandBeanListener{
	
	private DeleteSummaryHandler deleteSummaryHandler = new DeleteSummaryHandler();
	private SaveSummaryHandler saveSummaryHandler = new SaveSummaryHandler();
	private List<String> fieldsToPopulate=null;
	private List<String> fieldsToExcludeOnUpdate=null;
	private List<String> groupingCriteria=null;
	
	
	private String tallyType;
	
	public void setTallyType(String value){
		this.tallyType = value;
	}
	
	//Auto Calculate the Section Bottom Depth in Casing Tally.
	private void AutoCalTallyDepth(CommandBeanTreeNode parentNode, UserSelectionSnapshot userSelection, double casingLandedDepthMdMsl) throws Exception{
		CustomFieldUom thisConverter = new CustomFieldUom(userSelection.getLocale(), CasingTally.class, "sectionBottomMdMsl");
		double PreviousDepth=0.0;
		
		CasingTally prevTally = null;
		for (CommandBeanTreeNode child: parentNode.getList().get("CasingTally").values()) {
			Object object = child.getData();
			
			if (object instanceof CasingTally){
								
				CasingTally currCasingTally = (CasingTally) object;
				
				if (!"1".equals(currCasingTally.getExclude())){
					
					thisConverter.setReferenceMappingField(CasingTally.class, "sectionBottomMdMsl");
					if (prevTally==null)
					{
						PreviousDepth = casingLandedDepthMdMsl;
					}else{
						double csgLength = 0.0;
						thisConverter.setReferenceMappingField(CasingTally.class, "csgLength");
						if (prevTally.getCsgLength()!=null)
						{
							thisConverter.setBaseValueFromUserValue(prevTally.getCsgLength(),false);
							csgLength = thisConverter.getBasevalue();
						}
						PreviousDepth = PreviousDepth-csgLength;
					}	
					thisConverter.setBaseValue(PreviousDepth);
					currCasingTally.setSectionBottomMdMsl(thisConverter.getConvertedValue());
					prevTally = currCasingTally;

				}else{
					currCasingTally.setSectionBottomMdMsl(null);	
				}
				
				//if(commandBean.getOperatingMode() != BaseCommandBean.OPERATING_MODE_DEPOT) 
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(currCasingTally);
			}
		}
	}

	private void setExclude(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		Object object = node.getData();
		if (object instanceof CasingTally  && "1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(),GroupWidePreference.GWP_IS_JOINT_WAS_RUN_USED)))
		{
			CasingTally tally = (CasingTally) object;
			tally.setExclude(tally.getIsJointRun()==null?"0":(tally.getIsJointRun()?"0":"1"));
		}
	}
	//Auto Calculate the Section Top Depth in Casing Tally.
	private void AutoCalTopDepth(CommandBeanTreeNode parentNode, UserSelectionSnapshot userSelection) throws Exception
	{
		CustomFieldUom thisConverter = new CustomFieldUom(userSelection.getLocale(), CasingTally.class, "sectionBottomMdMsl");
		for (CommandBeanTreeNode child: parentNode.getList().get("CasingTally").values()) {
			Object object = child.getData();
			if(object instanceof CasingTally) {
				CasingTally csgtally = (CasingTally) object;
				String exclude = csgtally.getExclude();
				
				// do not count if exclude = 1
				if (!"1".equals(exclude)){
					Double nodeLength = csgtally.getCsgLength();
					Double nodeBottom = csgtally.getSectionBottomMdMsl();
					
					if (nodeLength!=null && nodeBottom!=null)
					{
						thisConverter.setReferenceMappingField(CasingTally.class, "csgLength");
						thisConverter.setBaseValueFromUserValue(nodeLength);
						nodeLength = thisConverter.getBasevalue();
												
						thisConverter.setReferenceMappingField(CasingTally.class, "sectionBottomMdMsl");
						thisConverter.setBaseValueFromUserValue(nodeBottom,false);
						nodeBottom = thisConverter.getBasevalue();
						
						Double nodeTop = nodeBottom - nodeLength;
						
						thisConverter.setReferenceMappingField(CasingTally.class, "sectionTopMdMsl");
						thisConverter.setBaseValue(nodeTop);
						csgtally.setSectionTopMdMsl(thisConverter.getConvertedValue());
					}else
					{
						csgtally.setSectionTopMdMsl(null);
					}
				}else{
					csgtally.setSectionTopMdMsl(null);
				}
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(csgtally);
			}
		}
	}
	//Auto Calculate the Section Bottom Depth in Casing Tally.
	private void AutoCalTallyCumVol(CommandBeanTreeNode parentNode, UserSelectionSnapshot userSelection) throws Exception{
		CustomFieldUom thisConverter = new CustomFieldUom(userSelection.getLocale(), CasingTally.class, "volumeDisplaced");
		
		double cumVolumeDisplaced=0.0;
		double cumHoleVolume=0.0;
		
		for (CommandBeanTreeNode child: parentNode.getList().get("CasingTally").values()) {
			Object object = child.getData();
			
			if (object instanceof CasingTally){
				CasingTally currCasingTally = (CasingTally) object;
				currCasingTally.getHoleVolume();
				currCasingTally.getVolumeDisplaced();
				
				double volumeDisplaced=0.0;
				double holeVolume=0.0;
				String exclude = currCasingTally.getExclude();
				
				if (!"1".equals(exclude))
				{	
					if (currCasingTally.getVolumeDisplaced()!=null)
					{
						thisConverter.setReferenceMappingField(CasingTally.class, "volumeDisplaced");
						thisConverter.setBaseValueFromUserValue(currCasingTally.getVolumeDisplaced(),false);
						volumeDisplaced = thisConverter.getBasevalue();
						cumVolumeDisplaced = cumVolumeDisplaced + volumeDisplaced;
					}
					if (currCasingTally.getHoleVolume()!=null)
					{
						thisConverter.setReferenceMappingField(CasingTally.class, "holeVolume");
						thisConverter.setBaseValueFromUserValue(currCasingTally.getHoleVolume(),false);
						holeVolume = thisConverter.getBasevalue();
						cumHoleVolume = cumHoleVolume + holeVolume;
					}
				}
				if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_TOTAL_DISPLACEMENT))){
					thisConverter.setReferenceMappingField(CasingTally.class, "cumDisplacementVolume");
					thisConverter.setBaseValue(cumVolumeDisplaced);
					currCasingTally.setCumDisplacementVolume(thisConverter.getConvertedValue());
				}
				
				
				thisConverter.setReferenceMappingField(CasingTally.class, "cumHoleVolume");
				thisConverter.setBaseValue(cumHoleVolume);
				currCasingTally.setCumHoleVolume(thisConverter.getConvertedValue());
				
			//	if(commandBean.getOperatingMode() != BaseCommandBean.OPERATING_MODE_DEPOT)  
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(currCasingTally);
			}
		}
	}
	
	private void calcDisplacement(CommandBeanTreeNode parentNode, UserSelectionSnapshot userSelection) throws Exception{
		CustomFieldUom thisConverter = new CustomFieldUom(userSelection.getLocale(), CasingTally.class, "volumeDisplaced");
		Double totalCumDisplacementVolume = 0.0;
		for (CommandBeanTreeNode child: parentNode.getList().get("CasingTally").values()) {
			Object object = child.getData();
			
			if (object instanceof CasingTally){
				CasingTally currCasingTally = (CasingTally) object;
				String exclude = currCasingTally.getExclude();
				
				if (!"1".equals(exclude))
				{	
					totalCumDisplacementVolume = totalCumDisplacementVolume + (currCasingTally.getVolumeDisplaced() != null ? currCasingTally.getVolumeDisplaced() : 0.0);
				}
			}
		}
		thisConverter.setBaseValueFromUserValue(totalCumDisplacementVolume);
		Double totalCumDisplacementVolBase = thisConverter.getBasevalue();
		
		CasingSection casingSection = (CasingSection) parentNode.getData();
		
		if (casingSection != null) {
			Double actualDisplacementVol = 0.0;
			Double displacementVolumeDifference = 0.0;
			String casingSectionUid = casingSection.getCasingSectionUid();
			
			if (casingSection.getActualDisplacementVolume() != null) actualDisplacementVol = casingSection.getActualDisplacementVolume();
			
			thisConverter.setReferenceMappingField(CasingSection.class, "actualDisplacementVolume");
			thisConverter.setBaseValueFromUserValue(actualDisplacementVol);
			actualDisplacementVol = thisConverter.getBasevalue();
			
			displacementVolumeDifference = actualDisplacementVol - totalCumDisplacementVolBase;
	
			thisConverter.setReferenceMappingField(CasingSection.class, "cumDisplacementVolume");
			thisConverter.setBaseValue(totalCumDisplacementVolume);
			casingSection.setCumDisplacementVolume(thisConverter.getConvertedValue());
			
			thisConverter.setReferenceMappingField(CasingSection.class, "displacementVolumeDifference");
			thisConverter.setBaseValue(displacementVolumeDifference);
			casingSection.setDisplacementVolumeDifference(thisConverter.getConvertedValue());
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(casingSection);
//			String strSql = "UPDATE CasingSection SET displacementVolumeDifference=:thisDisplacementVolumeDifference, cumDisplacementVolume=:thisTotalCumDisplacementVolume WHERE casingSectionUid=:thisCasingSectionUid";
//			String[] paramsFields = { "thisCasingSectionUid" , "thisDisplacementVolumeDifference" , "thisTotalCumDisplacementVolume"};
//			Object[] paramsValues = new Object[3];  
//			paramsValues[0] = casingSectionUid;
//			paramsValues[1] = displacementVolumeDifference;
//			paramsValues[2] = totalCumDisplacementVolume;
//			
//			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
	}

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		CustomFieldUom thisConverter1 = new CustomFieldUom(commandBean);			

		if(object instanceof CasingSection)
		{
			//this.loadCasingTallySummaryByGroupCriteria(node, userSelection.getGroupUid());
			CasingSection casingsection = (CasingSection) object;
			this.loadCementRecordList(node, casingsection, userSelection);
			
			//reset the previous record depth from
			Double from = null;
			commandBean.getRoot().getDynaAttr().put("from", from);
			commandBean.getRoot().getDynaAttr().put("casingsectionuid", casingsection.getCasingSectionUid());
			
			//get casing TVDSS 
			if(casingsection.getShoeTopTvdMsl() != null){
				thisConverter1 = new CustomFieldUom(commandBean, CasingSection.class, "shoeTopTvdMsl");
				thisConverter1.setBaseValueFromUserValue(casingsection.getShoeTopTvdMsl(), false);
				thisConverter1.removeDatumOffset();
				if (thisConverter1.isUOMMappingAvailable()) {
				     node.setCustomUOM("@actualTvdSS", thisConverter1.getUOMMapping(false));
				    }
				node.getDynaAttr().put("actualTvdSS", thisConverter1.getConvertedValue());
			}
			
			// 20100310-0253-sthien - Auto Calculate Section Depth in the Casing Tally. //jwong
			
		//	this.AutoCalTallyCumVol(node,commandBean, userSelection);
			this.loadCasingTallySummary(node, userSelection.getGroupUid());
		}
		
		//TO CALCULATE CUMULATIVE CASING LENGTH IN CASING TALLY
		if(object instanceof CasingTally)
		{
			this.setExclude(node, userSelection);
			Double csgLength = (Double) PropertyUtils.getProperty(object, "csgLength");
			String casingsectionuid = (String) PropertyUtils.getProperty(object, "casingSectionUid");
			String exclude = (String) PropertyUtils.getProperty(object, "exclude");
			if(csgLength !=null) {
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "csgLength");
				
				Double cumCsgLength = 0.00;
				String casingsectionId = null;				
				
				casingsectionId = (String) commandBean.getRoot().getDynaAttr().get("casingsectionuid");				
				
				if (casingsectionuid.equals(casingsectionId)) {
					cumCsgLength = (Double) commandBean.getRoot().getDynaAttr().get("cumCsgLength");					
				} else {
					cumCsgLength = 0.00;
				}				
				
				if (cumCsgLength == null) {					
					commandBean.getRoot().getDynaAttr().put("cumCsgLength", csgLength);
					//thisConverter.setBaseValueFromUserValue(csgLength);
					if (thisConverter.isUOMMappingAvailable()) node.setCustomUOM("@cumCsgLength", thisConverter.getUOMMapping());
					node.getDynaAttr().put("cumCsgLength", csgLength);					
					commandBean.getRoot().getDynaAttr().put("casingsectionuid", casingsectionuid);
				} else {
					if ("1".equals(exclude)){												
						commandBean.getRoot().getDynaAttr().put("cumCsgLength", cumCsgLength);
						//thisConverter.setBaseValueFromUserValue(cumCsgLength);
						if (thisConverter.isUOMMappingAvailable()) node.setCustomUOM("@cumCsgLength", thisConverter.getUOMMapping());
						node.getDynaAttr().put("cumCsgLength",cumCsgLength);
						commandBean.getRoot().getDynaAttr().put("casingsectionuid", casingsectionuid);
					}
					else {
						cumCsgLength += csgLength;
						commandBean.getRoot().getDynaAttr().put("cumCsgLength", cumCsgLength);
						//thisConverter.setBaseValueFromUserValue(cumCsgLength);
						if (thisConverter.isUOMMappingAvailable()) node.setCustomUOM("@cumCsgLength", thisConverter.getUOMMapping());
						node.getDynaAttr().put("cumCsgLength",cumCsgLength);
						commandBean.getRoot().getDynaAttr().put("casingsectionuid", casingsectionuid);
					}					
				}
			}
			
		}
			
	}
	
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(userSession);
		if ("CasingTally".equalsIgnoreCase(className))
		{
			Boolean isModified = false;
			Boolean isNewRecord = false;
			String casingSectionUid = "";
			CasingSection casingrecord = (CasingSection)parent.getData();
			if(casingrecord.getCasingSectionUid()!=null)
			{
				casingSectionUid = casingrecord.getCasingSectionUid();
			}
			//this is for checking whether the Stick up length field value in screen has being modify or not
			if(casingrecord.isPropertyModified("stickUpLengthFromMdMsl"))
			{
				isModified = true;
			}
			if(status.getSavedNodes()!=null)
			{
				//this is for checking on each edited casing tally records
				for(CommandBeanTreeNode node:status.getSavedNodes())
				{
					if (node.getData() instanceof CasingTally)
					{
						CasingTally casingTallyRecord=(CasingTally)node.getData();
						//this is for checking whether the casing tally record is newly create or not
						if(Boolean.parseBoolean(node.getDynaAttr().get("isNewlyCreated").toString()))
						{
							isNewRecord = Boolean.parseBoolean(node.getDynaAttr().get("isNewlyCreated").toString()); 
							break;
						}
						//this is for checking whether the length field or seq field value in casing tally section has being modify or not
						if(casingTallyRecord.isPropertyModified("csgLength") || casingTallyRecord.isPropertyModified("runOrder"))
						{
							isModified = true;
							break;
						}
					}
				}
				
			}
			//If the length field or seq field value in casing tally section has being modified or the stick up length field value in casing section 
			//has being modified or the casing tally record is deleted then calculate the depth from and depth to value in casing tally section.
			if(isModified || isNewRecord || status.isAnyNodeDeleted())
			{
				CasingSectionUtils.calculateDepthFromAndDepthTo(casingSectionUid, status.getUpdatedNodes(), userSession);
			}
			
			double casingLandedDepthMdMsl = 0.0;
			CustomFieldUom thisConverter = new CustomFieldUom(userSelection.getLocale(), CasingSection.class, "casingLandedDepthMdMsl");
			
			if (casingrecord.getCasingLandedDepthMdMsl()!=null)
			{
				thisConverter.setReferenceMappingField(CasingSection.class, "casingLandedDepthMdMsl");
				thisConverter.setBaseValueFromUserValue(casingrecord.getCasingLandedDepthMdMsl(),false);
				casingLandedDepthMdMsl = thisConverter.getBasevalue();
			}
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_TALLY_BOTTOM_DEPTH)))
			{
				this.AutoCalTallyDepth(parent, userSelection, casingLandedDepthMdMsl);
			}
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_TALLY_TOP_DEPTH)))
			{
				this.AutoCalTopDepth(parent, userSelection);
			}
			
			// 17371 - Restructure the afterdatanodeload
			this.AutoCalTallyCumVol(parent, userSelection);
			this.calcDisplacement(parent, userSelection);
		}
		
	}
	private void loadCementRecordList(CommandBeanTreeNode node, CasingSection casingsection, UserSelectionSnapshot userSelection) throws Exception {
	
		String[] paramsFields = {"casingSectionUid"};
		String[] paramsValues = {casingsection.getCasingSectionUid()};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM CementJob WHERE (isDeleted = false or isDeleted is null) and casingSectionUid=:casingSectionUid order by jobNumber", paramsFields, paramsValues);;
		String relatedCementJob = null;
		if (items.isEmpty()){
			node.getDynaAttr().put("IsCementjob", "no");
		}else {
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object obj_array = (Object) i.next();
				CementJob thisCementJob = (CementJob) obj_array;
				String cementJobNumber= StringUtils.isNotBlank(thisCementJob.getJobNumber())?URLEncoder.encode(thisCementJob.getJobNumber(), "utf-8"):"";
				String cementJobUid = StringUtils.isNotBlank(thisCementJob.getCementJobUid())?URLEncoder.encode(thisCementJob.getCementJobUid(), "utf-8"):"";
				String cementDailyUid = StringUtils.isNotBlank(thisCementJob.getDailyUid())?URLEncoder.encode(thisCementJob.getDailyUid(), "utf-8"):"";
				
				if (StringUtils.isBlank(relatedCementJob)) {
					//relatedCementJob = cementJobNumber + "=" + cementJobUid + "#" + cementDailyUid;
					relatedCementJob = "cementjobnumber=" + cementJobNumber + "&cementjobuid=" + cementJobUid + "&cementdailyuid=" + cementDailyUid;
				}else {
					//relatedCementJob = relatedCementJob + "," + cementJobNumber + "=" + cementJobUid + "#" + cementDailyUid;
					relatedCementJob += ",cementjobnumber=" + cementJobNumber + "&cementjobuid=" + cementJobUid + "&cementdailyuid=" + cementDailyUid;
				}
				
			}
			node.getDynaAttr().put("cementjob", relatedCementJob);
		}

	}
	
	private String getGroupCriteriaKey(CommandBeanTreeNode node) throws Exception
	{
		String key="";
		if (this.getGroupingCriteria()!=null)
		{
			for (String criteria:this.getGroupingCriteria())
			{
				Object value=node.getValue(criteria);
				if (value!=null)
				{
					if (StringUtils.isNotEmpty(value.toString()))
						key+=(key.length()>0?"\t":"")+value.toString();
					else
						key+=(key.length()>0?"\t":"")+"-";
				}else
				{
					key+=(key.length()>0?"\t":"")+"-";
				}
			}
		}
		return key;
	}
	//auto form the casing tally summary from casing tally
	private void loadCasingTallySummary(CommandBeanTreeNode parentNode, String groupUid) throws Exception{
		
		CasingTallySummary prevSummary =  null;
		double sectionNum = 1;
		double cumLength=0.0;
		double cumWeightForce=0.0;
		double cumVolumeDisplaced=0.0;
		int recordCount = 0;
		Boolean isSectionNumAuto=!this.getGroupingCriteria().contains("sectionNum");
		String currKey=null;
		for (CommandBeanTreeNode child: parentNode.getList().get("CasingTally").values()) {
			Object object = child.getData();
			
			if (object instanceof CasingTally){
				String childKey=this.getGroupCriteriaKey(child);
				
				
				CasingTally currCasingTally = (CasingTally) object;
				
				//do not count the exclude
				if ("1".equalsIgnoreCase(currCasingTally.getExclude()))  continue;
				
				recordCount = recordCount + 1;
				if (prevSummary == null)
				{
					currKey=childKey;
					prevSummary = new CasingTallySummary();
					prevSummary.setCasingTallyUids(currCasingTally.getCasingTallyUid());
					prevSummary.setGroupKey(currKey);
					if (this.fieldsToPopulate!=null)
					{
						for(String field:this.fieldsToPopulate)
						{
							if (PropertyUtils.getProperty(currCasingTally, field)!=null)
							{
								PropertyUtils.setProperty(prevSummary, field, PropertyUtils.getProperty(currCasingTally, field));
							}
						}
					}
					
					if(currCasingTally.getCsgLength() == null)
						prevSummary.setCsgLength(0.00);
					if(currCasingTally.getNumJoints() == null) 
						prevSummary.setNumJoints(0);
					if (isSectionNumAuto)
						prevSummary.setSectionNum(sectionNum);
					if(currCasingTally.getOverallRunningLength() == null)
						prevSummary.setOverallRunningLength(0.00);
					
				}
				else
				{
					if (currKey.equalsIgnoreCase(childKey))
					{
						prevSummary.setCasingTallyUids(prevSummary.getCasingTallyUids()+"\t"+currCasingTally.getCasingTallyUid());
						//compare if need to set the bottom depth
						if (currCasingTally.getSectionBottomMdMsl() != null && prevSummary.getSectionBottomMdMsl() != null)
						{
							if (currCasingTally.getSectionBottomMdMsl().compareTo(prevSummary.getSectionBottomMdMsl()) > 0) prevSummary.setSectionBottomMdMsl(currCasingTally.getSectionBottomMdMsl());
						}
						else if (currCasingTally.getSectionBottomMdMsl() != null && prevSummary.getSectionBottomMdMsl() == null)
						{
							 prevSummary.setSectionBottomMdMsl(currCasingTally.getSectionBottomMdMsl());
						}
						
						//compare if need to set the top depth
						if (currCasingTally.getSectionTopMdMsl() != null && prevSummary.getSectionTopMdMsl() != null)
						{
							if (currCasingTally.getSectionTopMdMsl().compareTo(prevSummary.getSectionTopMdMsl()) < 0) prevSummary.setSectionTopMdMsl(currCasingTally.getSectionTopMdMsl());
						}
						else if (currCasingTally.getSectionTopMdMsl() != null && prevSummary.getSectionTopMdMsl() == null)
						{
							 prevSummary.setSectionTopMdMsl(currCasingTally.getSectionBottomMdMsl());
						}
						
						if(currCasingTally.getNumJoints() != null) prevSummary.setNumJoints(prevSummary.getNumJoints() + currCasingTally.getNumJoints());
						if(currCasingTally.getCsgLength() != null) prevSummary.setCsgLength(prevSummary.getCsgLength() + currCasingTally.getCsgLength());
						if(currCasingTally.getOverallRunningLength() != null) prevSummary.setOverallRunningLength(prevSummary.getOverallRunningLength() + currCasingTally.getOverallRunningLength());
					}
					else
					{
						recordCount = 1;
						//add in the node and set current node as reference
						cumLength+=prevSummary.getCsgLength();
						prevSummary.setCumLength(cumLength);
						if (prevSummary.getWeightForce()!=null)
							cumWeightForce+=prevSummary.getWeightForce();
						prevSummary.setCumWeightForce(cumWeightForce);
						if (prevSummary.getVolumeDisplaced()!=null)
							cumVolumeDisplaced+=prevSummary.getVolumeDisplaced();
						prevSummary.setCumVolumeDisplaced(cumVolumeDisplaced);
						parentNode.addChild(CasingTallySummary.class.getSimpleName(),prevSummary);
						currKey=childKey;
						sectionNum++;
						
						//initiate another new object
						prevSummary = new CasingTallySummary();
						prevSummary.setCasingTallyUids(currCasingTally.getCasingTallyUid());
						prevSummary.setGroupKey(currKey);

						if (this.fieldsToPopulate!=null)
						{
							for(String field:this.fieldsToPopulate)
							{
								if (PropertyUtils.getProperty(currCasingTally, field)!=null)
								{
									PropertyUtils.setProperty(prevSummary, field, PropertyUtils.getProperty(currCasingTally, field));
								}
							}
						}
						
						if(currCasingTally.getCsgLength() == null)
							prevSummary.setCsgLength(0.00);
						if(currCasingTally.getNumJoints() == null) 
							prevSummary.setNumJoints(0);
						if(currCasingTally.getOverallRunningLength() == null)
							prevSummary.setOverallRunningLength(0.00);
						if (isSectionNumAuto)
							prevSummary.setSectionNum(sectionNum);
					}
				}
				double nodeLength = 0.0;
				double nodeOd = 0.0;
				double nodeId = 0.0;
				
				if (prevSummary.getCsgLength()!=null)
				{
					CustomFieldUom thisConverter = new CustomFieldUom(parentNode.getCommandBean(), CasingTally.class, "csgLength");
					thisConverter.setBaseValueFromUserValue(prevSummary.getCsgLength());
					nodeLength = thisConverter.getBasevalue();
				}
				if (prevSummary.getCasingOd()!=null)
				{
					CustomFieldUom thisConverter = new CustomFieldUom(parentNode.getCommandBean(), CasingTally.class, "casingOd");
					thisConverter.setBaseValueFromUserValue(prevSummary.getCasingOd());
					nodeOd = thisConverter.getBasevalue();
				}
				if (prevSummary.getCasingId()!=null)
				{
					CustomFieldUom thisConverter = new CustomFieldUom(parentNode.getCommandBean(), CasingTally.class, "casingId");
					thisConverter.setBaseValueFromUserValue(prevSummary.getCasingId());
					nodeId = thisConverter.getBasevalue();
				}
				
				double avgcsgLength = 0.0;
				avgcsgLength = nodeLength / recordCount;
				CustomFieldUom thisConverter = new CustomFieldUom(parentNode.getCommandBean(), CasingTally.class, "csgLength");
				thisConverter.setBaseValue(avgcsgLength);
				prevSummary.setAvgcsgLength(thisConverter.getConvertedValue());
				child.getDynaAttr().put("ffs", avgcsgLength);
				
				if (nodeLength > 0.0 && nodeOd > 0.0 && nodeId > 0.0)
				{
					double avgVolumeDisplaced = 0.0;
					double avgholeVolume = 0.0;
					
					avgVolumeDisplaced=3.142/4*avgcsgLength*((nodeOd*nodeOd)-(nodeId*nodeId));
					avgholeVolume = (nodeId*nodeId) * avgcsgLength * 3.142 / 4;
					
					thisConverter = new CustomFieldUom(parentNode.getCommandBean(), CasingTally.class, "volumeDisplaced");
					thisConverter.setBaseValue(avgVolumeDisplaced);
					prevSummary.setAvgVolumeDisplaced(thisConverter.getConvertedValue());
					
					thisConverter = new CustomFieldUom(parentNode.getCommandBean(), CasingTally.class, "holeVolume");
					thisConverter.setBaseValue(avgholeVolume);
					prevSummary.setAvgholeVolume(thisConverter.getConvertedValue());
				}
			}
		}
		
		//add in the last node
		if (prevSummary != null) {
			cumLength+=prevSummary.getCsgLength();
			prevSummary.setCumLength(cumLength);
			if (prevSummary.getWeightForce()!=null)
				cumWeightForce+=prevSummary.getWeightForce();
			prevSummary.setCumWeightForce(cumWeightForce);
			if (prevSummary.getVolumeDisplaced()!=null)
				cumVolumeDisplaced+=prevSummary.getVolumeDisplaced();
			prevSummary.setCumVolumeDisplaced(cumVolumeDisplaced);
			parentNode.addChild(CasingTallySummary.class.getSimpleName(),prevSummary);
		}
		
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		if (meta.getTableClass().equals(CasingTallySummary.class)) return true;
		else return false;
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
			
		return null;
	}
	

	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		if(obj instanceof CasingSummary || obj instanceof CasingSection) {
			if (("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_CASING_SECTON_DATA))) || ("2".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_CASING_SECTON_DATA)))) {
				CasingSectionUtils.UpdateReportDailyCasing(commandBean, session);
			}
		}
		if (obj instanceof CasingSection) {
			CasingSection thisCasingSection = (CasingSection) obj;
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "carryOverDepthFromCasing"))) {
				CasingSectionUtils.UpdateLeakOffTestCasingShoeDepth(thisCasingSection, commandBean, node, session);
			}
			CasingSectionUtils.autoLookupReportDailyLOTFIT(commandBean, session);
			
			String dailyUid = session.getCurrentDailyUid();
			String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND dailyUid=:dailyUid";		
			String[] paramsFields = {"dailyUid"};
			String[] paramsValues = {dailyUid};
			List<BharunDailySummary> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			if (lstResult.size() > 0) {
				for(BharunDailySummary thisBharunDailySummary : lstResult) {
					String bharunUid = thisBharunDailySummary.getBharunUid();	
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_SYS_HYDRAULIC_HORSE_POWER))) {
						CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
						BharunUtils.saveSysHydraulicHorsepower(thisConverter, bharunUid, dailyUid, commandBean.getSystemMessage(),session.getCurrentWellboreUid());
					}
					
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_BIT_PRESSURE_LOSS_PERCENTAGE))) {
						CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
						BharunUtils.saveBitPressureLossPercentage(thisConverter, bharunUid, dailyUid, commandBean.getSystemMessage(), session.getCurrentWellboreUid());
					}
				}
			}
			
			String importParameter = thisCasingSection.getImportParameters();
			if(StringUtils.isNotBlank(importParameter)) {
				String[] ip = importParameter.split("[.]");
				if(ip.length == 3) {
					String wbGeometryUid = ip[2];
					String hql = "UPDATE CasingSection SET isDeleted=true WHERE wellboreUid=:wellboreUid AND  (isDeleted IS NULL OR isDeleted = false) AND (importParameters IS NOT NULL AND importParameters != '') AND importParameters NOT LIKE '%." + wbGeometryUid + "'";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(hql, new String[] {"wellboreUid"}, new Object[] { thisCasingSection.getWellboreUid() });
				}
			}
		}
	}

	//set the action of the auto form casing tally summary to false
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		if(StringUtils.equals(targetClass, "CasingTallySummary")) {
			return false;
		}else
		if(StringUtils.equals(targetClass, null)) {
			Object obj = node.getData();
			
			if (obj instanceof CasingTallySummary) {
				if(StringUtils.equals(action, "edit") ||StringUtils.equals(action, "delete"))  return true;
				else return false;
			}
		}
		return true;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if(object instanceof CasingTally) {
			this.setExclude(node, new UserSelectionSnapshot(session));
			CasingTally csgtally = (CasingTally) object;
			Boolean isNewlyCreated = false;
			if(node.getInfo().isNewlyCreated())
			{
				isNewlyCreated = node.getInfo().isNewlyCreated();
			}
			node.getDynaAttr().put("isNewlyCreated", isNewlyCreated);
			if (csgtally.getNumJoints() == null) {
				csgtally.setNumJoints(1);
			}
			Double nodeLength = csgtally.getCsgLength();
			Double nodeWeight = csgtally.getWeight();
			Double nodeOd=csgtally.getCasingOd();
			Double nodeId=csgtally.getCasingId();
			
			if(nodeLength != null){
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "csgLength");
				thisConverter.setBaseValueFromUserValue(nodeLength);
				nodeLength = thisConverter.getBasevalue();
			}
			
			
			if(nodeWeight != null){
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "weight");
				thisConverter.setBaseValueFromUserValue(nodeWeight);
				nodeWeight = thisConverter.getBasevalue();
			}
			
			if(nodeOd != null){
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "casingOd");
				thisConverter.setBaseValueFromUserValue(nodeOd);
				nodeOd = thisConverter.getBasevalue();
			}
			
			if(nodeId != null){
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "casingId");
				thisConverter.setBaseValueFromUserValue(nodeId);
				nodeId = thisConverter.getBasevalue();
			}
			
			Double nodeWeightForce = 0.0;
			if (nodeWeight != null && nodeLength != null) 
			{	
				nodeWeightForce = nodeWeight * nodeLength * 9.81;
				if(nodeWeightForce != null){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "weightForce");
					thisConverter.setBaseValue(nodeWeightForce);
					csgtally.setWeightForce(thisConverter.getConvertedValue());
				}										
			}else{
				csgtally.setWeightForce(null);
			}
			
			Double nodeVolumeDisplaced=0.0;
			Double nodeholeVolume = 0.0;
			if (nodeLength!=null && nodeOd!=null && nodeId!=null)
			{
				nodeVolumeDisplaced=3.142/4*nodeLength*((nodeOd*nodeOd)-(nodeId*nodeId));
				if(nodeVolumeDisplaced != null){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "volumeDisplaced");
					thisConverter.setBaseValue(nodeVolumeDisplaced);
					csgtally.setVolumeDisplaced(thisConverter.getConvertedValue());
				}
				nodeholeVolume = (nodeId*nodeId) * nodeLength * 3.142 / 4;
				if(nodeholeVolume != null){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "holeVolume");
					thisConverter.setBaseValue(nodeholeVolume);
					csgtally.setHoleVolume(thisConverter.getConvertedValue());
				}
			}else{
				csgtally.setVolumeDisplaced(null);
				csgtally.setHoleVolume(null);
			}			
		}
		if(object instanceof CasingSection) {
			CasingSection thisCasingSection= (CasingSection) object;
			String category = thisCasingSection.getCasingCategory();
			
			if(this.tallyType != null)
			{
				thisCasingSection.setTallyType(this.tallyType);
			}
			/*
			if(category!=null){
				if(StringUtils.equals(category, "liner"))
				{
					thisCasingSection.setCasingOd(null);
					thisCasingSection.setCasingId(null);
				}else{
					thisCasingSection.setLinerOd(null);
					thisCasingSection.setLinerId(null);
				}
			}
			*/
		}
		
	}

	
	public void setGroupingCriteria(List<String> groupingCriteria) {
		this.groupingCriteria = groupingCriteria;
	}

	public List<String> getGroupingCriteria() {
		return groupingCriteria;
	}

	public ActionHandler getActionHandler(String action,
			CommandBeanTreeNode node) throws Exception {
		if (node.getData() instanceof CasingTallySummary ) {
			if (action.equals(Action.DELETE))
				return this.deleteSummaryHandler;
			if (action.equals(Action.SAVE))
				return this.saveSummaryHandler;
			return null;
		}
		if (node.getData() instanceof CasingComponent ) {
			if (action.equals(Action.SAVE))
				return new SaveCasingComponentActionHandler();
			return null;
		}
		return null;
	}
	
	private class SaveCasingComponentActionHandler implements ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			Object obj = node.getData();
			String casingsectionId = null;
			
			if (obj instanceof CasingComponent) {
				CasingComponent CasingComponent = (CasingComponent) obj;
				if (node.getParent().getData() instanceof CasingSection) {
					CasingSection casing = (CasingSection) node.getParent().getData();
					casingsectionId = casing.getCasingSectionUid();
				}
				
				if (CasingComponent !=null) {
					CasingTally CasingTally = new CasingTally();
					
					PropertyUtils.copyProperties(CasingTally, CasingComponent);
					CasingTally.setIsCasingComponent(true);
					CasingTally.setCasingSectionUid(casingsectionId);
					CasingTally.setType("casing_component");
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(CasingTally);
				}
			}
			
			return new ActionHandlerResponse(true, false, null, BaseCommandBean.OPERATION_PERFORMED_SAVE_EXISTING);
		}
	}
	
	private class DeleteSummaryHandler implements ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			Object data = node.getData();
			if (data instanceof CasingTallySummary) {
				deleteCasingTallySummary(node);
			}
			return new ActionHandlerResponse(true, false, null, BaseCommandBean.OPERATION_PERFORMED_DELETE);
		}
	}
	
	
	private void deleteCasingTallySummary(CommandBeanTreeNode node) throws Exception {
		CasingTallySummary cts = (CasingTallySummary) node.getData();
		String[] tallyUids=cts.getCasingTallyUids().split("\t");
		for(CommandBeanTreeNode tally:node.getParent().getList().get("CasingTally").values())
		{
			CasingTally ct=(CasingTally)tally.getData();
			String currKey=this.getGroupCriteriaKey(tally);
			if (currKey.equalsIgnoreCase(cts.getGroupKey()) && ArrayUtils.contains(tallyUids, ct.getCasingTallyUid()))
			{
					ct.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ct);	
			}
		}
	}
	private class SaveSummaryHandler implements ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			Object data = node.getData();
			if (data instanceof CasingTallySummary) {
				saveCasingTallySummary(commandBean, node);
			}
			return new ActionHandlerResponse(true, false, null, BaseCommandBean.OPERATION_PERFORMED_SAVE_EXISTING);
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		if(obj instanceof CasingSummary || obj instanceof CasingSection) {
			if (("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_CASING_SECTON_DATA))) || ("2".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_CASING_SECTON_DATA)))) {
				CasingSectionUtils.UpdateReportDailyCasing(commandBean, session);
			}
		}
		if (obj instanceof CasingSection) {
			CasingSectionUtils.autoLookupReportDailyLOTFIT(commandBean, session);

			String dailyUid = session.getCurrentDailyUid();
			//get bharun daily summary for this day to do calc
			String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND dailyUid=:dailyUid";		
			String[] paramsFields = {"dailyUid"};
			String[] paramsValues = {dailyUid};
			List<BharunDailySummary> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
			if (lstResult.size() > 0) {
				for (BharunDailySummary thisBharunDailySummary: lstResult) {
					String bharunUid = thisBharunDailySummary.getBharunUid();					
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_SYS_HYDRAULIC_HORSE_POWER))) {
						CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
						BharunUtils.saveSysHydraulicHorsepower(thisConverter, bharunUid, dailyUid, commandBean.getSystemMessage(),session.getCurrentWellboreUid());
					}
				}
			}
		}
	}
	
	private void saveCasingTallySummary(BaseCommandBean commandBean, CommandBeanTreeNode node) throws Exception {
		CasingTallySummary cts = (CasingTallySummary) node.getData();
		String[] tallyUids=cts.getCasingTallyUids().split("\t");

		for(CommandBeanTreeNode tally:node.getParent().getList().get("CasingTally").values())
			{
				CasingTally ct=(CasingTally)tally.getData();
				String currKey=this.getGroupCriteriaKey(tally);
				if (currKey.equalsIgnoreCase(cts.getGroupKey()) && ArrayUtils.contains(tallyUids, ct.getCasingTallyUid()))
				{
						this.populateSummaryEditableToDetail(commandBean, node, tally);
				}
			}
	}
	private void populateSummaryEditableToDetail(BaseCommandBean commandBean,CommandBeanTreeNode nodeSummary,
			CommandBeanTreeNode nodeTally) throws Exception {
			if (nodeSummary.getData() instanceof CasingTallySummary && nodeTally.getData() instanceof CasingTally)
			{
				CasingTally ct=(CasingTally)nodeTally.getData();
				CasingTallySummary cts=(CasingTallySummary)nodeSummary.getData();
				
				if (this.fieldsToPopulate!=null)
				{
					for( String field:this.fieldsToPopulate)
					{
						if ((this.fieldsToExcludeOnUpdate!=null && !this.fieldsToExcludeOnUpdate.contains(field)) || (this.fieldsToExcludeOnUpdate==null || this.fieldsToExcludeOnUpdate.size()==0))
						{
							PropertyUtils.setProperty(ct, field, PropertyUtils.getProperty(cts, field));
						}
							
					}
				}
				
				Double nodeLength = ct.getCsgLength();
				Double nodeWeight = ct.getWeight();
				Double nodeOd=ct.getCasingOd();
				Double nodeId=ct.getCasingId();
				
				if(nodeLength != null){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "csgLength");
					thisConverter.setBaseValueFromUserValue(nodeLength);
					nodeLength = thisConverter.getBasevalue();
				}
				
				
				if(nodeWeight != null){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "weight");
					thisConverter.setBaseValueFromUserValue(nodeWeight);
					nodeWeight = thisConverter.getBasevalue();
				}
				
				if(nodeOd != null){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "casingOd");
					thisConverter.setBaseValueFromUserValue(nodeOd);
					nodeOd = thisConverter.getBasevalue();
				}
				
				if(nodeId != null){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "casingId");
					thisConverter.setBaseValueFromUserValue(nodeId);
					nodeId = thisConverter.getBasevalue();
				}
				
				Double nodeWeightForce = 0.0;
				if (nodeWeight != null && nodeLength != null) 
				{	
					nodeWeightForce = nodeWeight * nodeLength * 9.81;
					if(nodeWeightForce != null){
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "weightForce");
						thisConverter.setBaseValue(nodeWeightForce);
						ct.setWeightForce(thisConverter.getConvertedValue());
					}										
				}else{
					ct.setWeightForce(null);
				}
				
				Double nodeVolumeDisplaced=0.0;
				if (nodeLength!=null && nodeOd!=null && nodeId!=null)
				{
					nodeVolumeDisplaced=3.142/4*nodeLength*((nodeOd*nodeOd)-(nodeId*nodeId));
					if(nodeVolumeDisplaced != null){
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CasingTally.class, "volumeDisplaced");
						thisConverter.setBaseValue(nodeVolumeDisplaced);
						ct.setVolumeDisplaced(thisConverter.getConvertedValue());
					}				
				}else{
					ct.setVolumeDisplaced(null);
				}
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ct);
			}
	}

	public void setFieldsToPopulate(List<String> fieldsToPopulate) {
		this.fieldsToPopulate = fieldsToPopulate;
	}

	public List<String> getFieldsToPopulate() {
		return fieldsToPopulate;
	}

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);		

		for (Field field : CasingTally.class.getDeclaredFields()) {
			thisConverter.setReferenceMappingField(CasingTally.class, field.getName());
			if (thisConverter.isUOMMappingAvailable()) {
				commandBean.setCustomUOM(CasingComponent.class, field.getName(), thisConverter.getUOMMapping());
			}
		}
		
		if (fieldsToPopulate!=null)
		{
			for(String field : fieldsToPopulate)
			{
				thisConverter.setReferenceMappingField(CasingTally.class, field);
				if (thisConverter.isUOMMappingAvailable()) {
					commandBean.setCustomUOM(CasingTallySummary.class, field, thisConverter.getUOMMapping());
				}
			}
		}
		thisConverter.setReferenceMappingField(CasingTally.class, "csgLength");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "cumLength", thisConverter.getUOMMapping());
		}
		thisConverter.setReferenceMappingField(CasingTally.class, "weightForce");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "cumWeightForce", thisConverter.getUOMMapping());
		}
		thisConverter.setReferenceMappingField(CasingTally.class, "volumeDisplaced");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "cumVolumeDisplaced", thisConverter.getUOMMapping());
		}
		thisConverter.setReferenceMappingField(CasingTally.class, "csgLength");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "avgcsgLength", thisConverter.getUOMMapping());
		}
		thisConverter.setReferenceMappingField(CasingTally.class, "volumeDisplaced");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "avgVolumeDisplaced", thisConverter.getUOMMapping());
		}
		thisConverter.setReferenceMappingField(CasingTally.class, "holeVolume");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "avgholeVolume", thisConverter.getUOMMapping());
		} 
		thisConverter.dispose();
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		UserSelectionSnapshot userSelection= new UserSelectionSnapshot(UserSession.getInstance(request));
		if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_LOT_FIT))){
			if(targetCommandBeanTreeNode != null){
				if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(CasingSection.class)) {
					if("casingOd".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
						//stockCodeChanged(request, commandBean, targetCommandBeanTreeNode);
						Double casingOd= Double.valueOf((targetCommandBeanTreeNode.getValue("casingOd").toString()));						
						if(casingOd>0.00){						
						LeakOffTestUtil.populateLOTFIT(commandBean, targetCommandBeanTreeNode.getData(), userSelection, "LOT","lot",casingOd);
						LeakOffTestUtil.populateLOTFIT(commandBean, targetCommandBeanTreeNode.getData(), userSelection, "FIT","fit",casingOd);					
						}
					}
				}
			}
		}		
	}

	public void setFieldsToExcludeOnUpdate(List<String> fieldsToExcludeOnUpdate) {
		this.fieldsToExcludeOnUpdate = fieldsToExcludeOnUpdate;
	}

	public List<String> getFieldsToExcludeOnUpdate() {
		return fieldsToExcludeOnUpdate;
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);
		
		if (object instanceof CasingSection) {
			CasingSection casingSection = (CasingSection) object;
			String prevDailyUid = userSelection.getDailyUid();
			//gwp setting hole size
			if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_HOLE_SIZE))){
				
				
				
				if (StringUtils.isNotBlank(prevDailyUid)){
					Daily prevDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(prevDailyUid);
					QueryProperties qp2 = new QueryProperties();
					qp2.setUomConversionEnabled(false);
					String strSql4 = "FROM Activity WHERE (isDeleted = false OR isDeleted IS NULL) AND dailyUid = :selectedDailyUid AND depthMdMsl IS NOT NULL " +
							"AND (isSimop = false OR isSimop IS NULL) AND (dayPlus IS NULL OR dayPlus = '0') and (isOffline = false OR isOffline IS NULL) ORDER BY dayPlus DESC, endDatetime DESC, startDatetime DESC ";
					String[] paramsFields4 = {"selectedDailyUid"};
					String[] paramsValues4 = {prevDaily.getDailyUid()};
					List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4,qp2);
					//CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "depthMdMsl");	
					if (lstResult4.size()>0){
							Activity activity = (Activity) lstResult4.get(0);														
							if(activity.getHoleSize()!=null)
							{
								casingSection.setOpenHoleId(activity.getHoleSize());
							}else
							{
								casingSection.setOpenHoleId(null);
							}
					}
					else{
						casingSection.setOpenHoleId(null);
					}
				}
			}
				
			//gwp setting hole depth
			if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_HOLE_DEPTH))){
				
									
				if (StringUtils.isNotBlank(prevDailyUid)){
					Daily prevDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(prevDailyUid);
					QueryProperties qp2 = new QueryProperties();
					qp2.setUomConversionEnabled(false);
					String strSql4 = "FROM ReportDaily WHERE (isDeleted = false OR isDeleted IS NULL) AND dailyUid = :selectedDailyUid ";
					String[] paramsFields4 = {"selectedDailyUid"};
					String[] paramsValues4 = {prevDaily.getDailyUid()};
					List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4,qp2);
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "depthMdMsl");	
					if (lstResult4.size()>0){
							ReportDaily reportDaily = (ReportDaily) lstResult4.get(0);															
							//MD
							if(reportDaily.getDepthMdMsl()!=null){
							thisConverter.setBaseValue(reportDaily.getDepthMdMsl());
							casingSection.setOpenHoleDepthMdMsl(thisConverter.getConvertedValue());
							}
							
							//TVD
							if(reportDaily.getDepthTvdMsl()!=null){
							thisConverter.setBaseValue(reportDaily.getDepthTvdMsl());
							casingSection.setOpenHoleDepthTvdMsl(thisConverter.getConvertedValue());
							}
					}
					else{
						casingSection.setOpenHoleDepthMdMsl(null);
						casingSection.setOpenHoleDepthTvdMsl(null);
					}
				}
			}
				
			//gwp setting mud data
			if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_MUD_TYPE_WEIGHT))){
									
				if (StringUtils.isNotBlank(prevDailyUid)){
					Daily prevDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(prevDailyUid);
					QueryProperties qp2 = new QueryProperties();
					qp2.setUomConversionEnabled(false);
					String strSql4 = "FROM MudProperties WHERE (isDeleted = false OR isDeleted IS NULL) AND dailyUid = :selectedDailyUid ORDER BY reportNumber desc ";
					String[] paramsFields4 = {"selectedDailyUid"};
					String[] paramsValues4 = {prevDaily.getDailyUid()};
					List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4,qp2);
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, MudProperties.class, "mudWeight");	
					if (lstResult4.size()>0){
						MudProperties mudProperties = (MudProperties) lstResult4.get(0);															
							//mud type
							if(mudProperties.getMudType()!=null){
								casingSection.setMudtype(getMudTypeName(mudProperties.getMudType(),userSelection));
							}else
							{
								casingSection.setMudtype(null);
							}
							
							//mud weight
							if(mudProperties.getMudWeight()!=null)
							{
								thisConverter.setBaseValue(mudProperties.getMudWeight());
								casingSection.setMudweight(thisConverter.getConvertedValue());
							}
							else
							{
								casingSection.setMudweight(null);
							}
					}
					else{
						casingSection.setMudtype(null);
						casingSection.setMudweight(null);
					}
				}
			}
				
			//gwp setting FIT/LOT
			/*if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_LOT_FIT))){
				Double casingOd = casingSection.getCasingId();
				if(casingOd!=null){
					if (StringUtils.isNotBlank(prevDailyUid)){
						
						QueryProperties qp2 = new QueryProperties();
						qp2.setUomConversionEnabled(false);					
						this.populateLOTFIT(commandBean, object, userSelection, "LOT","lot",casingOd);
						this.populateLOTFIT(commandBean, object, userSelection, "FIT","fit",casingOd);					
						}
						else{
							casingSection.setMudtype(null);
							casingSection.setMudweight(null);
						}					
				}
			}*/
		}
	}
	
	private String getMudTypeName(String mudShortCode, UserSelectionSnapshot userSelection) throws Exception {		
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://mud_properties.mud_type?key=code&value=label", userSelection, null);
		if (lookupMap != null) {
			LookupItem lookupItem = lookupMap.get(mudShortCode);
			if (lookupItem != null) {
				return (String) lookupItem.getValue();
			}
		}
		
		return mudShortCode;
	}

	
}
