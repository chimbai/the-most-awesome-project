package com.idsdatanet.d2.drillnet.activity;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ActivityEditorActivityCodeFilter implements LookupHandler {

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {		
		
		//TO FILTER PHASE CODE IN SELECTED DATE RATHER THAN LOAD ALL FROM LOOKUP_PHASE_CODE		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String selectedStartDailyUid  = (String) commandBean.getRoot().getDynaAttr().get("startdate");
		String selectedEndDailyUid  = (String) commandBean.getRoot().getDynaAttr().get("enddate");
		String selectedClassCode  = (String) commandBean.getRoot().getDynaAttr().get("classCodeLookup");
		
		if (selectedStartDailyUid == null || StringUtils.isBlank(selectedStartDailyUid))
		{
			String strSql3 ="FROM Daily WHERE (isDeleted=false OR isDeleted is null) AND operationUid = :thisOperationUid order by dayDate ASC";
			
			String[] paramNames3 = {"thisOperationUid"};
			Object[] paramValues3 = {UserSession.getInstance(request).getCurrentOperationUid()};
			List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramNames3, paramValues3);
			
			if (lstResult3.size() > 0){
				Daily thisDaily = (Daily) lstResult3.get(0);
				selectedStartDailyUid = thisDaily.getDailyUid();
			}
		}
		
		if (selectedEndDailyUid == null || StringUtils.isBlank(selectedEndDailyUid))
		{
			String strSql3 ="FROM Daily WHERE (isDeleted=false OR isDeleted is null) AND operationUid = :thisOperationUid order by dayDate DESC";
			
			String[] paramNames3 = {"thisOperationUid"};
			Object[] paramValues3 = {UserSession.getInstance(request).getCurrentOperationUid()};
			List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramNames3, paramValues3);
			
			if (lstResult3.size() > 0){
				Daily thisDaily = (Daily) lstResult3.get(0);
				selectedEndDailyUid = thisDaily.getDailyUid();
			}
		}
		
		Daily startDayDate = ApplicationUtils.getConfiguredInstance().getCachedDaily(selectedStartDailyUid);
		Daily endDayDate = ApplicationUtils.getConfiguredInstance().getCachedDaily(selectedEndDailyUid);
		
		if (startDayDate == null) return null;
		Date startDate = startDayDate.getDayDate();
		
		if (endDayDate == null) return null;
		Date endDate = endDayDate.getDayDate();
		
		String strSql = "SELECT a FROM Activity a,Daily d WHERE a.operationUid = :thisOperationUid " +
				"AND a.dailyUid = d.dailyUid " +
				"AND (a.isDeleted=false or a.isDeleted is null) AND (d.isDeleted=false or d.isDeleted is null) " +
				"AND a.phaseCode IS NOT NULL AND (d.dayDate >=:startDate AND d.dayDate <=:endDate) " +
				(StringUtils.isNotBlank(selectedClassCode) ? "AND a.classCode=:selectedClassCode " : "") +
				"GROUP BY a.phaseCode";
		
		String[] paramNames = {"thisOperationUid", "startDate", "endDate"};
		Object[] paramValues = {UserSession.getInstance(request).getCurrentOperationUid(), startDate, endDate};
		
		if(StringUtils.isNotBlank(selectedClassCode)) {
			paramNames = new String[] {"thisOperationUid", "startDate", "endDate", "selectedClassCode"};
			paramValues = new Object[] {UserSession.getInstance(request).getCurrentOperationUid(), startDate, endDate, selectedClassCode};
		}
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
		
		if (lstResult.size() > 0){
			
			for(Object objResult: lstResult){
				Activity thisActivity = (Activity) objResult;
				
				if (thisActivity != null && StringUtils.isNotBlank(thisActivity.toString())) {
					
					String strSql2 = "FROM LookupPhaseCode WHERE (isDeleted=false or isDeleted is null) AND shortCode = :thisPhaseCode AND operationCode = :thisOperationCode";
					String[] paramsFields2 = {"thisPhaseCode","thisOperationCode"};
					Object[] paramsValues2 = {thisActivity.getPhaseCode(),userSelection.getOperationType()};
					
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
					
					if (lstResult2.size() > 0){
						LookupPhaseCode thisLookupPhaseCode = (LookupPhaseCode) lstResult2.get(0);
						
						String key = thisLookupPhaseCode.getShortCode().toString();
						String value = thisLookupPhaseCode.getName().toString() + " (" + thisLookupPhaseCode.getShortCode().toString() + ")";

						LookupItem item = new LookupItem(key, value);
						result.put(key, item);
					}else{						
						if(thisActivity.getPhaseCode() != null || StringUtils.isNotEmpty(thisActivity.getPhaseCode().toString())){
							String key = thisActivity.getPhaseCode();
							String value = "(" + thisActivity.getPhaseCode() + ")";
							
							LookupItem item = new LookupItem(key, value);
							result.put(key, item);
						}
					}
				}
			}
		}
		
		return result;
	}
}
