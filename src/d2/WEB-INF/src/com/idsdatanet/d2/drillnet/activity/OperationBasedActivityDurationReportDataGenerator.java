package com.idsdatanet.d2.drillnet.activity;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

import java.util.Date;
import java.util.List;


/**
 * 
 * @author ltan
 * 
 * This bean is use to mine activity duration base on selected operation
 *
 */
public class OperationBasedActivityDurationReportDataGenerator implements ReportDataGenerator {
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		// get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String currentOperationUid = "";
		
		if(StringUtils.isNotBlank(userContext.getUserSelection().getOperationUid())) {
			currentOperationUid = userContext.getUserSelection().getOperationUid();
				//Use the strOperationUid to retrieve all of the activities under that selected operation
				// Currently returning nothing when should be returning 6 items.
				String strSql = "SELECT a.rootCauseCode, SUM(a.activityDuration) as totalDuration, a.classCode, a.internalClassCode FROM Daily d, Activity a " +
					"WHERE (d.isDeleted = false or d.isDeleted is null) AND (a.isDeleted = false or a.isDeleted is null) AND d.dailyUid = a.dailyUid " +
					"AND d.dayDate <= :userDate AND a.operationUid=:operationUid AND (a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL))" +
					"GROUP BY a.classCode,a.internalClassCode, a.rootCauseCode";
				String[] paramsFields = {"userDate","operationUid"};
				Object[] paramsValues = {todayDate,currentOperationUid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				if (lstResult!=null && lstResult.size() > 0){
					//unit converter to convert + format value
					CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
					for(Object objResult: lstResult){
						//due to result is in array, cast the result to array first and read 1 by 1
						Object[] objDuration = (Object[]) objResult;
						
						//default class to program, cause some record might have no code
						String rootCauseCode = "N/A";
						String classCode = "N/A";
						String internalClassCode ="N/A";
						Double totalDuration = 0.0;						
						
						if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())) rootCauseCode = objDuration[0].toString();
						
						if (objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())){
							totalDuration = Double.parseDouble(objDuration[1].toString());
							
							//assign unit to the value base on activity.activityDuration, this will do auto convert to user display unit + format
							thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
							thisConverter.setBaseValue(totalDuration);
							totalDuration = thisConverter.getConvertedValue();					
						}
						
						if (objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())) classCode = objDuration[2].toString();
						if (objDuration[3] != null && StringUtils.isNotBlank(objDuration[3].toString())) internalClassCode = objDuration[3].toString();
						
						ReportDataNode thisReportNode = reportDataNode.addChild("OperationBasedActivityDuration");
						thisReportNode.addProperty("rootCauseCode", rootCauseCode);
						thisReportNode.addProperty("classCode", classCode);
						thisReportNode.addProperty("internalClassCode", internalClassCode);
						thisReportNode.addProperty("duration", totalDuration.toString());	
					}
					
				}
			
			
			
		}
	}
}