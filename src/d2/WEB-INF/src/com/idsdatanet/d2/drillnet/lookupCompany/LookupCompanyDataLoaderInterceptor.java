package com.idsdatanet.d2.drillnet.lookupCompany;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LookupCompanyDataLoaderInterceptor implements DataLoaderInterceptor, CommandBeanListener, InitializingBean{
	
	public void afterPropertiesSet() throws Exception {
    }
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		String customCondition = "";
		String dynaValue  = (String) commandBean.getRoot().getDynaAttr().get("companyName");

		if (dynaValue!=null) {
			if (!"".equals(dynaValue.toString())) {
				
					customCondition += " (companyName like :companyName)";
					query.addParam("companyName", "%"+dynaValue+"%");
				
			}
		}
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		if (!root.getDynaAttr().containsKey("companyName"))
			root.getDynaAttr().put("companyName",null);
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
