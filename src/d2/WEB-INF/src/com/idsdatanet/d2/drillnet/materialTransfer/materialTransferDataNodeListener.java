package com.idsdatanet.d2.drillnet.materialTransfer;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.InventoryTransaction;
import com.idsdatanet.d2.core.model.InventoryTransactionDetail;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class materialTransferDataNodeListener extends EmptyDataNodeListener {

	@Override
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		
		if (node.getData() instanceof InventoryTransaction) {
			InventoryTransaction inventoryTransaction = (InventoryTransaction) node.getData();
			inventoryTransaction.setStatus("CLOSED");
			inventoryTransaction.setTransactionType("MATERIAL_TRANSFER");
			//inventoryTransaction.setDeliveryDatetime(session.getCurrentDaily().getDayDate());
		} else if (node.getData() instanceof InventoryTransactionDetail) {
			InventoryTransactionDetail inventoryTransactionDetail = (InventoryTransactionDetail) node.getData();
			inventoryTransactionDetail.setInventoryMasterUid("");
		}
		
	}

	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof InventoryTransaction) {
			InventoryTransaction inventoryTransaction = (InventoryTransaction) node.getData();
			
			Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, userSelection.getDailyUid());
			if (daily!=null) {
				//inventoryTransaction.setDeliveryDatetime(daily.getDayDate());
			}
		}
	}

}
