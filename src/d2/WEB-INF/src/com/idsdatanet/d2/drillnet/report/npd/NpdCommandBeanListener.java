package com.idsdatanet.d2.drillnet.report.npd;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.web.helper.FileDownloadWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.core.DepotConstants;
import com.idsdatanet.depot.core.query.QueryResult;
import com.idsdatanet.depot.core.util.ServerPropertiesUtils;
import com.idsdatanet.depot.npd.NpdManager;
import com.idsdatanet.depot.npd.OperationCodeConversion;
import com.idsdatanet.depot.npd.OperationMappingEntry;

public class NpdCommandBeanListener extends EmptyCommandBeanListener {
	private static final int WRITE_BUFFER_SIZE = 200 * 1024;
	private static final String datePlaceHolder = "{date}";
	private final String CLOSE_BUTTON = "<div align='center' style='margin:10px;' class='buttons'><input type='button' onclick='tb_remove()' value='close' /></div>";
	private String outputFileDateFormat = null;
	private String outputFileNameFormat = null;
	private NpdManager npdManager = null;
	private MailEngine mailEngine = null;
	private List<String> addressList = null;
	private String npdVersion = null;
	private boolean isUseClientMVC = false;
	
	public void setNpdManager(NpdManager npdManager) {
		this.npdManager = npdManager;
	}
	
	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}
	
	public void setAddressList(List<String> addressList) {
		this.addressList = addressList;
	}
	
	public void setOutputFileDateFormat(String outputFileDateFormat) {
		this.outputFileDateFormat = outputFileDateFormat;
	}
	
	public void setOutputFileNameFormat(String outputFileNameFormat) {
		this.outputFileNameFormat = outputFileNameFormat;
	}
	
	public void setNpdVersion(String npdVersion) {
		this.npdVersion = npdVersion;
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response,	BaseCommandBean commandBean, String invocationKey) throws Exception{
		String versionKind = (String)request.getParameter("versionKind");
		String action = request.getParameter("action");
				
		UserSession session = UserSession.getInstance(request);
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
		this.isUseClientMVC = commandBean.getUseClientMVCRenderer();
		String npdXmlOut = npdManager.getDrillReport(request, userSelection, this.npdVersion, versionKind);
		// preview
		if (action.equals("preview")) {
			String npdHtml = npdManager.getPreviewForDrillReport(npdXmlOut);
			this.previewFile(response, npdHtml);
		// download
		} else if (action.equals("download")) {
			this.downloadFile(request, response, userSelection, npdXmlOut);
		// send to NPD Server
		} else if (action.equals("send")) {
			this.sendDrillReport(npdXmlOut, commandBean, request, response);
		}
	}
	
	private void previewFile(HttpServletResponse response, String npdHtmlContent) throws Exception {
		String finalHtmlContent = "<div id='npdwrapper'>" + (!this.isUseClientMVC?CLOSE_BUTTON:"") + npdHtmlContent + (!this.isUseClientMVC?CLOSE_BUTTON:"") + "</div>";
		OutputStream out = response.getOutputStream();
		
		InputStream in = new ByteArrayInputStream(finalHtmlContent.getBytes("UTF-8"));
		
		byte[] buffer = new byte[WRITE_BUFFER_SIZE];
	    int bytesRead = 0;
	    while ((bytesRead = in.read(buffer)) >= 0) {
	        out.write(buffer, 0, bytesRead);
	    }
	    if(out != null) {
	    	out.flush();
	    	out.close();
	    }
	    if(in != null) {
	    	in.close();
	    }
	}
	
	private void downloadFile(HttpServletRequest request, HttpServletResponse response, UserSelectionSnapshot userSelection, String npdXmlContent) throws Exception {
		String dailyUid = userSelection.getDailyUid();
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		
		if (daily != null) {
			DateFormat format = new SimpleDateFormat(this.outputFileDateFormat);
			Date date = daily.getDayDate();
			if (date == null) date = new Date();
			
			String dayDate = format.format(date);
			
			String FileName = "";
			if (this.outputFileNameFormat.indexOf(datePlaceHolder) >= 0) {
				FileName = this.outputFileNameFormat.replace(datePlaceHolder, dayDate);
			}
			byte[] out = npdXmlContent.getBytes();
			InputStream in = new ByteArrayInputStream(out);

			FileDownloadWriter.writeFileOutputFromStream(request, response, in, true, FileName, null,out.length);
			
		} else {
			throw new Exception("Daily object not found.");
		}
	}
	
	public void sendDrillReport(String npdXmlOut, BaseCommandBean commandBean, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// get from config.ini
		String fileName = ApplicationConfig.getConfiguredInstance().getServerRootPath() + "WEB-INF/config/npd/" + ApplicationConfig.getConfiguredInstance().getNpdKeyStoreFilename();
		System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");
		System.setProperty("javax.net.ssl.keyStore", fileName);
		System.setProperty("javax.net.ssl.keyStorePassword", ApplicationConfig.getConfiguredInstance().getNpdKeyStorePassword());
		
		ImportExportServerProperties serverProperties = ServerPropertiesUtils.getConfiguredInstance().getServerPropertiesByServiceName(DepotConstants.NPD, DepotConstants.NPD_SERVICE_NAME);
		QueryResult queryResult = npdManager.sendDrillReportToNPDServer(serverProperties, npdXmlOut);
		
		StringWriter htmlOutput = new StringWriter();
		htmlOutput.append("<table height=\"100%\" width=\"100%\" cellpadding=\"5\" cellspacing=\"5\">");
		if (queryResult.hasError()) {
			for (String errorMsg : queryResult.getErrorMessages()) {
				htmlOutput.append("<tr class=\"error\">" + errorMsg + "</tr>");
			}
		} else {
			for (String infoMsg : queryResult.getInfoMessages()) {
				htmlOutput.append("<tr class=\"info\">" + infoMsg + "</tr>");
			}
		}
		htmlOutput.append("</table>");
		
		String finalHtmlContent = "<div id='npdwrapper'>" + CLOSE_BUTTON + htmlOutput.toString() + CLOSE_BUTTON + "</div>";
		OutputStream out = response.getOutputStream();
		InputStream in = new ByteArrayInputStream(finalHtmlContent.getBytes("UTF-8"));
		
		byte[] buffer = new byte[WRITE_BUFFER_SIZE];
	    int bytesRead = 0;
	    while ((bytesRead = in.read(buffer)) >= 0) {
	        out.write(buffer, 0, bytesRead);
	    }
	    if(out != null) {
	    	out.flush();
	    	out.close();
	    }
	    if(in != null) {
	    	in.close();
	    }
	}
	
	/**
	 * 
	 */
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		List<OperationMappingEntry> uniqueMappings = new ArrayList<OperationMappingEntry>();
		List<OperationMappingEntry> allMappings = new ArrayList<OperationMappingEntry>();
		List<Activity> allActivities = new ArrayList<Activity>();
		
		Map<String, Map<String, CommandBeanTreeNode>> map = commandBean.getRoot().getList();
		Map<String, CommandBeanTreeNode> actMap = map.get("Activity");
		
		for (CommandBeanTreeNode node:actMap.values()) {
			
			Activity act = (Activity)node.getData();
			String mainOp = "";
			if((String)node.getDynaAttr().get("mainop")!= null) {
				mainOp = (String)node.getDynaAttr().get("mainop");
			}
			String subOp = "";
			if ((String)node.getDynaAttr().get("subop") != null) {
				subOp = (String)node.getDynaAttr().get("subop");
			}
			boolean isUnique = true;
			
			for (OperationMappingEntry e:uniqueMappings){
				if (e.isMatch(act)) {
					if (!mainOp.equals(e.getMainOp()) || !subOp.equals(e.getSubOp())) {
						// codes are the identical but different MainOp / SubOp selected, return warning to user
						
						String desc = act.getActivityDescription();
						if (desc.length() > 40) {
							desc = desc.substring(0, 40) + "...";
						}
						
						commandBean.getSystemMessage().addInfo
							("The Activity '" + desc + "'\n" +
							"has identical Codes to another Activity in the list,\n" +
							"however the MainOp / SubOp selections are different.\n\n" +
							"Please make identical selections for these Activities,\n" +
							"or revise the Codes in the 'Activity' screen.");
						return;
					
					} else {
						// a mapping with identical codes and MainOp/SubOp already exists, so ignore
						isUnique = false;
					}
				}
			}
			
			// add Activity and Mapping to separate lists (linked by same index)
			OperationMappingEntry entry = new OperationMappingEntry(mainOp, subOp);
			entry.addCondition("classid", act.getClassCode());
			entry.addCondition("phaseid", act.getPhaseCode());
			entry.addCondition("operid", act.getTaskCode()); 
			entry.addCondition("rcid", act.getRootCauseCode());
			entry.addCondition("stageid", act.getJobTypeCode());
			
			allActivities.add(act);
			allMappings.add(entry);
			
			// add Mapping to unique list if applicable
			if (isUnique) {
				uniqueMappings.add(entry);
			}
		}
		
		// all Activities validated, save new mappings to existing mapping list object
		OperationCodeConversion.getConfiguredInstance().addMappings(uniqueMappings);
		
		// email these codes/selections to the recipient list
		UserSession session = UserSession.getInstance(request);
		this.emailCodesToVerify(allActivities, allMappings, session.getUserFirstName(), request);
		
	}
	
	/**
	 * Sends an email of these new mappings to the defined recipient list for verification,
	 * so that mappings will eventually become permanent by manually adding to 'npdOperationMapping.xml'.
	 * 
	 * *** The email (HTML) content for new activities/mappings are currently being generated in this method.
	 * *** In the future for full customisation, it should be re-coded to generate inside 'npdNewCodesEmail.ftl' instead.
	 * 
	 * @param unmappedActs
	 * @param newMappings
	 * @param userName
	 * @return
	 * @throws Exception
	 * @author Jackey
	 */
	private void emailCodesToVerify(List<Activity> unmappedActs, List<OperationMappingEntry> newMappings,
			String userName, HttpServletRequest request) throws Exception {
		
		Map<String, Object> params = new HashMap<String, Object>();
		StringWriter details = new StringWriter();
		details.append("----------------------------------------------------------<br/>");
		
		// iterate new mapping entries and add to description
		for (int i = 0; i < unmappedActs.size(); i++) {
			
			Activity a = unmappedActs.get(i);
			OperationMappingEntry e = newMappings.get(i);
			
			if (e.getConditionCount() > 0) {
				// append Activity description
				details.append("<table width='100%'>");
				details.append("<tr width='50%'>Activity Description:</tr>");
				details.append("<tr width='100%'>" + a.getActivityDescription() + "</tr>");
				details.append("</table>");
				
				// append Codes
				details.append("<table width='100%'>");
				if (e.getClassCode() != null) details.append("<tr width='100%'><td width='25%'>Class Code:</td><td>" + e.getClassCode() + "</td></tr>");
				if (e.getPhaseCode() != null) details.append("<tr width='100%'><td width='25%'>Phase Code:</td><td>" + e.getPhaseCode() + "</td></tr>");
				if (e.getJobTypeCode() != null) details.append("<tr width='100%'><td width='25%'>Job/Operation Code:</td><td>" + e.getJobTypeCode() + "</td></tr>");
				if (e.getTaskCode() != null) details.append("<tr width='100%'><td width='25%'>Task Code:</td><td>" + e.getTaskCode() + "</td></tr>");
				if (e.getRootCauseCode() != null) details.append("<tr width='100%'><td width='25%'>Root Cause Code:</td><td>" + e.getRootCauseCode() + "</td></tr>");
				details.append("</table>");
				
				// append selected MainOp / SubOps
				details.append("<table width='100%'>");
				details.append("<tr width='100%'><td width='25%'>Selected Main Op:</td><td>" + e.getMainOp() + "</td></tr>");
				details.append("<tr width='100%'><td width='25%'>Selected Sub Op:</td><td>" + e.getSubOp() + "</td></tr>");
				details.append("</table>");
				
				details.append("----------------------------------------------------------<br/>");
			}
		}
		
		String supportAddr = ApplicationConfig.getConfiguredInstance().getSupportEmail();
		String subject = "NPD Report - IDS Verification of New Mappings";
		
		//GET USER EMAIL AND SEND EMAIL NOTIFICATION
		UserSession session = UserSession.getInstance(request);
		Object objUser = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(User.class, session.getUserUid());
		User thisUser = (User) objUser;
		String[] recipientArray = null; 
		int i;
		
		if(StringUtils.isNotBlank(thisUser.getEmail()))
		{
			recipientArray = new String[this.addressList.size() + 1];
			for (i = 0; i < this.addressList.size(); i++) {
				recipientArray[i] = this.addressList.get(i);
			}
			recipientArray[i] = thisUser.getEmail();
		}
		else
		{
			recipientArray = new String[this.addressList.size()];
			for (i = 0; i < this.addressList.size(); i++) {
				recipientArray[i] = this.addressList.get(i);
			}
		}
			
		String strHeader = "";
		String strSiteUrl = "";
		
		//GET LOGIN URL
		String strSql = "SELECT DISTINCT loginFormUrl FROM SysLogLogin WHERE sessionId = :thisSessionId";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisSessionId", session.getCurrentSessionId());		
		if (!lstResult.isEmpty())
		{
			Object a = (Object) lstResult.get(0);
			if (a != null)  strSiteUrl = a.toString();
		}
			
		//GET OPERATION NAME AND DAY DATE
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(session.getCurrentDailyUid());
		strHeader = session.getCurrentOperation().getOperationName() + ", " + daily.getDayDate();
		
		params.put("username", userName);
		params.put("details", details);
		params.put("header", strHeader);
		params.put("siteurl", strSiteUrl);
				
		//this.mailEngine.sendMail(recipientArray, supportAddr, supportAddr, subject, details.toString());
		mailEngine.sendMail(recipientArray, supportAddr, supportAddr, subject, mailEngine.generateContentFromTemplate("core/npdNewCodesEmail.ftl", params));
	}
}
