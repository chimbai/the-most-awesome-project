package com.idsdatanet.d2.drillnet.leakOffTest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LeakOffTestCommandBeanListener extends EmptyCommandBeanListener{
	
	private Boolean autoPopulateTestDepth = false;
	private Boolean retrieveCasingRecordByWellboreUid = false;
	
	public void setAutoPopulateTestDepth(Boolean value) {
		this.autoPopulateTestDepth = value;
	}

	public void setRetrieveCasingRecordByWellboreUid(Boolean value) {
		this.retrieveCasingRecordByWellboreUid = value;
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(LeakOffTest.class)) {
				if("casingSize".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					casingSizeChanged(request, commandBean, targetCommandBeanTreeNode);
				}
			}
		}
	}
	
	private void casingSizeChanged(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		LeakOffTest thisLeakOffTest = (LeakOffTest) node.getData();
		
		UserSession userSession = UserSession.getInstance(request);
		String operationUid = userSession.getCurrentOperationUid();
		String wellboreUid = userSession.getCurrentWellboreUid();
		
		//show the correct unit as well
		Double casingSize = thisLeakOffTest.getCasingSize();
		if (casingSize != null) {
			if(this.retrieveCasingRecordByWellboreUid){
				String strSql = "SELECT shoeTopMdMsl, shoeTopTvdMsl FROM "+
				"CasingSection WHERE wellboreUid =:wellboreUid " +
				"AND (isDeleted = false or isDeleted is null) AND casingOd = :thisCasingSize";
				
				String[] paramsFields = {"wellboreUid", "thisCasingSize"};
		 		Object[] paramsValues = new Object[2];
				paramsValues[0] = wellboreUid;
				paramsValues[1] = casingSize;
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				
				if (lstResult.size() > 0) {
					
					Object[] a = (Object[]) lstResult.get(0);
					
					CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(), LeakOffTest.class, "shoeDepthMdMsl");
					if(a[0] != null) {
						thisConverter.setBaseValue(Double.parseDouble(a[0].toString()));
						thisLeakOffTest.setShoeDepthMdMsl(thisConverter.getConvertedValue());
					} 
					
					if(a[1] != null) {
						thisConverter.setReferenceMappingField(LeakOffTest.class, "shoeTvdMsl");
						thisConverter.setBaseValue(Double.parseDouble(a[1].toString()));
						thisLeakOffTest.setShoeTvdMsl(thisConverter.getConvertedValue());
						//set Test Depth to be autopopulated from shoeTvdMsl when casing size is selected
						if(this.autoPopulateTestDepth){	
							thisLeakOffTest.setTestDepthTvdMsl(thisConverter.getConvertedValue());
						}
					}
				}
			}
			else
			{
				String strSql = "SELECT shoeTopMdMsl, shoeTopTvdMsl FROM "+
				"CasingSection WHERE operationUid =:thisOperationUid " +
				"AND (isDeleted = false or isDeleted is null) AND casingOd = :thisCasingSize";
				
				String[] paramsFields = {"thisOperationUid", "thisCasingSize"};
		 		Object[] paramsValues = new Object[2];
				paramsValues[0] = operationUid;
				paramsValues[1] = casingSize;
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				
				if (lstResult.size() > 0) {
					
					Object[] a = (Object[]) lstResult.get(0);
					
					CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(), LeakOffTest.class, "shoeDepthMdMsl");
					if(a[0] != null) {
						thisConverter.setBaseValue(Double.parseDouble(a[0].toString()));
						thisLeakOffTest.setShoeDepthMdMsl(thisConverter.getConvertedValue());
					} 
					
					if(a[1] != null) {
						thisConverter.setReferenceMappingField(LeakOffTest.class, "shoeTvdMsl");
						thisConverter.setBaseValue(Double.parseDouble(a[1].toString()));
						thisLeakOffTest.setShoeTvdMsl(thisConverter.getConvertedValue());
						//set Test Depth to be autopopulated from shoeTvdMsl when casing size is selected
						if(this.autoPopulateTestDepth){	
							thisLeakOffTest.setTestDepthTvdMsl(thisConverter.getConvertedValue());
						}
					}
				}
			}
		}
		else
		{
			thisLeakOffTest.setShoeDepthMdMsl(null);
			thisLeakOffTest.setShoeTvdMsl(null);
			thisLeakOffTest.setTestDepthTvdMsl(null);
			
		}
	}
}
