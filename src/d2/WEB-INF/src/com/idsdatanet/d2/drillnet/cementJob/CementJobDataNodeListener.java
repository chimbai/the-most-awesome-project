package com.idsdatanet.d2.drillnet.cementJob;


import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.CementFluid;
import com.idsdatanet.d2.core.model.CementJob;
import com.idsdatanet.d2.core.model.CementStage;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CementJobDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private Boolean defaultCementJobDateTime = false;
	private Boolean cementJobCasingSummaryCasingOd = false;
	private Boolean casingSizeBasedOnHoleSize = false;

	/*protected LinkedHashMap<String, Double> casingSizeLookup = new LinkedHashMap<String, Double>();
	
	public void loadCasingSizeLookup(String key, Double value)
	{
		this.casingSizeLookup.put(key, value);
	}
	
	private Double getRawCasingSizeFromLookup(String key)
	{
		return this.casingSizeLookup.get(key);
	}*/
	
	public void setDefaultCementJobDateTime(Boolean value) {
		this.defaultCementJobDateTime = value;
	}
			
	public void setCementJobCasingSummaryCasingOd(Boolean value) {
		this.cementJobCasingSummaryCasingOd = value;
	}
	
	public void setCasingSizeBasedOnHoleSize(Boolean value) {
		this.casingSizeBasedOnHoleSize = value;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof CementStage) {
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CementStage.class, "mixing_duration");
			CementStage thisCementStage = (CementStage) object;
			//auto calculate mixing duration
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_MIXING_DURATION))){
				if (thisCementStage.getMixingEndDateTime() != null &&  thisCementStage.getMixingStartDateTime() != null) {
					double thisDuration = 0.0;
					
					thisDuration = thisCementStage.getMixingEndDateTime().getTime() - thisCementStage.getMixingStartDateTime().getTime();
					thisConverter.setBaseValue(thisDuration / 1000.0);
					thisCementStage.setMixingDuration(thisConverter.getConvertedValue());
				}
				else
				{
					thisCementStage.setMixingDuration(null);
				}
			}
			//auto calculate Displacement Duration
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DISPLACEMENT_DURATION))){
				if (thisCementStage.getDisplacementEndDateTime() != null &&  thisCementStage.getDisplacementStartDateTime() != null) {
					double thisDuration = 0.0;
					
					thisDuration = thisCementStage.getDisplacementEndDateTime().getTime() - thisCementStage.getDisplacementStartDateTime().getTime();
					thisConverter.setBaseValue(thisDuration / 1000.0);
					thisCementStage.setDisplacementDuration(thisConverter.getConvertedValue());
				}
				else
				{
					thisCementStage.setDisplacementDuration(null);
				}
			}
				
			//check if start time greater than end time
			if (thisCementStage.getDisplacementEndDateTime() != null &&  thisCementStage.getDisplacementStartDateTime() != null) {
				if(thisCementStage.getDisplacementStartDateTime().getTime() > thisCementStage.getDisplacementEndDateTime().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "displacementStartDateTime", "Start time can not be greater than end time.");
					return;
				}
			}
			if (thisCementStage.getMixingEndDateTime() != null &&  thisCementStage.getMixingStartDateTime() != null) {
				if(thisCementStage.getMixingStartDateTime().getTime() > thisCementStage.getMixingEndDateTime().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "mixingStartDateTime", "Start time can not be greater than end time.");
					return;
				}
			}
			
		}
		
		if(object instanceof CementJob)
		{
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDatumConversionEnabled(false);
			
			CementJob thisCementJob = (CementJob) object;

			if(thisCementJob.getJobEndDate()!= null && thisCementJob.getJobStartDate()!= null){
				if(thisCementJob.getJobStartDate().getTime() > thisCementJob.getJobEndDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "jobStartDate", "Start date time can not be greater than End date time.");
					return;
				}
			}
			if (!this.cementJobCasingSummaryCasingOd) {			
				// HANDLE ONLY CASE WHERE CASING SIZE IS KEYED UNDER CASING SECTION
				//ADD VALIDATION - PREVIOUS CASING SIZE SHOULDN'T SMALLER THAN CURRENT CASING SIZE
				if((thisCementJob.getCasingSectionUid() != null && !StringUtils.isBlank(thisCementJob.getCasingSectionUid())) && (thisCementJob.getPreviousCasingSectionUid() != null && !StringUtils.isBlank(thisCementJob.getPreviousCasingSectionUid())))
				{
					Double currentSize = 0.0;
					Double previousSize = 0.0;
									
					Map<String, LookupItem> currentSizeLookupMap = commandBean.getLookupMap(node, node.getData().getClass().getSimpleName(), "casingSectionUid");
					LookupItem currentSizeLookupItem = currentSizeLookupMap.get(thisCementJob.getCasingSectionUid());
					if(currentSizeLookupItem != null)
					{
						currentSize = Double.parseDouble(currentSizeLookupItem.getDescription());
					}
									
					Map<String, LookupItem> previousSizeLookupMap = commandBean.getLookupMap(node, node.getData().getClass().getSimpleName(), "previousCasingSectionUid");
					LookupItem previousSizeLookupItem = previousSizeLookupMap.get(thisCementJob.getPreviousCasingSectionUid());
					if(previousSizeLookupItem != null)
					{
						previousSize = Double.parseDouble(previousSizeLookupItem.getDescription());
					}
												
					if(previousSize < currentSize)
					{
						status.setContinueProcess(false, true);
						commandBean.getSystemMessage().addError("Previous casing size cannot smaller than current casing size.");
					}
				}
			}
			
			// only if request auto populate casingSectionUid & prevCasingSectionUid based on selected hole size - AGR SPECIFIC
			if (this.casingSizeBasedOnHoleSize){
				if (thisCementJob.getHoleSize() != null){
					if (StringUtils.isNotBlank(thisCementJob.getHoleSize().toString())) {
						//based on hole size, retrieve specific casing section UID
						String casingSectionUid = CementJobUtils.getCasingSectionUidBasedOnHoleSize(thisCementJob.getHoleSize(), thisCementJob.getOperationUid());							
						//based on retrieved current casing section UID, filter and get previous casing section UID
						String prevCasingSectionUid = CementJobUtils.getPrevCasingSectionUidBasedOnCurrCasing(casingSectionUid, thisCementJob.getOperationUid());
						
						if (StringUtils.isNotBlank(casingSectionUid) && casingSectionUid != null) {
							thisCementJob.setCasingSectionUid(casingSectionUid);
						}else{
							thisCementJob.setCasingSectionUid(null);
						}
						
						if (StringUtils.isNotBlank(prevCasingSectionUid) && prevCasingSectionUid != null) {
							thisCementJob.setPreviousCasingSectionUid(prevCasingSectionUid);
						}else{
							thisCementJob.setPreviousCasingSectionUid(null);
						}
					}else{
						thisCementJob.setCasingSectionUid(null);
						thisCementJob.setPreviousCasingSectionUid(null);
					}
				}else{
					thisCementJob.setCasingSectionUid(null);
					thisCementJob.setPreviousCasingSectionUid(null);
				}
			}
		}
	}

	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		String casingSectionUid = null;
		String openHoleId = null;
		if (request !=null){
			casingSectionUid = request.getParameter("casingSectionUid");
			openHoleId = request.getParameter("openHoleId");
		}
		
		if (obj instanceof CementJob) {
			CementJob thisCementJob = (CementJob) obj;
			CementJobUtils.getRelatedInfo(node, userSelection, request);
			
			if(meta.getTableClass() == CementJob.class && StringUtils.isNotBlank(casingSectionUid)){
				thisCementJob.setCasingSectionUid(casingSectionUid);
				CommandBeanTreeNode newNode = commandBean.getRoot().addCustomNewChildNodeForInput(thisCementJob);

				if (StringUtils.isNotBlank(openHoleId)){
					if (StringUtils.isNumeric(openHoleId)){
						Double holeSize = Double.parseDouble(openHoleId);
						thisCementJob.setHoleSize(holeSize );
						
						if (this.casingSizeBasedOnHoleSize){
							CementJobUtils.populateCasingSizeBasedOnHoleSize(newNode, userSelection, holeSize, userSelection.getOperationUid());
							CementJobUtils.populatePrevCasingSizeBasedOnHoleSize(newNode, userSelection, holeSize, userSelection.getOperationUid());
						}else{
							newNode.getDynaAttr().put("casingSizeBasedOnHoleSize", null);
							newNode.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", null);
						}
					}
				}else{
					newNode.getDynaAttr().put("casingSizeBasedOnHoleSize", null);
					newNode.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", null);
				}
				CementJobUtils.getCasingSection(newNode, casingSectionUid, "casingSection");
				CementJobUtils.getRelatedInfo(newNode, userSelection, request);
			}
			
			if (this.defaultCementJobDateTime) {
				if (userSelection.getDailyUid() != null){
					Daily today = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());			
					Date todayDate = today.getDayDate();
					Date defaultDateTime = DateUtils.addHours(todayDate, 0);				
						
					thisCementJob.setTimeStartruncsg(defaultDateTime);		
					thisCementJob.setTimeCsgonbottom(defaultDateTime);
					thisCementJob.setTimeStartcirc(defaultDateTime);
					thisCementJob.setTimeStartpressuretest(defaultDateTime);
					thisCementJob.setTimePumppreflush(defaultDateTime);
					thisCementJob.setTimeStartmixing(defaultDateTime);
					thisCementJob.setTimeFinishmixing(defaultDateTime);
					thisCementJob.setTimeStartdispl(defaultDateTime);
					thisCementJob.setTimeFinishdispl(defaultDateTime);
					thisCementJob.setTimeFinishpressuretest(defaultDateTime);
				}
			}
			CementJobUtils.setWaterSourceCementFromReportDaily(userSelection, thisCementJob);
		}		
	}
			
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		if(commandBean.getFlexClientControl() != null){
			commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
	
		Object obj = node.getData();
		
		if (obj instanceof CementJob) {
			//GET M.DEPTH FOR PREVIOUS CASING SIZE
			CementJob cementjob = (CementJob) obj;
			CementJobUtils.getRelatedInfo(node, userSelection, request);
			
			if(StringUtils.isNotBlank(cementjob.getPreviousCasingSectionUid()))
			{
				CementJobUtils.getCasingSection(node, cementjob.getPreviousCasingSectionUid(), "previousCasingSection");
				CementJobUtils.getOperationUidBasedOnCasing(node, cementjob.getPreviousCasingSectionUid(), "previousCasingSection");
			}
			
			if (StringUtils.isNotBlank(cementjob.getCasingSectionUid())) {
				CementJobUtils.getCasingSection(node, cementjob.getCasingSectionUid(), "casingSection");
				CementJobUtils.getOperationUidBasedOnCasing(node, cementjob.getCasingSectionUid(), "casingSection");
			}
			
			CementJobUtils.setWaterSourceCementFromReportDaily(userSelection, cementjob);	
			
			//only populate dynamic fields - casing size & previous casing size based on hole size if it is requested (casingSizeBasedOnHoleSize set to true)
			if (this.casingSizeBasedOnHoleSize){
				if(cementjob.getHoleSize() != null){
					if (StringUtils.isNotBlank(cementjob.getHoleSize().toString())) {
						CementJobUtils.populateCasingSizeBasedOnHoleSize(node, userSelection, cementjob.getHoleSize(), cementjob.getOperationUid());
						CementJobUtils.populatePrevCasingSizeBasedOnHoleSize(node, userSelection, cementjob.getHoleSize(), cementjob.getOperationUid());
					}else{
						node.getDynaAttr().put("casingSizeBasedOnHoleSize", null);
						node.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", null);
					}
				}else{
					node.getDynaAttr().put("casingSizeBasedOnHoleSize", null);
					node.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", null);
				}
			}			
		} 
		
		
		if (obj instanceof CementStage) {
			CementStage cementstage = (CementStage) obj;
			String cementJobUid = cementstage.getCementJobUid();
			
			String strSql = "select casingSectionUid FROM CementJob where cementJobUid = :cementJobUid and (isDeleted = false or isDeleted is null)";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "cementJobUid" ,cementJobUid);
			String casingSectionUid = null;
			if (!lstResult.isEmpty()) {
				Object b = (Object) lstResult.get(0);
				if (b != null) casingSectionUid = b.toString();
			}
			
			CementJobUtils.getCasingSection(node, casingSectionUid, "csg_cementStage");
			CementJobUtils.getRatHole(node, casingSectionUid, "csg_cementStage");
			
			//------------------------------------------------------------------------------------------
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			
			String thisCementStageUid = cementstage.getCementStageUid();
			if (StringUtils.isNotBlank(thisCementStageUid)) {
				//-------------------------------------------------------------------------------------------------------------------
				//Get Lead Volume 
				thisConverter.setReferenceMappingField(CementFluid.class, "volume");
				
				strSql = "select sum(volume) as leadVolume from CementFluid where (isDeleted = false or isDeleted is null) and fluidName='Lead' AND cementStageUid = :thisCementStageUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisCementStageUid", thisCementStageUid);
				Object a = (Object) lstResult.get(0);
				
				Double leadVolume = 0.00;				
				if (a != null) {					
					leadVolume = Double.parseDouble(a.toString());					
				}
				thisConverter.setBaseValue(leadVolume);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@leadVolume", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("leadVolume", thisConverter.getConvertedValue());
				
				//Get Tail Volume 
				strSql = "select sum(volume) as tailVolume from CementFluid where (isDeleted = false or isDeleted is null) and fluidName='Tail' AND cementStageUid = :thisCementStageUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisCementStageUid", thisCementStageUid);
				Object b = (Object) lstResult.get(0);
				
				Double tailVolume = 0.00;
				if (b != null) {					
					tailVolume = Double.parseDouble(b.toString());					
				}
				thisConverter.setBaseValue(tailVolume);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@tailVolume", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("tailVolume", thisConverter.getConvertedValue());
				
				//--------------------------------------------------------------------------------------------------------------
				//Get Excess Lead Volume
				thisConverter.setReferenceMappingField(CementFluid.class, "excessVolume");
				
				strSql = "select sum(excessVolume) as excessLeadVolume from CementFluid where (isDeleted = false or isDeleted is null) and fluidName='Lead' AND cementStageUid = :thisCementStageUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisCementStageUid", thisCementStageUid);
				Object c = (Object) lstResult.get(0);
			
				Double excessLeadVolume = 0.00;
				if (c != null) {					
					excessLeadVolume = Double.parseDouble(c.toString());					
				}
				thisConverter.setBaseValue(excessLeadVolume);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@excessLeadVolume", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("excessLeadVolume", thisConverter.getConvertedValue());
								
				//Get Excess Tail Volume 
				strSql = "select sum(excessVolume) as excessTailVolume from CementFluid where (isDeleted = false or isDeleted is null) and fluidName='Tail' AND cementStageUid = :thisCementStageUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisCementStageUid", thisCementStageUid);
				Object d = (Object) lstResult.get(0);
			
				Double excessTailVolume = 0.00;
				if (d != null) {					
					excessTailVolume = Double.parseDouble(d.toString());					
				}
				thisConverter.setBaseValue(excessTailVolume);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@excessTailVolume", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("excessTailVolume", thisConverter.getConvertedValue());
				
				//--------------------------------------------------------------------------------------------------------------
				//Get Excess Lead Percent
				thisConverter.setReferenceMappingField(CementFluid.class, "excessPercent");
								
				strSql = "FROM CementFluid where (isDeleted = false or isDeleted is null) and fluidName='Lead' AND cementStageUid = :thisCementStageUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisCementStageUid", thisCementStageUid);
				
				Double excessLeadPercent = 0.00;
				if (lstResult.size() > 0){
					for(Object objResult: lstResult){
						CementFluid thisCementFluidLead = (CementFluid) objResult;
						if (thisCementFluidLead.getExcessPercent()!=null){
							thisConverter.setBaseValueFromUserValue(thisCementFluidLead.getExcessPercent());
							excessLeadPercent = thisConverter.getBasevalue();
						}
					}
				}
				thisConverter.setBaseValue(excessLeadPercent);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@excessLeadPercent", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("excessLeadPercent", thisConverter.getConvertedValue());
				
				//Get Excess Tail Percent
				strSql = "FROM CementFluid where (isDeleted = false or isDeleted is null) and fluidName='Tail' AND cementStageUid = :thisCementStageUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisCementStageUid", thisCementStageUid);
				
				Double excessTailPercent = 0.00;
				if (lstResult.size() > 0){
					for(Object objResult: lstResult){
						CementFluid thisCementFluidTail = (CementFluid) objResult;
						if (thisCementFluidTail.getExcessPercent()!=null){
							thisConverter.setBaseValueFromUserValue(thisCementFluidTail.getExcessPercent());
							excessTailPercent = thisConverter.getBasevalue();
						}
					}
				}
				thisConverter.setBaseValue(excessTailPercent);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@excessTailPercent", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("excessTailPercent", thisConverter.getConvertedValue());
				
				//--------------------------------------------------------------------------------------------------------------
				//Get Total Cement Volume
				double cmtAboveThePlugVolume = 0.0;
				double cmtShoeTrackVolume = 0.0;
				double totalCementVolume = 0.0;
				
				thisConverter.setReferenceMappingField(CementStage.class, "cmt_above_the_plug_volume");
				if (cementstage.getCmtAboveThePlugVolume()!= null) {
					thisConverter.setBaseValueFromUserValue(cementstage.getCmtAboveThePlugVolume());
					cmtAboveThePlugVolume = thisConverter.getBasevalue();
				}
				
				thisConverter.setReferenceMappingField(CementStage.class, "cmt_shoe_track_volume");
				if (cementstage.getCmtShoeTrackVolume() != null) {
					thisConverter.setBaseValueFromUserValue(cementstage.getCmtShoeTrackVolume());
					cmtShoeTrackVolume = thisConverter.getBasevalue();
				}
				
				thisConverter.setReferenceMappingField(CementFluid.class, "volume");
				
				totalCementVolume = leadVolume + tailVolume + cmtAboveThePlugVolume + cmtShoeTrackVolume;
				thisConverter.setBaseValue(totalCementVolume);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@totalCementVolume", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalCementVolume", thisConverter.getConvertedValue());
			}

		}
		
		if (obj instanceof CementFluid) {
			CementFluid cementFluid = (CementFluid) obj;
			if (cementFluid.getCementStageUid()!=null){
				CementStage cementStage = (CementStage) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(CementStage.class, cementFluid.getCementStageUid());
				
				if (cementStage!=null && cementStage.getCementJobUid()!=null){
					CementJob cementJob = (CementJob) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(CementJob.class, cementStage.getCementJobUid());
					
					if (cementJob!=null){
						node.getDynaAttr().put("casingSectionUid", cementJob.getCasingSectionUid());
						node.getDynaAttr().put("type", cementJob.getType());
						node.getDynaAttr().put("cementJobUid", cementJob.getCementJobUid());
					}
				}				
			}
		}
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
				
		String customCondition 	= "";
		String currentClass	= meta.getTableClass().getSimpleName();
			
		if(currentClass.equals("CementJob")) {
			if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_VIEW_ALL_CEMENT_JOB)))
			{
				customCondition = "operationUid = session.operationUid";
			}
			else
			{
				customCondition = "dailyUid = session.dailyUid";
			}						
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		
		return null;
	}
		
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
}
