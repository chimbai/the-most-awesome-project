package com.idsdatanet.d2.drillnet.wellControlStack;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.WellControl;
import com.idsdatanet.d2.core.model.WellControlComponent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActiveBopStackSummaryDataNodeListener extends EmptyDataNodeListener {
	
    public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
    	
    		Object object = node.getData();
    		
    		if(object instanceof WellControlComponent) {
    		
    			String wellUid = userSelection.getWellUid();
    		
    			if(StringUtils.isNotBlank(wellUid)){
    			
    				WellControlComponent wellControlComponent = (WellControlComponent) node.getData();
    				String wellControlUid = wellControlComponent.getWellControlUid();
    			
    				String strSql = "FROM WellControl WHERE (isDeleted = false or isDeleted is null) and wellUid = :wellUid and wellControlUid = :wellControlUid";
    				String[] paramsFields = {"wellUid","wellControlUid"};
    				String[] paramsValues = {wellUid,wellControlUid};
				
    				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
    				if (!lstResult.isEmpty()){
    					WellControl wellControl =  (WellControl) lstResult.get(0);
    					node.getDynaAttr().put("sequence", wellControl.getSequence());
    					node.getDynaAttr().put("wellControlType", wellControl.getWellControlType());
    				}
    			}
    		}
    	}
    
}