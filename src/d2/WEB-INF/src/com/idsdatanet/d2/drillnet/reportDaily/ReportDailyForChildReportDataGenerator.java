package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class ReportDailyForChildReportDataGenerator implements ReportDataGenerator {
	
	private String defaultReportType;
	
	public void setReportType(String value){
		this.defaultReportType = value;
	}

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
	
		if(this.defaultReportType != null){
			reportType = this.defaultReportType;
		}
		
		String currentWellboreUid = userContext.getUserSelection().getWellboreUid();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		
		
		//Use the strWellboreList to retrieve all of the operations using an IN clause
		List <Operation> lstOperation = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM Operation WHERE (isDeleted = false or isDeleted is null) and wellboreUid = '" + currentWellboreUid + "' and operationUid != '" + currentOperationUid + "'");
		if(lstOperation.size() > 0){
			
			for(Operation objOperation: lstOperation){
				
				    String operationuid="";
					operationuid=objOperation.getOperationUid();
					String operationName="";
					Double afebudgetMobdemob = 0.0;
					Double afebudgetIntervention = 0.0;					
					String operationCode="";
					Double amountSpentPriorToSpud=0.0;
					Double daysOnWell=0.00;
					Double cumP50Duration=0.0;
					String daysOnWellUomSymbol="";
					String cumP50DurationUomSymbol="";
					
					Date startDate=null;
					Long startDatetimeEpoch = null;
					operationName=objOperation.getOperationName();
					if(objOperation.getStartDate()!=null)
					{
						startDate=objOperation.getStartDate();
						startDatetimeEpoch=startDate.getTime();
					}
					
					String strSql = "SELECT rd.plannedDaysCompleted, rd.reportCurrentStatus, rd.reportPlanSummary, rd.daycurve, rd.reportPeriodSummary, rd.daycost, rd.efcMobDemob,rd.last24hrsincident,rd.durationToCompletion, rd.actualVesselCost, rd.dailyUid from ReportDaily rd, Daily d WHERE rd.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) AND (rd.isDeleted = false or rd.isDeleted is null) AND rd.operationUid = :thisoperationUid AND d.dayDate = :userDate";
					String[] paramsFields = {"userDate", "thisoperationUid"};
					Object[] paramsValues = {todayDate, operationuid};
					
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
					
					String planMasterUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationuid);
					
					String[] paramNames = {"operationUid", "planMasterUid"};
					Object[] paramValues = {operationuid, planMasterUid};
					
					String queryString = "SELECT SUM(opp.p50Duration) as totalDuration FROM OperationPlanPhase opp, OperationPlanMaster opm " +
							"WHERE opm.operationPlanMasterUid = opp.operationPlanMasterUid AND " +
							"(opp.isDeleted=false or opp.isDeleted is null) AND (opm.isDeleted=false or opm.isDeleted is null) AND " +
							"opm.operationUid=:operationUid AND opp.operationPlanMasterUid=:planMasterUid";
					List lstResultp50duration = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, qp);		
										
					String[] paramsLabels = {"todayDate", "operationUid", "reportType"};
					Object[] paramsData = {todayDate, operationuid, reportType};
										
					if (lstResult.size() > 0){
						
						ReportDataNode ChildWellReportNode = reportDataNode.addChild("ChildWellReportDaily");
						ChildWellReportNode.addProperty("operationUid", operationuid);
						
						
						Object childWellOperationPlanPhase = (Object) lstResultp50duration.get(0);
							
						if (childWellOperationPlanPhase != null && StringUtils.isNotBlank(childWellOperationPlanPhase.toString()))
						{
							cumP50Duration=Double.parseDouble(childWellOperationPlanPhase.toString());
							
							CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), OperationPlanPhase.class, "p50Duration");
							
							if (thisConverter.isUOMMappingAvailable())
							{
								thisConverter.setBaseValue(cumP50Duration);
								cumP50Duration=thisConverter.getConvertedValue();
								cumP50DurationUomSymbol = thisConverter.getUomSymbol();
							}
							ChildWellReportNode.addProperty("cumP50Duration", cumP50Duration.toString());
						}						
						
						
						for(Object objReportDaily: lstResult)
						{
							Object[] childWellReport = (Object[]) objReportDaily;
							
							Double plannedDaysCompleted = 0.0;
							Double durationToCompletion =0.0;
							Double actualVesselCost =0.0;
							Double daycurve = 0.0;
							String reportCurrentStatus = "";
							String reportPlanSummary = "";
							String reportPeriodSummary="";
							String last24hrsincident="";
							Double daycost = 0.0;
							Double efcMobDemob = 0.0;
							String actualVesselCostSymbol = "";
							Double cumActualVesselCost =0.0;
							String cumActualVesselCostSymbol="";
							
							if (childWellReport[0] != null && StringUtils.isNotBlank(childWellReport[0].toString())){
								plannedDaysCompleted = Double.parseDouble(childWellReport[0].toString());
								
								CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "planned_days_completed");
								if (thisConverter.isUOMMappingAvailable())
								{
									thisConverter.setBaseValue(plannedDaysCompleted);
									plannedDaysCompleted = thisConverter.getConvertedValue();
								}
							}
							if (childWellReport[1] != null && StringUtils.isNotBlank(childWellReport[1].toString())) reportCurrentStatus = childWellReport[1].toString();
							if (childWellReport[2] != null && StringUtils.isNotBlank(childWellReport[2].toString())) reportPlanSummary = childWellReport[2].toString();
							if (childWellReport[3] != null && StringUtils.isNotBlank(childWellReport[3].toString())){
								daycurve = Double.parseDouble(childWellReport[3].toString());
								
								CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "daycurve");
								if (thisConverter.isUOMMappingAvailable())
								{
									thisConverter.setBaseValue(daycurve);
									daycurve = thisConverter.getConvertedValue();
								}
							}
							if (childWellReport[4] != null && StringUtils.isNotBlank(childWellReport[4].toString())) reportPeriodSummary = childWellReport[4].toString();
							if (childWellReport[5] != null && StringUtils.isNotBlank(childWellReport[5].toString())){
								daycost = Double.parseDouble(childWellReport[5].toString());
								
								CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "daycost");
								if (thisConverter.isUOMMappingAvailable())
								{
									thisConverter.setBaseValue(daycost);
									daycost = thisConverter.getConvertedValue();
								}
							}
							if(objOperation.getAfebudgetMobdemob()!=null && StringUtils.isNotBlank(objOperation.getAfebudgetMobdemob().toString()))
							{
								afebudgetMobdemob=objOperation.getAfebudgetMobdemob();
							}
							if(objOperation.getAfebudgetIntervention()!=null && StringUtils.isNotBlank(objOperation.getAfebudgetIntervention().toString()))
							{
								
								afebudgetIntervention=objOperation.getAfebudgetIntervention();
							}
							if(objOperation.getOperationCode()!=null && StringUtils.isNotBlank(objOperation.getOperationCode().toString()))
							{
								operationCode=objOperation.getOperationCode();
							}
							if(objOperation.getAmountSpentPriorToSpud()!=null && StringUtils.isNotBlank(objOperation.getAmountSpentPriorToSpud().toString()))
							{
								
								amountSpentPriorToSpud=objOperation.getAmountSpentPriorToSpud();
							}
							if(CommonUtil.getConfiguredInstance().daysOnOperation(operationuid, daily.getDailyUid())!=null && StringUtils.isNotBlank(CommonUtil.getConfiguredInstance().daysOnOperation(operationuid, daily.getDailyUid()).toString()))
							{
								CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activityDuration");
								daysOnWell=CommonUtil.getConfiguredInstance().daysOnOperation(operationuid, daily.getDailyUid());
								if (thisConverter.isUOMMappingAvailable())
								{
									thisConverter.setBaseValue(daysOnWell);
									daysOnWellUomSymbol = thisConverter.getUomSymbol();
									daysOnWell = thisConverter.getConvertedValue()/24.00;
								}
							}
							if (childWellReport[6] != null && StringUtils.isNotBlank(childWellReport[6].toString())){
								efcMobDemob = Double.parseDouble(childWellReport[6].toString());
							}
							
							if (childWellReport[7] != null && StringUtils.isNotBlank(childWellReport[7].toString())){
								last24hrsincident = childWellReport[7].toString();
							}
							
							if (childWellReport[8] != null && StringUtils.isNotBlank(childWellReport[8].toString())){
								durationToCompletion = Double.parseDouble(childWellReport[8].toString());
								
								CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "duration_to_completion");
								if (thisConverter.isUOMMappingAvailable())
								{
									thisConverter.setBaseValue(durationToCompletion);
									durationToCompletion = thisConverter.getConvertedValue();
								}
							}
							
							if (childWellReport[9] != null && StringUtils.isNotBlank(childWellReport[9].toString())){
								actualVesselCost = Double.parseDouble(childWellReport[9].toString());
								
								//assign unit to the value base on daily cost unit
								CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "actualVesselCost");
								if (thisConverter.isUOMMappingAvailable())
								{
									thisConverter.setBaseValue(actualVesselCost);
									actualVesselCost = thisConverter.getConvertedValue();
									actualVesselCostSymbol = thisConverter.getUomSymbol();
								}
							}
							
							if (childWellReport[10] != null && StringUtils.isNotBlank(childWellReport[10].toString())){
								String DailyUid = childWellReport[10].toString();;
								//calculate cum. actual vessel cost		
								cumActualVesselCost = CommonUtil.getConfiguredInstance().CalculatecumActualVesselCost(operationuid,DailyUid);
								if (cumActualVesselCost != null && StringUtils.isNotBlank(cumActualVesselCost.toString())){
									
									//assign unit to the value base on daily cost unit
									CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "actualVesselCost");
								    thisConverter.setBaseValue(cumActualVesselCost);
									cumActualVesselCost = thisConverter.getConvertedValue();
									cumActualVesselCostSymbol = thisConverter.getUomSymbol();														
								}
								if (cumActualVesselCost ==null) cumActualVesselCost = 0.0;
							}
							
							ReportDataNode thisReportNode = ChildWellReportNode.addChild("ReportDailyForChild");							
							thisReportNode.addProperty("plannedDaysCompleted", plannedDaysCompleted.toString());
							thisReportNode.addProperty("reportPlanSummary", reportPlanSummary);
							thisReportNode.addProperty("reportCurrentStatus", reportCurrentStatus);
							thisReportNode.addProperty("daycurve", daycurve.toString());
							thisReportNode.addProperty("reportPeriodSummary", reportPeriodSummary);
							thisReportNode.addProperty("operationName", operationName);
							thisReportNode.addProperty("last24hrsincident", last24hrsincident);
							thisReportNode.addProperty("durationToCompletion", durationToCompletion.toString());
							thisReportNode.addProperty("actualVesselCost", actualVesselCost.toString());
							thisReportNode.addProperty("actualVesselCostSymbol", actualVesselCostSymbol);
							if(startDatetimeEpoch!=null)
							{
								thisReportNode.addProperty("startDate", startDatetimeEpoch.toString());
							}
							else
							{
								thisReportNode.addProperty("startDate", "");
							}
							thisReportNode.addProperty("daycost", daycost.toString());
							thisReportNode.addProperty("afebudgetMobdemob", afebudgetMobdemob.toString());
							thisReportNode.addProperty("afebudgetIntervention", afebudgetIntervention.toString());
							thisReportNode.addProperty("operationCode", operationCode);
							thisReportNode.addProperty("amountSpentPriorToSpud", amountSpentPriorToSpud.toString());
							thisReportNode.addProperty("efcMobDemob", efcMobDemob.toString());
							thisReportNode.addProperty("daysOnWell", daysOnWell.toString());
							thisReportNode.addProperty("daysOnWellUomSymbol", daysOnWellUomSymbol);
							thisReportNode.addProperty("cumP50DurationUomSymbol", cumP50DurationUomSymbol);
							thisReportNode.addProperty("cumActualVesselCostSymbol", cumActualVesselCostSymbol);
							thisReportNode.addProperty("cumActualVesselCost", cumActualVesselCost.toString());
						}
						
	
						
						
					}	
				
			}
		}
		
			
	}
}
