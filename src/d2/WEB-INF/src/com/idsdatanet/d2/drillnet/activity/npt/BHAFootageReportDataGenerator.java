package com.idsdatanet.d2.drillnet.activity.npt;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.report.ReportOption;

import edu.emory.mathcs.backport.java.util.Collections;

public class BHAFootageReportDataGenerator extends NPTReportDataGenerator{
	
	private Boolean useIADCDuration = false;
	private Boolean countSectionByBharun = false;
	private Boolean includeSections = false;
	private Boolean isSummary = false;
	
	protected void generateActualData(UserContext userContext, Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		List<BHAFootageSummary> includedList = this.shouldInclude(userContext, startDate, endDate);
		this.includeQueryData(includedList, reportDataNode, userContext.getUserSelection(), startDate, endDate, userContext);	
	}
	
	private List<BHAFootageSummary> generatedata(UserContext userContext, Date startDate, Date endDate, String includeWellUid) throws Exception
	{
		List<BHAFootageSummary> includedList = new ArrayList<BHAFootageSummary>();
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String sql = "Select Distinct w.wellUid,w.wellName, wb.wellboreUid, wb.wellboreName, o.operationUid, w.country,"+BHIQueryUtils.getQuerySQL(getUseCountry())+", b.bharunUid, b.timeIn, b.timeOut, b.toolCategory, b.directionalCompany, b.toolSize, d.dailyUid, d.dayDate, a.sections, bds.progress, b.toolFailure, "+(useIADCDuration?"bds.IADCDuration":"bds.duration")+
			" From Well w,Wellbore wb, Operation o, Bharun b, Daily d, BharunDailySummary bds, Activity a"+
			" Where w.wellUid=wb.wellUid and wb.wellboreUid=o.wellboreUid"+
			" and o.operationUid=d.operationUid and o.operationUid=b.operationUid"+
			" and d.dailyUid=bds.dailyUid and b.bharunUid=bds.bharunUid and a.operationUid = o.operationUid"+
			" and (w.isDeleted is null or w.isDeleted = false) and (wb.isDeleted is null or wb.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false)"+
			" and (a.isDeleted is null or a.isDeleted = false) and (a.depthMdMsl>=b.depthInMdMsl and a.depthMdMsl<=b.depthOutMdMsl)"+
			" and (b.isDeleted is null or b.isDeleted = false) and (d.isDeleted is null or d.isDeleted = false)"+
			" and (bds.isDeleted is null or bds.isDeleted = false) and (a.startDatetime>=b.timeIn and a.endDatetime <=b.timeOut)"+
			" and (d.dayDate>=:startDate and d.dayDate<=:endDate)"+
			" and (b.toolCategory in (:toolCategory))"+
			" and (b.toolSize in (:toolSize))"+
			" and (b.directionalCompany in (:companyName))"+
			" and w.wellUid=:wellUid"+
			(includeSections?" and (a.sections in (:sections))":"")+
			BHIQueryUtils.getQueryFilter(getUseCountry())+
			" order by b.directionalCompany, o.operationArea, b.toolCategory";
		String[] paramNames = {"startDate","endDate","toolCategory","companyName","toolSize",BHIQueryUtils.getQueryParamName(getUseCountry()),"wellUid"};
		List<String> listParams = BHIQueryUtils.convertArrayToList(paramNames);
		if (includeSections)
			listParams.add("sections");
		
		Object[] values = {startDate,endDate, BHIQueryUtils.getReportOptionValue(userContext, BHIQueryUtils.CONSTANT_TOOL_CATEGORY), BHIQueryUtils.getReportOptionValue(userContext, BHIQueryUtils.CONSTANT_COMPANY), BHIQueryUtils.getReportOptionValue(userContext, BHIQueryUtils.CONSTANT_TOOL_SIZE,true), BHIQueryUtils.getQueryParamValue(userContext, getUseCountry()),includeWellUid};
		List<Object> listValues = BHIQueryUtils.convertArrayToList(values);
		if (includeSections)
			listValues.add(BHIQueryUtils.getReportOptionValue(userContext, BHIQueryUtils.CONSTANT_SECTIONS));
		
		
		
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, listParams, listValues,qp);
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), BharunDailySummary.class, "progress");
		CustomFieldUom thisConverter2 = new CustomFieldUom(userContext.getUserSelection().getLocale(), BharunDailySummary.class, "duration");

		if (result.size()>0)
		{
			for (Object[] item:result)
			{
				String wellUid = (String)item[0];
				String wellName = (String)item[1];
				String wellboreUid = (String)item[2];
				String wellboreName = (String)item[3];
				String operationUid = (String)item[4];
				String country = (String)item[5];
				String bharunUid = (String)item[7];
				Date timeIn = (Date)item[8];
				Date timeOut = (Date)item[9];
				String toolCategory = (String) item[10];
				String directionalCompany = (String) item[11];
				Double toolSize =(Double) item[12];
				String dailyUid =(String) item[13];
				Date dayDate =(Date) item[14];
				String section = (String) item[15];
				Double progress = (Double) item[16];
				Boolean failure = (Boolean) item[17];
				Double duration = (Double) item[18];
				if (progress!=null)
				{
				
				thisConverter.setBaseValue(progress);
				progress = thisConverter.getConvertedValue();
				}else{
					progress = 0.0;
				}
				if (duration!=null)
				{
					thisConverter2.setBaseValue(duration);
					duration = thisConverter2.getConvertedValue();
				}else{
					duration=0.0;
				}
				
				

				BHAFootageSummary bhafootage = new BHAFootageSummary( wellUid,  operationUid,
						 country,  null,  bharunUid,
						 timeIn,  timeOut,  toolCategory,
						 toolSize,  dailyUid,  dayDate,section,
						 directionalCompany,  progress);
				BHIQueryUtils.setCountryOrArea(bhafootage, getUseCountry(), (String)item[6]);
				bhafootage.setDuration(duration);
				bhafootage.setFailure(failure);
				bhafootage.setWellboreName(wellboreName);
				bhafootage.setWellboreUid(wellboreUid);
				bhafootage.setWellName(wellName);
				if (directionalCompany!=null)
					bhafootage.setIsCompetitor(!this.getCompanyCode().equalsIgnoreCase(directionalCompany));
				else
					bhafootage.setIsCompetitor(true);
				includedList.add(bhafootage);
					
			}
		}
		return includedList;
	}
	
	private void includeQueryData(List<BHAFootageSummary> footages, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end, UserContext userContext) throws Exception
	{
		ReportOption options = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_QUERIES)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_QUERIES):null;
		
		
		if (options.getValues().contains("Q6"))
			this.includeTotalFootagePerArea(footages, node, userSelection, start, end);
		if (options.getValues().contains("Q8") || options.getValues().contains("Q10") || options.getValues().contains("Q15"))
		{
			List<NPTToolSummary> npts = ExtendedNPTToolReportDataGenerator.shouldIncludeNPTSummary(userContext, start, end, this.getUseCountry());
			if (options.getValues().contains("Q8"))
				this.includeBHAPerformanceChart(footages, npts, node, userSelection);
			if (options.getValues().contains("Q10"))
				this.includeFieldAreaRopNPTAnalysis(footages, npts, node);
			if (options.getValues().contains("Q15"))
				this.includeDirectionalDrillingKPI(start, end, footages, npts, node, userSelection);
		}
		if (options.getValues().contains("Q9"))
			if (this.isSummary)
				this.includeFieldAreaRopAnalysisSummary(footages, node, userSelection, start, end);
		if (options.getValues().contains("Q11"))
			this.includeDistanceDrilledOverRop(footages,node,userSelection, start, end);
		if (options.getValues().contains("Q12") ||options.getValues().contains("Q13")||options.getValues().contains("Q14") )
			this.includeDistanceDrilledOverRopFailure(footages,node,userSelection, start, end);
		
	}
	
	private void includeBHAPerformanceChart(List<BHAFootageSummary> footages,List<NPTToolSummary> npts, ReportDataNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		ReportDataNode child = node.addChild("BHAPerformanceChart"); 
		ReportOption wells = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL):null;
		ReportOption sections = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_SECTIONS)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_SECTIONS):null;

		if (wells!=null)
		{
			for(String w: wells.getValues())
			{
				List<String> wSections = new ArrayList<String>();

				for (BHAFootageSummary foot : footages)
				{
					if (w.equalsIgnoreCase(foot.getWellUid()))
					{
						String wSection = foot.getWellUid()+":"+foot.getSection();
						if (!wSections.contains(wSection))
							wSections.add(wSection);
					}
				}
				for (String wSec : wSections)
				{
					Double progress = 0.00;
					Double duration = 0.00;
					Double rop = 0.00;
					List<String> bharunList = new ArrayList<String>();
					List<Date> bhaStartDates = new ArrayList<Date>();
					Double nptHours = 0.00;
					String section = null; 
					
					ReportDataNode data = child.addChild("data");
					data.addProperty("wellUid", w);
					data.addProperty("wellName", wells.getValue(w).toString());
					
					
					for (BHAFootageSummary foot : footages)
					{
						String wSection = foot.getWellUid()+":"+foot.getSection();
						if (wSection.equalsIgnoreCase(wSec))
						{
							if (section==null)section = foot.getSection(); 
							progress+=foot.getProgress();
							duration+=foot.getDuration();
							if (!bharunList.contains(foot.getBharunUid()))
							{
								bharunList.add(foot.getBharunUid());
								bhaStartDates.add(foot.getTimeIn());
							}
						}
					}
					data.addProperty("section", section);
					data.addProperty("section_label", sections.getValue(section).toString());
					data.addProperty("numBharun",String.valueOf(bharunList.size()));
					if (progress!=0.00 && duration !=0.00)
						rop = progress/duration;
					data.addProperty("duration", String.valueOf(duration));
					data.addProperty("duration_hours", String.valueOf(duration/24));
					data.addProperty("progress", String.valueOf(progress));
					data.addProperty("rop", String.valueOf(rop));
					Collections.sort(bhaStartDates);
					SimpleDateFormat sdf = new SimpleDateFormat(ApplicationConfig.getConfiguredInstance().getApplicationDateFormat());
					data.addProperty("startDate", sdf.format(bhaStartDates.get(0)));
					
					for (NPTToolSummary npt : npts)
					{
						String wSection = npt.getWellUid()+":"+npt.getSections();
						if (wSection.equalsIgnoreCase(wSec))
						{
							nptHours+=npt.getDuration();
						}
					}
					data.addProperty("nptHours", String.valueOf(nptHours));
				}
			}
		}
	}
	
	private void includeDirectionalDrillingKPI(Date start, Date end, List<BHAFootageSummary> footages, List<NPTToolSummary> npts, ReportDataNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		ReportDataNode nodeTotalFootagePerArea = node.addChild("DirectionalDrillingKPI"); 
		
		ReportOption cats = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY):null;
		ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;
		
		if (cats!=null){
			for (String cat : cats.getValues()){
				ReportDataNode child = nodeTotalFootagePerArea.addChild(cats.getValue(cat).toString().replace(" ", "_"));
				
				if (cos!=null){
					for (String coName : cos.getValues()){
						List<Date> dates = DateRangeUtils.getDateList(start, end, false, cat);
						for (Date date: dates){
							
							Double lostTime = 0.00;
							Double rotationTime = 0.00;
							Double footage = 0.00;
							ReportDataNode catChild = child.addChild("data");
							String formatLabel = DateRangeUtils.formatDateLabel(date, cat,true);
							catChild.addProperty("label", formatLabel);
							for (NPTToolSummary tool : npts)
							{
								String dateLabel = DateRangeUtils.formatDateLabel(tool.getDayDate(), cat, true);
								if (coName.equalsIgnoreCase(tool.getLookupCompanyUid()) && formatLabel.equalsIgnoreCase(dateLabel))
								{
									lostTime += tool.getDuration();
								}
							}
							for (BHAFootageSummary foot : footages)
							{
								String dateLabel = DateRangeUtils.formatDateLabel(foot.getDayDate(), cat, true);
								if (coName.equalsIgnoreCase(foot.getDirectionalCompany()) && formatLabel.equalsIgnoreCase(dateLabel))
								{
									rotationTime += foot.getDuration();
									footage += foot.getProgress();
								}
							}
							Double lostTimePer100HoursRot = 0.00;
							if (lostTime!=0.00 && rotationTime !=0.00)
								lostTimePer100HoursRot = lostTime *100 / rotationTime;
							
							Double lostTimePer1000FeetDrilled = 0.00;
							if (lostTime!=0.00 && footage!=0.00)
								lostTimePer1000FeetDrilled = lostTime * 1000 / footage;
							
							catChild.addProperty("lookupCompanyUid", coName);
							catChild.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(coName).toString());
							catChild.addProperty("progress", String.valueOf(footage));
							catChild.addProperty("duration",String.valueOf(rotationTime));
							catChild.addProperty("nptHours",String.valueOf(lostTime));
							catChild.addProperty("lostTimePer100HoursRot",String.valueOf(lostTimePer100HoursRot));
							catChild.addProperty("lostTimePer1000FeetDrilled",String.valueOf(lostTimePer1000FeetDrilled));
						}
					}
				}
			}
		}		
	}
	private void includeFieldAreaRopNPTAnalysis(List<BHAFootageSummary> footages,List<NPTToolSummary> npts, ReportDataNode node) throws Exception 
	{
		Double cDistance = 0.00;
		Double cTime = 0.00;
		Double cNpt = 0.00;
		List<String> cBharunList = new ArrayList<String>();
		List<String> cWList = new ArrayList<String>();
		
		Double distance = 0.00;
		Double time = 0.00;
		Double npt = 0.00;
		List<String> bharunList = new ArrayList<String>();
		List<String> wList = new ArrayList<String>();
		
		for (BHAFootageSummary foot : footages)
		{
			if (foot.getIsCompetitor()!=null)
			{
				if ( foot.getIsCompetitor())
				{
					cDistance+=foot.getProgress();
					cTime+=foot.getDuration();
					if (!cBharunList.contains(foot.getBharunUid()))
						cBharunList.add(foot.getBharunUid());
					if (!cWList.contains(foot.getWellUid()+":"+foot.getSection()))
						cWList.add(foot.getWellUid()+":"+foot.getSection());
				}else{
					distance+=foot.getProgress();
					time+=foot.getDuration();
					if (!bharunList.contains(foot.getBharunUid()))
						bharunList.add(foot.getBharunUid());
					if (!wList.contains(foot.getWellUid()+":"+foot.getSection()))
						wList.add(foot.getWellUid()+":"+foot.getSection());
				}
			}
		}
		for (NPTToolSummary tool : npts)
		{
			if (tool.getLookupCompanyUid().equalsIgnoreCase(this.getCompanyCode()))
			{
				npt+=tool.getDuration();
			}else{
				cNpt+=tool.getDuration();
			}
		}
		
		Double cTime10k = cTime/cDistance*10000;
		Double cAvgRop10k = 10000/cTime10k;
		Double cNpt10k = cNpt/cDistance*10000;
		Double cRopNpt = 10000/(cNpt10k+cTime10k);
		
		Double time10k = time/distance*10000;
		Double avgRop10k = 10000/time10k;
		Double npt10k = npt/distance*10000;
		Double ropNpt = 10000/(npt10k+time10k);
		
		ReportDataNode data = node.addChild("FieldAreaRopNPTAnalysis");
		
		includeChildNode(data,"Competitor Sections","numSections",cWList.size());
		includeChildNode(data,"INTEQ Sections","numSections",wList.size());
		
		includeChildNode(data,"Competitor BHAs","numBhas",cBharunList.size());
		includeChildNode(data,"INTEQ BHAs","numBhas",bharunList.size());
		
		includeChildNode(data,"Competitor Avg ROP","avgRop",cAvgRop10k);
		includeChildNode(data,"INTEQ Avg ROP","avgRop",avgRop10k);
		
		includeChildNode(data,"Competitor Drill Time / 10000ft","time10k",cTime10k);
		includeChildNode(data,"INTEQ Drill Time / 10000ft","time10k",time10k);
		
		includeChildNode(data,"Competitor Total NPT Time","npt",cNpt);
		includeChildNode(data,"INTEQ Total NPT Time","npt",npt);
		
		includeChildNode(data,"Competitor NPT Time / 10000ft","npt10k",cNpt10k);
		includeChildNode(data,"INTEQ NPT Time / 10000ft","npt10k",npt10k);
		
		includeChildNode(data,"Competitor Total Time / 10000ft","total10k",cNpt10k+cTime10k);
		includeChildNode(data,"INTEQ Total Time / 10000ft","total10k",npt10k+time10k);
		
		includeChildNode(data,"Competitor ROP with NPT","ropNpt",cRopNpt);
		includeChildNode(data,"INTEQ ROP with NPT","ropNpt",ropNpt);
		
	}
	private void includeChildNode(ReportDataNode node, String title, String field, Object value)
	{
		ReportDataNode child = node.addChild("data");
		child.addProperty("title", title);
		child.addProperty(field, String.valueOf(value));
	}
	
	
	private void includeDistanceDrilledOverRop(
			List<BHAFootageSummary> footages, ReportDataNode node,
			UserSelectionSnapshot userSelection, Date start, Date end) throws Exception {
		ReportDataNode nodeTotalFootagePerArea = node.addChild("DistanceDilledOverRop"); 
		
		ReportOption cats = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY):null;
		ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;
		
		if (cats!=null){
			for (String cat : cats.getValues()){
				ReportDataNode child = nodeTotalFootagePerArea.addChild(cats.getValue(cat).toString().replace(" ", "_"));
				
				if (cos!=null){
					boolean isFirst = true;
					for (String coName : cos.getValues()){
						if (!isFirst)
						{
							ReportDataNode catChild = child.addChild("data");
							catChild.addProperty("label", "");
							catChild.addProperty("lookupCompanyUid", "");
							
						}else{
							isFirst=false;
						}
						List<Date> dates = DateRangeUtils.getDateList(start, end, false, cat);
						for (Date date: dates){
							Double progress =0.00;
							Double duration =0.00;
							Double rop = 0.00;
							ReportDataNode catChild = child.addChild("data");
							String formatLabel = DateRangeUtils.formatDateLabel(date, cat,true);
							catChild.addProperty("label", formatLabel);
							for (BHAFootageSummary foot : footages)
							{
								String formatLabelData = DateRangeUtils.formatDateLabel(foot.getDayDate(), cat,true);
								if (formatLabel.equalsIgnoreCase(formatLabelData) && foot.getDirectionalCompany().equalsIgnoreCase(coName)){
									progress += foot.getProgress();
									duration += foot.getDuration();
								}
							}
							catChild.addProperty("lookupCompanyUid", coName);
							catChild.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(coName).toString());
							catChild.addProperty("progress", progress.toString());
							catChild.addProperty("duration", duration.toString());
							if (progress!=0.00 && duration !=0.00)
								rop = progress/duration;
							catChild.addProperty("rop", String.valueOf(rop));
						}
						
					}
				}
			}
		}		
	}
	

	private void includeFieldAreaRopAnalysisSummary(List<BHAFootageSummary> footages, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception {
		ReportDataNode nodeData = node.addChild("FieldAreaRopAnalysisSummary");
		ReportOption wells = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL):null;	
		ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;

		List<String> wellboreList = new ArrayList<String>();
		for (BHAFootageSummary foot : footages)
		{
			if (foot.getIsCompetitor()!=null && !foot.getIsCompetitor() && !wellboreList.contains(foot.getWellboreUid()))wellboreList.add(foot.getWellboreUid());
		}
		
		for (String wb : wellboreList)
		{
			List<String> bharunsCompany = new ArrayList<String>();
			for(BHAFootageSummary foot : footages)
			{
				if (wb.equalsIgnoreCase(foot.getWellboreUid()) && foot.getIsCompetitor()!=null && !foot.getIsCompetitor())
				{
					if (!bharunsCompany.contains(foot.getBharunUid()+":"+foot.getDirectionalCompany()))
						bharunsCompany.add(foot.getBharunUid()+":"+foot.getDirectionalCompany());
				}
			}
			for (String com : bharunsCompany)
			{
				ReportDataNode child = nodeData.addChild("data");
				Double progress = 0.00;
				Double duration = 0.00;
				Double rop = 0.00;
				String lookupCompanyUid = null;
				String bharunUid=null;
				String wellUid = null;
				String wellboreName = null;
				for (BHAFootageSummary foot : footages)
				{
					String comp = foot.getBharunUid()+":"+foot.getDirectionalCompany();
					if ( comp.equalsIgnoreCase(com) && wb.equalsIgnoreCase(foot.getWellboreUid()))
					{
						progress +=foot.getProgress();
						duration +=foot.getDuration();
						if (lookupCompanyUid == null)lookupCompanyUid = foot.getDirectionalCompany();
						if (bharunUid == null)bharunUid = foot.getBharunUid();
						if (wellUid == null)wellUid = foot.getWellUid();
						if ( wellboreName == null) wellboreName = foot.getWellboreName();
					}
				}
				
				child.addProperty("wellUid",wellUid);
				child.addProperty("wellName",wells.getValue(wellUid).toString());
				child.addProperty("wellboreUid",wb);
				child.addProperty("wellboreName",wellboreName);
				child.addProperty("bharunUid", bharunUid);
				child.addProperty("lookupCompanyUid", lookupCompanyUid);
				child.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(lookupCompanyUid).toString());
				child.addProperty("isCompetitor", String.valueOf(false));
				child.addProperty("progress", String.valueOf(progress));
				child.addProperty("duration", String.valueOf(duration));
				if (progress!=0.00 && duration !=0.00)
					rop = progress/duration;
				child.addProperty("rop", String.valueOf(rop));
				
			}
		}
		
		List<String> cWellboreList = new ArrayList<String>();
		for (BHAFootageSummary foot : footages)
		{
			if (foot.getIsCompetitor()!=null && foot.getIsCompetitor() && !cWellboreList.contains(foot.getWellboreUid()))cWellboreList.add(foot.getWellboreUid());
		}
		for (String wb : cWellboreList)
		{
			List<String> bharunsCompany = new ArrayList<String>();
			for(BHAFootageSummary foot : footages)
			{
				if (wb.equalsIgnoreCase(foot.getWellboreUid()) && foot.getIsCompetitor()!=null && foot.getIsCompetitor())
				{
					if (!bharunsCompany.contains(foot.getBharunUid()+":"+foot.getDirectionalCompany()))
						bharunsCompany.add(foot.getBharunUid()+":"+foot.getDirectionalCompany());
				}
			}
			for (String com : bharunsCompany)
			{
				ReportDataNode child = nodeData.addChild("data");
				Double progress = 0.00;
				Double duration = 0.00;
				Double rop = 0.00;
				String lookupCompanyUid = null;
				String bharunUid=null;
				String wellUid = null;
				String wellboreName = null;
				for (BHAFootageSummary foot : footages)
				{
					String comp = foot.getBharunUid()+":"+foot.getDirectionalCompany();
					if ( comp.equalsIgnoreCase(com) && wb.equalsIgnoreCase(foot.getWellboreUid()))
					{
						progress +=foot.getProgress();
						duration +=foot.getDuration();
						if (lookupCompanyUid == null)lookupCompanyUid = foot.getDirectionalCompany();
						if (bharunUid == null)bharunUid = foot.getBharunUid();
						if (wellUid == null)wellUid = foot.getWellUid();
						if ( wellboreName == null) wellboreName = foot.getWellboreName();
					}
				}
				
				child.addProperty("wellUid",wellUid);
				child.addProperty("wellName",wells.getValue(wellUid).toString());
				child.addProperty("wellboreUid",wb);
				child.addProperty("wellboreName",wellboreName);
				child.addProperty("bharunUid", bharunUid);
				child.addProperty("lookupCompanyUid", lookupCompanyUid);
				child.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(lookupCompanyUid).toString());
				child.addProperty("isCompetitor", String.valueOf(true));
				child.addProperty("cprogress", String.valueOf(progress));
				child.addProperty("cduration", String.valueOf(duration));
				if (progress!=0.00 && duration !=0.00)
					rop = progress/duration;
				child.addProperty("crop", String.valueOf(rop));
				
			}
		}
	}
	private void includeDistanceDrilledOverRopFailure(
			List<BHAFootageSummary> footages, ReportDataNode node,
			UserSelectionSnapshot userSelection, Date start, Date end) throws Exception {
		ReportDataNode nodeTotalFootagePerArea = node.addChild("DistanceDilledOverRopFailure"); 
		
		ReportOption cats = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY):null;
		ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;
		
		if (cats!=null){
			for (String cat : cats.getValues()){
				ReportDataNode child = nodeTotalFootagePerArea.addChild(cats.getValue(cat).toString().replace(" ", "_"));
				List<Date> dates = DateRangeUtils.getDateList(start, end, false, cat);

				if (dates!=null){
					boolean isFirst = true;
					for (Date date : dates){
						if (!isFirst)
						{
							ReportDataNode catChild = child.addChild("data");
							catChild.addProperty("label", "");
							catChild.addProperty("lookupCompanyUid", "");
						}else{
							isFirst = false;
						}
						if (cos!=null)
						{
							for (String coName:cos.getValues())
							{
								Double aveProgress=0.00;
								Double progress =0.00;
								Double duration =0.00;
								Double rop = 0.00;
								Double failure = 0.00;
								Double reliability = 0.00;
								ReportDataNode catChild = child.addChild("data");
								String formatLabel = DateRangeUtils.formatDateLabel(date, cat,true);
								catChild.addProperty("label", formatLabel);
								List<String> bharuns = new ArrayList<String>();
								List<String> sections = new ArrayList<String>();
								for (BHAFootageSummary foot : footages)
								{
									String formatLabelData = DateRangeUtils.formatDateLabel(foot.getDayDate(), cat,true);
									if (formatLabel.equalsIgnoreCase(formatLabelData) && foot.getDirectionalCompany().equalsIgnoreCase(coName)){
										progress += foot.getProgress();
										duration += foot.getDuration();
										if (!bharuns.contains(foot.getBharunUid()))
										{
											bharuns.add(foot.getBharunUid());
											if (foot.getFailure()!=null && foot.getFailure())
												failure++;
										}
										if (this.countSectionByBharun)
										{
											if (!sections.contains(foot.getBharunUid()+":"+foot.getSection()))sections.add(foot.getBharunUid()+":"+foot.getSection());
										}else{
											if (!sections.contains(foot.getOperationUid()+":"+foot.getSection()))sections.add(foot.getOperationUid()+":"+foot.getSection());
										}
									}
								}
								
								catChild.addProperty("lookupCompanyUid", coName);
								catChild.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(coName).toString());

								catChild.addProperty("progress", progress.toString());
								if (progress!=0.00)
									aveProgress=progress/bharuns.size();
								catChild.addProperty("numBharun", String.valueOf(bharuns.size()));
								catChild.addProperty("aveProgress", aveProgress.toString());
								catChild.addProperty("numSection", String.valueOf(sections.size()));


								catChild.addProperty("duration", duration.toString());
								if (progress!=0.00 && duration !=0.00)
									rop = progress/duration;
								catChild.addProperty("rop", String.valueOf(rop));
								catChild.addProperty("failure", failure.toString());
								if (failure!=null && bharuns.size()>0)
								{
									reliability = (bharuns.size()-failure)/bharuns.size()*100;
								}
								catChild.addProperty("reliability", reliability.toString());
							}
						}
					}
				}
			}
		}		
	}
	protected List<BHAFootageSummary> shouldInclude(UserContext userContext, Date startDate, Date endDate) throws Exception
	{
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		ReportOption wells = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL):null;
		List<BHAFootageSummary> result = new ArrayList<BHAFootageSummary>();
		for (String wellUid : wells.getValues())
		{
			result.addAll(this.generatedata(userContext, startDate, endDate,wellUid));
		}
		return result;
	}
	private void includeTotalFootagePerArea(List<BHAFootageSummary> footages, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception
	{
		
		ReportDataNode nodeTotalFootagePerArea = node.addChild("TotalFootagePerArea"); 
		
		ReportOption cats = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY):null;
		ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;
		
		if (cats!=null){
			for (String cat : cats.getValues()){
				ReportDataNode child = nodeTotalFootagePerArea.addChild(cats.getValue(cat).toString().replace(" ", "_"));
				List<Date> dates = DateRangeUtils.getDateList(start, end, false, cat);
				for (Date date: dates){
					if (cos!=null){
						
						for (String coName : cos.getValues()){
							Double progress =0.00;
							Integer index=1;
							ReportDataNode catChild = child.addChild("data");
							String formatLabel = DateRangeUtils.formatDateLabel(date, cat,true);
							//catChild.addProperty("toolCategory", toolCat);
							//catChild.addProperty("toolCategory_lookupLabel", toolCats.getValue(toolCat).toString());
							catChild.addProperty("label", formatLabel);
							//catChild.addProperty("startDate", sdf.format(DateRangeUtils.getDateByType(date, false, cat, false)));
							//catChild.addProperty("endDate",sdf.format( DateRangeUtils.getDateByType(date, true, cat, false)));
							for (BHAFootageSummary foot : footages)
							{
								String formatLabelData = DateRangeUtils.formatDateLabel(foot.getDayDate(), cat,true);
								if (formatLabel.equalsIgnoreCase(formatLabelData) && foot.getDirectionalCompany().equalsIgnoreCase(coName))
									progress += foot.getProgress();
							}
							catChild.addProperty("lookupCompanyUid", coName);
							catChild.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(coName).toString());

							catChild.addProperty("progress", progress.toString());
							index++;
						}
					}
				}
			}
		}
	}
	public void setUseIADCDuration(Boolean useIADCDuration) {
		this.useIADCDuration = useIADCDuration;
	}
	public Boolean getUseIADCDuration() {
		return useIADCDuration;
	}
	public void setCountSectionByBharun(Boolean countSectionByBharun) {
		this.countSectionByBharun = countSectionByBharun;
	}
	public Boolean getCountSectionByBharun() {
		return countSectionByBharun;
	}
	public void setIsSummary(Boolean isSummary) {
		this.isSummary = isSummary;
	}
	public Boolean getIsSummary() {
		return isSummary;
	}
	public void setIncludeSections(Boolean includeSections) {
		this.includeSections = includeSections;
	}

	public Boolean getIncludeSections() {
		return includeSections;
	}
}
