package com.idsdatanet.d2.drillnet.report;

import java.util.Map;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobDataMap;

public class ScheduledReportGenerationJob implements Job {
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try{
			JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
			int mapsize = jobDataMap.size();
			Iterator iterator = jobDataMap.entrySet().iterator();
			for (int i = 0; i < mapsize; i++)
			{
				Map.Entry entry = (Map.Entry) iterator.next();
				ScheduledReportGenerationJobParams params =  (ScheduledReportGenerationJobParams) entry.getValue();
			  
				if(params.reportType == ScheduledReportGenerationJobParams.REPORT_TYPE_MANAGEMENT_REPORT){
					params.reportManager.generateManagementReportOnCurrentSystemDatetime(params.reportAutoEmail);
				}else if(params.reportType == ScheduledReportGenerationJobParams.REPORT_TYPE_DDR){
					params.reportManager.generateDDROnCurrentSystemDatetime(params.reportAutoEmail);
				}else if(params.reportType == ScheduledReportGenerationJobParams.REPORT_TYPE_DEFAULT_REPORT){
					params.reportManager.generateDefaultReportOnCurrentSystemDatetime(params.reportAutoEmail, params.defaultReportType, params.defaultReportModule);
				}
			
			}

		}catch(Exception e){
			Log logger = LogFactory.getLog(this.getClass());
			logger.error(e.getMessage(), e);
		}
	}
}
