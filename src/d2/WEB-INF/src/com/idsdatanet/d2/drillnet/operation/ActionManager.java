package com.idsdatanet.d2.drillnet.operation;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.stt.RFTActionHandler;
import com.idsdatanet.d2.core.stt.STTActionHandler;
import com.idsdatanet.d2.core.stt.STTComConfig;
import com.idsdatanet.d2.core.stt.STTJobConfig;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;

public class ActionManager implements com.idsdatanet.d2.core.web.mvc.ActionManager{
	private DeleteHandler deleteHandler = new DeleteHandler();
	
	private class DeleteHandler implements com.idsdatanet.d2.core.web.mvc.ActionHandler{
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			ApplicationUtils.getConfiguredInstance().setDeleteFlagOnOperation(((Operation) node.getData()));
			return new ActionHandlerResponse(true, false, null, BaseCommandBean.OPERATION_PERFORMED_DELETE);
		}
	}
	
	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if(node.getData() instanceof com.idsdatanet.d2.core.model.Operation){
			if(com.idsdatanet.d2.core.web.mvc.Action.DELETE.equals(action)){
				return this.deleteHandler;
			}
		}
		if (com.idsdatanet.d2.core.web.mvc.Action.SEND_TO_TOWN.equals(action)) {
			return new STTActionHandler(comConfig, jobConfigs);
		} else if (com.idsdatanet.d2.core.web.mvc.Action.REFRESH_FROM_TOWN.equals(action)) {
			return new RFTActionHandler(comConfig, jobConfigs);
		}
		
		if ("toggleLockOperation".equals(action)) {
			return this.lockOperationHandler;
		}
		
		return null;
	}
	
	private LockOperationHandler lockOperationHandler = new LockOperationHandler();
	
	private class LockOperationHandler implements com.idsdatanet.d2.core.web.mvc.ActionHandler {
		
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			Operation currentOperation = userSession.getCurrentOperation();
			if (currentOperation != null) {
				currentOperation.setIsLocked(!BooleanUtils.isTrue(currentOperation.getIsLocked()));
				currentOperation.setLockedByUserUid(userSession.getCurrentUser().getUserUid());
				currentOperation.setLockedDatetime(new Date());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(currentOperation);
								
				if (commandBean.getFlexClientControl() != null) {
					commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
				}
			}
			return new ActionHandlerResponse(true);
		}
	}
	
	private STTComConfig comConfig;
	private List<STTJobConfig> jobConfigs;
	
	public void setComConfig(STTComConfig comConfig) {
		this.comConfig = comConfig;
	}

	public void setJobConfigs(List<STTJobConfig> jobConfigs) {
		this.jobConfigs = jobConfigs;
	}
	
}
