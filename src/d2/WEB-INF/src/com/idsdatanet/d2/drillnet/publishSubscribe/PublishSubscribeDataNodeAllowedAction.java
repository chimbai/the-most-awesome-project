package com.idsdatanet.d2.drillnet.publishSubscribe;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.model.PublishSubscribe;
import com.idsdatanet.d2.core.publishsubscribe.PublishSubscribeManager;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class PublishSubscribeDataNodeAllowedAction implements DataNodeAllowedAction {

	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if (obj instanceof PublishSubscribe)
		{
			PublishSubscribe ps = (PublishSubscribe) obj;
			if (StringUtils.equals(ps.getType(), PublishSubscribeManager.PUBLISHER)) return false;
			if (BooleanUtils.isTrue(ps.getIsActive())) return false;
		}
		if (StringUtils.equals(action, "selectAll")) return false;
		if (StringUtils.equals(action, "edit"))  return false;
		return true;
	}
}
