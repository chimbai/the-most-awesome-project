package com.idsdatanet.d2.drillnet.wellHistoryOverview;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ProductionString;
import com.idsdatanet.d2.core.model.ProductionStringDetail;
import com.idsdatanet.d2.core.model.ProductionStringDetailLog;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ProductionStringHistoryOverviewDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		node.getDynaAttr().put("screenOption", (String) commandBean.getRoot().getDynaAttr().get("ScreenOutput"));	
		
		// a trick to only load used Operation object (initially load as String, only load the real object when needed)
		if((obj instanceof String) && meta.getTableClass() == ProductionStringDetailLog.class){
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ProductionStringDetailLog WHERE productionStringDetailLogUid=:productionStringDetailLogUid", "productionStringDetailLogUid", obj.toString());
			obj = lstResult.get(0);
			node.setData(obj);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		if (obj instanceof ProductionStringDetailLog) {
			ProductionStringDetailLog productionStringDetailLog = (ProductionStringDetailLog) obj;
			
			String componentUid = productionStringDetailLog.getProductionStringDetailUid();
			
			if (StringUtils.isNotBlank(componentUid)) {
				ProductionStringDetail productionstringdetail = (ProductionStringDetail) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ProductionStringDetail.class, componentUid);
				
				if (productionstringdetail !=null) {
					node.getDynaAttr().put("componentType", productionstringdetail.getComponentType());
					node.getDynaAttr().put("description", productionstringdetail.getDescription());
					node.getDynaAttr().put("sequence", productionstringdetail.getSequence());
					node.getDynaAttr().put("remark", productionstringdetail.getRemark());
					
					CustomFieldUom thisConverter = new CustomFieldUom(userSelection.getLocale());
					
					generateUomField(node,thisConverter,"depthMdMsl",productionstringdetail.getDepthMdMsl(),true);
					generateUomField(node,thisConverter,"depthTvdMsl",productionstringdetail.getDepthTvdMsl(),true);	
					generateUomField(node,thisConverter,"bottomDepthMdMsl",productionstringdetail.getBottomDepthMdMsl(),true);
					generateUomField(node,thisConverter,"totalLength",productionstringdetail.getTotalLength(),false);	
					generateUomField(node,thisConverter,"sizeOd",productionstringdetail.getSizeOd(),false);	
					generateUomField(node,thisConverter,"sizeId",productionstringdetail.getSizeId(),false);				

					if (productionstringdetail.getServiceDateTime()!=null) node.getDynaAttr().put("serviceDateTime",sdf.format(productionstringdetail.getServiceDateTime()));
					
					String productionStringUid = productionstringdetail.getProductionStringUid();
					
					ProductionString productionString = (ProductionString) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ProductionString.class, productionStringUid);
					
					if (productionString != null) {
						node.getDynaAttr().put("stringNo", productionString.getStringNo());	
						
						if (productionString.getOperationUid()!=null){
							Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, productionString.getOperationUid());
							node.getDynaAttr().put("operationName", operation.getOperationName());
						}							
					}					
				}
				
			}
			
			Date serviceDatetime = productionStringDetailLog.getServiceDateTime();
			
			if (serviceDatetime !=null) {
				Calendar thisCalendar = Calendar.getInstance();
				thisCalendar.setTime(serviceDatetime);
				thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
				thisCalendar.set(Calendar.MINUTE, 0);
				thisCalendar.set(Calendar.SECOND , 0);
				thisCalendar.set(Calendar.MILLISECOND , 0);
				
				String operationUid = productionStringDetailLog.getOperationUid();
				
				String[] paramFields = {"operationUid", "serviceDate"};
				Object[] paramValues = {operationUid, thisCalendar.getTime()};
				//LOAD LATEST TUBING STRING REPORT
				List items2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles WHERE reportOperationUid =:operationUid and reportDayDate =:serviceDate and (isDeleted = false or isDeleted is null) and reportType = 'TSR' order by reportDayDate DESC", paramFields, paramValues);
				if(items2.size() > 0){
				
					ReportFiles thisReportFile = (ReportFiles) items2.get(0);
					File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());
					
					if(report_file.exists()){
						node.getDynaAttr().put("tsrReportDate", sdf.format(thisReportFile.getReportDayDate()));
						node.getDynaAttr().put("tsrReportFilesUid", thisReportFile.getReportFilesUid());
						node.getDynaAttr().put("tsrReportDisplayName", thisReportFile.getDisplayName());
						node.getDynaAttr().put("tsrFileExtension", this.getFileExtension(thisReportFile.getReportFile()));
						node.getDynaAttr().put("tsrReportType", "TSR");
					}
				}
			}

		}
		
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		return true;
	}

	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		
		boolean showLatest = false;
		
		String selectedScreenOutputFilter = (String) commandBean.getRoot().getDynaAttr().get("ScreenOutput");			
		if (selectedScreenOutputFilter !=null){				
			if (selectedScreenOutputFilter.equalsIgnoreCase("latest")) showLatest =true;
		}
						
		String[] paramsFields = null;
		Object[] paramsValues = null;	
		List<Object> output_maps = null;
		
		if (meta.getTableClass().equals(ProductionStringDetailLog.class)) {
			if (showLatest){					
				String productionStringUid= "";
				
				/*String sql = "select a.productionStringUid from ProductionString a,Operation b where a.operationUid = b.operationUid and (a.isDeleted is null or a.isDeleted = false) " +
							 "and (b.isDeleted is null or b.isDeleted = false) and a.wellboreUid =:wellboreUid order by a.installDateTime desc,b.sysOperationStartDatetime desc";
				*/
				
				String sql = "select productionStringUid from ProductionString where (isDeleted is null or isDeleted = false) and wellboreUid =:wellboreUid order by installDateTime desc";
				
				List lstData = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql ,"wellboreUid", userSelection.getWellboreUid());
				
				if (lstData.size()>0){
					for(Object item : lstData){
						if (item != null){
							productionStringUid = String.valueOf(item);
						}
						break;
					}
				}
			
				paramsFields = new String[] {"productionStringUid"};
				paramsValues = new Object[] {productionStringUid};	
				
				sql = "Select a.productionStringDetailLogUid,a.productionStringDetailUid from ProductionStringDetailLog a, ProductionString b, ProductionStringDetail c " +
				"where (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (c.isDeleted = false or c.isDeleted is null) " +
				"and b.productionStringUid=:productionStringUid and a.productionStringDetailUid = c.productionStringDetailUid and b.productionStringUid = c.productionStringUid group by a.productionStringDetailLogUid order by c.depthMdMsl ASC";

				List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql ,paramsFields, paramsValues);
				
				output_maps = new ArrayList<Object>();
				
				ArrayList filterList = new ArrayList<String>();
				
				for(Iterator i = items.iterator(); i.hasNext(); ){
					Object[] item = (Object[])i.next();
					String logUid = item[0].toString();
					String productionStringDetailUid =  item[1].toString(); 
					
					if (filterList.contains(productionStringDetailUid)) 
						continue;
					else
						filterList.add(productionStringDetailUid);
					
					output_maps.add(logUid);
				}
			}	
			else{
				
				paramsFields = new String[] {"wellboreUid"};
				paramsValues = new Object[] {userSelection.getWellboreUid()};	
				
				String sql = "Select a.productionStringDetailLogUid from ProductionStringDetailLog a, ProductionString b, ProductionStringDetail c " +
				"where (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (c.isDeleted = false or c.isDeleted is null) " +
				"and a.wellboreUid=:wellboreUid and a.productionStringDetailUid = c.productionStringDetailUid and b.productionStringUid = c.productionStringUid group by a.productionStringDetailLogUid order by b.installDateTime, b.stringNo, c.installDateTime, c.componentType, a.serviceDateTime DESC,a.depthMdMsl ASC";

				List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql ,paramsFields, paramsValues);
				
				output_maps = new ArrayList<Object>();
				
				for(Iterator i = items.iterator(); i.hasNext(); ){
					String logUid = (String)i.next();
					output_maps.add(logUid);
				}
			}
			
					
			
			return output_maps;
		}
		return null;
	}
	
	private String getFileExtension(String reportFile){
		int i = reportFile.lastIndexOf(".");
		if(i != -1){
			return reportFile.substring(i + 1);
		}else{
			return null;
		}
	}

	private void generateUomField(CommandBeanTreeNode node,CustomFieldUom thisConverter,String DataField, Object Data ,boolean isDatumField) throws Exception{
		
		String formattedvalue = "";
		
		thisConverter.setReferenceMappingField(ProductionStringDetail.class, DataField);
		
		if (Data != null)
		{
			if (thisConverter.isUOMMappingAvailable()){
				thisConverter.setBaseValueFromUserValue(Double.parseDouble(Data.toString()));
				if (isDatumField) thisConverter.addDatumOffset();
				formattedvalue = thisConverter.getFormattedValue();	
			}
			else{
				formattedvalue = Data.toString();
			}			
		}				
		
		node.getDynaAttr().put(DataField + "WithUomSymbol", formattedvalue);
	}
}
