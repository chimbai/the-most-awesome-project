package com.idsdatanet.d2.drillnet.activity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Campaign;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.ftr.FTRDataNodeListener;

public class TimeBaseDataNodeListener extends FTRDataNodeListener {
	private String defaultReportingPeriod = "0_24";
	
	public void setDefaultReportingPeriod(String value){
		this.defaultReportingPeriod = value;
	}
	
	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		super.afterDataNodeLoad(commandBean, meta, node, userSelection, request);
		
		Object object = node.getData();
		if (object instanceof Activity) {
			Activity act = (Activity) object;
			String preLabelActivityDescription = "";
			
			String timeBase = this.getCampaignReportingPeriod(userSelection.getOperationUid());
			if (timeBase==null || StringUtils.isBlank(timeBase)){
				timeBase = defaultReportingPeriod;
			}
			
			Calendar thisCalendar = Calendar.getInstance();
			
			if (act.getStartDatetime()!=null){
				thisCalendar.setTime(act.getStartDatetime());
			}
			Date startDate = thisCalendar.getTime();
			
			if (act.getEndDatetime()!=null){
				thisCalendar.setTime(act.getEndDatetime());
			}
			Date endDate = thisCalendar.getTime();
			
			Date startTimeBase = this.getStartTimeBaseAct(timeBase, userSelection.getDailyUid());
			Date endTimeBase = this.getEndTimeBaseAct(timeBase, userSelection.getDailyUid(), userSelection.getOperationUid());
			
			if (startTimeBase!=null && endTimeBase!=null){
				if (startDate.getTime()>startTimeBase.getTime() && startDate.getTime() < endTimeBase.getTime()){	
					node.getDynaAttr().put("startOffset", 0);
				}else{
					Long diff = startTimeBase.getTime() - startDate.getTime();
					node.getDynaAttr().put("startOffset", diff);
					
					if (diff>0){
						startDate = startTimeBase;
						preLabelActivityDescription = ""; //activity continues from previous reporting period
					}
					
				}
				
				if (endDate.getTime()>startTimeBase.getTime() && endDate.getTime() > endTimeBase.getTime()){	
					Long diff = endDate.getTime() - endTimeBase.getTime() ;
					node.getDynaAttr().put("endOffset", diff);
					
					if (diff>0){
						endDate = endTimeBase;
						preLabelActivityDescription = "[In Progress] ";
					}
				}else{
					node.getDynaAttr().put("endOffset", 0);
				}
			}	
			
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
			
			node.getDynaAttr().put("startDateTime", formatter.format(startDate));
			
			if ("23:59".equals(formatter.format(endDate))){
				node.getDynaAttr().put("endDateTime", "24:00");
			}else{
				node.getDynaAttr().put("endDateTime", formatter.format(endDate));
			}
			
			
			if(startDate != null && endDate != null){
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(startDate, endDate);
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
				thisConverter.setBaseValue(thisDuration);
				
				node.getDynaAttr().put("actDuration", thisConverter.getConvertedValue());
			}
			
			node.getDynaAttr().put("preLabelActivityDescription", preLabelActivityDescription);
		}
		
	}
	
	private Date getStartTimeBaseAct(String timeBase, String dailyUid) throws Exception{
		Date start = null;
		
		if (StringUtils.isNotBlank(timeBase)){
			String[] time = timeBase.split("_");
			
			if (time.length>0 && time.length==2){
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
				
				Calendar thisCalendar = Calendar.getInstance();
				thisCalendar.setTime(daily.getDayDate());
				thisCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
				start = thisCalendar.getTime();
			}
		}
	
		return start;
	}
	
	private Date getEndTimeBaseAct(String timeBase, String dailyUid, String operationUid) throws Exception{
		Date end = null;
		
		if (StringUtils.isNotBlank(timeBase)){
			String[] time = timeBase.split("_");
			
			if (time.length>0 && time.length==2){
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
				
				Calendar thisCalendar = Calendar.getInstance();
				thisCalendar.setTime(daily.getDayDate());
				thisCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
				
				thisCalendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(time[1]));
				end = thisCalendar.getTime();
				/*
				Daily tomorrow = ApplicationUtils.getConfiguredInstance().getTomorrow(operationUid, dailyUid);
				
				if (tomorrow != null) {
					List<ReportDaily> drillnetReportDailyList = ActivityUtils.getDrillnetReportDaily(tomorrow.getDailyUid());
					
					if (!drillnetReportDailyList.isEmpty()) {
						thisCalendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(time[1]));
						end = thisCalendar.getTime();
					}
				}*/
			}
		}
		
		
		
		return end;
	}
	
	public String getCampaignReportingPeriod(String operationUid) throws Exception{
		Operation op = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
		
		if(op!=null && StringUtils.isNotBlank(op.getOperationCampaignUid())){
			Campaign campaign = (Campaign) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Campaign.class, op.getOperationCampaignUid());
			
			if (campaign!=null){
				return campaign.getReportingPeriod();
			}
		}
		
		return null;
	}
}
