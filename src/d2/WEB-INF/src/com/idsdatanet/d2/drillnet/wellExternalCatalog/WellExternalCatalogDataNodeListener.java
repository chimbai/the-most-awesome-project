package com.idsdatanet.d2.drillnet.wellExternalCatalog;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.core.model.WellExternalCatalog;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
	
public class WellExternalCatalogDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener  {
	public static final String EXTERNAL_WELL_CATALOG = "EXT_WELL_CATALOG";
    
	/**
	 * Runs on clicking the "Import As New" or "Import Selected" button for a WEC record on WellExternalCatalog screen.
	 * Fetches the WEC record for import, and sends to forwardToImporter() to start the import process.
	 */
	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(targetCommandBeanTreeNode == null) throw new Exception("Unexpected Error. Cannot fetch selected WellExternalCatalog object!"); // for safe keeping
		WellExternalCatalog wec = (WellExternalCatalog) targetCommandBeanTreeNode.getData();
		// grab the addAdditionalFormRequestParams from the button set in wellExternalCatalog.js
		String importType = (String) request.getParameter("ImportType");
		// When the user click on either "Import All From Server" or "Import Well/Wellbore Only"
		if("importAll".equals(importType) || "importWWOnly".equals(importType)) 
			WellExternalCatalogManager.getConfiguredInstance().importWEC(wec, commandBean, importType);
		// if the user uses the multi-select import
		if("importSelected".equals(importType))
			WellExternalCatalogManager.getConfiguredInstance().importSelectedWECs(request, commandBean);
		// if the user uses the indexing external well feature
		if("indexExternalWells".equals(importType))
			WellExternalCatalogManager.getConfiguredInstance().indexExternalWells(request, commandBean);
	}

	@Override
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response,
			BaseCommandBean commandBean, String invocationKey) throws Exception {
		if("checkDepotVersion".equals(invocationKey))
			WellExternalCatalogManager.getConfiguredInstance().checkDepotVersion(request,response);
	}

	@Override
	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
