package com.idsdatanet.d2.drillnet.equipmentFailure;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.EquipmentFailure;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class EquipmentFailureCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(EquipmentFailure.class) && "@activityUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				EquipmentFailure equipmentFailure = (EquipmentFailure) targetCommandBeanTreeNode.getData();
				equipmentFailure.setDateFailStart(null);
				equipmentFailure.setDateFailEnd(null);
				equipmentFailure.setCauseOfFailure(null);
				equipmentFailure.setFailureType(null);
				equipmentFailure.setFailDuration(null);
				equipmentFailure.setDepthMdMsl(null);
				equipmentFailure.setFailureDetail(null);
				equipmentFailure.setCompany(null);
				targetCommandBeanTreeNode.getDynaAttr().put("Activity.equipmentCode", null);
				targetCommandBeanTreeNode.getDynaAttr().put("Activity.subEquipmentCode", null);

				String activityid = (String) targetCommandBeanTreeNode.getDynaAttr().get("activityUid");
				if (StringUtils.isNotBlank(activityid)) {
					Activity activity = (Activity) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Activity.class, activityid);
					if (activity != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {
						Date startTime = activity.getStartDatetime();
						Date endTime = activity.getEndDatetime();
						
						equipmentFailure.setDateFailStart(startTime);
						equipmentFailure.setDateFailEnd(endTime);
						equipmentFailure.setCauseOfFailure(activity.getFailureCause());
						equipmentFailure.setFailureType(activity.getFailureType());
						equipmentFailure.setCompany(activity.getLookupCompanyUid());
						
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, EquipmentFailure.class, "failDuration");
						if(startTime != null && endTime != null) {
							if(endTime.after(startTime)) {
								Long difference = CommonUtil.getConfiguredInstance().calculateDuration(startTime, endTime);
								//thisConverter.setReferenceMappingField(EquipmentFailure.class, "failDuration");
								thisConverter.setBaseValue(difference);
								equipmentFailure.setFailDuration(thisConverter.getConvertedValue());
							}
						}
						equipmentFailure.setDepthMdMsl(activity.getDepthMdMsl());
						//node.getDynaField().getValues().put("depth_md", activity.getDepthMdMsl());
						equipmentFailure.setFailureDetail(activity.getActivityDescription());
						//targetCommandBeanTreeNode.getDynaAttr().put("activityUid", activity.getActivityUid());
						targetCommandBeanTreeNode.getDynaAttr().put("Activity.equipmentCode", activity.getEquipmentCode());
						targetCommandBeanTreeNode.getDynaAttr().put("Activity.subEquipmentCode", activity.getSubEquipmentCode());
					}
				}
			}
		}
	}

}
