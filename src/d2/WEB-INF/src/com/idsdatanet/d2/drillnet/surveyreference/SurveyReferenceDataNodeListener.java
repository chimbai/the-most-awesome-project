package com.idsdatanet.d2.drillnet.surveyreference;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.graph.GraphManager;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.SurveyReference;
import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class SurveyReferenceDataNodeListener extends EmptyDataNodeListener {
	private GraphManager graphManager = null;
	private Boolean showSurveyByDayDepth = false;
		
	public GraphManager getGraphManager() {
		return graphManager;
	}

	public void setGraphManager(GraphManager graphManager) {
		this.graphManager = graphManager;
	}
	
	public Boolean getShowSurveyByDayDepth() {
		return showSurveyByDayDepth;
	}
	
	public void setShowSurveyByDayDepth(Boolean showSurveyByDayDepth){
		this.showSurveyByDayDepth = showSurveyByDayDepth;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		Object objAllow = commandBean.getRoot().getDynaAttr().get("allowContinue");
		
		if (objAllow!=null && "1".equalsIgnoreCase(objAllow.toString()))
		{
			status.setContinueProcess(false, true);
			if (commandBean.getFlexClientControl() != null) {
				commandBean.getFlexClientControl().setReloadAfterPageCancel();
			}
		}
		
		if (object instanceof SurveyReference)
		{
			this.autoFlagOtherStatus(commandBean, node, session, status, request);
			if(this.isStatusBackToSurvey(commandBean, node, session, status, request))
			{
				commandBean.getSystemMessage().addWarning("You have just set the Status from '{Current/Definitive}' to 'Survey'. There is no 'Current' or Definitive' survey");
			}
		}
		
	}
	
	private boolean isStatusBackToSurvey(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		SurveyReference survey = (SurveyReference)node.getData();
		if (StringUtils.isNotBlank(survey.getSurveyReferenceUid()) && StringUtils.isNotBlank(survey.getStatusIndicator()))
		{
			if ("Survey".equalsIgnoreCase(survey.getStatusIndicator()))
			{
				SurveyReference dbSurvey = this.getSurveyReferenceBy(survey.getSurveyReferenceUid());
				if ("Definitive".equalsIgnoreCase(dbSurvey.getStatusIndicator()) || "Current".equalsIgnoreCase(dbSurvey.getStatusIndicator()))
					return true;
			}
		}
		return false;
	}
	private void autoFlagOtherStatus(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
		SurveyReference survey = (SurveyReference)node.getData();
		if ("Definitive".equalsIgnoreCase(survey.getStatusIndicator()) || "Current".equalsIgnoreCase(survey.getStatusIndicator()))
		{
			if (StringUtils.isNotBlank(survey.getSurveyReferenceUid()))
			{
				SurveyReference dbSurvey = this.getSurveyReferenceBy(survey.getSurveyReferenceUid());
				if (dbSurvey!=null)
				{
					if (StringUtils.isBlank(dbSurvey.getStatusIndicator()) || "Survey".equalsIgnoreCase(dbSurvey.getStatusIndicator()))
					{
						String strSQL = "UPDATE SurveyReference set statusIndicator = 'Survey' where wellboreUid=:wellboreUid and surveyReferenceUid!=:surveyReferenceUid and statusIndicator IS NOT NULL";
						String[] params = {"wellboreUid","surveyReferenceUid"};
						Object[] values = {survey.getWellboreUid(), survey.getSurveyReferenceUid()};
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSQL, params, values);
					}
				}
			}else{
				String strSQL = "UPDATE SurveyReference set statusIndicator = 'Survey' where wellboreUid=:wellboreUid and statusIndicator IS NOT NULL";
				String[] params = {"wellboreUid"};
				Object[] values = {survey.getWellboreUid()};
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSQL, params, values);
			}
			
		}
		
		
		
	}
	
	private SurveyReference getSurveyReferenceBy(String surveyReferenceUid) throws Exception
	{
		Object obj = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SurveyReference.class, surveyReferenceUid);
		if (obj!=null)
			return (SurveyReference) obj;
		return null;
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {		
		Object object = node.getData();
		if (object instanceof SurveyReference) {
			SurveyReference thisSurveyRef = (SurveyReference) object;
			
			//for unit detection
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, SurveyStation.class, "depthMdMsl");
			if ("Metre".equals(thisConverter.getUomLabel())) thisSurveyRef.setBuildDropWalk(30.0);
			if ("Feet".equals(thisConverter.getUomLabel())) thisSurveyRef.setBuildDropWalk(100.0);
			
			UserContext userContext = UserContext.getUserContext(userSelection);
			Map<String, Object> customProperties = new HashMap<String, Object>();
			customProperties.put("surveyReferenceUid", thisSurveyRef.getSurveyReferenceUid());
			
			this.graphManager.process(userContext, request, null, "surveyReferenceTvdVsect", "medium", customProperties);
			this.graphManager.process(userContext, request, null, "surveyReferenceNsEw", "medium", customProperties);
			
		} else if (object instanceof SurveyStation) {
			//calculate TVDSS
			SurveyStation thisSurveyStation = (SurveyStation) object; 
			
			if (thisSurveyStation.getDepthTvdMsl()!=null) {
				// - this is the value to user reference point (without offset to msl) 
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, SurveyStation.class, "depthTvdMsl");
				thisConverter.setBaseValueFromUserValue(thisSurveyStation.getDepthTvdMsl(), false);
				thisConverter.removeDatumOffsetToUserReferencePoint();
				node.getDynaAttr().put("depthTvdSubsea", thisConverter.getConvertedValue());
				if (thisConverter.isUOMMappingAvailable()) node.setCustomUOM("@depthTvdSubsea", thisConverter.getUOMMapping());
				
				// - this should be the correct value to subsea - <updateid=20100322-0821-alaw>
				thisConverter = new CustomFieldUom(commandBean, SurveyStation.class, "depthTvdMsl");
				thisConverter.setBaseValueFromUserValue(thisSurveyStation.getDepthTvdMsl(), false);
				thisConverter.removeDatumOffset();
				node.getDynaAttr().put("depthTvdMslSubsea", thisConverter.getConvertedValue());
				if (thisConverter.isUOMMappingAvailable()) node.setCustomUOM("@depthTvdMslSubsea", thisConverter.getUOMMapping());
			}
		}
	}
	
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		if ("SurveyStation".equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted())
		{
			
			String currentDay = null;
			String currentTool = null;
			parent.sortChild("SurveyStation", new DepthComparator());
			for (CommandBeanTreeNode node:status.getUpdatedNodes())
			{
				SurveyStation currSurveyStation = (SurveyStation) node.getData();
				Boolean needUpdate = false;
				if (StringUtils.isBlank(currentDay)) {
					if (StringUtils.isBlank(currSurveyStation.getDailyUid())) {
						// do nothing.
					}else
						currentDay = currSurveyStation.getDailyUid();
				}else{
					if (StringUtils.isBlank(currSurveyStation.getDailyUid())) {
						currSurveyStation.setDailyUid(currentDay);
						needUpdate = true;
					}else{
						currentDay = currSurveyStation.getDailyUid();
					}
				}
				if (StringUtils.isBlank(currentTool)) {
					if (StringUtils.isBlank(currSurveyStation.getToolType())) {
						// do nothing.
					}else currentTool = currSurveyStation.getToolType();
				}else{
					if (StringUtils.isBlank(currSurveyStation.getToolType())) {
						currSurveyStation.setToolType(currentTool);
						needUpdate = true;
					}else{
						currentTool = currSurveyStation.getToolType();
					}
				}
				if (needUpdate)
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(currSurveyStation);
				
			}
			
			if(showSurveyByDayDepth){
				String operationUid="";
				String dailyUid="";
				if(StringUtils.isNotBlank(userSession.getCurrentOperationUid())){
					operationUid = userSession.getCurrentOperationUid();
				}
				if(StringUtils.isNotBlank(userSession.getCurrentDailyUid())){
					dailyUid = userSession.getCurrentDailyUid();
				}
				CommonUtil.getConfiguredInstance().setLastSurveyDepthByDayDepth(operationUid, dailyUid, userSession);
			}
		}
	}
	
	private class DepthComparator implements Comparator<CommandBeanTreeNode>{
		public int compare(CommandBeanTreeNode o1, CommandBeanTreeNode o2) {
			try{
				SurveyStation ss1 = (SurveyStation) o1.getData();
				SurveyStation ss2 = (SurveyStation) o2.getData();
				Double depth1 = null;
				Double depth2 = null;
				if(ss1.getDepthMdMsl()!=null && ss2.getDepthMdMsl()!=null){
					depth1 = ss1.getDepthMdMsl();
					depth2 = ss2.getDepthMdMsl();
				}else{
					depth1 = null;
					depth2 = null;
				}
				
				if(depth1 == null && depth2 == null){
					return 0;
				}else if(depth1 == null){
					return -1;
				}else if(depth2 == null){
					return 1;
				}else{
					return depth1.compareTo(depth2);
				}
			}catch(Exception e){
				Log logger = LogFactory.getLog(this.getClass());
				logger.error(e.getMessage(), e);
				return 0;
			}
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {		
		Object object = node.getData();
		if (object instanceof SurveyReference) {
			SurveyReference thisSurveyRef = (SurveyReference) object;
			
			//for unit detection
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, SurveyStation.class, "depthMdMsl");
			if ("Metre".equals(thisConverter.getUomLabel())) thisSurveyRef.setBuildDropWalk(30.0);
			if ("Feet".equals(thisConverter.getUomLabel())) thisSurveyRef.setBuildDropWalk(100.0);
			this.autoPopulateStatus(commandBean, meta, node, userSelection, request);
		}
		if (object instanceof SurveyStation) {
			this.autoPopulateOperationUid(commandBean, meta, node, userSelection, request);
		}
	}
	
	private void autoPopulateStatus(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception 
	{
		Collection<CommandBeanTreeNode> surveyReferenceList = commandBean.getRoot().getList().get(SurveyReference.class.getSimpleName()).values();
		int counter = 0;
		for ( CommandBeanTreeNode nodeReference : surveyReferenceList)
		{		
			SurveyReference survey = (SurveyReference) nodeReference.getData();	
			if ("Current".equalsIgnoreCase(survey.getStatusIndicator()) || "Definitive".equalsIgnoreCase(survey.getStatusIndicator()))
				counter++;
		}
		if (counter==0)
		{
			SurveyReference survey = (SurveyReference) node.getData();
			survey.setStatusIndicator("Current");
		}
	}
	
	private void autoPopulateOperationUid(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception 
	{
		SurveyStation surveyStation = (SurveyStation) node.getData();
		surveyStation.setOperationUid(userSelection.getOperationUid());
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		if(!showSurveyByDayDepth)
		{
			String operationUid="";
			String dailyUid="";
			if(StringUtils.isNotBlank(session.getCurrentOperationUid()))
			{
				operationUid = session.getCurrentOperationUid();
			}
			if(StringUtils.isNotBlank(session.getCurrentDailyUid()))
			{
				dailyUid = session.getCurrentDailyUid();
			}
			CommonUtil.getConfiguredInstance().setLastSurveyDepth(operationUid, dailyUid, commandBean);
		}
	}
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		String operationUid="";
		String dailyUid="";
		if(StringUtils.isNotBlank(session.getCurrentOperationUid()))
		{
			operationUid = session.getCurrentOperationUid();
		}
		if(StringUtils.isNotBlank(session.getCurrentDailyUid()))
		{
			dailyUid = session.getCurrentDailyUid();
		}
		if(showSurveyByDayDepth)
			CommonUtil.getConfiguredInstance().setLastSurveyDepthByDayDepth(operationUid, dailyUid, session);
		else
			CommonUtil.getConfiguredInstance().setLastSurveyDepth(operationUid, dailyUid, commandBean);
	}
	
	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{
		Object object = targetNode.getData();
		if (object instanceof SurveyReference) {
			SurveyReference surveyReference = (SurveyReference) object;
			surveyReference.setImportParameters(null);
		}
	}
}