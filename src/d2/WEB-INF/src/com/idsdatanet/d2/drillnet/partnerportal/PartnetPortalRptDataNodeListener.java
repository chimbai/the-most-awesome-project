package com.idsdatanet.d2.drillnet.partnerportal;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.PersonnelOnSite;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.view.FreemarkerUtils;

public class PartnetPortalRptDataNodeListener extends EmptyDataNodeListener {
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {

	}

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if(object instanceof ReportFiles) {
			ReportFiles rf = (ReportFiles) node.getData();

			if (rf.getDateGenerated() != null) {
				String timeTxt = FreemarkerUtils.formatDurationSince(rf.getDateGenerated());
				boolean isNew = FreemarkerUtils.isDocumentNewlyGenerated(rf.getDateGenerated());
				node.getDynaAttr().put("timeReportFileCreatedText", timeTxt);
				node.getDynaAttr().put("newlyGeneratedFlag", isNew);
			}	
		}

	}
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {

	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{

	}

}
