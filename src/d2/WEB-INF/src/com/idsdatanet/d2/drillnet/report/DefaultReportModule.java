package com.idsdatanet.d2.drillnet.report;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Campaign;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.EmailList;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.report.BeanDataGenerator;
import com.idsdatanet.d2.core.report.FOPJob;
import com.idsdatanet.d2.core.report.MultiWellXslJob;
import com.idsdatanet.d2.core.report.MultipleUserSelectionSnapshot;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.XslJob;
import com.idsdatanet.d2.core.report.birt.BirtReportJob;
import com.idsdatanet.d2.core.report.jasper.JasperJob;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.hibernate.tables.UomTemplate;
import com.idsdatanet.d2.core.uom.mapping.AbstractDatumFormatter;
import com.idsdatanet.d2.core.uom.mapping.UOMManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.infra.uom.OperationUomContext;

public class DefaultReportModule extends AbstractReportModule implements InitializingBean {
	public static final String TRANSFORMATION_TYPE_FOP = "fop";
	public static final String TRANSFORMATION_TYPE_XSL = "xsl";
	
	public static final String BIRT_REPORT_OUTPUT_TYPE_PDF = "pdf";
	public static final String BIRT_REPORT_OUTPUT_TYPE_WORD = "doc";
	public static final String BIRT_REPORT_OUTPUT_TYPE_XLS = "xls";
	public static final String BIRT_REPORT_OUTPUT_TYPE_HTML = "html";
	public static final String JASPER_REPORT_OUTPUT_TYPE_PDF = "pdf";
	public static final String JASPER_REPORT_OUTPUT_TYPE_XLS = "xls";
	public static final String JASPER_REPORT_OUTPUT_TYPE_WORD = "doc";
	protected static final String FILE_NAME_PATTERN_DATE = "date";
	protected static final String FILE_NAME_PATTERN_DATE_SEPARATOR = "::";
	protected static final String FILE_NAME_PATTERN_CURRENT_DATE = "currentdate";
	protected static final String FILE_NAME_PATTERN_WELL = "well";
	protected static final String FILE_NAME_PATTERN_WELLBORE = "wellbore";
	protected static final String FILE_NAME_PATTERN_WELL_WELLBORE_COMBINED = "well_wellbore";
	protected static final String FILE_NAME_PATTERN_OPERATION = "operation";
	protected static final String FILE_NAME_PATTERN_DAY_NUMBER = "dayNumber";
	protected static final String FILE_NAME_PATTERN_RIG = "rig";
	protected static final String FILE_NAME_PATTERN_QC_FLAG = "qcflag";
	protected static final String FILE_NAME_PATTERN_REPORT_TYPE = "reportType";
	protected static final String FILE_NAME_PATTERN_OPERATION_PREFIX = "operationPrefix";
	protected static final String FILE_NAME_PATTERN_PAPER_SIZE = "paperSize";
	protected static final String FILE_NAME_PATTERN_CUSTOM_PROPERTY_PREFIX = "customProperty:";
	protected static final String FILE_NAME_PATTERN_UOM_TEMPLATE_NAME = "uomTemplate";
	protected static final String FILE_NAME_PATTERN_OPERATION_TYPE = "operationType";
	protected static final String FILE_NAME_PATTERN_COMPANY = "company";
	protected static final String FILE_NAME_PATTERN_MIDNIGHT_DEPTH = "midnightDepth";
	protected static final String FILE_NAME_PATTERN_OPERATION_NAME_WITH_UNDERSCORE = "operationNameWithUnderscore";
	protected static final String FILE_NAME_PATTERN_DATUM_LABEL = "datumLabel";
	protected static final String FILE_NAME_PATTERN_CAMPAIGN = "campaign";
	protected static final String FILE_NAME_PATTERN_GENERATE_YYYY = "year";
	protected static final String FILE_NAME_PATTERN_GENERATE_HMS = "hms";
	protected static final String FILE_NAME_PATTERN_DATE_NUMBER = "dateNumber";
	protected static final String FILE_NAME_PATTERN_OPERATION_REPORT_NUMBER = "operationTypeReportNumber";
	
	private static Pattern CONFIGURABLE_REPORT_NAME_PATTERN = Pattern.compile("\\$\\{.*?\\}"); // this is thread safe
	private static String DYNA_ATTR_PAPER_SIZE = "reportPaperSize";
	private static String DYNA_ATTR_EMAIL_KEY = "emailKey";
	private static String DYNA_ATTR_LANGUAGE = "language";
	
	private ReportAutoEmail emailConfiguration=null;
	private boolean outputValidationMessages=false;
	private boolean overwriteFWRGlobalMessage=false;
	private boolean isParentChildFwr=false;
	
	private int gmtOffset = 0;
	private boolean replaceWhiteSpaceInFileName = false;
	
	private class PaperSizeLookupHandler implements LookupHandler {
		private Map<String, LookupItem> lookup = new LinkedHashMap<>();
		
		PaperSizeLookupHandler(Map<String, String> sizes){
			for(String key: sizes.keySet()){
				this.lookup.put(key, new LookupItem(key, sizes.get(key)));
			}
		}
		
		public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
			return this.lookup;
		}
	}
	
	public class EmailListLookupHandler implements LookupHandler {
		
		public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
			LinkedHashMap<String, LookupItem> result = new LinkedHashMap<>();
			String sql = "from EmailList where (isDeleted = false or isDeleted is null)";
			List<EmailList> emailList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
			if (emailList.size() > 0) {
				if (emailList != null) {
					for(EmailList eList: emailList) {
						LookupItem lookupItem = new LookupItem(eList.getEmailListKey(), eList.getEmailListKey());
						result.put(eList.getEmailListKey(), lookupItem);
					}
				}
			}
			return result;
		}

	}
	
	private String report_type = null;
	private String daily_type = null;
	private ReportDataGenerator dataGenerator = null;
	private boolean is_daily_required = true;
	private boolean is_operation_required = true;
	private int simpleAjaxRefreshInterval = -1;
	private String transformationType = TRANSFORMATION_TYPE_FOP;
	private String outputFileExtension = "pdf";
	private String uom_template_system_setting_key = null;
	private String datum_label_system_setting_key = null;
	private boolean selectXslAccordingToCurrentWellType = true;
	private Log logger = LogFactory.getLog(this.getClass());
	private String lastSelectedPaperSize = "";
	private Map<String,String> supportedPaperSizes = null;
	private boolean birt_report = false;
	protected boolean jasper_report = false;
	private String jasper_format = null;
	private boolean birt_report_include_other_document = false;
	private String birt_report_output_type = null;
	private boolean useReportLibrary = false;
	private boolean differentiateOutputFileByUomTemplate = false;
	private boolean differentiateOutputFileByDatumLabel = false;
	private String uomTemplateName = null;

	protected String defaultPaperSize = "A4";
	protected String defaultLanguage = "en";
	private Boolean singleReportPerDay = false;
	private boolean useOperationCompanyForReportSelection = false;
	private boolean useRigUsedForReportSelection=false; 
	
	public boolean isUseOperationCompanyForReportSelection() {
		return useOperationCompanyForReportSelection;
	}
	public void setUseOperationCompanyForReportSelection(
			boolean useOperationCompanyForReportSelection) {
		this.useOperationCompanyForReportSelection = useOperationCompanyForReportSelection;
	}
	public boolean isUseRigUsedForReportSelection() {
		return useRigUsedForReportSelection;
	}
	public void setUseRigUsedForReportSelection(
			boolean useRigUsedForReportSelection) {
		this.useRigUsedForReportSelection = useRigUsedForReportSelection;
	}
	public void setDifferentiateOutputFileByUomTemplate(boolean value){
		this.differentiateOutputFileByUomTemplate = value;
	}
	public boolean getDifferentiateOutputFileByUomTemplate(){
		return differentiateOutputFileByUomTemplate;
	}
	
	public void setDifferentiateOutputFileByDatumLabel(boolean value){
		this.differentiateOutputFileByDatumLabel = value;
	}
	public boolean getDifferentiateOutputFileByDatumLabel(){
		return differentiateOutputFileByDatumLabel;
	}
	
	public void setSimpleAjaxRefreshInterval(int value){
		this.simpleAjaxRefreshInterval = value;
	}
	
	public void setSelectXslAccordingToCurrentWellType(boolean value){
		this.selectXslAccordingToCurrentWellType = value;
	}
	
	protected int getSimpleAjaxRefreshInterval(){
		return this.simpleAjaxRefreshInterval;
	}
	
	public void setDailyRequired(boolean value){
		this.is_daily_required = value;
	}
	
	public void setOperationRequired(boolean value){
		this.is_operation_required = value;
	}
	
	public boolean getOperationRequired(){
		return this.is_operation_required;
	}
	
	public boolean getDailyRequired(){
		return this.is_daily_required;
	}
	
	public void setReportType(String type){
		this.report_type = type;
	}
	
	public void setDailyType(String value){
		this.daily_type = value;
	}
	
	public void setDataGenerator(ReportDataGenerator generator){
		this.dataGenerator = generator;
	}
	public ReportDataGenerator getDataGenerator(){
		return this.dataGenerator;
	}
	public void setTransformationType(String value){
		this.transformationType = value;
	}
	
	public void setSupportedPaperSizes(Map<String,String> sizes){
		this.supportedPaperSizes = sizes;
	}
	
	public void setDefaultPaperSize(String value){
		this.defaultPaperSize = value;
	}
	
	public void setSingleReportPerDay(Boolean value){
		this.singleReportPerDay = value;
	}
	
	public void setUomTemplateSystemSettingKey(String value){
		this.uom_template_system_setting_key = value;
	}
	
	public void setDatumLabelSystemSettingKey(String value){
		this.datum_label_system_setting_key = value;
	}
	
	public void setOutputFileExtension(String value){
		this.outputFileExtension = value;
	}
	
	public void setUomTemplateName(String uomTemplateName){
		this.uomTemplateName = uomTemplateName;
	}
	
	public String getOutputFileExtension() throws Exception {
		if(this.birt_report){
			if(BIRT_REPORT_OUTPUT_TYPE_PDF.equalsIgnoreCase(this.birt_report_output_type)){
				return "pdf";
			}else if(BIRT_REPORT_OUTPUT_TYPE_WORD.equalsIgnoreCase(this.birt_report_output_type)){
				return "doc";
			}else if (BIRT_REPORT_OUTPUT_TYPE_XLS.equalsIgnoreCase(this.birt_report_output_type)){
				return "xls";
			}else if (BIRT_REPORT_OUTPUT_TYPE_HTML.equalsIgnoreCase(this.birt_report_output_type)){
				return "html";
			}else{
				throw new Exception("Unrecognised birt report output type: " + this.birt_report_output_type);
			}
		}if(this.jasper_report){
			if(JASPER_REPORT_OUTPUT_TYPE_PDF.equalsIgnoreCase(this.jasper_format)){
				return "pdf";
			}else if (JASPER_REPORT_OUTPUT_TYPE_XLS.equalsIgnoreCase(this.jasper_format)){
				return "xls";
			}else if (JASPER_REPORT_OUTPUT_TYPE_WORD.equalsIgnoreCase(this.jasper_format)){
				return "doc";
			}else{
				throw new Exception("Unrecognised jasper report output type: " + this.jasper_format);
			}
		}else{
			return this.outputFileExtension;
		}
	}
	
	protected String getReportType(){
		return this.report_type;
	}
	
	protected String getReportType(UserSelectionSnapshot userSelection) throws Exception {
		return this.report_type;
	}

	protected String getDailyType(){
		return this.daily_type;
	}
	
	protected void beforeReportJobStart(UserSelectionSnapshot userSelection) throws Exception {
		if(this.uom_template_system_setting_key != null){
			String report_uom_template = com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference.getValue(userSelection.getGroupUid(), this.uom_template_system_setting_key);
			if(StringUtils.isNotBlank(report_uom_template)) userSelection.setUomTemplateUid(report_uom_template);
		}else if(this.datum_label_system_setting_key != null){			
			OperationUomContext operationUomContext = new OperationUomContext(true, true);
			String report_datum_label = null2EmptyString(this.getDatumDisplayName(operationUomContext, userSelection, userSelection.getUomDatumUid()));
			if(StringUtils.isNotBlank(report_datum_label)) userSelection.setUomTemplateUid(report_datum_label);
		}
		if(this.report_type.equalsIgnoreCase("MPM") || this.report_type.equalsIgnoreCase("MPMR") || this.report_type.equalsIgnoreCase("EWELL") || this.report_type.equalsIgnoreCase("DDSR") || this.report_type.equalsIgnoreCase("DDSRNC")){
			if(uomTemplateName!=null){
				
				String strSql = "FROM UomTemplate WHERE (isDeleted = false or isDeleted is null) and name=:name and groupUid=:groupUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] { "name", "groupUid" }, new Object[] { uomTemplateName, userSelection.getGroupUid() });
				
				if (lstResult.size() > 0){
					UomTemplate defaultTemplate = (UomTemplate) lstResult.get(0);
					if (defaultTemplate!=null) userSelection.setUomTemplateUid(defaultTemplate.getUomTemplateUid());
				}
			}
		}
	}

	protected List<ReportFiles> loadSourceReportFilesFromDB(UserSelectionSnapshot userSelection, String reportType) throws Exception {
		List<Object> param_values = new ArrayList<>();
		List<String> param_names = new ArrayList<>();
		String query = null;
		
		if(this.is_operation_required){
			param_names.add("operationUid"); param_values.add(userSelection.getOperationUid());
			param_names.add("reportType"); param_values.add(reportType);
			query = "from ReportFiles where reportOperationUid = :operationUid and reportType = :reportType and (isDeleted = false or isDeleted is null) order by dateGenerated";
		}else{
			param_names.add("reportType"); param_values.add(reportType);
			query = "from ReportFiles where reportType = :reportType and (isDeleted = false or isDeleted is null) order by dateGenerated";
		}

		return ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, param_names, param_values);
	}
	
	protected String getXslFileInLocalPath(UserContext userContext, ReportJobParams reportJobParams) throws Exception{
		if(StringUtils.isBlank(this.transformationType)){
			return null;
		} else if(this.jasper_report && StringUtils.isNotBlank(this.getJasperReportUnitUri(userContext, reportJobParams))) {
			return this.getJasperReportUnitUri(userContext, reportJobParams);
		} else {
			int well_op_type = ReportUtils.WELL_OPERATION_TYPE_DEFAULT;
			if(this.selectXslAccordingToCurrentWellType){
				Well well = ApplicationUtils.getConfiguredInstance().getCachedWellByOperation(userContext.getUserSelection().getOperationUid());
				if(well != null){
					if(com.idsdatanet.d2.drillnet.constant.Well.ONSHORE.equals(well.getOnOffShore())){
						well_op_type = ReportUtils.WELL_OPERATION_TYPE_ONSHORE;
					}else if(com.idsdatanet.d2.drillnet.constant.Well.OFFSHORE.equals(well.getOnOffShore())){
						well_op_type = ReportUtils.WELL_OPERATION_TYPE_OFFSHORE;
					}
				}
			}
			
			String group_id = userContext.getUserSelection().getGroupUid();
			
			String op_co = "";
			if (useOperationCompanyForReportSelection)
			{
				Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
				if (operation != null){
					group_id = operation.getGroupUid();
					op_co = operation.getOpCo();
					reportJobParams.setLookupCompanyUid(op_co);
				}
			}
			String rigUsed="";
			if(useRigUsedForReportSelection) {
				Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
				if (operation != null){
					group_id = operation.getGroupUid();
					rigUsed = operation.getRigInformationUid();
					reportJobParams.setRigInformationUid(rigUsed);
				}
				
			}
			String report_type = this.getReportType(userContext.getUserSelection());
			
			String xsl = ReportUtils.getConfiguredInstance().getXsl(group_id, report_type, well_op_type, reportJobParams.getPaperSize(), reportJobParams.getLanguage(), op_co,rigUsed);
			
			if(StringUtils.isBlank(xsl)) throw new Exception("Xsl file not found! [report type: " + null2EmptyString(report_type) + ", group id: " + null2EmptyString(userContext.getUserSelection().getGroupUid()) + ", paper size: " + null2EmptyString(reportJobParams.getPaperSize()) + ", language: " + null2EmptyString(reportJobParams.getLanguage()) + "]");
			return xsl;
		}
	}
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		return this.getOutputFileInLocalPath(userContext, paperSize, "en");
	}
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize, String language) throws Exception{
		String operation_uid = userContext.getUserSelection().getOperationUid();
		if(this.is_operation_required && StringUtils.isBlank(operation_uid)){
			throw new Exception("Operation must be selected");
		}
		
		String daily_uid = userContext.getUserSelection().getDailyUid();
		if(this.is_daily_required && StringUtils.isBlank(daily_uid)){
			throw new Exception("Day must be selected");
		}

		if(this.singleReportPerDay == true)
		{
			return this.getReportType(userContext.getUserSelection()) +	"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize, language)); 
		}
			
		return this.getReportType(userContext.getUserSelection()) + 
			(StringUtils.isBlank(operation_uid) ? "" : "/" + operation_uid) + 
			((StringUtils.isBlank(daily_uid) || !this.is_daily_required) ? "" : "/" + daily_uid) + 
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize, language));
	}

	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception{
		return this.getOutputFileName(userContext, paperSize, "en");
	}
	
	protected String getOutputFileName(UserContext userContext, String paperSize, String language) throws Exception{
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		if(this.is_operation_required && operation == null) throw new Exception("Operation must be selected");
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if(this.is_daily_required && daily == null) throw new Exception("Day must be selected");
		
		if(operation == null && this.is_operation_required) throw new Exception("Unable to generate output file name");
		if(this.singleReportPerDay == true)
		{
			Date currentdate = new Date();
			return ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(currentdate) + "." + this.getOutputFileExtension();
		}
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		UserSelectionSnapshot userSelection = userContext.getUserSelection();		
		return (operation == null || !this.is_operation_required ? "" : operation.getOperationName()) + " " + (daily == null || !this.is_daily_required ? "" : daily.getDayNumber()) + (this.differentiateOutputFileByUomTemplate ? " " + getUomTemplateNameForOutputFile(userContext.getUserSelection()) : "") + (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + (language=="en" ? "" : "(" + language + ")") + "." +(this.differentiateOutputFileByDatumLabel ? " " + this.getDatumDisplayName(operationUomContext, userSelection, userSelection.getUomDatumUid()) : "")+"."+this.getOutputFileExtension();
	}
	
	protected String getUomTemplateNameForOutputFile(UserSelectionSnapshot userSelection){
		UomTemplate t = UOMManager.getUomTemplate(userSelection.getUomTemplateUid());
		if(t == null) return "";
		if(StringUtils.isNotBlank(t.getShortCode())) return t.getShortCode();
		return t.getName();
	}

	protected String getUomTemplateNameForDisplay(UserSelectionSnapshot userSelection){
		UomTemplate t = UOMManager.getUomTemplate(userSelection.getUomTemplateUid());
		if(t == null) return "";
		if(StringUtils.isNotBlank(t.getShortCode())) return t.getShortCode();
		return t.getName();
	}
	
	private static double null2Zero(Double value){
		if(value == null) return 0;
		return value;
	}

	protected ReportDataGenerator getReportDataGenerator() throws Exception{
		return this.dataGenerator;
	}

	protected String null2EmptyString(String value){
		if(value == null) return "";
		return value;
	}

	protected String getReportDisplayName(ReportFiles reportFile, Operation operation, UserSelectionSnapshot userSelection) throws Exception {
		if(reportFile.getDisplayName() != null) return reportFile.getDisplayName();
		
		// this is to maintain backward compatibility with files generated previously
		return (this.getReportType(userSelection) == null ? "" : this.getReportType(userSelection) + " ") + 
				(operation == null ? "[OperationUid:" + null2EmptyString(reportFile.getReportOperationUid()) + "]" : CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), operation.getOperationUid())) +
				(StringUtils.isNotBlank(this.getDailyType()) ? " Day " + null2EmptyString(reportFile.getReportDayNumber()) : "");
	}
	
	protected String getReportDisplayEmailSubject(ReportFiles reportFile, ReportTypes reportType, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) throws Exception {
		String file_name_pattern = null;
		if(reportType != null){
			file_name_pattern = reportType.getEmailSubject();
		}
		
		if(StringUtils.isBlank(file_name_pattern)){
			return null;
		}
		
		return this.formatDisplayName(file_name_pattern, reportFile, operation, daily, reportDaily, userSelection, reportJobParams);
	}
	
	protected String getReportDisplayNameOnCreation(ReportFiles reportFile, ReportTypes reportType, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) throws Exception {
		String file_name_pattern = null;
		if(reportType != null){
			file_name_pattern = reportType.getReportFilenamePattern();
		}
		
		if(StringUtils.isBlank(file_name_pattern)){
			return null;
		}
		
		return this.formatDisplayName(file_name_pattern, reportFile, operation, daily, reportDaily, userSelection, reportJobParams);
	}
	
	protected String parseDisplayNameParam(String key, ReportFiles reportFiles, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) {
		try{
			if(key.startsWith(FILE_NAME_PATTERN_CUSTOM_PROPERTY_PREFIX)){
				String custom_property = key.substring(FILE_NAME_PATTERN_CUSTOM_PROPERTY_PREFIX.length());
				Object result = userSelection.getCustomProperty(custom_property);
				if(result == null){
					return "";
				}else{
					return result.toString();
				}
			}else if(FILE_NAME_PATTERN_DATE.equalsIgnoreCase(key) || key.startsWith(FILE_NAME_PATTERN_DATE+FILE_NAME_PATTERN_DATE_SEPARATOR)){
				if(daily != null){
					if (FILE_NAME_PATTERN_DATE.equalsIgnoreCase(key))
						return null2EmptyString(ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(daily.getDayDate()));
					else if (key.startsWith(FILE_NAME_PATTERN_DATE+FILE_NAME_PATTERN_DATE_SEPARATOR))
					{
						String strFormat = key.substring(FILE_NAME_PATTERN_DATE.length()+FILE_NAME_PATTERN_DATE_SEPARATOR.length());
						DateFormat dateFormat = new SimpleDateFormat(strFormat);
						return null2EmptyString(dateFormat.format(daily.getDayDate()));
					}else
						return "";
				}else{
					return "";
				}
			}else if(FILE_NAME_PATTERN_CURRENT_DATE.equalsIgnoreCase(key) || key.startsWith(FILE_NAME_PATTERN_CURRENT_DATE+FILE_NAME_PATTERN_DATE_SEPARATOR)){
				Date currentDate = new Date();
				if (FILE_NAME_PATTERN_CURRENT_DATE.equalsIgnoreCase(key))
				{
					return null2EmptyString(ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(currentDate));
				}else if (key.startsWith(FILE_NAME_PATTERN_CURRENT_DATE+FILE_NAME_PATTERN_DATE_SEPARATOR))
				{
					String strFormat = key.substring(FILE_NAME_PATTERN_CURRENT_DATE.length()+FILE_NAME_PATTERN_DATE_SEPARATOR.length());
					DateFormat dateFormat = new SimpleDateFormat(strFormat);
					return null2EmptyString(dateFormat.format(currentDate));
				}else{
					return "";
				}
			}else if(FILE_NAME_PATTERN_WELL.equalsIgnoreCase(key)){
				Object rs = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class,userSelection.getWellUid());
				if(rs != null){
					Well well = (Well) rs;
					if (well.getIsDeleted()== null ||(well.getIsDeleted()!=null && !well.getIsDeleted()))
						return null2EmptyString(well.getWellName());
				}
				return "";
			}else if(FILE_NAME_PATTERN_WELLBORE.equalsIgnoreCase(key)){
				List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where wellboreUid = :id and (isDeleted = false or isDeleted is null)", "id", userSelection.getWellboreUid());
				if(rs.size() > 0){
					Wellbore wellbore = (Wellbore) rs.get(0);
					return null2EmptyString(wellbore.getWellboreName());
				}else{
					return "";
				}
			} else if(FILE_NAME_PATTERN_WELL_WELLBORE_COMBINED.equalsIgnoreCase(key)) {
				return this.getWellAndWellboreCombinedReportName(userSelection);
			}
			else if(FILE_NAME_PATTERN_OPERATION.equalsIgnoreCase(key)){
				if(operation != null){
					if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "UseOperationNameOnlyForReportNaming"))){
						return null2EmptyString(operation.getOperationName());
					}
					else
					{
						return null2EmptyString(CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), operation.getOperationUid()));
					}
				}else{
					return "";
				}
			}else if(FILE_NAME_PATTERN_DAY_NUMBER.equalsIgnoreCase(key)){
				if(reportDaily != null){
					return formatDayNumber(null2EmptyString(reportDaily.getReportNumber()));
				}else{
					return "";
				}
			}else if(FILE_NAME_PATTERN_RIG.equalsIgnoreCase(key)){
				List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from RigInformation where rigInformationUid = :id and (isDeleted = false or isDeleted is null)", "id", userSelection.getRigInformationUid());
				if(rs.size() > 0){
					RigInformation rigInformation = (RigInformation) rs.get(0);
					return null2EmptyString(rigInformation.getRigName());
				}else{
					return "";
				}
			}else if(FILE_NAME_PATTERN_QC_FLAG.equalsIgnoreCase(key)){
				if(daily != null){
					return null2EmptyString(daily.getQcFlag());
				}else{
					return "";
				}
			}else if(FILE_NAME_PATTERN_REPORT_TYPE.equalsIgnoreCase(key)){
				if(reportFiles!=null)
					return null2EmptyString(reportFiles.getReportType());
				else
					return null2EmptyString(this.report_type);
			}else if(FILE_NAME_PATTERN_PAPER_SIZE.equalsIgnoreCase(key)){
				if(reportJobParams != null){
					return null2EmptyString(reportJobParams.getPaperSize());
				}else{
					return "";
				}
			}else if(FILE_NAME_PATTERN_OPERATION_PREFIX.equalsIgnoreCase(key)){
				if(operation != null){
					return null2EmptyString(operation.getNamePrefix());
				}else{
					return "";
				}
			}else if(FILE_NAME_PATTERN_UOM_TEMPLATE_NAME.equalsIgnoreCase(key)){
				return null2EmptyString(this.getUomTemplateNameForDisplay(userSelection));
			}else if(FILE_NAME_PATTERN_OPERATION_TYPE.equalsIgnoreCase(key)){
				if(operation != null){
					return null2EmptyString(operation.getOperationCode());
				}else{
					return "";
				}
			//20101008-0823-mlley - add company to report file name
			}else if (FILE_NAME_PATTERN_COMPANY.equalsIgnoreCase(key)) {
				String operatingCompany = null;
				if (operation !=null) operatingCompany = operation.getOpCo();
				
				if(StringUtils.isNotBlank(operatingCompany)){				
					if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "enabledOpCompanyAsLookup"))){
						List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupCompany where lookupCompanyUid = :id and (isDeleted = false or isDeleted is null)", "id", operatingCompany);
						if(rs.size() > 0){	
							LookupCompany lookupCompany = (LookupCompany) rs.get(0);
							return null2EmptyString(lookupCompany.getCompanyName());
						}
					}					
				}				
				return null2EmptyString(operatingCompany);
			
			//20110622-1406-kwildridge - add midnightDepth to report file name
			}else if(FILE_NAME_PATTERN_MIDNIGHT_DEPTH.equalsIgnoreCase(key)){
				OperationUomContext operationUomContext = new OperationUomContext(true, true);
				if (operation != null) {
					operationUomContext.setOperationUid(operation.getOperationUid());
				}
				
				CustomFieldUom converter = new CustomFieldUom(userSelection.getLocale(), ReportDaily.class, "depthMdMsl", operationUomContext);
				NumberFormat formatter = new DecimalFormat("00000");
				if (reportDaily!=null && reportDaily.getDepthMdMsl()!=null) {
					return formatter.format(reportDaily.getDepthMdMsl()) + converter.getUomSymbol();
				} else {
					 return "";
				}
			}else if(FILE_NAME_PATTERN_OPERATION_NAME_WITH_UNDERSCORE.equalsIgnoreCase(key)){
				if(operation != null){
					if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "UseOperationNameOnlyForReportNaming"))){
						return null2EmptyString(operation.getOperationName()).replaceAll(" ", "_");
					}
					else
					{
						return null2EmptyString(CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), operation.getOperationUid())).replaceAll(" ", "_");
					}
				}else{
					return "";
				}
				
			}else if(FILE_NAME_PATTERN_DATUM_LABEL.equalsIgnoreCase(key)){
				OperationUomContext operationUomContext = new OperationUomContext(true, true);
				return null2EmptyString(this.getDatumDisplayName(operationUomContext, userSelection, userSelection.getUomDatumUid()));
			}else if(FILE_NAME_PATTERN_CAMPAIGN.equalsIgnoreCase(key))	{
				String campaignName = null;
				if(operation != null) campaignName = operation.getOperationCampaignUid();
				
				if(StringUtils.isNotBlank(campaignName)){
						List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Campaign where campaignUid = :id and (isDeleted = false or isDeleted is null)", "id", campaignName);
						if(rs.size() > 0){	
							Campaign campaign = (Campaign) rs.get(0);
							return null2EmptyString(campaign.getCampaignName());
						}
				}
				return null2EmptyString(campaignName);
			}else if(FILE_NAME_PATTERN_GENERATE_YYYY.equalsIgnoreCase(key)){
				if(getCurrentYearStamp()!=null){
					return null2EmptyString(getCurrentYearStamp());
				}		
				return "";
				
			}else if(FILE_NAME_PATTERN_GENERATE_HMS.equalsIgnoreCase(key)){
                if (getCurrentTimeStamp()!=null){
                    return null2EmptyString(getCurrentTimeStamp());
                }               
                return "";
            }else if(key.startsWith(FILE_NAME_PATTERN_GENERATE_HMS)){
                String[] token = key.split(":");
                String offset = token[1].replace("+", "");
                if(StringUtils.isNotBlank(offset)) {
                    this.gmtOffset = Integer.parseInt(offset);
                }
                if (getCurrentTimeStamp()!=null){
                    return null2EmptyString(getCurrentTimeStamp());
                }               
                return "";
            }else if(FILE_NAME_PATTERN_DATE_NUMBER.equalsIgnoreCase(key)){
				if (daily!=null){
					return null2EmptyString(getReportDateStamp(daily.getDayDate()));
				}
				return "";
			}else if(FILE_NAME_PATTERN_OPERATION_REPORT_NUMBER.equalsIgnoreCase(key)){
				if(reportDaily != null){
					return null2EmptyString(reportDaily.getOperationTypeReportNumber());
				}else{
					return "";
				}	
			}else{
				return null;
			}
		}catch(Exception e){
			this.logger.error(e.getMessage(), e);
			return null;
		}
	}
	
	private String getDatumDisplayName(OperationUomContext operationUomContext, UserSelectionSnapshot userSelection, String opsDatumUid) throws Exception {
		String displayName = "";
		String separator = "";
		AbstractDatumFormatter datum_formatter = AbstractDatumFormatter.getConfiguredInstance();
		if(datum_formatter != null){
			String[] separators =  datum_formatter.getDatumLabelSeparators(userSelection.getGroupUid());
			if (separators.length>0) separator = separators[0];
		}
		
		String queryString = "FROM OpsDatum WHERE (isDeleted=false or isDeleted is NULL) AND opsDatumUid=:opsDatumUid";
		List<OpsDatum> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "opsDatumUid", opsDatumUid);
		if (list.size()>0) {
			OpsDatum opsDatum = list.get(0);
			
			CustomFieldUom formatter = new CustomFieldUom(userSelection.getLocale(), OpsDatum.class, "reportingDatumOffset");
			Double getDatum =null2Zero(opsDatum.getReportingDatumOffset());		
			if (opsDatum.getDatumReferencePoint()==null) opsDatum.setDatumReferencePoint("MSL");
			displayName = formatter.getFormattedValue(getDatum) +  separator + opsDatum.getDatumCode() + separator + opsDatum.getDatumReferencePoint();
			formatter.dispose();
		}
		
		return displayName;
	}
	
	private String formatDayNumber(String reportNumber){
		if (reportNumber.length()<=1)
			return "0" + reportNumber;
		else	
			return reportNumber;
	}
		
	protected String formatDisplayName(String pattern, ReportFiles reportFile, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) throws Exception {
		Matcher m = CONFIGURABLE_REPORT_NAME_PATTERN.matcher(pattern);
		StringBuffer sb = new StringBuffer();
		 
		while (m.find()) {
			String key = m.group().substring(2, m.group().length() - 1);
			String value = parseDisplayNameParam(key, reportFile, operation, daily, reportDaily, userSelection, reportJobParams);
			if(value != null) m.appendReplacement(sb, value.replace("\\", "\\\\").replace("$", "\\$"));
		}
		m.appendTail(sb);
		
		if (replaceWhiteSpaceInFileName && StringUtils.isNotEmpty(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_REPLACE_FILE_NAME_WHITE_SPACE_WITH_SYMBOL)) && StringUtils.isNotBlank((GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_REPLACE_FILE_NAME_WHITE_SPACE_WITH_SYMBOL))))
		{
			if ("blank".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_REPLACE_FILE_NAME_WHITE_SPACE_WITH_SYMBOL))){
				sb = new StringBuffer().append(sb.toString().replaceAll(" ", ""));
			}else{
				sb = new StringBuffer().append(sb.toString().replaceAll(" ", GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_REPLACE_FILE_NAME_WHITE_SPACE_WITH_SYMBOL)));
			}			
		}
		
		if(reportDaily != null){
			return formatLowerUpperDisplayName(sb.toString(),userSelection);
		}else{
			if (reportJobParams.isJoinReport())
				return formatLowerUpperDisplayName(sb.toString().replace("Day ", "").concat(" Combined"),userSelection);
			else
				return formatLowerUpperDisplayName(sb.toString(),userSelection);
		}
	}
		
	private String formatLowerUpperDisplayName (String displayName, UserSelectionSnapshot userSelection) throws Exception {
		 if("lowercase".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_REPORT_FILE_NAME_CASE_CONTROL))){
			 return displayName.toLowerCase();
		 }
		 else  if("uppercase".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_REPORT_FILE_NAME_CASE_CONTROL))){
			 return displayName.toUpperCase();
		 }
		 return displayName;
	}
	
	protected void beforeReportJobSubmission(JobContext job) throws Exception {
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(job.getUserContext().getUserSelection().getDailyUid());
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(job.getUserContext().getUserSelection().getOperationUid());
		
		job.setJobDescription("Generate " + this.getReportType(job.getUserContext().getUserSelection()) + " Report for " + (operation == null ? "" : operation.getOperationName()) + (daily == null ? "" : " Day #" + daily.getDayNumber()));
	}
	
	public void setUseBirtReportEngine(boolean value){
		this.birt_report = value;
	}
	
	public void setJasperFormat(String value){
		this.jasper_format = value;
	}
	
	public void setUseJasperReportEngine(boolean value){
		this.jasper_report = value;
	}
	
	public void setBirtReportOutputType(String value){
		this.birt_report_output_type = value;
	}
	
	public void setIncludeOtherDocument(boolean value){
		this.birt_report_include_other_document = value;
	}
	
	protected D2Job createReportJob(String xsl, String output, ReportDataGenerator[] generators, String reportType, UserContext userContext, Map runtimeContext,ReportJobParams reportJobData) throws Exception{
		
		if(this.birt_report){
			if (reportJobData.isMultiWellJob()){
				return new MultiWellXslJob(xsl, output, new ByteArrayOutputStream(),generators, reportType, this.birt_report_output_type, userContext,runtimeContext,reportJobData.getMultiWellList(), this.useReportLibrary);
			} else {
				return BirtReportJob.createBirtReportJob(xsl, output, generators, reportType, this.birt_report_output_type, userContext,runtimeContext, this.useReportLibrary, this.birt_report_include_other_document);
			}
		}
		else if (this.jasper_report){
				return JasperJob.createJasperReportJob(xsl, output, generators, reportType, this.jasper_format, userContext); 
		}else{
			if(TRANSFORMATION_TYPE_FOP.equals(this.transformationType)){
				return new FOPJob(xsl, output, generators, reportType);
			} else {
				return new XslJob(xsl, output, generators, reportType);
			}
		}
		
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		return true;
	}

	protected ReportJobParams getParametersForReportSubmission(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		ReportJobParams params = super.getParametersForReportSubmission(session, request, commandBean);
		
		Object paper = commandBean.getRoot().getDynaAttr().get(DYNA_ATTR_PAPER_SIZE);
		String paper_size = null;
		String language = commandBean.getRoot().getDynaAttr().get(DYNA_ATTR_LANGUAGE) == null ? null :commandBean.getRoot().getDynaAttr().get(DYNA_ATTR_LANGUAGE).toString();
		if(paper != null) paper_size = paper.toString();
		if(this.defaultPaperSize != null && StringUtils.isBlank(paper_size)) paper_size = this.defaultPaperSize;
		if(this.defaultLanguage != null && StringUtils.isBlank(language)) language = session.getUserLanguage();

		if(StringUtils.isNotBlank(paper_size)){
			if(params == null) params = new ReportJobParams();
			params.setPaperSize(paper_size);
		}
		
		if(StringUtils.isNotBlank(language)){
			if(params == null) params = new ReportJobParams();
			params.setLanguage(language);
		}
		
		if(this.isParentChildFwr){
			if(params == null) params = new ReportJobParams();
			params.setMultiWellJob(true);
		}
		
		return params;
	}

	public synchronized void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		super.onSubmitForServerSideProcess(commandBean, request, targetCommandBeanTreeNode);
		Object paper = commandBean.getRoot().getDynaAttr().get(DYNA_ATTR_PAPER_SIZE);
		this.lastSelectedPaperSize = (paper == null ? "" : paper.toString());
	}
	
	public List<Object> preFilterLoadedRawChildRecords(List<Object> data, CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		return null;
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		super.afterDataLoaded(commandBean, root, userSelection, request);
		root.getDynaAttr().put(DYNA_ATTR_PAPER_SIZE, this.lastSelectedPaperSize);
		root.getDynaAttr().put("showAllDays", this.isShowAllDays());
		root.getDynaAttr().put("overwriteFWRGlobalMessage", this.overwriteFWRGlobalMessage);
		root.getDynaAttr().put("showChildWellList", this.isParentChildFwr);
		if (StringUtils.isNotBlank(this.systemMsg)){
			commandBean.getSystemMessage().addInfo(this.systemMsg);
			this.systemMsg = null;
		}
		if (this.outputValidationMessages)
		{
			commandBean.getSystemMessage().clear();
			ReportValidation rv=this.generateValidation(UserContext.getUserContext(userSelection));
			for(String error:rv.getErrors())
			{
				commandBean.getSystemMessage().addWarning(error);
			}
		}
		if (this.overwriteFWRGlobalMessage)
		{
			String operationUid = userSelection.getOperationUid();
			String reportType = "FWR";
			if(StringUtils.isNotBlank(operationUid)){
				if (this.isParentChildFwr)reportType = "PCFWR";
				String strSql = "FROM ReportFiles WHERE reportOperationUid = :thisOperationUid AND reportType =:reportType AND (isDeleted = false OR isDeleted is NULL)";
				String[] params = new String[]{"thisOperationUid", "reportType"};
				Object[] values = new Object[]{operationUid, reportType};
				List<ReportFiles> fwrList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, params, values);
				File output_file = null;
				if(fwrList != null && fwrList.size() > 0){
					ReportFiles fwr = fwrList.get(0);
					String reportFilesUid = null;
					
					try{
						output_file = new File(ReportUtils.getFullOutputFilePath(fwr.getReportFile()));
					}catch(Exception e){
						this.logger.error(e.getMessage());
						e.printStackTrace();
					}
					if (output_file.exists() && output_file != null) {
						reportFilesUid = fwr.getReportFilesUid();
						root.getDynaAttr().put("FWRReportFilesUid", reportFilesUid);
					}
					
				}
				
			}
		}
		String emailKey = null;
		if (this.emailConfiguration!=null){
			if (StringUtils.isNotBlank(this.emailConfiguration.getEmailListKey())){
				emailKey = this.emailConfiguration.getEmailListKey();
			}
		}
		String emaillist = null;
		List results = null;
		if (emailKey != null){
			results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from EmailList where (isDeleted = false or isDeleted is null) and groupUid = :groupUid and emailListKey = :emailListKey", new String[] {"groupUid","emailListKey"}, new Object[] {userSelection.getGroupUid(), emailKey});
		}else{
			emailKey = "DefaultEmailList";
			results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from EmailList where (isDeleted = false or isDeleted is null) and groupUid = :groupUid and emailListKey = :emailListKey", new String[] {"groupUid","emailListKey"}, new Object[] {userSelection.getGroupUid(), "DefaultEmailList"});
		}
		if (results.size() > 0)
		{
			emaillist = ((EmailList) results.get(0)).getEmailAddressList();
		}
		root.getDynaAttr().put("emailAddress", emaillist);
		root.getDynaAttr().put("emailListKey", emailKey);
	}

	public void init(CommandBean commandBean){
		super.init(commandBean);
		if(this.supportedPaperSizes != null){
			if(commandBean instanceof BaseCommandBean){
				Map lookup = new HashMap();
				lookup.put("@reportPaperSize", new PaperSizeLookupHandler(this.supportedPaperSizes));
				((BaseCommandBean) commandBean).setLookup(lookup);
			}
		}
		
		if(commandBean instanceof BaseCommandBean){
			Map lookup = new HashMap();
			lookup.put("@emailKey", new EmailListLookupHandler());
			((BaseCommandBean) commandBean).setLookup(lookup);
		}
	}

	public void afterPropertiesSet() throws Exception {
		if(this.defaultPaperSize != null){
			this.lastSelectedPaperSize = this.defaultPaperSize;
		}
	}
	public void setOutputValidationMessages(
			boolean outputValidationMessages) {
		this.outputValidationMessages = outputValidationMessages;
	}

	public boolean isOutputValidationMessages() {
		return outputValidationMessages;
	}
	
	public void setOverwriteFWRGlobalMessage(boolean overwriteFWRGlobalMessage) {
		this.overwriteFWRGlobalMessage = overwriteFWRGlobalMessage;
	}

	public boolean isOverwriteFWRGlobalMessage() {
		return overwriteFWRGlobalMessage;
	}
	
	public void setIsParentChildFwr(boolean isParentChildFwr) {
		this.isParentChildFwr = isParentChildFwr;
	}

	public boolean isParentChildFwr() {
		return this.isParentChildFwr;
	}
	
	public ReportValidation generateValidation(UserContext userContext) throws Exception{
		ReportValidation rv=new ReportValidation();
		BeanDataGenerator bdg=(BeanDataGenerator) dataGenerator;
		bdg.generateValidation(userContext,this.getReportType(), rv);
		return rv;
	}
	
	void generateDefaultReportOnCurrentSystemDatetime(String loginUser, ReportAutoEmail reportAutoEmail, String reportType) throws Exception {
		
		ReportJobParams params = new ReportJobParams();
		params.setUserSessionReportJob(false);
		params.setReportAutoEmail(reportAutoEmail);
		params.setPaperSize(this.defaultPaperSize);	
		
		if (StringUtils.isNotBlank(reportType))
			this.report_type = reportType;
		
		if(this.singleReportPerDay)
		{
			//NON-OPERATION AND NON-DAILY BASED - NO CHECKING IF DAY IS AVAILABLE
			UserSelectionSnapshot userSelection = UserSelectionSnapshot.getInstanceForCustomLogin(loginUser);
			super.submitReportJob(userSelection, params);
		}
		else
		{
			if(this.is_operation_required)
			{
				//DAILY BASED - DAILY REPORT FOR EACH OPERATION
				List<Daily> list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(new Date());
				List<UserSelectionSnapshot> selections = new ArrayList<>();
				//cross check if daily record match the dailyReportType
				for(Daily daily: list){
					if(this.daily_type!=null){
						String sqlGet = "select reportType from ReportDaily where reportType=:reportType AND dailyUid=:dailyUid";
						String[] paramsFields = {"reportType", "dailyUid"};
						String[] paramsValues = {this.daily_type, daily.getDailyUid()};
						List getlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlGet, paramsFields, paramsValues);
											
						if(getlist.size() > 0)  {
							selections.add(UserSelectionSnapshot.getInstanceForCustomDaily(loginUser,daily.getDailyUid()));
						}
					}else{
						selections.add(UserSelectionSnapshot.getInstanceForCustomDaily(loginUser,daily.getDailyUid()));
					}
				}
				
				if(selections.size() > 0)
				{
					super.submitReportJob(selections, params);
				}
			}
			else
			{
				//OPERATION BASED - DEPENDING ON THE REPORTMODULE
				//IF USING ManagementReportModule THEN GENERATE COMBINED REPORT
				//ELSE GENERATE DISTINCT REPORT FOR EACH OPERATION
				Date reportDate = new Date();
				boolean useLocalizedCutoffTimeLogic = true;
				List<Daily> daily_list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic);
				if (!daily_list.isEmpty()){
					
					UserSelectionSnapshot userSelection = this.createCombinedMultipleDaysUserSelectionFromScheduler(loginUser, true);
					super.submitReportJob(userSelection, params);
				}
			}
		}
	}
	
	/**
	 * method to generate default report based on current system datetime
	 * @param loginUser user name usually idsadmin
	 * @param reportAutoEmail setting containing email address, subject, template
	 * @throws Exception
	 */
	public void generateDefaultReportOnCurrentSystemDatetime(String loginUser, ReportAutoEmail reportAutoEmail) throws Exception {
		
		this.generateDefaultReportOnCurrentSystemDatetime(loginUser, reportAutoEmail,null);
	}
	private UserSelectionSnapshot createCombinedMultipleDaysUserSelectionFromScheduler(String login, boolean excludeTightHoleOperation) throws Exception {
		Date reportDate = new Date();
		boolean useLocalizedCutoffTimeLogic = true;
		List<Daily> list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic, excludeTightHoleOperation);
		return this.createCombinedMultipleDaysUserSelection(login, reportDate, list);
	}
	
	/**
	 * method to send generate daily report based on current system datetime 
	 * @param loginUser user name usually idsadmin
	 * @param reportAutoEmail setting containing email address, subject, template
	 * @param dailies list of daily to be generated
	 * @throws Exception
	 */
	public void generateDailyReportOnCurrentSystemDatetime(String loginUser, ReportAutoEmail reportAutoEmail, List<Daily> dailies) throws Exception
	{
		List<UserSelectionSnapshot> selections = new ArrayList<>();
		for(Daily daily: dailies){
			if(this.daily_type!=null){
				String sqlGet = "select reportType from ReportDaily where (isDeleted is null or isDeleted = false) and reportType=:reportType AND dailyUid=:dailyUid";
				String[] paramsFields = {"reportType", "dailyUid"};
				String[] paramsValues = {this.daily_type, daily.getDailyUid()};
				List getlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlGet, paramsFields, paramsValues);
									
				if(getlist.size() > 0)  {
					selections.add(UserSelectionSnapshot.getInstanceForCustomDaily(loginUser,daily.getDailyUid()));
				}
			}else{
				selections.add(UserSelectionSnapshot.getInstanceForCustomDaily(loginUser,daily.getDailyUid()));
			}
		}
		if (selections.size()>0)
		{
			ReportJobParams params = new ReportJobParams();
			params.setUserSessionReportJob(false);
			params.setReportAutoEmail(reportAutoEmail);
			params.setPaperSize(defaultPaperSize);	
			params.setLanguage(reportAutoEmail.getLanguage());
			
			super.submitReportJob(selections, params);
		}
	}

	public void generateDefaultReportOnCurrentSystemDatetimeByDepotWellOperation(String loginUser,
			ReportAutoEmail reportAutoEmail, String reportType, Daily daily) throws Exception {

		List<UserSelectionSnapshot> selections = new ArrayList<>();
		
		if (StringUtils.isNotBlank(reportType))
			this.report_type = reportType;

		if (this.is_operation_required) {
			// DAILY BASED - DAILY REPORT FOR EACH OPERATION
			
			String sqlGet = "SELECT distinct wellUid, wellboreUid FROM DepotWellOperationMapping WHERE (isDeleted is null or isDeleted = false) and wellUid=:wellUid and wellboreUid=:wellboreUid";
			String[] paramsFields = { "wellUid", "wellboreUid" };
			String[] paramsValues = { daily.getWellUid(), daily.getWellboreUid() };
			List getlist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlGet, paramsFields, paramsValues);
				
			if (getlist.size() > 0) {
				selections.add(UserSelectionSnapshot.getInstanceForCustomDaily(loginUser, daily.getDailyUid()));
			}
			
			if (selections.size()>0)
			{
				ReportJobParams params = new ReportJobParams();
				params.setUserSessionReportJob(false);
				params.setReportAutoEmail(reportAutoEmail);
				params.setPaperSize(defaultPaperSize);	
				
				super.submitReportJob(selections, params);
			}
		}
	}

	private UserSelectionSnapshot createCombinedMultipleDaysUserSelection(String login, Date reportDate, List<Daily> days) throws Exception {
		MultipleUserSelectionSnapshot userSelection = new MultipleUserSelectionSnapshot(UserSelectionSnapshot.getInstanceForCustomLogin(login));
		userSelection.addCopies(this.createMultipleDaysUserSelection(login, days));
		
		//THIS PART IS ONLY FOR THE REPORT USING ManagementReportModule
		for(Daily daily: days){
			reportDate = daily.getDayDate();
		}
		
		userSelection.setCustomProperty("mgt_report_date", reportDate);
		
		return userSelection;
	}
	
	private List<UserSelectionSnapshot> createMultipleDaysUserSelection(String userLogin, List<Daily> list) throws Exception {
		List<UserSelectionSnapshot> result = new ArrayList<>();
		for(Daily daily: list){
			result.add(UserSelectionSnapshot.getInstanceForCustomDaily(userLogin, daily.getDailyUid()));
		}
		return result;
	}

	public void setUseReportLibrary(boolean useReportLibrary) {
		this.useReportLibrary = useReportLibrary;
	}

	public boolean isUseReportLibrary() {
		return useReportLibrary;
	}
	
	public void setEmailConfiguration(ReportAutoEmail emailConfiguration) {
		this.emailConfiguration = emailConfiguration;
	}

	public ReportAutoEmail getEmailConfiguration() {
		return emailConfiguration;
	}
	
	public void setReplaceWhiteSpaceInFileName(boolean replaceWhiteSpaceInFileName) {
		this.replaceWhiteSpaceInFileName = replaceWhiteSpaceInFileName;
	}
	public boolean isReplaceWhiteSpaceInFileName() {
		return replaceWhiteSpaceInFileName;
	}
	
	public String getCurrentTimeStamp() {
	    SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
	    if (this.gmtOffset != 0) {
	        String timeZoneString = "GMT";
	        if (this.gmtOffset > 0) {
	            timeZoneString += "+";
	        } else {
	            timeZoneString += "-";
	        }
	        timeZoneString += Math.abs(this.gmtOffset);
	        sdf.setTimeZone(TimeZone.getTimeZone(timeZoneString));
	    }
	    return sdf.format(new Date());
	}
	
	public String getCurrentYearStamp() {
	    return new SimpleDateFormat("yyyy").format(new Date());
	}
	
	public String getReportDateStamp(Date reportDate) {
	    return new SimpleDateFormat("yyyyMMdd").format(reportDate);
	}
	
	@Override
	protected String getJasperReportUnitUri(UserContext userContext, ReportJobParams reportJobParams) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	private String getWellAndWellboreCombinedReportName(UserSelectionSnapshot userSelection) throws Exception {
		String wellName = "";
		String wellboreName = "";
		Object wellObj = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
		if (wellObj != null) {
			Well well = (Well) wellObj;
			if (well.getIsDeleted() == null || (well.getIsDeleted() != null && !well.getIsDeleted())) {
				wellName = null2EmptyString(well.getWellName());
			}
		}
		
		List<Wellbore> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where wellboreUid = :id and (isDeleted = false or isDeleted is null)", "id", userSelection.getWellboreUid());
		if(rs.size() > 0){
			Wellbore wellbore = rs.get(0);
			wellboreName = null2EmptyString(wellbore.getWellboreName());
		}
		
		if (StringUtils.equals(wellName, wellboreName)) {
			return wellboreName;
		}
		return wellName + " " + wellboreName;
	}
}
