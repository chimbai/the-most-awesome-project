package com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.dailyReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.drillnet.report.DDRReportModule;
import com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.DailyReportModuleResolver;

public class DailyReportServiceNoAct extends AbstractReportService{
	
	private List<String> includeOperationTypePrefixList = null;
	private List<String> includeSpecificOperationType = null;
	private List<String> excludeSpecificOperationType = null;
		
	public List<String> getExcludeSpecificOperationType() {
		return excludeSpecificOperationType;
	}

	public void setExcludeSpecificOperationType(
			List<String> excludeSpecificOperationType) {
		this.excludeSpecificOperationType = excludeSpecificOperationType;
	}

	private static final String DEFAULT_OPERATION_TYPE_PREFIX = "DRLLG";
	
	synchronized public void run() throws Exception
	{
		List<Daily> dailies = CommonUtil.getConfiguredInstance().getDaysForReportAsAtNoAct(new Date());
		
		Map<DDRReportModule, List<Daily>> mapToProcess = new LinkedHashMap<DDRReportModule,List<Daily>>();
		
		for (Daily daily : dailies)
		{
			String operationCode = DailyReportModuleResolver.getOperationTypeByDailyUid(daily.getDailyUid());
			if (isOperationCodeIncluded(operationCode))
			{
				DDRReportModule module = DailyReportModuleResolver.getConfiguredInstance().getReportModuleByOperationType(operationCode);
				if (mapToProcess.get(module)==null)
					mapToProcess.put(module, new ArrayList<Daily>());
				List<Daily> list = mapToProcess.get(module);
				list.add(daily);
				mapToProcess.put(module, list);
			}
		}
		
		for (Map.Entry<DDRReportModule, List<Daily>> entry : mapToProcess.entrySet())
		{
			DDRReportModule reportModule = entry.getKey();

			reportModule.generateDailyReportOnCurrentSystemDatetime(this.getGenerateReportUserName(), this.getReportAutoEmail()	, entry.getValue());
		}
	}
	
	public Boolean isOperationCodeIncluded(String operationCode) throws Exception
	{
		if (this.excludeSpecificOperationType.size()>0 && this.excludeSpecificOperationType.contains(operationCode))
			return false;
		
		if (this.includeOperationTypePrefixList.size()==0 && this.includeSpecificOperationType.size()==0)
		{
			if (operationCode.equalsIgnoreCase(DEFAULT_OPERATION_TYPE_PREFIX))return true;
		}else{
			if (this.includeSpecificOperationType.contains(operationCode))
				return true;
			if (this.includeOperationTypePrefixList.contains(CommonUtils.parseForPrefixOperationType(operationCode)))
				return true;
		}
		return false;
	}
	
	public void setIncludeSpecificOperationType(
			List<String> includeSpecificOperationType) {
		this.includeSpecificOperationType = includeSpecificOperationType;
	}

	public List<String> getIncludeSpecificOperationType() {
		return includeSpecificOperationType;
	}

	public void setIncludeOperationTypePrefixList(
			List<String> includeOperationTypePrefixList) {
		this.includeOperationTypePrefixList = includeOperationTypePrefixList;
	}

	public List<String> getIncludeOperationTypePrefixList() {
		return includeOperationTypePrefixList;
	}
	
	public void afterPropertiesSet()
	{
		super.afterPropertiesSet();
		if (this.includeOperationTypePrefixList==null)this.includeOperationTypePrefixList = new ArrayList<String>();
		if (this.includeSpecificOperationType==null)this.includeSpecificOperationType = new ArrayList<String>();
		if (this.excludeSpecificOperationType==null)this.excludeSpecificOperationType = new ArrayList<String>();
	}
}
