package com.idsdatanet.d2.drillnet.activity;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ActivityMatrixDataNodeListener extends EmptyDataNodeListener{

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof Daily) {
			Daily thisDaily = (Daily) object;
			String dailyUid = thisDaily.getDailyUid();
			String operationUid = userSelection.getOperationUid();
			Operation thisOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
			Double operationHour = 24.0;
			if (thisOperation!=null && StringUtils.isNotBlank(thisOperation.getOperationHour())) {
				if (!Double.isNaN(Double.parseDouble(thisOperation.getOperationHour()))) {
					operationHour = Double.parseDouble(thisOperation.getOperationHour());
				}
			}
			
			ReportDaily rd = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), thisDaily.getDailyUid());
			String reportNumber = "-";
			if (rd!=null) reportNumber = rd.getReportNumber(); 
			thisDaily.setDayNumber(reportNumber);
			
			NumberFormat nf = NumberFormat.getNumberInstance();
			nf.setMinimumFractionDigits(2);
			nf.setMaximumFractionDigits(2);

			node.getDynaAttr().put("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), operationUid));
			Double simopHours = this.totalActivityHours(dailyUid,"isSimop") / 3600.0;
			node.getDynaAttr().put("simopHours", nf.format(simopHours));
			
			Double offlineHours = this.totalActivityHours(dailyUid,"isOffline") / 3600.0;
			//if (thisConverter.isUOMMappingAvailable()) node.setCustomUOM("@offlineHours", thisConverter.getUOMMapping());
			node.getDynaAttr().put("offlineHours", nf.format(offlineHours));
			
			Double nptHours = this.totalActivityHours(dailyUid, "nptHours") / 3600.0;
			node.getDynaAttr().put("nptHours", nf.format(nptHours));
			
			Double dayDuration = this.totalActivityHours(dailyUid,"") / 3600.0;
			node.getDynaAttr().put("hoursRecorded", nf.format(dayDuration));
			node.getDynaAttr().put("isNotFullDay", (Math.round(dayDuration) < operationHour?"1":""));
			
			Double cumDuration = CommonUtil.getConfiguredInstance().daysOnOperation(operationUid, dailyUid) / 3600.0;
			//thisConverter.setBaseValue(cumDuration);
			//if (thisConverter.isUOMMappingAvailable()) node.setCustomUOM("@cumHours", thisConverter.getUOMMapping());
			node.getDynaAttr().put("cumHours", nf.format(cumDuration));
			
			nf.setMinimumFractionDigits(3);
			nf.setMaximumFractionDigits(3);
			node.getDynaAttr().put("cumDays", nf.format(cumDuration / operationHour));
			node.getDynaAttr().put("note", this.isSpudDay(thisOperation, thisDaily));
		}
	}
	
	private String isSpudDay(Operation thisOperation, Daily thisDaily) {
		Date spudDate = thisOperation.getSpudDate();
		if (spudDate==null) return "";
		
		Date dayDate = thisDaily.getDayDate();
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		
		if (DateUtils.isSameDay(dayDate, spudDate)) {
			return "Spud @ "+df.format(spudDate);
		}
	
		return "";
	}
	
	private Double totalActivityHours(String dailyUid, String fieldName) {
		Double totalHours = 0.0;
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		try {
			String strSql = "SELECT sum(activityDuration) as totalDuration FROM Activity WHERE (isDeleted=false or isDeleted is null) AND dailyUid=:dailyUid AND (dayPlus IS NULL OR dayPlus=0)";
			if (!"".equals(fieldName)) {
				if("nptHours".equals(fieldName)) {
					strSql += " AND (internal_class_code = 'TP' OR internal_class_code = 'TU')";
				} else {
					strSql += " AND " + fieldName + "='1'";
				}
			} else {
				strSql += " AND (isSimop=false or isSimop is null)" +
						" AND (isOffline=false or isOffline is null)";
			}
			List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyUid, qp);
			if (rs!=null) {
				if (rs.size()>0) {
					Object a = rs.get(0);
					if (a!=null) totalHours = Double.parseDouble(a.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return totalHours;
	}
	
}
