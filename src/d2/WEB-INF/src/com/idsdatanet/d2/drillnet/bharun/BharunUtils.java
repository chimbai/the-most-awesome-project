package com.idsdatanet.d2.drillnet.bharun;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.ibm.icu.impl.duration.TimeUnit;
import com.idsdatanet.d2.calculation.drillnet.BharunCalculation;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.DownholeTools;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.web.mvc.SystemMessage;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.model.CasingSection;

public class BharunUtils {
	public static String nullSafeToString(Object value){
		if(value == null) return null;
		return value.toString();
	}
	
	public static String continueBharunNumberCondition (String operationUid, String dailyUid) throws Exception {
		String strInCondition = "";
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		if(daily != null) {
			Date todayDate = daily.getDayDate();
	
			String strSql = "select dailyidIn, dailyidOut, bharunUid from Bharun where (isDeleted = false or isDeleted is null) and operationUid =:operationUid";
			List <Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			
			if (lstResult.size() > 0) {
				
				for (Object[] bharunResult : lstResult) {
					if (StringUtils.isNotBlank(nullSafeToString(bharunResult[0])) && StringUtils.isNotBlank(nullSafeToString(bharunResult[1]))) {
					
						Daily bharunDateIn =  ApplicationUtils.getConfiguredInstance().getCachedDaily(bharunResult[0].toString());
						Daily bharunDateOut =  ApplicationUtils.getConfiguredInstance().getCachedDaily(bharunResult[1].toString());
						
						if (bharunDateIn !=null && bharunDateOut !=null){
							Date dateIn = bharunDateIn.getDayDate();
							Date dateOut = bharunDateOut.getDayDate();
							
							if (todayDate.getTime() <= dateOut.getTime() && todayDate.getTime() >= dateIn.getTime()) {
								String bharunUid = bharunResult[2].toString();
								strInCondition = strInCondition + "'" + bharunUid + "',"; 
		
							}
						}
					}else if (StringUtils.isNotBlank(nullSafeToString(bharunResult[0])) && StringUtils.isBlank(nullSafeToString(bharunResult[1]))) {
				
						Daily bharunDateIn =  ApplicationUtils.getConfiguredInstance().getCachedDaily(bharunResult[0].toString());
						
						if (bharunDateIn !=null) {
							Date dateIn = bharunDateIn.getDayDate();
							
							if (dateIn.getTime() > todayDate.getTime()){
								
							}else if (dateIn.getTime() <= todayDate.getTime()) {
								String bharunUid = bharunResult[2].toString();
								strInCondition = strInCondition + "'" + bharunUid + "',"; 
							}
						}
					}else if (StringUtils.isBlank(nullSafeToString(bharunResult[0])) && StringUtils.isBlank(nullSafeToString(bharunResult[1]))) {
						String bharunUid = bharunResult[2].toString();
						strInCondition = strInCondition + "'" + bharunUid + "',"; 
					}
				}
			
			}
			strInCondition = StringUtils.substring(strInCondition, 0, -1);
		}
		return strInCondition;
	}
	
	public static Double dailyTotalProgress(String operationUid, String dailyUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double totalProgress = 0.0;
		
		String strSql = "SELECT SUM(progress) FROM BharunDailySummary WHERE (isDeleted = false OR isDeleted is null)" +
						" AND dailyUid =:dailyUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyUid, qp);
		
		Object a = (Object) lstResult.get(0);
		if (a != null) totalProgress = Double.parseDouble(a.toString());
		
		return totalProgress;
	}
	
	/**
	 * Common Method for calculating Percent Slide
	 * @param totalSlideDistance
	 * @param totalRotatedDistance 
	 * @param slideDistance
	 * @throws Exception
	 * @return percentSlide
	 */
	public static Double calculatePercentSlide(Double totalSlideDistance, Double totalRotatedDistance, Double slideDistance) throws Exception{
		Double percentSlide = 0.00;
		Double totalSlideRotatedDistance = totalSlideDistance+totalRotatedDistance;
		
		if ((totalSlideRotatedDistance) > 0.00){
			percentSlide = (slideDistance/totalSlideRotatedDistance);
		}
		
		return percentSlide;
	}
	
	/**
	 * Common Method for calculating Percent Rotated
	 * @param totalSlideDistance
	 * @param totalRotatedDistance 
	 * @param rotatedDistance
	 * @throws Exception
	 * @return percentRotated
	 */
	public static Double calculatePercentRotated(Double totalSlideDistance, Double totalRotatedDistance, Double rotatedDistance) throws Exception{
		Double percentRotated = 0.00;
		Double totalSlideRotatedDistance = totalSlideDistance+totalRotatedDistance;
		
		if ((totalSlideRotatedDistance) > 0.00){
			percentRotated = (rotatedDistance/totalSlideRotatedDistance);
		}
		return percentRotated;
	}
	
	/**
	 * Common Method for calculating Percent Circulated
	 * @param totalDurationCirculated
	 * @param totalIADCDuration 
	 * @throws Exception
	 * @return percentCirculated
	 */
	public static Double calculatePercentCirculated(Double totalDurationCirculated, Double totalIADCDuration) throws Exception{
		Double percentCirculated = 0.00;
		
		if (totalDurationCirculated > 0.00 && totalIADCDuration > 0.00){
			percentCirculated = (totalDurationCirculated/totalIADCDuration);
		}
		
		return percentCirculated;
	}
	
	/**
	 * Method for calculating cumulative hours used for bha component based on totalHoursUsed keyed through different run.
	 * It will be take into consideration for service log which added to decide if there is any reset for next cumulative hours used.
	 * Cumulative value will only be calculated if there is serial number being keyed for each bha component. 
	 * As the cumulative calculation will work according to serial number.
	 * @param serialNumber
	 * @param dailyUid
	 * @param bharunUid
 	 * @throws Exception
	 * @return cumulativeHoursUsed
	 */
	public static Double calculateCumTotalHoursUsed(String serialNumber, String dailyUid, String bharunUid) throws Exception{
		//get current date for SQL query condition based dailyUid
		Daily currentDay = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		Date currentDate = null;
		if(currentDay != null){
			currentDate = currentDay.getDayDate();
		}else{
			currentDate = Calendar.getInstance().getTime();
		}
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		List lstResult = null;
		if(StringUtils.isNotEmpty(serialNumber)  && serialNumber != null){
			//get bha component's bharun.timeIn & timeOut if got entries based bharunUid
			Date timeIn = null;
			Date timeOut = null;
			if (StringUtils.isNotEmpty(bharunUid)  && bharunUid != null){
				String bhaString = "FROM Bharun WHERE (isDeleted=false OR isDeleted is null) AND bharunUid = :bharunUid";
				List<Bharun> bhalist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(bhaString, "bharunUid", bharunUid);
				if(bhalist != null && bhalist.size() > 0){
					Bharun bhaRec = bhalist.get(0);
					timeIn = bhaRec.getTimeIn();
					timeOut = bhaRec.getTimeOut();
				}
			}
			
			/* if time Out for component is filled then will include that to get last service log's time out else
			 * get last reset service log's time out based on serial # & current date time only  
			 */
			List<Date> serviceLoglist = null;
			if (timeOut != null){
				String queryString = "SELECT b.timeOut FROM Bharun b, InventoryServiceLog i WHERE i.bharunUid = b.bharunUid AND (b.isDeleted=false OR b.isDeleted is null) AND (i.isDeleted=false OR i.isDeleted is null)AND i.serialNum = :serialNum AND i.counterReset = 1 AND b.timeOut <= :currentDate AND b.timeOut <= :timeOut ORDER BY b.timeOut DESC";
				serviceLoglist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"serialNum", "currentDate", "timeOut"}, new Object[] {serialNumber, new Date(DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH).getTime()+86399999), timeOut});				
			}else{
				String queryString = "SELECT b.timeOut FROM Bharun b, InventoryServiceLog i WHERE i.bharunUid = b.bharunUid AND (b.isDeleted=false OR b.isDeleted is null) AND (i.isDeleted=false OR i.isDeleted is null)AND i.serialNum = :serialNum AND i.counterReset = 1 AND b.timeOut <= :currentDate ORDER BY b.timeOut DESC";
				serviceLoglist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"serialNum", "currentDate"}, new Object[] {serialNumber, new Date(DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH).getTime()+86399999)});				
			}				
			
			//BHA Component auto-calculate field: sum Total Hours Used to exclude deleted operation(20111019-0850-tfsim)
			String strSql2 = "SELECT SUM(c.totalHoursUsed) FROM " +
							" Bharun a, BhaComponent c, Operation o, Wellbore wb, Well w " +
							" WHERE " +
							" c.bharunUid=a.bharunUid and " +
							" o.operationUid = a.operationUid and wb.wellboreUid=o.wellboreUid and w.wellUid=wb.wellUid and" +
							" (a.isDeleted = false OR a.isDeleted is null) AND " + 
							" (c.isDeleted = false OR c.isDeleted is null) AND " + 
							" (o.isDeleted = false OR o.isDeleted is null) AND  " +
							" (wb.isDeleted = false OR wb.isDeleted is null) AND  " +
							" (w.isDeleted = false OR w.isDeleted is null) ";
			
			if(serviceLoglist != null && serviceLoglist.size() > 0){					
				Date lastResetDate = serviceLoglist.get(0);
				if(lastResetDate != null){
					if (lastResetDate.getTime() < (currentDate.getTime() + 86399999)){
						// Handle case where last reset date fall in current date slot							 
						if (lastResetDate.after(currentDate) || lastResetDate.equals(currentDate)){
							//Handle case for cumulative hours which fall after last reset date time
							if(timeIn.after(lastResetDate) || timeIn.equals(lastResetDate)){									
								if (timeOut!= null){
									String[] paramsFields2 = {"serialNumber", "lastResetDate", "timeOut"};
									Object[] paramsValues2 = new Object[3]; 
									paramsValues2[0] = serialNumber; 
									paramsValues2[1] = lastResetDate;
									paramsValues2[2] = timeOut;
									strSql2 += " AND c.serialNum = :serialNumber AND a.timeIn >= :lastResetDate and a.timeIn <= :timeOut";
									lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
								}else{
									String[] paramsFields2 = {"serialNumber", "lastResetDate", "currentDate"};
									Object[] paramsValues2 = new Object[3]; 
									paramsValues2[0] = serialNumber; 
									paramsValues2[1] = lastResetDate; 
									paramsValues2[2] = new Date(DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH).getTime()+86399999);
									strSql2 += " AND c.serialNum = :serialNumber AND a.timeIn >= :lastResetDate AND a.timeIn <= :currentDate";
									lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
								}								
							}else{
								/* Handle case for cumulative hours which fall before or equal to last reset date time. 
								 * Based on the last reset date time, search back for the previous reset date if exist for calculation of cumulative hours 
								 * else sum up hours from beginning until last reset date
								*/
								String queryString2 = "SELECT b.timeOut FROM Bharun b, InventoryServiceLog i WHERE i.bharunUid = b.bharunUid AND (b.isDeleted=false OR b.isDeleted is null) AND (i.isDeleted=false OR i.isDeleted is null)AND i.serialNum = :serialNum AND i.counterReset = 1 AND b.timeOut < :lastResetDate ORDER BY b.timeOut DESC";
								List<Date> serviceLoglist2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString2, new String[] {"serialNum", "lastResetDate"}, new Object[] {serialNumber, lastResetDate});
								if(serviceLoglist2 != null && serviceLoglist2.size() > 0){
									Date lastResetDate2 = serviceLoglist2.get(0);
									String[] paramsFields2 = {"serialNumber", "lastResetDate", "lastResetDate2"};
									Object[] paramsValues2 = new Object[3]; 
									paramsValues2[0] = serialNumber; 
									paramsValues2[1] = lastResetDate; 
									paramsValues2[2] = lastResetDate2;
									strSql2 += " AND c.serialNum = :serialNumber AND a.timeIn > :lastResetDate2 AND a.timeIn <= :lastResetDate";
									lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
								}else{
									String[] paramsFields2 = {"serialNumber", "lastResetDate", "timeOut"};
									Object[] paramsValues2 = new Object[3]; 
									paramsValues2[0] = serialNumber; 
									paramsValues2[1] = lastResetDate;
									paramsValues2[2] = timeOut;
									strSql2 += " AND c.serialNum = :serialNumber AND a.timeIn <= :lastResetDate and a.timeIn <= :timeOut";
									lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
								}
							}
						}else{
							/*  Handle case where last reset date time is less than current date.
							 *  Calculate cumulative hours from last reset date time until current date time if there is no component's time out being keyed
							 */
							if (timeOut != null){
								String[] paramsFields2 = {"serialNumber", "lastResetDate", "timeOut"};
								Object[] paramsValues2 = new Object[3]; 
								paramsValues2[0] = serialNumber; 
								paramsValues2[1] = lastResetDate; 
								paramsValues2[2] = timeOut;
								strSql2 += " AND c.serialNum = :serialNumber AND a.timeIn > :lastResetDate AND a.timeIn <= :timeOut";
								lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
							}else{
								String[] paramsFields2 = {"serialNumber", "lastResetDate", "currentDate"};
								Object[] paramsValues2 = new Object[3]; 
								paramsValues2[0] = serialNumber; 
								paramsValues2[1] = lastResetDate; 
								paramsValues2[2] = new Date(DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH).getTime()+86399999);
								strSql2 += " AND c.serialNum = :serialNumber AND a.timeIn > :lastResetDate AND a.timeIn <= :currentDate";
								lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
							}								
						}
					}
				}
			}else{
				/* For those bha component records that do not have reset service log, then will sum up those components' totalHoursUsed for range timeIn <= current date and timeIn <= current component's bharun.timeOut if timeOut is not null
				 * else just sum up those components' totalHoursUsed for range timeIn <= current date
				*/
				if (timeOut != null){
					String[] paramsFields2 = {"serialNumber", "currentDate", "timeOut"};
					Object[] paramsValues2 = new Object[3]; 
					paramsValues2[0] = serialNumber; 
					paramsValues2[1] = new Date(DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH).getTime()+86399999);
					paramsValues2[2] = timeOut;
					strSql2 += " AND c.serialNum = :serialNumber AND a.timeIn <= :currentDate and a.timeIn <= :timeOut";
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
				}else{
					String[] paramsFields2 = {"serialNumber", "currentDate"};
					Object[] paramsValues2 = new Object[2]; 
					paramsValues2[0] = serialNumber; 
					paramsValues2[1] = new Date(DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH).getTime()+86399999);						
					strSql2 += " AND c.serialNum = :serialNumber AND a.timeIn <= :currentDate";
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
				}					
			}
		}
		
		//Get the calculated cumulative hours as return value
		Double cumulativeHoursUsed = 0.00;			
		if(lstResult != null){
			Object thisResult = (Object) lstResult.get(0);				
			if (thisResult != null) {
				cumulativeHoursUsed = Double.parseDouble(thisResult.toString());				
			}
		}

		return cumulativeHoursUsed;		
	}	
	
	/**
	 * To calculate %HHP at bit value only then pass to savePercentHHPb to save the value
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter 
	 * @return Double
	 * @throws Exception
	 */
	private static Double calculatePercentHHPb(String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get average flow rate from drilling parameters
		String strSql = "SELECT AVG(a.flowAvg) as avgFlow FROM DrillingParameters a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.flowAvg > 0";
		String[] paramsFields = {"BharunUid", "DailyUid"};
		String[] paramsValues = {BharunUid, DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Object flowResult = (Object) lstResult.get(0);
		Double flowAvg = 0.00;
		if (flowResult != null){
			flowAvg = Double.parseDouble(flowResult.toString());
			
			//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
			//flowAvg = flowAvg / 0.0000630902;		
			//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
			thisConverter.setReferenceMappingField(DrillingParameters.class, "flowAvg");
			thisConverter.setBaseValue(flowAvg);
			thisConverter.changeUOMUnit("USGallonsPerMinute");
			flowAvg = thisConverter.getConvertedValue();
			
		}else{
			systemMessage.addWarning("%HHP at bit Calculation: Average Flow (Drilling Parameter) value empty -> %HHP at bit = 0.0");
			return 0.0;
		}
		
		//get stand pipe pressure from drilling parameters
		String strSql2 = "SELECT AVG(a.standpipePressure) as standpipePressure FROM DrillingParameters a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.standpipePressure > 0";
		String[] paramsFields2 = {"BharunUid", "DailyUid"};
		String[] paramsValues2 = {BharunUid, DailyUid};
		List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
		
		Object pressureResult = (Object) lstResult2.get(0);
		Double sppAvg = 0.00;
		if (pressureResult != null){
			sppAvg = Double.parseDouble(pressureResult.toString());
			
			//convert to psi uom
			thisConverter.setReferenceMappingField(DrillingParameters.class, "standpipePressure");
			thisConverter.setBaseValue(sppAvg);
			thisConverter.changeUOMUnit("PoundsPerSquareInch");
			sppAvg = thisConverter.getConvertedValue();
			
		}else{
			systemMessage.addWarning("%HHP at bit Calculation: Average Stand Pipe Pressure (Drilling Parameter) value empty -> %HHP at bit = 0.0");
			return 0.0;
		}
		
		//get bitrunUid
		strSql = "SELECT bitrunUid FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
		
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", BharunUid, qp);
		
		if (lstResult.size()<=0){
			systemMessage.addWarning("%HHP at bit Calculation: No Bit run associated to this BHA -> %HHP at bit = 0.0");
			return 0.0;
		}
		
		Object thisBitResult = (Object) lstResult.get(0);
		//get bitrunUid for acquire bit nozzle
		String bitrunUid = thisBitResult.toString();
				
		//get MW 
		strSql = "SELECT AVG(mudWeight) as avgMW FROM MudProperties WHERE (isDeleted = false or isDeleted is null) and dailyUid =:DailyUid";
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "DailyUid", DailyUid, qp);
		Object thisMWResult = (Object) lstResult.get(0);
		Double mudWeight = 0.00;
		if (thisMWResult != null){
			mudWeight = Double.parseDouble(thisMWResult.toString());
			
			//convert to PoundsMassPerUKGallon for calculation the IDS base is KilogramsPerCubicMetre		
			//mudWeight = mudWeight / 99.77633;
			//convert to PoundsMassPerUSGallon for calculation the IDS base is KilogramsPerCubicMetre
			thisConverter.setReferenceMappingField(MudProperties.class, "mudWeight");
			thisConverter.setBaseValue(mudWeight);
			thisConverter.changeUOMUnit("PoundsMassPerUSGallon");			
			mudWeight = thisConverter.getConvertedValue();
		}else{
			systemMessage.addWarning("%HHP at bit Calculation: Mud Weight (Mud Properties) value empty -> %HHP at bit = 0.0");
			return 0.0;
		}
		
		//get bit nozzle
		strSql = "SELECT nozzleQty, nozzleSize FROM BitNozzle WHERE (isDeleted = false or isDeleted is null) and bitrunUid =:bitrunUid";
		List<Object[]> lstNozzleResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bitrunUid", bitrunUid, qp);
		Double nozzleQty = 0.00;
		Double nozzleSize = 0.00;
		Double nozzleCalc = 0.00;
		Double nozzleTotal = 0.00;
		
		if (lstNozzleResult.size() > 0){
			for(Object[] thisNozzleResult: lstNozzleResult){
				if (thisNozzleResult[0] != null) nozzleQty = Double.parseDouble(thisNozzleResult[0].toString());
				if (thisNozzleResult[1] != null) nozzleSize = Double.parseDouble(thisNozzleResult[1].toString());
				
				// need to convert to 32nd of inch for calculation the base is m 
				//nozzleSize = nozzleSize / 0.0007937;
				thisConverter.setReferenceMappingField(BitNozzle.class, "nozzleSize");
				thisConverter.setBaseValue(nozzleSize);
				thisConverter.changeUOMUnit("32ndOfAnInch");			
				nozzleSize = thisConverter.getConvertedValue();
				
				nozzleCalc = nozzleQty * (nozzleSize * nozzleSize);
				nozzleTotal = nozzleTotal + nozzleCalc;
			}
		}
		else {
			systemMessage.addWarning("%HHP at bit Calculation: Bit Nozzle value empty -> %HHP at bit = 0.0");
			return 0.0;
		}
		
		
		//calculate Pb
		Double pbCalc = BharunCalculation.calculatePb(flowAvg, mudWeight, nozzleTotal);
		if (pbCalc == 0.00){
			return 0.0;
		}
		
		//calculate % HHP at bit
		Double percentHHPb = BharunCalculation.calculatePercentHHPb(pbCalc, flowAvg, sppAvg);				
		return percentHHPb;
	}
	
	/**
	 * To save %HHP at bit value  
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage
	 * @return void
	 * @throws Exception
	 */
	public static void savePercentHHPb (CustomFieldUom thisConverter, String BharunUid, String DailyUid, SystemMessage systemMessage) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double percentHHPb = BharunUtils.calculatePercentHHPb(BharunUid, DailyUid, systemMessage,thisConverter);
			
		//get bharun daily summary to update 
		String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid AND dailyUid=:DailyUid";		
		String[] paramsFields = {"BharunUid", "DailyUid"};
		String[] paramsValues = {BharunUid, DailyUid};
		List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if (lstResult1.size() > 0){			
			BharunDailySummary thisBhaDaily = (BharunDailySummary) lstResult1.get(0);
			thisBhaDaily.setBitHydraulicHpPercent(percentHHPb);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDaily, qp);		
		}		
	}

	/**
	 * To calculate Impact Force value only then pass to saveImpactForce to save the value
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter 
	 * @return Double
	 * @throws Exception
	 */
	private static Double calculateImpactForce(String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get average flow rate from drilling parameters
		String strSql = "SELECT AVG(a.flowAvg) as avgFlow FROM DrillingParameters a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.flowAvg > 0";
		String[] paramsFields = {"BharunUid", "DailyUid"};
		String[] paramsValues = {BharunUid, DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Object flowResult = (Object) lstResult.get(0);
		Double flowAvg = 0.00;
		if (flowResult != null){
			flowAvg = Double.parseDouble(flowResult.toString());
				
			//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
			thisConverter.setReferenceMappingField(DrillingParameters.class, "flowAvg");
			thisConverter.setBaseValue(flowAvg);
			thisConverter.changeUOMUnit("USGallonsPerMinute");
			flowAvg = thisConverter.getConvertedValue();
			
		}else{
			systemMessage.addWarning("Impact Force Calculation: Average Flow (Drilling Parameter) value empty -> Impact Force = 0.0");
			return 0.0;
		}
							
		//get MW 
		strSql = "SELECT AVG(mudWeight) as avgMW FROM MudProperties WHERE (isDeleted = false or isDeleted is null) and dailyUid =:DailyUid";
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "DailyUid", DailyUid, qp);
		Object thisMWResult = (Object) lstResult.get(0);
		Double mudWeight = 0.00;
		if (thisMWResult != null){
			mudWeight = Double.parseDouble(thisMWResult.toString());
			
			//convert to PoundsMassPerUSGallon for calculation the IDS base is KilogramsPerCubicMetre
			thisConverter.setReferenceMappingField(MudProperties.class, "mudWeight");
			thisConverter.setBaseValue(mudWeight);
			thisConverter.changeUOMUnit("PoundsMassPerUSGallon");			
			mudWeight = thisConverter.getConvertedValue();
		}else{
			systemMessage.addWarning("Impact Force Calculation: Mud Weight (Mud Properties) value empty -> Impact Force = 0.0");
			return 0.0;
		}
		
		//re-calculate jet velocity for accurate value before used it in impact force calculation
		Double jetvelocity = CommonUtil.getConfiguredInstance().calculateJetVelocity(BharunUid, DailyUid);
		if (jetvelocity != null){
			if (jetvelocity == 0.00 || mudWeight == 0.00){
				return 0.0;
			}else{
				//calculate impactForce
				Double impactForce = BharunCalculation.calculateImpactForce(flowAvg, mudWeight, jetvelocity);
				return impactForce;
			}
		}else{
			systemMessage.addWarning("Impact Force Calculation: Jet Velocity (Bharun Daily Summary) value empty -> Impact Force = 0.0");
			return 0.0;
		}			
	}
	
	/**
	 * To save Impact Force value  
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage
	 * @return void
	 * @throws Exception
	 */
	public static void saveImpactForce(CustomFieldUom thisConverter, String BharunUid, String DailyUid, SystemMessage systemMessage) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double impactForce = BharunUtils.calculateImpactForce(BharunUid, DailyUid, systemMessage,thisConverter);
			
		//get bharun daily summary to update 
		String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid AND dailyUid=:DailyUid";		
		String[] paramsFields = {"BharunUid", "DailyUid"};
		String[] paramsValues = {BharunUid, DailyUid};
		List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if (lstResult1.size() > 0){			
			BharunDailySummary thisBhaDaily = (BharunDailySummary) lstResult1.get(0);
			thisBhaDaily.setBitImpactForceAreaForce(impactForce);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDaily, qp);		
		}		
	}
	
	/**
	 * To calculate bha component's total hours used value based on sum of hours used from bha_component_hours_used records  
	 * @param String BharunUid, String BhaComponentUid
	 * @return void
	 * @throws Exception
	 */
	public static void calcBhaComponentTotalHoursUsed(String bharunUid, String bhaComponentUid) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		Double totalHoursUsed = 0.00;
								
		if((bharunUid !=null && StringUtils.isNotEmpty(bharunUid)) && (bhaComponentUid !=null && StringUtils.isNotEmpty(bhaComponentUid))){
			// Query the sum of hours used from from BhaComponentHoursUsed table based on bharunUid & bhaComponentUid
			String queryString = "SELECT sum(hoursUsed) " +
			"FROM BhaComponentHoursUsed b " +
			"WHERE (b.isDeleted=false OR b.isDeleted is null) " +								
			"AND b.bharunUid = :bharunUid " +
			"AND b.bhaComponentUid = :bhaComponentUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"bharunUid", "bhaComponentUid"}, new Object[] {bharunUid, bhaComponentUid});
			if(lstResult != null){
				Object thisResult = (Object) lstResult.get(0);				
				if (thisResult != null) {
					totalHoursUsed = Double.parseDouble(thisResult.toString());						
				}
			}
			if (totalHoursUsed>=0){
				//Update bha component's total hours used with the sum of hours used from BhaComponentHoursUsed table based on bharunUid & bhaComponentUid			
				String strSql1 = "UPDATE BhaComponent SET totalHoursUsed =:totalHoursUsed WHERE bhaComponentUid =:bhaComponentUid and bharunUid =:bharunUid";							
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, new String[] {"bharunUid", "bhaComponentUid", "totalHoursUsed"}, new Object[] {bharunUid, bhaComponentUid, totalHoursUsed}, qp);
			}				
		}		
	}

	/**
	 * To calculate bharun daily summary drag
	 * @param weightPickup, weightString
	 * @return double
	 * @throws Exception
	 */
	public static double calculateBHADailyDrag(Double weightPickup, Double weightString, CustomFieldUom thisConverter) throws Exception{

		if (weightPickup != null) {
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "weightPickup");
			thisConverter.setBaseValueFromUserValue(weightPickup);
			weightPickup = thisConverter.getBasevalue();
		}
			
		if (weightString != null) {
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "weightString");
			thisConverter.setBaseValueFromUserValue(weightString);
			weightString = thisConverter.getBasevalue();
		}
		
		double dragDown = weightPickup - weightString;	
		thisConverter.setReferenceMappingField(BharunDailySummary.class, "dragDown");
		thisConverter.setBaseValue(dragDown);

		return thisConverter.getConvertedValue();
	}
	
	/**
	 * Method to create or update Bharun and BharunDailySummary from Activity based on pre-defined free text remarks
	 * @param thisActivity
	 * @param fieldsMap
	 * @throws Exception
	 */
	public static void createBHABasedOnActivityRemarks(UserSelectionSnapshot userSelection,String moduleName,Activity thisActivity, Map<String,Object>fieldsMap) throws Exception{
		
		if(fieldsMap!=null){
			Bharun bharun=null;
			BharunDailySummary bharunDailySummary=null;
			
			String[] paramsFields = {"operationUid", "dailyUid", "activityUid"};
			Object[] paramsValues = {thisActivity.getOperationUid(), thisActivity.getDailyUid(), thisActivity.getActivityUid()};
			String strSql2 = "FROM Bharun WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND dailyidIn=:dailyUid AND activityUid=:activityUid";
			List<Bharun> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);
			
			if(ls.size()==0){
				
				String strSql = "FROM Bharun WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid";
				ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", thisActivity.getOperationUid());
							
				Integer bhaRunNumber = ls.size()+1;
				
				bharun = new Bharun();
				bharun.setDailyidIn(thisActivity.getDailyUid());
				bharun.setActivityUid(thisActivity.getActivityUid());
				bharun.setBhaRunNumber(bhaRunNumber.toString());
				bharun.setTimeIn(thisActivity.getStartDatetime());
				bharun.setDepthInMdMsl(thisActivity.getDepthFromMdMsl());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharun);
				
				if(bharun.getBharunUid()!=null){
					bharunDailySummary = new BharunDailySummary();
					bharunDailySummary.setBharunUid(bharun.getBharunUid());
					bharunDailySummary.setDailyUid(thisActivity.getDailyUid());
					
					Date time = null;
					/*if (bharunDailySummary.getCheckDateTime()!=null) {
						time = CommonUtil.getConfiguredInstance().stampTimeFieldWithDate(thisActivity.getDailyUid(), bharunDailySummary.getCheckDateTime());
					}else{
						Calendar calTime = Calendar.getInstance();
						calTime.set(Calendar.HOUR_OF_DAY, 0);
						calTime.set(Calendar.MINUTE, 0);
						calTime.set(Calendar.SECOND, 0);
						calTime.set(Calendar.MILLISECOND, 0);
						time = CommonUtil.getConfiguredInstance().stampTimeFieldWithDate(thisActivity.getDailyUid(), calTime.getTime());
					}
					bharunDailySummary.setCheckDateTime(time);*/
					for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
						if (StringUtils.isNotBlank(fieldEntry.getKey())) PropertyUtils.setProperty(bharunDailySummary, fieldEntry.getKey(), fieldEntry.getValue());
					}	
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharunDailySummary);
				}
			}else{
				bharun = ls.get(0);
				bharun.setTimeIn(thisActivity.getStartDatetime());
				bharun.setDepthInMdMsl(thisActivity.getDepthFromMdMsl());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharun);
			}
			
			if ("bharun".equalsIgnoreCase(moduleName)){				
				for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
					if (StringUtils.isNotBlank(fieldEntry.getKey())) PropertyUtils.setProperty(bharun, fieldEntry.getKey(), fieldEntry.getValue());
				}
				bharun.setTimeIn(thisActivity.getStartDatetime());
				bharun.setDepthInMdMsl(thisActivity.getDepthFromMdMsl());
				Boolean isSubmit = bharun.getIsSubmit();
				//if(isSubmit!=null && isSubmit) RevisionUtils.unSubmitRecordWithStaticRevisionLog(userSelection, bharun, "Auto update from time codes");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharun);
				
			}else if ("bharundailysummary".equalsIgnoreCase(moduleName)){
				if (bharunDailySummary==null ){
					String strSql = "FROM BharunDailySummary WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND bharunUid=:bharunUid AND dailyUid=:dailyUid";
					paramsFields = new String[]{"bharunUid", "dailyUid"};
					paramsValues = new Object[]{bharun.getBharunUid(), thisActivity.getDailyUid()};
					List<BharunDailySummary> lsBharunDailySummary = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					if(lsBharunDailySummary.size()==0){
						bharunDailySummary = new BharunDailySummary();
						bharunDailySummary.setBharunUid(bharun.getBharunUid());
						bharunDailySummary.setDailyUid(thisActivity.getDailyUid());
						
						Date time = null;
						/*if (bharunDailySummary.getCheckDateTime()!=null) {
							time = CommonUtil.getConfiguredInstance().stampTimeFieldWithDate(thisActivity.getDailyUid(), bharunDailySummary.getCheckDateTime());
						}else{
							Calendar calTime = Calendar.getInstance();
							calTime.set(Calendar.HOUR_OF_DAY, 0);
							calTime.set(Calendar.MINUTE, 0);
							calTime.set(Calendar.SECOND, 0);
							calTime.set(Calendar.MILLISECOND, 0);
							time = CommonUtil.getConfiguredInstance().stampTimeFieldWithDate(thisActivity.getDailyUid(), calTime.getTime());
						}
						bharunDailySummary.setCheckDateTime(time);*/
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharunDailySummary);
					}else{
						bharunDailySummary = lsBharunDailySummary.get(0);
					}
					
					for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
						if (StringUtils.isNotBlank(fieldEntry.getKey())) PropertyUtils.setProperty(bharunDailySummary, fieldEntry.getKey(), fieldEntry.getValue());
					}			
					Boolean isSubmit = bharunDailySummary.getIsSubmit();
					//if(isSubmit!=null && isSubmit) RevisionUtils.unSubmitRecordWithStaticRevisionLog(userSelection, bharunDailySummary, "Auto update from time codes");
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharunDailySummary);					
				}
				
			}			
		}
	}
	
	/**
	 * To calculate report daily's Total Well Drilling Hours value based on sum of Total Time on Bottom Hours for all Bits  
	 * @param 
	 * @return void
	 * @throws Exception
	 */
	public static void calcAccumulatedDrillingDuration(Daily daily) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		Double accumulatedDrillingDuration = 0.00;
								
	
		String queryString = "SELECT SUM(bds.duration) as totalDuration " +
		"FROM BharunDailySummary bds, Daily d " +
		"WHERE (bds.isDeleted = false or bds.isDeleted is null) AND (d.isDeleted = false or d.isDeleted is null) " +								
		"AND d.dailyUid = bds.dailyUid AND d.dayDate <= :currentDate AND d.operationUid = :operationUid";

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"currentDate", "operationUid"}, new Object[] {daily.getDayDate(), daily.getOperationUid()});
		if(lstResult != null){
			Object thisResult = (Object) lstResult.get(0);				
			if (thisResult != null) {
				accumulatedDrillingDuration = Double.parseDouble(thisResult.toString());						
			}
		}
		if (accumulatedDrillingDuration>=0){
			//Update report daily's Total Well Drilling Hours with the sum of Total Time on Bottom Hours for all Bits			
			String strSql1 = "UPDATE ReportDaily SET accumulatedDrillingDuration =:accumulatedDrillingDuration WHERE dailyUid =:dailyUid";							
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, new String[] {"accumulatedDrillingDuration", "dailyUid"}, new Object[] {accumulatedDrillingDuration, daily.getDailyUid()}, qp);
		}				
		
	}
	/**
	 * To re-calculate cumulative circ hour after the day is imported or stt
	 * @param String BharunUid, Object obj, UserSession session, CustomFieldUom thisConverter
	 * @return void
	 * @throws Exception
	 */
	public static void updateCumulativeCircHour(String dailyUid, String operationUid) throws Exception{
		Double totalDailyCircHour = 0.0;
		
		String strSql4 = "select distinct(dt.toolSerialNumber) from Bharun b, BharunDailySummary bds, Daily d, DownholeTools dt where (dt.isDeleted = false or dt.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
		"and b.operationUid=:operationUid and dt.tool='jar' and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid " +
		"order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
		
		List <Object> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4,
				new String[] {"operationUid"},
				new Object[] {operationUid});
		if(lstResult3!=null && lstResult3.size()>0)
		{
			for(Object record:lstResult3)
			{
				if(record!=null && record!="")
				{
					String strSql8 = "select distinct(d.dayDate) from Bharun b, BharunDailySummary bds, Daily d, DownholeTools dt where (dt.isDeleted = false or dt.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
					"and b.operationUid=:operationUid and dt.tool='jar' and dt.toolSerialNumber=:toolSerialNumber and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid " +
					"order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
					
					List <Object> lstResult8 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql8,
							new String[] {"operationUid","toolSerialNumber"},
							new Object[] {operationUid, record.toString()});
					
					if(lstResult8!=null && lstResult8.size()>0)
					{
						for(Object bhaRecord:lstResult8)
						{
							if(bhaRecord!=null)
							{
								totalDailyCircHour = 0.0;
								String strSql5 = "select dt.dailyCircHrs,dt.downholeToolsUid from Bharun b, BharunDailySummary bds, Daily d, DownholeTools dt where (dt.isDeleted = false or dt.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
								"and b.operationUid=:operationUid and dt.tool='jar' and dt.toolSerialNumber=:toolSerialNumber and d.dayDate<=:dayDate and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid " +
								"order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
								
								List <Object[]> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5,
										new String[] {"operationUid","toolSerialNumber","dayDate"},
										new Object[] {operationUid, record.toString(),bhaRecord});
								
								if(lstResult5!=null && lstResult5.size()>0)
								{
									for(Object[] dtrecord:lstResult5)
									{
										if(dtrecord[0]!=null)
										{
											totalDailyCircHour+=Double.parseDouble(dtrecord[0].toString());
										}
										String strSql = "From DownholeTools where (isDeleted is null or isDeleted = false) and " +
										"downholeToolsUid=:downholeToolsUid";
										
										List <DownholeTools> lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
												new String[] {"downholeToolsUid"},
												new Object[] {dtrecord[1].toString()});
										
										
										
										if(lstResult6!=null && lstResult6.size()>0)
										{
											for(DownholeTools downholetool:lstResult6)
											{
												PropertyUtils.setProperty(downholetool, "cumCircHrs", totalDailyCircHour);
												ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetool);
											}
										}
									}
								}
							}
						}
					}
				}
				else
				{
					String strSql7 = "select distinct(b.bharunUid) from Bharun b, BharunDailySummary bds, Daily d, DownholeTools dt where (dt.isDeleted = false or dt.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
					"and b.operationUid=:operationUid and dt.tool='jar' and (dt.toolSerialNumber is null or dt.toolSerialNumber='') and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid " +
					"order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
					
					List <Object> lstResult7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7,
							new String[] {"operationUid"},
							new Object[] {operationUid});
					
					if(lstResult7!=null && lstResult7.size()>0)
					{
						for(Object BhaRecordData:lstResult7)
						{
							
							String strSql8 = "select distinct(d.dayDate) from Bharun b, BharunDailySummary bds, Daily d where (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
							"and b.bharunUid=:bharunUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid " +
							"order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
							
							List <Object> lstResult8 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql8,
									new String[] {"bharunUid"},
									new Object[] {BhaRecordData.toString()});
							if(lstResult8!=null && lstResult8.size()>0)
							{
								for(Object bhaRecord:lstResult8)
								{
									totalDailyCircHour = 0.0;
									if(bhaRecord!=null)
									{
										String strSql9 = "select dt.dailyCircHrs,dt.downholeToolsUid from Bharun b, BharunDailySummary bds, Daily d, DownholeTools dt where (dt.isDeleted = false or dt.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
										"and b.operationUid=:operationUid and d.dayDate<=:dayDate and dt.tool='jar' and b.bharunUid=:bharunUid and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid " +
										"order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
										
										List <Object[]> lstResult9 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql9,
												new String[] {"operationUid","bharunUid","dayDate"},
												new Object[] {operationUid, BhaRecordData.toString(), bhaRecord});
										if(lstResult9!=null && lstResult9.size()>0)
										{
											for(Object[] downholeToolRecord:lstResult9)
											{
												if(downholeToolRecord[0]!=null)
												{
													totalDailyCircHour+=Double.parseDouble(downholeToolRecord[0].toString());
												}
												String strSql = "From DownholeTools where (isDeleted is null or isDeleted = false) and " +
												"downholeToolsUid=:downholeToolsUid";
												
												List <DownholeTools> lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
														new String[] {"downholeToolsUid"},
														new Object[] {downholeToolRecord[1].toString()});
												
												if(lstResult6!=null && lstResult6.size()>0)
												{
													for(DownholeTools downholetool:lstResult6)
													{
														PropertyUtils.setProperty(downholetool, "cumCircHrs", totalDailyCircHour);
														ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetool);
													}
												}
											}
											
										}
										
									}
								}
							}
							
						}
					}
				}
				
				
				
			}
		}
		
		String strSql7 = "select distinct(b.bharunUid) from Bharun b, BharunDailySummary bds, Daily d, DownholeTools dt where (dt.isDeleted = false or dt.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
		"and b.operationUid=:operationUid and dt.tool!='jar' and dt.tool!='' and dt.tool is not null and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid " +
		"order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
		
		List <Object> lstResult7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7,
				new String[] {"operationUid"},
				new Object[] {operationUid});
		
		if(lstResult7!=null && lstResult7.size()>0)
		{
			
			for(Object record:lstResult7)
			{
				
				String strSql8 = "select distinct(d.dayDate) from Bharun b, BharunDailySummary bds, Daily d where (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
				"and b.bharunUid=:bharunUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid " +
				"order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
				
				List <Object> lstResult8 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql8,
						new String[] {"bharunUid"},
						new Object[] {record.toString()});
				
				if(lstResult8!=null && lstResult8.size()>0)
				{
					for(Object bhaRecord:lstResult8)
					{
						if(bhaRecord!=null)
						{
							String strSql5 = "select distinct(dt.tool) from Bharun b, BharunDailySummary bds, Daily d, DownholeTools dt where (dt.isDeleted = false or dt.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
							"and b.operationUid=:operationUid and d.dayDate<=:dayDate and dt.tool!='jar' and dt.tool!='' and dt.tool is not null and b.bharunUid=:bharunUid and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid " +
							"order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
							
							List <Object> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5,
									new String[] {"operationUid","bharunUid","dayDate"},
									new Object[] {operationUid, record.toString(), bhaRecord});
							
							if(lstResult5!=null && lstResult5.size()>0)
							{
								for(Object dtRecord:lstResult5)
								{
									totalDailyCircHour = 0.0;
									String strSql9 = "select dt.dailyCircHrs,dt.downholeToolsUid from Bharun b, BharunDailySummary bds, Daily d, DownholeTools dt where (dt.isDeleted = false or dt.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
									"and b.operationUid=:operationUid and d.dayDate<=:dayDate and dt.tool=:tool and b.bharunUid=:bharunUid and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid " +
									"order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
									
									List <Object[]> lstResult9 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql9,
											new String[] {"operationUid","bharunUid","dayDate","tool"},
											new Object[] {operationUid, record.toString(), bhaRecord,dtRecord});
									
									if(lstResult9!=null && lstResult9.size()>0)
									{
										for(Object[] downholeToolRecord:lstResult9)
										{
											if(downholeToolRecord[0]!=null)
											{
												totalDailyCircHour+=Double.parseDouble(downholeToolRecord[0].toString());
											}
											String strSql = "From DownholeTools where (isDeleted is null or isDeleted = false) and " +
											"downholeToolsUid=:downholeToolsUid";
											
											List <DownholeTools> lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
													new String[] {"downholeToolsUid"},
													new Object[] {downholeToolRecord[1].toString()});
											
											if(lstResult6!=null && lstResult6.size()>0)
											{
												for(DownholeTools downholetool:lstResult6)
												{
													PropertyUtils.setProperty(downholetool, "cumCircHrs", totalDailyCircHour);
													ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetool);
												}
											}
										}
										
									}
								}
							}
						}
					}
				}
				
			}
		}
	}
	/**
	 * To re-calculate cumulative circ hour after the record is deleted
	 * @param String BharunUid, Object obj, UserSession session, CustomFieldUom thisConverter
	 * @return void
	 * @throws Exception
	 */
	public static void recalculateCumulativeCircHour(Date daydate, String currentSerialNumber, String tool, String operationUid, CustomFieldUom thisConverter, String bharunUid) throws Exception{
		Double totalDailyCircHour = 0.0;
		QueryProperties qp = new QueryProperties();
		thisConverter.setReferenceMappingField(DownholeTools.class, "dailyCircHrs");
		qp.setUomConversionEnabled(false);
		
		if(tool.equalsIgnoreCase("jar"))
		 {
			if(currentSerialNumber!=null && currentSerialNumber!="")
			{
				String strSql2 = "Select distinct(d.dayDate) FROM BharunDailySummary bds,Daily d where (d.isDeleted=false or d.isDeleted is null) and (bds.isDeleted=false or bds.isDeleted is null) and bds.dailyUid=d.dailyUid and d.operationUid=:operationUid and d.dayDate>=:dayDate order by d.dayDate";
				List <Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[] {"operationUid","dayDate"}, new Object[] {operationUid,daydate});
				if (lstResult.size() > 0 && lstResult!=null){
					for (Object objRecord: lstResult){
						totalDailyCircHour = 0.0;
						if(objRecord!=null)
						{
							String strSql3 = "select bds.bharunDailySummaryUid from Bharun b, BharunDailySummary bds, Daily d where (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
							"and b.operationUid=:operationUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid and " +
							"d.dayDate <=:currentRecordDate order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
							
							List <Object> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3,
									new String[] {"operationUid", "currentRecordDate"},
									new Object[] {operationUid, objRecord}, qp);
							
							if(lstResult2.size() >0 && lstResult2!=null)
								{
									for (Object record:lstResult2)
									{
										String strSql = "From DownholeTools where (isDeleted is null or isDeleted = false) and " +
										"bharunDailySummaryUid=:bharunDailySummaryUid and tool='jar' and toolSerialNumber=:toolSerialNumber";
										
										List <DownholeTools> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
												new String[] {"bharunDailySummaryUid","toolSerialNumber"},
												new Object[] {record.toString(), currentSerialNumber}, qp);
										if(lstResult3!=null && lstResult3.size()>0)
										{
											for(DownholeTools downholetool:lstResult3)
											{
												if(downholetool.getDailyCircHrs()!=null)
												{
													thisConverter.setBaseValue(downholetool.getDailyCircHrs());
													totalDailyCircHour += thisConverter.getBasevalue();
													
												}
												PropertyUtils.setProperty(downholetool, "cumCircHrs", totalDailyCircHour);
												ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetool, qp);
											}
										}
										
									}
								}
						}
					}
				}
			}
			else
			{
				String strSql2 = "Select distinct(d.dayDate) FROM BharunDailySummary bds,Daily d, Bharun b where b.bharunUid=:bharunUid and b.bharunUid=bds.bharunUid and (b.isDeleted=false or b.isDeleted is null) and (d.isDeleted=false or d.isDeleted is null) and (bds.isDeleted=false or bds.isDeleted is null) and bds.dailyUid=d.dailyUid and d.operationUid=:operationUid and d.dayDate>=:dayDate order by d.dayDate";
				List <Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[] {"operationUid","dayDate","bharunUid"}, new Object[] {operationUid,daydate,bharunUid});
				if (lstResult.size() > 0 && lstResult!=null){
					for (Object objRecord: lstResult){
						totalDailyCircHour = 0.0;
						if(objRecord!=null)
						{
							String strSql5 = "select bds.bharunDailySummaryUid from Bharun b, BharunDailySummary bds, Daily d where (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
							"and b.operationUid=:operationUid and b.bharunUid=:bharunUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid and " +
							"d.dayDate <=:currentRecordDate order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
							
							List <Object> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5,
									new String[] {"operationUid", "currentRecordDate", "bharunUid"},
									new Object[] {operationUid, objRecord, bharunUid}, qp);
							
							if(lstResult5.size() >0 && lstResult5!=null)
							{
								for (Object record:lstResult5)
								{
									String strSql = "From DownholeTools where (isDeleted is null or isDeleted = false) and " +
									"bharunDailySummaryUid=:bharunDailySummaryUid and tool=:tool and (toolSerialNumber is null or toolSerialNumber='')";
									
									List <DownholeTools> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
											new String[] {"bharunDailySummaryUid","tool"},
											new Object[] {record.toString(),tool}, qp);
									
									if(lstResult3!=null && lstResult3.size()>0)
									{
										for(DownholeTools downholetool:lstResult3)
										{
											if(downholetool.getDailyCircHrs()!=null)
											{
												thisConverter.setBaseValue(downholetool.getDailyCircHrs());
												totalDailyCircHour += thisConverter.getBasevalue();
												
											}
											PropertyUtils.setProperty(downholetool, "cumCircHrs", totalDailyCircHour);
											ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetool, qp);
										}
										
									}
									
								}
								
							}
							
						}
						
					}
				}
			}
			
			}
		else
			{
				String strSql2 = "Select distinct(d.dayDate) FROM BharunDailySummary bds,Daily d, Bharun b where b.bharunUid=:bharunUid and b.bharunUid=bds.bharunUid and (b.isDeleted=false or b.isDeleted is null) and (d.isDeleted=false or d.isDeleted is null) and (bds.isDeleted=false or bds.isDeleted is null) and bds.dailyUid=d.dailyUid and d.operationUid=:operationUid and d.dayDate>=:dayDate order by d.dayDate";
				List <Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[] {"operationUid","dayDate","bharunUid"}, new Object[] {operationUid,daydate,bharunUid});
				if (lstResult.size() > 0 && lstResult!=null){
					for (Object objRecord: lstResult){
						totalDailyCircHour = 0.0;
						if(objRecord!=null)
						{
							String strSql5 = "select bds.bharunDailySummaryUid from Bharun b, BharunDailySummary bds, Daily d where (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
							"and b.operationUid=:operationUid and b.bharunUid=:bharunUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid and " +
							"d.dayDate <=:currentRecordDate order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
							
							List <Object> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5,
									new String[] {"operationUid", "currentRecordDate", "bharunUid"},
									new Object[] {operationUid, objRecord, bharunUid}, qp);
							
							if(lstResult5.size() >0 && lstResult5!=null)
							{
								for (Object record:lstResult5)
								{
									String strSql = "From DownholeTools where (isDeleted is null or isDeleted = false) and " +
									"bharunDailySummaryUid=:bharunDailySummaryUid and tool=:tool and tool is not null and tool!=''";
									
									List <DownholeTools> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
											new String[] {"bharunDailySummaryUid","tool"},
											new Object[] {record.toString(),tool}, qp);
									
									if(lstResult3!=null && lstResult3.size()>0)
									{
										for(DownholeTools downholetool:lstResult3)
										{
											if(downholetool.getDailyCircHrs()!=null)
											{
												thisConverter.setBaseValue(downholetool.getDailyCircHrs());
												totalDailyCircHour += thisConverter.getBasevalue();
												
											}
											PropertyUtils.setProperty(downholetool, "cumCircHrs", totalDailyCircHour);
											ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetool, qp);
										}
										
									}
									
								}
								
							}
							
						}
						
					}
				}
		}
			
		
		
	}
	/**
	 * To calculate cumulative circ hour
	 * @param String BharunUid, Object obj, UserSession session, CustomFieldUom thisConverter
	 * @return void
	 * @throws Exception
	 */
	public static void calculateCumulativeCircHour(String dailyUid, String currentSerialNumber, String tool, String operationUid, CustomFieldUom thisConverter, String bharunUid, String downholeToolsUid) throws Exception{
		
		Double totalDailyCircHour = 0.0;
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		thisConverter.setReferenceMappingField(DownholeTools.class, "dailyCircHrs");
		Daily currDaily=ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		if((tool!="") && (tool!=null))
		{
			if(tool.equalsIgnoreCase("jar"))
			{
				if(currentSerialNumber!=null && currentSerialNumber!="")
				{
					String strSql2 = "Select distinct(d.dayDate) FROM BharunDailySummary bds,Daily d where (d.isDeleted=false or d.isDeleted is null) and (bds.isDeleted=false or bds.isDeleted is null) and bds.dailyUid=d.dailyUid and d.operationUid=:operationUid and d.dayDate>=:dayDate order by d.dayDate";
					List <Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[] {"operationUid","dayDate"}, new Object[] {operationUid,currDaily.getDayDate()});
					if (lstResult.size() > 0 && lstResult!=null){
						for (Object objRecord: lstResult){
							totalDailyCircHour = 0.0;
							if(objRecord!=null)
							{
								String strSql3 = "select bds.bharunDailySummaryUid from Bharun b, BharunDailySummary bds, Daily d where (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
								"and b.operationUid=:operationUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid and " +
								"d.dayDate <=:currentRecordDate order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
								
								List <Object> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3,
										new String[] {"operationUid", "currentRecordDate"},
										new Object[] {operationUid, objRecord}, qp);
								
								if(lstResult2.size() >0 && lstResult2!=null)
								{
									for (Object record:lstResult2)
									{
										String strSql = "From DownholeTools where (isDeleted = false or isDeleted is null) " +
										"and bharunDailySummaryUid=:bharunDailySummaryUid and tool='jar' and toolSerialNumber=:toolSerialNumber";
										
										List <DownholeTools> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
												new String[] {"bharunDailySummaryUid","toolSerialNumber"},
												new Object[] {record.toString(), currentSerialNumber}, qp);
										
										if(lstResult3!=null && lstResult3.size()>0)
										{
											for(DownholeTools downholetool:lstResult3)
											{
												if(downholetool.getDailyCircHrs()!=null)
												{
													thisConverter.setBaseValue(downholetool.getDailyCircHrs());
													totalDailyCircHour += thisConverter.getBasevalue();
													
												}
												PropertyUtils.setProperty(downholetool, "cumCircHrs", totalDailyCircHour);
												ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetool, qp);
											}
										}
										
									}
									
								}
								
							}
							
						}
					}
				}
				else
				{
					String strSql2 = "Select distinct(d.dayDate) FROM BharunDailySummary bds,Daily d, Bharun b where b.bharunUid=:bharunUid and b.bharunUid=bds.bharunUid and (b.isDeleted=false or b.isDeleted is null) and (d.isDeleted=false or d.isDeleted is null) and (bds.isDeleted=false or bds.isDeleted is null) and bds.dailyUid=d.dailyUid and d.operationUid=:operationUid and d.dayDate>=:dayDate order by d.dayDate";
					List <Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[] {"operationUid","dayDate","bharunUid"}, new Object[] {operationUid,currDaily.getDayDate(),bharunUid});
					if (lstResult.size() > 0 && lstResult!=null){
						for (Object objRecord: lstResult){
							totalDailyCircHour = 0.0;
							if(objRecord!=null)
							{
								String strSql3 = "select bds.bharunDailySummaryUid from Bharun b, BharunDailySummary bds, Daily d where (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
								"and b.operationUid=:operationUid and b.bharunUid=:bharunUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid and " +
								"d.dayDate <=:currentRecordDate order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
								
								List <Object> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3,
										new String[] {"operationUid", "currentRecordDate", "bharunUid"},
										new Object[] {operationUid, objRecord, bharunUid}, qp);
								
								if(lstResult2.size() >0 && lstResult2!=null)
								{
									for (Object record:lstResult2)
									{
										String strSql = "From DownholeTools where (isDeleted = false or isDeleted is null) " +
										"and bharunDailySummaryUid=:bharunDailySummaryUid and tool=:tool and (toolSerialNumber is null or toolSerialNumber='')";
										
										List <DownholeTools> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
												new String[] {"bharunDailySummaryUid","tool"},
												new Object[] {record.toString(),tool}, qp);
										
										if(lstResult3!=null && lstResult3.size()>0)
										{
											for(DownholeTools downholetool:lstResult3)
											{
												if(downholetool.getDailyCircHrs()!=null)
												{
													thisConverter.setBaseValue(downholetool.getDailyCircHrs());
													totalDailyCircHour += thisConverter.getBasevalue();
													
												}
												PropertyUtils.setProperty(downholetool, "cumCircHrs", totalDailyCircHour);
												ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetool, qp);
											}
											
										}
										
									}
									
								}
								
							}
							
						}
					}
				}
			}
			else
			{
				String strSql2 = "Select distinct(d.dayDate) FROM BharunDailySummary bds,Daily d, Bharun b where b.bharunUid=:bharunUid and b.bharunUid=bds.bharunUid and (b.isDeleted=false or b.isDeleted is null) and (d.isDeleted=false or d.isDeleted is null) and (bds.isDeleted=false or bds.isDeleted is null) and bds.dailyUid=d.dailyUid and d.operationUid=:operationUid and d.dayDate>=:dayDate order by d.dayDate";
				List <Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[] {"operationUid","dayDate","bharunUid"}, new Object[] {operationUid,currDaily.getDayDate(),bharunUid});
				if (lstResult.size() > 0 && lstResult!=null){
					for (Object objRecord: lstResult){
						totalDailyCircHour = 0.0;
						if(objRecord!=null)
						{
							String strSql3 = "select bds.bharunDailySummaryUid from Bharun b, BharunDailySummary bds, Daily d where (b.isDeleted = false or b.isDeleted is null) and (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
							"and b.operationUid=:operationUid and b.bharunUid=:bharunUid and bds.dailyUid=d.dailyUid and b.bharunUid=bds.bharunUid and " +
							"d.dayDate <=:currentRecordDate order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
							
							List <Object> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3,
									new String[] {"operationUid", "currentRecordDate", "bharunUid"},
									new Object[] {operationUid, objRecord, bharunUid}, qp);
							
							if(lstResult2.size() >0 && lstResult2!=null)
							{
								for (Object record:lstResult2)
								{
									String strSql = "From DownholeTools where (isDeleted = false or isDeleted is null) " +
									"and bharunDailySummaryUid=:bharunDailySummaryUid and tool=:tool and tool is not null and tool!=''";
									
									List <DownholeTools> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
											new String[] {"bharunDailySummaryUid","tool"},
											new Object[] {record.toString(),tool}, qp);
									
									if(lstResult3!=null && lstResult3.size()>0)
									{
										for(DownholeTools downholetool:lstResult3)
										{
											if(downholetool.getDailyCircHrs()!=null)
											{
												thisConverter.setBaseValue(downholetool.getDailyCircHrs());
												totalDailyCircHour += thisConverter.getBasevalue();
												
											}
											PropertyUtils.setProperty(downholetool, "cumCircHrs", totalDailyCircHour);
											ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetool, qp);
										}
										
									}
									
								}
								
							}
							
						}
						
					}
				}
			}
		}
		else
		{
			String strSql = "From DownholeTools where (isDeleted = false or isDeleted is null) " +
			"and downholeToolsUid=:downholeToolsUid";
			
			List <DownholeTools> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
					new String[] {"downholeToolsUid"},
					new Object[] {downholeToolsUid}, qp);
			
			if(lstResult3!=null && lstResult3.size()>0)
			{
				Double cumDailyCircHour = null;
				
				for(DownholeTools downholetool:lstResult3)
				{
					PropertyUtils.setProperty(downholetool, "cumCircHrs", cumDailyCircHour);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetool, qp);
				}
				
				
			}
		}
	}
	
	/**
	 * To calculate %HHP at bit value only then pass to savePercentHHPb to save the value
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter 
	 * @return Double
	 * @throws Exception
	 */
	private static Double calculateJetNozzlePressureLoss(String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get average flow rate from drilling parameters
		String strSql = "SELECT AVG(a.flowAvg) as avgFlow FROM DrillingParameters a, ReportDaily r WHERE (a.isDeleted = false or a.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.dailyUid=r.dailyUid AND a.depthTopMdMsl IN (SELECT max(a.depthTopMdMsl) FROM DrillingParameters a, ReportDaily r WHERE (a.isDeleted = false or a.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND r.depthMdMsl>=a.depthTopMdMsl AND r.dailyUid=a.dailyUid)";
		String paramsFields[] = {"BharunUid", "DailyUid"};
		String paramsValues[] = {BharunUid, DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double flowAvg = 0.00;
		
		if (lstResult.size() > 0){
			Object flowResult = (Object) lstResult.get(0);
			
			if (flowResult != null){
				flowAvg = Double.parseDouble(flowResult.toString());
				
				//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
				//flowAvg = flowAvg / 0.0000630902;		
				//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
				thisConverter.setReferenceMappingField(DrillingParameters.class, "flowAvg");
				thisConverter.setBaseValue(flowAvg);
				thisConverter.changeUOMUnit("USGallonsPerMinute");
				flowAvg = thisConverter.getConvertedValue();
				
			}
			else{
				systemMessage.addWarning("Pb Calculation: Average Flow (Drilling Parameter) value empty or Midnight Depth (Daily) less than Start Depth (Drilling Parameter) -> Pb = 0.0");
				return 0.0;
			}
		}
		else{
			systemMessage.addWarning("Pb Calculation: Average Flow (Drilling Parameter) value empty or Midnight Depth (Daily) less than Start Depth (Drilling Parameter) -> Pb = 0.0");
			return 0.0;
		}
		
		//get MW 
		strSql = "SELECT AVG(m.mudWeight) FROM MudProperties m, ReportDaily r WHERE (m.isDeleted = false or m.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND m.dailyUid =:DailyUid AND m.dailyUid=r.dailyUid AND m.depthMdMsl IN (SELECT max(m.depthMdMsl) FROM MudProperties m, ReportDaily r WHERE (m.isDeleted = false or m.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND m.dailyUid = :DailyUid AND m.depthMdMsl<=r.depthMdMsl AND m.dailyUid=r.dailyUid)";
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "DailyUid", DailyUid, qp);
		
		Double mudWeight = 0.00;
		
		if (lstResult.size() > 0){
			Object thisMWResult = (Object) lstResult.get(0);
			
			if (thisMWResult != null){
				mudWeight = Double.parseDouble(thisMWResult.toString());
				
				//convert to PoundsMassPerUKGallon for calculation the IDS base is KilogramsPerCubicMetre		
				//mudWeight = mudWeight / 99.77633;
				//convert to PoundsMassPerUSGallon for calculation the IDS base is KilogramsPerCubicMetre
				thisConverter.setReferenceMappingField(MudProperties.class, "mudWeight");
				thisConverter.setBaseValue(mudWeight);
				thisConverter.changeUOMUnit("PoundsMassPerUSGallon");			
				mudWeight = thisConverter.getConvertedValue();
			}
			else{
				systemMessage.addWarning("Pb Calculation: Mud Weight (Mud Properties) value empty or Midnight Depth (Daily) less than Check Depth (Mud Properties) -> Pb = 0.0");
				return 0.0;
			}
		}
		else{
			systemMessage.addWarning("Pb Calculation: Mud Weight (Mud Properties) value empty or Midnight Depth (Daily) less than Check Depth (Mud Properties) -> Pb = 0.0");
			return 0.0;
		}
		
		//get bitrunUid
		strSql = "SELECT bitrunUid FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
		
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", BharunUid, qp);
		
		if (lstResult.size()<=0){
			systemMessage.addWarning("Pb Calculation: No Bit run associated to this BHA -> Pb = 0.0");
			return 0.0;
		}
		
		Object thisBitResult = (Object) lstResult.get(0);
		//get bitrunUid for acquire bit nozzle
		String bitrunUid = thisBitResult.toString();
		
		//get bit nozzle
		strSql = "SELECT n.nozzleSize, n.nozzleQty " +
					"FROM BitNozzle n, Bitrun b " +
					"WHERE (n.isDeleted = false or n.isDeleted is null) " +
					"AND n.bitrunUid=b.bitrunUid " +
					"and (b.isDeleted = false or b.isDeleted is null) " +
					"and n.bitrunUid =:bitrunUid " +
					"AND b.depthInMdMsl IN " +
					"(SELECT MAX(b.depthInMdMsl) " +
					"FROM BitNozzle n, Bitrun b,ReportDaily r, BharunDailySummary bds " +
					"WHERE (n.isDeleted = false or n.isDeleted is null) " +
					"AND n.bitrunUid=b.bitrunUid " +
					"and (b.isDeleted = false or b.isDeleted is null) " +
					"AND r.dailyUid=bds.dailyUid " +
					"and b.bharunUid=bds.bharunUid " +
					"and n.bitrunUid = :bitrunUid " +
					"AND r.depthMdMsl>=b.depthInMdMsl)";
		List<Object[]> lstNozzleResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bitrunUid", bitrunUid, qp);
		Double nozzleSize = 0.00;
		Integer nozzleQty = 0;
		Double nozzleCalc = 0.00;
		Double nozzleTotal = 0.00;
		
		if (lstNozzleResult.size() > 0){
			for(Object[] thisNozzleResult: lstNozzleResult){
				if (thisNozzleResult != null){
					if (thisNozzleResult[0] != null) nozzleSize = Double.parseDouble(thisNozzleResult[0].toString());
					if (thisNozzleResult[1] != null) nozzleQty = Integer.parseInt(thisNozzleResult[1].toString());
				} else {
					systemMessage.addWarning("Pb Calculation: Bit Nozzle value empty or Midnight Depth (Daily) less than Depth In (bitrun) -> Pb = 0.0");
					return 0.0;
				}
				// need to convert to 32nd of inch for calculation the base is m 
				//nozzleSize = nozzleSize / 0.0007937;
				thisConverter.setReferenceMappingField(BitNozzle.class, "nozzleSize");
				thisConverter.setBaseValue(nozzleSize);
				thisConverter.changeUOMUnit("32ndOfAnInch");			
				nozzleSize = thisConverter.getConvertedValue();
				
				nozzleCalc = Math.pow(nozzleSize, 2) * nozzleQty;
				nozzleTotal = nozzleTotal + nozzleCalc;
			}
		}
		else {
			systemMessage.addWarning("Pb Calculation: Bit Nozzle value empty or Midnight Depth (Daily) less than Depth In (bitrun) -> Pb = 0.0");
			return 0.0;
		}
		
		
		//calculate Pb
		Double pbCalc = BharunCalculation.calculatePb(flowAvg, mudWeight, nozzleTotal);
		if (pbCalc == 0.00){
			return 0.0;
		}
		
	    return pbCalc;
	}
	
	/**
	 * To save Pb at bit value  
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage
	 * @return void
	 * @throws Exception
	 */
	public static void saveJetNozzlePressureLoss (CustomFieldUom thisConverter, String BharunUid, String DailyUid, SystemMessage systemMessage, String groupUid, String WellboreUid, String OpeartionUid) throws Exception  
	{
		Double pressureLoss = BharunUtils.calculateJetNozzlePressureLoss(BharunUid, DailyUid, systemMessage,thisConverter);
		
		//get bharun daily summary to update 
		String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid AND dailyUid=:DailyUid";		
		String[] paramsFields = {"BharunUid", "DailyUid"};
		String[] paramsValues = {BharunUid, DailyUid};
		List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult1.size() > 0){			
			BharunDailySummary thisBhaDaily = (BharunDailySummary) lstResult1.get(0);
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "nozzlePressureLoss");
			thisConverter.changeUOMUnit("PoundsPerSquareInch");
			thisConverter.setBaseValueFromUserValue(pressureLoss);
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "nozzlePressureLoss");
			thisBhaDaily.setNozzlePressureLoss(thisConverter.getConvertedValue());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDaily);
		}	
		if ("1".equals(GroupWidePreference.getValue(groupUid, GroupWidePreference.GWP_AUTO_CALCULATE_BIT_HYDRAULIC_HORSE_POWER))) {
			BharunUtils.saveBitHydraulicHorsepower(thisConverter, BharunUid, DailyUid, systemMessage);
		}
		if ("1".equals(GroupWidePreference.getValue(groupUid, GroupWidePreference.GWP_AUTO_CALCULATE_BIT_PRESSURE_LOSS_PERCENTAGE))) {
			BharunUtils.saveBitPressureLossPercentage(thisConverter, BharunUid, DailyUid, systemMessage, WellboreUid);
		}
	}
	
	private static Double calculateSurfacePressure(String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter, String WellboreUid) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get estimated mud weight from leak off test
		String strSql = "SELECT AVG(o.estimatedMudWeight) FROM LeakOffTest o, ReportDaily r WHERE (o.isDeleted = false or o.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND o.dailyUid = :DailyUid AND o.dailyUid=r.dailyUid And o.holeDepthMdMsl IN (SELECT max(o.holeDepthMdMsl) FROM LeakOffTest o, ReportDaily r WHERE (o.isDeleted = false or o.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND o.dailyUid = :DailyUid And o.holeDepthMdMsl<=r.depthMdMsl AND o.dailyUid=r.dailyUid)";
		String paramsFields[] = {"DailyUid"};
		String paramsValues[] = {DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		Double estimatedMudWeight = 0.00;
		if(lstResult != null && lstResult.size() > 0){
			Object estimatedMudWeightResult = (Object) lstResult.get(0);
			
			if (estimatedMudWeightResult != null){
				estimatedMudWeight = Double.parseDouble(estimatedMudWeightResult.toString());
				
				thisConverter.setReferenceMappingField(LeakOffTest.class, "estimatedMudWeight");
				thisConverter.setBaseValue(estimatedMudWeight);
				thisConverter.changeUOMUnit("PoundsMassPerUSGallon");
				estimatedMudWeight = thisConverter.getConvertedValue();
			}
			else{
				systemMessage.addWarning("Surface Pressure Calculation: Estimated Mud Weight (Leak Off test) value empty or Midnight Depth (Daily) less than Hole Depth (LOT) -> Surface Pressure = 0.0");
				return 0.0;
			}
		}
		else{
			systemMessage.addWarning("Surface Pressure Calculation: Estimated Mud Weight (Leak Off test) value empty or Midnight Depth (Daily) less than Hole Depth (LOT) -> Surface Pressure = 0.0");
			return 0.0;
		}
		
		//get mud weight from mud properties 
		strSql = "SELECT AVG(m.mudWeight) FROM MudProperties m, ReportDaily r WHERE (m.isDeleted = false or m.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND m.dailyUid =:DailyUid AND m.dailyUid=r.dailyUid AND m.depthMdMsl IN (SELECT max(m.depthMdMsl) FROM MudProperties m, ReportDaily r WHERE (m.isDeleted = false or m.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND m.dailyUid = :DailyUid AND m.depthMdMsl<=r.depthMdMsl AND m.dailyUid=r.dailyUid)";
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "DailyUid", DailyUid, qp);
		Double mudWeight = 0.00;
		if(lstResult != null && lstResult.size() > 0){
			Object thisMWResult = (Object) lstResult.get(0);
			
			if (thisMWResult != null){
				mudWeight = Double.parseDouble(thisMWResult.toString());
				
				//convert to PoundsMassPerUKGallon for calculation the IDS base is KilogramsPerCubicMetre		
				//mudWeight = mudWeight / 99.77633;
				//convert to PoundsMassPerUSGallon for calculation the IDS base is KilogramsPerCubicMetre
				thisConverter.setReferenceMappingField(MudProperties.class, "mudWeight");
				thisConverter.setBaseValue(mudWeight);
				thisConverter.changeUOMUnit("PoundsMassPerUSGallon");			
				mudWeight = thisConverter.getConvertedValue();
			}
			else{
				systemMessage.addWarning("Surface Pressure Calculation: Mud Weight (Mud Properties) value empty or Midnight Depth (Daily) less than Check Depth (Mud Poperties) -> Surface Pressure = 0.0");
				return 0.0;
			}
		}
		else{
			systemMessage.addWarning("Surface Pressure Calculation: Mud Weight (Mud Properties) value empty or Midnight Depth (Daily) less than Check Depth (Mud Poperties) -> Surface Pressure = 0.0");
			return 0.0;
		}
		
		//get depth_top_md_msl from casing section 
		strSql = "SELECT c.shoeTopTvdMsl FROM CasingSection c, ReportDaily r " +
				"WHERE (c.isDeleted = false or c.isDeleted is null) " +
				"AND (r.isDeleted = false or r.isDeleted is null) " +
				"AND c.wellboreUid = r.wellboreUid " +
				"AND c.wellboreUid = :WellboreUid " +
				"AND r.dailyUid = :DailyUid " +
				"AND c.installStartDate <= r.reportDatetime";	
		
		String paramsFields2[] = {"WellboreUid", "DailyUid"};
		String paramsValues2[] = {WellboreUid, DailyUid};
		List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
		
		Double shoeTopTvdMsl = 0.00;
		if(lstResult2 != null && lstResult2.size() > 0){
			Object thisShoeTopTvdMslResult = null;
			
			if(lstResult2.size() > 1){
				strSql = "SELECT MAX(c.shoeTopTvdMsl) FROM CasingSection c, ReportDaily r " +
							"WHERE (c.isDeleted = false or c.isDeleted is null) " +
							"AND (r.isDeleted = false or r.isDeleted is null) " +
							"AND r.dailyUid = :DailyUid " +
							"AND c.wellboreUid = r.wellboreUid " +
							"AND c.wellboreUid = :WellboreUid " +
							"AND c.shoeTopTvdMsl <= r.depthMdMsl " +
							"AND c.installStartDate <= r.reportDatetime";
				
				String paramsFields3[] = {"WellboreUid", "DailyUid"};
				String paramsValues3[] = {WellboreUid, DailyUid};
				
				List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields3, paramsValues3, qp);
				
				if(lstResult3 != null && lstResult3.size() > 0){
					thisShoeTopTvdMslResult = (Object) lstResult3.get(0);
				}
			}
			else{
				thisShoeTopTvdMslResult = (Object) lstResult2.get(0);
			}
			
			if (thisShoeTopTvdMslResult != null){
				shoeTopTvdMsl = Double.parseDouble(thisShoeTopTvdMslResult.toString());
				
				//convert to PoundsMassPerUKGallon for calculation the IDS base is KilogramsPerCubicMetre		
				//mudWeight = mudWeight / 99.77633;
				//convert to PoundsMassPerUSGallon for calculation the IDS base is KilogramsPerCubicMetre
				thisConverter.setReferenceMappingField(CasingSection.class, "shoeTopTvdMsl");
				thisConverter.setBaseValue(shoeTopTvdMsl);
				thisConverter.changeUOMUnit("Feet");			
				shoeTopTvdMsl = thisConverter.getConvertedValue();
			}
			else{
				systemMessage.addWarning("Surface Pressure Calculation: Report date (Daily) less than Installation Date (Casing Section) or Shoe Depth (Casing Section) value is empty or Midnight Depth (Daily) less than Shoe Depth (Casing Section) -> Surface Pressure = 0.0");
				return 0.0;
			}
		}
		else{
			systemMessage.addWarning("Surface Pressure Calculation: Report date (Daily) less than Installation Date (Casing Section) -> Surface Pressure = 0.0");
			return 0.0;
		}
		
		//calculate surface pressure
		Double surfacePresureCalc = BharunCalculation.calculateSurfacePressure(estimatedMudWeight, mudWeight, shoeTopTvdMsl);
		
		return surfacePresureCalc;
	}
	
	private static Double calculateSysHydraulicHorsepower(String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter, String WellboreUid) throws Exception    
	{
		Double surfacePressure = BharunUtils.calculateSurfacePressure(DailyUid, systemMessage,thisConverter, WellboreUid);
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get bharun daily summary for this day to do calc
		String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :BharunUid AND dailyUid=:DailyUid";		
		String[] paramsFields = {"BharunUid","DailyUid"};
		String[] paramsValues = {BharunUid, DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		Double flowAvg = 0.00;
		
		if (lstResult.size() > 0){
			for(Object objBhaDaily: lstResult){
				//get average flow rate from drilling parameters
				String strSql2 = "SELECT AVG(a.flowAvg) as avgFlow FROM DrillingParameters a, ReportDaily r WHERE (a.isDeleted = false or a.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid and a.dailyUid=r.dailyUid AND a.depthTopMdMsl IN (SELECT max(a.depthTopMdMsl) FROM DrillingParameters a, ReportDaily r WHERE (a.isDeleted = false or a.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND r.depthMdMsl>=a.depthTopMdMsl AND a.dailyUid=r.dailyUid)";
				String paramsFields2[] = {"BharunUid", "DailyUid"};
				String paramsValues2[] = {BharunUid, DailyUid};
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
				
				if(lstResult2 != null && lstResult2.size() > 0){
					Object flowResult = (Object) lstResult2.get(0);
					
					if (flowResult != null){
						flowAvg = Double.parseDouble(flowResult.toString());
						
						//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
						//flowAvg = flowAvg / 0.0000630902;		
						//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
						thisConverter.setReferenceMappingField(DrillingParameters.class, "flowAvg");
						thisConverter.setBaseValue(flowAvg);
						thisConverter.changeUOMUnit("USGallonsPerMinute");
						flowAvg = thisConverter.getConvertedValue();
					}
					else{
						systemMessage.addWarning("SysHHP Calculation: Average Flow (Drilling Parameter) value empty or Midnight Depth (Daily) less than Start Depth (Drilling Parameter) -> SysHHP = 0.0");
						return 0.0;
					}
				}
				else{
					systemMessage.addWarning("SysHHP Calculation: Average Flow (Drilling Parameter) value empty or Midnight Depth (Daily) less than Start Depth (Drilling Parameter) -> SysHHP = 0.0");
					return 0.0;
				}
			}
		}
		
		Double sysHHP = 0.00;
		
		// Calculate sysHHP
		sysHHP = (surfacePressure * flowAvg) / 1714;
		
		return sysHHP;
	}
	
	/**
	 * To save %sysHHP at bit value  
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage
	 * @return void
	 * @throws Exception
	 */
	public static void saveSysHydraulicHorsepower (CustomFieldUom thisConverter, String BharunUid, String DailyUid, SystemMessage systemMessage, String WellboreUid) throws Exception  {
		thisConverter.setReferenceMappingField(BharunDailySummary.class, "systemHydraulicHp");
		
		Double SysHydraulicHorsepower = BharunUtils.calculateSysHydraulicHorsepower(BharunUid, DailyUid, systemMessage,thisConverter, WellboreUid);
		
		//get bharun daily summary to update 
		String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :BharunUid AND dailyUid=:DailyUid";		
		String[] paramsFields = {"BharunUid","DailyUid"};
		String[] paramsValues = {BharunUid, DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
		if (lstResult.size() > 0){			
			BharunDailySummary thisBhaDaily = (BharunDailySummary) lstResult.get(0);
			thisConverter.setBaseValueFromUserValue(SysHydraulicHorsepower);
			thisBhaDaily.setSystemHydraulicHp(thisConverter.getConvertedValue());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDaily);
		}
	}
	
	private static Double calculateBitHydraulicHorsepower(String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get average flow rate from drilling parameters
		String strSql = "SELECT AVG(a.flowAvg) as avgFlow FROM DrillingParameters a, ReportDaily r WHERE (a.isDeleted = false or a.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.dailyUid=r.dailyUid AND a.depthTopMdMsl IN (SELECT max(a.depthTopMdMsl) FROM DrillingParameters a, ReportDaily r WHERE (a.isDeleted = false or a.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND r.depthMdMsl>=a.depthTopMdMsl AND a.dailyUid=r.dailyUid)";
		String paramsFields[] = {"BharunUid", "DailyUid"};
		String paramsValues[] = {BharunUid, DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		Double flowAvg = 0.0;	
		if(lstResult != null && lstResult.size() > 0){
			Object flowResult = (Object) lstResult.get(0);
					
			if (flowResult != null){
				flowAvg = Double.parseDouble(flowResult.toString());
						
				//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
				//flowAvg = flowAvg / 0.0000630902;		
				//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
				thisConverter.setReferenceMappingField(DrillingParameters.class, "flowAvg");
				thisConverter.setBaseValue(flowAvg);
				thisConverter.changeUOMUnit("USGallonsPerMinute");
				flowAvg = thisConverter.getConvertedValue();
			}
			else{
				systemMessage.addWarning("HHPb Calculation: Average Flow (Drilling Parameter) value empty or Midnight Depth (Daily) less than Start Depth (Drilling Parameter) -> HHPb = 0.0");
				return 0.0;
			}
		}
		else{
			systemMessage.addWarning("HHPb Calculation: Average Flow (Drilling Parameter) value empty or Midnight Depth (Daily) less than Start Depth (Drilling Parameter) -> HHPb = 0.0");
			return 0.0;
		}
		
		//get average flow rate from drilling parameters
		String strSql2 = "select nozzlePressureLoss from BharunDailySummary where bharunUid=:BharunUid and dailyUid=:DailyUid";
		String paramsFields2[] = {"BharunUid", "DailyUid"};
		String paramsValues2[] = {BharunUid, DailyUid};
		List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
		Double pb = 0.0;	
		if(lstResult2 != null && lstResult2.size() > 0){
			Object pbResult = (Object) lstResult2.get(0);
					
			if (pbResult != null){
				pb = Double.parseDouble(pbResult.toString());
						
				//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
				//flowAvg = flowAvg / 0.0000630902;		
				//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "nozzlePressureLoss");
				thisConverter.setBaseValue(pb);
				thisConverter.changeUOMUnit("PoundsPerSquareInch");
				pb = thisConverter.getConvertedValue();
			}
			else{
				systemMessage.addWarning("HHPb Calculation: Pb (BHA) value empty -> HHPb = 0.0");
				return 0.0;
			}
		}
		else{
			systemMessage.addWarning("HHPb Calculation: Pb (BHA) value empty -> HHPb = 0.0");
			return 0.0;
		}
		
		Double hhpb = 0.00;
		
		// Calculate sysHHP
		hhpb = (flowAvg * pb) / 1714;
		return hhpb;
	}
	
	/**
	 * To save %HHPb at bit value  
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage
	 * @return void
	 * @throws Exception
	 */
	public static void saveBitHydraulicHorsepower (CustomFieldUom thisConverter, String BharunUid, String DailyUid, SystemMessage systemMessage) throws Exception  
	{
		Double BitHydraulicHorsepower = BharunUtils.calculateBitHydraulicHorsepower(BharunUid, DailyUid, systemMessage,thisConverter);
		
		//get bharun daily summary to update 
		String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid AND dailyUid=:DailyUid";		
		String[] paramsFields = {"BharunUid", "DailyUid"};
		String[] paramsValues = {BharunUid, DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){			
			BharunDailySummary thisBhaDaily = (BharunDailySummary) lstResult.get(0);
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "bitHydraulicHp");
			thisConverter.changeUOMUnit("HydraulicHorsePower"); 
			thisConverter.setBaseValueFromUserValue(BitHydraulicHorsepower);
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "bitHydraulicHp");
			thisBhaDaily.setBitHydraulicHp(thisConverter.getConvertedValue());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDaily);
		}	
	}
	
	private static Double calculateBitPressureLossPercentage(String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter, String WellboreUid) throws Exception    
	{
		Double surfacePressure = BharunUtils.calculateSurfacePressure(DailyUid, systemMessage,thisConverter, WellboreUid);
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get bharun daily summary for this day to do calc
		String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :BharunUid AND dailyUid=:DailyUid";		
		String[] paramsFields = {"BharunUid","DailyUid"};
		String[] paramsValues = {BharunUid,DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		Double pb = 0.00;
		
		if (lstResult.size() > 0){
			//get average flow rate from drilling parameters
			String strSql2 = "select nozzlePressureLoss from BharunDailySummary where bharunUid=:BharunUid and dailyUid=:DailyUid";
			String paramsFields2[] = {"BharunUid", "DailyUid"};
			String paramsValues2[] = {BharunUid, DailyUid};
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
					
			if(lstResult2 != null && lstResult2.size() > 0){
				Object pbResult = (Object) lstResult2.get(0);
							
				if (pbResult != null){
					pb = Double.parseDouble(pbResult.toString());
								
					//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
					//flowAvg = flowAvg / 0.0000630902;		
					//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
					thisConverter.setReferenceMappingField(BharunDailySummary.class, "nozzlePressureLoss");
					thisConverter.setBaseValue(pb);
					thisConverter.changeUOMUnit("PoundsPerSquareInch");
					pb = thisConverter.getConvertedValue();
				}
				else{
						systemMessage.addWarning("%psib Calculation: Pb (BHA) value empty -> %psib = 0.0");
						return 0.0;
					}
				}
			else{
				systemMessage.addWarning("%psib Calculation: Pb (BHA) value empty -> %psib = 0.0");
				return 0.0;
			}
		}
		
		Double psib = 0.00;
		
		// Calculate psib
		if(surfacePressure != 0){
			psib = (pb / surfacePressure) * 100;
		}
		
		return psib;
	}
	
	/**
	 * To save %psib at bit value  
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage
	 * @return void
	 * @throws Exception
	 */
	public static void saveBitPressureLossPercentage (CustomFieldUom thisConverter, String BharunUid, String DailyUid, SystemMessage systemMessage, String WellboreUid) throws Exception  {
		thisConverter.setReferenceMappingField(BharunDailySummary.class, "bitPressureLoss");
		
		Double bitPressureLossPercentage = BharunUtils.calculateBitPressureLossPercentage(BharunUid, DailyUid, systemMessage, thisConverter, WellboreUid);
				
		//get bharun daily summary to update 
		String strSql2 = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND bharunUid=:BharunUid AND dailyUid=:DailyUid";		
		String paramsFields2[] = {"BharunUid", "DailyUid"};
		String paramsValues2[] = {BharunUid, DailyUid};
		List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				
		if (lstResult2.size() > 0){			
			BharunDailySummary thisBhaDaily = (BharunDailySummary) lstResult2.get(0);
			thisConverter.setBaseValueFromUserValue(bitPressureLossPercentage);
			thisBhaDaily.setBitPressureLoss(thisConverter.getConvertedValue());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDaily);
		}
	}
	
	/**
	 * To calculate bottom hole force (impact force) then pass to saveBottomHoleforce to save the value
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter 
	 * @return Double
	 * @throws Exception
	 */
	private static Double calculateNewImpactForce(String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get average flow rate from drilling parameters
		String strSql = "SELECT AVG(a.flowAvg) as avgFlow FROM DrillingParameters a, ReportDaily r WHERE (a.isDeleted = false or a.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.dailyUid=r.dailyUid AND a.depthTopMdMsl IN (SELECT max(a.depthTopMdMsl) FROM DrillingParameters a, ReportDaily r WHERE (a.isDeleted = false or a.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND r.depthMdMsl>=a.depthTopMdMsl AND a.dailyUid=r.dailyUid)";
		String paramsFields[] = {"BharunUid", "DailyUid"};
		String paramsValues[] = {BharunUid, DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double flowAvg = 0.00;
		
		if (lstResult.size() > 0){
			Object flowResult = (Object) lstResult.get(0);
			
			if (flowResult != null){
				flowAvg = Double.parseDouble(flowResult.toString());
				
				//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
				//flowAvg = flowAvg / 0.0000630902;		
				//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
				thisConverter.setReferenceMappingField(DrillingParameters.class, "flowAvg");
				thisConverter.setBaseValue(flowAvg);
				thisConverter.changeUOMUnit("USGallonsPerMinute");
				flowAvg = thisConverter.getConvertedValue();
				
			}
			else{
				systemMessage.addWarning("Impact force Calculation: Average Flow (Drilling Parameter) value empty or Midnight Depth (Daily) less than Start Depth (Drilling Parameter) -> Impact force = 0.0");
				return 0.0;
			}
		}
		else{
			systemMessage.addWarning("Impact force Calculation: Average Flow (Drilling Parameter) value empty or Midnight Depth (Daily) less than Start Depth (Drilling Parameter) -> Impact force = 0.0");
			return 0.0;
		}
		
		//get MW 
		strSql = "SELECT AVG(m.mudWeight) FROM MudProperties m, ReportDaily r WHERE (m.isDeleted = false or m.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND m.dailyUid =:DailyUid AND m.dailyUid=r.dailyUid AND m.depthMdMsl In (SELECT max(m.depthMdMsl) FROM MudProperties m, ReportDaily r WHERE (m.isDeleted = false or m.isDeleted is null) AND (r.isDeleted = false or r.isDeleted is null) AND m.dailyUid = :DailyUid AND m.depthMdMsl<=r.depthMdMsl AND m.dailyUid=r.dailyUid)";
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "DailyUid", DailyUid, qp);
		
		Double mudWeight = 0.00;
		
		if (lstResult.size() > 0){
			Object thisMWResult = (Object) lstResult.get(0);
			
			if (thisMWResult != null){
				mudWeight = Double.parseDouble(thisMWResult.toString());
				
				//convert to PoundsMassPerUKGallon for calculation the IDS base is KilogramsPerCubicMetre		
				//mudWeight = mudWeight / 99.77633;
				//convert to PoundsMassPerUSGallon for calculation the IDS base is KilogramsPerCubicMetre
				thisConverter.setReferenceMappingField(MudProperties.class, "mudWeight");
				thisConverter.setBaseValue(mudWeight);
				thisConverter.changeUOMUnit("PoundsMassPerUSGallon");			
				mudWeight = thisConverter.getConvertedValue();
			}
			else{
				systemMessage.addWarning("Impact force Calculation: Mud Weight (Mud Properties) value empty or Midnight Depth (Daily) less than Check Depth (Mud properties) -> Impact force = 0.0");
				return 0.0;
			}
		}
		else{
			systemMessage.addWarning("Impact force Calculation: Mud Weight (Mud Properties) value empty or Midnight Depth (Daily) less than Check Depth (Mud properties) -> Impact force = 0.0");
			return 0.0;
		}
		
		//get bitrunUid
		strSql = "SELECT bitrunUid FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
		
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", BharunUid, qp);
		
		if (lstResult.size()<=0){
			systemMessage.addWarning("Impact force Calculation: No Bit run associated to this BHA -> Impact Force = 0.0");
			return 0.0;
		}
		
		Object thisBitResult = (Object) lstResult.get(0);
		//get bitrunUid for acquire bit nozzle
		String bitrunUid = thisBitResult.toString();
		
		//get bit nozzle
		strSql = "SELECT n.nozzleSize, n.nozzleQty " +
					"FROM BitNozzle n, Bitrun b " +
					"WHERE (n.isDeleted = false or n.isDeleted is null) " +
					"AND n.bitrunUid=b.bitrunUid " +
					"and (b.isDeleted = false or b.isDeleted is null) " +
					"and n.bitrunUid =:bitrunUid " +
					"AND b.depthInMdMsl IN " +
					"(SELECT MAX(b.depthInMdMsl) " +
					"FROM BitNozzle n, Bitrun b,ReportDaily r, BharunDailySummary bds " +
					"WHERE (n.isDeleted = false or n.isDeleted is null) " +
					"AND n.bitrunUid=b.bitrunUid " +
					"and (b.isDeleted = false or b.isDeleted is null) " +
					"AND r.dailyUid=bds.dailyUid " +
					"and b.bharunUid=bds.bharunUid " +
					"and n.bitrunUid = :bitrunUid " +
					"AND r.depthMdMsl>=b.depthInMdMsl)";
		
		List<Object[]> lstNozzleResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bitrunUid", bitrunUid, qp);
		Double nozzleSize = 0.00;
		Integer nozzleQty = 0;
		Double nozzleCalc = 0.00;
		Double nozzleTotal = 0.00;
		
		if (lstNozzleResult.size() > 0){
			for(Object[] thisNozzleResult: lstNozzleResult){                    
                if (thisNozzleResult != null){                                  
                    if (thisNozzleResult[0] != null) nozzleSize = Double.parseDouble(thisNozzleResult[0].toString());
                    if (thisNozzleResult[1] != null) nozzleQty = Integer.parseInt(thisNozzleResult[1].toString());
                } else { 
					systemMessage.addWarning("Impact Force Calculation: Bit Nozzle (Bit) value empty or Midnight Depth (Daily) less than Depth in (Bit) -> Impact Force = 0.0");
					return 0.0;
				}
				// need to convert to 32nd of inch for calculation the base is m 
				//nozzleSize = nozzleSize / 0.0007937;
				thisConverter.setReferenceMappingField(BitNozzle.class, "nozzleSize");
				thisConverter.setBaseValue(nozzleSize);
				thisConverter.changeUOMUnit("32ndOfAnInch");			
				nozzleSize = thisConverter.getConvertedValue();
				
				nozzleCalc = Math.pow(nozzleSize, 2) * nozzleQty;
				nozzleTotal = nozzleTotal + nozzleCalc;
			}
		}
		else {
			systemMessage.addWarning("Impact Force Calculation: Bit Nozzle (Bit) value empty or Midnight Depth (Daily) less than Depth in (Bit) -> Impact Force = 0.0");
			return 0.0;
		}
		
		
		//calculate jet velocity
		Double jetVelocity = 0.00;
		
		if(nozzleTotal != 0){
			jetVelocity = (417.2 * flowAvg) / (nozzleTotal);
		}
		
		Double impactForce = BharunCalculation.calculateNewImpactForce(mudWeight, jetVelocity, flowAvg);
		if (impactForce == 0.00){
			return 0.0;
		}
		
	    return impactForce;
	}
	
	/**
	 * To save bottom hole force (impact force) at bit value  
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage
	 * @return void
	 * @throws Exception
	 */
	public static void saveBitImpactForce (CustomFieldUom thisConverter, String BharunUid, String DailyUid, SystemMessage systemMessage, String GroupUid, String WellboreUid) throws Exception  {
		thisConverter.setReferenceMappingField(BharunDailySummary.class, "bitImpactForce");
		
		Double newImpactForceCalc = BharunUtils.calculateNewImpactForce(BharunUid, DailyUid, systemMessage, thisConverter);
		
		//get bharun daily summary to update 
		String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid AND dailyUid=:DailyUid";		
		String[] paramsFields = {"BharunUid", "DailyUid"};
		String[] paramsValues = {BharunUid, DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){			
			BharunDailySummary thisBhaDaily = (BharunDailySummary) lstResult.get(0);
			thisConverter.setBaseValueFromUserValue(newImpactForceCalc);
			thisBhaDaily.setBitImpactForce(thisConverter.getConvertedValue());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDaily);
		}
		
		if ("1".equals(GroupWidePreference.getValue(GroupUid, GroupWidePreference.GWP_AUTO_CALCULATE_BIT_IMPACT_FORCE))) {
			BharunUtils.saveImpactForcePerBitArea(thisConverter, DailyUid, systemMessage, BharunUid);
		}
	}
	
	private static Double calculateImpactForcePerBitArea(String BharunUid, String DailyUid, SystemMessage systemMessage, CustomFieldUom thisConverter) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double bitImpactForce = 0.00;

		String strSql2 = "select bitImpactForce from BharunDailySummary where bharunUid=:BharunUid and dailyUid=:DailyUid";
		String paramsFields2[] = {"BharunUid", "DailyUid"};
		String paramsValues2[] = {BharunUid, DailyUid};
		List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
				
		if(lstResult2 != null && lstResult2.size() > 0){
			Object pbResult = (Object) lstResult2.get(0);
					
			if (pbResult != null){
				bitImpactForce = Double.parseDouble(pbResult.toString());
						
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "bitImpactForce");
				thisConverter.setBaseValue(bitImpactForce);
				thisConverter.changeUOMUnit("PoundsForce");
				bitImpactForce = thisConverter.getConvertedValue();
			}
			else{
				systemMessage.addWarning("IF/Area Calculation: Impact force (BHA) value empty -> IF/Area = 0.0");
				return 0.0;
			}
		}
		else{
			systemMessage.addWarning("IF/Area Calculation: Impact force (BHA) value empty -> IF/Area = 0.0");
			return 0.0;
		}
		
		//get bitrunUid
		String strSql = "SELECT bitDiameter FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", BharunUid, qp);
		
		if (lstResult != null && lstResult.size() > 0){
			Object thisBitResult = (Object) lstResult.get(0);
			Double bitDiameter = 0.0;
			
			if (thisBitResult != null){
				bitDiameter = Double.parseDouble(thisBitResult.toString());
						
				thisConverter.setReferenceMappingField(Bitrun.class, "bitDiameter");
				thisConverter.setBaseValue(bitDiameter);
				thisConverter.changeUOMUnit("Inch");	
				bitDiameter = thisConverter.getConvertedValue();
			}
			else
			{
				systemMessage.addWarning("IF/Area Calculation: Bit Diameter value empty -> IF/Area = 0.0");
				return 0.0;
			}
			
			Double ifPerArea = (bitImpactForce * 1.27) / (bitDiameter * bitDiameter);

			return ifPerArea;
		}
		else
		{
			systemMessage.addWarning("IF/Area Calculation: Bit Diameter value empty -> IF/Area = 0.0");
			return 0.0;
		}
	}
	
	/**
	 * To save impact force per bit area at bit value  
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage
	 * @return void
	 * @throws Exception
	 */
	public static void saveImpactForcePerBitArea (CustomFieldUom thisConverter, String DailyUid, SystemMessage systemMessage, String BharunUid) throws Exception  {
		thisConverter.setReferenceMappingField(BharunDailySummary.class, "bitImpactForceArea");
		
		Double impactForcePerBitAreaCalc = BharunUtils.calculateImpactForcePerBitArea(BharunUid, DailyUid, systemMessage, thisConverter);
		
		//get bharun daily summary to update 
		String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null)AND bharunUid = :BharunUid AND dailyUid=:DailyUid";		
		String[] paramsFields = {"BharunUid","DailyUid"};
		String[] paramsValues = {BharunUid,DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){			
			BharunDailySummary thisBhaDaily = (BharunDailySummary) lstResult.get(0);
			thisConverter.setBaseValueFromUserValue(thisConverter.getConvertedValue());
			thisBhaDaily.setBitImpactForceArea(impactForcePerBitAreaCalc);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDaily);
		}
	}
	
	/**
	 * To calculate HSI value only then pass to saveHSI to save the value
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage
	 * @return Double
	 * @throws Exception
	 */
	public static Double calculateHSI(String BharunUid, String DailyUid, SystemMessage systemMessage) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get average flow rate from drilling parameters
		String strSql = "SELECT AVG(a.flowAvg) as avgFlow FROM DrillingParameters a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.flowAvg > 0";
		String[] paramsFields = {"BharunUid", "DailyUid"};
		String[] paramsValues = {BharunUid, DailyUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Object flowResult = (Object) lstResult.get(0);
		Double flowAvg = 0.00;
		if (flowResult != null){
			flowAvg = Double.parseDouble(flowResult.toString());
			
			//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
			flowAvg = flowAvg / 0.0000630902;
		}else{
			systemMessage.addWarning("HSI Calculation: Average Flow (Drilling Parameter) value empty -> HSI = 0.0");
			return 0.0;
		}
		
		//get bit diameter
		strSql = "SELECT bitDiameter, bitrunUid FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
		
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", BharunUid, qp);
		
		if (lstResult.size()<=0){
			systemMessage.addWarning("HSI Calculation: No Bit run associated to this BHA -> HSI = 0.0");
			return 0.0;
		}
		
		Object[] thisBitResult = (Object[]) lstResult.get(0);
		//get bitrunUid for acquire bit nozzle
		String bitrunUid = thisBitResult[1].toString();
		
		Double bitDiameter = 0.00;
		if (thisBitResult[0] != null){
			bitDiameter = Double.parseDouble(thisBitResult[0].toString());
			
			//convert to inch for calculation the IDS base is m		
			bitDiameter = bitDiameter / 0.0254;
		}else{
			systemMessage.addWarning("HSI Calculation: Bit Diameter value empty -> HSI = 0.0");
			return 0.0;
		}
		
		//get MW 
		strSql = "SELECT AVG(mudWeight) as avgMW FROM MudProperties WHERE (isDeleted = false or isDeleted is null) and dailyUid =:DailyUid";
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "DailyUid", DailyUid, qp);
		Object thisMWResult = (Object) lstResult.get(0);
		Double mudWeight = 0.00;
		if (thisMWResult != null){
			mudWeight = Double.parseDouble(thisMWResult.toString());
			
			//convert to PoundsMassPerUSGallon for calculation the IDS base is KilogramsPerCubicMetre - 27353		
			mudWeight = mudWeight / 119.8264;
		}else{
			systemMessage.addWarning("HSI Calculation: Mud Weight (Mud Properties) value empty -> HSI = 0.0");
			return 0.0;
		}
		
		//get bit nozzle
		strSql = "SELECT nozzleQty, nozzleSize FROM BitNozzle WHERE (isDeleted = false or isDeleted is null) and bitrunUid =:bitrunUid";
		List<Object[]> lstNozzleResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bitrunUid", bitrunUid, qp);
		Double nozzleQty = 0.00;
		Double nozzleSize = 0.00;
		Double nozzleCalc = 0.00;
		Double nozzleTotal = 0.00;
		
		if (lstNozzleResult.size() > 0){
			for(Object[] thisNozzleResult: lstNozzleResult){
				if (thisNozzleResult[0] != null) nozzleQty = Double.parseDouble(thisNozzleResult[0].toString());
				if (thisNozzleResult[1] != null) nozzleSize = Double.parseDouble(thisNozzleResult[1].toString());
				
				// need to convert to 32nd of inch for calculation the base is m 
				nozzleSize = nozzleSize / 0.0007937;
				
				nozzleCalc = nozzleQty * (nozzleSize * nozzleSize);
				nozzleTotal = nozzleTotal + nozzleCalc;
			}
		}
		else {
			systemMessage.addWarning("HSI Calculation: Bit Nozzle value empty -> HSI = 0.0");
			return 0.0;
		}
		
		//use formula found in the green book page 169-170
		//finally calculate the HSI
		Double pbitcalc = BharunCalculation.calculatePb(flowAvg, mudWeight, nozzleTotal);
		Double calcHSI = (flowAvg * pbitcalc) / (1346 * bitDiameter * bitDiameter);
		
		//the base in IDS = WattPerSquareMetre, so need to convert from HSI to WattPerSquareMetre, make sure all calculation result is in IDS base 
		calcHSI = calcHSI * 1155837.16;
		
		
		return calcHSI;
	}
	
	/**
	 * To save HSI value  
	 * @param String BharunUid, String DailyUid, SystemMessage systemMessage
	 * @return void
	 * @throws Exception
	 */
	public static void saveHSI (String BharunUid, String DailyUid, SystemMessage systemMessage) throws Exception  
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double calcHSI = BharunUtils.calculateHSI (BharunUid, DailyUid, systemMessage);
			
		//get bharun daily summary to update 
		String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid AND dailyUid=:DailyUid";		
		String[] paramsFields = {"BharunUid", "DailyUid"};
		String[] paramsValues = {BharunUid, DailyUid};
		List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if (lstResult1.size() > 0){
			
			BharunDailySummary thisBhaDaily = (BharunDailySummary) lstResult1.get(0);
			thisBhaDaily.setHsi(calcHSI);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDaily, qp);
			
			//get dynamic field uid - NOT used after all field convert to fixed field
			/*
			strSql = "FROM DynamicFieldMeta WHERE (isDeleted = false or isDeleted is null) and tableName = 'bharun_daily_summary' AND fieldName='hsi'";
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			
			if (lstResult2.size() > 0){
				DynamicFieldMeta thisFieldMeta = (DynamicFieldMeta) lstResult2.get(0);
				
				strSql = "FROM DynamicField WHERE fieldUid =:dynamicFieldUid AND fieldKey=:BharunDailyUid";
				String[] paramsFields3 = {"dynamicFieldUid", "BharunDailyUid"};
				Object[] paramsValues3 = {thisFieldMeta.getFieldUid(), thisBhaDaily.getBharunDailySummaryUid()};
				List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields3, paramsValues3);
				
				if (lstResult3.size() > 0){
					//update the hsi value in the dynamic value table
					strSql = "UPDATE DynamicField SET fieldValueDouble=:calcHSI WHERE fieldUid =:dynamicFieldUid AND fieldKey=:BharunDailyUid";
					String[] paramsFields2 = {"calcHSI", "dynamicFieldUid", "BharunDailyUid"};
					Object[] paramsValues2 = {calcHSI, thisFieldMeta.getFieldUid(), thisBhaDaily.getBharunDailySummaryUid()};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields2, paramsValues2, qp);
				}
				else {
					DynamicField thisDynamicField = new DynamicField();
					thisDynamicField.setFieldKey(thisBhaDaily.getBharunDailySummaryUid());
					thisDynamicField.setFieldUid(thisFieldMeta.getFieldUid());
					thisDynamicField.setFieldValueDouble(calcHSI);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisDynamicField, qp);
				}
			}
			*/
			
		}		
	}
	
	/**To calculate slide and rotate duration based on phase code and task code
	 * For DEA_GE only
	 * */
	public static void calculateSlideRotateDuration (Bharun bh, BharunDailySummary thisDailySummary, CustomFieldUom thisConverter, String operationUid, String dailyUid, Daily thisDaily) throws Exception {
		String bhaDailyIn = bh.getDailyidIn();
		String bhaDailyOut = bh.getDailyidOut();

		Date thisDate = thisDaily.getDayDate();
		String operationCode = ApplicationUtils.getConfiguredInstance().getCachedAllOperations().get(operationUid).getOperationCode();
		
		Double totalSlideDuration = 0.00;
		Double totalDuration2 = 0.00;
		Double totalDuration3 = 0.00;
		Double totalRotatedDuration = 0.00;
		Double finalDuration = 0.00;
		Double finalRotatedDuration = 0.00;
		Double totalSlide = 0.00;
		Double totalRotated = 0.00;
		
		if(!bhaDailyIn.isEmpty()) {
			Daily bhaDateIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(bh.getDailyidIn());
			Date bhaIn = bhaDateIn.getDayDate();
			
			if(!bhaDailyOut.isEmpty()) {
				Long tIn = bh.getTimeIn().getTime();
				Long tOut = bh.getTimeOut().getTime();
				
				if(tIn != null && tOut != null) {
					Daily bhaDateOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(bh.getDailyidOut());
					Date bhaOut = bhaDateOut.getDayDate();
					
					Date timeIn = bh.getTimeIn();
					Date timeOut = bh.getTimeOut();
					
					String[] paramsFields = {"operationCode", "operationUid", "dailyUid", "timeIn", "timeOut"};
					String[] paramsFields2 = {"operationCode", "operationUid", "dailyUid", "timeIn"};
					Object[] paramsValues = {operationCode, operationUid, dailyUid, timeIn, timeOut};
					Object[] paramsValues2 = {operationCode, operationUid, dailyUid, timeIn};
					
					String strSQL = "SELECT a FROM Activity a, LookupTaskCode ltc WHERE ((ltc.groupingPurpose='SC' or ltc.groupingPurpose LIKE '%SC%RC%') AND ltc.groupingPurpose!='') "
							+ "AND ltc.operationCode=:operationCode "
							+ "AND a.phaseCode=ltc.phaseShortCode AND a.taskCode=ltc.shortCode "
							+ "AND (a.isDeleted = false or a.isDeleted is null) "
							+ "AND (a.dayPlus=0 or a.dayPlus is null) "
							+ "AND (a.startDatetime >= :timeIn) AND (a.endDatetime <= :timeOut) "
							+ "AND a.dailyUid=:dailyUid AND a.operationUid=:operationUid";

					String sql = "SELECT a FROM Activity a, LookupTaskCode ltc WHERE ((ltc.groupingPurpose='SC' or ltc.groupingPurpose LIKE '%SC%RC%') AND ltc.groupingPurpose!='') "
							+ "AND ltc.operationCode=:operationCode "
							+ "AND a.phaseCode=ltc.phaseShortCode AND a.taskCode=ltc.shortCode "
							+ "AND (a.isDeleted = false or a.isDeleted is null) "
							+ "AND (a.dayPlus=0 or a.dayPlus is null) "
							+ "AND (a.startDatetime <:timeIn) AND (a.endDatetime >= :timeIn) "
							+ "AND a.dailyUid=:dailyUid AND a.operationUid=:operationUid";
					
					String strSQL2 = "SELECT a FROM Activity a, LookupTaskCode ltc WHERE ((ltc.groupingPurpose='SC' or ltc.groupingPurpose LIKE '%SC%RC%') AND ltc.groupingPurpose!='') "
							+ "AND ltc.operationCode=:operationCode "
							+ "AND a.phaseCode=ltc.phaseShortCode AND a.taskCode=ltc.shortCode "
							+ "AND (a.isDeleted = false or a.isDeleted is null) "
							+ "AND (a.dayPlus=0 or a.dayPlus is null) "
							+ "AND (a.startDatetime >= :timeIn) AND (a.endDatetime >:timeOut) "
							+ "AND a.dailyUid=:dailyUid AND a.operationUid=:operationUid";
					
					String strSQL3 = "SELECT a FROM Activity a, LookupTaskCode ltc WHERE ((ltc.groupingPurpose='RC' or ltc.groupingPurpose LIKE '%SC%RC%') AND ltc.groupingPurpose!='') "
							+ "AND ltc.operationCode=:operationCode "
							+ "AND a.phaseCode=ltc.phaseShortCode AND a.taskCode=ltc.shortCode "
							+ "AND (a.isDeleted = false or a.isDeleted is null) "
							+ "AND (a.dayPlus=0 or a.dayPlus is null) "
							+ "AND (a.startDatetime >= :timeIn) AND (a.endDatetime <= :timeOut) "
							+ "AND a.dailyUid=:dailyUid AND a.operationUid=:operationUid";
					
					String sql2 = "SELECT a FROM Activity a, LookupTaskCode ltc WHERE ((ltc.groupingPurpose='RC' or ltc.groupingPurpose LIKE '%SC%RC%') AND ltc.groupingPurpose!='') "
							+ "AND ltc.operationCode=:operationCode "
							+ "AND a.phaseCode=ltc.phaseShortCode AND a.taskCode=ltc.shortCode "
							+ "AND (a.isDeleted = false or a.isDeleted is null) "
							+ "AND (a.dayPlus=0 or a.dayPlus is null) "
							+ "AND (a.startDatetime <:timeIn) AND (a.endDatetime >= :timeIn) "
							+ "AND a.dailyUid=:dailyUid AND a.operationUid=:operationUid";
					
					String strSQL4 = "SELECT a FROM Activity a, LookupTaskCode ltc WHERE ((ltc.groupingPurpose='RC' or ltc.groupingPurpose LIKE '%SC%RC%') AND ltc.groupingPurpose!='') "
							+ "AND ltc.operationCode=:operationCode "
							+ "AND a.phaseCode=ltc.phaseShortCode AND a.taskCode=ltc.shortCode "
							+ "AND (a.isDeleted = false or a.isDeleted is null) "
							+ "AND (a.dayPlus=0 or a.dayPlus is null) "
							+ "AND (a.startDatetime >= :timeIn) AND (a.endDatetime >:timeOut) "
							+ "AND a.dailyUid=:dailyUid AND a.operationUid=:operationUid";

					List res = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSQL, paramsFields, paramsValues);
					List res1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields2, paramsValues2);
					List res2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSQL2, paramsFields, paramsValues);
					List res3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSQL3, paramsFields, paramsValues);
					List res4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSQL4, paramsFields, paramsValues);
					List res5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql2, paramsFields2, paramsValues2);
					
					if(res.size() > 0) {
						if(res.get(0) != null) {
							for(Object objActivity: res) {
								Activity thisActivity = (Activity) objActivity;
								Double thisDuration = thisActivity.getActivityDuration();
								if((thisDate.after(bhaIn) || thisDate.equals(bhaIn)) || (thisDate.before(bhaOut) || thisDate.equals(bhaOut))) {
									if(thisActivity.getActivityDuration() != null) {
										totalSlide = totalSlide + thisDuration;
									}
								}
							}
							thisDailySummary.setSlideHours(totalSlide);
						}
					}else {
						thisDailySummary.setSlideHours(totalSlide);
					}
					
					if(res1.size() > 0) {
						if(res1.get(0) != null) {
							for(Object objActivity: res1) {
								Activity thisActivity = (Activity) objActivity;
								long startDatetime = thisActivity.getStartDatetime().getTime();
								long endDatetime = thisActivity.getEndDatetime().getTime();
								Double slideDuration = 0.00;
								if((thisDate.after(bhaIn) || thisDate.equals(bhaIn)) || (thisDate.before(bhaOut) || thisDate.equals(bhaOut))) {
									if(thisActivity.getActivityDuration() != null) {
										if(thisActivity.getStartDatetime().getTime() < tIn && thisActivity.getEndDatetime().getTime() > tIn) {
											long d = endDatetime - tIn;
											String s = d +"";
											Double extra = Double.parseDouble(s);
											Double extraDuration = extra / 3600000;
											slideDuration = extraDuration;
										}
									}
								}
								totalSlideDuration = totalSlideDuration + slideDuration;
							}
							totalDuration2 = totalSlideDuration + totalSlide;
							thisDailySummary.setSlideHours(totalDuration2);
						}
					}else {
						totalDuration2 = totalSlideDuration + totalSlide;
						thisDailySummary.setSlideHours(totalDuration2);
					}
					//Take every activity object and process each object data by checking the BHA time out and activity endDatetime
					
					if(res2.size() > 0) {
						if(res2.get(0) != null) {
							if(res.size() > 0) {
								for(Object objActivity: res2) {
									Activity thisActivity = (Activity) objActivity;
									long endDatetime = thisActivity.getEndDatetime().getTime();
									long startDatetime = thisActivity.getStartDatetime().getTime();
									Double thisDuration = thisActivity.getActivityDuration();
									if((thisDate.after(bhaIn) || thisDate.equals(bhaIn)) || (thisDate.before(bhaOut) || thisDate.equals(bhaOut))) {
										if(thisActivity.getActivityDuration() != null) {
											if(thisActivity.getStartDatetime().getTime() < tOut) {
												long d = tOut - startDatetime;
												String s = d +"";
												Double extra = Double.parseDouble(s);
												Double extraDuration = extra / 3600000;
												finalDuration = extraDuration;
											}else if ((thisActivity.getEndDatetime().getTime() > tOut) && (thisActivity.getStartDatetime().getTime() > tOut)){
												finalDuration = 0.00;
											}else {
												long d = endDatetime - tOut;
												String s = d +"";
												Double extra = Double.parseDouble(s);
												Double extraDuration = extra / 3600000;
												finalDuration = thisDuration - extraDuration;
											}
										}
									}
									totalSlideDuration = totalSlideDuration + finalDuration;
								}
								totalSlideDuration = totalSlideDuration + totalDuration2;
								thisDailySummary.setSlideHours(totalSlideDuration);
							}else {
								for(Object objActivity: res2) {
									Activity thisActivity = (Activity) objActivity;
									long endDatetime = thisActivity.getEndDatetime().getTime();
									long startDatetime = thisActivity.getStartDatetime().getTime();
									Double thisDuration = thisActivity.getActivityDuration();
									if((thisDate.after(bhaIn) || thisDate.equals(bhaIn)) || (thisDate.before(bhaOut) || thisDate.equals(bhaOut))) {
										if(thisActivity.getActivityDuration() != null) {
											if(thisActivity.getStartDatetime().getTime() < tOut) {
												long d = tOut - startDatetime;
												String s = d +"";
												Double extra = Double.parseDouble(s);
												Double extraDuration = extra / 3600000;
												finalDuration = extraDuration;
											}else if ((thisActivity.getEndDatetime().getTime() > tOut) && (thisActivity.getStartDatetime().getTime() > tOut)){
												finalDuration = 0.00;
											}else {
												long d = endDatetime - tOut;
												String s = d +"";
												Double extra = Double.parseDouble(s);
												Double extraDuration = extra / 3600000;
												finalDuration = thisDuration - extraDuration;
											}		
										}
									}
									totalSlideDuration = totalSlideDuration + finalDuration;
								}
								totalSlideDuration = totalSlideDuration + totalDuration2;
								thisDailySummary.setSlideHours(totalSlideDuration);
							}
						}
					}

					if(res3.size() > 0) {		
						if(res3.get(0) != null) {
							for(Object objActivity: res3) {
								Activity thisActivity = (Activity) objActivity;
								Double thisDuration = thisActivity.getActivityDuration();
								if((thisDate.after(bhaIn) || thisDate.equals(bhaIn)) || (thisDate.before(bhaOut) || thisDate.equals(bhaOut))) {
									if(thisActivity.getActivityDuration() != null) {
										totalRotated = totalRotated + thisDuration;
									}
								}
							}
							thisDailySummary.setDurationRotated(totalRotated);
						}
					}else {
						thisDailySummary.setDurationRotated(totalRotated);
					}
					
					if(res5.size() > 0) {
						if(res5.get(0) != null) {
							for(Object objActivity: res5) {
								Activity thisActivity = (Activity) objActivity;
								long startDatetime = thisActivity.getStartDatetime().getTime();
								long endDatetime = thisActivity.getEndDatetime().getTime();
								Double rotatedDuration = 0.00;
								if((thisDate.after(bhaIn) || thisDate.equals(bhaIn)) || (thisDate.before(bhaOut) || thisDate.equals(bhaOut))) {
									if(thisActivity.getActivityDuration() != null) {
										if(thisActivity.getStartDatetime().getTime() < tIn && thisActivity.getEndDatetime().getTime() > tIn) {
											long d = endDatetime - tIn;
											String s = d +"";
											Double extra = Double.parseDouble(s);
											Double extraDuration = extra / 3600000;
											rotatedDuration = extraDuration;
										}
									}
								}
								totalRotatedDuration = totalRotatedDuration + rotatedDuration;
							}
							totalDuration3 = totalRotatedDuration + totalRotated;
							thisDailySummary.setDurationRotated(totalDuration3);
						}
					}else {
						totalDuration3 = totalRotatedDuration + totalRotated;
						thisDailySummary.setDurationRotated(totalDuration3);
					}
						
					//Take every activity object and process the each object data by checking the BHA time out and activity endDatetime
					
					if(res4.size() > 0) {
						if(res4.get(0) != null) { 
							if(res3.size() > 0) {
								for(Object objActivity: res4) {
									Activity thisActivity = (Activity) objActivity;
									long endDatetime = thisActivity.getEndDatetime().getTime();
									long startDatetime = thisActivity.getStartDatetime().getTime();
									Double thisDuration = thisActivity.getActivityDuration();
									if((thisDate.after(bhaIn) || thisDate.equals(bhaIn)) || (thisDate.before(bhaOut) || thisDate.equals(bhaOut))) {
										if(thisActivity.getActivityDuration() != null) {
											if(thisActivity.getStartDatetime().getTime() < tOut) {							
												long d = tOut - startDatetime;
												String s = d +"";
												Double extra = Double.parseDouble(s);
												Double extraDuration = extra / 3600000;
												finalRotatedDuration = extraDuration;
											}else if ((thisActivity.getEndDatetime().getTime() > tOut) && (thisActivity.getStartDatetime().getTime() > tOut)){
												finalRotatedDuration = 0.00;
											}else {
												long d = endDatetime - tOut;
												String s = d +"";
												Double extra = Double.parseDouble(s);
												Double extraDuration = extra / 3600000;
												finalRotatedDuration = thisDuration - extraDuration;
											}
										}
									}
									totalRotatedDuration = totalRotatedDuration + finalRotatedDuration;
								}
								totalRotatedDuration = totalRotatedDuration + totalDuration3;
								thisDailySummary.setDurationRotated(totalRotatedDuration);
							}else {
								for(Object objActivity: res4) {
									Activity thisActivity = (Activity) objActivity;
									long endDatetime = thisActivity.getEndDatetime().getTime();
									long startDatetime = thisActivity.getStartDatetime().getTime();
									Double thisDuration = thisActivity.getActivityDuration();
									if((thisDate.after(bhaIn) || thisDate.equals(bhaIn)) || (thisDate.before(bhaOut) || thisDate.equals(bhaOut))) {
										if(thisActivity.getStartDatetime().getTime() < tOut) {							
											long d = tOut - startDatetime;
											String s = d +"";
											Double extra = Double.parseDouble(s);
											Double extraDuration = extra / 3600000;
											finalRotatedDuration = extraDuration;
										}else if ((thisActivity.getEndDatetime().getTime() > tOut) && (thisActivity.getStartDatetime().getTime() > tOut)){
											finalRotatedDuration = 0.00;
										}else {
											long d = endDatetime - tOut;
											String s = d +"";
											Double extra = Double.parseDouble(s);
											Double extraDuration = extra / 3600000;
											finalRotatedDuration = thisDuration - extraDuration;
										}
									}
									totalRotatedDuration = totalRotatedDuration + finalRotatedDuration;
								}
								totalRotatedDuration = totalRotatedDuration + totalDuration3;
								thisDailySummary.setDurationRotated(totalRotatedDuration);
							}
						}
					}
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisDailySummary);
				}
			}else {
				Date timeIn = bh.getTimeIn();
				
				Long tIn = bh.getTimeIn().getTime();

				String[] paramsFields = {"operationCode", "operationUid", "dailyUid"};
				Object[] paramsValues = {operationCode, operationUid, dailyUid};
				
				String strSQL = "SELECT a FROM Activity a, LookupTaskCode ltc WHERE ((ltc.groupingPurpose='SC' or ltc.groupingPurpose LIKE '%SC%RC%') AND ltc.groupingPurpose!='') "
						+ "AND ltc.operationCode=:operationCode "
						+ "AND a.phaseCode=ltc.phaseShortCode AND a.taskCode=ltc.shortCode "
						+ "AND (a.isDeleted = false or a.isDeleted is null) "
						+ "AND (a.dayPlus=0 or a.dayPlus is null) "
						+ "AND a.dailyUid=:dailyUid AND a.operationUid=:operationUid";
				String strSQL2 = "SELECT a FROM Activity a, LookupTaskCode ltc WHERE ((ltc.groupingPurpose='RC' or ltc.groupingPurpose LIKE '%SC%RC%') AND ltc.groupingPurpose!='') "
						+ "AND ltc.operationCode=:operationCode "
						+ "AND a.phaseCode=ltc.phaseShortCode AND a.taskCode=ltc.shortCode "
						+ "AND (a.isDeleted = false or a.isDeleted is null) "
						+ "AND (a.dayPlus=0 or a.dayPlus is null) "
						+ "AND a.dailyUid=:dailyUid AND a.operationUid=:operationUid";
				
				List res = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSQL, paramsFields, paramsValues);
				List res2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSQL2, paramsFields, paramsValues);
				
				if(res.size() > 0) {
					if(res.get(0) != null) {
						for(Object objActivity: res) {
							Activity thisActivity = (Activity) objActivity;
							long endDatetime = thisActivity.getEndDatetime().getTime();
							long startDatetime = thisActivity.getStartDatetime().getTime();
							Double thisDuration = thisActivity.getActivityDuration();
							
							Calendar bhaStartDatetimeObj = Calendar.getInstance();
							Calendar bhaStartDateObj = Calendar.getInstance();
							Calendar activitystarttime = Calendar.getInstance();
							activitystarttime.setTime(thisActivity.getStartDatetime());
							bhaStartDatetimeObj.setTime(timeIn);
							bhaStartDateObj.set(Calendar.YEAR, activitystarttime.get(Calendar.YEAR));
							bhaStartDateObj.set(Calendar.MONTH, activitystarttime.get(Calendar.MONTH));
							bhaStartDateObj.set(Calendar.DAY_OF_MONTH, activitystarttime.get(Calendar.DAY_OF_MONTH));
							bhaStartDateObj.set(Calendar.HOUR_OF_DAY, bhaStartDatetimeObj.get(Calendar.HOUR_OF_DAY));
							bhaStartDateObj.set(Calendar.MINUTE, bhaStartDatetimeObj.get(Calendar.MINUTE));
							bhaStartDateObj.set(Calendar.SECOND, bhaStartDatetimeObj.get(Calendar.SECOND));
							if(thisDate.equals(bhaIn)) {
								if((endDatetime > tIn) && (startDatetime < tIn)){
									long d = endDatetime - tIn;
									String s = d +"";
									Double extra = Double.parseDouble(s);
									Double extraDuration = extra / 3600000;
									thisDuration = extraDuration;
									totalSlide = totalSlide + thisDuration;
								}else if (endDatetime < tIn){
									thisDuration = 0.00;
									totalSlide = totalSlide + thisDuration;
								}else {
									totalSlide = totalSlide + thisDuration;
								}     
							}else if(thisDate.after(bhaIn)) {
/*								if((endDatetime > bhaStartDateObj.getTimeInMillis()) && (startDatetime < bhaStartDateObj.getTimeInMillis())){
									long d = endDatetime - bhaStartDateObj.getTimeInMillis();
									String s = d +"";
									Double extra = Double.parseDouble(s);
									Double extraDuration = extra / 3600000;
									thisDuration = extraDuration;
									totalSlide = totalSlide + thisDuration;
								}else if(endDatetime < bhaStartDateObj.getTimeInMillis()){
									thisDuration = 0.00;
									totalSlide = totalSlide + thisDuration;
								}else {
									totalSlide = totalSlide + thisDuration;
								}*/
								if((endDatetime > tIn) && (startDatetime < tIn)){
									long d = endDatetime - tIn;
									String s = d +"";
									Double extra = Double.parseDouble(s);
									Double extraDuration = extra / 3600000;
									thisDuration = extraDuration;
									totalSlide = totalSlide + thisDuration;
								}else if (endDatetime < tIn){
									thisDuration = 0.00;
									totalSlide = totalSlide + thisDuration;
								}else {
									totalSlide = totalSlide + thisDuration;
								}
							}
						}
						thisDailySummary.setSlideHours(totalSlide);
					}
				}else {
					thisDailySummary.setSlideHours(totalSlide);
				}
				
				if(res2.size() > 0) {		
					if(res2.get(0) != null) {
						for(Object objActivity: res2) {
							Activity thisActivity = (Activity) objActivity;
							long endDatetime = thisActivity.getEndDatetime().getTime();
							long startDatetime = thisActivity.getStartDatetime().getTime();
							Double thisDuration = thisActivity.getActivityDuration();
							
							Calendar bhaStartDatetimeObj = Calendar.getInstance();
							Calendar bhaStartDateObj = Calendar.getInstance();
							Calendar activitystarttime = Calendar.getInstance();
							activitystarttime.setTime(thisActivity.getStartDatetime());
							bhaStartDatetimeObj.setTime(timeIn);
							bhaStartDateObj.set(Calendar.YEAR, activitystarttime.get(Calendar.YEAR));
							bhaStartDateObj.set(Calendar.MONTH, activitystarttime.get(Calendar.MONTH));
							bhaStartDateObj.set(Calendar.DAY_OF_MONTH, activitystarttime.get(Calendar.DAY_OF_MONTH));
							bhaStartDateObj.set(Calendar.HOUR_OF_DAY, bhaStartDatetimeObj.get(Calendar.HOUR_OF_DAY));
							bhaStartDateObj.set(Calendar.MINUTE, bhaStartDatetimeObj.get(Calendar.MINUTE));
							bhaStartDateObj.set(Calendar.SECOND, bhaStartDatetimeObj.get(Calendar.SECOND));
							
							if(thisDate.equals(bhaIn)) {
								if((endDatetime > tIn) && (startDatetime < tIn)) {
									long d = endDatetime - tIn;
									String s = d +"";
									Double extra = Double.parseDouble(s);
									Double extraDuration = extra / 3600000;
									thisDuration = extraDuration;
									totalRotated = totalRotated + thisDuration;
								}else if (endDatetime < tIn){
									thisDuration = 0.00;
									totalRotated = totalRotated + thisDuration;
								}else {
									totalRotated = totalRotated + thisDuration;
								}
							}else if(thisDate.after(bhaIn)){
/*								if((endDatetime > bhaStartDateObj.getTimeInMillis()) && (startDatetime < bhaStartDateObj.getTimeInMillis())) {
									long d = endDatetime - bhaStartDateObj.getTimeInMillis();
									String s = d +"";
									Double extra = Double.parseDouble(s);
									Double extraDuration = extra / 3600000;
									thisDuration = extraDuration;
									totalRotated = totalRotated + thisDuration;
								}else if(endDatetime < bhaStartDateObj.getTimeInMillis()){
									thisDuration = 0.00;
									totalRotated = totalRotated + thisDuration;
								}else {
									totalRotated = totalRotated + thisDuration;
								}*/
								if((endDatetime > tIn) && (startDatetime < tIn)) {
									long d = endDatetime - tIn;
									String s = d +"";
									Double extra = Double.parseDouble(s);
									Double extraDuration = extra / 3600000;
									thisDuration = extraDuration;
									totalRotated = totalRotated + thisDuration;
								}else if (endDatetime < tIn){
									thisDuration = 0.00;
									totalRotated = totalRotated + thisDuration;
								}else {
									totalRotated = totalRotated + thisDuration;
								}
							}
						}
						thisDailySummary.setDurationRotated(totalRotated);
					}
				}else {
					thisDailySummary.setDurationRotated(totalRotated);
				}
			}		
		}else {
			thisDailySummary.setSlideHours(totalSlide);
			thisDailySummary.setDurationRotated(totalRotatedDuration);
		}
	}
	
	/**To calculate total run for slide and rotate duration based on phase code and task code
	 * For DEA_GE only
	 * */
	public static void calculateTotalRun (CommandBeanTreeNode node, Date currentDate, CustomFieldUom thisConverter, String operationUid, String dailyUid, String bharunUid) throws Exception {
		String[] paramsFields2 = {"todayDate", "operationUid", "bharunUid"};
		Object[] paramsValues2 = new Object[3]; paramsValues2[0] = currentDate; paramsValues2[1] = operationUid; paramsValues2[2] = bharunUid;
		
		String strSQL3 = "SELECT SUM(bds.duration) as totalDuration, SUM(bds.progress) as totalProgress, SUM(bds.IADCDuration) as totalIADCDuration, " +
		"SUM(bds.slideHours) as totalSlideHours, SUM(bds.durationRotated) as totalDurationRotated, " +
		"SUM(bds.durationCirculated) as totalDurationCirculated, SUM(bds.jarRotatingHours) as cumJarRotatingHours, " +
		"SUM(bds.krevs) as totalKrevs, SUM(bds.stalls) as totalStalls, SUM(bds.acceleratingHour)as cumAcceleratingHours, " +
		"SUM(bds.slideDistance) as totalSlideDistance, SUM(bds.rotatedDistance) as totalRotatedDistance, SUM(bds.jarhourcirculate) as cumJarCirculatingHours, " +
		"SUM(bds.inHoleHrs) as cumInHoleHrs " +
			"FROM BharunDailySummary bds, Daily d " +
			"WHERE (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
			"and d.dailyUid = bds.dailyUid AND d.dayDate <= :todayDate AND d.operationUid = :operationUid AND bds.bharunUid = :bharunUid";				
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSQL3, paramsFields2, paramsValues2);
		Object[] thisResult = (Object[]) lstResult.get(0);
		
		//CALCULATE TOTAL SLIDE HOURS
		Double totalSlideHours = 0.00;
		Double totalDurationRotated = 0.00;

		thisConverter.setReferenceMappingField(BharunDailySummary.class, "slideHours");
		if (thisResult[3] != null) {
			totalSlideHours = Double.parseDouble(thisResult[3].toString());
		}
		thisConverter.setBaseValue(totalSlideHours);
		if (thisConverter.isUOMMappingAvailable()) { 
			node.setCustomUOM("@totalSlideHours", thisConverter.getUOMMapping());
		}
		node.getDynaAttr().put("totalSlideHours", thisConverter.getConvertedValue());

		//CALCULATE TOTAL DURATION ROTATED
		thisConverter.setReferenceMappingField(BharunDailySummary.class, "durationRotated");
		if (thisResult[4] != null) {
			totalDurationRotated = Double.parseDouble(thisResult[4].toString());
		}
		thisConverter.setBaseValue(totalDurationRotated);
		if (thisConverter.isUOMMappingAvailable()) { 
			node.setCustomUOM("@totalDurationRotated", thisConverter.getUOMMapping());
		}
		node.getDynaAttr().put("totalDurationRotated", thisConverter.getConvertedValue());
	}
	
	/**To calculate percentage of duration on the day based on phase code and task code
	 * For DEA_GE only
	 * */
	public static void calculatePercentDay (BharunDailySummary thisDailySummary, CustomFieldUom slideConverter, CustomFieldUom rotatedConverter, CustomFieldUom circulatedConverter) throws Exception {
		
		Double slideHours = thisDailySummary.getSlideHours();
		Double durationRotated = thisDailySummary.getDurationRotated();
		Double durationCirculated = thisDailySummary.getDurationCirculated();	
		
		if(slideHours != null && durationRotated != null && durationCirculated != null) {
			
			BigDecimal sH = new BigDecimal(slideHours).setScale(2, RoundingMode.HALF_UP);
	        slideHours = sH.doubleValue();
	        
	        BigDecimal dR = new BigDecimal(durationRotated).setScale(2, RoundingMode.HALF_UP);
	        durationRotated = dR.doubleValue();
	        
	        BigDecimal dC = new BigDecimal(durationCirculated).setScale(2, RoundingMode.HALF_UP);
	        durationCirculated = dC.doubleValue();
	        
	        Double percentSlideDay = 0.00;
	        Double percentRotatedDay = 0.00;
	        Double percentCirculatedDay = 0.00;
	        
	        if(slideHours != 0.00 || durationRotated != 0.00) {
				percentSlideDay = (slideHours / (slideHours + durationRotated + durationCirculated))*100;
				percentRotatedDay = (durationRotated / (slideHours + durationRotated + durationCirculated))*100;
	        }
	        
	        if(durationCirculated != 0.00 && durationCirculated > 0.00) {
				percentCirculatedDay = (durationCirculated / (slideHours + durationRotated + durationCirculated))*100;	
	        }
			
	        slideConverter.setBaseValue(percentSlideDay);
			rotatedConverter.setBaseValue(percentRotatedDay);
			circulatedConverter.setBaseValue(percentCirculatedDay);
			
			percentSlideDay = slideConverter.getConvertedValue();
			percentRotatedDay = rotatedConverter.getConvertedValue();
			percentCirculatedDay = circulatedConverter.getConvertedValue();
			
			thisDailySummary.setPercentSlideDay(percentSlideDay);
			thisDailySummary.setPercentRotatedDay(percentRotatedDay);
			thisDailySummary.setPercentCirculatedDay(percentCirculatedDay);
		}else if(slideHours != null && durationRotated != null && durationCirculated == null) {
			if(slideHours == 0.0 && durationRotated == 0.0 && durationCirculated == null) {
				thisDailySummary.setPercentSlideDay(null);
				thisDailySummary.setPercentRotatedDay(null);
				thisDailySummary.setPercentCirculatedDay(null);
			}else{
				BigDecimal sH = new BigDecimal(slideHours).setScale(2, RoundingMode.HALF_UP);
		        slideHours = sH.doubleValue();
		        
		        BigDecimal dR = new BigDecimal(durationRotated).setScale(2, RoundingMode.HALF_UP);
		        durationRotated = dR.doubleValue();
				
				Double percentSlideDay = (slideHours / (slideHours + durationRotated))*100;
				Double percentRotatedDay = (durationRotated / (slideHours + durationRotated))*100;
				
				slideConverter.setBaseValue(percentSlideDay);
				rotatedConverter.setBaseValue(percentRotatedDay);
				
				percentSlideDay = slideConverter.getConvertedValue();
				percentRotatedDay = rotatedConverter.getConvertedValue();
				
				thisDailySummary.setPercentSlideDay(percentSlideDay);
				thisDailySummary.setPercentRotatedDay(percentRotatedDay);
				thisDailySummary.setPercentCirculatedDay(null);
			}
		}
	}
	
	/**
	 * To calculate AV value only then pass to saveAV to save the value
	 * @param String BharunUid, SystemMessage systemMessage
	 * @return Double
	 * @throws Exception
	 */
	public static void calculateAV (String groupUid,String BharunUid, String DailyUid, SystemMessage systemMessage) throws Exception  
	{
		//NOT VALID AFTER ALL FIELD IS FIXED FIELD due to this calculation is purely base on dynamic fields so in-order for it to work fields need to be in pairs as stated below
		/*
		//bharun - need to have hwdpX_od, hwdpX_id
		//bharun_daily_summary -  need to have  annvel_hwdpX
		
		//next time got more fields need to extend this array, field name had to follow rules above
		String[] fieldsInvolved = {"dp", "hwdp", "hwdp2", "hwdp3", "dc1", "dc2"};
		String strFieldNameOd = "";
		*/
		
		
		String strSql = "";
		List lstResult;
				
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get bharun object for od to do calculation
		strSql = "FROM Bharun a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid";
		String[] paramsFields3 = {"BharunUid"};
		String[] paramsValues3 = {BharunUid};
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields3, paramsValues3, qp);
		if (lstResult.size() > 0)
		{
			Bharun thisBharun = (Bharun) lstResult.get(0);
			
			//get average flow rate from drilling parameters
			strSql = "SELECT AVG(a.flowAvg) as avgFlow FROM DrillingParameters a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.flowAvg > 0";
			String[] paramsFields = {"BharunUid", "DailyUid"};
			String[] paramsValues = {BharunUid, DailyUid};
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			Object flowResult = (Object) lstResult.get(0);
			Double flowAvg = 0.00;
			if (flowResult != null){
				flowAvg = Double.parseDouble(flowResult.toString());
				//convert to USGallonsPerMinute for calculation from IDS base CubicMetrePerSecond
				flowAvg = flowAvg / 0.0000630902;
			}else{
				systemMessage.addWarning("AV Calculation: Average Flow (Drilling Parameter) value empty -> AV = 0.0");
				flowAvg = 0.0;
			}
			
			//get bit diameter
			strSql = "SELECT bitDiameter, bitrunUid FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", BharunUid, qp);
			Double bitDiameter = 0.00;
			if (lstResult.size() > 0)
			{
				Object[] thisBitResult = (Object[]) lstResult.get(0);
				
				if (thisBitResult[0] != null){
					bitDiameter = Double.parseDouble(thisBitResult[0].toString());
					//convert to inch for calculation the IDS base is m		
					bitDiameter = bitDiameter / 0.0254;
				}else{
					systemMessage.addWarning("AV Calculation: Bit Diameter value empty  -> AV = 0.0");
					bitDiameter = 0.0;			
				}
			}
			else
			{
				systemMessage.addWarning("AV Calculation: No Bitrun for this BHA -> AV = 0.0");
				bitDiameter = 0.0;	
			}
			
			/*
			String[] paramsFields1 = {"thisBharunUid"};
			String[] paramsValues1 = {BharunUid};
			*/
			Double valOd;
			Double calcAV;
			
			//get all bha daily summary object that involve in this calculation for today and update their AV 
			//get bharun daily summary to update dynamic field
			strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid AND dailyUid=:DailyUid";		
			String[] paramsFields2 = {"BharunUid", "DailyUid"};
			String[] paramsValues2 = {BharunUid, DailyUid};
			List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
			
			if (lstResult1.size() > 0){
				for(Iterator i = lstResult1.iterator(); i.hasNext(); ){
					Object obj_array = (Object) i.next();
					BharunDailySummary thisDailySummary = (BharunDailySummary) obj_array;

					Double casingId=0.0;
					if(thisBharun.getMinCsgSizeId() != null){
						casingId = thisBharun.getMinCsgSizeId();
						casingId = casingId / 0.0254;
					}
					
					Double linerId=0.0;
					if(thisBharun.getMinLinerSizeId() != null){
						linerId = thisBharun.getMinLinerSizeId();
						linerId = linerId / 0.0254;
					}
					
					//calc dp
					valOd = 0.00;
					if("1".equals(GroupWidePreference.getValue(groupUid,GroupWidePreference.GWP_CALC_AV_WITH_ALT_DIAMETER))){						
						bitDiameter = 0.0 ;
						if (thisBharun.getDpId() != null) bitDiameter = thisBharun.getDpId();	
						bitDiameter = bitDiameter / 0.0254;
					}					
					if (thisBharun.getDpOd() != null) valOd = thisBharun.getDpOd();					
					
					if (valOd == 0.00 || bitDiameter == 0.0){
						calcAV = 0.00;
					}
					else{
						calcAV = BharunUtils.calcAVValue(flowAvg,bitDiameter,valOd);
					}
					thisDailySummary.setAnnvelDp(calcAV);
					
					//calc dp_casing
					valOd = 0.00;
					if (thisBharun.getDpOd() != null) valOd = thisBharun.getDpOd();					
					
					if (valOd == 0.00 || casingId == 0.0){
						calcAV = 0.00;
					}
					else{
						calcAV = BharunUtils.calcAVValue(flowAvg,casingId,valOd);
					}
					thisDailySummary.setAnnvelDpCasing(calcAV);
					
					//calc dp_liner
					valOd = 0.00;
					if (thisBharun.getDpOd() != null) valOd = thisBharun.getDpOd();					
					
					if (valOd == 0.00 || linerId == 0.0){
						calcAV = 0.00;
					}
					else{
						calcAV = BharunUtils.calcAVValue(flowAvg,linerId,valOd);
					}
					thisDailySummary.setAnnvelDpLiner(calcAV);
					
					//calc dp2
					valOd = 0.00;
					if("1".equals(GroupWidePreference.getValue(groupUid,GroupWidePreference.GWP_CALC_AV_WITH_ALT_DIAMETER))){						
						bitDiameter = 0.0 ;
						if (thisBharun.getDpId() != null) bitDiameter = thisBharun.getDpId();	
						bitDiameter = bitDiameter / 0.0254;
					}
					if (thisBharun.getDp2Od() != null) valOd = thisBharun.getDp2Od();					
					
					if (valOd == 0.00 || bitDiameter == 0.0){
						calcAV = 0.00;
					}
					else{
						calcAV = BharunUtils.calcAVValue(flowAvg,bitDiameter,valOd);
					}
					thisDailySummary.setAnnvelDp2(calcAV);
					
					//calc dp2_casing
					valOd = 0.00;
					if (thisBharun.getDp2Od() != null) valOd = thisBharun.getDp2Od();					
					
					if (valOd == 0.00 || casingId == 0.0){
						calcAV = 0.00;
					}
					else{
						calcAV = BharunUtils.calcAVValue(flowAvg,casingId,valOd);
					}
					thisDailySummary.setAnnvelDp2Casing(calcAV);
					
					//calc dp2_liner
					valOd = 0.00;
					if (thisBharun.getDp2Od() != null) valOd = thisBharun.getDp2Od();					
					
					if (valOd == 0.00 || linerId == 0.0){
						calcAV = 0.00;
					}
					else{
						calcAV = BharunUtils.calcAVValue(flowAvg,linerId,valOd);
					}
					thisDailySummary.setAnnvelDp2Liner(calcAV);
					
					//calc hwdp
					valOd = 0.00;
					if("1".equals(GroupWidePreference.getValue(groupUid,GroupWidePreference.GWP_CALC_AV_WITH_ALT_DIAMETER))){						
						bitDiameter = 0.0 ;
						if (thisBharun.getHwdpId() != null) bitDiameter = thisBharun.getHwdpId();		
						bitDiameter = bitDiameter / 0.0254;
					}					
					if (thisBharun.getHwdpOd() != null) valOd = thisBharun.getHwdpOd();					
					
					if (valOd == 0.00 || bitDiameter == 0.0){
						calcAV = 0.00;
					}
					else{
						calcAV = BharunUtils.calcAVValue(flowAvg,bitDiameter,valOd);
					}
					thisDailySummary.setAnnvelHwdp(calcAV);
					
					//calc hwdp2
					valOd = 0.00;
					if("1".equals(GroupWidePreference.getValue(groupUid,GroupWidePreference.GWP_CALC_AV_WITH_ALT_DIAMETER))){						
						bitDiameter = 0.0 ;
						if (thisBharun.getHwdp2Id() != null) bitDiameter = thisBharun.getHwdp2Id();		
						bitDiameter = bitDiameter / 0.0254;
					}					
					if (thisBharun.getHwdp2Od() != null) valOd = thisBharun.getHwdp2Od();					
					
					if (valOd == 0.00 || bitDiameter == 0.0){
						calcAV = 0.00;
					}
					else{
						calcAV = BharunUtils.calcAVValue(flowAvg,bitDiameter,valOd);
					}
					thisDailySummary.setAnnvelHwdp2(calcAV);
					
					//calc hwdp3
					valOd = 0.00;
					if("1".equals(GroupWidePreference.getValue(groupUid,GroupWidePreference.GWP_CALC_AV_WITH_ALT_DIAMETER))){						
						bitDiameter = 0.0 ;
						if (thisBharun.getHwdp3Id() != null) bitDiameter = thisBharun.getHwdp3Id();	
						bitDiameter = bitDiameter / 0.0254;
					}					
					if (thisBharun.getHwdp3Od() != null) valOd = thisBharun.getHwdp3Od();					
					
					if (valOd == 0.00 || bitDiameter == 0.0){
						calcAV = 0.00;
					}
					else{
						calcAV = BharunUtils.calcAVValue(flowAvg,bitDiameter,valOd);
					}
					thisDailySummary.setAnnvelHwdp3(calcAV);
					
					//calc dc1
					valOd = 0.00;
					if("1".equals(GroupWidePreference.getValue(groupUid,GroupWidePreference.GWP_CALC_AV_WITH_ALT_DIAMETER))){						
						bitDiameter = 0.0 ;
						if (thisBharun.getDc1Id() != null) bitDiameter = thisBharun.getDc1Id();	
						bitDiameter = bitDiameter / 0.0254;
					}
					if (thisBharun.getDc1Od() != null) valOd = thisBharun.getDc1Od();					
					
					if (valOd == 0.00 || bitDiameter == 0.0) {
						calcAV = 0.00;
					}
					else{
						calcAV = BharunUtils.calcAVValue(flowAvg,bitDiameter,valOd);
					}
					thisDailySummary.setAnnvelDc1(calcAV);
					
					//calc dc2
					valOd = 0.00;
					if("1".equals(GroupWidePreference.getValue(groupUid,GroupWidePreference.GWP_CALC_AV_WITH_ALT_DIAMETER))){						
						bitDiameter = 0.0 ;
						if (thisBharun.getDc2Id() != null) bitDiameter = thisBharun.getDc2Id();		
						bitDiameter = bitDiameter / 0.0254;
					}
					if (thisBharun.getDc2Od() != null) valOd = thisBharun.getDc2Od();					
					
					if (valOd == 0.00 || bitDiameter == 0.0){
						calcAV = 0.00;
					}
					else{
						calcAV = BharunUtils.calcAVValue(flowAvg,bitDiameter,valOd);
					}
					thisDailySummary.setAnnvelDc2(calcAV);
					
					//save the bhadaily object
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisDailySummary, qp);
				}
			}
		} //check if no bharun
	}
	
	/**
	 * To calculate AV value 
	 * @param double flowAvg
	 * @param double holeSize
	 * @param double holeSize
	 * @return double calcAV
	 * @throws Exception
	 */
	private static Double calcAVValue (Double flowAvg, Double holeSize, Double dpSize){
		Double calcAV = 0.0;
		
		//use formula found in the green book page 169-170
		//AV = (24.5 * Flow Rate in gpm) / ((hole size in inch ^ 2) - (Drill Pipe Diameter in inch ^ 2))
		
		//change DP OD to inch for calculation IDS base is m
		dpSize = dpSize / 0.0254;
		
		if (dpSize.compareTo(holeSize)==0){
			calcAV = 0.0;
		}else {
			calcAV = (24.5 * flowAvg) / ((holeSize * holeSize) - (dpSize * dpSize));							
			//result for AV is in ft/min, need to convert to IDS base MetresPerSecond
			calcAV = calcAV * 0.00508;
		}
		
		return calcAV;
	}
}
