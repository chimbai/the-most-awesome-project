package com.idsdatanet.d2.drillnet.report.schedule;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class EmailConfiguration {
	
	private String subject;
	private String[] emailList;
	private String specificEmailDistributionKey;
	private String templateFile;
	private Boolean includeSupportEmail;
	private Boolean validEmail=false;
	private Boolean errorEmail=false;
	private Map<String,File>	attachment;
	private Map<String,Object> contentMapping= new HashMap<String,Object>();
	
	public EmailConfiguration(){
		
	}
	
	public EmailConfiguration (EmailConfiguration config)
	{
		this.subject=config.subject;
		this.emailList=config.emailList;
		this.templateFile=config.templateFile;
		this.validEmail=config.validEmail;
		this.errorEmail=config.errorEmail;
		this.attachment=config.attachment;
		this.contentMapping=config.contentMapping;
		this.specificEmailDistributionKey = config.specificEmailDistributionKey;
	}
	
	public void setEmailList(String[] emailList) {
		this.emailList = emailList;
	}
	public String[] getEmailList() {
		return emailList;
	}
	public void setAttachment(Map<String,File> attachment) {
		this.attachment = attachment;
	}
	public Map<String,File> getAttachment() {
		return attachment;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSubject() {
		return subject;
	}
	public void setTemplateFile(String templateFile) {
		this.templateFile = templateFile;
	}
	public String getTemplateFile() {
		return templateFile;
	}
	public void setValidEmail(Boolean validEmail) {
		this.validEmail = validEmail;
	}
	public Boolean getValidEmail() {
		return validEmail;
	}
	public void setErrorEmail(Boolean errorEmail) {
		this.errorEmail = errorEmail;
	}
	public Boolean getErrorEmail() {
		return errorEmail;
	}
	
	public void addContentMapping (String key, Object value)
	{
		this.contentMapping.put(key,value);
	}	
	
	public Map<String,Object> getContentMapping()
	{
		return this.contentMapping;
	}

	public void setSpecificEmailDistributionKey(
			String specificEmailDistributionKey) {
		this.specificEmailDistributionKey = specificEmailDistributionKey;
	}

	public String getSpecificEmailDistributionKey() {
		return specificEmailDistributionKey;
	}

	public void setIncludeSupportEmail(Boolean includeSupportEmail) {
		this.includeSupportEmail = includeSupportEmail;
	}

	public Boolean getIncludeSupportEmail() {
		return includeSupportEmail;
	}
	
	public void clearContentMapping () {
		this.contentMapping= new HashMap<String,Object>();
	}
}
