package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class ActivityTaskCodeCascadeLookupHandler implements CascadeLookupHandler{	
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		//activity, well, and operation objects are used because userSelection does not have information required by depot related processes.
		Activity activity = null;
		Well well = null;
		Operation operation = null;
		if(node != null && node.getData() instanceof Activity) {
			activity = (Activity) node.getData();
			well = ApplicationUtils.getConfiguredInstance().getCachedWell(activity.getWellUid());
			operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(activity.getOperationUid());
		} else { //assuming process is not from depot
			well = ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid());
			operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		}
		
		if(well != null && operation != null) {
			well = ApplicationUtils.getConfiguredInstance().getCachedWell(activity.getWellUid());
			operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(activity.getOperationUid());
			String sql ="FROM LookupTaskCode where  (isDeleted = false or isDeleted is null) and (operationCode =:operationCode or operationCode ='' or operationCode is null) AND " + 
					"(onOffShore =:onOffShore or onOffShore ='' or onOffShore is null) order by shortCode";
			List<LookupTaskCode> listLookupTaskCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { "operationCode", "onOffShore"}, new String[] {operation.getOperationCode(), well.getOnOffShore()});
			if (listLookupTaskCode.size() > 0) { 
				for (LookupTaskCode lookupTaskCodeResult : listLookupTaskCode) {
					LookupItem newItem = new LookupItem(lookupTaskCodeResult.getShortCode(),lookupTaskCodeResult.getName() + " (" + lookupTaskCodeResult.getShortCode() + ")");
					newItem.setActive(lookupTaskCodeResult.getIsActive());
					if ("es".equals(userSelection.getUserLanguage())) {
						if (StringUtils.isNotEmpty(lookupTaskCodeResult.getNameEs()))
							newItem.setValue(lookupTaskCodeResult.getNameEs() + " (" + newItem.getKey() + ")");
					}
					newItem.setTableUid(lookupTaskCodeResult.getLookupTaskCodeUid());
					result.put(lookupTaskCodeResult.getShortCode(), newItem);
				}
			}
		}
		return result;
	}

	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		//activity, well, and operation objects are used because userSelection does not have information required by depot related processes.
		Activity activity = null;
		Well well = null;
		Operation operation = null;
		if(node != null && node.getData() instanceof Activity) {
			activity = (Activity) node.getData();
			well = ApplicationUtils.getConfiguredInstance().getCachedWell(activity.getWellUid());
			operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(activity.getOperationUid());
		} else { //assuming process is not from depot
			well = ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid());
			operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		}
		
		if(well != null && operation != null) {
			String sql ="FROM LookupTaskCode where  (isDeleted = false or isDeleted is null) and (operationCode =:operationCode or operationCode ='' or operationCode is null) AND " + 
					"(onOffShore =:onOffShore or onOffShore ='' or onOffShore is null) and phaseShortCode =:pShortCode order by shortCode";
			List<LookupTaskCode> listLookupTaskCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { "operationCode", "onOffShore", "pShortCode"}, new String[] {operation.getOperationCode(), well.getOnOffShore(), key});
			if (listLookupTaskCode.size() > 0) { 
				for (LookupTaskCode lookupTaskCodeResult : listLookupTaskCode) {
					LookupItem newItem = new LookupItem(lookupTaskCodeResult.getShortCode(),lookupTaskCodeResult.getName() + " (" + lookupTaskCodeResult.getShortCode() + ")");
					newItem.setActive(lookupTaskCodeResult.getIsActive());
					if ("es".equals(userSelection.getUserLanguage())) {
						if (StringUtils.isNotEmpty(lookupTaskCodeResult.getNameEs()))
							newItem.setValue(lookupTaskCodeResult.getNameEs() + " (" + newItem.getKey() + ")");
					}
					newItem.setTableUid(lookupTaskCodeResult.getLookupTaskCodeUid());
					result.put(lookupTaskCodeResult.getShortCode(), newItem);
				}
			}
		}
		return result;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		String sqlPhaseCode = "FROM LookupPhaseCode "
				+ "WHERE (isDeleted IS NULL OR isDeleted=FALSE) "
				+ "AND (operationCode = :operationType OR operationCode = '' OR operationCode is null)";
		List<LookupPhaseCode> rsLookupPhaseCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlPhaseCode, 
				new String[] {"operationType"}, new String[] {userSelection.getOperationType()});
	
		if(rsLookupPhaseCode.size()>0){
			for (LookupPhaseCode lookupPhaseCode : rsLookupPhaseCode) {
				if (StringUtils.isNotBlank(lookupPhaseCode.getShortCode())){
					CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(lookupPhaseCode.getShortCode());
					cascadeLookupSet.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, lookupPhaseCode.getShortCode(), null)));
					result.add(cascadeLookupSet);
				}		
			}
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
		
	}
	
}
