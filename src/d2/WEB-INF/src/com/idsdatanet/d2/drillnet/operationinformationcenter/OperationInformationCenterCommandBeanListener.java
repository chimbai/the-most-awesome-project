package com.idsdatanet.d2.drillnet.operationinformationcenter;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;

public class OperationInformationCenterCommandBeanListener extends EmptyCommandBeanListener {
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		String filterType = request.getParameter("_action");
		
		if ("loadOICLookupFilter".equals(invocationKey)) {
			
			if (filterType!=null && StringUtils.isNotBlank(filterType)) {
				String strResult = "<loadOICLookupFilter>";
				
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT shortCode, lookupLabel FROM CommonLookup WHERE (isDeleted = false or isDeleted is null) " +
							" AND (isActive = 1 or isActive is null) AND lookupTypeSelection = :lookupTypeSelection ORDER BY sequence", "lookupTypeSelection", filterType);
				if (list!=null && !list.isEmpty()){
				
					for (Object obj : list) {
						Object[] _obj = (Object[]) obj;
						String shortCode = (String) _obj[0];
						String lookupLabel = (String) _obj[1];
						strResult = strResult + "<item key=\"" + shortCode + "\" value=\"" +  StringEscapeUtils.escapeXml(lookupLabel) + "\"/>";
					}
				}
				strResult = strResult + "</loadOICLookupFilter>";
				response.getWriter().write(strResult);
			}
			
			
		}
		
		return;
	}
}
