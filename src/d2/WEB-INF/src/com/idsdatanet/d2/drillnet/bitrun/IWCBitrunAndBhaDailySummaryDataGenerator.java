package com.idsdatanet.d2.drillnet.bitrun;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class IWCBitrunAndBhaDailySummaryDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		Date currentDate = null;
		String sql = "SELECT dayDate, dailyUid FROM Daily WHERE dailyUid = :dailyUid";
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql ,"dailyUid", userContext.getUserSelection().getDailyUid());
		
		if (result.size()>0){
			Object[] obj = (Object[])result.get(0);
			currentDate = (Date)obj[0];	
		}
		
		String[] paramsFields = {"operationUid", "currentDate"};
		Object[] paramsValues = new Object[2];
		paramsValues[0] = userContext.getUserSelection().getOperationUid();
		paramsValues[1] = currentDate;		
		
		sql = "SELECT bds.bharunDailySummaryUid, d.dayDate, br.bitrunNumber, bds.dailyUid, b.bharunUid "
				+ "FROM BharunDailySummary bds, Bitrun br, Daily d, Bharun b "
				+ "WHERE b.operationUid = :operationUid "
				+ "AND (bds.isDeleted = false OR bds.isDeleted IS NULL) AND (br.isDeleted = false OR br.isDeleted IS NULL) AND (d.isDeleted = false OR d.isDeleted IS NULL) "
				+ "AND bds.bharunUid = br.bharunUid AND bds.dailyUid = d.dailyUid AND bds.bharunUid = b.bharunUid AND d.dayDate <= :currentDate "
				+ "ORDER BY d.dayDate";
		
		result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql ,paramsFields, paramsValues);
		
		Double cumDuration = 0.0;
		Double cumTotalProgress = 0.0;
		Double cumAvgrop = 0.0;
		Double previousCumDuration = 0.0;
		Double previousCumTotalProgress = 0.0;
		Double previousCumAvgrop = 0.0;
		
		if (result.size()>0) {
			int countRecord = 1;
			for(Object[] obj:result)
			{
				String bharunDailySummaryUid = CommonUtils.null2EmptyString(obj[0]);
				Date daydate = (Date)obj[1];
				String bitrunNumber = CommonUtils.null2EmptyString(obj[2]);	
				String dailyUid = CommonUtils.null2EmptyString(obj[3]);	
				String bharunUid = CommonUtils.null2EmptyString(obj[4]);
				
				String strSql = "SELECT SUM(bds.duration) AS totalDuration, SUM(bds.progress) AS totalProgress "
						+ "FROM BharunDailySummary bds, Daily d ,Bitrun br "
						+ "WHERE (bds.isDeleted = false OR bds.isDeleted IS NULL) AND (d.isDeleted = false OR d.isDeleted IS NULL) "
						+ "AND d.dailyUid = bds.dailyUid AND d.dayDate <= :dayDate AND d.operationUid = :operationUid AND bds.bharunUid = :bharunUid "
						+ "AND bds.bharunUid = br.bharunUid";
				
				String[] paramsFields1 = {"dayDate", "operationUid", "bharunUid"};
				Object[] paramsValues1 = new Object[3];
				paramsValues1[0] = daydate;
				paramsValues1[1] = userContext.getUserSelection().getOperationUid();
				paramsValues1[2] = bharunUid;						
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1);
				Object[] thisResult = (Object[]) lstResult.get(0);
						
				//Calculate Total Duration (On Bottom Hours)
				Double totalDuration = 0.00;
				
				if (thisResult[0] != null) {
					totalDuration = Double.parseDouble(thisResult[0].toString());
				}
				
				//Calculate Total Bit Progress
				Double totalProgress = 0.00;
				
				if (thisResult[1] != null) {
					totalProgress = Double.parseDouble(thisResult[1].toString());
				}
				
				//Calculate Average ROP
				Double avgrop = 0.00;

				if (thisResult[0] != null && thisResult[1] != null) {
					if (totalDuration != 0) avgrop = totalProgress / totalDuration;
				}			
					
				cumDuration += totalDuration;
				cumTotalProgress += totalProgress;
				cumAvgrop += avgrop;
				
				if(countRecord<result.size()) {
					previousCumDuration += totalDuration;
					previousCumTotalProgress += totalProgress;
					previousCumAvgrop += avgrop;
				}				
							
				countRecord++;			
			}
			
			ReportDataNode lastCummulative = reportDataNode.addChild("lastCummulative");
			ReportDataNode previousCummulative = reportDataNode.addChild("previousCummulative");
			
			CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
		
			generateUomField(lastCummulative.addChild("lastCumDuration"),BharunDailySummary.class,thisConverter,"duration",cumDuration,false);
			generateUomField(previousCummulative.addChild("previousCumDuration"),BharunDailySummary.class,thisConverter,"duration",previousCumDuration,false);
		
			generateUomField(lastCummulative.addChild("lastCumTotalProgress"),BharunDailySummary.class,thisConverter,"progress",cumTotalProgress,false);
			generateUomField(previousCummulative.addChild("previousCumTotalProgress"),BharunDailySummary.class,thisConverter,"progress",previousCumTotalProgress,false);
		
			generateUomField(lastCummulative.addChild("lastCumAvgRop"),BharunDailySummary.class,thisConverter,"rop",cumAvgrop,false);
			generateUomField(previousCummulative.addChild("previousCumAvgRop"),BharunDailySummary.class,thisConverter,"rop",previousCumAvgrop,false);
			
		}
	}
	
	private void generateUomField(ReportDataNode node,Class refClass,CustomFieldUom thisConverter,String dataField, Object data,boolean isDatumField) throws Exception {
		generateUomField(node,refClass,thisConverter,dataField, data,isDatumField, true);
	}
	
	private void generateUomField(ReportDataNode node,Class refClass,CustomFieldUom thisConverter,String dataField, Object data,boolean isDatumField, boolean isBaseValue ) throws Exception {
		
		String rawValue = "";
		String dataUomSymbol = "";
		String formattedvalue = "";
		String displayValue = "";
		
		thisConverter.setReferenceMappingField(refClass, dataField);
		if (thisConverter.isUOMMappingAvailable()) dataUomSymbol = thisConverter.getUomSymbol().toString();
		
		if (data != null)
		{
			if (thisConverter.isUOMMappingAvailable()){
				if (!isBaseValue) {
					thisConverter.setBaseValueFromUserValue(Double.parseDouble(data.toString()));
				}
				else {
					thisConverter.setBaseValue(Double.parseDouble(data.toString()));
				}
				if (isDatumField) thisConverter.addDatumOffset();
				rawValue =  Double.toString(thisConverter.getConvertedValue()) ;
				formattedvalue = thisConverter.getFormattedValue();	
				displayValue = formattedvalue.replace(dataUomSymbol, "").trim() ;
			}
			else{
				displayValue = data.toString();
			}			
		}				
		
		node.addProperty(dataField, displayValue);
		node.addProperty(dataField + "_RawValue", rawValue);
		node.addProperty(dataField + "UomSymbol", dataUomSymbol);		
		if (StringUtils.isNotBlank(formattedvalue))
			node.addProperty(dataField + "WithUomSymbol", formattedvalue);
		else
			node.addProperty(dataField + "WithUomSymbol", displayValue);
	}
}
