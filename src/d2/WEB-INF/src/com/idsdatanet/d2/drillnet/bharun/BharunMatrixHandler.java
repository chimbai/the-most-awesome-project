package com.idsdatanet.d2.drillnet.bharun;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.matrix.DefaultMatrixHandler;
import com.idsdatanet.d2.drillnet.matrix.Matrix;

public class BharunMatrixHandler extends DefaultMatrixHandler {

	public void process(Matrix matrix, UserSelectionSnapshot userSelection) throws Exception {
		boolean f = false;
		try {
			Bharun.class.getDeclaredField("isIncomplete");
			f = true;
		} catch (NoSuchFieldException e) {
			f = false;
		}
		String sql = "";
		if(f){
			sql = "select dailySummary.dailyUid,bharun.bharunUid from Bharun bharun, BharunDailySummary dailySummary where (bharun.isDeleted = false or bharun.isDeleted is null) and (dailySummary.isDeleted = false or dailySummary.isDeleted is null) and bharun.bharunUid=dailySummary.bharunUid and bharun.operationUid = :operationUid";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", userSelection.getOperationUid());
			if(list != null && list.size() > 0) {
				Set<String> linkedHashSet = new LinkedHashSet<String>();
				String queryString = "";
				for(Object[] daily : list) {
					String dailyUid = null;
					if(daily[0] != null) {
						dailyUid = (String) daily[0];
					}
					if(daily[1] != null) {
						queryString = queryString + "'" + (String) daily[1] + "',";
					}
					linkedHashSet.add(dailyUid);
				}
				queryString = StringUtils.substring(queryString, 0, -1);
				if(queryString != "") { 
					sql = "select bharun.bharunUid,bharun.isIncomplete from Bharun bharun where (bharun.isDeleted = false or bharun.isDeleted is null) and bharun.bharunUid IN (" + queryString + ")";
					List<Object[]> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
					for(String id: linkedHashSet) {
						String dailyUid = null;
						String bharunUid = null;
						String isIncomplete = null;
						for(Object[] daily : list) {
							if(daily[0] != null) {
								dailyUid = (String) daily[0];
								if(id.equals(dailyUid)){
									if(daily[1] != null) {
										bharunUid = (String) daily[1];
										for(Object[] bha : list2){
											String bhaUid = (String) bha[0];
											if(bharunUid.equals(bhaUid)) {
												if(bha[1] != null){
													if(isIncomplete != "true") isIncomplete = String.valueOf(bha[1]);
												}
											}
										}
									}
								}
							}
						}
						matrix.addData(id,isIncomplete);
					}
				}
			}
		}else{
			sql = "select distinct dailySummary.dailyUid from Bharun bharun, BharunDailySummary dailySummary where (bharun.isDeleted = false or bharun.isDeleted is null) and (dailySummary.isDeleted = false or dailySummary.isDeleted is null) and bharun.bharunUid=dailySummary.bharunUid and bharun.operationUid = :operationUid";
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", userSelection.getOperationUid());
			if(list.size() > 0) {
				for(String dailyUid : list) {
					matrix.addData(dailyUid,"N/A");
				}
			}
		}
	}
}
