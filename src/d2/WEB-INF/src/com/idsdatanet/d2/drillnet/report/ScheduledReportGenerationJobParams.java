package com.idsdatanet.d2.drillnet.report;

public class ScheduledReportGenerationJobParams {
	public static final int REPORT_TYPE_UNKNOWN = -1;
	public static final int REPORT_TYPE_MANAGEMENT_REPORT = 1;
	public static final int REPORT_TYPE_DDR = 2;
	public static final int REPORT_TYPE_DEFAULT_REPORT = 3;
	
	public ReportManager reportManager = null;
	public int reportType = REPORT_TYPE_UNKNOWN;
	public ReportAutoEmail reportAutoEmail = null;
	public String defaultReportType = null;
	public DefaultReportModule defaultReportModule = null;
}
