package com.idsdatanet.d2.drillnet.opsDatum;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.operation.OperationUtils;

public class OpsDatumDataNodeListener extends EmptyDataNodeListener {
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		if(object instanceof OpsDatum) {			
			DatumManager.loadDatum();
			commandBean.getFlexClientControl().setReloadParentHtmlPage();
			
			OpsDatum opsDatum = (OpsDatum) node.getData();
			OperationUtils.updateWellGroundLevelBasedOnDefaultDatum(opsDatum.getOperationUid());
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		if(object instanceof OpsDatum) {
			OpsDatum opsDatum = (OpsDatum) object;
			if(StringUtils.equals(session.getCurrentUOMDatumUid(), opsDatum.getOpsDatumUid())){
				session.setCurrentUOMDatumUid(null);
			}
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update Operation set defaultDatumUid = null where operationUid = :operationUid and defaultDatumUid = :defaultDatumUid", new String[] {"operationUid","defaultDatumUid"}, new Object[] {session.getCurrentOperationUid(), opsDatum.getOpsDatumUid()});
			DatumManager.loadDatum();
			commandBean.getFlexClientControl().setReloadParentHtmlPage();
			OperationUtils.updateWellGroundLevelBasedOnDefaultDatum(opsDatum.getOperationUid());
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session,DataNodeProcessStatus status, HttpServletRequest request)throws Exception {
		Object object = node.getData();
		OpsDatumUtil.resetDatumFieldsIfIsGlReference(commandBean, object);
		if(object instanceof OpsDatum) {	
			OpsDatum opsDatum = (OpsDatum) object;
			if (opsDatum.getDatumReferencePoint() == null || opsDatum.getDatumReferencePoint() == ""){
				status.setFieldError(node, "datumReferencePoint", "No Datum Reference Point is selected. Please re-select.");
				status.setContinueProcess(false, true);
				return;
			}
			
		}
	}

	@Override
	public void beforeDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request)throws Exception {
		Object object = node.getData();
		if(object instanceof OpsDatum) {
			OpsDatum thisOpsDatum = (OpsDatum) object;
			String opsDatumUid = thisOpsDatum.getOpsDatumUid();
			if (OpsDatumUtil.checkIfOperationAssociateToDatum(opsDatumUid)){
				status.setContinueProcess(false);
				commandBean.getSystemMessage().addError("Unable to delete datum. At least one other operation is associated to the same datum.");
			}
		}
	}

}
