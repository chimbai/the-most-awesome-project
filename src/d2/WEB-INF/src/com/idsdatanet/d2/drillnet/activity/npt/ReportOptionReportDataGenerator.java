package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.Date;
import java.util.Map;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.report.ReportOption;

public class ReportOptionReportDataGenerator extends DateRangeReportDataGenerator {
	
	
	private void includeSelectedOption(UserSelectionSnapshot userSelection, ReportDataNode node)
	{
		Map<String,Object> customList = userSelection.getCustomProperties();
		for(Map.Entry<String, Object> entry : customList.entrySet())
		{
			String key = entry.getKey().replace("@", "");
			Object value = entry.getValue();
			if (value instanceof ReportOption)
			{
				this.includeReportOption(userSelection, node, (ReportOption)value, key);
			}
		}
	}
	
	
	protected void generateActualData(UserContext userContext,Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		includeSelectedOption(userContext.getUserSelection(), reportDataNode);
	}
	private void includeReportOption(UserSelectionSnapshot userSelection, ReportDataNode node, ReportOption option, String nodeName)
	{
		if (option.getIsMultiOption())
		{
			for (String code: option.getValues())
			{
				ReportDataNode childNode = node.addChild(nodeName); 
				childNode.addProperty("code", code);
				childNode.addProperty("label",option.getValue(code).toString());
			}
		}
	}
	

}
