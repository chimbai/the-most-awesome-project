package com.idsdatanet.d2.drillnet.keyPerformanceIndicator;

import java.util.List;

import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.KeyPerformanceIndicator;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class KeyPerformanceIndicatorUtils {
	
	public static void calc_npt_percent(UserSession session) throws Exception {
		Double totalNptDuration = null;
		Double totalActivityDuration = null;  
		Double totalNpt = null; 
	
		String operationUid = session.getCurrentOperationUid();
		
		String sql = "SELECT SUM(coalesce(activityDuration,0.0)) FROM Activity " +
				"WHERE (isDeleted = false OR isDeleted IS	 null) AND classCode = 'NPT' AND operationUid=:operationUid " +
				"AND carriedForwardActivityUid IS NULL " +
				"AND (isSimop = false OR isSimop IS null) " +
				"AND (isOffline = false OR isOffline is null)";
		
		String sql2 = "SELECT SUM(coalesce(activityDuration,0.0)) FROM Activity " +
			    "WHERE (isDeleted = false OR isDeleted IS null) AND operationUid=:operationUid " +
			    "AND carriedForwardActivityUid IS NULL " +
				"AND (isSimop = false OR isSimop IS null) " +
				"AND (isOffline = false OR isOffline Is null)";
		
		String[] paramsFields = {"operationUid"};
		Object[] paramsValues = {operationUid};
		List lsOper = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		List lsOperation2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql2, paramsFields, paramsValues);
		
		if(lsOper.size() > 0)
			totalNptDuration = (Double) lsOper.get(0);
		
		if(lsOperation2.size() > 0) 
			totalActivityDuration = (Double) lsOperation2.get(0);
			
		if(totalNptDuration != null && totalActivityDuration != null){ 
			totalNpt = totalNptDuration / totalActivityDuration * 100; 
		}
		if(totalNptDuration == null && totalActivityDuration != null){
			totalNpt = 0.0;       //totalNptDuration = 0; totalNpt = totalNptDuration / totalActivityDuration * 100
		} 
		
		List<KeyPerformanceIndicator> kpiList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From KeyPerformanceIndicator WHERE (isDeleted Is Null OR isDeleted = false) AND operationUid=:operationUid",new String[] {"operationUid"},new Object[] {operationUid});
		
		for(KeyPerformanceIndicator thiskeyPerformanceIndicator:kpiList){
			thiskeyPerformanceIndicator.setWellPerformNptPercentage(totalNpt);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thiskeyPerformanceIndicator);
		}
 
	}
}
	

