package com.idsdatanet.d2.drillnet.operationDvdAnnotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.OperationDvdAnnotation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class OperationDvdAnnotationDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener, DataNodeLoadHandler{

	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
	}
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		
		QueryProperties queryProperties = new QueryProperties();
		queryProperties.setUomConversionEnabled(false);
		Locale locale = null;
		UserSession userSession = UserSession.getInstance(request, true);
		if(userSession != null && userSession.getUserLocale() != null) {
			locale = userSession.getUserLocale();
		} else {
			locale = request.getLocale();
		}
		CustomFieldUom thisConverter = new CustomFieldUom(locale);
		
		Object object = targetCommandBeanTreeNode.getData();
		if (object instanceof OperationDvdAnnotation) {
			OperationDvdAnnotation a = (OperationDvdAnnotation) object;
			String dailyUid = a.getDailyUid();
			if (dailyUid==null) return;
			Date selectedTime = a.getTimeOccur();
			//if (selectedTime==null) return;
			
			Double lastDepth = 0.0;
			String strSql = "FROM Activity WHERE (isDeleted=false or isDeleted is null) and dailyUid=:dailyUid order by startDatetime";
			List<Activity> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyUid, queryProperties);
			if (rs.size()>0 && selectedTime!=null) {
				for (Activity rec : rs) {
					if (rec.getStartDatetime()!=null && rec.getDepthMdMsl()!=null) {
						int comparator = rec.getStartDatetime().compareTo(selectedTime);
						if (comparator==0) {
							lastDepth = rec.getDepthMdMsl();
							break;
						} else if (comparator==1) {
							if (lastDepth==0.0) lastDepth = rec.getDepthMdMsl();
							break;
						} else {
							lastDepth = rec.getDepthMdMsl();
						}
					}
				}
			} else {
				Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
				if (daily!=null) {
					strSql = "FROM Activity a, Daily d " +
							"WHERE (a.isDeleted=false or a.isDeleted is null) " +
							"AND (d.isDeleted=false or d.isDeleted is null) " +
							"AND d.dailyUid=a.dailyUid " +
							"AND a.operationUid=:operationUid " +
							"AND d.dayDate<:dayDate " +
							"order by d.dayDate desc, a.endDatetime desc";
					List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, 
							new String[] {"operationUid","dayDate"}, 
							new Object[] {daily.getOperationUid(), daily.getDayDate()},
							queryProperties);
					if (list.size()>0) {
						for (Object[] rec : list) {
							Activity activity = (Activity) rec[0];
							if (activity.getDepthMdMsl()!=null) {
								lastDepth = activity.getDepthMdMsl();
								break;
							}
						}
					}
				}
			}
			thisConverter.setReferenceMappingField(OperationDvdAnnotation.class, "depthOccur");
			thisConverter.setBaseValue(lastDepth);
			a.setDepthOccur(thisConverter.getConvertedValue());
		}
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
	}
	public void init(CommandBean commandBean){
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		if (meta.getTableClass().equals(OperationDvdAnnotation.class)) {
			String strSql = "FROM OperationDvdAnnotation a, Daily d " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND a.operationUid=:operationUid " +
					"order by d.dayDate, a.timeOccur";
			List<Object[]> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", UserSession.getInstance(request).getCurrentOperationUid());			
			List<Object> output_maps = new ArrayList<Object>();
			for (Object[] item : items) {
				output_maps.add(item[0]);
			}
			return output_maps;
		}
		return null;
	}

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		if (commandBean.getFlexClientControl()!=null) {
			commandBean.getFlexClientControl().setReloadParentHtmlPage();
		}
	}

	@Override
	public void afterDataNodeDelete(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		if (commandBean.getFlexClientControl()!=null) {
			commandBean.getFlexClientControl().setReloadParentHtmlPage();
		}
	}
	
}
