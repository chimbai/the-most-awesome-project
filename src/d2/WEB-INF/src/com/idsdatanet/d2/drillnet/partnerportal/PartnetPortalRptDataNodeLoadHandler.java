package com.idsdatanet.d2.drillnet.partnerportal;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.drillnet.partnerportal.PublicDDRReportList;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class PartnetPortalRptDataNodeLoadHandler implements DataNodeLoadHandler {

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		if ("ReportFiles".equals(meta.getTableClass().getSimpleName())) {
		
				List<Object> result = new ArrayList<Object>();

				String STRSQL = "FROM ReportFiles WHERE "
						+ "(isDeleted = false or isDeleted is null) "
						+ "and isPrivate = false "
						+ "and reportOperationUid=:operationUid "
						+ "ORDER BY dateGenerated DESC";
				List<ReportFiles> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(STRSQL, "operationUid", userSelection.getOperationUid());
				
				if (items.size()>0) {
					for (ReportFiles rf : items) {
						String filePath = ApplicationConfig.getConfiguredInstance().getServerRootPath() + "WEB-INF/report/pdf/"+ rf.getReportFile();
						File file = new File(filePath);
						
						if (file.exists()) {
							result.add(rf);
						}
					}
				}

			return result;
		}
		return null;
	}

}
