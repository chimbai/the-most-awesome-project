package com.idsdatanet.d2.drillnet.matrix;

import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.AjaxAutoPopulateListener;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxActionHandler;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxParameter;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;

/**
 * A simple commandBean for front end to get matrix data 
 * and make use of default configurations of BaseCommandBean at the same time.
 * @author RYAU
 *
 */
public class GoldboxMatrixCommandBean extends BaseCommandBean {
	private MatrixManager matrixManager;
	private LinkedHashMap<String, List<Matrix>> data;
	private List<ReportDaily> days;
	private LinkedHashMap<String, String> currentUsersession = new LinkedHashMap();
	private int simpleAjaxRefreshInterval = -1;
	/**
	 * Set default Matrix Manager to load matrix data and reportDaily list
	 * @param matrixManager
	 */
	public void setManager(MatrixManager matrixManager) {
		this.matrixManager = matrixManager;
	}
	/**
	 * load matrix data and load all reportDaily (days)
	 * @param request
	 * @throws Exception
	 */
	private void loadData(HttpServletRequest request) throws Exception {
		
		UserSession session = UserSession.getInstance(request);
		String operationUid = request.getParameter("operationUid");
//		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
//		this.data = matrixManager.processMatrix(userSelection, session);
//		this.days = matrixManager.getMatrixDays(userSelection);
		if(operationUid != null) {
			if (operationUid != session.getCurrentOperationUid()) {
				session.setCurrentOperationUid(operationUid);
				this.currentUsersession.put("operationUid", operationUid);
				session.refreshCachedData();
				MenuManager.getConfiguredInstance().refreshCachedData();
				ApplicationUtils.getConfiguredInstance().refreshCachedData();
				D2ApplicationEvent.refresh();
				this.getFlexClientControl().setReloadParentHtmlPage(true);
//				commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			}else {
//				this.currentUsersession.put("operationUid", operationUid);
			}
//			this.currentUsersession.put("operationUid", operationUid);
		}
		
		
	}
	/**
	 * Return matrix data
	 * @return
	 * @throws Exception
	 */
	public LinkedHashMap<String, List<Matrix>> getData() throws Exception {
		return this.data;
	}
	public String getJSONData() throws Exception {
		return JSONObject.fromObject(this.data).toString();
	}
	public String getJSONDailyUid() throws Exception {
		return JSONObject.fromObject(this.currentUsersession).toString();
	}
	/**
	 * Return a list of reportDaily according to current operationUid
	 * @return
	 * @throws Exception
	 */
	public List<ReportDaily> getDays() throws Exception {
		return this.days;
	}
	public String getJSONDays() throws Exception {
		return JSONArray.fromObject(this.days).toString();
	}
	/**
	 * BaseCommandBean load to initiate loadData()
	 */
	@Override
	public void load(HttpServletRequest request) throws Exception {
		setRoot(new CommandBeanTreeNodeImpl(this));
		this.loadData(request);
		this.setNavigationHistory(request);
	}
	
	public void setSimpleAjaxRefreshInterval(int value){
		this.simpleAjaxRefreshInterval = value;
	}
	
	protected int getSimpleAjaxRefreshInterval(){
		return this.simpleAjaxRefreshInterval;
	}
	
	public void restartRefreshChecking(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, SimpleAjaxParameter simpleAjax) throws Exception{
		if("generateReport".equals(simpleAjax.getAction())){
			simpleAjax.setResponse("Generating Report. Please wait...", "", true, this.getSimpleAjaxRefreshInterval(), null);
		}else{
			simpleAjax.setResponse(null, "Unknow command: " + simpleAjax.getAction(), false, this.getSimpleAjaxRefreshInterval(), null);
		}
	}
	
	public SimpleAjaxActionHandler getSimpleAjaxActionHandler() throws Exception {return null;}
	
	public AjaxAutoPopulateListener getAjaxAutoPopulateListener() throws Exception {return null;}
	
}
