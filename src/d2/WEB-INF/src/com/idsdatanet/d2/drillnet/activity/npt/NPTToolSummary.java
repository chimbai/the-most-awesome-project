package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.Date;

public class NPTToolSummary extends NPTSummary implements java.io.Serializable {
	
	
	private String bharunUid;
	private Date timeIn;
	private Date timeOut;
	private Double depthFromMdMsl;
	private Double depthToMdMsl;
	private String toolCategory;
	private Double toolSize;
	private String sections;
	private Boolean isCompetitor;
	public NPTToolSummary(String wellUid, String operationUid, String country,
			String lookupCompanyUid, String classCode, Double duration,
			String dailyUid, Date dayDate) {
		super(wellUid, operationUid, country, lookupCompanyUid, classCode, duration,
				dailyUid, dayDate);
		// TODO Auto-generated constructor stub
	}

	public void setBharunUid(String bharunUid) {
		this.bharunUid = bharunUid;
	}

	public String getBharunUid() {
		return bharunUid;
	}

	public void setTimeIn(Date timeIn) {
		this.timeIn = timeIn;
	}

	public Date getTimeIn() {
		return timeIn;
	}

	public void setTimeOut(Date timeOut) {
		this.timeOut = timeOut;
	}

	public Date getTimeOut() {
		return timeOut;
	}

	public void setDepthFromMdMsl(Double depthFromMdMsl) {
		this.depthFromMdMsl = depthFromMdMsl;
	}

	public Double getDepthFromMdMsl() {
		return depthFromMdMsl;
	}

	public void setDepthToMdMsl(Double depthToMdMsl) {
		this.depthToMdMsl = depthToMdMsl;
	}

	public Double getDepthToMdMsl() {
		return depthToMdMsl;
	}

	public void setToolCategory(String toolCategory) {
		this.toolCategory = toolCategory;
	}

	public String getToolCategory() {
		return toolCategory;
	}

	public void setToolSize(Double toolSize) {
		this.toolSize = toolSize;
	}

	public Double getToolSize() {
		return toolSize;
	}

	public void setSections(String sections) {
		this.sections = sections;
	}

	public String getSections() {
		return sections;
	}

	public void setIsCompetitor(Boolean isCompetitor) {
		this.isCompetitor = isCompetitor;
	}

	public Boolean getIsCompetitor() {
		return isCompetitor;
	}
}
