package com.idsdatanet.d2.drillnet.activity;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.CascadeLookupMeta;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.EquipmentFailure;
import com.idsdatanet.d2.core.model.LookupClassCode;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationPlanDetail;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.reportDaily.ReportDailyUtils;


public class ActivityCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {
	private Map<String,String> batchCodingFields = null;
	private Boolean ignore24HourChecking = false;
	private String classCode = "TB";
	private String companyName = "Baker Hughes Inteq";
	private Boolean clearEmptyDaily = false;
	private Boolean displayCompanyRCMessageOnSystemMessage = false;
	private Boolean showSystemMessageForActivity = true;
	
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		commandBean.getSystemMessage().clear();
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		UserSession session = UserSession.getInstance(request);
		if (clearEmptyDaily) 
		{
			this.unassociatedDailyRecordCleanUp(commandBean, session);
		}
				
		ActivityUtils.recalculateTotalDuration(commandBean,session.getCurrentDailyUid(),session.getCurrentOperationUid());
		
		ReportDaily rd = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(session, session.getCurrentDailyUid());
		Operation o = ApplicationUtils.getConfiguredInstance().getCachedOperation(session.getCurrentOperationUid());
		// Logic to calculate dry hole fields
		if (o != null && o.getSpudDate() != null && o.getDryHoleEndDateTime() != null) 
			ReportDailyUtils.setReportDailyDryHole(rd, o.getSpudDate(), o.getDryHoleEndDateTime());
		
		// Logic to calculate spud to td field
		if (o != null && o.getSpudDate() != null && o.getTdDateTdTime() != null)
			ReportDailyUtils.setReportDailySpudToTdDuration(rd, o.getSpudDate(), o.getTdDateTdTime());
				
		/* need not to reload page, total duration will be updated via "selective response" [ssjong]
		Boolean hasError = commandBean.getInfo().hasErrors();
		if (!hasError){
			if(commandBean.getFlexClientControl() != null){
				commandBean.getFlexClientControl().setReloadParentHtmlPage();
			}
		}
		*/
		
		// empty method to be extended for client specific usage
		this.clientSpecificProcessAfterProcessFormSubmission(commandBean, request);

	}
	
	protected void clientSpecificProcessAfterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
		// do nothing
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Boolean enabled = true;
		double totalActivityDuration = ActivityUtils.recalculateTotalDuration(commandBean,userSelection.getDailyUid(),userSelection.getOperationUid());
		String dailyUid = userSelection.getDailyUid();
		
		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CALCULATE_DAYS_SINCE_SPUD_WELL))) {
			ReportDailyUtils.autoPopulateDaySinceSpud(dailyUid, commandBean);
		}
		
		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_ACT_SHOW_WARNING_FOR_PLAN_REF))) {
			int unmappedPlanRefCount = 0;
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("Activity").values()) {
				if(node.getData() instanceof Activity) {
					Activity activity = (Activity) node.getData();
					if(activity != null && activity.getActivityUid() != null && (activity.getPlanReference() == null || activity.getPlanReference().equals(""))) {
						unmappedPlanRefCount++;
					}
				}
			}
			if(unmappedPlanRefCount > 0) {
				commandBean.getSystemMessage().addInfo(unmappedPlanRefCount + " number of entries have no Plan Reference assigned today.");
			}
		}
		
		String operationHour = "24";
		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "act_show_warning_if_not_24"))) {
			String newActivityRecordInputMode = null;
			if (request != null) {
				newActivityRecordInputMode = request.getParameter("newActivityRecordInputMode");
				UserSession session = UserSession.getInstance(request);
				if (session.getCurrentOperation()!=null) {
					operationHour = ActivityUtils.getOperationHour(session.getCurrentOperation());
				}
			}
			
			if (!("1".equals(newActivityRecordInputMode))) {
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
				thisConverter.setBaseValue(86400);
				if (StringUtils.isNotBlank(operationHour)) {
					Double totalHoursInSecond = Double.parseDouble(operationHour);
					if (!totalHoursInSecond.isNaN()) {
						totalHoursInSecond = totalHoursInSecond * 3600.0;
						thisConverter.setBaseValue(totalHoursInSecond);
					}
				}
				if (!thisConverter.getFormattedValue(totalActivityDuration).equals(thisConverter.getFormattedValue()) && !this.ignore24HourChecking) {
					commandBean.getSystemMessage().addInfo("Total hours for activities in the day (" + thisConverter.getFormattedValue(totalActivityDuration) + ") is not " + operationHour + " hours.");
				}
			}
			
			if (ActivityUtils.isTimeOverlapped(commandBean,userSelection.getDailyUid(),userSelection.getOperationUid())) {
				commandBean.getSystemMessage().addInfo("There is time overlapping / gaps between activities.");
			}
			if(isActivityDurationZero(commandBean)) {
				commandBean.getSystemMessage().addInfo("There is activity that has zero hour duration");
			}
		}
		
		Boolean showMsg = false;
		Boolean submodeIsKickOrEquipmentFailure = ActivityUtils.submodeIsKickOrEquimentFailure(commandBean, dailyUid, userSelection.getOperationUid());
		Boolean isSubmodeEmpty = ActivityUtils.isSubmodeEmpty(commandBean, dailyUid, userSelection.getOperationUid());
		if (submodeIsKickOrEquipmentFailure) {
			showMsg= true;
		}
		if (showMsg) {
			commandBean.getSystemMessage().addInfo("Kick and/or Equipment Failure details must be added for the purpose of the NPD report"); 
			commandBean.getSystemMessage().addInfo("Changes on submode (from Kick and/or Equipment Failure to others) might affect Kick and/or Equipment Failure details shown in the respective screens"); 
		}
/*		if (isSubmodeEmpty) {
			commandBean.getSystemMessage().addInfo("Changes on submode (from Kick and/or Equipment Failure to others) might affect Kick and/or Equipment Failure details shown in the respective screens"); 
		}*/
		
		Daily tomorrow = ApplicationUtils.getConfiguredInstance().getTomorrow(userSelection.getOperationUid(), userSelection.getDailyUid());
		if (tomorrow != null) { // Check if tomorrow got activity records or not
			String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid=:dailyUid and reportType!=:reportType";
			String reportType = "DGR";
			String[] paramsFields = {"dailyUid", "reportType"};
			Object[] paramsValues = {tomorrow.getDailyUid(), reportType};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			//if got next day with activity do not show button
			if (lstResult.size() > 0) {
		 		commandBean.getRoot().getDynaAttr().put("nextDayAvailable", "1");
			} else {
				commandBean.getRoot().getDynaAttr().put("nextDayAvailable", "0");
			}
		} else {
			if (!"24".equals(operationHour) && StringUtils.isNotBlank(operationHour)) {
				commandBean.getRoot().getDynaAttr().put("nextDayAvailable", "1");
			} else {
				commandBean.getRoot().getDynaAttr().put("nextDayAvailable", "0");
			}
		}
		
		List<ReportDaily> reportDailyList = ActivityUtils.getDrillnetReportDaily(dailyUid);
		if (reportDailyList == null || reportDailyList.isEmpty()) {
			commandBean.getSystemMessage().addInfo("There is no Reporting Day created for today. Please go to Daily screen to Start New Day.");
		}
		
		//SET DEFAULT INPUT MODE TO "ALL" IF NOT SELECTED
		String strInputMode  = (String) commandBean.getRoot().getDynaAttr().get("inputMode");
		if(StringUtils.isBlank(strInputMode)) {
			commandBean.getRoot().getDynaAttr().put("inputMode", "all");
		}
		
		if (this.displayCompanyRCMessageOnSystemMessage) {
			if (isRCEmptyForTroubleActivity(userSelection.getOperationUid())) {
				String rootcauseMessage = GroupWidePreference.getValue(userSelection.getGroupUid(),"messageAlertUserWhenRootCauseEmptyForActivityBatchCoding");
				if (StringUtils.isNotBlank(rootcauseMessage)) {
					commandBean.getSystemMessage().addInfo(rootcauseMessage);
				} else {
					commandBean.getSystemMessage().addInfo("Root Cause cannot be blank for activity records classified as TROUBLE.");
				}
			}
		}
		Operation currentOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		if (currentOperation!= null) {
			if (BooleanUtils.isTrue(currentOperation.getIsLocked())) {
				enabled = false;
			}
		}
		commandBean.getRoot().getDynaAttr().put("hideBatchButton", enabled);
		
		//Ticket: 21230 (OP-8283)
		String operationUid = "";
		Boolean disabled = false;
		Boolean showButton = false;
				
		if (userSelection.getOperationUid() != null) {
			operationUid = userSelection.getOperationUid();
		}
				
		if (userSelection.getDailyUid() != null) {
			dailyUid = userSelection.getDailyUid();
					
			Date currentDate = null;
					
			String strSql = "FROM Daily WHERE (isDeleted = false or isDeleted is null) and operationUid =:operationUid and dailyUid =:dailyUid";
			String[] paramsFields = {"operationUid","dailyUid"};
			Object[] paramsValues = {operationUid,dailyUid};
			List <Daily> lsDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
			if (lsDaily.size() > 0) {
				for (Daily objResult: lsDaily) {
					//to get the current date
					currentDate = objResult.getDayDate();
				}
						
				if (currentDate != null) {
					String strSql2 = "FROM Activity a, Daily d WHERE (a.isDeleted = false or a.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and a.operationUid =:operationUid and a.dailyUid = d.dailyUid and d.dayDate = '" + currentDate + "' and a.dayPlus != '1' ";
					List <Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "operationUid", operationUid);
							
					if (lstResult.isEmpty()) {
						disabled = true;
					}
				}	
			}
		}
		commandBean.getRoot().getDynaAttr().put("checkExistingActivity", disabled);
				
		Daily yesterday = ApplicationUtils.getConfiguredInstance().getYesterday(operationUid, dailyUid);
					
		if (yesterday != null) {
			//to get yesterday date
			Date yesterdayDate = yesterday.getDayDate();
					
			String strSql = "FROM Activity a, Daily d WHERE (a.isDeleted = false or a.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and a.operationUid =:operationUid and a.dailyUid=d.dailyUid and d.dayDate = '" + yesterdayDate + "' and a.dayPlus != '1' ";
			List <Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
						
			if (lstResult.size() > 0) {	
				showButton = true;
			}
		} 
		commandBean.getRoot().getDynaAttr().put("showCopyLastFromYesterdayButton", showButton);
	}
	
	private Boolean isActivityDurationZero(CommandBean commandBean) throws Exception {
		
		for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("Activity").values()) {
			Activity activity = (Activity) node.getData();
			if (activity.getIsDeleted() == null || !activity.getIsDeleted()) { 					
			    if (activity != null && activity.getActivityDuration() != null) {
					if (activity.getActivityDuration() == 0) {
							return true;
				    }
				}
			}
		}
		
		for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("NextDayActivity").values()) {
			NextDayActivity nextDayActivity = (NextDayActivity) node.getData();
			if (nextDayActivity.getIsDeleted() == null || !nextDayActivity.getIsDeleted()) {				
				if (nextDayActivity != null && nextDayActivity.getActivityDuration() != null) {
					if (nextDayActivity.getActivityDuration() == 0) {
						return true;
				    }
				}
			}
		}
		
		return false;
	}
	
	private Boolean isRCEmptyForTroubleActivity(String operationUid) throws Exception {
		
		String strSql = "SELECT a FROM Activity a, Daily d " +
					"WHERE (a.isDeleted = false or a.isDeleted is null) AND (d.isDeleted = false or d.isDeleted is null)" +
					" AND a.dailyUid = d.dailyUid " +
					"AND (a.dayPlus is null or a.dayPlus = 0) " +
					"AND a.operationUid = :operationUid " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND a.internalClassCode in ('TP', 'TU') AND (a.rootCauseCode is null or a.rootCauseCode='')";
		
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
		if(list.size() > 0) {
			return true;
		}
		return false;
		
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		String strInputMode  = (String) commandBean.getRoot().getDynaAttr().get("inputMode");
		String autoPopulateDescription  = (String) request.getParameter("auto_populate_description");
		Boolean SysPass = true;
		Object object = targetCommandBeanTreeNode.getData();
		String internal_classcode = (String) targetCommandBeanTreeNode.getDynaAttr().get("classCode_internalCode");
		if(StringUtils.isNotBlank(autoPopulateDescription)){	
			SysPass = false;
		}
		if ("planReference".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())) {
			if("P".equals(internal_classcode)||"TP".equals(internal_classcode)) {
				this.updatePropertiesBasedOnPlanReference(object);
			}
			SysPass = false;
		}
		if ("planTaskReference".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			if("P".equals(internal_classcode)||"TP".equals(internal_classcode)) {
				this.updatePropertiesBasedOnPlanTaskReference(object);
			}
			SysPass = false;
		}
		if ("planDetailReference".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			if("P".equals(internal_classcode)||"TP".equals(internal_classcode)) {
				this.updatePropertiesBasedOnPlanDetailReference(object);
			}
			SysPass = false;
		}
		if ("classCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			UserSession session = UserSession.getInstance(request);
			String classCode = (String) PropertyUtils.getProperty(object, "classCode");
			if(StringUtils.isBlank(classCode)) {
				PropertyUtils.setProperty(object, "planReference",null);
				PropertyUtils.setProperty(object, "planTaskReference",null);
				PropertyUtils.setProperty(object, "planDetailReference",null); 
			} else {
				String internal_cc = this.getInternalCode(classCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
				if("P".equals(internal_cc)||"TP".equals(internal_cc)) {
					this.updateProperties(object);
				} 
			}
			SysPass = false;
		}
		if ("phaseCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
		}
		if ("jobTypeCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
		}
		if ("taskCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
		}
		if ("userCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
		}
		if ("submode1".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
            	PropertyUtils.setProperty(object, "downtimeEventNumber",null); 
				PropertyUtils.setProperty(object, "lookupCompanyUid",null);
		}
		
		if ("downtimeEventNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
			String downtimeEventNumber = (String) PropertyUtils.getProperty(object, "downtimeEventNumber");
			 if (downtimeEventNumber != null && StringUtils.isNotBlank(downtimeEventNumber)) {
                List<EquipmentFailure> equipmentFailure = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM EquipmentFailure WHERE (isDeleted = FALSE or isDeleted IS NULL) AND equipmentFailureUid = :downtimeEventNumber", "downtimeEventNumber", downtimeEventNumber);
                if (!equipmentFailure.isEmpty()) {
                	EquipmentFailure ef = equipmentFailure.get(0);
           	 			PropertyUtils.setProperty(object, "lookupCompanyUid", ef.getCompany());
                }
			 }
		}
		//field to be made obsolete from activity
		/*if ("plugBumped".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
		}
		if ("floatHolding".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
		}*/
		if(StringUtils.isBlank(strInputMode))
		{
			commandBean.getRoot().getDynaAttr().put("inputMode", "all");
		}
		if(SysPass){		
			if("all".equalsIgnoreCase(commandBean.getRoot().getDynaAttr().get("inputMode").toString()))
			{
				commandBean.setSupportedAction("add", true);
				commandBean.setSupportedAction("copySelected", true);
				commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			}
			else
			{
				commandBean.setSupportedAction("add", false);
				commandBean.setSupportedAction("copySelected", false);
				commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			}
		}
		if(targetCommandBeanTreeNode != null && targetCommandBeanTreeNode.getDataDefinition() != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(Activity.class)){
				if("phaseCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){ //endor - 20120309-0638-ekueh
					ActivityUtils.autoCalculateTripNumber(UserSession.getInstance(request), targetCommandBeanTreeNode);
				}
			}
			
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(NextDayActivity.class)){
				if("phaseCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){ //endor - 20120309-0638-ekueh
					ActivityUtils.autoCalculateTripNumber(UserSession.getInstance(request), targetCommandBeanTreeNode);
				}
			}
			
		}
	}
	
	private void updateBatchCoding(HttpServletRequest request,HttpServletResponse response, CommandBean commandBean) throws Exception {
		for (String uid : this.getSelectedNodesUid(request))
		{
			Object obj = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Activity.class, uid);
			if (obj!=null)
			{
				boolean hasClassCode=false;
				Activity act = (Activity) obj;
				String fields = request.getParameter("fields");
				if (StringUtils.isNotBlank(fields))
				{
					for (String def : fields.split("[,]"))
					{
						if (def.equalsIgnoreCase("classCode"))
							hasClassCode = true;
						String value =request.getParameter(def); 
						Class type =PropertyUtils.getPropertyType(act, def);
					
						if(type == Boolean.class)
						{
							Boolean bol = "1".equalsIgnoreCase(value);
							PropertyUtils.setProperty(act,def, "-".equalsIgnoreCase(value)?null:bol);
						}else{
							PropertyUtils.setProperty(act, def, "-".equalsIgnoreCase(value)?null:value);
						}
					}
					if (hasClassCode)
					{
						if (this.getClassCode().equalsIgnoreCase(act.getClassCode()))
							act.setLookupCompanyUid(this.getCompanyName());
					}
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(act);
				}
			}
			
		}		
	}
	
	private List<String> getSelectedNodesUid(HttpServletRequest request) throws Exception
	{
		String value = request.getParameter("primaryKey");
		List list = new ArrayList();
		for (String str:value.split(","))
		{
			list.add(str);
		}
		return list;
	}
	
	private void unassociatedDailyRecordCleanUp(CommandBean commandBean, UserSession session) throws Exception {
		
		String thisOperationUid = session.getCurrentOperationUid();
		String strSql = "UPDATE Daily SET isDeleted = 1 WHERE dailyUid NOT IN (SELECT DISTINCT dailyUid FROM Activity WHERE operationUid = :activityOperationUid AND " +
			"(isDeleted IS NULL OR isDeleted = 0)) AND operationUid = :dailyOperationUid";
		String[] paramsFields = {"activityOperationUid","dailyOperationUid"};
		Object[] paramsValues = {thisOperationUid,thisOperationUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
		
		String strSql2 = "UPDATE ReportDaily SET isDeleted = 1 WHERE dailyUid NOT IN (SELECT DISTINCT dailyUid FROM Activity WHERE operationUid = :activityOperationUid AND " +
			"(isDeleted IS NULL OR isDeleted = 0)) AND operationUid = :dailyOperationUid";
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields, paramsValues);
		
		D2ApplicationEvent.refresh();
	}
	private Map<String,LookupItem> getLookup(BaseCommandBean commandBean, HttpServletRequest request, String className, String field) throws Exception
	{
		UserSelectionSnapshot selection = new UserSelectionSnapshot(UserSession.getInstance(request));
		Object handler = null;
		if (commandBean.isCascadeLookup(className, field))
		{
			CascadeLookupMeta meta = commandBean.getCascadeLookupMeta(className, field);
			if (meta!=null)
			handler = meta.getLookupUriOrHandler();
		}
		if (commandBean.isLookup(className, field))
		{
			handler = commandBean.getLookup().get(className+"."+field);
		}
		if (handler !=null)
		{
			if (handler instanceof LookupHandler)
				return ((LookupHandler)handler).getLookup(commandBean, commandBean.getRoot(), selection, request, null);
			if (handler instanceof String)
				return LookupManager.getConfiguredInstance().getLookup(handler.toString(), selection, null);
		}
		return null;
	}
	
	private void collectBatchCodingBinding(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean) throws Exception{
		if (this.batchCodingFields!=null)
		{
			SimpleXmlWriter writer = new SimpleXmlWriter(response); 
			writer.startElement("root");
			for (Map.Entry<String, String> entry : this.batchCodingFields.entrySet())
			{
				String key = entry.getKey();
				String title = entry.getValue();
				String className = key.split("[.]")[0];
				String field = key.split("[.]")[1];
				Map<String,LookupItem> lookupItems = getLookup(commandBean,request, className, field);
				writer.startElement("codingDefinition");
				writer.addElement("fieldName", field);
				writer.addElement("title", title);
				if (lookupItems!=null && lookupItems.size()>0)
				{
					for (Map.Entry<String, LookupItem> entryLookup : lookupItems.entrySet())
					{
						if(BooleanUtils.isNotFalse(entryLookup.getValue().getActive())){
							writer.startElement("lookupItem");
							writer.addElement("code", entryLookup.getValue().getKey());
							writer.addElement("label", entryLookup.getValue().getValue().toString());
							writer.endElement();
						}
					}
				}
				writer.endElement();
			}
			writer.endElement();
			writer.close();
		}
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("collectBatchCodingDefinition".equalsIgnoreCase(invocationKey))
		{
			this.collectBatchCodingBinding(request, response, commandBean);
		}
		if ("updateBatchCoding".equalsIgnoreCase(invocationKey))
		{
			this.updateBatchCoding(request, response, commandBean);
		}
		if ("collectLessonLearnedRecord".equalsIgnoreCase(invocationKey))
		{
			this.collectLessonLearnedRecord(request, response, commandBean);
		}
		if ("collectPlannedLessonTicketRecord".equalsIgnoreCase(invocationKey))
		{
			this.collectPlannedLessonTicketRecord(request, response, commandBean);
		}
		if ("activityByOperationList".equalsIgnoreCase(invocationKey)) {
			
			String activityUidSelected = request.getParameter("activityUidSelected");

			if(StringUtils.isNotBlank(activityUidSelected)){
				//change activity code
				
				String selectedActivityUid = activityUidSelected;
				
				String selectedSectionCode = request.getParameter("sectionSelected");
				String selectedClassCode = request.getParameter("classCodeSelected");
				String selectedPhaseCode = request.getParameter("phaseCodeSelected");
				String selectedTaskCode = request.getParameter("taskCodeSelected");
				//String selectedRootCauseCode = request.getParameter("rootCauseCodeSelected");
				String selectedVendor = request.getParameter("vendorSelected");
				String selectedOfInterest = request.getParameter("ofInterestSelected");
				String selectedBusinessSegment = request.getParameter("businessSegmentSelected");
				String selectedNptMainCategory = request.getParameter("nptMainCategorySelected");
				String selectedNptSubCategory = request.getParameter("nptSubCategorySelected");
				String selectedNptSubject = request.getParameter("nptSubjectSelected");
				String selectedBhaNumber = request.getParameter("bhaNumberSelected");
								
				if (StringUtils.isNotBlank(selectedActivityUid)) {
					for (String item : selectedActivityUid.split(",")) {
						
						String activityUid = item.trim();
						
						if (StringUtils.isNotBlank(activityUid)) {
							//String[] paramsFields = {"sections","classCode","phaseCode","activityUid"};
							//Object[] paramsValues = {selectedSectionCode,selectedClassCode, selectedPhaseCode, activityUid};
							
							if (StringUtils.isNotBlank(selectedSectionCode)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedSectionCode)?null:selectedSectionCode;
								String strSql = "UPDATE Activity SET sections=:sections where activityUid=:activityUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"sections", "activityUid"} , new Object[] {selectedCode, activityUid});
							}
							
							if (StringUtils.isNotBlank(selectedClassCode)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedClassCode)?null:selectedClassCode;
								String strSql = "UPDATE Activity SET classCode=:classCode where activityUid=:activityUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"classCode", "activityUid"} , new Object[] {selectedCode, activityUid});
							}
							
							if (StringUtils.isNotBlank(selectedPhaseCode)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedPhaseCode)?null:selectedPhaseCode;
								String strSql = "UPDATE Activity SET phaseCode=:phaseCode where activityUid=:activityUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"phaseCode", "activityUid"} , new Object[] {selectedCode, activityUid});
							}
							
							if (StringUtils.isNotBlank(selectedTaskCode)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedTaskCode)?null:selectedTaskCode;
								String strSql = "UPDATE Activity SET taskCode=:taskCode where activityUid=:activityUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"taskCode", "activityUid"} , new Object[] {selectedCode, activityUid});
							}
							
							/*if (StringUtils.isNotBlank(selectedRootCauseCode)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedRootCauseCode)?null:selectedRootCauseCode;
								String strSql = "UPDATE Activity SET rootCauseCode=:rootCauseCode where activityUid=:activityUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"rootCauseCode", "activityUid"} , new Object[] {selectedCode, activityUid});
							}*/
							
							if (StringUtils.isNotBlank(selectedVendor)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedVendor)?null:selectedVendor;
								String strSql = "UPDATE Activity SET lookupCompanyUid=:vendor where activityUid=:activityUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"vendor", "activityUid"} , new Object[] {selectedCode, activityUid});
							}
							
							if (StringUtils.isNotBlank(selectedOfInterest)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedOfInterest)?null:selectedOfInterest;
								
								String strSql = "";
								
								if (("1").equalsIgnoreCase(selectedCode)) {
									 strSql = "UPDATE Activity SET ofInterest=1 where activityUid=:activityUid";
								}else if (("0").equalsIgnoreCase(selectedOfInterest)) {
									 strSql = "UPDATE Activity SET ofInterest=0 where activityUid=:activityUid";
								}else {
									 strSql = "UPDATE Activity SET ofInterest=null where activityUid=:activityUid";
								}
	
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"activityUid"} , new Object[] {activityUid});
							}
							
							
							if (StringUtils.isNotBlank(selectedBusinessSegment)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedBusinessSegment)?null:selectedBusinessSegment;
								String strSql = "UPDATE Activity SET businessSegment=:businessSegment where activityUid=:activityUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"businessSegment", "activityUid"} , new Object[] {selectedCode, activityUid});
							}
							
							if (StringUtils.isNotBlank(selectedNptMainCategory)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedNptMainCategory)?null:selectedNptMainCategory;
								String strSql = "UPDATE Activity SET nptMainCategory=:nptMainCategory where activityUid=:activityUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"nptMainCategory", "activityUid"} , new Object[] {selectedCode, activityUid});
							}
							
							if (StringUtils.isNotBlank(selectedNptSubCategory)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedNptSubCategory)?null:selectedNptSubCategory;
								String strSql = "UPDATE Activity SET nptSubCategory=:nptSubCategory where activityUid=:activityUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"nptSubCategory", "activityUid"} , new Object[] {selectedCode, activityUid});
							}
							
							if (StringUtils.isNotBlank(selectedNptSubject)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedNptSubject)?null:selectedNptSubject;
								String strSql = "UPDATE Activity SET nptSubject=:nptSubject where activityUid=:activityUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"nptSubject", "activityUid"} , new Object[] {selectedCode, activityUid});
							}
							
							if (StringUtils.isNotBlank(selectedBhaNumber)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedBhaNumber)?null:selectedBhaNumber;
								String strSql = "UPDATE Activity SET bhaNumber=:bhaNumber where activityUid=:activityUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"bhaNumber", "activityUid"} , new Object[] {selectedCode, activityUid});
							}

						}
					}
				}

			}
			
			SimpleXmlWriter writer = new SimpleXmlWriter(response); 
	
			UserSession session = UserSession.getInstance(request);
			String operationUid = session.getCurrentOperationUid();
			String operationCode = session.getCurrentOperationType();
			String onOffShore = session.getCurrentWellOnOffShore();

			DateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			CustomFieldUom thisConverter = new CustomFieldUom(session.getUserLocale());
			writer.startElement("root");
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select d.dayDate, a.activityUid, a.dailyUid, " +
					"a.startDatetime, a.endDatetime, a.depthMdMsl, a.activityDuration, a.sections, a.classCode, a.phaseCode, a.activityDescription, " +
					"a.taskCode, a.rootCauseCode, a.businessSegment, a.ofInterest, a.lookupCompanyUid, a.nptMainCategory, a.nptSubCategory, a.nptSubject, a.bhaNumber From Activity a, Daily d " +
					"where (a.isDeleted is null or a.isDeleted = false) and (d.isDeleted is null or d.isDeleted = false) and " +
					"a.operationUid=:operationUid and a.dailyUid=d.dailyUid and (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') order by d.dayDate, a.startDatetime, a.endDatetime", "operationUid", operationUid, qp);
			
			if (list.size() > 0){
				
				for (Object[] rec : list) {
					writer.startElement("ActivityByOperation");
					writer.addElement("activityDate", (String) dateFormatter.format(rec[0]));
					writer.addElement("activityUid", (String) rec[1]);
					writer.addElement("dailyUid", (String) rec[2]);
					writer.addElement("startDateTime", CommonUtil.getConfiguredInstance().showTimeAs2400(rec[3]));
					writer.addElement("endDateTime", CommonUtil.getConfiguredInstance().showTimeAs2400(rec[4]));
						
					if (rec[5] !=null){
						thisConverter = new CustomFieldUom(session.getUserLocale(), Activity.class, "depthMdMsl");
						thisConverter.setBaseValue(Double.parseDouble(rec[5].toString()));
						writer.addElement("depthMdMsl",thisConverter.getFormattedValue());
					}else {
						writer.addElement("depthMdMsl","");
					}
					
					if (rec[6] !=null) {
						thisConverter = new CustomFieldUom(session.getUserLocale(), Activity.class, "activityDuration");
						thisConverter.setBaseValue(Double.parseDouble(rec[6].toString()));
						writer.addElement("duration", thisConverter.getFormattedValue());
					}else {
						writer.addElement("duration", "");
					}
					
					writer.addElement("sections", (String) this.nullToEmptyString(rec[7]));
					writer.addElement("classCode", (String) this.nullToEmptyString(rec[8]));
					writer.addElement("phaseCode", (String) this.nullToEmptyString(rec[9]));
					writer.addElement("description", (String) this.nullToEmptyString(rec[10]));
					writer.addElement("taskCode", (String) this.nullToEmptyString(rec[11]));
					writer.addElement("rootCauseCode", (String) this.nullToEmptyString(rec[12]));
					writer.addElement("businessSegment", (String) this.nullToEmptyString(rec[13]));
					writer.addElement("ofInterest", (String) this.nullToEmptyString(rec[14]));
					writer.addElement("vendor", (String) this.nullToEmptyString(rec[15]));
					writer.addElement("nptMainCategory", (String) this.nullToEmptyString(rec[16]));
					writer.addElement("nptSubCategory", (String) this.nullToEmptyString(rec[17]));
					writer.addElement("nptSubject", (String) this.nullToEmptyString(rec[18]));
					writer.addElement("bhaNumber", (String) this.nullToEmptyString(rec[19]));
					writer.endElement();
				}
				
				//get sections
				Set<Map.Entry<String, LookupItem>> lookupResult = LookupManager.getConfiguredInstance().getLookup("xml://Activity.sections?key=code&amp;value=label", new UserSelectionSnapshot(UserSession.getInstance(request)),null).entrySet();
				writer.startElement("LookupSections");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupSections");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();
				
				for (Map.Entry entry: lookupResult) {
					LookupItem x = (LookupItem) entry.getValue();
					String lookupKey = entry.getKey().toString();
					String lookupValue = x.getValue().toString();
					writer.startElement("LookupSections");
					writer.addElement("code", lookupKey);
					writer.addElement("label", lookupValue);				
					writer.endElement();
					
				}
				
				//get lookup class code
				List<LookupClassCode> lookupClassCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From LookupClassCode where " +
						"(operationCode=:operationCode or operationCode = '' or operationCode is null) and (isDeleted is null or isDeleted = false) " +
						"group by shortCode order by sequence", "operationCode", operationCode);
				
				
				
				writer.startElement("LookupClassCode");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupClassCode");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();

				if (lookupClassCode.size() > 0){
					for (LookupClassCode rec : lookupClassCode) {
						writer.startElement("LookupClassCode");
						writer.addElement("code", rec.getShortCode());
						writer.addElement("label", rec.getName() + " (" + rec.getShortCode() + ")");
						writer.endElement();
						
					}
				}
				
				//get lookup phase code
				List<LookupPhaseCode> lookupPhaseCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From LookupPhaseCode where " +
						"(operationCode=:operationCode or operationCode = '' or operationCode is null) and (isDeleted is null or isDeleted = false) " +
						"and (onOffShore=:onOffShore or onOffShore='' or onOffShore is null) group by shortCode order by name", new String[] {"operationCode", "onOffShore"}, new Object[] {operationCode, onOffShore});
				
				writer.startElement("LookupPhaseCode");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupPhaseCode");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();
				
				if (lookupPhaseCode.size() > 0){
					for (LookupPhaseCode rec : lookupPhaseCode) {
						writer.startElement("LookupPhaseCode");
						writer.addElement("code", rec.getShortCode());
						writer.addElement("label", rec.getName() + " (" + rec.getShortCode() + ")");
						writer.endElement();
						
					}
				}

				//get task code
				List<LookupTaskCode> lookupTaskCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From LookupTaskCode where " +
						"(operationCode=:operationCode or operationCode = '' or operationCode is null) and (isDeleted is null or isDeleted = false) " +
						"and (onOffShore=:onOffShore or onOffShore='' or onOffShore is null) group by shortCode order by sequence", new String[] {"operationCode", "onOffShore"}, new Object[] {operationCode, onOffShore});
				
				Collections.sort(lookupTaskCode, new LookupTaskCodeComparator());
				writer.startElement("LookupTaskCode");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupTaskCode");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();
				
				if (lookupTaskCode.size() > 0){
					for (LookupTaskCode rec : lookupTaskCode) {
						writer.startElement("LookupTaskCode");
						writer.addElement("code", rec.getShortCode());
						writer.addElement("label", rec.getName() + " (" + rec.getShortCode() + ")");
						writer.endElement();
						
					}
				}
				
				//get RootCause code
				/*List<LookupRootCauseCode> lookupRootCause = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From LookupRootCauseCode where " +
						"(operationCode=:operationCode or operationCode = '' or operationCode is null) and (isDeleted is null or isDeleted = false) " +
						"group by shortCode order by name", new String[] {"operationCode"}, new Object[] {operationCode});
				
				writer.startElement("LookupRootCauseCode");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupRootCauseCode");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();
				
				if (lookupRootCause.size() > 0){
					for (LookupRootCauseCode rec : lookupRootCause) {
						writer.startElement("LookupRootCauseCode");
						writer.addElement("code", rec.getShortCode());
						writer.addElement("label", rec.getName() + " (" + rec.getShortCode() + ")");
						writer.endElement();
						
					}
				}*/
				
				//get vendor lookup
				List<LookupCompany> lookupVendor = ApplicationUtils.getConfiguredInstance().getDaoManager().find("From LookupCompany where " +
						"(isDeleted is null or isDeleted = false) order by companyName");
				
				writer.startElement("LookupCompany");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupCompany");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();
				
				if (lookupVendor.size() > 0){
					for (LookupCompany rec : lookupVendor) {
						writer.startElement("LookupCompany");
						writer.addElement("code", rec.getLookupCompanyUid());
						writer.addElement("label", rec.getCompanyName());
						writer.endElement();
						
					}
				}
								
				//get Business Segment Lookup
				Set<Map.Entry<String, LookupItem>> lookupBusinessSegment = LookupManager.getConfiguredInstance().getLookup("xml://Activity.businessSegment?key=code&amp;value=label", new UserSelectionSnapshot(UserSession.getInstance(request)),null).entrySet();
				
				writer.startElement("LookupBusinessSegment");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupBusinessSegment");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();
				
				for (Map.Entry entry: lookupBusinessSegment) {
					LookupItem x = (LookupItem) entry.getValue();
					String lookupKey = entry.getKey().toString();
					String lookupValue = x.getValue().toString();
					writer.startElement("LookupBusinessSegment");
					writer.addElement("code", lookupKey);
					writer.addElement("label", lookupValue);				
					writer.endElement();
					
				}
				
				
				//get of interest Lookup
				Set<Map.Entry<String, LookupItem>> lookupOfInterest = LookupManager.getConfiguredInstance().getLookup("xml://yes_no?key=code&amp;value=label", new UserSelectionSnapshot(UserSession.getInstance(request)),null).entrySet();
				
				writer.startElement("LookupOfInterest");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupOfInterest");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();
				
				for (Map.Entry entry: lookupOfInterest) {
					LookupItem x = (LookupItem) entry.getValue();
					String lookupKey = entry.getKey().toString();
					String lookupValue = x.getValue().toString();
					writer.startElement("LookupOfInterest");
					writer.addElement("code", lookupKey);
					writer.addElement("label", lookupValue);				
					writer.endElement();
					
				}
				
				//get nptMainCategory 				
				Set<Map.Entry<String, LookupItem>> lookupNptMainCategory = LookupManager.getConfiguredInstance().getLookup("xml://Activity.nptMainCategory?key=code&amp;value=label", new UserSelectionSnapshot(UserSession.getInstance(request)),null).entrySet();
				
				writer.startElement("LookupNptMainCategory");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupNptMainCategory");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();
				
				for (Map.Entry entry: lookupNptMainCategory) {
					LookupItem x = (LookupItem) entry.getValue();
					String lookupKey = entry.getKey().toString();
					String lookupValue = x.getValue().toString();
					writer.startElement("LookupNptMainCategory");
					writer.addElement("code", lookupKey);
					writer.addElement("label", lookupValue);				
					writer.endElement();
					
				}
				
				
				//get nptSubCategory 	
				
				CascadeLookupSet[] cascadeLookupNptSubCategory = LookupManager.getConfiguredInstance().getCompleteCascadeLookupSet("xml://Activity.nptSubCategory?key=code&amp;value=label", new UserSelectionSnapshot(UserSession.getInstance(request)));
				writer.startElement("LookupNptSubCategory");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.addElement("groupName", "");
				writer.endElement();
				
				writer.startElement("LookupNptSubCategory");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.addElement("groupName", "-");
				writer.endElement();
			
				for(CascadeLookupSet cascadeLookup : cascadeLookupNptSubCategory){
					Map<String, LookupItem> lookupList = cascadeLookup.getLookup();
					for(String value : lookupList.keySet()){
						writer.startElement("LookupNptSubCategory");
						writer.addElement("code", value);
						writer.addElement("label", lookupList.get(value).getValue().toString());
						writer.addElement("groupName", cascadeLookup.getKey().toString());
						writer.endElement();
					}
				}
				
				//get nptSubject lookup
				Set<Map.Entry<String, LookupItem>> lookupNptSubject = LookupManager.getConfiguredInstance().getLookup("xml://npt_subject?key=code&amp;value=label", new UserSelectionSnapshot(UserSession.getInstance(request)),null).entrySet();
				
				writer.startElement("LookupNptSubject");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupNptSubject");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();
				
				for (Map.Entry entry: lookupNptSubject) {
					LookupItem x = (LookupItem) entry.getValue();
					String lookupKey = entry.getKey().toString();
					String lookupValue = x.getValue().toString();
					writer.startElement("LookupNptSubject");
					writer.addElement("code", lookupKey);
					writer.addElement("label", lookupValue);				
					writer.endElement();
					
				}
				
				writer.endElement();
				writer.close();
			
			
			}
		
		}
		
	}
	
	private void collectLessonLearnedRecord(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		String activityUid = request.getParameter("activityUid");

		if (!activityUid.isEmpty()) {
			String strSql = "SELECT l.lessonTicketUid, l.lessonTicketNumberPrefix, l.lessonTicketNumber " +
							"FROM ActivityLessonTicketLink a, LessonTicket l " +
							"WHERE (a.isDeleted = FALSE OR a.isDeleted IS NULL) " +
							"AND (l.isDeleted = FALSE OR l.isDeleted IS NULL) " +
							"AND l.lessonTicketUid = a.lessonTicketUid " +
							"AND a.activityUid = :activityUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "activityUid", activityUid);

			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			
			if (!lstResult.isEmpty()) {
				for (int i = 0; i < lstResult.size(); i++) {
					Object[] lt = (Object[]) lstResult.get(i);
					String lessonTicketPrefix = "", lessonTicketNumber = "";
	
					if (StringUtils.isNotBlank((String) lt[1]))
						lessonTicketPrefix = lt[1].toString();
					
					if (StringUtils.isNotBlank((String) lt[2]))
						lessonTicketNumber = lessonTicketPrefix + " - " + lt[2].toString();
	
					writer.startElement("LessonTicket");
					writer.addElement("id", lt[0].toString());
					writer.addElement("label", lessonTicketNumber);
					writer.endElement();
				}
			}
			writer.endElement();
			writer.close();
		}
	}
	
	private void collectPlannedLessonTicketRecord(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(UserSession.getInstance(request));
		String wellUid = userSelection.getWellUid();	
		String phaseCode = (!StringUtils.isBlank(request.getParameter("phaseCode"))) ? request.getParameter("phaseCode") : "";
		String taskCode = (!StringUtils.isBlank(request.getParameter("taskCode"))) ? request.getParameter("taskCode") : "";

		String strSql = "SELECT lessonTicketUid, lessonTicketNumberPrefix, lessonTicketNumber FROM LessonTicket l " +
						"WHERE (l.isDeleted = FALSE OR l.isDeleted IS NULL) " +
						"AND l.phaseCode = :phaseCode " + 
						"AND l.taskCode = :taskCode " + 
						"AND l.wellUid = :wellUid " + 
						"AND l.lessonReportType = 'plan'";
		String[] paramsFields = {"phaseCode", "taskCode", "wellUid"};
		String[] paramsValues = {phaseCode, taskCode, wellUid};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);

		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
			
		if (!lstResult.isEmpty()) {
			for (int i = 0; i < lstResult.size(); i++) {
				Object[] lt = (Object[]) lstResult.get(i);
				String lessonTicketPrefix = "", lessonTicketNumber = "";
	
				if (StringUtils.isNotBlank((String) lt[1]))
					lessonTicketPrefix = lt[1].toString();
					
				if (StringUtils.isNotBlank((String) lt[2]))
					lessonTicketNumber = "PLL - " + lessonTicketPrefix + " - " + lt[2].toString();
	
				writer.startElement("LessonTicket");
				writer.addElement("id", lt[0].toString());
				writer.addElement("label", lessonTicketNumber);
				writer.endElement();
			}
		}
		writer.endElement();
		writer.close();
	}

	private String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}
	
	public void setBatchCodingFields(Map<String,String> batchCodingFields) {
		this.batchCodingFields = batchCodingFields;
	}

	public Map<String,String> getBatchCodingFields() {
		return batchCodingFields;
	}

	public void setIgnore24HourChecking(Boolean ignore24HourChecking) {
		this.ignore24HourChecking = ignore24HourChecking;
	}

	public Boolean getIgnore24HourChecking() {
		return ignore24HourChecking;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClearEmptyDaily(Boolean clearEmptyDaily) {
		this.clearEmptyDaily = clearEmptyDaily;
	}

	public Boolean getClearEmptyDaily() {
		return clearEmptyDaily;
	}

	public void setShowSystemMessageForActivity(Boolean showSystemMessageForActivity) {
		this.showSystemMessageForActivity = showSystemMessageForActivity;
	}

	public Boolean getShowSystemMessageForActivity() {
		return showSystemMessageForActivity;
	}
	
	public void setDisplayCompanyRCMessageOnSystemMessage(
			Boolean displayCompanyRCMessageOnSystemMessage) {
		this.displayCompanyRCMessageOnSystemMessage = displayCompanyRCMessageOnSystemMessage;
	}

	public Boolean getDisplayCompanyRCMessageOnSystemMessage() {
		return displayCompanyRCMessageOnSystemMessage;
	}

	private class LookupTaskCodeComparator implements  Comparator<LookupTaskCode>{
		public int compare(LookupTaskCode o1, LookupTaskCode o2){
			try{
				
				String s1 = WellNameUtil.getPaddedStr(o1.getShortCode());
				String s2 = WellNameUtil.getPaddedStr(o2.getShortCode());
				
				if (s1 == null || s2 == null) return 0;
				return s1.compareTo(s2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
		//ticket#20110906-1338-ysim: SANTOS CSM - Validation Check: Total activity duration should not exceed 24hrs
		UserSession session = UserSession.getInstance(request);
		
		if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "disAllowSavingIfTotalHourMoreThan24"))) {
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
			//get operationHour set in operation screen 
			Double operationHour =  24.0;
			if (session.getCurrentOperation()!=null) {
				operationHour = Double.parseDouble(ActivityUtils.getOperationHour(session.getCurrentOperation()));
			}	
			
			Double totalActivityDuration = 0.0;
			Date newDate = new Date();
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("Activity").values()) {
				Activity activity = (Activity) node.getData();
				
				if (BooleanUtils.isTrue(activity.getIsSimop()) || BooleanUtils.isTrue(activity.getIsOffline())) continue; // exclude from calculation
				// only calculate duration when it is save action 
				if(!node.getAtts().getAction().equals(Action.DELETE)) {
					if (activity != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {					
						//need to calculate activity duration as calculation not yet done
						Date start = ActivityUtils.getPopulatedStartTime(activity, commandBean);
						Date end = CommonUtil.getConfiguredInstance().setDate(activity.getEndDatetime(), newDate);
						
						Double activityDuration = ActivityUtils.getActivityDurationInHour(start, end, thisConverter);						
						totalActivityDuration += activityDuration;	
					}
				}
			}
			
			//show error message when total activity Hour more than operation Hour
			if (totalActivityDuration > operationHour){	
				if(this.showSystemMessageForActivity)
				{
					commandBean.getRoot().setDirty(true);
					commandBean.getSystemMessage().addError("Total Activity Hours exceeds " + ActivityUtils.getOperationHour(session.getCurrentOperation()) + "hrs. Please check and correct the record." ,true, true);
					commandBean.setAbortFormProcessing(true);
				}
			  	   
			}
		}
		
	}
	
	private void updateProperties(Object object) throws Exception {
		this.updatePropertiesBasedOnPlanReference(object);
		this.updatePropertiesBasedOnPlanTaskReference(object);
		this.updatePropertiesBasedOnPlanDetailReference(object);
	}
	private void updatePropertiesBasedOnPlanReference(Object object) throws Exception {
		String planRefUid = (String) PropertyUtils.getProperty(object, "planReference");
		if(StringUtils.isNotBlank(planRefUid)) {
			OperationPlanPhase planPhase = (OperationPlanPhase) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanPhase.class, planRefUid);
			PropertyUtils.setProperty(object, "phaseCode", planPhase.getPhaseCode());
			PropertyUtils.setProperty(object, "holeSize", planPhase.getHoleSize());
			this.updatePropertiesBasedOnPlanTaskReference(object);
			this.updatePropertiesBasedOnPlanDetailReference(object);
		} else {
			PropertyUtils.setProperty(object, "phaseCode", null);
			PropertyUtils.setProperty(object, "holeSize", null);
			PropertyUtils.setProperty(object, "taskCode", null);
			PropertyUtils.setProperty(object, "userCode", null);
		}
	}
	
	private void updatePropertiesBasedOnPlanTaskReference(Object object) throws Exception {
		String planTaskRefUid = (String) PropertyUtils.getProperty(object, "planTaskReference");
		if(StringUtils.isNotBlank(planTaskRefUid)) {
			OperationPlanTask planTask = (OperationPlanTask) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanTask.class, planTaskRefUid);
			PropertyUtils.setProperty(object, "taskCode", planTask.getTaskCode());
			this.updatePropertiesBasedOnPlanDetailReference(object);
		} else {
			String planRefUid = (String) PropertyUtils.getProperty(object, "planReference");
			if(StringUtils.isNotBlank(planRefUid)) {
				PropertyUtils.setProperty(object, "taskCode", null);
				PropertyUtils.setProperty(object, "userCode", null);
			}
		}
	}
	
	private void updatePropertiesBasedOnPlanDetailReference(Object object) throws Exception {
		String planDetailRefUid = (String) PropertyUtils.getProperty(object, "planDetailReference");
		if(StringUtils.isNotBlank(planDetailRefUid)) {
			OperationPlanDetail planDetail = (OperationPlanDetail) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanDetail.class, planDetailRefUid);
			PropertyUtils.setProperty(object, "userCode", planDetail.getDetailCode());
		} else {
			String planRefUid = (String) PropertyUtils.getProperty(object, "planReference");
			if(StringUtils.isNotBlank(planRefUid)) {
				PropertyUtils.setProperty(object, "userCode", null);
			}
		}
	}
	
	private String getInternalCode(String classCode, String groupUid, String operationCode) throws Exception{
		
		String internalCode = "";
		
		String strSql = "SELECT internalCode FROM LookupClassCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisClassCode AND (operationCode = :thisOperationCode OR operationCode = '') AND groupUid = :thisGroupUid";
		String[] paramsFields = {"thisClassCode", "thisGroupUid", "thisOperationCode"};
		Object[] paramsValues = {classCode, groupUid, operationCode};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			Object internalCodeResult = (Object) lstResult.get(0);
			if (internalCodeResult != null) internalCode = internalCodeResult.toString();
		}
		if (StringUtils.isBlank(internalCode)) internalCode = classCode;
		
		return internalCode;
	}
	
		
}
