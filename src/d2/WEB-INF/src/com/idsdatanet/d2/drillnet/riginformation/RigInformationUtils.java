package com.idsdatanet.d2.drillnet.riginformation;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RigTour;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
/**
 * Methods that are used by RigInformation Screen
 * @author Moses
 *
 */
public class RigInformationUtils {
	
	/**
	 * Method to check whether the rig is assign to any active operation
	 * @param rig should provide current screen selected rig
	 * @return return true if it is used and false if not used
	 * @throws Exception
	 */
	public static boolean isRigUsed(RigInformation rig) throws Exception
	{
		String sql = "From RigInformation r, Operation o, ReportDaily rd " +
				" Where (r.isDeleted is null or r.isDeleted = false)" +
				" and (o.isDeleted is null or o.isDeleted = false)" +
				" and (rd.isDeleted is null or rd.isDeleted = false)" +
				" and (rd.operationUid = o.operationUid)" +
				" and (rd.rigInformationUid=r.rigInformationUid or o.rigInformationUid=r.rigInformationUid)" +
				" and (r.rigInformationUid=:rigInformationUid)";
		
		List<RigInformation> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"rigInformationUid",rig.getRigInformationUid());
		return result.size()>0;
	}
	
	/**
	 * check whether the current rigtour is the last in the rig
	 * @param rigTour
	 * @return return true if its the last in db false it there is another one still existing
	 * @throws Exception
	 */
	public static boolean isRigTourUsed(RigTour rigTour) throws Exception
	{
		String sql = "From RigInformation r, Operation o, ReportDaily rd , TourDaily td" +
		" Where (r.isDeleted is null or r.isDeleted = false)" +
		" and (o.isDeleted is null or o.isDeleted = false)" +
		" and (td.isDeleted is null or td.isDeleted = false)" +
		" and (rd.isDeleted is null or rd.isDeleted = false)" +
		" and (rd.operationUid = o.operationUid)" +
		" and (rd.dailyUid = td.dailyUid)" +
		" and (rd.rigInformationUid=r.rigInformationUid or o.rigInformationUid=r.rigInformationUid)" +
		" and (td.rigTourUid=:rigTourUid)";

		List<RigTour> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"rigTourUid",rigTour.getRigTourUid());
		return result.size()>0;
	}
	public static boolean isLastRigTour(RigTour rigTour) throws Exception
	{
		String rigInformationUid = rigTour.getRigInformationUid();
		String sql = "From RigTour " +
		" Where (isDeleted is null or isDeleted = false) " +
		" and rigInformationUid=:rigInformationUid";
		List<RigTour> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"rigInformationUid",rigInformationUid);
		return result.size()==1;
	}
	
	public static Long GetRigTourHour(Date StartTime, Date EndTime) throws Exception
	{
		if (StartTime!=null && EndTime!=null)
		{
			boolean needToAddBack = false;
			boolean needToMinus = false;
			
			Long duration = null;
				
			if(StartTime.getTime()<=EndTime.getTime()){
				//CALCULATE DURATION		
				Calendar thisCalendar = Calendar.getInstance();
				thisCalendar.setTime(EndTime);
				if (thisCalendar.get(Calendar.SECOND) == 59) { // if end is 23:59:59
					needToAddBack = true;
				}
				
				Calendar thisStartCalendar = Calendar.getInstance();
				thisStartCalendar.setTime(StartTime);
				if (thisStartCalendar.get(Calendar.SECOND) == 59) { // if start is 23:59:59
					needToMinus = true;				
				}
				duration = (EndTime.getTime() - StartTime.getTime()) / 1000;
			}else{
				Calendar thisCalendar = Calendar.getInstance();
				thisCalendar.setTime(EndTime);
				thisCalendar.add(Calendar.DATE, +1);	
				if (thisCalendar.get(Calendar.SECOND) == 59) { // if end is 23:59:59
					needToAddBack = true;
				}
				Date end =  thisCalendar.getTime();
				
				Calendar thisStartCalendar = Calendar.getInstance();
				thisStartCalendar.setTime(StartTime);
				if (thisStartCalendar.get(Calendar.SECOND) == 59) { // if start is 23:59:59
					needToMinus = true;				
				}
				duration = (end.getTime() - StartTime.getTime()) / 1000;
			}
		
			//add back 1 second if case of end 23:59:59
			if (needToAddBack) duration = duration + 1;
			if (needToMinus) duration = duration - 1;
			
			return duration;
		}
		return null;
	}
}
