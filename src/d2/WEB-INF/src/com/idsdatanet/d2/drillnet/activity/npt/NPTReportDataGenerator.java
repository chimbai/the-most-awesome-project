package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class NPTReportDataGenerator extends DateRangeReportDataGenerator{
	private Boolean useCountry = false;
	private String companyCode;
	private String companyNPTClassCode;
	private String otherNPTClassCode;
	public String getCompanyNPTClassCode() {
		return companyNPTClassCode;
	}


	public void setCompanyNPTClassCode(String companyNPTClassCode) {
		this.companyNPTClassCode = companyNPTClassCode;
	}


	public String getOtherNPTClassCode() {
		return otherNPTClassCode;
	}


	public void setOtherNPTClassCode(String otherNPTClassCode) {
		this.otherNPTClassCode = otherNPTClassCode;
	}


	
	
	public String getCompanyCode() {
		return companyCode;
	}


	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}


	protected void generateActualData(UserContext userContext, Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		String[] paramNames = {"startDate","endDate","companyNPTClassCode","otherNPTClassCode"};
		Object[] values = {startDate,endDate,companyNPTClassCode,otherNPTClassCode};
		String sql = "SELECT DISTINCT w.wellUid, a.operationUid, w.country, a.lookupCompanyUid, a.classCode FROM Well w, Activity a where a.wellUid=w.wellUid and (a.isDeleted is null or a.isDeleted = False) and (w.isDeleted is null or w.isDeleted = False) and (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') and (a.classCode=:companyNPTClassCode or a.classCode=:otherNPTClassCode) and (a.startDatetime>=:startDate and a.endDatetime<=:endDate)";
		String countryname = null;
				
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, values);
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
		for (Object[] item :result)
		{
			//send short code and get country name in return
			countryname = CommonUtil.getConfiguredInstance().getCountryFullName((String)item[2]);
			NPTSummary npt = new NPTSummary((String)item[0],(String)item[1], countryname, (String)item[3], (String)item[4],null,null,null);
			
			String [] param = {"startDate","endDate","operationUid","classCode","lookupCompanyUid"};
			Object [] value = {startDate, endDate,npt.getOperationUid(),npt.getClassCode(), npt.getLookupCompanyUid()};
			String sqlSum = "SELECT sum(activityDuration) FROM Activity where (isDeleted is null or isDeleted = False) and (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') and (startDatetime>=:startDate and endDatetime<=:endDate) and classCode=:classCode and lookupCompanyUid=:lookupCompanyUid and operationUid=:operationUid";
			List<Object> objList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlSum,param,value, qp);
			if (objList.size()>0)	
			{
				if (objList.get(0)!=null)
				{
					npt.setDuration(Double.parseDouble(objList.get(0).toString()));
					thisConverter.setBaseValue(npt.getDuration());
					npt.setDuration(thisConverter.getConvertedValue());
				}
			}
			if (companyNPTClassCode.equalsIgnoreCase(npt.getClassCode()))
				npt.setLookupCompanyUid(companyCode);
			reportDataNode.addChild(npt);
		}
		
			
	}




	public void setUseCountry(Boolean useCountry) {
		this.useCountry = useCountry;
	}


	public Boolean getUseCountry() {
		return useCountry;
	}
	
	
	
}
