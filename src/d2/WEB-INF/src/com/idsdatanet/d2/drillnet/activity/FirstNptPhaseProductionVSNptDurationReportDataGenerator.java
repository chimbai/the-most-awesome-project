package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class FirstNptPhaseProductionVSNptDurationReportDataGenerator implements ReportDataGenerator {
		private List<String> nptCode;
		private List<String> phaseCode;
		
		private Double cumActualDuration  = 0.0;
		private Double cumNptDuration  = 0.0;
	
		public void disposeOnDataGeneratorExit() {
			// TODO Auto-generated method stub	
		}
		
		public void setNptCode(List<String> nptCode)
		{
			if(nptCode != null){
				this.nptCode = new ArrayList<String>();
				for(String value: nptCode){
					this.nptCode.add(value.trim().toUpperCase());
				}
			}
		}
		
		public List<String> getNptCode() {
			return this.nptCode;
		}
		
		public void setPhaseCode(List<String> phaseCode)
		{
			if(phaseCode != null){
				this.phaseCode = new ArrayList<String>();
				for(String value: phaseCode){
					this.phaseCode.add(value.trim().toUpperCase());
				}
			}
		}
		
		public List<String> getPhaseCode() {
			return this.phaseCode;
		}
		
		public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		// get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		
		Date todayDate = daily.getDayDate();
		String nptClassCodeList = "";
		String phaseCodeList = "";
		String strSql = "";
		String firstNptPhase = "";
		CustomFieldUom durationConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activityDuration");
		
		if(this.getNptCode() != null){
			for(String value:this.nptCode){
				nptClassCodeList = nptClassCodeList + "'" + value.toString() + "',";
			}
			
			nptClassCodeList = StringUtils.substring(nptClassCodeList, 0, -1);
			nptClassCodeList = StringUtils.upperCase(nptClassCodeList);
		}
		
		if(this.getPhaseCode() != null){
			for(String value:this.phaseCode){
				phaseCodeList = phaseCodeList + "'" + value.toString() + "',";
			}
			
			phaseCodeList = StringUtils.substring(phaseCodeList, 0, -1);
			phaseCodeList = StringUtils.upperCase(phaseCodeList);
		}
			
		// Query 1st NPT Phase Code of today's Activities
		strSql = "FROM Activity " +
				 "WHERE (isDeleted IS FALSE OR isDeleted IS NULL) " +
				 "AND classCode IN (" + nptClassCodeList + ") " +
				 "AND dailyUid = :dailyUid " +
				 "AND ((dayPlus IS NULL OR dayPlus=0) AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL)) " +
				 "ORDER BY dayPlus, startDatetime";
		List currentDayActivityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", daily.getDailyUid(),qp);
			
		if(currentDayActivityList != null && currentDayActivityList.size() > 0){
			Activity firstNptActivity = (Activity)currentDayActivityList.get(0);
			if(firstNptActivity != null){
				firstNptPhase = firstNptActivity.getPhaseCode();
			}
		}
			
		if(StringUtils.isNotBlank(firstNptPhase)){
			String phaseName = "N/A";
			String phaseCode = "N/A";
			
			for(String value:this.phaseCode){
				if(StringUtils.equals(firstNptPhase, value)){
					phaseCode = value;
					break;
				}
			}
			
			if(!StringUtils.equals(phaseCode,"N/A")){
				phaseName = getPhaseName(firstNptPhase,userContext.getUserSelection().getOperationType(),qp);
			}
			else{
				phaseName = "Other";
			}
				
			ReportDataNode thisReportNode = reportDataNode.addChild("currentNptPhase");
			thisReportNode.addProperty("shortCode",phaseCode);
			thisReportNode.addProperty("name",phaseName.trim().toUpperCase());
					
			queryActualDurationPerPhase(userContext.getUserSelection().getOperationUid(),todayDate,phaseCode,phaseCodeList,durationConverter,qp);
			queryNPTDurationPerPhase(userContext.getUserSelection().getOperationUid(),todayDate,phaseCode,phaseCodeList,nptClassCodeList,durationConverter,qp);
				
			Double totalProductiveDuration = this.getCumActualDuration() - this.getCumNptDuration();
				
			thisReportNode = reportDataNode.addChild("currentPhaseNptVsProductive");
			thisReportNode.addProperty("series","Productive");
			thisReportNode.addProperty("duration", totalProductiveDuration.toString());
			thisReportNode.addProperty("totalDuration", this.getCumActualDuration().toString());
				
			if(this.getCumNptDuration() > 0.0){
				thisReportNode = reportDataNode.addChild("currentPhaseNptVsProductive");
				thisReportNode.addProperty("series","NPT");
				thisReportNode.addProperty("duration", this.getCumNptDuration().toString());
				thisReportNode.addProperty("totalDuration", this.getCumActualDuration().toString());
			}
		}
	}	
		
	private String getPhaseName(String code, String operationType, QueryProperties qp) throws Exception{
		String phaseName = "N/A";
		
		//Query phase name
		String strSql = "SELECT name FROM LookupPhaseCode " +
				 "WHERE (isDeleted IS FALSE OR isDeleted IS NULL) " +
				 "AND shortCode = :shortCode " +
				 "AND (operationCode = :thisOperationType OR operationCode = '')";
		String[] paramsFields = {"shortCode","thisOperationType"};
		Object[] paramsValues = {code, operationType};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			Object lookupPhaseCodeResult = (Object) lstResult.get(0);
			
			if(lookupPhaseCodeResult != null){
				phaseName = lookupPhaseCodeResult.toString();
			}
		}
		
		return phaseName;
	}
	
	private void queryActualDurationPerPhase(String operationUid, Date todayDate, String phaseCode, String phaseCodeList, CustomFieldUom durationConverter, QueryProperties qp) throws Exception{
		String strSql = "";
		
		if(!StringUtils.equals(phaseCode,"N/A")){
			strSql = "SELECT a.phaseCode, SUM(a.activityDuration) as totalDuration FROM Activity a, Daily d " +
				     "WHERE (a.isDeleted IS FALSE OR a.isDeleted IS NULL) " +
				     "AND (d.isDeleted IS FALSE OR d.isDeleted IS NULL) " +
		 		     "AND a.dailyUid = d.dailyUid " +
				     "AND a.operationUid = :thisOperationUid " +
				     "AND d.dayDate <= :userDate " +
				     "AND a.phaseCode = :phaseCode " +
				     "AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL))";
		}
		else{
			strSql = "SELECT a.phaseCode, SUM(a.activityDuration) as totalDuration FROM Activity a, Daily d " +
				     "WHERE (a.isDeleted IS FALSE OR a.isDeleted IS NULL) " +
				     "AND (d.isDeleted IS FALSE OR d.isDeleted IS NULL) " +
		 		     "AND a.dailyUid = d.dailyUid " +
				     "AND a.operationUid = :thisOperationUid " +
				     "AND d.dayDate <= :userDate " +
				     "AND a.phaseCode NOT IN (" + phaseCodeList + ") " +
				     "AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " +
				     "GROUP BY a.phaseCode";
		}
		
		List lstResult = null;
		
		if(!StringUtils.equals(phaseCode,"N/A")){
			String[] paramsFields = {"thisOperationUid","userDate","phaseCode"};
			Object[] paramsValues = {operationUid,todayDate,phaseCode};
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
		else{
			String[] paramsFields = {"thisOperationUid","userDate"};
			Object[] paramsValues = {operationUid,todayDate};
		    lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
		
		if(lstResult != null && lstResult.size() > 0){
			cumActualDuration = 0.0; 
			
			for(Object objResult: lstResult){
				Object[] obj = (Object[]) objResult;
				Double duration = 0.0;
				
				if(obj[1]!=null && StringUtils.isNotBlank(obj[1].toString())) duration = Double.parseDouble(obj[1].toString());
				
				cumActualDuration += duration;
			}
			
			if(cumActualDuration != null){
				durationConverter.setBaseValue(cumActualDuration);
				this.setCumActualDuration(durationConverter.getConvertedValue());
			}
		}
	}
	
	private void queryNPTDurationPerPhase(String operationUid, Date todayDate, String phaseCode, String phaseCodeList, String nptClassCodeList, CustomFieldUom durationConverter, QueryProperties qp) throws Exception{
		String strSql = "";
		
		if(!StringUtils.equals(phaseCode,"N/A")){
			strSql = "SELECT a.phaseCode, SUM(a.activityDuration) as totalDuration FROM Activity a, Daily d " +
		 		     "WHERE (a.isDeleted IS FALSE OR a.isDeleted IS NULL) " +
		 		     "AND (d.isDeleted IS FALSE OR d.isDeleted IS NULL) " +
		 		     "AND a.dailyUid = d.dailyUid " +
		 		     "AND a.operationUid = :thisOperationUid " +
		 		     "AND d.dayDate <= :userDate " +
		 		     "AND a.phaseCode = :phaseCode " +
		 		     "AND a.classCode IN (" + nptClassCodeList + ") " +
		 		     "AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL))";
		}
		else{
			strSql = "SELECT a.phaseCode, SUM(a.activityDuration) as totalDuration FROM Activity a, Daily d " +
		 		     "WHERE (a.isDeleted IS FALSE OR a.isDeleted IS NULL) " +
		 		     "AND (d.isDeleted IS FALSE OR d.isDeleted IS NULL) " +
		 		     "AND a.dailyUid = d.dailyUid " +
		 		     "AND a.operationUid = :thisOperationUid " +
		 		     "AND d.dayDate <= :userDate " +
		 		     "AND a.phaseCode NOT IN (" + phaseCodeList + ") " +
		 		     "AND a.classCode IN (" + nptClassCodeList + ") " +
		 		     "AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " +
		 		     "GROUP BY a.phaseCode";
		}
		
		List lstResult = null;
		
		if(!StringUtils.equals(phaseCode,"N/A")){
			String[] paramsFields = {"thisOperationUid","userDate","phaseCode"};
			Object[] paramsValues = {operationUid,todayDate,phaseCode};
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
		else{
			String[] paramsFields = {"thisOperationUid","userDate"};
			Object[] paramsValues = {operationUid,todayDate};
		    lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		}
		
		if(lstResult != null && lstResult.size() > 0){
			cumNptDuration = 0.0; 
			
			for(Object objResult: lstResult){
				Object[] obj = (Object[]) objResult;
				Double duration = 0.0;
				
				if(obj[1]!=null && StringUtils.isNotBlank(obj[1].toString())) duration = Double.parseDouble(obj[1].toString());
				
				cumNptDuration += duration;
			}
			
			if(cumNptDuration != null){
				durationConverter.setBaseValue(cumNptDuration);
				this.setCumNptDuration(durationConverter.getConvertedValue());
			}
		}
	}
	
	public void setCumActualDuration(Double cumActualDuration) {
		this.cumActualDuration = cumActualDuration;
	}

	public Double getCumActualDuration() {
		return cumActualDuration;
	}

	public void setCumNptDuration(Double cumNptDuration) {
		this.cumNptDuration = cumNptDuration;
	}

	public Double getCumNptDuration() {
		return cumNptDuration;
	}
}