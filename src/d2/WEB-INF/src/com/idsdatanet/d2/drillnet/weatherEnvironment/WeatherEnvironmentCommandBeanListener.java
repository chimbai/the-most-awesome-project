package com.idsdatanet.d2.drillnet.weatherEnvironment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.ExcelImportTemplateManager;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;

public class WeatherEnvironmentCommandBeanListener extends EmptyCommandBeanListener implements CommandBeanListener {

    private ExcelImportTemplateManager excelImportTemplateManager = null;

    public ExcelImportTemplateManager getExcelImportTemplateManager() {
        return excelImportTemplateManager;
    }

    public void setExcelImportTemplateManager(ExcelImportTemplateManager excelImportTemplateManager) {
        this.excelImportTemplateManager = excelImportTemplateManager;
    }

    @Override
    public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
        String action = request.getParameter("action");
        if (StringUtils.equals(ExcelImportTemplateManager.IMPORT_TEMPLATE_EXCEL, action)) {
            this.excelImportTemplateManager.importExcelFile((BaseCommandBean) commandBean, this, request, request.getParameter(ExcelImportTemplateManager.KEY_TEMPLATE), request.getParameter(ExcelImportTemplateManager.KEY_FILE), "WeatherEnvironment");
        }
    }

    @Override
    public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
        if (ExcelImportTemplateManager.IMPORT_TEMPLATE_EXCEL.equalsIgnoreCase(invocationKey)) {
            this.excelImportTemplateManager.uploadFile(request, response, ExcelImportTemplateManager.KEY_FILE);
        }
        return;
    }
    
}