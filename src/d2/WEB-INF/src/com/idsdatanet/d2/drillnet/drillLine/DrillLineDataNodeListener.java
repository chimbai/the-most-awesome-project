package com.idsdatanet.d2.drillnet.drillLine;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.BhaComponent;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DrillPipeTally;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.TourWireline;
import com.idsdatanet.d2.core.model.TourWirelineDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DrillLineDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener, DataLoaderInterceptor, DataNodeLoadHandler, DataNodeAllowedAction {

	CustomFieldUom thisConverter;
	
	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {

		if (thisConverter==null) thisConverter = new CustomFieldUom(commandBean);
		if (node.getData() instanceof TourWireline) {
			
			//this.loadTonMileCalculations(commandBean, node, userSelection);
			
			TourWireline wireline = (TourWireline) node.getData();
			if (wireline.getRigInformationUid()!=null) {
				RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, wireline.getRigInformationUid());
				if (rig!=null) {
					node.getDynaAttr().put("lineQuantity", rig.getLineQuantity());
					thisConverter.setReferenceMappingField(RigInformation.class, "lineQuantity");
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@lineQuantity", thisConverter.getUOMMapping());
					}
				}
			}
		}
	}

	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		
		if (thisConverter==null) thisConverter = new CustomFieldUom(commandBean);
		if (node.getData() instanceof TourWirelineDetail) {
			TourWirelineDetail tourWirelineDetail = (TourWirelineDetail) node.getData();
			//DrillLineUtils.calculateSlipAndCut(tourWirelineDetail, thisConverter);
			DrillLineUtils.calculateWearTripSinceLastCut(tourWirelineDetail, thisConverter);
			DrillLineUtils.calculateAccumulatedWear(tourWirelineDetail.getTourWirelineUid());
		}
		
	}

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		if ("TourWireline".equals(meta.getTableClass().getSimpleName())) {
			String dailyUid = userSelection.getDailyUid();
			if (dailyUid!=null) {
				Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
				if (daily!=null && daily.getDayDate()!=null) {
					Date dayDate = daily.getDayDate();
					Calendar startDatetime = Calendar.getInstance();
					startDatetime.setTime(dayDate);
					startDatetime.add(Calendar.DATE, 1);
					
					String customCondition = " (installDateTime<:startDatetime) " +
							" AND (uninstallDateTime>=:endDatetime or uninstallDateTime is null) ";
					query.addParam("startDatetime", startDatetime.getTime());
					query.addParam("endDatetime", dayDate);
					
					conditionClause += (StringUtils.isNotBlank(conditionClause.trim())?" AND ":"") + customCondition;
					return conditionClause;
				}
			}			
		}
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		Map children = root.getChild("TourWireline");
		if (children==null || children.size()==0 || (children.containsKey("_empty_") && children.size()<=1)) {
			commandBean.getSystemMessage().addWarning("To add a Drill Line, proceed to the Rig Information screen.");
		}
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		if (request!=null) {
			DrillLineUtils.createTourWirelineDetail(commandBean, UserSession.getInstance(request));
		}
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}
/*
	private void loadTonMileCalculations(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception {
		if (node.getData() instanceof TourWireline) {
			TourWireline wireline = (TourWireline) node.getData();
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			String queryString = "FROM Activity WHERE (isDeleted=false or isDeleted is null) " +
					"AND (isSimop=false or isSimop is null) " +
					"AND (isOffline=false or isOffline is null) " +
					"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
					"AND dailyUid=:dailyUid " +
					"AND startDatetime>=:startDatetime " +
					(wireline.getUninstallDateTime()!=null?"AND endDatetime<=:endDatetime ":"") +
					"ORDER BY startDatetime";
			String[] paramNames = {"dailyUid", "startDatetime"};
			Object[] paramValues = {userSelection.getDailyUid(), wireline.getInstallDateTime()};
			
			if (wireline.getUninstallDateTime()!=null) {
				paramNames = new String[] {"dailyUid", "startDatetime", "endDatetime"};
				paramValues = new Object[] {userSelection.getDailyUid(), wireline.getInstallDateTime(), wireline.getUninstallDateTime()};
			}
			
			Double bf = DrillLineUtils.calculateBuoyancyFactor(userSelection.getDailyUid(), wireline.getInstallDateTime(), wireline.getUninstallDateTime(), qp);
			Double bhaBfWeight = DrillLineUtils.calculateBhaBuoyedWeight(userSelection.getDailyUid(), wireline.getInstallDateTime(), wireline.getUninstallDateTime(), bf, qp);
			Double drillPipeBfWeight = DrillLineUtils.calculateDrillPipeBuoyedWeight(userSelection.getDailyUid(), wireline.getInstallDateTime(), wireline.getUninstallDateTime(), bf, qp);
			
			
			Double cumDistance = 0.0;
			List<Activity> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, qp);
			for (Activity activity:list) {
				TonMileCalculation tonMile = new TonMileCalculation();
				tonMile.setTourWirelineUid(wireline.getTourWirelineUid());
				
				tonMile.setStartDatetime(activity.getStartDatetime());
				tonMile.setEndDatetime(activity.getEndDatetime());
				tonMile.setDailyUid(activity.getDailyUid());
				CommandBeanTreeNode childNode = node.addChild(tonMile.getClass().getSimpleName(), tonMile);
				
				Double distance = null;
				Double stringMaxDepthMdMsl = null;
				if (activity.getStringFromDepthMdMsl()!=null && activity.getStringToDepthMdMsl()!=null) {
					if (activity.getStringToDepthMdMsl() > activity.getStringFromDepthMdMsl()) {
						stringMaxDepthMdMsl = activity.getStringToDepthMdMsl();
						distance = activity.getStringToDepthMdMsl() - activity.getStringFromDepthMdMsl();
					} else {
						stringMaxDepthMdMsl = activity.getStringFromDepthMdMsl();
						distance = activity.getStringFromDepthMdMsl() - activity.getStringToDepthMdMsl();
					}
					
				} else {
					if (activity.getStringFromDepthMdMsl()!=null) {
						stringMaxDepthMdMsl = activity.getStringFromDepthMdMsl();
						tonMile.setStringMaxDepthMdMsl(activity.getStringFromDepthMdMsl());
					} else {
						stringMaxDepthMdMsl = activity.getStringToDepthMdMsl();
					}
				}
				
				if ("10.2.1".equals(activity.getUserCode()) || "10.2.2".equals(activity.getUserCode())) { //reset daily wear
					tonMile.setReset(true);
					cumDistance = 0.0;
				}
				if (distance!=null) {
					cumDistance += distance;
				}
				
				thisConverter.setReferenceMappingField(Activity.class, "stringToDepthMdMsl");
				if (thisConverter.isUOMMappingAvailable()) {
					if (distance!=null) {
						thisConverter.setBaseValue(distance);
						childNode.getDynaAttr().put("distanceTraveled", thisConverter.getConvertedValue());
						childNode.setCustomUOM("@distanceTraveled", thisConverter.getUOMMapping());
					}
					
					if (stringMaxDepthMdMsl!=null) {
						thisConverter.setBaseValue(stringMaxDepthMdMsl);
						childNode.getDynaAttr().put("stringMaxDepthMdMsl", thisConverter.getConvertedValue());
						childNode.setCustomUOM("@stringMaxDepthMdMsl", thisConverter.getUOMMapping());
					}
					
					thisConverter.setBaseValue(cumDistance);
					childNode.getDynaAttr().put("cumulativeDistanceTraveled", thisConverter.getConvertedValue());
					childNode.setCustomUOM("@cumulativeDistanceTraveled", thisConverter.getUOMMapping());
				}
				
				thisConverter.setReferenceMappingField(BhaComponent.class, "weightMass");
				if (thisConverter.isUOMMappingAvailable()) {
					thisConverter.setBaseValue(bhaBfWeight);
					childNode.getDynaAttr().put("buoyedBhaWeight", thisConverter.getConvertedValue());
					childNode.setCustomUOM("@buoyedBhaWeight", thisConverter.getUOMMapping());
				}
				
				thisConverter.setReferenceMappingField(DrillPipeTally.class, "weight");
				if (thisConverter.isUOMMappingAvailable()) {
					thisConverter.setBaseValue(drillPipeBfWeight);
					childNode.getDynaAttr().put("buoyedDrillPipeWeight", thisConverter.getConvertedValue());
					childNode.setCustomUOM("@buoyedDrillPipeWeight", thisConverter.getUOMMapping());
				}
				
				
			}
			
		}
	}
*/
	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		if ("TonMileCalculation".equals(meta.getTableClass().getSimpleName())) {
			return true;
		}
		return false;
	}

	public boolean allowedAction(CommandBeanTreeNode node, UserSession session,
			String targetClass, String action, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof TourWirelineDetail) {
			if (Action.EDIT.equals(action)) {
				return true;
			}
		}
		return false;
	}

}
