package com.idsdatanet.d2.drillnet.drillingparameters;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DrillingParamatersDataNodeAllowedAction  implements DataNodeAllowedAction {
	
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		
		if(! node.getInfo().isTemplateNode()) {
			if(StringUtils.equals(targetClass, "DrillingParameters")) {
				String thisDailyUid = session.getCurrentDailyUid();
				if (StringUtils.isNotBlank(thisDailyUid)) {					
					String strSql = "FROM BharunDailySummary WHERE dailyUid = :dailyUid AND (isDeleted IS NULL OR isDeleted = '')";
					List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", thisDailyUid);
					if(list.size() < 1) {
						if(StringUtils.equals(action, "pasteAsNew") || StringUtils.equals(action, "add"))
						{
							return false;
						}
					}
				}		
			}			
		}
		return true;
	}
}
