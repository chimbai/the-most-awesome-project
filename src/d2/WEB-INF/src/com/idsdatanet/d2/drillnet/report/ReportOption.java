package com.idsdatanet.d2.drillnet.report;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ReportOption {
	private String code;
	private Object value;
	private Boolean isMultiOption;
	private Map<String,Object> multiSelectValue;
	private Map<String,Object> multiSelectKey;
	private List<String> values;
	
	public ReportOption(String code, Object value, Boolean isMultiOption,
			Map<String, Object> multiSelectValue, List<String> values) {
		super();
		this.setCode(code);
		this.setValue(value);
		this.setIsMultiOption(isMultiOption);
		this.setMultiSelectValue(multiSelectValue);
		this.setValues(values);
	}


	public static ReportOption createReportOption(String code, Object value)
	{
		return new ReportOption(code,value,false,null,null);
	}
	public static ReportOption createMultiSelectReportOption(List<String> values, Map<String, Object> multiSelectValue)
	{
		return new ReportOption(null,null,true,multiSelectValue,values);
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getCode() {
		return code;
	}


	public void setValue(Object value) {
		this.value = value;
	}


	public Object getValue() {
		return value;
	}


	public void setIsMultiOption(Boolean isMultiOption) {
		this.isMultiOption = isMultiOption;
	}


	public Boolean getIsMultiOption() {
		return isMultiOption;
	}


	public void setMultiSelectValue(Map<String,Object> multiSelectValue) {
		this.multiSelectValue = multiSelectValue;
		if (multiSelectValue!=null)
		{
			this.multiSelectKey = new LinkedHashMap<String,Object>();
			for (Map.Entry<String, Object> entry : this.multiSelectValue.entrySet())
			{
				this.multiSelectKey.put(entry.getValue().toString(), entry.getKey());
			}
		}
	}


	public Map<String,Object> getMultiSelectValue() {
		return multiSelectValue;
	}
	
	public Object getKey(Object value)
	{
		return this.multiSelectKey.get(value);
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public Integer getIndex(String value) {
		return this.values.indexOf(value);
	}
	
	public String getLabel(Integer index){
		Object obj= this.multiSelectValue.get(this.values.get(index));
		if (obj!=null)
			return obj.toString();
		return null;
	}
	
	public List<String> getValues() {
		if (values==null)
			values = new ArrayList<String>();
		return values;
	}
	
	public Object getValue(String key)
	{
		if (this.multiSelectValue==null)
			return null;
		return this.multiSelectValue.get(key);
	}
}
