package com.idsdatanet.d2.drillnet.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.graph.BaseGraph;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.LookupClassCode;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class lookahead extends BaseGraph {
	private final static Integer ACTUAL_DAY_DEPTH 	  = 0;
	private final static Integer PLANNED_COST 		  = 1;
	private final static Integer ACTUAL_COST 		  = 2;
	private final static Integer LA_DAY_DEPTH  		  = 3;
	private Map<String, XYDataset> xyDatasetMap   	  = new HashMap<String, XYDataset>();
	private List operationPlanTaskLists 		  	  = null;
	private List dailyLists 			 		  	  = null;
	private QueryProperties queryProperties 		  = new QueryProperties();
	private NumberFormat formatter 					  = new DecimalFormat("#.###");
	private Double maxDepth							  = 0.0;
	private Double maxDuration						  = 0.0;
	
	private Boolean showActualDataToDate = false;
	private Boolean showProductiveTimeDepth = false;
	
	public void setShowProductiveTimeDepth(Boolean showProductiveTimeDepth) {
		this.showProductiveTimeDepth = showProductiveTimeDepth;
	}
	public Boolean getShowProductiveTimeDepth() {
		return this.showProductiveTimeDepth;
	}
	
	public void setShowActualDataToDate(Boolean showActualDataToDate) {
		this.showActualDataToDate = showActualDataToDate;
	}
	
	// dashed line
	private BasicStroke stroke = new java.awt.BasicStroke(1f,
		        BasicStroke.CAP_SQUARE,
		        BasicStroke.JOIN_MITER,
		        10.0f,
		        new float[] {2f, 3f, 2f, 3f},
		        0f
		    );
	
	// default constructor
	public lookahead() {}
	
	@Override
	public boolean isAllowedAlwaysPaint() {
		HttpServletRequest currentHttpRequest = this.getCurrentHttpRequest();
		
		return true;
	}
	
	
	/**
	 * Main method that must be implemented in order to paint graph.
	 * @param request A HttpServletRequest object from BaseGraphController. Use to get any parameter from HTTP.
	 * @param response A HttpServletResponse object from BaseGraphController. Basically use for outputstream purpose.
	 */
	@Override
	public JFreeChart paint() throws Exception {
		String groupUid = null;
		Locale locale = null;
		this.queryProperties.setUomConversionEnabled(false);
		
		if (this.getCurrentHttpRequest() != null) {
			UserSession session = UserSession.getInstance(this.getCurrentHttpRequest());
			groupUid = session.getCurrentGroupUid();
			locale = session.getUserLocale();
		} else {
			groupUid = this.getCurrentUserContext().getUserSelection().getGroupUid();
			locale = this.getCurrentUserContext().getUserSelection().getLocale();
		}
		this.formatter = (DecimalFormat) DecimalFormat.getInstance(locale);
		
		if(this.getCurrentUserContext() != null) {
			String operationUid = this.getCurrentUserContext().getUserSelection().getOperationUid();
			
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
			
			String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, operationUid);
			if(StringUtils.isNotBlank(operationName)) {
				setTitle(operationName);
			}
			
			//this.setFileName(operationUid + "_" + "la.jpg");
			if (this.showActualDataToDate) {
				String fileName = this.getFileName();
				String dailyUid = this.getCurrentUserContext().getUserSelection().getDailyUid();
				if (dailyUid!=null) {
					fileName = fileName.replaceAll(operationUid + "_", "");
					if (!(fileName.indexOf(dailyUid + "_")>=0)) {
						fileName = dailyUid + "_" + fileName;
						this.setFileName(fileName);
					}
				}
			}
			
	//		this.operationPlanPhaseLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanPhase where (isDeleted = false or isDeleted is null) and operationPlanMasterUid = :operationPlanMasterUid order by sequence,phaseSequence", "operationPlanMasterUid", this.currentOperationPlanMasterUid, this.queryProperties);
			this.operationPlanTaskLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanTask where (isDeleted = false or isDeleted is null) and operationUid = :operationUid order by sequence", "operationUid", operationUid, this.queryProperties);
			this.dailyLists 			 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and reportType=:reportType order by reportDatetime", new String[] {"operationUid", "reportType"}, new Object[] {operationUid, reportType}, this.queryProperties);
		}
		
		// start drawing ...
		CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale());
		
		thisConverter.setReferenceMappingField(Activity.class, "depthMdMsl");
		String depth = "Depth (" + thisConverter.getUomSymbol() + ")";
		
		XYDataset dataset = createActualDayDepthDataset();
		JFreeChart chart = ChartFactory.createXYLineChart(
			getTitle(),      			// chart title
            "Days (d)",                  // x axis label
            depth,                    	// y axis label
            dataset,                  	// data
            PlotOrientation.VERTICAL,
            true,                     	// include legend
            true,                     	// tooltips
            false                     	// urls
		);
        
		// main settings
        XYPlot plot = chart.getXYPlot();
        plot.setOrientation(PlotOrientation.VERTICAL);
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.lightGray);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.getRangeAxis().setFixedDimension(15.0);
        XYItemRenderer renderer = plot.getRenderer();
        renderer.setSeriesPaint(0, Color.black);
        if (this.showProductiveTimeDepth){
        	renderer.setSeriesPaint(1, Color.green);
        }
        

        // Create curve lines. 
        this.createPlannedCostAndDepthDataset(groupUid);         
      
        XYDataset laDataset = (XYDataset) this.xyDatasetMap.get("la");
    	plot.setDataset(LA_DAY_DEPTH, laDataset);
    	plot.mapDatasetToRangeAxis(LA_DAY_DEPTH, ACTUAL_DAY_DEPTH);
        XYItemRenderer plannedLARenderer = new StandardXYItemRenderer();
        plannedLARenderer.setSeriesPaint(0, Color.green);
        plot.setRenderer(LA_DAY_DEPTH, plannedLARenderer); 
        
 //       String graphtitlevalue = GroupWidePreference.getValue(groupUid, "setDvdGraphTitle");
        if (isTitleEnabled()) chart.addSubtitle(new TextTitle("LookAhead"));
        chart.setBackgroundPaint(Color.lightGray);
        chart.getXYPlot().getRangeAxis(ACTUAL_DAY_DEPTH).setInverted(true);
        ((NumberAxis) chart.getXYPlot().getRangeAxis()).setNumberFormatOverride(formatter);
        ((NumberAxis) chart.getXYPlot().getDomainAxis()).setNumberFormatOverride(formatter);
        
        return chart;
	}
	
	/**
	 * Create a dataset for JFreeChart to draw a Planned Phase - P10 Curve line
	 * @return XYDataset
	 * @throws Exception
	 */
	private void createPlannedCostAndDepthDataset(String groupUid) throws Exception {

		XYSeries laCurveSeries = new XYSeries("LookAhead Planned Curve");

        CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(), Activity.class, "depthMdMsl");
		
		String wellboreUid = null;
		
		if (this.getCurrentUserContext() != null) {
			wellboreUid = this.getCurrentUserContext().getUserSelection().getWellboreUid();
		}
		
		if (StringUtils.isNotBlank(wellboreUid)) {
			Wellbore wellbore 	= (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, wellboreUid);
			
			if (StringUtils.isBlank(wellbore.getParentWellboreUid())) {
				laCurveSeries.add(0.0, 0.0);
			}else {
				Double kickOffMd = 0.0;
				if(wellbore.getKickoffMdMsl() != null)thisConverter.setBaseValue(wellbore.getKickoffMdMsl());
				kickOffMd = thisConverter.getConvertedValue();
				laCurveSeries.add(0.0, kickOffMd);
			}
			
			Double cumLADuration = 0.0;
			
			if(this.operationPlanTaskLists != null && operationPlanTaskLists.size() > 0) {
				Double LADuration  = 0.0;
				Double depthMd 		= null;
				for(Iterator i=operationPlanTaskLists.iterator(); i.hasNext();) {
					OperationPlanTask operationPlanTask = (OperationPlanTask)i.next();
					
					if (operationPlanTask.getDepthMdMsl()!=null){
						thisConverter.setBaseValue(operationPlanTask.getDepthMdMsl());
						depthMd = thisConverter.getConvertedValue();
					}
					
					LADuration = operationPlanTask.getDuration();
					if(LADuration != null && depthMd != null) {
						LADuration = LADuration / 86400;
						cumLADuration += LADuration;
						
						laCurveSeries.add(cumLADuration, depthMd);

						if (depthMd > this.maxDepth) this.maxDepth = depthMd;
						if (cumLADuration > this.maxDuration) this.maxDuration = cumLADuration;

					}

				}
			}
			
			XYSeriesCollection laDataset = new XYSeriesCollection();
			laDataset.addSeries(laCurveSeries);
			
			this.xyDatasetMap.put("la", laDataset);
		}
	}
	
	/**
	 * Create a dataset for JFreeChart to draw a Actual Day Depth line
	 * @return
	 * @throws Exception
	 */
	
	private XYDataset createActualDayDepthDataset() throws Exception {
		XYSeries actualSeries = new XYSeries("Actual Day/Depth");
		XYSeries productiveSeries = new XYSeries("Productive Time/Depth");
		
		if(this.dailyLists != null && dailyLists.size() > 0) {
			Double global_duration = 0.0;
			Double productive_duration = 0.0;
			
			CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(), Activity.class, "depthMdMsl");
			String wellboreUid = null;
			String currentDailyUid = null;
			if (this.getCurrentUserContext() != null) {
				UserContext userContext = this.getCurrentUserContext();
				wellboreUid = userContext.getUserSelection().getWellboreUid();
				currentDailyUid = userContext.getUserSelection().getDailyUid();

				if (StringUtils.isNotBlank(wellboreUid)) {
					Wellbore wellbore 	= (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, wellboreUid);
					
					if (StringUtils.isBlank(wellbore.getParentWellboreUid())) {
						actualSeries.add(0.0, 0.0);
						if (this.showProductiveTimeDepth){
							productiveSeries.add(0.0, 0.0);
						}					
					}else {
						Double kickOffMd = 0.0;
						if(wellbore.getKickoffMdMsl() != null)thisConverter.setBaseValue(wellbore.getKickoffMdMsl());
						kickOffMd = thisConverter.getConvertedValue();
						actualSeries.add(0.0, kickOffMd);
						if (this.showProductiveTimeDepth){
							productiveSeries.add(0.0, kickOffMd);
						}
						
					}

					Map<String, String> classCode = new HashMap<String, String>();					
					String lookupClassCodeQuery = "from LookupClassCode where (isDeleted = false or isDeleted is null)";
					List lookupClassCodeLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().find(lookupClassCodeQuery, this.queryProperties);
					if (lookupClassCodeLists !=null && lookupClassCodeLists.size() > 0){
						for(Iterator it=lookupClassCodeLists.iterator(); it.hasNext();) {
							LookupClassCode lookupClassCode = (LookupClassCode)it.next();
							classCode.put(lookupClassCode.getShortCode(), lookupClassCode.getInternalCode());
							
						}
					}
								
					for(Iterator i=dailyLists.iterator(); i.hasNext();) {
						ReportDaily reportDaily 		 = (ReportDaily)i.next();
						
						String activityQuery = "from Activity where (isDeleted = false or isDeleted is null) and dailyUid=:dailyUid and operationUid = :operationUid AND (dayPlus=0 or dayPlus is null) AND (isOffline=0 or isOffline is null) AND (isSimop=0 or isSimop is null) order by endDatetime";
						String[] paramNames  = {"dailyUid", "operationUid"};
						String[] paramValues = {reportDaily.getDailyUid(), userContext.getUserSelection().getOperationUid()};
						List activityLists   = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(activityQuery, paramNames, paramValues, this.queryProperties);

						if(activityLists != null && activityLists.size() > 0) {
							for(Iterator it=activityLists.iterator(); it.hasNext();) {
								Activity activity 	= (Activity)it.next();
								Double duration 	= activity.getActivityDuration();
								
								Double dayDepth = null;
								
								if (activity.getDepthMdMsl() !=null) {
									thisConverter.setBaseValue(activity.getDepthMdMsl());
									dayDepth = thisConverter.getConvertedValue();
								}
								
								if(duration != null && dayDepth != null) {
									global_duration += (duration / 86400);									
									actualSeries.add(global_duration, dayDepth);		
																		
									String internalCode = classCode.get(activity.getClassCode());
									if (this.showProductiveTimeDepth){
										if (!("TU".equals(internalCode) || "TP".equals(internalCode))){
											productive_duration += (duration / 86400);
											productiveSeries.add(productive_duration, dayDepth);
										}		
									}
																								
									if (dayDepth>this.maxDepth) this.maxDepth = dayDepth;
									if (global_duration > this.maxDuration) this.maxDuration = global_duration;
									if (this.showProductiveTimeDepth){
										if (productive_duration > this.maxDuration) this.maxDuration = productive_duration;
									}
									
								}
								
							}
						}
						
						if (reportDaily.getDailyUid().equals(currentDailyUid) && showActualDataToDate) break;
					}
				}
			}
		}
		
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(actualSeries);
		if (this.showProductiveTimeDepth){
			dataset.addSeries(productiveSeries);
		}
		
		return dataset;
	}
	
	public void dispose() {
		this.xyDatasetMap.clear();
		this.operationPlanTaskLists = null;
		this.dailyLists 			 = null;
	}
}
