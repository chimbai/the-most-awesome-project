package com.idsdatanet.d2.drillnet.rigpump;

import java.util.List;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class RigPumpUtils {
	
	/**
	 * Method for get the volume stroke (from parent level - rigPumpParam)
	 * @param rigPumpParamUid
	 * @throws Exception
	 * @return vps
	 */
	
	public static Double getVolumeStroke(String rigPumpParamUid) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		
		Double vps = 0.00;
		String sql = "SELECT outputVolumeStk from RigPumpParam where (isDeleted = false or isDeleted is null) and rigPumpParamUid=:rigPumpParamUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "rigPumpParamUid", rigPumpParamUid, qp);
		if (lstResult.size() > 0) {
			Object vfro = (Object) lstResult.get(0);
			if (vfro != null) vps = (Double) vfro;
		
		}
		return vps;
	}
}
