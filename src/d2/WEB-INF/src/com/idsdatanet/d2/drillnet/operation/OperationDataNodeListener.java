//** changes made on this file should also be applied to well operation if needed.

package com.idsdatanet.d2.drillnet.operation;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.NumberUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Campaign;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DepotWellOperationMapping;
import com.idsdatanet.d2.core.model.GeneralComment;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.TargetZone;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.uom.mapping.UOMManager;
import com.idsdatanet.d2.core.uom.mapping.UOMMapping;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.well.Constant;
import com.idsdatanet.d2.drillnet.well.DefaultDatumLookupHandler;
import com.idsdatanet.d2.drillnet.wellexplorer.WellExplorerConstant;
import com.idsdatanet.d2.safenet.environmentDischarges.EnvironmentDischargesUtil;
import com.idsdatanet.depot.core.DepotUserSelectionSnapshot;
import com.idsdatanet.depot.core.scheduler.DepotWellMappingEvent;
import com.idsdatanet.depot.edm.util.EdmUtils;
import com.idsdatanet.depot.edm.wellimport.EDMDepotWellImportDataNodeListener;
import com.idsdatanet.depot.edm.wellimport.EdmWellImportSessionData;

public class OperationDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor, DataNodeAllowedAction {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private Boolean defaultUOMTemplate = false;
	private Boolean calcDaysSpentPriorToSpud = false;
	private String tournetDailyRedirect = null;
	private Boolean carryOverRigOnHireDateToStartdate = false;
	private Boolean useOperationNameDelimiter = false;
	private String defaultCompanyField = "";private Boolean mapCountryToUomTemplate = false;
	private Map<String, String> countryUomTemplateMapping = new HashMap<String, String>();
	private Boolean calculateActualDurationFromActivity = false;
	
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;
	
	public void setMapCountryToUomTemplate(Boolean value){
		this.mapCountryToUomTemplate = value;
	}
	
	public void setCountryUomTemplateMapping(Map<String, String> map){
		this.countryUomTemplateMapping = map;
	}
	
	public Boolean getUseOperationNameDelimiter() {
		return useOperationNameDelimiter;
	}

	public void setUseOperationNameDelimiter(Boolean value) {
		this.useOperationNameDelimiter = value;
	}
	
	public Boolean getCarryOverRigOnHireDateToStartdate() {
		return carryOverRigOnHireDateToStartdate;
	}

	public void setCarryOverRigOnHireDateToStartdate(Boolean value) {
		this.carryOverRigOnHireDateToStartdate = value;
	}

	public void setCalcDaysSpentPriorToSpud(Boolean value) {
		this.calcDaysSpentPriorToSpud = value;
	}
	
	public void setDefaultUOMTemplate(Boolean value) {
		this.defaultUOMTemplate = value;
	}

	public void setDefaultCompanyField(String value) {
		this.defaultCompanyField = value;
	}
	
	public void setCalculateActualDurationFromActivity(Boolean value) {
		this.calculateActualDurationFromActivity = value;
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		//ApplicationUtils.getConfiguredInstance().refreshCachedData();
		Object object = node.getData();
				
		if(object instanceof com.idsdatanet.d2.core.model.Operation){

			Operation operation = (Operation) object;
			
			OperationUtils.setTightHoleRelatedContentAfterSaveOperation(operation, session);

			// put this newly created operation into the user selected ops teams, only the ops team that the current user has access to will show in the multiselect
			if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				OperationUtils.updateOpTeams(node);
			}
			
			// refresh
			List<String> changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(operation.getOperationUid());
			D2ApplicationEvent.publishTableChangedEvent(Operation.class, changedRowPrimaryKeys);
			
			changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(operation.getWellboreUid());
			D2ApplicationEvent.publishTableChangedEvent(Wellbore.class, changedRowPrimaryKeys);
			
			changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(operation.getWellUid());
			D2ApplicationEvent.publishTableChangedEvent(Well.class, changedRowPrimaryKeys);
			
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
			
			// add the current well to the recent wells list, this must be done before the session.setCurrentOperationUid(...) call below
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_WELL_QUERY_FILTER_ENABLED))) {
				if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
					String queryFilterUid = session.getSystemSelectionFilter().addRecentObject(session.getUserUid(), operation.getWellUid(), session.getCurrentView());
					session.getSystemSelectionFilter().setQueryFilterUid(queryFilterUid, false);
					
					User currentUser = ApplicationUtils.getConfiguredInstance().getCachedUser(session.getUserUid());
					currentUser.setWellQueryFilterUid(queryFilterUid);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(currentUser);
				}
			}
			
			// remember the selected day
			Daily selectedDaily = session.getCurrentDaily();
			
			// this will always reset the selected day
			session.setCurrentOperationUid(operation.getOperationUid());
			
			if (selectedDaily != null) {
				// if the selected operation does not change, put back the selected day
				if (operation.getOperationUid().equals(selectedDaily.getOperationUid())) {
					session.setCurrentDailyUid(selectedDaily.getDailyUid());
				}
			}
			session.setCurrentRigOnOffShoreByWellUid(operation.getWellUid());
			
			session.setCurrentCampaignUid(operation.getOperationCampaignUid());
			
			// redirect to report daily
			if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				OperationUtils.redirectToReportDaily(session, commandBean, operation, "operation.html", tournetDailyRedirect);
			}
			
			OperationUtils.updateFirstDayProgress(commandBean, operation, session);
					
			// if we just updated an existing record, reload the page for the well and datum drop down to reflect the change
			if (operationPerformed != BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				if (commandBean.getFlexClientControl() != null) {
					commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
				}
			}
			OperationUtils.updateDatumRelatedContentAfterSave(node, session, request);
			OperationUtils.saveActualCosts(node);
			
			//check if it is set as true then 
			if(this.calcDaysSpentPriorToSpud)
			{
				//assign unit to the value base on daysSpentPriorToSpud
				CustomFieldUom thisConverter=new CustomFieldUom(commandBean,Operation.class,"daysSpentPriorToSpud");
				OperationUtils.updateDaysSpentPriorToSpud(operation.getOperationUid(), thisConverter);
			}
			

			// set operation uid to DWOM if this is from EDM Well Import
			EdmWellImportSessionData edmWellImportSessionData = (EdmWellImportSessionData) session.getAttribute(EDMDepotWellImportDataNodeListener.EDM_WELL_IMPORT_SESSION_DATA);
			if (edmWellImportSessionData != null) {
				DepotWellOperationMapping dwom = (DepotWellOperationMapping)edmWellImportSessionData.getDwom();
				dwom.setIsDeleted(false);
				dwom.setMappingBehavior("edm_well_import");
				dwom.setOperationUid(operation.getOperationUid());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(dwom);
				session.removeAttribute(EDMDepotWellImportDataNodeListener.EDM_WELL_IMPORT_SESSION_DATA);
				
				this.publishOperationEvent(session, request, dwom, operation.getWellUid(), operation.getWellboreUid(), operation.getOperationUid());
				
			}
			else if(Boolean.parseBoolean(node.getDynaAttr().get("isNewlyCreated").toString())) 
			{	
				// create DWOM when mapping behavior is edm_well_import found (wellbore level) for newly created operation
				String hql = "from DepotWellOperationMapping where (isDeleted is null or isDeleted = false) and mappingBehavior='edm_well_import' and wellUid=:wellUid and wellboreUid=:wellboreUid";
				List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"wellUid", "wellboreUid"}, new Object[] {operation.getWellUid(), operation.getWellboreUid()});
				if (result.size() > 0) {
					DepotWellOperationMapping dwom = (DepotWellOperationMapping)result.get(0);
					dwom.setDepotWellOperationMappingUid(null);
					dwom.setDepotOperationUid(null);
					dwom.setDepotOperationName(null);
					dwom.setOperationUid(operation.getOperationUid());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(dwom);
						
					this.publishOperationEvent(session, request, dwom, operation.getWellUid(), operation.getWellboreUid(), operation.getOperationUid());
				}
			}
		}
	}
		
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof GeneralComment) {
			GeneralComment thisGeneralComment = (GeneralComment)object;
			thisGeneralComment.setCommentType("operation_comment");
			thisGeneralComment.setDailyUid(null);
		
		}
		
		if(object instanceof Operation) {
			Operation thisOperation = (Operation)object;
			
			String opWellName = (String) node.getDynaAttr().get("wellName");
			String opRigName = (String) node.getDynaAttr().get("rigName");
			String opWellOnOffShore = (String) node.getDynaAttr().get("onOffShore");
			String opWellboreName = (String) node.getDynaAttr().get("wellBoreName");
			
			if (thisOperation.getOperationCode() != null){
				String opCode = thisOperation.getOperationCode();
				String relatedOpCode = OperationUtils.getMobilizationRelatedOpCode(opCode);
				if (relatedOpCode !=null){
					node.getDynaAttr().put("reportingDatumOffset", null);
				}
			}
			
			String reportingDatumOffset = (String) node.getDynaAttr().get("reportingDatumOffset");
			String datumType = (String) node.getDynaAttr().get("datumType");
			String offsetMsl = (String) node.getDynaAttr().get("offsetMsl");
			String entryType = (String) node.getDynaAttr().get("entryType");
			String campaignName = getDynaAttr(node, "campaignName");
			//String surfaceSlantLength = (String) node.getDynaAttr().get("surfaceSlantLength");
			String slantAngle = (String) node.getDynaAttr().get("slantAngle");			
			Object objIsGLRef = node.getDynaAttr().get("isGlReference");
			Boolean isGlReference = objIsGLRef==null?false:Boolean.parseBoolean(objIsGLRef.toString());
			
			Double dblSlantAngle = toDouble(slantAngle, commandBean.getUserLocale());
			Double surfaceSlantLength = 0.0;
			
			if (carryOverRigOnHireDateToStartdate) {
				thisOperation.setStartDate(thisOperation.getRigOnHireDate());
			}
			
			if (StringUtils.isNotBlank(reportingDatumOffset) && StringUtils.isNotBlank(slantAngle) && "1".equals(entryType)) {
				Double reportingDatumOffsetRaw = 0.0;
				Double slantAngleRaw = 0.0;
				Double dblReportingDatumOffset = toDouble(reportingDatumOffset, commandBean.getUserLocale());
				
				CustomFieldUom thisConverterField = new CustomFieldUom(commandBean, OpsDatum.class, "reportingDatumOffset");
				thisConverterField.setBaseValueFromUserValue(dblReportingDatumOffset);
				reportingDatumOffsetRaw = thisConverterField.getBasevalue();
				
				thisConverterField.setReferenceMappingField(OpsDatum.class, "slantAngle");
				thisConverterField.setBaseValueFromUserValue(dblSlantAngle);
				slantAngleRaw = thisConverterField.getBasevalue();
				
				//check angle unit is it in degree or radian, if in degree need to convert to radian for calculation
				
				double convertionFactor = 1.0;
				double thisSlantAngle;
				
				if ("Degree".equals(thisConverterField.getUomLabel())) convertionFactor = 1 / 57.2957549575;
				else if ("Gradian".equals(thisConverterField.getUomLabel())) convertionFactor = 0.015707963;
				
				thisSlantAngle = slantAngleRaw * convertionFactor;
				surfaceSlantLength = reportingDatumOffsetRaw / Math.cos(thisSlantAngle);
				
				thisConverterField.setReferenceMappingField(OpsDatum.class, "surfaceSlantLength");			
				
				thisConverterField.setBaseValue(surfaceSlantLength);
				surfaceSlantLength = thisConverterField.getConvertedValue();
				
			}
			
			thisOperation.setSysOperationSubclass("well");
			
			// validate uniqueness here, if failed, return immediately, before anything get created below
			if (StringUtils.isNotBlank(opRigName)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from RigInformation where (isDeleted = false or isDeleted is null) and rigName = :rigName", "rigName", opRigName);
				if (list.size() > 0) {
					status.setDynamicAttributeError(node, "rigName", "The rig '" + opRigName + "' already exists in database");
					status.setContinueProcess(false, true);
				}
			}
			
			
			if (thisOperation.getOperationCode().equals("PRD") && node.getInfo().isNewlyCreated()) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and operationCode='PRD' and wellUid = :wellUid", "wellUid", thisOperation.getWellUid() );
				if (list != null && list.size() > 0) {   
					status.setFieldError(node, "operationCode", "There's an existing Production operation in this well"); 
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (StringUtils.isNotBlank(campaignName)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Campaign where (isDeleted = false or isDeleted is null) and campaignName = :campaignName", "campaignName", campaignName);
				if (list != null && list.size() > 0) {
					status.setDynamicAttributeError(node, "campaignName", "The campaign '" + campaignName + "' already exists in database");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (status.isProceed()) {
				if(StringUtils.isNotBlank(opRigName)){
					RigInformation thisRigInformation = CommonUtil.getConfiguredInstance().createRigInformation(opRigName);
					commandBean.getSystemMessage().addInfo("A new rig has been created with minimal information from the Well Operation screen. Please make sure that the Rig Information module is reviewed and relevant details are added in.");
					thisOperation.setRigInformationUid(thisRigInformation.getRigInformationUid());
				}
				
				if(StringUtils.isNotBlank(opWellName)){
					Well thisWell = CommonUtil.getConfiguredInstance().createWell(opWellName, opWellOnOffShore);
					commandBean.getSystemMessage().addInfo("A new well has been created with minimal information from the Well screen. Please make sure that the Well module is reviewed and relevant details are added in.");
					thisOperation.setWellUid(thisWell.getWellUid());
				}
				
				if(StringUtils.isNotBlank(opWellboreName)){
					Wellbore thisWellbore = CommonUtil.getConfiguredInstance().createWellbore(opWellboreName, thisOperation.getWellUid(), null);
					commandBean.getSystemMessage().addInfo("A new wellbore has been created with minimal information from the Well Operation screen. Please make sure that the Wellbore module is reviewed and relevant details are added in.");
					thisOperation.setWellboreUid(thisWellbore.getWellboreUid());
				}
				
				//String opWellUid = thisOperation.getWellUid();
				if (StringUtils.isNotBlank(opWellOnOffShore)) {
					CommonUtil.getConfiguredInstance().setWellOnOffShore(thisOperation.getWellUid(), opWellOnOffShore);
					//commandBean.getSystemMessage().addInfo("wellUid:"+thisOperation.getWellUid()+"\n"+"OffOn:"+opWellOnOffShore);
				}
				
				if (StringUtils.isNotBlank(opWellOnOffShore) && "OFF".equalsIgnoreCase(opWellOnOffShore)) {
					if (StringUtils.isNotBlank(reportingDatumOffset) && StringUtils.isBlank(datumType)) {
						status.setDynamicAttributeError(node, "datumType", "Please select a value");
						status.setContinueProcess(false, true);
						return;
					}
				}
				
				if (DefaultDatumLookupHandler.MSL.equals(thisOperation.getDefaultDatumUid())) {
					thisOperation.setDefaultDatumUid(null);
					thisOperation.setzRtToGround(null);
				}
				
				if (StringUtils.isBlank(thisOperation.getOperationUid())) {
					// when adding a new operation
					if (StringUtils.isNotBlank(reportingDatumOffset) && StringUtils.isNotBlank(datumType)) {
						// and user choose to create a new datum
						CustomFieldUom datumConverter = new CustomFieldUom(commandBean, OpsDatum.class, "reportingDatumOffset");
						datumConverter.setBaseValueFromUserValue(this.toDouble(reportingDatumOffset, commandBean.getUserLocale()), false);
						Double dblReportingDatumOffset = datumConverter.getBasevalue();

						datumConverter.setReferenceMappingField(OpsDatum.class, "offsetMsl");
						datumConverter.setBaseValueFromUserValue(this.toDouble(offsetMsl, commandBean.getUserLocale()), false);
						Double dblOffsetMsl = datumConverter.getBasevalue();
						
						datumConverter.setReferenceMappingField(OpsDatum.class, "slantAngle");
						datumConverter.setBaseValueFromUserValue(this.toDouble(slantAngle, commandBean.getUserLocale()), false);
						dblSlantAngle = datumConverter.getBasevalue();

						commandBean.overrideThreadLocalCurrentSelectedDatum(isGlReference, datumType, dblReportingDatumOffset, dblOffsetMsl, dblSlantAngle, surfaceSlantLength, entryType);
					} else {
						// MSL
						commandBean.overrideThreadLocalCurrentSelectedDatum(isGlReference,datumType, 0, 0, 0, 0, entryType);
					}
				}
				
				if (StringUtils.isNotBlank(campaignName)) {
					Campaign campaign = CommonUtil.getConfiguredInstance().createCampaignOperation(campaignName);
					commandBean.getSystemMessage().addInfo("A new campaign has been created with minimal information from the Well Operation screen. Please make sure that the Campaign module is reviewed and relevant details are added in.", true, true);
					thisOperation.setOperationCampaignUid(campaign.getCampaignUid());
				}
			}
			
			//check wellbore
			// skip if current operating mode is DEPOT - this rule is not applicable because wellboreUid is not in db during addToStore.
			if (commandBean.getOperatingMode() != BaseCommandBean.OPERATING_MODE_DEPOT) {
				String wellboreUid = thisOperation.getWellboreUid();
				String wellUid = thisOperation.getWellUid();
				
				String sql = "select wellboreUid from Wellbore where wellUid =:wellUid and wellboreUid =:wellboreUid";
				String[] paramsFields = {"wellboreUid", "wellUid"};
				Object[] paramsValues = {wellboreUid, wellUid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
				if (lstResult.size()<=0) {
					status.setContinueProcess(false, true);
					commandBean.getSystemMessage().addError("Selected wellbore does not belong to the well, please re-select", true);
				}
			}
			
			//do checking for actual cost for santos 
			String[] afeRefFieldValues = new String[] {thisOperation.getProjectNo(),thisOperation.getSecondaryProjectNo(), thisOperation.getOtherProjectNo()};
			String[] afeRefFieldNames = new String[] {"projectNo","secondaryProjectNo", "otherProjectNo"};
			for (int i=0; i<afeRefFieldValues.length ; i++) {
				String costAmount = CommonUtils.null2EmptyString(node.getDynaAttr().get("operationActualCost.costAmount"+i));
				if (StringUtils.isBlank(afeRefFieldValues[i]) && StringUtils.isNotBlank(costAmount)) {
					status.setFieldError(node,afeRefFieldNames[i], "Project No is required.");
					status.setContinueProcess(false, true);
				}
			}			

			// VALIDATE WHEN START DATE IS ENTERED, END DATE MUST BE ENTERED
			/*if (thisOperation.getDryHoleEndDateTime()== null && thisOperation.getDryHoleStartDateTime()!= null ){
				status.setFieldError(node, "dryHoleEndDateTime", "Dry Hole End Date is required.");
				status.setContinueProcess(false, true);
			}*/
			
			// VALIDATE WHEN END DATE IS ENTERED, START DATE MUST BE ENTERED
			if (thisOperation.getDryHoleEndDateTime()!= null && thisOperation.getSpudDate()== null ){
				status.setFieldError(node, "spudDate", "Spud Date & Time is required.");
				status.setContinueProcess(false, true);
			}
		
			//DATE CHECKING, IF END-DATE IS EARLIER THAN START DATE
			if (thisOperation.getDryHoleEndDateTime()!= null && thisOperation.getSpudDate()!= null ){
				if (thisOperation.getDryHoleEndDateTime().before(thisOperation.getSpudDate())) {
					status.setFieldError(node, "dryHoleEndDateTime", "Dry Hole End Date cannot be earlier than Spud Date & Time.\nPlease re-select.");
					status.setContinueProcess(false, true);
					return;
				}
				
						
			}

			//Update monthly currency key
			UOMMapping mapping = UOMManager.getCurrentClassPropertyMapping(ReportDaily.class, "daycost", false, thisOperation.getUomTemplateUid());
			Date startDate = thisOperation.getStartDate();
			Date sysOpsStartDate = thisOperation.getSysOperationStartDatetime();
			String strMonthlyDate = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
			if(startDate != null) {
				strMonthlyDate = sdf.format(startDate);
			} else if(sysOpsStartDate != null) {
				strMonthlyDate = sdf.format(sysOpsStartDate);
			}
			if (StringUtils.isNotBlank(strMonthlyDate)) {
				thisOperation.setMonthlyCurrencyKey(strMonthlyDate+mapping.getUnitShortHand());
			}
			
			if (session.getCurrentOperation() != null){
				if(thisOperation.getUomTemplateUid()!=null && !thisOperation.getUomTemplateUid().equals(session.getCurrentOperation().getUomTemplateUid())) {
					String strSql = "FROM Daily WHERE (isDeleted = false or isDeleted is null) AND operationUid=:thisOperationUid";
					String[] paramsFields = {"thisOperationUid"};
					Object[] paramsValues = {thisOperation.getOperationUid()};
					List<Daily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					for (Daily daily : lstResult){
						CommonUtil.getConfiguredInstance().updateDailyMonthlyCurrencyKey(daily.getDailyUid(), mapping);
					}
				}
			}
			
			//Logic to calculate dry hole duration, if spud date and dryhole date did not change, this will not kick in
			//If either one change, set null and recalculate for whole operation report daily dry hole fields
			if (session.getCurrentOperation() != null){
				if (thisOperation.getSpudDate() != null && thisOperation.getDryHoleEndDateTime() != null) {
					if (!thisOperation.getSpudDate().equals(session.getCurrentOperation().getSpudDate()) || !thisOperation.getDryHoleEndDateTime().equals(session.getCurrentOperation().getDryHoleEndDateTime())) {
						
						String sql = null;
						String[] paramsFields = {"operationUid"};
						Object[] paramsValues = {thisOperation.getOperationUid()};
						sql = "UPDATE ReportDaily rd SET rd.dryHoleDuration=null, rd.dryHoleNptDuration=null, rd.costPerSecond=null, rd.dryHoleCost=null "
								+ "WHERE rd.operationUid =:operationUid AND (rd.isDeleted = false or rd.isDeleted is null) ";
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sql, paramsFields , paramsValues);
						
						OperationUtils.calculateDaysSinceDryHole(thisOperation.getOperationUid(), thisOperation.getGroupUid(), thisOperation.getSpudDate(), thisOperation.getDryHoleEndDateTime());
					}
				} else {
					
					String sql = null;
					String[] paramsFields = {"operationUid"};
					Object[] paramsValues = {thisOperation.getOperationUid()};
					sql = "UPDATE ReportDaily rd SET rd.dryHoleDuration=null, rd.dryHoleNptDuration=null, rd.costPerSecond=null, rd.dryHoleCost=null "
							+ "WHERE rd.operationUid =:operationUid AND (rd.isDeleted = false or rd.isDeleted is null) ";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sql, paramsFields , paramsValues);
					
				}
			}
			
			//Logic to calculate spud to td duration, if spud date and td date time did not change, this will not kick in
			//If either one change, set null and recalculate for whole operation report daily spud to td field
			if (session.getCurrentOperation() != null){
				if (thisOperation.getSpudDate() != null && thisOperation.getTdDateTdTime() != null) {
					if (!thisOperation.getSpudDate().equals(session.getCurrentOperation().getSpudDate()) || !thisOperation.getTdDateTdTime().equals(session.getCurrentOperation().getTdDateTdTime())) {
						
						String sql = null;
						String[] paramsFields = {"operationUid"};
						Object[] paramsValues = {thisOperation.getOperationUid()};
						sql = "UPDATE ReportDaily rd SET rd.spudToTdDuration=null "
								+ "WHERE rd.operationUid =:operationUid AND (rd.isDeleted = false or rd.isDeleted is null) ";
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sql, paramsFields , paramsValues);
						
						OperationUtils.calculateSpudToTdDurationDaily(thisOperation.getOperationUid(), thisOperation.getGroupUid(), thisOperation.getSpudDate(), thisOperation.getTdDateTdTime());
					}
				} else {
					
					String sql = null;
					String[] paramsFields = {"operationUid"};
					Object[] paramsValues = {thisOperation.getOperationUid()};
					sql = "UPDATE ReportDaily rd SET rd.spudToTdDuration=null "
							+ "WHERE rd.operationUid =:operationUid AND (rd.isDeleted = false or rd.isDeleted is null) ";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sql, paramsFields , paramsValues);
					
				}
			}
		}
		
		if (status.isProceed() && object instanceof TargetZone) {
			TargetZone thisTargetZone = (TargetZone) object;
			Operation thisOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, thisTargetZone.getOperationUid());
			thisTargetZone.setWellUid(thisOperation.getWellUid());
			thisTargetZone.setWellboreUid(thisOperation.getWellboreUid());
		}
		
		//Set isNewlyCreated to DynaAttribute
		node.getDynaAttr().put("isNewlyCreated", node.getInfo().isNewlyCreated());
		
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof Operation) {
			Operation thisOperation = (Operation)object;
			
			String thiswellUid = thisOperation.getWellUid();
			
			String strSql = "FROM Well WHERE (isDeleted = false or isDeleted is null) and wellUid = :thisWellUid";
			String[] paramsFields = {"thisWellUid"};
			Object[] paramsValues = {thiswellUid};
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			//boolean onShoreWell = false;
			if (lstResult.size() > 0){
				Well tmpWell = (Well) lstResult.get(0);
				node.getDynaAttr().put("onOffShore", tmpWell.getOnOffShore());
				node.getDynaAttr().put("country", tmpWell.getCountry());
				//onShoreWell = "ON".equals(tmpWell.getOnOffShore());
				
				if(tmpWell.getGlHeightMsl() != null){
					CustomFieldUom glElevationUOM = new CustomFieldUom(commandBean, Well.class, "glHeightMsl");
					glElevationUOM.setBaseValueFromUserValue(tmpWell.getGlHeightMsl());
					node.getDynaAttr().put("well.glHeightMsl", glElevationUOM.getFormattedValue());
				}
			}
			
			String wellCountry = OperationUtils.wellCountry(thiswellUid);
			node.getDynaAttr().put("wellCountry", wellCountry);
			
			//get dvd plan name
			/*String thisoperationUid = thisOperation.getOperationUid();
			paramsFields[0] = "thisoperationUid";
			paramsValues[0] = thisoperationUid;
			strSql = "select df.fieldValue as dvdplanid from DynamicField df where (isDeleted = false or isDeleted is null) and df.fieldUid = 'operation.dvdplanid' and df.fieldKey = :thisoperationUid";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			String operationPlanPhaseUid = "";
			if (lstResult.size()>0)
			{
				Object a = (Object) lstResult.get(0);
				if (a != null) operationPlanPhaseUid = a.toString();
			}
			
			paramsFields[0] = "operationPlanPhaseUid";
			paramsValues[0] = operationPlanPhaseUid;
			strSql = "select df.fieldValue as planname from DynamicField df, OperationPlanPhase opp where (opp.isDeleted = false or opp.isDeleted is null) and df.fieldUid = 'operation_plan_phase.planname' and df.fieldKey = :operationPlanPhaseUid";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			String planname = "";
			if (lstResult.size()>0)
			{
				Object a = (Object) lstResult.get(0);
				if (a != null) planname = a.toString();
			}			
			node.getDynaAttr().put("planname", planname);*/
			
			//get dvd plan name
			OperationUtils.setDvdPlanName(node, thisOperation);
			
			//Check LookAhead Plan Data
			strSql = "from OperationPlanTask where (isDeleted = false or isDeleted is null) and operationUid = :thisOperationUid";
			String[] paramsFields2 = {"thisOperationUid"};
			Object[] paramsValues2 = {thisOperation.getOperationUid()};
			
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
			
			if (lstResult2.size() > 0){
				String lookaheadPlanName = thisOperation.getOperationName() + " Lookahead Plan";
				setDynaAttr(node, "lookaheadPlan", lookaheadPlanName);
			}
			
			String uomDatumUid = userSelection.getUomDatumUid();
			if (StringUtils.isNotBlank(uomDatumUid)) {
				UOMMapping userDatumMapping = DatumManager.getUOMMappingForUserDatumOffset();
				double userDatumOffset = DatumManager.getUserDatumOffsetInCurrentUOM(uomDatumUid, commandBean.getUserLocale());
				
				if (userDatumMapping != null) node.setCustomUOM("@rtmsl", userDatumMapping);
				node.getDynaAttr().put("rtmsl", userDatumOffset);
				
				if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT){
					if (userDatumMapping != null) node.setCustomUOM("@currentDatumDisplayValue", userDatumMapping);
					node.getDynaAttr().put("currentDatumDisplayValue", userDatumOffset);
				}
				
				//Calculate @datumTopElevation field which are total the OffsetMsl and reportingDatumOffset 
				OpsDatum opsDatum = DatumManager.getOpsDatum(uomDatumUid);
				double OffsetMsl = 0.00;
				if(opsDatum.getOffsetMsl() != null) {
				    OffsetMsl = opsDatum.getOffsetMsl();
				}
				double reportingDatumOffset = opsDatum.getReportingDatumOffset();
				CustomFieldUom thisConverterField = new CustomFieldUom(commandBean, OpsDatum.class, "OffsetMsl");
			
				thisConverterField.setBaseValue(OffsetMsl);                     
                              
                if (userDatumMapping != null) node.setCustomUOM("@opsDatum.offsetMsl", userDatumMapping);
                node.getDynaAttr().put("opsDatum.offsetMsl", thisConverterField.getConvertedValue());

				thisConverterField = new CustomFieldUom(commandBean, OpsDatum.class, "reportingDatumOffset");
				
				double datumTopElevation = OffsetMsl + reportingDatumOffset;
				
				thisConverterField.setBaseValue(datumTopElevation);
				
				if (userDatumMapping != null) node.setCustomUOM("@datumTopElevation", userDatumMapping);
				node.getDynaAttr().put("datumTopElevation", thisConverterField.getConvertedValue());
				
				Double slantAngle = null;
				if(opsDatum.getSlantAngle()!=null){
					slantAngle = opsDatum.getSlantAngle();
					thisConverterField = new CustomFieldUom(commandBean, OpsDatum.class, "slantAngle");
					
					thisConverterField.setBaseValue(slantAngle);
					String test = String.valueOf(thisConverterField.getConvertedValue());
					
					if (userDatumMapping != null) node.setCustomUOM("@slantAngle", thisConverterField.getUOMMapping());
					node.getDynaAttr().put("slantAngle", test);
				}
				
				Double surfaceSlantLength = null;
				if(opsDatum.getSurfaceSlantLength()!=null){
					surfaceSlantLength = opsDatum.getSurfaceSlantLength();
					thisConverterField = new CustomFieldUom(commandBean, OpsDatum.class, "surfaceSlantLength");
					
					thisConverterField.setBaseValue(surfaceSlantLength);
					
					if (userDatumMapping != null) node.setCustomUOM("@surfaceSlantLength", thisConverterField.getUOMMapping());
					node.getDynaAttr().put("surfaceSlantLength", thisConverterField.getConvertedValue());
				}
				
			}else{
				if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT){
					node.getDynaAttr().put("currentDatumDisplayValue", "MSL");
				}
			}

			if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT){
				node.getDynaAttr().put("currentDatumDisplayLabel", OperationUtils.getLabelForOperationDefaultDatum(userSelection.getUomDatumUid(), commandBean.getUserLocale(), userSelection.getGroupUid()));
			}
			
			//add full operation which can be used in report
			String completeOperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), thisOperation.getOperationUid(), null, useOperationNameDelimiter);

			if (StringUtils.isNotBlank(completeOperationName)){
				node.getDynaAttr().put("completeOperationName", completeOperationName);
			}
			
			OperationUtils.setDatumRelatedContentOnLoad(node, userSelection);
			
			OperationUtils.setAfeAllowEditFlag(node);
			
			OperationUtils.setAllowChangeOperationType(node);
			
			// opsTeams is defined as a required field, but while in edit mode,
			// the field is hidden and the value will not be used, so this is to
			// trick the validator that it has a value
			node.getDynaAttr().put("opsTeams", "_empty_");
			
			
			
			String queryString = "FROM Daily where (isDeleted=false or isDeleted is null) and operationUid=:operationUid order by dayDate desc";
			List<Daily> dailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", thisOperation.getOperationUid());
			if (dailyList.size()>0) {
				OperationUtils.calculateTotalFluidCost(dailyList, node);
				OperationUtils.calculateDaysOnWell(dailyList, node);				
			}			
			
			OperationUtils.calculateCostRelatedContent(node, true,true);		
			
			OperationUtils.setLockOperationFlag(node);

			OperationUtils.setCanManageTightHoleUserFlag(node, request);
			//The setCanSetTightHoleFlag function is for getting the canSetTightHole value from user screen so that this value can be used for checking in operation screen 
			//for determine whether the Is Tight Hole field is editable or not
			OperationUtils.setCanSetTightHoleFlag(node, request);
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			//Ticket:20091023-0635-dlee
			double spudToTdDuration = CommonUtil.getConfiguredInstance().spud_td_duration_simple_date_calc(thisOperation.getSpudDate(), thisOperation.getTdDateTdTime());
			thisConverter.setReferenceMappingField(Operation.class, "days_spent_prior_to_spud");
			thisConverter.setBaseValue(spudToTdDuration);
			node.getDynaAttr().put("spud_td_duration_simple_date_calc", thisConverter.getConvertedValue());
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@spud_td_duration_simple_date_calc", thisConverter.getUOMMapping());
			}
			

			
			if (thisOperation.getStartDate()!= null) {
				double actualDurationDays = CommonUtil.getConfiguredInstance().calculateActualDuration(thisOperation.getOperationUid(), thisOperation.getStartDate());
				
				if (calculateActualDurationFromActivity){
					if (userSelection!=null && userSelection.getDailyUid()!=null){
						actualDurationDays = CommonUtil.getConfiguredInstance().daysOnOperation(thisOperation.getOperationUid(), userSelection.getDailyUid());
					}
				}
				
				thisConverter.setReferenceMappingField(Operation.class, "plannedDuration");
				thisConverter.setBaseValue(actualDurationDays);
				node.getDynaAttr().put("actualDuration", thisConverter.getConvertedValue());
				
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@actualDuration", thisConverter.getUOMMapping());
				}
			}
			
			//GET DRILLING OPERATION DATA
			strSql = "FROM Operation WHERE operationCode = 'DRLLG' AND wellUid = :wellUid AND (isDeleted IS NULL OR isDeleted = '')";
			List list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellUid", thisOperation.getWellUid());
			String thisOperationUid = "";
			if (list2.size() > 0) {
				Operation listOperation = (Operation) list2.get(0);
				thisOperationUid = listOperation.getOperationUid();			
			}

			//GET DRILLING OPERATION REPORT DAILY DATA
			if(StringUtils.isNotBlank(thisOperationUid))
			{
				thisConverter.setReferenceMappingField(ReportDaily.class, "depthMdMsl");
				String thisCurrentMD = CommonUtil.getConfiguredInstance().getCurrentMD(thisOperationUid);

				if (thisCurrentMD != null)
				{
					thisConverter.setBaseValue(Double.parseDouble(thisCurrentMD.toString()));	
					node.getDynaAttr().put("depthMdMsl", thisConverter.getConvertedValue());
					if (thisConverter.isUOMMappingAvailable()){					
						node.setCustomUOM("@depthMdMsl", thisConverter.getUOMMapping());
					}
				}
				
				String thisCurrentTVD = CommonUtil.getConfiguredInstance().getCurrentTVD(thisOperationUid);
				if (thisCurrentTVD != null)
				{
					thisConverter.setReferenceMappingField(ReportDaily.class, "depthTvdMsl");
					thisConverter.setBaseValue(Double.parseDouble(thisCurrentTVD.toString()));
					node.getDynaAttr().put("depthTvdMsl", thisConverter.getConvertedValue());
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@depthTvdMsl", thisConverter.getUOMMapping());				
					}				
				}
							
			}
			
			if(thisOperation.getWellUid() != null || StringUtils.isNotBlank(thisOperation.getWellUid()))
			{
				String defaultDatumUid = CommonUtil.getConfiguredInstance().getKBelevation(thisOperation.getWellUid());
				if (defaultDatumUid !=null)
				{
					thisConverter.setReferenceMappingField(OpsDatum.class, "reportingDatumOffset");
					Double kbElevation = 0.00;
					String datumReferencePoint = "MSL";
					String datumCode = "";
					String datumLabel = "";
					
					OpsDatum datum = DatumManager.getOpsDatum(defaultDatumUid);
					if (datum !=null)
					{
						kbElevation = datum.getReportingDatumOffset();
						thisConverter.setBaseValue(kbElevation);
						datumLabel = thisConverter.getFormattedValue();
						if(datum.getDatumCode() != null || StringUtils.isNotBlank(datum.getDatumCode())) {
							datumCode = datum.getDatumCode();
						}
						if(datum.getDatumReferencePoint() != null || StringUtils.isNotBlank(datum.getDatumReferencePoint())) {
							datumReferencePoint = datum.getDatumReferencePoint();
						}
					}
					datumLabel = datumLabel + " " + datumCode + " " +  datumReferencePoint;
					node.getDynaAttr().put("kbElevation", datumLabel);		
						
				}
			}
			
			if(thisOperation.getWellboreUid() != null || StringUtils.isNotBlank(thisOperation.getWellboreUid()))
			{					
				if (OperationUtils.isLastOperation(thisOperation.getWellboreUid())){
					node.getDynaAttr().put("isLastOperation", "true");
				}
			}	
			
			
			if (thisOperation.getRigInformationUid()!=null) {
				RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, thisOperation.getRigInformationUid());
				if (rig!=null) {
					node.getDynaAttr().put("rigType", rig.getRigSubType());
					node.getDynaAttr().put("drillingEquipmentDescription", rig.getDrillingEquipmentDescription());
				}
			}
			
			// Rig Information standard fields jwong
			if(StringUtils.isNotBlank(thisOperation.getRigInformationUid())){
				RigInformation rigInformation = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, thisOperation.getRigInformationUid());
				if(rigInformation != null){
					Map<String, Object> RigInformationFieldValues = PropertyUtils.describe(rigInformation);
					for (Map.Entry entry : RigInformationFieldValues.entrySet()) {
						setDynaAttr(node, "rigInformation." + entry.getKey(), entry.getValue());
					}
				}
			}
			
			OperationUtils.setActualCosts(commandBean, node);	
			
			//Ticket: 20030 - to generate days lapsed for LTA event type
			Double daysLapsed = OperationUtils.calculateLastLtaDaysLapsed(thisOperation.getOperationUid(),userSelection);
			CustomFieldUom thisConverterField = new CustomFieldUom(commandBean,ReportDaily.class, "days_since_lta");
			thisConverterField.setBaseValue(daysLapsed);
			if (thisConverterField.isUOMMappingAvailable()){
				node.setCustomUOM("@daysLapsedLTA",thisConverterField.getUOMMapping());
			}
			node.getDynaAttr().put("daysLapsedLTA",thisConverterField.getConvertedValue());
		}
	}
	
	
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof Operation) {
			
			Operation operation = (Operation) object;
			operation.setWellUid(userSelection.getWellUid());
			operation.setWellboreUid(userSelection.getWellboreUid());
			OperationUtils.setDefaultOperationStartDate(node, userSelection);
			operation.setWellboreFinalPurpose("DEV");
			//comment this off as the field is a mandatory field so that user will be more aware of the type they are selecting
			//operation.setOperationCode("DRLLG");
			OperationUtils.populateUomTemplateForNewOperation(node, userSelection);
			
			operation.setTightHole(false);
			
			boolean onShoreWell = OperationUtils.isOnShoreWell(userSelection.getWellUid());
			
			node.getDynaAttr().put("onOffShore", (onShoreWell ? "ON" : "OFF"));
			OperationUtils.setDefaultDatumRelatedValueForNewOperation(node, userSelection);			
			node.getDynaAttr().put("editable", true);
					
			_afterNewEmptyInputNodeLoad(commandBean, meta, node,userSelection, request);
			OperationUtils.setAccessToOpsTeamOnlyFlag(node, userSelection);
			
			//set operation name prefix
			String operationPrefix = OperationUtils.getOperationNamePrefix(userSelection, operation, null);
			if (StringUtils.isNotBlank(operationPrefix)) operation.setOperationName(operationPrefix);
			
		
		}
	}
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {

		String wellUid = "";
		if (node.getData() instanceof Operation && ("1".equals(request.getParameter("fromWell"))|| "1".equalsIgnoreCase(request.getParameter("fromWellbore"))))
		{
			
			if (StringUtils.isNotBlank(request.getParameter("copyFromOperationUid"))) {
				String sourceOperationUid = request.getParameter("copyFromOperationUid");
				node.getDynaAttr().put("currentCopyFromOperationUid", sourceOperationUid);
				Operation selectedOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, sourceOperationUid);
				if (selectedOperation!=null) {
					Operation newOperation = (Operation) BeanUtils.cloneBean(selectedOperation);
					newOperation.setOperationUid(null);
					newOperation.setOperationCode(null);
					node.setData(newOperation);
				}
			}
			
			Operation thisOperation = (Operation) node.getData();
			
			commandBean.getFlexClientControl().setDisableAllCancelButtons(true);
			String wellboreUid = request.getParameter("wellboreUid");
			Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(wellboreUid);
			Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(wellbore.getWellUid());
			
			if(this.mapCountryToUomTemplate && this.countryUomTemplateMapping.size() > 0 && StringUtils.isNotBlank(well.getCountry())) {
				//String uomTemplateUid = this.getUomTemplateBasedOnCountry(well.getCountry());
				String uomTemplateName = "Main Template";
				if(StringUtils.isNotBlank(this.countryUomTemplateMapping.get(well.getCountry())))
					uomTemplateName = this.countryUomTemplateMapping.get(well.getCountry());
				String hql = "FROM UomTemplate WHERE name=:name AND groupUid=:groupUid AND (isDeleted IS NULL OR isDeleted = FALSE)";
				List results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"name", "groupUid"}, new Object[] {uomTemplateName, userSelection.getGroupUid()}, QueryProperties.create().setFetchFirstRowOnly());
				if(results.size() > 0) {
					//session.setCurrentUOMTemplateUid(PropertyUtils.getProperty(results.get(0), "uomTemplateUid").toString());
					PropertyUtils.setProperty(node.getData(), "uomTemplateUid", PropertyUtils.getProperty(results.get(0), "uomTemplateUid").toString());
				}
				
			}
			
			thisOperation.setWellUid(wellbore.getWellUid());
			thisOperation.setWellboreUid(wellboreUid);
			if (StringUtils.isNotBlank(this.defaultCompanyField) && StringUtils.isNotEmpty(GroupWidePreference.getValue(userSelection.getGroupUid(),"defaultCompany"))) {
				PropertyUtils.setProperty(thisOperation, this.defaultCompanyField, GroupWidePreference.getValue(userSelection.getGroupUid(),"defaultCompany"));
			}
			node.getDynaAttr().put("disableWellbore", "1");
			boolean onShoreWell = OperationUtils.isOnShoreWell(wellbore.getWellUid());
			node.getDynaAttr().put("onOffShore", (onShoreWell ? "ON" : "OFF"));
			OperationUtils.setActualCosts(commandBean, node);
			
			boolean isNewWellbore = OperationUtils.isNewWellbore(wellboreUid);
			node.getDynaAttr().put("isNewWellbore",  (isNewWellbore ? "1" : "0"));
			String wellCountry = OperationUtils.wellCountry(wellbore.getWellUid());
			node.getDynaAttr().put("wellCountry", wellCountry);
			
			//set country to AU when empty record is added.
			if (node.getData() instanceof Operation){
				String hql = "select country from Well where wellUid = :wellUid";
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "wellUid", ("".equals(wellUid)?well.getWellUid():wellUid));
				if(list.size() > 0)
				{
					for(Object obj: list){
						if("Australia".equals(obj) || "AU".equals(obj)){
							{
									node.getDynaAttr().put("country", obj);
							}
						}
					}
				}
			}
			
		}
		
		// set operation name if this is from EDM Well Import
		UserSession session = UserSession.getInstance(request);
		EdmWellImportSessionData edmWellImportSessionData = (EdmWellImportSessionData) session.getAttribute(EDMDepotWellImportDataNodeListener.EDM_WELL_IMPORT_SESSION_DATA);
		if (edmWellImportSessionData != null) {
			Operation thisOperation = (Operation) node.getData();
			thisOperation.setRigInformationUid(edmWellImportSessionData.getRigUid());
			thisOperation.setOperationName(edmWellImportSessionData.getWellName());
			thisOperation.setWellUid(edmWellImportSessionData.getWellUid());
			thisOperation.setWellboreUid(edmWellImportSessionData.getWellboreUid());
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof Operation) {
			Operation operation = (Operation) object;
			Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid());
			operation.setWellUid(userSelection.getWellUid());
			operation.setWellboreUid(userSelection.getWellboreUid());
			OperationUtils.setDefaultOperationStartDate(node, userSelection);
			operation.setWellboreFinalPurpose("DEV");
			//comment this off as the field is a mandatory field so that user will be more aware of the type they are selecting
			//operation.setOperationCode("DRLLG");
			if (StringUtils.isNotBlank(this.defaultCompanyField) && StringUtils.isNotEmpty(GroupWidePreference.getValue(userSelection.getGroupUid(),"defaultCompany"))) {
				PropertyUtils.setProperty(operation, this.defaultCompanyField, GroupWidePreference.getValue(userSelection.getGroupUid(),"defaultCompany"));
			}
			operation.setTightHole(false);
			operation.setRigWorkTimeSchedule(null);
			
			OperationUtils.setDefaultDatumRelatedValueForNewOperation(node, userSelection);
			node.getDynaAttr().put("editable", true);
			OperationUtils.setCanSetTightHoleFlag(node, request);
			boolean onShoreWell = OperationUtils.isOnShoreWell(userSelection.getWellUid());
			
			node.getDynaAttr().put("onOffShore", (onShoreWell ? "ON" : "OFF"));
			
			if (userSelection!=null && userSelection.getOperationUid()!=null){
				Operation selectedOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, userSelection.getOperationUid());
				
				if (selectedOperation!=null && StringUtils.isNotBlank(selectedOperation.getDefaultDatumUid())){
					OpsDatum datum = DatumManager.getOpsDatum(selectedOperation.getDefaultDatumUid());
					if (datum!=null){
						CustomFieldUom converter = new CustomFieldUom(commandBean);
						if (datum.getDatumReferencePoint()!=null) node.getDynaAttr().put("datumReferencePoint", datum.getDatumReferencePoint());
						if (datum.getReportingDatumOffset()!=null){
							converter.setReferenceMappingField(OpsDatum.class, "reportingDatumOffset");
							converter.setBaseValue(datum.getReportingDatumOffset());
							node.getDynaAttr().put("reportingDatumOffset", converter.getConvertedValue());
						}
						if (datum.getDatumCode()!=null) node.getDynaAttr().put("datumType", datum.getDatumCode());
						if (datum.getOffsetMsl()!=null) {
							converter.setReferenceMappingField(OpsDatum.class, "offsetMsl");
							converter.setBaseValue(datum.getOffsetMsl());
							node.getDynaAttr().put("offsetMsl", converter.getConvertedValue());
						}
						if(datum.getIsGlReference()!=null && datum.getIsGlReference()){
							node.getDynaAttr().put("isGlReference", "true");
						}else {
							node.getDynaAttr().put("isGlReference", ""); 
						}
					}
				}
			}
			
			_afterNewEmptyInputNodeLoad(commandBean, meta, node,userSelection, request);
			OperationUtils.setAccessToOpsTeamOnlyFlag(node, userSelection);
			
			//check if it is true then display the default template
			if(this.defaultUOMTemplate){ 
				OperationUtils.populateUomTemplateForNewOperation(node, userSelection);
			}
			
			
			
			boolean isNewWellbore = OperationUtils.isNewWellbore(userSelection.getWellboreUid());
			node.getDynaAttr().put("isNewWellbore",  (isNewWellbore ? "1" : "0"));
			String wellCountry = OperationUtils.wellCountry(userSelection.getWellUid());
			node.getDynaAttr().put("wellCountry", wellCountry);
		}
	}

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "(isDeleted = false or isDeleted is null)";
		
		String wellExplorerWellboreUid = null;
		String wellExplorerOperationUid = null;
		UserSession session = null;
		if (request != null) {
			wellExplorerWellboreUid = request.getParameter(WellExplorerConstant.WELLBORE_UID);
			wellExplorerOperationUid = request.getParameter(WellExplorerConstant.OPERATION_UID);
			session = UserSession.getInstance(request);
		}
		
		String dynaAttrWellUid = (String) commandBean.getRoot().getDynaAttr().get("wellUid");
		if (StringUtils.isNotBlank(dynaAttrWellUid)) {
			if (!com.idsdatanet.d2.drillnet.constant.UI.SELECT_OPTION_ALL.equals(dynaAttrWellUid)) {
				query.addParam("customFilterWellUid", commandBean.getRoot().getDynaAttr().get("wellUid"));
				customCondition += " and wellUid = :customFilterWellUid";
			}
		} else {
			if (StringUtils.isNotBlank(wellExplorerOperationUid) && session != null) {
				customCondition += " and operationUid = :customFilterOperationUid";
				query.addParam("customFilterOperationUid", wellExplorerOperationUid);
				
				String dailyUid = ApplicationUtils.getConfiguredInstance().getLastDailyUidInOperation(wellExplorerOperationUid);
				if(StringUtils.isNotBlank(dailyUid)) {
					session.setCurrentDailyUid(dailyUid);
				} else {
					session.setCurrentOperationUid(wellExplorerOperationUid);
				}
			} else if (StringUtils.isNotBlank(wellExplorerWellboreUid)) {
				customCondition += " and wellboreUid = :customFilterWellboreUid";
				query.addParam("customFilterWellboreUid", wellExplorerWellboreUid);
			} else {
				customCondition += " and operationUid = :customFilterOperationUid";
				query.addParam("customFilterOperationUid", userSelection.getOperationUid()); // use userSelection whenever possible (instead of UserSession)
			}
		}
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		
		Object obj = node.getData();
		if (obj instanceof Operation) {
			Operation operation = (Operation) obj;
			OperationUtils.deleteOperationFromOpTeam(operation);
			
			// Delete LAR Setting
			OperationUtils.deleteLARmapping(operation.getOperationUid());
			
			//cascade delete operation structure
			ApplicationUtils.getConfiguredInstance().setLastAccessedOperationUid(operation.getOperationUid());
			
			// delete operation from DepotWellOperationMapping and EdmSync 
			EdmUtils.deleteOperationDataFromDepotWellOperationMappingAndEdmSync(operation.getWellUid(), operation.getWellboreUid(), operation.getOperationUid());
			
			List<String> changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(operation.getOperationUid());
			D2ApplicationEvent.publishTableChangedEvent(Operation.class, changedRowPrimaryKeys);
			
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
			session.setCurrentOperationUid(null);
			session.setCurrentDailyUid(null);
			if (commandBean.getFlexClientControl() != null) {
				commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			}
		}
		
		//session.setCurrentOperationUid(null);
		//session.setCurrentDailyUid(null);
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
	
	
	
	private String getDynaAttr(CommandBeanTreeNode node, String dynaAttrName) throws Exception {
		Object dynaAttrValue = node.getDynaAttr().get(dynaAttrName);
		if (dynaAttrValue != null) {
			return dynaAttrValue.toString();
		}
		return null;
	}
	
	private void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		}
	}

	private double toDouble(String text, Locale locale) {
		if (StringUtils.isNotBlank(text)) {
			try {
				return (Double) NumberUtils.parseNumber(text, Double.class, NumberFormat.getInstance(locale));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
	public void beforeDataNodeDelete(CommandBean commandBean, 
			CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, 
			HttpServletRequest request) throws Exception{
		
		Object object = node.getData();
		
		if(object instanceof Operation)
		{
			Operation thisOperation = (Operation)object;
			
			if(thisOperation.getOperationCode().startsWith(Constant.DRLLG))
			{
				if(thisOperation.getWellboreUid() != null || StringUtils.isNotBlank(thisOperation.getWellboreUid()))
				{					
					String strSql = "FROM Operation WHERE wellboreUid = :wellboreUid AND operationCode not like 'DRLLG%' AND (isDeleted IS NULL OR isDeleted = '')";
					List list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellboreUid", thisOperation.getWellboreUid());				
					if (list2.size() > 0) {					
						status.setContinueProcess(false);
						commandBean.getSystemMessage().addError("This Operation cannot be deleted, there is other operation types (Completion/Intervention/Workover) having the same Wellbore as this Operation.");
					}
				}
			}
			
			// Delete DVD Summary records for current operation
			CommonUtil.getConfiguredInstance().deleteExistingDvdSummary("operation", session.getCurrentOperationUid());
			EnvironmentDischargesUtil.calculateTotalWellAfterWellOpsDeleted(commandBean, node, session);
		}
	}

	public boolean allowedAction(CommandBeanTreeNode node, UserSession session,
			String targetClass, String action, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		if (node.isRoot())
		{
			if ("add".equalsIgnoreCase(action))
			{
				if ("Operation".equalsIgnoreCase(targetClass))
				{
					Map<String, CommandBeanTreeNode> mapList = node.getChild(Operation.class.getSimpleName());
					for (Iterator iter = mapList.entrySet().iterator(); iter.hasNext();) 
					{
						Map.Entry entry = (Map.Entry) iter.next();
						CommandBeanTreeNode nodeOperation = (CommandBeanTreeNode)entry.getValue();
						Object disableWellbore = nodeOperation.getDynaAttr().get("disableWellbore");
						if (disableWellbore!=null && "1".equalsIgnoreCase(disableWellbore.toString()))
							return false;
					}
						
				}	
			}
		}
		return true;
	}

	public void setTournetDailyRedirect(String tournetDailyRedirect) {
		this.tournetDailyRedirect = tournetDailyRedirect;
	}

	public String getTournetDailyRedirect() {
		return tournetDailyRedirect;
	}
	
	/**
	 * EDM > Function to call applicationEventPublisher, publish event after create DWOM mapping.
	 * 
	 * @param session
	 * @param request
	 * @param dwom
	 * @param WellUid
	 * @param WellboreUid
	 * @param OperationUid
	 * @throws Exception
	 */
	private void publishOperationEvent(UserSession session, HttpServletRequest request, DepotWellOperationMapping dwom, String WellUid, String WellboreUid, String OperationUid) throws Exception {
        // publish well mapping event
		ImportExportServerProperties serverProperties = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, dwom.getImportExportServerPropertiesUid());
        DepotUserSelectionSnapshot userSelection = new DepotUserSelectionSnapshot(session);
        userSelection.setWellUid(WellUid);
        userSelection.setWellboreUid(WellboreUid);
        userSelection.setOperationUid(OperationUid);
			DepotWellMappingEvent event = new DepotWellMappingEvent(DepotWellMappingEvent.EVENT_CREATE_OR_UPDATE, this, request, serverProperties.getDataTransferType(), dwom, serverProperties, userSelection);
			
			this.applicationEventPublisher.publishEvent(event);
	}
	
}