package com.idsdatanet.d2.drillnet.reportSummary;

import java.io.File;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CementJob;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.model.PlugAndAbandon;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class ReportSummaryDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	
	private Map< String, ReportDaily> reportDailyList = new HashMap<String, ReportDaily>();
	private Map<String, String> completeOperationNameList = new HashMap<String, String>(); 
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	private String nullToEmptyString(Object value) {
		if (value==null) return "";
		return value.toString();
	}
	
	public String getDailyUidWithSelectedField(String selectedOperationUid, Date selectedDateFrom, Date selectedDateTo, String selectedQcStatus) throws Exception{
		String operationUid = "";
		String dateFrom ="";
		String dateTo = "";
		String qcStatus = "";
		ArrayList pF = new ArrayList(); 
		ArrayList pV = new ArrayList(); 

		if (StringUtils.isNotBlank(selectedOperationUid)) {
			operationUid = " AND rd.operationUid=:operationUid";
			pF.add("operationUid");
			pV.add(selectedOperationUid);
		}
		if (selectedDateFrom!=null){
			dateFrom = " AND d.dayDate>=:dateFrom";
			pF.add("dateFrom");
			pV.add(selectedDateFrom);
		}
		if (selectedDateTo!=null){
			dateTo = " AND d.dayDate<=:dateTo";
			pF.add("dateTo");
			pV.add(selectedDateTo);
		}
		if (StringUtils.isNotBlank(selectedQcStatus)){
			qcStatus = " AND rd.qcFlag=:qcFlag";
			pF.add("qcFlag");
			pV.add(selectedQcStatus);
		}
		
		String filterCondition = "";
		String sql = "select d.dailyUid from Daily d, ReportDaily rd " +
				"where (d.isDeleted = false or d.isDeleted is null) " +
				"AND (rd.isDeleted = false or rd.isDeleted is null) " +
				"and rd.dailyUid=d.dailyUid " + operationUid + dateFrom + dateTo + qcStatus;			
		
		String[] paramsFields = (String[]) pF.toArray(new String [pF.size()]);
		Object[] paramsValues = pV.toArray();
		List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		if(list.size() > 0)  {
			for (String dailyUid : list) {
				if (StringUtils.isNotBlank(dailyUid)) {
					filterCondition += (StringUtils.isNotBlank(filterCondition)?"','":"") + dailyUid;
				}
			}			
		}																
		
		return "'"+filterCondition+"'";
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		
		String filter = (String) commandBean.getRoot().getDynaAttr().get("filter");
		String currentOperationUid = nullToEmptyString(commandBean.getRoot().getDynaAttr().get("operationUid"));
		if (!currentOperationUid.equals(userSelection.getOperationUid())) {
			commandBean.getRoot().getDynaAttr().put("operationUid", userSelection.getOperationUid());
			commandBean.getRoot().getDynaAttr().put("filter", "");
			filter = "";
		}
		
		List<Object> output_maps = new ArrayList<Object>();		
		DateFormat df = new SimpleDateFormat("dd MMM yyyy");
		String selectedSearchType = null;
		Date selectedDateFrom = null;
		Date selectedDateTo = null;
		String selectedQcStatus = null;
		String selectedDailyUid =null;
		String selectedOperationUid = "";
		List arrayParamNames = new ArrayList();
		List arrayParamValues = new ArrayList();
		if (StringUtils.isNotBlank(filter)){
			String[] filterField = filter.split(",");
			selectedSearchType = filterField[0];
			selectedDateFrom = (StringUtils.isNotBlank(filterField[1])?df.parse(filterField[1]):null);
			selectedDateTo = (StringUtils.isNotBlank(filterField[1])?df.parse(filterField[2]):null);
			selectedQcStatus = filterField[3];
			
			commandBean.getRoot().getDynaAttr().put("searchType", selectedSearchType);
			commandBean.getRoot().getDynaAttr().put("qcStatus", selectedQcStatus);
			commandBean.getRoot().getDynaAttr().put("dateFrom", selectedDateFrom);
			commandBean.getRoot().getDynaAttr().put("dateTo", selectedDateTo);
		} else {
			commandBean.getRoot().getDynaAttr().put("searchType", "CURRENT WELL");
			commandBean.getRoot().getDynaAttr().put("qcStatus", "");
			String queryString = "SELECT MIN(dayDate), MAX(dayDate) FROM Daily WHERE (isDeleted=false or isDeleted is NULL) and operationUid=:operationUid";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", userSelection.getOperationUid());
			if (list.size()>0) {
				selectedDateFrom = (Date) list.get(0)[0];
				selectedDateTo = (Date) list.get(0)[1];
			}
			commandBean.getRoot().getDynaAttr().put("dateFrom", selectedDateFrom);
			commandBean.getRoot().getDynaAttr().put("dateTo", selectedDateTo);
		}		
		
		if (!"ALL WELLS".equals(selectedSearchType)) selectedOperationUid = userSelection.getOperationUid();
		selectedDailyUid = getDailyUidWithSelectedField(selectedOperationUid, selectedDateFrom, selectedDateTo, selectedQcStatus);
		
		if (meta.getTableClass().equals(ReportDaily.class)) {
			String strSql = "FROM ReportDaily a, Operation o, Daily d " +
					"WHERE (a.isDeleted IS NULL OR a.isDeleted = FALSE) " +
					"AND (o.isDeleted IS NULL OR o.isDeleted = FALSE) " +
					"AND (d.isDeleted IS NULL OR d.isDeleted = FALSE) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND d.operationUid=o.operationUid " +
					"AND d.dailyUid IN (" + selectedDailyUid + ") " +
					"ORDER BY o.operationName, d.dayDate";
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			List<Object[]> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			
			for(Object[] objResult: items){
				ReportDaily reportDaily = (ReportDaily) objResult[0];
				String reportType = nullToEmptyString(CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(reportDaily.getOperationUid()));
				if (reportType.equals(reportDaily.getReportType())) {
					output_maps.add(reportDaily);
				}
			}						
		} else  if (meta.getTableClass().equals(CementJob.class)) {
			String strSql = "FROM CementJob a, Operation o, Daily d " +
					"WHERE (a.isDeleted IS NULL OR a.isDeleted = FALSE) " +
					"AND (o.isDeleted IS NULL OR o.isDeleted = FALSE) " +
					"AND (d.isDeleted IS NULL OR d.isDeleted = FALSE) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND d.operationUid=o.operationUid " +
					"AND d.dailyUid IN (" + selectedDailyUid + ") " +
					"ORDER BY o.operationName, d.dayDate";
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			List<Object[]> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			
			for(Object[] objResult: items){
				output_maps.add(objResult[0]);
			}						
		} else if (meta.getTableClass().equals(LeakOffTest.class)) {
			String strSql = "FROM LeakOffTest a, Operation o, Daily d " +
					"WHERE (a.isDeleted IS NULL OR a.isDeleted = FALSE) " +
					"AND (o.isDeleted IS NULL OR o.isDeleted = FALSE) " +
					"AND (d.isDeleted IS NULL OR d.isDeleted = FALSE) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND d.operationUid=o.operationUid " +
					"AND d.dailyUid IN (" + selectedDailyUid + ") " +
					"ORDER BY o.operationName, d.dayDate";
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			List<Object[]> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			
			for(Object[] objResult: items){
				output_maps.add(objResult[0]);
			}						
		} else if (meta.getTableClass().equals(PlugAndAbandon.class)) {
			String strSql = "FROM PlugAndAbandon a, Operation o, Daily d " +
					"WHERE (a.isDeleted IS NULL OR a.isDeleted = FALSE) " +
					"AND (o.isDeleted IS NULL OR o.isDeleted = FALSE) " +
					"AND (d.isDeleted IS NULL OR d.isDeleted = FALSE) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND d.operationUid=o.operationUid " +
					"AND d.dailyUid IN (" + selectedDailyUid + ") " +
					"ORDER BY o.operationName, d.dayDate";
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			List<Object[]> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			
			for(Object[] objResult: items){
				output_maps.add(objResult[0]);
			}						
		}
		
		return output_maps;
	}	

	private Map<String, LookupItem> getLookupList(HttpServletRequest request, String uri) {
		Map<String, LookupItem> result = new HashMap<String, LookupItem>(); 
		try {
			result = LookupManager.getConfiguredInstance().getLookup(uri, new UserSelectionSnapshot(UserSession.getInstance(request)), null);
		} catch (Exception e) {
			System.out.println("lookup not found : " + uri);
		}
		return result;
	}
	
	private ReportDaily getReportDaily(String operationUid, String dailyUid) throws Exception {
		if (reportDailyList.containsKey(dailyUid)) return reportDailyList.get(dailyUid);
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		String sql = "FROM ReportDaily rd, Daily d " +
				"WHERE (d.isDeleted = false or d.isDeleted is null) " +
				"AND (rd.isDeleted = false or rd.isDeleted is null) " +
				"AND rd.dailyUid=d.dailyUid " +
				"AND d.dailyUid=:dailyUid " +
				"AND rd.reportType=:reportType";		
		String[] paramsFields = {"dailyUid","reportType"};
		Object[] paramsValues = {dailyUid, reportType};
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		if (list.size()>0) {
			Object[] obj_array = list.get(0);
			ReportDaily reportDaily = (ReportDaily) obj_array[0]; 
			reportDailyList.put(dailyUid, reportDaily);
			return reportDaily;
		}
		return null;
	}
	
	
	public String getDailyUidQcFlag(HttpServletRequest request, String operationUid, String dailyUid) throws Exception{
		String selectedDayQcFlag = "";
		Map<String, LookupItem>qcFlag = getLookupList(request, "xml://reportDaily.qcflag?key=code&amp;value=label");
		ReportDaily reportDaily = this.getReportDaily(operationUid, dailyUid);
		if (reportDaily!=null) {
			if (StringUtils.isNotBlank(reportDaily.getQcFlag())) {	
				LookupItem lookupItem = qcFlag.get(reportDaily.getQcFlag());
				if (lookupItem!=null) {
					selectedDayQcFlag = nullToEmptyString(lookupItem.getValue());	
				}
			
			} 
		}
		return selectedDayQcFlag;
	}
	
	
	public String getDayDate(String operationUid, String dailyUid) throws Exception{
		String selectedDayDate = "";
		DateFormat df = new SimpleDateFormat("dd MMM yyyy");
		ReportDaily reportDaily = this.getReportDaily(operationUid, dailyUid);
		if (reportDaily!=null) {
			Date date = (Date) reportDaily.getReportDatetime();
			selectedDayDate = "#"+ nullToEmptyString(reportDaily.getReportNumber())  + " ("+ df.format(date).toString() + ")";						
		}
		return selectedDayDate;
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
	
		Object object = node.getData();
	
		if (object instanceof ReportDaily) {		
			ReportDaily thisReportDaily = (ReportDaily) object;
			if (thisReportDaily.getDailyUid()!=null && thisReportDaily.getOperationUid()!=null){
				node.getDynaAttr().put("dailyUid", getDayDate(thisReportDaily.getOperationUid(),thisReportDaily.getDailyUid()));
				node.getDynaAttr().put("reportList", loadReportList(thisReportDaily.getDailyUid(),"DDR"));
				node.getDynaAttr().put("qcFlag", getDailyUidQcFlag(request,thisReportDaily.getOperationUid(),thisReportDaily.getDailyUid()));
				node.getDynaAttr().put("reportType", "DDR");
			}
		} else if (object instanceof CementJob) {		
			CementJob thisCementJob = (CementJob) object;
			if (thisCementJob.getDailyUid()!=null && thisCementJob.getOperationUid()!=null){
				node.getDynaAttr().put("dailyUid", getDayDate(thisCementJob.getOperationUid(),thisCementJob.getDailyUid()));
				node.getDynaAttr().put("reportList", loadReportList(thisCementJob.getDailyUid(),"DCCR"));
				node.getDynaAttr().put("qcFlag", nullToEmptyString(thisCementJob.getCheckedby()));
				node.getDynaAttr().put("reportType", "DCCR");
			}
		} else if (object instanceof LeakOffTest) {		
			LeakOffTest thisLeakOffTest = (LeakOffTest) object;
			if (thisLeakOffTest.getDailyUid()!=null && thisLeakOffTest.getOperationUid()!=null){
				node.getDynaAttr().put("dailyUid", getDayDate(thisLeakOffTest.getOperationUid(),thisLeakOffTest.getDailyUid()));
				node.getDynaAttr().put("reportList", loadReportList(thisLeakOffTest.getDailyUid(),"LOTR"));
				node.getDynaAttr().put("qcFlag", getDailyUidQcFlag(request,thisLeakOffTest.getOperationUid(),thisLeakOffTest.getDailyUid()));
				node.getDynaAttr().put("reportType", "LOTR");
			}
		} else if (object instanceof PlugAndAbandon) {		
			PlugAndAbandon thisPlugAndAbandon = (PlugAndAbandon) object;
			if (thisPlugAndAbandon.getDailyUid()!=null && thisPlugAndAbandon.getOperationUid()!=null){
				node.getDynaAttr().put("dailyUid", getDayDate(thisPlugAndAbandon.getOperationUid(),thisPlugAndAbandon.getDailyUid()));
				node.getDynaAttr().put("reportList", loadReportList(thisPlugAndAbandon.getDailyUid(),"PNAR"));
				node.getDynaAttr().put("qcFlag", getDailyUidQcFlag(request,thisPlugAndAbandon.getOperationUid(),thisPlugAndAbandon.getDailyUid()));
				node.getDynaAttr().put("reportType", "PNAR");
			}
		}
		
	}

	private String loadReportList(String dailyUid, String thisReportType) throws Exception {
		String reportInfo = "";
		List<Object[]> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, Daily d " +
				"WHERE rf.dailyUid=d.dailyUid " +
				"AND (rf.isDeleted=false or rf.isDeleted is null) " +
				"AND (d.isDeleted=false or d.isDeleted is null) " +
				"AND rf.reportType=:reportFilesReportType " +
				"AND d.dailyUid=:dailyUid " +
				"ORDER BY d.dayDate DESC", 
				new String[]{"reportFilesReportType", "dailyUid"}, 
				new Object[]{thisReportType, dailyUid});
					
		for(Object[] obj_array : items){
			ReportFiles thisReportFile = (ReportFiles) obj_array[0];
			Daily thisDaily = (Daily)obj_array[1];
			
			File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());	
				    
			if(report_file.exists()){			
				String displayName = thisReportFile.getDisplayName();
				if (StringUtils.isBlank(displayName)){
					displayName = thisReportFile.getReportType() + " " + getOperationName(thisDaily.getGroupUid(), thisDaily.getOperationUid()) + " Day "+ thisReportFile.getReportDayNumber();
				}
				reportInfo = "reportfileuid="  + URLEncoder.encode(thisReportFile.getReportFilesUid(),"utf-8") + "&reportname=" + URLEncoder.encode(displayName,"utf-8") + "&filename=" + URLEncoder.encode(thisReportFile.getReportFile(),"utf-8");
				break;
			}
		}
		return reportInfo;
	}
	
	public String getOperationName(String groupUid, String operationUid) throws Exception{
		if (completeOperationNameList.containsKey(operationUid)) return completeOperationNameList.get(operationUid);
		String operatioName = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, operationUid); 
		completeOperationNameList.put(operationUid, operatioName);
		return operatioName;
	}
	
	
}
