package com.idsdatanet.d2.drillnet.waterManagement;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.beanutils.PropertyUtils;
import com.idsdatanet.d2.core.model.WellFluid;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;

public class WaterManagementActionManager implements ActionManager {

	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if(com.idsdatanet.d2.core.web.mvc.Action.SAVE.equals(action)){
			
			Object object = node.getData();
			
			if (object instanceof WaterOutOfSite) {
				return new SaveWaterOutOfSiteActionHandler();
			}
			return null;
		}else{
			return null;
		}
	}

	/**
	 * Method for saved the water out of site records
	 * @param commandBean
	 * @param userSession
	 * @param aclManager
	 * @param request
	 * @param node
	 * @param key
	 * @throws Exception Standard Error Throwing Exception
	 * @return new ActionHandlerResponse
	 */
	private class SaveWaterOutOfSiteActionHandler implements ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			Object obj = node.getData();
			String waterSource="";
			String fluidType="";
			Double volumeOut =null;
			Double fluidPh =null;
			Double fluidConductivity =null;
			Double suspendedSolidDensity = null;
			Double dissolvedSolidDensity = null;
			Double organicContentDensity =null;
			if (obj instanceof WaterOutOfSite) {
				WaterOutOfSite WaterOutOfSite = (WaterOutOfSite) obj;

				if (WaterOutOfSite !=null) {
					WellFluid WellFluid = new WellFluid();

					waterSource = WaterOutOfSite.getWaterSource();
					fluidType = WaterOutOfSite.getFluidType();
					volumeOut = WaterOutOfSite.getVolumeOut();
					fluidPh = WaterOutOfSite.getFluidPh();
					fluidConductivity = WaterOutOfSite.getFluidConductivity();
					suspendedSolidDensity = WaterOutOfSite.getSuspendedSolidDensity();
					dissolvedSolidDensity = WaterOutOfSite.getDissolvedSolidDensity();
					organicContentDensity = WaterOutOfSite.getOrganicContentDensity();
					
					PropertyUtils.copyProperties(WellFluid, WaterOutOfSite);
					WellFluid.setWaterSource(waterSource);
					WellFluid.setFluidType(fluidType);
					WellFluid.setVolumeOut(volumeOut);
					WellFluid.setFluidPh(fluidPh);
					WellFluid.setFluidConductivity(fluidConductivity);
					WellFluid.setSuspendedSolidDensity(suspendedSolidDensity);
					WellFluid.setDissolvedSolidDensity(dissolvedSolidDensity);
					WellFluid.setOrganicContentDensity(organicContentDensity);
					WellFluid.setType("load");
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(WellFluid);
				}
			}
			
			return new ActionHandlerResponse(true);
		}
	}
	
}