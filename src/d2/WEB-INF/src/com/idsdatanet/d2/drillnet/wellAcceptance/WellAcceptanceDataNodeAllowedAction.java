package com.idsdatanet.d2.drillnet.wellAcceptance;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.WellAcceptanceCategory;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellAcceptanceDataNodeAllowedAction implements DataNodeAllowedAction {

	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof WellAcceptanceCategory) {
			if(StringUtils.equals(action, Action.DELETE_SELECTED) || StringUtils.equals(action, Action.DELETE))  return false;
		}
		
		return true;
	}
}
