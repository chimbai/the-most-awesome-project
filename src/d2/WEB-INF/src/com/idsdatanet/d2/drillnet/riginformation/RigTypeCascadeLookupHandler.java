package com.idsdatanet.d2.drillnet.riginformation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class RigTypeCascadeLookupHandler implements CascadeLookupHandler{
	private String lookupType=null;
	
	public String getLookupType() {
		return lookupType;
	}

	public void setLookupType(String lookupType) {
		this.lookupType = lookupType;
	}
	
	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String sql = "FROM CommonLookup "
				+ "WHERE (isDeleted IS NULL OR isDeleted=FALSE) "
				+ "AND (isActive IS NULL OR isActive=TRUE) "
				+ "AND lookupTypeSelection=:lookupType "
				+ "AND (parentReferenceKey=:key OR parentReferenceKey IS NULL OR parentReferenceKey='')"
				+ "ORDER BY lookupLabel";
		List<CommonLookup> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"lookupType","key"}, new String[] {this.lookupType,key});
		if(rs!=null && rs.size()>0){
			for (CommonLookup thisCommonLookup : rs) {
				result.put(thisCommonLookup.getShortCode(), new LookupItem(thisCommonLookup.getShortCode(), thisCommonLookup.getLookupLabel()));
			}
		}
		return result;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		String parentLookup = null;
		String sqlCommonLookupType = "SELECT parentLookup "
				+ "FROM CommonLookupType "
				+ "WHERE (isDeleted IS NULL OR isDeleted=FALSE) "
				+ "AND lookupTypeSelection=:lookupType";
		List<String> rsCommonLookupType = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlCommonLookupType,"lookupType",this.lookupType);
		if(rsCommonLookupType!=null && rsCommonLookupType.size()>0){
			parentLookup = rsCommonLookupType.get(0).toString();
		}
		
		String sql = "SELECT shortCode "
				+ "FROM CommonLookup "
				+ "WHERE (isDeleted IS NULL OR isDeleted=FALSE) "
				+ "AND (isActive IS NULL OR isActive=TRUE) "
				+ "AND lookupTypeSelection=:parentLookup "
				+ "ORDER BY lookupLabel";
		List<String> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"parentLookup",parentLookup);
		if(rs.size()>0){
			for (String parentShortCode : rs) {
				if(parentShortCode!=null && !parentShortCode.isEmpty()){
					CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(parentShortCode);
					cascadeLookupSet.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, parentShortCode, null)));
					result.add(cascadeLookupSet);
				}		
			}
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
		
	}

}
