package com.idsdatanet.d2.drillnet.casingSection;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CasingSectionCasingSizeLookupHandler implements LookupHandler{

	private URI xmlLookup = null;
	private String tallyType = null;

	public String getTallyType() {
		return this.tallyType;
	}

	public void setTallyType(String tallyType) {
		this.tallyType = tallyType;
	}
	
	public void setXmlLookup(URI xmlLookup) {
		this.xmlLookup = xmlLookup;
	}
	
	public URI getXmlLookup() {
		return this.xmlLookup;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		Map<String, LookupItem> casingLookup = LookupManager.getConfiguredInstance().getLookup(this.getXmlLookup().toString(), userSelection, null);			
		
		List<CasingSection> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM CasingSection WHERE (isDeleted = false or isDeleted is null) AND wellboreUid = :wellboreUid AND casingOd > 0 AND (tallyType IS NULL OR tallyType = '') ORDER BY sectionName, casingOd", "wellboreUid", userSelection.getWellboreUid());
		
		if("tubing".equals(this.tallyType)){
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM CasingSection WHERE (isDeleted = false or isDeleted is null) AND wellboreUid = :wellboreUid AND casingOd > 0 AND tallyType = 'tubing' ORDER BY sectionName, casingOd", "wellboreUid", userSelection.getWellboreUid());
		}	
		if("rod".equals(this.tallyType)){
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM CasingSection WHERE (isDeleted = false or isDeleted is null) AND wellboreUid = :wellboreUid AND casingOd > 0 AND tallyType = 'rod' ORDER BY sectionName, casingOd", "wellboreUid", userSelection.getWellboreUid());
		}
		
		if((list != null && list.size() > 0) && (casingLookup != null)) {
			for(CasingSection casingSection : list) {
				CustomFieldUom thisConverter = new CustomFieldUom((Locale) null, CasingSection.class, "casingOd");
				String strCasingOd 			 = thisConverter.formatOutputPrecision(casingSection.getCasingOd());
				String strCasingLabel = null;
				if(StringUtils.isNotBlank(strCasingOd) && casingLookup.containsKey(strCasingOd)) {
					LookupItem lookupItem = casingLookup.get(strCasingOd);
					if(lookupItem != null)
					{
						String uri = "xml://casingsection.section_name?key=code&value=label";
						Map<String, LookupItem> SectionNameLookup = LookupManager.getConfiguredInstance().getLookup(uri, null, null);
						
						if (SectionNameLookup.get(casingSection.getSectionName())!=null) {
							strCasingLabel = SectionNameLookup.get(casingSection.getSectionName()).getValue().toString();
						}else{							
							strCasingLabel = casingSection.getSectionName() + "(Lookup N/A)";
						}
						strCasingLabel = strCasingLabel + " - " + lookupItem.getValue();
						result.put(casingSection.getCasingSectionUid(), new LookupItem(casingSection.getCasingSectionUid(), strCasingLabel, lookupItem.getKey()));
					}
				}
			}
		}
		
		return result;
	}
}
