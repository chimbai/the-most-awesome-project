package com.idsdatanet.d2.drillnet.drillPlanExport;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WellLookupHandler implements LookupHandler{
	@Override
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		List<Object> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select wellUid, wellName from Well where (isDeleted = false or isDeleted is null) and (isDemoWell = false or isDemoWell is null) and wellUid in (select wellUid from Operation where (isDeleted = false or isDeleted is null) and operationCode = :operationCode) order by wellName", "operationCode", "DRLLG");
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		for(Object item: rs) {
			Object[] data = (Object[]) item;
			String wellUid = (String) data[0];
			String wellName = (String) data[1];
			result.put(wellUid, new LookupItem(wellUid, wellName));
		}
		return result;
	}
}
