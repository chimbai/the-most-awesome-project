package com.idsdatanet.d2.drillnet.bharun;


import com.idsdatanet.d2.core.stt.STTTownSideCallback;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class BharunSTTCallback implements STTTownSideCallback {
	public void afterSendToTownCompleted(UserSelectionSnapshot userSelection) {
		try {
			BharunUtils.updateCumulativeCircHour(userSelection.getDailyUid(),userSelection.getOperationUid());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}