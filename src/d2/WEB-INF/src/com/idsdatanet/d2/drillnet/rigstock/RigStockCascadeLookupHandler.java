package com.idsdatanet.d2.drillnet.rigstock;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * @author ekhoo
 *
 * Cascade lookup handler to get a list of unit based on the selected stock brand 
 */
public class RigStockCascadeLookupHandler implements CascadeLookupHandler{
	
	private String stockType = "rigbulkstock";
	
	public void setStockType(String stockType) {
		this.stockType = stockType;
	}
	
	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
	    String sql ="SELECT lookupRigStockUid, storedUnit FROM LookupRigStock where stockBrand=:stockName and (isDeleted = false or isDeleted is null) and " +
	    		"(type=:stockType or type = '' or type is NULL) and " +
	    		"(isActive = '1') and " +
				"(operationCode =:operationCode or operationCode ='' or operationCode is null) ";
				List<Object[]> listRigStockUnit = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { "stockName", "operationCode","stockType"}, new String[] { key, userSelection.getOperationType(), this.stockType });
				if (listRigStockUnit.size() > 0) {
					for (Object[] listRigStockUnitResult : listRigStockUnit) {
						if (listRigStockUnitResult[0] != null && listRigStockUnitResult[1] != null) {
							result.put(listRigStockUnitResult[0].toString(), new LookupItem(listRigStockUnitResult[0].toString(), listRigStockUnitResult[1].toString()));
						}
					}
				}
		
		return result;
	}
	
	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		String sql ="SELECT stockBrand from LookupRigStock where (isDeleted = false or isDeleted is null)" +
				" and (type=:stockType or type = '' or type is NULL) and " +
				"(operationCode =:operationCode or operationCode ='' or operationCode is null) ";
		List<Object> listStock = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"operationCode", "stockType"}, new String[] {userSelection.getOperationType(), this.stockType});
		if (listStock.size() > 0) {
			for (Object listStockResult : listStock) {
				if (listStockResult != null) {
					String stockBrand = listStockResult.toString();
					CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(stockBrand);
					cascadeLookupSet.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, stockBrand, null)));
					result.add(cascadeLookupSet);				
				}
			}
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		return null;
	}
}
