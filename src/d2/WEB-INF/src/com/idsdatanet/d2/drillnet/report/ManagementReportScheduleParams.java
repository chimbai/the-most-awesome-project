package com.idsdatanet.d2.drillnet.report;

import java.util.List;

public class ManagementReportScheduleParams {
	private boolean distinctReport = false;
	private String reportUserUid = null;
	private List operationTypes = null;
	
	public void setDistinctReport(boolean value){
		this.distinctReport = value;
	}
	
	public void setReportUserUid(String value){
		this.reportUserUid = value;
	}
	
	public boolean isDistinctReport(){
		return this.distinctReport;
	}
	
	public String getReportUserUid(){
		return this.reportUserUid;
	}
	
	public void setOperationTypes(List values) {
		this.operationTypes = values;
	}
	
	public List getOperationTypes() {
		return this.operationTypes;
	}
}
