package com.idsdatanet.d2.drillnet.report;

public class ReportJobStatus {
	public static final int STATUS_UNKNOWN = -1;
	public static final int STATUS_NO_JOB = 1;
	public static final int STATUS_JOB_RUNNING = 2;
	public static final int STATUS_JOB_DONE = 3;
	public static final int STATUS_JOB_ERROR = 4;
	
	private int status = STATUS_UNKNOWN;
	private String responseMessage = null;
	
	public ReportJobStatus(int status){
		this(status,null);
	}

	public ReportJobStatus(int status, String msg){
		this.status = status;
		this.responseMessage = msg;
	}
	
	public int getStatus(){
		return this.status;
	}
	
	public String getResponseMessage(){
		return this.responseMessage;
	}
}
