package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

/**
 * @author jwong
 *
 * Cascading lookup handler for returning a list Phase Code
 */
public class PhaseCodeCascadeLookupHandler implements CascadeLookupHandler{	
		
	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		if (key==null) key="";
	    String sql ="SELECT shortCode, name FROM LookupPhaseCode WHERE (isDeleted = false OR isDeleted is null) AND (operationCode=:operationCode OR operationCode='') AND (isActive = true OR isActive IS NULL) GROUP BY shortCode, name ORDER BY name";
		List<Object[]> listPhaseCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { "operationCode" }, new String[] { key });
		if (listPhaseCode.size() > 0) {
			for (Object[] listPhaseCodeResult : listPhaseCode) {
				String PhaseName = "";
				if (listPhaseCodeResult[1] != null) 
					PhaseName = listPhaseCodeResult[1].toString() + " (" + listPhaseCodeResult[0].toString() + ")";
				else PhaseName = " (" + listPhaseCodeResult[0].toString() + ")";
				result.put(listPhaseCodeResult[0].toString(), new LookupItem(listPhaseCodeResult[0].toString(), PhaseName));
			}
		}
		
		return result;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		Set<Map.Entry<String, LookupItem>> lookupOpsType = LookupManager.getConfiguredInstance().getLookup("xml://operationtype?key=code&amp;value=label", new UserSelectionSnapshot(UserSession.getInstance(request)),null).entrySet();
		
		CascadeLookupSet cascadeLookupSet1 = new CascadeLookupSet("");
		cascadeLookupSet1.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, "", null)));
		result.add(cascadeLookupSet1);	
		
		for (Map.Entry entry: lookupOpsType) {
			LookupItem x = (LookupItem) entry.getValue();
			String lookupKey = x.getKey().toString();
			CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(lookupKey);
			cascadeLookupSet.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, lookupKey, null)));
			result.add(cascadeLookupSet);		
		}

		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		return null;
	}

}
