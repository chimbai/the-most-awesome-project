package com.idsdatanet.d2.drillnet.multiWellStt;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.birt.report.model.api.util.StringUtil;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.stt.AbstractSTTEngine;
import com.idsdatanet.d2.core.stt.STTComConfig;
import com.idsdatanet.d2.core.stt.STTJobConfig;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;

public class MultiWellSttDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler, CommandBeanListener, ActionHandler {
	
	private Map<String, String> completeOperationNameList = new HashMap<String, String>(); 
	private Map<String, String> operationStatusList = new HashMap<String, String>(); 
	private Boolean setClear = true;
	
	private Log logger = LogFactory.getLog(this.getClass());
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	private String nullToEmptyString(Object value) {
		if (value==null) return "";
		return value.toString();
	}
	
	public String getOperationUidWithSelectedField(String selectedOperationUid, Date selectedDateFrom, Date selectedDateTo, String selectedOperationCode, String selectedRigInformationUid, String selectedField) throws Exception{
		String operationUid = "";
		String dateFrom ="";
		String dateTo = "";
		String operationCode = "";
		String rigInformationUid = "";
		String field = "";
		ArrayList pF = new ArrayList(); 
		ArrayList pV = new ArrayList(); 

		if (StringUtils.isNotBlank(selectedOperationUid)) {
			operationUid = " AND op.operationUid=:operationUid";
			pF.add("operationUid");
			pV.add(selectedOperationUid);
		}
		if (selectedDateFrom!=null){
			dateFrom = " AND d.dayDate>=:dateFrom";
			pF.add("dateFrom");
			pV.add(selectedDateFrom);
		}
		if (selectedDateTo!=null){
			dateTo = " AND d.dayDate<=:dateTo";
			pF.add("dateTo");
			pV.add(selectedDateTo);
		}
		if (StringUtils.isNotBlank(selectedOperationCode)){
			operationCode = " AND op.operationCode=:operationCode";
			pF.add("operationCode");
			pV.add(selectedOperationCode);
		}
		if (StringUtils.isNotBlank(selectedRigInformationUid)){
			rigInformationUid = " AND op.rigInformationUid=:rigInformationUid";
			pF.add("rigInformationUid");
			pV.add(selectedRigInformationUid);
		}
		if (StringUtils.isNotBlank(selectedField)){
			field = " AND w.field like :field";
			pF.add("field");
			pV.add(selectedField);
		}
		
		String filterCondition = "";
		String sql = "select distinct op.operationUid from Operation op, Daily d, ReportDaily rd, Well w " +
				"where (op.isDeleted = false or op.isDeleted is null) " +
				"AND (rd.isDeleted = false or rd.isDeleted is null) " +
				"AND (w.isDeleted = false or w.isDeleted is null) " +
				"AND (d.isDeleted = false or d.isDeleted is null) " +
				"and w.wellUid=op.wellUid " + 
				"and rd.dailyUid=d.dailyUid " +
				"and rd.operationUid=op.operationUid " + 
				operationUid + dateFrom + dateTo + operationCode + rigInformationUid + field;			
		
		String[] paramsFields = (String[]) pF.toArray(new String [pF.size()]);
		Object[] paramsValues = pV.toArray();
		List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		if(list.size() > 0)  {
			for (String operationList : list) {
				if (StringUtils.isNotBlank(operationList)) {
					filterCondition += (StringUtils.isNotBlank(filterCondition)?"','":"") + operationList;
				}
			}			
		}																
		setClear = true;
		return "'"+filterCondition+"'";
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		
		
		if (setClear){
			commandBean.getRoot().getDynaAttr().put("status", "");
			commandBean.getRoot().getDynaAttr().put("ops", "");
			commandBean.getRoot().getDynaAttr().put("sysMsg", "");
			commandBean.getSystemMessage().clear();
		}
		
		String filter = (String) commandBean.getRoot().getDynaAttr().get("filter");
		String currentOperationUid = nullToEmptyString(commandBean.getRoot().getDynaAttr().get("operationUid"));
		if (!currentOperationUid.equals(userSelection.getOperationUid())) {
			commandBean.getRoot().getDynaAttr().put("operationUid", userSelection.getOperationUid());
			commandBean.getRoot().getDynaAttr().put("filter", "");
			filter = "";
		}
		
		List<Object> output_maps = new ArrayList<Object>();		
		DateFormat df = new SimpleDateFormat("dd MMM yyyy");
		Date selectedDateFrom = null;
		Date selectedDateTo = null;
		String filteredOperationUid =null;
		String selectedOperationUid = "";
		String selectedOperationCode = null;
		String selectedRigInformationUid = null;
		String selectedField = null;	
		
		List arrayParamNames = new ArrayList();
		List arrayParamValues = new ArrayList();
		if (StringUtils.isNotBlank(filter)){
			String[] filterField = filter.split(",");
			if (filterField.length > 0)
				selectedOperationCode = filterField[0];
			if (filterField.length > 1){
				selectedDateFrom = (StringUtils.isNotBlank(filterField[1])?df.parse(filterField[1]):null);
			}
			if (filterField.length > 2){			
				selectedDateTo = (StringUtils.isNotBlank(filterField[2])?df.parse(filterField[2]):null);
			}
			if (filterField.length > 3){
				selectedRigInformationUid = nullToEmptyString(filterField[3]);
			}
			if (filterField.length > 4){
				selectedField = filterField[4];
			}
			
			selectedOperationUid = null;
			commandBean.getRoot().getDynaAttr().put("dateFrom", selectedDateFrom);
			commandBean.getRoot().getDynaAttr().put("dateTo", selectedDateTo);
			commandBean.getRoot().getDynaAttr().put("operationCode", selectedOperationCode);
			commandBean.getRoot().getDynaAttr().put("rigInformationUid", selectedRigInformationUid);
			commandBean.getRoot().getDynaAttr().put("field", selectedField);

		} else {
			selectedOperationUid = userSelection.getOperationUid();
			String queryString = "SELECT MIN(dayDate), MAX(dayDate) FROM Daily WHERE (isDeleted=false or isDeleted is NULL) and operationUid=:operationUid";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", userSelection.getOperationUid());
			if (list.size()>0) {
				selectedDateFrom = (Date) list.get(0)[0];
				selectedDateTo = (Date) list.get(0)[1];
			}
			commandBean.getRoot().getDynaAttr().put("dateFrom", selectedDateFrom);
			commandBean.getRoot().getDynaAttr().put("dateTo", selectedDateTo);
			commandBean.getRoot().getDynaAttr().put("operationCode", selectedOperationCode);
			commandBean.getRoot().getDynaAttr().put("rigInformationUid", selectedRigInformationUid);
			commandBean.getRoot().getDynaAttr().put("field", selectedField);
		}		
		
		if (!StringUtil.isBlank(selectedOperationUid) || selectedDateFrom!=null || selectedDateTo!=null || !StringUtil.isBlank(selectedOperationCode) || !StringUtil.isBlank(selectedRigInformationUid) || !StringUtil.isBlank(selectedField))
			filteredOperationUid = getOperationUidWithSelectedField(selectedOperationUid, selectedDateFrom, selectedDateTo, selectedOperationCode, selectedRigInformationUid, selectedField);
		else {
			filteredOperationUid="''";
		}
			
		
		if (meta.getTableClass().equals(Operation.class)) {
			String strSql = "FROM Operation o " +
					"WHERE (o.isDeleted IS NULL OR o.isDeleted = FALSE) " +
					"AND o.operationUid IN (" + filteredOperationUid + ") " +
					"ORDER BY o.operationName";
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			
			if (items.size() > 0){
				for(Object objResult: items){
					Operation operation = (Operation) objResult;
					operation.setOperationName(getOperationName(operation.getGroupUid(), operation.getOperationUid()));
					output_maps.add(operation);
				}
			}					
			else{
				commandBean.getSystemMessage().addError("No well(s) found");
			}
		}	
		return output_maps;
	}	

	
	
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		this.getOperationStatus(commandBean);
		
		Object object = node.getData();
		if (object instanceof Operation) {		
			Operation thisOperation= (Operation) object;
			if (thisOperation.getOperationUid()!=null){
				String getStatus = operationStatusList.get(thisOperation.getOperationUid());
				String sysMsg = (String) nullToEmptyString(commandBean.getRoot().getDynaAttr().get("sysMsg"));
				
				if ("1".equals(getStatus)){
					node.getDynaAttr().put("sttStatus", "Success");
				}
				else if ("0".equals(getStatus)){
					node.getDynaAttr().put("sttStatus", "Fail");
				}
				else {
					node.getDynaAttr().put("sttStatus", "");
				}
				if (StringUtils.isNotBlank(sysMsg))
					commandBean.getSystemMessage().addError(sysMsg);
			}
		} 
	}

	public void getOperationStatus(CommandBean commandBean) throws Exception{
		operationStatusList.clear();
		String status = (String) nullToEmptyString(commandBean.getRoot().getDynaAttr().get("status"));
		String ops = (String) nullToEmptyString(commandBean.getRoot().getDynaAttr().get("ops"));
		String[] statusList = null;
		String[] opsList = null;
		
		if (StringUtils.isNotBlank(status)){
			statusList = status.split(",");
		}
		if (StringUtils.isNotBlank(ops)){
			opsList = ops.split(",");
		}
		if (opsList != null && statusList != null){
			if (opsList.length > 0 && statusList.length > 0){
				for (int i =0; i < opsList.length; i++) {
					if (operationStatusList.containsKey(nullToEmptyString(opsList[i]))) continue;
					operationStatusList.put(nullToEmptyString(opsList[i]), nullToEmptyString(statusList[i]));
				}
			}
		}
			
	}
	
	public String getOperationName(String groupUid, String operationUid) throws Exception{
		if (completeOperationNameList.containsKey(operationUid)) return completeOperationNameList.get(operationUid);
		String operatioName = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, operationUid); 
		completeOperationNameList.put(operationUid, operatioName);
		return operatioName;
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if("1".equals(request.getParameter("send_to_town"))){
			try{
				String selectedOperations = request.getParameter("selected_operations");
				commandBean.getRoot().getDynaAttr().put("status", "");
				commandBean.getRoot().getDynaAttr().put("ops", "");
				
				if (StringUtils.isNotBlank(selectedOperations)){
					String[] selectedOperation = selectedOperations.split(",");
					for (int i=0; i < selectedOperation.length ; i++)
					{
						String successStatus = (String) nullToEmptyString(commandBean.getRoot().getDynaAttr().get("status"));		    
			        	if (successStatus.contains("0")) break;
			        	
						String selectedOp = selectedOperation[i].toString();
						List<UserSelectionSnapshot> session = this.getUserSelectionSnapshots(selectedOp);
						
						Iterator it = session.iterator();
						
				        while(it.hasNext())
				        {			     	
				        	UserSelectionSnapshot value= (UserSelectionSnapshot)it.next();
				        	
				        	MultiWellSTTActionHandler actionHandler =  new MultiWellSTTActionHandler(comConfig, jobConfigs);
				        	actionHandler.process((BaseCommandBean) commandBean, value, null, request,targetCommandBeanTreeNode, null);			        	
				        }					
					}		
					setClear = false;
					if (commandBean.getFlexClientControl() != null) {
						commandBean.getFlexClientControl().setReloadParentHtmlPage();
					}
				}
				else{
					commandBean.getSystemMessage().addError("No well(s) selected");
				}
				
				
			}catch(Exception e){
				this.logger.error(e.getMessage(), e);
				commandBean.getSystemMessage().addError("Send to town failed:\n" + e.getMessage());
			}		

		}
	}

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public ActionHandlerResponse process(BaseCommandBean commandBean,
			UserSession userSession, AclManager aclManager,
			HttpServletRequest request, CommandBeanTreeNode node, String key)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*
	 * ActionManager for send to town and refresh from town
	 */
	private STTComConfig comConfig;
	private List<STTJobConfig> jobConfigs;
	
	public void setComConfig(STTComConfig comConfig) {
		this.comConfig = comConfig;
	}

	public void setJobConfigs(List<STTJobConfig> jobConfigs) {
		this.jobConfigs = jobConfigs;
	}
	
	@SuppressWarnings("unchecked")
	private List<UserSelectionSnapshot> getUserSelectionSnapshots(String operationUid) throws Exception {
		List<UserSelectionSnapshot> list = new ArrayList<UserSelectionSnapshot>();
		
		if (StringUtils.isNotBlank(operationUid)) {
			List<Operation> operations = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and operationUid = :operationUid", "operationUid", operationUid);
			if (operations != null) {
				for (Operation operation : operations) {
					list.add(UserSelectionSnapshot.getInstanceForCustomOperation(AbstractSTTEngine.SYSTEM_USER, operation.getOperationUid()));
				}
			}
		} 	
		return list;
	}

}
