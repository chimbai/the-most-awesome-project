package com.idsdatanet.d2.drillnet.reportDaily;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ReportDailyCasingUtils {
	
	/**
	 * Auto Populates LOT/FIT Fields in ReportDaily object if
	 * GWP 'autoPopulateLOTFIT' is enabled
	 * @param commandBean
	 * @param node
	 * @param session
	 * @throws Exception
	 */
	public static void autoLookupLOTFIT(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception {
		if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_LOOKUP_FIT_LOT))){
			Object object = node.getData();
			
			ReportDaily thisReportDaily = null;
			CasingSection thisCasingSection = null;
			
			if (object instanceof ReportDaily) {
				thisReportDaily = (ReportDaily) object;
				thisReportDaily.setSysReportDailySubclass(ModuleConstants.MODULE_KEY_WELL);
			} else {
				return;
			} 
			
			Object o = CommonUtil.getConfiguredInstance().lastCasingSection(session.getCurrentGroupUid(), session.getCurrentWellboreUid(), session.getCurrentDailyUid());
			if (o == null) { return; }
			thisCasingSection = (CasingSection) o;
			ReportDailyCasingUtils.populateLOTFIT(commandBean, thisReportDaily, thisCasingSection);
		}
	}
	
	public static void populateLOTFIT(CommandBean commandBean, ReportDaily thisReportDaily, CasingSection thisCasingSection) throws Exception {
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "last_lot");
		if (thisCasingSection.getLot() != null) {
			thisConverter.setReferenceMappingField(ReportDaily.class, "last_lot");	
			thisConverter.setBaseValue(thisCasingSection.getLot());
			thisReportDaily.setLastLot(thisConverter.getConvertedValue());
		} else {
			thisReportDaily.setLastLot(null);
		}
		if (thisCasingSection.getFit() != null) {
			thisConverter.setReferenceMappingField(ReportDaily.class, "last_fit");
			thisConverter.setBaseValue(thisCasingSection.getFit());
			thisReportDaily.setLastFit(thisConverter.getConvertedValue());
		} else {
			thisReportDaily.setLastFit(null);
		}
	}
	
}