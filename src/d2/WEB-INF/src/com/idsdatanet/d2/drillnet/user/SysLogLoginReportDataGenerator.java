package com.idsdatanet.d2.drillnet.user;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.model.User;

public class SysLogLoginReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		
		
		String strSql = "FROM User WHERE (isDeleted IS NULL OR isDeleted = '') AND groupUid = :groupUid AND userUid IN (SELECT a.userUid FROM UserAclGroup a,AclGroup c WHERE c.aclGroupUid = a.aclGroupUid AND (a.isDeleted IS NULL OR a.isDeleted = '') AND c.groupUid=:groupUid)";
		String[] paramsFields = {"groupUid", "groupUid"};
		Object[] paramsValues = {userContext.getUserSelection().getGroupUid(), userContext.getUserSelection().getGroupUid()};
		List<Object> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if(ls != null)
		{
			for(Object obj: ls)
			{
				ReportDataNode thisReportNode = reportDataNode.addChild("SysLogLogin");
				
				User user = (User) obj;
				thisReportNode.addProperty("userUid", user.getUserUid());
				thisReportNode.addProperty("userName", user.getUserName());
				thisReportNode.addProperty("fname", user.getFname());
				
				if(user.getIsblocked() != null && user.getIsblocked() == true)
				{
					thisReportNode.addProperty("isblocked", "Yes");	
				}
				else
				{
					thisReportNode.addProperty("isblocked", "No");	
				}
								
				String strUserUid = user.getUserUid();
				Calendar todayDate = Calendar.getInstance();
				Integer intMonth =  todayDate.get(Calendar.MONTH);
				Integer intYear = todayDate.get(Calendar.YEAR);
				
				if(intMonth == 0)
				{
					intMonth = 12;
					intYear = intYear - 1;
				}
				
				String strQueryDate = "";
				if(intMonth < 10)
				{
					strQueryDate = intYear + "-0" + intMonth + "-%";
				}
				else
				{
					strQueryDate = intYear + "-" + intMonth + "-%";
				}
										
				//GET LOGIN SUCCESS COUNT
				String strSql2 = "SELECT COUNT(*) FROM SysLogLogin WHERE loginUserUid = :userUid AND loginSuccess = 1 AND loginDatetime LIKE '" + strQueryDate + "'";
				String[] paramsFields2 = {"userUid"};
				Object[] paramsValues2 = {strUserUid};
				
				List ls2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				if(ls2 != null)
				{
					Object loginSuccessCount = (Object) ls2.get(0); 
					thisReportNode.addProperty("loginSuccess", loginSuccessCount.toString());
				}
				
				//GET LOGIN FAILED COUNT
				String strSql3 = "SELECT COUNT(*) FROM SysLogLogin WHERE loginUserUid = :userUid AND loginError IS NOT NULL AND loginDatetime LIKE '" + strQueryDate + "'";
				String[] paramsFields3 = {"userUid"};
				Object[] paramsValues3 = {strUserUid};
				
				List ls3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3);
				if(ls3 != null)
				{
					Object loginErrorCount = (Object) ls3.get(0); 
					thisReportNode.addProperty("loginError", loginErrorCount.toString());
				}
				
				DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
								
				//GET LATEST LOGIN THIS MONTH
				String strSql4 = "SELECT MAX(loginDatetime) FROM SysLogLogin WHERE loginUserUid = :userUid AND loginSuccess = 1 AND loginDatetime LIKE '" + strQueryDate + "'";
				String[] paramsFields4 = {"userUid"};
				Object[] paramsValues4 = {strUserUid};
				
				List ls4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4);
				if(ls4 != null)
				{
					Object lastLoginThisMonth = (Object) ls4.get(0); 
					if(lastLoginThisMonth != null)
					{
						thisReportNode.addProperty("lastLoginThisMonth", dateFormatter.format(lastLoginThisMonth));
					}
					else
					{
						thisReportNode.addProperty("lastLoginThisMonth", "- No Login -");
					}
				}
				
				//GET LATEST LOGIN
				String strSql5 = "SELECT MAX(loginDatetime) FROM SysLogLogin WHERE loginUserUid = :userUid AND loginSuccess = 1";
				String[] paramsFields5 = {"userUid"};
				Object[] paramsValues5 = {strUserUid};
				
				List ls5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramsFields5, paramsValues5);
				if(ls5 != null)
				{
					Object lastLogin = (Object) ls5.get(0); 
					if(lastLogin != null)
					{
						thisReportNode.addProperty("lastLogin", dateFormatter.format(lastLogin));
					}
					else
					{
						thisReportNode.addProperty("lastLogin", "- No Login -");
					}
				}
						
			}
		}
	}
}
