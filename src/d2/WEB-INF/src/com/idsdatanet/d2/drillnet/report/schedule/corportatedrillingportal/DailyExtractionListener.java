package com.idsdatanet.d2.drillnet.report.schedule.corportatedrillingportal;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;



public class DailyExtractionListener implements CommandBeanListener {


	private static final String FIELD_EMAIL_ADDRESS ="emailAddressList";


	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		if (root.isRoot())
		{
			if (DailyReportExtractionService.getConfiguredInstance().getEnabled())
				root.getDynaAttr().put("enabled", "1");
			else
				commandBean.getSystemMessage().addWarning("CDP isn't Enabled");
		}
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		String[] emailList=null;	
		
		String input=(String)commandBean.getRoot().getDynaAttr().get(FIELD_EMAIL_ADDRESS);
		if (!StringUtils.isBlank(input))
			emailList=input.split(",");
			
		Integer status=DailyReportExtractionService.getConfiguredInstance().runExtractionAndSendEmail(request,emailList);
		commandBean.getSystemMessage().clear();
		switch(status)
		{
		case DailyReportExtractionService.SUCCESS:
			commandBean.getSystemMessage().addInfo("Report Generated & Sent",true);
			break;
		case DailyReportExtractionService.ERROR:
			commandBean.getSystemMessage().addError("ERROR Found");
			break;
		case DailyReportExtractionService.MISSING:
			commandBean.getSystemMessage().addWarning("Missing Mandatory Field");
			break;
		case DailyReportExtractionService.NOT_QCED:
			commandBean.getSystemMessage().addWarning("Report Not QCed");
			break;
		}
	}

	
	
}
