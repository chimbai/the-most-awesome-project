package com.idsdatanet.d2.drillnet.fileBridgeFilesManager;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class FileBridgeFilesManagerSortLookupHandler implements LookupHandler {

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {		

		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		result.put("", new LookupItem("", "Uploaded On"));
		result.put("sortByReportDatetime", new LookupItem("sortByReportDatetime", "Day"));
		
		return result;
	}
}
