package com.idsdatanet.d2.drillnet.connectionAnnotation;

import java.util.List;


import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Annotations;
import com.idsdatanet.d2.core.model.RigStateRaw;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ConnectionAnnotationDataNodeListener extends EmptyDataNodeListener {

	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		Object object = node.getData();
		if (object instanceof RigStateRaw) {
			RigStateRaw rsr = (RigStateRaw) node.getData();
			String strSql = "SELECT threshold, rootCause, description FROM Annotations WHERE rigStateRawUid=:rigStateRawUid AND (isDeleted = FALSE OR isDeleted IS NULL)";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigStateRawUid", rsr.getRigStateRawUid());

			if (lstResult.size() > 0) {
				Object[] obj = (Object[]) lstResult.get(0);
				node.getDynaAttr().put("threshold", obj[0].toString());
				node.getDynaAttr().put("rootCause", obj[1].toString());
				node.getDynaAttr().put("description", obj[2].toString());
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof RigStateRaw) {
			RigStateRaw rsr = (RigStateRaw) node.getData();
			Annotations ann;
			

			String strSql = "FROM Annotations WHERE rigStateRawUid=:rigStateRawUid AND (isDeleted = FALSE OR isDeleted IS NULL)";
			List<Annotations> antResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigStateRawUid", rsr.getRigStateRawUid());

			if (antResult.size() > 0) {
				ann = (Annotations) antResult.get(0);
			} else {
				ann = new Annotations();
				ann.setRigStateRawUid(rsr.getRigStateRawUid());
				ann.setDailyUid(session.getCurrentDailyUid());
			}
			
			Object threshold = node.getDynaAttr().get("threshold");
			ann.setThreshold(threshold == null ? "" : threshold.toString());
			
			Object rootCause = node.getDynaAttr().get("rootCause");
			ann.setRootCause(rootCause == null ? "" : rootCause.toString());
			
			Object description = node.getDynaAttr().get("description");
			ann.setDescription(description == null ? "" : description.toString());
			
			if (StringUtils.isBlank(ann.getThreshold()) && StringUtils.isBlank(ann.getRootCause()) && StringUtils.isBlank(ann.getDescription()))
				ann.setIsDeleted(true);
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ann);
		}
	}
}
