package com.idsdatanet.d2.drillnet.opsDatum;

import java.util.List;

import org.apache.commons.lang.BooleanUtils;

import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;


/**
 * All common utility related to OpsDatum.
 * @author Fu
 *
 */

public class OpsDatumUtil {
	
	/**
	 * Method used for custom setting on fields : datumCode , datumReferencePoint, reportingDatumOffset if isGlReference is selected (true) 
	 * @param commandBean
	 * @param object
	 * @throws Exception Standard Error Throwing Exception
	 * @return No Return
	 */
	public static void resetDatumFieldsIfIsGlReference(CommandBean commandBean, Object object) throws Exception{
		
		if(object instanceof OpsDatum) {	
			OpsDatum opsDatum = (OpsDatum) object;
			if (BooleanUtils.isTrue(opsDatum.getIsGlReference())){
				opsDatum.setDatumReferencePoint("GL");
				opsDatum.setDatumCode("");
				opsDatum.setReportingDatumOffset(0.00);				
			}
		}
	}
	
	public static boolean checkIfOperationAssociateToDatum(String opsDatumUid) throws Exception {
		
		String strSql = "SELECT o.operationUid FROM Operation o, OpsDatum d WHERE o.defaultDatumUid =:thisOpsDatumUid AND o.operationUid = d.operationUid AND (o.isDeleted = false or o.isDeleted is null) AND (d.isDeleted = false or d.isDeleted is null)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"thisOpsDatumUid"}, new Object[]{opsDatumUid});

		if (lstResult.size() > 0 && lstResult != null){
			return true;
		}
		return false;
	}
}
