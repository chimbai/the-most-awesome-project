package com.idsdatanet.d2.drillnet.cementJob;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CasingSummary;
import com.idsdatanet.d2.core.model.CementJob;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CementJobUtils {
	public static void getRelatedInfo(CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof CementJob)
		{			
			if (StringUtils.isBlank(userSelection.getWellUid())) return;
			
			//GET VALUES FROM WELL TABLE
			Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
			Map<String, Object> wellFieldValues = PropertyUtils.describe(well);
			for (Map.Entry entry : wellFieldValues.entrySet()) {
				setDynaAttr(node, "well_" + entry.getKey(), entry.getValue());
			}
					
			//GET VALUES FROM OPERATION TABLE
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, userSelection.getOperationUid());
			Map<String, Object> operationFieldValues = PropertyUtils.describe(operation);
			for (Map.Entry entry : operationFieldValues.entrySet()) {
				setDynaAttr(node, "operation_" + entry.getKey(), entry.getValue());
			}
			
			//GET RIGNAME
			Operation thisOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, userSelection.getOperationUid());
			String rigInformationUid = ApplicationUtils.getConfiguredInstance().getRigInformationUid(thisOperation.getOperationUid(), null);
									
			if (StringUtils.isNotBlank(rigInformationUid)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from RigInformation where (isDeleted = false or isDeleted is null) and rigInformationUid = :rigInformationUid", "rigInformationUid", rigInformationUid);
				if (list != null && list.size() > 0) {
					RigInformation riginformation = (RigInformation) list.get(0);
					if (riginformation != null) setDynaAttr(node, "rigInformationUid", riginformation.getRigName().toString());
				}
			}
						
			//GET CURRENT DATUM NAME
			setDynaAttr(node, "currentDatumName", node.getCommandBean().getInfo().getCurrentDatumDisplayName());
			
			//GET VALUES FROM REPORTDAILY TABLE
			String[] paramsFields1 = {"thisOperationUid", "thisDailyUid"};
	 		Object[] paramsValues1 = new Object[2];
			paramsValues1[0] = userSelection.getOperationUid();
			paramsValues1[1] = userSelection.getDailyUid();
			
			String strSql1 = "select reportDailyUid FROM ReportDaily where operationUid = :thisOperationUid and dailyUid = :thisDailyUid and reportType ='DDR' and (isDeleted = false or isDeleted is null)";
			List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1,paramsValues1);
			
			String ddrReportDailyUid = null;
			if (!lstResult1.isEmpty()) {
				Object b = (Object) lstResult1.get(0);
				if (b != null) ddrReportDailyUid = b.toString();
				
				ReportDaily ddrReportDaily = (ReportDaily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ReportDaily.class, ddrReportDailyUid);
				if (ddrReportDaily != null) { 
					Map<String, Object> reportDailyFieldValues = PropertyUtils.describe(ddrReportDaily);
					for (Map.Entry entry : reportDailyFieldValues.entrySet()) {
						setDynaAttr(node, "ddr_" + entry.getKey(), entry.getValue());
					}
				}
			}
			
			//GET VALUES FROM MUDPROPERTIES TABLE			
			// allow to select which mud check to show - currently only show the first one 
			String[] paramsFields2 = {"thisOperationUid", "thisDailyUid"};
	 		Object[] paramsValues2 = new Object[2];
			paramsValues2[0] = userSelection.getOperationUid();
			paramsValues2[1] = userSelection.getDailyUid();
			
			String strSql2 = "select mudPropertiesUid FROM MudProperties where operationUid = :thisOperationUid and dailyUid = :thisDailyUid and (isDeleted = false or isDeleted is null) order by reportNumber";
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2,paramsValues2);
			
			String mudPropertiesUid = null;
			if (!lstResult2.isEmpty()) {
				Object b = (Object) lstResult2.get(0);
				if (b != null) mudPropertiesUid = b.toString();
				
				MudProperties mudproperties = (MudProperties) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(MudProperties.class, mudPropertiesUid);
				
				if (mudproperties !=null){
					Map<String, Object> mudPropertiesFieldValues = PropertyUtils.describe(mudproperties);
					for (Map.Entry entry : mudPropertiesFieldValues.entrySet()) {
						setDynaAttr(node, "mudProperties_" + entry.getKey(), entry.getValue());
					}
				}
			}

		}
	}
	
	public static void getCasingSection(CommandBeanTreeNode node, String casingSectionUid, String fieldName) throws Exception {
		if (StringUtils.isNotBlank(casingSectionUid)){
			CasingSection casingsection = (CasingSection) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(CasingSection.class, casingSectionUid);
			
			if (casingsection != null) {
				Map<String, Object> csgFieldValues = PropertyUtils.describe(casingsection);
				
				for (Map.Entry entry : csgFieldValues.entrySet()) {
					if (entry.getValue() == null) {
						setDynaAttr(node, fieldName + "_" + entry.getKey(), "");
					}else {
						setDynaAttr(node, fieldName + "_" + entry.getKey(), entry.getValue());
					}
				}
			}
			
		}else {
			CasingSection casingsection = new CasingSection();
			Map<String, Object> csgFieldValues = PropertyUtils.describe(casingsection);
			for (Map.Entry entry : csgFieldValues.entrySet()) {
				setDynaAttr(node, fieldName + "_" + entry.getKey(), "");
			}
		}
	}
	
	
	public static void getRatHole(CommandBeanTreeNode node, String casingSectionUid, String fieldName) throws Exception {
		
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and casingSectionUid = :thisCasingSectionUid", "thisCasingSectionUid", casingSectionUid);
		if (list != null && list.size() > 0) {
			CasingSection thisCasingSection = (CasingSection) list.get(0);
			if (thisCasingSection != null)
			{
				Double thisRatHole = 0.00;
				Double thisOpenHoleDepthMdMsl = 0.00; 
				Double thisShoeTopMdMsl = 0.00;
				
				if(thisCasingSection.getOpenHoleDepthMdMsl() != null)
				{
					thisOpenHoleDepthMdMsl = thisCasingSection.getOpenHoleDepthMdMsl();
				}
				
				if(thisCasingSection.getShoeTopMdMsl() != null)
				{
					thisShoeTopMdMsl = thisCasingSection.getShoeTopMdMsl();
				}
				
				thisRatHole = thisOpenHoleDepthMdMsl - thisShoeTopMdMsl;
				setDynaAttr(node, fieldName + "_ratHole", thisRatHole);
			}
		}
	}
	
	private static void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		}
	}
	
	public static void setWaterSourceCementFromReportDaily(UserSelectionSnapshot userSelection, CementJob cementJob) throws Exception {
		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoLookupCementWaterSourceFromReportDaily"))) {
			if (cementJob!=null) {
				String dailyUid = cementJob.getDailyUid();
				if (dailyUid==null) dailyUid = userSelection.getDailyUid();
				if (dailyUid!=null) {
					String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(userSelection.getOperationUid());
					String queryString = "FROM ReportDaily WHERE (isDeleted=false or isDeleted is null) AND dailyUid=:dailyUid and reportType=:reportType";
					List<ReportDaily> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"dailyUid", "reportType"}, new Object[]{dailyUid, reportType});
					if (list.size()>0) {
						ReportDaily reportDaily = list.get(0);
						cementJob.setWaterSourceCementing(reportDaily.getWaterSourceCementing());
					}
				}
			}
		}
	}

	public static void populateCasingSizeBasedOnHoleSize(CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Double holeSize, String operationUid) throws Exception {
		if (holeSize > 0){
			String[] paramsFields1 = {"operationUid", "holeSize"};
	 		Object[] paramsValues1 = new Object[2];
			paramsValues1[0] = operationUid;
			paramsValues1[1] = holeSize;
			List<CasingSection> csgList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and openHoleId = :holeSize order by installStartDate", paramsFields1,paramsValues1);
			if(csgList != null && csgList.size() > 0){
				CasingSection casingSection = csgList.get(0);
				if (casingSection != null){
					String casingSectionUid = casingSection.getCasingSectionUid();
					// call getConcatCasingSize function in order to retrieve concatenated casing size from casing summary based on casingSectionUid
					String casingSizeLabel = getConcatCasingSize(casingSectionUid, userSelection); 
					if (!StringUtils.isBlank(casingSizeLabel) && casingSizeLabel != null){
						node.getDynaAttr().put("casingSizeBasedOnHoleSize", casingSizeLabel);
					}else{
						node.getDynaAttr().put("casingSizeBasedOnHoleSize", null);
					}
				}				
			}else{
				node.getDynaAttr().put("casingSizeBasedOnHoleSize", null);
			}
		}else{
			node.getDynaAttr().put("casingSizeBasedOnHoleSize", null);
		}
	}
	
	private static String getConcatCasingSize(String casingSectionUid, UserSelectionSnapshot userSelection) throws Exception {
		String casingSizeLabel = null;
		int count = 0;
		
		//based on casingSectionUid, filter all related casing summary records' casingOd to populate into the lookup item
		List<CasingSummary> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSummary where (isDeleted = false or isDeleted is null) and casingSectionUid = :casingSectionUid and (casingOd is not null and casingOd != '') ORDER BY casingOd DESC", "casingSectionUid", casingSectionUid);
		if((list != null && list.size() > 0)) {
			for(CasingSummary casingSummary : list) {
				CustomFieldUom thisConverter = new CustomFieldUom((Locale) null, CasingSummary.class, "casingOd");
				String strCasingOd = thisConverter.formatOutputPrecision(casingSummary.getCasingOd());
				
				if(StringUtils.isNotBlank(strCasingOd)) {
					//call function to retrieve the lookup item label value based on casingOd
					String lookupItem = getCsgSizeLookupLabel(strCasingOd, userSelection);
					if(lookupItem != null)
					{
						if (count == 0){
							casingSizeLabel = lookupItem;				
						}else{
							casingSizeLabel = casingSizeLabel + " x " + lookupItem;
						}
						count ++;
					}
				}
			}
		}
		return casingSizeLabel;
	}
	
	private static String getCsgSizeLookupLabel(String casingSize, UserSelectionSnapshot userSelection) throws Exception {
		String casingSizeLabel = null;
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://cement_job.holeSize?key=code&value=label", userSelection, null);
		if (lookupMap != null) {
			LookupItem lookupItem = lookupMap.get(casingSize);
			if (lookupItem != null) {
				return (String) lookupItem.getValue();
			}
		}		
		return casingSizeLabel;
	}
	
	public static String getCasingSectionUidBasedOnHoleSize(Double holeSize, String operationUid) throws Exception {
		String casingSectionUid = null;
		if (holeSize > 0){		
			String[] paramsFields1 = {"operationUid", "holeSize"};
	 		Object[] paramsValues1 = new Object[2];
			paramsValues1[0] = operationUid;
			paramsValues1[1] = holeSize;
			List<CasingSection> csgList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and openHoleId = :holeSize order by installStartDate", paramsFields1,paramsValues1);
			if(csgList != null && csgList.size() > 0){
				CasingSection casingSection = csgList.get(0);
				if (casingSection != null){
					casingSectionUid = casingSection.getCasingSectionUid();					
				}				
			}			
		}
		return casingSectionUid;
	}

	public static void populatePrevCasingSizeBasedOnHoleSize(CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Double holeSize, String operationUid) throws Exception {
		if (holeSize > 0){
			String[] paramsFields1 = {"operationUid", "holeSize"};
	 		Object[] paramsValues1 = new Object[2];
			paramsValues1[0] = operationUid;
			paramsValues1[1] = holeSize;
			List<CasingSection> csgList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and openHoleId = :holeSize order by installStartDate", paramsFields1,paramsValues1);
			if(csgList != null && csgList.size() > 0){
				CasingSection casingSection = csgList.get(0);
				if (casingSection != null){
					Date currInstallStartDate = casingSection.getInstallStartDate();
					if (currInstallStartDate != null){
						String[] paramsFields2 = {"operationUid", "currInstallStartDate"};
				 		Object[] paramsValues2 = new Object[2];
						paramsValues2[0] = operationUid;
						paramsValues2[1] = currInstallStartDate;
						List<CasingSection> csgList2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and installStartDate < :currInstallStartDate order by installStartDate DESC", paramsFields2,paramsValues2);
						if(csgList2 != null && csgList2.size() > 0){
							CasingSection prevCasingSection = csgList2.get(0);
							if (prevCasingSection != null){
								String casingSectionUid = prevCasingSection.getCasingSectionUid();
								// call getConcatCasingSize function in order to retrieve concatenated casing size from casing summary based on casingSectionUid
								String casingSizeLabel = getConcatCasingSize(casingSectionUid, userSelection); 
								if (!StringUtils.isBlank(casingSizeLabel) && casingSizeLabel != null){
									node.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", casingSizeLabel);
								}else{
									node.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", null);									
								}
							}
						}else{
							String casingSectionUid = casingSection.getCasingSectionUid();
							// call getConcatCasingSize function in order to retrieve concatenated casing size from casing summary based on casingSectionUid
							String casingSizeLabel = getConcatCasingSize(casingSectionUid, userSelection); 
							if (!StringUtils.isBlank(casingSizeLabel) && casingSizeLabel != null){
								node.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", casingSizeLabel);
							}else{
								node.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", null);
							}
						}
					}else{
						node.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", null);
					}
				}				
			}else{
				node.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", null);
			}
		}
	}
	
	public static String getPrevCasingSectionUidBasedOnCurrCasing(String casingSectionUid, String operationUid) throws Exception {
		String prevCasingSectionUid = null;
		if (StringUtils.isNotBlank(casingSectionUid) && casingSectionUid != null) {		
			List<CasingSection> csgList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and casingSectionUid = :casingSectionUid order by installStartDate", "casingSectionUid",casingSectionUid);
			if(csgList != null && csgList.size() > 0){
				CasingSection casingSection = csgList.get(0);
				if (casingSection != null){
					Date currInstallStartDate = casingSection.getInstallStartDate();					
					if (currInstallStartDate != null){
						//based on current installStartDate, filter all records that is < current installStartDate, sort listing by installStartDate DESC, get the first record's casingSectionUid (which indicated previous casing record)
						String[] paramsFields2 = {"operationUid", "currInstallStartDate"};
				 		Object[] paramsValues2 = new Object[2];
						paramsValues2[0] = operationUid;
						paramsValues2[1] = currInstallStartDate;
						List<CasingSection> csgList2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and installStartDate < :currInstallStartDate order by installStartDate DESC", paramsFields2,paramsValues2);
						if(csgList2 != null && csgList2.size() > 0){
							CasingSection prevCasingSection = csgList2.get(0);
							if (prevCasingSection != null){
								prevCasingSectionUid = prevCasingSection.getCasingSectionUid();
							}
						}else{
							prevCasingSectionUid = casingSectionUid;
						}	
					}
				}				
			}			
		}
		return prevCasingSectionUid;
	}
	
	public static void getOperationUidBasedOnCasing(CommandBeanTreeNode node, String casingSectionUid, String fieldName) throws Exception {
		
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and casingSectionUid = :thisCasingSectionUid", "thisCasingSectionUid", casingSectionUid);
		if (list != null && list.size() > 0) {
			CasingSection thisCasingSection = (CasingSection) list.get(0);
			if (thisCasingSection != null)
			{
				String thisOperation = null;
				
				if(thisCasingSection.getOperationUid() != null)
				{
					thisOperation = thisCasingSection.getOperationUid();
				}
				
				setDynaAttr(node, fieldName + "_operationUid", thisOperation);
			}
		}
	}
	
	// OMV P1 FTR
	public static void createCementJobBasedOnActivityRemarks(UserSelectionSnapshot userSelection, Activity thisActivity, Map<String,Object>fieldsMap) throws Exception{
		if(fieldsMap!=null){
			String[] paramsFields = {"operationUid", "dailyUid", "activityUid"};
			Object[] paramsValues = {thisActivity.getOperationUid(), thisActivity.getDailyUid(), thisActivity.getActivityUid()};
			String strSql = "FROM CementJob WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND dailyUid=:dailyUid AND activityUid=:activityUid";
			List<CementJob> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			CementJob cementjob = null;
			
			if(ls.size()==0){
				paramsFields = new String[] {"operationUid", "dailyUid"};
				paramsValues = new Object[] {thisActivity.getOperationUid(), thisActivity.getDailyUid()};
				String strSql2 = "FROM CementJob WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND dailyUid=:dailyUid";
				ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);
				
				Integer cementJobNumber = ls.size()+1;
				
				cementjob = new CementJob();
				cementjob.setOperationUid(thisActivity.getOperationUid());
				cementjob.setDailyUid(thisActivity.getDailyUid());
				cementjob.setActivityUid(thisActivity.getActivityUid());
				cementjob.setJobNumber(cementJobNumber.toString());
				cementjob.setTimePumpcmttail(thisActivity.getStartDatetime());
			}else{
				cementjob = ls.get(0);			
			}
			
			if (cementjob!=null ){
				for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
					if (StringUtils.isNotBlank(fieldEntry.getKey())) PropertyUtils.setProperty(cementjob, fieldEntry.getKey(), fieldEntry.getValue());
				}
				cementjob.setTimePumpcmttail(thisActivity.getStartDatetime());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(cementjob);
			}
		}
	}
}
