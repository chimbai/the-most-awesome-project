package com.idsdatanet.d2.drillnet.hserelatedoperationsetup;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class HseRelatedOperationSetupFlashComponentConfiguration extends AbstractFlashComponentConfiguration {
	
	public String getFlashvars(UserSession userSession, HttpServletRequest request, String sessionId, String targetSubModule) throws Exception {
		return "d2Url=../../";
	}
	
	public String getFlashComponentId(String targetSubModule) {
		return "hserelatedoperationsetup";
	}
}
