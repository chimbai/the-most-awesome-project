package com.idsdatanet.d2.drillnet.wellAcceptance;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.WellAcceptance;
import com.idsdatanet.d2.core.model.WellAcceptanceCategory;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellAcceptanceDataNodeListener extends EmptyDataNodeListener {
	private String criteria = "Well Acceptance Criteria";
	private boolean fullDescription = true;
	private String dateFormat = "ddMMMyy";
	
	public void setCriteria(String value) {
		this.criteria = value;
	}
	
	public void setFullDescription(boolean value) {
		this.fullDescription = value;
	}
	
	public void setDateFormatter(String value) {
		this.dateFormat = value;
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}

	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof WellAcceptance) {
			node.getDynaAttr().put("categories", "1");
		}
	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof WellAcceptance) {
			WellAcceptance wellAcceptance= (WellAcceptance) object;
			
			if (wellAcceptance.getWellUid()!=null){
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellAcceptance.getWellUid());
				
				if (well!=null){
					wellAcceptance.setTotalDepthMdMsl(getLastWellbore(well.getWellUid()));
					wellAcceptance.setWellUid(well.getWellUid());
					wellAcceptance.setWellName(well.getWellName());
					
					Operation op = getLastOperation(well.getWellUid());
					if (op!=null){
						wellAcceptance.setEndDateTime(op.getRigOffHireDate());
						wellAcceptance.setSpudDateTime(op.getSpudDate());
						wellAcceptance.setOperationUid(op.getOperationUid());
						wellAcceptance.setWellboreUid(op.getWellboreUid());		
					}
				}
			}	
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		
		if (object instanceof WellAcceptance) {
			
			WellAcceptance wellAcceptance = (WellAcceptance) object;
			Map<String, CommandBeanTreeNode> categoryNodes = node.getChild(WellAcceptanceCategory.class.getSimpleName());
			
			if(categoryNodes == null || categoryNodes.isEmpty()) {
				String sameRef = "";
				String sameCriteria = "";
				
				String strSql = "FROM CommonLookup WHERE (isDeleted is null or isDeleted = false) AND lookupTypeSelection=:lookupTypeSelection ORDER BY sequence";
				String[] paramsFields = {"lookupTypeSelection"};
				Object[] paramsValues = {this.criteria};
				
				List<CommonLookup> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (lstResult.size() > 0){
					for (CommonLookup list : lstResult){
						WellAcceptanceCategory records = new WellAcceptanceCategory();
						String label = list.getLookupLabel();
						
						if (label.contains("::")){
							String[] rp = label.split("::");
							
							if (rp[0]!=null){
								label = rp[0];
							}
							if (rp[1]!=null){
								records.setResponsibleParty(rp[1]);
							}						
						}
						if (label.contains("--")){
							String[] sub = label.split("--");
							
							if (sub[0]!=null && !sameCriteria.equals(sub[0])){
								sameCriteria = sub[0];
								if (fullDescription){
									records.setCategory(list.getDescription());
								}else{
									records.setCategory(sub[0]);
								}
							}
							if (sub[1]!=null){
								records.setSize(sub[1]);
							}
						}else{
							if (fullDescription){
								records.setCategory(list.getDescription());
							}else{
								records.setCategory(label);
							}
						}
						
						String ref = list.getShortCode();
						if (ref.contains("-")){
							String[] no = ref.split("-");
							
							if (no[0]!=null && !sameRef.equals(no[0])){
								sameRef = no[0];
								records.setRef(Integer.valueOf(no[0]));
							}
						}else{
							records.setRef(Integer.valueOf(ref));
						}
						
						String order = list.getShortCode().replace("-", ".");		
						records.setOrder(Double.parseDouble(order));
						
						records.setStatus("no");
						records.setWellAcceptanceUid(wellAcceptance.getWellAcceptanceUid());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(records); 
					}
				}
			}
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof WellAcceptance) {
			WellAcceptance wellAcceptance= (WellAcceptance) object;
			
			SimpleDateFormat df = new SimpleDateFormat(dateFormat);	
			node.getDynaAttr().put("categories", "1");
			
			if (wellAcceptance.getEndDateTime()!=null){
				node.getDynaAttr().put("spudDate", df.format(wellAcceptance.getEndDateTime()));
			}
			
			if (wellAcceptance.getSpudDateTime()!=null){
				node.getDynaAttr().put("endDate", df.format(wellAcceptance.getSpudDateTime()));
			}
			
			if (wellAcceptance.getWellAcceptanceUid()!=null){
				node.getDynaAttr().put("score", getScore(wellAcceptance.getWellAcceptanceUid()));
				node.getDynaAttr().put("categories", "0");
			}
		}
		
		if (object instanceof WellAcceptanceCategory) {
			WellAcceptanceCategory category= (WellAcceptanceCategory) object;
			if (category.getRef()!=null){
				node.getDynaAttr().put("ref", "1");
				node.getDynaAttr().put("refNo", category.getRef());
			}else{
				node.getDynaAttr().put("ref", "0"); 
			}
				
		}
	}
	
	private boolean getWellAcceptanceByName(String wellName) throws Exception{
		
		String strSql = "FROM WellAcceptance WHERE (isDeleted is null or isDeleted = false) AND wellName = :wellName";
		String[] paramsFields = {"wellName"};
		Object[] paramsValues = {wellName};
		
		List<WellAcceptance> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			return true;
		}
		
		return false;
	}
	
	private Integer getTotalWellAcceptance(String wellAcceptanceUid) throws Exception{
		
		String strSql = "FROM WellAcceptanceCategory WHERE (isDeleted is null or isDeleted = false) AND wellAcceptanceUid = :wellAcceptanceUid AND ref IS NOT NULL";
		String[] paramsFields = {"wellAcceptanceUid"};
		Object[] paramsValues = {wellAcceptanceUid};
		
		List<WellAcceptanceCategory> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			return lstResult.size();
		}
		
		return null;
	}

	private String getScore(String wellAcceptanceUid) throws Exception{
		int totalNa = 0;
		int totalYes = 0;
		int totalNo = 0;
		int totalCriteria =0;
		
		if (getTotalWellAcceptance(wellAcceptanceUid)!=null){
			totalCriteria = getTotalWellAcceptance(wellAcceptanceUid);
		
			for (int i=1; i<=totalCriteria; i++){
				int status = getWellAcceptanceCategory(wellAcceptanceUid, i);
				
				if (status<0){
					totalNa++;
				}else if(status==1){
					totalYes++;
				}else{
					totalNo++;
				}
			}
		}
		
		String scoreLabel = "Yes: " + totalYes + ", No: " + totalNo;
		double score = 0.00;
		
		if (totalCriteria-totalNa>0){
			score = (Double.valueOf(totalYes)/Double.valueOf((totalCriteria-totalNa))) * 100.00;
			//int value = (int)score;
		}
		scoreLabel = String.format("%.1f", score) + "%, " + scoreLabel ;
		
		return scoreLabel;
	}

	private int getWellAcceptanceCategory(String wellAcceptanceUid, int ref) throws Exception{
		
		String strSql = "FROM WellAcceptanceCategory WHERE (isDeleted is null or isDeleted = false) AND wellAcceptanceUid = :wellAcceptanceUid AND order >= :minRef AND order < :maxRef";
		String[] paramsFields = {"wellAcceptanceUid", "minRef", "maxRef"};
		Object[] paramsValues = {wellAcceptanceUid, Double.valueOf(ref), Double.valueOf(ref+1)};

		List<WellAcceptanceCategory> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		int naCount = 0;
		
		if (lstResult.size() > 0){
			for (WellAcceptanceCategory category : lstResult){
				if ("no".equals(category.getStatus())){
					return 0;
				}
				if ("na".equals(category.getStatus())){
					naCount++;
				}
			}
			if (lstResult.size()==naCount){
				return -1;
			}
		}
		
		return 1;
	}

	private Double getLastWellbore(String wellUid) throws Exception{
		
		Double finalTdMdMsl = null;
		
		String strSql = "FROM Wellbore WHERE (isDeleted is null or isDeleted = false) AND wellUid = :wellUid ORDER BY finalTdMdMsl DESC";
		String[] paramsFields = {"wellUid"};
		Object[] paramsValues = {wellUid};
		
		List<Wellbore> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			Wellbore wellbore = (Wellbore) lstResult.get(0);

			if (wellbore.getFinalTdMdMsl() != null) {
				return wellbore.getFinalTdMdMsl();
			}
		}
		
		return finalTdMdMsl;
	}
	
	private Operation getLastOperation(String wellUid) throws Exception{
		
		String strSql = "FROM Operation WHERE (isDeleted is null or isDeleted = false) AND wellUid = :wellUid ORDER BY rigOffHireDate DESC";
		String[] paramsFields = {"wellUid"};
		Object[] paramsValues = {wellUid};
		
		List<Operation> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			Operation op = (Operation) lstResult.get(0);
			
			return op;
		}
		
		return null;
	}
}