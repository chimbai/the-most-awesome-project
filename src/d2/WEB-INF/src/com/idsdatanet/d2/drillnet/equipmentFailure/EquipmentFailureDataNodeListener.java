package com.idsdatanet.d2.drillnet.equipmentFailure;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityEquipmentFailureLink;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.EquipmentFailure;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.activity.ActivityUtils;

public class EquipmentFailureDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	private String toolUid = null;
	private String toolType = null;
	private String selectedToolUid = null;
	private String selectedToolType = null;
	private static final String DYN_ATTR_EQUIPMENT_DATE = "equipmentDate";
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();	
		if (object instanceof EquipmentFailure){
			EquipmentFailure thisEquipFail = (EquipmentFailure) object;
			String strSql = "SELECT activityUid FROM ActivityEquipmentFailureLink where equipmentFailureUid = :equipmentFailureUid and (isDeleted = false or isDeleted is null)";
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "equipmentFailureUid", thisEquipFail.getEquipmentFailureUid());			
			if (list != null && list.size() > 0) {
				Object b = (Object) list.get(0);
				if (b.toString() !=null) {
					node.getDynaAttr().put("activityUid", b.toString());
					Activity activity = (Activity) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Activity.class, b.toString());
					if (activity!=null) {
						node.getDynaAttr().put("Activity.equipmentCode", activity.getEquipmentCode());
						node.getDynaAttr().put("Activity.subEquipmentCode", activity.getSubEquipmentCode());
					}
				}
			}
			
			String toolType = thisEquipFail.getToolType();
			if ("lwd_tool".equalsIgnoreCase(toolType))
			{
				node.getDynaAttr().put("lwd_tool",thisEquipFail.getFailedItem());
				node.getDynaAttr().put("lwd_com",thisEquipFail.getCompany());
			}
			if ("wireline_tool".equalsIgnoreCase(toolType))
			{
				node.getDynaAttr().put("wireline_tool",thisEquipFail.getFailedItem());
				node.getDynaAttr().put("wireline_com",thisEquipFail.getCompany());
			}
			
		}
	}

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		 /*this._afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);*/
		
		Object obj = node.getData();
		
		if (request !=null){
			
			if (request.getParameter("toolId")!=null)
				toolUid = request.getParameter("toolId");
			if (request.getParameter("toolType")!=null)
				toolType = request.getParameter("toolType");
			
			
		}
		
		commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
		if (obj instanceof EquipmentFailure) {
			EquipmentFailure thisEquipmentFailure = (EquipmentFailure) obj;
			
			String strSql = "From EquipmentFailure " +
			 "WHERE (isDeleted = false or isDeleted is null) and toolUid=:toolId and toolType=:toolType"; 
			String[] paramsFields = {"toolId", "toolType"};
			Object[] paramsValues = {toolUid, toolType};
			List<Object[]> lstEquipmentFailure = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields,paramsValues);
			
			if (lstEquipmentFailure != null && lstEquipmentFailure.size() > 0) {
				this._afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
			}else{
				if(meta.getTableClass() == EquipmentFailure.class && StringUtils.isNotBlank(toolUid)){
					thisEquipmentFailure.setToolUid(toolUid);
					thisEquipmentFailure.setToolType(toolType);
					if (request.getParameter("toolId")!=null) {
						toolUid = request.getParameter("toolId");
						CommandBeanTreeNode newNode = commandBean.getRoot().addCustomNewChildNodeForInput(thisEquipmentFailure);
						EquipmentFailureUtils.getLwdTool(newNode, toolUid);
						}else{
							this._afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
						}
				}
			}
		}
	}
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		 this._afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	public void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (request !=null){
			if (request.getParameter("toolId")!=null)
				toolUid = request.getParameter("toolId");
			if (request.getParameter("toolType")!=null)
				toolType = request.getParameter("toolType");
			if (obj instanceof EquipmentFailure && StringUtils.isNotBlank(toolUid) && StringUtils.isNotBlank(toolType)) {
				
				if ("lwd_tool".equalsIgnoreCase(toolType))
					EquipmentFailureUtils.getLwdTool(node, toolUid);
				if ("wireline_tool".equalsIgnoreCase(toolType))
					EquipmentFailureUtils.getWirelineTool(node, toolUid);
			}
		}
	}
	
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();	
		if (object instanceof EquipmentFailure){
			EquipmentFailure thisEquipFail = (EquipmentFailure) object;
			String selectedActivity = (String) node.getDynaAttr().get("activityUid");
			
			if (BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW == operationPerformed) {
				saveActivityUid(thisEquipFail.getEquipmentFailureUid(), selectedActivity);
				
			}else if (BaseCommandBean.OPERATION_PERFORMED_SAVE_EXISTING == operationPerformed) {
				
				String strSql = "SELECT equipmentFailureUid FROM ActivityEquipmentFailureLink where equipmentFailureUid = :equipmentFailureUid and (isDeleted = false or isDeleted is null)";
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "equipmentFailureUid", thisEquipFail.getEquipmentFailureUid());			
				if (list != null && list.size() > 0) {
					//Update exisiting link record if the equipment failure uid is existed
					String[] paramsFields = {"activityUid", "thisEquipFailUid"};
					Object[] paramsValues = {selectedActivity, thisEquipFail.getEquipmentFailureUid()};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE ActivityEquipmentFailureLink SET activityUid =:activityUid WHERE equipmentFailureUid = :thisEquipFailUid", paramsFields, paramsValues);
				}else {
					//create new link record the uid not found
					saveActivityUid(thisEquipFail.getEquipmentFailureUid(), selectedActivity);
				}
				
				String sql = "SELECT company FROM EquipmentFailure where equipmentFailureUid = :equipmentFailureUid and (isDeleted = false or isDeleted is null)";
				List res = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "equipmentFailureUid", thisEquipFail.getEquipmentFailureUid());			
				if (res != null && res.size() > 0 && res.get(0) != null) {
					String company = res.get(0).toString();
					String[] paramsFields = {"company", "thisEquipFailUid"}; 
					Object[] paramsValues = {company, thisEquipFail.getEquipmentFailureUid()};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE Activity SET lookupCompanyUid =:company WHERE downtimeEventNumber =:thisEquipFailUid", paramsFields, paramsValues);
				}
			}
		}
	}
	
	private void saveActivityUid(String equipmentFailureUid, String selectedActivity) throws Exception {
		if (StringUtils.isNotBlank(equipmentFailureUid) && StringUtils.isNotBlank(selectedActivity)) {
			ActivityEquipmentFailureLink thisLink =  new ActivityEquipmentFailureLink();
			thisLink.setEquipmentFailureUid(equipmentFailureUid);
			thisLink.setActivityUid(selectedActivity);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisLink);
		}
		
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object object = node.getData();	
		if (object instanceof EquipmentFailure){
			EquipmentFailure thisEquipFail = (EquipmentFailure) object;
			String thisEquipFailUid = thisEquipFail.getEquipmentFailureUid();
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE ActivityEquipmentFailureLink SET isDeleted = true WHERE equipmentFailureUid = :thisEquipFailUid", new String[] { "thisEquipFailUid" }, new Object[] { thisEquipFailUid });
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		Calendar equipmentDate = Calendar.getInstance();
		
		if (object instanceof EquipmentFailure)
		{
			EquipmentFailure failure = (EquipmentFailure) object;
			if (request !=null)
			{
				String toolUid = request.getParameter("toolId");
				if (StringUtils.isNotBlank(toolUid))
				{
					failure.setToolUid(toolUid);
				}
				String toolType = request.getParameter("toolType");
				if (StringUtils.isNotBlank(toolType))
				{
					failure.setToolType(toolType);
				}
			}
			
			calculateCostIncurred(commandBean, node, session, request);
			
			Date thisEquipmentDate = null;
			if(node.getDynaAttr().get("equipmentDate") != null)
			{
				if(CommonDateParser.parse(node.getDynaAttr().get(DYN_ATTR_EQUIPMENT_DATE).toString()) != null)
				{
					thisEquipmentDate = CommonDateParser.parse(node.getDynaAttr().get(DYN_ATTR_EQUIPMENT_DATE).toString());
				}
				else
				{
					thisEquipmentDate = (Date) node.getDynaAttr().get(DYN_ATTR_EQUIPMENT_DATE);
				}
			}else{
				
				if (session.getCurrentDaily()!=null)
					thisEquipmentDate = session.getCurrentDaily().getDayDate();
					
			}
			
			if(thisEquipmentDate != null && (commandBean.getOperatingMode() != BaseCommandBean.OPERATING_MODE_DEPOT))
			{	
				equipmentDate.setTime(thisEquipmentDate);
				
				if(PropertyUtils.getProperty(object, "dateFailStart") != null)
				{
					Calendar dateFailStart = Calendar.getInstance();
					dateFailStart.setTime((Date) PropertyUtils.getProperty(object, "dateFailStart"));
					dateFailStart.set(Calendar.YEAR, equipmentDate.get(Calendar.YEAR));
					dateFailStart.set(Calendar.MONTH, equipmentDate.get(Calendar.MONTH));
					dateFailStart.set(Calendar.DAY_OF_MONTH, equipmentDate.get(Calendar.DAY_OF_MONTH));
					PropertyUtils.setProperty(object, "dateFailStart", dateFailStart.getTime());
				}
													
				Calendar dateFailEnd = Calendar.getInstance();
				dateFailEnd.setTime((Date) PropertyUtils.getProperty(object, "dateFailEnd"));
				dateFailEnd.set(Calendar.YEAR, equipmentDate.get(Calendar.YEAR));
				dateFailEnd.set(Calendar.MONTH, equipmentDate.get(Calendar.MONTH));
				dateFailEnd.set(Calendar.DAY_OF_MONTH, equipmentDate.get(Calendar.DAY_OF_MONTH));
				PropertyUtils.setProperty(object, "dateFailEnd", dateFailEnd.getTime());
				
				D2ApplicationEvent.refresh();
			}
			
			List<Date> dateFailEndList = new ArrayList<Date>();
			
			// get parent node and loop through child to get each end datetime
			CommandBeanTreeNode parentNode = node.getParent();
			for (Iterator i = parentNode.getList().values().iterator(); i.hasNext();) {
				Map items = (Map) i.next();
				
				for (Iterator j = items.values().iterator(); j.hasNext();) {
					CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
					if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(object.getClass())) {
						Boolean isDeleted = (Boolean) PropertyUtils.getProperty(childNode.getData(), "isDeleted");
						if (isDeleted == null || !isDeleted.booleanValue()) {
							dateFailEndList.add((Date) PropertyUtils.getProperty(childNode.getData(), "dateFailEnd"));
						}
					}
				}
			}
			Collections.sort(dateFailEndList, new ActivityUtils.ReversedDateComparator());
			commandBean.getRoot().getDynaAttr().put("dateFailEnd", dateFailEndList);
			
			Date start = (Date) PropertyUtils.getProperty(object, "dateFailStart");
			Date end = (Date) PropertyUtils.getProperty(object, "dateFailEnd");
		}
	}

	private void calculateCostIncurred(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request) throws Exception {
		
		Object objFailure = node.getData();
		
		if (objFailure instanceof EquipmentFailure)
		{
			EquipmentFailure failure = (EquipmentFailure) objFailure;
			Date failDate =failure.getDateFailStart();
			Double dailyCost = null;
			if (session != null) dailyCost = CommonUtil.getConfiguredInstance().getDailyDayCost(failDate, session);
			
			if (dailyCost!=null && failure.getFailDuration()!=null)
			{
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
				thisConverter.setReferenceMappingField(EquipmentFailure.class, "failDuration");
				thisConverter.setBaseValueFromUserValue(failure.getFailDuration());
				double duration = thisConverter.getBasevalue() / 3600;
				Double estimatedFailureCost = dailyCost / 24 * duration;
				failure.setEstimatedFailureCost(estimatedFailureCost);
			}
		}

	}
	
	private void populateDynAttrEquipmentDate(CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Boolean getDailyFromNode) throws Exception
	{
		
		
		String dailyUid = userSelection.getDailyUid();
		Date equipmentDate = null;
		if (getDailyFromNode && PropertyUtils.getProperty(node.getData(), "dailyUid")!=null)
		{
			dailyUid = (String) PropertyUtils.getProperty(node.getData(), "dailyUid");
		}
		if (StringUtils.isNotBlank(dailyUid))
		{
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
			if (daily!=null && daily.getDayDate()!=null)
				equipmentDate = daily.getDayDate();
		}
		node.getDynaAttr().put(DYN_ATTR_EQUIPMENT_DATE, equipmentDate);
	}

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {

		if (request !=null)
		{
			if (EquipmentFailure.class.equals(meta.getTableClass())) 
			{
				if (request.getParameter("toolId")!=null)
					selectedToolUid = request.getParameter("toolId");
				else
					selectedToolUid = toolUid;
				if (request.getParameter("toolType")!=null)
					selectedToolType = request.getParameter("toolType");
				else
					selectedToolType = toolType;
				if (StringUtils.isNotBlank(selectedToolUid) && StringUtils.isNotBlank(selectedToolType))
				{
					String filter = "toolType =:thisToolType and toolUid =:thisToolUid";
					if (conditionClause.trim().length()>0)
						filter = " and " + filter;
					query.addParam("thisToolType", selectedToolType);
					query.addParam("thisToolUid", selectedToolUid);
					return conditionClause.replace("{_tool_filter_}", filter);
				}
				else
				{
					return conditionClause.replace("{_tool_filter_}", "");
				}
			}
		}
		
		return null;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
