package com.idsdatanet.d2.drillnet.wellHistoryOverview;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class wellHistoryOverviewLookupHandler implements LookupHandler {
	private URI xmlLookup = null;
	
	public void setXmlLookup(URI xmlLookup) {
		this.xmlLookup = xmlLookup;
	}
	
	public URI getXmlLookup() {
		return this.xmlLookup;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		Map<String, LookupItem> LookupList = LookupManager.getConfiguredInstance().getLookup(this.xmlLookup.toString() , userSelection, null);
		
		result.put("showall", new LookupItem("showall", "[All]"));
		for (Map.Entry<String, LookupItem> entry : LookupList.entrySet())
		{
			String key = entry.getKey();
			LookupItem LookupObj = (LookupItem) entry.getValue();
			result.put(key, new LookupItem(LookupObj.getKey(), LookupObj.getValue()));
		} 
		
		return result;
	}
}