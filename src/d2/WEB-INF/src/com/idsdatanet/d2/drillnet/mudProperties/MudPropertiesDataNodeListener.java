package com.idsdatanet.d2.drillnet.mudProperties;

import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.MudRheology;
import com.idsdatanet.d2.core.model.MudVolumes;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.SurveyReference;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.bharun.BharunUtils;


public class MudPropertiesDataNodeListener extends EmptyDataNodeListener {
	private String altMudWeightUnitId;
	private Integer altMudWeightPrecision;
	private Boolean defaultCompletionFluidData = false;

	public String getAltMudWeightUnitId() {
		return altMudWeightUnitId;
	}

	public void setAltMudWeightUnitId(String altMudWeightUnitId) {
		this.altMudWeightUnitId = altMudWeightUnitId;
	}

	public Integer getAltMudWeightPrecision() {
		return altMudWeightPrecision;
	}

	public void setAltMudWeightPrecision(Integer altMudWeightPrecision) {
		this.altMudWeightPrecision = altMudWeightPrecision;
	}
	
	public void setDefaultCompletionFluidData(Boolean value) {
		this.defaultCompletionFluidData = value;
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof MudProperties) {
			//variable declaration
			MudProperties mudproperties = (MudProperties) object;
			//check if it is true then
			if(this.defaultCompletionFluidData){			
				String type = userSelection.getOperationType();
				//set mud type by default to completion fluid data while operation type is completion upon add new mud properties
				if(type.equalsIgnoreCase("CMPLT"))
					mudproperties.setMudType("cmpt_fluid");
					node.getDynaAttr().put("mudOperationType",type);
			}
		}
	}

	public void afterDataNodeProcessed(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, String action, int operationPerformed, boolean errorOccurred) throws Exception{
		Object obj = node.getData();
		
		//set HSI & %HHP at bit, Impact Force calculated value in bharun_daily_summary when save mud properties
		if (obj instanceof MudProperties) {
			MudProperties mudProperties = (MudProperties) obj;
			
			//get bharun daily summary for this day to do calc
			String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND dailyUid=:DailyUid";		
			String[] paramsFields = {"DailyUid"};
			String[] paramsValues = {mudProperties.getDailyUid()};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			if (lstResult.size() > 0){
				for(Object objBhaDaily: lstResult){
					BharunDailySummary thisBharunDailySummary = (BharunDailySummary) objBhaDaily;
					String bharunUid = thisBharunDailySummary.getBharunUid();
					String dailyUid = thisBharunDailySummary.getDailyUid();
					// call method to populate HSI if GWP turned on
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "calchsi"))) {
						BharunUtils.saveHSI(bharunUid, dailyUid, commandBean.getSystemMessage());
					}
					
					//call method to populate %HHP if GWP turned on
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PERCENT_HHPB))){
						CustomFieldUom thisConverter=new CustomFieldUom(commandBean,DrillingParameters.class, "flowAvg");
						BharunUtils.savePercentHHPb(thisConverter, bharunUid, dailyUid, commandBean.getSystemMessage());
					}
					
					//call method to populate Impact Force if GWP turned on
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_IMPACT_FORCE))){
						CustomFieldUom thisConverter=new CustomFieldUom(commandBean,DrillingParameters.class, "flowAvg");
						BharunUtils.saveImpactForce(thisConverter, bharunUid, dailyUid, commandBean.getSystemMessage());
					}
					
					// call method to populate Jet Nozzle Pressure Loss if GQP turned on
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_JET_NOZZLE_PRESSURE_LOSS))) {
						CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
						BharunUtils.saveJetNozzlePressureLoss(thisConverter, bharunUid, dailyUid, commandBean.getSystemMessage(), session.getCurrentGroupUid(), session.getCurrentWellboreUid(), session.getCurrentOperationUid());
					}
					
					// call method to populate System Hydraulic Horse Power if GQP turned on
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_SYS_HYDRAULIC_HORSE_POWER))) {
						CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
						BharunUtils.saveSysHydraulicHorsepower(thisConverter, bharunUid, dailyUid, commandBean.getSystemMessage(), session.getCurrentWellboreUid());
					}
					
					// call method to populate Bit Pressure Loss Percentage if GWP turned on
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_BIT_PRESSURE_LOSS_PERCENTAGE))) {
						CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
						BharunUtils.saveBitPressureLossPercentage(thisConverter, bharunUid, dailyUid, commandBean.getSystemMessage(), session.getCurrentWellboreUid());
					}
					
					// call method to populate Bottom Hole Force (Impact Force) if GWP turned on
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_BIT_IMPACT_FORCE))) {
						CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
						BharunUtils.saveBitImpactForce(thisConverter, bharunUid, dailyUid, commandBean.getSystemMessage(), session.getCurrentGroupUid(), session.getCurrentWellboreUid());
					}
				}
			}
			
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof MudProperties) {
			
			//variable declaration
			MudProperties mudProperties = (MudProperties) obj;
			this.loadMudVolumesRecordList(node, mudProperties, userSelection);
			String operationUid = mudProperties.getOperationUid();
			String mudType = mudProperties.getMudType();
			String dailyid = mudProperties.getDailyUid();
			Date reportDate=mudProperties.getReportTime();
			String checkNo=mudProperties.getReportNumber();
			Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyid);
			//check if required query parameters is not null
			if (selectedDaily.getDayDate()!= null && operationUid != null && mudType != null) 
			{
				Double cumMetalCuttings = 0.00;
				Double cumMetalCuttingsMass = 0.00;
				
				String[] paramsFields = {"userDate", "thisOperationUid", "mudType"};
				Object[] paramsValues = {selectedDaily.getDayDate(), operationUid, mudType};
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, MudProperties.class, "metalCuttings");
				
				//numeric or percentage value
				//query statement for getting cumulative metal cuttings for numeric or percentage value
				String strSql = "SELECT SUM(mudproperties.metalCuttings) FROM Daily daily, MudProperties mudproperties WHERE (daily.isDeleted = false or daily.isDeleted is null) and (mudproperties.isDeleted = false or mudproperties.isDeleted is null) and mudproperties.dailyUid = daily.dailyUid AND daily.dayDate <= :userDate AND daily.operationUid = :thisOperationUid and mudproperties.mudType = :mudType";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				//return query result
				Object ResultMetalCuttings = (Object) lstResult.get(0);
				if (ResultMetalCuttings != null) cumMetalCuttings = Double.parseDouble(ResultMetalCuttings.toString()) ;
				
				//set the base value cumulative metal cutting to based on metal cuttings fields on screen, 
				//and assign to field @cumMetalCuttings
				thisConverter.setBaseValue(cumMetalCuttings);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumMetalCuttings", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumMetalCuttings", thisConverter.getConvertedValue());
	
				//mass measure
				//query statement for getting cumulative metal cuttings for mass measure
				String strSqlMass = "SELECT SUM(mudproperties.metalCuttingsMass) FROM Daily daily, MudProperties mudproperties WHERE (daily.isDeleted = false or daily.isDeleted is null) and (mudproperties.isDeleted = false or mudproperties.isDeleted is null) and mudproperties.dailyUid = daily.dailyUid AND daily.dayDate <= :userDate AND daily.operationUid = :thisOperationUid and mudproperties.mudType = :mudType";
				List lstResultMass = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlMass, paramsFields, paramsValues);
				
				//return query result
				Object ResultMetalCuttingsMass = (Object) lstResultMass.get(0);
				if (ResultMetalCuttingsMass != null) cumMetalCuttingsMass = Double.parseDouble(ResultMetalCuttingsMass.toString()) ;
				
				//set the base value cumulative metal cutting to based on metal cuttings fields on screen, 
				//and assign to field @cumMetalCuttingsMass
				thisConverter.setReferenceMappingField(MudProperties.class, "metalCuttingsMass");
				thisConverter.setBaseValue(cumMetalCuttingsMass);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumMetalCuttingsMass", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumMetalCuttingsMass", thisConverter.getConvertedValue());
			}
			
			
			//calculate cumulative mud cost to date by mud type
			//variable declaration
			String mudtype = "";
			if (mudProperties.getMudType()!=null) mudtype = mudProperties.getMudType().toString();
			String[] paramsFieldsType = {"todayDate", "operationUid", "mudType"};
			Object[] paramsValuesType = {selectedDaily.getDayDate(), operationUid, mudtype};
			
			//query statement to sum mud cost up to date with selected date group by mud type
			String strSql = "SELECT SUM(mud.mudCost) as totalmudcost FROM MudProperties mud, Daily d WHERE (mud.isDeleted = false or mud.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and d.dailyUid = mud.dailyUid AND d.dayDate <= :todayDate AND mud.operationUid = :operationUid and mud.mudType = :mudType";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldsType, paramsValuesType);
			
			Object ResultMudCost = (Object) lstResult.get(0);
			Double dailyMudCostByType = 0.00;
			if (ResultMudCost != null) dailyMudCostByType = Double.parseDouble(ResultMudCost.toString());
			
			//get the field mapping of the mud cost and assign to cumMudTypeCost
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, MudProperties.class, "mudCost");
			thisConverter.setBaseValue(dailyMudCostByType);
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@cumMudTypeCost", thisConverter.getUOMMapping());
			}
			
			node.getDynaAttr().put("cumMudTypeCost", thisConverter.getConvertedValue());
			
			
			CommonUtil.getConfiguredInstance().setAlternativeUnitAsDynAttr(node, MudProperties.class, "mudWeight", altMudWeightUnitId, altMudWeightPrecision, "mudWeightAltUnit", mudProperties.getMudWeight());
			
			//calculate cumulative dhloss to date
			//variable declaration
			Double cumDhloss = 0.00;
			String[] paramsFieldsType2 = {"todayDate", "operationUid"};
			Object[] paramsValuesType2 = {selectedDaily.getDayDate(), operationUid};
			String[] paramsFieldsType3 = {"todayDate", "operationUid","checkNo"};
			Object[] paramsValuesType3 = {selectedDaily.getDayDate(), operationUid,checkNo};
			String[] paramsFieldsType4 = {"todayDate", "operationUid","checkNo","reportTime"};
			Object[] paramsValuesType4 = {selectedDaily.getDayDate(), operationUid,checkNo,reportDate};
			
			//query statement to sum dhloss up to the date one day before the selected date 
			
			Double cumDhlossBeforeDate =0.00;
			String strSql2 = "SELECT SUM(mud.dhloss) as totaldhloss1 FROM MudProperties mud, Daily d WHERE (mud.isDeleted = false or mud.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and d.dailyUid = mud.dailyUid AND mud.operationUid = :operationUid AND d.dayDate < :todayDate";
			List lstResultloss = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFieldsType2, paramsValuesType2);
			Object ResultDhloss = (Object) lstResultloss.get(0);
			if (ResultDhloss != null) cumDhlossBeforeDate = Double.parseDouble(ResultDhloss.toString());
			
			//query statement for cumdh for selected date with checkNum
			Double cumDhlossOnDateWithCheckNum =0.00;
			String strSql3 = "SELECT SUM(mud.dhloss) as totaldhloss2 FROM MudProperties mud, Daily d WHERE (mud.isDeleted = false or mud.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and d.dailyUid = mud.dailyUid AND mud.operationUid = :operationUid AND d.dayDate = :todayDate AND mud.reportNumber< :checkNo";
			lstResultloss = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFieldsType3, paramsValuesType3);
			ResultDhloss = (Object) lstResultloss.get(0);
			if (ResultDhloss != null) cumDhlossOnDateWithCheckNum = Double.parseDouble(ResultDhloss.toString());
			
			//query statement for cumdh for selected date with CheckTime
			Double cumDhlossOnDateWithCheckTime = 0.00;
			String strSql4 = "SELECT SUM(mud.dhloss) as totaldhloss2 FROM MudProperties mud, Daily d WHERE (mud.isDeleted = false or mud.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and d.dailyUid = mud.dailyUid AND mud.operationUid = :operationUid AND d.dayDate = :todayDate AND mud.reportNumber= :checkNo AND mud.reportTime<= :reportTime";
			lstResultloss = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFieldsType4, paramsValuesType4);
			ResultDhloss = (Object) lstResultloss.get(0);
			if (ResultDhloss != null) cumDhlossOnDateWithCheckTime = Double.parseDouble(ResultDhloss.toString());
		
			//Calculate cumDhLoss up until today
			cumDhloss = cumDhlossBeforeDate + cumDhlossOnDateWithCheckNum + cumDhlossOnDateWithCheckTime;
			
			//get the field mapping of the dhloss and assign to cumDhloss
			CustomFieldUom thisConverterloss = new CustomFieldUom(commandBean, MudProperties.class, "dhloss");
			thisConverterloss.setBaseValue(cumDhloss);
			
			if (thisConverterloss.isUOMMappingAvailable()){
				node.setCustomUOM("@cumDhloss", thisConverterloss.getUOMMapping());
			}
			node.getDynaAttr().put("cumDhloss", thisConverterloss.getConvertedValue());
		}

		
	}
	
	private void loadMudVolumesRecordList(CommandBeanTreeNode node, MudProperties mudProperties, UserSelectionSnapshot userSelection) throws Exception {
		
		String[] paramsFields = {"mudPropertiesUid"};
		String[] paramsValues = {mudProperties.getMudPropertiesUid()};
		//get mud volumes based on the mud_properties_uid
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM MudVolumes WHERE (isDeleted = false or isDeleted is null) and mudPropertiesUid=:mudPropertiesUid order by checknumber", paramsFields, paramsValues);;
		String relatedMudVolumes = null;
		//check whether have the mud volume records or not
		if (items.isEmpty()){
			//if it has no mud volume records, then the IsMudVolumes dynamic attribute is set to no
			node.getDynaAttr().put("IsMudVolumes", "no");
		}else {
			//if it has mud volume record, then loop for all records to get the checknumber, mud_volume_uid and daily_uid value
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object obj_array = (Object) i.next();
				//variable declaration
				MudVolumes thisMudVolumes = (MudVolumes) obj_array;
				String MudVolumesNumber= StringUtils.isNotBlank(thisMudVolumes.getChecknumber())?URLEncoder.encode(thisMudVolumes.getChecknumber(), "utf-8"):"";
				String mudVolumesUid = StringUtils.isNotBlank(thisMudVolumes.getMudVolumesUid())?URLEncoder.encode(thisMudVolumes.getMudVolumesUid(), "utf-8"):"";
				String mudDailyUid = StringUtils.isNotBlank(thisMudVolumes.getDailyUid())?URLEncoder.encode(thisMudVolumes.getDailyUid(), "utf-8"):"";
				
				if (StringUtils.isBlank(relatedMudVolumes)) {
					//relatedMudVolumes = mudVolumesNumber + "=" + mudVolumesUid + "#" + mudDailyUid;
					relatedMudVolumes = "mudvolumesnumber=" + MudVolumesNumber + "&mudvolumesuid=" + mudVolumesUid + "&muddailyuid=" + mudDailyUid;
				}else {
					//relatedMudVolumes = relatedMudVolumes + "," + mudVolumesNumber + "=" + mudVolumesUid + "#" + mudDailyUid;
					relatedMudVolumes += ",mudvolumesnumber=" + MudVolumesNumber + "&mudvolumesuid=" + mudVolumesUid + "&muddailyuid=" + mudDailyUid;
				}
				
			}
			 //put all the related mud volume values into mudvolumes dynamic attribute
			node.getDynaAttr().put("mudvolumes", relatedMudVolumes);
		}

	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		if (node.getData() instanceof MudProperties) {
			Object object = node.getData();
			MudProperties mudProperties = (MudProperties) object;
			Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, mudProperties.getDailyUid());
			Calendar dailyDayDate = Calendar.getInstance();
			dailyDayDate.setTime(daily.getDayDate());
			if(PropertyUtils.getProperty(object, "reportTime") != null)
			{
				Calendar reportTime = Calendar.getInstance();
				reportTime.setTime((Date) PropertyUtils.getProperty(object, "reportTime"));
				reportTime.set(Calendar.YEAR, dailyDayDate.get(Calendar.YEAR));
				reportTime.set(Calendar.MONTH, dailyDayDate.get(Calendar.MONTH));
				reportTime.set(Calendar.DAY_OF_MONTH, dailyDayDate.get(Calendar.DAY_OF_MONTH));
				PropertyUtils.setProperty(object, "reportTime", reportTime.getTime());
			}
		}
	}
	
	//change DDR mud type value when addnew/edit/update data on mud properties screen.
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		if (node.getData() instanceof MudProperties) 
		{
			//auto update DGR mud type when DDR mud properties mud type is changed
			MudProperties mudProperties = (MudProperties) node.getData();
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_MUD_TYPE))){
				String reportType = "DGR";
						
				if (mudProperties.getDailyUid() != null) 
				{
					String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid and dailyUid = :selectedDailyUid AND reportType = :thisReportType";
					String[] paramsFields = {"selectedOperationUid", "selectedDailyUid", "thisReportType"};
					String[] paramsValues = {session.getCurrentOperationUid(), mudProperties.getDailyUid(), reportType};
					List<ReportDaily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					if (lstResult.size() > 0)
					{
						ReportDaily selectedDDRReportDaily = (ReportDaily) lstResult.get(0);
						String strSql1 = "FROM MudProperties WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid and dailyUid = :selectedDailyUid order by reportNumber desc";
						String[] paramsFields1 = {"selectedOperationUid", "selectedDailyUid"};
						String[] paramsValues1 = {selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid()};
						List<MudProperties> lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
						if (lstResult1.size() > 0)
						{
							MudProperties mudData = (MudProperties) lstResult1.get(0);
							String strSql2 = "UPDATE ReportDaily SET mudType =:mudType WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
							String[] paramsFields2 = {"mudType", "operationUid", "dailyUid", "reportType"};
							Object[] paramsValues2 = {mudData.getMudType(), mudData.getOperationUid(), mudData.getDailyUid(), reportType};
							ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields2, paramsValues2);
		
						}
					}
				}
			}
			
			//18761 update all bha total weights 
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_WEIGHT_DRY_WET_BELOW_JAR_CALCULATION))) {
				CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(),MudProperties.class, "mudWeight");
				if (session != null && session.getCurrentDailyUid() != null) {
					MudPropertiesUtil.updateBHATotalWeights(mudProperties.getOperationUid(), session.getCurrentDailyUid(), thisConverter);	
				}
				else {
					MudPropertiesUtil.updateBHATotalWeights(mudProperties.getOperationUid(), null, thisConverter);					
				}	
			}
		}
		
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		if (node.getData() instanceof MudProperties) 
		{
			//auto update DGR mud type when DDR mud properties mud type is changed
			MudProperties mudProperties = (MudProperties) node.getData();
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_MUD_TYPE))){
				String reportType = "DGR";
						
				if (mudProperties.getDailyUid() != null) 
				{
					String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid and dailyUid = :selectedDailyUid AND reportType = :thisReportType";
					String[] paramsFields = {"selectedOperationUid", "selectedDailyUid", "thisReportType"};
					String[] paramsValues = {session.getCurrentOperationUid(), mudProperties.getDailyUid(), reportType};
					List<ReportDaily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					if (lstResult.size() > 0)
					{
						ReportDaily selectedDDRReportDaily = (ReportDaily) lstResult.get(0);
						String strSql1 = "FROM MudProperties WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid and dailyUid = :selectedDailyUid order by reportNumber desc";
						String[] paramsFields1 = {"selectedOperationUid", "selectedDailyUid"};
						String[] paramsValues1 = {selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid()};
						List<MudProperties> lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
						if (lstResult1.size() > 0)
						{
							MudProperties mudData = (MudProperties) lstResult1.get(0);
							String strSql2 = "UPDATE ReportDaily SET mudType =:mudType WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
							String[] paramsFields2 = {"mudType", "operationUid", "dailyUid", "reportType"};
							Object[] paramsValues2 = {mudData.getMudType(), mudData.getOperationUid(), mudData.getDailyUid(), reportType};
							ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields2, paramsValues2);
		
						}
						else
						{
							String mudType = null;
							String strSql2 = "UPDATE ReportDaily SET mudType =:mudType WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
							String[] paramsFields2 = {"mudType", "operationUid", "dailyUid", "reportType"};
							Object[] paramsValues2 = {mudType, selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
							ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields2, paramsValues2);
						}
					}
				}
			}
			
			//18761 update all bha total weights  
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_WEIGHT_DRY_WET_BELOW_JAR_CALCULATION))) {
				CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(),MudProperties.class, "mudWeight");
				if (session != null && session.getCurrentDailyUid() != null) {
					MudPropertiesUtil.updateBHATotalWeights(mudProperties.getOperationUid(), session.getCurrentDailyUid(), thisConverter);	
				}
				else {
					MudPropertiesUtil.updateBHATotalWeights(mudProperties.getOperationUid(), null, thisConverter);
				}	
			}
		}
		
	}
	
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession session, CommandBeanTreeNode parent) throws Exception
	{

		if (MudRheology.class.getSimpleName().equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted())
		{
			UserSelectionSnapshot userSelection = new UserSelectionSnapshot( session );
			
			// Calculate mudPV field ONLY if GWP "autoCalculateMudPv" is turned on
			if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_MUDPV))){
				// call calculateMudPV method which located under MudPropertiesUtil for field computation
				MudPropertiesUtil.calculateMudPV(status.getUpdatedNodes(), session, parent);
			}
			// Calculate mudYp field ONLY if GWP "autoCalculateMudYp" is turned on
			if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_MUDYP))){
				// call calculateMudYp method which located under MudPropertiesUtil for field computation
				MudPropertiesUtil.calculateMudYp(status.getUpdatedNodes(), session, parent);
			}
		}
	}
	
	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{
		Object object = targetNode.getData();
		if (object instanceof MudProperties) {
			MudProperties mudProperties = (MudProperties) object;
			mudProperties.setImportParameters(null);
		}
	}
	

	
}
