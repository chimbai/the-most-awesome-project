package com.idsdatanet.d2.drillnet.activity.lookup;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.LookupClassCode;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class ActivityClassCodeLookupHandler implements LookupHandler{	
	private String orderBy = "name";
	
	public void setOrderBy(String value){
		this.orderBy = value;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		//activity, well, and operation objects are used because userSelection does not have information required by depot related processes.
		Activity activity = null;
		Well well = null;
		Operation operation = null;
		if(node != null && node.getData() instanceof Activity) {
			activity = (Activity) node.getData();
			well = ApplicationUtils.getConfiguredInstance().getCachedWell(activity.getWellUid());
			operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(activity.getOperationUid());
		} else { //assuming process is not from depot
			well = ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid());
			operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		}
		
		if(well != null && operation != null) {
		    String sql ="FROM LookupClassCode where  (isDeleted = false or isDeleted is null) and (operationCode =:operationCode or operationCode ='' or operationCode is null) " + 
				" order by " + this.orderBy;
			List<LookupClassCode> listLookupClassCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { "operationCode"}, new String[] {operation.getOperationCode()});
			if (listLookupClassCode.size() > 0) { 
				for (LookupClassCode lookupClassCode : listLookupClassCode) {
					LookupItem newItem = new LookupItem(lookupClassCode.getShortCode(),lookupClassCode.getName() + " (" + lookupClassCode.getShortCode() + ")");
					newItem.setActive(lookupClassCode.getIsActive());
					if ("es".equals(userSelection.getUserLanguage())) {
						if (StringUtils.isNotEmpty(lookupClassCode.getNameEs()))
							newItem.setValue(lookupClassCode.getNameEs() + " (" + newItem.getKey() + ")");
					}
					Map<String,String> filterProperties = new LinkedHashMap<String,String>();
					filterProperties.put("internalCode",lookupClassCode.getInternalCode()!=null?lookupClassCode.getInternalCode():null);
					newItem.setFilterProperties(filterProperties);
					newItem.setTableUid(lookupClassCode.getLookupClassCodeUid());
					result.put(lookupClassCode.getShortCode(), newItem);
				}
			}
		}
		

		return result;
	}
	
}
