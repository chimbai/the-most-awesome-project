package com.idsdatanet.d2.drillnet.hserelatedoperationsetup;

import java.io.ByteArrayOutputStream;
import java.util.List;
import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.model.HseRelatedOperation;


public class HseRelatedOperationSetupUtils extends BlazeRemoteClassSupport {

	public String getOperationList() {
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			String strSql = "SELECT operationUid, operationName FROM Operation WHERE (isDeleted='' OR isDeleted IS NULL) AND operationUid <> :thisOperationUid AND operationUid NOT IN (SELECT relatedOperationUid FROM HseRelatedOperation WHERE operationUid = :operationUid AND (isDeleted='' OR isDeleted IS NULL)) ORDER BY operationName";
			String[] paramsFields = {"thisOperationUid", "operationUid"};
			Object[] paramsValues = {getCurrentUserSession().getCurrentOperationUid(), getCurrentUserSession().getCurrentOperationUid()};
			
			List<Object[]> objs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			for (Object[] obj : objs) {
				writer.startElement("Operation");
				writer.addElement("operationUid", (String) obj[0]);
				writer.addElement("operationName", (String) obj[1]);
				writer.endElement();
			}
			writer.endElement();
			writer.close();
			
			return bytes.toString();
		} catch (Exception e) {
			return "<root/>";
		}
	}
	
	public String getRelatedOperationList() {
		
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			String strSql = "SELECT a.operationName AS operationName, b.relatedOperationUid AS relatedOperationUid FROM Operation a, HseRelatedOperation b WHERE (b.isDeleted='' OR b.isDeleted IS NULL) AND b.operationUid = :thisOperationUid AND a.operationUid = b.relatedOperationUid ORDER BY a.operationName";
			List<Object[]> objs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisOperationUid", getCurrentUserSession().getCurrentOperationUid());
			
			for (Object[] obj : objs) {
				writer.startElement("HseRelatedOperation");
				writer.addElement("operationName", (String) obj[0]);
				writer.addElement("relatedOperationUid", (String) obj[1]);
				writer.endElement();
				
			}
			writer.endElement();
			writer.close();
			
			return bytes.toString();
		} catch (Exception e) {
			return "<root/>";
		}
	}
	
	public void saveRelatedOperationList(List<String> operationUidList) {
		try {
			String strSql = "SELECT hseRelatedOperationUid, relatedOperationUid FROM HseRelatedOperation WHERE operationUid = :thisOperationUid AND (isDeleted='' OR isDeleted IS NULL)";
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisOperationUid", getCurrentUserSession().getCurrentOperationUid());
			for (Object obj: list) {
				Object[] objArray = (Object[]) obj;
				
				if (operationUidList.contains(objArray[1])) {
					operationUidList.remove(objArray[1]);
				} else {
					String strSql2 = "UPDATE HseRelatedOperation SET isDeleted = true WHERE hseRelatedOperationUid = :thisHseRelatedOperationUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, new String[] {"thisHseRelatedOperationUid"}, new Object[] {objArray[0]});
				}
			}
			for (String operationUid : operationUidList) {
				HseRelatedOperation thisHseRelatedOperation = new HseRelatedOperation();
				thisHseRelatedOperation.setOperationUid(getCurrentUserSession().getCurrentOperationUid());
				thisHseRelatedOperation.setRelatedOperationUid(operationUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisHseRelatedOperation);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
}
