package com.idsdatanet.d2.drillnet.report.schedule.bop;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.context.ServletContextAware;

import com.idsdatanet.d2.core.model.Bop;
import com.idsdatanet.d2.core.model.BopLog;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.report.schedule.EmailConfiguration;

public class NextBopTestReportExtractionService extends DefaultBeanSupport  implements ServletContextAware, InitializingBean {

	private static final String RIG_PATTERN="{rigname}";

	private Integer noOfDaysToAlert = 3;
	private MailEngine mailEngine=null;
	private String supportEmail=null;
	private EmailConfiguration emailConfig = null;
	private Boolean enabled = false;
	
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	
	public EmailConfiguration getEmailConfig() {
		return emailConfig;
	}

	public void setEmailConfig(EmailConfiguration emailConfig) {
		this.emailConfig = emailConfig;
	}

	public Integer getNoOfDaysToAlert() {
		return noOfDaysToAlert;
	}

	public void setNoOfDaysToAlert(Integer noOfDaysToAlert) {
		this.noOfDaysToAlert = noOfDaysToAlert;
	}

	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}

	public MailEngine getMailEngine() {
		return mailEngine;
	}

	public void setSupportEmail(String supportEmail) {
		this.supportEmail = supportEmail;
	}
	
	public String getSupportEmail() {
		return supportEmail;
	}

	public synchronized void run() throws Exception {
		if (this.getEnabled()) {
			this.checkNextBopTestAndSendEmail();
		}
	}		

	private synchronized void checkNextBopTestAndSendEmail() throws Exception {
		try {
			Calendar todayMidnight = Calendar.getInstance();
			todayMidnight.setTime(new Date());
			todayMidnight.set(Calendar.HOUR, 23);
			todayMidnight.set(Calendar.MINUTE, 59);
			todayMidnight.set(Calendar.SECOND, 59);
	
			Calendar todayDate = Calendar.getInstance();
			todayDate.setTime(new Date());
			todayDate.set(Calendar.HOUR, 0);
			todayDate.set(Calendar.MINUTE, 0);
			todayDate.set(Calendar.SECOND, 0);
			
			Calendar nextBopTestDate = Calendar.getInstance();
			nextBopTestDate.setTime(new Date());
			nextBopTestDate.add(Calendar.DATE, this.noOfDaysToAlert); //add N days
			nextBopTestDate.set(Calendar.HOUR, 23);
			nextBopTestDate.set(Calendar.MINUTE, 59);
			nextBopTestDate.set(Calendar.SECOND, 59);
	
			
			String queryString = "FROM RigInformation " +
					"WHERE (isDeleted=false or isDeleted is NULL) " +
					"AND rigInformationUid in (select rigInformationUid FROM Bop WHERE (isDeleted=false or isDeleted is NULL) and (installDate<=:todayDate) and (removeDate is null or removeDate>=:todayMidnight)) " +
					"AND (bopNotificationEmail is not null) ";
			List<RigInformation> rigList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"todayDate", "todayMidnight"}, new Object[]{todayDate.getTime(), todayMidnight.getTime()});
			
			for (RigInformation rig : rigList) {
				if (StringUtils.isBlank(rig.getBopNotificationEmail())) continue;
				Map<String, Object> bopTestList = new HashMap<String, Object>();
				queryString = "select b.bopUid, max(bl.nextTestDatetime) FROM Bop b, BopLog bl " +
						"WHERE (b.isDeleted=false or b.isDeleted is NULL) " +
						"AND (bl.isDeleted=false or bl.isDeleted is NULL) " +
						"AND b.bopUid=bl.bopUid " +
						"AND b.rigInformationUid=:rigInformationUid " +
						"AND (b.installDate<=:todayDate) " +
						"AND (b.removeDate is null or b.removeDate>=:todayMidnight) " +
						"AND (bl.nextTestDatetime is not null) " +
						"GROUP BY b.bopUid ";
				List<Object[]> logList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, 
						new String[]{"rigInformationUid", "todayDate", "todayMidnight"}, 
						new Object[]{rig.getRigInformationUid(), todayDate.getTime(), todayMidnight.getTime()});
				for (Object[] log : logList) {
					String bopUid = (String) log[0];
					Date nextTestDateTime = (Date) log[1];
					if (nextTestDateTime.before(todayDate.getTime())) {
						Map<String, Object> map = new HashMap<String, Object>();
						this.setLastBopLogDetails(map, bopUid);
						setFieldsToMap(map, rig);
						map.put("logStatus", "OVERDUE");
						bopTestList.put(bopUid, map);
					} else if (nextTestDateTime.before(nextBopTestDate.getTime())) {
						Map<String, Object> map = new HashMap<String, Object>();
						this.setLastBopLogDetails(map, bopUid);
						setFieldsToMap(map, rig);
						map.put("logStatus", "SOON");
						bopTestList.put(bopUid, map);
					}
				}
				
				if (bopTestList.size()>0 && emailConfig!=null) {
					emailConfig.setEmailList(rig.getBopNotificationEmail().split(","));
					emailConfig.clearContentMapping();
					emailConfig.addContentMapping("BopTestList", bopTestList);
					emailConfig.addContentMapping("serverDatetime", (new Date()).toString());
					this.mailEngine.sendMail(emailConfig.getEmailList(), this.getSupportEmail(), ApplicationConfig.getConfiguredInstance().getSupportEmail(),
							this.constructEmailSubject(emailConfig.getSubject(), setFieldsToMap(new HashMap<String,Object>(), rig)), this.mailEngine.generateContentFromTemplate(emailConfig.getTemplateFile(), emailConfig.getContentMapping()));
				}
			}
		} catch (Exception ex) {
			this.getLogger().error(ex);
		}
	}
	
	private void setLastBopLogDetails(Map map, String bopUid) throws Exception {
		Bop bop = (Bop) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Bop.class, bopUid);
		if (bop!=null) setFieldsToMap(map, bop);
		String queryString = "FROM BopLog WHERE (isDeleted=false or isDeleted is null) and bopUid=:bopUid ORDER BY nextTestDatetime DESC";
		List<BopLog> bopLogs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "bopUid", bopUid);
		if (bopLogs.size()>0) {
			setFieldsToMap(map, bopLogs.get(0));
		}
	}
	
	private Map<String, Object> setFieldsToMap(Map map, Object dataset) throws Exception {
		SimpleDateFormat reportDateFormatter = ApplicationConfig.getConfiguredInstance().getReportDateFormater();
		CustomFieldUom thisConverter = new CustomFieldUom((Locale)null);
		Map<String, Object> objectMap = PropertyUtils.describe(dataset);
		for (Map.Entry<String, Object> entry : objectMap.entrySet()) {
			if (entry.getValue()!=null) {
				if (entry.getValue() instanceof Date) {
					map.put(entry.getKey(), reportDateFormatter.format(entry.getValue()));
				} else {
					thisConverter.setReferenceMappingField(dataset.getClass(), entry.getKey());
					if (thisConverter.isUOMMappingAvailable()) {
						map.put(entry.getKey(), thisConverter.getFormattedValue((Double)entry.getValue()));
					} else {
						map.put(entry.getKey(), entry.getValue().toString());
					}
				}
			}
		}
		return map;
	}

	private String constructEmailSubject(String subject, Map data) throws Exception
	{
		subject = subject.replace(RIG_PATTERN, (String) data.get("rigName"));
		return subject;
	}
	

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		if (this.supportEmail==null)
			this.supportEmail=ApplicationConfig.getConfiguredInstance().getSupportEmail();
		if (StringUtils.isEmpty(this.supportEmail) && this.getEnabled())
			throw new Exception("[D2] ["+this.getClass().getSimpleName()+"] Error Support Email Not Configured");
		if (emailConfig==null) 
			throw new Exception("[D2] ["+this.getClass().getSimpleName()+"] Error Email Config Not Configured");
	}

	public void setServletContext(ServletContext arg0) {
		// TODO Auto-generated method stub
		
	}
}