package com.idsdatanet.d2.drillnet.partnerportal;

import com.idsdatanet.d2.core.model.FileManagerFiles;

public class PublicUploadedFileList {
		private FileManagerFiles uploadedFiles;
		
		public void setData(FileManagerFiles uploadedFiles){
			this.uploadedFiles = uploadedFiles;
		}
		
		public String getFileName(){
			return this.uploadedFiles.getFileName();
		}
		
		public String getFileUid(){
			return this.uploadedFiles.getFileManagerFilesUid();
		}
}
