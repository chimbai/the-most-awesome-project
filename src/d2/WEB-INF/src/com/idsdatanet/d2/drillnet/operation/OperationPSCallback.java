package com.idsdatanet.d2.drillnet.operation;

import java.util.List;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.stt.DefaultSTTActionCallback;
import com.idsdatanet.d2.core.uom.hibernate.tables.UomTemplate;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class OperationPSCallback extends DefaultSTTActionCallback {
	public void afterSendToTownCompleted(UserSelectionSnapshot userSelection) {
 
	}
	
	public void afterRefreshFromTownCompleted(UserSelectionSnapshot userSelection) {
		try {
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, userSelection.getOperationUid());

			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from UomTemplate where (isDeleted = false or isDeleted is null) and groupUid = :groupUid and defaultTemplate = true", "groupUid", userSelection.getGroupUid());
			if (lstResult.size() > 0) {
				UomTemplate uomTemplate = (UomTemplate) lstResult.get(0);
				operation.setUomTemplateUid(uomTemplate.getUomTemplateUid());
			    ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(operation);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
