package com.idsdatanet.d2.drillnet.reportDaily;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.PobMaster;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigPumpParam;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.common.startnewday.StartNewDaySetting;

public class ReportDailyCommandBeanListener extends DefaultBeanSupport implements com.idsdatanet.d2.core.web.mvc.CommandBeanListener  {
	
	private static final String QC_NOT_DONE = "0";
	
	private String defaultReportType;
	//private Boolean ignore24HourChecking = false;
	
	public void setReportType(String value){
		this.defaultReportType = value;
	}
	public String rangeType = null;
	public String selectedOperation = null;
	public String selectedDay = null;
	public String excludeOperationType = null;
	public Boolean excludeCopyDataFilters = false ;
	public Boolean carryDataFromAllOperations = false ;
	private String forwardShiftReportType;
	private String shiftCountry = "AT";
	
	public void setCarryDataFromAllOperations(Boolean value) {
		this.carryDataFromAllOperations = value;
	}
	
	public void setForwardShiftReportType(String value){
		this.forwardShiftReportType = value;
	}
	
	public void setShiftCountry(String value){
		this.shiftCountry = value;
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
//		double totalActivityDuration = ActivityUtils.recalculateTotalDuration(commandBean,userSelection.getDailyUid(),userSelection.getOperationUid());
//		
//		String operationHour = "24";
//		if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "act_show_warning_if_not_24")))
//		{	
//			String newActivityRecordInputMode = null;
//			
//			if (!("1".equals(newActivityRecordInputMode))){
//				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
//				
//				thisConverter.setBaseValue(86400);
//				if (StringUtils.isNotBlank(operationHour)) {
//					Double totalHoursInSecond = Double.parseDouble(operationHour);
//					if (!totalHoursInSecond.isNaN()) {
//						totalHoursInSecond = totalHoursInSecond * 3600.0;
//						thisConverter.setBaseValue(totalHoursInSecond);
//					}
//				}
//				
//				if (!thisConverter.getFormattedValue(totalActivityDuration).equals(thisConverter.getFormattedValue()) && !this.ignore24HourChecking){ 
//					commandBean.getSystemMessage().addInfo("Total hours for activities in the day (" + thisConverter.getFormattedValue(totalActivityDuration) + ") is not " + operationHour + " hours.");
//				}
//			}
//			
//			// Start Ticket:20090409-0703-ekhoo - Checking time overlapping or gaps and prompt warning message
//			//if("0".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoCalculateActivityDuration"))){
//				if (ActivityUtils.isTimeOverlapped(commandBean,userSelection.getDailyUid(),userSelection.getOperationUid())) {
//					commandBean.getSystemMessage().addInfo("There is time overlapping / gaps between activities.");
//				}
//
//			//}
//			// End Ticket:20090409-0703-ekhoo
//		}
		
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN){
			if("1".equals(request.getParameter("newRecordInputMode"))) commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
			
			String rigInformationUid = "";
			Date thisLastDate = null;
			
			/*
			Map<String, CommandBeanTreeNode> mapData = root.getList().get("ReportDaily");
			
			for(CommandBeanTreeNode thisNode: mapData.values()){
				
				if (thisNode.getAtts().getEditMode()){
				
					Object thisObj = thisNode.getData();
					
					if (thisObj instanceof ReportDaily){
						
						ReportDaily currentReportDaily = (ReportDaily) thisObj;
						
						rigInformationUid = currentReportDaily.getRigInformationUid();
						thisLastDate = CommonDateParser.parse(thisNode.getDynaAttr().get("daydate").toString());
						
						break;
					}
				}
			}
			*/
			
			//have to use same logic to get the date like in dataNode for start new record before user press Refresh
			// default date
			Operation thisOperation = UserSession.getInstance(request).getCurrentOperation();
			if (thisOperation != null) {
				// default rig used
				if (rigInformationUid == "") rigInformationUid = ApplicationUtils.getConfiguredInstance().getRigInformationUid(thisOperation.getOperationUid(), null);
				
				//if user not fill in date
				if (thisLastDate == null) {
					String strSql = "SELECT MAX(reportDatetime) FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and reportType=:thisReportType AND operationUid=:thisOperationUid GROUP BY operationUid";
					String[] paramsFields = {"thisReportType", "thisOperationUid"};
					Object[] paramsValues = {this.defaultReportType, thisOperation.getOperationUid()};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					if (!lstResult.isEmpty())
					{
						Object b = (Object) lstResult.get(0);
						if (b != null) thisLastDate = (Date) b;
				
					}
				}

				if (thisLastDate != null) {				
					thisLastDate = DateUtils.addDays(thisLastDate, 1);	
					
				} else {
					// TODO this is not time zone sensitive, fix this
					Date newDayDate = CommonUtil.getConfiguredInstance().currentDateTime(userSelection.getGroupUid(), userSelection.getWellUid());
					
					// get from operation's start date for report daily's date if operation is not null or empty.
					if(StringUtils.isNotBlank(userSelection.getOperationUid())) {
						Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, userSelection.getOperationUid());
						if(operation != null && (operation.getIsDeleted() == null || !operation.getIsDeleted())) {
							if(operation.getStartDate() != null) newDayDate = operation.getStartDate();
						}
					}
					
					//need to reset time to 00:00 so will not effect date in future after add GMT offset
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime(newDayDate);
					thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
					thisCalendar.set(Calendar.MINUTE, 0);
					thisCalendar.set(Calendar.SECOND , 0);
					thisCalendar.set(Calendar.MILLISECOND , 0);
					
					newDayDate = thisCalendar.getTime();
					
					thisLastDate = newDayDate;
				}
				
				this.excludeCopyDataFilters = ReportDailyUtils.showCopyDataFilters(thisOperation.getGroupUid(), thisOperation.getOperationCode()) ;	
				
				this.loadAvailableCarryForwardOptions(commandBean, request, thisLastDate, rigInformationUid);
			}
			
			// user who are not allowed to access un-QC'd data are not allowed to start new day
			if (request != null) {
				UserSession session = UserSession.getInstance(request);
				commandBean.setSupportedAction(Action.ADD, StringUtils.isBlank(session.getCurrentUserAccessScope()) || QC_NOT_DONE.equals(session.getCurrentUserAccessScope()));
			}
			
			if (forwardShiftReportType!=null && StringUtils.isNotBlank(forwardShiftReportType)){
				
				if (ReportDailyUtils.isOmvCountrySpecific(userSelection.getWellUid(), userSelection.getOperationUid(), forwardShiftReportType, shiftCountry)){
					String operationHour = "24";
					double totalDuration = ReportDailyUtils.recalculateTotalShiftDuration(commandBean,userSelection.getDailyUid(),userSelection.getOperationUid());
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
					thisConverter.setBaseValue(86400);
					
					if (StringUtils.isNotBlank(operationHour)) {
						Double totalHoursInSecond = Double.parseDouble(operationHour);
						if (!totalHoursInSecond.isNaN()) {
							totalHoursInSecond = totalHoursInSecond * 3600.0;
							thisConverter.setBaseValue(totalHoursInSecond);
						}
					}
					
					if (!thisConverter.getFormattedValue(totalDuration).equals(thisConverter.getFormattedValue())){ 
						commandBean.getSystemMessage().addInfo("Total shift hours created (" + thisConverter.getFormattedValue(totalDuration) + ") is not " + operationHour + " hours.");
					}
					
					if (ReportDailyUtils.isShiftTimeOverlapped(commandBean,userSelection.getDailyUid(),userSelection.getOperationUid())) {
						commandBean.getSystemMessage().addInfo("There is at least a time overlap or gap in between shifts.");
					}
				}
			}
		}
	}

	protected void loadAvailableCarryForwardOptions(CommandBean commandBean, HttpServletRequest request, Object dayDate, String selectedRigUid) throws Exception{
		ArrayList list = new ArrayList();
		list = this.loadAvailableCarryForwardOptions(request, dayDate, selectedRigUid, null);
		commandBean.getReferenceData().put("carry_forward_list", list);
	}

	protected Map<String,String> getDailyCommandBean(UserSession session) throws Exception
	{
		return StartNewDaySetting.getConfiguredInstance().getDailyCommandBeans();
	}
	
	protected TreeModelDataDefinitionMeta getMetaForCarryOverDataChecking(BaseCommandBean commandBean) throws Exception
	{
		if (commandBean.getRootDataDefinitionMeta().size()>0)return commandBean.getRootDataDefinitionMeta().get(0);
		return null;
	}
	
	private ArrayList loadAvailableCarryForwardOptions(HttpServletRequest request, Object dayDate, String selectedRigUid, String copyFromOperationUid) throws Exception{
		
		ArrayList list = new ArrayList();
		
		
		Map<String,String> dailyCommandBean = this.getDailyCommandBean(UserSession.getInstance(request));
		
		Operation thisOperation = null;
			
		//check if it is copy from another operation or not
		if (StringUtils.isNotBlank(copyFromOperationUid)) thisOperation =  ApplicationUtils.getConfiguredInstance().getCachedOperation(copyFromOperationUid);
		else thisOperation = UserSession.getInstance(request).getCurrentOperation();
		
		if (thisOperation != null) {
			
			String lastdayUid = "";
			//lastdayUid = ApplicationUtils.getConfiguredInstance().getLastDailyUidInOperation(thisOperation.getOperationUid());
			
			if(dayDate != null) 
			{
				
				if (!this.excludeCopyDataFilters && "1".equals(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "copyDataFiltersWithDateRange")) && this.selectedDay != null){
					lastdayUid = this.selectedDay;
				}
				else {
					lastdayUid = this.getLastDailyUidByDate(dayDate, copyFromOperationUid, thisOperation);
				}
			}
								
			if (StringUtils.isNotBlank(lastdayUid)){
				
				
				String[] paramsFields = {"lastdayUid"};
				String[] paramsValues = {lastdayUid};
				
				//check modules that is carry by operation, to allow copy from yesterday
				for (String CommandBean : dailyCommandBean.keySet()){
					if (!this.excludeCommandBeanInCarryForward(request, CommandBean))
					{
						Object bean = this.getApplicationContext().getBean(CommandBean);
						BaseCommandBean thisBean = (BaseCommandBean) bean;
						TreeModelDataDefinitionMeta meta = this.getMetaForCarryOverDataChecking(thisBean);
						if (meta != null) {						
							if (!("rig".equals(thisBean.getCopyFromYesterdayBy()))) {
								Boolean allowSelect = this.allowCarryOverModuleDataByOperation(thisBean, meta, paramsFields, paramsValues, UserSession.getInstance(request));
								if (allowSelect)list.add(new String[]{dailyCommandBean.get(CommandBean), CommandBean});
							}
						}
					}
				}
			}
			
			//check modules that is carry by rig, to allow copy from yesterday
			//if no rig pass in try to get default rig
			if(StringUtils.isBlank(selectedRigUid)){
				// default rig used
				String rigInformationUid = ApplicationUtils.getConfiguredInstance().getRigInformationUid(thisOperation.getOperationUid(), null);
				if (rigInformationUid != null) {
					//node.getDynaField().getValues().put("rigid", rigInformationUid);
					selectedRigUid = rigInformationUid;
				}
			}
			
			if(StringUtils.isNotBlank(selectedRigUid)){
				String lastRigDayUid = "";
				//lastRigDayUid = ApplicationUtils.getConfiguredInstance().getLastDailyUidInOperation(thisOperation.getOperationUid());
				
				if(dayDate != null) 
				{
					Date userDate = null;
					if(dayDate instanceof java.util.Date){
						userDate = (Date) dayDate;
					}else{
						userDate = CommonDateParser.parse(dayDate.toString());
					}
					
					//if user enter an invalid date
					if(userDate == null) return null;
					
					//if is not copy from another operation, need to get previous day, else will be the selected day in another operation
					//if (StringUtils.isBlank(copyFromOperationUid)) userDate = DateUtils.addDays(userDate, -1);
					
					//Only search from current userDate and a day prior to userDate
					Date searchLimit = DateUtils.addDays(userDate, -2);
					
					//get dailyUid with Report Date and rig
					String[] paramsFields1 = {"searchLimit", "userDate", "thisRigInformationUid"};
					Object[] paramsValues1 = {searchLimit, userDate, selectedRigUid};
					String strSql = "SELECT d.dailyUid FROM ReportDaily rd, Daily d, Operation o WHERE (o.isDeleted = false or o.isDeleted is null) AND (rd.isDeleted = false or rd.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and o.operationUid=d.operationUid and o.operationUid=rd.operationUid and d.dailyUid = rd.dailyUid AND (d.dayDate > :searchLimit AND d.dayDate <= :userDate) AND rd.rigInformationUid = :thisRigInformationUid ORDER BY d.dayDate DESC";
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1);
					
					if (!lstResult.isEmpty())
					{
						Object a = (Object) lstResult.get(0);
						if (a != null) lastRigDayUid = a.toString();
					}
				}
				
				if (StringUtils.isNotBlank(lastRigDayUid)){
					String[] paramsFields = {"lastdayUid"};
					String[] paramsValues = {lastRigDayUid};
					
					//check modules that is carry by rig, to allow copy from yesterday
					for (String CommandBean : dailyCommandBean.keySet()){
						if (!this.excludeCommandBeanInCarryForward(request, CommandBean))
						{
							Object bean = this.getApplicationContext().getBean(CommandBean);
							BaseCommandBean thisBean = (BaseCommandBean) bean;
							TreeModelDataDefinitionMeta meta = this.getMetaForCarryOverDataChecking(thisBean);
							if (meta != null) {		
								if ("rig".equals(thisBean.getCopyFromYesterdayBy())) {
									Boolean allowSelect = this.allowCarryOverModuleDataByRig(thisBean, meta, paramsFields, paramsValues, UserSession.getInstance(request),dayDate);
									if (allowSelect)list.add(new String[]{dailyCommandBean.get(CommandBean), CommandBean});
	
								}
							}
						}
					}
				}
			}
		}//test operation not null
		
		return list;	
	}
	
	private Boolean excludeCommandBeanInCarryForward(HttpServletRequest request, String commandBeanId) throws Exception
	{
		UserSession session = UserSession.getInstance(request);
		String operationType = session.getCurrentOperationType();
		if (StartNewDaySetting.getConfiguredInstance().getExcludeCommandBeanByOperationType()!=null)
		{
			List<String> excludeList = StartNewDaySetting.getConfiguredInstance().getExcludeCommandBeanByOperationType().get(operationType);
			if (excludeList !=null && excludeList.contains(commandBeanId))
				return true;
		}
		return false;
	}
	
	protected String getLastDailyUidByDate(Object dayDate, String copyFromOperationUid, Operation thisOperation) throws Exception
	{
		Date userDate = null;
		if(dayDate instanceof java.util.Date){
			userDate = (Date) dayDate;
		}else{
			userDate = CommonDateParser.parse(dayDate.toString());
		}
		
		//if user enter an invalid date
		if(userDate == null) return null;
			
		//if is not copy from another operation, need to get previous day, else will be the selected day in another operation
		//if (StringUtils.isBlank(copyFromOperationUid)) userDate = DateUtils.addDays(userDate, -1);
		
		//Only search from current userDate and a day prior to userDate
		Date searchLimit = DateUtils.addDays(userDate, -2);
		
		//get dailyUid with Report Date
		String[] paramsFields = {"searchLimit", "userDate", "thisOperationUid"};
		Object[] paramsValues = new Object[3]; paramsValues[0] = searchLimit; paramsValues[1] = userDate; paramsValues[2] = thisOperation.getOperationUid();
		String strSql = "SELECT dailyUid FROM Daily WHERE (isDeleted = false or isDeleted is null) and (dayDate > :searchLimit AND dayDate <= :userDate) AND operationUid = :thisOperationUid ORDER BY dayDate DESC";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (!lstResult.isEmpty())
		{
			Object a = (Object) lstResult.get(0);
			if (a != null) return a.toString();
		}
		return null;
	}
	
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
		
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(ReportDaily.class)){
				ReportDaily thisReportDaily = (ReportDaily) targetCommandBeanTreeNode.getData();
				this.loadAvailableCarryForwardOptions(commandBean, request, targetCommandBeanTreeNode.getDynaAttr().get("daydate"), thisReportDaily.getRigInformationUid());
			}
		}
		if(ReportDaily.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())){
			if("rigsuperintendent".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				//To get Bharun record when onChange runNumber
				rigmanagerChanged(commandBean, request, targetCommandBeanTreeNode);			
			}
		}
		ReportDailyUtils.setStatus(targetCommandBeanTreeNode, commandBean, request);
	}
	
	private void rigmanagerChanged(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode node) throws Exception {
		
		String pobMasterUid = (String) PropertyUtils.getProperty(node.getData(), "rigsuperintendent");		
		
		String strSql = "FROM PobMaster WHERE (isDeleted = false or isDeleted is null) AND pobMasterUid = :thisPobMasterUid";			
		List Result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisPobMasterUid", pobMasterUid);
		
		String mobilePhone = null;
		
		if (Result.size() > 0){
			PobMaster thisPobMaster = (PobMaster) Result.get(0);
			if (StringUtils.isNotBlank(thisPobMaster.getMobilePhone())){
				mobilePhone = thisPobMaster.getMobilePhone();
			}		
		}
	
		Object thisObj = node.getData();		
		if (thisObj instanceof ReportDaily){
			
			ReportDaily currentReportDaily = (ReportDaily) thisObj;	
			currentReportDaily.setRigsuperintendentPhone(mobilePhone);
		}
		
	}	
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("flexCarryForwardSelection".equals(invocationKey)) {
			String strResult = "<carryForwardSelection>";
			String rigInformationUid = request.getParameter("rigInformationUid");
			String copyFromOperationUid = request.getParameter("copyFromOperationUid");
			String userDate = request.getParameter("userDate");
			
			if (!this.excludeCopyDataFilters && "1".equals(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "copyDataFiltersWithDateRange"))){
				if (request!= null){
					this.selectedOperation = null;
					this.rangeType = null;
					this.selectedDay = null;
					
					if (request.getParameter("rangeType")!=  null){
						this.rangeType = request.getParameter("rangeType");
					}
					
					if (request.getParameter("selectedOperation")!=  null){
						this.selectedOperation = request.getParameter("selectedOperation");
					}
					
					if (request.getParameter("selectedDay")!=  null){
						this.selectedDay = request.getParameter("selectedDay");
					}
				}
			}
			
			if(StringUtils.isNotBlank(rigInformationUid)  && StringUtils.isNotBlank(userDate))
			{
				ArrayList list = new ArrayList();
				list = this.loadAvailableCarryForwardOptions(request, userDate, rigInformationUid, copyFromOperationUid);
				if (!list.isEmpty()){					
					for (Object selectionList : list){
						String[] selectionArr = (String[]) selectionList;
						strResult = strResult + "<item key=\"" + selectionArr[0].toString() + "\" value=\"" +  selectionArr[1].toString() + "\"/>";
					}
				}
			}
			strResult = strResult + "</carryForwardSelection>";
			response.getWriter().write(strResult);
		}
		
		if ("flexCarryForwardOtherOperationSelection".equals(invocationKey)) {			
			SimpleDateFormat flexDateFormater =  new SimpleDateFormat("dd MMM yyyy");
			ArrayList list = new ArrayList();
			
			if (!this.excludeCopyDataFilters && "1".equals(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "copyDataFiltersWithDateRange"))){
				list = this.loadAvailableCarryForwardOtherOperationOptions(request, this.rangeType);
				
				if (!list.isEmpty()){	
					
					SimpleXmlWriter writer = new SimpleXmlWriter(response);
					writer.startElement("root");
					
					for (Object selectionList : list){
						String thisOperationUid = selectionList.toString();
						
						String strSql = "FROM Operation WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid";
						String[] paramsFields = {"thisOperationUid"};
						String[] paramsValues = {thisOperationUid};
						
						List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
						
						if (!lstResult.isEmpty()) {
							Operation thisOperation = (Operation) lstResult.get(0);
							writer.startElement("Operation");
							writer.addElement("operationUid", thisOperation.getOperationUid());
							writer.addElement("operationName",CommonUtil.getConfiguredInstance().getCompleteOperationName(UserSession.getInstance(request).getCurrentGroupUid(), thisOperation.getOperationUid()));
							writer.endElement();						
						}
					}
					
					if (this.selectedOperation != null){
						String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid ";
						String[] paramsFields = {"thisOperationUid"};
						String[] paramsValues = {this.selectedOperation};
						
						List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
						
						if (!lstResult.isEmpty()) {
							if (lstResult.size() > 0){
								for(Object reportdaily: lstResult){
									ReportDaily thisReportDaily = (ReportDaily) reportdaily;
									writer.startElement("Daily");
									writer.addElement("dailyUid", thisReportDaily.getDailyUid());
									writer.addElement("dayDate", flexDateFormater.format(thisReportDaily.getReportDatetime()));
									writer.addElement("dayNumber", thisReportDaily.getReportNumber());
									writer.addElement("rigInformationUid", thisReportDaily.getRigInformationUid());
									writer.addElement("operationUid", thisReportDaily.getOperationUid());
									writer.addElement("operationName",CommonUtil.getConfiguredInstance().getCompleteOperationName(UserSession.getInstance(request).getCurrentGroupUid(), thisReportDaily.getOperationUid()));
									writer.endElement();
								}
							}							
						}
					}	

					writer.endElement();
					writer.close();
				}
			}
			else { //default carry forward
				
				list = this.loadAvailableCarryForwardOtherOperationOptions(request, null);
				if (!list.isEmpty()){	
					
					SimpleXmlWriter writer = new SimpleXmlWriter(response);
					writer.startElement("root");
					
					for (Object selectionList : list){
						String thisDailyUid = selectionList.toString();
						
						String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND dailyUid = :thisDailyUid AND reportType!='DGR'";
						String[] paramsFields = {"thisDailyUid"};
						String[] paramsValues = {thisDailyUid};
						
						if (!this.carryDataFromAllOperations){
							String extra = " AND reportType='" + this.defaultReportType + "' ";

							strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND dailyUid = :thisDailyUid " + extra + "";
						}
						
						List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
						
						//clear daily object if no report daily tie to it anymore
						if (!lstResult.isEmpty()) {
							ReportDaily thisReportDaily = (ReportDaily) lstResult.get(0);
							writer.startElement("Daily");
							writer.addElement("dailyUid", thisReportDaily.getDailyUid());
							writer.addElement("dayDate", flexDateFormater.format(thisReportDaily.getReportDatetime()));
							writer.addElement("dayNumber", thisReportDaily.getReportNumber());
							writer.addElement("rigInformationUid", thisReportDaily.getRigInformationUid());
							writer.addElement("operationUid", thisReportDaily.getOperationUid());
							writer.addElement("operationName",CommonUtil.getConfiguredInstance().getCompleteOperationName(UserSession.getInstance(request).getCurrentGroupUid(), thisReportDaily.getOperationUid()));
							writer.endElement();						
						}
					}
					writer.endElement();
					writer.close();
				}
			}				
		}
		
		if ("validatePassword".equalsIgnoreCase(invocationKey))
		{
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			writer.addElement("status",ReportDailyUtils.validatePassword(request)?"1":"");
			writer.endElement();
			writer.close();
		}
		return;
	}
	
	/**
	 * This function is use to get options available to be copy from
	 * @param Request
	 * @return list of dailyUid, rigInformationUid, operationUid from other operation to be able to carry from
	 * @throws Exception
	 */	
	private ArrayList loadAvailableCarryForwardOtherOperationOptions(HttpServletRequest request, String copyFromLastXDaysWhenStartNewDay) throws Exception{
		Date newDayDate = CommonUtil.getConfiguredInstance().currentDateTime(UserSession.getInstance(request).getCurrentGroupUid(), "");
		double pickCriteria = Double.parseDouble(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "copyFromLastXDaysWhenStartNewDay"));	
		
		if (copyFromLastXDaysWhenStartNewDay != null) pickCriteria = Double.parseDouble(copyFromLastXDaysWhenStartNewDay);	
		
		ArrayList list = new ArrayList();
		
		//get the number of days backward to form pick criteria
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTime(newDayDate);
		thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
		thisCalendar.set(Calendar.MINUTE, 0);
		thisCalendar.set(Calendar.SECOND , 0);
		thisCalendar.set(Calendar.MILLISECOND , 0);
		
		Date toDate = thisCalendar.getTime();
		
		thisCalendar.add(Calendar.DAY_OF_MONTH, ((int) Math.round(pickCriteria) * -1));
		Date fromDate = thisCalendar.getTime();
		
		//get dailyUid with Report Date
		String[] paramsFields = {"fromDate", "toDate"};
		Object[] paramsValues = new Object[2]; paramsValues[0] = fromDate; paramsValues[1] = toDate;
		String strSql = "SELECT operationUid FROM Daily WHERE (isDeleted = false or isDeleted is null) AND (dayDate >= :fromDate AND dayDate <= :toDate) GROUP BY operationUid";
		
		// 18161: Requested change to include operations from other operation types
		/*
		if (copyFromLastXDaysWhenStartNewDay != null){
			String extra = "";
			Operation thisOperation = UserSession.getInstance(request).getCurrentOperation();
			if (thisOperation.getOperationCode()!=null){
				extra =" AND op.operationCode IN ('" + thisOperation.getOperationCode() + "') ";
			}
			strSql = "SELECT d.operationUid FROM Daily d, Operation op WHERE (d.isDeleted = false or d.isDeleted is null) AND (op.isDeleted = false or op.isDeleted is null) AND d.operationUid=op.operationUid " + extra + " AND (d.dayDate >= :fromDate AND d.dayDate <= :toDate) GROUP BY d.operationUid";
		}*/
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		
		//get all the user allowed to access operation
		Operations accessibleOperations = UserSession.getInstance(request).getCachedAllAccessibleOperations();
		
		if (!lstResult.isEmpty()){					
			for (Object effectedOps : lstResult){
				String thisOpUid = (String) effectedOps;
				String thisDailyUid;
				
				//check if this operation is accessible by user
				if (accessibleOperations.containsKey(thisOpUid)){
					
					if (copyFromLastXDaysWhenStartNewDay != null){
						list.add(thisOpUid);
					}
					else {
						thisDailyUid = ApplicationUtils.getConfiguredInstance().getLastDailyUidInOperation(thisOpUid);
						list.add(thisDailyUid);	
					}
								
				}			
			}
		}
		
		return list;		
	}

	public void init(CommandBean commandBean){
	}
	
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		String dailyUid = userSelection.getDailyUid();
		
		if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CALCULATE_DAYS_SINCE_SPUD_WELL))){
			ReportDailyUtils.autoPopulateDaySinceSpud(dailyUid, commandBean);
		}

	}
	protected String getSQLBasedOnMeta(TreeModelDataDefinitionMeta meta, UserSession session)throws Exception
	{
		return "FROM " + meta.getTableClass().getName() + " WHERE (isDeleted = false or isDeleted is null) and dailyUid= :lastdayUid";
	}
	
	protected Boolean allowCarryOverModuleDataByRig(BaseCommandBean thisBean, TreeModelDataDefinitionMeta meta, String[] paramsFields, Object[] paramsValues, UserSession session, Object dayDate) throws Exception
	{
		String strSql = this.getSQLBasedOnMeta(meta,session);
		
		if (meta.getTableClass().getName().equalsIgnoreCase("com.idsdatanet.d2.core.model.RigStock"))
		{
			if (thisBean.getBeanName().equalsIgnoreCase("rigStockCommandBean")){
				strSql = strSql + " AND (supportVesselInformationUid is null or supportVesselInformationUid='') AND (type='rigbulkstock' OR type = '' OR type is NULL)";
			}
			//need extra logic to differentiate fluid stock
			else if (thisBean.getBeanName().equalsIgnoreCase("fluidStockCommandBean")){
				strSql = strSql + " AND (supportVesselInformationUid is null or supportVesselInformationUid='') AND (type='fluids')";
			}
		}							
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			boolean allowSelect = true;
			
			Date newDate = null;
			
			if(dayDate instanceof java.util.Date){
				newDate = (Date) dayDate;
			}else{
				newDate = CommonDateParser.parse(dayDate.toString());
			}
			
			//Check rig pump if already removed
			if (meta.getTableClass().getName().equalsIgnoreCase("com.idsdatanet.d2.core.model.RigPumpParam"))
			{
				allowSelect = false;
				for(Object objResult: lstResult)
				{
					RigPumpParam rigPumpParam = (RigPumpParam) objResult;
					String rigPumpUid = rigPumpParam.getRigPumpUid();
					strSql = "FROM RigPump WHERE (isDeleted = false or isDeleted is null) AND rigPumpUid =:rigPumpUid and ((installDate <=:dayDate and removeDate >=:dayDate) or (installDate is null and removeDate is null) or (installDate <=:dayDate and removeDate is null) or (installDate is null and removeDate >=:dayDate))";
					List lstTmpResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"rigPumpUid", "dayDate"}, new Object[] {rigPumpUid, newDate});
					if (lstTmpResult.size() > 0) {
							allowSelect =  true;												
					}
				}
							
			}
			return allowSelect;
		}
		return false;
	}
	
	protected Boolean allowCarryOverModuleDataByOperation(BaseCommandBean thisBean, TreeModelDataDefinitionMeta meta, String[] paramsFields, Object[] paramsValues, UserSession session) throws Exception
	{
		
			String strSql = this.getSQLBasedOnMeta(meta,session);
			
			//need to add extra condition for rig stock because of table are shared for rig stock and support vessel stock
			if (meta.getTableClass().getName().equalsIgnoreCase("com.idsdatanet.d2.core.model.RigStock"))
			{
				if (thisBean.getBeanName().equalsIgnoreCase("rigStockCommandBean")){
					strSql = strSql + " AND (supportVesselInformationUid is null or supportVesselInformationUid='') AND (type='rigbulkstock' OR type = '' OR type is NULL)";
				}
				//need extra logic to differentiate fluid stock
				else if (thisBean.getBeanName().equalsIgnoreCase("fluidStockCommandBean")){
					strSql = strSql + " AND (supportVesselInformationUid is null or supportVesselInformationUid='') AND (type='fluids')";
				}
			}
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			if (lstResult.size() > 0){
				
				boolean allowSelect = true;
				
				//GWP: add extra condition for BHA run, where bharun that have date out should not be able to copy paste from yesterday
				if ("0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "allowCopyBHAWhenGotDateOut")))
				{
					
					if (meta.getTableClass().getName().equalsIgnoreCase("com.idsdatanet.d2.core.model.BharunDailySummary"))
					{
						allowSelect = false;
						for(Object objResult: lstResult)
						{
							BharunDailySummary thisBhaDailySummary = (BharunDailySummary) objResult;
							String thisBharunUid = thisBhaDailySummary.getBharunUid();
							strSql = "FROM Bharun WHERE (isDeleted = false or isDeleted is null) AND bharunUid =:BharunUid";
							List lstTmpResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", thisBharunUid);
							if (lstTmpResult.size() > 0) {
								Bharun thisBharun = (Bharun) lstTmpResult.get(0);
								//IF HAVE RUN WITH NO DATEOUT, DOESN'T MATTER WHICH RUN, JUST SHOW OPTION 
								if (thisBharun.getDailyidOut() == null || thisBharun.getDailyidOut().equalsIgnoreCase("")) 
								{
									allowSelect =  true;
								}
																				
							}
						}
									
					}
				}
				
				return allowSelect;
			}
		
		return false;
	}
}
