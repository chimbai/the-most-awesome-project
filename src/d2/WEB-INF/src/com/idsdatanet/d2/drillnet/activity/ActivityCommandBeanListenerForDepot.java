package com.idsdatanet.d2.drillnet.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.w3c.dom.Node;

import com.ibm.icu.util.Calendar;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DepotWellOperationMapping;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.LogCurveDefinition;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.SchedulerTemplate;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.core.DepotConstants;
import com.idsdatanet.depot.core.DepotContext;
import com.idsdatanet.depot.core.mapping.DepotMappingModule;
import com.idsdatanet.depot.core.mapping.MappingAggregator;
import com.idsdatanet.depot.core.query.QueryResult;
import com.idsdatanet.depot.core.soap.SoapQuery;
import com.idsdatanet.depot.core.soap.SoapQueryDelegate;
import com.idsdatanet.depot.core.util.DepotUtils;
import com.idsdatanet.depot.witsml.dataobject.log.LogCurveDataSource;
import com.idsdatanet.depot.witsml.dataobject.log.LogDataBean;

public class ActivityCommandBeanListenerForDepot extends EmptyCommandBeanListener implements ApplicationContextAware {
	public class WitsmlLogCurve {
		SchedulerTemplate template;
		LogCurveDefinition logCurveDef = null;
		DepotWellOperationMapping wellMapping = null;
		LogCurveDataSource dataSource = null;
		
		public WitsmlLogCurve(LogCurveDefinition logCurveDef, DepotWellOperationMapping dwom, LogCurveDataSource dataSource, SchedulerTemplate template) throws Exception {
			this.logCurveDef = logCurveDef;
			this.dataSource = dataSource;
			this.dataSource.setLogCurveDefinition(logCurveDef);
			
			this.wellMapping = dwom;
			this.template = template;
		}
		
		public LogCurveDefinition getLogCurveDef() {
			return logCurveDef;
		}
		
		public void setLogCurveDef(LogCurveDefinition logCurveDef) {
			this.logCurveDef = logCurveDef;
		}
		
		public DepotWellOperationMapping getWellMapping() {
			return wellMapping;
		}
		
		public void setWellMapping(DepotWellOperationMapping wellMapping) {
			this.wellMapping = wellMapping;
		}

		public LogCurveDataSource getDataSource() {
			return dataSource;
		}

		public void setDataSource(LogCurveDataSource dataSource) {
			this.dataSource = dataSource;
		}

		public SchedulerTemplate getTemplate() {
			return template;
		}

		public void setTemplate(SchedulerTemplate template) {
			this.template = template;
		}		
	}
	
	private ApplicationContext applicationContext;
	
	public void setApplicationContext(ApplicationContext appContext) {
		this.applicationContext = appContext;
	}
	
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		String witsmlImportKey = request.getParameter("witsmlImportKey");
		if (StringUtils.isNotBlank(witsmlImportKey) && StringUtils.equals(witsmlImportKey, "LogActivityTDE")) {
			String importParameters = request.getParameter("importParameters");
			if (StringUtils.isNotBlank(importParameters)) {
				String[] args = importParameters.split("[.]");
				String schedulerTemplateUid = args[0];
				String dwomUid = args[1];
				String logCurveUid = args[2];
				
				SchedulerTemplate schedulerTemplate = (SchedulerTemplate)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SchedulerTemplate.class, schedulerTemplateUid);
				DepotWellOperationMapping dwom = (DepotWellOperationMapping)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(DepotWellOperationMapping.class, dwomUid);
				LogCurveDefinition logCurveDef = (LogCurveDefinition)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LogCurveDefinition.class, logCurveUid);
				LogCurveDataSource dataSource = (LogCurveDataSource)this.getApplicationContext().getBean(logCurveDef.getLogSoapQueryId());
				commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
				this.queryWitsmlLogAndPopulateToScreen(new WitsmlLogCurve(logCurveDef, dwom, dataSource, schedulerTemplate), commandBean, UserSession.getInstance(request));
			} else {
				commandBean.getSystemMessage().addError("Import Parameters are not configured.");
			}
		}
	}

	private void queryWitsmlLogAndPopulateToScreen(WitsmlLogCurve witsmlLogCurve, CommandBean commandBean, UserSession session) throws Exception {
		SchedulerTemplate schedulerTemplate = witsmlLogCurve.getTemplate();
		DepotWellOperationMapping dwom = witsmlLogCurve.getWellMapping();
		LogCurveDataSource dataSource = witsmlLogCurve.getDataSource();
		ImportExportServerProperties serverProperties = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, schedulerTemplate.getImportExportServerPropertiesUid());
		MappingAggregator mappingAggregator = DepotMappingModule.getConfiguredInstance().getMappingAggregator(DepotConstants.WITSML, dataSource.getMappingAggregatorId());
		
		QueryResult qResult = new QueryResult();
		qResult.setDepotType(DepotConstants.WITSML);
		qResult.setDepotVersion(mappingAggregator.getStoreVersion());
		qResult.setTransType(DepotConstants.GET_FROM_STORE);
		qResult.setClientId(DepotUtils.SCHEDULER_USER_AGENT);
		qResult.setDepotObjectId(mappingAggregator.getDataObjectId());
		
		DepotContext depotContext = new DepotContext(null, mappingAggregator.getStoreVersion(), mappingAggregator.getDefaultNamespace());
		depotContext.setWellUid(dwom.getDepotWellUid());
		depotContext.setWellboreUid(dwom.getDepotWellboreUid());
		depotContext.setOperationUid(session.getCurrentOperationUid());
		depotContext.setDailyUid(session.getCurrentDailyUid());
		
		if (dataSource instanceof SoapQuery) {
			final List<Object> records = new ArrayList<Object>();
			SoapQuery soapQuery = (SoapQuery)dataSource;
			soapQuery.setJobProperties(serverProperties, mappingAggregator, dwom, session.getCurrentDaily().getDayDate(), depotContext, schedulerTemplate.getSchedulerType(), mappingAggregator.getDataObjectId());
			soapQuery.invoke(qResult, new SoapQueryDelegate() {
				public void beforeDataSave(Object tdeData) {
					if(tdeData instanceof List<?>){
						List<Object> dataBean = (List<Object>)tdeData;
						for(Object obj : dataBean){
							if(obj != null) records.add(obj);
						}
					}
				}
				public void afterDataSave(Object activity) {
					// Do nothing
				}
				public boolean canSave() {
					return false;
				}
			});
			if (qResult.isSuccessful()) {
				if (records.size() > 0) {
					String sql = "FROM Activity WHERE dailyUid=:dailyUid AND (isDeleted IS NULL or isDeleted = false) AND dayPlus != 1 ORDER BY startDatetime";
					List<Activity> activityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "dailyUid", session.getCurrentDailyUid());
					long lastSavedStartDatetime = activityList.size() > 0 ? activityList.get(activityList.size()-1).getStartDatetime().getTime() : 0;
					long lastSavedEndDatetime = activityList.size() > 0 ? activityList.get(activityList.size()-1).getEndDatetime().getTime() : 0;
					String lastSavedUid = activityList.size() > 0 ? activityList.get(activityList.size()-1).getActivityUid() : null;
					
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					String startTime = null;
					String endTime = null;
//					Double prevDepth = 0.0;
//					Daily prevDay = ApplicationUtils.getConfiguredInstance().getYesterday(session.getCurrentOperationUid(), session.getCurrentDailyUid());
//					if(prevDay != null){
//						List<Activity> prevDayActivityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "dailyUid", prevDay.getDailyUid());
//						prevDepth = prevDayActivityList.size() > 0 ? prevDayActivityList.get(prevDayActivityList.size()-1).getDepthMdMsl() : 0.0;
//					}
					for (Object activity : records) {
						if(activity instanceof Activity){
							Activity act = (Activity)activity;
							
							if(startTime == null){
								cal.setTimeInMillis(act.getStartDatetime().getTime());
				            	startTime = dateTimeFormat.format(cal.getTime());
							}
							cal.setTimeInMillis(act.getEndDatetime().getTime());
							endTime = dateTimeFormat.format(cal.getTime());
							
							if((act.getStartDatetime().getTime() == lastSavedStartDatetime) && (act.getEndDatetime().getTime() > lastSavedEndDatetime)){
								CommandBeanTreeNodeImpl node = (CommandBeanTreeNodeImpl)commandBean.getRoot();
								for(Iterator it=node.getList().values().iterator(); it.hasNext();) {
									Map items = (Map)it.next();
									for(Iterator data=items.values().iterator(); data.hasNext();) {
										CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) data.next();
										Object childObj = childNode.getData();
										
										if(childObj instanceof Activity) {
											if(((Activity) childObj).getActivityUid().equalsIgnoreCase(lastSavedUid)){
//												prevDepth = prevDepth + act.getDepthMdMsl();
												childNode.getAtts().setAction(Action.SAVE);
												childNode.getAtts().setEditMode(true);
												((Activity) childObj).setEndDatetime(act.getEndDatetime());
												((Activity) childObj).setActivityDescription(act.getActivityDescription());
												((Activity) childObj).setDepthMdMsl(act.getDepthMdMsl());
												childNode.getCommandBean().getFlexClientControl().setReloadAfterPageCancel();
											}
										}
									}
								}
	            			}
							else if((act.getStartDatetime().getTime() > lastSavedStartDatetime) && (act.getEndDatetime().getTime() > lastSavedEndDatetime)){
								//prevDepth = prevDepth + act.getDepthMdMsl();
								//act.setDepthMdMsl(prevDepth);
								CommandBeanTreeNodeImpl node = (CommandBeanTreeNodeImpl)commandBean.getRoot().addCustomNewChildNodeForInput(act);
								node.getAtts().setAction(Action.SAVE);
								node.getAtts().setEditMode(true);
								node.setNewlyCreated(true);
								node.setDirty(true);
								node.getAtts().setFocusFlag(true);
							}
						}
						if(activity instanceof NextDayActivity){
							NextDayActivity nextDay = (NextDayActivity)activity;
							cal.setTimeInMillis(nextDay.getEndDatetime().getTime());
							endTime = dateTimeFormat.format(cal.getTime());
							//prevDepth = prevDepth + nextDay.getDepthMdMsl();
							//nextDay.setDepthMdMsl(prevDepth);
							CommandBeanTreeNodeImpl node = (CommandBeanTreeNodeImpl)commandBean.getRoot().addCustomNewChildNodeForInput(nextDay);
							node.getAtts().setAction(Action.SAVE);
							node.getAtts().setEditMode(true);
							node.setNewlyCreated(true);
							node.setDirty(true);
							node.getAtts().setFocusFlag(true);
						}
					}
					commandBean.getSystemMessage().addInfo("Data from " + startTime + " to " + endTime + " is processed.");
				} else {
					commandBean.getSystemMessage().addWarning("No data available on " + serverProperties.getServiceName());
				}
			} else {
				commandBean.getSystemMessage().addError("Failed to query data on " + serverProperties.getServiceName() + " with errors: " + qResult.getAllMessages());
			}
		} else {
			commandBean.getSystemMessage().addError("Log Curve Data Source is not an instance of SoapQuery.");
		}
	}
}
