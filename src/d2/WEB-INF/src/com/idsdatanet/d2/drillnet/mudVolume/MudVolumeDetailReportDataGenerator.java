package com.idsdatanet.d2.drillnet.mudVolume;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.MudVolumeDetails;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


public class MudVolumeDetailReportDataGenerator implements ReportDataGenerator {
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
			
		String dailyUid = null;
		
		if (StringUtils.isNotBlank(userContext.getUserSelection().getDailyUid())){
			dailyUid = userContext.getUserSelection().getDailyUid();
		}
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		
		if (daily == null) return;
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Map<String, LookupItem> lookupList = null;
		
		String uri = "xml://mudvolumedetail.label_lookup?key=code&value=label";
		CascadeLookupSet[] cascadeLookupSet = LookupManager.getConfiguredInstance().getCompleteCascadeLookupSet(uri,userContext.getUserSelection());
	
		for(CascadeLookupSet cascadeLookup : cascadeLookupSet){
			
			lookupList = cascadeLookup.getLookup();
			for(String value : lookupList.keySet()){
				
				ReportDataNode thisReportNode = reportDataNode.addChild(value);
				
				//get daily total volume by label
				String[] paramsFields = {"label", "dailyUid", "operationUid"};
				Object[] paramsValues = {value, daily.getDailyUid(), daily.getOperationUid()};
				
				String strSql = "select sum(mvd.volume) as totalVolume from MudVolumeDetails mvd, MudVolumes mv where (mvd.isDeleted = false or mvd.isDeleted is null) " +
						"and (mv.isDeleted = false or mv.isDeleted is null) and mvd.label =:label and mvd.mudVolumesUid = mv.mudVolumesUid and mv.dailyUid =:dailyUid and " +
						"mv.operationUid =:operationUid";
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				
				if (lstResult.size() > 0){
					Object a = lstResult.get(0);
					if (a !=null) {
						Double totalVolume = Double.parseDouble(a.toString());
						CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), MudVolumeDetails.class, "volume");
						thisConverter.setBaseValue(totalVolume);
						Double totalTodayVol = thisConverter.getConvertedValue();
						
						thisReportNode.addProperty("today", totalTodayVol.toString());
					}else {
						thisReportNode.addProperty("today", "");
					}
					
				}
				
				
				//get previous day total volume by label
				String[] paramsFields2 = {"label", "dayDate", "operationUid"};
				Object[] paramsValues2 = {value, daily.getDayDate(), daily.getOperationUid()};
				
				strSql = "select sum(mvd.volume) as totalVolume from MudVolumeDetails mvd, MudVolumes mv, Daily d where (mvd.isDeleted = false or mvd.isDeleted is null) " +
						"and (mv.isDeleted = false or mv.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and " +
						"mvd.label =:label and mvd.mudVolumesUid = mv.mudVolumesUid and mv.dailyUid = d.dailyUid and " +
						"mv.operationUid =:operationUid and d.dayDate <:dayDate";
				
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
				
				if (lstResult.size() > 0){
					Object a = lstResult.get(0);
					if (a !=null) {
						Double totalVolume = Double.parseDouble(a.toString());
						CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), MudVolumeDetails.class, "volume");
						thisConverter.setBaseValue(totalVolume);
						Double totalPreviousVol = thisConverter.getConvertedValue();
						
						thisReportNode.addProperty("previous_day", totalPreviousVol.toString());
					}else {
						thisReportNode.addProperty("previous_day", "");
					}
					
				}
	
			}
		}
		
	}
	

}
