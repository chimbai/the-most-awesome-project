package com.idsdatanet.d2.drillnet.operationinformationcenter;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.ReportFiles;

public class BaseReportList {
	private ReportFiles thisReportFiles;
	private String operationName;
		
	public void setData(ReportFiles reportFiles, String operationName){
		this.thisReportFiles = reportFiles;
		this.operationName = operationName;
	}
	
	public String getReportDisplayName(){
		if(StringUtils.isNotBlank(this.thisReportFiles.getDisplayName())) return this.thisReportFiles.getDisplayName();
		return this.operationName + "_" + this.thisReportFiles.getReportDayNumber() + ".pdf";
	}

	public String getFileExtension(){
		int i = this.thisReportFiles.getReportFile().lastIndexOf(".");
		if(i != -1){
			return this.thisReportFiles.getReportFile().substring(i + 1);
		}else{
			return null;
		}
	}
	
	public String getReportFilesUid(){
		return this.thisReportFiles.getReportFilesUid();
	}
	
	public Date getReportDate(){
		return this.thisReportFiles.getReportDayDate();
	}
	
	public String getReportType(){
		return this.thisReportFiles.getReportType();
	}
}
