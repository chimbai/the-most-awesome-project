package com.idsdatanet.d2.drillnet.rushmore;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Basin;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Platform;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RushmoreWells;
import com.idsdatanet.d2.core.model.RushmoreWellsCompletion;
import com.idsdatanet.d2.core.model.SandControl;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class RushmoreCPRDataNodeListener extends EmptyDataNodeListener {
	private String cprVersion = null;
	
	public void setCprVersion(String cprVersion) {
		this.cprVersion = cprVersion;
	}

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof RushmoreWellsCompletion) {
			node.getDynaAttr().put("rushmoreCprVersion", this.cprVersion);
		}
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof RushmoreWellsCompletion) {
			RushmoreWellsCompletion thisRushmoreWellsCompletion = (RushmoreWellsCompletion) object;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			//get current well object to auto fill data
			String wellUid = userSelection.getWellUid();
			String operationUid = userSelection.getOperationUid();
			
			//set default value
			thisRushmoreWellsCompletion.setWellPlatform("L");	
			thisRushmoreWellsCompletion.setProductionAfterTd("0");
			thisRushmoreWellsCompletion.setSandAcrossReservoir("0");
			thisRushmoreWellsCompletion.setProductionIs("0");
			thisRushmoreWellsCompletion.setSandInstalled("0");
			thisRushmoreWellsCompletion.setPerforatingIs("0");
			thisRushmoreWellsCompletion.setStimulatedIs("0");
			thisRushmoreWellsCompletion.setSubseaIs("0");
			thisRushmoreWellsCompletion.setOtherIs("0");
			thisRushmoreWellsCompletion.setConcurrentIs("0");
			thisRushmoreWellsCompletion.setUseNewTechnology("0");
			
			thisRushmoreWellsCompletion.setWellIsIntelligent("1");
			thisRushmoreWellsCompletion.setWellIntelligentFunction("FF");
			thisRushmoreWellsCompletion.setWellStringType("P");
			thisRushmoreWellsCompletion.setProductionCasingRunIsCemented("1");
			thisRushmoreWellsCompletion.setTypeOfLinerIsolation("C");
			thisRushmoreWellsCompletion.setCompletionTripNum(1.0);
			thisRushmoreWellsCompletion.setCompletionDownHoleMonitor("L");
			thisRushmoreWellsCompletion.setReservoirDownHoleMonitor("0");
			thisRushmoreWellsCompletion.setProductionStringEquipment("D");
			thisRushmoreWellsCompletion.setSandControlEquipment("D");
			thisRushmoreWellsCompletion.setRunCompletionEquipment("D");
			thisRushmoreWellsCompletion.setXmasTreeEquipment("D");
			thisRushmoreWellsCompletion.setPerforatingEquipment("D");
			thisRushmoreWellsCompletion.setStimulatedEquipment("D");
			thisRushmoreWellsCompletion.setOtherEquipment("D");
			thisRushmoreWellsCompletion.setConcurrentSameWell("0");
			thisRushmoreWellsCompletion.setConcurrentDifferentWell("0");
			thisRushmoreWellsCompletion.setCostFinalCost("F");		
			
			
			String strSql = "FROM Well WHERE (isDeleted = false or isDeleted is null) AND wellUid = :thisWellUid";			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisWellUid", wellUid);
			String basin, field = "";
			if (lstResult.size() > 0){
				Well thisWell = (Well) lstResult.get(0);			
				thisRushmoreWellsCompletion.setCountry(thisWell.getCountry());						//get Country value
				thisRushmoreWellsCompletion.setBlockNo((String)thisWell.getBlock());				//get Block Number value
				thisRushmoreWellsCompletion.setFormalName(thisWell.getWellName());					//get Official or formal well name value
				thisRushmoreWellsCompletion.setCommonName(thisWell.getWellName());					//get 'In house' or common well name value
													
				String PlatformUid = thisWell.getPlatformUid();
				String strSqlPlat = "FROM Platform WHERE (isDeleted = false or isDeleted is null) AND platformUid = :PlatformUid";			
				List ResultPlat = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlPlat, "PlatformUid", PlatformUid);
				if (ResultPlat.size() > 0){
					Platform thisPlatform = (Platform) ResultPlat.get(0);
					thisRushmoreWellsCompletion.setPlatformName(thisPlatform.getPlatformName());	//get Platform value
				}	
				
				basin = thisWell.getBasin();
				field = thisWell.getField();
				String basinSql = "FROM Basin WHERE (isDeleted = false or isDeleted is null) AND basinUid = :thisBasinUid";			
				List basinResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(basinSql, "thisBasinUid", basin);					
				if (basinResult.size() > 0){
					Basin thisBasin = (Basin) basinResult.get(0);
					basin = thisBasin.getName();
				}
				
				if (field != "" && basin==""){
					thisRushmoreWellsCompletion.setFieldName(field);
				} else if (field=="" && basin != ""){
					thisRushmoreWellsCompletion.setFieldName(basin);
				} else if (field != "" && basin != ""){
					thisRushmoreWellsCompletion.setFieldName(field + " or " + basin);
				} else if (field=="" && basin==""){
					thisRushmoreWellsCompletion.setFieldName("N/A");								//get Basin/Field value
				}
				
				thisRushmoreWellsCompletion.setWellWaterDepthMsl(thisWell.getWaterDepth());			//get Water Depth value 				
			}
			
			CustomFieldUom thisUnitConverter = new CustomFieldUom(commandBean, ReportDaily.class, "depthMdMsl");			
			String activityDepthUnit = thisUnitConverter.getUOMMapping().getUnitID();
			
			if (activityDepthUnit.equals("Metre")){
				thisRushmoreWellsCompletion.setWellUnitOfMeasurement("M");
			} else if (activityDepthUnit.equals("Feet")){
				thisRushmoreWellsCompletion.setWellUnitOfMeasurement("F");		// get Units of Measure value
			}
			
			/*String[] paramsFields = {"thisTableName","thisFieldName"};
			Object[] paramsValues = {"report_daily", "depth_md_msl"};
			String strSql2 = "SELECT unitMappingUid FROM UnitMapping WHERE (isDeleted = false or isDeleted is null) AND tableName = :thisTableName AND fieldName = :thisFieldName";			
			List Result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);			
			String depthUnit = "";
			if (Result2.size() > 0){
				
				Object thisDepthUnit = Result2.get(0);
				depthUnit = thisDepthUnit.toString();	
				
				String strSql3 = "SELECT unitUid from UomTemplateMapping where uomTemplateMappingUid= :thisDepthUnit AND (isDeleted = false or isDeleted is null)";			
				List Result3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, "thisDepthUnit", depthUnit);			
				String activityDepthUnit = "";
				if (Result3.size() > 0){
					
					Object thisDepthUomTemplateMapping = Result3.get(0);
					activityDepthUnit = thisDepthUomTemplateMapping.toString();
					
					if (activityDepthUnit.equals("Metre")){
						thisRushmoreWellsCompletion.setWellUnitOfMeasurement("M");
					} else if (activityDepthUnit.equals("Feet")){
						thisRushmoreWellsCompletion.setWellUnitOfMeasurement("F");		// get Units of Measure value
					}
				}
			}*/
			
			String strSql4 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid ORDER BY reportDatetime DESC";			
			List Result4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, "thisOperationUid", operationUid, qp);			
			CustomFieldUom mtdConverter,tvdConverter ;
			
			if (Result4.size() > 0){
				ReportDaily thisReportDaily = (ReportDaily) Result4.get(0);
				
				if (thisReportDaily.getDepthMdMsl()!=null){
					mtdConverter = new CustomFieldUom(commandBean, RushmoreWellsCompletion.class, "wellMeasuredTdMsl");				
					mtdConverter.setBaseValue(thisReportDaily.getDepthMdMsl());
					thisRushmoreWellsCompletion.setWellMeasuredTdMsl(mtdConverter.getConvertedValue());	//get MTD value	
				} else if (thisReportDaily.getLastMdMsl() != null){
					mtdConverter = new CustomFieldUom(commandBean, RushmoreWellsCompletion.class, "wellMeasuredTdMsl");				
					mtdConverter.setBaseValue(thisReportDaily.getLastMdMsl());
					thisRushmoreWellsCompletion.setWellMeasuredTdMsl(mtdConverter.getConvertedValue());	//get MTD value	
				}
				
				if (thisReportDaily.getDepthTvdMsl()!=null){
					tvdConverter = new CustomFieldUom(commandBean, RushmoreWellsCompletion.class, "wellTvdMsl");				
					tvdConverter.setBaseValue(thisReportDaily.getDepthTvdMsl());
					thisRushmoreWellsCompletion.setWellTvdMsl(tvdConverter.getConvertedValue());		//get TVD value
				} else if (thisReportDaily.getLastTvdMsl() != null){
					tvdConverter = new CustomFieldUom(commandBean, RushmoreWellsCompletion.class, "wellTvdMsl");				
					tvdConverter.setBaseValue(thisReportDaily.getLastTvdMsl());
					thisRushmoreWellsCompletion.setWellTvdMsl(tvdConverter.getConvertedValue());		//get TVD value
				}				
				
				
				
				
			}
			
			String strSql5 = "SELECT DISTINCT(mudType) as drillingFluidType FROM MudProperties WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid";
			List Result5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, "thisOperationUid", operationUid);
			String mudtype = "";
			if (!Result5.isEmpty())
			{
				Object thisMudProperties = Result5.get(0);
				if (thisMudProperties != null) {
					mudtype = thisMudProperties.toString();
					
					if (mudtype.equals("h2o")){
						thisRushmoreWellsCompletion.setWellFluidInHole("W");
					} else if (mudtype.equals("oil")){
						thisRushmoreWellsCompletion.setWellFluidInHole("O");
					} else if (mudtype.equals("sbm")){
						thisRushmoreWellsCompletion.setWellFluidInHole("S");
					} else if (mudtype.equals("ester")){
						thisRushmoreWellsCompletion.setWellFluidInHole("E");
					} else if (mudtype.equals("foam")){
						thisRushmoreWellsCompletion.setWellFluidInHole("F");
					} else if (mudtype.equals("air")){
						thisRushmoreWellsCompletion.setWellFluidInHole("A");
					} else if (mudtype.equals("oth")){
						thisRushmoreWellsCompletion.setWellFluidInHole("");			// get Fluid in hole prior to clean-up value
					}	
				}
			}
			thisUnitConverter.setReferenceMappingField(ReportDaily.class, "daycost");
			String costUnit = thisUnitConverter.getUOMMapping().getUnitID();
			
			if (costUnit.equals("USD")){
				thisRushmoreWellsCompletion.setCostCurrency("USD");
			} else if (costUnit.equals("NOK")){
				thisRushmoreWellsCompletion.setCostCurrency("NOK");
			} else if (costUnit.equals("GBP")){
				thisRushmoreWellsCompletion.setCostCurrency("GBP");
			} else if (costUnit.equals("EUR")){
				thisRushmoreWellsCompletion.setCostCurrency("EUR");					
			} else if (costUnit.equals("AUD")){
				thisRushmoreWellsCompletion.setCostCurrency("AUD");			// get currency used to report well costs 
			}
			
			/*String[] paramsFields2 = {"thisTableName","thisFieldName"};
			Object[] paramsValues2 = {"report_daily", "daycost"};
			String strSql6 = "SELECT unitMappingUid FROM UnitMapping WHERE (isDeleted = false or isDeleted is null) AND tableName = :thisTableName AND fieldName = :thisFieldName";			
			List Result6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramsFields2, paramsValues2);			
			String dayCostUnit = "";
			if (Result6.size() > 0){
				
				Object thisCostUnit = Result6.get(0);
				dayCostUnit = thisCostUnit.toString();	
				
				String strSql7 = "SELECT unitUid from UomTemplateMapping where uomTemplateMappingUid= :thisDayCostUnit AND (isDeleted = false or isDeleted is null)";			
				List Result7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7, "thisDayCostUnit", dayCostUnit);			
				String costUnit = "";
				if (Result7.size() > 0){
					
					Object thisDayCostUomTemplateMapping = Result7.get(0);
					
					if (thisDayCostUomTemplateMapping != null) {
						costUnit = thisDayCostUomTemplateMapping.toString();
						
						if (costUnit.equals("USD")){
							thisRushmoreWellsCompletion.setCostCurrency("USD");
						} else if (costUnit.equals("NOK")){
							thisRushmoreWellsCompletion.setCostCurrency("NOK");
						} else if (costUnit.equals("GBP")){
							thisRushmoreWellsCompletion.setCostCurrency("GBP");
						} else if (costUnit.equals("EUR")){
							thisRushmoreWellsCompletion.setCostCurrency("EUR");					
						} else if (costUnit.equals("AUD")){
							thisRushmoreWellsCompletion.setCostCurrency("AUD");			// get currency used to report well costs 
						}
					}
				}
			}*/
			
			//get current operation object to auto fill data					
			String strSql8 = "FROM Operation WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid";			
			List Result8 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql8, "thisOperationUid", operationUid);			
			if (Result8.size() > 0){
				Operation thisOperation = (Operation) Result8.get(0);
				thisRushmoreWellsCompletion.setWellDateFinish(thisOperation.getRigOffHireDate());	//get Date Finish value
				
				//get current rigImformation object to auto fill data	
				String rigInformationUid = thisOperation.getRigInformationUid();			
				String strSql11 = "FROM RigInformation WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid = :thisRigInformationUid";			
				List Result11 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql11, "thisRigInformationUid", rigInformationUid);			
				String rigType = "";
				if (Result11.size() > 0){
					thisRushmoreWellsCompletion.setSandAcrossReservoir("1");
					thisRushmoreWellsCompletion.setProductionIs("1");
					thisRushmoreWellsCompletion.setSandInstalled("1");
					thisRushmoreWellsCompletion.setPerforatingIs("1");
					thisRushmoreWellsCompletion.setStimulatedIs("1");
					thisRushmoreWellsCompletion.setSubseaIs("1");
					thisRushmoreWellsCompletion.setOtherIs("1");
					
					RigInformation thisRigInformation = (RigInformation) Result11.get(0);					
					rigType = thisRigInformation.getRigSubType();
					
					if ("jackup".equals(rigType)){
						thisRushmoreWellsCompletion.setProductionStringRig("JK");
						thisRushmoreWellsCompletion.setRunCompletionRig("JK");
						thisRushmoreWellsCompletion.setXmasTreeRig("JK");
						thisRushmoreWellsCompletion.setPerforatingRig("JK");
						thisRushmoreWellsCompletion.setStimulatedRig("JK");
						thisRushmoreWellsCompletion.setOtherRig("JK");
						thisRushmoreWellsCompletion.setSandControlRig("JK");
					} else if ("drillship".equals(rigType)){
						thisRushmoreWellsCompletion.setProductionStringRig("DS");
						thisRushmoreWellsCompletion.setRunCompletionRig("DS");
						thisRushmoreWellsCompletion.setXmasTreeRig("DS");
						thisRushmoreWellsCompletion.setPerforatingRig("DS");
						thisRushmoreWellsCompletion.setStimulatedRig("DS");
						thisRushmoreWellsCompletion.setOtherRig("DS");
						thisRushmoreWellsCompletion.setSandControlRig("DS");
					} else if ("platform".equals(rigType)){
						thisRushmoreWellsCompletion.setProductionStringRig("PL");
						thisRushmoreWellsCompletion.setRunCompletionRig("PL");
						thisRushmoreWellsCompletion.setXmasTreeRig("PL");
						thisRushmoreWellsCompletion.setPerforatingRig("PL");
						thisRushmoreWellsCompletion.setStimulatedRig("PL");
						thisRushmoreWellsCompletion.setOtherRig("PL");
						thisRushmoreWellsCompletion.setSandControlRig("PL");
					} else if ("submersible".equals(rigType)){
						thisRushmoreWellsCompletion.setProductionStringRig("SU");
						thisRushmoreWellsCompletion.setRunCompletionRig("SU");
						thisRushmoreWellsCompletion.setXmasTreeRig("SU");
						thisRushmoreWellsCompletion.setPerforatingRig("SU");
						thisRushmoreWellsCompletion.setStimulatedRig("SU");
						thisRushmoreWellsCompletion.setOtherRig("SU");
						thisRushmoreWellsCompletion.setSandControlRig("SU");
					} else if ("semisubmersible".equals(rigType)){
						thisRushmoreWellsCompletion.setProductionStringRig("SS");
						thisRushmoreWellsCompletion.setRunCompletionRig("SS");
						thisRushmoreWellsCompletion.setXmasTreeRig("SS");
						thisRushmoreWellsCompletion.setPerforatingRig("SS");
						thisRushmoreWellsCompletion.setStimulatedRig("SS");
						thisRushmoreWellsCompletion.setOtherRig("SS");
						thisRushmoreWellsCompletion.setSandControlRig("SS");
					} else {
						thisRushmoreWellsCompletion.setProductionStringRig("");	
						thisRushmoreWellsCompletion.setRunCompletionRig("");
						thisRushmoreWellsCompletion.setXmasTreeRig("");
						thisRushmoreWellsCompletion.setPerforatingRig("");
						thisRushmoreWellsCompletion.setStimulatedRig("");
						thisRushmoreWellsCompletion.setOtherRig("");
						thisRushmoreWellsCompletion.setSandControlRig("");			// get Rig Type value
					}	
				}				
			}
			
			String[] paramsFields3 = {"thisOperationUid", "thisPhaseCode1"};
			Object[] paramsValues3 = {operationUid, "PCO"};
			String strSql9 = "SELECT SUM(a.activityDuration) as workoverTotalTime FROM Activity a, Daily d " +
					"WHERE (a.isDeleted = false or a.isDeleted is null) " +
					"AND (d.isDeleted = false or d.isDeleted is null) " +
					"AND a.dailyUid = d.dailyUid " +
					"AND a.operationUid = :thisOperationUid " +
					"AND (a.phaseCode = :thisPhaseCode1) " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null)";
			List Result9 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql9, paramsFields3, paramsValues3, qp);
			CustomFieldUom thisConverter;			
			if (Result9.size() > 0)
			{
				Object thisActivity = Result9.get(0);
				if (thisActivity != null) {
					thisConverter = new CustomFieldUom(commandBean, RushmoreWells.class, "workoverTotalTime");
					thisConverter.setBaseValue(Double.parseDouble(thisActivity.toString()));
					thisRushmoreWellsCompletion.setWorkoverTotalTime(thisConverter.getConvertedValue());			// get Workover Total Time Days
				}
			}			
						
			String strSql10 = "FROM SandControl WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid ORDER BY screenOd DESC";
			List Result10 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql10, "thisOperationUid", operationUid, qp);
			CustomFieldUom sandConverter;
			String sandControlId = "";
			
			if (Result10.size() > 0)
			{
				thisRushmoreWellsCompletion.setSandAcrossReservoir("1");
				
				SandControl thisSandControl = (SandControl) Result10.get(0);
				sandControlId = thisSandControl.getSandControlUid();
				thisRushmoreWellsCompletion.setSandControlDescription(thisSandControl.getEquipment());		// get Sand Control Description
				
				if (thisSandControl.getScreenOd() != null) {
					sandConverter = new CustomFieldUom(commandBean, RushmoreWells.class, "sandControlScreenSize");				
					sandConverter.setBaseValue(thisSandControl.getScreenOd());
					thisRushmoreWellsCompletion.setSandControlScreenSize(sandConverter.getConvertedValue());			// get Screen Size
				}
				
				if (thisSandControl.getStringLength() != null) {
					sandConverter = new CustomFieldUom(commandBean, RushmoreWells.class, "sandControlStringLength");				
					sandConverter.setBaseValue(thisSandControl.getStringLength());
					thisRushmoreWellsCompletion.setSandControlStringLength(sandConverter.getConvertedValue());			// get Length of sand control string
				}
				
				if (thisSandControl.getScreenWeight() != null) {
					sandConverter = new CustomFieldUom(commandBean, RushmoreWells.class, "sandControlScreenWeight");				
					sandConverter.setBaseValue(thisSandControl.getScreenWeight());
					thisRushmoreWellsCompletion.setSandControlScreenWeight(sandConverter.getConvertedValue());			// get Screen Weight
				}
				
				String[] paramsFields4 = {"thisOperationUid", "thisSandControlUid"};
				Object[] paramsValues4 = {operationUid, sandControlId};
				String strSql11 = "FROM SandControlMaterial WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid AND sandControlUid = :thisSandControlUid";
				List Result11 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql11, paramsFields4, paramsValues4);
				if (Result11.size() > 0)
				{
					for (Object rec:Result11) {
						String material = rec.toString();
						//(SandControl) this
						//System.out.println("---- here2 " + material);	
							
										
						//thisRushmoreWellsCompletion.setSandControlScreenMaterial();			// get sand material
					}
				}
			}
			
					
			//System.out.println("---- here2 ");				
			//Double daysOnOperation = CommonUtil.getConfiguredInstance().daysOnOperation(userSelection.getOperationUid(), userSelection.getDailyUid());
			
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof RushmoreWellsCompletion) {
			node.getDynaAttr().put("rushmoreCprVersion", this.cprVersion);
		}
	}
}
