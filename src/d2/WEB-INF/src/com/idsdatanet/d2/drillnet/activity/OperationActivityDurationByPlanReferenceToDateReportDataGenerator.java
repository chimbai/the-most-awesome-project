package com.idsdatanet.d2.drillnet.activity;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @author dlee
 * This class help to mine data for activity duration for the operation up to the selected date group by phase code
 * for daily reporting use
 * 
 * configuration is set in ddr.xml
 */

public class OperationActivityDurationByPlanReferenceToDateReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		String sequence = null;
		String phaseCode = "N/A";
		String phaseName = "N/A";
		String planName = "N/A";
		String planReferenceUid = "N/A";
		String planReferenceDescription = "N/A";
		
		Double pDuration = 0.0;
		Double tpDuration = 0.0;
		Double uDuration = 0.0;
		Double tuDuration = 0.0;
		Double totalDuration = 0.0;
		Double totalHours = 0.0;
		Double minDepth = 0.0;
		Double maxDepth = 0.0;
		Double meterage = 0.0;
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		// get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if(daily == null) return;
		Date todayDate = daily.getDayDate();
		
		String strSql = "SELECT a.planReference, a.phaseCode, MIN(a.depthFromMdMsl) AS minDepth, MAX(a.depthMdMsl) AS maxDepth, SUM(a.activityDuration) AS totalDuration, min(a.startDatetime) " + 
				"FROM Daily d, Activity a, OperationPlanPhase o WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) AND (o.isDeleted = false OR o.isDeleted IS NULL)" + 
				"AND a.planReference = o.operationPlanPhaseUid AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
				"NOT(a.phaseCode IS NULL OR a.phaseCode = '') AND (a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " + 
				"GROUP BY a.planReference, a.phaseCode ORDER BY min(a.startDatetime)";
		
		String[] paramsFields = {"userDate", "thisOperationUid"};
		Object[] paramsValues = {todayDate, userContext.getUserSelection().getOperationUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if(lstResult.size() > 0) {
			//unit converter to convert + format value
			CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
			
			for(Object objResult: lstResult) {
				Object[] obj = (Object[]) objResult;
				String thisPlanReference = obj[0].toString();
				String p_sql = "SELECT SUM(a.activityDuration) AS pDuration " + 
						"FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) " + 
						"AND a.classCode='P' AND a.planReference =:thisPlanReference AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
						"NOT(a.phaseCode IS NULL OR a.phaseCode = '') AND (a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) ";
				String tp_sql = "SELECT SUM(a.activityDuration) AS pDuration " + 
						"FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) " + 
						"AND a.classCode='TP' AND a.planReference =:thisPlanReference AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
						"NOT(a.phaseCode IS NULL OR a.phaseCode = '') AND (a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) ";
				String u_sql = "SELECT SUM(a.activityDuration) AS pDuration " + 
						"FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) " + 
						"AND a.classCode='U' AND a.planReference =:thisPlanReference AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
						"NOT(a.phaseCode IS NULL OR a.phaseCode = '') AND (a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) ";
				String tu_sql = "SELECT SUM(a.activityDuration) AS pDuration " + 
						"FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) " + 
						"AND a.classCode='TU' AND a.planReference =:thisPlanReference AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
						"NOT(a.phaseCode IS NULL OR a.phaseCode = '') AND (a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) ";
				
				String[] paramsArg = {"userDate", "thisOperationUid", "thisPlanReference"};
				Object[] paramsVal = {todayDate, userContext.getUserSelection().getOperationUid(), thisPlanReference};
				
				List p_result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(p_sql, paramsArg, paramsVal, qp);
				List tp_result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(tp_sql, paramsArg, paramsVal, qp);
				List u_result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(u_sql, paramsArg, paramsVal, qp);
				List tu_result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(tu_sql, paramsArg, paramsVal, qp);

				if(p_result.get(0) != null && StringUtils.isNotBlank(p_result.get(0).toString())) {
					pDuration = Double.parseDouble(p_result.get(0).toString());
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(pDuration);
					pDuration = thisConverter.getConvertedValue();
				}else {
					pDuration = 0.0;
				}

				if(tp_result.get(0) != null && StringUtils.isNotBlank(tp_result.get(0).toString())) {
					tpDuration = Double.parseDouble(tp_result.get(0).toString());
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(tpDuration);
					tpDuration = thisConverter.getConvertedValue();
				}else {
					tpDuration = 0.0;
				}

				if(u_result.get(0) != null && StringUtils.isNotBlank(u_result.get(0).toString())) {
					uDuration = Double.parseDouble(u_result.get(0).toString());
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(uDuration);
					uDuration = thisConverter.getConvertedValue();
				}else {
					uDuration = 0.0;
				}
				
				if(tu_result.get(0) != null && StringUtils.isNotBlank(tu_result.get(0).toString())) {
					tuDuration = Double.parseDouble(tu_result.get(0).toString());
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(tuDuration);
					tuDuration = thisConverter.getConvertedValue();
				}else {
					tuDuration = 0.0;
				}
				
				if(obj[0] != null) {
					if(StringUtils.isNotBlank(obj[0].toString())){
						planReferenceUid = obj[0].toString();
						String strSql2 = "SELECT sequence, phaseName FROM OperationPlanPhase WHERE (isDeleted is null OR isDeleted = false) AND operationPlanPhaseUid = :thisPlanReference AND operationUid = :thisOperationUid";
						String[] paramsFields2 = {"thisPlanReference", "thisOperationUid"};
						Object[] paramsValues2 = {planReferenceUid, userContext.getUserSelection().getOperationUid()};
						
						List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
					     if(lstResult2.size()>0) {
					      for(Object planReferenceResult: lstResult2) {
					    	  Object[] res = (Object[]) planReferenceResult;
						      if(res[1] != null) planReferenceDescription = res[1].toString();
						      if(res[0] != null) sequence = res[0].toString();
						      planName = sequence + " " + planReferenceDescription;
					      }
					     }
					}else {
						planReferenceUid = "";
						planName = "";
					}
				}
				
				if(obj[1] != null && StringUtils.isNotBlank(obj[1].toString())) {
					phaseCode = obj[1].toString();
					String strSql2 = "SELECT name FROM LookupPhaseCode WHERE (isDeleted is null OR isDeleted = false) AND shortCode = :thisPhaseCode AND (operationCode = :thisOperationType OR operationCode = '')";
					String[] paramsFields2 = {"thisPhaseCode", "thisOperationType"};
					Object[] paramsValues2 = {phaseCode, userContext.getUserSelection().getOperationType().toString()};
					
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				    if(lstResult2.size()>0) {
				      Object lookupPhaseCodeResult = (Object) lstResult2.get(0);
				      if(lookupPhaseCodeResult != null) phaseName = lookupPhaseCodeResult.toString();
				    }
				}
				
				if(obj[2] != null && StringUtils.isNotBlank(obj[2].toString())) {
					minDepth = Double.parseDouble(obj[2].toString());
					
					//assign unit to the value base on activity.depthMdMsl, this will do auto convert to user display unit + format
					thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");
					thisConverter.setBaseValue(minDepth);
					minDepth = thisConverter.getConvertedValue();
				}
				
				if(obj[3] != null && StringUtils.isNotBlank(obj[3].toString())) {
					maxDepth = Double.parseDouble(obj[3].toString());
					
					//assign unit to the value base on activity.depthMdMsl, this will do auto convert to user display unit + format
					thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");
					thisConverter.setBaseValue(maxDepth);
					maxDepth = thisConverter.getConvertedValue();
				}
				
				if(obj[4] != null && StringUtils.isNotBlank(obj[4].toString())) {
					totalDuration = Double.parseDouble(obj[4].toString());
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(totalDuration);
					totalDuration = thisConverter.getConvertedValue();
				}
				
				meterage = maxDepth - minDepth;
				totalHours = totalHours + totalDuration;
				reportDataNode.addProperty("totalHours", totalHours.toString());
				addToReportDataNode(reportDataNode, phaseCode, phaseName, planReferenceUid, planName, totalDuration, pDuration, tpDuration, uDuration, tuDuration, meterage, maxDepth, sequence);
			}
		}		
	}

	private void addToReportDataNode(ReportDataNode reportDataNode, String phase, String phaseName, String planReferenceUid, String planName, Double totalDuration, Double pDuration, Double tpDuration, Double uDuration, Double tuDuration, Double meterage, Double maxDepth, String sequence) {
		
		ReportDataNode thisReportNode = reportDataNode.addChild("code");
		thisReportNode.addProperty("name", phase);
		thisReportNode.addProperty("phaseName", phaseName);
		thisReportNode.addProperty("planReferenceUid", planReferenceUid);
		thisReportNode.addProperty("planReferenceDescription", planName);
		thisReportNode.addProperty("totalDuration", totalDuration.toString());
		thisReportNode.addProperty("totalPDuration", pDuration.toString());
		thisReportNode.addProperty("totalTPDuration", tpDuration.toString());
		thisReportNode.addProperty("totalUDuration", uDuration.toString());
		thisReportNode.addProperty("totalTUDuration", tuDuration.toString());
		thisReportNode.addProperty("meterage", meterage.toString());
		thisReportNode.addProperty("maxDepth", maxDepth.toString());
		thisReportNode.addProperty("sequence", sequence);
	}
}
