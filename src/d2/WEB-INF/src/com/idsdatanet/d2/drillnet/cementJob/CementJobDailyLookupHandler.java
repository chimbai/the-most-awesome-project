package com.idsdatanet.d2.drillnet.cementJob;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.CementJob;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class CementJobDailyLookupHandler implements LookupHandler{
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		UserSession session = UserSession.getInstance(request);
		String operationUid = session.getCurrentOperationUid();
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		List<CementJob> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM CementJob WHERE (isDeleted=false or isDeleted is null) AND operationUid=:operationUid order by jobNumber", "operationUid", operationUid);
		if (!list.isEmpty()){
			Collections.sort(list,new CementJobDailyComparator());
			for (CementJob rec : list) {			
				String dailyUid = rec.getDailyUid();
				//Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(key);
				ReportDaily rd = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(session, dailyUid);
				if (rd !=null){
					String value = "# " + rd.getReportNumber() + " (" + df.format(rd.getReportDatetime()) + ")";
					LookupItem item = new LookupItem(dailyUid, value);
					result.put(dailyUid, item);
				}
			}
		}
		return result;
	}
	
	private class CementJobDailyComparator implements Comparator<CementJob>{

		public int compare(CementJob cmtjob1, CementJob cmtjob2) {
			// TODO Auto-generated method stub
			try {
				Daily d1 = ApplicationUtils.getConfiguredInstance().getCachedDaily(cmtjob1.getDailyUid());
				Daily d2 = ApplicationUtils.getConfiguredInstance().getCachedDaily(cmtjob2.getDailyUid());
				return d1.getDayDate().compareTo(d2.getDayDate());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 0;
		}
		
	}
}
