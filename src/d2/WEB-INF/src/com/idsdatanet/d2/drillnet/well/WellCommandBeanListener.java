package com.idsdatanet.d2.drillnet.well;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.operation.OperationUtils;
import com.idsdatanet.depot.edm.sync.EDMSyncEngine;

public class WellCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {
	
	@Override
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		root.getDynaAttr().put("edmSyncEnabled", EDMSyncEngine.getConfiguredInstance().isEnabled());
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		
		if(targetCommandBeanTreeNode != null){
			if(Well.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())){
				String targetField = targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField();
				if("@copyFromOperationUid".equals(targetField)){
					this.populate(commandBean, targetCommandBeanTreeNode, request);
					if (commandBean.getFlexClientControl() != null) {
						commandBean.getFlexClientControl().setReloadAfterPageCancel();
					}
					
				} else if("@locationType".equals(targetField)){ //Bottom Hole Location
					targetCommandBeanTreeNode.getDynaAttr().put("uwiString", null);
					
					//dls field
					targetCommandBeanTreeNode.getDynaAttr().put("locationException", null);
					targetCommandBeanTreeNode.getDynaAttr().put("dlsLsd", null);
					targetCommandBeanTreeNode.getDynaAttr().put("dlsSection", null);
					targetCommandBeanTreeNode.getDynaAttr().put("dlsTownship", null);
					targetCommandBeanTreeNode.getDynaAttr().put("dlsRange", null);
					targetCommandBeanTreeNode.getDynaAttr().put("dlsMeridian", null);
					targetCommandBeanTreeNode.getDynaAttr().put("eventSequence", null);
					
					//nts field
					targetCommandBeanTreeNode.getDynaAttr().put("ntsQuarter", null);
					targetCommandBeanTreeNode.getDynaAttr().put("ntsUnit", null);
					targetCommandBeanTreeNode.getDynaAttr().put("ntsBlock", null);
					targetCommandBeanTreeNode.getDynaAttr().put("ntsMapNo", null);
					targetCommandBeanTreeNode.getDynaAttr().put("ntsMapLot", null);
					targetCommandBeanTreeNode.getDynaAttr().put("ntsMapPoint", null);
					
					//fps field
					targetCommandBeanTreeNode.getDynaAttr().put("fpsGrid", null);
					targetCommandBeanTreeNode.getDynaAttr().put("fpsSection", null);
					targetCommandBeanTreeNode.getDynaAttr().put("fpsLatitude", null);
					targetCommandBeanTreeNode.getDynaAttr().put("fpsLatitudeMin", null);
					targetCommandBeanTreeNode.getDynaAttr().put("fpsLongitude", null);
					targetCommandBeanTreeNode.getDynaAttr().put("fpsLongitudeMin", null);
					
				} else if("@locationTypeSurface".equals(targetField)){ //Surface Legal Location
					targetCommandBeanTreeNode.getDynaAttr().put("uwiStringSurface", null);
					
					//dls field
					targetCommandBeanTreeNode.getDynaAttr().put("locationExceptionSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("dlsLsdSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("dlsSectionSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("dlsTownshipSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("dlsRangeSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("dlsMeridianSurface", null);
					
					//nts field
					targetCommandBeanTreeNode.getDynaAttr().put("ntsQuarterSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("ntsUnitSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("ntsBlockSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("ntsMapNoSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("ntsMapLotSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("ntsMapPointSurface", null);
					
					//fps field
					targetCommandBeanTreeNode.getDynaAttr().put("fpsGridSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("fpsSectionSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("fpsLatitudeSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("fpsLatitudeMinSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("fpsLongitudeSurface", null);
					targetCommandBeanTreeNode.getDynaAttr().put("fpsLongitudeMinSurface", null);
				}
			}
		}
	}
	
	private void populate(CommandBean commandBean, CommandBeanTreeNode node, HttpServletRequest request) throws Exception {
		String copyFromOperationUid = (String) node.getDynaAttr().get("copyFromOperationUid");
		UserSession session = UserSession.getInstance(request);
		
		if ("_currentSelectedOperationUid".equals(copyFromOperationUid)) {
			// this is a marker set by custom add new button at flex side
			copyFromOperationUid = session.getCurrentOperationUid();
			node.getDynaAttr().put("showCopyFrom", "true");
		}
		
		//see if user select to copy data from another well or not, if Yes only take data from that well
		if (StringUtils.isNotBlank(copyFromOperationUid)) {
			copyDataFromWell(node, copyFromOperationUid);
			node.getDynaAttr().put("currentCopyFromOperationUid", copyFromOperationUid);
		}
		
		//set the copy from operation selection to null, so when page refresh copy from will not fire again
		node.getDynaAttr().put("copyFromOperationUid", null);
	}

	public static void copyDataFromWell(CommandBeanTreeNode node, String sourceOperationUid) throws Exception {
		if(node.getDataDefinition() == null) return;
		if(node.getDataDefinition().getTableClass() != Well.class) return;

		Operation selectedOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, sourceOperationUid);
		Wellbore parentWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, selectedOperation.getWellboreUid());
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, parentWellbore.getWellUid());

		Well newWell = (Well) BeanUtils.cloneBean(well);
		newWell.setWellUid(null);
		newWell.setIsWellEnd(false);
		node.setData(newWell);
		node.getNodeUserMessages().addFieldWarning("wellName", "A <b>Well</b> with this name already exist in the system.<br/>You can still proceed but this will create<br />duplicate wells of the same name in the system.");
		
		//refresh the node so every ui component is reloaded - if not the multi-select component is not activated
		node.refreshNodeAfterDataChangedProgrammatically();
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if (OperationUtils.CHECKING_WELL_NAME_EXIST.equals(invocationKey)) {
			OperationUtils.checkingWellNameExist(request, response);
		}
		return;
	}
}
