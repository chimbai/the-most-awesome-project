package com.idsdatanet.d2.drillnet.report.offwr;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.casingSection.CasingSectionDataNodeListener;

public class OffwrCasingSectionDataNodeListener extends CasingSectionDataNodeListener {
	
	@Override
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		super.afterDataNodeLoad(commandBean, meta, node, userSelection, request);
		Object object = node.getData();
		if (object instanceof CasingSection) {
			CasingSection casingSection = (CasingSection) object;
			
			// Get LeakOffTest.testType based on MAX ReportDaily.reportDatetime WHERE CasingSection.casingOd=LeakOffTest.casingSize
			QueryProperties qp = new QueryProperties();
			qp.setFetchFirstRowOnly();
			String strSql = "SELECT l.testType FROM ReportDaily rd, Daily d, CasingSection c, LeakOffTest l WHERE "
					+ "(rd.isDeleted = false or rd.isDeleted is null) AND (d.isDeleted = false or d.isDeleted is null) AND (c.isDeleted = false or c.isDeleted is null) AND (l.isDeleted = false or l.isDeleted is null) "
					+ "AND c.casingOd=l.casingSize AND d.dailyUid=l.dailyUid AND d.dailyUid=rd.dailyUid AND rd.reportType!='DGR' "
					+ "AND c.wellboreUid=:wellboreUid "
					+ "AND c.casingOd=:casingOd "
					+ "ORDER BY rd.reportDatetime DESC";		
			String[] paramsFields = {"wellboreUid", "casingOd"};
			Object[] paramsValues = {casingSection.getWellboreUid(), casingSection.getCasingOd()};
			List<String> testTypeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			if (!testTypeList.isEmpty()) {
				String testType = testTypeList.get(0);
				node.getDynaAttr().put("LeakOffTest.testType", testType);
			}
		}
	}
	
}
