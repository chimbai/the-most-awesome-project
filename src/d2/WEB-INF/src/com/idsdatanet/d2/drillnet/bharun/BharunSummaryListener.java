package com.idsdatanet.d2.drillnet.bharun;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.infra.uom.OperationUomAndDatumSelector;
import com.idsdatanet.d2.infra.uom.OperationUomContext;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;

public class BharunSummaryListener extends EmptyDataNodeListener implements DataNodeLoadHandler, DataNodeAllowedAction, ActionManager, CommandBeanListener{
	
	private static final String SHOW_ALL_WELLS = "showAllWells";
	private static final String SHOW_NO_DATE = "showNoDates";
	private static final String SHOW_NO_DEPTH = "showNoDepth";
	private static final String SHOW_NO_TOOL_TYPE = "showNoToolType";
	private static final String SHOW_NO_COMPANY = "showNoCompany";
	private static final String SHOW_INVALID_PROGRESS = "showInvalidProgress";
	
	
	
	private List<String> bhaCustomUOMFields = null;
	private List<String> bharunFields = null;
	
	public List<String> getBharunFields() {
		return bharunFields;
	}



	public void setBharunFields(List<String> bharunFields) {
		this.bharunFields = bharunFields;
	}



	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();

		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		
		if(object instanceof Bharun)
		{
			Bharun bharun = (Bharun) object;
			operationUomContext.setOperationUid(bharun.getOperationUid());

			this.setCustomUOM(commandBean, node,userSelection,operationUomContext);
			this.populateCompleteOperationName(commandBean,node, userSelection);
			this.loadBharunDailySummary(commandBean, node);
			
		}
		if (object instanceof BharunDailySummary)
		{
			Bharun bharun = (Bharun)node.getParent().getData();
			operationUomContext.setOperationUid(bharun.getOperationUid());
			this.setCustomUOM(commandBean, node,userSelection,operationUomContext);
		}
		
	
	}
	
	
	
	private void setCustomUOM(CommandBean commandBean, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, OperationUomContext operationUomContext) throws Exception {
		// TODO Auto-generated method stub
		if (this.getBhaCustomUOMFields() !=null && this.getBhaCustomUOMFields().size()>0)
		{
			Object obj = node.getData();
			for (String field : this.getBhaCustomUOMFields())
			{	
				String[] key = field.split("[.]");
				if (key[0].equalsIgnoreCase(obj.getClass().getSimpleName()))
				{
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Bharun.class, key[1],operationUomContext);
					node.setCustomUOM(key[1], thisConverter.getUOMMappingWithLocalDatum());
				}
			}
		}
		

	}



	private void populateCompleteOperationName(CommandBean commandBean,CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception {
		// TODO Auto-generated method stub
		//if (isTrue(commandBean.getRoot().getDynaAttr().get(SHOW_ALL_WELLS)))
			node.getDynaAttr().put("completeOperationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(),(String)node.getValue("operationUid")));
	}



	private void loadBharunDailySummary(CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
		// TODO Auto-generated method stub
		Bharun bharun = (Bharun) node.getData();
		String bharunUid = bharun.getBharunUid();
		String sql = "Select b.operationUid, bds From BharunDailySummary bds, Bharun b, Daily d Where" +
		"(bds.isDeleted is null or bds.isDeleted =False) and" +
		"(d.isDeleted is null or d.isDeleted =False) and" +
		"(b.isDeleted is null or b.isDeleted =False) and" +
		"(d.dailyUid=bds.dailyUid) and" +
		"(b.bharunUid=bds.bharunUid) and" +
		"(bds.bharunUid=:bharunUid) order by d.dayDate";
		QueryProperties qp = new QueryProperties();
		qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "bharunUid",bharunUid, qp);
		
		Double progress =null;
		Double duration =null;
		
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		operationUomContext.setOperationUid(bharun.getOperationUid());
		
		if (result.size()>0)
		{
			for (Object[] row : result)
			{
				BharunDailySummary bds = (BharunDailySummary) row[1];
				if (bds.getProgress()!=null)
				{
					if (progress!=null)
						progress+=bds.getProgress();
					else
						progress = bds.getProgress();
				}
				
				if (bds.getDuration()!=null)
				{
					if (duration!=null)
						duration+=bds.getDuration();
					else
						duration = bds.getDuration();
				}
					
				CommandBeanTreeNode child = node.addChild(BharunDailySummary.class.getSimpleName(), row[1]);
				Daily d = ApplicationUtils.getConfiguredInstance().getCachedDaily(bds.getDailyUid());
				child.getDynaAttr().put("dayDate", d.getDayDate());
			}
			CustomFieldUom thisProgress = new CustomFieldUom(commandBean, BharunDailySummary.class, "progress",operationUomContext);
			CustomFieldUom thisDuration = new CustomFieldUom(commandBean, BharunDailySummary.class, "duration",operationUomContext);

			if (bharun.getDepthInMdMsl()!=null && bharun.getDepthOutMdMsl()!=null)
			{
				CustomFieldUom thisConverterIn = new CustomFieldUom(commandBean, Bharun.class, "depthInMdMsl",operationUomContext);			
				thisConverterIn.setBaseValueFromUserValue(bharun.getDepthInMdMsl());
				CustomFieldUom thisConverterOut = new CustomFieldUom(commandBean, Bharun.class, "depthOutMdMsl",operationUomContext);
				thisConverterOut.setBaseValueFromUserValue(bharun.getDepthOutMdMsl());
				Double totalProgress = thisConverterOut.getBasevalue()-thisConverterIn.getBasevalue();
				
				thisProgress.setBaseValue(totalProgress);
				node.getDynaAttr().put("totalProgress",thisProgress.getConvertedValue());
				node.setCustomUOM("@totalProgress",thisProgress.getUOMMappingWithLocalDatum());
				
			}
			node.getDynaAttr().put("recordedTotalProgress", progress);
			node.getDynaAttr().put("recordedTotalDuration", duration);

			node.setCustomUOM("@recordedTotalProgress",thisProgress.getUOMMappingWithLocalDatum());
			node.setCustomUOM("@recordedTotalDuration",thisDuration.getUOMMappingWithLocalDatum());

		}
	}



	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		if (meta.getTableClass().equals(Bharun.class) || meta.getTableClass().equals(BharunDailySummary.class)) return true;
		else return false;
	}
	
	private String collectFieldsToBeShown(){
		String fields = "b.bharunUid";
		if (this.bharunFields!=null && this.bharunFields.size()>0)
		{
			for (String field:this.getBharunFields())
				fields+=", b."+field;
		}
		return fields;
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if (meta.getTableClass().equals(Bharun.class))
		{
			
			OperationUomContext operationUomContext = new OperationUomContext(true, true);

			List<Object> output_maps = new ArrayList<Object>();
			QueryProperties qp = new QueryProperties();
			qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			Boolean showInvalidProgress = isTrue(commandBean.getRoot().getDynaAttr().get(SHOW_INVALID_PROGRESS));

			if (!showInvalidProgress)
			{
				int startIndex = pagination.getCurrentPage()*pagination.getRowsPerPage();
				qp.setRowsToFetch(startIndex, pagination.getRowsPerPage());
			}
			List<Object[]> queryResult = this.loadData(qp, commandBean, userSelection,pagination,operationUomContext);
			for (Object[] resultRow : queryResult)
			{
				String operationUid = (String)resultRow[0];
				if (resultRow[1] instanceof Bharun)
				{
					output_maps.add(resultRow[1]);
				}
			}
			
			pagination.normalizeCurrentPageNo();
			return output_maps;
		}
		return null;
	}
	

	private Boolean isTrue(Object value)
	{
		if (value==null)
			return false;
		if (value.equals("1") || value.equals("true"))
			return true;
		return false;
	}
	
	private String getCondition(CommandBean commandBean) throws Exception
	{
		Boolean showNoDepth = isTrue(commandBean.getRoot().getDynaAttr().get(SHOW_NO_DEPTH));
		Boolean showNoDate = isTrue(commandBean.getRoot().getDynaAttr().get(SHOW_NO_DATE));
		Boolean showNoToolType = isTrue(commandBean.getRoot().getDynaAttr().get(SHOW_NO_TOOL_TYPE));
		Boolean showNoCompany = isTrue(commandBean.getRoot().getDynaAttr().get(SHOW_NO_COMPANY));
		
		String condition = "";
		if (showNoDepth || showNoDate || showNoToolType || showNoCompany)
		{
			if (showNoDepth)
				condition+=" (b.depthInMdMsl IS NULL or b.depthOutMdMsl IS NULL) or";
			if (showNoDate)
				condition+=" (b.timeIn IS NULL or b.timeOut IS NULL) or";
			if (showNoToolType)
				condition+=" (b.toolCategory IS NULL or b.toolCategory ='') or";
			if (showNoCompany)
				condition+=" (b.directionalCompany IS NULL or b.directionalCompany ='') or";
			
			condition=condition.substring(0,condition.length()-2);
		}
		return condition;
	}
	
	private List<Object[]> filterThoseDepthNotMatching(CommandBean commandBean,List<Object[]> result, Pagination pagination, OperationUomContext operationUomContext) throws Exception
	{
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean,BharunDailySummary.class,"progress");
		List<Object[]> output = new ArrayList<Object[]>();
		for (Object[] obj : result)
		{
			Bharun bharun = (Bharun) obj[1];
			Double depthDiff = null;
			if (bharun.getDepthInMdMsl()!=null && bharun.getDepthOutMdMsl()!=null)
			{
				CustomFieldUom thisConverterIn = new CustomFieldUom(commandBean, Bharun.class, "depthInMdMsl",operationUomContext);
				thisConverterIn.setBaseValueFromUserValue(bharun.getDepthInMdMsl());
				
				CustomFieldUom thisConverterOut = new CustomFieldUom(commandBean, Bharun.class, "depthOutMdMsl",operationUomContext);
				thisConverterOut.setBaseValueFromUserValue(bharun.getDepthOutMdMsl());
				depthDiff = thisConverterOut.getBasevalue()-thisConverterIn.getBasevalue();
			}
			Double cumDepth = (Double) obj[2];
			if (depthDiff !=null && cumDepth!=null)
			{
				thisConverter.setBaseValue(depthDiff);
				String strDepthDiff = thisConverter.getFormattedValue();
				thisConverter.setBaseValue(cumDepth);
				String strCumDepth = thisConverter.getFormattedValue();
				if (!strDepthDiff.equalsIgnoreCase(strCumDepth))
					output.add(obj);
			}else if((depthDiff ==null && cumDepth!=null) || (depthDiff !=null && depthDiff!=0.00 && cumDepth==null))
			{
				output.add(obj);
			}
		}
		int startIndex = pagination.getCurrentPage()*pagination.getRowsPerPage();
		List<Object[]> returnList = new ArrayList<Object[]>();
		if (output.size()>0)
		{
			for (int start = startIndex;start<startIndex+pagination.getRowsPerPage();start++)
			{
				if ((output.size()-1)>=start)
				returnList.add(output.get(start));
			}
		}
		pagination.setTotalRows(output.size());
		return returnList;
	}
	
	private List<Object[]> loadData(QueryProperties qp, CommandBean commandBean, UserSelectionSnapshot userSelection, Pagination pagination, OperationUomContext operationUomContext) throws Exception
	{
		Boolean showAllWells = isTrue(commandBean.getRoot().getDynaAttr().get(SHOW_ALL_WELLS));
		Boolean showInvalidProgress = isTrue(commandBean.getRoot().getDynaAttr().get(SHOW_INVALID_PROGRESS));
		
		String condition = this.getCondition(commandBean);
		if (StringUtils.isNotBlank(condition))
			condition = " AND ("+condition+")";

		String sql = "Select o.operationUid, b, (Select sum(progress) From BharunDailySummary bds Where (isDeleted is null or isDeleted = False) and bharunUid=b.bharunUid) From Bharun b, Operation o,Well w Where" +
		"(b.isDeleted is null or b.isDeleted =False) and" +
		"(o.isDeleted is null or o.isDeleted =False) and" +
		"(w.isDeleted is null or w.isDeleted =False) and" +
		"(w.wellUid=o.wellUid) and" +
		"(o.operationUid=b.operationUid)";
		String countSQL = "Select count(*) From Bharun b, Operation o, Well w Where" +
		"(b.isDeleted is null or b.isDeleted =False) and" +
		"(o.isDeleted is null or o.isDeleted =False) and" +
		"(w.isDeleted is null or w.isDeleted =False) and" +
		"(w.wellUid=o.wellUid) and" +
		"(o.operationUid=b.operationUid)";
		List<Object[]> result = null;
		Integer totalRows = 0;
		String orderBy = " Order by w.wellName,b.bhaRunNumber,b.timeIn,b.timeOut";
		if (showAllWells)
		{
			
			if (showInvalidProgress)
			{
				result = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql+condition+orderBy,qp);
				result = this.filterThoseDepthNotMatching(commandBean, result,pagination, operationUomContext);
				
			}else{
				List<Object> resultCount = ApplicationUtils.getConfiguredInstance().getDaoManager().find(countSQL+condition);
				totalRows = Integer.valueOf(resultCount.get(0).toString());				
				result = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql+condition+orderBy,qp);	
			}
			
		}else{
			if (showInvalidProgress)
			{
				result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql + " and o.operationUid=:operationUid"+condition+orderBy,"operationUid",userSelection.getOperationUid(),qp);
				result = this.filterThoseDepthNotMatching(commandBean, result,pagination, operationUomContext);				
			}else
			{
				List<Object> resultCount = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(countSQL + " and o.operationUid=:operationUid"+condition,"operationUid",userSelection.getOperationUid(),qp);
				totalRows = Integer.valueOf(resultCount.get(0).toString());
				result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql + " and o.operationUid=:operationUid"+condition+orderBy,"operationUid",userSelection.getOperationUid(),qp);
			}
		}
		if (!showInvalidProgress)
			pagination.setTotalRows(totalRows);
		return result;
	}
	
	
	
	//set the action of the auto form casing tally summary to false
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		if(StringUtils.equals(targetClass, null)) {
			Object obj = node.getData();
			
			if (obj instanceof Bharun || obj instanceof BharunDailySummary) {
				if(StringUtils.equals(action, "edit"))  return true;
				else return false;
			}
		}
		return false;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof Bharun)
		{
			Bharun bharun = (Bharun) object;
			
			if (bharun.getTimeIn()!=null)
			{
				Daily dailyIdIn = ApplicationUtils.getConfiguredInstance().getDailyByOperationAndDate(bharun.getOperationUid(),DateUtils.truncate(bharun.getTimeIn(), Calendar.DAY_OF_MONTH));
				if (dailyIdIn==null)
				{
					status.setContinueProcess(false, true);
					status.setFieldError(node,"timeIn", "This day doesn't exist in the Operation");
				}
				else{
					bharun.setDailyidIn(dailyIdIn.getDailyUid());
				}
			}
			if (bharun.getTimeOut()!=null)
			{
				Daily dailyIdOut = ApplicationUtils.getConfiguredInstance().getDailyByOperationAndDate(bharun.getOperationUid(),DateUtils.truncate(bharun.getTimeOut(), Calendar.DAY_OF_MONTH));
				if (dailyIdOut==null)
				{
					status.setContinueProcess(false, true);
					status.setFieldError(node,"timeOut", "This day doesn't exist in the Operation");
				}
				else{
					bharun.setDailyidOut(dailyIdOut.getDailyUid());
				}
			}
		}
		
	}

	
	
	public ActionHandler getActionHandler(String action,
			CommandBeanTreeNode node) throws Exception {
		if (node.getData() instanceof Bharun ) {
			if (action.equals(Action.SAVE))
				return new BharunSaveActionHandler();
		}
		if (node.getData() instanceof BharunDailySummary ) {
			if (action.equals(Action.SAVE))
				return new BharunSaveActionHandler();
		}
		return null;
	}
	
	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void setBhaCustomUOMFields(List<String> bhaCustomUOMFields) {
		this.bhaCustomUOMFields = bhaCustomUOMFields;
	}

	public List<String> getBhaCustomUOMFields() {
		return bhaCustomUOMFields;
	}

	private class BharunSaveActionHandler implements ActionHandler{

		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {
			// TODO Auto-generated method stub
			Object obj = node.getData();
			
			if (obj instanceof Bharun)
			{
				Bharun bharun = (Bharun)obj;
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(obj, this.setUomTemplateUidAndUomDatum(bharun.getOperationUid()));
			}
			if (obj instanceof BharunDailySummary)
			{
				Object objParent = node.getParent().getData();
				Bharun bharun = (Bharun) objParent;
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(obj, this.setUomTemplateUidAndUomDatum(bharun.getOperationUid()));
				
			}
			
			return new ActionHandlerResponse(true);
		}
		
		private QueryProperties setUomTemplateUidAndUomDatum(String operationUid) throws Exception
		{
			QueryProperties qp = new QueryProperties();
			List operation = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select defaultDatumUid, uomTemplateUid from Operation where operationUid = :operationUid", "operationUid", operationUid);
			if(operation.size() == 0) return qp;
			
			Object[] values = (Object[]) operation.get(0);
			String defaultDatumUid = (String) values[0];
			String uomTemplateUid = (String) values[1];
			qp.setUomDatumUid(defaultDatumUid);
			qp.setUomTemplateUid(uomTemplateUid);
			qp.setIgnoreBlankDatum(false);
			qp.setIgnoreBlankUomTemplate(false);
			return qp;
		}
		
	}	
}
