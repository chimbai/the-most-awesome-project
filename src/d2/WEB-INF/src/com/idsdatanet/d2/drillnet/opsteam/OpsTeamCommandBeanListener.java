package com.idsdatanet.d2.drillnet.opsteam;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.CommandBean;

public class OpsTeamCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		Boolean hasError = commandBean.getInfo().hasErrors();
		if (!hasError) {
			if (commandBean.getFlexClientControl() != null) {
				commandBean.getFlexClientControl().setReloadParentHtmlPage();
			}
		}
	}

}
