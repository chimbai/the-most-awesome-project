package com.idsdatanet.d2.drillnet.operation;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.web.mvc.CachedUserSession;
import com.idsdatanet.d2.core.web.mvc.DynamicTabAllowedHandler;

public class OperationDynamicTabAllowedHandler implements DynamicTabAllowedHandler {

	public boolean isDynamicTabAllow(String tabId, CachedUserSession cachedUserSession) throws Exception {
		if ("tightholesetup".equalsIgnoreCase(tabId)){
			Operation currentOperation = cachedUserSession.getCurrentOperation();
			if (currentOperation != null){
				if (currentOperation.getTightHole() != null && currentOperation.getTightHole() && cachedUserSession.getUserSession().getCanManageTightHoleUsers())
					return true;
			}
			return false;
		}else{
			return true;
		}
	}

}
