package com.idsdatanet.d2.drillnet.campaign;

import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Campaign;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CampaignDataNodeListener extends EmptyDataNodeListener {

    @Override
    public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof Campaign) {
			Campaign campaign = (Campaign)obj;
			
			String operationList = "";
			String dailyUid = "";
			String[] paramsFields = {"thisCampaign"};
			String[] paramsValues = {campaign.getCampaignUid()};
			
			String sql= "FROM Operation where operationCampaignUid=:thisCampaign and (isCampaignOverviewOperation is null or isCampaignOverviewOperation =false) and (isDeleted=false or isDeleted=null) order by startDate";
			List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);

            for (Iterator<Object> i = items.iterator(); i.hasNext();) {
				Operation operation = (Operation) i.next();
	
				String operationName= URLEncoder.encode(this.nullToEmptyString(operation.getOperationName()), "utf-8");
				String operationUid = URLEncoder.encode(operation.getOperationUid(), "utf-8");
				
				//get latest report daily link
				String defaultReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
				String reportSql= "from ReportDaily where operationUid=:operationUid and (isDeleted=false or isDeleted=null) and reportType =:reportType ORDER BY reportDatetime DESC";
				List<Object> reportItems = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(reportSql, new String[]{"operationUid", "reportType"}, new Object[] {operationUid, defaultReportType});

				if(!reportItems.isEmpty()){
					 ReportDaily reportDaily = (ReportDaily)reportItems.get(0);
					 dailyUid = reportDaily.getDailyUid();
				}
				if (StringUtils.isBlank(operationList)) {
					operationList = "operationname=" + operationName + "&dailyuid=" + dailyUid;
				}else {
					operationList = operationList + ",operationname=" + operationName + "&dailyuid=" + dailyUid;
				}
				
				node.getDynaAttr().put("operationList", operationList);
			}			
		}
	}

    @Override
    public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
        Object object = node.getData();
        if (object instanceof Campaign) {
            ApplicationUtils.getConfiguredInstance().refreshCachedData();
            if (commandBean.getFlexClientControl() != null) {
                commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
            }
        }
    }
	
    @Override
    public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
        Object object = node.getData();
        if (object instanceof Campaign) {
            ApplicationUtils.getConfiguredInstance().refreshCachedData();
            if (commandBean.getFlexClientControl() != null) {
                commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
            }
        }
    }

	private String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}
	
}
