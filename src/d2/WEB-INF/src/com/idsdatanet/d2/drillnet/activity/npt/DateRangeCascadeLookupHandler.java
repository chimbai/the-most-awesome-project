package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;


public class DateRangeCascadeLookupHandler implements CascadeLookupHandler{	
	
	private static final String WEEKLY="0";
	private static final String MONTHLY="1";
	private static final String QUARTERLY="2";
	private static final String HALFYEARLY="3";
	private static final String YEARLY="4";
	
	private String uriDateRangeType = null;
	private Boolean isLookupEndDate = false;

	
	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
			String province = "";
		    Map<String, LookupItem> output = new LinkedHashMap<String, LookupItem>();
		    
			Operations ops = UserSession.getInstance(request).getCachedAllAccessibleOperations();
			Date startDate = null;
			Date endDate = null;
			for (_Operation op:ops.values())
			{
				if (startDate==null)
					startDate = op.getSysOperationStartDatetime();
				else{
					if (op.getSysOperationStartDatetime()!=null)
						startDate = op.getSysOperationStartDatetime().before(startDate)?op.getSysOperationStartDatetime():startDate;
				}
				if (endDate==null)
					endDate = op.getSysOperationLastDatetime();
				else{
					if (op.getSysOperationLastDatetime()!=null)
						endDate = op.getSysOperationLastDatetime().after(endDate)?op.getSysOperationLastDatetime():endDate;
				}
			}
				
			List<Date> dateList = DateRangeUtils.getDateList(startDate, endDate, isLookupEndDate, key);
			
			for (Date date:dateList)
			{
				Date inputDate = date;
				if (date.before(startDate))
					inputDate = startDate;
				if (date.after(endDate))
					inputDate = endDate;
				String code = String.valueOf(inputDate.getTime());
				String label = DateRangeUtils.formatDateLabel(inputDate, key);
				LookupItem lookup = new LookupItem(code, label);
				//lookup.addDepotValue("quarter", label);
				output.put(code, lookup);
			}
			return output;
		
   }

	
	
   public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
					
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
	
		Map<String,LookupItem> lookup = LookupManager.getConfiguredInstance().getLookup(this.getUriDateRangeType(), userSelection, null);
		
		
		if (lookup.size() > 0) {
			for (Entry<String,LookupItem> entry : lookup.entrySet()) {
				if (entry != null) {
					String key = entry.getKey();
					CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(key);
					cascadeLookupSet.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, key, null)));
					result.add(cascadeLookupSet);				
				}
			}
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {				
		return null;
	}

	public void setUriDateRangeType(String uriDateRangeType) {
		this.uriDateRangeType = uriDateRangeType;
	}

	public String getUriDateRangeType() {
		return uriDateRangeType;
	}
	public void setIsLookupEndDate(Boolean isLookupEndDate) {
		this.isLookupEndDate = isLookupEndDate;
	}

	public Boolean getIsLookupEndDate() {
		return isLookupEndDate;
	}

}
