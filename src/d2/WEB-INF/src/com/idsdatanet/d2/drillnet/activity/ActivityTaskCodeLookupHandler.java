package com.idsdatanet.d2.drillnet.activity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class ActivityTaskCodeLookupHandler implements LookupHandler{	
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		//activity, well, and operation objects are used because userSelection does not have information required by depot related processes.
		Activity activity = null;
		Well well = null;
		Operation operation = null;
		if(node != null && node.getData() instanceof Activity) {
			activity = (Activity) node.getData();
			well = ApplicationUtils.getConfiguredInstance().getCachedWell(activity.getWellUid());
			operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(activity.getOperationUid());
		} else { //assuming process is not from depot
			well = ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid());
			operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		}
		
		if(well != null && operation != null) {
		    String sql ="FROM LookupTaskCode where  (isDeleted = false or isDeleted is null) and (operationCode =:operationCode or operationCode ='' or operationCode is null) AND " + 
				"(onOffShore =:onOffShore or onOffShore ='' or onOffShore is null) order by shortCode";
			List<LookupTaskCode> listLookupTaskCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { "operationCode", "onOffShore"}, new String[] {operation.getOperationCode(), well.getOnOffShore()});
			if (listLookupTaskCode.size() > 0) { 
				for (LookupTaskCode lookupTaskCodeResult : listLookupTaskCode) {
					LookupItem newItem = new LookupItem(lookupTaskCodeResult.getShortCode(),lookupTaskCodeResult.getName() + " (" + lookupTaskCodeResult.getShortCode() + ")");
					newItem.setActive(lookupTaskCodeResult.getIsActive());
					if ("es".equals(userSelection.getUserLanguage())) {
						if (StringUtils.isNotEmpty(lookupTaskCodeResult.getNameEs()))
							newItem.setValue(lookupTaskCodeResult.getNameEs() + " (" + newItem.getKey() + ")");
					}
					newItem.setTableUid(lookupTaskCodeResult.getLookupTaskCodeUid());
					result.put(lookupTaskCodeResult.getShortCode(), newItem);
				}
			}
		}
		

		return result;
	}
	
}
