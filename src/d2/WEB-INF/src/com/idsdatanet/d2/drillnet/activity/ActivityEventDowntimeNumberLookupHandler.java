package com.idsdatanet.d2.drillnet.activity;

import java.util.LinkedHashMap;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.model.OpsTeam;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.BasinFormation;
import com.idsdatanet.d2.core.model.EquipmentFailure;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityEventDowntimeNumberLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> results = new LinkedHashMap<String, LookupItem>();
		
		String operationUid = userSelection.getOperationUid();
		String dailyUid = userSelection.getDailyUid();
		
		String strSql = "FROM EquipmentFailure WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :selectedDailyUid";
		String[] paramsFields = {"selectedOperationUid","selectedDailyUid"};
		Object[] paramsValues = {operationUid,dailyUid};
		List<EquipmentFailure> equipmentFailures = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (equipmentFailures != null && !equipmentFailures.isEmpty()) {
			for (EquipmentFailure equipmentFailure : equipmentFailures) {
				results.put(equipmentFailure.getEquipmentFailureUid(), new LookupItem(equipmentFailure.getEquipmentFailureUid(), equipmentFailure.getDowntimeEventNumber()));
			} 
		}

		return results;
	}
}
