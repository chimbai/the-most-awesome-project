package com.idsdatanet.d2.drillnet.publishSubscribe;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.PublishSubscribe;
import com.idsdatanet.d2.core.publishsubscribe.PublishSubscribeManager;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class PublishSubscribeDataNodeListener extends EmptyDataNodeListener {

	@Override 
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object data = node.getData();
		if (data instanceof PublishSubscribe)
		{
			PublishSubscribe ps = (PublishSubscribe) data;
			ps.setType(PublishSubscribeManager.SUBSCRIBER);
		}
	}

	@Override
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)	throws Exception {
		Object data = node.getData();

	}

	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed) throws Exception {
		Object data = node.getData();

	}
	
	@Override
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,	UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {

	}
	
	@Override
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object data = node.getData();

	}
	
}
