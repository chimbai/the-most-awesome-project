package com.idsdatanet.d2.drillnet.reportDaily;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.BhaComponent;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.BopLog;
import com.idsdatanet.d2.core.model.CementItem;
import com.idsdatanet.d2.core.model.CementJob;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.LookupBhaComponent;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.MudRheology;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.PersonnelOnSite;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.mudVolume.MudVolumeUtil;

public class CrescentDailyReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		UserSelectionSnapshot userSelection = userContext.getUserSelection();

		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		if (daily == null) return;
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, daily.getOperationUid());
		
		if (operation!=null){
			ReportDataNode child = reportDataNode.addChild("DDR");
			
			Map<String,LookupItem> bitSizeLookup= LookupManager.getConfiguredInstance().getLookup("xml://bitsize?key=code&amp;value=label", userContext.getUserSelection(), null);
			Map<String,LookupItem> bitTypeLookup= LookupManager.getConfiguredInstance().getLookup("xml://bitrun.bit_type?key=code&amp;value=label", userContext.getUserSelection(), null);
			Map<String,LookupItem> bhaComponentTypeLookup= LookupManager.getConfiguredInstance().getLookup("xml://bhacomponent.equipment_type_lookup?key=code&amp;value=label&amp;active=isActive", userContext.getUserSelection(), null);
			Map<String,LookupItem> bitWearGaugeLookup= LookupManager.getConfiguredInstance().getLookup("xml://bitrun.wear_final_gauge?key=code&amp;value=label", userContext.getUserSelection(), null);
			Map<String,LookupItem> cementItemLookup= LookupManager.getConfiguredInstance().getLookup("xml://cementItem.item?key=code&amp;value=label", userContext.getUserSelection(), null);
			
			Date rigAcceptanceDate = null;
			String strSql = "FROM Operation WHERE (isDeleted = false or isDeleted is null) AND wellUid= :wellUid ORDER BY rigAcceptanceDateTime";
			String[] paramsFieldOp = {"wellUid"};
			Object[] paramsValueOp = {daily.getWellUid()};
			List<Operation> resultOp = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldOp, paramsValueOp);
			
			if(resultOp.size()>0){
				for (Operation op : resultOp){
					if (rigAcceptanceDate!=null) break;
					if (op.getRigAcceptanceDateTime()!=null) rigAcceptanceDate = op.getRigAcceptanceDateTime();
				}
			}
			
			Calendar thisCalendar = Calendar.getInstance();
			thisCalendar.setTime(daily.getDayDate());
			thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
			thisCalendar.set(Calendar.MINUTE, 59);
			thisCalendar.set(Calendar.SECOND , 59);
			thisCalendar.set(Calendar.MILLISECOND , 59);
			
			String query = "from ReportDaily where (isDeleted = false or isDeleted is null) and reportType <> 'DGR' and dailyUid=:dailyUid";
			String[] paramNames = {"dailyUid"};
			Object[] paramValues = {daily.getDailyUid()};
			List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramNames, paramValues);
	
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			
			//Depth
			if (daily.getDailyUid()!=null){
				child = reportDataNode.addChild("Depth");
				Double depthMdMsl = 0.00;
				Double rdDepthMdMsl = 0.00;
				Double metresDrilled = 0.00;
				
				if (reportDailyList.size()>0){
					ReportDaily rd = (ReportDaily) reportDailyList.get(0);
					if(rd.getDepthMdMsl()!=null) {
						rdDepthMdMsl = rd.getDepthMdMsl();
					}
				}
				
				Daily previousDaily = ApplicationUtils.getConfiguredInstance().getYesterday(daily.getOperationUid(), daily.getDailyUid());
			 	if (previousDaily != null) {
			 	
			 		query = "from ReportDaily where (isDeleted = false or isDeleted is null) and reportType <> 'DGR' and dailyUid=:dailyUid";
					String[] paramNamesP = {"dailyUid"};
					Object[] paramValuesP = {previousDaily.getDailyUid()};
					List<ReportDaily> reportDailyPreviousList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramNamesP, paramValuesP);
			
					if (reportDailyPreviousList.size()>0){
						ReportDaily rdP = (ReportDaily) reportDailyPreviousList.get(0);
						if(rdP.getDepthMdMsl()!=null) {
							depthMdMsl = rdP.getDepthMdMsl();
						}
					}
			 	}
			 	
				metresDrilled = rdDepthMdMsl - depthMdMsl;				
				child.addProperty("metresDrilled", metresDrilled.toString());
				
				strSql = "FROM Activity a, Daily d WHERE (a.isDeleted = false or a.isDeleted is null) AND (d.isDeleted = false or d.isDeleted is null) AND a.dailyUid=d.dailyUid "+
						"AND (a.isSimop=FALSE or a.isSimop IS NULL) AND (a.isOffline=FALSE or a.isOffline IS NULL) AND a.dayPlus='0' "+
						"AND a.operationUid=:operationUid AND a.startDatetime <= :reportDatetime ORDER BY a.startDatetime DESC ";
				String[] paramsFieldAct = {"operationUid", "reportDatetime"}; 
				Object[] paramsValueAct= {daily.getOperationUid(), thisCalendar.getTime()};
				List<Object[]> resultAct = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldAct, paramsValueAct);
				
				if(resultAct.size()>0){
					int count = 1;
					String latestPhase = "";
					Double duration = 0.00;		
					Double drillingHours = 0.00;
					Double averageROP = 0.00;

					for(Object[] rec : resultAct){
						Activity act = (Activity) rec[0];
						
						if (count==1) {
							latestPhase = act.getPhaseCode()!=null?act.getPhaseCode():"";
							child.addProperty("currentPhase", latestPhase);
						}
						if (latestPhase.equals(act.getPhaseCode())){
							if(act.getActivityDuration()!=null) duration = duration + act.getActivityDuration();
							//if (act.getDepthMdMsl()!=null) depthMdMsl = act.getDepthMdMsl();
						}else{						
							break;
						}
						
						count++;
					}
					child.addProperty("currentPhaseDays", duration.toString());								
					
					strSql = "SELECT SUM(COALESCE(activityDuration,0.00))/3600 FROM Activity WHERE (isDeleted = false or isDeleted is null) "+
							"AND (isSimop=FALSE or isSimop IS NULL) AND (isOffline=FALSE or isOffline IS NULL) AND dayPlus='0' "+
							"AND dailyUid=:dailyUid AND userCode IN ('ROTDRL','ROTPDM','SLDPDM','RSS','MRSS','HOLOP','URPH','URWD') ";
					String[] paramsField = {"dailyUid"}; 
					Object[] paramsValue = {daily.getDailyUid()};
					List<Object> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
					
					if(result.size()>0){
						Object thisResult = (Object) result.get(0);
						if (thisResult!=null) {
							drillingHours = (Double) thisResult;
							child.addProperty("drillingHours", thisResult.toString());
						}else{
							child.addProperty("drillingHours", drillingHours.toString());
						}
					}else{
						child.addProperty("drillingHours", drillingHours.toString());
					}
					if (drillingHours>0){
						averageROP = metresDrilled/drillingHours;
						child.addProperty("averageROP", averageROP.toString());
					}else{
						child.addProperty("averageROP", averageROP.toString());
					}
					 
				}
			}	
			
			//NEW NPT
			if(daily.getDailyUid() != null) {
				child = reportDataNode.addChild("Npt");
				Double cummNPT = 0.00;
				Double totalNPT = 0.00;
				Double NPT = 0.00;
				
				//getting daily NPT
				strSql = "SELECT SUM(COALESCE(activityDuration,0.00))/3600 FROM Activity WHERE (isDeleted = false or isDeleted is null) "+
						"AND (isSimop=FALSE or isSimop IS NULL) AND (isOffline=FALSE or isOffline IS NULL) AND dayPlus='0' "+
						"AND dailyUid=:dailyUid AND internalClassCode IN ('TP','TU')";
				
				String[] paramsFieldDailyNPT = {"dailyUid"};
				Object[] paramsValueDailyNPT = {daily.getDailyUid()};
				List<Object> resultNPT = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldDailyNPT, paramsValueDailyNPT);
				if(resultNPT.size()>0) {
					Object thisResult = (Object) resultNPT.get(0);
					if(thisResult!=null) {
						child.addProperty("dailyNPT", thisResult.toString());
					}
				}
				
				//get Cummulative NPT hours
				if(rigAcceptanceDate != null) {
					strSql = "SELECT SUM(COALESCE(activityDuration,0.00))/3600 FROM Activity WHERE (isDeleted = false or isDeleted is null) "
							+ "AND (isSimop=FALSE or isSimop IS NULL) AND "
							+ "(isOffline=FALSE or isOffline IS NULL) AND dayPlus='0' "
							+ "AND wellUid=:wellUid "
							+ "AND internalClassCode IN ('TP','TU') "
							+ "AND startDatetime >= :rigAcceptanceDateTime "
							+ "AND startDatetime <=:reportDate";
					
					String[] paramsFieldCummNpt = {"wellUid", "rigAcceptanceDateTime", "reportDate"}; 
					Object[] paramsValueCummNpt = {daily.getWellUid(), rigAcceptanceDate, thisCalendar.getTime()};
					List<Object> resultCummNpt = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldCummNpt, paramsValueCummNpt);
					
					if(resultCummNpt.size()>0) {
						Object thisResult = (Object) resultCummNpt.get(0);
						if(thisResult!=null) {
							cummNPT = (Double) thisResult;
						}
					}
				}
				child.addProperty("cummNPT", cummNPT.toString());
				
				//Get Total NPT
				if (rigAcceptanceDate!=null){						
					strSql = "SELECT SUM(COALESCE(activityDuration,0.00))/3600 FROM Activity WHERE (isDeleted = false or isDeleted is null) "+
							"AND (isSimop=FALSE or isSimop IS NULL) AND (isOffline=FALSE or isOffline IS NULL) AND dayPlus='0' "+
							"AND wellUid=:wellUid "+
							"AND startDatetime >= :rigAcceptanceDateTime AND startDatetime <=:reportDate";
					String[] paramsFieldTotalNPT = {"wellUid", "rigAcceptanceDateTime", "reportDate"}; 
					Object[] paramsValueTotalNPT = {daily.getWellUid(), rigAcceptanceDate, thisCalendar.getTime()};
					List<Object> resultTotalNPT = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldTotalNPT, paramsValueTotalNPT);
					
					if(resultTotalNPT.size()>0){
						Object thisResult = (Object) resultTotalNPT.get(0);
						if (thisResult!=null) {
							 totalNPT = (Double) thisResult;
						}
					}
				}
				
				if(totalNPT > 0) {
					NPT = cummNPT/totalNPT * 100;
				}
				child.addProperty("percentNPT", NPT.toString());
			}
			
			
			  //OLD NPT 
				/**if (daily.getDailyUid() != null) {
					Double cumHours = 0.00;

					strSql = "SELECT rootCauseCode, rootCauseSubCode, companyServiceProvided, lookupCompanyUid, startDatetime FROM Activity WHERE (isDeleted = FALSE or isDeleted IS NULL) "
							+ "AND (isSimop=FALSE or isSimop IS NULL) AND (isOffline=FALSE or isOffline IS NULL) AND dayPlus='0' "
							+ "AND dailyUid IN (SELECT dailyUid FROM Daily WHERE (isDeleted = false or isDeleted is null) AND dayDate <=:dayDate AND operationUid=:operationUid) "
							+ "GROUP BY rootCauseCode, rootCauseSubCode, companyServiceProvided, lookupCompanyUid  ";
					String[] paramsFieldNpt = { "operationUid", "dayDate" };
					Object[] paramsValueNpt = { daily.getOperationUid(), daily.getDayDate() };
					List<Object[]> resultNpt = ApplicationUtils.getConfiguredInstance().getDaoManager()
							.findByNamedParam(strSql, paramsFieldNpt, paramsValueNpt);

					if (resultNpt.size() > 0) {
						for (Object[] act : resultNpt) {
							child = reportDataNode.addChild("Npt");
							String rootCauseCode = "";
							String rootCauseSubCode = "";
							String companyServiceProvided = "";
							String lookupCompanyUid = "";

							if (act[0] != null) {
								rootCauseCode = act[0].toString();
							}
							if (act[1] != null) {
								rootCauseSubCode = act[1].toString();
							}
							if (act[2] != null) {
								companyServiceProvided = act[2].toString();
							}

							child.addProperty("rootCauseCode", rootCauseCode);
							child.addProperty("rootCauseSubCode", rootCauseSubCode);

							if (act[3] != null) {
								lookupCompanyUid = act[3].toString();
								LookupCompany company = (LookupCompany) ApplicationUtils.getConfiguredInstance()
										.getDaoManager().getObject(LookupCompany.class, lookupCompanyUid);
								if (company != null)
									child.addProperty("lookupCompanyUid", company.getCompanyName());

								if (companyServiceProvided != null) {
									query = "from LookupCompanyService where (isDeleted = false or isDeleted is null) and code=:code ";
									String[] paramNamesLcs = { "code" };
									Object[] paramValuesLcs = { companyServiceProvided };
									List<LookupCompanyService> lookupCompanyServiceList = ApplicationUtils
											.getConfiguredInstance().getDaoManager()
											.findByNamedParam(query, paramNamesLcs, paramValuesLcs);

									if (lookupCompanyServiceList.size() > 0) {
										LookupCompanyService service = (LookupCompanyService) lookupCompanyServiceList
												.get(0);
										if (service != null) {
											child.addProperty("companyServiceProvided", service.getName());
										}
									}
								}
							}

							child.addProperty("startDatetime", act[4].toString());

							strSql = "SELECT SUM(COALESCE(activityDuration,0.00))/3600 FROM Activity WHERE (isDeleted = false or isDeleted is null) "
									+ "AND (isSimop=FALSE or isSimop IS NULL) AND (isOffline=FALSE or isOffline IS NULL) AND dayPlus='0' "
									+ "AND dailyUid=:dailyUid AND internalClassCode IN ('TP','TU')";
							String[] paramsFieldA = { "dailyUid" };
							Object[] paramsValueA = { daily.getDailyUid() };
							List<Object> resultA = ApplicationUtils.getConfiguredInstance().getDaoManager()
									.findByNamedParam(strSql, paramsFieldA, paramsValueA);

							if (resultA.size() > 0) {
								Object thisResult = (Object) resultA.get(0);
								if (thisResult != null)
									child.addProperty("dailyNPT", thisResult.toString());
							}

							Double cummNPT = 0.00;
							Double totalNPT = 0.00;
							if (rigAcceptanceDate != null) {
								strSql = "SELECT SUM(COALESCE(activityDuration,0.00))/3600 FROM Activity WHERE (isDeleted = false or isDeleted is null) "
										+ "AND (isSimop=FALSE or isSimop IS NULL) AND (isOffline=FALSE or isOffline IS NULL) AND dayPlus='0' "
										+ "AND wellUid=:wellUid AND internalClassCode IN ('TP','TU') "
										+ "AND startDatetime >= :rigAcceptanceDateTime";
								String[] paramsFieldB = { "wellUid", "rigAcceptanceDateTime" };
								Object[] paramsValueB = { daily.getWellUid(), rigAcceptanceDate };
								List<Object> resultB = ApplicationUtils.getConfiguredInstance().getDaoManager()
										.findByNamedParam(strSql, paramsFieldB, paramsValueB);

								if (resultB.size() > 0) {
									Object thisResult = (Object) resultB.get(0);
									if (thisResult != null)
										cummNPT = (Double) thisResult;
									cumHours = cumHours + cummNPT;
								}
							}
							child.addProperty("cummNPT", cummNPT.toString());

							if (rigAcceptanceDate != null) {
								strSql = "SELECT SUM(COALESCE(activityDuration,0.00))/3600 FROM Activity WHERE (isDeleted = false or isDeleted is null) "
										+ "AND (isSimop=FALSE or isSimop IS NULL) AND (isOffline=FALSE or isOffline IS NULL) AND dayPlus='0' "
										+ "AND wellUid=:wellUid "
										+ "AND startDatetime >= :rigAcceptanceDateTime AND startDatetime <=:reportDate";
								String[] paramsFieldB = { "wellUid", "rigAcceptanceDateTime", "reportDate" };
								Object[] paramsValueB = { daily.getWellUid(), rigAcceptanceDate,
										thisCalendar.getTime() };
								List<Object> resultB = ApplicationUtils.getConfiguredInstance().getDaoManager()
										.findByNamedParam(strSql, paramsFieldB, paramsValueB);

								if (resultB.size() > 0) {
									Object thisResult = (Object) resultB.get(0);
									if (thisResult != null)
										totalNPT = (Double) thisResult;
								}
							}

							Double NPT = 0.00;
							if (totalNPT > 0) {
								NPT = cummNPT / totalNPT * 100;
							}
							child.addProperty("percentNPT", NPT.toString());

							strSql = "FROM Activity WHERE (isDeleted = false or isDeleted is null) "
									+ "AND (isSimop=FALSE or isSimop IS NULL) AND (isOffline=FALSE or isOffline IS NULL) AND dayPlus='0' "
									+ "AND dailyUid=:dailyUid AND rootCauseCode=:rootCauseCode AND rootCauseSubCode=:rootCauseSubCode AND companyServiceProvided=:companyServiceProvided AND lookupCompanyUid=:lookupCompanyUid "
									+ "ORDER BY startDatetime ASC ";
							String[] paramsFieldAct = { "dailyUid", "rootCauseCode", "rootCauseSubCode",
									"companyServiceProvided", "lookupCompanyUid" };
							Object[] paramsValueAct = { daily.getDailyUid(), rootCauseCode, rootCauseSubCode,
									companyServiceProvided, lookupCompanyUid };
							List<Activity> resultAct = ApplicationUtils.getConfiguredInstance().getDaoManager()
									.findByNamedParam(strSql, paramsFieldAct, paramsValueAct);

							if (resultAct.size() > 0) {
								String desc = "";
								int count = 1;

								for (Activity activiity : resultAct) {
									if (StringUtils.isNotBlank(activiity.getActivityDescription())) {
										if (count == 1) {
											desc = activiity.getActivityDescription();
										} else {
											desc = desc + "\n" + activiity.getActivityDescription();
										}
										count++;
									}
								}
								child.addProperty("description", desc);
							}

							child.addProperty("cumHours", cumHours.toString());

						}
					}
				}**/
			
			//Bha
			if (daily.getDailyUid()!=null){
				Integer countBha = 0;
				
				strSql = "From Bharun b, BharunDailySummary bds, Daily d Where (b.isDeleted is null or b.isDeleted = false) AND (bds.isDeleted is null or bds.isDeleted = false) AND (d.isDeleted is null or d.isDeleted = false) AND bds.dailyUid=d.dailyUid AND bds.dailyUid=:dailyUid AND b.bharunUid=bds.bharunUid ORDER BY b.depthInMdMsl DESC";
				List<Object[]> resultBha = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid",daily.getDailyUid());
				
				if (resultBha.size()>0){
					for(Object[] rec: resultBha){
						Boolean proceeds = false;
						Bharun bha = (Bharun) rec[0];
						
						if (bha.getDailyidIn()!=null){
							
							Daily bitDailyIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(bha.getDailyidIn());
							if (bitDailyIn!=null && bitDailyIn.getDayDate().getTime()<= daily.getDayDate().getTime()){
								proceeds= true;
							}
							
							if(proceeds){
								if(bha.getBharunUid()!=null){
									countBha++;
									child = reportDataNode.addChild("Bha");
									child.addProperty("bharunUid", bha.getBharunUid());
									child.addProperty("bhaRunNumber", bha.getBhaRunNumber());
									child.addProperty("bhaObjective", getCommonLookup("Bharun.bhaObjective", bha.getBhaObjective()));
									
									if(bha.getDailyidIn()!=null){
										Daily dailyidIn = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, bha.getDailyidIn());
										
										if (dailyidIn!=null){
											child.addProperty("dailyidIn", dateFormat.format(dailyidIn.getDayDate()));
										}
									}
									
									if(bha.getDailyidOut()!=null){
										Daily dailyidOut = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, bha.getDailyidOut());
										
										if (dailyidOut!=null){
											child.addProperty("dailyidOut", dateFormat.format(dailyidOut.getDayDate()));
										}
									}
									
									if (bha.getDepthInMdMsl()!=null){
										thisConverter.setReferenceMappingField(Bharun.class, "depthInMdMsl");
										thisConverter.setBaseValueFromUserValue(bha.getDepthInMdMsl());
										Double depthInMdMsl = thisConverter.getConvertedValue(true);

										child.addProperty("depthInMdMsl", thisConverter.formatOutputPrecision(depthInMdMsl));
									}
									
									if (bha.getDepthOutMdMsl()!=null){
										thisConverter.setReferenceMappingField(Bharun.class, "depthOutMdMsl");
										thisConverter.setBaseValueFromUserValue(bha.getDepthOutMdMsl());
										Double depthOutMdMsl = thisConverter.getConvertedValue(true);

										child.addProperty("depthOutMdMsl", thisConverter.formatOutputPrecision(depthOutMdMsl));
									}
									
									if (bha.getBhaTotalLength()!=null)child.addProperty("bhaTotalLength",bha.getBhaTotalLength().toString());									
									if (bha.getWeightabvjarWet()!=null)child.addProperty("weightabvjarWet",bha.getWeightabvjarWet().toString());
									if (bha.getWeightbelowjarwet()!=null)child.addProperty("weightbelowjarwet",bha.getWeightbelowjarwet().toString());
									if (bha.getWeightBhaTotalWet()!=null)child.addProperty("weightBhaTotalWet",bha.getWeightBhaTotalWet().toString());
									
									List<BhaComponent> resultBhaComponent = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From BhaComponent Where (isDeleted is null or isDeleted = false) and bharunUid=:bharunUid ", "bharunUid", bha.getBharunUid());					

									if (resultBhaComponent.size()>0){
										String bhaList ="";
										int count = 1;
										String type = "";
										
										for (BhaComponent bc : resultBhaComponent){
											if (bc.getType()!=null) {
												type = getLookupValue(bhaComponentTypeLookup,bc.getType());
												
												if ("".equals(type) || StringUtils.isBlank(type)){
													String sql = "FROM LookupBhaComponent WHERE (isDeleted = false or isDeleted is null) AND lookupBhaComponentUid =:lookupBhaComponentUid ";
													List<LookupBhaComponent> lookupBhaComponentList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "lookupBhaComponentUid", bc.getType());
													
													if (lookupBhaComponentList.size() > 0) {
														LookupBhaComponent lookup = (LookupBhaComponent) lookupBhaComponentList.get(0);
														type = lookup.getType();
													}
												}
											}
											
											if (count==1){
												bhaList = "(" + type + ", " + bc.getJointlength() + ")";
											}else{
												bhaList = bhaList + " (" + type + "," + bc.getJointlength() + ")";
											}				
											count ++;
										}
										child.addProperty("BHA", bhaList);
									}
									
									strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND bharunUid = :bharunUid ";
									String[] paramsFieldBds = {"bharunUid", "dailyUid"};
									Object[] paramsValueBds = {bha.getBharunUid(), daily.getDailyUid()};
									List<BharunDailySummary> resultBds = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldBds, paramsValueBds);
									
									if (resultBds.size() > 0){				
										BharunDailySummary thisResult = (BharunDailySummary) resultBds.get(0);
										if (thisResult.getAcceleratingHour()!=null) {
											thisConverter.setReferenceMappingField(BharunDailySummary.class, "acceleratingHour");
											thisConverter.setBaseValueFromUserValue(thisResult.getAcceleratingHour());
											Double acceleratingHour = thisConverter.getConvertedValue();
											
											child.addProperty("acceleratingHour", thisConverter.formatOutputPrecision(acceleratingHour));
										}
										if (thisResult.getJarhourcirculate()!=null){
											thisConverter.setReferenceMappingField(BharunDailySummary.class, "jarhourcirculate");
											thisConverter.setBaseValueFromUserValue(thisResult.getJarhourcirculate());
											Double jarhourcirculate = thisConverter.getConvertedValue();
											
											child.addProperty("jarhourcirculate", thisConverter.formatOutputPrecision(jarhourcirculate));
										}
										if (thisResult.getDurationRotated()!=null){
											thisConverter.setReferenceMappingField(BharunDailySummary.class, "durationRotated");
											thisConverter.setBaseValueFromUserValue(thisResult.getDurationRotated());
											Double durationRotated = thisConverter.getConvertedValue();
											
											child.addProperty("durationRotated", thisConverter.formatOutputPrecision(durationRotated));
										}
										if (thisResult.getMwdHours()!=null){
											thisConverter.setReferenceMappingField(BharunDailySummary.class, "mwdHours");
											thisConverter.setBaseValueFromUserValue(thisResult.getMwdHours());
											Double mwdHours = thisConverter.getConvertedValue();							
											
											child.addProperty("mwdHours", thisConverter.formatOutputPrecision(mwdHours));
										}
										if (thisResult.getLwdHours()!=null){
											thisConverter.setReferenceMappingField(BharunDailySummary.class, "lwdHours");
											thisConverter.setBaseValueFromUserValue(thisResult.getLwdHours());
											Double lwdHours = thisConverter.getConvertedValue();
											
											child.addProperty("lwdHours", thisConverter.formatOutputPrecision(lwdHours));
										}
									}							
								}
							}
						}						
					}	
				}
				
				if (countBha==0){
					child = reportDataNode.addChild("Bha");
					child.addProperty("bharunUid", "uid");
					child.addProperty("bhaRunNumber", "");
				}
			}
			
			//Bit
			if (daily.getDailyUid()!=null){	
				strSql = "From Bharun b, BharunDailySummary bds, Daily d Where (b.isDeleted is null or b.isDeleted = false) AND (bds.isDeleted is null or bds.isDeleted = false) AND (d.isDeleted is null or d.isDeleted = false) AND bds.dailyUid=d.dailyUid AND bds.dailyUid=:dailyUid AND b.bharunUid=bds.bharunUid ORDER BY b.depthInMdMsl DESC";
				List<Object[]> resultBha = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid",daily.getDailyUid());
				
				if (resultBha.size()>0){				
					for(Object[] rec : resultBha){
						Bharun bha = (Bharun) rec[0];
						
						if(bha.getBharunUid()!=null){
							List<Bitrun> resultBit = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From Bitrun Where (isDeleted is null or isDeleted = false) and bharunUid=:bharunUid ", "bharunUid", bha.getBharunUid());					

							if (resultBit.size()>0){
								for (Bitrun bit : resultBit){
									Boolean proceed = false;
									
									if (bit.getDailyUidIn()!=null){
										
										Daily bitDailyIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(bit.getDailyUidIn());
										if (bitDailyIn!=null && bitDailyIn.getDayDate().getTime()<= daily.getDayDate().getTime()){
											proceed= true;
											
											/*
											if (bit.getDailyUidOut()!=null){
												Daily bitDailyOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(bit.getDailyUidOut());
												
												if (bitDailyOut!=null && bitDailyOut.getDayDate().getTime()>=daily.getDayDate().getTime()){								
													proceed = true;
												}else if(bitDailyOut==null){
													proceed = true;
												}
											}
											*/
										}
									}
									
									if (proceed){
										child = reportDataNode.addChild("Bit");
										child.addProperty("bitrunNumber", bit.getBitrunNumber());
																			
										if (bit.getBitDiameter()!=null) {
											thisConverter.setReferenceMappingField(Bitrun.class, "bitDiameter");
											thisConverter.setBaseValueFromUserValue(bit.getBitDiameter());
											child.addProperty("bitDiameter", getLookupValue(bitSizeLookup,thisConverter.getFormattedValue().replace(thisConverter.getUomSymbol(), "").trim()));
										}
										
										LookupCompany company = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, bit.getMake());
										if (company!=null) child.addProperty("make", company.getCompanyName());
										
										child.addProperty("bitType", getLookupValue(bitTypeLookup,bit.getBitType()));
										
										child.addProperty("model", bit.getModel());
										child.addProperty("iadcCode", bit.getIadcCode());
										child.addProperty("serialNumber", bit.getSerialNumber());
										if (bit.getBitLength()!=null) child.addProperty("bitLength", bit.getBitLength().toString());
										
										if (bha.getDepthInMdMsl()!=null){
											thisConverter.setReferenceMappingField(Bharun.class, "depthInMdMsl");
											thisConverter.setBaseValueFromUserValue(bha.getDepthInMdMsl());
											Double depthInMdMsl = thisConverter.getConvertedValue(true);
										
											child.addProperty("depthInMdMsl", thisConverter.formatOutputPrecision(depthInMdMsl));
										}
										
										if (bha.getDepthOutMdMsl()!=null){
											thisConverter.setReferenceMappingField(Bharun.class, "depthOutMdMsl");
											thisConverter.setBaseValueFromUserValue(bha.getDepthOutMdMsl());
											Double depthOutMdMsl = thisConverter.getConvertedValue(true);
											
											child.addProperty("depthOutMdMsl",  thisConverter.formatOutputPrecision(depthOutMdMsl));
										}
										
										if (bit.getTfa()!=null) child.addProperty("tfa", bit.getTfa().toString());
										child.addProperty("condFinalInner", bit.getCondFinalInner());
										child.addProperty("condFinalOuter", bit.getCondFinalOuter());
										child.addProperty("condFinalDull", bit.getCondFinalDull());
										child.addProperty("condFinalLocation", bit.getCondFinalLocation());
										child.addProperty("condFinalBearing", bit.getCondFinalBearing());					
										child.addProperty("condFinalGauge", getLookupValue(bitWearGaugeLookup,bit.getCondFinalGauge()));
										
										String condOther = "";
										if(bit.getCondFinalOther()!=null){
											Integer count = 0;
											String[] other = bit.getCondFinalOther().split("\t");
											 for (int i = 0; i < other.length; i++) {
												 if (count>0) condOther= condOther + "," + other[i].toString();
												 else condOther= condOther + other[i].toString();
												 count++;
											 }
					
										}
										child.addProperty("condFinalOther", condOther);
										child.addProperty("condFinalReason", bit.getCondFinalReason());							
										
										strSql = "FROM BitNozzle WHERE (isDeleted = false or isDeleted is null) AND bitrunUid = :bitrunUid ORDER BY nozzleNumber";
										String[] paramsField = {"bitrunUid"};
										Object[] paramsValue = {bit.getBitrunUid()};
										List<BitNozzle> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
										
										if (result.size() > 0){
											String nozzles = "";
											Integer count = 1;
											for (BitNozzle noz : result){
												if (count==1)
													nozzles = Math.round(noz.getNozzleQty()) + "x" + Math.round(noz.getNozzleSize());
												else{
													nozzles =  nozzles + ", " + Math.round(noz.getNozzleQty()) + "x" + Math.round(noz.getNozzleSize()) ;
												}
												count++;
											}
											child.addProperty("nozzles", nozzles);
										}
										
										strSql = "SELECT SUM(COALESCE(duration,0.0)), SUM(COALESCE(progress,0.0)), SUM(COALESCE(krevs,0.0)), SUM(COALESCE(IADCDuration,0.0)),SUM(COALESCE(bitHoursOut,0.0)) FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND bharunUid = :bharunUid ";
										String[] paramsField1 = {"bharunUid", "dailyUid"};
										Object[] paramsValue1 = {bha.getBharunUid(), daily.getDailyUid()};
										List<Object[]> result1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField1, paramsValue1);
										
										if (result1.size() > 0){				
											Object[] thisResult = (Object[]) result1.get(0);
											
											Double totalProgress = 0.00;
											if (thisResult[1] != null) {
												totalProgress = Double.parseDouble(thisResult[1].toString());
											}
											
											Double totalDuration = 0.00;
											if (thisResult[0] != null) {
												totalDuration = Double.parseDouble(thisResult[0].toString());
											}
											/*
											thisConverter.setReferenceMappingField(BharunDailySummary.class, "rop");
											Double avgrop = 0.00;
											if (thisResult[0] != null && thisResult[1] != null) {
												if (totalDuration!=0) avgrop = totalProgress / totalDuration;
											}
											thisConverter.setBaseValue(avgrop);
											
											avgrop = thisConverter.getConvertedValue();
											child.addProperty("rop", avgrop.toString());
											*/
											
											strSql = "SELECT SUM(COALESCE(bds.krevs,0.0)) FROM BharunDailySummary bds INNER JOIN Daily d ON bds.dailyUid=d.dailyUid WHERE (bds.isDeleted = false or bds.isDeleted is null) AND (d.isDeleted = false or d.isDeleted is null) AND d.dayDate <= :todayDate AND bds.bharunUid = :bharunUid ";
											String[] paramsFieldK = {"bharunUid", "todayDate"};
											Object[] paramsValueK = {bha.getBharunUid(), daily.getDayDate()};
											List<Object> resultK = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldK, paramsValueK);
											
											if (resultK.size() > 0){				
												Object thisKrev = (Object) resultK.get(0);
												
												thisConverter.setReferenceMappingField(BharunDailySummary.class, "krevs");
												Double totalKrevs = 0.00;
												if (thisKrev != null) {
													totalKrevs = Double.parseDouble(thisKrev.toString());
												}
												thisConverter.setBaseValue(totalKrevs);
												
												totalKrevs = thisConverter.getConvertedValue();
												child.addProperty("krevs", totalKrevs.toString());
											}														
											
											strSql = "SELECT SUM(COALESCE(bds.progress,0.0)), SUM(COALESCE(bds.duration,0.0)) FROM BharunDailySummary bds, Daily d  WHERE (bds.isDeleted = false or bds.isDeleted is null) AND (d.isDeleted = false or d.isDeleted is null) " +
													"AND d.dailyUid = bds.dailyUid AND d.operationUid = :operationUid AND bds.bharunUid = :bharunUid AND d.dayDate <= :todayDate";
											String[] paramsFieldB = {"bharunUid", "operationUid", "todayDate"};
											Object[] paramsValueB = {bha.getBharunUid(), daily.getOperationUid(), daily.getDayDate()};
											List<Object[]> resultB = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldB, paramsValueB);
											
											if (resultB.size() > 0){				
												Object[] thisResultB = (Object[]) resultB.get(0);
												
												Double totalProgressB = 0.00;
												if (thisResultB[0] != null) {
													totalProgressB = Double.parseDouble(thisResultB[0].toString());
												}											
												
												thisConverter.setReferenceMappingField(BharunDailySummary.class, "progress");
												if (thisResultB[0] != null) {
													thisConverter.setBaseValue(totalProgressB);
													totalProgressB = thisConverter.getConvertedValue(true);								
												}
												/*
												Double distancePriorRun = 0.00;
												thisConverter.setReferenceMappingField(Bitrun.class, "distancePriorRun");
												if (bit.getDistancePriorRun() != null){
													thisConverter.setBaseValueFromUserValue(bit.getDistancePriorRun());
													distancePriorRun = thisConverter.getBasevalue();
												}
												thisConverter.setBaseValue(distancePriorRun + totalProgressB);
												distancePriorRun = thisConverter.getConvertedValue(true);
												*/
												child.addProperty("totalDistancePriorRun", thisConverter.formatOutputPrecision(totalProgressB));
												
												thisConverter.setReferenceMappingField(BharunDailySummary.class, "duration");
												Double totalBitDuration = 0.00;
												if (thisResultB[1] != null) {
													totalBitDuration = Double.parseDouble(thisResultB[1].toString());
													thisConverter.setBaseValue(totalBitDuration);
													totalBitDuration = thisConverter.getConvertedValue();
												}
												/*
												Double durationPriorRun = 0.00;
												thisConverter.setReferenceMappingField(Bitrun.class, "durationPriorRun");

												if (bit.getDurationPriorRun() != null){
													thisConverter.setBaseValueFromUserValue(bit.getDurationPriorRun());
													durationPriorRun = thisConverter.getBasevalue();
												}
												thisConverter.setBaseValue(durationPriorRun + totalBitDuration);
												durationPriorRun = thisConverter.getConvertedValue();
												*/
												child.addProperty("totalDurationPriorRun", thisConverter.formatOutputPrecision(totalBitDuration));
												
												Double avgrop = 0.00;
												if (totalBitDuration != null && totalProgressB != null) {
													if (totalBitDuration!=0) avgrop = totalProgressB / totalBitDuration;
												}
												
												child.addProperty("rop", avgrop.toString());
											}	
											
											thisConverter.setReferenceMappingField(BharunDailySummary.class, "bitHoursOut");
											Double bitHoursOut = 0.00;
											if (thisResult[4] != null) {
												bitHoursOut = Double.parseDouble(thisResult[4].toString());
											}
											thisConverter.setBaseValue(bitHoursOut);
											
											bitHoursOut = thisConverter.getConvertedValue();
											child.addProperty("bitHoursOut", bitHoursOut.toString());
											
										}
			
										strSql = "FROM DrillingParameters "
                                                + "WHERE (isDeleted is null or isDeleted = false) "
                                                + "AND bharunUid =:bharunUid "
                                                + "AND dailyUid =:dailyUid "
                                                + "ORDER BY depthTopMdMsl DESC";
                                        String[] paramsFieldD = {"bharunUid", "dailyUid"};

                                        Object[] paramsValueD = {bit.getBharunUid(), daily.getDailyUid()};
                                        List<DrillingParameters> resultDp = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldD, paramsValueD);

										if (resultDp.size() > 0){
											DrillingParameters dp = (DrillingParameters) resultDp.get(0);
											
											if (dp.getWobMinForce()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "wobMinForce");
												child.addProperty("wobMinForce", thisConverter.formatOutputPrecision(dp.getWobMinForce()));
											}
											
											if(dp.getWobMaxForce()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "wobMaxForce");
												child.addProperty("wobMaxForce", thisConverter.formatOutputPrecision(dp.getWobMaxForce()));
											}
											
											if(dp.getSurfaceRpmMin()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "surfaceRpmMin");
												child.addProperty("surfaceRpmMin", thisConverter.formatOutputPrecision(dp.getSurfaceRpmMin()));
											}
											
											if(dp.getSurfaceRpmMax()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "surfaceRpmMax");
												child.addProperty("surfaceRpmMax", thisConverter.formatOutputPrecision(dp.getSurfaceRpmMax()));
											}
											
											if (dp.getFlowMin()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "flowMin");
												child.addProperty("flowMin", thisConverter.formatOutputPrecision(dp.getFlowMin()));
											}
											
											if (dp.getFlowMax()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "flowMax");
												child.addProperty("flowMax", thisConverter.formatOutputPrecision(dp.getFlowMax()));
											}
											
											if (dp.getStandpipePressureMin()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "standpipePressureMin");
												child.addProperty("standpipePressureMin", thisConverter.formatOutputPrecision(dp.getStandpipePressureMin()));
											}
											
											if (dp.getStandpipePressureMax()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "standpipePressureMax");
												child.addProperty("standpipePressureMax", thisConverter.formatOutputPrecision(dp.getStandpipePressureMax()));
											}										
										}
										
									}
								}
							}
						}
					}
				}					
			}
			//Personnel
			if (daily.getDailyUid()!=null){
				int reportColumn = 2;
				strSql = "FROM PersonnelOnSite WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid ORDER BY crewCompany, sequence";
				String[] paramsField = {"operationUid", "dailyUid"};
				Object[] paramsValue = {daily.getOperationUid(), daily.getDailyUid()};
				List<PersonnelOnSite> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				String crewCompany ="";
				
				if (result.size() > 0){
					Integer column = 0;
					Boolean newRow = true;
					
					for (PersonnelOnSite pob : result){				
						if (crewCompany.equals(pob.getCrewCompany())){
							continue;
						}
						
						if (newRow) child = reportDataNode.addChild("PersonnelOnSite");
						
						crewCompany = pob.getCrewCompany();
						
						LookupCompany company = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, pob.getCrewCompany());
						if (company!=null) {
							
							child.addProperty("crewCompany"+column,company.getCompanyName());
							child.addProperty("pax"+column, this.getPax(daily.getOperationUid(), daily.getDailyUid(), pob.getCrewCompany()).toString());
							child.addProperty("noStopCards"+column, this.getNoStopCard(daily.getOperationUid(), daily.getDailyUid(), pob.getCrewCompany()).toString());
						}					
						
						column ++;
						if (column>reportColumn){
							column=0;
							newRow= true;
						}else{
							newRow= false;
						}
						
						child.addProperty("column", column.toString());			
					}				
				}else{
					for (Integer i=1;i<=1;i++){
						child = reportDataNode.addChild("PersonnelOnSite");
						child.addProperty("column", i.toString());
					}	
				}
			}
			
			//Drilling Contractor Green Hands / Absentees / Shortages
			if (daily.getDailyUid()!=null){
				strSql = "FROM PersonnelOnSite WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid ORDER BY crewCompany, sequence";
				String[] paramsField = {"operationUid", "dailyUid"};
				Object[] paramsValue = {daily.getOperationUid(), daily.getDailyUid()};
				List<PersonnelOnSite> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				Boolean gotPob = false;
				
				if (result.size() > 0){
					Integer maxRow = 0;
					Integer Gh = this.getMaxDrillingContractor(daily, "GH");
					Integer ABS = this.getMaxDrillingContractor(daily, "ABS");
					Integer ST = this.getMaxDrillingContractor(daily, "ST");
					
					
					if (Gh > maxRow){
						maxRow = Gh;
					}
					if (ABS > maxRow){
						maxRow = ABS;
					}
					if (ST > maxRow){
						maxRow = ST;
					}
					
					for (Integer i=1;i<=maxRow;i++){
						child = reportDataNode.addChild("DrillingContractor");
						this.getDrillingContractor(child, daily, "GH", i);
						this.getDrillingContractor(child, daily, "ABS", i);
						this.getDrillingContractor(child, daily, "ST", i);			
					}
					
					if (maxRow>0) gotPob = true;
					
				}	
				
				if (!gotPob){
					child = reportDataNode.addChild("DrillingContractor");
					child.addProperty("column", "1");
				}
			}
			
			//Drilling Fluid
			if (daily.getDailyUid()!=null){
				child = reportDataNode.addChild("MudProperties");
				
				strSql = "FROM MudProperties WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid AND (isPlan = FALSE OR isPlan IS NULL) ORDER BY reportTime";
				String[] paramsField = {"operationUid", "dailyUid"};
				Object[] paramsValue = {daily.getOperationUid(), daily.getDailyUid()};
				List<MudProperties> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if (result.size() > 0){
					Integer count = 1;
					Integer diff = 0;
					Integer column = 1;
					Double cumCuttings = 0.0;
					
					if (result.size()>3){
						diff = result.size() - 3;
					}
					Map<String,LookupItem> mudTypeLookup = LookupManager.getConfiguredInstance().getLookup("xml://mud_properties.mud_type?key=code&amp;value=label", userContext.getUserSelection(), null);
					DateFormat dateFormatTime = new SimpleDateFormat("HH:mm");
					
					for (MudProperties mud : result){
						if (mud.getAvgCuttings()!=null) cumCuttings = cumCuttings + mud.getAvgCuttings();
						
						if (count ==1 || count == diff+2 || count == diff+3){
							
							if (mud.getReportTime()!=null) {
								if (dateFormatTime.format(mud.getReportTime()).equals("23:59")){
									child.addProperty("reportTime"+"_"+column, "24:00");
								}else{
									child.addProperty("reportTime"+"_"+column, dateFormatTime.format(mud.getReportTime()));
								}
							}
							
							child.addProperty("mudType"+"_"+column, getLookupValue(mudTypeLookup, mud.getMudType()));
							child.addProperty("otherlabel1"+"_"+column, getCommonLookup("MudProperties.otherlabel1", mud.getOtherlabel1()));
							if (mud.getDepthMdMsl()!=null) child.addProperty("depthMdMsl"+"_"+column, mud.getDepthMdMsl().toString());
							if (mud.getMudTestTemperature()!=null) child.addProperty("mudTestTemperature"+"_"+column, mud.getMudTestTemperature().toString());
							if (mud.getMudWeight()!=null) child.addProperty("mudWeight"+"_"+column, mud.getMudWeight().toString());
							if (mud.getMudFv()!=null) child.addProperty("mudFv"+"_"+column, mud.getMudFv().toString());
							if (mud.getEcd()!=null) child.addProperty("ecd"+"_"+column, mud.getEcd().toString());
							if (mud.getMudPv()!=null) child.addProperty("mudPv"+"_"+column, mud.getMudPv().toString());
							if (mud.getMudYp()!=null) child.addProperty("mudYp"+"_"+column, mud.getMudYp().toString());
							if (mud.getMudPh()!=null) child.addProperty("mudPh"+"_"+column, mud.getMudPh().toString());
							
							if (mud.getMudGel10s()!=null) child.addProperty("mudGel10s"+"_"+column, mud.getMudGel10s().toString());
							if (mud.getMudGel10m()!=null) child.addProperty("mudGel10m"+"_"+column, mud.getMudGel10m().toString());
							if (mud.getMudGel30m()!=null) child.addProperty("mudGel30m"+"_"+column, mud.getMudGel30m().toString());
							if (mud.getMudApiFl()!=null) child.addProperty("fluidLossApiVolume"+"_"+column, mud.getMudApiFl().toString());
							if (mud.getHthpTestTemperature()!=null) child.addProperty("hthpTestTemperature"+"_"+column, mud.getHthpTestTemperature().toString());
							if (mud.getHthpFl()!=null) child.addProperty("hthpFl"+"_"+column, mud.getHthpFl().toString());
							if (mud.getMudApiCake()!=null) child.addProperty("mudApiCake"+"_"+column, mud.getMudApiCake().toString());
							if (mud.getMudMbtDensity()!=null) child.addProperty("mudMbtDensity"+"_"+column, mud.getMudMbtDensity().toString());
							if (mud.getMudPmVolume()!=null) child.addProperty("mudPm"+"_"+column, mud.getMudPmVolume().toString());
							if (mud.getLubricantConcentration()!=null) child.addProperty("lubricantConcentration"+"_"+column, mud.getLubricantConcentration().toString());
							
							if (mud.getMudCaConcentration()!=null) child.addProperty("mudCaConcentration"+"_"+column, this.formatDataWithUom(thisConverter, mud.getMudCaConcentration(), MudProperties.class, "mudCaConcentration", false, false));
							if (mud.getKplusConcentration()!=null) child.addProperty("kplusConcentration"+"_"+column, this.formatDataWithUom(thisConverter, mud.getKplusConcentration(), MudProperties.class, "kplusConcentration", false, false));
							if (mud.getCaclMud()!=null) child.addProperty("caclMud"+"_"+column, mud.getCaclMud().toString());
							if (mud.getMudKclConcentration()!=null) child.addProperty("mudKclConcentration"+"_"+column, mud.getMudKclConcentration().toString());
							if (mud.getMudChlorideIonConcentration()!=null) child.addProperty("mudChlorideIonConcentration"+"_"+column, mud.getMudChlorideIonConcentration().toString());
							if (mud.getSandPcVolpervol()!=null) child.addProperty("sandPcVolpervol"+"_"+column, mud.getSandPcVolpervol().toString());
							if (mud.getHgsPcVolpervol()!=null) child.addProperty("hgsPcVolpervol"+"_"+column, mud.getHgsPcVolpervol().toString());
							if (mud.getLgsPcVolpervol()!=null) child.addProperty("lgsPcVolpervol"+"_"+column, mud.getLgsPcVolpervol().toString());
							if (mud.getMudPf()!=null) child.addProperty("mudPf"+"_"+column, this.formatDataWithUom(thisConverter, mud.getMudPf(), MudProperties.class, "mudPf", false, false));
							if (mud.getMudMfVolume()!=null) child.addProperty("mudMfVolume"+"_"+column, this.formatDataWithUom(thisConverter, mud.getMudMfVolume(), MudProperties.class, "mudMfVolume", false, false));
							if (getRheologyReading(mud.getMudPropertiesUid(), 3)!=null) child.addProperty("rheologyReading3"+"_"+column, getRheologyReading(mud.getMudPropertiesUid(), 3).toString());
							if (getRheologyReading(mud.getMudPropertiesUid(), 6)!=null) child.addProperty("rheologyReading6"+"_"+column, getRheologyReading(mud.getMudPropertiesUid(), 6).toString());
							if (mud.getSgsConcentration()!=null) child.addProperty("sgsConcentration"+"_"+column, mud.getSgsConcentration().toString());
							if (mud.getPolymerConcentration()!=null) child.addProperty("polymerConcentration"+"_"+column, this.formatDataWithUom(thisConverter, mud.getPolymerConcentration(), MudProperties.class, "polymerConcentration", false, false));
		
							if (mud.getDissolvedH2sDensity()!=null) child.addProperty("dissolvedH2sDensity"+"_"+column, mud.getDissolvedH2sDensity().toString());
							if (mud.getRetortSolidsPcVolpervol()!=null) child.addProperty("retortSolidsPcVolpervol"+"_"+column, mud.getRetortSolidsPcVolpervol().toString());
							if (mud.getRetortOilPcVolpervol()!=null) child.addProperty("retortOilPcVolpervol"+"_"+column, mud.getRetortOilPcVolpervol().toString());
							if (mud.getRetortH2oPcVolpervol()!=null) child.addProperty("retortH2oPcVolpervol"+"_"+column, mud.getRetortH2oPcVolpervol().toString());
							if (mud.getOwratio()!=null) child.addProperty("owratio"+"_"+column, mud.getOwratio().toString());
							if (mud.getAvgCuttings()!=null) child.addProperty("avgCuttings"+"_"+column, mud.getAvgCuttings().toString());
							child.addProperty("cumCuttings"+"_"+column, cumCuttings.toString());
							if (mud.getChipConcentration()!=null) child.addProperty("chipConcentration"+"_"+column, mud.getChipConcentration().toString());
							if (mud.getDrillPipeAnnularVelocity()!=null) child.addProperty("drillPipeAnnularVelocity"+"_"+column, mud.getDrillPipeAnnularVelocity().toString());
							if (mud.getCciConcentration()!=null) child.addProperty("cciConcentration"+"_"+column, mud.getCciConcentration().toString());
							if (mud.getHthpCake()!=null) child.addProperty("hthpCake"+"_"+column, mud.getHthpCake().toString());
							if (mud.getH2sScavengerConc()!=null) child.addProperty("h2sScavengerConc"+"_"+column, this.formatDataWithUom(thisConverter, mud.getH2sScavengerConc(), MudProperties.class, "h2sScavengerConc", false, false));					
							
							column++;
						}
						
						count++;
					}
				}else{
					child.addProperty("mudType"+"_1", "");
					child.addProperty("mudType"+"_2", "");
					child.addProperty("mudType"+"_3", "");
				}
				
				strSql = "FROM MudProperties WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid AND (isPlan = TRUE) ORDER BY reportTime DESC";
				List<MudProperties> resultPlan = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if (resultPlan.size() > 0){
					Double cumCuttings = 0.0;
					String column = "p";
					
					Map<String,LookupItem> mudTypeLookup = LookupManager.getConfiguredInstance().getLookup("xml://mud_properties.mud_type?key=code&amp;value=label", userContext.getUserSelection(), null);
					DateFormat dateFormatTime = new SimpleDateFormat("HH:mm");
					
					for (MudProperties mud : resultPlan){
						if (mud.getAvgCuttings()!=null) cumCuttings = cumCuttings + mud.getAvgCuttings();
						
						if (mud.getReportTime()!=null) {
							if (dateFormatTime.format(mud.getReportTime()).equals("23:59")){
								child.addProperty("reportTime"+"_"+column, "24:00");
							}else{
								child.addProperty("reportTime"+"_"+column, dateFormatTime.format(mud.getReportTime()));
							}
						}
						
						child.addProperty("mudType"+"_"+column, getLookupValue(mudTypeLookup, mud.getMudType()));
						child.addProperty("otherlabel1"+"_"+column, getCommonLookup("MudProperties.otherlabel1", mud.getOtherlabel1()));
						if (mud.getDepthMdMsl()!=null) child.addProperty("depthMdMsl"+"_"+column, mud.getDepthMdMsl().toString());
						if (mud.getMudTestTemperature()!=null) child.addProperty("mudTestTemperature"+"_"+column, mud.getMudTestTemperature().toString());
						if (mud.getMudWeight()!=null) child.addProperty("mudWeight"+"_"+column, mud.getMudWeight().toString());
						if (mud.getMudFv()!=null) child.addProperty("mudFv"+"_"+column, mud.getMudFv().toString());
						if (mud.getEcd()!=null) child.addProperty("ecd"+"_"+column, mud.getEcd().toString());
						if (mud.getMudPv()!=null) child.addProperty("mudPv"+"_"+column, mud.getMudPv().toString());
						if (mud.getMudYp()!=null) child.addProperty("mudYp"+"_"+column, mud.getMudYp().toString());
						if (mud.getMudPh()!=null) child.addProperty("mudPh"+"_"+column, mud.getMudPh().toString());
						
						if (mud.getMudGel10s()!=null) child.addProperty("mudGel10s"+"_"+column, mud.getMudGel10s().toString());
						if (mud.getMudGel10m()!=null) child.addProperty("mudGel10m"+"_"+column, mud.getMudGel10m().toString());
						if (mud.getMudGel30m()!=null) child.addProperty("mudGel30m"+"_"+column, mud.getMudGel30m().toString());
						if (mud.getMudApiFl()!=null) child.addProperty("fluidLossApiVolume"+"_"+column, mud.getMudApiFl().toString());
						if (mud.getHthpTestTemperature()!=null) child.addProperty("hthpTestTemperature"+"_"+column, mud.getHthpTestTemperature().toString());
						if (mud.getHthpFl()!=null) child.addProperty("hthpFl"+"_"+column, mud.getHthpFl().toString());
						if (mud.getMudApiCake()!=null) child.addProperty("mudApiCake"+"_"+column, mud.getMudApiCake().toString());
						if (mud.getMudMbtDensity()!=null) child.addProperty("mudMbtDensity"+"_"+column, mud.getMudMbtDensity().toString());
						if (mud.getMudPmVolume()!=null) child.addProperty("mudPm"+"_"+column, mud.getMudPmVolume().toString());
						if (mud.getLubricantConcentration()!=null) child.addProperty("lubricantConcentration"+"_"+column, mud.getLubricantConcentration().toString());
						
						if (mud.getMudCaConcentration()!=null) child.addProperty("mudCaConcentration"+"_"+column, this.formatDataWithUom(thisConverter, mud.getMudCaConcentration(), MudProperties.class, "mudCaConcentration", false, false));
						if (mud.getKplusConcentration()!=null) child.addProperty("kplusConcentration"+"_"+column, this.formatDataWithUom(thisConverter, mud.getKplusConcentration(), MudProperties.class, "kplusConcentration", false, false));
						if (mud.getCaclMud()!=null) child.addProperty("caclMud"+"_"+column, mud.getCaclMud().toString());
						if (mud.getMudKclConcentration()!=null) child.addProperty("mudKclConcentration"+"_"+column, mud.getMudKclConcentration().toString());
						if (mud.getMudChlorideIonConcentration()!=null) child.addProperty("mudChlorideIonConcentration"+"_"+column, mud.getMudChlorideIonConcentration().toString());
						if (mud.getSandPcVolpervol()!=null) child.addProperty("sandPcVolpervol"+"_"+column, mud.getSandPcVolpervol().toString());
						if (mud.getHgsPcVolpervol()!=null) child.addProperty("hgsPcVolpervol"+"_"+column, mud.getHgsPcVolpervol().toString());
						if (mud.getLgsPcVolpervol()!=null) child.addProperty("lgsPcVolpervol"+"_"+column, mud.getLgsPcVolpervol().toString());
						if (mud.getMudPf()!=null) child.addProperty("mudPf"+"_"+column, this.formatDataWithUom(thisConverter, mud.getMudPf(), MudProperties.class, "mudPf", false, false));
						if (mud.getMudMfVolume()!=null) child.addProperty("mudMfVolume"+"_"+column, this.formatDataWithUom(thisConverter, mud.getMudMfVolume(), MudProperties.class, "mudMfVolume", false, false));
						if (getRheologyReading(mud.getMudPropertiesUid(), 3)!=null) child.addProperty("rheologyReading3"+"_"+column, getRheologyReading(mud.getMudPropertiesUid(), 3).toString());
						if (getRheologyReading(mud.getMudPropertiesUid(), 6)!=null) child.addProperty("rheologyReading6"+"_"+column, getRheologyReading(mud.getMudPropertiesUid(), 6).toString());
						if (mud.getSgsConcentration()!=null) child.addProperty("sgsConcentration"+"_"+column, mud.getSgsConcentration().toString());
						if (mud.getPolymerConcentration()!=null) child.addProperty("polymerConcentration"+"_"+column, this.formatDataWithUom(thisConverter, mud.getPolymerConcentration(), MudProperties.class, "polymerConcentration", false, false));
						
						if (mud.getDissolvedH2sDensity()!=null) child.addProperty("dissolvedH2sDensity"+"_"+column, mud.getDissolvedH2sDensity().toString());
						if (mud.getRetortSolidsPcVolpervol()!=null) child.addProperty("retortSolidsPcVolpervol"+"_"+column, mud.getRetortSolidsPcVolpervol().toString());
						if (mud.getRetortOilPcVolpervol()!=null) child.addProperty("retortOilPcVolpervol"+"_"+column, mud.getRetortOilPcVolpervol().toString());
						if (mud.getRetortH2oPcVolpervol()!=null) child.addProperty("retortH2oPcVolpervol"+"_"+column, mud.getRetortH2oPcVolpervol().toString());
						if (mud.getOwratio()!=null) child.addProperty("owratio"+"_"+column, mud.getOwratio().toString());
						if (mud.getAvgCuttings()!=null) child.addProperty("avgCuttings"+"_"+column, mud.getAvgCuttings().toString());
						child.addProperty("cumCuttings"+"_"+column, cumCuttings.toString());
						if (mud.getChipConcentration()!=null) child.addProperty("chipConcentration"+"_"+column, mud.getChipConcentration().toString());
						if (mud.getDrillPipeAnnularVelocity()!=null) child.addProperty("drillPipeAnnularVelocity"+"_"+column, mud.getDrillPipeAnnularVelocity().toString());
						if (mud.getCciConcentration()!=null) child.addProperty("cciConcentration"+"_"+column, mud.getCciConcentration().toString());
						//if (mud.getHthpCake()!=null) child.addProperty("hthpCake"+"_"+column, mud.getHthpCake().toString());		
						if (mud.getHthpCake()!=null) child.addProperty("hthpCake"+"_"+column, this.formatDataWithUom(thisConverter, mud.getHthpCake(), MudProperties.class, "hthpCake", false, false));						
						if (mud.getH2sScavengerConc()!=null) child.addProperty("h2sScavengerConc"+"_"+column, this.formatDataWithUom(thisConverter, mud.getH2sScavengerConc(), MudProperties.class, "h2sScavengerConc", false, false));					
						
						break;
					}
				}else{
					child.addProperty("mudType"+"_p", "");
				}
			}
			
			//Mud System
			if (daily.getDailyUid()!=null){
				child = reportDataNode.addChild("MetalRecovery");		
				
				strSql = "FROM MudProperties WHERE (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid ORDER BY reportTime DESC";
				String[] paramsFieldBop = {"dailyUid"};
				Object[] paramsValueBop = {daily.getDailyUid()};
				List<MudProperties> resultMud = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldBop, paramsValueBop);
				
				if(resultMud.size()>0){
					MudProperties mp = (MudProperties) resultMud.get(0);
					if (mp.getMetalCuttingsMass()!=null) child.addProperty("metalCuttingsMass", mp.getMetalCuttingsMass().toString());
					if (mp.getCumMetalRecovery()!=null) child.addProperty("cumMetalRecovery", mp.getCumMetalRecovery().toString());
				}
				
				//Get Sum of Metal Recovery until Current Reporting Day In The Well
				strSql = "SELECT SUM(mpr.metalCuttingsMass) as totalMass FROM MudProperties mpr, Daily d WHERE (mpr.isDeleted = false or mpr.isDeleted is null) "
						+ "and (d.isDeleted = false or d.isDeleted is null) "
						+ "and mpr.dailyUid = d.dailyUid "
						+ "and d.dayDate <= :userdate "
						+ "and mpr.wellUid = :wellUid";
				String[] paramsFieldMetal = {"userdate", "wellUid"};
				Object[] paramsValueMetal = {daily.getDayDate(), daily.getWellUid()};
				List resultMetal = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldMetal, paramsValueMetal);
			    Object cumMetalRecoveryInWell = (Object) resultMetal.get(0);
			    if(cumMetalRecoveryInWell != null) child.addProperty("cumMetalRecoveryInWell", cumMetalRecoveryInWell.toString());
			    
			    //get Sum of Metal Recovery Daily
			    strSql = "SELECT SUM(metalCuttingsMass) FROM MudProperties WHERE (isDeleted = false or isDeleted is null) "
						+ "and dailyUid = :dailyUid "
						+ "and wellUid = :wellUid";
				String[] paramsFieldMetal1 = {"dailyUid", "wellUid"};
				Object[] paramsValueMetal1 = {daily.getDailyUid(), daily.getWellUid()};
				List resultMetal1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldMetal1, paramsValueMetal1);
			    Object cumMetalRecoveryInDay = (Object) resultMetal1.get(0);
			    if(cumMetalRecoveryInDay != null) child.addProperty("cumMetalRecoveryInDay", cumMetalRecoveryInDay.toString());
				
				child = reportDataNode.addChild("MudVolumes");
				Double cumSurfaceLossesSection = MudVolumeUtil.calculateCumulativeLossesVolumeFor(daily.getOperationUid(), daily.getDailyUid(), "loss", "loss_surface");
				if (cumSurfaceLossesSection!=null) child.addProperty("cumSurfaceLossesSection", cumSurfaceLossesSection.toString());
				
				Double cumSubsurfaceLossesSection = MudVolumeUtil.calculateCumulativeLossesVolumeFor(daily.getOperationUid(), daily.getDailyUid(), "loss", "loss_dh");
				if (cumSubsurfaceLossesSection!=null) child.addProperty("cumSubsurfaceLossesSection", cumSubsurfaceLossesSection.toString());
			}
			
			//Cement
			if (daily.getDailyUid()!=null){
						
				strSql = "FROM CementJob WHERE (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid ORDER BY jobStartDate DESC";
				String[] paramsFieldBop = {"dailyUid"};
				Object[] paramsValueBop = {daily.getDailyUid()};
				List<CementJob> resultCement = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldBop, paramsValueBop);
				
				if(resultCement.size()>0){
					CementJob c = (CementJob) resultCement.get(0);
					
					if (c!=null){
						strSql = "FROM CementItem WHERE (isDeleted = false or isDeleted is null) and cementJobUid = :cementJobUid ";
						String[] paramsFieldC = {"cementJobUid"};
						Object[] paramsValueC = {c.getCementJobUid()};
						List<CementItem> resultItem = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldC, paramsValueC);
						
						if (resultItem.size()>0){
							for (CementItem i : resultItem){
								child = reportDataNode.addChild("Cement");
								if(i.getItem()!=null){
									child.addProperty("item", getLookupValue(cementItemLookup,i.getItem()));
								}												
								
								child.addProperty("location", i.getLocation());
								child.addProperty("comment", i.getComments());
							}
						} 
					}
				}else{
					child = reportDataNode.addChild("Cement");
					child.addProperty("item", "");
					child.addProperty("location", "");
					child.addProperty("comment", "");
				}
			}
			
			//HSE
			if (daily.getDailyUid()!=null){
				child = reportDataNode.addChild("Hse");		
				
				strSql = "FROM BopLog WHERE (isDeleted = false or isDeleted is null) and wellUid = :wellUid AND testingDatetime <= :currentDate ORDER BY testingDatetime DESC";
				String[] paramsFieldBop = {"wellUid", "currentDate"};
				Object[] paramsValueBop = {daily.getWellUid(), thisCalendar.getTime()};
				List<BopLog> resultBop = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldBop, paramsValueBop);
				
				if(resultBop.size()>0){
					BopLog bop = (BopLog) resultBop.get(0);
					child.addProperty("bopLastTest", dateFormat.format(bop.getTestingDatetime()));
				}
				
				strSql = "FROM BopLog WHERE (isDeleted = false or isDeleted is null) and wellUid = :wellUid AND nextTestDatetime >= :currentDate ORDER BY nextTestDatetime DESC";
				String[] paramsFieldBop2 = {"wellUid", "currentDate"};
				Object[] paramsValueBop2 = {daily.getWellUid(), thisCalendar.getTime()};
				List<BopLog> resultBop2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldBop2, paramsValueBop2);
				
				if(resultBop2.size()>0){
					BopLog bop = (BopLog) resultBop2.get(0);
					child.addProperty("bopNextTest", dateFormat.format(bop.getNextTestDatetime()));
				}
				
				strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid AND hseEventdatetime <= :currentDate AND incidentCategory IN (SELECT hseCategory FROM LookupIncidentCategory WHERE (isDeleted = false OR isDeleted IS NULL) AND internalCode=:internalCode ) ORDER BY sequence";
				String[] paramsField = {"dailyUid", "currentDate", "internalCode"};
				Object[] paramsValue = {daily.getDailyUid(), thisCalendar.getTime(), "SMEET"};
				List<HseIncident> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if (result.size() > 0){
					String hseShortdescription = "";
					for (HseIncident hse : result){
						hseShortdescription = hseShortdescription + hse.getHseShortdescription() + "\n";
					}
					child.addProperty("hseShortdescription", hseShortdescription);
				}
				
				if (reportDailyList.size()>0){
					ReportDaily rd = (ReportDaily) reportDailyList.get(0);
					
					if(rd.getLastbopDate()!=null){
						double daysSinceLast = calculateDaysSince(daily.getDayDate(), rd.getLastbopDate());
						thisConverter.setReferenceMappingField(ReportDaily.class, "days_since_lta");
						thisConverter.setBaseValue(daysSinceLast);	
						
						Double day = thisConverter.getConvertedValue();
						child.addProperty("daysSinceLast", day.toString());
					}else{
						child.addProperty("daysSinceLast", "");
					}
				}		
				
				if(rigAcceptanceDate!=null){
					
					strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) and wellUid = :wellUid AND hseEventdatetime <= :currentDate AND hseEventdatetime >= :rigAcceptanceDate AND incidentCategory IN (SELECT hseCategory FROM LookupIncidentCategory WHERE (isDeleted = false OR isDeleted IS NULL) AND internalCode=:internalCode ) ORDER BY hseEventdatetime DESC";
					String[] paramsField1 = {"wellUid", "currentDate", "internalCode", "rigAcceptanceDate"};
					Object[] paramsValue1 = {daily.getWellUid(), thisCalendar.getTime(), "LTI", rigAcceptanceDate};
					List<HseIncident> resultLti = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField1, paramsValue1);
					
					if(resultLti.size()>0){
						HseIncident hse = (HseIncident) resultLti.get(0);
						child.addProperty("dateLastLTI", dateFormat.format(hse.getHseEventdatetime()));
						
						double daysLapsed = CommonUtil.getConfiguredInstance().calculateDaysSinceHseIncident(daily.getDayDate(), hse.getHseEventdatetime(), userSelection.getGroupUid());
						thisConverter.setReferenceMappingField(ReportDaily.class, "days_since_lta");
						thisConverter.setBaseValue(daysLapsed);	
						
						Double days = thisConverter.getConvertedValue();
						child.addProperty("daysSinceLastLTI", days.toString());
						
					}
					
					Object[] paramsValue2 = {daily.getWellUid(), thisCalendar.getTime(), "RECORD", rigAcceptanceDate};
					List<HseIncident> resultRecord = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField1, paramsValue2);
					
					if(resultRecord.size()>0){
						HseIncident hse = (HseIncident) resultRecord.get(0);
						if (hse.getNumberOfIncidents()!=null && hse.getNumberOfIncidents()!=0){
							child.addProperty("dateLastRecordableIncident", dateFormat.format(hse.getHseEventdatetime()));
							
							double daysLapsed = CommonUtil.getConfiguredInstance().calculateDaysSinceHseIncident(daily.getDayDate(), hse.getHseEventdatetime(), userSelection.getGroupUid());
							thisConverter.setReferenceMappingField(ReportDaily.class, "days_since_lta");
							thisConverter.setBaseValue(daysLapsed);	
							
							Double days = thisConverter.getConvertedValue();
							child.addProperty("daysSinceLastRecordableIncident", days.toString());
						}				
					}
					
					this.getHseNumberOfIncidentByRigAcceptanceDate(child, daily, thisCalendar, rigAcceptanceDate, "HIPO", "hiPo");
					this.getHseNumberOfIncidentByRigAcceptanceDate(child, daily, thisCalendar, rigAcceptanceDate, "SPILLS", "spills");
					this.getHseNumberOfIncidentByRigAcceptanceDate(child, daily, thisCalendar, rigAcceptanceDate, "RECORD", "recordable");
					this.getHseNumberOfIncidentByRigAcceptanceDate(child, daily, thisCalendar, rigAcceptanceDate, "WCIL1", "WCIL1");
					this.getHseNumberOfIncidentByRigAcceptanceDate(child, daily, thisCalendar, rigAcceptanceDate, "WCIL2", "WCIL2");
					this.getHseNumberOfIncidentByRigAcceptanceDate(child, daily, thisCalendar, rigAcceptanceDate, "WCIL3", "WCIL3");
					this.getHseNumberOfIncidentByRigAcceptanceDate(child, daily, thisCalendar, rigAcceptanceDate, "SAFE", "totalSafe");					
					this.getHseNumberOfIncidentByRigAcceptanceDate(child, daily, thisCalendar, rigAcceptanceDate, "UNSAFE", "totalUnsafe");		
					//this.getHseNumberOfIncidentByRigAcceptanceDate(child, daily, thisCalendar, rigAcceptanceDate, "OPEN", "open");		
				}
				
				this.getHseNumberOfIncidentByReportDate(child, daily, thisCalendar, "TOTAL", "totalNoAttendees");
				this.getHseNumberOfIncidentByReportDate(child, daily, thisCalendar, "JSA", "jsa");
				this.getHseNumberOfIncidentByReportDate(child, daily, thisCalendar, "STOP", "stop");				
				this.getHseNumberOfIncidentByReportDate(child, daily, thisCalendar, "SAFE", "safe");
				this.getHseNumberOfIncidentByReportDate(child, daily, thisCalendar, "UNSAFE", "unsafe");
				this.getHseNumberOfIncidentByReportDate(child, daily, thisCalendar, "OPEN", "open");
			}
		}
		
	}
	
	private String getLookupValue(Map<String,LookupItem> lookupList, Object lookupvalue) throws Exception {
		String retValue = "";
		
		if (lookupvalue !=null && StringUtils.isNotBlank(lookupvalue.toString())  && lookupList !=null){
			 LookupItem lookup = lookupList.get(lookupvalue.toString());
			if (lookup !=null)	retValue = lookup.getValue().toString();					
		} 
		
		return retValue;
	}
	
	private Double getConvertedUOM(CustomFieldUom thisConverter, Double value, String unit, Boolean offset) throws Exception{
		if (thisConverter!=null){
			thisConverter.setBaseValueFromUserValue(value);
			thisConverter.changeUOMUnit(unit);
			if (offset) thisConverter.addDatumOffset();
			
			return thisConverter.getConvertedValue();
		}

		return null;
	}
	
	private void getDrillingContractor(ReportDataNode child, Daily daily, String crewStatus, Integer count) throws Exception{
		String strSql = "SELECT crewCompany, crewPosition, SUM(COALESCE(pax,0.0)) FROM PersonnelOnSite WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid AND crewStatus=:crewStatus GROUP BY crewCompany, crewPosition";
		String[] paramsField = {"operationUid", "dailyUid", "crewStatus"};
		Object[] paramsValue = {daily.getOperationUid(), daily.getDailyUid(), crewStatus};
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		Integer counter = 1;
		
		if (result.size() > 0){				
			for (Object[] pob : result){
				
				if (counter==count){
					child.addProperty("column", crewStatus);
					
					if (pob[0]!=null){
						LookupCompany company = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, pob[0].toString());
						if (company!=null) child.addProperty("crewCompany"+crewStatus, company.getCompanyName());
					}else{
						child.addProperty("crewCompany"+crewStatus, "");
					}
					
					if (pob[1]!=null){
						child.addProperty("crewPosition"+crewStatus, pob[1].toString());
					}else{
						child.addProperty("crewPosition"+crewStatus, "");
					}
					
					if (pob[2]!=null){
						child.addProperty("pax"+crewStatus, pob[2].toString());
					}else{
						child.addProperty("pax"+crewStatus, "");
					}
				}		
				
				counter++;
			}	
			
		}
	}
	
	private Integer getMaxDrillingContractor(Daily daily, String crewStatus) throws Exception{	
		String strSql = "SELECT crewCompany, crewPosition, SUM(COALESCE(pax,0.0)) FROM PersonnelOnSite WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid AND crewStatus=:crewStatus GROUP BY crewCompany, crewPosition";
		String[] paramsField = {"operationUid", "dailyUid", "crewStatus"};
		Object[] paramsValue = {daily.getOperationUid(), daily.getDailyUid(), crewStatus};
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		
		if (result.size() > 0){				
				return result.size();
		}
		
		return 0;
	}
	
	private String getCommonLookup(String lookupTypeSelection, String shortCode) throws Exception{
		String lookupLabel = "";
		
		String strSql = "FROM CommonLookup Where (isDeleted is null or isDeleted = false) and lookupTypeSelection=:lookupTypeSelection AND shortCode=:shortCode ";
		String[] paramsField = {"lookupTypeSelection", "shortCode"};
		Object[] paramsValue = {lookupTypeSelection, shortCode};
		List<CommonLookup> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		
		if (result.size() > 0){
			for (CommonLookup cl : result){
				lookupLabel = cl.getLookupLabel();
			}
		}
		return lookupLabel;
	}
	
	private Double getRheologyReading(String mudPropertiesUid, Integer rpm) throws Exception{
		Double reading = null;
		
		String strSql = "FROM MudRheology Where (isDeleted is null or isDeleted = false) and mudPropertiesUid=:mudPropertiesUid AND rpm=:rpm";
		String[] paramsField = {"mudPropertiesUid", "rpm"};
		Object[] paramsValue = {mudPropertiesUid, rpm};
		List<MudRheology> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		
		if (result.size() > 0){
			reading = 0.0;
			
			for (MudRheology mr : result){
				if (mr.getRheologyReading()!=null) reading = reading + mr.getRheologyReading();
			}
		}
		
		return reading;
		
	}
	
	public Double calculateDaysSince(Date dayDate, Date hseIncidentEventDatetime) throws Exception {
		double usecLapsed = dayDate.getTime() - hseIncidentEventDatetime.getTime(); 
		double daysLapsedInBase = usecLapsed / 1000; 
		
		if (daysLapsedInBase < 0) {
			return 0.0;
		}
		
		return daysLapsedInBase;
	}
	
	private void getHseNumberOfIncidentByRigAcceptanceDate(ReportDataNode child , Daily daily, Calendar thisCalendar, Date rigAcceptanceDate, String internalCode, String field) throws Exception{
		String strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) and wellUid = :wellUid AND hseEventdatetime <= :currentDate AND hseEventdatetime >= :rigAcceptanceDate AND incidentCategory IN (SELECT hseCategory FROM LookupIncidentCategory WHERE (isDeleted = false OR isDeleted IS NULL) AND internalCode=:internalCode ) ORDER BY hseEventdatetime DESC";
		String[] paramsField = {"wellUid", "currentDate", "internalCode", "rigAcceptanceDate"};
		Object[] paramsValue = {daily.getWellUid(), thisCalendar.getTime(), internalCode, rigAcceptanceDate};
		List<HseIncident> result= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		
		if(result.size()>0){
			Integer numberOfIncident = 0;
			
			for (HseIncident hse : result){
				if (hse.getNumberOfIncidents()!=null) numberOfIncident = numberOfIncident + hse.getNumberOfIncidents();
			}
			
			child.addProperty(field, numberOfIncident.toString());
		}
	}
	
	private void getHseNumberOfIncidentByReportDate(ReportDataNode child , Daily daily, Calendar thisCalendar, String internalCode, String field) throws Exception{
		String strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) and wellUid = :wellUid AND hseEventdatetime >= :reportingDate AND hseEventdatetime <= :currentDate AND incidentCategory IN (SELECT hseCategory FROM LookupIncidentCategory WHERE (isDeleted = false OR isDeleted IS NULL) AND internalCode=:internalCode ) ORDER BY hseEventdatetime DESC";
		String[] paramsField = {"wellUid", "currentDate", "internalCode", "reportingDate"};
		Object[] paramsValue = {daily.getWellUid(), thisCalendar.getTime(), internalCode, daily.getDayDate()};
		List<HseIncident> result= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		
		if(result.size()>0){
			Integer numberOfIncident = 0;
			
			for (HseIncident hse : result){
				if (hse.getNumberOfIncidents()!=null) numberOfIncident = numberOfIncident + hse.getNumberOfIncidents();
			}
			
			child.addProperty(field, numberOfIncident.toString());
		}
	}
	
	private Integer getPax(String operationUid, String dailyUid, String crewCompany) throws Exception{
		Integer pax = 0;
		String strSql = "FROM PersonnelOnSite WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid AND crewCompany=:crewCompany ORDER BY sequence";
		String[] paramsField = {"operationUid", "dailyUid", "crewCompany"};
		Object[] paramsValue = {operationUid, dailyUid, crewCompany};
		List<PersonnelOnSite> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		
		if (result.size() > 0){
			
			for (PersonnelOnSite pob : result){
				if (pob.getPax()!=null){
					pax = pax + pob.getPax();
				}
			}
		}
		
		return pax;
	}
	
	private Double getNoStopCard(String operationUid, String dailyUid, String crewCompany) throws Exception{
		Double card = 0.00;
		String strSql = "FROM PersonnelOnSite WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid AND crewCompany=:crewCompany ORDER BY sequence";
		String[] paramsField = {"operationUid", "dailyUid", "crewCompany"};
		Object[] paramsValue = {operationUid, dailyUid, crewCompany};
		List<PersonnelOnSite> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		
		if (result.size() > 0){
			
			for (PersonnelOnSite pob : result){
				if (pob.getNoStopCards()!=null){
					card = card + pob.getNoStopCards();
				}
			}
		}
		
		return card;
	}
	
	private String formatDataWithUom(CustomFieldUom thisConverter, Double value, Class className, String fieldName, Boolean uom, Boolean space) throws Exception {
		if (value!=null) {
			if (thisConverter!=null) {
				thisConverter.setReferenceMappingField(className, fieldName);
				if (thisConverter.isUOMMappingAvailable()) {
					return thisConverter.formatOutputPrecision(value) + (space?" ":"") + (uom?thisConverter.getUomSymbol():"");
				}				
			}
			return String.valueOf(value);
		}		
		return "";
	}

}
