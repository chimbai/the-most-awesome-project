package com.idsdatanet.d2.drillnet.lookuprigstock;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.LookupRigStock;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LookupRigStockDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof LookupRigStock) {
			LookupRigStock lookuprigstock = (LookupRigStock)object;
			String stockType = (String) commandBean.getRoot().getDynaAttr().get("type");
			if (StringUtils.isNotBlank(stockType) && StringUtils.isBlank(lookuprigstock.getType())) {
				lookuprigstock.setType(stockType);
			}
			
			String stockcode = lookuprigstock.getStockCode();
			String storedunit = lookuprigstock.getStoredUnit();
			String stockbrand = lookuprigstock.getStockBrand();
			String type = lookuprigstock.getType();
			String lookupRigStockUid = lookuprigstock.getLookupRigStockUid();
			if (lookupRigStockUid==null) lookupRigStockUid = "";
			
			if (stockcode==null){
				String strSql = "from LookupRigStock where (isDeleted = false or isDeleted is null) and stockCode is null and storedUnit = :storedUnit and stockBrand = :stockBrand ";
				strSql = strSql + "AND type = :type AND lookupRigStockUid <> :lookupRigStockUid";
				String[] paramsFields = {"storedUnit", "stockBrand", "type", "lookupRigStockUid"};
				String[] paramsValues = {storedunit, stockbrand, type, lookupRigStockUid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				if (lstResult.size() > 0) {
					status.addError("Record already exist (name: " + stockbrand + ", unit: " + storedunit + ", type: " + type + ")");
					status.setContinueProcess(false, true);
				}
			}else{
				String strSql = "from LookupRigStock where (isDeleted = false or isDeleted is null) and stockCode = :stockCode and storedUnit = :storedUnit and stockBrand = :stockBrand ";
				strSql = strSql + "AND type = :type AND lookupRigStockUid <> :lookupRigStockUid";
				String[] paramsFields = {"stockCode", "storedUnit", "stockBrand", "type", "lookupRigStockUid"};
				String[] paramsValues = {stockcode, storedunit, stockbrand, type, lookupRigStockUid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				if (lstResult.size() > 0) {
					status.addError("Record already exist (name: " + stockbrand + ", unit: " + storedunit + ", type: " + type + ")");
					status.setContinueProcess(false, true);
				}
			}
		}
		
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		LookupRigStock lookuprigstock = (LookupRigStock) object;
		String operationType = (String) commandBean.getRoot().getDynaAttr().get("operationType");
		if (StringUtils.isNotBlank(operationType)) {
			lookuprigstock.setOperationCode(operationType);
		}
	
	}

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		if ("LookupRigStock".equals(meta.getTableClass().getSimpleName())) {
			String stockType = (String) commandBean.getRoot().getDynaAttr().get("type");
			if (StringUtils.isNotBlank(stockType)) {
				if (StringUtils.isNotBlank(conditionClause)) conditionClause += " AND "; 
				conditionClause += " type=:type ";
				query.addParam("type", stockType);
				
			}
			
			String operationType = (String) commandBean.getRoot().getDynaAttr().get("operationType");
			if (StringUtils.isNotBlank(operationType)) {
				if (StringUtils.isNotBlank(conditionClause)) conditionClause += " AND "; 
				conditionClause += " operationCode=:operationType ";
				query.addParam("operationType", operationType);
				
			}
			
			return conditionClause;
		}
		return null;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
