package com.idsdatanet.d2.drillnet.constant;

public class ReportTypes {
	public static final String REPORT_DDR = "DDR";
	public static final String REPORT_DGR = "DGR";
	public static final String REPORT_CMPLT = "CMPLT";
}
