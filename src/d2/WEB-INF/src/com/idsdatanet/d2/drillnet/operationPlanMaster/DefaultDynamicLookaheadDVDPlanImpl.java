package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.impl.cookie.DateUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.OperationPlanDetail;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.service.Manager;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DefaultDynamicLookaheadDVDPlanImpl implements DynamicLookaheadDVDPlan {

	@SuppressWarnings("unchecked")
	@Override
	public void processDVDPlanPhaseActualData(List<String> planReferenceUids) throws Exception {
		if(planReferenceUids == null || planReferenceUids.size() == 0) {
			return;
		}
		
		Manager dao = ApplicationUtils.getConfiguredInstance().getDaoManager();
		String queryString = "from OperationPlanPhase where (isDeleted is null or isDeleted = false) and operationPlanPhaseUid in (:planReferenceUids)";
		List<OperationPlanPhase> result = dao.findByNamedParam(queryString, "planReferenceUids", planReferenceUids);
		
		if(result == null || result.size() == 0) {
			return;
		}
		
		for(OperationPlanPhase rec : result) {
			calculateDvdPlanActualDatesAndDuration(rec);
			dao.saveObject(rec);
		}
	}
	
	private void calculateDvdPlanActualDatesAndDuration(OperationPlanPhase rec) throws Exception {
		if(rec == null || rec.getOperationPlanPhaseUid() == null) {
			return;
		}

		CustomFieldUom uomConverter = null;
		
		uomConverter = new CustomFieldUom(OperationPlanPhase.class, "duration");
		uomConverter.setBaseValue(this.calculateActualDuration(rec));
		rec.setDuration(uomConverter.getConvertedValue());
		
		uomConverter = new CustomFieldUom(OperationPlanPhase.class, "actualDepthMdMsl");
		uomConverter.setBaseValue(this.calculateActualDepth(rec));
		rec.setActualDepthMdMsl(uomConverter.getConvertedValue());

		rec.setActualStartDatetime(this.calculateActualStartDatetime(rec));
		rec.setActualEndDatetime(this.calculateActualEndDatetime(rec));
	}
	
	
	private Double calculateActualDuration(OperationPlanPhase record) throws Exception {
		return calculateActualDuration(record, record.getOperationUid());
	}
	
	private Double calculateActualDepth(OperationPlanPhase record) throws Exception {
		return calculateActualDepth(record, record.getOperationUid());
	}
	
	public Double calculateActualDuration(OperationPlanPhase record, String operationUid) throws Exception {
		Double actualDuration = 0.0;
		
		String operationPlanPhaseUid = record.getOperationPlanPhaseUid();
		String strSql = "SELECT sum(a.activityDuration) as totalActualTime from Activity a, Daily d " +
				"WHERE a.operationUid =:operationUid " +
				"AND a.planReference =:operationPlanPhaseUid " +
				"AND a.dailyUid =d.dailyUid " +
				"AND (a.isDeleted = false or a.isDeleted is null) " + 
				"AND (d.isDeleted = false or d.isDeleted is null) " + 
				"AND (a.dayPlus is null or a.dayPlus = 0) " +
				"AND (a.isSimop=false or a.isSimop is null) " +
				"AND (a.isOffline=false or a.isOffline is null)";
		String[] paramNames = {"operationUid", "operationPlanPhaseUid"};
		Object[] paramValues = {operationUid, operationPlanPhaseUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
		
		if(lstResult != null && lstResult.size() > 0 && lstResult.get(0) != null) {
			actualDuration = Double.parseDouble(lstResult.get(0).toString());
		}
		
		return actualDuration;
	}

	@Override
	public Double calculateActualDepth(OperationPlanPhase record, String operationUid) throws Exception {
		Double actualDepth = 0.0;
		
		String strSql2 = "SELECT a.depthMdMsl from Activity a, ReportDaily rd WHERE a.dailyUid = rd.dailyUid AND a.operationUid =:operationUid AND a.planReference =:operationPlanPhaseUid AND (a.dayPlus is null or a.dayPlus = 0) AND (a.isSimop is null or a.isSimop = false) AND (a.isOffline is null or a.isOffline = false) AND (a.isDeleted = false or a.isDeleted is null) AND (rd.isDeleted = false or rd.isDeleted is null) ORDER BY rd.reportDatetime DESC,a.endDatetime desc";
		String[] paramNames2 = {"operationUid", "operationPlanPhaseUid"};
		Object[] paramValues2 = {operationUid, record.getOperationPlanPhaseUid()};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramNames2, paramValues2);
		
		if(lstResult != null && lstResult.size() > 0 && lstResult.get(0) != null) {
			actualDepth = Double.parseDouble(lstResult.get(0).toString());
		}
		
		return actualDepth;
	}
	
	private Double calculateActualDurationTask(OperationPlanTask record) throws Exception {
		return calculateActualDurationTask(record, record.getOperationUid());
	}
	
	private Double calculateActualDepthTask(OperationPlanTask record) throws Exception {
		return calculateActualDepthTask(record, record.getOperationUid());
	}
	
	public Double calculateActualDurationTask(OperationPlanTask record, String operationUid) throws Exception {
		Double actualDuration = 0.0;
		
		String operationPlanTaskUid = record.getOperationPlanTaskUid();
		String strSql = "SELECT sum(a.activityDuration) as totalActualTime from Activity a, Daily d " +
				"WHERE a.operationUid =:operationUid " +
				"AND a.planTaskReference =:operationPlanTaskUid " +
				"AND a.dailyUid =d.dailyUid " +
				"AND (a.isDeleted = false or a.isDeleted is null) " + 
				"AND (d.isDeleted = false or d.isDeleted is null) " + 
				"AND (a.dayPlus is null or a.dayPlus = 0) " +
				"AND (a.isSimop=false or a.isSimop is null) " +
				"AND (a.isOffline=false or a.isOffline is null)";
		String[] paramNames = {"operationUid", "operationPlanTaskUid"};
		Object[] paramValues = {operationUid, operationPlanTaskUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
		
		if(lstResult != null && lstResult.size() > 0 && lstResult.get(0) != null) {
			actualDuration = Double.parseDouble(lstResult.get(0).toString());
		}
		
		return actualDuration;
	}

	@Override
	public Double calculateActualDepthTask(OperationPlanTask record, String operationUid) throws Exception {
		Double actualDepth = 0.0;
		
		String strSql2 = "SELECT a.depthMdMsl from Activity a, ReportDaily rd WHERE a.dailyUid = rd.dailyUid AND a.operationUid =:operationUid AND a.planTaskReference =:operationPlanTaskUid AND (a.dayPlus is null or a.dayPlus = 0) AND (a.isSimop is null or a.isSimop = false) AND (a.isOffline is null or a.isOffline = false) AND (a.isDeleted = false or a.isDeleted is null) AND (rd.isDeleted = false or rd.isDeleted is null) ORDER BY rd.reportDatetime DESC,a.endDatetime desc";
		String[] paramNames2 = {"operationUid", "operationPlanTaskUid"};
		Object[] paramValues2 = {operationUid, record.getOperationPlanTaskUid()};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramNames2, paramValues2);
		
		if(lstResult != null && lstResult.size() > 0 && lstResult.get(0) != null) {
			actualDepth = Double.parseDouble(lstResult.get(0).toString());
		}
		
		return actualDepth;
	}
	
	private Double calculateActualDurationDetail(OperationPlanDetail record) throws Exception {
		return calculateActualDurationDetail(record, record.getOperationUid());
	}
	
	public Double calculateActualDurationDetail(OperationPlanDetail record, String operationUid) throws Exception {
		Double actualDuration = 0.0;
		
		String operationPlanDetailUid = record.getOperationPlanDetailUid();
		String strSql = "SELECT sum(a.activityDuration) as totalActualTime from Activity a, Daily d " +
				"WHERE a.operationUid =:operationUid " +
				"AND a.planDetailReference =:operationPlanDetailUid " +
				"AND a.dailyUid =d.dailyUid " +
				"AND (a.isDeleted = false or a.isDeleted is null) " + 
				"AND (d.isDeleted = false or d.isDeleted is null) " + 
				"AND (a.dayPlus is null or a.dayPlus = 0) " +
				"AND (a.isSimop=false or a.isSimop is null) " +
				"AND (a.isOffline=false or a.isOffline is null)";
		String[] paramNames = {"operationUid", "operationPlanDetailUid"};
		Object[] paramValues = {operationUid, operationPlanDetailUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
		
		if(lstResult != null && lstResult.size() > 0 && lstResult.get(0) != null) {
			actualDuration = Double.parseDouble(lstResult.get(0).toString());
		}
		
		return actualDuration;
	}
	
	private Double calculateActualDepthDetail(OperationPlanDetail record) throws Exception {
		return calculateActualDepthDetail(record, record.getOperationUid());
	}
	
	@Override
	public Double calculateActualDepthDetail(OperationPlanDetail record, String operationUid) throws Exception {
		Double actualDepth = 0.0;
		
		String strSql2 = "SELECT a.depthMdMsl from Activity a, ReportDaily rd WHERE a.dailyUid = rd.dailyUid AND a.operationUid =:operationUid AND a.planDetailReference =:operationPlanDetailUid AND (a.dayPlus is null or a.dayPlus = 0) AND (a.isSimop is null or a.isSimop = false) AND (a.isOffline is null or a.isOffline = false) AND (a.isDeleted = false or a.isDeleted is null) AND (rd.isDeleted = false or rd.isDeleted is null) ORDER BY rd.reportDatetime DESC,a.endDatetime desc";
		String[] paramNames2 = {"operationUid", "operationPlanDetailUid"};
		Object[] paramValues2 = {operationUid, record.getOperationPlanDetailUid()};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramNames2, paramValues2);
		
		if(lstResult != null && lstResult.size() > 0 && lstResult.get(0) != null) {
			actualDepth = Double.parseDouble(lstResult.get(0).toString());
		}
		
		return actualDepth;
	}

	@Override
	public Date calculateActualStartDatetime(OperationPlanPhase record) throws Exception {
		Date actualStartDatetime = null;
		String query = "select min(a.startDatetime) as actualStartDatetime from Activity a, Daily d "
				+ "where (a.isDeleted is null or a.isDeleted = false) "
				+ "and (d.isDeleted is null or d.isDeleted = false) "
				+ "and (a.dailyUid = d.dailyUid) "
				+ "and (a.dayPlus is null or a.dayPlus = 0) "
				+ "and (a.isOffline is null or a.isOffline = false) "
				+ "and (a.isSimop=false or a.isSimop is null) "
				+ "and a.planReference = :planReference";
		String [] paramName = {"planReference"};
		Object [] paramValue = {record.getOperationPlanPhaseUid()}; 
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramName, paramValue);
		
		if(result != null && result.size() > 0 && result.get(0) != null) {
			actualStartDatetime = DateUtils.parseDate(result.get(0).toString(), new String[] {"yyyy-MM-dd HH:mm:ss"});
		}
		
		return actualStartDatetime;
	}

	@Override
	public Date calculateActualEndDatetime(OperationPlanPhase record) throws Exception {
		Date actualEndDatetime = null;
		String query = "select max(a.endDatetime) as actualEndDatetime from Activity a, Daily d "
				+ "where (a.isDeleted is null or a.isDeleted = false) "
				+ "and (d.isDeleted is null or d.isDeleted = false) "
				+ "and (a.dailyUid = d.dailyUid) "
				+ "and (a.dayPlus is null or a.dayPlus = 0) "
				+ "and (a.isOffline is null or a.isOffline = false) "
				+ "and (a.isSimop=false or a.isSimop is null) "
				+ "and a.planReference = :planReference";
		
		String [] paramName = {"planReference"};
		Object [] paramValue = {record.getOperationPlanPhaseUid()}; 
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramName, paramValue);
		
		if(result != null && result.size() > 0 && result.get(0) != null) {
			actualEndDatetime = DateUtils.parseDate(result.get(0).toString(), new String[] {"yyyy-MM-dd HH:mm:ss"});
		}
		
		return actualEndDatetime;
	}

	@Override
	public String calculateDayPlusMinus(Double actualDuration, Double plannedDuration) throws Exception {
		String dayplusminus = "0.0";
		if(actualDuration != null && plannedDuration != null) {
			DecimalFormat formatter = new DecimalFormat("#0.0");
			if (plannedDuration < actualDuration) {						
				dayplusminus = "-" + CommonUtils.roundUpFormat(formatter,Math.abs(actualDuration - plannedDuration));
			}
			else if (plannedDuration > actualDuration) {
				dayplusminus = "+" + CommonUtils.roundUpFormat(formatter,Math.abs(actualDuration - plannedDuration));
			}	
		}
		return dayplusminus;
	}

	@Override
	public Double calculateActualCost(OperationPlanPhase record, String operationUid, HttpServletRequest request) throws Exception {
		String strSql3 = "SELECT sum(a.activityDuration) as totalDuration, a.dailyUid FROM Activity a, Daily d " +
				"WHERE (a.isDeleted = false OR a.isDeleted is null) " +
				"AND (d.isDeleted = false OR d.isDeleted is null) " +
				"AND a.dailyUid = d.dailyUid " +
				"AND a.operationUid =:operationUid " +
				"AND a.planReference =:operationPlanPhaseUid " +
				"AND (a.isSimop=false or a.isSimop is null) " +
				"AND (a.isOffline=false or a.isOffline is null) " +
				"AND (a.dayPlus is null or a.dayPlus = 0) " +
				"GROUP BY a.dailyUid";
		String[] paramNames3 = {"operationUid", "operationPlanPhaseUid"};
		Object[] paramValues3 = {operationUid, record.getOperationPlanPhaseUid()};
						
		List<Object[]> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramNames3, paramValues3);
		
		Double actualCost = 0.00;
		
		if (lstResult3.size() > 0) {
			for (Object[] thisResult: lstResult3){						
				Double totalDuration = 0.00;
				Double dayCost = 0.00;
				String dailyUid = null;
				if (thisResult[0] != null){
					totalDuration = Double.parseDouble(thisResult[0].toString());
				}
				if (thisResult[1] != null){
					dailyUid = thisResult[1].toString();
				}
				
				if (request != null) {
					ReportDaily rd = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), dailyUid);						
					if (rd != null) {									
						if (rd.getDaycost() != null) dayCost = rd.getDaycost();
						
						double totalActivityDuration = 0.00;
						
						String strSql4 = "SELECT sum(activityDuration) as totalActivityDuration FROM Activity " +
								"WHERE dailyUid =:dailyUid " +
								"AND (isDeleted = false OR isDeleted is null) " +
								"AND (dayPlus is null or dayPlus = 0) " +
								"and (isOffline is null or isOffline = false) " +
								"AND (isSimop=false or isSimop is null) ";
						String[] paramNames4 = {"dailyUid"};
						Object[] paramValues4 = {dailyUid};
										
						List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramNames4, paramValues4);
						
						if (lstResult4.size() > 0) {
							if (lstResult4.get(0) != null) totalActivityDuration = Double.parseDouble(lstResult4.get(0).toString());										
						}
						
						if (totalDuration != null) {
							actualCost = actualCost + (dayCost/(totalActivityDuration/3600))*(totalDuration/3600); 
						}
					}
				}
			}
		}
		return actualCost;
	}
	
	public Double calculateNptDuration(String operationPlanPhaseUid, String operationUid) throws Exception {
		Double nptDuration = 0.0;
		
		String strSql = "SELECT sum(a.activityDuration) as totalActualTime from Activity a, Daily d " +
				"WHERE a.operationUid =:operationUid " +
				"AND a.planReference =:operationPlanPhaseUid " +
				"AND a.dailyUid =d.dailyUid " +
				"AND (a.internalClassCode = 'TP' or a.internalClassCode = 'TU') " +
				"AND (a.isDeleted = false or a.isDeleted is null) " + 
				"AND (d.isDeleted = false or d.isDeleted is null) " + 
				"AND (a.dayPlus is null or a.dayPlus = 0) " +
				"AND (a.isSimop=false or a.isSimop is null) " +
				"AND (a.isOffline=false or a.isOffline is null)";
		String[] paramNames = {"operationUid", "operationPlanPhaseUid"};
		Object[] paramValues = {operationUid, operationPlanPhaseUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
		
		if(lstResult != null && lstResult.size() > 0 && lstResult.get(0) != null) {
			nptDuration = Double.parseDouble(lstResult.get(0).toString());
		}
		
		return nptDuration;
	}
	
	public Double calculateNptDurationTask(String operationPlanTaskUid, String operationUid) throws Exception {
		Double nptDuration = 0.0;
		
		String strSql = "SELECT sum(a.activityDuration) as totalActualTime from Activity a, Daily d " +
				"WHERE a.operationUid =:operationUid " +
				"AND a.planTaskReference =:operationPlanTaskUid " +
				"AND a.dailyUid =d.dailyUid " +
				"AND (a.internalClassCode = 'TP' or a.internalClassCode = 'TU') " +
				"AND (a.isDeleted = false or a.isDeleted is null) " + 
				"AND (d.isDeleted = false or d.isDeleted is null) " + 
				"AND (a.dayPlus is null or a.dayPlus = 0) " +
				"AND (a.isSimop=false or a.isSimop is null) " +
				"AND (a.isOffline=false or a.isOffline is null)";
		String[] paramNames = {"operationUid", "operationPlanTaskUid"};
		Object[] paramValues = {operationUid, operationPlanTaskUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
		
		if(lstResult != null && lstResult.size() > 0 && lstResult.get(0) != null) {
			nptDuration = Double.parseDouble(lstResult.get(0).toString());
		}
		
		return nptDuration;
	}
	
	public Double calculateNptDurationDetail(String operationPlanDetailUid, String operationUid) throws Exception {
		Double nptDuration = 0.0;
		
		String strSql = "SELECT sum(a.activityDuration) as totalActualTime from Activity a, Daily d " +
				"WHERE a.operationUid =:operationUid " +
				"AND a.planDetailReference =:operationPlanDetailUid " +
				"AND a.dailyUid =d.dailyUid " +
				"AND (a.internalClassCode = 'TP' or a.internalClassCode = 'TU') " +
				"AND (a.isDeleted = false or a.isDeleted is null) " + 
				"AND (d.isDeleted = false or d.isDeleted is null) " + 
				"AND (a.dayPlus is null or a.dayPlus = 0) " +
				"AND (a.isSimop=false or a.isSimop is null) " +
				"AND (a.isOffline=false or a.isOffline is null)";
		String[] paramNames = {"operationUid", "operationPlanDetailUid"};
		Object[] paramValues = {operationUid, operationPlanDetailUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
		
		if(lstResult != null && lstResult.size() > 0 && lstResult.get(0) != null) {
			nptDuration = Double.parseDouble(lstResult.get(0).toString());
		}
		
		return nptDuration;
	}
}
