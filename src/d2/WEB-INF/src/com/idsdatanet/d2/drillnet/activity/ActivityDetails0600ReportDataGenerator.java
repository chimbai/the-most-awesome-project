package com.idsdatanet.d2.drillnet.activity;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class ActivityDetails0600ReportDataGenerator
  implements ReportDataGenerator
{
  public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation validation)
    throws Exception
  {
    QueryProperties qp = new QueryProperties();
    qp.setUomConversionEnabled(false);

    Boolean checkYesterday = false;
    Boolean checkTomorrow = false;

    Daily today = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
    Daily yesterday = ApplicationUtils.getConfiguredInstance().getYesterday(userContext.getUserSelection().getOperationUid(), userContext.getUserSelection().getDailyUid());
    Daily tomorrow = ApplicationUtils.getConfiguredInstance().getTomorrow(userContext.getUserSelection().getOperationUid(), userContext.getUserSelection().getDailyUid());

    Date todayDate = null;
    if(today!=null && today.getDayDate()!=null)
    	todayDate = today.getDayDate();
    
    Calendar thisCalendar = Calendar.getInstance();
    if (yesterday != null)
    {
    	Date previousDate = null;
    	 Date yesterdayDate = null;
    	
    	if(todayDate!=null){
    		thisCalendar.setTime(todayDate);
    	    thisCalendar.add(Calendar.DATE, -1);
    	    thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
    	    thisCalendar.set(Calendar.MINUTE, 0);
    	    thisCalendar.set(Calendar.SECOND , 0);
    	    thisCalendar.set(Calendar.MILLISECOND , 0);
    	    previousDate = thisCalendar.getTime();
    	}
      
    	if(yesterday.getDayDate()!=null){
    		yesterdayDate = yesterday.getDayDate();
    		thisCalendar.setTime(yesterdayDate);
    		thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
    		thisCalendar.set(Calendar.MINUTE, 0);
    		thisCalendar.set(Calendar.SECOND , 0);
    		thisCalendar.set(Calendar.MILLISECOND , 0);
    		yesterdayDate = thisCalendar.getTime();
    	}
    	
		if ((yesterdayDate != null) && (yesterdayDate.compareTo(previousDate) == 0)) {
			checkYesterday = true;
		}
    }

    if (tomorrow != null)
    {
    	Date nextDayDate = null;
    	Date tomorrowDate = null;
    	
    	if(todayDate!=null){
    		thisCalendar.setTime(todayDate);
    		thisCalendar.add(Calendar.DATE, +1);
    		thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
    		thisCalendar.set(Calendar.MINUTE, 0);
    		thisCalendar.set(Calendar.SECOND , 0);
    		thisCalendar.set(Calendar.MILLISECOND , 0);
    		nextDayDate = thisCalendar.getTime();
    	}
		
		if (tomorrow.getDayDate()!=null){
			tomorrowDate = tomorrow.getDayDate();
			thisCalendar.setTime(tomorrowDate);
			thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
			thisCalendar.set(Calendar.MINUTE, 0);
			thisCalendar.set(Calendar.SECOND , 0);
			thisCalendar.set(Calendar.MILLISECOND , 0);
			tomorrowDate = thisCalendar.getTime();
		}
		
		
		if((tomorrowDate != null) && (tomorrowDate.compareTo(nextDayDate) == 0)) {
			checkTomorrow = true;
		}
    }

    String todayStatement = "";
    String dayPlusStatement = "";
    String tmrStatement = "";
    Boolean dateIncrement = false;
    List lstResult = null;

    if (checkYesterday) {
      todayDate.setHours(6);
      todayStatement = "AND startDatetime >= :todayTime ";
    }

    String strSqlToday = "FROM Activity WHERE (dayPlus IS NULL OR dayPlus=0) AND (dailyUid = :todayUid) " + (
      yesterday != null ? todayStatement : "") + 
      "AND (isDeleted = false or isDeleted is null) " + 
      "AND (isSimop <> 1 OR isSimop IS NULL) " + 
      "AND (isOffline <> 1 OR isOffline IS NULL) " + 
      "ORDER BY startDatetime, endDatetime";

    if (checkYesterday) {
      String[] paramsFields = { "todayUid", "todayTime" };
      Object[] paramsValues = { today.getDailyUid(), todayDate };
      lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlToday, paramsFields, paramsValues, qp);
    } else {
      String[] paramsFields = { "todayUid" };
      Object[] paramsValues = { today.getDailyUid() };
      lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlToday, paramsFields, paramsValues, qp);
    }

    if (lstResult!=null && lstResult.size() > 0) {
      createProperties(userContext, reportDataNode, lstResult, dateIncrement);
    }

    if (checkTomorrow) {
      tmrStatement = "AND (dayPlus IS NULL OR dayPlus=0) ";
      tmrStatement = tmrStatement + "AND (dailyUid = :tmrUid)";
      tmrStatement = tmrStatement + "AND startDatetime < :tmrTime ";
    } else {
      dayPlusStatement = "AND (dayPlus=1) ";
      dayPlusStatement = dayPlusStatement + "AND (dailyUid = :todayUid) ";
      dayPlusStatement = dayPlusStatement + "AND startDatetime < :todayTime ";
      dateIncrement = true;
    }

    String strSqlTomorrow = "FROM Activity WHERE (isDeleted = false or isDeleted is null) AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL) " + (
      checkTomorrow.booleanValue() ? tmrStatement : dayPlusStatement) + 
      "ORDER BY startDatetime, endDatetime";

    if (checkTomorrow) {
      Date tmrDate = tomorrow.getDayDate();
      tmrDate.setHours(6);

      String[] paramsFields2 = { "tmrUid", "tmrTime" };
      Object[] paramsValues2 = { tomorrow.getDailyUid(), tmrDate };
      lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlTomorrow, paramsFields2, paramsValues2, qp);
    }
    else {
      String[] paramsFields2 = { "todayUid", "todayTime" };
      Object[] paramsValues2 = { today.getDailyUid(), todayDate };
      lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlTomorrow, paramsFields2, paramsValues2, qp);
    }

    if (lstResult.size() > 0)
      createProperties(userContext, reportDataNode, lstResult, dateIncrement);
  }

  public void disposeOnDataGeneratorExit()
  {
  }

  private void createProperties(UserContext userContext, ReportDataNode reportDataNode, List<Activity> resullt, Boolean dateIncrement)
    throws Exception
  {
    CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "duration");

    for (Activity thisActivity : resullt) {
    	Date startDatetime = null;
    	Long epochStart = null;
    	if(thisActivity.getStartDatetime()!=null){
    		startDatetime = thisActivity.getStartDatetime();
    		epochStart = Long.valueOf(startDatetime.getTime());
    	}
		
    	Date endDatetime = null;
    	Long epochEnd = null;
    	if(thisActivity.getEndDatetime()!=null){
    		endDatetime = thisActivity.getEndDatetime();
    		epochEnd = Long.valueOf(endDatetime.getTime());
    	}
		
		
		if (dateIncrement) {
			epochStart = Long.valueOf(epochStart.longValue() + 86400000L);
			epochEnd = Long.valueOf(epochEnd.longValue() + 86400000L);
		}
		
		String taskCode = null;
		if(thisActivity.getTaskCode()!=null){
			taskCode = thisActivity.getTaskCode().toString();
		}
		
		Double activityDuration = null;
		if(thisActivity.getActivityDuration()!=null){
			activityDuration = thisActivity.getActivityDuration();
			thisConverter.setBaseValue(activityDuration.doubleValue());
			activityDuration = Double.valueOf(thisConverter.getConvertedValue());
		}
		
		String activityDescription = null;
		if(thisActivity.getActivityDescription()!=null){
			activityDescription = thisActivity.getActivityDescription().toString();
		}
		
		ReportDataNode thisReportNode = reportDataNode.addChild("ActivityDetails");
		thisReportNode.addProperty("code", taskCode);
		thisReportNode.addProperty("duration", activityDuration.toString());
		thisReportNode.addProperty("description", activityDescription);
		thisReportNode.addProperty("start", epochStart.toString());
		thisReportNode.addProperty("end", epochEnd.toString());
		}
	}
}