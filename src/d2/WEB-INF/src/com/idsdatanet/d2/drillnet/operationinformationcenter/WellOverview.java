package com.idsdatanet.d2.drillnet.operationinformationcenter;

import java.util.Date;

public class WellOverview implements java.io.Serializable {
	
	private String wellUid;
	private String wellName;
	private String operationUid;
	private String field;
	private String previousWellName;
	private Date daydate;
	private String dailyUid;
	private Double downholeWaterLevelMdMsl;
	private Double annulusPressure;
	private Double waterProduction;
	private Double gasProduction;
	private String downtimeComment;
	private String generalComment;
	private Double downholeGaugeReading;
	private Double pumpSubmergenceMdMsl;
	private Double volumetricEfficiency;
	private Double oilProductionVolume;
	private Double oilCut;
	private Double totalOilWaterProd;
	private Double topDepthMdMsl;
	private Double temperature;
	private Double downholeFluidLevelMdMsl;
	private Double waterInjectionVolume;
	private String opsComment;
	
	public WellOverview(String wellUid, String wellName, String previousWellName, String field) {
		this.wellUid = wellUid;
		this.wellName = wellName;
		this.previousWellName = previousWellName;
		this.field = field;	
	}
	
	public void setWellUid(String wellUid) {
		this.wellUid = wellUid;
	}
	public String getWellUid() {
		return wellUid;
	}
	
	public void setOperationUid(String operationUid) {
		this.operationUid = operationUid;
	}
	public String getOperationUid() {
		return operationUid;
	}
	
	public void setPreviousWellName(String previousWellName) {
		this.previousWellName = previousWellName;
	}
	public String getPreviousWellName() {
		return previousWellName;
	}
	
	public void setField(String field) {
		this.field = field;
	}
	public String getField() {
		return field;
	}
	
	public void setWellName(String wellName) {
		this.wellName = wellName;
	}
	public String getWellName() {
		return wellName;
	}
	
	public void setDaydate(Date daydate) {
		this.daydate = daydate;
	}
	public Date getDaydate() {
		return daydate;
	}
	
	public void setDailyUid(String dailyUid) {
		this.dailyUid = dailyUid;
	}
	public String getDailyUid() {
		return dailyUid;
	}
	
	public void setDownholeWaterLevelMdMsl(Double downholeWaterLevelMdMsl) {
		this.downholeWaterLevelMdMsl = downholeWaterLevelMdMsl;
	}
	public Double getDownholeWaterLevelMdMsl() {
		return downholeWaterLevelMdMsl;
	}

	public void setAnnulusPressure(Double annulusPressure) {
		this.annulusPressure = annulusPressure;
	}
	public Double getAnnulusPressure() {
		return annulusPressure;
	}
	
	public void setWaterProduction(Double waterProduction) {
		this.waterProduction = waterProduction;
	}
	public Double getWaterProduction() {
		return waterProduction;
	}
	
	public void setGasProduction(Double gasProduction) {
		this.gasProduction = gasProduction;
	}
	public Double getGasProduction() {
		return gasProduction;
	}

	public void setDowntimeComment(String downtimeComment) {
		this.downtimeComment = downtimeComment;
	}
	public String getDowntimeComment() {
		return downtimeComment;
	}
	public void setGeneralComment(String generalComment) {
		this.generalComment = generalComment;
	}
	public String getGeneralComment() {
		return generalComment;
	}
	
	public void setDownholeGaugeReading(Double downholeGaugeReading) {
		this.downholeGaugeReading = downholeGaugeReading;
	}
	public Double getDownholeGaugeReading() {
		return downholeGaugeReading;
	}
	
	public void setpumpSubmergenceMdMsl(Double pumpSubmergenceMdMsl) {
		this.pumpSubmergenceMdMsl = pumpSubmergenceMdMsl;
	}
	public Double getpumpSubmergenceMdMsl() {
		return pumpSubmergenceMdMsl;
	}
	
	public void setVolumetricEfficiency(Double volumetricEfficiency) {
		this.volumetricEfficiency = volumetricEfficiency;
	}
	public Double getVolumetricEfficiency() {
		return volumetricEfficiency;
	}
	
	public void setOilCut(Double oilCut) {
		this.oilCut = oilCut;
	}
	public Double getOilCut() {
		return oilCut;
	}
	
	public void setOilProductionVolume(Double oilProductionVolume) {
		this.oilProductionVolume = oilProductionVolume;
	}
	public Double getOilProductionVolume() {
		return oilProductionVolume;
	}
	
	public void setTotalOilWaterProd(Double totalOilWaterProd) {
		this.totalOilWaterProd = totalOilWaterProd;
	}
	public Double getTotalOilWaterProd() {
		return totalOilWaterProd;
	}
	
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	public Double getTemperature() {
		return temperature;
	}
	
	public void setDownholeFluidLevelMdMsl(Double downholeFluidLevelMdMsl) {
		this.downholeFluidLevelMdMsl = downholeFluidLevelMdMsl;
	}
	public Double getDownholeFluidLevelMdMsl() {
		return downholeFluidLevelMdMsl;
	}
	
	public void setTopDepthMdMsl(Double topDepthMdMsl) {
		this.topDepthMdMsl = topDepthMdMsl;
	}
	public Double getTopDepthMdMsl() {
		return topDepthMdMsl;
	}
	
	public void setWaterInjectionVolume(Double waterInjectionVolume) {
		this.waterInjectionVolume = waterInjectionVolume;
	}
	public Double getWaterInjectionVolume() {
		return waterInjectionVolume;
	}
	
	public void setOpsComment(String opsComment) {
		this.opsComment = opsComment;
	}
	public String getOpsComment() {
		return opsComment;
	}

}
