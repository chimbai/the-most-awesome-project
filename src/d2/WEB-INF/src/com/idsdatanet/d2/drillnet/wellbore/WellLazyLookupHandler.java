package com.idsdatanet.d2.drillnet.wellbore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.flex.FlexClientAdaptorForCommandBean;

public class WellLazyLookupHandler implements LookupHandler {
	private static final String LAZY_LOOKUP_START_KEY = "_start_with_key";
	
	private Boolean inStringSearch = false;
	
	public void setInStringSearch(Boolean value) {
		this.inStringSearch = value;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
	
		LinkedHashMap<String, LookupItem> results = new LinkedHashMap<String, LookupItem>();
		List<LookupItem> lookupItems = new ArrayList<LookupItem>();
		QueryProperties qp = new QueryProperties();
		qp.setRowsToFetch(0, FlexClientAdaptorForCommandBean.LAZY_LOAD_LOOKUP_ROW_TO_FETCH * 3);
		List<Well> list = null;
		
		String queryString ="FROM Well where (isDeleted=false or isDeleted is null) ";

		if (request != null) {
			String startKey = request.getParameter(LAZY_LOOKUP_START_KEY);
			
			if (startKey!=null) {
					
				queryString +=  "and wellName like :wellName order by wellName";
				String paramNames[] = {"wellName"};
				Object paramValues[] = {(inStringSearch?"%":"") + startKey.trim() + "%"};
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
				
				int counter = 0; 
				for (Well well : list) {
					String label = well.getWellName();
					
					if (this.inStringSearch) {
						if (label.toLowerCase().indexOf(startKey.toLowerCase())>=0) {
							lookupItems.add(new LookupItem(well.getWellUid(), label));
							counter++;
						}
					} else {
						if (label.toLowerCase().indexOf(startKey.toLowerCase())==0) {
							lookupItems.add(new LookupItem(well.getWellUid(), label));
							counter++;
						}
					}
					
					if (FlexClientAdaptorForCommandBean.LAZY_LOAD_LOOKUP_ROW_TO_FETCH <= counter) break;
				}
				
			} else {
				if (node!=null) {
					String wellUid = null;
					if (node.getData() instanceof Wellbore) { //for wellboreCommandBean
						wellUid = ((Wellbore) node.getData()).getWellUid();
					} else if (node.getData() instanceof Operation) {
						wellUid = ((Operation) node.getData()).getWellUid();
					}
					if (wellUid!=null) {
						queryString += " AND wellUid=:wellUid";
						list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "wellUid", wellUid);
						
						for (Well well : list) {
							lookupItems.add(new LookupItem(well.getWellUid(), well.getWellName()));
							
						}
					}
				}
				
			}
		}
	
		
		Collections.sort(lookupItems, new LookupItemComparator());
		for (LookupItem lookupItem : lookupItems) {
			results.put(lookupItem.getKey(), lookupItem);
		}
		return results;
	}
	
	private class LookupItemComparator implements Comparator<LookupItem> {
		
		public int compare(LookupItem v1, LookupItem v2) {
			if (v1 == null || v1.getValue() == null || v2 == null || v2.getValue() == null || StringUtils.equals((String)v1.getValue(), (String)v2.getValue())) {
				return 0;
			}
			return WellNameUtil.getPaddedStr((String)v1.getValue()).compareTo(WellNameUtil.getPaddedStr((String)v2.getValue()));
		}
	}
}
