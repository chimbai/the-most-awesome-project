package com.idsdatanet.d2.drillnet.activity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LookupClassCode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ActivityEditorDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private static final String DYN_ATTR_ACTIVITY_DATE = "activityDate";
	
	private class ReversedDateComparator implements Comparator<java.util.Date>{
		public int compare(Date d1, Date d2){
			if(d1 != null && d2 != null){
				if(d1.getTime() > d2.getTime()) return -1;
				if(d1.getTime() < d2.getTime()) return 1;
				return 0;
			}else{
				if(d1 == null && d2 == null) return 0;
				if(d1 != null) return -1;
				return 1;
			}
		}
	}
	
	private void updateCompanyWarning(CommandBeanTreeNode node, DataNodeProcessStatus status, UserSession session) throws Exception {
		String classCode = null;
		String internalCode = null;
		String company = null;
		if(node.getData() instanceof Activity){
			Activity activity = (Activity) node.getData();
			classCode = activity.getClassCode();
			internalCode = this.getInternalCode(classCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
			company = activity.getLookupCompanyUid();
		}
		if (("TP".equalsIgnoreCase(internalCode) || "TU".equalsIgnoreCase(internalCode)) && (StringUtils.isBlank(company))) {
			//set the message here
			node.getNodeUserMessages().clearFieldMessages("lookupCompanyUid");
			node.getDynaAttr().put("lookupCompanyUid", "Activity records that has been classified as TROUBLE must have the related company documented.");
			node.getNodeUserMessages().addFieldError("lookupCompanyUid", "Activity records that has been classified as TROUBLE must have the related company documented.");
			status.setContinueProcess(false, true);
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {		
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		Calendar activityDate = Calendar.getInstance();
		
		if(object instanceof Activity) {
			
			//check if GWP set to when code is Trouble, user must select a company
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "actMustEnterCompanyIfTrouble"))){
				this.updateCompanyWarning(node, status, session);
			}				
			
			String activityUid = (String) PropertyUtils.getProperty(object, "activityUid");
			if ("".equals(activityUid)) {
				PropertyUtils.setProperty(object, "activityUid", null);
			}
			
			Date thisActivityDate = null;
			if(node.getDynaAttr().get("activityDate") != null)
			{
				if(CommonDateParser.parse(node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE).toString()) != null)
				{
					thisActivityDate = CommonDateParser.parse(node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE).toString());
				}
				else
				{
					thisActivityDate = (Date) node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE);
				}
			}else{
				
				String dailyUid = (String) PropertyUtils.getProperty(object, "dailyUid");
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
				if (daily != null)
					thisActivityDate = daily.getDayDate();
					
			}
			
			//GET ACTIVITY DATE TO ASSIGN DAILYUID
			if(thisActivityDate != null)
			{
				activityDate.setTime(thisActivityDate);
				
				//STAMP STARTDATETIME & ENDDATETIME WITH CURRENT ACTIVITY DATE
				if(PropertyUtils.getProperty(object, "startDatetime") != null)
				{
					Calendar startDatetime = Calendar.getInstance();
					startDatetime.setTime((Date) PropertyUtils.getProperty(object, "startDatetime"));
					startDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
					startDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
					startDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
					PropertyUtils.setProperty(object, "startDatetime", startDatetime.getTime());
				}
													
				Calendar endDatetime = Calendar.getInstance();
				endDatetime.setTime((Date) PropertyUtils.getProperty(object, "endDatetime"));
				endDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
				endDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
				endDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
				PropertyUtils.setProperty(object, "endDatetime", endDatetime.getTime());
				
				D2ApplicationEvent.refresh();
				
			}else{

				//STAMP STARTDATETIME & ENDDATETIME WITH CURRENT ACTIVITY DATE
				if(PropertyUtils.getProperty(object, "startDatetime") != null)
				{
					Calendar startDatetime = Calendar.getInstance();
					startDatetime.setTime((Date) PropertyUtils.getProperty(object, "startDatetime"));
					startDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
					startDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
					startDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
					PropertyUtils.setProperty(object, "startDatetime", startDatetime.getTime());
				}
													
				Calendar endDatetime = Calendar.getInstance();
				endDatetime.setTime((Date) PropertyUtils.getProperty(object, "endDatetime"));
				endDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
				endDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
				endDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
				PropertyUtils.setProperty(object, "endDatetime", endDatetime.getTime());
				
				D2ApplicationEvent.refresh();
			}
			
			List<Date> endDatetimeList = new ArrayList<Date>();
			
			// get parent node and loop through child to get each end datetime
			CommandBeanTreeNode parentNode = node.getParent();
			for (Iterator i = parentNode.getList().values().iterator(); i.hasNext();) {
				Map items = (Map) i.next();
				
				for (Iterator j = items.values().iterator(); j.hasNext();) {
					CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
					if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(object.getClass())) {
						Boolean isDeleted = (Boolean) PropertyUtils.getProperty(childNode.getData(), "isDeleted"); // shouldn't need this, temporary fix for Flex client
						if (isDeleted == null || !isDeleted.booleanValue()) {
							endDatetimeList.add((Date) PropertyUtils.getProperty(childNode.getData(), "endDatetime"));
						}
					}
				}
			}
			Collections.sort(endDatetimeList, new ReversedDateComparator());
			commandBean.getRoot().getDynaAttr().put("endDatetime", endDatetimeList);
			
			Date start = (Date) PropertyUtils.getProperty(object, "startDatetime");
			Date end = (Date) PropertyUtils.getProperty(object, "endDatetime");
			
			// if user didn't enter start time, find the first smaller than the current end time.
			if(start == null) {
				DateFormat df = new SimpleDateFormat("kkmmss");
				
				boolean found = false;
				for(Date endDatetime : endDatetimeList) {
					Calendar currentEndDatetime = Calendar.getInstance();
					currentEndDatetime.setTime(end);
					
					Calendar cachedEndDatetime = Calendar.getInstance();
					cachedEndDatetime.setTime(endDatetime);
					
					if(df.format(cachedEndDatetime.getTime()).compareTo(df.format(currentEndDatetime.getTime()))==-1) {
						
						if(thisActivityDate!=null){
							activityDate.setTime(thisActivityDate);
						}
						cachedEndDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
						cachedEndDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
						cachedEndDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
						
												
						PropertyUtils.setProperty(object, "startDatetime", cachedEndDatetime.getTime());
						found = true;
						break;
					}
				}
				// if no matches, set to 00:00
				if(!found) {
					Calendar zero = Calendar.getInstance();
					zero.clear();
					zero.set(Calendar.HOUR_OF_DAY, 0);
					zero.set(Calendar.MINUTE, 0);
					zero.set(Calendar.SECOND, 0);
					zero.set(Calendar.MILLISECOND, 0);
					
					
					if(thisActivityDate!=null){
						activityDate.setTime(thisActivityDate);
					}
					zero.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
					zero.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
					zero.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
					
										
					PropertyUtils.setProperty(object, "startDatetime", zero.getTime());
				}
				start = (Date) PropertyUtils.getProperty(object, "startDatetime");
			}
			
			// check if start time greater than end time.
			if(start != null && end != null){
				if(start.getTime() > end.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "startDatetime", "Start time can not be greater than end time.");
					return;
				}
				
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
				
				thisConverter.setBaseValue(thisDuration);
				
				PropertyUtils.setProperty(object, "activityDuration", (thisConverter.getConvertedValue()));
			}
			
			//fix case where if first the record is select trouble, then user put RC, then if change to program, RC is hide then when save RC value still in data
			//clear RC record and company record if user select program or unprogram
			String classCode = (String) PropertyUtils.getProperty(object, "classCode");
			String internalCode = this.getInternalCode(classCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
			PropertyUtils.setProperty(object, "internalClassCode", internalCode);
			if (StringUtils.equalsIgnoreCase("P", internalCode) || StringUtils.equalsIgnoreCase("U", internalCode))
			{
				PropertyUtils.setProperty(object, "rootCauseCode", null);
				PropertyUtils.setProperty(object, "lookupCompanyUid", null);
				
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {		
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {

		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		
		if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
			
			//TO OBTAIN THESE FILTER VALUE FROM USER SELECTION
			String selectedStartDailyUid  = (String) commandBean.getRoot().getDynaAttr().get("startdate");
			String selectedEndDailyUid  = (String) commandBean.getRoot().getDynaAttr().get("enddate");
			String selectedClassCode  = (String) commandBean.getRoot().getDynaAttr().get("classCodeLookup");
			String selectedPhaseCode  = (String) commandBean.getRoot().getDynaAttr().get("phaseCodeLookup");
			
			Daily startDayDate = ApplicationUtils.getConfiguredInstance().getCachedDaily(selectedStartDailyUid);
			if (startDayDate == null) return conditionClause.replace(CUSTOM_CONDITION_MARKER, "");
			Date startDate = startDayDate.getDayDate();
			
			Daily endDayDate = ApplicationUtils.getConfiguredInstance().getCachedDaily(selectedEndDailyUid);
			if (endDayDate == null) return conditionClause.replace(CUSTOM_CONDITION_MARKER, "");
			Date endDate = endDayDate.getDayDate();
			
			Calendar selectedEndDate = Calendar.getInstance();
			selectedEndDate.setTime(endDate);
			selectedEndDate.set(Calendar.HOUR_OF_DAY, 23);
			selectedEndDate.set(Calendar.MINUTE, 59);
			selectedEndDate.set(Calendar.SECOND , 59);
			selectedEndDate.set(Calendar.MILLISECOND , 59);
			endDate = selectedEndDate.getTime();
			
			//DATE CHECKING, IF END-DATE IS EARLIER THAN START DATE
			if (endDate.before(startDate)){	
				commandBean.getSystemMessage().addWarning("End date cannot be earlier than Start date.\nPlease re-select.");				
			}
			
			//IF SELECTED CLASS CODE IS EMPTY,USER WILL STILL ABLE TO QUERY THE RESULT. THIS QUERY WILL LOAD ALL THE CLASS CODE FROM LOOKUPCLASSCODE
			if (selectedClassCode == null || StringUtils.isEmpty(selectedClassCode)){
				String strSql2 = "FROM LookupClassCode WHERE (isDeleted=false OR isDeleted is null) AND (operationCode = :thisOperationCode or operationCode='') GROUP BY shortCode";
				String[] paramsFields2 = {"thisOperationCode"};
				Object[] paramsValues2 = {userSelection.getOperationType()};
				
				List listClassCodes = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				
				if (listClassCodes.size() > 0){
					
					for(Object listClassCode : listClassCodes){
						LookupClassCode thisLookupClassCode = (LookupClassCode) listClassCode;
						if(selectedClassCode == null || selectedClassCode == ""){
							//TO FORM SELECTED-CLASS-CODE SQL STATEMENT
							selectedClassCode = "'" + thisLookupClassCode.getShortCode() +"',";
						}
						else{
							selectedClassCode = selectedClassCode + "'" + thisLookupClassCode.getShortCode() + "',";
						}						
					}
					selectedClassCode = StringUtils.substring(selectedClassCode, 0, -1);
					selectedClassCode = StringUtils.upperCase(selectedClassCode);
				}
			}
			//OTHERWISE IT WILL GET THE CLASS CODE BASED ON USER SELECTION
			else{
				selectedClassCode = "'" + selectedClassCode + "'";
			}
			
			customCondition = "AND classCode IN (" + selectedClassCode + ") AND (startDatetime >=:startDate AND endDatetime <=:endDate) " +
					(StringUtils.isNotBlank(selectedPhaseCode) ? " AND phaseCode=:selectedPhaseCode " : "");
			query.addParam("startDate", startDate);
			query.addParam("endDate", endDate);
			if(StringUtils.isNotBlank(selectedPhaseCode)) {
				query.addParam("selectedPhaseCode", selectedPhaseCode);
			}
		}
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {

		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {

		return null;
	}
		
	private String getInternalCode(String classCode, String groupUid, String operationCode) throws Exception{
		
		String internalCode = "";
		
		String strSql = "SELECT internalCode FROM LookupClassCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisClassCode AND (operationCode = :thisOperationCode OR operationCode = '') AND groupUid = :thisGroupUid";
		String[] paramsFields = {"thisClassCode", "thisGroupUid", "thisOperationCode"};
		Object[] paramsValues = {classCode, groupUid, operationCode};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			Object internalCodeResult = (Object) lstResult.get(0);
			if (internalCodeResult != null) internalCode = internalCodeResult.toString();
		}
		if (StringUtils.isBlank(internalCode)) internalCode = classCode;
		
		return internalCode;
	}
}
