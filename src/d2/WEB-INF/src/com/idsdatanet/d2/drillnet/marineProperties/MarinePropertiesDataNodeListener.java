package com.idsdatanet.d2.drillnet.marineProperties;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.MarineProperties;
import com.idsdatanet.d2.core.model.RigGenerator;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class MarinePropertiesDataNodeListener extends EmptyDataNodeListener {
	private class RigType{
		String rigInformationUid = null;
		String rigType = null;
		String wellOnOffShore = null;
	}
	
	private RigType rigType = new RigType();

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof MarineProperties) {
			if(StringUtils.isNotBlank(UserSession.getInstance(request).getCurrentRigInformationUid())){
				MarineProperties thisMarineProperties = (MarineProperties) object;
				//auto set the rigUid form current rig
				thisMarineProperties.setRigInformationUid(UserSession.getInstance(request).getCurrentRigInformationUid());	
			}
		
			//GET RIGTYPE FOR TEMPLATE FILTER WHEN ADD NEW RECORD
			rigType.rigInformationUid = userSelection.getRigInformationUid();
			rigType.rigType = this.getRigType(userSelection.getRigInformationUid());
			rigType.wellOnOffShore = this.getWellOnOffShore(userSelection.getWellUid());
			node.getDynaAttr().put("rigType", rigType.rigType);
			node.getDynaAttr().put("wellOnOffShore", rigType.wellOnOffShore + "_" + rigType.rigType);

			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			if (StringUtils.isNotBlank(rigType.rigInformationUid)) {
				RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigType.rigInformationUid);
				if (rig!=null) {
					Map<String, Object> rigFieldValues = PropertyUtils.describe(rig);
					for (Map.Entry<String, Object> entry : rigFieldValues.entrySet()) {
						setDynaAttr(node, thisConverter, RigInformation.class, entry.getKey(), entry.getValue());
					}
				}
			}
			
			if (StringUtils.isNotBlank(userSelection.getWellUid())) {
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
				if (well!=null) {
					setDynaAttr(node, thisConverter, Well.class, "waterDepth", well.getWaterDepth());
				}
			}
		}
		
	}
	
	private String getRigType(String rigInformationUid) throws Exception{
		String[] paramsFields = {"rigInformationUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = rigInformationUid;
		
		String strSql = "SELECT rigSubType FROM RigInformation WHERE (isDeleted = false or isDeleted is null) and rigInformationUid =:rigInformationUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (!lstResult.isEmpty())
		{
			Object thisResult = (Object) lstResult.get(0);
			
			if (thisResult != null) {
				return thisResult.toString();
			}else {
				return null;
			}
		}else {
			return null;
		}

	}

	private String getWellOnOffShore (String wellUid) throws Exception{
		if (wellUid==null) return null;
		String onOffShore = null;
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellUid);
		if (well==null) return null;
		onOffShore = well.getOnOffShore();
		return onOffShore;
		
	}
	
	private void setDynaAttr(CommandBeanTreeNode node, CustomFieldUom converter, Class thisClass, String fieldName, Object value) throws Exception {
		node.getDynaAttr().put(thisClass.getSimpleName() + "." + fieldName, value);
		converter.setReferenceMappingField(thisClass, fieldName);
		if (converter.isUOMMappingAvailable()) {
			node.setCustomUOM("@" + thisClass.getSimpleName() + "." + fieldName, converter.getUOMMapping());
		}

	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		if(node.getData() instanceof MarineProperties){
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			//if(! StringUtils.equals(rigType.rigInformationUid, userSelection.getRigInformationUid())){
				rigType.rigInformationUid = userSelection.getRigInformationUid();
				rigType.rigType = this.getRigType(userSelection.getRigInformationUid());
				rigType.wellOnOffShore = this.getWellOnOffShore(userSelection.getWellUid());
			//}
			node.getDynaAttr().put("rigType", rigType.rigType);
			node.getDynaAttr().put("wellOnOffShore", rigType.wellOnOffShore + "_" + rigType.rigType);
			
			if (StringUtils.isNotBlank(rigType.rigInformationUid)) {
				RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigType.rigInformationUid);
				if (rig!=null) {
					Map<String, Object> rigFieldValues = PropertyUtils.describe(rig);
					for (Map.Entry<String, Object> entry : rigFieldValues.entrySet()) {
						setDynaAttr(node, thisConverter, RigInformation.class, entry.getKey(), entry.getValue());
					}
				}
			}
			
			if (StringUtils.isNotBlank(userSelection.getWellUid())) {
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
				if (well!=null) {
					setDynaAttr(node, thisConverter, Well.class, "waterDepth", well.getWaterDepth());
				}
			}
		}
	}
	
	@Override
	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{
		this.afterDataNodeLoad(commandBean, null, targetNode, new UserSelectionSnapshot(userSession), null);
	}

	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		if (node.getData() instanceof MarineProperties) {
			MarineProperties marineProperties = (MarineProperties) node.getData();
			RigInformation rigInformation = null;
			if (StringUtils.isNotBlank(session.getCurrentRigInformationUid())) {
				rigInformation = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, session.getCurrentRigInformationUid()); 
			}
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_VARIABLE_DECK_MARGIN))){
				MarinePropertiesUtil.calculateVariableDeckLoadMargin(commandBean, session, rigInformation, marineProperties);
			}
		}
	}
			
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
				
		if(node.getData() instanceof RigGenerator){

			Object object = node.getData();		
					
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RigGenerator.class, "hoursUsed");
					
			Date start = (Date) PropertyUtils.getProperty(object, "startGeneratorDatetime");
			Date finish = (Date) PropertyUtils.getProperty(object, "finishGeneratorDatetime");

			if(start != null && finish != null){
				if(start.getTime() > finish.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "startGeneratorDatetime", "Start Date/Time cannot be greater than Finish Date/Time.");
					return;
				}

				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, finish);
									
				thisConverter.setBaseValue(thisDuration);				
				PropertyUtils.setProperty(object, "hoursUsed", (thisConverter.getConvertedValue()));
			}else{
				Double thisDuration = 0.00;				
				PropertyUtils.setProperty(object, "hoursUsed", thisDuration);				
			}	
		}
	}

	
}