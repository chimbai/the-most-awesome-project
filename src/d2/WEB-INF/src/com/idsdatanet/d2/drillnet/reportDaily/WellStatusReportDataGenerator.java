package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;




public class WellStatusReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get operation uid
		String wellUid = userContext.getUserSelection().getWellUid();
		Locale locale = userContext.getUserSelection().getLocale();
		String operationUid = userContext.getUserSelection().getOperationUid();
		String currentDailyUid = userContext.getUserSelection().getDailyUid();
		String thisOpsReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		Daily currentDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (currentDaily==null) return;
		Date today_date = currentDaily.getDayDate();
		String todayDailyUid = currentDaily.getDailyUid();
		
		Double nptDuration = 0.0;
		Double totalDuration = 0.0;
		//get total npt in the well
		Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(userContext.getUserSelection().getWellUid());
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
	
		Daily tomorrow = ApplicationUtils.getConfiguredInstance().getTomorrow(userContext.getUserSelection().getOperationUid(), userContext.getUserSelection().getDailyUid());
		
		if (tomorrow != null){
			Date tmrDate = tomorrow.getDayDate();
			String tmrDailyUid = tomorrow.getDailyUid();
			String sql = "SELECT SUM(activityDuration) FROM Activity " +
					"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
					"AND operationUid = :operationUid " +
					"AND (isSimop=false OR isSimop is null) " +
			 		"AND (isOffline=false OR isOffline is null) " +
			 		"AND (dayPlus IS NULL OR dayPlus=0) " +
			 		"AND (startDatetime < :userTmrDate) " + 
					"AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='')";
			String[] paramsFields = {"userTmrDate", "operationUid"};
			Object[] paramsValues = {tmrDate, operationUid};	
			List<Double> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
			if(!result.isEmpty())
				totalDuration = result.get(0);
			sql = sql + " AND classCode IN ('TP', 'TU')";
			
			result  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
			
			if(!result.isEmpty())
				nptDuration = result.get(0);
		}
		else {	
		String sql = "SELECT SUM(activityDuration) FROM Activity " +
		"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
		"AND operationUid = :operationUid " +
		"AND (isSimop=false OR isSimop is null) " +
 		"AND (isOffline=false OR isOffline is null) " +
 		"AND (dayPlus IS NULL OR dayPlus=0) " +
		"AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='')";
		
		List<Double> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", operationUid, qp);
		
		if(!result.isEmpty())
			totalDuration = result.get(0);
		
		sql = sql + " AND classCode IN ('TP', 'TU')";
		
		result  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", operationUid, qp);
		
		if(!result.isEmpty())
			nptDuration = result.get(0);
		}
		//get total activity duration in the well
		
		//add to report
		
		if(totalDuration == null)
			totalDuration = 0.0;
		if(nptDuration == null)
			nptDuration = 0.0;
		
		CustomFieldUom thisConverter = new CustomFieldUom(locale, Activity.class, "activity_duration");
		if (thisConverter !=null)
		{
			thisConverter.setBaseValue(totalDuration);
			totalDuration = thisConverter.getConvertedValue();
			thisConverter.setBaseValue(nptDuration);
			nptDuration = thisConverter.getConvertedValue();
		}
		
		ReportDataNode thisReportNode = reportDataNode.addChild("wellStatus");
		thisReportNode.addProperty("nptDuration", nptDuration.toString());
		thisReportNode.addProperty("totalActivityDuration", totalDuration.toString());
		
		thisReportNode.addProperty("nptDurationLast24", this.getNptLast24(currentDaily, qp));
		thisReportNode.addProperty("nptDurationCumulative", this.getNptCumulative(currentDaily, qp));
		thisReportNode.addProperty("wowDuration", this.getWowDuration(currentDaily, qp));
		
		//24 hour progress
		Double prev_depth = 0.00;
		Double current_depth = 0.00;
		
		
		
		ReportDaily currentReportDaily = ApplicationUtils.getConfiguredInstance().getReportDaily(todayDailyUid, thisOpsReportType);
		current_depth = currentReportDaily.getDepthMdMsl();
		
		String sql1 = "SELECT depthMdMsl FROM ReportDaily " +
		"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
		"AND operationUid = :thisOpsUid " +
		"AND reportDatetime < :thisDate " +
		"ORDER BY reportDatetime DESC";
		
		String[] paramsFields = {"thisOpsUid", "thisDate"};
		Object[] paramsValues = {operationUid, today_date};
		List <Double> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql1, paramsFields, paramsValues);
		
		if (!lstResult.isEmpty()){
			prev_depth = lstResult.get(0);
		}
		String DepthUomSymbol="";
		thisConverter.setReferenceMappingField(ReportDaily.class, "depthMdMsl");
		
		if (thisConverter.isUOMMappingAvailable())
		{
			DepthUomSymbol=thisConverter.getUomSymbol();
		}
		Double results=current_depth-prev_depth;
		String resultsStr = null;
		
		if(results != null)
			resultsStr = thisConverter.formatOutputPrecision(results);
		
		
		if(results != null)
			thisReportNode.addProperty("results", resultsStr.replace(",", ""));
		else
			thisReportNode.addProperty("results", "0.0");
		
		thisReportNode.addProperty("depthUomSymbol", DepthUomSymbol);
		
	
			Date tmrDate= DateUtils.addDays(today_date,1);
			String sql2 = "SELECT hse.incidentCategory,SUM(hse.numberOfIncidents) "+
						"FROM HseIncident hse, LookupIncidentCategory lic "+
						"WHERE (hse.isDeleted=false or hse.isDeleted is null) " +
						"AND (lic.isDeleted=false or lic.isDeleted is null) " +
						"AND (hse.incidentCategory=lic.hseCategory) " +
						"AND hse.operationUid=:operationUid "+
						"AND hse.hseEventdatetime < :tmrDate " +
						"AND (lic.internalCode!='LWC' AND lic.internalCode!='RWC'  " +
						"AND lic.internalCode!='MTC' AND lic.internalCode!='FAC' ) " +
						"GROUP BY hse.incidentCategory "+
						"ORDER BY hse.hseEventdatetime DESC";
			
			String[] paramsFields2 = {"tmrDate",  "operationUid"};
			Object[] paramsValues2 = {tmrDate, operationUid};
			
			List safetyComments = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql2, paramsFields2, paramsValues2, qp);
			if (safetyComments.size() > 0){
				for(Object obj : safetyComments)
				{
					Object[] safetyComment = (Object[]) obj;
					String incidentCategory = CommonUtils.null2EmptyString(safetyComment[0]);
					String numberOfIncidents = CommonUtils.null2EmptyString(safetyComment[1]);
					ReportDataNode thisReportNode1 = reportDataNode.addChild("safetyComment");
					thisReportNode1.addProperty("incidentCategory", incidentCategory);
					thisReportNode1.addProperty("numberOfIncidents", numberOfIncidents);
					
			}
			}
		
	}
	
	private String getNptLast24(Daily currentDaily, QueryProperties qp) throws Exception{
		Double nptDuration = 0.0;
		if (currentDaily!=null){
			String sql = "SELECT SUM(a.activityDuration) FROM Activity a, Daily d " +
					"WHERE (a.isDeleted IS NULL OR a.isDeleted=FALSE) AND (d.isDeleted IS NULL OR d.isDeleted=FALSE) " +
					"AND a.dailyUid = d.dailyUid " +
					"AND a.operationUid = :operationUid " +
					"AND (a.isSimop=false OR a.isSimop is null) " +
			 		"AND (a.isOffline=false OR a.isOffline is null) " +
			 		"AND (a.dayPlus IS NULL OR a.dayPlus=0) " +
			 		"AND (a.dailyUid = :dailyUid) " + 
					"AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') " +
					"AND a.internalClassCode IN ('TP', 'TU')";
			
			String[] paramsFields = {"dailyUid", "operationUid"};
			Object[] paramsValues = {currentDaily.getDailyUid(), currentDaily.getOperationUid()};	
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
			if(result.size()>0){
				if (result.get(0)!=null) nptDuration = (Double) result.get(0);
			}
		}
		
		return nptDuration.toString();
	}
	
	private String getNptCumulative(Daily currentDaily, QueryProperties qp) throws Exception{
		Double nptDuration = 0.0;
		Double totalDuration = 0.0;
		Double cumulative = 0.0;
		if (currentDaily!=null){
			Date tmrDate= DateUtils.addDays(currentDaily.getDayDate(),1);
			String sql = "SELECT SUM(a.activityDuration) FROM Activity a, Daily d " +
					"WHERE (a.isDeleted IS NULL OR a.isDeleted=FALSE) AND (d.isDeleted IS NULL OR d.isDeleted=FALSE) " +
					"AND a.dailyUid = d.dailyUid " +
					"AND a.operationUid = :operationUid " +
					"AND (a.isSimop=false OR a.isSimop is null) " +
			 		"AND (a.isOffline=false OR a.isOffline is null) " +
			 		"AND (a.dayPlus IS NULL OR a.dayPlus=0) " +
			 		"AND (d.dayDate < :tmrDate) " + 
					"AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') " ;
			
			String[] paramsFields = {"operationUid", "tmrDate"};
			Object[] paramsValues = {currentDaily.getOperationUid(), tmrDate};	
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
			if(result.size()>0){
				if (result.get(0)!=null) totalDuration = (Double) result.get(0);
			}
			
			sql = sql + " AND a.internalClassCode IN ('TP', 'TU')";
			result  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
			
			if(result.size()>0)
				if (result.get(0)!=null) nptDuration = (Double) result.get(0);
		}
		
		if (totalDuration>0){
			cumulative = (nptDuration/totalDuration)*100;
		}
		
		return cumulative.toString();
	}
	
	private String getWowDuration(Daily currentDaily, QueryProperties qp) throws Exception{
		Double nptDuration = 0.0;
		if (currentDaily!=null){
			String sql = "SELECT SUM(a.activityDuration) FROM Activity a, Daily d " +
					"WHERE (a.isDeleted IS NULL OR a.isDeleted=FALSE) AND (d.isDeleted IS NULL OR d.isDeleted=FALSE) " +
					"AND a.dailyUid = d.dailyUid " +
					"AND a.operationUid = :operationUid " +
					"AND (a.isSimop=false OR a.isSimop is null) " +
			 		"AND (a.isOffline=false OR a.isOffline is null) " +
			 		"AND (a.dayPlus IS NULL OR a.dayPlus=0) " +
			 		"AND (a.dailyUid = :dailyUid) " + 
					"AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') " +
					"AND a.rootCauseCode ='WOW' ";
			
			String[] paramsFields = {"dailyUid", "operationUid"};
			Object[] paramsValues = {currentDaily.getDailyUid(), currentDaily.getOperationUid()};	
			List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
			if(result.size()>0){
				if (result.get(0)!=null) nptDuration = (Double) result.get(0);
			}
		}
		
		return nptDuration.toString();
	}
}
