package com.idsdatanet.d2.drillnet.rigIssue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.model.RigIssue;
import com.idsdatanet.d2.core.model.RigIssueLog;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class RigIssueDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	
	private static String SORTORDER = "ASC";

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof RigIssue) {
			RigIssue thisRigIssue = (RigIssue) object;
			thisRigIssue.setRigInformationUid(checkHideRigIssueStatus(commandBean.getRoot().getDynaAttr().get("rigInformationUid").toString()));
			thisRigIssue.setOperationUid(commandBean.getRoot().getDynaAttr().get("operationUid").toString());
			if(thisRigIssue.getStatus() == null) thisRigIssue.setStatus(false);
			if(thisRigIssue.getHideOnReport() == null) thisRigIssue.setHideOnReport(false);
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object object = node.getData();
		if(object instanceof RigIssueLog) {
			RigIssueLog thisRigIssueLog = (RigIssueLog) object;
			String rigIssueUid = thisRigIssueLog.getRigIssueUid();
			RigIssueLog lastLog = this.getLastRigIssueLog(rigIssueUid);
			
			String[] paramNames = {"rigIssueUid"};
			Object[] paramValues = {rigIssueUid};
			String strSql = "FROM RigIssue WHERE rigIssueUid = :rigIssueUid and (isDeleted = false or isDeleted is null)";
			List<RigIssue> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
			if (lstResult.size()>0){
				RigIssue thisRigIssue = lstResult.get(0);
				if (lastLog==null && (BooleanUtils.isTrue(thisRigIssue.getStatus()))){
					thisRigIssue.setStatus(false);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisRigIssue);
				}
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		
		/*after saving new component data, auto creating first log for it */
		if (object instanceof RigIssue) {
			RigIssue thisRigIssue = (RigIssue) object;
			String rigIssueUid = thisRigIssue.getRigIssueUid();
			Date logDateTime = thisRigIssue.getDateRaised();
			
			// checked if user added an issue log, else auto create 1 new records
			if( !isFirstLogAvailable(rigIssueUid) ){
				if(BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW == operationPerformed){
					RigIssueLog issueLog = new RigIssueLog();
					issueLog.setRigIssueUid(rigIssueUid);
					issueLog.setIssueStatus("new");
					issueLog.setLogDateTime(logDateTime);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(issueLog);
				}
			}
		}
		
		if(object instanceof RigIssueLog) {		
			RigIssueLog thisRigIssueLog = (RigIssueLog) object;
			String rigIssueUid = thisRigIssueLog.getRigIssueUid();
			RigIssueLog lastLog = this.getLastRigIssueLog(rigIssueUid);
			String[] paramNames = {"rigIssueUid"};
			
			Object[] paramValues = {rigIssueUid};
			String strSql = "FROM RigIssue WHERE rigIssueUid = :rigIssueUid and (isDeleted = false or isDeleted is null)";
			List<RigIssue> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
			if (lstResult.size()>0){
				RigIssue thisRigIssue = lstResult.get(0);
				if ((lastLog.getIssueStatus().equalsIgnoreCase("closed")) && (BooleanUtils.isFalse(thisRigIssue.getStatus()))){
					thisRigIssue.setStatus(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisRigIssue);
				}else if ((!lastLog.getIssueStatus().equalsIgnoreCase("closed")) && (BooleanUtils.isTrue(thisRigIssue.getStatus()))){
					thisRigIssue.setStatus(false);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisRigIssue);
				}
			}
			
			
			
		}
	}	
	
	// check if there is any Rig issue log exist/created
	private Boolean isFirstLogAvailable(String rigIssueUid) throws Exception {
		// TODO Auto-generated method stub
		String strSql = "From RigIssueLog Where rigIssueUid = :rigIssueUid And (isDeleted = false or isDeleted is null)";
		String[] paramNames = {"rigIssueUid"};
		Object[] paramValues = {rigIssueUid};
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames,paramValues);
		
		if( result.size() > 0 ){
			return true;
		}else{
			return false;
		}
		
	}
	
	public RigIssueLog getLastRigIssueLog(String rigIssueUid) throws Exception{
		String STRSQL = "FROM RigIssueLog WHERE (isDeleted = false or isDeleted is null) AND rigIssueUid=:rigIssueUid Order by logDateTime DESC";
		String[] paramNames = {"rigIssueUid"};
		Object[] paramValues = {rigIssueUid};
		List<RigIssueLog> rigIssueLogList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(STRSQL, paramNames, paramValues);
		
		if (rigIssueLogList.size()>0){
			return rigIssueLogList.get(0);
		}
		return null;
	}
	
	public String checkHideRigIssueStatus(String rigInformationUid) throws Exception
	{
		String sql = "select hideRigIssue from RigInformation where (isDeleted = false or isDeleted is null) and rigInformationUid=:rigInformationUid";	
		
		String[] paramsFields = {"rigInformationUid"};
		Object[] paramsValues = {rigInformationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);	
		if (!lstResult.isEmpty())
		{
			Object thisResult = (Object) lstResult.get(0);
			if(thisResult != null) 
				{
					if("true".equals(thisResult.toString()))
					{
						return "";
					}				
				}
		}
		return rigInformationUid;
	}
	
	public String checkOperationIsFilterToRig(String operationUid, String rigInformationUid) throws Exception
	{
		String sql = "select operationUid from Operation where (isDeleted = false or isDeleted is null) and operationUid=:operationUid and rigInformationUid=:rigInformationUid";
		
		String[] paramsFields = {"operationUid", "rigInformationUid"};
		Object[] paramsValues = {operationUid, rigInformationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);	
		if (lstResult.isEmpty())
		{
			return "";
		}
		return operationUid;
	}
	
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
			
		if (meta.getTableClass().equals(RigIssue.class)) {
			
			//TO OBTAIN THESE FILTER VALUE FROM USER SELECTION
			String selectedRigInformationUid  = "";
			selectedRigInformationUid = (String) commandBean.getRoot().getDynaAttr().get("rigInformationUid");
			
			String selectedOperationUid= "";
			selectedOperationUid = (String) commandBean.getRoot().getDynaAttr().get("operationUid");
			
			selectedRigInformationUid = checkHideRigIssueStatus(selectedRigInformationUid) ;
			selectedOperationUid = checkOperationIsFilterToRig(selectedOperationUid,selectedRigInformationUid);
				
			String linkedRigInformationUid = request.getParameter("rigInformationUid");
			if(!StringUtils.isBlank(linkedRigInformationUid) || linkedRigInformationUid != null)
			{
				selectedRigInformationUid = checkHideRigIssueStatus(linkedRigInformationUid) ;
				commandBean.getRoot().getDynaAttr().put("rigInformationUid", selectedRigInformationUid);
			}	
			
			String linkedOperationUid = request.getParameter("operationUid");
			if(!StringUtils.isBlank(linkedOperationUid) || linkedOperationUid != null)
			{
				selectedOperationUid = checkOperationIsFilterToRig(linkedOperationUid,selectedRigInformationUid);
				commandBean.getRoot().getDynaAttr().put("operationUid", selectedOperationUid);
			}
			
			String selectedTypeOfIssue  = (String) commandBean.getRoot().getDynaAttr().get("typeOfIssue");
			String selectedIssueItem  = (String) commandBean.getRoot().getDynaAttr().get("issueItem");
			String selectedSortedBy  = (String) commandBean.getRoot().getDynaAttr().get("sortedBy");
			String selectedSortingMethod  = (String) commandBean.getRoot().getDynaAttr().get("sortingMethod");
			String selectedRootCause  = (String) commandBean.getRoot().getDynaAttr().get("rootCause");
						
			String strSql = "FROM RigIssue WHERE rigInformationUid = :rigInformationUid AND status = :status";
			List arrayParamNames = new ArrayList();
			List arrayParamValues = new ArrayList();
			
			//NOT ALLOW THE FOLLOWING FIELDS TO BE BLANK, SET DEFAULT FILTERING
			if(StringUtils.isBlank(selectedRigInformationUid) || selectedRigInformationUid == null)
			{
				selectedRigInformationUid = checkHideRigIssueStatus(userSelection.getRigInformationUid()) ;
				commandBean.getRoot().getDynaAttr().put("rigInformationUid", selectedRigInformationUid);
			}
			
			if(StringUtils.isBlank(selectedOperationUid) || selectedOperationUid == null)
			{
				selectedOperationUid = checkOperationIsFilterToRig(userSelection.getOperationUid(),commandBean.getRoot().getDynaAttr().put("rigInformationUid", selectedRigInformationUid).toString());
				commandBean.getRoot().getDynaAttr().put("operationUid", selectedOperationUid);
			}
			
			if(StringUtils.isBlank(selectedTypeOfIssue))
			{
				selectedTypeOfIssue = "0";
				commandBean.getRoot().getDynaAttr().put("typeOfIssue", "false");
			}
			
			if(StringUtils.isBlank(selectedIssueItem))
			{
				selectedIssueItem = "all";
				commandBean.getRoot().getDynaAttr().put("issueItem", "all");
			}
			
			if(StringUtils.isBlank(selectedSortedBy))
			{
				selectedSortedBy = "referenceNumber";
				commandBean.getRoot().getDynaAttr().put("sortedBy", "referenceNumber");
			}
			
			if(StringUtils.isBlank(selectedSortingMethod))
			{
				selectedSortingMethod = "ASC";
				commandBean.getRoot().getDynaAttr().put("sortingMethod", "ASC");
			}
			
			//node.getParent().getDynaAttr().put(SORTORDER, selectedSortingMethod);
			SORTORDER = selectedSortingMethod;
			
			if (StringUtils.isNotBlank(selectedRootCause)) {
				strSql = strSql + " AND rootCause=:rootCause ";
				arrayParamNames.add("rootCause");
				arrayParamValues.add(selectedRootCause);
			}
				
			//IF SELECT TO SHOW ALL, THEN IGNORE QUERY hideOnReport
			if(selectedIssueItem.equals("all"))
			{
				strSql = strSql + " AND (isDeleted IS NULL OR isDeleted = FALSE) ORDER BY " + selectedSortedBy + " " + selectedSortingMethod;
				arrayParamNames.add("rigInformationUid");
				arrayParamNames.add("status");
				//arrayParamNames.add("operationUid");
								
				arrayParamValues.add(selectedRigInformationUid);
				arrayParamValues.add(Boolean.parseBoolean(selectedTypeOfIssue));
				//arrayParamValues.add(selectedOperationUid);
			}
			else
			{
				strSql = strSql + " AND hideOnReport = :hideOnReport AND (isDeleted IS NULL OR isDeleted = FALSE) ORDER BY " + selectedSortedBy + " " + selectedSortingMethod;
				arrayParamNames.add("rigInformationUid");
				arrayParamNames.add("status");
				arrayParamNames.add("hideOnReport");
				//arrayParamNames.add("operationUid");
								
				arrayParamValues.add(selectedRigInformationUid);
				arrayParamValues.add(Boolean.parseBoolean(selectedTypeOfIssue));
				arrayParamValues.add(Boolean.parseBoolean(selectedIssueItem));
				//arrayParamValues.add(selectedOperationUid);
			}
			
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			
			List<RigIssue> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			List<Object> output_maps = new ArrayList<Object>();
			
			//USE COMPARATOR TO SORT WHEN SORT TYPE is ReferenceNumber
			if (selectedSortedBy.equalsIgnoreCase("referenceNumber")) {
				Collections.sort(items, new RigIssueComparator());
			}
			
			for(Object objResult: items){
				RigIssue thisRigIssue = (RigIssue) objResult;
				output_maps.add(thisRigIssue);
			}
						
			return output_maps;
						
		}
		
		if (meta.getTableClass().equals(RigIssueLog.class)) {
			List<Object> output_maps = new ArrayList<Object>();
			Object object = node.getData();
			
			if (object instanceof RigIssue) {
				RigIssue thisRigIssue = (RigIssue) object;
				if (thisRigIssue.getRigIssueUid()!=null){
					String 	sql = "FROM RigIssueLog WHERE rigIssueUid =:rigIssueUid  AND (isDeleted IS NULL OR isDeleted = FALSE) ORDER BY logDateTime ";
					List<RigIssueLog> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"rigIssueUid", thisRigIssue.getRigIssueUid());		
					
					for(Object objResult: items){
						RigIssueLog thisRigIssueLog = (RigIssueLog) objResult;
						output_maps.add(thisRigIssueLog);
					}
					return output_maps;
				}
			}	
		}
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}
	
	private class RigIssueComparator implements Comparator<RigIssue>{
		public int compare(RigIssue o1, RigIssue o2){
			try {				
				String f1 = WellNameUtil.getPaddedStr(o1.getReferenceNumber().toString());
				String f2 = WellNameUtil.getPaddedStr(o2.getReferenceNumber().toString());
				
				if (f1 == null || f2 == null) return 0;
				if ("DESC".equalsIgnoreCase(SORTORDER)) {
					return f2.compareTo(f1);
				} else {
					return f1.compareTo(f2);
				}				
			} catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
	}
	
}
