package com.idsdatanet.d2.drillnet.report;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ProbabilisticWellCostPlanningReportModule extends MultiWellReportModule{
	
	private static final String FIELD_SELECTED_OPS = "reportSelectedOperations";
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		String report_type = this.getReportType(userContext.getUserSelection());

		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		if(operation == null) throw new Exception("Operation must be selected");
		String name = operation.getOperationName() + " " + 
			(StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
		
		return report_type + "/" + operation.getOperationUid() + "/" + ReportUtils.replaceInvalidCharactersInFileName(name);
	}

	protected List<String> getSelectedOperations(CommandBean commandBean,UserSession session) throws Exception {
		Object selected_operations = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_OPS);
		if(selected_operations == null){
			return null;
		}
		if(StringUtils.isBlank(selected_operations.toString())){
			return null;
		}
		
		String[] id_list = selected_operations.toString().split(",");
		List<String> result = new ArrayList<String>();
		for(String id: id_list){
			result.add(id.trim());
		}
		return result;
	}
	
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		List<String> list = this.getSelectedOperations(commandBean,session);;
		
		if (list != null && list.size() > 0) {
			
			List<UserSelectionSnapshot> selections = new ArrayList<UserSelectionSnapshot>();
			for(String operationUid: list){
				selections.add(UserSelectionSnapshot.getInstanceForCustomOperation(session, operationUid));
			}
			
			return this.submitReportJob(selections, this.getParametersForReportSubmission(session, request, commandBean));				

		}else{
			//throw new Exception("No data fit the selected criteria therefore unable to generate report.");
			commandBean.getSystemMessage().addWarning("No data fit the selected criteria therefore unable to generate report.");
			return null;
		}
	}
	
	/**
	 * Set report job to use the multi-well setting
	 */
	protected ReportJobParams getParametersForReportSubmission(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		ReportJobParams params = super.getParametersForReportSubmission(session, request, commandBean);
		params.setMultiWellJob(false);
		return params;
	}


}
