package com.idsdatanet.d2.drillnet.lookupProductList;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.templateimport.DefaultTemplateImportHandler;
import com.idsdatanet.d2.core.web.mvc.templateimport.EmptySlot;


public class LookupProductListTemplateImportHandler extends DefaultTemplateImportHandler {
	@Override
	protected void setupAdditionalBindingValues(int line, List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode){
		
		if (bindingValue !=null) {
			
			if (bindingValue.get(0) instanceof EmptySlot) {
				
			}else {
				
				String stockCode = StringUtils.upperCase(bindingValue.get(0).toString());
				String storedUnit = null;
				String mudVendorCompanyUid = StringUtils.upperCase(bindingValue.get(3).toString());
				if (bindingValue.get(1) instanceof EmptySlot) {
					
				}else {
					storedUnit = StringUtils.upperCase(bindingValue.get(1).toString());
				}
				
				try {
					List lstResult = null;
					
					if (StringUtils.isNotBlank(storedUnit)) {
						String sql = "select lookupRigStockUid from LookupRigStock where (isDeleted = false or isDeleted is null) and UPPER(stockBrand)=:stockCode " +
								"and UPPER(storedUnit)=:storedUnit and type='fluids'";
						
						lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"stockCode", "storedUnit"}, new Object[] {stockCode,storedUnit});
					
					}else {
						
						String sql = "select lookupRigStockUid from LookupRigStock where (isDeleted = false or isDeleted is null) and UPPER(stockBrand)=:stockCode and type='fluids'";
						lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "stockCode",stockCode);
					}
					
					if (lstResult.size() > 0) {
						Object a = (Object) lstResult.get(0);
						
						if (a != null) {
							bindingValue.set(0, a.toString());
							
						}
						
					}else {
						if (StringUtils.isNotBlank(storedUnit)) {
							bindingValue.set(0, stockCode + " (" + storedUnit + ")");
						}
					}
					
					// mudVendorCompanyUid
					lstResult = null;
					if (StringUtils.isNotBlank(mudVendorCompanyUid)) {
						String sql = "select DISTINCT(lookupCompanyUid) from LookupCompany where (isDeleted = false or isDeleted is null) and UPPER(companyName)=:companyName "; 
						
						lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"companyName"}, new Object[] {mudVendorCompanyUid});
						
						if (lstResult.size() > 0) {
							Object a = (Object) lstResult.get(0);
							String lookupCompanyUid = a.toString();
							sql = "select DISTINCT(lookupCompanyUid) from LookupCompanyService where (isDeleted = false or isDeleted is null) and lookupCompanyUid =:thislookupCompanyUid and code = '_MUD'"; 
							
							lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"thislookupCompanyUid"}, new Object[] {lookupCompanyUid});
						
							if (lstResult.size() > 0) {
								a = (Object) lstResult.get(0);
								
								if (a != null) {
									bindingValue.set(3, a.toString());							
								}
								
							}else {
								if (StringUtils.isNotBlank(mudVendorCompanyUid)) {
									bindingValue.set(3,mudVendorCompanyUid);
								}
							}
						}
						else{
							if (StringUtils.isNotBlank(mudVendorCompanyUid)) {
								bindingValue.set(3,mudVendorCompanyUid);
							}
						}	
					}
					
					
					//region use trim()
				
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
			}
		}
	}
	
	

}
