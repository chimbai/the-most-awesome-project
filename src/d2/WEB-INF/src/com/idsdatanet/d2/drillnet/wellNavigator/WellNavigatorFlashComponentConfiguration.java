package com.idsdatanet.d2.drillnet.wellNavigator;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellNavigatorFlashComponentConfiguration extends AbstractFlashComponentConfiguration {
	
	private Map<String,String> moduleRedirectionList;
	
	public String getFlashvars(UserSession userSession, HttpServletRequest request, String sessionId, String targetSubModule) throws Exception{
		return "baseUrl=" + urlEncode(userSession.getClientBaseUrl()) + this.includeModuleRedirectionUrls();
	}
	
	public String getFlashComponentId(String targetSubModule){
		return "WellNavigator";
	}
	
	private String includeModuleRedirectionUrls() throws Exception
	{
		String moduleUrl="";
		if (this.moduleRedirectionList!=null)
		{
			for(Map.Entry<String, String> entry : this.moduleRedirectionList.entrySet())
			{
				moduleUrl+="&"+entry.getKey()+"="+urlEncode(entry.getValue());
			}
		}
		return moduleUrl;
	}
	
	public void setModuleRedirectionList(Map<String,String> moduleRedirectionList) {
		this.moduleRedirectionList = moduleRedirectionList;
	}

	public Map<String,String> getModuleRedirectionList() {
		return moduleRedirectionList;
	}
}
