package com.idsdatanet.d2.drillnet.activity;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @author dlee
 * This class help to mine data for activity duration for the operation up to the selected date group by phase code
 * for daily reporting use
 * 
 * configuration is set in ddr.xml
 */

public class WFT_IQOperationActivityDurationByClassCodeAndPlanReferenceToDateReportGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		String sequence = null;
		String phaseCode = "N/A";
		String planReferenceUid = "N/A";
		
		Double pDuration = 0.00;
		Double nDuration = 0.00;
		Double cDuration = 0.00;
		Double xDuration = 0.00;
		Double totalDuration = 0.00;
		Double totalHours = 0.00;
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		// get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if(daily == null) return;
		Date todayDate = daily.getDayDate();
		
		String strSql = "SELECT a.planReference, a.classCode, SUM(a.activityDuration) AS totalDuration " + 
				"FROM Daily d, Activity a, OperationPlanPhase o WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) AND (o.isDeleted = false OR o.isDeleted IS NULL)" + 
				"AND a.planReference = o.operationPlanPhaseUid AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
				"NOT(a.phaseCode IS NULL OR a.phaseCode = '') AND (a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " + 
				"GROUP BY a.planReference";
		
		String[] paramsFields = {"userDate", "thisOperationUid"};
		Object[] paramsValues = {todayDate, userContext.getUserSelection().getOperationUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if(lstResult.size() > 0) {
			//unit converter to convert + format value
			CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
			
			for(Object objResult: lstResult) {
				Object[] obj = (Object[]) objResult;
				String thisPlanReference = obj[0].toString();
				String p_sql = "SELECT SUM(a.activityDuration) AS pDuration " + 
						"FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) " + 
						"AND a.classCode='P' AND a.planReference =:thisPlanReference AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
						"(a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) ";
				String n_sql = "SELECT SUM(a.activityDuration) AS pDuration " + 
						"FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) " + 
						"AND a.classCode='N' AND a.planReference =:thisPlanReference AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
						"(a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) ";
				String c_sql = "SELECT SUM(a.activityDuration) AS pDuration " + 
						"FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) " + 
						"AND a.classCode='C' AND a.planReference =:thisPlanReference AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
						"(a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) ";
				String x_sql = "SELECT SUM(a.activityDuration) AS pDuration " + 
						"FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) " + 
						"AND a.classCode='X' AND a.planReference =:thisPlanReference AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
						"(a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) ";
				
				String[] paramsArg = {"userDate", "thisOperationUid", "thisPlanReference"};
				Object[] paramsVal = {todayDate, userContext.getUserSelection().getOperationUid(), thisPlanReference};
				
				List p_result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(p_sql, paramsArg, paramsVal, qp);
				List n_result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(n_sql, paramsArg, paramsVal, qp);
				List c_result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(c_sql, paramsArg, paramsVal, qp);
				List x_result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(x_sql, paramsArg, paramsVal, qp);

				if(p_result.get(0) != null && StringUtils.isNotBlank(p_result.get(0).toString())) {
					pDuration = Double.parseDouble(p_result.get(0).toString());
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(pDuration);
					pDuration = thisConverter.getConvertedValue();
				}else {
					pDuration = 0.00;
				}

				if(n_result.get(0) != null && StringUtils.isNotBlank(n_result.get(0).toString())) {
					nDuration = Double.parseDouble(n_result.get(0).toString());
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(nDuration);
					nDuration = thisConverter.getConvertedValue();
				}else {
					nDuration = 0.00;
				}

				if(c_result.get(0) != null && StringUtils.isNotBlank(c_result.get(0).toString())) {
					cDuration = Double.parseDouble(c_result.get(0).toString());
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(cDuration);
					cDuration = thisConverter.getConvertedValue();
				}else {
					cDuration = 0.00;
				}
				
				if(x_result.get(0) != null && StringUtils.isNotBlank(x_result.get(0).toString())) {
					xDuration = Double.parseDouble(x_result.get(0).toString());
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(xDuration);
					xDuration = thisConverter.getConvertedValue();
				}else {
					xDuration = 0.00;
				}
				
				if(obj[0] != null) {
					if(StringUtils.isNotBlank(obj[0].toString())){
						planReferenceUid = obj[0].toString();
						String strSql2 = "SELECT sequence, phaseCode FROM OperationPlanPhase WHERE (isDeleted is null OR isDeleted = false) AND operationPlanPhaseUid = :thisPlanReference AND operationUid = :thisOperationUid";
						String[] paramsFields2 = {"thisPlanReference", "thisOperationUid"};
						Object[] paramsValues2 = {planReferenceUid, userContext.getUserSelection().getOperationUid()};
						
						List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
					     if(lstResult2.size()>0) {
					      for(Object planReferenceResult: lstResult2) {
					    	  Object[] res = (Object[]) planReferenceResult;
					    	  if(res[0] != null) sequence = res[0].toString();
						      if(res[1] != null) phaseCode = res[1].toString();
						      
					      }
					     }
					}else {
						planReferenceUid = "";
						phaseCode = "";
					}
				}
				
				if(obj[2] != null && StringUtils.isNotBlank(obj[2].toString())) {
					totalDuration = Double.parseDouble(obj[2].toString());
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(totalDuration);
					totalDuration = thisConverter.getConvertedValue();
				}
				
				totalHours = totalHours + totalDuration;
				reportDataNode.addProperty("totalHours", totalHours.toString());
				addToReportDataNode(reportDataNode, planReferenceUid, phaseCode, totalDuration, pDuration, nDuration, cDuration, xDuration, sequence);
			}
		}		
	}

	private void addToReportDataNode(ReportDataNode reportDataNode, String planReferenceUid, String phaseCode, Double totalDuration, Double pDuration, Double nDuration, Double cDuration, Double xDuration, String sequence) {
		
		ReportDataNode thisReportNode = reportDataNode.addChild("code");
		thisReportNode.addProperty("planReferenceUid", planReferenceUid);
		thisReportNode.addProperty("phaseCode", phaseCode);
		thisReportNode.addProperty("totalDuration", totalDuration.toString());
		thisReportNode.addProperty("totalPDuration", pDuration.toString());
		thisReportNode.addProperty("totalNDuration", nDuration.toString());
		thisReportNode.addProperty("totalCDuration", cDuration.toString());
		thisReportNode.addProperty("totalXDuration", xDuration.toString());
		thisReportNode.addProperty("sequence", sequence);
	}
}
