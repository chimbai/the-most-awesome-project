package com.idsdatanet.d2.drillnet.wellControlStack;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.WellControl;
import com.idsdatanet.d2.core.model.WellControlComponent;
import com.idsdatanet.d2.core.model.WellControlComponentTest;
import com.idsdatanet.d2.core.model.WellControlStack;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellControlStackDataNodeListener extends EmptyDataNodeListener {
    
    public boolean installDateTimeValidation = false;

    @Override
    public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
        Object object = node.getData();
        if (object instanceof WellControlStack) {
            this.processWellControlStack(commandBean, node, status, object);
        } else if (object instanceof WellControl) {
            this.processWellControl(commandBean, node, status, object);
        } else if (object instanceof WellControlComponent) {
            this.processWellControlComponent(commandBean, node, status, object);
            this.updateWellControlComponentUids(node);
        } else if (object instanceof WellControlComponentTest) {
            this.processWellControlComponentTest(commandBean, node, status, object);
            this.updateWellControlComponentTestUids(node);
        }
    }
    
    @Override
    public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession session, CommandBeanTreeNode parent) throws Exception {
        if ("WellControlStack".equalsIgnoreCase(className) && status.isAnyNodeSaved()) {
            Collections.sort(status.getUpdatedNodes(), new WellControlStackComparator());
            this.assignSequenceNumbers(status.getUpdatedNodes(), session, parent);
        }
    }
    
    @Override
    public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception {    
        Object object = targetNode.getData();
        if (object instanceof WellControlStack) {
            WellControlStack wcs = (WellControlStack) object;
            wcs.setSequence(null); //on paste as new record data with initial amount will be set to null
        }
    }
    
    private void processWellControlStack(CommandBean commandBean, CommandBeanTreeNode node, DataNodeProcessStatus status, Object object) throws Exception {
        Date wellMinDate = null;
        Date wellMaxDate = null;
        Date stackInstallDatetime = null;
        Date stackRemoveDatetime = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        List<Date> lstRemoveDatetime = new ArrayList<Date>();
        
        String wellUid = (String) PropertyUtils.getProperty(object, "wellUid");
        wellMinDate = WellControlStackUtils.getMinDailyDayDateFromWellUid(wellUid);
        wellMaxDate = WellControlStackUtils.getMaxDailyDayDateFromWellUid(wellUid);
        
        commandBean.getSystemMessage().clear();
        CommandBeanTreeNode parentNode = node.getParent();
        
        int removeDatetimeCounter = 0;
        for (Iterator i = parentNode.getList().values().iterator(); i.hasNext();) {
            Map items = (Map) i.next();
            for (Iterator j = items.values().iterator(); j.hasNext();) {
                CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
                if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(object.getClass())) {
                    Boolean isDeleted = (Boolean) PropertyUtils.getProperty(childNode.getData(), "isDeleted");
                    if (isDeleted == null || !isDeleted.booleanValue()) {
                        stackInstallDatetime = (Date) PropertyUtils.getProperty(childNode.getData(), "installDatetime");
                        stackRemoveDatetime = (Date) PropertyUtils.getProperty(childNode.getData(), "removeDatetime");
                        // Date In Well Check
                        if (this.installDateTimeValidation && stackInstallDatetime != null && (WellControlStackUtils.stripTime(stackInstallDatetime).getTime() > wellMaxDate.getTime() || WellControlStackUtils.stripTime(stackInstallDatetime).getTime() < wellMinDate.getTime())) {
                            status.setFieldError(node, "installDatetime", "Date must be within or equal to " + sdf.format(wellMinDate) + " to " + sdf.format(wellMaxDate) + ".");
                            status.setContinueProcess(false, true);
                            commandBean.getFlexClientControl().setReloadAfterPageCancel();
                            return;
                        }
                        if (stackRemoveDatetime == null) {
                            removeDatetimeCounter += 1;
                        } else {
                            // Date In Well Check
                            if (this.installDateTimeValidation && (WellControlStackUtils.stripTime(stackRemoveDatetime).getTime() > wellMaxDate.getTime() || WellControlStackUtils.stripTime(stackRemoveDatetime).getTime() < wellMinDate.getTime())) {
                                status.setFieldError(node, "removeDatetime", "Date must be within or equal to " + sdf.format(wellMinDate) + " to " + sdf.format(wellMaxDate) + ".");
                                status.setContinueProcess(false, true);
                                commandBean.getFlexClientControl().setReloadAfterPageCancel();
                                return;
                            }
                            lstRemoveDatetime.add(stackRemoveDatetime);
                            // Remove earlier than Install Check
                            if (stackInstallDatetime != null && stackRemoveDatetime.getTime() < stackInstallDatetime.getTime()) {
                                status.setFieldError(node, "removeDatetime", "Remove Date Time cannot be earlier than Install Date Time.");
                                status.setContinueProcess(false, true);
                                commandBean.getFlexClientControl().setReloadAfterPageCancel();
                                return;
                            }
                        }
                    }
                }
            }
        }
        // Unique BOP Stack Check
        if (removeDatetimeCounter > 1) {
            commandBean.getSystemMessage().addError("There is already a BOP Stack installed.");
            status.setContinueProcess(false, true);
            commandBean.getFlexClientControl().setReloadAfterPageCancel();
            return;
        }
        
        if (this.installDateTimeValidation) {
            // Date Validation for Remove Check (Cannot be later than latest install datetime of child-WellControlComponent)
            stackRemoveDatetime = (Date) PropertyUtils.getProperty(node.getData(), "removeDatetime");
            if (stackRemoveDatetime != null) {
                Date latestDatetime = stackRemoveDatetime;
                Map<String, CommandBeanTreeNode> wellControlList = node.getChild("WellControl");
                for (Iterator iter = wellControlList.entrySet().iterator(); iter.hasNext();) {
                    Map.Entry entry = (Map.Entry) iter.next();
                    CommandBeanTreeNode tmpNode = (CommandBeanTreeNode)entry.getValue();
                    if (tmpNode.getData() instanceof WellControl) {
                        Map<String, CommandBeanTreeNode> wellControlComponentList = tmpNode.getChild("WellControlComponent");
                        for (Iterator iter2 = wellControlComponentList.entrySet().iterator(); iter2.hasNext();) {
                            Map.Entry entry2 = (Map.Entry) iter2.next();
                            CommandBeanTreeNode tmpNode2 = (CommandBeanTreeNode)entry2.getValue();
                            if (tmpNode2.getData() instanceof WellControlComponent) {
                                WellControlComponent wcc = (WellControlComponent) tmpNode2.getData();
                                if (wcc.getInstallDatetime() != null && wcc.getInstallDatetime().getTime() > latestDatetime.getTime()) {
                                    latestDatetime = wcc.getInstallDatetime();
                                }
                                if (latestDatetime.getTime() > stackRemoveDatetime.getTime()) {
                                    status.setFieldError(node, "removeDatetime", "There are BOP Component(s) that are installed after the specified Date Time.");
                                    status.setContinueProcess(false, true);
                                    commandBean.getFlexClientControl().setReloadAfterPageCancel();
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            // Date Validation for Install Date (Cannot be later than latest earliest datetime of child-WellControlComponent)
            stackInstallDatetime = (Date) PropertyUtils.getProperty(node.getData(), "installDatetime");
            if (stackInstallDatetime != null) {
                Date earliestDatetime = stackInstallDatetime;
                Map<String, CommandBeanTreeNode> wellControlList = node.getChild("WellControl");
                for (Iterator iter = wellControlList.entrySet().iterator(); iter.hasNext();) {
                    Map.Entry entry = (Map.Entry) iter.next();
                    CommandBeanTreeNode tmpNode = (CommandBeanTreeNode)entry.getValue();
                    if (tmpNode.getData() instanceof WellControl) {
                        Map<String, CommandBeanTreeNode> wellControlComponentList = tmpNode.getChild("WellControlComponent");
                        for (Iterator iter2 = wellControlComponentList.entrySet().iterator(); iter2.hasNext();) {
                            Map.Entry entry2 = (Map.Entry) iter2.next();
                            CommandBeanTreeNode tmpNode2 = (CommandBeanTreeNode)entry2.getValue();
                            if (tmpNode2.getData() instanceof WellControlComponent) {
                                WellControlComponent wcc = (WellControlComponent) tmpNode2.getData();
                                if (wcc.getInstallDatetime() != null && wcc.getInstallDatetime().getTime() < earliestDatetime.getTime()) {
                                    earliestDatetime = wcc.getInstallDatetime();
                                }
                                if (earliestDatetime.getTime() < stackInstallDatetime.getTime()) {
                                    status.setFieldError(node, "installDatetime", "There are BOP Component(s) that are installed before the specified Date Time.");
                                    status.setContinueProcess(false, true);
                                    commandBean.getFlexClientControl().setReloadAfterPageCancel();
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void processWellControl(CommandBean commandBean, CommandBeanTreeNode node, DataNodeProcessStatus status, Object object) throws Exception {
        WellControl wc = (WellControl) object;
        if (wc instanceof WellControl) {
            if (wc.getInspectionInterval() != null) {
                if (wc.getInspectionInterval() < 0) {
                    status.setFieldError(node, "inspectionInterval", "Inspection Interval must be a positive number!");
                    status.setContinueProcess(false, true);
                    commandBean.getFlexClientControl().setReloadAfterPageCancel();
                    return;
                } else {
                    if (wc.getLastInspectionDatetime() != null) {
                        String thisInterval = CommonUtils.roundUpFormat(new DecimalFormat("#0"), wc.getInspectionInterval());
                        wc.setNextInspectionDatetime(new Date(wc.getLastInspectionDatetime().getTime() + Long.parseLong(thisInterval) * 86400000));
                    }
                }
            } else {
                wc.setNextInspectionDatetime(null);
            }
        }
    }
    
    private void processWellControlComponent(CommandBean commandBean, CommandBeanTreeNode node, DataNodeProcessStatus status, Object object) throws Exception {
        Date minDate = null;
        Date maxDate = null;
        Date installDatetime = null;
        Date removeDatetime = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH mm");
        
        CommandBeanTreeNode stackNode = node.getParent().getParent();
        Object parentObj = stackNode.getData();
        if (parentObj instanceof WellControlStack) {
            WellControlStack wcs = (WellControlStack) parentObj;
            minDate = wcs.getInstallDatetime();
            maxDate = wcs.getRemoveDatetime();
            if (minDate == null) {
                commandBean.getSystemMessage().addError("The BOP Stack for the BOP Component has no Install Datetime!");
                status.setContinueProcess(false, true);
                commandBean.getFlexClientControl().setReloadAfterPageCancel();
                return;
            }
        }
        
        commandBean.getSystemMessage().clear();
        CommandBeanTreeNode parentNode = node.getParent();
        
        for (Iterator i = parentNode.getList().values().iterator(); i.hasNext();) {
            Map items = (Map) i.next();
            for (Iterator j = items.values().iterator(); j.hasNext();) {
                CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
                if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(object.getClass())) {
                    Boolean isDeleted = (Boolean) PropertyUtils.getProperty(childNode.getData(), "isDeleted");
                    if (isDeleted == null || !isDeleted.booleanValue()) {
                        installDatetime = (Date) PropertyUtils.getProperty(childNode.getData(), "installDatetime");
                        removeDatetime = (Date) PropertyUtils.getProperty(childNode.getData(), "removeDatetime");
                        if (this.installDateTimeValidation && ((maxDate != null && installDatetime.getTime() > maxDate.getTime()) || installDatetime.getTime() < minDate.getTime())) {
                            if (maxDate != null) {
                                status.setFieldError(node, "installDatetime", "Date must be within or equal to " + sdf.format(minDate) + " to " + sdf.format(maxDate) + ".");
                            } else {
                                status.setFieldError(node, "installDatetime", "Date must be equal to or later than " + sdf.format(minDate) + ".");
                            }
                            status.setContinueProcess(false, true);
                            commandBean.getFlexClientControl().setReloadAfterPageCancel();
                            return;
                        }
                        if (removeDatetime != null) {
                            if (installDatetime != null && removeDatetime.getTime() < installDatetime.getTime()) {
                                status.setFieldError(node, "removeDatetime", "Remove Date Time cannot be earlier than Install Date Time.");
                                status.setContinueProcess(false, true);
                                commandBean.getFlexClientControl().setReloadAfterPageCancel();
                                return;
                            }
                        }
                    }
                }
            }
        }
        
        if (this.installDateTimeValidation) {
            // Date Validation for Remove Check (Cannot be later than latest date of child)
            removeDatetime = (Date) PropertyUtils.getProperty(node.getData(), "removeDatetime");
            if (removeDatetime != null) {
                Date latestDatetime = removeDatetime;
                Map<String, CommandBeanTreeNode> wellControlList = node.getChild("WellControlComponentTest");
                for (Iterator iter = wellControlList.entrySet().iterator(); iter.hasNext();) {
                    Map.Entry entry = (Map.Entry) iter.next();
                    CommandBeanTreeNode tmpNode = (CommandBeanTreeNode)entry.getValue();
                    if (tmpNode.getData() instanceof WellControlComponentTest) {
                        WellControlComponentTest wcct = (WellControlComponentTest) tmpNode.getData();
                        if (wcct.getTestDatetime() != null && wcct.getTestDatetime().getTime() > latestDatetime.getTime()) {
                            latestDatetime = wcct.getTestDatetime();
                        }
                        if (latestDatetime.getTime() > removeDatetime.getTime()) {
                            status.setFieldError(node, "removeDatetime", "There are BOP Component Test(s) done after the specified Date Time.");
                            status.setContinueProcess(false, true);
                            commandBean.getFlexClientControl().setReloadAfterPageCancel();
                            return;
                        }
                    }
                }
            }
            // Date Validation for Remove Check (Cannot be later than latest date of child)
            installDatetime = (Date) PropertyUtils.getProperty(node.getData(), "installDatetime");
            if (installDatetime != null) {
                Date earliestDatetime = installDatetime;
                Map<String, CommandBeanTreeNode> wellControlList = node.getChild("WellControlComponentTest");
                for (Iterator iter = wellControlList.entrySet().iterator(); iter.hasNext();) {
                    Map.Entry entry = (Map.Entry) iter.next();
                    CommandBeanTreeNode tmpNode = (CommandBeanTreeNode)entry.getValue();
                    if (tmpNode.getData() instanceof WellControlComponentTest) {
                        WellControlComponentTest wcct = (WellControlComponentTest) tmpNode.getData();
                        if (wcct.getTestDatetime() != null && wcct.getTestDatetime().getTime() < earliestDatetime.getTime()) {
                            earliestDatetime = wcct.getTestDatetime();
                        }
                        if (earliestDatetime.getTime() < installDatetime.getTime()) {
                            status.setFieldError(node, "installDatetime", "There are BOP Component Test(s) done before the specified Date Time.");
                            status.setContinueProcess(false, true);
                            commandBean.getFlexClientControl().setReloadAfterPageCancel();
                            return;
                        }
                    }
                }
            }
        }
    }
    
    private void processWellControlComponentTest(CommandBean commandBean, CommandBeanTreeNode node, DataNodeProcessStatus status, Object object) throws Exception {
        Date minDate = null;
        Date maxDate = null;
        Date testDatetime = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH mm");
        
        Object parentObj = node.getParent().getData();
        if (parentObj instanceof WellControlComponent) {
            WellControlComponent wc = (WellControlComponent) parentObj;
            minDate = wc.getInstallDatetime();
            maxDate = wc.getRemoveDatetime();
        }
        
        commandBean.getSystemMessage().clear();
        CommandBeanTreeNode parentNode = node.getParent();
        
        for (Iterator i = parentNode.getList().values().iterator(); i.hasNext();) {
            Map items = (Map) i.next();
            for (Iterator j = items.values().iterator(); j.hasNext();) {
                CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
                if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(object.getClass())) {
                    Boolean isDeleted = (Boolean) PropertyUtils.getProperty(childNode.getData(), "isDeleted");
                    if (isDeleted == null || !isDeleted.booleanValue()) {
                        testDatetime = (Date) PropertyUtils.getProperty(childNode.getData(), "testDatetime");
                        if (this.installDateTimeValidation && (maxDate != null && testDatetime.getTime() > maxDate.getTime()) || testDatetime.getTime() < minDate.getTime()) {
                            if (maxDate != null) {
                                status.setFieldError(node, "testDatetime", "Date must be within or equal to " + sdf.format(minDate) + " to " + sdf.format(maxDate) + ".");
                            } else {
                                status.setFieldError(node, "testDatetime", "Date must be equal to or later than " + sdf.format(minDate) + ".");
                            }
                            status.setContinueProcess(false, true);
                            commandBean.getFlexClientControl().setReloadAfterPageCancel();
                            return;
                        }
                    }
                }
            }
        }
        
        WellControlComponentTest wc = (WellControlComponentTest) object;
        if (wc instanceof WellControlComponentTest) {
            if (wc.getTestInterval() != null) {
                // Validate Interval >= 0
                if (wc.getTestInterval() < 0) {
                    status.setFieldError(node, "testInterval", "Test Interval must be a positive number!");
                    status.setContinueProcess(false, true);
                    commandBean.getFlexClientControl().setReloadAfterPageCancel();
                    return;
                } else {
                    if (wc.getTestDatetime() != null) {
                        String thisInterval = CommonUtils.roundUpFormat(new DecimalFormat("#0"), wc.getTestInterval());
                        wc.setNextWellControlTestDatetime(new Date(wc.getTestDatetime().getTime() + Long.parseLong(thisInterval) * 86400000));
                    }
                }
            } else {
                wc.setNextWellControlTestDatetime(null);                
            }
        }
    }
    
    private void assignSequenceNumbers(List<CommandBeanTreeNode> updatedNodes, UserSession session, CommandBeanTreeNode parent) throws Exception {
        Integer maxSeq = 0;
        Integer seq;
        for (Iterator i = parent.getList().values().iterator(); i.hasNext();) {
            Map items = (Map) i.next();
            for (Iterator j = items.values().iterator(); j.hasNext();) {
                CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
                if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(WellControlStack.class)) {
                    Boolean isDeleted = (Boolean) PropertyUtils.getProperty(childNode.getData(), "isDeleted");
                    if (isDeleted == null || !isDeleted.booleanValue()) {
                        seq = (Integer) PropertyUtils.getProperty(childNode.getData(), "sequence");
                        if (seq != null && seq > maxSeq) {
                            maxSeq = seq;
                        }
                    }
                }
            }
        }
        
        for (CommandBeanTreeNode node : updatedNodes) {
            Object obj = node.getData();
            if (obj instanceof WellControlStack) {
                WellControlStack wellControlStack = (WellControlStack) obj;
                if (wellControlStack.getSequence() == null) {
                    wellControlStack.setSequence(++maxSeq);
                    ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(wellControlStack);
                }
            }
        }
    }
    
    private void updateWellControlComponentUids(CommandBeanTreeNode node) throws Exception {
        CommandBeanTreeNode parentNode = node.getParent();
        if (parentNode != null && parentNode.getData() instanceof WellControl) {
            WellControlComponent wcc = (WellControlComponent) node.getData();
            WellControl wc = (WellControl) parentNode.getData();
            wcc.setWellControlStackUid(wc.getWellControlStackUid());
        }
    }
    
    private void updateWellControlComponentTestUids(CommandBeanTreeNode node) throws Exception {
        CommandBeanTreeNode parentNode = node.getParent();
        if (parentNode != null && parentNode.getData() instanceof WellControlComponent) {
            WellControlComponentTest wcct = (WellControlComponentTest) node.getData();
            WellControlComponent wcc = (WellControlComponent) parentNode.getData();
            wcct.setWellControlStackUid(wcc.getWellControlStackUid());
            wcct.setWellControlUid(wcc.getWellControlUid());
        }
    }
    
    public boolean isInstallDateTimeValidation() {
        return installDateTimeValidation;
    }

    public void setInstallDateTimeValidation(boolean installDateTimeValidation) {
        this.installDateTimeValidation = installDateTimeValidation;
    }
    
    public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		if(commandBean.getFlexClientControl() != null){
			commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		if(commandBean.getFlexClientControl() != null){
			commandBean.getFlexClientControl().setReloadParentHtmlPage();
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		 Object object = node.getData();
        if (object instanceof WellControlStack) {
        	String msg = "BOPs may be associated to other operation(s) of this well. Changing BOP records will also change the BOP records tied to all other operation(s) of this well. \n\n";
    		msg += "Operations: \n\n";
    		String existingList = "";
    		String queryString = "SELECT wb.wellboreName, o.operationName "+
    							"FROM Operation o, Wellbore wb, Well w "+
    							"WHERE (o.isDeleted IS NULL OR o.isDeleted = FALSE) "+
    							"AND (wb.isDeleted IS NULL OR wb.isDeleted = FALSE) "+
    							"AND (w.isDeleted IS NULL OR w.isDeleted = FALSE) "+
    							"AND o.wellboreUid = wb.wellboreUid "+
    							"AND o.wellUid = w.wellUid "+
    							"AND o.wellUid = :wellUid "+
    							"ORDER BY wb.wellboreNamePadded, o.operationNamePadded";
    		
    		List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "wellUid", userSelection.getWellUid());
    		
    		if (list.size() > 0){
    			for (Object result : list){
    				Object[] obj = (Object[]) result;
    				
    				String wellboreName = CommonUtils.null2EmptyString(obj[0]);
    				String operationName = CommonUtils.null2EmptyString(obj[1]);
    				
    				existingList += "\u2022 " + wellboreName + " > " + operationName + " \n";
    			}
    		}	
    		msg += existingList;
    		node.getDynaAttr().put("affectedOperationList", existingList);
    		commandBean.getSystemMessage().addInfo(msg);
        }
	}
}