package com.idsdatanet.d2.drillnet.kpi;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.webservice.SimpleJsonResponse;
import com.idsdatanet.d2.core.web.webservice.SimpleXmlResponse;
import com.idsdatanet.depot.idsbridge.provider.kpi.IDSBridgeKPIProvider;
import com.idsdatanet.depot.idsbridge.response.IDSBridgeResponse;
import com.idsdatanet.depot.idsbridge.response.Message;

public class KpiRepositoryCommandbeanListener extends EmptyCommandBeanListener {
	private List<IDSBridgeKPIProvider> kpiSyncProviders;
	
	public void setKpiSyncProviders(List<IDSBridgeKPIProvider> kpiSyncProviders) {
		this.kpiSyncProviders = kpiSyncProviders;
	}

	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
		if ("sync_kpi_config".equalsIgnoreCase(invocationKey)) {
			boolean success = false;
			SimpleXmlResponse writer = SimpleJsonResponse.getSimpleJsonResponse(response);
			writer.startElement("root");
			try {
				if (this.kpiSyncProviders != null) {
					StringBuffer sb = new StringBuffer();
					for (IDSBridgeKPIProvider provider : this.kpiSyncProviders) {
						IDSBridgeResponse kpi_response = provider.syncAll();
						if (!kpi_response.isSuccess()) {
							// return with error
							writer.addElement("error", kpi_response.getMessagesWithType(Message.TYPE_ERROR));
							break;
						} else {
							sb.append(kpi_response.getMessagesWithType(Message.TYPE_INFO));
						}
					}
					String infos = sb.toString();
					if (StringUtils.isNotBlank(infos)) {
						writer.addElement("info", infos);
					}
					success = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
				writer.addElement("error", e.getMessage());
			} finally {
				writer.addElement("success", (success? "true":"false"));
				writer.endElement();
				writer.close();
			}
		}
	}
}
