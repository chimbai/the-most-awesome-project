package com.idsdatanet.d2.drillnet.surveyreference;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class SurveyReferenceDataNodeAllowedAction implements DataNodeAllowedAction {

	public boolean allowedAction(CommandBeanTreeNode node, UserSession session,
			String targetClass, String action, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
			Object object = node.getData();
			
			if (object instanceof SurveyStation) {
				SurveyStation survStation = (SurveyStation) object;
				String operationUid = session.getCurrentOperationUid();
					if (operationUid != null && !operationUid.equals(survStation.getOperationUid())) {
						if (StringUtils.equals(action, "delete") || StringUtils.equals(action, "edit")) {
							return false;
						}
					}
				return true;
			}

		return true;
	}
	
}

