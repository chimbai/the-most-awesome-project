package com.idsdatanet.d2.drillnet.activity;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class TdeRigStateCommandBean extends BaseCommandBean {
	private LinkedHashMap<String, String> data;
	private String reportName = null;
	private String rigName = null;
	private String utcTimeRange = null;
	
	
	public LinkedHashMap<String, String> getData() throws Exception {
		return this.data;
	}
	
	/**
	 * BaseCommandBean load to initiate loadData()
	 */
	@Override
	public void load(HttpServletRequest request) throws Exception {
		setRoot(new CommandBeanTreeNodeImpl(this));
		
		UserSession session = UserSession.getInstance(request);
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
		
		this.reportName = "";
		this.rigName = "";
		this.utcTimeRange = "";
		
		String rigInformationUid = userSelection.getRigInformationUid();
		RigInformation thisRigInformation = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigInformationUid);
		if(thisRigInformation!=null && thisRigInformation.getRigName()!=null){
			this.rigName = thisRigInformation.getRigName();
			this.rigName = StringUtils.replace(this.rigName, " ", "+");
		}
		
		String wellUid = userSelection.getWellUid();
		Double tzGmtOffset = null;
		Well thisWell = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellUid);
		if(thisWell!=null && thisWell.getTzGmtOffset()!=null)
			tzGmtOffset = thisWell.getTzGmtOffset();
		String dailyUid = userSelection.getDailyUid();
		Date reportDatetime = null;
		Long reportDatetimeEpochMS = null;
		Long reportDatetime_UTC0_EpochMS_0 = null;
		Long reportDatetime_UTC0_EpochMS_24 = null;
		ReportDaily thisReportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(UserSession.getInstance(request), dailyUid);
		if(thisReportDaily!=null && thisReportDaily.getReportDatetime()!=null){
			reportDatetime = thisReportDaily.getReportDatetime();
			// make the time to end of today
			reportDatetimeEpochMS = reportDatetime.getTime();
			
			Calendar currentSystemDatetime = Calendar.getInstance();
			currentSystemDatetime.set(Calendar.YEAR, currentSystemDatetime.get(Calendar.YEAR));
			currentSystemDatetime.set(Calendar.MONTH, currentSystemDatetime.get(Calendar.MONTH));
			currentSystemDatetime.set(Calendar.DAY_OF_MONTH, currentSystemDatetime.get(Calendar.DAY_OF_MONTH));
			currentSystemDatetime.set(Calendar.HOUR_OF_DAY, 0);
			currentSystemDatetime.set(Calendar.MINUTE, 0);
			currentSystemDatetime.set(Calendar.SECOND, 0);
			currentSystemDatetime.set(Calendar.MILLISECOND, 0);
			
			if(reportDatetimeEpochMS.equals(currentSystemDatetime.getTimeInMillis()))
				this.reportName = "RealTime";
			else{
				this.reportName = "Daily";
			
				if(tzGmtOffset!=null){
					long tzGmtOffsetEpochMs = (long) (tzGmtOffset * 60 * 60 * 1000);
					reportDatetime_UTC0_EpochMS_0 = reportDatetimeEpochMS - tzGmtOffsetEpochMs;
					reportDatetime_UTC0_EpochMS_24 = reportDatetime_UTC0_EpochMS_0 + (24*60*60*1000);
					this.utcTimeRange = reportDatetime_UTC0_EpochMS_0.toString()+"-"+reportDatetime_UTC0_EpochMS_24.toString();
				}
			}
		}
		
		this.data = new LinkedHashMap<String, String>();
		this.data.put("reportName", this.reportName);
		this.data.put("rigName", this.rigName);
		this.data.put("utcTimeRange", this.utcTimeRange);
		
	}
}
