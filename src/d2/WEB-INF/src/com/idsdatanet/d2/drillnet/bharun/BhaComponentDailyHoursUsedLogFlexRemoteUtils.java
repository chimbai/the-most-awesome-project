package com.idsdatanet.d2.drillnet.bharun;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.model.BhaComponentHoursUsed;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class BhaComponentDailyHoursUsedLogFlexRemoteUtils extends BlazeRemoteClassSupport{
	
	public void getDailyHoursUsedLogList(HttpServletRequest request, HttpServletResponse response, String bhaComponentUid, String bharunUid) {
		//String result = "<root/>";
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		//if (StringUtils.isBlank(bhaComponentUid) || StringUtils.isBlank(bharunUid)) return result;
		try {
			//ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			
			// Populate DailyHoursUsedLog listing for loading in grid view
			String queryString = "SELECT b.bhaComponentHoursUsedUid, b.hoursUsed, b.dailyUid, b.comment, d.dayDate, r.reportNumber " +
								 "FROM BhaComponentHoursUsed b, Daily d, ReportDaily r " +
								 "WHERE (b.isDeleted=false OR b.isDeleted is null) " +
								 "AND (d.isDeleted=false OR d.isDeleted is null) " +
								 "AND (r.isDeleted=false OR r.isDeleted is null) " +
								 "AND b.dailyUid = d.dailyUid " +
								 "AND d.dailyUid = r.dailyUid " +
								 "AND b.bharunUid = :bharunUid " +
								 "AND b.bhaComponentUid = :bhaComponentUid GROUP BY  b.bhaComponentHoursUsedUid ORDER BY d.dayDate";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"bharunUid", "bhaComponentUid"}, new Object[] {bharunUid, bhaComponentUid});
			if (lstResult.size() > 0){
				for(Object objResult: lstResult){
					//due to result is in array, cast the result to array first and read 1 by 1
					Object[] obj = (Object[]) objResult;
					String bhaComponentHoursUsedUid = "";
					String hoursUsed = "";
					String dailyUid = "";
					String comment = "";
					Date daydate = null;
					String dayNumber = "";
					
									
					if (obj[0] != null && StringUtils.isNotBlank(obj[0].toString())) bhaComponentHoursUsedUid = obj[0].toString();
					if (obj[1] != null && StringUtils.isNotBlank(obj[1].toString())) hoursUsed = obj[1].toString();
					if (obj[2] != null && StringUtils.isNotBlank(obj[2].toString())) dailyUid = obj[2].toString();
					if (obj[3] != null && StringUtils.isNotBlank(obj[3].toString())) comment = obj[3].toString();
					if (obj[4] != null && StringUtils.isNotBlank(obj[4].toString())) daydate = (Date) obj[4];
					if (obj[5] != null && StringUtils.isNotBlank(obj[5].toString())) dayNumber = obj[5].toString();
															
					// Write the attributes into record
					SimpleAttributes attr = new SimpleAttributes();
					attr.addAttribute("bhaComponentHoursUsedUid",bhaComponentHoursUsedUid);
					attr.addAttribute("hoursUsed", hoursUsed);
					attr.addAttribute("dailyUid", dailyUid);
					attr.addAttribute("comment", comment);
					
					if (daydate != null){
						SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
						attr.addAttribute("daydate", formatter.format(daydate));
					}else{
						attr.addAttribute("daydate","");
					}
					
					attr.addAttribute("dayNumber", dayNumber);
										
					//write the element record into the DailyHoursUsedLog
					writer.addElement("DailyHoursUsedLog", "", attr);
				}
			}
			
			//Populate specific BHA's all related Day Date lookup items into XML for ExtendedComboBox Data Provider
			String queryString2 = "SELECT b.dailyUid, d.dayDate, r.reportNumber " +
			 "FROM BharunDailySummary b, Daily d, ReportDaily r " +
			 "WHERE (b.isDeleted=false OR b.isDeleted is null) " +
			 "AND (d.isDeleted=false OR d.isDeleted is null) " +
			 "AND (r.isDeleted=false OR r.isDeleted is null) " +
			 "AND b.dailyUid = d.dailyUid " +
			 "AND d.dailyUid = r.dailyUid " +
			 "AND b.bharunUid = :bharunUid " +
			 "GROUP BY d.dailyUid ORDER BY r.reportNumber";
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString2, new String[] {"bharunUid"}, new Object[] {bharunUid});
			if (lstResult2.size() > 0){
				for(Object objResult2: lstResult2){
					//due to result is in array, cast the result to array first and read 1 by 1
					Object[] obj2 = (Object[]) objResult2;
					String dailyUid = "";					
					Date daydate = null;
					String dayNumber = "";
					if (obj2[0] != null && StringUtils.isNotBlank(obj2[0].toString())) dailyUid = obj2[0].toString();
					if (obj2[1] != null && StringUtils.isNotBlank(obj2[1].toString())) daydate = (Date) obj2[1];
					if (obj2[2] != null && StringUtils.isNotBlank(obj2[2].toString())) dayNumber = obj2[2].toString();
					
					// Write the attributes into record
					SimpleAttributes attr = new SimpleAttributes();
					attr.addAttribute("dailyUid", dailyUid);
					if (daydate != null){
						SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
						attr.addAttribute("daydate", formatter.format(daydate));
					}else{
						attr.addAttribute("daydate","");
					}
					attr.addAttribute("dayNumber", dayNumber);
					
					//write the element record into the DayDateLookup
					writer.addElement("DayDateLookup", "", attr);
				}
			}
			writer.close();
			//result = bytes.toString();
			//return result;
		} catch (Exception e) {
			e.printStackTrace();
			//return "<root/>";
		}		
	}
	
	public Boolean saveNewDailyHoursUsedLog(HttpServletRequest request, HttpServletResponse response, String descr, String txtHours, String cbDayDate, String bharunUid, String bhaComponentUid, String wellUid, String wellboreUid) {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		Double hoursUsed = 0.00;
		
		try {					
			if(txtHours !=null && StringUtils.isNotEmpty(txtHours)){
				if(StringUtils.isNotEmpty(cbDayDate)){
					hoursUsed = Double.parseDouble(txtHours);
					CustomFieldUom thisConverter = new CustomFieldUom(UserSession.getInstance(request).getUserLocale(), BhaComponentHoursUsed.class, "hoursUsed");
					thisConverter.setBaseValueFromUserValue(hoursUsed);
					
					BhaComponentHoursUsed hoursUsedLog = new BhaComponentHoursUsed();
					hoursUsedLog.setComment(descr);
					hoursUsedLog.setHoursUsed(thisConverter.getConvertedValue());
					hoursUsedLog.setDailyUid(cbDayDate);
					hoursUsedLog.setGroupUid(UserSession.getInstance(request).getCurrentGroupUid());
					hoursUsedLog.setBharunUid(bharunUid);
					hoursUsedLog.setBhaComponentUid(bhaComponentUid);
					hoursUsedLog.setWellboreUid(wellboreUid);
					hoursUsedLog.setWellUid(wellUid);		
					//filter to get bharun's operation uid
					if (bharunUid!=null) {
						String[] paramsFields = {"bharunUid"};
						Object[] paramsValues = new Object[1]; 
						paramsValues[0] = bharunUid; 
						List<String> bhaOpList=ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select operationUid from Bharun where (isDeleted = false or isDeleted is null) and bharunUid=:bharunUid", paramsFields, paramsValues,qp);
						if (bhaOpList.size()>0)
						{							
							hoursUsedLog.setOperationUid(bhaOpList.get(0));							
						}	
					}
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(hoursUsedLog);
					
					populateBhaComponentTotalHoursUsed(bharunUid, bhaComponentUid);
					
				}else{
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Boolean delDailyHoursUsedLog(HttpServletRequest request, HttpServletResponse response, String bhaComponentHoursUsedUid, String bharunUid, String bhaComponentUid) {
		try {					
				if(bhaComponentHoursUsedUid != null){	
					QueryProperties qp = new QueryProperties();
					qp.setUomConversionEnabled(false);		
					
					//query base on the bhaComponentHoursUsedUid, set is_deleted = 1 if found to indicated deleted record			
					String strSql1 = "UPDATE BhaComponentHoursUsed SET isDeleted = 1 WHERE bhaComponentHoursUsedUid =:bhaComponentHoursUsedUid";
					String[] paramsFields1 = {"bhaComponentHoursUsedUid"};
					Object[] paramsValues1 = {bhaComponentHoursUsedUid};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);		
					
					populateBhaComponentTotalHoursUsed(bharunUid, bhaComponentUid);
				}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Boolean populateBhaComponentTotalHoursUsed(String bharunUid, String bhaComponentUid) {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		Double totalHoursUsed = 0.00;
		
		try {					
			if((bharunUid !=null && StringUtils.isNotEmpty(bharunUid)) && (bhaComponentUid !=null && StringUtils.isNotEmpty(bhaComponentUid))){
				// Query the sum of hours used from from BhaComponentHoursUsed table based on bharunUid & bhaComponentUid
				String queryString = "SELECT sum(hoursUsed) " +
				"FROM BhaComponentHoursUsed b " +
				"WHERE (b.isDeleted=false OR b.isDeleted is null) " +								
				"AND b.bharunUid = :bharunUid " +
				"AND b.bhaComponentUid = :bhaComponentUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"bharunUid", "bhaComponentUid"}, new Object[] {bharunUid, bhaComponentUid}, qp);
				if(lstResult != null){
					Object thisResult = (Object) lstResult.get(0);				
					if (thisResult != null) {
						totalHoursUsed = Double.parseDouble(thisResult.toString());						
					}
				}
				if (totalHoursUsed>=0){
					//Update bha component's total hours used with the sum of hours used from BhaComponentHoursUsed table based on bharunUid & bhaComponentUid			
					String strSql1 = "UPDATE BhaComponent SET totalHoursUsed =:totalHoursUsed WHERE bhaComponentUid =:bhaComponentUid and bharunUid =:bharunUid";							
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, new String[] {"bharunUid", "bhaComponentUid", "totalHoursUsed"}, new Object[] {bharunUid, bhaComponentUid, totalHoursUsed}, qp);
				}				
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
