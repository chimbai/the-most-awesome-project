package com.idsdatanet.d2.drillnet.lookupClassCode;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.LookupClassCode;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LookupClassCodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor{

	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		String selectedOperationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
		
		Object object = node.getData();
		if (object instanceof LookupClassCode) {
			LookupClassCode data = (LookupClassCode) object;
			data.setOperationCode(selectedOperationCode);
		}
	}

	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";


	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		
		if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
			String selectedOperationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
			if (StringUtils.isNotBlank(selectedOperationCode)){
				customCondition+=(customCondition.length()>0?" AND ":"")+" operationCode =:operationCode ";
				query.addParam("operationCode", selectedOperationCode);
			}
			else
			{
				customCondition = " 1 = 1";
			}
		}
		
		if (userSelection.getCustomProperty("reportOperationCodesFilter")!=null) {
			List<String> reportOperationCodesFilter = (List<String>) userSelection.getCustomProperty("reportOperationCodesFilter");
			if (reportOperationCodesFilter.size()>0) {
				customCondition+=(customCondition.length()>0?" AND ":"")+" (operationCode in (:reportOperationCodesFilter) or operationCode='' or operationCode is null) ";
				query.addParam("reportOperationCodesFilter", reportOperationCodesFilter);
			}
		}
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
