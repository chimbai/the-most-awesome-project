package com.idsdatanet.d2.drillnet.activity;


public class BHIActivityDataNodeListener extends ActivityDataNodeListener {
	/*
	@Override	
	protected void clientSpecificProcessAfterDataNodeSaveOrUpdate(CommandBeanTreeNode node) throws Exception{
		Object object = node.getData();
		
		if (object instanceof Activity) {
			Activity activity = (Activity) object;
			String operationUid = activity.getOperationUid();
			if (activity.getIncidentNumber()!=null) {
				int thisIncidentNumber = activity.getIncidentNumber();
				if (!"".equals(thisIncidentNumber)) {
					this.calculateIncidentLevel(operationUid,thisIncidentNumber);
				}
			}
		}
	}
	
	private void calculateIncidentLevel(String operationUid, int incidentNumber) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String querySql = "SELECT SUM(activityDuration) FROM Activity WHERE (isDeleted = false OR isDeleted is null)" +
				 " AND operationUid=:operationUid AND incidentNumber=:incidentNumber";
		List lstSumDuration = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(querySql, new String[] {"operationUid", "incidentNumber"}, new Object[]{operationUid, incidentNumber}, qp);
		Object duration = (Object) lstSumDuration.get(0);
		Double sumDurationForThisIncidentNumber = Double.parseDouble(duration.toString());
		
		int incidentLevel = 0;
		
		// check the level and update the incident level of the activity with the same incident number (based on the spec given by BHI)
		if (sumDurationForThisIncidentNumber >= 3600 && sumDurationForThisIncidentNumber <= 14400) {
			incidentLevel = 1;
		} else if (sumDurationForThisIncidentNumber >= 14401 && sumDurationForThisIncidentNumber <= 28800) {
			incidentLevel = 2;
		} else if (sumDurationForThisIncidentNumber >= 28801 && sumDurationForThisIncidentNumber <= 57600) {
			incidentLevel = 3;
		} else if (sumDurationForThisIncidentNumber >= 57601 && sumDurationForThisIncidentNumber <= 72000) {
			incidentLevel = 4;
		} else if (sumDurationForThisIncidentNumber >= 72001) {
			incidentLevel = 5;
		}
		
		if (incidentLevel > 0) {
			String updateSql = "UPDATE Activity SET incidentLevel=:incidentLevel WHERE (isDeleted = false OR isDeleted is null)" +
					" AND operationUid=:operationUid" +
					" AND incidentNumber=:incidentNumber";
			String[] paramNames = {"incidentLevel","operationUid","incidentNumber"};
			Object[] paramValues = {incidentLevel, operationUid, incidentNumber};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(updateSql, paramNames, paramValues, qp);	
		}
	}*/ 
}
