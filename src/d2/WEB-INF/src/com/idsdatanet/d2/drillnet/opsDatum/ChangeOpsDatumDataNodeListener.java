package com.idsdatanet.d2.drillnet.opsDatum;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.idsdatanet.d2.core.dao.hibernate.TableMapping;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.query.util.DatabaseUtil;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.hibernate.tables.UnitMapping;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.util.ClassUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.SystemExceptionHandler;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.operation.OperationUtils;

public class ChangeOpsDatumDataNodeListener extends EmptyDataNodeListener {
	private DataSource dataSource = null;
	public void setDataSource(DataSource value){
		this.dataSource = value;
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		if(object instanceof OpsDatum) {			
			DatumManager.loadDatum();
			commandBean.getFlexClientControl().setReloadParentHtmlPage();
			
			OpsDatum opsDatum = (OpsDatum) node.getData();
			OperationUtils.updateWellGroundLevelBasedOnDefaultDatum(opsDatum.getOperationUid());
		}
	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session,DataNodeProcessStatus status, HttpServletRequest request)throws Exception {
		Object object = node.getData();
		OpsDatumUtil.resetDatumFieldsIfIsGlReference(commandBean, object);
		if(object instanceof OpsDatum) {	
			OpsDatum opsDatum = (OpsDatum) object;
			Double reportingDatumOffsetNew = Double.parseDouble((String) node.getDynaAttr().get("reportingDatumOffset"));
			Double offsetMslNew = Double.parseDouble((String) node.getDynaAttr().get("offsetMsl"));
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, OpsDatum.class, "reportingDatumOffset");
			Double diff = (opsDatum.getReportingDatumOffset() + opsDatum.getOffsetMsl()) - (reportingDatumOffsetNew + offsetMslNew);
			thisConverter.setBaseValueFromUserValue(diff);
			this.fixDatumFields(thisConverter.getBasevalue(), opsDatum.getOperationUid(), opsDatum.getWellboreUid(), opsDatum.getWellUid());
			
			opsDatum.setReportingDatumOffset(reportingDatumOffsetNew);
			opsDatum.setOffsetMsl(offsetMslNew);
		}
	}
	
	public void fixDatumFields(Double differentValue, String operationUid, String wellboreUid, String wellUid) throws Exception {
		String sql = null;
		Connection connection = null; 
		Statement statement = null;
		try {
			connection = this.dataSource.getConnection();		
			statement = connection.createStatement();
			
			String strSql = "FROM UnitMapping WHERE datumConversion='1' AND (isDeleted = false or isDeleted is null) order by tableName, fieldName";
			List<UnitMapping> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			List<String> wellTablelist = new ArrayList<String>();
			wellTablelist.add("well");		
//			wellTablelist.add("lwd_formation_pressure_detail");
			wellTablelist.add("lwd_mud");
//			wellTablelist.add("operation_plan_phase_vault");
			wellTablelist.add("rcd_run");
			wellTablelist.add("production_pump_param_comp_log");
			wellTablelist.add("production_pump_param");
			
			List<String> wellboreTablelist = new ArrayList<String>();
			wellboreTablelist.add("bharun_daily_summary");
			wellboreTablelist.add("cement_fluid");
			wellboreTablelist.add("cement_job");
			wellboreTablelist.add("cement_stage");
			wellboreTablelist.add("corrosion_ring_properties");
			wellboreTablelist.add("downhole_log");
			wellboreTablelist.add("hole_section");
			wellboreTablelist.add("last_rig_state");
			wellboreTablelist.add("lost_circulation");
			wellboreTablelist.add("plug_and_abandon_detail");
			wellboreTablelist.add("schematic_annotation");
			wellboreTablelist.add("stand_drilling_parameters");
			wellboreTablelist.add("surface_casing_vent_flow_test");
			wellboreTablelist.add("tour_bharun_summary");
			wellboreTablelist.add("tour_survey_station");
			wellboreTablelist.add("well_production_test_detail");
			wellboreTablelist.add("wellbore_plug_back_log");
			wellboreTablelist.add("wellbore");
			
			for (UnitMapping um : lstResult) {
				String tableName = um.getTableName();
				String fieldName = um.getFieldName();
				String lastEditDateTime = ",last_edit_datetime=NOW(),record_owner_uid='uniqueRigId01',last_edit_user_uid='datumchange'";
				String cName =  DatabaseUtil.sqlTableToHQLTable(tableName);
				Class clz = TableMapping.getTableModelClass(cName);
				if (clz==null)  continue;
				String fName =  DatabaseUtil.sqlToHQL(fieldName);
				boolean check = ClassUtils.isPropertyExists(clz, fName);
				if (!check)  continue;
				if (wellTablelist.contains(tableName)) {
					sql = "UPDATE " + tableName + " SET " + fieldName + "=" + fieldName + (differentValue>=0?"+":"") + differentValue.toString() + lastEditDateTime
					+ " WHERE well_uid='" + wellUid +  "'";
				}else if (wellboreTablelist.contains(tableName)) {
					sql = "UPDATE " + tableName + " SET " + fieldName + "=" + fieldName + (differentValue>=0?"+":"") + differentValue.toString() + lastEditDateTime
					+ " WHERE wellbore_uid='" + wellboreUid +  "'";
				}else {
					if ("lwd_formation_pressure_detail".equals(tableName)) {
						sql = "UPDATE " + tableName + " SET " + fieldName + "=" + fieldName + (differentValue>=0?"+":"") + differentValue.toString() + lastEditDateTime +
								" Where lwd_formation_pressure_uid IN (select lwd_formation_pressure_uid FROM lwd_formation_pressure " +
								" WHERE operation_uid='" + operationUid +  "')";
					}else if ("operation_plan_phase_vault".equals(tableName)) {
						// Skip
					}else {
						sql = "UPDATE " + tableName + " SET " + fieldName + "=" + fieldName + (differentValue>=0?"+":"") + differentValue.toString() + lastEditDateTime
						+ " WHERE operation_uid='" + operationUid +  "'";
					}
				}
				statement.execute(sql);
			}
			
			
		}catch (Exception e) {
		
		}finally{
			if (connection !=null) connection.close();
			if (statement !=null) statement.close();							
		}	
	}
	
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		Object object = node.getData();
		if (object instanceof OpsDatum) {
		//	OpsDatum opsDatum = (OpsDatum) object;
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, OpsDatum.class, "reportingDatumOffset");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@reportingDatumOffset", thisConverter.getUOMMapping());
			}
			
			thisConverter.setReferenceMappingField(OpsDatum.class, "offsetMsl");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@offsetMsl", thisConverter.getUOMMapping());
			}
		}
	}
		
		
}
