package com.idsdatanet.d2.drillnet.report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.dao.XMLPropertiesField;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.report.MultipleUserSelectionSnapshot;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class OpsTeamManagementReportModule extends DefaultReportModule {
	private static final String MGT_REPORT_DATE = "mgt_report_date";
	private static final String MGT_REPORT_COMBINED_REPORT = "mgt_report_combined_report";
	
	private static final String FIELD_SELECTED_DATE = "mgtSelectedDate";
	private static final String FIELD_SELECTED_OPERATIONS = "mgtSelectedOperations";
	private static final String FIELD_DISTINCT_REPORT = "mgtDistinctReport";
	private static final String LATEST_REPORT_FLAG = "latestReport";
	
	private SimpleDateFormat formatter = null;
		
	/* not used
	private class ListOfDailyCondition implements HqlConditionHandler{
		private List<Daily> listOfDaily = null;
		
		ListOfDailyCondition(List<Daily> dailyList){
			this.listOfDaily = dailyList;
		}
		
		public String getHqlConditionClause(List<String> paramNames, List<Object> paramValues, String paramPrefix) throws Exception {
			if(this.listOfDaily == null) return "('')";
			if(this.listOfDaily.size() == 0) return "('')";
			
			StringBuffer buffer = new StringBuffer();
			int idx = 0;
			buffer.append("(");
			for(Daily daily: this.listOfDaily){
				if(idx > 0){
					buffer.append(",");
				}
				String name = paramPrefix + String.valueOf(++idx);
				buffer.append(":" + name);
				paramNames.add(name);
				paramValues.add(daily.getDailyUid());
			}
			buffer.append(")");
			return buffer.toString();
		}
	}
	*/
	
	public void setReportNameDateFormat(String format){
		this.formatter = new SimpleDateFormat(format);
	}
	
	private Map getSelectedOperations(CommandBean commandBean) throws Exception {
		Object selected_operations = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_OPERATIONS);
		if(selected_operations == null){
			return null;
		}
		if(StringUtils.isBlank(selected_operations.toString())){
			return null;
		}
		
		String[] id_list = selected_operations.toString().split(",");
		Map result = new HashMap();
		for(String id: id_list){
			result.put(id.trim(), null);
		}
		return result;
	}

	private boolean isEmptyString(Object value){
		if(value == null) return true;
		return StringUtils.isBlank(value.toString());
	}

	protected String getReportDisplayNameOnCreation(ReportFiles reportFile, ReportTypes reportType, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) throws Exception {
		String file_name_pattern = null;
		if(reportType != null){
			file_name_pattern = reportType.getReportFilenamePattern();
		}
		
		if(StringUtils.isBlank(file_name_pattern)){
			return null;
		}

		// look for <distinct></distinct> or <combined></combined>, both must be specified if used
		if(file_name_pattern.indexOf("<distinct>") != -1){
			if(this.isCombinedReport(userSelection)){
				file_name_pattern = this.getFileNamePattern(file_name_pattern, "combined");
			}else{
				file_name_pattern = this.getFileNamePattern(file_name_pattern, "distinct");
			}
		}
		
		return this.formatDisplayName(file_name_pattern, reportFile, operation, daily, reportDaily, userSelection, reportJobParams);
	}
	
	private String getFileNamePattern(String pattern, String subType){
		int start = pattern.indexOf("<" + subType + ">");
		if(start == -1) return pattern;
		int end = pattern.indexOf("</" + subType + ">");
		if(end == -1) return pattern;
		return pattern.substring(start + subType.length() + 2, end);
	}

	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		boolean useLocalizedCutoffTimeLogic = false;
		Date reportDate = null;
		Object dateParam = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_DATE);
		
		if(isEmptyString(dateParam)) dateParam = LATEST_REPORT_FLAG;
		
		if(LATEST_REPORT_FLAG.equals(dateParam)){
			reportDate = new Date();
			useLocalizedCutoffTimeLogic = true;
		}else{
			reportDate = CommonDateParser.parse(dateParam.toString());
			useLocalizedCutoffTimeLogic = false;
		}
		
		List<Daily> filtered_list = new ArrayList<Daily>();
		
		Map selected_operations = this.getSelectedOperations(commandBean);
		if(selected_operations != null){
			List<Daily> daily_list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic);
			for(Daily daily: daily_list){
				if(selected_operations.containsKey(daily.getOperationUid())){
					filtered_list.add(daily);
					reportDate = daily.getDayDate();
				}
			}
			
			

			boolean distinct_report = "true".equals(commandBean.getRoot().getDynaAttr().get(FIELD_DISTINCT_REPORT));
			String opsTeamUid = (String) commandBean.getRoot().getDynaAttr().get("mgtOpsTeamUid");
			
			if(distinct_report){
				return this.submitReportJob(this.createDistinctMultipleDaysUserSelection(session.getUserName(), reportDate, filtered_list, opsTeamUid), this.getParametersForReportSubmission(session, request, commandBean));
			}else{
				return this.submitReportJob(this.createCombinedMultipleDaysUserSelection(session.getUserName(), reportDate, filtered_list, opsTeamUid), this.getParametersForReportSubmission(session, request, commandBean));
			}
		}else{
			throw new Exception("No data fit the selected criteria therefore unable to generate report.");
		}
		
		/*
		String selectedOpsTeams = (String) commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_OPS_TEAMS);
		if (StringUtils.isNotBlank(selectedOpsTeams)) {
			List<Daily> dailyList = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic);
			
			List<UserSelectionSnapshot> l = new ArrayList<UserSelectionSnapshot>();
			
			for (String opsTeamUid : selectedOpsTeams.split(",")) {
				List<Daily> filteredList = new ArrayList<Daily>();
				Set<String> selectOperations = new HashSet<String>();
				List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select o.operationUid from OpsTeam t, OpsTeamOperation o where (t.isDeleted = false or t.isDeleted is null) and (o.isDeleted = false or o.isDeleted is null) and t.opsTeamUid = o.opsTeamUid and t.opsTeamUid = :opsTeamUid", "opsTeamUid", opsTeamUid);
				if (list != null) {
					for (String operationUid : list) {
						selectOperations.add(operationUid);
					}
				}
				
				for (Daily daily: dailyList) {
					if (selectOperations.contains(daily.getOperationUid())) {
						filteredList.add(daily);
						reportDate = daily.getDayDate();
					}
				}
				
				l.add(this.createCombinedMultipleDaysUserSelection(session.getUserName(), reportDate, filteredList, opsTeamUid));
			}
			this.submitReportJob(l, this.getParametersForReportSubmission(session, request, commandBean));
		}else{
			throw new Exception("No data fit the selected criteria therefore unable to generate report.");
		}
		*/
	}

	private List<UserSelectionSnapshot> createMultipleDaysUserSelection(String userLogin, List<Daily> list) throws Exception {
		List<UserSelectionSnapshot> result = new ArrayList<UserSelectionSnapshot>();
		for(Daily daily: list){
			result.add(UserSelectionSnapshot.getInstanceForCustomDaily(userLogin, daily.getDailyUid()));
		}
		return result;
	}

	private UserSelectionSnapshot createCombinedMultipleDaysUserSelectionFromScheduler(String login, boolean excludeTightHoleOperation) throws Exception {
		Date reportDate = new Date();
		boolean useLocalizedCutoffTimeLogic = true;
		List<Daily> list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic, excludeTightHoleOperation);
		return this.createCombinedMultipleDaysUserSelection(login, reportDate, list);
	}
	
	private UserSelectionSnapshot createCombinedMultipleDaysUserSelection(String login, Date reportDate, List<Daily> days) throws Exception {
		return this.createCombinedMultipleDaysUserSelection(login, reportDate, days, null);
	}

	private UserSelectionSnapshot createCombinedMultipleDaysUserSelection(String login, Date reportDate, List<Daily> days, String opsTeamUid) throws Exception {
		MultipleUserSelectionSnapshot userSelection = new MultipleUserSelectionSnapshot(UserSelectionSnapshot.getInstanceForCustomLogin(login));
		userSelection.addCopies(this.createMultipleDaysUserSelection(login, days));
		
		for(Daily daily: days){
			reportDate = daily.getDayDate();
		}
		
		userSelection.setCustomProperty(MGT_REPORT_DATE, reportDate);
		userSelection.setCustomProperty(MGT_REPORT_COMBINED_REPORT, new Boolean(true));
		if (StringUtils.isNotBlank(opsTeamUid)) {
			userSelection.setCustomProperty("opsTeamUid", opsTeamUid);
			List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select name from OpsTeam t where t.opsTeamUid = :opsTeamUid", "opsTeamUid", opsTeamUid);
			if (rs != null && !rs.isEmpty()) {
				userSelection.setCustomProperty("opsTeamName", rs.get(0));
			}
		}
		return userSelection;
	}
	
	protected String parseDisplayNameParam(String key, ReportFiles reportFiles, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) {
		if(FILE_NAME_PATTERN_DATE.equalsIgnoreCase(key)){
			try{
				return null2EmptyString(ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(userSelection.getCustomProperties().get(MGT_REPORT_DATE)));
			}catch(Exception e){
				Log logger = LogFactory.getLog(this.getClass());
				logger.error(e.getMessage(), e);
				return null;
			}
		}else {
			return super.parseDisplayNameParam(key, reportFiles, operation, daily, reportDaily, userSelection, reportJobParams);
		}
	}
	
	private List<UserSelectionSnapshot> createDistinctMultipleDaysUserSelection(String login, Date reportDate, List<Daily> days) throws Exception {
		return this.createDistinctMultipleDaysUserSelection(login, reportDate, days, null);
	}

	private List<UserSelectionSnapshot> createDistinctMultipleDaysUserSelection(String login, Date reportDate, List<Daily> days, String opsTeamUid) throws Exception {
		List<UserSelectionSnapshot> result = this.createMultipleDaysUserSelection(login, days);
		for(UserSelectionSnapshot userSelection : result){
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if (daily != null) reportDate = daily.getDayDate();
			userSelection.setCustomProperty(MGT_REPORT_DATE, reportDate);
			userSelection.setCustomProperty(MGT_REPORT_COMBINED_REPORT, new Boolean(false));
			if (StringUtils.isNotBlank(opsTeamUid)) {
				userSelection.setCustomProperty("opsTeamUid", opsTeamUid);
				List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select name from OpsTeam t where t.opsTeamUid = :opsTeamUid", "opsTeamUid", opsTeamUid);
				if (rs != null && !rs.isEmpty()) {
					userSelection.setCustomProperty("opsTeamName", rs.get(0));
				}
			}
		}
		return result;
	}

	void generateManagementReportOnCurrentSystemDatetime(String loginUser, ReportAutoEmail reportAutoEmail) throws Exception {
		UserSelectionSnapshot userSelection = this.createCombinedMultipleDaysUserSelectionFromScheduler(loginUser, true);
		
		//do a check see if got data or not, if not will skip generating report
		//due to this is auto report, so time is always current server time
		Date reportDate = new Date();
		boolean useLocalizedCutoffTimeLogic = true;
		List<Daily> daily_list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic);
		
		//if not empty then fire
		if (!daily_list.isEmpty()){
			ReportJobParams params = new ReportJobParams();
			params.setUserSessionReportJob(false);
			params.setReportAutoEmail(reportAutoEmail);
			params.setPaperSize(super.defaultPaperSize);			
			this.submitReportJob(userSelection, params);
		}
	}

	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		String report_type = this.getReportType(userContext.getUserSelection());
		if(this.isCombinedReport(userContext.getUserSelection())){
			String name = report_type + " " + this.formatter.format(userContext.getUserSelection().getCustomProperty(MGT_REPORT_DATE)) + (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
			String opsTeamUid = (String) userContext.getUserSelection().getCustomProperty("opsTeamUid");
			if (StringUtils.isNotBlank(opsTeamUid)) {
				return report_type + "/" + opsTeamUid + "/" + ReportUtils.replaceInvalidCharactersInFileName(name);
			}
			return report_type + "/" + ReportUtils.replaceInvalidCharactersInFileName(name);
		}else{
			Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
			if(operation == null) throw new Exception("Operation must be selected");
			String name = operation.getOperationName() + " " + this.formatter.format(userContext.getUserSelection().getCustomProperty(MGT_REPORT_DATE)) + (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + " " + "." + this.getOutputFileExtension();
			return report_type + "/" + operation.getOperationUid() + "/" + ReportUtils.replaceInvalidCharactersInFileName(name);
		}
	}
	
	private boolean isCombinedReport(UserSelectionSnapshot userSelection){
		Object value = userSelection.getCustomProperty(MGT_REPORT_COMBINED_REPORT);
		if(value instanceof Boolean) return (Boolean) value;
		return false;
	}

	protected void beforeSaveReportEntry(JobContext jobContext, ReportFiles report) throws Exception{
		XMLPropertiesField extraProperties = new XMLPropertiesField();
		
		if (this.isCombinedReport(jobContext.getUserContext().getUserSelection())) {
			String delimiter = "";
			StringBuilder sb = new StringBuilder();
			MultipleUserSelectionSnapshot multipleUserSelection = (MultipleUserSelectionSnapshot) jobContext.getUserContext().getUserSelection();
			for (UserSelectionSnapshot userSelection : multipleUserSelection.getCopies()) {
				sb.append(delimiter);
				sb.append(userSelection.getOperationUid());
				delimiter = "\t";
			}
			extraProperties.setProperty("tabSeparatedOperationUid", sb.toString());
			
			report.setReportOperationUid(null);
			report.setReportDayDate((Date) jobContext.getUserContext().getUserSelection().getCustomProperty(MGT_REPORT_DATE));
		} else {
			extraProperties.setProperty("tabSeparatedOperationUid", report.getReportOperationUid());
		}
		
		report.setExtraProperties(extraProperties.serializePropertiesToXmlString());
		
		String opsTeamUid = (String) jobContext.getUserContext().getUserSelection().getCustomProperty("opsTeamUid");
		if (StringUtils.isNotBlank(opsTeamUid)) {
			report.setOpsTeamUid(opsTeamUid);
		} else {
			report.setOpsTeamUid(null);
		}
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if(meta.getTableClass().equals(ReportOutputFile.class)){
			return this.loadReportOutputFiles(userSelection, request, commandBean);
		}else{
			return null;
		}
	}
	
	public List<Object> loadReportOutputFiles(UserSelectionSnapshot userSelection, HttpServletRequest request, CommandBean commandBean) throws Exception {
		List<Object> accessibleReportFiles = new ArrayList<Object>();
		UserSession userSession = UserSession.getInstance(request);
		
		String opsTeamUid = (String) commandBean.getRoot().getDynaAttr().get("mgtOpsTeamUid");
		Set<String> opsTeamOperations = null;
		if (StringUtils.isNotBlank(opsTeamUid)) {
			opsTeamOperations = new HashSet<String>();
			List<String> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select o.operationUid from OpsTeam t, OpsTeamOperation o where (t.isDeleted = false or t.isDeleted is null) and (o.isDeleted = false or o.isDeleted is null) and o.opsTeamUid = t.opsTeamUid and t.opsTeamUid = :opsTeamUid", "opsTeamUid", opsTeamUid);
			if (rs != null) {
				for (String operationUid : rs) {
					opsTeamOperations.add(operationUid);
				}
			}
		}
		
		for (Object obj : super.loadReportOutputFiles(userSelection, request)) {
			ReportOutputFile reportOutputFile = (ReportOutputFile) obj;
			boolean forbidden = false;
			XMLPropertiesField extraProperties = new XMLPropertiesField();
			extraProperties.readPropertiesFromXmlString(reportOutputFile.getExtraProperties());
			String tabSeparatedOperationUid = extraProperties.getProperty("tabSeparatedOperationUid");
			if (StringUtils.isNotBlank(tabSeparatedOperationUid)) {
				for (String operationUid : tabSeparatedOperationUid.split("\t")) {
					// deny access if one of the operations is not accessible, or, one of the operations is not in the selected team
					if (!userSession.getCachedAllAccessibleOperations().containsKey(operationUid) || (opsTeamOperations != null && !opsTeamOperations.contains(operationUid))) {
						forbidden = true;
						break;
					}
				}
			}
			if (!forbidden) {
				accessibleReportFiles.add(obj);
			}
		}
		return accessibleReportFiles;
	}

	protected String getReportDisplayName(ReportFiles reportFile, Operation operation, UserSelectionSnapshot userSelection) throws Exception{
		if(StringUtils.isNotBlank(reportFile.getDisplayName())) return reportFile.getDisplayName();
		if(operation == null){
			//change file name to auto follow report type
			return this.getReportType() + " " + this.formatter.format(reportFile.getReportDayDate());
		}else{
			return super.getReportDisplayName(reportFile, operation, userSelection);
		}
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("flexMgtReportLoadOperationList".equals(invocationKey)) {
			boolean useLocalizedCutoffTimeLogic = false;
			Date reportDate = null;
			String value = request.getParameter("reportDate");
			request.getSession().setAttribute("opsTeamMgtReportController.root.dynaAttr[mgtSelectedDate]", value); // FIXME
			String opsTeamUid = (String) commandBean.getRoot().getDynaAttr().get("mgtOpsTeamUid");
			
			if(LATEST_REPORT_FLAG.equals(value)){
				reportDate = new Date();
				useLocalizedCutoffTimeLogic = true;
			}else{
				reportDate = CommonDateParser.parse(value);
				useLocalizedCutoffTimeLogic = false;
			}
			
			List<Daily> list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic);
			
			Set<String> opsTeamOperations = null;
			if (StringUtils.isNotBlank(opsTeamUid)) {
				opsTeamOperations = new HashSet<String>();
				List<String> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select o.operationUid from OpsTeam t, OpsTeamOperation o where (t.isDeleted = false or t.isDeleted is null) and (o.isDeleted = false or o.isDeleted is null) and o.opsTeamUid = t.opsTeamUid and t.opsTeamUid = :opsTeamUid", "opsTeamUid", opsTeamUid);
				if (rs != null) {
					for (String operationUid : rs) {
						opsTeamOperations.add(operationUid);
					}
				}
			}
			
			//get all the user allowed to access operation
			Operations accessibleOperations = UserSession.getInstance(request).getCachedAllAccessibleOperations();
			
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			for(Daily rec:list){
				Operation o = ApplicationUtils.getConfiguredInstance().getCachedOperation(rec.getOperationUid());
				if (o==null) continue;
				
				//check if operation is accessible or not
				// deny access if one of the operations is not accessible, or, one of the operations is not in the selected team
				if (accessibleOperations.containsKey(rec.getOperationUid()) && (opsTeamOperations == null || opsTeamOperations.contains(rec.getOperationUid()))) {
					writer.startElement("Operation");
					writer.addElement("operationUid", rec.getOperationUid());
					String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(UserSession.getInstance(request).getCurrentGroupUid(), rec.getOperationUid());
					writer.addElement("operationName", operationName);
					writer.endElement();
				}				
			}
			writer.endElement();
			writer.close();
		} else {
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);
		}
	}
}
