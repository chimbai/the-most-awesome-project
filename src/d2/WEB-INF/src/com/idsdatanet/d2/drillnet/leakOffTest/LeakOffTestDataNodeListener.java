package com.idsdatanet.d2.drillnet.leakOffTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.graph.GraphManager;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.bharun.BharunUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class LeakOffTestDataNodeListener extends EmptyDataNodeListener {
	private Boolean autoCalculateMAASP = true;
	private GraphManager graphManager = null;
	private String graphTitle=null;
		
	public GraphManager getGraphManager() {
		return graphManager;
	}

	public void setGraphManager(GraphManager graphManager) {
		this.graphManager = graphManager;
	}
	
	public void setAutoCalculateMAASP(Boolean value){
		this.autoCalculateMAASP = value;
	}

	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {

		Object object = node.getData();
		if (object instanceof LeakOffTest) {
			LeakOffTest leakofftest = (LeakOffTest)object;
			UserContext userContext = UserContext.getUserContext(userSelection);
			
			Map<String, Object> customProperties = new HashMap<String, Object>();
			customProperties.put("leakOffTestUid", leakofftest.getLeakOffTestUid());
			
			this.graphManager.process(userContext, request, null, "leakofftest", "medium", customProperties);
			//Calculate EMW using existing node (to cater when changing DATUM)
			LeakOffTestUtil.calculateEMW(commandBean, object);
			if(this.autoCalculateMAASP) {
				this.calculateMAASP(commandBean, object);
			}
			
			if (leakofftest.getEstimatedMudWeight() != null)
			  {
				  CustomFieldUom theConverter = new CustomFieldUom(commandBean);
				  theConverter.setReferenceMappingField(LeakOffTest.class, "estimatedMudWeight");
				  theConverter.setBaseValueFromUserValue(leakofftest.getEstimatedMudWeight(), false);
				  if (theConverter.isUOMMappingAvailable()){
					  node.setCustomUOM("@estimatedMudWeight", theConverter.getUOMMapping());
				  }
				  Double estimatedMudWeightOriValue = theConverter.getConvertedValue();
				  DecimalFormat df = new DecimalFormat("#.#");
				  
				  BigDecimal estimatedMudWeight = new BigDecimal(estimatedMudWeightOriValue);
				  estimatedMudWeight = estimatedMudWeight.setScale(1, RoundingMode.DOWN);
				  
				  node.getDynaAttr().put("estimatedMudWeight", df.format(estimatedMudWeight));
			  }
		}
	}
	
	public void afterDataNodeProcessed(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, String action, int operationPerformed, boolean errorOccurred) throws Exception{
		Object obj = node.getData();
		
		if (obj instanceof LeakOffTest) {
			LeakOffTest leakofftest = (LeakOffTest)obj;
			
			LeakOffTestUtil.calculateEMW(commandBean, obj);
			
			//get bharun daily summary for this day to do calc
			String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) AND dailyUid=:DailyUid";		
			String[] paramsFields = {"DailyUid"};
			String[] paramsValues = {leakofftest.getDailyUid()};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			if (lstResult.size() > 0){
				for(Object objBhaDaily: lstResult){
					BharunDailySummary thisBharunDailySummary = (BharunDailySummary) objBhaDaily;
					String bharunUid = thisBharunDailySummary.getBharunUid(); 
					
					// call method to populate System hydraulic Horse Power if GWP turned on
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_SYS_HYDRAULIC_HORSE_POWER))) {
						CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
						BharunUtils.saveSysHydraulicHorsepower(thisConverter, bharunUid, leakofftest.getDailyUid(), commandBean.getSystemMessage(), session.getCurrentWellboreUid());
					}
					
					// call method to populate Bit Pressure Loss Percentage if GWP turned on
					if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_BIT_PRESSURE_LOSS_PERCENTAGE))) {
						CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
						BharunUtils.saveBitPressureLossPercentage(thisConverter,bharunUid, leakofftest.getDailyUid(), commandBean.getSystemMessage(), session.getCurrentWellboreUid());
					}
				}
				
			}
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
	}
	
	private void calculateMAASP(CommandBean commandBean, Object object) throws Exception{
		if (object instanceof LeakOffTest) {
			LeakOffTest leakofftest = (LeakOffTest)object;

			//Calculation for MAASP in SI (Pa) = (kg/m3 - kg/m3) * 9.81 * m, all in IDS base unit 
			Double dblMaxAllowableFluidDensity = 0.0;
			Double dblMudDensity = 0.0;
			Double dblShoeTvdMsl = 0.0;
			Double dblMaaspPressure = 0.0;
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, LeakOffTest.class, "max_allowable_fluid_density");
			if(leakofftest.getMaxAllowableFluidDensity() != null)
			{
				thisConverter.setBaseValueFromUserValue(leakofftest.getMaxAllowableFluidDensity());
				dblMaxAllowableFluidDensity = thisConverter.getBasevalue();
			}
			
			if(leakofftest.getMudDensity() != null)
			{
				thisConverter.setReferenceMappingField(LeakOffTest.class, "mud_density");
				thisConverter.setBaseValueFromUserValue(leakofftest.getMudDensity());
				dblMudDensity = thisConverter.getBasevalue();
			}
			
			if(leakofftest.getShoeTvdMsl() != null)
			{
				thisConverter.setReferenceMappingField(LeakOffTest.class, "shoe_tvd_msl");
				thisConverter.setBaseValueFromUserValue(leakofftest.getShoeTvdMsl());
				dblShoeTvdMsl = thisConverter.getBasevalue();
			}
					
			dblMaaspPressure = (dblMaxAllowableFluidDensity - dblMudDensity) * 9.81 * dblShoeTvdMsl;
			thisConverter.setReferenceMappingField(LeakOffTest.class, "maasp_pressure");
			thisConverter.setBaseValue(dblMaaspPressure);
			leakofftest.setMaaspPressure(Math.floor(thisConverter.getConvertedValue()));	//round down by request
			//End of MAASP calculation
			
			String strSqlUpdate = "UPDATE LeakOffTest SET maaspPressure =:maaspPressure WHERE leakOffTestUid =:leakOffTestUid";	
			String[] paramNames = {"maaspPressure", "leakOffTestUid"};
			Object[] paramValues = {thisConverter.getConvertedValue(), leakofftest.getLeakOffTestUid()};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
		}
	}
	
}
