package com.idsdatanet.d2.drillnet.rigstock;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.EnvironmentDischarges;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class RigStockDataNodeListener extends EmptyDataNodeListener {
	
	private String getLastDayForFWR;
	private Boolean calculateUseable = true;
	private String stockType;
	private Boolean isCombined = false;
	private String itemCostLookupReference;
	
	public void setIsCombined(Boolean value){
		this.isCombined = value;
	}
	
	public void setGetLastDayForFWR(String value){
		this.getLastDayForFWR = value;
	}
	
	public void setCalculateUseable(Boolean value){
		this.calculateUseable = value;
	}
	
	public void setType(String type)
	{
		this.stockType = type;
	}
	
	public void setItemCostLookupReference(String itemCostLookupReference) {
		this.itemCostLookupReference = itemCostLookupReference;
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		RigStockUtil.populateRigStock(commandBean, node, userSelection, request, thisConverter, getLastDayForFWR, stockType, isCombined); 
	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RigStock.class, "amt_start");
		
		if(isCombined){
			String[] detail = RigStockUtil.getStockClassDetail(node);
			if (detail != null){
				if (RigStockUtil.checkStockExist(session.getCurrentDailyUid(), detail[0], detail[2], detail[1])){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "stockCode", "The value already exists in the database");
					return;
				}
			}
		}
		
		RigStockUtil.saveRigStock(commandBean, node, session, request, status, thisConverter, stockType, calculateUseable, isCombined, itemCostLookupReference);
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{

	}
	
	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{	//on paste as new record data with initial amount will be set to null
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RigStock.class, "amt_start");
		
		Object object = targetNode.getData();
		Boolean isBatchDrill = userSession.getCurrentOperation().getIsBatchDrill();
		if (isBatchDrill==null)
			isBatchDrill=false;
		if (object instanceof RigStock) {
			if (isCombined){
				stockType = RigStockUtil.getRigStockType(targetNode);
			}
			RigStockUtil.setIsBatchDrillFlag(targetNode, new UserSelectionSnapshot(userSession));
			RigStock rigstock = (RigStock)object;
			
			Double previousBalance = 0.0;
			
			// get the current date from the session
			if (!isBatchDrill)
			{
				rigstock.setAmtInitial(null);	

				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSession.getCurrentDailyUid());
				
				previousBalance = CommonUtil.getConfiguredInstance().getOperationStockcodePreviousBalance(rigstock.getOperationUid(), rigstock.getRigInformationUid(), rigstock.getStockCode(), daily, stockType);
				thisConverter.setBaseValue(previousBalance);
				if (thisConverter.isUOMMappingAvailable()){
					targetNode.setCustomUOM("@previousBalance", thisConverter.getUOMMapping());
				}
				targetNode.getDynaAttr().put("previousBalance", thisConverter.getConvertedValue());	
			}else{
				if (targetNode.getDynaAttr().get("currentBalance")!=null)
				{
					rigstock.setAmtInitial((Double) targetNode.getDynaAttr().get("currentBalance"));
					targetNode.getDynaAttr().remove("previousBalance");
				}else{
					
					Double currentBalance = null;
					if (this.calculateUseable == true) {
						currentBalance = rigstock.getUseable();
					}else{
						if (rigstock.getAmtInitial()!=null){
							Double amountToday = rigstock.getAmtStart() - rigstock.getAmtUsed() + rigstock.getAmtDiscrepancy();
							currentBalance = rigstock.getAmtInitial() + amountToday;
						}else{
							Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(rigstock.getDailyUid());
							previousBalance = CommonUtil.getConfiguredInstance().getOperationStockcodePreviousBalance(rigstock.getOperationUid(), rigstock.getRigInformationUid(), rigstock.getStockCode(), daily, stockType);
							thisConverter.setBaseValue(previousBalance);
							currentBalance = thisConverter.getConvertedValue();
						}
					}
					
					rigstock.setAmtInitial(currentBalance);
				}	
			}
			//CALCULATE USEABLE AMOUNT ON PASTE AS NEW
			Double dblPreviousBalance = rigstock.getAmtInitial()!=null?rigstock.getAmtInitial():thisConverter.getConvertedValue();
			Double dblRequiredMinimum = 0.0;
			Double dblUseable = 0.0;
			
			if (this.calculateUseable == true) {
				if(rigstock.getRequiredAmountMin() != null) dblRequiredMinimum = rigstock.getRequiredAmountMin();
				dblUseable = dblPreviousBalance - dblRequiredMinimum;
				rigstock.setUseable(dblUseable);
			}
					
			targetNode.getDynaAttr().put("currentBalance", null);
			targetNode.getDynaAttr().put("cumRecvd", null);
			targetNode.getDynaAttr().put("cumUsed", null);
			targetNode.getDynaAttr().put("cumCost", null);
			
			//reset all figure to null when copy from yesterday
			rigstock.setAmtStart(null);
			rigstock.setAmtUsed(null);
			rigstock.setAmtDiscrepancy(null);
			rigstock.setTrfToRig(null);
			rigstock.setTrfToBeach(null);
			rigstock.setAmtReturned(null);
			rigstock.setDailycost(null);
			targetNode.getDynaAttr().put("isFirstDay", CommonUtil.getConfiguredInstance().getOperationStockcodeFirstDay(userSession.getCurrentOperationUid(), rigstock.getStockCode(), userSession.getCurrentDailyUid()));
			

		}
	}
	
	public void onDataNodeCarryForwardFromYesterday(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception{
		//on carry record data with initial amount will be set to null
		
		Object object = node.getData();
		
		if (object instanceof RigStock) {
			Boolean isBatchDrill = session.getCurrentOperation().getIsBatchDrill();
			if (isBatchDrill==null)
				isBatchDrill=false;
			
			RigStock rigstock = (RigStock)object;
			if (isBatchDrill)
			{
				if (node.getDynaAttr().get("currentBalance")!=null)
				{
					rigstock.setAmtInitial((Double) node.getDynaAttr().get("currentBalance"));
					node.getDynaAttr().remove("previousBalance");
				}
			}else{
				rigstock.setAmtInitial(null);	
			}
			
			rigstock.setAmtStart(null);
			rigstock.setAmtUsed(null);
			rigstock.setAmtDiscrepancy(null);
			rigstock.setDailycost(null);
		}
	}		
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof RigStock) {	
			if (isCombined){
				stockType = RigStockUtil.getRigStockType(node);
			}
			RigStock rigStock = (RigStock) object;
			if (userSelection.getDailyUid() != null){
				rigStock.setMudVendor(RigStockUtil.getMudVendorCompany(userSelection.getDailyUid()));				
			}
			rigStock.setType(stockType);
		}		
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Double sumAmount = null;
		Object obj = node.getData();
		if (obj instanceof RigStock) {
			String dailyUid = session.getCurrentDailyUid();
			String sql = "SELECT SUM(amtUsed) FROM RigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND supportVesselInformationUid IS NULL " 
					+ "AND dailyUid = :dailyUid " 
					+ "AND stockCode IN (SELECT lookupRigStockUid FROM LookupRigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND stockBrand IN ('DIESEL' , 'FUEL', 'DIESEL OIL'))";
			String[] paramsFields = {"dailyUid"};
			Object[] paramsValues = {dailyUid};
			List res = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,paramsFields,paramsValues);
			if(res.size()>0){
				if(res.get(0) != null) {
					sumAmount = Double.parseDouble(res.get(0).toString());
					String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='MODU Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
					String[] paramsFields1 = {"dailyUid"};
					String[] paramsValues1 = {dailyUid};
					List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
					if (resED.size() > 0) {
						for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
							thisEnvironmentDischarges.setActualAmount(sumAmount);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
						}
					}	
				}else {
					String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='MODU Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
					String[] paramsFields1 = {"dailyUid"};
					String[] paramsValues1 = {dailyUid};
					List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
					if (resED.size() > 0) {
						for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
							thisEnvironmentDischarges.setActualAmount(sumAmount);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
						}
					}
				}
			}else {
				String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='MODU Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
				String[] paramsFields1 = {"dailyUid"};
				String[] paramsValues1 = {dailyUid};
				List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
				if (resED.size() > 0) {
					for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
						thisEnvironmentDischarges.setActualAmount(sumAmount);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
					}
				}
			}
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Double sumAmount = null;
		Object obj = node.getData();
		if (obj instanceof RigStock) {
			String dailyUid = session.getCurrentDailyUid();
			String sql = "SELECT SUM(amtUsed) FROM RigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND supportVesselInformationUid IS NULL " 
					+ "AND dailyUid = :dailyUid " 
					+ "AND stockCode IN (SELECT lookupRigStockUid FROM LookupRigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND stockBrand IN ('DIESEL' , 'FUEL', 'DIESEL OIL'))";
			String[] paramsFields = {"dailyUid"};
			Object[] paramsValues = {dailyUid};
			List res = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,paramsFields,paramsValues);
			if(res.size()>0){
				if(res.get(0) != null) {
					sumAmount = Double.parseDouble(res.get(0).toString());
					String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='MODU Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
					String[] paramsFields1 = {"dailyUid"};
					String[] paramsValues1 = {dailyUid};
					List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
					if (resED.size() > 0) {
						for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
							thisEnvironmentDischarges.setActualAmount(sumAmount);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
						}
					}	
				}else {
					String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='MODU Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
					String[] paramsFields1 = {"dailyUid"};
					String[] paramsValues1 = {dailyUid};
					List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
					if (resED.size() > 0) {
						for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
							thisEnvironmentDischarges.setActualAmount(sumAmount);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
						}
					}
				}
			}else {
				String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='MODU Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
				String[] paramsFields1 = {"dailyUid"};
				String[] paramsValues1 = {dailyUid};
				List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
				if (resED.size() > 0) {
					for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
						thisEnvironmentDischarges.setActualAmount(sumAmount);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
					}
				}
			}
		}
	}
}
