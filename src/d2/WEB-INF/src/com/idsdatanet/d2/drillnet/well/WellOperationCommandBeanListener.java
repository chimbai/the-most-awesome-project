package com.idsdatanet.d2.drillnet.well;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dynamicfield.DynamicFieldUtil;
import com.idsdatanet.d2.core.model.DynamicFieldMeta;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.operation.OperationUtils;

public class WellOperationCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(targetCommandBeanTreeNode != null){
			Object object = targetCommandBeanTreeNode.getData();
			
			if(Operation.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())){
				String targetField = targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField();
				if("@parentWellboreUid".equals(targetField) || "operationCode".equals(targetField) || "@copyFromOperationUid".equals(targetField) || 
						"operationalCode".equals(targetField)){
					
					this.populate(commandBean, targetCommandBeanTreeNode, request);
					
					if("operationCode".equals(targetField) || "operationalCode".equals(targetField)){												
						this.populateOperationNamePrefix(targetCommandBeanTreeNode, request);
						
						if (object instanceof Operation) {
							Operation operation = (Operation) object;
							if (operation.getOperationCode() != null){
								String opCode = operation.getOperationCode();
								String relatedOpCode = OperationUtils.getMobilizationRelatedOpCode(opCode);
								if (relatedOpCode !=null){
									targetCommandBeanTreeNode.getDynaAttr().put("reportingDatumOffset", 0);
									targetCommandBeanTreeNode.getDynaAttr().put("offsetMsl", 0);
								}
							}							
						}
		
					}

					if (commandBean.getFlexClientControl() != null) {
						commandBean.getFlexClientControl().setReloadAfterPageCancel();
					}
				}else if("uomTemplateUid".equals(targetField)){
					OperationUtils.changeUOMTemplate(targetCommandBeanTreeNode, request);
				}else if("@well.onOffShore".equals(targetField)){
					OperationUtils.setDefaultDatumReferencePoint(targetCommandBeanTreeNode);
					
					if (object instanceof Operation) {
						Operation operation = (Operation) object;
						if (operation.getOperationCode() != null){
							String opCode = operation.getOperationCode();
							String relatedOpCode = OperationUtils.getMobilizationRelatedOpCode(opCode);
							if (relatedOpCode !=null){
								targetCommandBeanTreeNode.getDynaAttr().put("reportingDatumOffset", 0);
								targetCommandBeanTreeNode.getDynaAttr().put("offsetMsl", 0);
							}
						}							
					}
					
				}else if("@well.tzGmtOffset".equals(targetField)){					
					this.populateOperationNamePrefix(targetCommandBeanTreeNode, request);
				}
			}
		}
	}
	
	private void populateOperationNamePrefix(CommandBeanTreeNode node, HttpServletRequest request) throws Exception {		
		if (request !=null){
			UserSession session = UserSession.getInstance(request);
			UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
			Object object = node.getData();
			if (object instanceof Operation) {
				Operation operation = (Operation) object;
				Double offset =0.0;	
				
				if (node.getDynaAttr().get("well.tzGmtOffset") !=null && !"".equals(node.getDynaAttr().get("well.tzGmtOffset"))) {
					offset = Double.parseDouble(node.getDynaAttr().get("well.tzGmtOffset").toString());
				}
				String operationPrefix = OperationUtils.getOperationNamePrefix(userSelection, operation, offset);
				if (StringUtils.isNotBlank(operationPrefix)) node.getDynaAttr().put("wellName", operationPrefix);
			}
		}
	}
	
	public static void copyDataFromOperation(CommandBeanTreeNode node, String sourceOperationUid) throws Exception {
		if(node.getDataDefinition() == null) return;
		if(node.getDataDefinition().getTableClass() != Operation.class) return;

		Operation selectedOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, sourceOperationUid);
		Wellbore parentWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, selectedOperation.getWellboreUid());
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, parentWellbore.getWellUid());
		loadOperation(node, selectedOperation);
		loadWell(node, well);
		loadWellbore(node, parentWellbore);		
		//refresh the node so every ui component is reloaded - if not the multi-select component is not activated
		node.refreshNodeAfterDataChangedProgrammatically();
	}
	
	private void populate(CommandBean commandBean, CommandBeanTreeNode node, HttpServletRequest request) throws Exception {
		Operation operation = (Operation) node.getData();
		String parentWellboreUid = (String) node.getDynaAttr().get("parentWellboreUid");
		String copyFromOperationUid = (String) node.getDynaAttr().get("copyFromOperationUid");
		String operationType = operation.getOperationCode();
		String name = (String) node.getDynaAttr().get("wellName");
		boolean isNewRecord = node.getInfo().isNewlyCreated();
		UserSession session = UserSession.getInstance(request);
		
		if(CommonUtil.getConfiguredInstance().isValidForCheckingWellNameDuplicate(session, name, parentWellboreUid, operationType, isNewRecord)) {
			if(CommonUtil.getConfiguredInstance().isWellNameDuplicate(name, operationType, parentWellboreUid)) {
				commandBean.getSystemMessage().addWarning("A <b>Well</b> with this name already exist in the system. You can still proceed but this will create duplicate wells of the same name in the system.", true, true);
			}
		}
		
		if ("_currentSelectedOperationUid".equals(copyFromOperationUid)) {
			// this is a marker set by custom add new button at flex side
			copyFromOperationUid = session.getCurrentOperationUid();
			
			node.getDynaAttr().put("showCopyFrom", "true");
		}
		
		//see if user select to copy data from another well or not, if Yes only take data from that well
		if (StringUtils.isNotBlank(copyFromOperationUid)) {
			copyDataFromOperation(node, copyFromOperationUid);
		}		
		else if (StringUtils.isNotBlank(parentWellboreUid) && StringUtils.isBlank(copyFromOperationUid)) {
			if (isDrilling(operation)) {
				// user is probably trying to create wellbore, load well
				Wellbore parentWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, parentWellboreUid);
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, parentWellbore.getWellUid());
				loadWell(node, well);
			} else {
				// user is probably trying to create operation, load well and wellbore
				Wellbore parentWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, parentWellboreUid);
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, parentWellbore.getWellUid());
				loadWell(node, well);
				loadWellbore(node, parentWellbore);
			}
		}
		
		//set the copy from operation selection to null, so when page refresh copy from will not fire again
		node.getDynaAttr().put("copyFromOperationUid", null);
		
		OperationUtils.setAccessToOpsTeamOnlyFlag(node, new UserSelectionSnapshot(session));
		
		OperationUtils.setDefaultDatumReferencePoint(node);
		
		if("ON".equals(node.getDynaAttr().get("well.onOffShore"))){
			node.getDynaAttr().put("well.glHeightMsl", "");
		}
	}

	
	
	private static void loadOperation(CommandBeanTreeNode node, Operation operation) throws Exception {
		setDynaAttr(node, "wellName", operation.getOperationName());
		if (isDrilling(operation)) {
			Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
			setDynaAttr(node, "parentWellboreUid", wellbore.getParentWellboreUid());
		} else {
			setDynaAttr(node, "parentWellboreUid", operation.getWellboreUid());
		}
		
		// clone the Operation, DO NOT reference the current one because it may still be in use at this point in time (if it is the current selected Operation)
		Operation newOperation = (Operation) BeanUtils.cloneBean(operation);
		newOperation.setOperationUid(null);
		newOperation.setOperationCode(null);
		newOperation.setSpudDate(null);
		
		node.setData(newOperation);
		
		//set datum to null, and force user to pick rather auto and lead to data error in future
		// create a new OpsDatum, as it is Operation dependent
		//if (StringUtils.isNotBlank(operation.getDefaultDatumUid())) {
			//OpsDatum opsDatum = (OpsDatum) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OpsDatum.class, operation.getDefaultDatumUid());
			//if (opsDatum != null && (opsDatum.getIsDeleted() == null || !opsDatum.getIsDeleted())) {
				//setDynaAttr(node, "rtmsl", opsDatum.getOffsetMsl());
				//setDynaAttr(node, "datumType", opsDatum.getDatumCode());
	
			//}
		//}
		newOperation.setDefaultDatumUid(null);
	}
	
	private static boolean isDrilling(Operation operation) {
		return operation.getOperationCode().startsWith(Constant.DRLLG); // e.g. DRLLG, DRLLG_NEWCODE (woodside_au_off)
	}
	
	private static void loadWellbore(CommandBeanTreeNode node, Wellbore wellbore) throws Exception {
		
		Map<String, Object> wellboreFieldValues = PropertyUtils.describe(wellbore);
		for (Map.Entry entry : wellboreFieldValues.entrySet()) {
			setDynaAttr(node, "wellbore." + entry.getKey(), entry.getValue());
		}
		
		Map<String, Object> wellboreDynamicFieldValues = DynamicFieldUtil.getFieldValues(wellbore.getGroupUid(), wellbore);
		Map<String, DynamicFieldMeta> wellboreDynamicFieldMetaMap = DynamicFieldUtil.getConfiguredInstance().getDynamicFieldMeta(Wellbore.class, wellbore.getGroupUid());
		for (Iterator i = wellboreDynamicFieldMetaMap.keySet().iterator(); i.hasNext(); ) {
			String fieldName = (String) i.next();
			if (wellboreDynamicFieldValues.containsKey(fieldName)) {
				Object value = wellboreDynamicFieldValues.get(fieldName);
				if (value instanceof Double) {
					setDynaAttr(node, "wellbore." + fieldName, String.valueOf(value));
				} else {
					setDynaAttr(node, "wellbore." + fieldName, value);
				}
			}
		}
	}
	
	private static void loadWell(CommandBeanTreeNode node, Well well) throws Exception {
		
		Map<String, Object> wellFieldValues = PropertyUtils.describe(well);
		for (Map.Entry entry : wellFieldValues.entrySet()) {
			setDynaAttr(node, "well." + entry.getKey(), entry.getValue());
		}
				
		Map<String, Object> wellDynamicFieldValues = DynamicFieldUtil.getFieldValues(well.getGroupUid(), well);
		Map<String, DynamicFieldMeta> wellDynamicFieldMetaMap = DynamicFieldUtil.getConfiguredInstance().getDynamicFieldMeta(Well.class, well.getGroupUid());
		for (Iterator i = wellDynamicFieldMetaMap.keySet().iterator(); i.hasNext(); ) {
			String fieldName = (String) i.next();
			if (wellDynamicFieldValues.containsKey(fieldName)) {
				Object value = wellDynamicFieldValues.get(fieldName);
				if (value instanceof Double) {
					setDynaAttr(node, "well." + fieldName, String.valueOf(value)); // see ticket 20080409-1753-atang
				} else {
					setDynaAttr(node, "well." + fieldName, value);
				}
			}
		}
	}

	private static void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		}
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if (OperationUtils.CHECK_WELL_NAME_EXIST.equals(invocationKey)) {
			OperationUtils.checkWellNameExistence(request, response);
		} else if (OperationUtils.CHANGE_DATUM.equals(invocationKey)) {
			OperationUtils.changeDatum(commandBean, request, response);
		}
		return;
	}

	
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		OperationUtils.setCustomUom(true, commandBean, userSelection);
	}
}
