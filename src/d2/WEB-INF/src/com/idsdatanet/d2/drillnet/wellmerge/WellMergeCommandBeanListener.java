package com.idsdatanet.d2.drillnet.wellmerge;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.hibernate.TableMapping;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.depot.core.util.DepotUtils;

public class WellMergeCommandBeanListener extends EmptyCommandBeanListener {
	
	public static final String WELL_MERGE = "wellMerge";
	public static final String WELLBORE_MERGE = "wellboreMerge";
	public static final String OPERATION_MERGE = "operationMerge";
	
	private List<?> _excludedTables = null;
	private Map<String, Map<String, String>> fieldMapping = null;
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		
		StringBuilder query = new StringBuilder();
		query.append("<query>");
		query.append(System.getProperty("line.separator"));
		// Copies all Wellbores from Source Well to Target Well
		if (WELL_MERGE.equals(invocationKey)) {
			this.mergeWell(request, query);
		// Copies all Operations from Source Wellbore to Target Wellbore
		} else if (WELLBORE_MERGE.equals(invocationKey)) {
			this.mergeWellbore(request, query);
		// Copies all Dailys from Source Operation to Target Operation
		} else if (OPERATION_MERGE.equals(invocationKey)) {
			this.mergeOperation(request, query);
		}
		query.append("</query>");
		response.getWriter().write(query.toString());
		return;
	}
	
	private void mergeWell(HttpServletRequest request, StringBuilder query) throws Exception {
		String sourceWellUid = request.getParameter("sourceWell");
		String targetWellUid = request.getParameter("targetWell");
		if(!StringUtils.equals(sourceWellUid, targetWellUid)) {
			this.remapObjectChildren(sourceWellUid, targetWellUid, "wellUid", "Well", query);
			this.mergeObjectData(sourceWellUid, targetWellUid, "Well", Well.class, query);
		}
	}
	
	private void mergeWellbore(HttpServletRequest request, StringBuilder query) throws Exception {
		String sourceWellUid = request.getParameter("sourceWell");
		String targetWellUid = request.getParameter("targetWell");
		String sourceWellboreUid = request.getParameter("sourceWellbore");
		String targetWellboreUid = request.getParameter("targetWellbore");
		if(StringUtils.equals(sourceWellUid, targetWellUid)) {
			if(!StringUtils.equals(sourceWellboreUid, targetWellboreUid)) {
				this.remapObjectChildren(sourceWellboreUid, targetWellboreUid, "wellboreUid", "Wellbore", query);
				this.mergeObjectData(sourceWellboreUid, targetWellboreUid, "Wellbore", Wellbore.class, query);
			}
		}
	}
	
	private void mergeOperation(HttpServletRequest request, StringBuilder query) throws Exception {
		String sourceWellboreUid = request.getParameter("sourceWellbore");
		String targetWellboreUid = request.getParameter("targetWellbore");
		String sourceOperationUid = request.getParameter("sourceOperation");
		String targetOperationUid = request.getParameter("targetOperation");
		if(StringUtils.equals(sourceWellboreUid, targetWellboreUid)) {
			if(!StringUtils.equals(sourceOperationUid, targetOperationUid)) {
				this.remapObjectChildren(sourceOperationUid, targetOperationUid, "operationUid", "Operation", query);
				this.mergeObjectData(sourceOperationUid, targetOperationUid, "Operation", Operation.class, query);
			}
		}
	}
	
	private void mergeObjectData(String sourceUid, String targetUid, String key, Class<?> className, StringBuilder query) throws Exception {
		
		// Get the objects
		Object sourceObject = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(className, sourceUid);
		Object targetObject = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(className, targetUid);
		
		// Field Mapping
		if (this.fieldMapping != null && this.fieldMapping.containsKey(key)) {
			Map<String, String> fieldList = this.fieldMapping.get(key);
			for (Map.Entry<String, String> entry : fieldList.entrySet()) {
				// Do merging here
			    //System.out.println(entry.getKey() + "/" + entry.getValue());
			}
		}
		
		// Delete old
		if(PropertyUtils.isWriteable(sourceObject, "isDeleted")) {
			PropertyUtils.setProperty(sourceObject, "isDeleted", true);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(sourceObject);
		}
	}
	
	// Remap Children of Object
	@SuppressWarnings("rawtypes")
	private void remapObjectChildren(String sourceUid, String targetUid, String key, String className, StringBuilder query) throws Exception {
		if(StringUtils.isEmpty(sourceUid) || StringUtils.isEmpty(targetUid) || StringUtils.isEmpty(key) || StringUtils.isEmpty(className)) {
			return;
		}
		List<Class> all_tables = TableMapping.getTableModel();
		if (all_tables != null) {
			for (Class clazz : all_tables) {
				Map class_properties = PropertyUtils.describe(clazz.newInstance());
				if (class_properties.containsKey(key)) {
					if (this._excludedTables!=null && (this._excludedTables.contains(clazz.getSimpleName()) || StringUtils.equals(clazz.getSimpleName(), className))) {
						continue;
					}
					String hql = "UPDATE " + clazz.getName() + " SET " + key + " = :targetUid WHERE " + key + " = :sourceUid";
					String[] paramName = {"targetUid", "sourceUid"};
					String[] paramValue = {targetUid, sourceUid};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(hql, paramName, paramValue);
					
					// Output SQL String
					String sqlOutput = "UPDATE " + 
							DepotUtils.getConfiguredInstance().convertCamelCaseToUnderscore(clazz.getSimpleName()) + " SET " + 
							DepotUtils.getConfiguredInstance().convertCamelCaseToUnderscore(key) + " = :targetUid WHERE " + 
							DepotUtils.getConfiguredInstance().convertCamelCaseToUnderscore(key) + " = :sourceUid;";
					sqlOutput = sqlOutput.replace(":targetUid", "'" + targetUid + "'");
					sqlOutput = sqlOutput.replace(":sourceUid", "'" + sourceUid + "'");
					query.append(sqlOutput);
					query.append(System.getProperty("line.separator"));
				}
			}
			String sqlDelete = "UPDATE " + 
				DepotUtils.getConfiguredInstance().convertCamelCaseToUnderscore(className) + " SET is_deleted = 1 WHERE " + 
				DepotUtils.getConfiguredInstance().convertCamelCaseToUnderscore(key) + " = :sourceUid;";
			sqlDelete = sqlDelete.replace(":sourceUid", "'" + sourceUid + "'");
			query.append(sqlDelete);
			query.append(System.getProperty("line.separator"));
		}
	}
	
	public List<?> getExcludedTables() {
		return _excludedTables;
	}

	public void setExcludedTables(List<?> tables) {
		_excludedTables = tables;
	}
	
	public Map<String, Map<String, String>> getFieldMapping() {
		return fieldMapping;
	}

	public void setFieldMapping(Map<String, Map<String, String>> fieldMapping) {
		this.fieldMapping = fieldMapping;
	}
	
}
