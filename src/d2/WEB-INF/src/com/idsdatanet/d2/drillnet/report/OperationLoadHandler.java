package com.idsdatanet.d2.drillnet.report;

import java.util.Date;
import java.util.List;

public interface OperationLoadHandler {

	public List getOperationList(Date startDate, Date endDate) throws Exception;

}
