package com.idsdatanet.d2.drillnet.matrix;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

/**
 * A simple class to hold matrix data.
 * @author RYAU
 *
 */
public class Matrix {
	MatrixDataDefinitionMeta meta = null;
	String url = null;
	private List<JSONObject> data = new ArrayList<JSONObject>();
	private String isIncompleteState = "N/A";
	
	public Matrix(MatrixDataDefinitionMeta meta) {
		this.meta = meta;
		this.url = meta.getUrl();
	}

	/**
	 * Return module name
	 * @return
	 */
	public String getModuleName() {
		return this.meta.getTitle();
	}
	
	/**
	 * Return url link to the module
	 * @return
	 */
	public String getUrl() {
		return this.url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public MatrixDataDefinitionMeta getMetaDefinition() {
		return this.meta;
	}
	public List<JSONObject> getData() {
		return this.data;
	}
	/**
	 * Add data to matrix list
	 * @param dailyUid
	 * @param url
	 */
	public void addData(String value, String isIncompleteState) {
		JSONObject jsonData = new JSONObject();
		jsonData.put("dailyUid",value);
		jsonData.put("isIncompleteState",isIncompleteState);
		this.data.add(jsonData);
		this.checkIncomplete();
	}
	/**
	 * Check if given dailyUid exists in this matrix list
	 * @return
	 */
	public boolean exist(String value) {
		if(data.size() > 0) {
			if(data.contains(value)) {
				return true;
			}
		}
		return false;
	}
	
	private void checkIncomplete() {
		if(data.size() > 0) {
			for(JSONObject daily : data) {
				if(daily.has("isIncompleteState")){
					if(daily.getString("isIncompleteState") == "true") this.isIncompleteState = "true";
				}
			}
		}
	}
}
