package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.PlanOperationTime;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class PlanOperationTimeDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof PlanOperationTime) {
			CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
			
			PlanOperationTime thisPlanOperationTime = (PlanOperationTime) object;
			if (thisPlanOperationTime.getOperationHours() != null &&  thisPlanOperationTime.getContingencyPercentage() != null){
				thisPlanOperationTime.setPlannedOperationHours(OperationPlanMasterUtils.getPlannedOperationHours(thisPlanOperationTime.getOperationHours(), thisPlanOperationTime.getContingencyPercentage(), thisConverter));
			}
			else
				thisPlanOperationTime.setPlannedOperationHours(null);
		}
	}
		
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {

		if (meta.getTableClass().equals(PlanOperationTime.class)) {

			String operationHour = "";
			String strSql = "SELECT operationHour FROM Operation WHERE (isDeleted is null or isDeleted = false) AND operationUid = :operationUid AND groupUid = :groupUid";
			String[] paramsFields = {"operationUid", "groupUid"};
			Object[] paramsValues = {userSelection.getOperationUid(), userSelection.getGroupUid()};
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			if (lstResult.size() > 0){
				Object result = (Object) lstResult.get(0);
				if (result != null) {
					if (!StringUtils.isBlank(result.toString())) operationHour = result.toString() + " hours";
				}
			}
			commandBean.getRoot().getDynaAttr().put("rigOperationSchedule", operationHour);
			
			//BY DEFAULT, LOAD DATA BY sequence, phaseShortCode
			strSql = "FROM PlanOperationTime WHERE operationUid = :operationUid AND groupUid = :groupUid AND (isDeleted IS NULL OR isDeleted = '') ORDER BY sequence, phaseShortCode";
			String[] paramNames =  {"operationUid", "groupUid"};
			Object[] paramValues = {userSelection.getOperationUid(), userSelection.getGroupUid()};
						
			List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			List<Object> output_maps = new ArrayList<Object>();
			
			for(Object objResult: items){
				PlanOperationTime planOperationTime = (PlanOperationTime) objResult;
				output_maps.add(planOperationTime);
			}
						
			return output_maps;
		}
		return null;

	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}
	
}
