package com.idsdatanet.d2.drillnet.report;

import com.idsdatanet.d2.core.report.BeanDataPreProcessor;
import com.idsdatanet.d2.core.util.ClassUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

public class CommandBeanDeletedDayFilter implements BeanDataPreProcessor {
	private class DaysFilter extends EmptyDataNodeListener {
		private List<String> excludedClasses;
		private Map<String,String> dailys;
		
		DaysFilter(List<String> excludedClasses, Map<String,String> dailys){
			this.excludedClasses = excludedClasses;
			this.dailys = dailys;
		}
		
		private boolean isCheck(TreeModelDataDefinitionMeta meta){
			if(this.excludedClasses == null) return true;
			if(this.excludedClasses.contains(meta.getTableClass().getSimpleName())) return false;
			return true;
		}
		
		public List<Object> preFilterLoadedRawChildRecords(List<Object> data, CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
			if(! isCheck(meta)) return null;
			
			if(! ClassUtils.isPropertyExists(meta.getTableClass(), "dailyUid")) return null;
			
			List<Object> newList = new ArrayList<Object>();
			for(Object obj: data){
				String dailyUid = BeanUtils.getProperty(obj, "dailyUid");
				if(StringUtils.isBlank(dailyUid) || this.dailys.containsKey(dailyUid)){
					newList.add(obj);
				}
			}
			
			return newList;
		}
	}
	
	private Map<String, List<String>> exclude_deleted_day_checking = null;
	private Map<String, Map<String,String>> operation_daily = new HashMap<String, Map<String,String>>();
	
	/**
	 * set entry that should be excluded from "deleted day" filter in the format of:
	 * 
	 * module::class_name
	 * 
	 * for example: Activity::Activity
	 * 
	 * @param values
	 */
	public void setExcludeChecking(List<String> values){
		if(this.exclude_deleted_day_checking == null) this.exclude_deleted_day_checking = new HashMap<String, List<String>>();
		
		for(String value: values){
			String[] parts = value.split("::");
			List<String> list = this.exclude_deleted_day_checking.get(parts[0].trim());
			if(list == null){
				list = new ArrayList<String>();
				this.exclude_deleted_day_checking.put(parts[0].trim(), list);
			}
			list.add(parts[1].trim());
		}
	}

	public void dataGenerationStart(){
		// do nothing
	}
	
	public void dataGenerationEnd(){
		this.operation_daily.clear();
	}
	
	private Map<String,String> getValidDaily(String operationUid) throws Exception {
		Map<String,String> result = this.operation_daily.get(operationUid);
		if(result != null) return result;
		
		result = new HashMap<String,String>();
		List<String> daily_ids = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select distinct A.dailyUid from Daily A, ReportDaily B where A.dailyUid = B.dailyUid and (A.isDeleted is null or A.isDeleted = :dailyDeleted) and (B.isDeleted is null or B.isDeleted = :reportDailyDeleted) and A.operationUid = :operationUid", new String[] {"dailyDeleted","reportDailyDeleted","operationUid"}, new Object[] {new Boolean(false), new Boolean(false), operationUid});
	
		for(String id : daily_ids){
			result.put(id, ""); // store in map for easier retrieval
		}
		
		this.operation_daily.put(operationUid, result);
		return result;
	}
	
	private List<String> getExcludedClasses(String module){
		if(this.exclude_deleted_day_checking == null) return null;
		
		List<String> module_classes = this.exclude_deleted_day_checking.get(module);
		List<String> for_all_classes = this.exclude_deleted_day_checking.get("*");
		
		if(module_classes == null && for_all_classes == null) return null;
		
		List<String> result = new ArrayList<String>();
		if(module_classes != null) result.addAll(module_classes);
		if(for_all_classes != null) result.addAll(for_all_classes);
		
		return result;
	}
	
	public void preProcessBean(String module, String beanId, Object bean, UserSelectionSnapshot userSelection) throws Exception{
		if(bean instanceof CommandBean){
			List<String> excludedClasses = this.getExcludedClasses(module);
			if(excludedClasses != null){
				if(excludedClasses.contains("*")) return;
			}
			BaseCommandBean commandBean = (BaseCommandBean) bean;
			commandBean.setSystemDataNodeListener(new DaysFilter(excludedClasses, this.getValidDaily(userSelection.getOperationUid())));
		}
	}
}
