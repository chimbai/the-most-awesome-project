package com.idsdatanet.d2.drillnet.operationPlanMaster;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.CostAfeDetail;
import com.idsdatanet.d2.core.model.CostAfeDetailAdjusted;
import com.idsdatanet.d2.core.model.CostAfeDetailVault;
import com.idsdatanet.d2.core.model.CostAfeMaster;
import com.idsdatanet.d2.core.model.CostAfeMasterAdjusted;
import com.idsdatanet.d2.core.model.CostAfeMasterVault;
import com.idsdatanet.d2.core.model.OperationAfe;
import com.idsdatanet.d2.core.model.OperationPlanMaster;
import com.idsdatanet.d2.core.model.OperationPlanMasterVault;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.OperationPlanPhaseVault;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class SantosOperationPlanDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener, DataNodeAllowedAction {
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
	
	
	}
	
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean); 
		if(object instanceof OperationPlanPhase) {
			OperationPlanPhase phase = (OperationPlanPhase) object;
			String phaseCode = phase.getPhaseCode(); //(String) PropertyUtils.getProperty(object, "phaseCode");
			Double budget = this.calculatePhaseBudget(userSelection.getOperationUid(), phaseCode, null);
			if (budget!=null) phase.setPhaseBudget(budget);
			budget = this.calculatePhaseBudget(userSelection.getOperationUid(), phaseCode, "original");
			if (budget!=null) node.getDynaAttr().put("phaseBudgetOriginal", budget);
		} else if(object instanceof OperationPlanMaster || object instanceof OperationPlanMasterVault) {
			String costType = (String) PropertyUtils.getProperty(object, "costPhaseType");
			String gwpvalue = null;
			String altgwpvalue = null;
			String sqlCondition = "";
			List<String> excludedPhaseCode = new ArrayList<String>();
			List<String> altExcludedPhaseCode = new ArrayList<String>();
			
			if (StringUtils.isNotBlank(costType)) {
				
				if ("CS".equalsIgnoreCase(costType)) {
					gwpvalue = GroupWidePreference.getValue(userSelection.getGroupUid(), "phasesExcludedForCaseAndSuspend");
					altgwpvalue = GroupWidePreference.getValue(userSelection.getGroupUid(), "phasesExcludedForPlugAndAbandon");
				}else if ("PA".equalsIgnoreCase(costType)) {
					gwpvalue = GroupWidePreference.getValue(userSelection.getGroupUid(), "phasesExcludedForPlugAndAbandon");
					altgwpvalue = GroupWidePreference.getValue(userSelection.getGroupUid(), "phasesExcludedForCaseAndSuspend");
				}
				
				if (StringUtils.isNotBlank(gwpvalue)) {
					String strCondtion = "";
					
					for (String item:gwpvalue.toString().split(";")) {
						String phase = item.toString();
						if (StringUtils.isNotBlank(phase)) {
							strCondtion = strCondtion + "'" + phase + "',";
							excludedPhaseCode.add(item.toString());
						}
					}
					
					strCondtion = StringUtils.substring(strCondtion, 0, -1);
					sqlCondition = "and phaseCode NOT in (" + strCondtion + ")";
				}
				
				if (StringUtils.isNotBlank(altgwpvalue)) {
					for (String item:altgwpvalue.toString().split(";")) {
						String phase = item.toString();
						if (StringUtils.isNotBlank(phase)) {
							altExcludedPhaseCode.add(item.toString());
						}
					}
				}
			}
			
			Double totalPhaseBudget = 0.0;
			String childClassName = "";
			Double totalPhaseBudgetOriginal = 0.0;
			
			if (object instanceof OperationPlanMaster) {
				totalPhaseBudget = this.calculateTotalPhaseBudget(userSelection.getOperationUid(), sqlCondition, null);
				totalPhaseBudgetOriginal = this.calculateTotalPhaseBudget(userSelection.getOperationUid(), sqlCondition, "original");
				childClassName = "OperationPlanPhase";
			} else if (object instanceof OperationPlanMasterVault) {
				childClassName = "OperationPlanPhaseVault";
			}
			
			if (totalPhaseBudget !=null) {
				thisConverter.setReferenceMappingField(OperationPlanPhase.class, "phaseBudget");
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@totalPhaseBudget", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalPhaseBudget", totalPhaseBudget);
			}	
			
			if (totalPhaseBudgetOriginal !=null) {
				thisConverter.setReferenceMappingField(OperationPlanPhase.class, "phaseBudget");
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@totalPhaseBudgetOriginal", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalPhaseBudgetOriginal", totalPhaseBudgetOriginal);
			}	
			
			//calculate breakdown 
			Map<String, CommandBeanTreeNode> mapList = node.getChild(childClassName);
			Double cumP10 = 0.0;
			Double cumP50 = 0.0;
			Double cumP90 = 0.0;
			Double altCumP10 = 0.0;
			Double altCumP50 = 0.0;
			Double altCumP90 = 0.0;
			for (Iterator iter = mapList.entrySet().iterator(); iter.hasNext();) 
			{
				Map.Entry entry = (Map.Entry) iter.next();
				CommandBeanTreeNode childNode = (CommandBeanTreeNode)entry.getValue();
				Object childObj = childNode.getData();
				
				if(childObj instanceof OperationPlanPhase || childObj instanceof OperationPlanPhaseVault) {
					String phaseCode = (String) PropertyUtils.getProperty(childObj, "phaseCode");
					Double budget = (Double) PropertyUtils.getProperty(childObj, "phaseBudget");
					
					if (excludedPhaseCode.contains(phaseCode)) {
						Double p10d = (Double) PropertyUtils.getProperty(childObj, "p10Duration");
						if (p10d!=null) altCumP10 += p10d; 
						childNode.getDynaAttr().put("cumP10Duration", altCumP10);

						Double p50d = (Double) PropertyUtils.getProperty(childObj, "p50Duration");
						if (p50d!=null) altCumP50 += p50d; 
						childNode.getDynaAttr().put("breakdownPct", "-");
						childNode.getDynaAttr().put("cumP50Duration", altCumP50);

						Double p90d = (Double) PropertyUtils.getProperty(childObj, "p90Duration");
						if (p90d!=null) altCumP90 += p90d; 
						childNode.getDynaAttr().put("cumP90Duration", altCumP90);

					}else {
						if (budget==null || budget == 0 || totalPhaseBudget == null || totalPhaseBudget == 0) {
							childNode.getDynaAttr().put("breakdownPct", "-");
						}else {
							Double pct = (budget / totalPhaseBudget) * 100;
							NumberFormat formatter = new DecimalFormat("#0.00");
							childNode.getDynaAttr().put("breakdownPct", formatter.format(pct) + " %");
						}
						
						//calculate cum. phase duration
						Double p10d = (Double) PropertyUtils.getProperty(childObj, "p10Duration");
						if (p10d != null) {
							cumP10 += p10d;
							if (!altExcludedPhaseCode.contains(phaseCode)) altCumP10 += p10d; 
						}
						childNode.getDynaAttr().put("cumP10Duration", cumP10);
						
						Double p50d = (Double) PropertyUtils.getProperty(childObj, "p50Duration");
						if (p50d != null) {
							cumP50 += p50d;
							if (!altExcludedPhaseCode.contains(phaseCode)) altCumP50 += p50d; 
						}
						childNode.getDynaAttr().put("cumP50Duration", cumP50);
						
						Double p90d = (Double) PropertyUtils.getProperty(childObj, "p90Duration");
						if (p90d != null) {
							cumP90 += p90d;
							if (!altExcludedPhaseCode.contains(phaseCode)) altCumP90 += p90d; 
						}
						childNode.getDynaAttr().put("cumP90Duration", cumP90);

					}	
					thisConverter.setReferenceMappingField(childObj.getClass(), "p10Duration");	
					if (thisConverter.isUOMMappingAvailable()){
						childNode.setCustomUOM("@cumP10Duration", thisConverter.getUOMMapping());
					}
					thisConverter.setReferenceMappingField(childObj.getClass(), "p50Duration");	
					if (thisConverter.isUOMMappingAvailable()){
						childNode.setCustomUOM("@cumP50Duration", thisConverter.getUOMMapping());
					}
					thisConverter.setReferenceMappingField(childObj.getClass(), "p90Duration");	
					if (thisConverter.isUOMMappingAvailable()){
						childNode.setCustomUOM("@cumP90Duration", thisConverter.getUOMMapping());
					}
				}
			}	

		}
		
	}
	
	private Double calculatePhaseBudget(String operationUid, String phaseCode, String type) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String adjusted = "original".equals(type)?"":"Adjusted";
		String strSql = "SELECT SUM(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) " +
				"FROM CostAfeDetail"+adjusted+" d, CostAfeMaster"+adjusted+" m " +
				"where (d.isDeleted = false OR d.isDeleted is null) " +
				"AND (m.isDeleted = false OR m.isDeleted is null) " +
				"and d.costAfeMaster"+adjusted+"Uid=m.costAfeMaster"+adjusted+"Uid " +
				"and m.operationUid=:operationUid " + 
				"and d.phaseCode=:phaseCode ";
		List<Double> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"operationUid","phaseCode"}, new Object[]{operationUid, phaseCode}, qp);
		
		if (lstResult.size() > 0){
			return lstResult.get(0);
		}
		return null;
	}
	
	private Double calculateTotalPhaseBudget(String operationUid, String condition, String type) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String adjusted = "original".equals(type)?"":"Adjusted";
		String strSql = "SELECT SUM(coalesce(d.itemCost,0.0) * coalesce(d.quantity,0.0) * coalesce(d.estimatedDays,0.0)) " +
			"FROM CostAfeDetail"+adjusted+" d, CostAfeMaster"+adjusted+" m " +
			"where (d.isDeleted = false OR d.isDeleted is null) " +
			"AND (m.isDeleted = false OR m.isDeleted is null) " +
			"and d.costAfeMaster"+adjusted+"Uid=m.costAfeMaster"+adjusted+"Uid " +
			"and m.operationUid=:operationUid " + 
			condition.replaceAll("phaseCode", "d.phaseCode");
		List<Double> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid, qp);
		
		if (lstResult.size() > 0){
			return lstResult.get(0);
		}
		return null;
	}
	
	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean,String invocationKey) throws Exception {
		if ("carryAFEFromActualOrVault".equalsIgnoreCase(invocationKey)) {
			
			String source = request.getParameter("selectedAFEsource");
			String sourceUid = request.getParameter("selectedAFEUid");
			String carryAfe = request.getParameter("carryAfe");
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDatumConversionEnabled(false);

			if (StringUtils.isNotBlank(sourceUid)) {
				if ("actual".equalsIgnoreCase(source) || "adjusted".equalsIgnoreCase(source)) {
					List<OperationPlanMaster> opm = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationPlanMaster where operationPlanMasterUid=:masterUid and" +
							" (isDeleted = false OR isDeleted is null)", "masterUid", sourceUid, qp);
					
					if (opm.size()>0) {
						OperationPlanMaster operationPlanMaster = opm.get(0) ;
						String sourceOperationUid = operationPlanMaster.getOperationUid();
						
						OperationPlanMaster newDvdPlan = new OperationPlanMaster();

						PropertyUtils.copyProperties(newDvdPlan, operationPlanMaster);
						newDvdPlan.setOperationPlanMasterUid(null);
						newDvdPlan.setWellUid(null);
						newDvdPlan.setWellboreUid(null);
						newDvdPlan.setOperationUid(null);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newDvdPlan ,qp);
						
						
						List<OperationPlanPhase> opp = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationPlanPhase where operationPlanMasterUid=:masterUid and" +
								" (isDeleted = false OR isDeleted is null)", "masterUid", sourceUid ,qp);
						if (opp.size()>0) {
							
							for (OperationPlanPhase operationPlanPhase : opp){
						
								OperationPlanPhase newDvdPlanPhase = new OperationPlanPhase();

								PropertyUtils.copyProperties(newDvdPlanPhase, operationPlanPhase);
								newDvdPlanPhase.setOperationPlanPhaseUid(null);
								newDvdPlanPhase.setOperationPlanMasterUid(newDvdPlan.getOperationPlanMasterUid());
								newDvdPlanPhase.setWellUid(null);
								newDvdPlanPhase.setWellboreUid(null);
								newDvdPlanPhase.setOperationUid(null);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newDvdPlanPhase, qp);
								
							}
						}
						
						if (BooleanUtils.toBoolean(carryAfe)) {
							if ("actual".equalsIgnoreCase(source)) {
								String queryString = "FROM CostAfeMaster WHERE (isDeleted=false or isDeleted is NULL) AND operationUid=:operationUid";
								List<CostAfeMaster> master = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", sourceOperationUid, qp);
								if (master.size()>0) {
									CostAfeMaster afe = master.get(0);
									CostAfeMaster masterAfe = new CostAfeMaster();
									PropertyUtils.copyProperties(masterAfe, afe);
									masterAfe.setCostAfeMasterUid(null);
									masterAfe.setWellUid(null);
									masterAfe.setWellboreUid(null);
									masterAfe.setOperationUid(null);
									ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(masterAfe ,qp);
									
									OperationAfe operationAfe = new OperationAfe();
									operationAfe.setAfeType("MASTER");
									operationAfe.setCostAfeMasterUid(masterAfe.getCostAfeMasterUid());
									ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(operationAfe);
									
									List<CostAfeDetail> detailList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM CostAfeDetail " +
											"WHERE (isDeleted=false or isDeleted is null) and costAfeMasterUid=:sourceUid", "sourceUid", afe.getCostAfeMasterUid(), qp);
									for (CostAfeDetail detail : detailList) {
										CostAfeDetail newDetail = new CostAfeDetail();
										PropertyUtils.copyProperties(newDetail, detail);
										newDetail.setCostAfeDetailUid(null);
										newDetail.setCostAfeMasterUid(masterAfe.getCostAfeMasterUid());
										newDetail.setWellUid(null);
										newDetail.setWellboreUid(null);
										newDetail.setOperationUid(null);
										ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newDetail, qp);
									}
								}
							} else {
								String queryString = "FROM CostAfeMasterAdjusted WHERE (isDeleted=false or isDeleted is NULL) AND operationUid=:operationUid";
								List<CostAfeMasterAdjusted> master = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", sourceOperationUid, qp);
								if (master.size()>0) {
									CostAfeMasterAdjusted afe = master.get(0);
									CostAfeMaster masterAfe = new CostAfeMaster();
									PropertyUtils.copyProperties(masterAfe, afe);
									masterAfe.setCostAfeMasterUid(null);
									masterAfe.setWellUid(null);
									masterAfe.setWellboreUid(null);
									masterAfe.setOperationUid(null);
									ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(masterAfe ,qp);
									
									OperationAfe operationAfe = new OperationAfe();
									operationAfe.setAfeType("MASTER");
									operationAfe.setCostAfeMasterUid(masterAfe.getCostAfeMasterUid());
									ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(operationAfe);
									
									List<CostAfeDetailAdjusted> detailList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM CostAfeDetailAdjusted " +
											"WHERE (isDeleted=false or isDeleted is null) and costAfeMasterAdjustedUid=:sourceUid", "sourceUid", afe.getCostAfeMasterAdjustedUid(), qp);
									for (CostAfeDetailAdjusted detail : detailList) {
										CostAfeDetail newDetail = new CostAfeDetail();
										PropertyUtils.copyProperties(newDetail, detail);
										newDetail.setCostAfeDetailUid(null);
										newDetail.setCostAfeMasterUid(masterAfe.getCostAfeMasterUid());
										newDetail.setWellUid(null);
										newDetail.setWellboreUid(null);
										newDetail.setOperationUid(null);
										ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newDetail, qp);
									}
								}
							}
							 
						}
					}
				}else if ("vault".equalsIgnoreCase(source)) {
					List<Object[]> opm = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("" +
							" FROM OperationPlanMasterVault p, CostAfeMasterVault c " +
							" where p.operationPlanMasterVaultUid=c.operationPlanMasterVaultUid " +
							" AND c.costAfeMasterVaultUid=:masterUid " +
							" and (p.isDeleted = false OR p.isDeleted is null)" +
							" and (c.isDeleted = false OR c.isDeleted is null)", "masterUid", sourceUid, qp);
					
					if (opm.size()>0) {
						Object[] rec = opm.get(0) ;
						OperationPlanMasterVault operationPlanMasterVault = (OperationPlanMasterVault) rec[0] ;
						
						OperationPlanMaster newDvdPlan = new OperationPlanMaster();

						PropertyUtils.copyProperties(newDvdPlan, operationPlanMasterVault);
						newDvdPlan.setOperationPlanMasterUid(null);
						newDvdPlan.setWellUid(null);
						newDvdPlan.setWellboreUid(null);
						newDvdPlan.setOperationUid(null);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newDvdPlan ,qp);
						
						
						List<OperationPlanPhaseVault> opp = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("" +
								"FROM OperationPlanPhaseVault " +
								"where operationPlanMasterVaultUid=:masterUid " +
								"and (isDeleted = false OR isDeleted is null)", "masterUid", operationPlanMasterVault.getOperationPlanMasterVaultUid() ,qp);
						if (opp.size()>0) {
							
							for (OperationPlanPhaseVault operationPlanPhaseVault : opp){
						
								OperationPlanPhase newDvdPlanPhase = new OperationPlanPhase();

								PropertyUtils.copyProperties(newDvdPlanPhase, operationPlanPhaseVault);
								newDvdPlanPhase.setOperationPlanPhaseUid(null);
								newDvdPlanPhase.setOperationPlanMasterUid(newDvdPlan.getOperationPlanMasterUid());
								newDvdPlanPhase.setWellUid(null);
								newDvdPlanPhase.setWellboreUid(null);
								newDvdPlanPhase.setOperationUid(null);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newDvdPlanPhase, qp);
								
							}
						}
						
						if (BooleanUtils.toBoolean(carryAfe)) {
							String queryString = "FROM CostAfeMasterVault WHERE (isDeleted=false or isDeleted is null) AND costAfeMasterVaultUid=:sourceUid";
							List<CostAfeMasterVault> master = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "sourceUid", sourceUid, qp);
							if (master.size()>0) {
								CostAfeMasterVault vaultAfe = master.get(0);
								CostAfeMaster masterAfe = new CostAfeMaster();
								PropertyUtils.copyProperties(masterAfe, vaultAfe);
								masterAfe.setCostAfeMasterUid(null);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(masterAfe ,qp);
								
								OperationAfe operationAfe = new OperationAfe();
								operationAfe.setAfeType("MASTER");
								operationAfe.setCostAfeMasterUid(masterAfe.getCostAfeMasterUid());
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(operationAfe);
								
								List<CostAfeDetailVault> detailList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM CostAfeDetailVault " +
										"WHERE (isDeleted=false or isDeleted is null) and costAfeMasterVaultUid=:sourceUid", "sourceUid", sourceUid, qp);
								for (CostAfeDetailVault detail : detailList) {
									CostAfeDetail newDetail = new CostAfeDetail();
									PropertyUtils.copyProperties(newDetail, detail);
									newDetail.setCostAfeDetailUid(null);
									newDetail.setCostAfeMasterUid(masterAfe.getCostAfeMasterUid());
									newDetail.setWellUid(null);
									newDetail.setWellboreUid(null);
									newDetail.setOperationUid(null);
									ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newDetail, qp);
								}
							}
						}
					}
				}
			}
			
			SimpleXmlWriter writer = new SimpleXmlWriter(response); 
			writer.startElement("root");
			
			List<Object[]> actualList = ApplicationUtils.getConfiguredInstance().getDaoManager().find("select p.operationPlanMasterUid, " +
					"p.dvdPlanName from OperationPlanMaster p, Operation o " +
					"where (p.isDeleted = false OR p.isDeleted is null) " +
					"and (o.isDeleted = false OR o.isDeleted is null) " +
					"and o.operationUid=p.operationUid " +
					"order by p.dvdPlanName", qp);
			
			writer.startElement("ActualDvdPlanList");
			writer.addElement("code", "");
			writer.addElement("label", "");
			writer.endElement();
			
			for (Object[] rec : actualList) {
				if (StringUtils.isNotBlank((String)rec[1])) {
					writer.startElement("ActualDvdPlanList");
					writer.addElement("code", (String) rec[0]);
					writer.addElement("label", (String) rec[1]);
					writer.endElement();
				}
			}

			List<Object[]> vaultList = ApplicationUtils.getConfiguredInstance().getDaoManager().find("" +
					"SELECT m.costAfeMasterVaultUid, m.afeNumber, p.dvdPlanName, SUM(coalesce(c.itemCost,0.0)*coalesce(c.quantity,0.0)*coalesce(c.estimatedDays,0.0)) " +
					"FROM OperationPlanMasterVault p, CostAfeMasterVault m, CostAfeDetailVault c " +
					"WHERE (p.isDeleted = false OR p.isDeleted is null) " +
					"AND (m.isDeleted=false or m.isDeleted is null) " +
					"AND (c.isDeleted=false or c.isDeleted is null) " +
					"AND m.operationPlanMasterVaultUid=p.operationPlanMasterVaultUid " +
					"AND m.costAfeMasterVaultUid=c.costAfeMasterVaultUid " +
					"AND c.phaseCode not in ('PA','CS') " +
					"GROUP BY m.costAfeMasterVaultUid, m.afeNumber, p.dvdPlanName " +
					"order by p.dvdPlanName, m.afeNumber", qp);
			
			
			writer.startElement("VaultDvdPlanList");
			writer.addElement("code", "");
			writer.addElement("label", "");
			writer.endElement();
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean.getUserLocale(), CostAfeDetailVault.class, "item_total");
			
			for (Object[] rec : vaultList) {
				Double totalCost = (Double) rec[3];
				if (totalCost==null) continue;
				if (totalCost>0.0) {
					String label = rec[2] + " (" + rec[1] + ") " + thisConverter.getFormattedValue(totalCost, false); 
					writer.startElement("VaultDvdPlanList");
					writer.addElement("code", (String) rec[0]);
					writer.addElement("label", label);
					writer.endElement();
				}
			}
			
			writer.endElement();
			writer.close();
		}
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
			
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
			if (request!=null){
				if(StringUtils.isNotBlank(request.getParameter("target_class"))){
					int count = 0;
					if ("OperationPlanPhaseVault".equals(request.getParameter("target_class"))) {
					
						if (targetCommandBeanTreeNode !=null) {
					
							if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(OperationPlanMasterVault.class)){
								
								String masterUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "operationPlanMasterVaultUid");
								count = this.getLastSequence("OperationPlanPhaseVault", "operationPlanMasterVaultUid", masterUid);
								Double[] lastDepth = this.getLastDepth("OperationPlanPhaseVault", "operationPlanMasterVaultUid", masterUid);
								
								UserSession session = UserSession.getInstance(request);
								String gwpvalue = GroupWidePreference.getValue(session.getCurrentGroupUid(), "defaultPhaseCodesForDVDPlan");
								
								for (String item:gwpvalue.toString().split(";")) {
									String phase = item.toString();
									
									if (StringUtils.isNotBlank(phase)) {
										count++;
										OperationPlanPhaseVault newPlanPhase = new OperationPlanPhaseVault();
										newPlanPhase.setPhaseCode(phase);
										newPlanPhase.setSequence(count);
										newPlanPhase.setDepthMdMsl(lastDepth[0]);
										newPlanPhase.setDepthTvdMsl(lastDepth[1]);
										targetCommandBeanTreeNode.addCustomNewChildNodeForInput(newPlanPhase);
									}
								}
								commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
							}
							
						}

					}else if ("OperationPlanPhase".equals(request.getParameter("target_class"))) {
						
						if (targetCommandBeanTreeNode !=null) {
							
							if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(OperationPlanMaster.class)){
								
								String masterUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "operationPlanMasterUid");
								count = this.getLastSequence("OperationPlanPhase", "operationPlanMasterUid", masterUid);
								Double[] lastDepth = this.getLastDepth("OperationPlanPhase", "operationPlanMasterUid", masterUid);								
								UserSession session = UserSession.getInstance(request);
								String gwpvalue = GroupWidePreference.getValue(session.getCurrentGroupUid(), "defaultPhaseCodesForDVDPlan");
								
								for (String item:gwpvalue.toString().split(";")) {
									String phase = item.toString();
									
									if (StringUtils.isNotBlank(phase)) {
										count++;
										OperationPlanPhase newPlanPhase = new OperationPlanPhase();
										newPlanPhase.setPhaseCode(phase);
										newPlanPhase.setSequence(count);
										newPlanPhase.setDepthMdMsl(lastDepth[0]);
										newPlanPhase.setDepthTvdMsl(lastDepth[1]);
										targetCommandBeanTreeNode.addCustomNewChildNodeForInput(newPlanPhase);
									}
								}
								commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
							}
							
						}
					}
				}
			}
		}
	}
	
	private int getLastSequence(String table, String parentUid, String masterUid) throws Exception {
		String strSql = "SELECT Max(sequence) FROM " + table + " where (isDeleted = false OR isDeleted is null) and " + parentUid + "=:masterUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "masterUid", masterUid);

		if (lstResult.size() > 0){
			Object a = lstResult.get(0);
			
			if (a !=null){
				int maxSeq = Integer.parseInt(a.toString());
				return maxSeq;
			}
		}

		return 0;
	}
	
	private Double[] getLastDepth(String table, String parentUid, String masterUid) throws Exception {
		Double[] values = new Double[2];
		
		values[0] = null;
		values[1] = null;
		
		String strSql = "SELECT Max(depthMdMsl), Max(depthTvdMsl) FROM " + table + " where (isDeleted = false OR isDeleted is null) and " + parentUid + "=:masterUid";
		List <Double[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "masterUid", masterUid);

		if (lstResult.size() > 0){
			Object[] rec = lstResult.get(0);
			
			if (rec[0] !=null){
				values[0] = Double.parseDouble(rec[0].toString());
			}
			
			if (rec[1] !=null){
				values[1] = Double.parseDouble(rec[1].toString());
			}
		}

		return values;
	}

	public boolean allowedAction(CommandBeanTreeNode node, UserSession session,
			String targetClass, String action, HttpServletRequest request)
			throws Exception {
		if ("OperationPlanMaster".equals(targetClass)) {
			if ("add".equals(action) || "copySelected".equals(action) || "pasteAsNew".equals(action)) {
				String queryString = "FROM OperationPlanMaster WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid";
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", session.getCurrentOperationUid());
				if (list.size()>0) {
					return false;
				}
				return true;
			}
		}
		return true;
	}

	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		if (node.getData() instanceof OperationPlanPhaseVault) {
			OperationPlanPhaseVault rec = (OperationPlanPhaseVault) node.getData();
			if (rec.getP50Duration()!=null){
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, OperationPlanPhaseVault.class, "p50Duration");
				thisConverter.setBaseValueFromUserValue(rec.getP50Duration());
				
				Double p50Duration = thisConverter.getBasevalue();
				String queryString = "FROM CostAfeMasterVault m, CostAfeDetailVault d " +
						"WHERE m.costAfeMasterVaultUid = d.costAfeMasterVaultUid " +
						"AND (m.isDeleted=false or m.isDeleted is null) " +
						"AND (d.isDeleted=false or d.isDeleted is null) " +
						"AND m.operationPlanMasterVaultUid=:operationPlanMasterVaultUid " +
						"AND d.phaseCode=:phaseCode AND d.recurringItem=1";
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"operationPlanMasterVaultUid", "phaseCode"}, new Object[]{rec.getOperationPlanMasterVaultUid(), rec.getPhaseCode()});
				for (Object[] afeLink : list) {	
					CostAfeDetailVault detail =(CostAfeDetailVault) afeLink[1];
					detail.setEstimatedDays(p50Duration/86400.0);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(detail);
				}
			}
		}else if (node.getData() instanceof OperationPlanPhase) {
			OperationPlanPhase thisPlanPhase = (OperationPlanPhase) node.getData();
			if (thisPlanPhase.getP50Duration()!=null) {
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, OperationPlanPhase.class, "p50Duration");
				thisConverter.setBaseValueFromUserValue(thisPlanPhase.getP50Duration());
				
				Double p50Duration = thisConverter.getBasevalue();
				String queryString = "FROM CostAfeMaster m, CostAfeDetail d " +
				"WHERE m.costAfeMasterUid = d.costAfeMasterUid " +
				"AND (m.isDeleted=false or m.isDeleted is null) " +
				"AND (d.isDeleted=false or d.isDeleted is null) " +
				"AND m.operationUid=:operationUid " +
				"AND d.phaseCode=:phaseCode AND d.recurringItem=1";
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"operationUid", "phaseCode"}, new Object[]{session.getCurrentOperationUid(), thisPlanPhase.getPhaseCode()});
				for (Object[] afeLink : list) {	
					CostAfeDetail detail =(CostAfeDetail) afeLink[1];
					detail.setEstimatedDays(p50Duration/86400.0);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(detail);
				}
				String adjustedQueryString = "FROM CostAfeMasterAdjusted m, CostAfeDetailAdjusted d " +
				"WHERE m.costAfeMasterAdjustedUid = d.costAfeMasterAdjustedUid " +
				"AND (m.isDeleted=false or m.isDeleted is null) " +
				"AND (d.isDeleted=false or d.isDeleted is null) " +
				"AND m.operationUid=:operationUid " +
				"AND d.phaseCode=:phaseCode AND d.recurringItem=1";
				List<Object[]> adjustedList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(adjustedQueryString, new String[]{"operationUid", "phaseCode"}, new Object[]{session.getCurrentOperationUid(), thisPlanPhase.getPhaseCode()});
				for (Object[] afeLink : adjustedList) {	
					CostAfeDetailAdjusted adjustedDetail =(CostAfeDetailAdjusted) afeLink[1];
					adjustedDetail.setEstimatedDays(p50Duration/86400.0);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(adjustedDetail);
				}
			}
		} else if (node.getData() instanceof OperationPlanMasterVault) {
			commandBean.getFlexClientControl().setReloadParentHtmlPage(false);
		}
	}

}
