package com.idsdatanet.d2.drillnet.report;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellHistoryReportModule extends DefaultReportModule{
	
	private Map<String,Integer> optionLimit;
	
	public void init(CommandBean commandBean){
		super.init(commandBean);
		if(commandBean instanceof BaseCommandBean){
			
			try {
				//commandBean.getRoot().getDynaAttr().put(WELL_FIELD, null);
				//commandBean.getRoot().getDynaAttr().put(FIELD_FIELD, null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
		
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		commandBean.getSystemMessage().clear();
		UserSelectionSnapshot userSelection=new UserSelectionSnapshot(session);
		
		Object screenOutput = commandBean.getRoot().getDynaAttr().get("ScreenOutput");
		Object OperationFilter = commandBean.getRoot().getDynaAttr().get("OperationFilter");
		Object operationCampaignUid = commandBean.getRoot().getDynaAttr().get("operationCampaignUid");
		Object wellUid = commandBean.getRoot().getDynaAttr().get("wellUid");
		Object operationCodeFilter = commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
		
		String selectedUid = "";
		String selectedScreenOputput = "";
		String selectedOperationFilter = "";
		String selectedOperationCode = "";
		
		if (screenOutput != null){
			selectedScreenOputput = screenOutput.toString();
			userSelection.setCustomProperty("cp_ScreenOutput", selectedScreenOputput);
		}
		
		if (StringUtils.isBlank(selectedScreenOputput)){
			commandBean.getSystemMessage().addError("Please select a view filter", true);
		}
		
		if (OperationFilter != null){
			selectedOperationFilter = OperationFilter.toString();
			userSelection.setCustomProperty("cp_OperationFilter", selectedOperationFilter);
		}
		
		if (selectedOperationFilter.equalsIgnoreCase("well")){
			if (wellUid != null){
				if (StringUtils.isNotBlank(wellUid.toString())){
					selectedUid = wellUid.toString();
					userSelection.setCustomProperty("cp_wellUid", wellUid.toString());
				}						
			}		
			if (StringUtils.isBlank(selectedUid)){
				commandBean.getSystemMessage().addError("Please select a well filter", true);	
			}
		}else if (selectedOperationFilter.equalsIgnoreCase("campaign")){
			if (operationCampaignUid != null){
				if (StringUtils.isNotBlank(operationCampaignUid.toString())){
					selectedUid = operationCampaignUid.toString();
					userSelection.setCustomProperty("cp_operationCampaignUid", operationCampaignUid.toString());
				}			
			}	
			if (StringUtils.isBlank(selectedUid)){
				commandBean.getSystemMessage().addError("Please select a campaign filter", true);	
			}
		}else{
			commandBean.getSystemMessage().addError("Please select either campaign or well filter", true);
		}
		
		if (operationCodeFilter != null){
			selectedOperationCode = operationCodeFilter.toString();
			userSelection.setCustomProperty("cp_operationCodeFilter", selectedOperationCode);
		}
			
		if (commandBean.getSystemMessage().getTotalErrors()==0 ){
			return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));				
		}		
		
		return null;
	}
		
	
	public void setOptionLimit(Map<String,Integer> optionLimit) {
		this.optionLimit = optionLimit;
	}

	public Map<String,Integer> getOptionLimit() {
		return optionLimit;
	}	
}
