package com.idsdatanet.d2.drillnet.rigstock;

import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class CombinedRigStockActionManager implements ActionManager {

	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if(com.idsdatanet.d2.core.web.mvc.Action.SAVE.equals(action)){
			
			Object object = node.getData();
			
			if (object instanceof FluidStock || object instanceof SupplementaryStock || object instanceof MudStock) {
				return new RigStockUtil.SaveStockActionHandler();
			}
			return null;
		}else{
			return null;
		}
	}

	
	
}