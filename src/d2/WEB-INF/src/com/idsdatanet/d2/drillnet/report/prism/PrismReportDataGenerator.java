package com.idsdatanet.d2.drillnet.report.prism;


import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class PrismReportDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
		String currentDailyUid = userContext.getUserSelection().getDailyUid();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		String reportTypeValue = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(currentOperationUid);
		//Retrieve the report file data
		List <ReportFiles> lstReportFiles = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM ReportFiles WHERE (isDeleted = false or isDeleted is null) and dailyUid = '" + currentDailyUid + "' and reportOperationUid = '" + currentOperationUid + "' and reportType='" + reportTypeValue + "'");
		if(lstReportFiles.size() > 0){
			
			for(ReportFiles objReportFiles: lstReportFiles){
				
				    String displayName="";
				    displayName=objReportFiles.getDisplayName();
				    ReportDataNode ReportFileNode = reportDataNode.addChild("ReportFiles");
				    ReportFileNode.addProperty("displayName", displayName);
					
			}
		}
		
			
	}
}
