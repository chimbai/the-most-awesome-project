package com.idsdatanet.d2.drillnet.bitrun;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class BitrunDataLoaderInterceptor implements DataLoaderInterceptor {
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private static final String CUSTOM_FROM_MARKER = "{_custom_from_}";

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
	
		String currentClass		= meta.getTableClass().getSimpleName();
		String customCondition 	= "";

		if(currentClass.equals("Bitrun")) {
			if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_STT) {
				// do not filter by isDeleted flag here, deleted data always need to be loaded for Send To Town
				customCondition = "bitrun.groupUid = session.groupUid and bitrun.operationUid = session.operationUid and bitrun.bharunUid = dailySummary.bharunUid and dailySummary.dailyUid = session.dailyUid";
			} else {
				//remove the filter condition for group_uid as parent/global site not able to see the child bharun data
				customCondition = "(bitrun.isDeleted = false or bitrun.isDeleted is null) and (dailySummary.isDeleted = false or dailySummary.isDeleted is null) and bitrun.operationUid = session.operationUid and bitrun.bharunUid = dailySummary.bharunUid and dailySummary.dailyUid = session.dailyUid";
			}
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		return null;
	
	}

	public String generateHQLFromClause(String fromClause,CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		if(StringUtils.isNotBlank(fromClause) && fromClause.indexOf(CUSTOM_FROM_MARKER) < 0) return null;
		
		String currentClass	= meta.getTableClass().getSimpleName();
		String customFrom = "";
		
		if (currentClass.equals("Bitrun")) {
			customFrom = "Bitrun bitrun, BharunDailySummary dailySummary";
			return fromClause.replace(CUSTOM_FROM_MARKER, customFrom);
		}
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

}
