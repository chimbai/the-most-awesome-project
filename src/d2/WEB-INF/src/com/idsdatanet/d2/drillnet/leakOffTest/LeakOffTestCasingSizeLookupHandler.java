package com.idsdatanet.d2.drillnet.leakOffTest;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LeakOffTestCasingSizeLookupHandler implements LookupHandler {
	private URI xmlLookup = null;
	private Boolean retrieveCasingRecordByWellboreUid = false;
	private String sortDirection = "asc";
	
	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}

	public void setXmlLookup(URI xmlLookup) {
		this.xmlLookup = xmlLookup;
	}
	
	public URI getXmlLookup() {
		return this.xmlLookup;
	}
	
	public void setRetrieveCasingRecordByWellboreUid(Boolean value) {
		this.retrieveCasingRecordByWellboreUid = value;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		Map<String, LookupItem> casingLookup = LookupManager.getConfiguredInstance().getLookup(this.getXmlLookup().toString(), userSelection, null);
		List<CasingSection> list = null;
		if(this.retrieveCasingRecordByWellboreUid){
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid and casingOd > 0 order by casingOd " + sortDirection, "wellboreUid", userSelection.getWellboreUid());
		}
		else
		{
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and casingOd > 0 order by casingOd " + sortDirection, "operationUid", userSelection.getOperationUid());
		}
		
		if((list != null && list.size() > 0) && (casingLookup != null)) {
			for(CasingSection casingSection : list) {
				CustomFieldUom thisConverter = new CustomFieldUom((Locale) null, CasingSection.class, "casingOd");
				String strCasingOd 			 = thisConverter.formatOutputPrecision(casingSection.getCasingOd());
				
				if(StringUtils.isNotBlank(strCasingOd) && casingLookup.containsKey(strCasingOd)) {
					LookupItem lookupItem = casingLookup.get(strCasingOd);
					if(lookupItem != null)
					{
						result.put(strCasingOd, new LookupItem(strCasingOd, lookupItem.getValue(), lookupItem.getKey()));
					}
				}
			}
		}
		
		return result;
	}
}