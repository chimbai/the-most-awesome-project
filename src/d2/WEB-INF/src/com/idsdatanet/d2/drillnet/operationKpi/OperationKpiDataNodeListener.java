
//** changes made on this file should also be applied to well operation if needed.

package com.idsdatanet.d2.drillnet.operationKpi;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.OperationKpi;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.holeSection.KpiUtils;

public class OperationKpiDataNodeListener extends EmptyDataNodeListener {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private Boolean defaultUOMTemplate = false;
	private String operationKpiUnit = "operation_kpi.uom"; 
	
	public void setOperationKpiUnit(String value){
		this.operationKpiUnit = value;
	}
	
	public void setDefaultUOMTemplate(Boolean value) {
		this.defaultUOMTemplate = value;
	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof OperationKpi) {
			KpiUtils.handleConversion(commandBean, object, operationKpiUnit);
		}
	}

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
}