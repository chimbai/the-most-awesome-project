package com.idsdatanet.d2.drillnet.partnerportal;

import org.apache.commons.lang.StringUtils;

public class PublicOtherReportList extends BasePublicReportList {
	
	public String getReportDisplayName(){
		if(StringUtils.isNotBlank(this.thisReportFiles.getDisplayName())) return this.thisReportFiles.getDisplayName();
		return this.reportType + " " + this.operationName + "_" + this.thisReportFiles.getReportDayNumber() + ".pdf";
	}

}
