package com.idsdatanet.d2.drillnet.user;

import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.multiselect.StandardTreeNodeMultiSelect;

public class AclGroupMultiSelect extends StandardTreeNodeMultiSelect {
	
	/**
	 * Load multi-select field's values for the specified data node
	 */
	@SuppressWarnings("unchecked")
	public List<String> loadMultiSelectKeys(CommandBeanTreeNode node) throws Exception {
		String parent_id = BeanUtils.getProperty(node.getData(), this.parentIdField);
		return this.daoManager.findByNamedParam("select " + this.childLookupField + " from " + this.childTableClass.getName() + " where (isDeleted = false or isDeleted is null) and " + this.childIdField + " = :parent_id and " + this.childLookupField + " <> :hideAclGroupUid", new String[] {"parent_id", "hideAclGroupUid"}, new Object[] {parent_id, StringUtils.defaultString((String) node.getDynaAttr().get("hideAclGroupUid"))});
	}

}
