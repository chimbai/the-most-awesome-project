package com.idsdatanet.d2.drillnet.surveyreference;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ibm.icu.text.SimpleDateFormat;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class SurveyStationDayLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot selectionSnapshot, HttpServletRequest request, LookupCache cache) throws Exception {
		Map<String, LookupItem> result = cache.getFromCache(this.getClass().getName());
		if(result != null) return result;
		
		//find all operations for the selected wellbore, then find the day associated with each operation. Set as active if it is the current session's operation
		
		ApplicationUtils appUtils = ApplicationUtils.getConfiguredInstance();
		result = new LinkedHashMap<String, LookupItem>();
		String sql = "SELECT distinct operationUid FROM Operation WHERE (isDeleted = false or isDeleted is null) AND wellboreUid =:wellboreUid";
		List<String> operationResults = appUtils.getDaoManager().findByNamedParam(sql, "wellboreUid",selectionSnapshot.getWellboreUid());
		
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
		
		if (operationResults.size() > 0) {
			for (String operationResult : operationResults) {
				if (operationResult != null) {
					List<Daily> all_days = appUtils.getDaoManager().findByNamedParam("from Daily where (isDeleted = false or isDeleted is null) and operationUid = :operationUid order by dayDate", "operationUid", operationResult);
					for (Daily daily : all_days) {
						List<String> rptDailyDayNumber = appUtils.getDaoManager().findByNamedParam("select reportNumber from ReportDaily where (isDeleted = false or isDeleted is null) and reportType!='DGR' and dailyUid = :thisDailyUid", "thisDailyUid", daily.getDailyUid());
						if(!rptDailyDayNumber.isEmpty())
						{
							LookupItem lookupItem = new LookupItem(daily.getDailyUid(), "#" + rptDailyDayNumber.get(0) + " (" + df.format(daily.getDayDate()) + ")");
							if (daily.getOperationUid() != null && !daily.getOperationUid().equals(selectionSnapshot.getOperationUid())){
								lookupItem.setActive(Boolean.FALSE);
							}
							result.put(daily.getDailyUid(), lookupItem);
						}
					}
				}
			}
		}
		
		cache.saveToCache(this.getClass().getName(), result);
		return result;
	}
}
