package com.idsdatanet.d2.drillnet.activity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.idsdatanet.d2.infra.uom.OperationUomAndDatumSelector;
import com.idsdatanet.d2.infra.uom.OperationUomContext;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.activity.npt.BHIQueryUtils;
import com.idsdatanet.d2.drillnet.activity.npt.NPTReportDataGenerator;
import com.idsdatanet.d2.drillnet.activity.npt.NPTSummary;
import com.idsdatanet.d2.drillnet.report.ReportOption;

public class OnBottomIADCDrillingTimeReportDataGenerator extends NPTReportDataGenerator{

	private List<String> taskCode;
	private List<String> phaseCode;
	
	
	protected void generateActualData(UserContext userContext, Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{			
		List<NPTSummary> includedList = this.shouldIncludeWell(userContext, startDate, endDate);
		this.includeQueryData(includedList, reportDataNode, userContext.getUserSelection(), startDate, endDate);
		this.populateDistinctHoleByField(reportDataNode, userContext.getUserSelection(), startDate, endDate, userContext);
	}
	
	private List<NPTSummary> generatedata(UserContext userContext, Date startDate, Date endDate, String includeWellUid) throws Exception
	{
		OperationUomContext operationUomContext = new OperationUomContext(true, true);


		String[] paramNames = {"startDate","endDate","wellUid"};
		Object[] values = {startDate,endDate,includeWellUid};
		List<String> listParams = BHIQueryUtils.convertArrayToList(paramNames);
		List<Object> listValues = BHIQueryUtils.convertArrayToList(values);

		if (this.getTaskCode()!=null && this.getTaskCode().size()>0)
		{
			listParams.add("taskCode");
			listValues.add(this.getTaskCode());
		}
		if (this.getPhaseCode()!=null && this.getPhaseCode().size()>0)
		{
			listParams.add("phaseCode");
			listValues.add(this.getPhaseCode());

		}
		
		
		String sql = "SELECT o.operationUid, w.wellUid,w.wellName, o.operationName, w.field, w.country, a.activityDuration, a.startDatetime, a.depthMdMsl, a.sections, a.phaseCode, wb.wellboreUid, wb.wellboreName, a.taskCode, a.activityUid"+			
			" FROM Well w,Wellbore wb, Operation o, Activity a, Daily d "+ 
			" Where w.wellUid=wb.wellUid and wb.wellboreUid=o.wellboreUid"+
			" and d.operationUid=o.operationUid"+ 
			" and a.dailyUid=d.dailyUid"+
			" and (a.isDeleted is null or a.isDeleted = False)"+
			" and (d.isDeleted is null or d.isDeleted = False)"+
			" and (w.isDeleted is null or w.isDeleted = false) and (wb.isDeleted is null or wb.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false)"+
			" and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) "+
			((this.getPhaseCode()!=null && this.getPhaseCode().size()>0)?" and (a.phaseCode IN (:phaseCode))":"")+
			((this.getTaskCode()!=null && this.getTaskCode().size()>0)?" and (a.taskCode IN (:taskCode))":"")+
			" and (a.startDatetime>=:startDate and a.endDatetime<=:endDate)"+ 						
			" and w.wellUid=:wellUid"+			
			" order by w.field, w.wellName, wb.wellboreUid, d.dayDate, a.startDatetime, a.endDatetime";
		
		List<NPTSummary> includedList = new ArrayList<NPTSummary>();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, listParams, listValues, qp);
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
		Double cumDuration =0.00;
		String parentWellboreUid=null;
		for (Object[] item :result)
		{
			String operationUid = (String) item[0];
			String wellUid = (String) item[1];
			String wellname = (String) item[2];
			String operationName = (String) item[3];
			String field = (String) item[4];
			String country = (String) item[5];
			Double activityDuration = (Double) item[6];
			Date startDatetime = (Date) item[7];			
			Double depthMdMsl = (Double) item[8];
			String sections = (String) item[9];
			String phaseCode = (String) item[10]; 
			String wellboreUid = (String) item[11]; 
			String wellboreName = (String) item[12]; 
			String taskCode = (String) item[13]; 
			String activityUid=(String)item[14];
			
			operationUomContext.setOperationUid(operationUid);
			if (depthMdMsl!= null && activityDuration != null){
				NPTSummary npt = new NPTSummary(wellUid, null, country, null, null, activityDuration, null, null);			
				npt.setWellName(wellname);
				npt.setOperationName(operationName);		
				npt.setField(field);
				npt.setCountry(country);
				npt.setSections(sections);
				npt.setTaskCode(taskCode);
				npt.setPhaseCode(phaseCode);
				npt.setWellboreUid(wellboreUid);
				npt.setWellboreName(wellboreName);
				npt.setStartDatetime(startDatetime);	
				npt.setActivityUid(activityUid);

				if (((parentWellboreUid!=null && !parentWellboreUid.equalsIgnoreCase(wellboreUid)) || parentWellboreUid==null))
				{
					cumDuration = 0.0;
					NPTSummary nptNew = new NPTSummary(wellUid, null, country, null, null, activityDuration, null, null);			
					nptNew.setWellName(wellname);
					nptNew.setOperationName(operationName);		
					nptNew.setField(field);
					nptNew.setCountry(country);
					nptNew.setSections(sections);
					nptNew.setTaskCode(taskCode);
					nptNew.setPhaseCode(phaseCode);
					nptNew.setWellboreUid(wellboreUid);
					nptNew.setWellboreName(wellboreName);
					nptNew.setStartDatetime(startDatetime);		
					nptNew.setDuration(cumDuration);
					CustomFieldUom depthConverter = new CustomFieldUom((Locale)null, Activity.class, "depthMdMsl", operationUomContext);
					nptNew.setDepthMdMslUom(depthConverter.getUomSymbol());
					depthConverter.setBaseValue(depthMdMsl);
					nptNew.setDepthMdMsl(depthConverter.getConvertedValue());
					includedList.add(nptNew);
				}
				
				thisConverter.setBaseValue(activityDuration);
				cumDuration += thisConverter.getConvertedValue();	
				npt.setDuration(cumDuration);
				
				CustomFieldUom depthConverter = new CustomFieldUom((Locale)null, Activity.class, "depthMdMsl", operationUomContext);
				npt.setDepthMdMslUom(depthConverter.getUomSymbol());
				depthConverter.setBaseValue(depthMdMsl);
				npt.setDepthMdMsl(depthConverter.getConvertedValue());
				

				includedList.add(npt);
				parentWellboreUid=wellboreUid;

			}			
		}
		return includedList;
		
	}
	
	private List<NPTSummary> shouldIncludeWell(UserContext userContext, Date startDate, Date endDate) throws Exception
	{
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		ReportOption wells = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL):null;
		List<NPTSummary> result = new ArrayList<NPTSummary>();
		for (String wellUid : wells.getValues())
		{
			result.addAll(this.generatedata(userContext, startDate, endDate,wellUid));
		}
		return result;
	}

	private void includeQueryData(List<NPTSummary> nptSummary, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ReportDataNode nodeChild = node.addChild("OnBottomIADCDrillingTime");		
				
		for (NPTSummary npt : nptSummary)
		{					
			ReportDataNode catChild = nodeChild.addChild("data");
			catChild.addProperty("welluid", npt.getWellUid());
			catChild.addProperty("wellname", npt.getWellName());
			catChild.addProperty("operationname", npt.getOperationName());
			catChild.addProperty("field",npt.getField());			
			catChild.addProperty("startdatetime",dateFormat.format(npt.getStartDatetime()));
			catChild.addProperty("startdatetime_epoch",String.valueOf(npt.getStartDatetime().getTime()));
			catChild.addProperty("depthMdMsl",String.valueOf(npt.getDepthMdMsl()));
			catChild.addProperty("depthMdMslUom",npt.getDepthMdMslUom());						
			catChild.addProperty("cumDuration", String.valueOf(npt.getDuration()));	
			catChild.addProperty("sections", npt.getSections());
			catChild.addProperty("taskCode", npt.getTaskCode());
			catChild.addProperty("taskCode", npt.getTaskCode());
			catChild.addProperty("wellboreUid", npt.getWellboreUid());
			catChild.addProperty("wellboreName", npt.getWellboreName());
			catChild.addProperty("activityUid", npt.getActivityUid());
		}		
		
	}
	
	private void populateDistinctHoleByField(ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end, UserContext userContext) throws Exception
	{
		//to populate Distinct HOLE SECTIONS, BY FIELD
		ReportOption fields = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_FIELD)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_FIELD):null;	
		ReportDataNode nodeChild2 = node.addChild("Field_HoleSections");	
		
		for (String fieldName : fields.getValues()){
			
			String[] paramNames = {"startDate","endDate","field","wellUid"};
			Object[] values = {start,end,fieldName,BHIQueryUtils.getReportOptionValue(userContext,BHIQueryUtils.CONSTANT_WELL)};
			List<String> listParams = BHIQueryUtils.convertArrayToList(paramNames);
			List<Object> listValues = BHIQueryUtils.convertArrayToList(values);

			if (this.getTaskCode()!=null && this.getTaskCode().size()>0)
			{
				listParams.add("taskCode");
				listValues.add(this.getTaskCode());
			}
			if (this.getPhaseCode()!=null && this.getPhaseCode().size()>0)
			{
				listParams.add("phaseCode");
				listValues.add(this.getPhaseCode());

			}
			
			
			String sql = "SELECT DISTINCT a.sections"+			
				" FROM Well w,Wellbore wb, Operation o, Activity a, Daily d "+ 
				" Where w.wellUid=wb.wellUid and wb.wellboreUid=o.wellboreUid"+
				" and d.operationUid=o.operationUid"+ 
				" and a.dailyUid=d.dailyUid"+
				" and (a.isDeleted is null or a.isDeleted = False)"+
				" and (d.isDeleted is null or d.isDeleted = False)"+
				" and (w.isDeleted is null or w.isDeleted = false) and (wb.isDeleted is null or wb.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false)"+
				" and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) "+
				((this.getPhaseCode()!=null && this.getPhaseCode().size()>0)?" and (a.phaseCode IN (:phaseCode))":"")+
				((this.getTaskCode()!=null && this.getTaskCode().size()>0)?" and (a.taskCode IN (:taskCode))":"")+
				" and (a.startDatetime>=:startDate and a.endDatetime<=:endDate)"+ 						
				" and w.wellUid in (:wellUid)"+
				" and (w.field =:field)"+
				" order by w.field, w.wellName,d.dayDate, a.startDatetime, a.endDatetime";
		
			List<String> query = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, listParams , listValues);
			String sections = BHIQueryUtils.concat(query, ",", true);
			
			//Create another report data node for populated DISTINCT Field , Hole Sections			
			ReportDataNode catChild2 = nodeChild2.addChild("data");
			catChild2.addProperty("Field", (String) fieldName);
			catChild2.addProperty("sections", (String) sections);			
		}	
	}

	public void setTaskCode(List<String> taskCode) {
		this.taskCode = taskCode;
	}

	public List<String> getTaskCode() {
		return taskCode;
	}

	public void setPhaseCode(List<String> phaseCode) {
		this.phaseCode = phaseCode;
	}

	public List<String> getPhaseCode() {
		return phaseCode;
	}

	

	
}
