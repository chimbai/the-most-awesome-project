package com.idsdatanet.d2.drillnet.operationSummary;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.lookup.*;

public class MonthYearLookupHandler implements LookupHandler {
	private String monthYearOrderBy = null;
	
	public void setMonthYearOrderBy(String value){
		this.monthYearOrderBy = value;
	}
	
	private String getOrderBy(){
		if(StringUtils.isBlank(this.monthYearOrderBy)) 
			return "DATE_FORMAT(reportDatetime,'%Y') ASC, DATE_FORMAT(reportDatetime,'%m') ASC";
		return this.monthYearOrderBy;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String sql = "select distinct(DATE_FORMAT(reportDatetime,'%m-%Y')), DATE_FORMAT(reportDatetime,'%b %Y') from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid = :operationUid order by " + this.getOrderBy();
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", userSelection.getOperationUid());
		for (Object obj[] : list) {
			LookupItem item = new LookupItem(obj[0].toString(), obj[1]);
			result.put(obj[0].toString(), item);
		}
		
		return result;
	}

}
