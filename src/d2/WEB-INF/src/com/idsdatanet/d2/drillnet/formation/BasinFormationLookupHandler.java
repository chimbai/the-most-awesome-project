package com.idsdatanet.d2.drillnet.formation;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.BasinFormation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.lookup.*;

public class BasinFormationLookupHandler implements LookupHandler {
	private String basinFormationOrderBy = null;
	
	public void setBasinFormationOrderBy(String value){
		this.basinFormationOrderBy = value;
	}
	
	private String getOrderBy(){
		if(StringUtils.isBlank(this.basinFormationOrderBy)) return "name";
		return this.basinFormationOrderBy;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String hql = "from BasinFormation as A, Well as B where (A.isDeleted = false or A.isDeleted is null) and (B.isDeleted = false or B.isDeleted is null) and B.basin = A.basinUid and B.wellUid = :wellUid order by " + this.getOrderBy();
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "wellUid", userSelection.getWellUid());
		for (Object[] obj : list) {
			BasinFormation basinFormation = (BasinFormation) obj[0];
			LookupItem item = new LookupItem(basinFormation.getName(), basinFormation.getName());
			Map<String,String> filterProperties = new LinkedHashMap<String,String>();
			filterProperties.put("zoneAgeZone",basinFormation.getAgeName()!=null?basinFormation.getAgeName():null);
			item.setFilterProperties(filterProperties);
			result.put(basinFormation.getName(), item);
		}
		return result;
	}

}
