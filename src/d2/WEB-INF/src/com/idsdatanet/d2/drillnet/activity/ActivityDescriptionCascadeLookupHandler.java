package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.ActivityDescriptionMatrix;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityDescriptionCascadeLookupHandler implements CascadeLookupHandler{	
		
	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		List<Object[]> mapList = new ArrayList();
		
		if (key==null) key="";
		Integer Count = 0;
		String clsCode = "";
		String phsCode = "";
		String tskCode = "";
		String usrCode = "";
		for (String item : key.split(":")) {
			Count = Count + 1;
			if (Count == 1){
				clsCode = item;
			}else if(Count == 2){
				phsCode = item;
			}else if(Count == 3){
				tskCode = item;
			}else if(Count == 4){
				usrCode = item;
			}
		}
		
	    String sql ="FROM ActivityDescriptionMatrix WHERE (isDeleted = false OR isDeleted is null) AND " +
	    		"classShortCode = :classShortCode and phaseShortCode = :phaseShortCode and " + 
	 			"taskShortCode = :taskShortCode and userShortCode = :userShortCode";
		List<Object[]> listADM = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, 
				new String[] {"classShortCode", "phaseShortCode", "taskShortCode", "userShortCode" }, new String[] { clsCode,  phsCode, tskCode, usrCode});
		if (listADM.size() > 0) {
			for (Object listADMResult : listADM) {
				String Remarks = "";
				ActivityDescriptionMatrix ADM = (ActivityDescriptionMatrix) listADMResult;
				Remarks = ADM.getRemarks();
				//result.put(Remarks, new LookupItem(Remarks, Remarks));
				LookupItem lookupItem = new LookupItem(ADM.getActivityDescriptionMatrixUid(), Remarks);
				mapList.add(new Object[] {ADM.getActivityDescriptionMatrixUid(), lookupItem});
			}
		}
		
		//Sorting by ActivityDescriptionMatrix.remarks
		Collections.sort(mapList, new ADMComparator());
		for(Iterator i = mapList.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			LookupItem item = (LookupItem) obj_array[1];
			if (!result.containsKey(item.getKey())) {
				String remarks = item.getValue().toString();
				result.put(remarks, new LookupItem(remarks, remarks));
			}
		}
		
		return result;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		String sql ="FROM ActivityDescriptionMatrix WHERE (isDeleted = false OR isDeleted is null)";
		List<Object[]> listADM = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		if (listADM.size() > 0) {
			for (Object listADMResult : listADM) {
				if (listADMResult != null) {
					ActivityDescriptionMatrix ADM = (ActivityDescriptionMatrix) listADMResult;
					
					String clsCode = ADM.getClassShortCode();
					String phsCode = ADM.getPhaseShortCode();
					String tskCode = ADM.getTaskShortCode();
					String usrCode = ADM.getUserShortCode();
					String combinString = clsCode + ":" + phsCode + ":" + tskCode + ":" + usrCode;
					CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(combinString);
					cascadeLookupSet.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, combinString, null)));
					result.add(cascadeLookupSet);				
				}
			}
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		return null;
	}
	
	private class ADMComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				LookupItem in1 = (LookupItem) o1[1];
				LookupItem in2 = (LookupItem) o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(in1.getValue().toString().toLowerCase());
				String f2 = WellNameUtil.getPaddedStr(in2.getValue().toString().toLowerCase());
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}

}
