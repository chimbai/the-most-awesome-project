package com.idsdatanet.d2.drillnet.activityCodeSyncLogger;

import com.idsdatanet.d2.core.stt.active.DaoCommand;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.DepotSyncJob;
import com.idsdatanet.d2.core.model.LookupClassCode;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.LookupRootCauseCode;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.depot.core.DepotConstants;
import com.idsdatanet.depot.core.DepotContext;
import com.idsdatanet.depot.core.DepotUserContext;
import com.idsdatanet.depot.core.mapping.DepotMappingModule;
import com.idsdatanet.depot.core.mapping.MappingAggregator;
import com.idsdatanet.depot.core.query.QueryResult;
import com.idsdatanet.depot.core.webservice.FakeHttpServletRequest;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.XppDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

/**
 * Depot object mapping and obtain of XML file
 * @author mstan
 *
 */
public class ActivityCodeSyncMapping {
	private static XStream xstream = null;

	/**
	 * To get the object in the log file and perform depot mapping to get XML string
	 * @param File f (the transaction log file)
	 */
	public static void getMap(File f) throws Exception {
		String uid = null;
		String objectId = null;
		InputStream gzipInputStream = null;
		ObjectInput objectInput = null;
		try {
			gzipInputStream = new GZIPInputStream(new FileInputStream(f));
			objectInput = getXStream().createObjectInputStream(gzipInputStream);
			while(true) {
				Object obj = objectInput.readObject();
				if (obj instanceof DaoCommand) {
					DaoCommand daoCommand = (DaoCommand)obj;
					Object[] objects = daoCommand.getParams();
					Object idsObj = objects[0];
					
					if (idsObj instanceof LookupClassCode)
					{
						LookupClassCode classCode = (LookupClassCode) idsObj;
						objectId = "lookupClassCode";
						uid = classCode.getLookupClassCodeUid();
					}
					else if(idsObj instanceof LookupPhaseCode)
					{
						LookupPhaseCode phaseCode = (LookupPhaseCode) idsObj;
						objectId = "lookupPhaseCode";
						uid = phaseCode.getLookupPhaseCodeUid();
					}
					else if(idsObj instanceof LookupTaskCode)
					{
						LookupTaskCode taskCode = (LookupTaskCode) idsObj;
						objectId = "lookupTaskCode";
						uid = taskCode.getLookupTaskCodeUid();
					}
					else if(idsObj instanceof LookupRootCauseCode)
					{
						LookupRootCauseCode rootCauseCode = (LookupRootCauseCode) idsObj;
						objectId = "lookupRootCauseCode";
						uid = rootCauseCode.getLookupRootCauseCodeUid();
					}

					if(objectId != null && StringUtils.isNotBlank(objectId) && uid != null && StringUtils.isNotBlank(uid))
						getXMLOutput(objectId, uid);
				}
			}
		}
		catch(EOFException eof){
		}
	}
	
		/**
		 * To create physical xml file from xml string
		 * @param String xmlData (the xml string)
		 * @param String screen (the screen involved)
		 * @return return String filename (the physical XML file name).
		 */
		private static String createXMLFile(String xmlData, String screen) throws Exception {
			try {
				String filename = (new Date()).getTime() + "_" + screen +".xml";
	 
				File file = new File(ApplicationConfig.getConfiguredInstance().getServerRootPath() + "WEB-INF/depot/xml");
				File file2 = new File(ApplicationConfig.getConfiguredInstance().getServerRootPath() + "WEB-INF/depot/xml/" + filename);
				
				if (!file.exists()) {
					file.mkdirs();
				}
	 
				// if file doesnt exists, then create it
				if (!file2.exists()) {
					file2.createNewFile();
				}
	 
				FileWriter fw = new FileWriter(file2.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(xmlData);
				bw.close();
	 
				return (filename);
	 
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
	}

		private static XStream getXStream() {
			if (xstream == null) {
				xstream = new XStream(new XppDriver()) {
					/*
					 * This is a custom mapper to ignore unknown fields
					 * automatically, because when a field is removed from the
					 * class, deserializing an old version that contains the field
					 * will cause an exception
					 */
		            @Override
					protected MapperWrapper wrapMapper(MapperWrapper next) {
		                return new MapperWrapper(next) {
		                    @Override
							public boolean shouldSerializeMember(Class definedIn, String fieldName) {
		                    	return definedIn != Object.class ? super.shouldSerializeMember(definedIn, fieldName) : false;
		                    }
		                };
		            }
		        };
			}
			return xstream;
		}
		
		/**
		 * To get xml string from depot mapping
		 * @param String objectId (the depot object)
		 * @param String uid (the object uid)
		 */
		private static void getXMLOutput(String objectId, String uid) throws Exception{
			
			String xmlContent = null;
			Boolean includeDeletedObject = DepotUserContext.getThreadLocalInstance().isIncludeDeletedRecords();
			
			if(StringUtils.isNotBlank(objectId) && StringUtils.isNotBlank(uid))
			{
				MappingAggregator mappingAggregator = DepotMappingModule.getConfiguredInstance().getMappingAggregator(DepotConstants.D2, objectId);
				Boolean includeEmptyValue = mappingAggregator.canOutputEmptyValue();
				mappingAggregator.setOutputEmptyValue(true);
				
				DepotContext depotContext = new DepotContext(new FakeHttpServletRequest(), mappingAggregator.getStoreVersion(), mappingAggregator.getDefaultNamespace());
				depotContext.setUid(uid);
				DepotUserContext.getThreadLocalInstance().setIncludeDeletedRecords(true);
				
				if (mappingAggregator!=null){
					UserSelectionSnapshot userSelection = initUserSelection("idsadmin");
					String queryTemplate = mappingAggregator.getQueryTemplateForExport();
					queryTemplate = depotContext.resolveQueryTemplateParameters(queryTemplate);
					QueryResult queryResult = mappingAggregator.getFromStore(new FakeHttpServletRequest(), userSelection, queryTemplate, true, false);
					xmlContent = queryResult.getOutputString();
					String filePath = createXMLFile(xmlContent, objectId);
					if(filePath != null && StringUtils.isNotBlank(filePath))
						createDepotJob(filePath);
				}
				mappingAggregator.setOutputEmptyValue(includeEmptyValue);
				DepotUserContext.getThreadLocalInstance().setIncludeDeletedRecords(includeDeletedObject);
			}
		} 
		
		/**
		 * Create depot job
		 * @param String filePath (the path where the physical XML file is stored)
		 */
		private static void createDepotJob(String filePath) throws Exception {
			
			String sql = "SELECT serverPropertiesUid FROM ImportExportServerProperties WHERE (isDeleted = false or isDeleted is null) AND dataTransferType = 'actCodeSync'";
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
			
			if(!list.isEmpty())
			{
				for (String str : list)
				{
					DepotSyncJob dptJob = new DepotSyncJob();
					String serverUid = str;
					dptJob.setSyncRetryCount(0);
					dptJob.setXmlFilepath(filePath);
					dptJob.setSyncStatus("INCOMPLETE");
					dptJob.setImportExportServerPropertiesUid(serverUid);
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(dptJob);
				}
			}
		}

		public static UserSelectionSnapshot initUserSelection(String userName) throws Exception{
			UserSelectionSnapshot userSelection = new UserSelectionSnapshot();
		    userSelection.populdateDataAccordingToUserLogin(userName);
		    
		    return userSelection;
		}
	}
	


