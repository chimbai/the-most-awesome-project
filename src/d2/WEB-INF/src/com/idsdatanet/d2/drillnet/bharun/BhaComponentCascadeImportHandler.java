package com.idsdatanet.d2.drillnet.bharun;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;


import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.LookupBhaComponent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.templateimport.DefaultTemplateImportHandler;
import com.idsdatanet.d2.core.web.mvc.templateimport.EmptySlot;

public class BhaComponentCascadeImportHandler extends DefaultTemplateImportHandler{
	private String defaultUri = null;
	private String parentLookupURI = null;

	public void setDefaultUri(String defaultUri) {
		this.defaultUri = defaultUri;
	}
	
	public String getDefaultUri() {
		return this.defaultUri;
	}
	
	public void setParentLookupURI(String parentLookupURI) {
		this.parentLookupURI = parentLookupURI;
	}

	public String getParentLookupURI() {
		return this.parentLookupURI;
	}
	
	@Override
	protected void setupAdditionalBindingValues(int line,
			List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode) {
		
		if (bindingValue != null) {
			if (bindingValue.get(0) instanceof EmptySlot) {
				
			} else {
				try {
					//this is for csv import of the data into the cascade field 
					this.processComponentTypeCascadeLookup(bindingProperties, bindingValue, targetMeta, commandBean, parentNode);
					this.processEquipmentCascadeLookup(bindingProperties, bindingValue, targetMeta, commandBean, parentNode);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		
	}
	
	private void processComponentTypeCascadeLookup(List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode) throws Exception
	{
		String propertyName = this.getPropertyNameFromField(targetMeta, "componentType");
		int index = bindingProperties.indexOf(propertyName);
		String value = null;
		if(index > -1)
		{
			value = bindingValue.get(index).toString();
		}
		//this is to get the component type lookup items from db
		if (StringUtils.isNotBlank(value))
		{
			String sql1="from CommonLookup where (isDeleted = false or isDeleted is null)";
			List<CommonLookup> commonLookupList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql1);
			LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
			if (commonLookupList.size() > 0) {
				for (CommonLookup commonLookup : commonLookupList) {
					if (commonLookup.getShortCode() != null && commonLookup.getLookupLabel() != null) {
						LookupItem lookupItem = new LookupItem(commonLookup.getShortCode(), commonLookup.getLookupLabel());
						lookupItem.setActive(commonLookup.getIsActive());
						
						result.put(lookupItem.getKey(), lookupItem);
					}
				}
				
				//this is for checking the bha component value in csv import template with the bha component value in common lookup screen
				for (Map.Entry<String, LookupItem> lookupItem: result.entrySet()) {
					if (lookupItem.getValue().getValue().toString().equalsIgnoreCase(value))
						bindingValue.set(index, lookupItem.getKey());
				}
			}
			if(this.parentLookupURI!=null)
			{
				Map<String,LookupItem> lookups = LookupManager.getConfiguredInstance().getLookup(this.parentLookupURI, new UserSelectionSnapshot(), null);
				for (Map.Entry<String, LookupItem> eLookup : lookups.entrySet())
				{
					//this is for checking the component type value in csv import template with the component type value in lookup.xml
					if (eLookup.getValue().getValue().toString().equalsIgnoreCase(value))
						bindingValue.set(index, eLookup.getKey());
				}
			}
		}
	}
	
	private void processEquipmentCascadeLookup(List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode) throws Exception
	{
		String propertyName = this.getPropertyNameFromField(targetMeta, "type");
		int index = bindingProperties.indexOf(propertyName);
		String value = null;
		if(index > -1)
		{
			value = bindingValue.get(index).toString();
		}
		if (StringUtils.isNotBlank(value))
		{
			//This is to get the bha component lookup items from the lookup bha component table.
			String sql = "from LookupBhaComponent where (isDeleted = false or isDeleted is null) order by type";
			List<LookupBhaComponent> lookupBhaComponentList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
			LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
			if (lookupBhaComponentList.size() > 0) {
				for (LookupBhaComponent lookupBhaComponent : lookupBhaComponentList) {
					if (lookupBhaComponent.getLookupBhaComponentUid() != null && lookupBhaComponent.getType() != null) {
						LookupItem lookupItem = new LookupItem(lookupBhaComponent.getLookupBhaComponentUid(), lookupBhaComponent.getType());
						lookupItem.setActive(lookupBhaComponent.getIsActive());
						// supply witsml lookup
						if (StringUtils.isNotBlank(lookupBhaComponent.getWitsmlLookupKey())) {
							lookupItem.addDepotValue("witsml", lookupBhaComponent.getWitsmlLookupKey());
						}
						// supply image key lookup
						if (StringUtils.isNotBlank(lookupBhaComponent.getImageKey()))
						{
							lookupItem.addDepotValue("schematic", lookupBhaComponent.getImageKey());
						}
						result.put(lookupItem.getKey(), lookupItem);
					}
				}
				//this is for checking the bha component value in csv import template with the bha component value in lookup bha component screen
				for (Map.Entry<String, LookupItem> lookupItem: result.entrySet()) {
					if (lookupItem.getValue().getValue().toString().equalsIgnoreCase(value))
						bindingValue.set(index, lookupItem.getKey());
				}
			}
			//this is to get the bha component lookup items from the lookup.xml
			if(this.defaultUri!=null)
			{
				Map<String, LookupItem> lookupList = new HashMap<String, LookupItem>();
				CascadeLookupSet[] cascadeLookupSet = LookupManager.getConfiguredInstance().getCompleteCascadeLookupSet(this.defaultUri, new UserSelectionSnapshot());
				for(CascadeLookupSet cascadeLookup : cascadeLookupSet){
					lookupList.putAll(cascadeLookup.getLookup());
				}
				//this is for checking the bha component value in csv import template with the bha component value in lookup.xml
				for (Map.Entry<String, LookupItem> lookupItem: lookupList.entrySet()) {
					if (lookupItem.getValue().getValue().toString().equalsIgnoreCase(value))
						bindingValue.set(index, lookupItem.getKey());
				}
				
			}
		}
	}
	
}
