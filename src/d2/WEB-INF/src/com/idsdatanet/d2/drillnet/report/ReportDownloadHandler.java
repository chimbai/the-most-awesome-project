package com.idsdatanet.d2.drillnet.report;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.web.mvc.*;
import com.idsdatanet.d2.core.model.ReportFiles;

public class ReportDownloadHandler extends AbstractDownloadHandler {
	private static final String PARAMETER_REPORT_ID = "reportId";
	private static final String PARAMETER_DISPLAY_FILENAME = "filename";
	private static final String PARAMETER_ZIP="zip";
	
	private Log log = LogFactory.getLog(getClass());
	
	public void process(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String reportid = request.getParameter(PARAMETER_REPORT_ID);
		String zip=request.getParameter(PARAMETER_ZIP);
		Boolean isZip=Boolean.parseBoolean(zip);
		if(StringUtils.isBlank(reportid)){
			response.getWriter().print("Report id not specified");
			return;
		}
		
		ReportFiles reportFile = null;
		try{
			reportFile = (ReportFiles) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ReportFiles.class, reportid);
		}catch(Exception e){
			log.error(e.getMessage());
		}
		
		if(reportFile == null || (reportFile.getIsDeleted() != null && reportFile.getIsDeleted())){
			response.getWriter().print("Report file not found: " + reportid);
			return;
		}
		
		File output_file = null;
		
		try{
			output_file = new File(ReportUtils.getFullOutputFilePath(reportFile.getReportFile()));
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		if(output_file == null){
			response.getWriter().print("Unable to create a reference to report file");
			return;
		}
		
		if(! output_file.exists()){
			response.getWriter().print("Report file does not exist");
			return;
		}
		
		String download_filename = request.getParameter(PARAMETER_DISPLAY_FILENAME);
		if(StringUtils.isNotBlank(download_filename)){
			String file_ext = this.getFileExtension(output_file);
			if(StringUtils.isNotBlank(file_ext)){
				if(! download_filename.endsWith(file_ext)) download_filename += file_ext;
			}
		}
		if (isZip)
		{
			String name = request.getParameter("name");
			this.outputAsZippedDownloadAttachment(output_file, name, request, response);			
		}
		else{		
			this.outputAsDownloadAttachment(output_file, download_filename, request, response);
		}
	}
	
	private String getFileExtension(File file){
		String name = file.getName();
		int i = name.lastIndexOf(".");
		if(i == -1){
			return null;
		}else{
			return name.substring(i);
		}
	}	
}
