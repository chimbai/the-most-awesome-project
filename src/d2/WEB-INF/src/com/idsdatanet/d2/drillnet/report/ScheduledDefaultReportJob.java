package com.idsdatanet.d2.drillnet.report;

public class ScheduledDefaultReportJob extends ScheduledAbstractReportJob {
	protected int getReportParamsReportType(){
		return ScheduledReportGenerationJobParams.REPORT_TYPE_DEFAULT_REPORT;
	}
}
