package com.idsdatanet.d2.drillnet.report;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserRequestContext;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.schematics.report.MultiSchematicReportListener;

public class FullBhaSchematicReportModule extends DefaultReportModule{
	
	
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {

		UserSelectionSnapshot userSelection=UserSelectionSnapshot.getInstanceForCustomDaily(session, session.getCurrentDailyUid());
		
		List<UserSelectionSnapshot> selections=this.getDayList(session);
		
		userSelection.setCustomProperty(MultiSchematicReportListener.MULTI_SCHEMATIC_GENERATION, selections);
		
		return super.submitReportJob(userSelection,this.getParametersForReportSubmission(session, request, commandBean));
	
	}

	
	protected ReportOutputFile onLoadReportOutputFile(ReportOutputFile reportOutputFile){
		if(reportOutputFile.isAssociatedDailyAvailable()){
			try {
				// exclude this report from the list if the related reportDaily is outside the access scope
				ReportDaily reportDaily = getRelatedReportDaily(reportOutputFile.getDailyUid(), this.getDailyType());
				UserSession userSession = UserRequestContext.getThreadLocalInstance().getUserSessionOptional();
				if (userSession != null && !userSession.withinAccessScope(reportDaily)) {
					return null;
				}
			} catch (Exception e) {
				// if we can't figure out the access scope, bail
			}
			return reportOutputFile;
		}else{
			return null;
		}
	}
	
	protected List<UserSelectionSnapshot> getDayList(UserSession userSession) throws Exception{
		String strSql = "FROM Bharun WHERE (isDeleted = false or isDeleted is null) and operationUid='"+userSession.getCurrentOperationUid()+"'";
		
		List<Bharun> bhaList=ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
		List<UserSelectionSnapshot> userSelectionList=new ArrayList<UserSelectionSnapshot>();
		for(Bharun d:bhaList)
		{
			if(StringUtils.isNotEmpty(d.getDailyidIn())) userSession.setCurrentDailyUid(d.getDailyidIn());
			userSelectionList.add(UserSelectionSnapshot.getInstanceForCustomDaily(userSession, userSession.getCurrentDailyUid()));
		}
		return userSelectionList;
	}
	
}
