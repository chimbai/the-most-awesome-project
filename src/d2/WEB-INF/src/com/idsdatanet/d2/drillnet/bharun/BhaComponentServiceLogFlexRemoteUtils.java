package com.idsdatanet.d2.drillnet.bharun;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.InventoryServiceLog;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class BhaComponentServiceLogFlexRemoteUtils extends BlazeRemoteClassSupport{
	
	public void getServiceLogList(HttpServletRequest request, HttpServletResponse response, String serialNum, String currBharunUid) {
		//String result = "<root/>";
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		//if (StringUtils.isBlank(serialNum)) return result;
		try {
			//ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			
			
			String queryString = "SELECT i.inventoryServiceLogUid, i.equipmentDescription, i.counterReset, i.serviceDateTime, i.bharunUid, i.operationUid, b.timeOut " +
								 "FROM InventoryServiceLog i, Bharun b " +
								 "WHERE (i.isDeleted=false OR i.isDeleted is null) AND (b.isDeleted=false OR b.isDeleted is null) " +
								 "AND i.bharunUid = b.bharunUid " +
								 "AND i.serialNum = :serialNum ORDER BY b.timeOut, i.serviceDateTime";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"serialNum"}, new Object[] {serialNum});
			if (lstResult.size() > 0){
				for(Object objResult: lstResult){
					//due to result is in array, cast the result to array first and read 1 by 1
					Object[] obj = (Object[]) objResult;
					String inventoryServiceLogUid = "";
					String equipmentDescription = "";
					String counterReset = "";
					Date serviceDateTime = null;
					String bharunUid = "";
					String operationUid = "";
					Date timeout = null;
									
					if (obj[0] != null && StringUtils.isNotBlank(obj[0].toString())) inventoryServiceLogUid = obj[0].toString();
					if (obj[1] != null && StringUtils.isNotBlank(obj[1].toString())) equipmentDescription = obj[1].toString();
					if (obj[2] != null && StringUtils.isNotBlank(obj[2].toString())) counterReset = obj[2].toString();
					if (obj[3] != null && StringUtils.isNotBlank(obj[3].toString())) serviceDateTime = (Date) obj[3];					
					if (obj[4] != null && StringUtils.isNotBlank(obj[4].toString())) bharunUid = obj[4].toString();
					if (obj[5] != null && StringUtils.isNotBlank(obj[5].toString())) operationUid = obj[5].toString();
					if (obj[6] != null && StringUtils.isNotBlank(obj[6].toString())) timeout = (Date) obj[6];
										
					// Write the attributes into record
					SimpleAttributes attr = new SimpleAttributes();
					attr.addAttribute("inventoryServiceLogUid",inventoryServiceLogUid);
					attr.addAttribute("serialNum", serialNum);
					attr.addAttribute("equipmentDescription", equipmentDescription);
					attr.addAttribute("counterReset", counterReset);
					
					if (serviceDateTime != null){
						SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
						attr.addAttribute("serviceDateTime", formatter.format(serviceDateTime));
					}else{
						attr.addAttribute("serviceDateTime","");
					}
					
					attr.addAttribute("bharunUid", bharunUid);
					String bharunNumber = "";
					String dailyUid = "";
					if (bharunUid!="") {
						// to filter in order to get bha run number
						String[] paramsFields = {"bharunUid"};
						Object[] paramsValues = new Object[1]; 
						paramsValues[0] = bharunUid; 
						List<Bharun> bhaList=ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Bharun where (isDeleted = false or isDeleted is null) and bharunUid=:bharunUid", paramsFields, paramsValues,qp);
						if (bhaList.size()>0)
						{
							Bharun currBharun=bhaList.get(0);
							bharunNumber = currBharun.getBhaRunNumber();
							attr.addAttribute("bharunNumber", bharunNumber);
						}						 
						
						// to filter in order to get last bharun daily summary's daily_uid for specific bha#
						List dailyList=ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select bs.dailyUid, d.dayDate from BharunDailySummary bs, Daily d where (bs.isDeleted = false or bs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and bs.dailyUid = d.dailyUid and bs.bharunUid=:bharunUid order by d.dayDate DESC", paramsFields, paramsValues,qp);
						if (dailyList.size() > 0){
							Object objDaily = dailyList.get(0);	
							Object[] latestObj = (Object[]) objDaily;
							if (latestObj[0] != null && StringUtils.isNotBlank(latestObj[0].toString())) dailyUid = latestObj[0].toString();
							attr.addAttribute("dailyUid", dailyUid);
						}						
					}else{
						attr.addAttribute("bharunNumber", bharunNumber);
						attr.addAttribute("dailyUid", dailyUid);
					}
					
					attr.addAttribute("operationUid", operationUid);
					String operationName = "";
					if (operationUid!=null) {
						String[] paramsFields = {"operationUid"};
						Object[] paramsValues = new Object[1]; 
						paramsValues[0] = operationUid; 
						List<Operation> opList=ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and operationUid=:operationUid", paramsFields, paramsValues,qp);
						if (opList.size()>0)
						{
							Operation currOperation=opList.get(0);
							operationName = currOperation.getOperationName();
							attr.addAttribute("operationName", operationName);
						}	
					}else{
						attr.addAttribute("operationName", operationName);
					}

					if (timeout != null){
						SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
						attr.addAttribute("bharunDailyTimeout", formatter.format(timeout));
					}else{
						attr.addAttribute("bharunDailyTimeout","");
					}
					
					//write the element record into the InventoryServiceLog
					writer.addElement("InventoryServiceLog", "", attr);
				}
			}
			
			//Populate current accessed BHA's Time Out value into XML for the usage of controlling "Add New Service Date" button's visibility
			Date currTimeOut = null;
			String queryString2 = "SELECT b.timeOut FROM Bharun b " +
			 "WHERE (b.isDeleted=false OR b.isDeleted is null) " +			 
			 "AND b.bharunUid = :currBharunUid";
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString2, new String[] {"currBharunUid"}, new Object[] {currBharunUid});
			if(lstResult2 != null){
				Object thisResult2 = (Object) lstResult2.get(0);				
				if (thisResult2 != null) {
					currTimeOut = (Date) thisResult2;
				}
			}

			// Write the attributes into record
			SimpleAttributes attr = new SimpleAttributes();
			if (currTimeOut != null){				
				attr.addAttribute("currTimeOut", "true");
			}else{
				attr.addAttribute("currTimeOut","false");
			}

			//write the element record into the currTimeOutStatus
			writer.addElement("currTimeOutStatus", "", attr);
			writer.endElement();
			writer.close();
			//result = bytes.toString();
			//return result;
		} catch (Exception e) {
			e.printStackTrace();
			//return "<root/>";
		}		
	}
	
	public Boolean saveNewServiceLog(HttpServletRequest request, HttpServletResponse response, String serialNum, String descr, String servDate, Boolean isReset, String bharunUid, String bhaComponentUid, String wellUid, String wellboreUid) {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		try {					
			if(serialNum !=null){
				if(StringUtils.isNotEmpty(servDate)){
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
					Date date = dateFormat.parse(servDate);
					InventoryServiceLog servLog = new InventoryServiceLog();
					servLog.setEquipmentDescription(descr);
					servLog.setSerialNum(serialNum);
					servLog.setServiceDateTime(date);
					servLog.setCounterReset(isReset);
					servLog.setGroupUid(UserSession.getInstance(request).getCurrentGroupUid());
					servLog.setBharunUid(bharunUid);
					servLog.setBhaComponentUid(bhaComponentUid);
					servLog.setWellboreUid(wellboreUid);
					servLog.setWellUid(wellUid);
					
					//filter to get bharun time out & operation uid
					if (bharunUid!=null) {
						String[] paramsFields = {"bharunUid"};
						Object[] paramsValues = new Object[1]; 
						paramsValues[0] = bharunUid; 
						List<Bharun> bhaList=ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Bharun where (isDeleted = false or isDeleted is null) and bharunUid=:bharunUid", paramsFields, paramsValues,qp);
						if (bhaList.size()>0)
						{
							Bharun currBharun=bhaList.get(0);
							servLog.setOperationUid(currBharun.getOperationUid());
							servLog.setBharunDailyTimeOut(currBharun.getTimeOut());
							servLog.setBharunDailyidOut(currBharun.getDailyidOut());
						}	
					}
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(servLog);
					
				}else{
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Boolean delServiceLog(HttpServletRequest request, HttpServletResponse response, String inventoryServiceLogUid) {
		try {					
				if(inventoryServiceLogUid != null){	
					QueryProperties qp = new QueryProperties();
					qp.setUomConversionEnabled(false);		
					
					//query base on the inventoryServiceLogUid, set is_deleted = 1 if found	to indicated deleted record			
					String strSql1 = "UPDATE InventoryServiceLog SET isDeleted = 1 WHERE inventoryServiceLogUid =:inventoryServiceLogUid";
					String[] paramsFields1 = {"inventoryServiceLogUid"};
					Object[] paramsValues1 = {inventoryServiceLogUid};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);			
				}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
