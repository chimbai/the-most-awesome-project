package com.idsdatanet.d2.drillnet.rigpump;

import com.idsdatanet.d2.core.model.MudProperties;


public class Mud extends MudProperties implements java.io.Serializable {
	public String rigPumpUid;
	
	public String getRigPumpUid() {
		return this.rigPumpUid;
	}

	public void setRigPumpUid(String rigPumpUid) {
		super.propertyChanged("rigPumpUid", this.rigPumpUid, rigPumpUid);
		this.rigPumpUid = rigPumpUid;
	
	}
	

}
