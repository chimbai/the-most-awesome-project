package com.idsdatanet.d2.drillnet.report;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobResponse;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.report.ReportJobResponse;
import com.idsdatanet.d2.core.report.birt.ReportUtils;
import com.idsdatanet.d2.core.web.mvc.AbstractUOMFormatter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ConcatPdfsJob implements D2Job {

	private String output_file = null;
	private String report_type = null;
	private String display_name = null;
	
	public ConcatPdfsJob(String output, String displayName, String reportType) throws Exception {
		this.output_file = output;
		this.report_type = reportType;
		this.display_name = displayName;
	}
	
	private ReportFiles getReportFile(String dailyUid, String reportType) throws Exception {
		String queryString = "FROM ReportFiles WHERE (isDeleted=false or isDeleted is null) AND reportType=:reportType and dailyUid=:dailyUid order by lastEditDatetime DESC";
		List<ReportFiles> files = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"reportType","dailyUid"}, new Object[]{reportType, dailyUid}); 
		if (files.size()>0) {
			return files.get(0);
		}
		return null; 
	}
	
	private ReportUtils getReportUtils(UserSelectionSnapshot userSelection) throws Exception {
		if(AbstractUOMFormatter.isConfiguredInstanceExists()){
			return new ReportUtils(userSelection, AbstractUOMFormatter.getConfiguredInstance());
		}else{
			return new ReportUtils(userSelection, null);
		}
	}
	
	public void run(UserContext userContext, JobResponse jobResponse) throws Exception {
		
		Object userData = userContext.getUserData();
		UserSelectionSnapshot userSelection = userContext.getUserSelection();

		if (userData instanceof Map) {
			List<String> allPdfFiles = new ArrayList<String>();
			Map<DDRReportModule, List<Daily>> mapToProcess = (Map<DDRReportModule, List<Daily>>) userData; 
			for (Map.Entry<DDRReportModule, List<Daily>> entry : mapToProcess.entrySet()) {
				DDRReportModule reportModule = entry.getKey();
				for(Daily daily: entry.getValue()){
					String dailyUid = daily.getDailyUid();
					String reportType = reportModule.getReportType();
					ReportFiles reportFile = this.getReportFile(dailyUid, reportType);
					File attachedFile = new File(com.idsdatanet.d2.drillnet.report.ReportUtils.getFullOutputFilePath(reportFile.getReportFile()));
					if(attachedFile.exists()) {
						allPdfFiles.add(com.idsdatanet.d2.drillnet.report.ReportUtils.getFullOutputFilePath(reportFile.getReportFile()));
					}
				}
			}
			
			//merge PDF files
			ReportUtils reportUtils = this.getReportUtils(userSelection);
			reportUtils.concatPdfs(allPdfFiles, com.idsdatanet.d2.drillnet.report.ReportUtils.getFullOutputFilePath(this.output_file));
			
			ReportFiles reportFiles = null;
			
			boolean newFile = false;
			
			List<Object> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportFiles where (isDeleted = false or isDeleted is null) and reportFile = :reportFile", "reportFile", this.output_file);
			if(rs.size() > 0){
				reportFiles = (ReportFiles) rs.get(0);
			}else{
				reportFiles = new ReportFiles();
				newFile = true;
			}
			
			reportFiles.setDateGenerated(new Date());
			reportFiles.setReportUserUid(userSelection.getUserUid());
			reportFiles.setReportOperationUid(null);				
			reportFiles.setDailyUid(null);
			reportFiles.setReportFile(this.output_file);
			reportFiles.setReportType(this.report_type);
			reportFiles.setGroupUid(userSelection.getGroupUid());
			reportFiles.setReportDayNumber(null);
			reportFiles.setReportDayDate(null);
			reportFiles.setDisplayName(this.display_name);
			
			if(newFile){
				reportFiles.setIsPrivate(! "1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_NEWLY_GENERATED_REPORT_DEFAULT_PUBLIC)));
			}
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportFiles);
		}

		
		if(jobResponse != null){
			jobResponse.setResponseMessage("Report generated: " + (this.getOutputFile() == null ? "[unspecified]" : this.getOutputFile().getAbsolutePath()));
			jobResponse.setResponseData(new ReportJobResponse(this.getOutputFile(), null));
			jobResponse.setSuccess(true);
		}
	}

	public void dispose() {
		this.output_file = null;
		this.report_type = null;
		this.display_name = null;
	}

	/**
	 * Return a File object for the output file
	 * @return
	 * @throws Exception
	 */
	protected File getOutputFile() throws Exception {
		if(StringUtils.isBlank(this.output_file)) return null;
		return new File(this.output_file);
	}
}
