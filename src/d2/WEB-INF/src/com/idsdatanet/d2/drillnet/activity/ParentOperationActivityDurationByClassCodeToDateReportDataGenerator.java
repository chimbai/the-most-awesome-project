package com.idsdatanet.d2.drillnet.activity;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @author Jackson
 * 
 * Method to mine activity duration record based on class code/ internal class code that available within a well's parent
 * System will always loop till the up most wellbore and get all the duration by class code
 * 
 */
public class ParentOperationActivityDurationByClassCodeToDateReportDataGenerator  implements ReportDataGenerator{
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String currentWellboreUid = userContext.getUserSelection().getWellboreUid();
		Wellbore currentWellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(currentWellboreUid);
		String strOperationList = "";
		String strBit = "";
		
		//check if this current wellbore have a parent, then get all it parent for query
		if (currentWellbore != null && StringUtils.isNotBlank(currentWellbore.getParentWellboreUid())){
			
			//Get all parent wellbore operation 
			List<String> operationList = ActivityUtils.getParentWellboreOperationList(currentWellboreUid);
			if(operationList != null){
				strBit = "";
				for(String operation: operationList){
					strOperationList = strOperationList + strBit + "'" + operation + "'";
					strBit = ",";
				}
			
				//Use the strOperationList to retrieve all of the activities using an IN clause
				String strSql = "SELECT a.classCode, a.internalClassCode, SUM(a.activityDuration) as totalDuration FROM Daily d, Activity a " +
					"WHERE (d.isDeleted = false or d.isDeleted is null) AND (a.isDeleted = false or a.isDeleted is null) AND d.dailyUid = a.dailyUid " +
					"AND NOT(a.classCode is null or a.classCode = '') AND d.dayDate <= :userDate AND a.operationUid IN (" + strOperationList + 
					") AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " +
					"GROUP BY a.classCode";
				String[] paramsFields = {"userDate"};
				Object[] paramsValues = {todayDate};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				if (lstResult.size() > 0){
					CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activityDuration");
					
					for(Object objResult: lstResult){
						//due to result is in array, cast the result to array first and read 1 by 1
						Object[] objDuration = (Object[]) objResult;
						
						String classCode = "P";
						String internalClassCode = "P"; 
					
						Double totalDuration = 0.0;						
						
						if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())) classCode = objDuration[0].toString();
						if (objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())) internalClassCode = objDuration[1].toString();
						
						if (objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())){
							totalDuration = Double.parseDouble(objDuration[2].toString());
							
							//assign unit to the value base on activity.activityDuration, this will do auto convert to user display unit + format
							thisConverter.setBaseValue(totalDuration);
							totalDuration = thisConverter.getConvertedValue();					
						}
						
						ReportDataNode thisReportNode = reportDataNode.addChild("code");
						thisReportNode.addProperty("name", classCode);
						thisReportNode.addProperty("internalClassCode", internalClassCode);
						thisReportNode.addProperty("duration", totalDuration.toString());	
					}			
				}
			}
		} else {
			
			String strSql = "SELECT a.classCode, SUM(a.activityDuration) AS totalDuration, a.internalClassCode FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND " +
				"(a.isDeleted = false OR a.isDeleted IS NULL) AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
				"((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) GROUP BY a.classCode";
				String[] paramsFields = {"userDate", "thisOperationUid"};
				Object[] paramsValues = {todayDate, userContext.getUserSelection().getOperationUid()};
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			if (lstResult.size() > 0){
				for(Object objResult: lstResult){
					//due to result is in array, cast the result to array first and read 1 by 1
					Object[] objDuration = (Object[]) objResult;
					
					//default class to program, cause some record might have no class code
					String classCode = "P";
					String internalClassCode = "P";
	
					Double totalDuration = 0.0;
					
					if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())) classCode = objDuration[0].toString();
					if (objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())){
						totalDuration = Double.parseDouble(objDuration[1].toString());
						
						//assign unit to the value base on activity.activityDuration, this will do auto convert to user display unit + format
						CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
						thisConverter.setBaseValue(totalDuration);
						totalDuration = thisConverter.getConvertedValue();
					}
					if (objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())) internalClassCode = objDuration[2].toString();
					ReportDataNode thisReportNode = reportDataNode.addChild("code");
					thisReportNode.addProperty("name", classCode);
					thisReportNode.addProperty("internalClassCode", internalClassCode);
					thisReportNode.addProperty("duration", totalDuration.toString());
					
				}			
			}
		}
	}
}
