package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class NPTByPreviousRMReportDataGenerator extends NPTReportDataGenerator{
	
	protected void generateActualData(UserContext userContext, Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		List<NPTSummary> includedList = this.shouldIncludeWell(userContext, startDate, endDate);
		this.includeQueryData(includedList, reportDataNode, userContext.getUserSelection(), startDate, endDate);
	}
	
	private List<NPTSummary> generatedata(UserContext userContext, Date startDate, Date endDate, String includeRMOperationUid, String operationType) throws Exception
	{
		String taskName = "N/A";
		String[] paramNames = {"includeRMOperationUid"};
		Object[] values = {includeRMOperationUid};
		String sql = "SELECT o.operationUid, o.operationName, sum(a.activityDuration) as duration,a.classCode, a.taskCode,a.phaseCode "+
			" FROM Operation o, Activity a"+ 
			" Where o.operationUid=a.operationUid"+ 
			" and o.operationCode='RM'"+
			" and (a.isDeleted is null or a.isDeleted = False)"+
			" and (o.isDeleted is null or o.isDeleted = False)"+			
			" and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) "+
			" and (a.internalClassCode='TP' or a.internalClassCode='TU')"+ 			
			" and o.operationUid=:includeRMOperationUid"+
			" group by a.taskCode order by o.startDate desc";		
		
		List<NPTSummary> includedList = new ArrayList<NPTSummary>();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, values);
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
		for (Object[] item :result)
		{		
			NPTSummary npt = new NPTSummary(null,(String)item[0], null, null, null,null,null,null);
			npt.setDuration(Double.parseDouble(item[2].toString()));
			npt.setOperationName((String)item[1]);
			npt.setOperationUid((String)item[0]);
			thisConverter.setBaseValue(npt.getDuration());
			npt.setDuration(thisConverter.getConvertedValue());	
			
			String taskCode = (String)item[4];
			String strSql2 = "SELECT name FROM LookupTaskCode WHERE (isDeleted is null OR isDeleted = false) AND shortCode = :thisTaskCode AND (operationCode = :thisOperationType OR operationCode = '')";
			String[] paramsFields2 = {"thisTaskCode", "thisOperationType"};
			Object[] paramsValues2 = {taskCode, operationType};
			
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
			if (lstResult2.size()>0)
			{
				Object lookupTaskCodeResult = (Object) lstResult2.get(0);
				if(lookupTaskCodeResult != null) taskName = lookupTaskCodeResult.toString();
			}			
			npt.setClassCode((String)item[3]);
			npt.setTaskCode(taskName);
			npt.setPhaseCode((String)item[5]);
			includedList.add(npt);
		}
		return includedList;
		
	}
	
	private List<NPTSummary> shouldIncludeWell(UserContext userContext, Date startDate, Date endDate) throws Exception
	{
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		String wellboreUid= userSelection.getWellboreUid();
	
		String[] params = {"wellboreUid","startDate"};
		Object[] values = {wellboreUid,startDate};
		String sql = "SELECT operationUid,operationName,operationCode FROM Operation where (isDeleted is null or isDeleted = False) and wellboreUid=:wellboreUid and startDate<=:startDate and operationCode='RM' order by startDate DESC";
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		List<Object[]> getResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, params, values);
		
		List<NPTSummary> result = new ArrayList<NPTSummary>();
		for (Object[] item :getResult)
		{
			result.addAll(this.generatedata(userContext, startDate, endDate,(String)item[0],(String)item[2]));
			break; // always get only first result
		}
		return result;
	}

	private void includeQueryData(List<NPTSummary> nptSummary, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception
	{
		ReportDataNode nodeChild = node.addChild("NPTByPreviousRM");		
		for (NPTSummary npt : nptSummary)
		{	
			ReportDataNode catChild = nodeChild.addChild("RMOperation");
			catChild.addProperty("operationUid", npt.getOperationUid());
			catChild.addProperty("operationName", npt.getOperationName());	
			catChild.addProperty("classCode", String.valueOf(npt.getClassCode()));
			catChild.addProperty("taskCode", String.valueOf(npt.getTaskCode()));
			catChild.addProperty("phaseCode", String.valueOf(npt.getPhaseCode()));
			catChild.addProperty("duration", String.valueOf(npt.getDuration()));
		}
	
	}

}
