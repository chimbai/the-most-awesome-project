package com.idsdatanet.d2.drillnet.equipmentFailure;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.EquipmentFailure;
import com.idsdatanet.d2.core.model.LwdSuite;
import com.idsdatanet.d2.core.model.LwdTool;
import com.idsdatanet.d2.core.model.WirelineRun;
import com.idsdatanet.d2.core.model.WirelineTool;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class EquipmentFailureUtils {
	
	
	public static void getLwdTool(CommandBeanTreeNode node, String lwdToolUid) throws Exception {
		Object objFailure = node.getData();
		
		if (objFailure !=null && objFailure instanceof EquipmentFailure && StringUtils.isNotBlank(lwdToolUid)){
			EquipmentFailure thisEquipmentFailure = (EquipmentFailure) objFailure;
			Object objTool =ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LwdTool.class, lwdToolUid); 
			
			if (objTool != null && objTool instanceof LwdTool) {
				LwdTool tool = (LwdTool) objTool;
				
				thisEquipmentFailure.setDateFailStart(tool.getFailureDatetime());
				thisEquipmentFailure.setFailedItem(tool.getToolDesc());
				thisEquipmentFailure.setToolUid(lwdToolUid);
				thisEquipmentFailure.setToolType("lwd_tool");
				
				node.getDynaAttr().put("lwd_tool", tool.getToolDesc());
				
				Object objSuite =ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LwdSuite.class, tool.getLwdSuiteUid()); 
				
				if (objSuite != null && objSuite instanceof LwdSuite)
				{
					LwdSuite suite = (LwdSuite) objSuite;

					thisEquipmentFailure.setRunNumber(suite.getSuiteNum());
					thisEquipmentFailure.setCompany(suite.getCompanyUid());
					node.getDynaAttr().put("lwd_com", suite.getCompanyUid());
				}
				
			}
			
		}
	}
	
	public static void getWirelineTool(CommandBeanTreeNode node, String toolUid) throws Exception {
		Object objFailure = node.getData();
		
		if (objFailure !=null && objFailure instanceof EquipmentFailure && StringUtils.isNotBlank(toolUid)){
			EquipmentFailure thisEquipmentFailure = (EquipmentFailure) objFailure;
			Object objTool =ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(WirelineTool.class, toolUid); 
			
			if (objTool != null && objTool instanceof WirelineTool) {
				WirelineTool wtool = (WirelineTool) objTool;
				
				thisEquipmentFailure.setDateFailStart(wtool.getFailureDatetime());
				thisEquipmentFailure.setFailedItem(wtool.getToolCode());
				thisEquipmentFailure.setToolUid(toolUid);
				thisEquipmentFailure.setToolType("wireline_tool");
				node.getDynaAttr().put("wireline_tool", wtool.getToolCode());
				
				Object objRun =ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(WirelineRun.class, wtool.getWirelineRunUid()); 
				
				if (objRun != null && objRun instanceof WirelineRun)
				{
					WirelineRun wirerun = (WirelineRun) objRun;

					thisEquipmentFailure.setRunNumber(wirerun.getRunNumber());
					thisEquipmentFailure.setCompany(wirerun.getSvcCo());
					node.getDynaAttr().put("wireline_com", wirerun.getSvcCo());
				}
			}
		}
	}
	
	private static void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		}
	}
}
