package com.idsdatanet.d2.drillnet.well;

public class HierarchicalOperationLookupHandler extends HierarchicalAccessibleOperationLookupHandler {
	@Override
	protected boolean onlyReturnAccessibleOperations() {
		return false;
	}

}
