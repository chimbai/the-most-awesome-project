package com.idsdatanet.d2.drillnet.report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.JobServer;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.DailyReportModuleResolver;

public class CombinedDDRReportModule extends ManagementReportModule {
	private static final String MGT_REPORT_DATE = "mgt_report_date";
	private static final String FIELD_SELECTED_DATE = "mgtSelectedDate";
	private static final String FIELD_SELECTED_OPERATIONS = "mgtSelectedOperations";
	private static final String FIELD_SELECTED_OPERATION_TYPE = "mgtSelectedOperationType";
	private static final String LATEST_REPORT_FLAG = "latestReport";
	private long reportJobTimeoutMilliSeconds = 300000; // default to 5 minutes
	
	public void setReportJobTimeoutMilliSeconds(long value){
		this.reportJobTimeoutMilliSeconds = value;
	}
	
	private boolean isEmptyString(Object value){
		if(value == null) return true;
		return StringUtils.isBlank(value.toString());
	}
	
	private Map getSelectedOperations(CommandBean commandBean) throws Exception {
		Object selected_operations = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_OPERATIONS);
		if(selected_operations == null){
			return null;
		}
		if(StringUtils.isBlank(selected_operations.toString())){
			return null;
		}
		
		String[] id_list = selected_operations.toString().split(",");
		Map result = new HashMap();
		for(String id: id_list){
			result.put(id.trim(), null);
		}
		return result;
	}

	@Override
	protected synchronized ReportJobParams submitReportJobWithEmail(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		return this._submitReportJob(session, request, commandBean, true);
	}

	@Override
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		return this._submitReportJob(session, request, commandBean, false);
	}
	
	private ReportJobParams _submitReportJob(UserSelectionSnapshot userSelection, ReportJobParams reportJobData) throws Exception {
		Date reportDate = new Date();
		
		Map<String, String> selected_operations = new LinkedHashMap<String, String>();
		
		if(reportJobData.getMultiWellList() != null){
			List<UserSelectionSnapshot> multiWellList = reportJobData.getMultiWellList();
			for (UserSelectionSnapshot thisSelection : multiWellList) {
				selected_operations.put(thisSelection.getOperationUid(), thisSelection.getDailyUid());
			}
			
			List<Daily> filtered_list = new ArrayList<Daily>();
			List<Daily> daily_list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, true);
			for(Daily daily: daily_list){
				if(selected_operations.containsKey(daily.getOperationUid())){
					filtered_list.add(daily);
				}
			}
			
			Map<DDRReportModule, List<Daily>> mapToProcess = new LinkedHashMap<DDRReportModule,List<Daily>>();
			for (Daily daily : filtered_list)
			{
				String operationCode = DailyReportModuleResolver.getOperationTypeByDailyUid(daily.getDailyUid());
				DDRReportModule module = DailyReportModuleResolver.getConfiguredInstance().getReportModuleByOperationType(operationCode);
				if (mapToProcess.get(module)==null) {
					mapToProcess.put(module, new ArrayList<Daily>());
				}
				List<Daily> list = mapToProcess.get(module);
				list.add(daily);
				mapToProcess.put(module, list);
			}
			
			String userName = "";
			if (StringUtils.isNotBlank(userSelection.getUserUid())) {
				User user = (User) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(User.class, userSelection.getUserUid());
				if (user!=null) userName = user.getUserName();
			}				
			for (Map.Entry<DDRReportModule, List<Daily>> entry : mapToProcess.entrySet()) {
				DDRReportModule reportModule = entry.getKey();
				for(Daily daily: entry.getValue()){
					ReportJobParams params = new ReportJobParams();
					params.setUserSessionReportJob(false);
					params.setReportAutoEmail(this.getEmailConfiguration());
					params.setPaperSize(defaultPaperSize);	
					reportModule.submitReportJob(UserSelectionSnapshot.getInstanceForCustomDaily(userName,daily.getDailyUid()), params);
				}
			}
			//For the getEmailContentMapping function in below mergePdfs function, it is for the email content values to pass over to mergePdfs function
			return this._mergePdfs(userSelection, mapToProcess, reportDate, reportJobData.getReportAutoEmail(), reportJobData.getEmailContentMapping());
		}else{
			throw new Exception("No data fit the selected criteria therefore unable to generate report.");
		}
	}
	
	private ReportJobParams _submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean, Boolean autoEmail) throws Exception {
		
		if (this.isUserSessionReportJobRunning()) throw new Exception("Current job is still running");
		
		boolean useLocalizedCutoffTimeLogic = false;
		Date reportDate = null;
		Object dateParam = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_DATE);
		Object operationTypes = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_OPERATION_TYPE);
		
		if(isEmptyString(dateParam)) dateParam = LATEST_REPORT_FLAG;
		
		if(LATEST_REPORT_FLAG.equals(dateParam)){
			reportDate = new Date();
			useLocalizedCutoffTimeLogic = true;
		}else{
			if (dateParam.toString().indexOf(",") > -1) {
				String[] dateRange = dateParam.toString().split(",");
				reportDate = CommonDateParser.parse(dateRange[0].toString());
			}else {
				reportDate = CommonDateParser.parse(dateParam.toString());
			}
			useLocalizedCutoffTimeLogic = false;
		}  
		
		Map selected_operations = this.getSelectedOperations(commandBean);
		if(selected_operations != null){
			List<Daily> filtered_list = new ArrayList<Daily>();
			List<Daily> daily_list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(reportDate, useLocalizedCutoffTimeLogic);
			for(Daily daily: daily_list){
				if(selected_operations.containsKey(daily.getOperationUid())){
					filtered_list.add(daily);
					reportDate = daily.getDayDate();
				}
			}
			
			Map<DDRReportModule, List<Daily>> mapToProcess = new LinkedHashMap<DDRReportModule,List<Daily>>();
			for (Daily daily : filtered_list)
			{
				String operationCode = DailyReportModuleResolver.getOperationTypeByDailyUid(daily.getDailyUid());
				DDRReportModule module = DailyReportModuleResolver.getConfiguredInstance().getReportModuleByOperationType(operationCode);
				if (mapToProcess.get(module)==null) {
					mapToProcess.put(module, new ArrayList<Daily>());
				}
				List<Daily> list = mapToProcess.get(module);
				list.add(daily);
				mapToProcess.put(module, list);
			}
			
			for (Map.Entry<DDRReportModule, List<Daily>> entry : mapToProcess.entrySet()) {
				DDRReportModule reportModule = entry.getKey();
				for(Daily daily: entry.getValue()){
					ReportJobParams params = new ReportJobParams();
					params.setUserSessionReportJob(false);
					params.setReportAutoEmail(this.getEmailConfiguration());
					params.setPaperSize(defaultPaperSize);	
					reportModule.submitReportJob(UserSelectionSnapshot.getInstanceForCustomDaily(session.getUserName(),daily.getDailyUid()), params);
				}
			}

			ReportAutoEmail reportAutoEmail = null;
			ReportJobParams params = new ReportJobParams();
			if (autoEmail) {
				ReportAutoEmail config = this.getEmailConfiguration();
				reportAutoEmail = new ReportAutoEmail();
				reportAutoEmail.setEmailList(this.emailAddress);
				reportAutoEmail.setUseEmailList(true);
				if (config!=null && config.getEmailSubject()!=null){
					reportAutoEmail.setEmailSubject(config.getEmailSubject());
				}else{
					reportAutoEmail.setEmailSubject("IDS Report");
				}
				if (config!=null && config.getEmailContentTemplateFile()!=null){
					reportAutoEmail.setEmailContentTemplateFile(config.getEmailContentTemplateFile());
				}else{
					reportAutoEmail.setEmailContentTemplateFile("report/MgtReportEmailBody.ftl");
				}
				if (config!=null && config.getEmailListKey()!=null){
					reportAutoEmail.setEmailListKey(config.getEmailListKey());
				}else{
					reportAutoEmail.setEmailListKey("DefaultEmailList");
				}
				reportAutoEmail.setSendEmailAfterReportGeneration(true);
				reportAutoEmail.setOpsTeamBased(false);
			}
			//For the getEmailContentMapping function in below mergePdfs function, it is for the email content values to pass over to mergePdfs function
			UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
			userSelection.setCustomProperty(FIELD_SELECTED_OPERATION_TYPE, operationTypes);
			return this._mergePdfs(userSelection, mapToProcess, reportDate, reportAutoEmail, params.getEmailContentMapping());
		}else{
			throw new Exception("No data fit the selected criteria therefore unable to generate report.");
		}
		
	}
	
	private ReportJobParams _mergePdfs(UserSelectionSnapshot userSelection, Map<DDRReportModule, List<Daily>> reportList, Date reportDate, ReportAutoEmail reportAutoEmail, Map emailContentMapping) throws Exception {
		//UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
		userSelection.setCustomProperty(MGT_REPORT_DATE, reportDate);
		UserContext userContext = UserContext.getUserContext(userSelection, reportList);
		if(userContext == null) throw new Exception("UserContext must not be null");
		
		ReportJobParams reportJobData = new ReportJobParams();
		reportJobData.setUserSessionReportJob(true);
		reportJobData.setReportAutoEmail(this.getEmailConfiguration());
		reportJobData.setPaperSize(defaultPaperSize);
		//Set the values that want to show in email content in reportJobData so that can passing these values to the sendEmailAfterReportGenerated function
		reportJobData.setEmailContentMapping(emailContentMapping);
		reportJobData.setLocalOutputFile(this.getOutputFileInLocalPath(userContext, reportJobData.getPaperSize()));
		
		if (reportAutoEmail!=null) {
			reportJobData.setReportAutoEmail(reportAutoEmail);
		}
		
		String output_file = reportJobData.getLocalOutputFile();
		
		if(StringUtils.isBlank(output_file)) throw new Exception("Output file must not be null");
		
		ReportFiles reportFiles = new ReportFiles();
		boolean newFile = true;
		List<Object> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportFiles where (isDeleted = false or isDeleted is null) and reportFile = :reportFile", "reportFile", output_file);
		if(rs.size() > 0){
			reportFiles = (ReportFiles) rs.get(0);
			newFile = false;
		}
		reportFiles.setDateGenerated(new Date());
		reportFiles.setReportUserUid(userSelection.getUserUid());
		reportFiles.setReportOperationUid(null);				
		reportFiles.setDailyUid(null);
		reportFiles.setReportFile(output_file);
		reportFiles.setReportType(this.getReportType());
		reportFiles.setGroupUid(userSelection.getGroupUid());
		reportFiles.setReportDayNumber(null);
		reportFiles.setReportDayDate(reportDate);
		
		ReportTypes reportTypes = ReportUtils.getConfiguredInstance().getReportType(userSelection.getGroupUid(), this.getReportType(),reportJobData.getPaperSize());
		String displayName = this.getReportDisplayNameOnCreation(reportFiles, reportTypes, null, null, null, userSelection, reportJobData);
		reportFiles.setDisplayName(displayName);
		if(newFile){
			reportFiles.setIsPrivate(! "1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_NEWLY_GENERATED_REPORT_DEFAULT_PUBLIC)));
		}
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportFiles);
		
		
		D2Job job = new ConcatPdfsJob(output_file, displayName, this.getReportType());
		
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
		JobContext currentJob = JobServer.getConfiguredInstance().createJob(job, userContext);
		currentJob.setTimeout(this.reportJobTimeoutMilliSeconds);
		currentJob.setJobGroup(REPORT_JOB_POOL_NAME);
		currentJob.setJobDescription("Generate Combined Daily Report for Date " + df.format(reportDate));
		currentJob.setJobListener(this);
		
		this.reportJobs.put(currentJob, reportJobData);
		
		if(reportJobData.isUserSessionReportJob()) this.userSessionReportJob = currentJob;

		ReportJobResult jobResult = reportJobData.addReportJobResult();
		jobResult.setReportFilePath(output_file);
		
		JobServer.getConfiguredInstance().submitJob(currentJob);

		return reportJobData;

	}
	
	
	@Override
	public synchronized ReportJobParams submitReportJob(
			List<UserSelectionSnapshot> list) throws Exception {
		return this.submitReportJob(list, null);
	}

	@Override
	public synchronized ReportJobParams submitReportJob(
			List<UserSelectionSnapshot> list, ReportJobParams params)
			throws Exception {
		if(list == null) return null;
		if(list.isEmpty()) return null;
		
		if(params == null) params = new ReportJobParams();
		params.setMultiWellList(list);
		UserSelectionSnapshot userSelection = list.get(0);
		if(userSelection == null) return null;
		return this._submitReportJob(userSelection, params);
	}

}
