package com.idsdatanet.d2.drillnet.report;

import java.util.Date;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

public abstract class ScheduledAbstractReportJob implements FactoryBean, InitializingBean {
	private ReportManager reportManager = null;
	private String emailListKey = null;
	private String emailSubject = null;
	private String emailBodyTemplateFile = null;
	private boolean sendEmailAfterReportGeneration = true;
	private boolean opsTeamBased = false;
	private String defaultReportType = null;
	private DefaultReportModule defaultReportModule = null;
		
	public void setSendEmailAfterReportGeneration(boolean value){
		this.sendEmailAfterReportGeneration = value;
	}
	
	public void setReportManager(ReportManager value){
		this.reportManager = value;
	}
	
	public void setEmailDistributionListKey(String value){
		this.emailListKey = value;
	}
	
	public void setEmailBodyTemplateFile(String value){
		this.emailBodyTemplateFile = value;
	}
	
	public void setEmailSubject(String value){
		this.emailSubject = value;
	}
	
	public void setOpsTeamBased(boolean opsTeamBased) {
		this.opsTeamBased = opsTeamBased;
	}
	
	public void setDefaultReportType(String value) {
		this.defaultReportType = value;
	}
	
	public void setDefaultReportModule(DefaultReportModule value) {
		this.defaultReportModule = value;
	}

	public Object getObject() throws Exception {
		ScheduledReportGenerationJobParams params = new ScheduledReportGenerationJobParams();
		params.reportManager = this.reportManager;
		params.reportType = this.getReportParamsReportType();
		if(this.defaultReportType != null) params.defaultReportType = this.defaultReportType;
		if(this.defaultReportModule != null) params.defaultReportModule = this.defaultReportModule;
					
		ReportAutoEmail autoEmail = new ReportAutoEmail();
		autoEmail.setEmailListKey(this.emailListKey);
		autoEmail.setEmailSubject(this.emailSubject);
		autoEmail.setEmailContentTemplateFile(this.emailBodyTemplateFile);
		autoEmail.setSendEmailAfterReportGeneration(this.sendEmailAfterReportGeneration);
		autoEmail.setOpsTeamBased(this.opsTeamBased);

		params.reportAutoEmail = autoEmail;
		
		JobDetail job = JobBuilder.newJob(com.idsdatanet.d2.drillnet.report.ScheduledReportGenerationJob.class).withIdentity(this.getClass().getName() + String.valueOf(new Date().getTime())).storeDurably().build();
		job.getJobDataMap().put(ScheduledReportGenerationJobParams.class.getName(), params);
		return job;
	}

	protected abstract int getReportParamsReportType();
	
	public Class getObjectType() {
		return org.quartz.JobDetail.class;
	}

	public boolean isSingleton() {
		return false;
	}
	
	public void afterPropertiesSet() throws Exception {
		if (getReportParamsReportType() != ScheduledReportGenerationJobParams.REPORT_TYPE_DDR && this.opsTeamBased) {
			throw new Exception("opsTeamBased can only apply for daily report job");
		}
	}
}
