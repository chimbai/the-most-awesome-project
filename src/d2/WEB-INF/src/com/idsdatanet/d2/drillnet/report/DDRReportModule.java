package com.idsdatanet.d2.drillnet.report;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.EmailList;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserRequestContext;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.view.FreemarkerUtils;

public class DDRReportModule extends DefaultReportModule {
	private static final String FIELD_SELECTED_DAYS = "reportSelectedDays";
	//private List<ReportDaily> _allowedReportDaily = null;
	private Date timeReportFileCreated;
	
	public boolean getNewlyGeneratedFlag(){
		if(this.timeReportFileCreated == null) return false;
		return FreemarkerUtils.isDocumentNewlyGenerated(this.timeReportFileCreated);
	}
	
	public String getTimeReportFileCreatedText(){
		if(this.timeReportFileCreated == null) return null;
		return FreemarkerUtils.formatDurationSince(this.timeReportFileCreated);
	}
	
	private String getUserName(String userUid) throws Exception {
		User user = ApplicationUtils.getConfiguredInstance().getCachedUser(userUid);
		if(user == null) return "";
		if(user.getFname()!=null)
		{
			return user.getFname();
		}
		return user.getUserName();
	}
	
	protected ReportOutputFile onLoadReportOutputFile(ReportOutputFile reportOutputFile){
		if(reportOutputFile.isAssociatedDailyAvailable()){
			try {
				// exclude this report from the list if the related reportDaily is outside the access scope
				ReportDaily reportDaily = getRelatedReportDaily(reportOutputFile.getDailyUid(), this.getDailyType());
				UserSession userSession = UserRequestContext.getThreadLocalInstance().getUserSessionOptional();
				if (userSession != null && !userSession.withinAccessScope(reportDaily)) {
					return null;
				}
			} catch (Exception e) {
				// if we can't figure out the access scope, bail
			}
			return reportOutputFile;
		}else{
			return null;
		}
	}

	void generateDDROnCurrentSystemDatetime(String loginUser, ReportAutoEmail reportAutoEmail) throws Exception {
		List<Daily> list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(new Date());
		List<UserSelectionSnapshot> selections = new ArrayList<UserSelectionSnapshot>();
		for(Daily daily: list){
			selections.add(UserSelectionSnapshot.getInstanceForCustomDaily(loginUser,daily.getDailyUid()));
		}

		ReportJobParams params = new ReportJobParams();
		params.setUserSessionReportJob(false);
		params.setReportAutoEmail(reportAutoEmail);
		params.setPaperSize(super.defaultPaperSize);	
		
		super.submitReportJob(selections, params);
	}
	
	public ReportJobParams generateDDRByDailyUid(String dailyUid, String loginUser, ReportAutoEmail reportAutoEmail) throws Exception {
		UserSelectionSnapshot userSelection = UserSelectionSnapshot.getInstanceForCustomDaily(loginUser, dailyUid);
		
		ReportJobParams params = new ReportJobParams();
		params.setUserSessionReportJob(false);
		params.setReportAutoEmail(reportAutoEmail);
		params.setPaperSize(super.defaultPaperSize);	
		
		return super.submitReportJob(userSelection, params);
	}
	
	protected String getReportType(UserSelectionSnapshot userSelection) throws Exception {
		if ("DDR".equals(super.getReportType())) {
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "DDRandDIRShareSameController"))){
				if ("INTV".equals(userSelection.getOperationType())) return "INTV";
			}
			if ("CMPLT".equals(userSelection.getOperationType())) return "CMPLT";
			if ("CSG".equals(userSelection.getOperationType())) return "CSG"; //coal seam gas
		}
		
		return super.getReportType(userSelection);
	}
	
	private List<ReportDaily> getAllowedReportDaily(UserSession session) throws Exception {
		//if (this._allowedReportDaily!=null) return this._allowedReportDaily;
		Operation thisOperation = session.getCurrentOperation();
		String sortOrder = this.getReportDateChooserSortOrder();

		if (thisOperation == null) return null;
		
		String dailyType = this.getDailyType(new UserSelectionSnapshot(session));
		if(dailyType == null){
			dailyType = this.getDailyType();
			//for tight hole mgt and tight hole geo mgt report
			if ("MGTTH".equals(super.getReportType())) {
				dailyType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(thisOperation.getOperationUid());
			}
		}
		//this._allowedReportDaily = CommonUtil.getConfiguredInstance().getDailyForOperation(thisOperation.getOperationUid(), dailyType, sortOrder);
		return CommonUtil.getConfiguredInstance().getDailyForOperation(thisOperation.getOperationUid(), dailyType, sortOrder);
	}
	private Boolean isSelectedDayReportDailyAllowed(UserSession session, String dailyUid) throws Exception {
		if (dailyUid==null) return false;
		
		List<ReportDaily> list = this.getAllowedReportDaily(session);
		if (list==null) return false;
		if (list.size()==0) return false;
		for(ReportDaily rec:list){
			if (rec.getDailyUid().equals(dailyUid)) return true;
		}
		return false;
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("flexDdrReportLoadDayList".equals(invocationKey)) {
			List<ReportDaily> list = this.getAllowedReportDaily(UserSession.getInstance(request));
			if(list == null) return;
			if(list.size() == 0) return;
			
			SimpleDateFormat flexDateFormater =  new SimpleDateFormat("dd MMM yyyy");
			
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			for(ReportDaily rec:list){
				if (UserSession.getInstance(request).withinAccessScope(rec)){
					writer.startElement("Daily");
					writer.addElement("dailyUid", rec.getDailyUid());
					writer.addElement("dayDate", flexDateFormater.format(rec.getReportDatetime()));
					writer.addElement("dayNumber", rec.getReportNumber());
					writer.endElement();
				}
			}
			writer.endElement();
			writer.close();
			
			
		} else if ("getDDRReportFileList".equalsIgnoreCase(invocationKey)) { // get from getDDRReportFileList
			//String result = "<root/>";
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDatumConversionEnabled(false);
			String dailyUid = request.getParameter("dailyUid");
			String operationUid = request.getParameter("operationUid");
			String reportType = request.getParameter("reportType");
			
			if (StringUtils.isBlank(dailyUid) || StringUtils.isBlank(operationUid) || StringUtils.isBlank(reportType)){
				
			}
			try {
				//ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				SimpleXmlWriter writer = new SimpleXmlWriter(response);
				writer.startElement("root");
				
				// Populate DailyHoursUsedLog listing for loading in grid view
				String queryString = "SELECT b.reportFilesUid, b.isPrivate, b.displayName, b.reportUserUid, b.reportFile " +
									 "FROM ReportFiles b " +
									 "WHERE (b.isDeleted=false OR b.isDeleted is null) " +
									 "AND b.dailyUid = :dailyUid " +
									 "AND b.reportOperationUid = :operationUid " +
									 "AND b.reportType = :reportType ORDER BY b.dateGenerated";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"dailyUid", "operationUid", "reportType"}, new Object[] {dailyUid, operationUid, reportType});
				if (lstResult.size() > 0){
					for(Object objResult: lstResult){
						
						//due to result is in array, cast the result to array first and read 1 by 1
						Object[] obj = (Object[]) objResult;
						
						String reportFilesUid = "";
						String isPrivate = "";
						String displayName = "";
						String reportUserUid = "";
						String reportFile = "";
										
						if (obj[0] != null && StringUtils.isNotBlank(obj[0].toString())) reportFilesUid = obj[0].toString();
						if (obj[1] != null && StringUtils.isNotBlank(obj[1].toString())) isPrivate = obj[1].toString();
						if (obj[2] != null && StringUtils.isNotBlank(obj[2].toString())) displayName = obj[2].toString();
						if (obj[3] != null && StringUtils.isNotBlank(obj[3].toString())) reportUserUid = obj[3].toString();
						if (obj[4] != null && StringUtils.isNotBlank(obj[4].toString())) reportFile = obj[4].toString();
						
						File f = new File(ReportUtils.getFullOutputFilePath( reportFile ) );
						if( f.exists() ){
							this.timeReportFileCreated = new Date( f.lastModified());
							
							// Write the attributes into record
							SimpleAttributes attr = new SimpleAttributes();
							attr.addAttribute("reportFilesUid",reportFilesUid);
							attr.addAttribute("isPrivate", isPrivate);
							attr.addAttribute("displayName", displayName);
							attr.addAttribute("fileExtension", getFileExtension(reportFile));
							attr.addAttribute("reportUserUid", getUserName( reportUserUid ) );
							attr.addAttribute("newlyGeneratedFlag", String.valueOf( getNewlyGeneratedFlag() ) );
							attr.addAttribute("timeReportFileCreatedText", String.valueOf( getTimeReportFileCreatedText() ) );
												
							//write the element record into the DDRReportFile
							writer.addElement("DDRReportFile", "", attr);
						}					
					}
				}
				writer.endElement();
				writer.close();
				//result = bytes.toString();
				//return result;
			} catch (Exception e) {
				e.printStackTrace();
				//return "<root/>";
			}	
		} else {
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);
		}
	}
	
	//section to do generate multiple report base on user selection
	//get user selected day in flex object
	protected List<String> getSelectedDays(CommandBean commandBean) throws Exception {
		Object selected_days = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_DAYS);
		if(selected_days == null){
			return null;
		}
		if(StringUtils.isBlank(selected_days.toString())){
			return null;
		}
		
		String[] id_list = selected_days.toString().split(",");
		List<String> result = new ArrayList<String>();
		for(String id: id_list){
			result.add(id.trim());
		}
		return result;
	}
	
	protected List<String> getSelectedDaysForShowAllDays(CommandBean commandBean) throws Exception {
		List<String> result = new ArrayList<String>();
		for (CommandBeanTreeNode childNode : commandBean.getRoot().getList().get("ReportOutputFile").values()) {
			if (childNode.getAtts().getSelected()) {
				ReportOutputFile reportOutputFile = (ReportOutputFile) childNode.getData();
				result.add(reportOutputFile.getDailyUid());
			}
		}
		return result;
	}
	
	//override the report generating function, if got multiple day selected construct the List of UserSelectionSnapShot and generate report	
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		Boolean combinedDDR = "true".equals(commandBean.getRoot().getDynaAttr().get("includeCombinedDdr"));
		
		List<String> list = null;
		if (this.isShowAllDays()) {
			list = this.getSelectedDaysForShowAllDays(commandBean);
		} else {
			list = this.getSelectedDays(commandBean);
		}
		
		if (list != null && list.size() > 0) {
			if(combinedDDR && list.size() < 2){
				//system message
				commandBean.getSystemMessage().addError("Only 1 report selected therefore combine DDR is not generated.\nBut single report generation is still in progress.");
			}
			List<UserSelectionSnapshot> selections = new ArrayList<UserSelectionSnapshot>();
			boolean existingCombinedDailyUid = false;
			for(String dailyUid: list){
				boolean checkCombinedDailyUid = dailyUid.contains(":combined");
				if(checkCombinedDailyUid){
					existingCombinedDailyUid=true;
					break;
				}
				if (this.isSelectedDayReportDailyAllowed(session, dailyUid.toString())) {
					selections.add(UserSelectionSnapshot.getInstanceForCustomDaily(session, dailyUid.toString()));
				}
			}
			if (combinedDDR && existingCombinedDailyUid && selections.size() > 0){
				commandBean.getSystemMessage().addError("No data fit the selected criteria.\nBut other selected report(s) generation is still in progress.");
			}
			if(selections.size() < 1){
				commandBean.getSystemMessage().addError("No data fit the selected criteria therefore unable to generate report.");
				return null;
			}else{
				ReportJobParams rjp = super.submitReportJob(selections, this.getParametersForReportSubmission(session, request, commandBean));
				rjp.setJoinReport(combinedDDR);
				return null;
			}
		} else {
			if (this.isSelectedDayReportDailyAllowed(session, session.getCurrentDailyUid())) {
				if(combinedDDR){
					commandBean.getSystemMessage().addError("There is no report selected therefore combine DDR is not generated.\nBut single report generation is still in progress.");
				}
				return super.submitReportJob(UserSelectionSnapshot.getInstanceForCustomDaily(session, session.getCurrentDailyUid()), this.getParametersForReportSubmission(session, request, commandBean));		
			} else {
				commandBean.getSystemMessage().addError("No data fit the selected criteria therefore unable to generate report.");
				return null;
			}
		}
	}
	
	protected ReportJobParams submitReportJobWithEmail(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		List<String> list = null;
		if (this.isShowAllDays()) {
			list = this.getSelectedDaysForShowAllDays(commandBean);
		} else {
			list = this.getSelectedDays(commandBean);
		}
		
		if (list != null && list.size() > 0) {
			List<UserSelectionSnapshot> selections = new ArrayList<UserSelectionSnapshot>();
			for(String dailyUid: list){
				if (this.isSelectedDayReportDailyAllowed(session, dailyUid.toString())) {
					selections.add(UserSelectionSnapshot.getInstanceForCustomDaily(session, dailyUid.toString()));
				}
			}
			return super.submitReportJobWithEmail(selections, this.getParametersForReportSubmission(session, request, commandBean));
			
		} else {
			if (this.isSelectedDayReportDailyAllowed(session, session.getCurrentDailyUid())) {
				return super.submitReportJobWithEmail(UserSelectionSnapshot.getInstanceForCustomDaily(session, session.getCurrentDailyUid()), this.getParametersForReportSubmission(session, request, commandBean));		
			} else {
				commandBean.getSystemMessage().addError("No data fit the selected criteria therefore unable to generate report.");
				return null;
			}
		}
	}
	
	@Override
	public synchronized void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		super.onSubmitForServerSideProcess(commandBean, request, targetCommandBeanTreeNode);
		if(commandBean.getRoot().getDynaAttr().get("emailKey") != null) {
			String emailKey = commandBean.getRoot().getDynaAttr().get("emailKey").toString();
			if (!StringUtils.isBlank(emailKey)){
				String strSql = "FROM EmailList WHERE (isDeleted is null or isDeleted = false) AND emailListKey = :thisEmailKey";
				String[] paramsFields = {"thisEmailKey"};
				Object[] paramsValues = {emailKey};
				List<EmailList> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				if(lstResult.size() > 0) {
					for(EmailList el: lstResult) {
						String email = el.getEmailAddressList();
						commandBean.getRoot().getDynaAttr().put("emailAddress", email);
					}
				}		
			}else {
				commandBean.getRoot().getDynaAttr().remove("emailAddress");
			}
		}
	}

	@Override
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		super.afterDataNodeLoad(commandBean, meta, node, userSelection, request);
		
		Object object = node.getData();
		
		if ( object instanceof ReportOutputFile ){
			ReportOutputFile outputFile = ( ReportOutputFile ) object;
			if( outputFile.getNumberOfReport() > 1 ) node.getDynaAttr().put("isMultipleReportOutputFile", "true");
		}
		
	}

	@Override
	public void beforeDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		super.beforeDataNodeDelete(commandBean, node, session, status, request);
		
		Object object = node.getData();
		
		ApplicationUtils util = ApplicationUtils.getConfiguredInstance();
		
		if( object instanceof ReportOutputFile ){
			ReportOutputFile outputFile = ( ReportOutputFile ) object;
			if( outputFile.getNumberOfReport() > 1 ) {
				String strSql = "FROM ReportFiles WHERE (isDeleted=false OR isDeleted is null) " +
								 "AND dailyUid = :dailyUid " +
								 "AND reportOperationUid = :operationUid " +
								 "AND reportType = :reportType " +
								 "AND reportFilesUid != :reportFilesUid";
				String[] paramsFields1 = {"dailyUid", "operationUid", "reportType", "reportFilesUid"};
				Object[] paramsValues1 = {outputFile.getDailyUid(), outputFile.getOperationUid(), outputFile.getReportType(), outputFile.getReportFilesUid()};
				List<ReportFiles> reportFilesList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam( strSql, paramsFields1, paramsValues1 );
			
				if( reportFilesList.size() > 0 ){
					for( ReportFiles reportFile : reportFilesList ){
						File file = this.getReportOutputFile(reportFile);
						if ( file.exists() ){
							ReportOutputFile output_file = null;
							Daily daily = util.getCachedDaily(reportFile.getDailyUid());
							
							Operation operation = (reportFile.getReportOperationUid() != null ? util.getCachedOperation(reportFile.getReportOperationUid()) : null);
							String display_name = this.getReportDisplayName(reportFile, operation, new UserSelectionSnapshot(session));
							output_file = this.onLoadReportOutputFile(new ReportOutputFile(reportFile, operation, daily, display_name, file));
							this.delete( output_file );
						}
					}
				}
			}
		}		
	}
	
	private void delete(ReportOutputFile report) throws Exception {
		String reportFilePath = report.getReportFilePath();
		if (StringUtils.isNotBlank(reportFilePath)) {
			File reportFile = new File(ReportUtils.getFullOutputFilePath(report.getReportFilePath()));
			if(reportFile.exists()) reportFile.delete();
		}
		
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update ReportFiles set isDeleted = true where reportFilesUid = :reportFilesUid", new String[] { "reportFilesUid" }, new Object[] { report.getReportFilesUid() });
	}	
	
	private String getFileExtension(String file){
		if(file == null) return null;
		int i = file.lastIndexOf(".");
		if(i != -1){
			return file.substring(i + 1);
		}else{
			return null;
		}
	}

	@Override
	protected void sendEmailAfterReportGenerated(ReportAutoEmail autoEmail, ReportFiles reportFile, JobContext jobContext, Map emailContentMapping, String paperSize) throws Exception {
		String wellName ="";
		String reportDate = "";
		String reportNo = "";
		String rigName = "";
		String emailSubject = autoEmail.getEmailSubject();
		String wellOpsName = "";
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, jobContext.getUserContext().getUserSelection().getOperationUid());
		String operationName = operation.getOperationName();
		Well thisWell = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, jobContext.getUserContext().getUserSelection().getWellUid());
		if (thisWell!=null) wellName = thisWell.getWellName();
		RigInformation thisRig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, jobContext.getUserContext().getUserSelection().getRigInformationUid());
		if (thisRig!=null) rigName = thisRig.getRigName();
		wellOpsName = CommonUtil.getConfiguredInstance().getCompleteOperationName(operation.getGroupUid(), operation.getOperationUid());
		String dailyUid = jobContext.getUserContext().getUserSelection().getDailyUid();
		if(dailyUid != null){
			Daily thisDaily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
			if(thisDaily!=null){
				if (jobContext.getUserContext().getUserSelection().getOperationType()!=null){										
					ReportDaily thisReportDaily = (ReportDaily) ApplicationUtils.getConfiguredInstance().getReportDailyByOperationType(dailyUid, jobContext.getUserContext().getUserSelection().getOperationType());
										
					if (thisReportDaily!=null) {
						reportDate = new SimpleDateFormat("dd-MMM-yyyy").format(thisReportDaily.getReportDatetime());
						reportNo = thisReportDaily.getReportNumber();
					}
				}	
			}
		}
	
		if (StringUtils.isBlank(emailSubject)) {
			ReportTypes report_type_record = ReportUtils.getConfiguredInstance().getReportType(jobContext.getUserContext().getUserSelection().getGroupUid(), reportFile.getReportType(), paperSize, autoEmail.getLanguage(), "", "");
			if(report_type_record!=null){
				emailSubject = report_type_record.getEmailSubject();
				if(StringUtils.isBlank(emailSubject)){
					emailSubject = "IDS Report";
				}
			}
		}
		if(emailSubject.contains("<REPORT DATE>")){
			emailSubject = emailSubject.replace("<REPORT DATE>", reportDate);
		}
		if(emailSubject.contains("<WELL NAME>")){
			emailSubject = emailSubject.replace("<WELL NAME>", wellName);						
		}
		if(emailSubject.contains("<REPORT NO>")){
			emailSubject = emailSubject.replace("<REPORT NO>", reportNo);	
		}
		if(emailSubject.contains("<OPERATION NAME>")){
			emailSubject = emailSubject.replace("<OPERATION NAME>", operationName);	
		}
		if(emailSubject.contains("<RIG>")){
			emailSubject = emailSubject.replace("<RIG>", rigName);	
		}
		if(emailSubject.contains("<WELLOPS>")){
			emailSubject = emailSubject.replace("<WELLOPS>", wellOpsName);	
		}

		autoEmail.setEmailSubject(emailSubject);
		emailContentMapping.put("operationName", operationName);
		emailContentMapping.put("reportNo", reportNo);
	
		
		super.sendEmailAfterReportGenerated(autoEmail, reportFile, jobContext, emailContentMapping, paperSize);
	}
}
