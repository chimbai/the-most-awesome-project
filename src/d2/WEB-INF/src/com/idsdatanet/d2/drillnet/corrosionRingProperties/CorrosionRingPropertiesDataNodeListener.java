package com.idsdatanet.d2.drillnet.corrosionRingProperties;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.CorrosionRingProperties;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CorrosionRingPropertiesDataNodeListener extends EmptyDataNodeListener{
			
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();		
		
		if(object instanceof CorrosionRingProperties) {
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CorrosionRingProperties.class, "exposureDuration");
	
			// Auto - calculate exposure duration (hrs)
			// Get install time & remove time
			Date start = (Date) PropertyUtils.getProperty(object, "installDateTime");
			Date end = (Date) PropertyUtils.getProperty(object, "removeDateTime");
			
			// check if install time greater than remove time.
			if(start != null && end != null){
				if(start.getTime() > end.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "installDateTime", "Date/Time In cannot be greater than Date/Time Out.");
					return;
				}
				
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
							
				thisConverter.setReferenceMappingField(CorrosionRingProperties.class, "exposureDuration");
				thisConverter.setBaseValue(thisDuration);				
				PropertyUtils.setProperty(object, "exposureDuration", (thisConverter.getConvertedValue()));
			}else{
				// set exposure = 0 if installDatetime or removeDatetime is null
				Double thisDuration = 0.00;				
				PropertyUtils.setProperty(object, "exposureDuration", thisDuration);				
			}			
			
			// Auto-calculated weight loss(g)	
			CorrosionRingProperties corrosionRing=(CorrosionRingProperties) object;			
			Double initialWeight = corrosionRing.getInitialWeight();
			Double finalWeight = corrosionRing.getFinalWeight();
			Double weightLoss = 0.00;
			
			if (initialWeight!=null && finalWeight!=null){
				//call calcWeightLoss method to do calculation
				weightLoss = CorrosionRingPropertiesUtil.calcWeightLoss(initialWeight, finalWeight, thisConverter);	
				
				thisConverter.setReferenceMappingField(CorrosionRingProperties.class, "weightLoss");
				thisConverter.setBaseValue(weightLoss);
				PropertyUtils.setProperty(object, "weightLoss", (thisConverter.getConvertedValue()));				
			}else{
				// set default weightLoss = 0 if initialWeight or finalWeight is null				
				PropertyUtils.setProperty(object, "weightLoss", weightLoss);	
			}
		}
	}	
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
				
		if(obj instanceof CorrosionRingProperties) {
			Double corrosionRate = null;
			CustomFieldUom thisConverter=new CustomFieldUom(commandBean,CorrosionRingProperties.class, "exposureDuration");
			CorrosionRingProperties corrosionRing=(CorrosionRingProperties) node.getData();
			
			//only if kFactor & exposure hrs is not null then calculate for corrosion rate
			if (corrosionRing.getkFactor() != null && corrosionRing.getExposureDuration()!= 0){
				Double exposureDuration = corrosionRing.getExposureDuration();
				Double kFactor = corrosionRing.getkFactor();
				Double weightLoss = corrosionRing.getWeightLoss();
				
				//auto-calculate corrosion rate
				corrosionRate = CorrosionRingPropertiesUtil.calcCorrosionRate(exposureDuration, kFactor, weightLoss, thisConverter);
						
				thisConverter.setReferenceMappingField(CorrosionRingProperties.class, "corrosionRate");
				thisConverter.setBaseValue(corrosionRate);
				corrosionRing.setCorrosionRate(thisConverter.getConvertedValue());				
			}else{			
				corrosionRing.setCorrosionRate(null);
			}
			//save object after changes had been set
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(corrosionRing);
		}
	}
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		Object obj = node.getData();
		
		if(obj instanceof CorrosionRingProperties) {
			CorrosionRingProperties corrosionRing=(CorrosionRingProperties) node.getData();
			
			String cmbMudType = "";
			cmbMudType = CorrosionRingPropertiesUtil.populateMudType(corrosionRing, userSelection, qp);
			//allocate concatenated mud type string into dynamic attribute
			node.getDynaAttr().put("mudType", cmbMudType);			
		}
	}

		
}
