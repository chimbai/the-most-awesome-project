package com.idsdatanet.d2.drillnet.rigpump;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.RigPump;
import com.idsdatanet.d2.core.model.RigPumpParam;
import com.idsdatanet.d2.core.model.RigSlowPumpParam;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class RigPumpParamDataNodeListener extends EmptyDataNodeListener {
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		// ticket #20100715-0843-mlley: calculate Pump Flow Rate if the GWP Settings has been turned ON
		Object object = node.getData();		
		if (object instanceof RigPumpParam) {
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "calcPumpFlowRate"))) {
				CommonUtil.getConfiguredInstance().calculatePumpFlowRate(commandBean, object);
			}
		}
		if (object instanceof RigSlowPumpParam) {
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "pumpflowrateslowreading_calculation"))) {
				CommonUtil.getConfiguredInstance().calculatePumpFlowRate(commandBean, object);
			}
		}
		// end	
	}
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof RigPumpParam){
				
			//Validation Code for Installed Date > Remove Date
			RigPumpParam thisRigPumpParam = (RigPumpParam) object;
			
			if(thisRigPumpParam.getInstallDatetime()!= null && thisRigPumpParam.getRemoveDatetime()!= null){
				if(thisRigPumpParam.getInstallDatetime().getTime() > thisRigPumpParam.getRemoveDatetime().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "removeDatetime", "Remove Date cannot be earlier than Install Date.");
					return;
				}
			}
		}	
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof RigPumpParam) {
			RigPumpParam rigPumpParam = (RigPumpParam) object;
			
			if (rigPumpParam.getRigPumpUid()!=null){
				RigPump rigPump = (RigPump) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigPump.class, rigPumpParam.getRigPumpUid());
				
				if (rigPump!=null){
					node.getDynaAttr().put("unitNumber", rigPump.getUnitNumber());
					node.getDynaAttr().put("unitModel", rigPump.getUnitModel());
				}
			}
		}
	}
}
