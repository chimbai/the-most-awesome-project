package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class RigInformationBasedOperationLookupHandler implements LookupHandler {	
		
	private List<String> rigType;
	private String completion;
	
	public void setRigType(List<String> rigType)
	{
		if(rigType != null){
			this.rigType = new ArrayList<String>();
			for(String value: rigType){
				this.rigType.add(value.trim().toUpperCase());
			}
		}
	}
	
	public void setCompletion(String completion)
	{
		this.completion = completion;
	}
	
	public String getCompletion() {
		return this.completion;
	}
	
	public List<String> getRigType() {
		return this.rigType;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		
		String strInCondition = "";
		String exclude = "";
		
		if(this.getRigType() != null)
		{
			for(String value:this.rigType)
			{
				strInCondition = strInCondition + "'" + value.toString() + "',";
			}
			if (this.getCompletion()!=null)
			{
				exclude = " " + this.getCompletion().toString();
			}
			strInCondition = StringUtils.substring(strInCondition, 0, -1);
			strInCondition = " and rigSubType" + exclude + " IN (" + StringUtils.upperCase(strInCondition) + ") ORDER BY rigName";							
		}
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		
		if (userSelection.getOperationType()!=null)
		{
			String sql = "select typeWellFilter, rigInformationUid, rigName from RigInformation where (isDeleted = false or isDeleted is null) and groupUid=:groupUid" + strInCondition;
			
			List<Object []> rigInformationResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "groupUid", userSelection.getGroupUid());
			if (rigInformationResults.size() > 0) {
				for (Object[] rigInformationResult : rigInformationResults) {
					if (rigInformationResult[0] != null) {
						String [] temp = rigInformationResult[0].toString().split("\t");
						 for (int i = 0 ; i < temp.length ; i++) {
						       if (temp[i].toString().trim().equals(userSelection.getOperationType().toString()))
						       {				    	 
						    	   result.put(rigInformationResult[1].toString(), new LookupItem(rigInformationResult[1].toString(), rigInformationResult[2].toString()));
						       }
						 }					
					}
				}
			}
		}
		return result;
	}
}

