package com.idsdatanet.d2.drillnet.reportDaily;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.idsdatanet.d2.core.web.mvc.AjaxAutoPopulateListener;

public class ReportDailyAjaxAutoPopulateListener implements AjaxAutoPopulateListener {
	
	public void populate(String d2ClassName, String tiggeredD2FieldName, Map target) throws Exception {
		if (d2ClassName.equals("ReportDaily")) {
			target.put("daydate", new SimpleDateFormat("dd MMM yyyy").format(new Date()));
			
			// TODO
			target.put("depthMdMsl", "");
			target.put("depthTvdMsl", "");
		}
	}

}
