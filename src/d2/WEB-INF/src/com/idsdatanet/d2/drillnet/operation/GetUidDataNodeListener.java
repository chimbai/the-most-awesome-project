package com.idsdatanet.d2.drillnet.operation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class GetUidDataNodeListener extends EmptyDataNodeListener {
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String dailyUid="";
		String wellUid="";
		String wellboreUid="";
		wellUid=userSelection.getWellUid();
		wellboreUid=userSelection.getWellboreUid();
		dailyUid=userSelection.getDailyUid();
		String daynumber="";
		
		String strSql = "SELECT wellName from Well WHERE (isDeleted = false or isDeleted is null) AND wellUid = :thiswellUid";
		String[] paramsFields = {"thiswellUid"};
		Object[] paramsValues = {wellUid};
		
		String strSql1 = "SELECT wellboreName from Wellbore WHERE (isDeleted = false or isDeleted is null) AND wellboreUid = :thiswellboreUid";
		String[] paramsFields1 = {"thiswellboreUid"};
		Object[] paramsValues1 = {wellboreUid};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			Object wellname = (Object) lstResult.get(0);
			
			if (wellname != null && StringUtils.isNotBlank(wellname.toString()))
			{
				node.getDynaAttr().put("wellName",wellname.toString());
			}
		}
		
		List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
		if (lstResult1.size() > 0){
			Object wellborename = (Object) lstResult1.get(0);
			
			if (wellborename != null && StringUtils.isNotBlank(wellborename.toString()))
			{
				node.getDynaAttr().put("wellboreName",wellborename.toString());
			}
		}
		List <Daily> lstDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM Daily WHERE (isDeleted = false or isDeleted is null) and dailyUid = '" + dailyUid + "'");
		if (lstDaily.size() > 0){
			for(Daily objDaily: lstDaily){
				if (objDaily.getDayDate() != null && StringUtils.isNotBlank(objDaily.getDayDate().toString()))
				{
					Date dailydate=null;
					dailydate=objDaily.getDayDate();
					DateFormat df=new SimpleDateFormat("dd MMMM yyyy");
					String daydate="";
					daydate = "(" +	df.format(dailydate) + ")";
					node.getDynaAttr().put("dayDate",daydate.toString());	
				}
				if (objDaily.getDayNumber() != null && StringUtils.isNotBlank(objDaily.getDayNumber().toString()))
				{
					daynumber=objDaily.getDayNumber().toString();
					node.getDynaAttr().put("dayNumber",daynumber.toString());	
				}
			}
				
					
				
			}
			
		
		node.getDynaAttr().put("wellUid",wellUid);
		node.getDynaAttr().put("wellboreUid",wellboreUid);
		node.getDynaAttr().put("dailyUid",dailyUid);
	}

	
}