package com.idsdatanet.d2.drillnet.bop;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.well.Constant;

public class FullOperationNameLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		String strSql = "select w.wellName, wb.wellboreName, o.operationName, wb.wellboreType, o.operationCode, o.operationUid " +
				"FROM Operation o, Well w, Wellbore wb " +
				"WHERE (o.isDeleted = false or o.isDeleted is null) " +
				"AND (w.isDeleted = false or w.isDeleted is null) " +
				"AND (wb.isDeleted = false or wb.isDeleted is null) " +
				"AND w.wellUid = wb.wellUid " +
				"AND wb.wellboreUid = o.wellboreUid";
		List<Object[]> results = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
		String finalName = "";
		
		if(results.size() > 0)  {
			for(Object[] param : results){
				String opName = "";
				String wellName = "";
				String wbName = "";
				String wellboreType = "";
				String operationCode = "";
				String operationUid = "";
				
				if (param[0] != null) wellName = param[0].toString();
				if (param[1] != null) wbName = param[1].toString();			
				if (param[2] != null) opName = param[2].toString();
				if (param[3] != null) wellboreType = param[3].toString();
				if (param[4] != null) operationCode = param[4].toString();
				if (param[5] != null) operationUid = param[5].toString();
				
				//check if GWP skip wellbore name when initial drilling is set on or not
				if ("0".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "ShowWellboreNameForInitialHole"))){
					if (StringUtils.isNotBlank(wellboreType) && "INITIAL".equalsIgnoreCase(wellboreType)){
						wbName = "";					
					}
				}
				
				if ("0".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "ShowOperationNameForDrilling"))){
					if (StringUtils.isNotBlank(operationCode) && operationCode.startsWith(Constant.DRLLG)){
						opName =  "";
					}
				}
				
				finalName = opName.trim();
				if (! opName.equals(wbName)) finalName = wbName.trim() + " >> " + finalName;
				if (! wbName.equals(wellName)) finalName = wellName.trim() + " >> " + finalName;	
				
				result.put(operationUid, new LookupItem(operationUid, finalName));
			}
		}
		
		return result;
	}

}
