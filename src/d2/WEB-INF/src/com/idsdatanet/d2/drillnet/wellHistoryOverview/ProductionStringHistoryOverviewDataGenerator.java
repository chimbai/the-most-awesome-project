package com.idsdatanet.d2.drillnet.wellHistoryOverview;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.ProductionStringDetail;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class ProductionStringHistoryOverviewDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
						
		Map<String,LookupItem> componentTypeLookup= LookupManager.getConfiguredInstance().getLookup("xml://productionString.component_type?key=code&amp;value=label", userContext.getUserSelection(), null);
		
		/* String sql = "select a.productionStringUid,a.wellUid,a.wellboreUid,a.operationUid,a.stringNo,b.operationName from ProductionString a,Operation b where a.operationUid = b.operationUid and (a.isDeleted is null or a.isDeleted = false) " +
					 "and (b.isDeleted is null or b.isDeleted = false) and a.wellboreUid =:wellboreUid order by a.installDateTime desc,b.sysOperationStartDatetime desc";
		*/
		
		String sql = "select a.productionStringUid,a.wellUid,a.wellboreUid,a.operationUid,a.stringNo,b.operationName from ProductionString a,Operation b where a.operationUid = b.operationUid and (a.isDeleted is null or a.isDeleted = false) " +
		 "and (b.isDeleted is null or b.isDeleted = false) and a.wellboreUid =:wellboreUid order by a.installDateTime desc";
		
		List<Object[]> lstData  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql ,"wellboreUid", userContext.getUserSelection().getWellboreUid());
		
		if (lstData.size()>0){
			
			ReportDataNode currentDataNode = null;
			
			ReportDataNode ProductionStringNode = reportDataNode.addChild("ProductionString");
			ReportDataNode ProductionStringDetailNode = reportDataNode.addChild("ProductionStringDetail");
			
			for(Object[] item : lstData){
				
				String productionStringUid = item[0].toString();				
				
				currentDataNode = ProductionStringNode.addChild("DataInfo");
				currentDataNode.addProperty("productionStringUid", productionStringUid);
				currentDataNode.addProperty("wellUid", item[1].toString());
				currentDataNode.addProperty("wellboreUid", item[2].toString());
				currentDataNode.addProperty("operationUid", item[3].toString());
				currentDataNode.addProperty("stringNo", item[4].toString());
				currentDataNode.addProperty("operationName", item[5].toString());
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ProductionStringDetail WHERE productionStringUid=:productionStringUid order by depthMdMsl ASC", "productionStringUid", productionStringUid);
				
				
				CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
				
				for(Object data : lstResult){
										
					ProductionStringDetail productionStringDetail = (ProductionStringDetail) data;
					
					currentDataNode = ProductionStringDetailNode.addChild("DataInfo");	
					currentDataNode.addProperty("productionStringUid", productionStringDetail.getProductionStringUid());	
					currentDataNode.addProperty("productionStringDetailUid", productionStringDetail.getProductionStringDetailUid());	
					currentDataNode.addProperty("componentType",  getLookupValue(componentTypeLookup,productionStringDetail.getComponentType()));
					currentDataNode.addProperty("description", productionStringDetail.getDescription());
					if (productionStringDetail.getSequence()!=null) currentDataNode.addProperty("sequence", productionStringDetail.getSequence().toString());
					currentDataNode.addProperty("remark", productionStringDetail.getRemark());			
					
					generateUomField(currentDataNode,thisConverter,"depthMdMsl", productionStringDetail.getDepthMdMsl(),true);
					generateUomField(currentDataNode,thisConverter,"depthTvdMsl", productionStringDetail.getDepthTvdMsl(),true);
					generateUomField(currentDataNode,thisConverter,"bottomDepthMdMsl", productionStringDetail.getBottomDepthMdMsl(),true);
					generateUomField(currentDataNode,thisConverter,"totalLength", productionStringDetail.getTotalLength(),false);
					generateUomField(currentDataNode,thisConverter,"sizeOd", productionStringDetail.getSizeOd(),false);
					generateUomField(currentDataNode,thisConverter,"sizeId", productionStringDetail.getSizeId(),false);
				
					if (productionStringDetail.getServiceDateTime()!=null) currentDataNode.addProperty("serviceDateTime",sdf.format(productionStringDetail.getServiceDateTime()));
					
				}
				break;
			}
		}
	}
	
	private void generateUomField(ReportDataNode currentDataNode,CustomFieldUom thisConverter,String DataField, Object Data,boolean isDatumField ) throws Exception{
	
		String rawValue = "";
		String dataUomSymbol = "";
		String formattedvalue = "";
		
		thisConverter.setReferenceMappingField(ProductionStringDetail.class, DataField);
		if (thisConverter.isUOMMappingAvailable()) dataUomSymbol = thisConverter.getUomSymbol().toString();
		
		if (Data != null)
		{
			if (thisConverter.isUOMMappingAvailable()){
				thisConverter.setBaseValueFromUserValue(Double.parseDouble(Data.toString()));
				if (isDatumField) thisConverter.addDatumOffset();
				formattedvalue = thisConverter.getFormattedValue();	
				rawValue = formattedvalue.replace(dataUomSymbol, "").trim() ;
			}
			else{
				rawValue = Data.toString();
			}			
		}				
		
		currentDataNode.addProperty(DataField, rawValue);
		currentDataNode.addProperty(DataField + "UomSymbol", dataUomSymbol);
		
	}
	private String getLookupValue(Map<String,LookupItem> lookupList, Object lookupvalue) throws Exception {
		String retValue = "";
		
		if (lookupvalue !=null && StringUtils.isNotBlank(lookupvalue.toString())  && lookupList !=null){
			 LookupItem lookup = lookupList.get(lookupvalue.toString());
			if (lookup !=null)	retValue = lookup.getValue().toString();					
		} 
		
		return retValue;
	}

}
