package com.idsdatanet.d2.drillnet.wellmerge;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WellboreCascadeLookupHandler implements CascadeLookupHandler {

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		return null;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		List<Well> wellList = WellMergeUtils.getWellList();
		if (wellList != null) {
			List<CascadeLookupSet> results = new ArrayList<CascadeLookupSet>();
			for (Well well : wellList) {
				String wellUid = well.getWellUid();
				CascadeLookupSet cls = new CascadeLookupSet(wellUid);
				LinkedHashMap<String, LookupItem> cascadeLookup = this.getCascadeLookup(commandBean, null, userSelection, request, wellUid, null);
				if(cascadeLookup != null) {
					cls.setLookup(cascadeLookup);
					results.add(cls);
				}
				results.add(cls);
			}
			CascadeLookupSet[] cascadeLookups = new CascadeLookupSet[results.size()];
			results.toArray(cascadeLookups);
			return cascadeLookups;
		}
		return null;
	}

	public LinkedHashMap<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		List<Wellbore> wellboreList = WellMergeUtils.getWellboreList(key);
		if (wellboreList != null) {
			LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
			for(Wellbore wellbore : wellboreList) {
				String wellboreUid = wellbore.getWellboreUid();
				String wellboreName = wellbore.getWellboreName();
				wellboreName += " [" + wellboreUid + "]";
				LookupItem item = new LookupItem(wellboreUid, wellboreName);
				result.put(wellboreUid, item);
			}
			return result;
		}
		return null;
	}

}
