package com.idsdatanet.d2.drillnet.report;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.BhaComponent;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.CementJob;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.MudVolumeDetails;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


public class MpmReportDataGenerator implements ReportDataGenerator {
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		// get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String currentWellboreUid = userContext.getUserSelection().getWellboreUid();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		
		ReportDataNode thisReportNode = reportDataNode.addChild("mudLoss");
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), MudVolumeDetails.class, "volume");	
		
		if(daily!=null && StringUtils.isNotBlank(currentOperationUid)){
			
			Double cummSurface = getCummMudLost(currentOperationUid,todayDate,thisConverter,"'loss_surface','loss_equipment','loss_degasser','loss_desander','loss_desilter','loss_centrifuge'");
			Double cummDownhole = getCummMudLost(currentOperationUid,todayDate,thisConverter,"'loss_dh'");
			Double cummMudLoss = cummDownhole + cummSurface;
			
			thisReportNode.addProperty("cumSurface", cummSurface.toString());
			thisReportNode.addProperty("cumDownhole", cummDownhole.toString());
			thisReportNode.addProperty("cumMudLoss", cummMudLoss.toString());
		}
		
		
		if(daily.getDailyUid()!=null){
			String sql = "FROM DrillingParameters dp, BharunDailySummary d " +
					"WHERE (d.isDeleted = false or d.isDeleted is null) " +
					"AND (dp.isDeleted = false or dp.isDeleted is null) " +
					"AND dp.dailyUid=d.dailyUid " +
					"AND d.dailyUid=:dailyUid " ;		
			String[] paramsFields = {"dailyUid"};
			Object[] paramsValues = {daily.getDailyUid()};
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
			if (list.size()>0) {
				
				for(Object[] objResult: list){
					BharunDailySummary bharunDailySummary = (BharunDailySummary) objResult[1]; 
					
					if(bharunDailySummary!=null){
						thisReportNode = reportDataNode.addChild("drillParameters");
						if (bharunDailySummary.getBharunUid()!=null) thisReportNode.addProperty("bharunUid", bharunDailySummary.getBharunUid().toString());
						if (bharunDailySummary.getWeightPickup()!=null) thisReportNode.addProperty("pickupWeight", bharunDailySummary.getWeightPickup().toString());
						if (bharunDailySummary.getWeightSlackOff()!=null) thisReportNode.addProperty("slackoffWeight", bharunDailySummary.getWeightSlackOff().toString());					
					}
					
				}		
			}
		}
		
		
		if(currentOperationUid!=null){
			String sql = "FROM Bharun bha, Bitrun bit " +
					"WHERE (bha.isDeleted = false or bha.isDeleted is null) " +
					"AND (bit.isDeleted = false or bit.isDeleted is null) " +
					"AND bha.bharunUid=bit.bharunUid " +
					"AND bha.operationUid=:operationUid " ;		
			String[] paramsFields = {"operationUid"};
			Object[] paramsValues = {currentOperationUid};
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
			if (list.size()>0) {
				for(Object[] objResult: list){
					Bharun bharun = (Bharun) objResult[0]; 
					
					if(bharun!=null){
						thisReportNode = reportDataNode.addChild("bitrun");
						if (bharun.getBharunUid()!=null) thisReportNode.addProperty("bharunUid", bharun.getBharunUid().toString());
						if (bharun.getBhaRunNumber()!=null) thisReportNode.addProperty("bhaRunNumber", bharun.getBhaRunNumber().toString());
						if (bharun.getDepthInMdMsl()!=null) thisReportNode.addProperty("depthInMdMsl", bharun.getDepthInMdMsl().toString());
						if (bharun.getDepthOutMdMsl()!=null) thisReportNode.addProperty("depthOutMdMsl", bharun.getDepthOutMdMsl().toString());
					}
					
					Bitrun bitrun = (Bitrun) objResult[1]; 
					if(bitrun!=null){
						
						String sql1 = "FROM BitNozzle b, Bitrun bit " +
								"WHERE (b.isDeleted = false or b.isDeleted is null) " +
								"AND (bit.isDeleted = false or bit.isDeleted is null) " +
								"AND b.bitrunUid=bit.bitrunUid " +
								"AND bit.bitrunUid=:bitrunUid " ;		
						String[] paramsFields1 = {"bitrunUid"};
						Object[] paramsValues1 = {bitrun.getBitrunUid()};
						List<Object[]> list1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql1, paramsFields1, paramsValues1, qp);
						if (list1.size()>0) {
							String bitNozzles ="";
							int count = 0;
							thisReportNode = reportDataNode.addChild("bitNozzle");
							
							for(Object[] objResult1: list1){
								BitNozzle bitNozzle = (BitNozzle) objResult1[0]; 	
								String nozzleQty = "";
								String nozzleSize = "";
								
								if (bitNozzle!=null){
									
									if (bitNozzle.getNozzleSize()!=null) {
										thisConverter.setReferenceMappingField(BitNozzle.class, "nozzleSize");
										thisConverter.setBaseValue(bitNozzle.getNozzleSize());										
										Double size = thisConverter.getConvertedValue();
										
										nozzleSize = thisConverter.formatOutputPrecision(size).toString();
									}
									
									if (bitNozzle.getNozzleQty()!=null) {
										nozzleQty = bitNozzle.getNozzleQty().toString();
									}
									
									if (count>0) bitNozzles = bitNozzles + ";" ;	
									bitNozzles = bitNozzles + nozzleQty + " * " + nozzleSize ;
																						
									count++;
								}
														
							}
							thisReportNode.addProperty("bitrunUid", bitrun.getBitrunUid().toString());
							thisReportNode.addProperty("bitNozzles", bitNozzles.toString());
							
						}
						
					}
					
				}
			}
		}
		
		thisReportNode = reportDataNode.addChild("holeSection");
		Integer noHoleSection = getTotalHoleSection(currentWellboreUid,todayDate);
		thisReportNode.addProperty("noHoleSection", (noHoleSection>0)?noHoleSection.toString():"");
		
		Date holeSectionStartDate = null;
		Date holeSectionEndDate = null;
		String sectionName = "";
		Double depthFromMdMsl = 0.0;
		Double depthToMdMsl = 0.0;
		String sectionObjectives = "";
		
		Calendar thisCalender = Calendar.getInstance();
		thisCalender.setTime(todayDate);
		thisCalender.add(Calendar.DATE, 1);
		thisCalender.add(Calendar.SECOND, -1);
		Date nextDate = thisCalender.getTime();
		
		String strSql = "SELECT a.dateTimeIn, a.dateTimeOut, a.sectionName, a.depthFromMdMsl, a.depthToMdMsl, a.sectionObjectives FROM HoleSection a " +
				"WHERE (a.isDeleted = false or a.isDeleted is null) " +
				"AND a.dateTimeIn <= :reportingDate AND (a.dateTimeOut >= :reportingDate OR a.dateTimeOut IS NULL) AND a.wellboreUid=:wellboreUid " ;
		String[] paramsFields = {"reportingDate" ,"wellboreUid"};
		Object[] paramsValues = {nextDate, currentWellboreUid};
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult!=null && lstResult.size() > 0){
			for(Object[] thisResult: lstResult){
				if (thisResult[0]!=null) holeSectionStartDate = (Date) thisResult[0];
				if (thisResult[1]!=null) holeSectionEndDate = (Date) thisResult[1];
				if (thisResult[2]!=null) sectionName = (String) thisResult[2];
				if (thisResult[3]!=null) depthFromMdMsl = (Double) thisResult[3];
				if (thisResult[4]!=null) depthToMdMsl = (Double) thisResult[4];
				if (thisResult[5]!=null) sectionObjectives = (String) thisResult[5];
				break;
			}
		}
		
		thisReportNode.addProperty("holeSectionStartDate", (holeSectionStartDate!=null)?holeSectionStartDate.toString():"");
		thisReportNode.addProperty("holeSectionEndDate", (holeSectionEndDate!=null)?holeSectionEndDate.toString():"");
		thisReportNode.addProperty("sectionName", (sectionName!=null)?sectionName.toString():"");
		thisReportNode.addProperty("depthFromMdMsl", (depthFromMdMsl!=null)?depthFromMdMsl.toString():"");
		thisReportNode.addProperty("depthToMdMsl", (depthToMdMsl!=null)?depthToMdMsl.toString():"");
		thisReportNode.addProperty("sectionObjectives", (sectionObjectives!=null)?sectionObjectives.toString():"");
		
		thisReportNode = reportDataNode.addChild("CementJob");
		String cementJobSectionName ="";
		Integer count = 0;
		
		strSql = "SELECT casingSectionUid, openHoleDepthMdMsl FROM CasingSection " +
				"WHERE (isDeleted = false or isDeleted is null) " +
				"AND wellboreUid=:wellboreUid AND daterun=:todayDate " ;
		String[] paramsField = {"wellboreUid", "todayDate"};
		Object[] paramsValue = {currentWellboreUid, todayDate};
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		
		if (lstResult!=null && lstResult.size() > 0){
			for(Object[] casings: lstResult){
				if (casings[0]!=null && casings[1]!=null) {
					String uid = this.getCementSectionName((String)casings[0], (Double)casings[1]);
					
					if (uid!=null)	{
						cementJobSectionName = uid;
						count++;
					}
				}
				
			}
		}
		
		if (count>1){
			thisReportNode.addProperty("cementJobSectionName", "");
		}else{
			thisReportNode.addProperty("cementJobSectionName", cementJobSectionName);
		}
		
		this.getBharunHoleSize(currentOperationUid, reportDataNode, thisReportNode, daily.getDailyUid());
		this.getCostDetails(daily.getDailyUid(),currentOperationUid, reportDataNode, thisReportNode);
	}
	
	
	public Double getCummMudLost(String currentOperationUid, Date todayDate, CustomFieldUom thisConverter, String label) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		Double totalVolume = 0.00;
		
		String strSql = "SELECT SUM(b.volume) as totalLoss FROM MudVolumes a, MudVolumeDetails b, Daily d " +
				"WHERE (a.isDeleted = false or a.isDeleted is null) AND (b.isDeleted = false or b.isDeleted is null) AND (d.isDeleted = false or d.isDeleted is null) " +
				"AND b.label IN (" + label + ") " +
				"AND a.mudVolumesUid=b.mudVolumesUid AND d.dailyUid = a.dailyUid AND d.dayDate <= :userDate AND a.operationUid=:operationUid " ;
		String[] paramsFields = {"userDate" ,"operationUid"};
		Object[] paramsValues = {todayDate, currentOperationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult!=null && lstResult.size() > 0){			
			Object thisResult = (Object) lstResult.get(0);	
			
			if (thisResult != null) {
				totalVolume = Double.parseDouble(thisResult.toString());
				thisConverter.setReferenceMappingField(MudVolumeDetails.class, "volume");
				thisConverter.setBaseValue(totalVolume);
				totalVolume = thisConverter.getConvertedValue();						
			}				
		}
		
		return totalVolume;
	}
	
	public Integer getTotalHoleSection(String currentWellboreUid, Date todayDate) throws Exception{
		Calendar thisCalender = Calendar.getInstance();
		thisCalender.setTime(todayDate);
		thisCalender.add(Calendar.DATE, 1);
		thisCalender.add(Calendar.SECOND, -1);
		Date nextDate = thisCalender.getTime();
		
		Integer total = 0;
		
		String strSql = "FROM HoleSection a " +
				"WHERE (a.isDeleted = false or a.isDeleted is null) " +
				"AND a.dateTimeIn <= :reportingDate AND a.wellboreUid=:wellboreUid " ;
		String[] paramsFields = {"reportingDate" ,"wellboreUid"};
		Object[] paramsValues = {nextDate, currentWellboreUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult!=null && lstResult.size() > 0){			
			total = lstResult.size();
		}
		
		return total;
	}
	
	public String getCementSectionName(String casingSectionUid, Double openHoleDepthMdMsl) throws Exception{
		String sectionName = null;
		
		String strSql = "FROM CementJob a " +
				"WHERE (a.isDeleted = false or a.isDeleted is null) " +
				"AND a.depthMdMsl <= :openHoleDepthMdMsl AND a.casingSectionUid=:casingSectionUid ORDER by a.depthMdMsl DESC" ;
		String[] paramsFields = {"casingSectionUid", "openHoleDepthMdMsl"};
		Object[] paramsValues = {casingSectionUid, openHoleDepthMdMsl};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult!=null && lstResult.size() > 0){			
			CementJob cementJob = (CementJob) lstResult.get(0); 	
			if (cementJob!=null) sectionName = cementJob.getCementJobUid();
		}
		
		return sectionName;
	}
	
	public void getBharunHoleSize(String operationUid, ReportDataNode reportDataNode, ReportDataNode thisReportNode, String dailyUid) throws Exception{
		String holeSize = "";
		String strSql = "SELECT b.bharunUid, b.bhaRunNumber FROM Bharun b, BharunDailySummary d " +
				"WHERE (b.isDeleted = false or b.isDeleted is null) " +
				"AND (d.isDeleted = false or d.isDeleted is null) " +
				"AND b.bharunUid = d.bharunUid " +
				"AND b.operationUid=:operationUid " +
				"AND d.dailyUid =:dailyUid ORDER BY b.depthInMdMsl ASC ";
		String[] paramsFields = {"operationUid", "dailyUid"};
		Object[] paramsValues = {operationUid, dailyUid};
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult!=null && lstResult.size() > 0){
			Integer count = 1;
			//Collections.sort(lstResult, new BharunNumberComparator());

			for(Object[] bharun: lstResult){
				if (bharun[0]!=null) {
					holeSize = this.getBhaComponentMaxOd((String)bharun[0]);
					
					if (holeSize!=null) {
						thisReportNode = reportDataNode.addChild("Bharun");
						thisReportNode.addProperty("bharunUid", bharun[0].toString());
						thisReportNode.addProperty("bhaComponentUid", holeSize);
					}
					
					this.getBharunComponent(bharun[0].toString(), bharun[1]!=null?bharun[1].toString():"", reportDataNode, thisReportNode, dailyUid, count);
					count++;
				}
			}
		}
	}
	
	public String getBhaComponentMaxOd(String bharunUid) throws Exception{
		String odLookup = null;
		String od = null;
		String odMax = null;
		Double componentOdLookup = null;
		Double componentOd = null;
		Double componentOdMax = null;
		
		String strSql = "FROM BhaComponent a, LookupBhaComponent b " +
				"WHERE (a.isDeleted = false or a.isDeleted is null) AND (b.isDeleted = false or b.isDeleted is null) " +
				"AND a.type=b.lookupBhaComponentUid " +
				"AND b.type LIKE '%Bit%' AND a.bharunUid=:bharunUid ORDER by a.componentOd DESC" ;
		String[] paramsFields = {"bharunUid"};
		Object[] paramsValues = {bharunUid};
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult!=null && lstResult.size() > 0){
			for (Object[] rec : lstResult) {
				BhaComponent bhaComponent = (BhaComponent) rec[0];
				if (bhaComponent!=null){
					odLookup = bhaComponent.getBhaComponentUid();
					componentOdLookup = bhaComponent.getComponentOd();
					break;
				}
			}
		}
			
		strSql = "FROM BhaComponent a, Bharun b " +
				"WHERE (a.isDeleted = false or a.isDeleted is null) " +
				"AND (b.isDeleted = false or b.isDeleted is null) " +
				"AND b.bharunUid = a.bharunUid " +
				"AND a.type LIKE '%Bit%' AND a.bharunUid=:bharunUid ORDER by a.componentOd DESC" ;
		String[] paramsField = {"bharunUid"};
		Object[] paramsValue = {bharunUid};
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		if (lstResult!=null && lstResult.size() > 0){
			for (Object[] rec : lstResult) {
				BhaComponent bhaComponent = (BhaComponent) rec[0];
				if (bhaComponent!=null){
					od = bhaComponent.getBhaComponentUid();
					componentOd = bhaComponent.getComponentOd();
					break;
				}
			}
		}
		
		if (odLookup!=null && od!=null){
			if (componentOdLookup > componentOd){
				odMax = odLookup;
			}else{
				odMax = od;
			}
		}else if(odLookup!=null && od==null){
			odMax = odLookup;
		}else if(odLookup==null && od!=null){
			odMax = od;
		}
		
		return odMax;
	}
	
	public void getBharunComponent(String bharunUid, String bhaRunNumber, ReportDataNode reportDataNode, ReportDataNode thisReportNode, String dailyUid, Integer count) throws Exception{
		if(bharunUid!=null){
			String sql = "Select COUNT(c.bhaComponentUid) FROM BhaComponent c, Bharun b , BharunDailySummary d " +
					"WHERE (b.isDeleted = false or b.isDeleted is null) " +
					"AND (c.isDeleted = false or c.isDeleted is null) " +
					"AND (d.isDeleted = false or d.isDeleted is null) " +
					"AND b.bharunUid = d.bharunUid " +
					"AND b.bharunUid=c.bharunUid " +
					"AND b.bharunUid=:bharunUid "  +
					"AND d.dailyUid =:dailyUid ORDER BY b.depthInMdMsl DESC ";		
			String[] paramsFields = {"bharunUid", "dailyUid"};
			Object[] paramsValues = {bharunUid, dailyUid};
			List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
			if (list.size()>0) {		
				Object values = list.get(0);
				
				if(values!=null){
					thisReportNode = reportDataNode.addChild("BhaComponent");
					thisReportNode.addProperty("bharunUid", bharunUid);
					thisReportNode.addProperty("bhaComponentNo", values.toString());
					thisReportNode.addProperty("bhaRunNumber", bhaRunNumber);
					thisReportNode.addProperty("dailyUid", dailyUid);
					thisReportNode.addProperty("count", count.toString());
				}
			}
		}
	}
	
	public void getCostDetails(String dailyUid, String operationUid, ReportDataNode reportDataNode, ReportDataNode thisReportNode) throws Exception{
		Integer count = 1; 
		String strSql = "SELECT accountCode, afeShortDescription, SUM(itemTotal) FROM CostDailysheet " +
				"WHERE (isDeleted = false or isDeleted is null) " +
				"AND dailyUid=:dailyUid GROUP BY accountCode, afeShortDescription ORDER BY accountCode, afeShortDescription " ;
		String[] paramsFields = {"dailyUid"};
		Object[] paramsValues = {dailyUid};
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult!=null && lstResult.size() > 0){
			for(Object[] cost: lstResult){
				if (cost[0]!=null) {
					thisReportNode = reportDataNode.addChild("CostDailysheet");
					thisReportNode.addProperty("count", count.toString());
					thisReportNode.addProperty("accountCode", cost[0].toString());
					thisReportNode.addProperty("afeShortDescription", cost[1].toString());
					thisReportNode.addProperty("itemTotal", cost[2].toString());
					count ++;
					String afeTotal = this.getCostAfe(cost[0].toString(), operationUid, reportDataNode, thisReportNode);
					if (afeTotal!=null){
						thisReportNode.addProperty("afeItemTotal", afeTotal);
					}
				}
			}
		}
	}
	
	public String getCostAfe(String accountCode, String operationUid, ReportDataNode reportDataNode, ReportDataNode thisReportNode) throws Exception{
		String itemTotal = null;
		if(accountCode!=null){
			String sql = "Select SUM(itemTotal) FROM CostAfeDetail " +
					"WHERE (isDeleted = false or isDeleted is null) " +
					"AND operationUid=:operationUid " +		
					"AND accountCode=:accountCode ";
			String[] paramsFields = {"operationUid", "accountCode"};
			Object[] paramsValues = {operationUid, accountCode};
			List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
			if (list.size()>0) {		
				Object values = list.get(0);
				
				if(values!=null){
					itemTotal = values.toString();
				}
			}
		}
		return itemTotal;
	}
	
	private class BharunNumberComparator implements Comparator<Object[]> {
		public int compare(Object[] o1, Object[] o2) {
			try {
				Object in1 = o1[1];
				Object in2 = o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(in1.toString());
				String f2 = WellNameUtil.getPaddedStr(in2.toString());
				
				if (f1 == null || f2 == null) return 0;
				return f2.compareTo(f1);
				
			} catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
	}
}