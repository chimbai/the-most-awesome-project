package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.infra.uom.OperationUomAndDatumSelector;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.PlanOperationTime;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.activity.ActivityUtils;

public class PlanOperationTimeReportDataGenerator implements ReportDataGenerator{

	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		
		Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(userContext.getUserSelection().getWellUid());
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		
		CustomFieldUom durationConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activityDuration");
		Double operationHour = 24.0;
		if (operation.getOperationHour()!=null && !"".equals(operation.getOperationHour())) operationHour = Double.parseDouble(operation.getOperationHour());
		this.addChildElement(reportDataNode, userContext.getUserSelection().getOperationUid(), operation.getOperationCode(), well.getOnOffShore(), operationHour, durationConverter);
	}

	
	private void addChildElement(ReportDataNode reportDataNode, String operationUid, String operationCode, String onOffShore, Double operationHour, CustomFieldUom durationConverter) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		
		Integer seq = 1;
		Double cpt = 0.0;
		Double ctt = 0.0;
		Double cpt_d = 0.0;
		Boolean emptyRecord = true;
		
		String queryString = "SELECT pot.phaseShortCode, act.operationUid " +
			"FROM Activity act, PlanOperationTime pot, Daily d " +
			"WHERE (act.isDeleted=false or act.isDeleted is null) " +
			"AND (pot.isDeleted=false or pot.isDeleted is null) " +
			"AND (d.isDeleted=false or d.isDeleted is null) " +
			"AND act.planReference=pot.planOperationTimeUid " +
			"AND act.dailyUid=d.dailyUid " +
			"AND (act.isSimop=false or act.isSimop is null) " +
			"AND (act.isOffline=false or act.isOffline is null) " +
			"AND (act.carriedForwardActivityUid='' or act.carriedForwardActivityUid is null) " +
			"AND act.activityDuration>0.0 " +
			"AND d.operationUid=pot.operationUid " +
			"AND act.operationUid=:operationUid " +
			"GROUP BY pot.phaseShortCode, act.operationUid ";
		
		List<Object[]> acts;
		acts = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"operationUid"}, new Object[]{operationUid});
		
		for (Object[] pot: acts) {
			String shortCode = (String) pot[0];
			Double ppt = 0.0;
			Double upt = 0.0;
			Double npt = 0.0;
			Double pt = 0.0;
			Double tu = 0.0;
			
			if (emptyRecord){
				ReportDataNode childNode_0 = reportDataNode.addChild("PlanOperationTime");
				childNode_0.addProperty("seq", "0");
				childNode_0.addProperty("cpt", cpt.toString());
				childNode_0.addProperty("ctt", ctt.toString());
				childNode_0.addProperty("cpt_d", cpt_d.toString());
				emptyRecord = false;
			}
			
			if (StringUtils.isNotBlank(operationUid)) {
				ReportDataNode childNode = reportDataNode.addChild("PlanOperationTime");

				childNode.addProperty("shortCode", shortCode);
				childNode.addProperty("seq", seq.toString());
				childNode.addProperty("name", OperationPlanMasterUtils.getLookupPhaseCodeName(shortCode, operationCode, onOffShore));
				
				ArrayList list = new ArrayList(); 
				String[] planReferenceUids = null;
				
				List<PlanOperationTime> lookup = OperationPlanMasterUtils.getPlanOperationTimeListByShortCode(shortCode, operationUid, qp);
				if (lookup.size() > 0){
					for (PlanOperationTime rec : lookup) {
						list.add(rec.getPlanOperationTimeUid());	
						pt += rec.getOperationHours()==null?0.0:rec.getOperationHours();	
					}
					cpt_d += pt/operationHour;		
					planReferenceUids = (String[]) list.toArray(new String [list.size()]);
				}
				
				if (planReferenceUids != null){
					ppt = ActivityUtils.getActivityTotalDurationByInternalClassCode("P", operationUid, qp, planReferenceUids);
					upt = ActivityUtils.getActivityTotalDurationByInternalClassCode("U", operationUid, qp, planReferenceUids);
					tu = ActivityUtils.getActivityTotalDurationByInternalClassCode("TU", operationUid, qp, planReferenceUids);
					npt = ActivityUtils.getActivityTotalDurationByInternalClassCode("TP", operationUid, qp, planReferenceUids);	
				}
				cpt += (ppt + upt)/operationHour ;
				ctt += (ppt + upt + npt + tu)/operationHour;
					
				durationConverter.setBaseValue(ppt);
				childNode.addProperty("ppt", durationConverter.formatOutputPrecision());
				
				durationConverter.setBaseValue(upt);
				childNode.addProperty("upt", durationConverter.formatOutputPrecision());
				
				durationConverter.setBaseValue(npt);
				childNode.addProperty("npt", durationConverter.formatOutputPrecision());
				
				durationConverter.setBaseValue(tu);
				childNode.addProperty("tu", durationConverter.formatOutputPrecision());
				
				durationConverter.setBaseValue(cpt);
				childNode.addProperty("cpt", durationConverter.formatOutputPrecision());
				
				durationConverter.setBaseValue(ctt);
				childNode.addProperty("ctt", durationConverter.formatOutputPrecision());
				
				durationConverter.setBaseValue(pt);
				childNode.addProperty("pt", durationConverter.formatOutputPrecision());
				
				durationConverter.setBaseValue(cpt_d);
				childNode.addProperty("cpt_d", durationConverter.formatOutputPrecision());
			}	
			seq++;
		}					

	}
	
}
