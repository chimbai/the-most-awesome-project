package com.idsdatanet.d2.drillnet.bharun;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.BhaComponent;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DownholeTools;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.HoleSection;
import com.idsdatanet.d2.core.model.LookupBhaComponent;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.activity.ActivityUtils;
import com.idsdatanet.d2.drillnet.mudProperties.MudPropertiesUtil;
import com.idsdatanet.d2.drillnet.reportDaily.ReportDailyUtils;

public class BharunDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	private static final String FLEX_LAYOUT_BHARUNDAILYSUMMARY_EXISTS = "flex_layout_bharundailysummary_exists";
	private static final String BHARUNDAILYSUMMARY_ANNVEL_DP = "bharundailysummary_annvel_dp";
	private static final String BHARUNDAILYSUMMARY_ANNVEL_DP_CASING = "bharundailysummary_annvel_dp_casing";
	private static final String BHARUNDAILYSUMMARY_ANNVEL_DP_LINER = "bharundailysummary_annvel_dp_liner";
	private static final String BHARUNDAILYSUMMARY_ANNVEL_DP2 = "bharundailysummary_annvel_dp2";
	private static final String BHARUNDAILYSUMMARY_ANNVEL_DP2_CASING = "bharundailysummary_annvel_dp2_casing";
	private static final String BHARUNDAILYSUMMARY_ANNVEL_DP2_LINER = "bharundailysummary_annvel_dp2_liner";
	private static final String BHARUNDAILYSUMMARY_ANNVEL_HWDP = "bharundailysummary_annvel_hwdp";
	private static final String BHARUNDAILYSUMMARY_ANNVEL_HWDP2 = "bharundailysummary_annvel_hwdp2";
	private static final String BHARUNDAILYSUMMARY_ANNVEL_HWDP3 = "bharundailysummary_annvel_hwdp3";
	private static final String BHARUNDAILYSUMMARY_ANNVEL_DC1 = "bharundailysummary_annvel_dc1";
	private static final String BHARUNDAILYSUMMARY_ANNVEL_DC2 = "bharundailysummary_annvel_dc2";
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private static final String CUSTOM_FROM_MARKER = "{_custom_from_}";
	public static final String CONTINUE_FROM_PREVIOUS_RUN = "continue_bha_from_previous_run";
	private static String holeSectionUid=null;
	private static Double pi=3.142857;
	private Boolean usedCumTotalHoursUsed = false;
	public boolean dateTimeValidation = false;
	private boolean TWcalculation = false;
	private boolean calcumduration = false;
	public boolean weightMassForce = false;

	public void setUsedCumTotalHoursUsed(Boolean usedCumTotalHoursUsed){
		this.usedCumTotalHoursUsed = usedCumTotalHoursUsed;
	}

	public void setTWcalculation(Boolean TWcalculation){
		this.TWcalculation = TWcalculation;
	}
	
	public void setcalcumduration(Boolean calcumduration){
		this.calcumduration = calcumduration;
	}
		
	public void setWeightMassForce(Boolean weightMassForce){
		this.weightMassForce = weightMassForce;
	}
	
	private void handleBharunDialySummaryAnnvelDP(TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception {
		/*if(meta.getTableClass() == Bharun.class){
			node.getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_DP, "");
			node.getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_HWDP, "");
			node.getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_HWDP2, "");
			node.getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_HWDP3, "");
			node.getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_DC1, "");
			node.getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_DC2, "");
			node.getDynaAttr().put(FLEX_LAYOUT_BHARUNDAILYSUMMARY_EXISTS, "");
		
		}else*/
		
		if(meta.getTableClass() == BharunDailySummary.class){
			
			if (node.getValue("annvelDp") != null) {
				node.getParent().getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_DP, (Double)node.getValue("annvelDp"));
			}
			if (node.getValue("annvelDpCasing") != null) {
				node.getParent().getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_DP_CASING, (Double)node.getValue("annvelDpCasing"));
			}
			if (node.getValue("annvelDpLiner") != null) {
				node.getParent().getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_DP_LINER, (Double)node.getValue("annvelDpLiner"));
			}
			if (node.getValue("annvelDp2")!= null) {
				node.getParent().getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_DP2, (Double)node.getValue("annvelDp2"));				
			}
			if (node.getValue("annvelDp2Casing") != null) {
				node.getParent().getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_DP2_CASING, (Double)node.getValue("annvelDp2Casing"));
			}
			if (node.getValue("annvelDp2Liner") != null) {
				node.getParent().getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_DP2_LINER, (Double)node.getValue("annvelDp2Liner"));
			}		
			if (node.getValue("annvelHwdp") != null) {
				node.getParent().getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_HWDP, (Double)node.getValue("annvelHwdp"));
			}
			
			if (node.getValue("annvelHwdp2") != null) {
				node.getParent().getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_HWDP2, (Double)node.getValue("annvelHwdp2"));
			}
			
			if (node.getValue("annvelHwdp3") !=null) {
				node.getParent().getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_HWDP3, (Double)node.getValue("annvelHwdp3"));
			}
			
			if (node.getValue("annvelDc1") !=null) {
				node.getParent().getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_DC1, (Double)node.getValue("annvelDc1"));
			}
			
			if (node.getValue("annvelDc2")!=null){
				node.getParent().getDynaAttr().put(BHARUNDAILYSUMMARY_ANNVEL_DC2, (Double)node.getValue("annvelDc2"));
			}
			
			showCustomUOM(node.getParent(), userSelection.getGroupUid());
			node.getParent().getDynaAttr().put(FLEX_LAYOUT_BHARUNDAILYSUMMARY_EXISTS, "true");
		}
		
	}
	
	private static void showCustomUOM(CommandBeanTreeNode node, String groupUid) throws Exception {
		Object object = node.getData();
		if (object instanceof Bharun) {
			CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "annvelDp");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@bharundailysummary_annvel_dp", thisConverter.getUOMMapping());
			}
			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "annvelDpCasing");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@bharundailysummary_annvel_dp_casing", thisConverter.getUOMMapping());
			}
			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "annvelDpLiner");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@bharundailysummary_annvel_dp_liner", thisConverter.getUOMMapping());
			}
			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "annvelDp2");									
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@bharundailysummary_annvel_dp2", thisConverter.getUOMMapping());
			}
			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "annvelDp2Casing");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@bharundailysummary_annvel_dp2_casing", thisConverter.getUOMMapping());
			}
			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "annvelDp2Liner");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@bharundailysummary_annvel_dp2_liner", thisConverter.getUOMMapping());
			}
			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "annvelHwdp");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@bharundailysummary_annvel_hwdp", thisConverter.getUOMMapping());
			}
			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "annvelHwdp2");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@bharundailysummary_annvel_hwdp2", thisConverter.getUOMMapping());
			}
			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "annvelHwdp3");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@bharundailysummary_annvel_hwdp3", thisConverter.getUOMMapping());
			}
			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "annvelDc1");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@bharundailysummary_annvel_dc1", thisConverter.getUOMMapping());
			}
			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "annvelDc2");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@bharundailysummary_annvel_dc2", thisConverter.getUOMMapping());
			}
			
			thisConverter.dispose();
		}
	}
	private void loadBharunList(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception {
		Object obj = node.getData();
		if (obj instanceof Bitrun)
		{
			Bitrun bitrun = (Bitrun) obj;
			if (StringUtils.isNotBlank(bitrun.getBharunUid()))
			{
				Bharun bharun = (Bharun) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Bharun.class, bitrun.getBharunUid());
				String relatedBharun = null;
				if (bharun ==null){
					node.getDynaAttr().put("IsBharun", "no");
				}else {
					
					String bharunNumber= StringUtils.isNotBlank(bharun.getBhaRunNumber())?URLEncoder.encode(bharun.getBhaRunNumber(), "utf-8"):"";	
					relatedBharun = "bharunNumber=" + bharunNumber + "&bharunUid=" + bitrun.getBharunUid();
	
					node.getDynaAttr().put("bharun", relatedBharun);
				}
			}
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		if (meta.getTableClass() == Bharun.class){
			if ("0".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "calc_annvel"))) {
				showCustomUOM(node, userSelection.getGroupUid());
				node.getDynaAttr().put(FLEX_LAYOUT_BHARUNDAILYSUMMARY_EXISTS, "true");
			}
		}
		Object object = node.getData();
	
		if(object instanceof Bitrun) {
			node.getDynaAttr().put("isHide", true);
		}
		if(object instanceof BhaComponent) {
			BhaComponent bhacomponentrecord = (BhaComponent) object;
			if(userSelection.getOperationType().equalsIgnoreCase("DRLLG"))
			{
				bhacomponentrecord.setComponentType("drilling components");
			}
		}
		
	}


	

	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{	
		Object object = targetNode.getData();
		if (object instanceof Bitrun) {
			Bitrun bitrun = (Bitrun) object;
			bitrun.setBharunUid(null);
			bitrun.setOperationUid(null);
			bitrun.setDailyUidIn(null);
			bitrun.setDailyUidOut(null);
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		this.handleBharunDialySummaryAnnvelDP(meta, node, userSelection);
		
		
		Object object = node.getData();
		if(object instanceof BharunDailySummary)
		{
			ArrayList list = new ArrayList(); 
			BharunDailySummary thisBharunDailySummary = (BharunDailySummary) object;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			String strSql2 = "FROM BharunDailySummary WHERE (isDeleted is null or isDeleted = false) and bharunUid=:bharunUid";
			List <BharunDailySummary> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2,
					new String[] {"bharunUid"},
					new Object[] {thisBharunDailySummary.getBharunUid()}, qp);
			
			if(lstResult2!=null && lstResult2.size()>0)
			{
				for(BharunDailySummary bdsRecord:lstResult2)
				{
					
					String strSql3 = "FROM DownholeTools WHERE (isDeleted is null or isDeleted = false) and bharunDailySummaryUid=:bharunDailySummaryUid";
					List <DownholeTools> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3,
							new String[] {"bharunDailySummaryUid"},
							new Object[] {bdsRecord.getBharunDailySummaryUid()}, qp);
					if(lstResult3!=null && lstResult3.size()>0)
					{
						for(DownholeTools downholetools:lstResult3)
						{
							list.add(downholetools.getDownholeToolsUid());
						}
						node.getDynaAttr().put("downholeToolsUid", list);
					}
					
				}
			}
			
			//torque autopopulate
			if(TWcalculation) {
				String strSql = "SELECT torqueMin, torqueAvg, torqueMax FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) and bharunUid =:bharunUid"
						+ " and startTime is not null and dailyUid =: dailyUid order by startTime desc";
				String[] paramsFields1 = { "bharunUid", "dailyUid"};
				Object[] paramsValues1 = {thisBharunDailySummary.getBharunUid(), thisBharunDailySummary.getDailyUid()};
				List lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);

				double torqueMin = 0.00;
				double torqueAvg = 0.00;
				double torqueMax = 0.00;
				
				if (lstResult5.size() > 0)
				{
					Object[] thisResult = (Object[]) lstResult5.get(0);
					
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BharunDailySummary.class, "torqueMin");
					
					//get torque min
					if (thisResult[0] != null) {
						torqueMin = Double.parseDouble(thisResult[0].toString());
					}
					thisConverter.setBaseValue(torqueMin);
					if (thisConverter.isUOMMappingAvailable()) { 
						node.setCustomUOM("@torqueMin", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("torqueMin", thisConverter.getConvertedValue());
					
					//get torqueavg
					thisConverter.setReferenceMappingField(BharunDailySummary.class, "torqueAvg");
					if (thisResult[1] != null) {
						torqueAvg = Double.parseDouble(thisResult[1].toString());
					}
					thisConverter.setBaseValue(torqueAvg);
					if (thisConverter.isUOMMappingAvailable()) { 
						node.setCustomUOM("@torqueAvg", thisConverter.getUOMMapping());
					}
					
					node.getDynaAttr().put("torqueAvg", thisConverter.getConvertedValue());

					//get torquemax
					thisConverter.setReferenceMappingField(BharunDailySummary.class, "torqueMax");
					if (thisResult[2] != null) {
						torqueMax = Double.parseDouble(thisResult[2].toString());
					}
					thisConverter.setBaseValue(torqueMax);
					if (thisConverter.isUOMMappingAvailable()) { 
						node.setCustomUOM("@torqueMax", thisConverter.getUOMMapping());
					}
					
					node.getDynaAttr().put("torqueMax", thisConverter.getConvertedValue());
				}
			}
		}
		
		
		
		if(object instanceof DownholeTools)
		{
			DownholeTools thisDownholeTools = (DownholeTools) object;
			ArrayList list = new ArrayList(); 
			node.getDynaAttr().put("serialNumber", thisDownholeTools.getToolSerialNumber());
			node.getDynaAttr().put("tool", thisDownholeTools.getTool());
			list.add(thisDownholeTools.getDownholeToolsUid());
			node.getDynaAttr().put("downholeToolsUid", list);
		}
		
		if(object instanceof Bharun)
		{
			Bharun thisBharun = (Bharun) object;
			ArrayList list = new ArrayList(); 
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			String strSql2 = "FROM BharunDailySummary WHERE (isDeleted is null or isDeleted = false) and bharunUid=:bharunUid";
			List <BharunDailySummary> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2,
					new String[] {"bharunUid"},
					new Object[] {thisBharun.getBharunUid()}, qp);
			
			if(lstResult2!=null && lstResult2.size()>0)
			{
				for(BharunDailySummary bdsRecord:lstResult2)
				{
					String strSql3 = "FROM DownholeTools WHERE (isDeleted is null or isDeleted = false) and bharunDailySummaryUid=:bharunDailySummaryUid";
					List <DownholeTools> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3,
							new String[] {"bharunDailySummaryUid"},
							new Object[] {bdsRecord.getBharunDailySummaryUid()}, qp);
					if(lstResult3!=null && lstResult3.size()>0)
					{
						for(DownholeTools downholetools:lstResult3)
						{
							list.add(downholetools.getDownholeToolsUid());
						}
						node.getDynaAttr().put("downholeToolsUid", list);
					}
					
				}
			}
			node.getDynaAttr().put("depthIn", thisBharun.getDepthInMdMsl());
			
			//get average flow rate from drilling parameters
			String strSql = "SELECT AVG(a.flowAvg) as avgFlow FROM DrillingParameters a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid AND a.dailyUid = :DailyUid AND a.flowAvg > 0";
			String[] paramsFields = {"BharunUid", "DailyUid"};
			String[] paramsValues = {thisBharun.getBharunUid(), userSelection.getDailyUid()};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			
			double flowAvg = 0.00;
			
			if (lstResult.size() > 0)
			{
				Object flowResult = (Object) lstResult.get(0);
				
				if (flowResult != null){
					flowAvg = Double.parseDouble(flowResult.toString());
				}
			}
						
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "flowAvg");
			thisConverter.setBaseValue(flowAvg);
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@flowAvg", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("flowAvg", thisConverter.getConvertedValue());
			
			//get bit diameter
			strSql = "SELECT bitDiameter FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", thisBharun.getBharunUid(), qp);

			double bitDiameter = 0.00;
			
			if (lstResult.size() > 0)
			{
				Object thisBitResult = (Object) lstResult.get(0);
				
				if (thisBitResult != null)
				{
					bitDiameter = Double.parseDouble(thisBitResult.toString());
				}
			}
			
			thisConverter.setReferenceMappingField(Bharun.class, "minCsgSizeId");
			thisConverter.setBaseValue(bitDiameter);
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@bitDiameter", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("bitDiameter", thisConverter.getConvertedValue());
			
			
			//18115
			if(TWcalculation) {
				strSql = "SELECT cumWeightMass FROM BhaComponent WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid ORDER BY sequence DESC";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", thisBharun.getBharunUid(), qp);
	
				double cumweightmass = 0.00;
				
				if (lstResult.size() > 0)
				{
					Object thisBhaComponentResult = (Object) lstResult.get(0);
					
					if (thisBhaComponentResult != null)
					{
						cumweightmass = Double.parseDouble(thisBhaComponentResult.toString());
					}
				}
				
				thisConverter.setReferenceMappingField(BhaComponent.class, "cumWeightMass");
				thisConverter.setBaseValue(cumweightmass);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumWeightMass", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumWeightMass", thisConverter.getConvertedValue());
				
				//get cum item weight
				Double itemweightmass = 0.00;
				String type = "jar";
				
				String strSqlbha = "FROM BhaComponent b WHERE b.bharunUid =: BharunUid and (b.isDeleted = false or b.isDeleted is null) order by b.sequence";
				List<BhaComponent> lstResultbha = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlbha, 
						new String[] {"BharunUid"}, 
						new Object[] {thisBharun.getBharunUid()}, qp);
				
				String strSqllbha = "FROM LookupBhaComponent b WHERE type LIKE '%jar%' and (b.isDeleted = false or b.isDeleted is null)";
				List<LookupBhaComponent> lstResultlbha = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSqllbha);
				
				if(lstResultbha != null && lstResultbha.size() > 0) {
					
					Boolean isJar = false;
					Boolean containsJar = false;
					
					for(BhaComponent tw:lstResultbha) {					
						for(LookupBhaComponent lbc : lstResultlbha) {
						
							if(StringUtils.containsIgnoreCase(tw.getType(), "jar") || tw.getType().equals(lbc.getLookupBhaComponentUid())) { 
								itemweightmass = 0.00;
								isJar = true;
								containsJar = true;
								break;
							} else {
								isJar = false;
							}
						}
						if(tw.getWeightMass() != null && isJar == false) {
							itemweightmass += tw.getWeightMass();
						}
						
						if(!containsJar) {
							itemweightmass = 0.0;
						}
					}				
				}
				
				thisConverter.setReferenceMappingField(BhaComponent.class, "weightMass");
				thisConverter.setBaseValue(itemweightmass);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@itemWeight", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("itemWeight", thisConverter.getConvertedValue());
			}
		}
		
		
		
		//To show bit wear only when the date out of the bit is larger than current selected date.
		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "showBitWearOnDateOutOnly"))){	
		
			int hideBitWear = 0;
			
			if (object instanceof Bitrun) {
				
				Bitrun thisBitrun = (Bitrun) object;
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				Daily bitDateOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisBitrun.getDailyUidOut());
				Date currentDayDate = null;
				Date bitOutDayDate = null;
				
				if (daily != null) {
					currentDayDate = daily.getDayDate();
				
					if (bitDateOut != null) {
						bitOutDayDate = bitDateOut.getDayDate();
					
						if (bitOutDayDate.after(currentDayDate)){
							hideBitWear = 1;
						}
						else hideBitWear = 0;
					}
					else hideBitWear = 1;								
				}	
				node.getDynaAttr().put("isHide", hideBitWear);
			}
		}
		
			
		if (object instanceof BhaComponent)
		{		
			BhaComponent thisBhaComponent = (BhaComponent) object;
			
			// Pump cumulative Length From physical instance to dynamic field
			if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CALCULATE_BHA_DETAIL_CUM_LENGTH)))
			{
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BhaComponent.class, "cumjointlength");

				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cLength", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cLength", thisBhaComponent.getCumjointlength());

			}
			

			//Pump Cumulative Weight Mass From physical instance to dynamic field
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BhaComponent.class, "cumWeightMass");
			node.getDynaAttr().put("cWeightMass", thisBhaComponent.getCumWeightMass());
			
			if(thisConverter.getUOMMapping() != null) 
			{
				node.setCustomUOM("@cWeightMass", thisConverter.getUOMMapping());
			}
			
			//Pump Cumulative Weight Force From physical instance to dynamic field
			thisConverter.setReferenceMappingField(BhaComponent.class, "cumWeightForce");
			node.getDynaAttr().put("cWeightForce", thisBhaComponent.getCumWeightForce());
			
			if(thisConverter.getUOMMapping() != null) 
			{
				node.setCustomUOM("@cWeightForce", thisConverter.getUOMMapping());;
			}
			
			//	Set Dynamic Attribute for showing the Service History button if serial number is entered
			if(thisBhaComponent.getSerialNum() != null && !StringUtils.isEmpty(thisBhaComponent.getSerialNum())){
				node.getDynaAttr().put("serialNumExist", "1");
			}else{
				node.getDynaAttr().put("serialNumExist", "0");
			}
			//	End of Set Dynamic Attribute for showing the Service History button if serial number is entered
					
			if (usedCumTotalHoursUsed){
				/*  Ticket: 20110216-0405-jwong - New calculation logic for cumulative hours used
				 *	Calculate total hours used for the bha component throughout different bha run which have same serial #, depend on last reset date and time out.
				 *	If no serial number for the bha component, will not calculate the total hours used.	
				 */			
				// 1. get the necessary parameters which needed to be used by cumulative calculation method
				BhaComponent comp = (BhaComponent) object;
				String serialNumber = "";			
				String dailyUid = "";
				String bharunUid = ""; 
				
				serialNumber = comp.getSerialNum();			
				bharunUid = comp.getBharunUid();
				dailyUid = userSelection.getDailyUid();
	
				// 2. Call the cumulative calculation method & get the returned calculated hours
				Double cumulativeHoursUsed = BharunUtils.calculateCumTotalHoursUsed(serialNumber, dailyUid, bharunUid);
							
				// 3. Populate the returned calculated cumulative hours into dynamic field
				if(cumulativeHoursUsed != null && cumulativeHoursUsed >= 0){
					thisConverter = new CustomFieldUom(commandBean, BhaComponent.class, "totalHoursUsed");
					thisConverter.setBaseValue(cumulativeHoursUsed);
					if (thisConverter.isUOMMappingAvailable()) { 
						node.setCustomUOM("@cumulativeHoursUsed", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumulativeHoursUsed", thisConverter.getConvertedValue());				
				}
				
				String typeLabel = this.getTypeLookupLabel(thisBhaComponent, node, commandBean);
				node.getDynaAttr().put("typeLabel", typeLabel);
			}
			
			
			
		
		}
		
		String bhaRunUid = null;
		if (request != null) {
			bhaRunUid = request.getParameter(CONTINUE_FROM_PREVIOUS_RUN);
		}
		
		if(meta.getTableClass() == Bharun.class && StringUtils.isNotBlank(bhaRunUid)){
			CommandBeanTreeNode child = node.addCustomNewChildNodeForInput(new BharunDailySummary());
			child.getAtts().setSelected(true);
			commandBean.getFlexClientControl().setReloadAfterPageCancel();
		}
		
		//Auto calculate slide and rotate Duration
		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoCalculateSlideAndRotateDuration"))){
			if(object instanceof BharunDailySummary) {
				Bharun bh = (Bharun) node.getParent().getData();
				BharunDailySummary thisDailySummary = (BharunDailySummary) node.getData();				
				// get the current dailyUid and operationUid from the record
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisDailySummary.getDailyUid());
				if(daily == null) return;
				
				Date currentDate = daily.getDayDate();
				String bharunUid = thisDailySummary.getBharunUid();
				String operationUid = daily.getOperationUid();
				String dailyUid = thisDailySummary.getDailyUid();
				Daily thisDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BharunDailySummary.class, "duration");
				BharunUtils.calculateSlideRotateDuration(bh, thisDailySummary, thisConverter, operationUid, dailyUid, thisDaily);
				BharunUtils.calculateTotalRun(node, currentDate, thisConverter, operationUid, dailyUid, bharunUid);
			}
		}
		
		if(object instanceof BharunDailySummary) {
			BharunDailySummary thisDailySummary = (BharunDailySummary) node.getData();
			
			CustomFieldUom slideConverter = new CustomFieldUom(commandBean, BharunDailySummary.class, "percentSlideDay");
			CustomFieldUom rotatedConverter = new CustomFieldUom(commandBean, BharunDailySummary.class, "percentRotatedDay");
			CustomFieldUom circulatedConverter = new CustomFieldUom(commandBean, BharunDailySummary.class, "percentCirculatedDay");
			
			BharunUtils.calculatePercentDay(thisDailySummary, slideConverter, rotatedConverter, circulatedConverter);
		}
		
		//Auto calculate IADC Duration
		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoCalculateIADCDuration"))){
			Double IADChrs = 0.00;
			if(object instanceof BharunDailySummary) {
				BharunDailySummary thisBhaDailySummary = (BharunDailySummary) object;
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisBhaDailySummary.getDailyUid());
				if(daily == null) return;
				
				String bharunUid = thisBhaDailySummary.getBharunUid();
				String operationUid = daily.getOperationUid();
				Date currentDate = daily.getDayDate();
				Double slideHours = thisBhaDailySummary.getSlideHours();
				Double durationRotated = thisBhaDailySummary.getDurationRotated();
				
//				if(slideHours != null  && durationRotated != null) {
//					IADChrs = slideHours + durationRotated;
//					thisBhaDailySummary.setIADCDuration(IADChrs);
//				}else{
//					thisBhaDailySummary.setIADCDuration(IADChrs);
//				}
			//	ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDailySummary);
				
				String[] paramsFields = {"todayDate", "operationUid", "bharunUid"};
				Object[] paramsValues = new Object[3]; paramsValues[0] = currentDate; paramsValues[1] = operationUid; paramsValues[2] = bharunUid;
				
				String strSQL = "SELECT SUM(bds.duration) as totalDuration, SUM(bds.progress) as totalProgress, SUM(bds.IADCDuration) as totalIADCDuration, " +
				"SUM(bds.slideHours) as totalSlideHours, SUM(bds.durationRotated) as totalDurationRotated, " +
				"SUM(bds.durationCirculated) as totalDurationCirculated, SUM(bds.jarRotatingHours) as cumJarRotatingHours, " +
				"SUM(bds.krevs) as totalKrevs, SUM(bds.stalls) as totalStalls, SUM(bds.acceleratingHour)as cumAcceleratingHours, " +
				"SUM(bds.slideDistance) as totalSlideDistance, SUM(bds.rotatedDistance) as totalRotatedDistance, SUM(bds.jarhourcirculate) as cumJarCirculatingHours, " +
				"SUM(bds.inHoleHrs) as cumInHoleHrs " +
					"FROM BharunDailySummary bds, Daily d " +
					"WHERE (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
					"and d.dailyUid = bds.dailyUid AND d.dayDate <= :todayDate AND d.operationUid = :operationUid AND bds.bharunUid = :bharunUid";				
				
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSQL, paramsFields, paramsValues);
				Object[] thisResult = (Object[]) lstResult.get(0);
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BharunDailySummary.class, "duration");

				//CALCULATE TOTAL IADC DURATION
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "IADCDuration");
				Double totalIADCDuration = 0.00;
				if (thisResult[2] != null) {
					totalIADCDuration = Double.parseDouble(thisResult[2].toString());
				}
				thisConverter.setBaseValue(totalIADCDuration);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalIADCDuration", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalIADCDuration", thisConverter.getConvertedValue());
				
				
			}			
		}
		
		if (meta.getTableClass() == BharunDailySummary.class) {
			if(StringUtils.isNotBlank(bhaRunUid)) {
				// do nothing
			} else {
				BharunDailySummary thisDailySummary = (BharunDailySummary) node.getData();
				
				// get the current dailyUid and operationUid from the record
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisDailySummary.getDailyUid());
				if(daily == null) return;
				
				Date currentDate = daily.getDayDate();
				String operationUid = daily.getOperationUid();				
				String bharunUid = thisDailySummary.getBharunUid();
				
				String[] paramsFields = {"todayDate", "operationUid", "bharunUid"};
				Object[] paramsValues = new Object[3]; paramsValues[0] = currentDate; paramsValues[1] = operationUid; paramsValues[2] = bharunUid;
				
				String strSql = "SELECT SUM(bds.duration) as totalDuration, SUM(bds.progress) as totalProgress, SUM(bds.IADCDuration) as totalIADCDuration, " +
				"SUM(bds.slideHours) as totalSlideHours, SUM(bds.durationRotated) as totalDurationRotated, " +
				"SUM(bds.durationCirculated) as totalDurationCirculated, SUM(bds.jarRotatingHours) as cumJarRotatingHours, " +
				"SUM(bds.krevs) as totalKrevs, SUM(bds.stalls) as totalStalls, SUM(bds.acceleratingHour)as cumAcceleratingHours, " +
				"SUM(bds.slideDistance) as totalSlideDistance, SUM(bds.rotatedDistance) as totalRotatedDistance, SUM(bds.jarhourcirculate) as cumJarCirculatingHours, " +
				"SUM(bds.inHoleHrs) as cumInHoleHrs " +
					"FROM BharunDailySummary bds, Daily d " +
					"WHERE (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
					"and d.dailyUid = bds.dailyUid AND d.dayDate <= :todayDate AND d.operationUid = :operationUid AND bds.bharunUid = :bharunUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				Object[] thisResult = (Object[]) lstResult.get(0);
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BharunDailySummary.class, "duration");
				
				//CALCULATE TOTAL TIME ON BOTTOM
				Double totalDuration = 0.00;
				if (thisResult[0] != null) {
					totalDuration = Double.parseDouble(thisResult[0].toString());
				}
				thisConverter.setBaseValue(totalDuration);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalDuration", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalDuration", thisConverter.getConvertedValue());
				
				//CALCULATE TOTAL IADC DURATION
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "IADCDuration");
				Double totalIADCDuration = 0.00;
				if (thisResult[2] != null) {
					totalIADCDuration = Double.parseDouble(thisResult[2].toString());
				}
				thisConverter.setBaseValue(totalIADCDuration);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalIADCDuration", thisConverter.getUOMMapping());
				}
				
				node.getDynaAttr().put("totalIADCDuration", thisConverter.getConvertedValue());

				//CALCULATE TOTAL SLIDE HOURS
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "slideHours");
				Double totalSlideHours = 0.00;
				if (thisResult[3] != null) {
					totalSlideHours = Double.parseDouble(thisResult[3].toString());
				}
				thisConverter.setBaseValue(totalSlideHours);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalSlideHours", thisConverter.getUOMMapping());
				}
				
				node.getDynaAttr().put("totalSlideHours", thisConverter.getConvertedValue());
				
				//CALCULATE TOTAL DURATION ROTATED
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "durationRotated");
				Double totalDurationRotated = 0.00;
				if (thisResult[4] != null) {
					totalDurationRotated = Double.parseDouble(thisResult[4].toString());
				}
				thisConverter.setBaseValue(totalDurationRotated);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalDurationRotated", thisConverter.getUOMMapping());
				}
				
				node.getDynaAttr().put("totalDurationRotated", thisConverter.getConvertedValue());
				
				//CALCULATE TOTAL DURATION CIRCULATED
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "durationCirculated");
				Double totalDurationCirculated = 0.00;
				if (thisResult[5] != null) {
					totalDurationCirculated = Double.parseDouble(thisResult[5].toString());
				}
				thisConverter.setBaseValue(totalDurationCirculated);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalDurationCirculated", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalDurationCirculated", thisConverter.getConvertedValue());
				
				//CALCULATE PERCENT TOTAL RUN
				Double percentSlideDuration = 0.00;
				Double percentRotatedDuration = 0.00;
				Double percentCirculatedDuration = 0.00;
				
				Double slideHours = Double.parseDouble(node.getDynaAttr().get("totalSlideHours").toString());
				Double durationRotated = Double.parseDouble(node.getDynaAttr().get("totalDurationRotated").toString());
				Double durationCirculated = Double.parseDouble(node.getDynaAttr().get("totalDurationCirculated").toString());
				
				if(slideHours != null && durationRotated != null && durationCirculated != null) {
					if(slideHours == 0.00 && durationRotated == 0.00 && durationCirculated == 0.00) {
						thisDailySummary.setPercentSlideDuration(null);
						thisDailySummary.setPercentRotatedDuration(null);
						thisDailySummary.setPercentCirculatedDuration(null);
					}else {
						percentSlideDuration = (slideHours / (slideHours + durationRotated + durationCirculated))*100;
						percentRotatedDuration = (durationRotated / (slideHours + durationRotated + durationCirculated))*100;
						percentCirculatedDuration = (durationCirculated / (slideHours + durationRotated + durationCirculated))*100;
						
						thisDailySummary.setPercentSlideDuration(percentSlideDuration);
						thisDailySummary.setPercentRotatedDuration(percentRotatedDuration);
						thisDailySummary.setPercentCirculatedDuration(percentCirculatedDuration);
					}
				}else if(slideHours != null && durationRotated != null && durationCirculated == null) {
					percentSlideDuration = (slideHours / (slideHours + durationRotated))*100;
					percentRotatedDuration = (durationRotated / (slideHours + durationRotated))*100;
					
					thisDailySummary.setPercentSlideDuration(percentSlideDuration);
					thisDailySummary.setPercentRotatedDuration(percentRotatedDuration);
				}
				
				//CALCULATE TOTAL BIT PROGRESS
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "progress");
				Double totalProgress = 0.00;
				if (thisResult[1] != null) {
					totalProgress = Double.parseDouble(thisResult[1].toString());
				}
				thisConverter.setBaseValue(totalProgress);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalProgress", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalProgress", thisConverter.getConvertedValue());
				
				//CALCULATE AVERAGE ROP
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "rop");
				Double avgrop = 0.00;
				if (thisResult[0] != null && thisResult[1] != null) {
					if (totalDuration!=0) avgrop = totalProgress / totalDuration;
				}
				thisConverter.setBaseValue(avgrop);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@avgROP", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("avgROP", thisConverter.getConvertedValue());

				//CALCULATE AVERAGE IADC ROP
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "rop");
				Double avgIADCrop = 0.00;
				if (totalProgress != null && totalIADCDuration != null) {
					if (totalDuration!=0) {
						if(totalIADCDuration != 0) {
							avgIADCrop = totalProgress / totalIADCDuration;
						}
					}
				}
				thisConverter.setBaseValue(avgIADCrop);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@avgIADCrop", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("avgIADCrop", thisConverter.getConvertedValue());
				
				//AUTO-CALCULATE ROP FROM USER VALUE
				//NEED UOM CONVERSION
				Double thisDurationBaseValue = 0.00;
				Double thisIADCDurationBaseValue = 0.00;
				Double thisProgressBaseValue = 0.00;
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "duration");
				if(thisDailySummary.getDuration() != null)
				{
					thisConverter.setBaseValueFromUserValue(thisDailySummary.getDuration());
					thisDurationBaseValue = thisConverter.getBasevalue();
				}
				
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "IADCDuration");
				if(thisDailySummary.getIADCDuration() != null)
				{
					thisConverter.setBaseValueFromUserValue(thisDailySummary.getIADCDuration());
					thisIADCDurationBaseValue = thisConverter.getBasevalue();
				}
				
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "progress");
				if(thisDailySummary.getProgress() != null)
				{
					thisConverter.setBaseValueFromUserValue(thisDailySummary.getProgress());
					thisProgressBaseValue = thisConverter.getBasevalue();
				}
				
				Double thisRop = 0.00;
				if (thisDailySummary.getDuration() != null && thisDailySummary.getProgress() != null){
					if(thisDurationBaseValue != 0) thisRop = thisProgressBaseValue / thisDurationBaseValue;				
				}
				Double thisIADCRop = 0.00;
				if (thisDailySummary.getIADCDuration() != null && thisDailySummary.getProgress() != null){
					if(thisIADCDurationBaseValue != 0) thisIADCRop = thisProgressBaseValue / thisIADCDurationBaseValue;				
				}
				
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "rop");
				thisConverter.setBaseValue(thisRop);
				thisDailySummary.setRop(thisConverter.getConvertedValue());	
				thisConverter.setBaseValue(thisIADCRop);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@IADCrop", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("IADCrop", thisConverter.getConvertedValue());
				
				//CALCULATE CUM JAR ROTATING HOURS
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "jar_rotating_hours");
				Double cumJarHours = 0.00;
				if (thisResult[6] != null) {
					cumJarHours = Double.parseDouble(thisResult[6].toString());
				}
				thisConverter.setBaseValue(cumJarHours);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@cumJarRotatingHours", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumJarRotatingHours", thisConverter.getConvertedValue());
				
				//CALCULATE TOTAL KREVS
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "krevs");
				Double totalKrevs = 0.00;
				if (thisResult[7] != null) {
					totalKrevs = Double.parseDouble(thisResult[7].toString());
				}
				thisConverter.setBaseValue(totalKrevs);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalKrevs", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalKrevs", thisConverter.getConvertedValue());
				
				//CALCULATE TOTAL STALLS
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "stalls");
				Double totalStalls = 0.00;
				if (thisResult[8] != null) {
					totalStalls = Double.parseDouble(thisResult[8].toString());
				}
				thisConverter.setBaseValue(totalStalls);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalStalls", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalStalls", thisConverter.getConvertedValue());
				
				//CALCULATE CUM ACCLERATOR HOURS
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "accelerating_hour");
				Double cumAcceleratingHours = 0.00; 
				if (thisResult[9] != null) {
					cumAcceleratingHours = Double.parseDouble(thisResult[9].toString());
				}
				thisConverter.setBaseValue(cumAcceleratingHours);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@cumAcceleratingHours", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumAcceleratingHours", thisConverter.getConvertedValue());
				
				//CALCULATE CUM SLIDE DISTANCE
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "slideDistance");
				Double totalSlideDistance = 0.00;
				if (thisResult[10] != null) {
					totalSlideDistance = Double.parseDouble(thisResult[10].toString());
				}
				thisConverter.setBaseValue(totalSlideDistance);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalSlideDistance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalSlideDistance", thisConverter.getConvertedValue());
				
				//CALCULATE CUM ROTATED DISTANCE
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "rotatedDistance");
				Double totalRotatedDistance = 0.00;
				if (thisResult[11] != null) {
					totalRotatedDistance = Double.parseDouble(thisResult[11].toString());
				}
				thisConverter.setBaseValue(totalRotatedDistance);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@totalRotatedDistance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalRotatedDistance", thisConverter.getConvertedValue());
				
				//CALCULATE CUM JAR CIRCULATING HOURS
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "jarhourcirculate");
				Double cumJarCircHours = 0.00;
				if (thisResult[12] != null) {
					cumJarCircHours = Double.parseDouble(thisResult[12].toString());
				}
				thisConverter.setBaseValue(cumJarCircHours);
				if (thisConverter.isUOMMappingAvailable()) { 
					node.setCustomUOM("@cumJarCirculatingHours", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumJarCirculatingHours", thisConverter.getConvertedValue());
				
				//CALCULATE CUM IN HOLE HOURS
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "inHoleHrs");
				Double cumInHoleHrs = 0.00;
				if (thisResult[13] != null) {
					cumInHoleHrs = Double.parseDouble(thisResult[13].toString());
				}
				thisConverter.setBaseValue(cumInHoleHrs);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@cumInHoleHrs", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumInHoleHrs", thisConverter.getConvertedValue());
				
				String bharunDailySummaryUid = thisDailySummary.getBharunDailySummaryUid();
				
				String[] paramsFields2 = {"todayDate", "operationUid", "bharunDailySummaryUid"};
				Object[] paramsValues2 = {currentDate,operationUid,bharunDailySummaryUid};
				
				String strSql2 = "SELECT bds.nozzlePressureLoss, bds.systemHydraulicHp, bds.bitHydraulicHp, bds.bitPressureLoss, bds.bitImpactForce, bds.bitImpactForceArea " +
				"FROM BharunDailySummary bds, Daily d " +
				"WHERE (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
				"and d.dailyUid = bds.dailyUid AND d.dayDate = :todayDate AND d.operationUid = :operationUid AND bds.bharunDailySummaryUid = :bharunDailySummaryUid";
			
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				
				Object[] thisResult2 = (Object[]) lstResult2.get(0);
				
				CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, BharunDailySummary.class, "nozzlePressureLoss");
				
				//CALCULATE Nozzle Pressure Loss
				thisConverter2.setReferenceMappingField(BharunDailySummary.class, "nozzlePressureLoss");
				Double nozzlePressureLoss = 0.00;
				
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_JET_NOZZLE_PRESSURE_LOSS)))
				{
					if (thisResult2[0] != null) {
						nozzlePressureLoss = Double.parseDouble(thisResult2[0].toString());
					}
				}
				else{
					if (thisResult2[0] != null) {
						nozzlePressureLoss = Double.parseDouble(thisResult2[0].toString());
					}
					else{
						thisDailySummary.setNozzlePressureLoss(0.00);
					}
				}
				thisConverter2.setBaseValueFromUserValue(nozzlePressureLoss);
				if (thisConverter2.isUOMMappingAvailable()) { 
					node.setCustomUOM("@nozzlePressureLoss", thisConverter2.getUOMMapping());
				}
				node.getDynaAttr().put("nozzlePressureLoss", thisConverter2.getConvertedValue());
				
				//CALCULATE System Hydraulic Hp
				thisConverter2.setReferenceMappingField(BharunDailySummary.class, "systemHydraulicHp");
				Double systemHydraulicHp = 0.00;
				
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_SYS_HYDRAULIC_HORSE_POWER)))
				{
					if (thisResult2[1] != null) {
						systemHydraulicHp = Double.parseDouble(thisResult2[1].toString());
					}
				}
				else{
					if (thisResult2[1] != null) {
						systemHydraulicHp = Double.parseDouble(thisResult2[1].toString());
					}
					else{
						thisDailySummary.setSystemHydraulicHp(0.00);
					}
				}
				thisConverter2.setBaseValueFromUserValue(systemHydraulicHp);
				if (thisConverter2.isUOMMappingAvailable()) { 
					node.setCustomUOM("@systemHydraulicHp", thisConverter2.getUOMMapping());
				}
				node.getDynaAttr().put("systemHydraulicHp", thisConverter2.getConvertedValue());
				
				//CALCULATE Bit Hydraulic Hp
				thisConverter2.setReferenceMappingField(BharunDailySummary.class, "bitHydraulicHp");
				Double bitHydraulicHp = 0.00;
				
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_BIT_HYDRAULIC_HORSE_POWER)))
				{
					if (thisResult2[2] != null) {
						bitHydraulicHp = Double.parseDouble(thisResult2[2].toString());
					}
				}
				else{
					if (thisResult2[2] != null) {
						bitHydraulicHp = Double.parseDouble(thisResult2[2].toString());
					}
					else{
						thisDailySummary.setBitHydraulicHp(0.00);
					}
				}
				thisConverter2.setBaseValueFromUserValue(bitHydraulicHp);
				if (thisConverter2.isUOMMappingAvailable()) { 
					node.setCustomUOM("@bitHydraulicHp", thisConverter2.getUOMMapping());
				}
				node.getDynaAttr().put("bitHydraulicHp", thisConverter2.getConvertedValue());
				
				//CALCULATE Bit Hydraulic Hp Percent
				thisConverter2.setReferenceMappingField(BharunDailySummary.class, "bitPressureLoss");
				Double bitPressureLoss = 0.00;
				
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_BIT_PRESSURE_LOSS_PERCENTAGE)))
				{
					if (thisResult2[3] != null) {
						bitPressureLoss = Double.parseDouble(thisResult2[3].toString());
					}
				}
				else{
					if (thisResult2[3] != null) {
						bitPressureLoss = Double.parseDouble(thisResult2[3].toString());
					}
					else{
						thisDailySummary.setBitPressureLoss(0.00);
					}
				}
				thisConverter2.setBaseValueFromUserValue(bitPressureLoss);
				if (thisConverter2.isUOMMappingAvailable()) { 
					node.setCustomUOM("@bitPressureLoss", thisConverter2.getUOMMapping());
				}
				node.getDynaAttr().put("bitPressureLoss", thisConverter2.getConvertedValue());

				
				//CALCULATE Impact Force
				thisConverter2.setReferenceMappingField(BharunDailySummary.class, "bitImpactForce");
				Double bitImpactForce = 0.00;
				
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_BIT_IMPACT_FORCE)))
				{
					if (thisResult2[4] != null) {
						bitImpactForce = Double.parseDouble(thisResult2[4].toString());
					}
				}
				else{
					if (thisResult2[4] != null) {
						bitImpactForce = Double.parseDouble(thisResult2[4].toString());
					}
					else{
						thisDailySummary.setBitImpactForce(0.00);
					}
				}
				thisConverter2.setBaseValueFromUserValue(bitImpactForce);
				if (thisConverter2.isUOMMappingAvailable()) { 
					node.setCustomUOM("@bitImpactForce", thisConverter2.getUOMMapping());
				}
				node.getDynaAttr().put("bitImpactForce", thisConverter2.getConvertedValue());
				
				//CALCULATE IF/AREA
				thisConverter2.setReferenceMappingField(BharunDailySummary.class, "bitImpactForceArea");
				Double bitImpactForceArea = 0.00;
				
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_IMPACT_FORCE_PER_BIT_AREA)))
				{
					if (thisResult2[5] != null) {
						bitImpactForceArea = Double.parseDouble(thisResult2[5].toString());
					}
				}
				else{
					if (thisResult2[5] != null) {
						bitImpactForceArea = Double.parseDouble(thisResult2[5].toString());
					}
					else{
						thisDailySummary.setBitImpactForceArea(0.00);
					}
				}
				thisConverter2.setBaseValueFromUserValue(bitImpactForceArea);
				if (thisConverter2.isUOMMappingAvailable()) { 
					node.setCustomUOM("@bitImpactForceArea", thisConverter2.getUOMMapping());
				}
				node.getDynaAttr().put("bitImpactForceArea", thisConverter2.getConvertedValue());

				//CALCULATE PERCENT SLIDE
				if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALC_PERCENT_SLIDE))){
					Double slideDistance=0.0;
					
					totalSlideDistance = Double.parseDouble(node.getDynaAttr().get("totalSlideDistance").toString());
					totalRotatedDistance = Double.parseDouble(node.getDynaAttr().get("totalRotatedDistance").toString());
					if (node.getValue("slideDistance")!= null){
						slideDistance = (Double) node.getValue("slideDistance");
					}
					
					Double percentSlide=BharunUtils.calculatePercentSlide(totalSlideDistance, totalRotatedDistance, slideDistance);
					thisConverter.setReferenceMappingField(BharunDailySummary.class, "percentSlide");
					thisConverter.setBaseValue(percentSlide);
					thisDailySummary.setPercentSlide(thisConverter.getConvertedValue());
				}
				
				//CALCULATE PERCENT ROTATE
				if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALC_PERCENT_ROTATED))){
					Double rotatedDistance=0.0;
					
					totalSlideDistance = Double.parseDouble(node.getDynaAttr().get("totalSlideDistance").toString());
					totalRotatedDistance = Double.parseDouble(node.getDynaAttr().get("totalRotatedDistance").toString());
					if (node.getValue("rotatedDistance")!= null){
						rotatedDistance = (Double) node.getValue("rotatedDistance");
					}
					
					Double percentRotated = BharunUtils.calculatePercentRotated(totalSlideDistance, totalRotatedDistance, rotatedDistance);
					thisConverter.setReferenceMappingField(BharunDailySummary.class, "percentRotated");
					thisConverter.setBaseValue(percentRotated);
					thisDailySummary.setPercentRotated(thisConverter.getConvertedValue());
				}
				
				//CALCULATE PERCENT CIRCULATED
				if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_AUTO_CALC_PERCENT_CIRCULATED))){
					totalDurationCirculated = Double.parseDouble(node.getDynaAttr().get("totalDurationCirculated").toString());
					totalIADCDuration = Double.parseDouble(node.getDynaAttr().get("totalIADCDuration").toString());
					
					Double percentCirculated = BharunUtils.calculatePercentCirculated(totalDurationCirculated, totalIADCDuration);
					thisConverter.setReferenceMappingField(BharunDailySummary.class, "percentCirculated");
					thisConverter.setBaseValue(percentCirculated);
					thisDailySummary.setPercentCirculated(thisConverter.getConvertedValue());
				}
				
				//CALCULATE JET VELOCITY
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "jetvelocity");
				Double jetvelocity = CommonUtil.getConfiguredInstance().calculateJetVelocity(bharunUid, thisDailySummary.getDailyUid());
				if (jetvelocity!=null) {
					thisConverter.setBaseValue(jetvelocity);
					thisDailySummary.setJetvelocity(thisConverter.getConvertedValue());
				} else {
					thisDailySummary.setJetvelocity(null);
				}
				
				//CALCULATE TOTAL WELL DRILLING HOURS
				BharunUtils.calcAccumulatedDrillingDuration(daily);
				
			}
		}
		
		if (object instanceof Bharun) {
			Bharun thisbharun = (Bharun) object;
			// to get day_date from BharunDailySummary for report use
			if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT){
				String operationUid = thisbharun.getOperationUid();
				String bharunUid = thisbharun.getBharunUid();
				String[] paramsFields2 = {"operationUid", "bharunUid"};
				Object[] paramsValues2 = new Object[2]; paramsValues2[0] = operationUid; paramsValues2[1] = bharunUid;
				String strSql2 = "SELECT min(d.dayDate) " +
					"FROM BharunDailySummary bds, Daily d " +
					"WHERE (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
					"and d.dailyUid = bds.dailyUid AND d.operationUid = :operationUid AND bds.bharunUid = :bharunUid";
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				
				Date dayDate = null;
				if (lstResult2.size() >0) {
					dayDate = (Date) lstResult2.get(0);
				}
				node.getDynaAttr().put("BharunDailySummary_minDayDate", dayDate);
			}
			
			this.loadBitrunRecordList(node, thisbharun.getBharunUid(), userSelection);
			this.includeCompleteComponentString(node, commandBean);
		}
		if (object instanceof Bitrun) {
			this.includeIADCBitDull(commandBean, node);
			this.loadBharunList(node,userSelection);
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			Bitrun thisBitrun = (Bitrun) object;
			String strSql = "Select depthInMdMsl, depthOutMdMsl FROM Bharun where (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bharunUid", thisBitrun.getBharunUid(), qp);
			if(list.size()>0){
				for (Object[] bharun : list) {
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Bharun.class, "depthInMdMsl");
					Double depthInMdMsl = 0.00;
					Double depthOutMdMsl = 0.00;
					if(bharun[0] != null) {
						depthInMdMsl = Double.parseDouble(bharun[0].toString());						
					}
					thisConverter.setBaseValue(depthInMdMsl);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@depthInMdMsl", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("depthInMdMsl", thisConverter.getConvertedValue());
					
					if(bharun[1] != null) {
						depthOutMdMsl = Double.parseDouble(bharun[1].toString());						
					}
					thisConverter.setBaseValue(depthOutMdMsl);
					thisConverter.setReferenceMappingField(Bharun.class, "depthOutMdMsl");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@depthOutMdMsl", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("depthOutMdMsl", thisConverter.getConvertedValue());
				}
			}
			
			Bitrun bitrun = (Bitrun) object;
			String[] paramsFields = {"operationUid", "bharunUid"};
			Object[] paramsValues = {bitrun.getOperationUid(), bitrun.getBharunUid()};
			
			String strSql1 = "SELECT SUM(bds.progress) as totalProgress, " +
			"SUM(bds.IADCDuration) as totalIADCDuration " +
			"FROM BharunDailySummary bds, Daily d " +
			"WHERE (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
			"and d.dailyUid = bds.dailyUid AND d.operationUid = :operationUid AND bds.bharunUid = :bharunUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields, paramsValues);
			
			Object[] thisResult = (Object[]) lstResult.get(0);
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BharunDailySummary.class, "progress");
			Double totalProgress = 0.00;
			if (thisResult[0] != null) {
				totalProgress = Double.parseDouble(thisResult[0].toString());
				thisConverter.setBaseValue(totalProgress);
				totalProgress = thisConverter.getBasevalue();
			}
			Double distancePriorRun = 0.00;
			thisConverter.setReferenceMappingField(Bitrun.class, "distancePriorRun");
			if (bitrun.getDistancePriorRun() != null){
				thisConverter.setBaseValueFromUserValue(bitrun.getDistancePriorRun());
				distancePriorRun = thisConverter.getBasevalue();
			}
			thisConverter.setBaseValue(distancePriorRun + totalProgress);
			if (thisConverter.isUOMMappingAvailable()) { 
				node.setCustomUOM("@totalDistancePriorRun", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("totalDistancePriorRun", thisConverter.getConvertedValue(true));
			
			
			thisConverter.setReferenceMappingField(BharunDailySummary.class, "IADCDuration");
			Double totalIADCDuration = 0.00;
			if (thisResult[1] != null) {
				totalIADCDuration = Double.parseDouble(thisResult[1].toString());
				thisConverter.setBaseValue(totalIADCDuration);
				totalIADCDuration = thisConverter.getBasevalue();
			}
			Double durationPriorRun = 0.00;
			thisConverter = new CustomFieldUom(commandBean, Bitrun.class, "durationPriorRun");
			if (bitrun.getDurationPriorRun() != null){
				thisConverter.setBaseValueFromUserValue(bitrun.getDurationPriorRun());
				durationPriorRun = thisConverter.getBasevalue();
			}
			thisConverter.setBaseValue(durationPriorRun + totalIADCDuration);
			if (thisConverter.isUOMMappingAvailable()) { 
				node.setCustomUOM("@totalDurationPriorRun", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("totalDurationPriorRun", thisConverter.getConvertedValue());
			
			Double wobMinForce = 0.00;
			Double wobMaxForce = 0.00;
			Double surfaceRpmMin = 0.00;
			Double surfaceRpmMax = 0.00;
			Double stringWeightRotaryAvgForce = 0.00;
			Double slackOffWeightAvgForce = 0.00;
			Double pickupWeightAvgForce = 0.00;
			
			strSql = "Select wobMinForce, wobMaxForce, surfaceRpmMin, surfaceRpmMax, stringWeightRotaryAvgForce, slackOffWeightAvgForce, pickupWeightAvgForce FROM DrillingParameters where (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid ORDER BY depthTopMdMsl DESC";
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bharunUid", thisBitrun.getBharunUid(), qp);
			if(list.size()>0){
				
				for (Object[] drillingParameters : list) {
					thisConverter.setReferenceMappingField(DrillingParameters.class, "wobMinForce");
					if(drillingParameters[0] != null) {
						wobMinForce = Double.parseDouble(drillingParameters[0].toString());						
					}
					thisConverter.setBaseValue(wobMinForce);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@wobMinForce", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("wobMinForce", thisConverter.getConvertedValue());
					
					thisConverter.setReferenceMappingField(DrillingParameters.class, "wobMaxForce");
					if(drillingParameters[1] != null) {
						wobMaxForce = Double.parseDouble(drillingParameters[1].toString());						
					}
					thisConverter.setBaseValue(wobMaxForce);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@wobMaxForce", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("wobMaxForce", thisConverter.getConvertedValue());
					
					thisConverter.setReferenceMappingField(DrillingParameters.class, "surfaceRpmMin");
					if(drillingParameters[2] != null) {
						surfaceRpmMin = Double.parseDouble(drillingParameters[2].toString());						
					}
					thisConverter.setBaseValue(surfaceRpmMin);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@surfaceRpmMin", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("surfaceRpmMin", thisConverter.getConvertedValue());
					
					thisConverter.setReferenceMappingField(DrillingParameters.class, "surfaceRpmMax");
					if(drillingParameters[3] != null) {
						surfaceRpmMax = Double.parseDouble(drillingParameters[3].toString());						
					}
					thisConverter.setBaseValue(surfaceRpmMax);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@surfaceRpmMax", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("surfaceRpmMax", thisConverter.getConvertedValue());
					
					thisConverter.setReferenceMappingField(DrillingParameters.class, "stringWeightRotaryAvgForce");
					if(drillingParameters[4] != null) {
						stringWeightRotaryAvgForce = Double.parseDouble(drillingParameters[4].toString());						
					}
					thisConverter.setBaseValue(stringWeightRotaryAvgForce);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@stringWeightRotaryAvgForce", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("stringWeightRotaryAvgForce", thisConverter.getConvertedValue());
					
					thisConverter.setReferenceMappingField(DrillingParameters.class, "slackOffWeightAvgForce");
					if(drillingParameters[5] != null) {
						slackOffWeightAvgForce = Double.parseDouble(drillingParameters[5].toString());						
					}
					thisConverter.setBaseValue(slackOffWeightAvgForce);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@slackOffWeightAvgForce", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("slackOffWeightAvgForce", thisConverter.getConvertedValue());
					
					thisConverter.setReferenceMappingField(DrillingParameters.class, "pickupWeightAvgForce");
					if(drillingParameters[6] != null) {
						pickupWeightAvgForce = Double.parseDouble(drillingParameters[6].toString());						
					}
					thisConverter.setBaseValue(pickupWeightAvgForce);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@pickupWeightAvgForce", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("pickupWeightAvgForce", thisConverter.getConvertedValue());
					break;
				}
			}
			
			//p2
			if(calcumduration) {
				Bitrun bitrun2 = (Bitrun) object;
				
				if(bitrun2.getTimeIn() != null) {
					String[] paramsFields1 = {"operationUid", "timeIn"};
					Object[] paramsValues1 =  {bitrun2.getOperationUid(), bitrun2.getTimeIn()};
					String strSql2 = "SELECT SUM(b.duration) as cumtimeonbottom, SUM(b.iadcDuration) as cumiadchrs, SUM(b.krevs) as cumkrevs, "
							+ "SUM(b.progress) as cumprogress, SUM(b.rop) as cumrop"
							+ " FROM Bitrun b WHERE (b.isDeleted = false or b.isDeleted is null)"
							+ " and b.operationUid =:operationUid and b.timeIn <= :timeIn";
									
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields1, paramsValues1, qp);
					
					double duration = 0.00;
					double iadcduration = 0.00;
					double krews = 0.00;
					double progress = 0.00;
					double rop = 0.00;
					
					Object[] a = (Object[]) lstResult2.get(0);
					
					//calculate Cumulative Time on Bottom
					if(a[0] != null) {
						duration = (Double)a[0];
					}
					thisConverter.setReferenceMappingField(Bitrun.class, "duration");
					thisConverter.setBaseValue(duration);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("duration", thisConverter.getConvertedValue());
					
					//calculate Cumulative IADC hours
					if(a[1] != null) {
						iadcduration = (Double)a[1];
					}
					thisConverter.setReferenceMappingField(Bitrun.class, "iadcduration");
					thisConverter.setBaseValue(iadcduration);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumIadcDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumIadcDuration", thisConverter.getConvertedValue());
					
					//calculate Cumulative krews
					if(a[2] != null) {
						krews = (Double)a[2];
					}
					thisConverter.setReferenceMappingField(Bitrun.class, "krews");
					thisConverter.setBaseValue(krews);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@krevs", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("krevs", thisConverter.getConvertedValue());
					
					//calculate Cumulative Progress
					if(a[3]!= null) {
						progress = (Double)a[3];
					}
					thisConverter.setReferenceMappingField(Bitrun.class, "progress");
					thisConverter.setBaseValue(progress);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumProgress", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumProgress", thisConverter.getConvertedValue());
					
					//calculate Cumulative Rop
					if(a[4]!= null) {
						rop = (Double)a[4];
					}
					thisConverter.setReferenceMappingField(Bitrun.class, "rop");
					thisConverter.setBaseValue(rop);
					
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumRop", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumRop", thisConverter.getConvertedValue());
					
				}
			}
		}
	}	

	
	private void includeCompleteComponentString(CommandBeanTreeNode node, CommandBean commandBean) throws Exception
	{
		String stringComponent=null;
		Map<String, CommandBeanTreeNode> componentList=node.getChild("BhaComponent");
		Map<String, LookupItem> thisLookupMap = commandBean.getLookupMap(node, "BhaComponent", "type");
		for (Iterator iter = componentList.entrySet().iterator(); iter.hasNext();)
		{
			Map.Entry entry = (Map.Entry) iter.next();
			CommandBeanTreeNode nodeComp=(CommandBeanTreeNode)entry.getValue();
			if (nodeComp.getData() instanceof BhaComponent) {
				BhaComponent comp=(BhaComponent)nodeComp.getData();
				if (thisLookupMap == null) {
					if (stringComponent==null) {
						stringComponent=comp.getType();
					} else {
						stringComponent+=", "+comp.getType();
					}					
				} else {
					LookupItem type=thisLookupMap.get(comp.getType());
					if (type!=null) {
						if (stringComponent==null) {
							stringComponent=type.getValue().toString();
						} else {
							stringComponent+=", "+type.getValue().toString();
						}
					} else {
						if (stringComponent==null) {
							stringComponent=comp.getType();
						} else {
							stringComponent+=", "+comp.getType();
						}
					}
				}
			}
		}
		if (stringComponent!=null)
			node.getDynaAttr().put("stringComponents", stringComponent);
	}
		
	private void includeIADCBitDull(CommandBean commandBean, CommandBeanTreeNode node) throws Exception
	{
		Object obj=node.getData();
		if (obj instanceof Bitrun)
		{
			Bitrun bit=(Bitrun) obj;
			
			String cond_final_inner=null;
			if (bit.getCondFinalInner()!=null)
				cond_final_inner=bit.getCondFinalInner();
			
			String cond_final_outer=null;
			if (bit.getCondFinalOuter()!=null)
				cond_final_outer=bit.getCondFinalOuter();
			
			String cond_final_dull=null;
			if (bit.getCondFinalDull()!=null)
				cond_final_dull=bit.getCondFinalDull();
			
			String cond_final_location=null;
			if (bit.getCondFinalLocation()!=null)
				cond_final_location=bit.getCondFinalLocation();
			
			String cond_final_bearing=null;
			if (bit.getCondFinalBearing()!=null)
				cond_final_bearing=bit.getCondFinalBearing();
			
			String cond_final_gauge=null;
			if (bit.getCondFinalGauge()!=null)
				cond_final_gauge=bit.getCondFinalGauge();
				
			String cond_final_other=null;
			if (bit.getCondFinalOther()!=null)
				cond_final_other=bit.getCondFinalOther().replaceAll("\t", "/");
			
			String cond_final_reason=null;
			if (bit.getCondFinalReason()!=null)
				cond_final_reason=bit.getCondFinalReason();
			
			String iadcBitDull=cond_final_inner+"-"+cond_final_outer+"-"+cond_final_dull+"-"+cond_final_location+"-"+cond_final_bearing+"-"+cond_final_gauge+"-"+cond_final_other+"-"+cond_final_reason;
			node.getDynaAttr().put("iadcBitDull", iadcBitDull);
		}
	}
	
	private String getDynaAttr(CommandBeanTreeNode node, String dynaAttrName) throws Exception {
		Object dynaAttrValue = node.getDynaAttr().get(dynaAttrName);
		if (dynaAttrValue != null) {
			return dynaAttrValue.toString();
		}
		return null;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
	
		if (object instanceof BharunDailySummary) {
			BharunDailySummary thisDailySummary = (BharunDailySummary) object;
			//auto calculate bit on bottom hour
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_BIT_ON_BOTTOM_HOUR))){
				double totalBitOnBottomHour = 0.0;
				if (thisDailySummary.getSlideHours() != null) totalBitOnBottomHour = totalBitOnBottomHour + thisDailySummary.getSlideHours();
				if (thisDailySummary.getDurationRotated() != null) totalBitOnBottomHour = totalBitOnBottomHour + thisDailySummary.getDurationRotated();
				thisDailySummary.setDuration(totalBitOnBottomHour);
			}
			
			if ("0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "calc_annvel"))) {
				this.setAnnvel(node.getParent(), thisDailySummary);					
			}
			
			Double currentDurationRotated = 0.00;
			Double currentSlideHours = 0.00;
			Double currentDurationCirculated = 0.00;
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BharunDailySummary.class, "durationRotated");
			if (thisDailySummary.getDurationRotated() != null){
				currentDurationRotated = thisDailySummary.getDurationRotated();
				thisConverter.setBaseValueFromUserValue(currentDurationRotated);
				currentDurationRotated = thisConverter.getBasevalue();
			}
			
			if (thisDailySummary.getSlideHours() != null){
				currentSlideHours = thisDailySummary.getSlideHours();
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "slideHours");
				thisConverter.setBaseValueFromUserValue(currentSlideHours);
				currentSlideHours = thisConverter.getBasevalue();
			}
			
			if (thisDailySummary.getDurationCirculated() != null){
				currentDurationCirculated = thisDailySummary.getDurationCirculated();
				thisConverter.setReferenceMappingField(BharunDailySummary.class, "durationCirculated");
				thisConverter.setBaseValueFromUserValue(currentDurationCirculated);
				currentDurationCirculated = thisConverter.getBasevalue();
			}
			/*
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALC_PERCENT_SLIDE))){
				Double percentSlide = 0.00;
				if (currentSlideHours == 0.00){
					thisDailySummary.setPercentSlide(0.00);
				}else{
					percentSlide = (currentSlideHours / (currentSlideHours + currentDurationRotated + currentDurationCirculated));
					thisConverter.setReferenceMappingField(BharunDailySummary.class, "percentSlide");
					thisConverter.setBaseValue(percentSlide);
					thisDailySummary.setPercentSlide(thisConverter.getConvertedValue());
				}
			}
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALC_PERCENT_ROTATED))){
				Double percentRotated = 0.00;
				if (currentDurationRotated == 0.00){
					thisDailySummary.setPercentRotated(0.00);
				}else{
					percentRotated = (currentDurationRotated / (currentSlideHours + currentDurationRotated + currentDurationCirculated));
					thisConverter.setReferenceMappingField(BharunDailySummary.class, "percentRotated");
					thisConverter.setBaseValue(percentRotated);
					thisDailySummary.setPercentRotated(thisConverter.getConvertedValue());
				}
				
			}
			*/
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoCalculateIADCDuration"))){
				Double IADChrs = 0.00;
				Double slideHours = thisDailySummary.getSlideHours();
				Double durationRotated = thisDailySummary.getDurationRotated();
				
				if(slideHours != null  && durationRotated != null) {
					IADChrs = slideHours + durationRotated;
					thisDailySummary.setIADCDuration(IADChrs);
				}else{
					thisDailySummary.setIADCDuration(IADChrs);
				}
			//	ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBhaDailySummary);
	
			}
				
			//auto calculate drag
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "calculateBHADailyDrag"))){
				double weightPickup = 0.0;
				double weightString = 0.0;
				
				if (thisDailySummary.getWeightPickup() != null) weightPickup = thisDailySummary.getWeightPickup();
				if (thisDailySummary.getWeightString() != null) weightString = thisDailySummary.getWeightString();
					
				thisDailySummary.setDragDown(BharunUtils.calculateBHADailyDrag(weightPickup, weightString, thisConverter));
				
			}
			
			
			
		} else if (object instanceof BhaComponent) {
			BhaComponent thisBhaComponent = (BhaComponent) object;		
			String equipmentType = getDynaAttr(node, "equipmentType");
			if (StringUtils.isNotBlank(equipmentType)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupBhaComponent where (isDeleted = false or isDeleted is null) and type = :type", "type", equipmentType);
				if (list != null && list.size() > 0) {
					status.setDynamicAttributeError(node, "equipmentType", "The '" + equipmentType + "' already exists in database");
					status.setContinueProcess(false, true);
					return;
				}
				LookupBhaComponent lookup = new LookupBhaComponent();
				lookup.setType(equipmentType);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lookup);
				thisBhaComponent.setType(lookup.getLookupBhaComponentUid());
			}
			this.calculateWeightMassAndForce(commandBean,thisBhaComponent,session);
		}
		
		//to flag the daily object to dirty when save run, due to if calculate av is set to false, user can edit the av value
		if(object instanceof Bharun && "0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "calc_annvel"))){
			for(Iterator it=node.getList().values().iterator(); it.hasNext();) {				
				Map items = (Map)it.next();
				for(Iterator data=items.values().iterator(); data.hasNext();) {
					CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) data.next();
					Object childObj = childNode.getData();
					if(childObj instanceof BharunDailySummary) {						
						//force the BharunDailySummary to save
						//TODO - JongA want to introduce new handling later
						childNode.getAtts().setAction(Action.SAVE);
						//break;
					}
				}
			}
		}
		
		// pick up the bharunUid if the record isn't new.
		if(object instanceof Bharun)
		{
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_WELLBORE_PROPERTIES)))
				validateHoleSection(object);
			//Validation Code for DateIn < DateOut
			Bharun thisBharun = (Bharun) object;
			Daily thisDailyStart = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisBharun.getDailyidIn());
			Daily thisDailyEnd = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisBharun.getDailyidOut());
			if (thisDailyStart != null && thisDailyEnd != null){
				
				Date thisDateIn = null;
				Date thisDateOut = null;
				
				if(thisBharun.getTimeIn() != null){
					
					if(CommonDateParser.parse(thisDailyStart.getDayDate().toString()) != null)
					{
						thisDateIn = CommonDateParser.parse(thisDailyStart.getDayDate().toString());	
					}
					else
					{
						thisDateIn = (Date) thisDailyStart.getDayDate();
					}
					
					if(thisDateIn != null){
						this.setTimeField(thisBharun, thisDateIn, "timeIn");					
					}
				}else{
					if(this.dateTimeValidation) {
						//ticket number#15805 - prompt error if both Time In/Out are empty
						if (thisBharun.getTimeOut() == null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in both date and time");
							status.setFieldError(node, "timeIn", "Please fill in both date and time");
							status.setFieldError(node, "timeOut", "Please fill in both date and time");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;	
						}
						//ticket number#15805 - prompt error if only Time In is empty
						else {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in the Date/Time In");
							status.setFieldError(node, "timeIn", "Please fill in the Date/Time In");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;	
						}
					}else {
						PropertyUtils.setProperty(object, "timeIn", thisDailyStart.getDayDate());
					}
				}
				
				if(thisBharun.getTimeOut() != null){					
					
					if(CommonDateParser.parse(thisDailyEnd.getDayDate().toString()) != null)
					{
						thisDateOut = CommonDateParser.parse(thisDailyEnd.getDayDate().toString());
					}
					else
					{
						thisDateOut = (Date) thisDailyEnd.getDayDate();
					}
					
					if(thisDateOut != null){
						this.setTimeField(thisBharun, thisDateOut, "timeOut");				
					}
				}else{
					if(this.dateTimeValidation) {
						//ticket number#15805 - prompt error if only Time Out is empty
						status.setContinueProcess(false, true);
						commandBean.getSystemMessage().addError("Please fill in the Date/Time Out");
						status.setFieldError(node, "timeOut", "Please fill in the Date/Time Out");
						commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
						return;
					}else {
						PropertyUtils.setProperty(object, "timeOut", thisDailyEnd.getDayDate());
					}
					
				}
				
				if(thisBharun.getTimeIn() != null && thisBharun.getTimeOut() != null){
					if(thisBharun.getTimeIn().getTime() > thisBharun.getTimeOut().getTime()){
						if(this.dateTimeValidation) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Date/Time In cannot be after Date/Time Out");
							status.setFieldError(node, "dailyidIn", "Date/Time In cannot be after Date/Time Out");
							status.setFieldError(node, "timeIn", "Date/Time In cannot be after Date/Time Out");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}else {
							status.setContinueProcess(false, true);
//							Error display code that should work but does not show up.
//							status.setFieldError(node, "dailyidIn","Start date can not be greater than end time.");
							commandBean.getSystemMessage().addError("Start date time cannot be greater than end date time.");
							return;
						}

					}
				}
				
				if(this.dateTimeValidation) {
					if ((CommonDateParser.parse(thisDailyStart.getDayDate().toString()).equals((CommonDateParser.parse(thisDailyStart.getDayDate().toString()))))) {
						//ticket number#15805 - prompt error if Date/Time in are same as Date/Time Out
						if(thisBharun.getTimeIn().getTime() == thisBharun.getTimeOut().getTime()){
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Date/Time In cannot be the same as Date/Time Out");
							status.setFieldError(node, "dailyidIn", "Date/Time In cannot be the same as Date/Time Out");
							status.setFieldError(node, "dailyidOut", "Date/Time In cannot be the same as Date/Time Out");
							status.setFieldError(node, "timeIn", "Date/Time In cannot be the same as Date/Time Out");
							status.setFieldError(node, "timeOut", "Date/Time In cannot be the same as Date/Time Out");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;	
						}
					}
				}
				
				
//				if(thisDailyEnd.getDayDate().before(thisDailyStart.getDayDate())){
//					status.setContinueProcess(false, true);
////	Error display code that should work but does not show up.
////					status.setFieldError(node, "dailyidIn","Start date can not be greater than end time.");
//					commandBean.getSystemMessage().addError("Date/Time In cannot be after Date/Time Out");
//					commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
//					return;
//				}
			}
			if(commandBean.getOperatingMode()!=BaseCommandBean.OPERATING_MODE_DEPOT) {	
				if(thisDailyStart == null){
					if(this.dateTimeValidation) {
						//ticket number#15805 - prompt error if only Date In is empty
						if(thisBharun.getTimeIn() != null && thisBharun.getTimeOut() != null && thisDailyEnd != null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in the Date/Time In");
							status.setFieldError(node, "dailyidIn", "Please fill in the Date/Time In");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}
						
						//ticket number#15805 - prompt error if both Date In/Out are empty
						if (thisBharun.getTimeIn() != null && thisBharun.getTimeOut() != null && thisDailyEnd == null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in both date and time");
							status.setFieldError(node, "dailyidIn", "Please fill in both date and time");
							status.setFieldError(node, "dailyidOut", "Please fill in both date and time");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}
						
						//ticket number#15805 - prompt error if both Date/Time In are empty
						if(thisBharun.getTimeIn() == null && thisBharun.getTimeOut() != null && thisDailyEnd != null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in the Date/Time In");
							status.setFieldError(node, "dailyidIn", "Please fill in the Date/Time In");
							status.setFieldError(node, "timeIn", "Please fill in the Date/Time In");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}
						
						//ticket number#15805 - prompt error if both Date In and Time Out are empty
						if(thisBharun.getTimeIn() != null && thisBharun.getTimeOut() == null && thisDailyEnd != null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in both date and time");
							status.setFieldError(node, "dailyidIn", "Please fill in both date and time");
							status.setFieldError(node, "timeOut", "Please fill in both date and time");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}
						
						
						//ticket number#15805 - prompt error if only Time Out is not empty
						if(thisBharun.getTimeIn() == null && thisBharun.getTimeOut() != null && thisDailyEnd == null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in both date and time");
							status.setFieldError(node, "dailyidIn", "Please fill in both date and time");
							status.setFieldError(node, "dailyidOut", "Please fill in both date and time");
							status.setFieldError(node, "timeIn", "Please fill in both date and time");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}
						
						//ticket number#15805 - prompt error if only Time In is not empty
						if(thisBharun.getTimeIn() != null && thisBharun.getTimeOut() == null && thisDailyEnd == null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in both date and time");
							status.setFieldError(node, "dailyidIn", "Please fill in both date and time");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}
						
						//ticket number#15805 - prompt error if only Date Out is not empty
						if(thisBharun.getTimeIn() == null && thisBharun.getTimeOut() == null && thisDailyEnd != null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in both date and time");
							status.setFieldError(node, "dailyidIn", "Please fill in both date and time");
							status.setFieldError(node, "timeIn", "Please fill in both date and time");
							status.setFieldError(node, "timeOut", "Please fill in both date and time");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}
					}else if(commandBean.getOperatingMode()!=BaseCommandBean.OPERATING_MODE_DEPOT){
						thisBharun.setTimeIn(null);
					}
				}
				else{
					if(thisBharun.getTimeIn() != null){
						Date thisDateIn = null;
						
						if(CommonDateParser.parse(thisDailyStart.getDayDate().toString()) != null)
						{
							thisDateIn = CommonDateParser.parse(thisDailyStart.getDayDate().toString());
						}
						else
						{
							thisDateIn = (Date) thisDailyStart.getDayDate();
						}
						
						if(thisDateIn != null){
							this.setTimeField(thisBharun, thisDateIn, "timeIn");						
						}
					}else{
						if(this.dateTimeValidation) {
							//ticket number#15805 - prompt error if only Date In is not empty
							if(thisBharun.getTimeIn() == null && thisBharun.getTimeOut() == null && thisDailyEnd == null) {
								status.setContinueProcess(false, true);
								commandBean.getSystemMessage().addError("Please fill in both date and time");
								status.setFieldError(node, "timeIn", "Please fill in both date and time");
								commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
								return;
							}
							
							//ticket number#15805 - prompt error if Date Out and Time In are empty
							if(thisBharun.getTimeIn() == null && thisBharun.getTimeOut() != null && thisDailyEnd == null) {
								status.setContinueProcess(false, true);
								commandBean.getSystemMessage().addError("Please fill in both date and time");
								status.setFieldError(node, "dailyidOut", "Please fill in both date and time");
								status.setFieldError(node, "timeIn", "Please fill in both date and time");
								commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
								return;
							}
						}else {
							PropertyUtils.setProperty(object, "timeIn", thisDailyStart.getDayDate());
						}
					}
				}
				
				//if Day Date out is null set time to null else amend day date out to time out
				if(thisDailyEnd == null){
					if(this.dateTimeValidation) {
						//ticket number#15805 - prompt error if only Date Out is empty
						if(thisBharun.getTimeIn() != null && thisBharun.getTimeOut() != null && thisDailyStart != null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in the Date/Time Out");
							status.setFieldError(node, "dailyidOut", "Please fill in the Date/Time Out");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}else {
							thisBharun.setTimeOut(null);
						}
					}else if(commandBean.getOperatingMode()!=BaseCommandBean.OPERATING_MODE_DEPOT){
						thisBharun.setTimeOut(null);
					}
				}else{
					if(thisBharun.getTimeOut() != null){
						Date thisDateOut = null;
						
						if(CommonDateParser.parse(thisDailyEnd.getDayDate().toString()) != null)
						{
							thisDateOut = CommonDateParser.parse(thisDailyEnd.getDayDate().toString());
						}
						else
						{
							thisDateOut = (Date) thisDailyEnd.getDayDate();
						}
						
						if(thisDateOut != null){
							this.setTimeField(thisBharun, thisDateOut, "timeOut");						
						}
					}else{
						PropertyUtils.setProperty(object, "timeOut", thisDailyEnd.getDayDate());
					}
						
				}
				
				this.autoPopulateBharunPropertyToBitrun(node, commandBean,session);
			}
		}
		
		if (object instanceof Bitrun) {
			Bitrun thisBitrun = (Bitrun) object;
			Daily thisDailyStart = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisBitrun.getDailyUidIn());
			Daily thisDailyEnd = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisBitrun.getDailyUidOut());
			if (thisDailyStart != null && thisDailyEnd != null) {
				Date thisDateIn = null;
				Date thisDateOut = null;
				if (thisBitrun.getTimeIn() != null) {
					if (CommonDateParser.parse(thisDailyStart.getDayDate().toString()) != null) {
						thisDateIn = CommonDateParser.parse(thisDailyStart.getDayDate().toString());
					} else {
						thisDateIn = (Date) thisDailyStart.getDayDate();
					}
					if (thisDateIn != null) {
						this.setTimeField(thisBitrun, thisDateIn, "timeIn");
					}
				} else {
					if(this.dateTimeValidation) {
						//ticket number#15805 - prompt error if both Time In/Out are empty
						if (thisBitrun.getTimeOut() == null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in both date and time");
							status.setFieldError(node, "timeIn", "Please fill in both date and time");
							status.setFieldError(node, "timeOut", "Please fill in both date and time");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;	
						}
						//ticket number#15805 - prompt error if only Time In is empty
						else {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in the Date/Time In");
							status.setFieldError(node, "timeIn", "Please fill in the Date/Time In"); 
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;	
						}
					}else {
						PropertyUtils.setProperty(object, "timeIn", thisDailyStart.getDayDate());
					}
				}
				if (thisBitrun.getTimeOut() != null) {
					if (CommonDateParser.parse(thisDailyEnd.getDayDate().toString()) != null) {
						thisDateOut = CommonDateParser.parse(thisDailyEnd.getDayDate().toString());
					} else {
						thisDateOut = (Date) thisDailyEnd.getDayDate();
					}
					if (thisDateOut != null) {
						this.setTimeField(thisBitrun, thisDateOut, "timeOut");
					}
				} else {
					if(this.dateTimeValidation) {
						if (thisBitrun.getTimeOut() == null) {
							//ticket number#15805 - prompt error if only Time Out is empty
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in the Date/Time Out");
							status.setFieldError(node, "timeOut", "Please fill in the Date/Time Out"); 
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}else {
							PropertyUtils.setProperty(object, "timeOut", thisDailyEnd.getDayDate());
						}
					}else {
						PropertyUtils.setProperty(object, "timeOut", thisDailyEnd.getDayDate());
					}
					
				}
				if (thisBitrun.getTimeIn() != null && thisBitrun.getTimeOut() != null) {
					if (thisBitrun.getTimeIn().getTime() > thisBitrun.getTimeOut().getTime()) {
						if(this.dateTimeValidation) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Date/Time In cannot be after Date/Time Out");
							status.setFieldError(node, "dailyUidIn", "Date/Time In cannot be after Date/Time Out");
							status.setFieldError(node, "timeIn", "Date/Time In cannot be after Date/Time Out"); 
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}else {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Start date time cannot be greater than end date time.");
							return;
						}
						
					}
				}
				
				if(this.dateTimeValidation) {
					if ((CommonDateParser.parse(thisDailyStart.getDayDate().toString()).equals((CommonDateParser.parse(thisDailyStart.getDayDate().toString()))))) {
						//ticket number#15805 - prompt error if Date/Time in are same as Date/Time Out
						if(thisBitrun.getTimeIn().getTime() == thisBitrun.getTimeOut().getTime()){
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Date/Time In cannot be the same as Date/Time Out");
							status.setFieldError(node, "dailyUidIn", "Date/Time In cannot be the same as Date/Time Out");
							status.setFieldError(node, "dailyUidOut", "Date/Time In cannot be the same as Date/Time Out");
							status.setFieldError(node, "timeIn", "Date/Time In cannot be the same as Date/Time Out");
							status.setFieldError(node, "timeOut", "Date/Time In cannot be the same as Date/Time Out"); 
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;	
						}
					}
				}
				
			}
			
			if(this.dateTimeValidation && commandBean.getOperatingMode()!=BaseCommandBean.OPERATING_MODE_DEPOT) {
				if(thisDailyStart == null){
					
					//ticket number#15805 - prompt error if only Date In is empty
					if(thisBitrun.getTimeIn() != null && thisBitrun.getTimeOut() != null && thisDailyEnd != null) {
						status.setContinueProcess(false, true);
						commandBean.getSystemMessage().addError("Please fill in the Date/Time In");
						status.setFieldError(node, "dailyUidIn", "Please fill in the Date/Time In");
						commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
						return;
					}
					
					//ticket number#15805 - prompt error if both Date In/Out are empty
					if (thisBitrun.getTimeIn() != null && thisBitrun.getTimeOut() != null && thisDailyEnd == null) {
						status.setContinueProcess(false, true);
						commandBean.getSystemMessage().addError("Please fill in both date and time");
						status.setFieldError(node, "dailyUidIn", "Please fill in both date and time");
						status.setFieldError(node, "dailyUidOut", "Please fill in both date and time"); 
						commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
						return;
					}
					
					//ticket number#15805 - prompt error if both Date/Time In are empty
					if(thisBitrun.getTimeIn() == null && thisBitrun.getTimeOut() != null && thisDailyEnd != null) {
						status.setContinueProcess(false, true);
						commandBean.getSystemMessage().addError("Please fill in the Date/Time In");
						status.setFieldError(node, "dailyUidIn", "Please fill in the Date/Time In");
						status.setFieldError(node, "timeIn", "Please fill in the Date/Time In"); 
						commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
						return;
					}
					
					//ticket number#15805 - prompt error if both Date In and Time Out are empty
					if(thisBitrun.getTimeIn() != null && thisBitrun.getTimeOut() == null && thisDailyEnd != null) {
						status.setContinueProcess(false, true);
						commandBean.getSystemMessage().addError("Please fill in both date and time");
						status.setFieldError(node, "dailyUidIn", "Please fill in both date and time");
						status.setFieldError(node, "timeOut", "Please fill in both date and time");
						commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
						return;
					}
					
					//ticket number#15805 - prompt error if only Time Out is not empty
					if(thisBitrun.getTimeIn() == null && thisBitrun.getTimeOut() != null && thisDailyEnd == null) {
						status.setContinueProcess(false, true);
						commandBean.getSystemMessage().addError("Please fill in both date and time");
						status.setFieldError(node, "dailyUidIn", "Please fill in both date and time");
						status.setFieldError(node, "dailyUidOut", "Please fill in both date and time");
						status.setFieldError(node, "timeIn", "Please fill in both date and time"); 
						commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
						return;
					}
					
					//ticket number#15805 - prompt error if only Time In is not empty
					if(thisBitrun.getTimeIn() != null && thisBitrun.getTimeOut() == null && thisDailyEnd == null) {
						status.setContinueProcess(false, true);
						commandBean.getSystemMessage().addError("Please fill in both date and time");
						status.setFieldError(node, "dailyUidIn", "Please fill in both date and time");
						commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
						return;
					}
					
					//ticket number#15805 - prompt error if Date Out is not empty
					if(thisBitrun.getTimeIn() == null && thisBitrun.getTimeOut() == null && thisDailyEnd != null) {
						status.setContinueProcess(false, true);
						commandBean.getSystemMessage().addError("Please fill in both date and time");
						status.setFieldError(node, "dailyUidIn", "Please fill in both date and time");
						status.setFieldError(node, "timeIn", "Please fill in both date and time");
						status.setFieldError(node, "timeOut", "Please fill in both date and time");
						commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
						return;
					}
					//thisBharun.setTimeIn(null);
				}else{
					if(thisBitrun.getTimeIn() != null){
						Date thisDateIn = null;
						
						if(CommonDateParser.parse(thisDailyStart.getDayDate().toString()) != null)
						{
							thisDateIn = CommonDateParser.parse(thisDailyStart.getDayDate().toString());
						}
						else
						{
							thisDateIn = (Date) thisDailyStart.getDayDate();
						}
						
						if(thisDateIn != null){
							this.setTimeField(thisBitrun, thisDateIn, "timeIn");						
						}
					}else{
						//ticket number#15805 - prompt error if only Date In is not empty
						if(thisBitrun.getTimeIn() == null && thisBitrun.getTimeOut() == null && thisDailyEnd == null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in both date and time");
							status.setFieldError(node, "timeIn", "Please fill in both date and time");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}
						
						//ticket number#15805 - prompt error if Date Out and Time In are empty
						if(thisBitrun.getTimeIn() == null && thisBitrun.getTimeOut() != null && thisDailyEnd == null) {
							status.setContinueProcess(false, true);
							commandBean.getSystemMessage().addError("Please fill in both date and time");
							status.setFieldError(node, "dailyUidOut", "Please fill in both date and time");
							status.setFieldError(node, "timeIn", "Please fill in both date and time");
							commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
							return;
						}
					}
				}
				
				//if Day Date out is null set time to null else amend day date out to time out
				if(thisDailyEnd == null){
					//ticket number#15805 - prompt error if only Date Out is empty
					if(thisBitrun.getTimeIn() != null && thisBitrun.getTimeOut() != null && thisDailyStart != null) {
						status.setContinueProcess(false, true);
						commandBean.getSystemMessage().addError("Please fill in the Date/Time Out");
						status.setFieldError(node, "dailyUidOut", "Please fill in the Date/Time Out");
						commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
						return;
					}else {
						thisBitrun.setTimeOut(null);
					}
				}else{
					if(thisBitrun.getTimeOut() != null){
						Date thisDateOut = null;
						
						if(CommonDateParser.parse(thisDailyEnd.getDayDate().toString()) != null)
						{
							thisDateOut = CommonDateParser.parse(thisDailyEnd.getDayDate().toString());
						}
						else
						{
							thisDateOut = (Date) thisDailyEnd.getDayDate();
						}
						
						if(thisDateOut != null){
							this.setTimeField(thisBitrun, thisDateOut, "timeOut");						
						}
					}else{
						PropertyUtils.setProperty(object, "timeOut", thisDailyEnd.getDayDate());
					}
						
				}
			}
		}
		
	}
	
	@Override
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession session, CommandBeanTreeNode parent) throws Exception
	{
		if ("BhaComponent".equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted())
		{
			//Comparator to sort bha component list
			Collections.sort(status.getUpdatedNodes(), new BhaComponentSequenceComparator());
			
			this.calculateVolumeDisplacement(status.getUpdatedNodes(), session,parent);
			this.updateTotalWeightOfStringOnAir(status.getUpdatedNodes(), session, parent);
			this.calculateBhaTotalLength(status.getUpdatedNodes(), session, parent);
			this.calculateCumulativeWeightMassAndForce(status.getUpdatedNodes(), session, parent);
			this.calculateTotalJointsInTally(status.getUpdatedNodes(), session, parent);
			
			//18761 calculate Total Weight Dry, Total Weight Wet, Total Weight Below Jar Dry, Total Weight Below Jar Wet if GWP is on
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_WEIGHT_DRY_WET_BELOW_JAR_CALCULATION))) {
				//initialise stuff
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);
				CustomFieldUom thisConverter = new CustomFieldUom(parent.getCommandBean(),MudProperties.class, "mudWeight");
				String bharunUid = null;
				String operationUid = null;
				String bhaDailyidIn = null;
				String bhaDailyidOut = null;
				if (parent.getData() instanceof Bharun)
				{
					Bharun bha = (Bharun)parent.getData();
					bharunUid = bha.getBharunUid();
					operationUid = bha.getOperationUid();
					bhaDailyidIn = bha.getDailyidIn();
					bhaDailyidOut = bha.getDailyidOut();
				}
				
				//1. calculate buoyancy factor
				String currentDailyUid = null;
				if (session != null && session.getCurrentDailyUid() != null && !session.getCurrentDailyUid().trim().equals("")) {
					currentDailyUid = session.getCurrentDailyUid();
				}
				Double buoyancyFactor = calculateBuoyancyFactor(operationUid, bhaDailyidIn, bhaDailyidOut, currentDailyUid, qp, thisConverter);

				//2. calculate total weight dry
				//for total weight dry in bharun screen, can just take updated nodes of component to calculate, but when updating from mud properties screen, need to use sql script.
				//so must remember both bharun and mud properties need to be updated if there're any changes
				Double totalWeightDry = this.calculateTotalWeightDry(status.getUpdatedNodes(), parent, thisConverter);

				//3. calculate total weight wet
				Double totalWeightWet = null;
				if (buoyancyFactor != null && totalWeightDry != null) {
					totalWeightWet = totalWeightDry * buoyancyFactor;
				}
				
				//4. calculate total weight below jar dry
				Double totalWeightBelowJarDry = MudPropertiesUtil.calculateTotalWeightBelowJarDry(bharunUid, qp);
				
				//5. calculate total weight below jar wet
				Double totalWeightBelowJarWet = null;
				if (buoyancyFactor != null && totalWeightBelowJarDry != null) {
					totalWeightBelowJarWet = totalWeightBelowJarDry * buoyancyFactor;
				}
				
				//6. update the total weights into the bharun record
				//MudPropertiesUtil.updateTotalWeightsInBHA(totalWeightDry, totalWeightWet, totalWeightBelowJarDry, totalWeightBelowJarWet, bharunUid, qp);
				MudPropertiesUtil.updateTotalWeightsDryInBHA(totalWeightDry, totalWeightBelowJarDry, bharunUid, qp);
				if (session != null && session.getCurrentDailyUid() != null && !session.getCurrentDailyUid().trim().equals("")) {
					MudPropertiesUtil.updateTotalWeightsWetInBHA(totalWeightWet, totalWeightBelowJarWet, bharunUid, session.getCurrentDailyUid(), qp);	
				}

			}
		}
		if(("Bharun".equalsIgnoreCase(className) && status.isAnyNodeDeleted()) || ("BharunDailySummary".equalsIgnoreCase(className) && status.isAnyNodeDeleted()))
		{
			ArrayList list = new ArrayList();
			String[] downholeToolsUid = null;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			CustomFieldUom thisConverter = new CustomFieldUom(parent.getCommandBean());
			String bharunUid ="";
			for (CommandBeanTreeNode node:status.getDeletedNodes())
			{
				if("Bharun".equalsIgnoreCase(className))
				{
					Bharun bha = (Bharun)node.getData();
					bharunUid = bha.getBharunUid();
				}
				if("BharunDailySummary".equalsIgnoreCase(className))
				{
					BharunDailySummary dbsRecord = (BharunDailySummary)node.getData();
					bharunUid = dbsRecord.getBharunUid();
				}
				if((ArrayList) node.getDynaAttr().get("downholeToolsUid")!=null)
				{
					list = (ArrayList) node.getDynaAttr().get("downholeToolsUid");
					downholeToolsUid = (String[]) list.toArray(new String [list.size()]);
				}
				
				
				
				String strSql = "From Daily where (isDeleted = false or isDeleted is null) " +
				"and operationUid=:operationUid order by dayDate";
				
				List <Daily> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
						new String[] {"operationUid"},
						new Object[] {session.getCurrentOperationUid()}, qp);
				
				String strSql3 = "From DownholeTools where " +
				"downholeToolsUid in ('"+StringUtils.join(downholeToolsUid,"','")+"')";
				
				List <DownholeTools> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql3, qp);
				
				if(lstResult3!=null && lstResult3.size()>0)
				{
					for(DownholeTools downholetools:lstResult3)
					{
						
						if(lstResult4!=null && lstResult4.size()>0)
							{
								for(Daily daily:lstResult4)
								{								
									BharunUtils.recalculateCumulativeCircHour(daily.getDayDate(), downholetools.getToolSerialNumber(), downholetools.getTool(), session.getCurrentOperationUid(), thisConverter, bharunUid);
								}
							}
						
						
					}
					
				}		
			}
			
		}
		if("BharunDailySummary".equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted()) {
			//18761 calculate Total Weight Dry, Total Weight Wet, Total Weight Below Jar Dry, Total Weight Below Jar Wet if GWP is on
			//note, this is when continue from previous run is used, and bharun daily summary is saved (no change to bharun or component)
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_WEIGHT_DRY_WET_BELOW_JAR_CALCULATION))) {
				//initialise stuff
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);
				CustomFieldUom thisConverter = new CustomFieldUom(parent.getCommandBean(),MudProperties.class, "mudWeight");
				String bharunUid = null;
				String operationUid = null;
				String bhaDailyidIn = null;
				String bhaDailyidOut = null;
				if (parent.getData() instanceof Bharun)
				{
					Bharun bha = (Bharun)parent.getData();
					bharunUid = bha.getBharunUid();
					operationUid = bha.getOperationUid();
					bhaDailyidIn = bha.getDailyidIn();
					bhaDailyidOut = bha.getDailyidOut();
				}
				
				//1. calculate buoyancy factor
				String currentDailyUid = null;
				if (session != null && session.getCurrentDailyUid() != null && !session.getCurrentDailyUid().trim().equals("")) {
					currentDailyUid = session.getCurrentDailyUid();
				}
				Double buoyancyFactor = calculateBuoyancyFactor(operationUid, bhaDailyidIn, bhaDailyidOut, currentDailyUid, qp, thisConverter);

				//2. calculate total weight dry
				//for total weight dry in bharun screen, can just take updated nodes of component to calculate, but when updating from mud properties screen, need to use sql script.
				//so must remember both bharun and mud properties need to be updated if there're any changes
				//Double totalWeightDry = this.calculateTotalWeightDry(status.getUpdatedNodes(), parent, thisConverter);
				Double totalWeightDry = MudPropertiesUtil.calculateTotalWeightDry(bharunUid, qp, thisConverter);

				//3. calculate total weight wet
				Double totalWeightWet = null;
				if (buoyancyFactor != null && totalWeightDry != null) {
					totalWeightWet = totalWeightDry * buoyancyFactor;
				}
				
				//4. calculate total weight below jar dry
				Double totalWeightBelowJarDry = MudPropertiesUtil.calculateTotalWeightBelowJarDry(bharunUid, qp);
				
				//5. calculate total weight below jar wet
				Double totalWeightBelowJarWet = null;
				if (buoyancyFactor != null && totalWeightBelowJarDry != null) {
					totalWeightBelowJarWet = totalWeightBelowJarDry * buoyancyFactor;
				}
				
				//6. update the total weights into the bharun record
				//MudPropertiesUtil.updateTotalWeightsInBHA(totalWeightDry, totalWeightWet, totalWeightBelowJarDry, totalWeightBelowJarWet, bharunUid, qp);
				MudPropertiesUtil.updateTotalWeightsDryInBHA(totalWeightDry, totalWeightBelowJarDry, bharunUid, qp);
				if (session != null && session.getCurrentDailyUid() != null && !session.getCurrentDailyUid().trim().equals("")) {
					MudPropertiesUtil.updateTotalWeightsWetInBHA(totalWeightWet, totalWeightBelowJarWet, bharunUid, session.getCurrentDailyUid(), qp);	
				}
				
			}
		}
		if("DownholeTools".equalsIgnoreCase(className) && status.isAnyNodeDeleted())
		{
			ArrayList list = new ArrayList();
			String[] downholeToolsUid = null;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			CustomFieldUom thisConverter = new CustomFieldUom(parent.getCommandBean());
			String bharunUid="";
			
			Daily currDaily=ApplicationUtils.getConfiguredInstance().getCachedDaily(session.getCurrentDailyUid());
			for (CommandBeanTreeNode node:status.getDeletedNodes())
			{
				if((ArrayList) node.getDynaAttr().get("downholeToolsUid")!=null)
				{
					list = (ArrayList) node.getDynaAttr().get("downholeToolsUid");
					downholeToolsUid = (String[]) list.toArray(new String [list.size()]);
				}
				
				BharunDailySummary bdsRecord = (BharunDailySummary)node.getParent().getData();
				bharunUid = bdsRecord.getBharunUid();

				String strSql3 = "From DownholeTools where " +
				"downholeToolsUid in ('"+StringUtils.join(downholeToolsUid,"','")+"')";
				
				List <DownholeTools> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql3, qp);
				
				if(lstResult3!=null && lstResult3.size()>0)
				{
					for(DownholeTools downholetools:lstResult3)
					{
						
						BharunUtils.recalculateCumulativeCircHour(currDaily.getDayDate(), downholetools.getToolSerialNumber(), downholetools.getTool(), session.getCurrentOperationUid(), thisConverter, bharunUid);
						
					}
					
				}		
			}
			
			
		}
		
		if ("DownholeTools".equalsIgnoreCase(className) && status.isAnyNodeSaved()) {
			BharunDailySummary bds=(BharunDailySummary)parent.getData();
			String tool="";
			String serialNumber ="";
			String operationUid="";
			String dailyUid = "";
			String downholetoolSerialNumber="";
			String downholeToolRecord="";
			
			CustomFieldUom thisConverter = new CustomFieldUom(parent.getCommandBean());
			operationUid = session.getCurrentOperationUid();
			dailyUid = session.getCurrentDailyUid();
			for (CommandBeanTreeNode node:status.getUpdatedNodes())
			{
				if (node.getData() instanceof DownholeTools)
				{
					DownholeTools downholetools = (DownholeTools)node.getData();
					if(node.getDynaAttr().get("serialNumber")!=null)
					{
						serialNumber = node.getDynaAttr().get("serialNumber").toString();
					}
					if(node.getDynaAttr().get("tool")!=null)
					{
						tool = node.getDynaAttr().get("tool").toString();
					}
					if(downholetools.getToolSerialNumber()!=null)
					{
						downholetoolSerialNumber = downholetools.getToolSerialNumber();
					}
					if(downholetools.getTool()!=null)
					{
						downholeToolRecord = downholetools.getTool();
					}
					if(downholetoolSerialNumber.equalsIgnoreCase(serialNumber))
					{
						if(downholeToolRecord.equalsIgnoreCase(tool))
						{
							BharunUtils.calculateCumulativeCircHour(dailyUid, downholetools.getToolSerialNumber(), downholetools.getTool(), operationUid, thisConverter,bds.getBharunUid(),downholetools.getDownholeToolsUid());
						}
						else
						{
							BharunUtils.calculateCumulativeCircHour(dailyUid, downholetools.getToolSerialNumber(), tool, operationUid, thisConverter,bds.getBharunUid(),downholetools.getDownholeToolsUid());
							BharunUtils.calculateCumulativeCircHour(dailyUid, downholetools.getToolSerialNumber(), downholetools.getTool(), operationUid, thisConverter,bds.getBharunUid(),downholetools.getDownholeToolsUid());
							
						}
					}
					else
					{
						if(downholeToolRecord.equalsIgnoreCase(tool))
						{
							BharunUtils.calculateCumulativeCircHour(dailyUid, serialNumber, downholetools.getTool(), operationUid, thisConverter,bds.getBharunUid(),downholetools.getDownholeToolsUid());
							BharunUtils.calculateCumulativeCircHour(dailyUid, downholetools.getToolSerialNumber(), downholetools.getTool(), operationUid, thisConverter,bds.getBharunUid(),downholetools.getDownholeToolsUid());
							
						}
						else
						{
							BharunUtils.calculateCumulativeCircHour(dailyUid, serialNumber, tool, operationUid, thisConverter,bds.getBharunUid(),downholetools.getDownholeToolsUid());
							BharunUtils.calculateCumulativeCircHour(dailyUid, downholetools.getToolSerialNumber(), downholetools.getTool(), operationUid, thisConverter,bds.getBharunUid(),downholetools.getDownholeToolsUid());
							
						}
						
					}
				}
				
			}
			
			
		}
	}
	
	private void calculateTotalJointsInTally(Collection<CommandBeanTreeNode> nodes, UserSession session, CommandBeanTreeNode parent) throws Exception{
		
		String bharunUid=null;
		if (parent.getData() instanceof Bharun)
		{
			Bharun bha=(Bharun)parent.getData();
			bharunUid=bha.getBharunUid();
		}
		if (parent.getData() instanceof Bharun)
		{
			Integer totalJointsInTally = 0;

			for(CommandBeanTreeNode node:nodes)
			{
				if (node.getData() instanceof BhaComponent)
				{
					BhaComponent comp=(BhaComponent) node.getData();
					if (comp.getJointNumber()!=null && "1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_BHA_DETAIL_TOTAL_JOINTS_AS_MULTIPLIER))) {
						totalJointsInTally+=comp.getJointNumber();
					}else{
						totalJointsInTally++;
					}
				}
			}
			String strSql1 = "UPDATE Bharun SET totalJointNumber =:totalJointsInTally WHERE bharunUid =:bharunUid";
			String[] paramsFields1 = {"totalJointsInTally", "bharunUid"};
			Object[] paramsValues1 = {totalJointsInTally, bharunUid};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1);
		}
	}
	
	private void calculateCumulativeWeightMassAndForce(Collection<CommandBeanTreeNode> nodes, UserSession session, CommandBeanTreeNode parent) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		CustomFieldUom thisConverter=new CustomFieldUom(parent.getCommandBean());

		if (parent.getData() instanceof Bharun)
		{
			Double cumWeightForce = 0.00;
			Double cumWeightMass = 0.00;

			for(CommandBeanTreeNode node:nodes)
			{
				if (node.getData() instanceof BhaComponent)
				{
					BhaComponent comp=(BhaComponent) node.getData();
					if (comp.getWeightForce()!=null)
					{
						thisConverter.setReferenceMappingField(BhaComponent.class, "weightForce");
						thisConverter.setBaseValueFromUserValue(comp.getWeightForce());
						
						cumWeightForce+=thisConverter.getBasevalue();
					}
					if (comp.getWeightMass()!=null)
					{
						thisConverter.setReferenceMappingField(BhaComponent.class, "weightMass");
						thisConverter.setBaseValueFromUserValue(comp.getWeightMass());
						cumWeightMass+=thisConverter.getBasevalue();
					}
					String strSql1 = "UPDATE BhaComponent SET cumWeightForce =:cumWeightForce, cumWeightMass=:cumWeightMass WHERE bhaComponentUid =:bhaComponentUid";
					String[] paramsFields = {"cumWeightForce","cumWeightMass", "bhaComponentUid"};
					Object[] paramsValues = {cumWeightForce,cumWeightMass, comp.getBhaComponentUid()};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields, paramsValues, qp);
				}
			}
		}
	}
	
	private void calculateBhaTotalLength(Collection<CommandBeanTreeNode> nodes, UserSession session, CommandBeanTreeNode parent) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String bharunUid=null;
		if (parent.getData() instanceof Bharun)
		{
			Bharun bha=(Bharun)parent.getData();
			bharunUid=bha.getBharunUid();
		}
		Double bhaTotalLength = 0.00;
		CustomFieldUom thisConverter=null;
		for(CommandBeanTreeNode node:nodes)
		{
			
			if (node.getData() instanceof BhaComponent)
			{
				BhaComponent comp=(BhaComponent) node.getData();
				Integer jointNumber=comp.getJointNumber();
				
				Double jointLength=comp.getJointlength();
				if (jointLength!=null)
				{
					thisConverter = new CustomFieldUom(session.getUserLocale(),BhaComponent.class, "jointlength");
					thisConverter.setBaseValueFromUserValue(jointLength);
					jointLength=thisConverter.getBasevalue();
				}
				
				if (jointLength != null ) {
					if (jointNumber != null && "1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_BHA_DETAIL_TOTAL_JOINTS_AS_MULTIPLIER))) {
						jointLength=jointLength*jointNumber;
					}
					
					bhaTotalLength+=jointLength;
				}
				if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_CALCULATE_BHA_DETAIL_CUM_LENGTH)))
				{
					String strSql1 = "UPDATE BhaComponent SET cumjointlength =:cumjointlength WHERE bhaComponentUid =:bhaComponentUid";
					String[] paramsFields = {"cumjointlength", "bhaComponentUid"};
					Object[] paramsValues = {bhaTotalLength, comp.getBhaComponentUid()};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields, paramsValues, qp);
				}
			}
			
		}
		if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_TRANSFER_BHA_DETAIL_LENGTH_TO_TOTAL_LENGTH)))
		{
			String strSql1 = "UPDATE Bharun SET bhaTotalLength =:bhaTotalLength WHERE bharunUid =:bharunUid";
			String[] paramsFields1 = {"bhaTotalLength", "bharunUid"};
			Object[] paramsValues1 = {bhaTotalLength, bharunUid};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
		}		
		
	}
	
	private void updateTotalWeightOfStringOnAir(Collection<CommandBeanTreeNode> nodes, UserSession session, CommandBeanTreeNode parent) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String bharunUid=null;
		if (parent.getData() instanceof Bharun)
		{
			Bharun bha=(Bharun)parent.getData();
			bharunUid=bha.getBharunUid();
		}
		Double weightStringForce = 0.00;
		CustomFieldUom thisConverter=new CustomFieldUom(session.getUserLocale(),BhaComponent.class,"weightForce");
		for(CommandBeanTreeNode node:nodes)
		{
			if (node.getData() instanceof BhaComponent)
			{
				BhaComponent comp=(BhaComponent) node.getData();
				if (comp.getWeightForce()!=null)
				{
					thisConverter.setBaseValueFromUserValue(comp.getWeightForce());
					weightStringForce+=thisConverter.getBasevalue();
				}
			}
			
		}
		String strSql1 = "UPDATE Bharun SET weightStringForce =:weightStringForce WHERE bharunUid =:thisBharunUid";
		String[] paramsFields1 = {"weightStringForce", "thisBharunUid"};
		Object[] paramsValues1 = {weightStringForce, bharunUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
		
	}
	
	private void calculateVolumeDisplacement(Collection<CommandBeanTreeNode> nodes, UserSession session, CommandBeanTreeNode parent) throws Exception {
			
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String bharunUid=null;
		Double cumOpenEndVolumeDisplaced = 0.00;
		CustomFieldUom thisConverter=null;
		if (parent.getData() instanceof Bharun)
		{
			Bharun bha=(Bharun)parent.getData();
			bharunUid=bha.getBharunUid();
		}
		
		for(CommandBeanTreeNode node:nodes)
		{
			if (node.getData() instanceof BhaComponent )
			{
				BhaComponent comp=(BhaComponent) node.getData();
				
				Double volumeDisplacement=null;
				String bhaComponentUid=comp.getBhaComponentUid();
				Integer jointNumber=comp.getJointNumber();
				
				Double jointLength=comp.getJointlength();
				thisConverter = new CustomFieldUom(session.getUserLocale(),BhaComponent.class, "jointLength");
				if (jointLength!=null)
				{
					thisConverter.setBaseValueFromUserValue(jointLength);
					jointLength=thisConverter.getBasevalue();
				}
				Double od=comp.getComponentOd();
				if (od!=null)
				{
					thisConverter.setReferenceMappingField(BhaComponent.class, "componentOd");
					thisConverter.setBaseValueFromUserValue(od);
					od=thisConverter.getBasevalue();
				}
				Double id=comp.getComponentId();
				if (id!=null)
				{
					thisConverter.setReferenceMappingField(BhaComponent.class, "componentId");
					thisConverter.setBaseValueFromUserValue(id);
					id=thisConverter.getBasevalue();
				}	
				if (jointLength != null && od!=null && id!=null) {
					if (jointNumber != null && "1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_BHA_DETAIL_TOTAL_JOINTS_AS_MULTIPLIER))) {
						jointLength=jointLength*jointNumber;
					}
					volumeDisplacement=jointLength*(pi/4)*((od*od)-(id*id));
					cumOpenEndVolumeDisplaced+=volumeDisplacement;
				}
				
				String strSql1 = "UPDATE BhaComponent SET cumOpenEndVolumeDisplaced =:cumOpenEndVolumeDisplaced WHERE bhaComponentUid =:bhaComponentUid";
				String[] paramsFields = {"cumOpenEndVolumeDisplaced", "bhaComponentUid"};
				Object[] paramsValues = {cumOpenEndVolumeDisplaced, bhaComponentUid};
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields, paramsValues, qp);
				
			}
		}
		String strSql1 = "UPDATE Bharun SET totalOpenEndVolumeDisplaced =:cumOpenEndVolumeDisplaced WHERE bharunUid =:bharunUid";
		String[] paramsFields1 = {"cumOpenEndVolumeDisplaced", "bharunUid"};
		Object[] paramsValues1 = {cumOpenEndVolumeDisplaced, bharunUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
				
		
	}

	public void beforeDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception
	{
		Object obj=node.getData();
		
		// pick up the bharunUid if the record that is flag to deleted. The bharunUid is used for comparison of holeSection Data to get the holeSectionUid
		if (obj instanceof Bharun)
		{
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_WELLBORE_PROPERTIES)))
				validateHoleSection(obj);
			
			// flag the recorded hour used for the serialize bha component
			Bharun bharun = (Bharun) obj;
			String bharunUid = bharun.getBharunUid();
			
			String strSql = "FROM BharunDailySummary WHERE (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid";
			List<BharunDailySummary> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"bharunUid"}, new Object[]{bharunUid});
			for (BharunDailySummary bhaDailySummary : list) {
				String dailyUid = bhaDailySummary.getDailyUid();
				if(StringUtils.isNotEmpty(dailyUid) && dailyUid != null){
					String strSql2 = "FROM BhaComponent WHERE (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid";
					List<BhaComponent> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, new String[] {"bharunUid"}, new Object[]{bharunUid});
					for (BhaComponent bhaComponent : list2) {
						String bhaComponentUid = bhaComponent.getBhaComponentUid();
						String[] paramsFields = {"bhaComponentUid", "dailyUid"};
						Object[] paramsValues = new Object[2]; 
						paramsValues[0] = bhaComponentUid; 
						paramsValues[1] = dailyUid;
						String strSqlDelete = "Update BhaComponentHoursUsed Set isDeleted = true where (isDeleted = false or isDeleted is null) AND dailyUid=:dailyUid AND bhaComponentUid=:bhaComponentUid";
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlDelete, paramsFields, paramsValues);
					}
				}
			}		

		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object obj=node.getData();
		
		if (holeSectionUid!=null){
			
			if (obj instanceof Bharun)
			{
				// flag the recorded holeSection to deleted
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_WELLBORE_PROPERTIES)))
				{
					QueryProperties qp = new QueryProperties();
					
					qp.setUomConversionEnabled(false);
					qp.setDatumConversionEnabled(false);
					
					String[] fields = { "holeSectionUid"};
					Object[] values = new Object[1];  
					values[0] = holeSectionUid;
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("Update HoleSection Set isDeleted=true  where holeSectionUid=:holeSectionUid", fields, values,qp);
					holeSectionUid=null;
				}
			}
		}
		// flag the recorded hour used for the serialize bha component
		if (obj instanceof BharunDailySummary)
		{
			BharunDailySummary bhaDaily = (BharunDailySummary) obj;
			//	Delete related bha_component_hours_used records for the deleted daily summary of specific bha.
			String dailyUid = bhaDaily.getDailyUid();
			String bharunUid = bhaDaily.getBharunUid();
			if((StringUtils.isNotEmpty(dailyUid) && dailyUid != null) && (StringUtils.isNotEmpty(bharunUid) && bharunUid != null)){
				
				//get those affected bha_component_uid for later total_hours_used update usage after daily hour used is deleted
				String[] paramsFields2 = {"dailyUid", "bharunUid"};
				Object[] paramsValues2 = new Object[2]; 
				paramsValues2[0] = dailyUid; 
				paramsValues2[1] = bharunUid;
				String strSql2 = "SELECT distinct bhaComponentUid " +
					"FROM BhaComponentHoursUsed " +
					"WHERE (isDeleted = false or isDeleted is null) " +
					"AND dailyUid = :dailyUid AND bharunUid = :bharunUid";
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				
				// Set those related bha_component_hours_used records as deleted
				String[] paramsFields = {"dailyUid", "bharunUid"};
				Object[] paramsValues = new Object[2]; 
				paramsValues[0] = dailyUid;
				paramsValues[1] = bharunUid;
				String strSql = "Update BhaComponentHoursUsed Set isDeleted = true where (isDeleted = false or isDeleted is null) AND dailyUid = :dailyUid AND bharunUid = :bharunUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
				
				// Then, recalculate for those affected components' total_hours_used & update it
				if (lstResult2.size() >0) {
					String bhaComponentUid = null;
					for (Object bhaComponentResult : lstResult2) {
						if(StringUtils.isNotBlank(bhaComponentResult.toString())){
							bhaComponentUid = bhaComponentResult.toString();
							BharunUtils.calcBhaComponentTotalHoursUsed(bharunUid, bhaComponentUid);							
						}
					}				
				}
			}
			// End of Delete related bha_component_hours_used records for the deleted bha daily summary of specific bha.	
			
			// check if only daily summary associate to the bha run 
			String sql = "SELECT bharunDailySummaryUid " +
				"FROM BharunDailySummary " +
				"WHERE (isDeleted = false or isDeleted is null) " +
				"AND (sysDeleted is null) " +
				"AND bharunUid = :bharunUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"bharunUid"}, new Object[]{bharunUid});
			if (lstResult.size() == 0){
				// flag sys_deleted to bharun daily summary
				sql = "Update BharunDailySummary Set sysDeleted = 1 where (isDeleted = true) AND dailyUid = :dailyUid AND bharunUid = :bharunUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sql, new String[] {"dailyUid", "bharunUid"}, new Object[]{dailyUid,bharunUid});
				
				// flag sys_deleted, is_deleted to bharun
				sql = "Update Bharun Set isDeleted =true, sysDeleted = 1 where (isDeleted = false or isDeleted is null) AND bharunUid = :bharunUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sql, new String[] {"bharunUid"}, new Object[]{bharunUid});
			}

		}
		/* Ticket: 20110216-0405-jwong, as told by Jee(SA) to comment off this logic which is incorrect for cumulative hour used calculation if there exists more than 1 bharun with same serial # at same day
		if (obj instanceof BhaComponent)
		{
			BhaComponent bhaComp = (BhaComponent) obj;
			//	Delete related bha_component_hours_used records for the deleted bha component.
			String bhaComponentUid = bhaComp.getBhaComponentUid();
			if(StringUtils.isNotEmpty(bhaComponentUid) && bhaComponentUid != null){
				String[] paramsFields = {"bhaComponentUid"};
				Object[] paramsValues = new Object[1]; 
				paramsValues[0] = bhaComponentUid;
				String strSql = "Update BhaComponentHoursUsed Set isDeleted = '1' where (isDeleted = false or isDeleted is null) AND bhaComponentUid = :bhaComponentUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
			}
			// End of Delete related bha_component_hours_used records for the deleted bha component.
		}
		*/
				
		if (obj instanceof BhaComponent)
		{
			BhaComponent bhaComp = (BhaComponent) obj;
			//	Delete related inventory service log records for the deleted bha component.
			String bhaComponentUid = bhaComp.getBhaComponentUid();
			if(StringUtils.isNotEmpty(bhaComponentUid) && bhaComponentUid != null){
				String[] paramsFields = {"bhaComponentUid"};
				Object[] paramsValues = new Object[1]; paramsValues[0] = bhaComponentUid;
				String strSql = "Update InventoryServiceLog Set isDeleted = true where (isDeleted = false or isDeleted is null) AND bhaComponentUid = :bhaComponentUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
			}
			// End of Delete related inventory service log records for the deleted bha component.
			
			//Delete related bha_component_hours_used records for the deleted bha component.			
			if(StringUtils.isNotEmpty(bhaComponentUid) && bhaComponentUid != null){
				String[] paramsFields2 = {"bhaComponentUid"};
				Object[] paramsValues2 = new Object[1]; 
				paramsValues2[0] = bhaComponentUid;
				String strSql2 = "Update BhaComponentHoursUsed Set isDeleted = true where (isDeleted = false or isDeleted is null) AND bhaComponentUid = :bhaComponentUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields2, paramsValues2);
			}
			// End of Delete related bha_component_hours_used records for the deleted bha component.
		}
		
		//re-calculate and save jet velocity after delete
		this.saveJetVelocity(node);
	}
	
	//ticket number#15805 - validate for specific client
	 public boolean isDateTimeValidation() {
	        return dateTimeValidation;
	    }
	//ticket number#15805 - validate for specific client
	 public void setDateTimeValidation(boolean dateTimeValidation) {
	        this.dateTimeValidation = dateTimeValidation;
	    }
	
	
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		
		
		
		if (obj instanceof Bharun) {
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			thisConverter.setReferenceMappingField(DownholeTools.class, "dailyCircHrs");
			Daily currDaily=ApplicationUtils.getConfiguredInstance().getCachedDaily(session.getCurrentDailyUid());
			Bharun bha = (Bharun)node.getData();
				if(node.getDynaAttr().get("depthIn")!=null)
				{
					if(bha.getDepthInMdMsl()!=null)
					{
						if(bha.getDepthInMdMsl()!=Double.parseDouble(node.getDynaAttr().get("depthIn").toString()))
						{
							String strSql3 = "Select dt.toolSerialNumber From Bharun b, BharunDailySummary bds, DownholeTools dt where (b.isDeleted is null or b.isDeleted=false) and (bds.isDeleted is null or bds.isDeleted=false) and (dt.isDeleted is null or dt.isDeleted=false) and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and bds.bharunUid=b.bharunUid and " +
							"bds.dailyUid=:dailyUid and b.bharunUid=:bharunUid and dt.tool='jar' and b.operationUid=:operationUid group by dt.toolSerialNumber";
							
							List <Object> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, new String[] {"dailyUid","operationUid","bharunUid"}, new Object[] {session.getCurrentDailyUid(), session.getCurrentOperationUid(),bha.getBharunUid()}, qp);
							
							if(lstResult3!=null && lstResult3.size()>0)
							{
								for(Object bhaRecord:lstResult3)
								{
									Double totalDailyCircHour = 0.0;
									String strSql4 = "Select dt.dailyCircHrs,dt.downholeToolsUid From Bharun b, BharunDailySummary bds, DownholeTools dt, Daily d where (d.isDeleted is null or d.isDeleted=false) and (b.isDeleted is null or b.isDeleted=false) and (bds.isDeleted is null or bds.isDeleted=false) and (dt.isDeleted is null or dt.isDeleted=false) and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and d.dailyUid=bds.dailyUid and bds.bharunUid=b.bharunUid and " +
									"d.dayDate<=:dayDate and dt.tool='jar' and b.operationUid=:operationUid and dt.toolSerialNumber=:toolSerialNumber order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
										
										List <Object[]> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, new String[] {"dayDate","operationUid","toolSerialNumber"}, new Object[] {currDaily.getDayDate(), session.getCurrentOperationUid(), bhaRecord.toString()}, qp);
										if(lstResult4!=null && lstResult4.size()>0)
										{
											for(Object[] downholetool:lstResult4)
											{
												String strSql = "From DownholeTools where (isDeleted is null or isDeleted = false) and " +
												"downholeToolsUid=:downholeToolsUid";
												
												List <DownholeTools> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
														new String[] {"downholeToolsUid"},
														new Object[] {downholetool[1].toString()}, qp);
												if(lstResult5!=null && lstResult5.size()>0)
												{
													for(DownholeTools downholetools:lstResult5)
													{
														if(downholetool[0]!=null)
														{
															thisConverter.setBaseValue(Double.parseDouble(downholetool[0].toString()));
															totalDailyCircHour += thisConverter.getBasevalue();
															
														}
														PropertyUtils.setProperty(downholetools, "cumCircHrs", totalDailyCircHour);
														ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetools, qp);
													}
													
												}
												
											}
										}
								}
								
								
							}
							
							
						}
					}
					else
					{
						String strSql3 = "Select dt.toolSerialNumber From Bharun b, BharunDailySummary bds, DownholeTools dt where (b.isDeleted is null or b.isDeleted=false) and (bds.isDeleted is null or bds.isDeleted=false) and (dt.isDeleted is null or dt.isDeleted=false) and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and bds.bharunUid=b.bharunUid and " +
						"bds.dailyUid=:dailyUid and b.bharunUid=:bharunUid and dt.tool='jar' and b.operationUid=:operationUid group by dt.toolSerialNumber";
						
						List <Object> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, new String[] {"dailyUid","operationUid","bharunUid"}, new Object[] {session.getCurrentDailyUid(), session.getCurrentOperationUid(),bha.getBharunUid()}, qp);
						
						if(lstResult3!=null && lstResult3.size()>0)
						{
							for(Object bhaRecord:lstResult3)
							{
								Double totalDailyCircHour = 0.0;
								String strSql4 = "Select dt.dailyCircHrs,dt.downholeToolsUid From Bharun b, BharunDailySummary bds, DownholeTools dt, Daily d where (d.isDeleted is null or d.isDeleted=false) and (b.isDeleted is null or b.isDeleted=false) and (bds.isDeleted is null or bds.isDeleted=false) and (dt.isDeleted is null or dt.isDeleted=false) and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and d.dailyUid=bds.dailyUid and bds.bharunUid=b.bharunUid and " +
								"d.dayDate<=:dayDate and dt.tool='jar' and b.operationUid=:operationUid and dt.toolSerialNumber=:toolSerialNumber order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
									
									List <Object[]> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, new String[] {"dayDate","operationUid","toolSerialNumber"}, new Object[] {currDaily.getDayDate(), session.getCurrentOperationUid(), bhaRecord.toString()}, qp);
									if(lstResult4!=null && lstResult4.size()>0)
									{
										for(Object[] downholetool:lstResult4)
										{
											String strSql = "From DownholeTools where (isDeleted is null or isDeleted = false) and " +
											"downholeToolsUid=:downholeToolsUid";
											
											List <DownholeTools> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
													new String[] {"downholeToolsUid"},
													new Object[] {downholetool[1].toString()}, qp);
											if(lstResult5!=null && lstResult5.size()>0)
											{
												for(DownholeTools downholetools:lstResult5)
												{
													if(downholetool[0]!=null)
													{
														thisConverter.setBaseValue(Double.parseDouble(downholetool[0].toString()));
														totalDailyCircHour += thisConverter.getBasevalue();
														
													}
													PropertyUtils.setProperty(downholetools, "cumCircHrs", totalDailyCircHour);
													ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetools, qp);
												}
												
											}
											
										}
									}
							}
							
							
						}
					}
					
				}
				else
				{
					if(bha.getDepthInMdMsl()!=null)
					{
						String strSql3 = "Select dt.toolSerialNumber From Bharun b, BharunDailySummary bds, DownholeTools dt where (b.isDeleted is null or b.isDeleted=false) and (bds.isDeleted is null or bds.isDeleted=false) and (dt.isDeleted is null or dt.isDeleted=false) and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and bds.bharunUid=b.bharunUid and " +
						"bds.dailyUid=:dailyUid and b.bharunUid=:bharunUid and dt.tool='jar' and b.operationUid=:operationUid group by dt.toolSerialNumber";
						
						List <Object> lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, new String[] {"dailyUid","operationUid","bharunUid"}, new Object[] {session.getCurrentDailyUid(), session.getCurrentOperationUid(),bha.getBharunUid()}, qp);
						
						if(lstResult3!=null && lstResult3.size()>0)
						{
							for(Object bhaRecord:lstResult3)
							{
								Double totalDailyCircHour = 0.0;
								String strSql4 = "Select dt.dailyCircHrs,dt.downholeToolsUid From Bharun b, BharunDailySummary bds, DownholeTools dt, Daily d where (d.isDeleted is null or d.isDeleted=false) and (b.isDeleted is null or b.isDeleted=false) and (bds.isDeleted is null or bds.isDeleted=false) and (dt.isDeleted is null or dt.isDeleted=false) and dt.bharunDailySummaryUid=bds.bharunDailySummaryUid and d.dailyUid=bds.dailyUid and bds.bharunUid=b.bharunUid and " +
								"d.dayDate<=:dayDate and dt.tool='jar' and b.operationUid=:operationUid and dt.toolSerialNumber=:toolSerialNumber order by d.dayDate, b.depthInMdMsl, b.bhaRunNumber";
									
									List <Object[]> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, new String[] {"dayDate","operationUid","toolSerialNumber"}, new Object[] {currDaily.getDayDate(), session.getCurrentOperationUid(), bhaRecord.toString()}, qp);
									if(lstResult4!=null && lstResult4.size()>0)
									{
										for(Object[] downholetool:lstResult4)
										{
											String strSql = "From DownholeTools where (isDeleted is null or isDeleted = false) and " +
											"downholeToolsUid=:downholeToolsUid";
											
											List <DownholeTools> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
													new String[] {"downholeToolsUid"},
													new Object[] {downholetool[1].toString()}, qp);
											if(lstResult5!=null && lstResult5.size()>0)
											{
												for(DownholeTools downholetools:lstResult5)
												{
													if(downholetool[0]!=null)
													{
														thisConverter.setBaseValue(Double.parseDouble(downholetool[0].toString()));
														totalDailyCircHour += thisConverter.getBasevalue();
														
													}
													PropertyUtils.setProperty(downholetools, "cumCircHrs", totalDailyCircHour);
													ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(downholetools, qp);
												}
												
											}
											
										}
									}
							}
							
							
						}
					}
				}
				
				//18761 calculate Total Weight Dry, Total Weight Wet, Total Weight Below Jar Dry, Total Weight Below Jar Wet if GWP is on
				if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_WEIGHT_DRY_WET_BELOW_JAR_CALCULATION))) {
					//initialise stuff
					qp = new QueryProperties();
					qp.setUomConversionEnabled(false);
					thisConverter = new CustomFieldUom(commandBean,MudProperties.class, "mudWeight");

					if (bha.isPropertyModified("dailyidIn") || bha.isPropertyModified("dailyidOut")) {
						String currentDailyUid = null;
						if (session.getCurrentDailyUid() != null) {
							currentDailyUid = session.getCurrentDailyUid();
						}
						//1. calculate buoyancy factor
						//Double buoyancyFactor = calculateBuoyancyFactor(bha.getOperationUid(), bha.getDailyidIn(), bha.getDailyidOut(), qp, thisConverter);
						Double buoyancyFactor = calculateBuoyancyFactor(bha.getOperationUid(), bha.getDailyidIn(), bha.getDailyidOut(), currentDailyUid, qp, thisConverter);

						//2. calculate total weight dry
						Double totalWeightDry = MudPropertiesUtil.calculateTotalWeightDry(bha.getBharunUid(), qp, thisConverter);

						//3. calculate total weight wet
						Double totalWeightWet = null;
						if (buoyancyFactor != null && totalWeightDry != null) {
							totalWeightWet = totalWeightDry * buoyancyFactor;
						}
						
						//4. calculate total weight below jar dry
						Double totalWeightBelowJarDry = MudPropertiesUtil.calculateTotalWeightBelowJarDry(bha.getBharunUid(), qp);
						
						//5. calculate total weight below jar wet
						Double totalWeightBelowJarWet = null;
						if (buoyancyFactor != null && totalWeightBelowJarDry != null) {
							totalWeightBelowJarWet = totalWeightBelowJarDry * buoyancyFactor;
						}
						
						//6. update the total weights into the bharun record
						//MudPropertiesUtil.updateTotalWeightsInBHA(totalWeightDry, totalWeightWet, totalWeightBelowJarDry, totalWeightBelowJarWet, bha.getBharunUid(), qp);
						MudPropertiesUtil.updateTotalWeightsDryInBHA(totalWeightDry, totalWeightBelowJarDry, bha.getBharunUid(), qp);
						if (session != null && session.getCurrentDailyUid() != null && !session.getCurrentDailyUid().trim().equals("")) {
							MudPropertiesUtil.updateTotalWeightsWetInBHA(totalWeightWet, totalWeightBelowJarWet, bha.getBharunUid(), session.getCurrentDailyUid(), qp);	
						}
					}
				}
		}
		if(obj instanceof Bharun && (BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW == operationPerformed || BaseCommandBean.OPERATION_PERFORMED_SAVE_EXISTING == operationPerformed || BaseCommandBean.OPERATION_PERFORMED_SAVE_IMPORTED == operationPerformed)) {
			Bharun bharun = (Bharun)obj;
			boolean isBharunDailySummaryExist = false;
			Daily thisDailyIn=ApplicationUtils.getConfiguredInstance().getCachedDaily(bharun.getDailyidIn());
			Daily thisDailyOut=ApplicationUtils.getConfiguredInstance().getCachedDaily(bharun.getDailyidOut());
			
			ActivityUtils.populateActivityBharunNumber(node, new UserSelectionSnapshot(session));
			
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_WELLBORE_PROPERTIES)))
			{
				CustomFieldUom thisConverter=null;
				Double depthInMdMsl=null;
				Double depthOutMdMsl=null;
				Double maxOd=null;
				
				if (bharun.getDepthInMdMsl()!=null){
					thisConverter = new CustomFieldUom(commandBean,Bharun.class, "depthInMdMsl");
					thisConverter.setBaseValueFromUserValue(bharun.getDepthInMdMsl());
					depthInMdMsl=thisConverter.getBasevalue();
				}
				
				if (bharun.getDepthOutMdMsl()!=null){
					thisConverter.setReferenceMappingField(Bharun.class, "depthOutMdMsl");
					thisConverter.setBaseValueFromUserValue(bharun.getDepthOutMdMsl());
					depthOutMdMsl=thisConverter.getBasevalue();
				}
				
				if (bharun.getDrillStringSummaryMaxOd()!=null){
					thisConverter.setReferenceMappingField(Bharun.class, "drillStringSummaryMaxOd");
					thisConverter.setBaseValueFromUserValue(bharun.getDrillStringSummaryMaxOd());
					maxOd=thisConverter.getBasevalue();
				}
				
				validateBharun(bharun.getWellboreUid(),depthInMdMsl,depthOutMdMsl,maxOd,(thisDailyIn!=null?thisDailyIn.getDayDate():null),(thisDailyOut!=null?thisDailyOut.getDayDate():null));
			}
			for(Iterator it=node.getList().values().iterator(); it.hasNext();) {
				if(isBharunDailySummaryExist) break;
				
				Map items = (Map)it.next();
				for(Iterator data=items.values().iterator(); data.hasNext();) {
					CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) data.next();
					Object childObj = childNode.getData();
					if(childObj instanceof BharunDailySummary) {
						isBharunDailySummaryExist = true;						
						break;
					}
				}
			}
			
			if(! isBharunDailySummaryExist) {
				String dailyUid = session.getCurrentDailyUid();
				// daily uid is blank only during depot addtostore of tubular object.
				if (StringUtils.isNotBlank(dailyUid)) {
					BharunDailySummary bhaDailySummary = new BharunDailySummary();
					bhaDailySummary.setBharunUid(bharun.getBharunUid());
					bhaDailySummary.setGroupUid(session.getCurrentGroupUid());
					bhaDailySummary.setDailyUid(session.getCurrentDailyUid());
					bhaDailySummary.setJarRotatingHours(0.0);
					
					if ("0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "calc_annvel"))) {
						this.setAnnvel(node, bhaDailySummary);					
					}
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bhaDailySummary);
				}
			}
			
			//18761 calculate Total Weight Dry, Total Weight Wet, Total Weight Below Jar Dry, Total Weight Below Jar Wet if GWP is on
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_WEIGHT_DRY_WET_BELOW_JAR_CALCULATION))) {
				//initialise stuff
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean,MudProperties.class, "mudWeight");

				//if mud properties is created before bharun, then it should be populated
				String currentDailyUid = null;
				if (session.getCurrentDailyUid() != null) {
					currentDailyUid = session.getCurrentDailyUid();
				}
				//1. calculate buoyancy factor
				//Double buoyancyFactor = calculateBuoyancyFactor(bha.getOperationUid(), bha.getDailyidIn(), bha.getDailyidOut(), qp, thisConverter);
				Double buoyancyFactor = calculateBuoyancyFactor(bharun.getOperationUid(), bharun.getDailyidIn(), bharun.getDailyidOut(), currentDailyUid, qp, thisConverter);

				//2. calculate total weight dry
				Double totalWeightDry = MudPropertiesUtil.calculateTotalWeightDry(bharun.getBharunUid(), qp, thisConverter);

				//3. calculate total weight wet
				Double totalWeightWet = null;
				if (buoyancyFactor != null && totalWeightDry != null) {
					totalWeightWet = totalWeightDry * buoyancyFactor;
				}

				//4. calculate total weight below jar dry
				Double totalWeightBelowJarDry = MudPropertiesUtil.calculateTotalWeightBelowJarDry(bharun.getBharunUid(), qp);

				//5. calculate total weight below jar wet
				Double totalWeightBelowJarWet = null;
				if (buoyancyFactor != null && totalWeightBelowJarDry != null) {
					totalWeightBelowJarWet = totalWeightBelowJarDry * buoyancyFactor;
				}

				//6. update the total weights into the bharun record
				//MudPropertiesUtil.updateTotalWeightsInBHA(totalWeightDry, totalWeightWet, totalWeightBelowJarDry, totalWeightBelowJarWet, bha.getBharunUid(), qp);
				MudPropertiesUtil.updateTotalWeightsDryInBHA(totalWeightDry, totalWeightBelowJarDry, bharun.getBharunUid(), qp);
				if (session != null && session.getCurrentDailyUid() != null && !session.getCurrentDailyUid().trim().equals("")) {
					MudPropertiesUtil.updateTotalWeightsWetInBHA(totalWeightWet, totalWeightBelowJarWet, bharun.getBharunUid(), session.getCurrentDailyUid(), qp);	
				}
			}
		}
		//re-calculate and save jet velocity after update
		this.saveJetVelocity(node);

		//re-calculate and save the bit depth drilled in & out and bit hours in & out 
		if (obj instanceof Bitrun) {
			CommonUtil.getConfiguredInstance().calcCumulativeBitProgressDuration(session.getCurrentDailyUid(), session.getCurrentOperationUid());
			
			//p2
//			DrillingParameters thisDrillParam = (DrillingParameters) obj;
//			Bitrun thisbitrun = (Bitrun) obj;
//			QueryProperties qp = new QueryProperties();
//			qp.setUomConversionEnabled(false);
//			String strSql3 = "SELECT wobAvg, ropAvg FROM DrillingParameters WHERE "
//					+ "(isDeleted = false or isDeleted is null) AND bharunUid = :BharunUid order by startTime desc";
//			String[] paramsFields2 = {"BharunUid"};
//			String[] paramsValues2 = {thisbitrun.getBharunUid()};
//			List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields2, 
//					paramsValues2, qp);
//		
//			if(lstResult1!=null && lstResult1.size()>0) {
//				
//				Object[] queryResult = (Object[]) lstResult1.get(0);
//				double ropavg = 0.00;
//				double wobAvg = 0.00;
//				
//				//calculate wobAvg
//				if(queryResult[0] != null) {
//					wobAvg = Double.parseDouble(queryResult[0].toString());
//				}
//				CustomFieldUom thisConverter1 = new CustomFieldUom(commandBean, DrillingParameters.class, "rop");
//				thisConverter1.setBaseValue(ropavg);
//				
//				//calculate ropavg
//				if(queryResult[1] != null) {
//					ropavg = Double.parseDouble(queryResult[1].toString());
//				}
//				CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, DrillingParameters.class, "wobAvg");
//				thisConverter2.setBaseValue(wobAvg);
//				
//				String strSql1 = "FROM Bitrun " 
//						+ " WHERE bharunUid =:bharunUid and(isDeleted is null or isDeleted = false) ";
//				String[] paramsFields1 = { "bharunUid"};
//				Object[] paramsValues1 = {thisbitrun.getBharunUid()};
//				List <Bitrun> lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(
//						strSql1, paramsFields1, paramsValues1, qp);
//				
//			
//				if(lstResult5!=null && lstResult5.size()>0) {
//					Bitrun bitrun = lstResult5.get(0);
//					
//					bitrun.setRop(thisConverter1.getConvertedValue());
//					bitrun.setWobAvg(thisConverter2.getConvertedValue());
//					
//					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bitrun);
//				}
//			}
//			QueryProperties qp = new QueryProperties();
//			qp.setUomConversionEnabled(false);
//			Bitrun bitrun = (Bitrun) obj;
//			String strSql3 = "FROM DrillingParameters WHERE "
//					+ "(isDeleted = false or isDeleted is null) AND bharunUid = :BharunUid";
//			String[] paramsFields2 = {"BharunUid"};
//			String[] paramsValues2 = {bitrun.getBharunUid()};
//			List<Bitrun> lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields2, 
//					paramsValues2, qp);
//		
//			if(lstResult1!=null && lstResult1.size()>0) {
//				
//				Bitrun thisbitrun2 = lstResult1.get(0);
//				
//				//get wob
//				if(thisbitrun2.getWobAvg() != null) {
//					Double wobavg = 0.00;
//					wobavg = Double.parseDouble(thisbitrun2.getWobAvg().toString());
//					
//					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "wobAvg");
//					thisConverter.setBaseValue(wobavg);
//					
//					bitrun.setWobAvg(thisConverter.getConvertedValue());
//				}
//				
//				//get rop
//				if(thisbitrun2.getRop() != null) {
//					Double rop = 0.00;
//					rop = Double.parseDouble(thisbitrun2.getRop().toString());
//					
//					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, DrillingParameters.class, "rop");
//					thisConverter.setBaseValue(rop);
//					
//					bitrun.setRop(thisConverter.getConvertedValue());
//				}
//				
//				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bitrun);
//			}
		}
		
		

		
		/* Ticket: 20110216-0405-jwong, as told by Jee(SA) to comment off this logic which is incorrect for cumulative hour used calculation if there exists more than 1 bharun with same serial # at same day
		//	Save bha component hours used in separate table: bha_component_hours_used
		if(obj instanceof BhaComponent){
			BhaComponent thisBhaComponent = (BhaComponent) obj;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			// If hours used in not null save into database
			if(node.getDynaAttr().get("hoursUsed") != null && StringUtils.isNotBlank(node.getDynaAttr().get("hoursUsed").toString())){
				String bhaComponentUid = thisBhaComponent.getBhaComponentUid();
				String serialNumber = thisBhaComponent.getSerialNum();
				String dailyUid = session.getCurrentDailyUid();
				Double hoursUsed = Double.valueOf(node.getDynaAttr().get("hoursUsed").toString());
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BhaComponentHoursUsed.class, "hoursUsed");
				thisConverter.setBaseValueFromUserValue(hoursUsed);
				hoursUsed = thisConverter.getBasevalue();				
				
				String[] paramsFields = {"bhaComponentUid", "dailyUid"};
				Object[] paramsValues = new Object[2]; paramsValues[0] = bhaComponentUid; paramsValues[1] = dailyUid;
				
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from BhaComponentHoursUsed where (isDeleted = false or isDeleted is null) AND bhaComponentUid = :bhaComponentUid AND dailyUid = :dailyUid", paramsFields, paramsValues);
				// If record already exist
				if(list != null && list.size() > 0){
					//	Step 1: Update the serial number for all the records of bha_component_hours_used for current bha component.
					String[] paramsFields1 = {"serialNumber", "bhaComponentUid"};
					Object[] paramsValues1 = {serialNumber , bhaComponentUid};
					String strSql1 = "UPDATE BhaComponentHoursUsed SET serialNumber =:serialNumber WHERE bhaComponentUid =:bhaComponentUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
					
					//	Step 2: Update the hours used for the particular day record for the bha component.
					String[] paramsFields2 = {"hoursUsed", "bhaComponentUid", "dailyUid"};
					Object[] paramsValues2 = {hoursUsed, bhaComponentUid, dailyUid};					
					String strSql2 = "UPDATE BhaComponentHoursUsed SET hoursUsed = :hoursUsed WHERE bhaComponentUid =:bhaComponentUid AND dailyUid = :dailyUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
				}else{
					// If no existing hours used for bha component, insert new record
					BhaComponentHoursUsed compHoursUsed = new BhaComponentHoursUsed();
					compHoursUsed.setBhaComponentUid(thisBhaComponent.getBhaComponentUid());
					compHoursUsed.setSerialNumber(thisBhaComponent.getSerialNum());					
					compHoursUsed.setDailyUid(dailyUid);
					compHoursUsed.setGroupUid(session.getCurrentGroupUid());
					compHoursUsed.setHoursUsed(hoursUsed);
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(compHoursUsed, qp);
				}
			}	
		}
		//	End of Save bha component hours used in separate table: bha_component_hours_used
		*/		
	}
	
	private void autoPopulateBharunPropertyToBitrun(CommandBeanTreeNode node, CommandBean commandBean, UserSession session) throws Exception
	{
		Object obj = node.getData();
		if (obj instanceof Bharun && "1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(),GroupWidePreference.GWP_AUTO_POPULATE_BHARUN_DEPTH_DAILY_TO_BITRUN)))
		{
			Bharun bharun = (Bharun) obj;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			List<Bitrun> bitrunList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From Bitrun Where (isDeleted Is Null or isDeleted = false) and bharunUid=:bharunUid","bharunUid", bharun.getBharunUid(), qp);
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			
		
			for  (Bitrun bitrun : bitrunList)
			{
				bitrun.setDailyUidIn(bharun.getDailyidIn());
				
				bitrun.setDailyUidOut(bharun.getDailyidOut());
				
				if (bharun.getDepthInMdMsl()!=null)
				{
					thisConverter.setReferenceMappingField(Bharun.class, "depthInMdMsl");	
					thisConverter.setBaseValueFromUserValue(bharun.getDepthInMdMsl(), false);
					bitrun.setDepthInMdMsl(thisConverter.getBasevalue());
				}else{
					bitrun.setDepthInMdMsl(null);
				}
				if (bharun.getDepthOutMdMsl()!=null)
				{
					thisConverter.setReferenceMappingField(Bharun.class, "depthOutMdMsl");
					thisConverter.setBaseValueFromUserValue(bharun.getDepthOutMdMsl(), false);
					bitrun.setDepthOutMdMsl(thisConverter.getBasevalue());
				}else{
					bitrun.setDepthOutMdMsl(null);
				}
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bitrun, qp);
			}
		}
	}
	
	private void saveJetVelocity(CommandBeanTreeNode node) throws Exception {
		
		String bharunUid = null;
		List<String> dailyUidList = new ArrayList();
		if (node.getData() instanceof BharunDailySummary) {
			BharunDailySummary bhaDailySummary = (BharunDailySummary) node.getData();
			bharunUid = bhaDailySummary.getBharunUid();
			dailyUidList.add(bhaDailySummary.getDailyUid());
		} else if (node.getData() instanceof BitNozzle) {
			BitNozzle bitNozzle = (BitNozzle) node.getData();
			String bitrunUid = bitNozzle.getBitrunUid();
			Bitrun bitrun = (Bitrun) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Bitrun.class, bitrunUid);
			if (bitrun!=null) {
				bharunUid = bitrun.getBharunUid();
				String strSql = "FROM BharunDailySummary WHERE (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid";
				List<BharunDailySummary> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"bharunUid"}, new Object[]{bharunUid});
				for (BharunDailySummary bhaDailySummary : list) {
					dailyUidList.add(bhaDailySummary.getDailyUid());
				}
			}
		} else if (node.getData() instanceof Bitrun) {
			Bitrun bitrun = (Bitrun) node.getData();
			bharunUid = bitrun.getBharunUid();
			String strSql = "FROM BharunDailySummary WHERE (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid";
			List<BharunDailySummary> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"bharunUid"}, new Object[]{bharunUid});
			for (BharunDailySummary bhaDailySummary : list) {
				dailyUidList.add(bhaDailySummary.getDailyUid());
			}
		}
		if (bharunUid==null) return;
		if (dailyUidList.size()==0) return;
		
		CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
		thisConverter.setReferenceMappingField(BharunDailySummary.class, "jetvelocity");
		
		for (String dailyUid : dailyUidList) {
			Double jetvelocity = CommonUtil.getConfiguredInstance().calculateJetVelocity(bharunUid, dailyUid);
			if (jetvelocity!=null) {
				thisConverter.setBaseValue(jetvelocity);
				jetvelocity = thisConverter.getConvertedValue();
			}
			String strSql = "FROM BharunDailySummary WHERE (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid and dailyUid=:dailyUid";
			List<BharunDailySummary> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"bharunUid","dailyUid"}, new Object[]{bharunUid, dailyUid});
			for (BharunDailySummary bhaDailySummary : list) {
				bhaDailySummary.setJetvelocity(jetvelocity);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bhaDailySummary);
			}
		}
	}
	
	private void calculateWeightMassAndForce(CommandBean commandBean, BhaComponent thisBhaComponent, UserSession session) throws Exception
	{
		//Calculate Weight Mass 
		Double nodeLength = thisBhaComponent.getJointlength();
		Double nodeWeight = thisBhaComponent.getWeight();
		Integer nodeJointNumber=thisBhaComponent.getJointNumber();
		if(nodeLength != null){
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BhaComponent.class, "jointlength");
			thisConverter.setBaseValueFromUserValue(nodeLength);
			nodeLength = thisConverter.getBasevalue();
		}
		
		
		if(nodeWeight != null){
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BhaComponent.class, "weight");
			thisConverter.setBaseValueFromUserValue(nodeWeight);
			nodeWeight = thisConverter.getBasevalue();
		}
		
		
		
		Double nodeWeightMass = 0.0;
		Double nodeWeightForce = 0.0;
		if (!weightMassForce){
			if (nodeWeight != null && nodeLength != null) {	
				
				if (nodeJointNumber != null && "1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_BHA_DETAIL_TOTAL_JOINTS_AS_MULTIPLIER)))
					nodeLength=nodeLength*nodeJointNumber;
				
				nodeWeightMass = nodeWeight * nodeLength;
				nodeWeightForce = nodeWeightMass * 9.81;
				if(nodeWeightMass != null){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, BhaComponent.class, "weightMass");
					thisConverter.setBaseValue(nodeWeightMass);
					thisBhaComponent.setWeightMass(thisConverter.getConvertedValue());
					thisConverter.setReferenceMappingField(BhaComponent.class, "weightForce");
					thisConverter.setBaseValue(nodeWeightForce);
					thisBhaComponent.setWeightForce(thisConverter.getConvertedValue());
				}										
			}else{
					thisBhaComponent.setWeightMass(null);
					thisBhaComponent.setWeightForce(null);
			}
		}
	}
	
	private Double calculateTotalWeightDry(Collection<CommandBeanTreeNode> nodes, CommandBeanTreeNode parent, CustomFieldUom thisConverter) throws Exception
	{

		Double bhaTotalWeightDry = null;
		thisConverter.setReferenceMappingField(BhaComponent.class, "weight");
		
		for(CommandBeanTreeNode node:nodes)
		{
			if (node.getData() instanceof BhaComponent)
			{
				BhaComponent comp = (BhaComponent) node.getData();
				Double compWeight = comp.getWeight();
				Double jointLength = comp.getJointlength();
				
				if (compWeight != null && jointLength != null) {
					thisConverter.setReferenceMappingField(BhaComponent.class, "weight");
					thisConverter.setBaseValueFromUserValue(compWeight);
					compWeight = thisConverter.getBasevalue();
					
					thisConverter.setReferenceMappingField(BhaComponent.class, "jointlength");
					thisConverter.setBaseValueFromUserValue(jointLength);
					jointLength = thisConverter.getBasevalue();
					
					if (bhaTotalWeightDry == null) {
						bhaTotalWeightDry = 0.00;
					}
					bhaTotalWeightDry += (compWeight * jointLength);
				}
			}
		}
		
		return bhaTotalWeightDry;
	}
	
	private Double calculateBuoyancyFactor(String operationUid, String bhaDailyidIn, String bhaDailyidOut, String currentDailyUid, QueryProperties qp, CustomFieldUom thisConverter) throws Exception
	{
		Double buoyancyFactor = null;
		if (operationUid != null && bhaDailyidIn != null)
		{
			//find the "Day Date" from Daily based on "BHA Date In" and "BHA Date Out"
			//note: "BHA Date In" must have value for the calculation to proceed
			//then find the "Mud Weight" from MudProperties which is latest 
			//latest - if it's between date in and date out of bha, take the latest in that range, if not found, then take the latest mud properties (before the date in and date out)
		
			Daily bhaDailyIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(bhaDailyidIn);
			Daily bhaDailyOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(bhaDailyidOut);
			Date currentDate = null;
			if (currentDailyUid != null) {
				Daily currentDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
				if (currentDaily != null) {
					currentDate = currentDaily.getDayDate();
				}
			}
			Date bhaInDatetime = null;
			Date bhaOutDatetime = null;
			if (bhaDailyIn != null) {
				bhaInDatetime = bhaDailyIn.getDayDate();
				
				if (bhaDailyOut != null) {
					bhaOutDatetime = bhaDailyOut.getDayDate();
				}
				thisConverter.setReferenceMappingField(MudProperties.class, "mudWeight");
				//buoyancyFactor = MudPropertiesUtil.calculateBuoyancyFactor(operationUid, bhaInDatetime, bhaOutDatetime, qp, thisConverter);
				buoyancyFactor = MudPropertiesUtil.calculateBuoyancyFactor(operationUid, bhaInDatetime, bhaOutDatetime, currentDate, qp, thisConverter);
			}
		}
		
		return buoyancyFactor;
	}
	
	private void validateHoleSection(Object object) throws Exception{
		Bharun bharun = (Bharun)object;
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		String[] paramsFields = {"bharunUid"};
		Object[] paramsValues = new Object[1]; 
		paramsValues[0] = bharun.getBharunUid(); 
		List<Bharun> result=ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Bharun where (isDeleted = false or isDeleted is null) and bharunUid=:bharunUid", paramsFields, paramsValues,qp);
		if (result.size()>0)
		{
			Bharun currBharun=result.get(0);
			Daily currDailyIn=ApplicationUtils.getConfiguredInstance().getCachedDaily(currBharun.getDailyidIn());
			Daily currDailyOut=ApplicationUtils.getConfiguredInstance().getCachedDaily(currBharun.getDailyidOut());
			HoleSection exactHole=isSectionNew(currBharun.getWellboreUid(),currBharun.getDepthInMdMsl(),currBharun.getDepthOutMdMsl(),currBharun.getDrillStringSummaryMaxOd(),(currDailyIn!=null?currDailyIn.getDayDate():null),(currDailyOut!=null?currDailyOut.getDayDate():null));
			holeSectionUid=(exactHole!=null?exactHole.getHoleSectionUid():null);
		}
	}
	
	private HoleSection isSectionNew (String wellboreUid, Double depthFromMdMsl, Double depthToMdMsl, Double holeSize, Date dateTimeIn, Date dateTimeOut) throws Exception
	{
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		
		List<String> paramsFields = new ArrayList<String>();
		List<Object> paramsValues = new ArrayList<Object>();
		String queryString="";
		paramsFields.add("wellboreUid");
		paramsValues.add(wellboreUid);
		
		if(dateTimeIn != null){
			paramsFields.add("dateTimeIn");
			paramsValues.add(dateTimeIn);
			queryString+=" and dateTimeIn=:dateTimeIn";
		}else{
			queryString+=" and dateTimeIn is null";
		}
		
		if(dateTimeOut != null){
			paramsFields.add("dateTimeOut");
			paramsValues.add(dateTimeOut);
			queryString+=" and dateTimeOut=:dateTimeOut";
		}else{
			queryString+=" and dateTimeOut is null";
		}
		
		if(holeSize != null){
			paramsFields.add("holeSize");
			paramsValues.add(holeSize);
			queryString+=" and holeSize=:holeSize";
		}else{
			queryString+=" and holeSize is null";
		}
		
		if(depthFromMdMsl != null){
			paramsFields.add("depthFromMdMsl");
			paramsValues.add(depthFromMdMsl);
			queryString+=" and depthFromMdMsl=:depthFromMdMsl";
		}else{
			queryString+=" and depthFromMdMsl is null";
		}
		
		if(depthToMdMsl != null){
			paramsFields.add("depthToMdMsl");
			paramsValues.add(depthToMdMsl);
			queryString+=" and depthToMdMsl=:depthToMdMsl";
		}else{
			queryString+=" and depthToMdMsl is null";
		}

		List<HoleSection> wellbore=ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From HoleSection Where wellboreUid=:wellboreUid " + queryString, paramsFields, paramsValues,qp);
		
		return (wellbore.size()>0?wellbore.get(0):null);
	}
		
	private void validateBharun(String wellboreUid, Double depthFromMdMsl, Double depthToMdMsl, Double holeSize, Date dateIn, Date dateOut) throws Exception{
		
		if (holeSectionUid !=null)
		{
			updateHoleSection(holeSectionUid,dateIn,dateOut,holeSize,depthFromMdMsl,depthToMdMsl);
		}else{
			HoleSection exactHole=isSectionNew(wellboreUid,depthFromMdMsl,depthToMdMsl,holeSize,dateIn,dateOut);
			if (exactHole!=null)
				updateHoleSection(exactHole.getHoleSectionUid(),dateIn,dateOut,holeSize,depthFromMdMsl,depthToMdMsl);
			else
				saveNewHole(dateIn,dateOut,holeSize,depthFromMdMsl,depthToMdMsl);
		}
		holeSectionUid=null;
		
	}
	
	private void updateHoleSection(String holeSectionUid,Date dateTimeIn, Date dateTimeOut, Double holeSize, Double depthFromMdMsl, Double depthToMdMsl) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		String[] paramsFields = { "holeSectionUid","depthFromMdMsl", "depthToMdMsl","holeSize","dateTimeIn","dateTimeOut"};
		Object[] paramsValues = new Object[6];  
		paramsValues[0] = holeSectionUid;
		paramsValues[1] = depthFromMdMsl;
		paramsValues[2] = depthToMdMsl;
		paramsValues[3] = holeSize;
		paramsValues[4] = dateTimeIn;
		paramsValues[5] = dateTimeOut;
		
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("Update HoleSection Set depthFromMdMsl=:depthFromMdMsl, depthToMdMsl=:depthToMdMsl, holeSize=:holeSize, dateTimeIn=:dateTimeIn, dateTimeOut=:dateTimeOut  where holeSectionUid=:holeSectionUid", paramsFields, paramsValues,qp);
		
	}
	
	private void saveNewHole(Date dateIn, Date dateOut, Double holeSize, Double depthFrom, Double depthTo) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		HoleSection newHole=new HoleSection();
		newHole.setDateTimeIn(dateIn);
		newHole.setDateTimeOut(dateOut);
		newHole.setHoleSize(holeSize);
		newHole.setDepthFromMdMsl(depthFrom);
		newHole.setDepthToMdMsl(depthTo);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newHole,qp);
	}
	
	private BharunDailySummary setAnnvel(CommandBeanTreeNode node, BharunDailySummary bhaDailySummary) throws Exception {		
		Double annvelDp = 0.0;
		Double annvelDpCasing = 0.0;                                                                                                        
        Double annvelDpLiner = 0.0;                                             
        Double annvelDp2 = 0.0;                                                                                                                               
        Double annvelDp2Casing = 0.0;                                                                                                       
        Double annvelDp2Liner = 0.0;  
		Double annvelHwdp = 0.0;
		Double annvelHwdp2= 0.0;
		Double annvelHwdp3 = 0.0;
		Double annvelDc1 = 0.0;
		Double annvelDc2 = 0.0;
		
		if ((node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP)!=null) &&(StringUtils.isNotBlank(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP).toString()))) {
			annvelDp = Double.valueOf(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP).toString());
		}
		
		if ((node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP_CASING)!=null) &&(StringUtils.isNotBlank(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP_CASING).toString()))) {
			annvelDpCasing = Double.valueOf(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP_CASING).toString());
		}
		
		if ((node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP_LINER)!=null) &&(StringUtils.isNotBlank(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP_LINER).toString()))) {
			annvelDpLiner = Double.valueOf(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP_LINER).toString());
		}
		
		if ((node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP2)!=null) &&(StringUtils.isNotBlank(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP2).toString()))) {
			annvelDp2 = Double.valueOf(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP2).toString());
		}
	
		if ((node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP2_CASING)!=null) &&(StringUtils.isNotBlank(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP2_CASING).toString()))) {
			annvelDp2Casing = Double.valueOf(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP2_CASING).toString());
		}
		
		if ((node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP2_LINER)!=null) &&(StringUtils.isNotBlank(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP2_LINER).toString()))) {
			annvelDp2Liner = Double.valueOf(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DP2_LINER).toString());
		}
		
		
		if ((node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_HWDP)!=null) &&(StringUtils.isNotBlank(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_HWDP).toString()))) {
			annvelHwdp = Double.valueOf(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_HWDP).toString());
		}
		
		if ((node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_HWDP2)!=null) && (StringUtils.isNotBlank(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_HWDP2).toString()))) {
			 annvelHwdp2 = Double.valueOf(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_HWDP2).toString());
		}
		
		if ((node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_HWDP3)!=null) &&(StringUtils.isNotBlank(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_HWDP3).toString()))) {
			 annvelHwdp3 = Double.valueOf(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_HWDP3).toString());
		}
		
		if ((node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DC1)!=null) &&(StringUtils.isNotBlank(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DC1).toString()))) {
			 annvelDc1 = Double.valueOf(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DC1).toString());
		}
		
		if ((node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DC2)!=null) &&(StringUtils.isNotBlank(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DC2).toString()))) {
			 annvelDc2 = Double.valueOf(node.getDynaAttr().get(BHARUNDAILYSUMMARY_ANNVEL_DC2).toString());
		}
		
		bhaDailySummary.setAnnvelDp(annvelDp);
		bhaDailySummary.setAnnvelDp(annvelDpCasing);
		bhaDailySummary.setAnnvelDp(annvelDpLiner);
		bhaDailySummary.setAnnvelDp2(annvelDp2);
		bhaDailySummary.setAnnvelDp(annvelDp2Casing);
		bhaDailySummary.setAnnvelDp(annvelDp2Liner);
		bhaDailySummary.setAnnvelHwdp2(annvelHwdp2);
		bhaDailySummary.setAnnvelHwdp3(annvelHwdp3);
		bhaDailySummary.setAnnvelHwdp(annvelHwdp);
		bhaDailySummary.setAnnvelDc1(annvelDc1);
		bhaDailySummary.setAnnvelDc2(annvelDc2);
		
		return(bhaDailySummary);
		
	}
	
	public void afterDataNodeProcessed(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, String action, int operationPerformed, boolean errorOccurred) throws Exception{
		Object obj = node.getData();		
		/* NO GOOD no dynamic data will show in this section
		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "showBitWearOnDateOutOnly"))) {				
			
			//set value for show Bit Out Date and Bit Wear counter when save Bitrun
			if (obj instanceof Bitrun) {
				node.getDynaAttr().put("isHide", "1");
				
				Bitrun thisBitrun = (Bitrun) obj;
				String BitrunUid = thisBitrun.getBitrunUid();				
				CommonUtil.getConfiguredInstance().showBitWear(BitrunUid, commandBean, node, session);	
				
			}
			
			if (obj instanceof BitNozzle) {
				node.getDynaAttr().put("isHide", "2");
				
				BitNozzle thisBitNozzle = (BitNozzle) obj;
				String BitrunUid = thisBitNozzle.getBitrunUid();		
				CommonUtil.getConfiguredInstance().showBitWear(BitrunUid, commandBean, node, session);	
				
			}
			
			if (obj instanceof BharunDailySummary) {
				node.getDynaAttr().put("isHide", "3");
				
				BharunDailySummary thisBharunDailySummary = (BharunDailySummary) obj;
				String BharunUid = thisBharunDailySummary.getBharunUid();		
				int hideBitWear = 0;
				QueryProperties qp = new QueryProperties();
				String strSql = "FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", BharunUid, qp);
				Bitrun thisBitrun;
				if (lstResult.size() > 0){
					for(Object objBitrun: lstResult){
						
						thisBitrun = (Bitrun) objBitrun;
						Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(session.getCurrentDailyUid());
						Daily bitDateOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisBitrun.getDailyUidOut());
						
						if (daily != null) {
							Date currentDayDate = daily.getDayDate();
						
							if (bitDateOut != null) {
								Date bitOutDayDate = bitDateOut.getDayDate();
							
								if (bitOutDayDate.compareTo(currentDayDate)<=0){
									hideBitWear = 1;
								}
								else hideBitWear = 0;
							}
							else hideBitWear = 1;
						}		
					}
				}node.getDynaAttr().put("isHide", hideBitWear);
			}
		}*/
		
		if (!"0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "calctfa"))) {				
			//set TFA calculated value in bitrun when save bit nozzle
			if (obj instanceof BitNozzle) {
				BitNozzle thisBitNozzle = (BitNozzle) obj;
				String BitrunUid = thisBitNozzle.getBitrunUid();
				String BharunUid = CommonUtil.getConfiguredInstance().getBharunUidFormBitrun(BitrunUid);
				if (BharunUid != null) CommonUtil.getConfiguredInstance().saveTFA(BharunUid, commandBean);
			}

			//set TFA calculated value when save bharun_daily_summary
			if (obj instanceof BharunDailySummary) {
				BharunDailySummary thisBharunDailySummary = (BharunDailySummary) obj;
				String BharunUid = thisBharunDailySummary.getBharunUid();
				if (BharunUid != null) CommonUtil.getConfiguredInstance().saveTFA(BharunUid, commandBean);
			}
			
			//set TFA calculated value when save Bitrun
			if (obj instanceof Bitrun) {
				Bitrun thisBitrun = (Bitrun) obj;
				String BharunUid = thisBitrun.getBharunUid();
				if (BharunUid != null) CommonUtil.getConfiguredInstance().saveTFA(BharunUid, commandBean);
			}
			
		}
		
		if (!"0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "calc_annvel"))) {				
			
			//set AV calculated value in bharun_daily_summary when save bharun_daily_summary
			if (obj instanceof BharunDailySummary) {
				BharunDailySummary thisBharunDailySummary = (BharunDailySummary) obj;
				BharunUtils.calculateAV(session.getCurrentGroupUid(), thisBharunDailySummary.getBharunUid(), thisBharunDailySummary.getDailyUid(), commandBean.getSystemMessage());
			}
			
			//set AV calculated value in bharun_daily_summary when save bitrun
			if (obj instanceof Bitrun) {
				Bitrun thisBitrun = (Bitrun) obj;			
				BharunUtils.calculateAV(session.getCurrentGroupUid(), thisBitrun.getBharunUid(), session.getCurrentDailyUid(), commandBean.getSystemMessage());
			}
			
			//set AV calculated value in bharun_daily_summary when save bharun
			if (obj instanceof Bharun) {
				Bharun thisBharun = (Bharun) obj;			
				BharunUtils.calculateAV(session.getCurrentGroupUid(), thisBharun.getBharunUid(), session.getCurrentDailyUid(), commandBean.getSystemMessage());
			}
		}
		
		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "calchsi"))) {
			//set HSI calculated value in bharun_daily_summary when save bharun_daily_summary
			if (obj instanceof BharunDailySummary) {
				BharunDailySummary thisBharunDailySummary = (BharunDailySummary) obj;
				String BharunUid = thisBharunDailySummary.getBharunUid();
				String DailyUid = thisBharunDailySummary.getDailyUid();			
				BharunUtils.saveHSI(BharunUid, DailyUid, commandBean.getSystemMessage());
			}
			
			//set HSI calculated value in bharun_daily_summary when save bitrun
			if (obj instanceof Bitrun) {
				Bitrun thisBitrun = (Bitrun) obj;	
				String BharunUid = thisBitrun.getBharunUid();			
				BharunUtils.saveHSI(BharunUid, session.getCurrentDailyUid(), commandBean.getSystemMessage());
			}
			
			//set HSI calculated value in bharun_daily_summary when save bit nozzle
			if (obj instanceof BitNozzle) {
				BitNozzle thisBitNozzle = (BitNozzle) obj;
				String BitrunUid = thisBitNozzle.getBitrunUid();
				String BharunUid = CommonUtil.getConfiguredInstance().getBharunUidFormBitrun(BitrunUid);		
				BharunUtils.saveHSI(BharunUid, session.getCurrentDailyUid(), commandBean.getSystemMessage());
			}
		}

		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_JET_NOZZLE_PRESSURE_LOSS))) {
			CustomFieldUom thisConverter=new CustomFieldUom(commandBean,DrillingParameters.class, "flowAvg");
			
			if (obj instanceof BitNozzle) {
				BitNozzle thisBitNozzle = (BitNozzle) obj;
				String BitrunUid = thisBitNozzle.getBitrunUid();
				String BharunUid = CommonUtil.getConfiguredInstance().getBharunUidFormBitrun(BitrunUid);		
				BharunUtils.saveJetNozzlePressureLoss(thisConverter, BharunUid, session.getCurrentDailyUid(), commandBean.getSystemMessage(), session.getCurrentGroupUid(), session.getCurrentWellboreUid(), session.getCurrentOperationUid());
			}
		}
		
		// call method to populate Impact Force if GWP turned on
		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_BIT_IMPACT_FORCE))) {
			if (obj instanceof BitNozzle) {
				BitNozzle thisBitNozzle = (BitNozzle) obj;
				String BitrunUid = thisBitNozzle.getBitrunUid();
				String BharunUid = CommonUtil.getConfiguredInstance().getBharunUidFormBitrun(BitrunUid);
				CustomFieldUom thisConverter=new CustomFieldUom(commandBean);
				BharunUtils.saveBitImpactForce(thisConverter, BharunUid, session.getCurrentDailyUid(), commandBean.getSystemMessage(), session.getCurrentGroupUid(), session.getCurrentWellboreUid());		
			}
		}
		
		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PERCENT_HHPB))) {
			CustomFieldUom thisConverter=new CustomFieldUom(commandBean,DrillingParameters.class, "flowAvg");
			
			//set %HHP at bit calculated value in bharun_daily_summary when save bharun_daily_summary
			if (obj instanceof BharunDailySummary) {
				BharunDailySummary thisBharunDailySummary = (BharunDailySummary) obj;
				String BharunUid = thisBharunDailySummary.getBharunUid();
				String DailyUid = thisBharunDailySummary.getDailyUid();			
				BharunUtils.savePercentHHPb(thisConverter, BharunUid, DailyUid, commandBean.getSystemMessage());
			}
			
			//set %HHP at bit calculated value in bharun_daily_summary when save bitrun
			/*
			if (obj instanceof Bitrun) {
				Bitrun thisBitrun = (Bitrun) obj;	
				String BharunUid = thisBitrun.getBharunUid();			
				BharunUtils.savePercentHHPb(thisConverter, BharunUid, session.getCurrentDailyUid(), commandBean.getSystemMessage());
			}*/
			
			//set %HHP at bit calculated value in bharun_daily_summary when save bit nozzle
			if (obj instanceof BitNozzle) {
				BitNozzle thisBitNozzle = (BitNozzle) obj;
				String BitrunUid = thisBitNozzle.getBitrunUid();
				String BharunUid = CommonUtil.getConfiguredInstance().getBharunUidFormBitrun(BitrunUid);		
				BharunUtils.savePercentHHPb(thisConverter, BharunUid, session.getCurrentDailyUid(), commandBean.getSystemMessage());
			}
		}

		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_IMPACT_FORCE))) {
			CustomFieldUom thisConverter=new CustomFieldUom(commandBean,DrillingParameters.class, "flowAvg");
			
			//set Impact Force calculated value in bharun_daily_summary when save bharun_daily_summary
			if (obj instanceof BharunDailySummary) {
				BharunDailySummary thisBharunDailySummary = (BharunDailySummary) obj;
				String BharunUid = thisBharunDailySummary.getBharunUid();
				BharunUtils.saveBitImpactForce(thisConverter, BharunUid, session.getCurrentDailyUid(), commandBean.getSystemMessage(), session.getCurrentGroupUid(), session.getCurrentWellboreUid());
			}
			
			//set Impact Force calculated value in bharun_daily_summary when save bit nozzle
			if (obj instanceof BitNozzle) {
				BitNozzle thisBitNozzle = (BitNozzle) obj;
				String BitrunUid = thisBitNozzle.getBitrunUid();
				String BharunUid = CommonUtil.getConfiguredInstance().getBharunUidFormBitrun(BitrunUid);		
				BharunUtils.saveBitImpactForce(thisConverter, BharunUid, session.getCurrentDailyUid(), commandBean.getSystemMessage(), session.getCurrentGroupUid(), session.getCurrentWellboreUid());
			}
		}
		
		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "copyCumulativeValueFromDrillParam"))) {				
			
			//set Bit Daily value in bharun_daily_summary when save bharun_daily_summary
			if (obj instanceof BharunDailySummary) {
				BharunDailySummary thisBharunDailySummary = (BharunDailySummary) obj;
				String DailyUid = thisBharunDailySummary.getDailyUid();
				CommonUtil.getConfiguredInstance().copyValueFromDrilParam(DailyUid);
			}
		}
		
		if (obj instanceof BharunDailySummary) {
			//calculate bit depth drilled in & out, Bit Hour In & Out
			BharunDailySummary thisBharunDailySummary = (BharunDailySummary) obj;
			String dailyUid = thisBharunDailySummary.getDailyUid();
			CommonUtil.getConfiguredInstance().calcCumulativeBitProgressDuration(dailyUid, session.getCurrentOperationUid());

			//calculate drilling ahead average rop - 20100111-0509-asim
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_CALC_DRILLING_AHEAD_AVG_ROP))) {
				ReportDailyUtils.calculateDrillingAheadAvgRop(node, session.getCurrentOperationUid(), dailyUid);
			}
		}		
	}
	
	private void loadBitrunRecordList(CommandBeanTreeNode node, String bharunUid, UserSelectionSnapshot userSelection) throws Exception {
	
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid=:bharunUid order by bitrunNumber", "bharunUid", bharunUid);
		String relatedBitrun = null;
		if (items.isEmpty()){
			node.getDynaAttr().put("isBitrun", "no");
		}else {
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object obj_array = (Object) i.next();
				Bitrun bitrun = (Bitrun) obj_array;
				String bitrunNumber= URLEncoder.encode(bitrun.getBitrunNumber(), "utf-8");
				String bitrunUid = URLEncoder.encode(bitrun.getBitrunUid(), "utf-8");
				
				if (StringUtils.isBlank(relatedBitrun)) {
					relatedBitrun = "bitrunnumber=" + bitrunNumber + "&bitrunuid=" + bitrunUid;
				}else {
					relatedBitrun = relatedBitrun + ",bitrunnumber=" + bitrunNumber + "&bitrunuid=" + bitrunUid;
				}
				
			}
			node.getDynaAttr().put("bitrun", relatedBitrun);
		}

	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		if(StringUtils.isNotBlank(fromClause) && fromClause.indexOf(CUSTOM_FROM_MARKER) < 0) return null;
		
		String bhaRunUid = null;
		if (request != null) {
			bhaRunUid = request.getParameter(CONTINUE_FROM_PREVIOUS_RUN);
		}
		String currentClass	= meta.getTableClass().getSimpleName();
		String customFrom = "";
		
		if(currentClass.equals("Bharun")) {
			if(StringUtils.isNotBlank(bhaRunUid)) {
				customFrom = "Bharun bharun";
			} else {
				customFrom = "Bharun bharun, BharunDailySummary dailySummary";
			}
			return fromClause.replace(CUSTOM_FROM_MARKER, customFrom);
		}
		return null;
	}

	

	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, 
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String bhaRunUid = null;
		if (request != null) {
			bhaRunUid = request.getParameter(CONTINUE_FROM_PREVIOUS_RUN);
		}
		String currentClass		= meta.getTableClass().getSimpleName();
		String customCondition 	= "";
		
		if(currentClass.equals("Bharun")) {
			if(StringUtils.isNotBlank(bhaRunUid)) {
				customCondition = "bharun.groupUid = session.groupUid and (bharun.isDeleted = false or bharun.isDeleted is null) and bharun.operationUid = session.operationUid and bharun.bharunUid = :bharunUid";
				query.addParam("bharunUid", bhaRunUid);
			} else {
				if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_STT) {
					// do not filter by isDeleted flag here, deleted data always need to be loaded for Send To Town
					customCondition = "bharun.groupUid = session.groupUid and bharun.operationUid = session.operationUid and bharun.bharunUid = dailySummary.bharunUid and dailySummary.dailyUid = session.dailyUid";
				} else {
					//remove the filter condition for group_uid as parent/global site not able to see the child bharun data
					customCondition = "(bharun.isDeleted = false or bharun.isDeleted is null) and (dailySummary.isDeleted = false or dailySummary.isDeleted is null) and bharun.operationUid = session.operationUid and bharun.bharunUid = dailySummary.bharunUid and dailySummary.dailyUid = session.dailyUid";
				}
			}
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		return null;
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
	private class BhaComponentSequenceComparator implements Comparator<CommandBeanTreeNode>{
		public int compare(CommandBeanTreeNode o1, CommandBeanTreeNode o2){
			try{
				Object in1 = o1.getData();
				Object in2 = o2.getData();
				
				BhaComponent f1 = (BhaComponent) in1;
				BhaComponent f2 = (BhaComponent) in2;
				
				if(f1.getSequence() != null && f2.getSequence() != null){
					return f1.getSequence().compareTo(f2.getSequence());
				}else if(f1.getSequence() != null){
					return 1;
				}else if(f2.getSequence() != null){
					return -1;
				}else{
					return 0;
				}
			}catch(Exception e){
				//e.printStackTrace();
				return 0;
			}
		}
	}
	
	
	
	private void setTimeField(Object thisBharun, Date thisDate, String fieldName) throws Exception{
		Calendar dayDate = Calendar.getInstance();
		dayDate.setTime(thisDate);
				
		if (PropertyUtils.getProperty(thisBharun, fieldName) != null)
		{
			Calendar TimeFieldDatetime = Calendar.getInstance();
			TimeFieldDatetime.setTime((Date) PropertyUtils.getProperty(thisBharun, fieldName));
			TimeFieldDatetime.set(Calendar.YEAR, dayDate.get(Calendar.YEAR));
			TimeFieldDatetime.set(Calendar.MONTH, dayDate.get(Calendar.MONTH));
			TimeFieldDatetime.set(Calendar.DAY_OF_MONTH, dayDate.get(Calendar.DAY_OF_MONTH));
			PropertyUtils.setProperty(thisBharun, fieldName, TimeFieldDatetime.getTime());
		}
	}
	
	public String getTypeLookupLabel(BhaComponent comp, CommandBeanTreeNode node, CommandBean commandBean) throws Exception{
		String lookupLabel = "";		
			
		Map<String, LookupItem> thisLookupMap = commandBean.getLookupMap(node, "BhaComponent", "type");
		if (thisLookupMap != null) {		
			LookupItem type=thisLookupMap.get(comp.getType());
			if (type!=null) {				
				lookupLabel = type.getValue().toString();
				
			}else{
				lookupLabel = comp.getType();
			}
		}
		
		return lookupLabel;		 
	}
	
	

	
}