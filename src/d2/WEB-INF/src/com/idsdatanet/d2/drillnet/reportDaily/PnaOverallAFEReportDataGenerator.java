package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @jwong
 * 
 * Used for calculate total P&A AFE and  Actual Spend fro the P&A operation type & same Campaign
 * 
 */

public class PnaOverallAFEReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		if (daily == null) return;
		if (!operation.getOperationCode().equals("PNA")) return;
		if (StringUtils.isEmpty(operation.getOperationCampaignUid())) return;
		Date endDate = daily.getDayDate();
		String thisCampaignUid = operation.getOperationCampaignUid();
		CustomFieldUom costConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Operation.class, "afe");
		Double overallAfe = 0.0;
		Double totalCost = 0.0;
		
		String strSql = "SELECT SUM(afe) FROM Operation WHERE (isDeleted = false or isDeleted is null) "
				+ "and operationCampaignUid = :thisOperationCampaignUid and operationCode='PNA'";
		String[] paramsFields = {"thisOperationCampaignUid"};
		Object[] paramsValues= {thisCampaignUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		Object a = (Object) lstResult.get(0);
		if (a != null) overallAfe = Double.parseDouble(a.toString());
		
		String strSql2 = "SELECT operationUid, SUM(daycost) , SUM(costadjust) FROM ReportDaily where (isDeleted = false or isDeleted is null) " + 
				"and reportDatetime <=:todayDate and reportType!='DGR' and operationUid in " +
				"(SELECT operationUid FROM Operation WHERE (isDeleted = false or isDeleted is null) "
				+ "and operationCampaignUid = :thisOperationCampaignUid and operationCode='PNA') GROUP BY operationUid";
		String[] paramsFields2 = {"todayDate", "thisOperationCampaignUid"};
		Object[] paramsValues2= {endDate, thisCampaignUid};
		List<Object[]> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
		if (lstResult2.size()>0) {
			for (int i = 0; i < lstResult2.size(); i++)	{
				Object[] b = lstResult2.get(i);
				String operationUid = (String) b[0]; 
				if (b[1] != null) totalCost = totalCost + Double.parseDouble(b[1].toString());
				if (b[2] != null) totalCost = totalCost + Double.parseDouble(b[2].toString());
				Operation ops = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
				if (ops.getAmountSpentPriorToSpud()!=null) totalCost = totalCost + ops.getAmountSpentPriorToSpud();
				if (ops.getAmountSpentPriorToOperation()!=null) totalCost = totalCost + ops.getAmountSpentPriorToOperation();
			}
		}

		ReportDataNode thisReportNode = reportDataNode.addChild("OverallAfe");
		thisReportNode.addProperty("OverallAfe", costConverter.getFormattedValue(overallAfe, false));
		thisReportNode.addProperty("TotalCost", costConverter.getFormattedValue(totalCost, false));
	}
	
}
