package com.idsdatanet.d2.drillnet.daily;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CreateNewDailyController extends AbstractController {
	
	public class CreateNewDateResult {
		public String message = "";
		public Boolean refresh = false;
		
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public Boolean getRefresh() {
			return refresh;
		}
		public void setRefresh(Boolean refresh) {
			this.refresh = refresh;
		}
	}
	
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String dayNum = (String) request.getParameter("dayNum");
		String dayDate = (String) request.getParameter("dayDate");
		UserSession session = UserSession.getInstance(request);
		Date newDayDate = new Date();
		CreateNewDateResult result = new CreateNewDateResult();
		
		if(StringUtils.isNotBlank(dayDate)) {
			newDayDate = CommonDateParser.parse(dayDate);
			if(newDayDate == null) {
				result.setMessage("Please enter a valid date.");
			} else {
				String sql = "from Daily where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and dayDate = :dayDate";
				String[] paramNames = {"operationUid", "dayDate"};
				Object[] paramValues = {session.getCurrentOperationUid(), newDayDate};
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, paramValues);
				
				if(list != null && list.size() > 0) {
					result.setMessage("The Day Date that you had entered already exists. Please choose another date.");
				} else {
					Daily daily = new Daily();
					daily.setOperationUid(session.getCurrentOperationUid());
					daily.setDayDate(newDayDate);
					daily.setDayNumber("N/A");
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(daily);
					
					//update sys_operation_start_datetime and sys_operation_last_datetime in operation table
					CommonUtil.getConfiguredInstance().setStartAndLastOperationDate(daily.getOperationUid());
					
					if(StringUtils.isNotBlank(dayNum)) {
						ReportDaily reportDaily = new ReportDaily();
						reportDaily.setGroupUid(session.getCurrentGroupUid());
						reportDaily.setOperationUid(session.getCurrentOperationUid());
						reportDaily.setDailyUid(daily.getDailyUid());
						reportDaily.setReportDatetime(newDayDate);
						reportDaily.setReportNumber(dayNum);
						reportDaily.setReportType("DDR");
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportDaily);
					}
					ApplicationUtils.getConfiguredInstance().refreshCachedData();
					session.setCurrentDailyUid(daily.getDailyUid());
					result.setRefresh(true);
				}
			}
		} else {
			result.setMessage("Day Date is required.");
		}
		
		ModelMap model = new ModelMap();
		model.addAttribute("info", result);
		
		return new ModelAndView("drillnet/createNewDateResult", "createNewDate", model);
	}	
}
