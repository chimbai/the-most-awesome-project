package com.idsdatanet.d2.drillnet.drillingparameters;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class DrillingParametersCommandBeanListener extends EmptyCommandBeanListener {
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		
		String thisDailyUid = userSelection.getDailyUid();
		if (StringUtils.isNotBlank(thisDailyUid)) {
			String strSql = "FROM BharunDailySummary WHERE dailyUid = :dailyUid AND (isDeleted IS NULL OR isDeleted = '')";
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", thisDailyUid);
			//System.out.println(list.size());
			if(list.size() == 0 || list == null)
			{
				commandBean.setSupportedAction("add", false);
				commandBean.setSupportedAction("deleteSelected", false);
				commandBean.getSystemMessage().addWarning("Add new record is disabled as no BHA data is found for today");
			}
			else
			{
				commandBean.setSupportedAction("add", true);
				commandBean.setSupportedAction("deleteSelected", true);
			}
		}
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(DrillingParameters.class)) {
				if("drillMode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					drillModeChanged(request, commandBean, targetCommandBeanTreeNode);
				}
			}
		}
	}
	
	private void drillModeChanged(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
		if(Action.DELETE.equals(node.getAtts().getAction())) return; //why do we need to test for "delete" ? ssjong - 04/may/2009

		DrillingParameters thisDrillingParameters = (DrillingParameters) node.getData();
		
		String drillMode = thisDrillingParameters.getDrillMode();
		if (StringUtils.isNotBlank(drillMode)) {
			
			String[] values = drillMode.split("_");
			if(values.length > 1) {
				Boolean excludeFromNewHole = Boolean.parseBoolean(values[0]);
				thisDrillingParameters.setExcludeFromNewHole(excludeFromNewHole);
			} 
		}
	}
	
}
