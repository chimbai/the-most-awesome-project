package com.idsdatanet.d2.drillnet.operation;

import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.NumberUtils;

import com.ibm.icu.text.DecimalFormat;
import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.dynamicfield.DynamicFieldUtil;
import com.idsdatanet.d2.core.log.CommandLoggingContext;
import com.idsdatanet.d2.core.log.CommandLoggingControl;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CostAfeMaster;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DepotWellOperationMapping;
import com.idsdatanet.d2.core.model.DepotWitsmlLogRequest;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.DynamicFieldMeta;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationActualCost;
import com.idsdatanet.d2.core.model.OperationPlanMaster;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.OpsTeam;
import com.idsdatanet.d2.core.model.OpsTeamOperation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigStateProcessLog;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.model.TightHoleUser;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.hibernate.tables.UomTemplate;
import com.idsdatanet.d2.core.uom.mapping.Datum;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.uom.mapping.UserUOMSelection;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.costnet.CostNetConstants;
import com.idsdatanet.d2.drillnet.reportDaily.ReportDailyUtils;
import com.idsdatanet.d2.drillnet.well.ChangeDatumUtils;
import com.idsdatanet.d2.drillnet.well.DefaultDatumLookupHandler;

/**
 * 
 * @author Moses
 *
 */
public class OperationUtils {
	public static final String CHECK_WELL_NAME_EXIST = "checkWellNameExist";
	public static final String CHANGE_DATUM = "changeDatum";
	private static final String QC_NOT_DONE = "0";
	public static final String CHECKING_WELL_NAME_EXIST = "checkingWellNameExist";
	public static final String CHECKING_WELLBORE_NAME_EXIST = "checkingWellboreNameExist";
	public static final String CHECKING_OPERATION_NAME_EXIST = "checkingOperationNameExist";
	/**
	 * This checking is for Add New Button
	   If user no access to any ops team, ops team list will not show
	   If accessToOpsTeamOnly is true, then required field to show
	   If accessToOpsTeamOnly is null/false, then field with non-required to show 
	 * @param node
	 * @param session
	 * @throws Exception
	 */
	public static void setAccessToOpsTeamOnlyFlag(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		Boolean accessToOpsTeamOnly = null;		
		List<OpsTeam> opsTeams = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT t FROM OpsTeam t, OpsTeamUser u WHERE t.opsTeamUid = u.opsTeamUid AND (t.isDeleted = false or t.isDeleted is null) AND (u.isDeleted = false or u.isDeleted is null) AND u.userUid = :userUid ORDER BY t.name ASC", "userUid", userSelection.getUserUid());
		if (opsTeams != null && !opsTeams.isEmpty()) {
			if (StringUtils.isNotBlank(userSelection.getUserUid())) {
				User user = ApplicationUtils.getConfiguredInstance().getCachedUser(userSelection.getUserUid());
				if (user.getAccessToOpsTeamOperationsOnly() != null) {
					if (user.getAccessToOpsTeamOperationsOnly() == true) {
						accessToOpsTeamOnly = true;
					} else {
						accessToOpsTeamOnly = false;
						node.getDynaAttr().put("opsTeams", "_empty_");
					}
				} else {
					accessToOpsTeamOnly = false;
					node.getDynaAttr().put("opsTeams", "_empty_");
				}
			}				
		} else {
			node.getDynaAttr().put("opsTeams", "_empty_");
		}
		node.getDynaAttr().put("accessToOpsTeamOnly", accessToOpsTeamOnly);
	}
	
	public static void setDefaultDatumReferencePoint(CommandBeanTreeNode node) throws Exception{
		if("ON".equals(node.getDynaAttr().get("well.onOffShore"))){
			node.getDynaAttr().put("datumReferencePoint", "GL");
		}else{
			node.getDynaAttr().put("datumReferencePoint", "MSL");
			node.getDynaAttr().put("offsetMsl", "");
		}
	}
	
	public static void autoPopulateUOMTemplateBasedOnOperationType(CommandBeanTreeNode node, String uomTemplateUid) throws Exception
	{
		Operation operation = (Operation) node.getData();
		operation.setUomTemplateUid(uomTemplateUid);
	}
	
	public static void autoPopulateOperationHourBasedOnOperationType(CommandBeanTreeNode node, String operationHour) throws Exception
	{
		Operation operation = (Operation) node.getData();
		operation.setOperationHour(operationHour);
	}
	
	public static void checkWellNameExistence(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String name = request.getParameter("name");
		String parentWellUid = request.getParameter("parentId");
		String operationType = request.getParameter("operationType");
		boolean isNewRecord = request.getParameter("isNewRecord").equals("1")? true : false;
		boolean isDuplicated = false;
		UserSession session = UserSession.getInstance(request);
		String strResult = "<wellNameExist>";
		
		if(CommonUtil.getConfiguredInstance().isValidForCheckingWellNameDuplicate(session, name, parentWellUid, operationType, isNewRecord)) {
			isDuplicated = CommonUtil.getConfiguredInstance().isWellNameDuplicate(name, operationType, parentWellUid);
		}
		strResult += "<item name=\"duplicate\" value=\"" + isDuplicated + "\" /></wellNameExist>";
		
		response.getWriter().write(strResult);
	}
	
	public static void changeDatum(BaseCommandBean commandBean, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		CommandLoggingControl loggingControl = CommandLoggingContext.getConfiguredInstance().joinOrStartLoggingCommand();
		try{
			ChangeDatumUtils.doSaveOpsDatum(commandBean, request, response);
			updateWellGroundLevelBasedOnDefaultDatum(request.getParameter("operationUid"));
		}finally{		
			loggingControl.endLoggingCommand();
		}	
		
	}
	
	public static void setCustomUom(Boolean includeWellWellbore, CommandBean commandBean, UserSelectionSnapshot userSelection) throws Exception
	{
		CustomFieldUom converter = new CustomFieldUom(commandBean);
		
		if (includeWellWellbore)
		{
			// set units for standard fields in Well.class
			for (Field field : Well.class.getDeclaredFields()) {
				converter.setReferenceMappingField(Well.class, field.getName());
				if(converter.isUOMMappingAvailable()){
					commandBean.setCustomUOM(Operation.class, "@well." + field.getName(), converter.getUOMMapping());
				}
			}
			
			// set units for dynamic fields in Wellbore.class
			Map<String, DynamicFieldMeta> wellDynamicFieldMetaMap = DynamicFieldUtil.getConfiguredInstance().getDynamicFieldMeta(Well.class, userSelection.getGroupUid());
			for (Iterator i = wellDynamicFieldMetaMap.keySet().iterator(); i.hasNext(); ) {
				String fieldName = (String) i.next();
				converter.setReferenceMappingField(Well.class, fieldName);
				if(converter.isUOMMappingAvailable()){
					commandBean.setCustomUOM(Operation.class, "@well." + fieldName, converter.getUOMMapping());
				}
			}
			
			// set units for standard fields in Wellbore.class
			for (Field field : Wellbore.class.getDeclaredFields()) {
				converter.setReferenceMappingField(Wellbore.class, field.getName());
				if(converter.isUOMMappingAvailable()){
					commandBean.setCustomUOM(Operation.class, "@wellbore." + field.getName(), converter.getUOMMapping());
				}
			}
			
			// set units for dynamic fields in Wellbore.class
			Map<String, DynamicFieldMeta> wellboreDynamicFieldMetaMap = DynamicFieldUtil.getConfiguredInstance().getDynamicFieldMeta(Wellbore.class, userSelection.getGroupUid());
			for (Iterator i = wellboreDynamicFieldMetaMap.keySet().iterator(); i.hasNext(); ) {
				String fieldName = (String) i.next();
				converter.setReferenceMappingField(Wellbore.class, fieldName);
				if(converter.isUOMMappingAvailable()){
					commandBean.setCustomUOM(Operation.class, "@wellbore." + fieldName, converter.getUOMMapping());
				}
			}
		}
		
		converter.setReferenceMappingField(OpsDatum.class, "reportingDatumOffset");
		if(converter.isUOMMappingAvailable()){
			commandBean.setCustomUOM(Operation.class, "@reportingDatumOffset", converter.getUOMMapping());
		}
		
		converter.setReferenceMappingField(OpsDatum.class, "offsetMsl");
		if(converter.isUOMMappingAvailable()){
			commandBean.setCustomUOM(Operation.class, "@offsetMsl", converter.getUOMMapping());
		}
		
		/*converter.setReferenceMappingField(OpsDatum.class, "surfaceSlantLength");
		if(converter.isUOMMappingAvailable()){
			commandBean.setCustomUOM(Operation.class, "@surfaceSlantLength", converter.getUOMMapping());
		}*/
		
		converter.setReferenceMappingField(OpsDatum.class, "slantAngle");
		if(converter.isUOMMappingAvailable()){
			commandBean.setCustomUOM(Operation.class, "@slantAngle", converter.getUOMMapping());
		}
	}
	
	public static void setDvdPlanName(CommandBeanTreeNode node, Operation operation) throws Exception
	{
		String dvdPlanUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operation.getOperationUid());
		if (dvdPlanUid!=null) {
			OperationPlanMaster operationPlanMaster = (OperationPlanMaster) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanMaster.class, dvdPlanUid);
			if (operationPlanMaster!=null) {
				node.getDynaAttr().put("planname", operationPlanMaster.getDvdPlanName());
			}
		}
	}
	
	public static String getLabelForOperationDefaultDatum(String datumUid, Locale locale, String groupUid) throws Exception{
		if (DefaultDatumLookupHandler.MSL.equals(datumUid) || StringUtils.isBlank(datumUid)) {
			return "MSL";
		} else {
			Datum datum = DatumManager.getDatum(datumUid, locale, groupUid);
			if (datum != null) {
				if (StringUtils.isBlank(datum.getDatumCode())){
					return datum.getDatumReferencePoint();
				}else {
					return datum.getDatumCode() + " to " + datum.getDatumReferencePoint();
				}
			}else{
				return null;
			}
		}
	}
	
	public static void setDatumRelatedContentOnLoad(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		Operation operation = (Operation) node.getData();
		node.getDynaAttr().put( "defaultDatumLabel", getLabelForOperationDefaultDatum(operation.getDefaultDatumUid(), node.getCommandBean().getUserLocale(), userSelection.getGroupUid()));
		node.getDynaAttr().put( "currentDatumName", node.getCommandBean().getInfo().getCurrentDatumDisplayName());

		if (StringUtils.isBlank(operation.getDefaultDatumUid())) {
			operation.setDefaultDatumUid(DefaultDatumLookupHandler.MSL);
		}
		
		node.getDynaAttr().put( "datumType", GroupWidePreference.getValue(userSelection.getGroupUid(), "setDefaultDatumReferencePoint")); // set default value of @datumType at server side, as currently there is no way to set default value for field embedded in Flex client's CreateNewComboBoxField
		node.getDynaAttr().put("entryType", "0");
		
		
		Datum currentDatum = node.getCommandBean().getInfo().getCurrentDatum();
		if(currentDatum != null){

			node.getDynaAttr().put( "currentDatumType", currentDatum.getDatumCode());
			
			node.getDynaAttr().put("currentDatumReferencePoint", currentDatum.getDatumReferencePoint());
			if (currentDatum.getIsGlReference()!=null)
				node.getDynaAttr().put("isGlReference", currentDatum.getIsGlReference()?"1":"");
			else
				node.getDynaAttr().put("isGlReference", "");
		}
	}
	
	public static void setAllowChangeOperationType(CommandBeanTreeNode node) throws Exception
	{
		Operation operation = (Operation) node.getData();
		// do not allow to change operation type when there is any report daily created
		List<Object> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select count(*) from ReportDaily where (isDeleted=false or isDeleted is null) and operationUid=:operationUid", "operationUid", operation.getOperationUid());
		if (rs != null && !rs.isEmpty()) {
			try {
				if (rs.get(0) != null) {
					int reportDailyCount = Integer.parseInt(rs.get(0).toString());
					if (reportDailyCount > 0) {
						node.getDynaAttr().put("allowEditOperationType", -1);
					}
				}
			} catch (Exception e) {
			}
		}
	}
	
	public static void setAfeAllowEditFlag(CommandBeanTreeNode node) throws Exception
	{
		Operation operation = (Operation) node.getData();
		List<Object[]> afeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationAfe o, CostAfeMaster c where (o.isDeleted=false or o.isDeleted is null) and (c.isDeleted=false or c.isDeleted is null) and (o.afeType=:afeType) and o.costAfeMasterUid=c.costAfeMasterUid and o.operationUid=:operationUid", new String[] {"afeType", "operationUid"}, new Object[] {CostNetConstants.MASTER_AFE, operation.getOperationUid()});
		if (afeList.size()>0) {
			String afeNumber = "";
			Double afeTotal = null;
			for (Object[] rec : afeList) {
				CostAfeMaster costAfeMaster = (CostAfeMaster) rec[1];
				afeNumber += ("".equals(afeNumber)?"":"\n") + costAfeMaster.getAfeNumber();
				Double thisAfeTotal = CommonUtil.getConfiguredInstance().calculateAfeTotal(costAfeMaster.getCostAfeMasterUid(), operation.getOperationUid());
				if (thisAfeTotal!=null) {
					if (afeTotal==null) afeTotal = 0.0;
					afeTotal += thisAfeTotal;
				}
			}
			operation.setAfeNumber(afeNumber);
			operation.setAfe(afeTotal);
			node.getDynaAttr().put("allowEditAfe", -1);
			node.getDynaAttr().put("allowEditAfeNumber", -1);
		}
		
		afeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationAfe o, CostAfeMaster c where (o.isDeleted=false or o.isDeleted is null) and (c.isDeleted=false or c.isDeleted is null) and (o.afeType=:afeType or o.afeType='' or o.afeType is null) and o.costAfeMasterUid=c.costAfeMasterUid and o.operationUid=:operationUid", new String[] {"afeType", "operationUid"}, new Object[] {CostNetConstants.SUPPLEMENTARY_AFE, operation.getOperationUid()});
		if (afeList.size()>0) {
			String afeNumber = "";
			Double afeTotal = null;
			for (Object[] rec : afeList) {
				CostAfeMaster costAfeMaster = (CostAfeMaster) rec[1];
				afeNumber += ("".equals(afeNumber)?"":"\n") + costAfeMaster.getAfeNumber();
				Double thisAfeTotal = CommonUtil.getConfiguredInstance().calculateAfeTotal(costAfeMaster.getCostAfeMasterUid(), operation.getOperationUid());
				if (thisAfeTotal!=null) {
					if (afeTotal==null) afeTotal = 0.0;
					afeTotal += thisAfeTotal;
				}
			}
			operation.setSecondaryafeRef(afeNumber);
			operation.setSecondaryafeAmt(afeTotal);
			node.getDynaAttr().put("allowEditSupplemanteryAfe", -1);
			node.getDynaAttr().put("allowEditSupplemanteryAfeNumber", -1);
		}
	}
	
	
	public static void calculateCostRelatedContent(CommandBeanTreeNode node, boolean includeAfeFieldEstimatedVariance, Boolean includeDrillPerDepthAndRop) throws Exception
	{
		Operation operation = (Operation) node.getData();
		
		//CALCULATE TOTAL OF AFE AND SECONDARY AFE AMOUNT
		//Double dblMainAfeAmount = 0.0;
		//Double dblSecondaryAfeAmount = 0.0;
		Double dblTotalAfeAmount = null;
		if(operation.getAfe() != null || operation.getSecondaryafeAmt() != null) dblTotalAfeAmount = 0.0;
		if(operation.getAfe() != null) dblTotalAfeAmount += operation.getAfe();
		if(operation.getSecondaryafeAmt() != null) dblTotalAfeAmount += operation.getSecondaryafeAmt();
		//dblTotalAfeAmount = dblMainAfeAmount + dblSecondaryAfeAmount;
		
		CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(), Operation.class, "afe");
		if (dblTotalAfeAmount!=null) {
			thisConverter.setBaseValue(dblTotalAfeAmount);
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@cumAfeAmount", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("cumAfeAmount", thisConverter.getConvertedValue());
		}
		
		Double thisOperationCost = CommonUtil.getConfiguredInstance().calculateOperationCost(operation.getOperationUid());
		if (thisOperationCost!=null) {
			thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
			node.getDynaAttr().put("estimatedCost", (thisOperationCost==null?0:thisOperationCost));
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@estimatedCost", thisConverter.getUOMMapping());
			}
		}	
		if (includeAfeFieldEstimatedVariance)
		{
			Double afeFieldEstimatedVariance = null;
			if (dblTotalAfeAmount!=null) afeFieldEstimatedVariance = dblTotalAfeAmount;
			if (thisOperationCost!=null) {
				if (afeFieldEstimatedVariance==null) afeFieldEstimatedVariance = 0.0;
				afeFieldEstimatedVariance -= thisOperationCost;
			}
			if (afeFieldEstimatedVariance!=null) {
				thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
				node.getDynaAttr().put("afeFieldEstimatedVariance", afeFieldEstimatedVariance);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@afeFieldEstimatedVariance", thisConverter.getUOMMapping());
				}
			}
		}
		if (includeDrillPerDepthAndRop)
		{
			thisConverter.setReferenceMappingField(DrillingParameters.class, "duration");

			//calculate total drilling hours = sum time on bottom from the drilling parameters
			Double totalDrillingHours = CommonUtil.getConfiguredInstance().calcTotalValueFromDrilParamByOperation("duration", operation.getOperationUid());
			thisConverter.setReferenceMappingField(DrillingParameters.class, "duration");
			thisConverter.setBaseValue(totalDrillingHours);
			node.getDynaAttr().put("totalDrillingHours", thisConverter.getConvertedValue());
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@totalDrillingHours", thisConverter.getUOMMapping());
			}
			
			//calculate total drilling hours = sum time on bottom from the drilling parameters
			Double totalDepthDrill = CommonUtil.getConfiguredInstance().calcTotalValueFromDrilParamByOperation("progress", operation.getOperationUid());
			thisConverter.setReferenceMappingField(DrillingParameters.class, "progress");
			thisConverter.setBaseValue(totalDepthDrill);
			totalDepthDrill = thisConverter.getConvertedValue();
			node.getDynaAttr().put("totalDepthDrill", totalDepthDrill);
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@totalDepthDrill", thisConverter.getUOMMapping());
			}
			
			if (thisOperationCost!=null && totalDepthDrill!=null) {
				if (thisOperationCost>0 && totalDepthDrill>0) {
					Double costPerDepth = thisOperationCost / totalDepthDrill;
					thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
					String costPerDepthOut = thisConverter.getFormattedValue(costPerDepth, false);
					thisConverter.setReferenceMappingField(DrillingParameters.class, "progress");
					if (thisConverter.isUOMMappingAvailable()){
						costPerDepthOut = costPerDepthOut + "/" + thisConverter.getUomSymbol();
					}
					node.getDynaAttr().put("costPerDepthDrill", costPerDepthOut);
				}
			}
			
			Double avgROP = 0.00;
			if ((totalDrillingHours > 0 && totalDepthDrill > 0)) {
				avgROP = totalDepthDrill / totalDrillingHours;
			}
			
			thisConverter.setReferenceMappingField(DrillingParameters.class, "ropAvg");
			thisConverter.setBaseValue(avgROP);
			node.getDynaAttr().put("average_ROP_drilling", thisConverter.getConvertedValue());
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@average_ROP_drilling", thisConverter.getUOMMapping());
			}
			
			
			Double totalActivityDuration = CommonUtil.getConfiguredInstance().calculateActivityDurationByOperation(operation.getOperationUid());
			thisConverter.setReferenceMappingField(DrillingParameters.class, "duration");
			thisConverter.setBaseValue(totalActivityDuration);
			node.getDynaAttr().put("totalActivityDuration", thisConverter.getConvertedValue());
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@totalActivityDuration", thisConverter.getUOMMapping());
			}
			
			Double avgRopAllTime = 0.00;
			
			if ((totalActivityDuration > 0 && totalDepthDrill > 0)) {
				avgRopAllTime = totalDepthDrill / totalActivityDuration;
			}
			
			thisConverter.setReferenceMappingField(DrillingParameters.class, "ropAvg");
			thisConverter.setBaseValue(avgRopAllTime);
			node.getDynaAttr().put("average_ROP_all_time", thisConverter.getConvertedValue());
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@average_ROP_all_time", thisConverter.getUOMMapping());
			}
		}
	}
	public static void calculateTotalFluidCost(List<Daily> dailyList, CommandBeanTreeNode node) throws Exception
	{
		Operation operation = (Operation) node.getData();
		if (dailyList.size()>0) {
			Double thisOperationFluidCost = CommonUtil.getConfiguredInstance().calculateCummulativeRigStockCost(operation.getOperationUid(), "fluids", dailyList.get(0));
			if (thisOperationFluidCost!=null) {
				CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(), RigStock.class, "itemCost");
				node.getDynaAttr().put("fluidsTotalCost", thisOperationFluidCost);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@fluidsTotalCost", thisConverter.getUOMMapping());
				}
			}		
			
		}
	}
	
	public static void calculateDaysOnWell(List<Daily> dailyList, CommandBeanTreeNode node) throws Exception
	{
		Operation operation = (Operation) node.getData();
		if (dailyList.size()>0) {
			//get the value for days on well = sum of activity hour + days spend prior to spud
			CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(), Operation.class, "days_spent_prior_to_spud");
			String dailyUid = dailyList.get(0).getDailyUid();
			thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysOnOperation(operation.getOperationUid(), dailyUid));
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@daysOnWell", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("daysOnWell", thisConverter.getConvertedValue());
		}
	}
	public static boolean isLastOperation(String wellboreUid) throws Exception{
		String strSql = "FROM Operation WHERE wellboreUid = :wellboreUid and (isDeleted IS NULL OR isDeleted = '')";
		
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellboreUid", wellboreUid);				
		if (list.size() == 1) {
			return true;
		}	
		return false;
	}
	
	public static boolean isOnShoreWell(String wellUid) throws Exception {
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select onOffShore from Well where wellUid = :wellUid", "wellUid", wellUid);
		if(result.size() > 0){
			return "ON".equals(result.get(0));
		}else{
			return false;
		}
	}
	public static String getOperationCodeLabel(CommandBean commandBean,
			CommandBeanTreeNode node, String operationCode) throws Exception {
		Map<String, LookupItem> lookupMap = commandBean.getLookupMap(node, node.getData().getClass().getSimpleName(), "operationCode");
		if (lookupMap != null) {
			LookupItem lookupItem = lookupMap.get(operationCode);
			if (lookupItem != null) {
				return (String) lookupItem.getValue();
			}
		}
		return null;
	}
	
	public static void setDefaultOperationStartDate(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(),GroupWidePreference.GWP_AUTO_POPULATE_LOCAL_TIME_AS_STARTTIME_ON_CREATE)))
		{
			Object object = node.getData();
			if (object instanceof Operation) {
				Operation operation = (Operation) object;
				operation.setStartDate(CommonUtil.getConfiguredInstance().currentDateTime(userSelection.getGroupUid(), userSelection.getWellUid())); // TODO this is not timezone sensitive, fix this
			}
		}
	}
	
	public static void populateUomTemplateForNewOperation(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		Operation operation = (Operation) node.getData();
		if (StringUtils.isNotBlank(userSelection.getUomTemplateUid())) {
			operation.setUomTemplateUid(userSelection.getUomTemplateUid());
		} else {
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from UomTemplate where (isDeleted = false or isDeleted is null) and groupUid = :groupUid and defaultTemplate = true", "groupUid", userSelection.getGroupUid());
			if (lstResult.size() > 0) {
				UomTemplate uomTemplate = (UomTemplate) lstResult.get(0);
				operation.setUomTemplateUid(uomTemplate.getUomTemplateUid());
			}
		}
	}
	
	public static void setDefaultDatumRelatedValueForNewOperation(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		Operation operation = (Operation) node.getData();		
		//set datum to null, and force user to pick rather auto and lead to data error in future
		node.getDynaAttr().put("emptyDefaultDatumUid", null);
		node.getDynaAttr().put("datumType", GroupWidePreference.getValue(userSelection.getGroupUid(), "setDefaultDatumReferencePoint"));
		node.getDynaAttr().put("entryType", "0");
		operation.setDefaultDatumUid(null);
	
		node.getDynaAttr().put("currentDatumType", GroupWidePreference.getValue(userSelection.getGroupUid(), "setDefaultDatumReferencePoint"));
		node.getDynaAttr().put("currentDatumReferencePoint", "MSL");
		node.getDynaAttr().put("entryType", "0");
		
		operation.setzRtToGround(null);
	}
	
	public static void deleteOperationFromOpTeam(Operation operation) throws Exception
	{
		if (operation != null && StringUtils.isNotBlank(operation.getOperationUid())) {
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE OpsTeamOperation SET isDeleted = true WHERE operationUid = :operationUid", new String[] { "operationUid" }, new Object[] { operation.getOperationUid() });
		}
	}
	
	public static void setTightHoleRelatedContentAfterSaveOperation(Operation operation ,UserSession session) throws Exception
	{
		if (BooleanUtils.isTrue(operation.getTightHole())) {
			// if this is a tight hole, make sure both the current user and 'idsadmin' are always in the allowed access list
			initializeDefaultTightHoleUsers(operation, session);
			
			// if this is a tight hole, set all reports and uploaded files to private
			autoSetTightHoleFilesPrivate(operation);
		} else {
			// remove all tight hole users if they exist
			removeAllTightHoleUsers(operation.getOperationUid());
		}
	}
	public static void initializeDefaultTightHoleUsers(Operation operation, UserSession session) throws Exception {
		Set<String> userUidSet = new HashSet<String>();
		userUidSet.add(session.getUserUid());
		
		List<String> rs1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT u.userUid FROM User u WHERE (u.isDeleted = false OR u.isDeleted IS NULL) AND u.userName = :userName AND u.groupUid = :groupUid", new String[] {"userName", "groupUid"}, new Object[] {"idsadmin", session.getCurrentGroupUid()});
		if (rs1 != null && !rs1.isEmpty()) {
			userUidSet.add(rs1.get(0));
		}
		
		for (String userUid : userUidSet) {
			List<String> rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT t.userUid FROM TightHoleUser t WHERE (t.isDeleted = false OR t.isDeleted IS NULL) AND t.operationUid = :operationUid AND t.userUid = :userUid", new String[] {"operationUid", "userUid"}, new Object[] {operation.getOperationUid(), userUid});
			if (rs2 == null || rs2.isEmpty()) {
				TightHoleUser t = new TightHoleUser();
				t.setGroupUid(operation.getGroupUid());
				t.setWellUid(operation.getWellUid());
				t.setWellboreUid(operation.getWellboreUid());
				t.setOperationUid(operation.getOperationUid());
				t.setUserUid(userUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(t);
			}
		}
	}
	public static void autoSetTightHoleFilesPrivate(Operation operation) throws Exception {		
		
		String strSql = "UPDATE FileManagerFiles SET isPrivate=1 WHERE attachToOperationUid =:thisOperationUid";
		String[] paramsFields = {"thisOperationUid"};
		String[] paramsValues = {operation.getOperationUid()};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
		
		strSql = "UPDATE ReportFiles SET isPrivate=1 WHERE reportOperationUid =:thisOperationUid";
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
	}
	
	public static void removeAllTightHoleUsers(String operationUid) throws Exception {
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE TightHoleUser SET isDeleted = true WHERE operationUid = :operationUid", new String[] { "operationUid" }, new String[] { operationUid });
	}
	
	public static void setLockOperationFlag(CommandBeanTreeNode node) throws Exception
	{
		Operation operation = (Operation) node.getData();
		if(operation.getIsLocked() != null && operation.getIsLocked())
		{
			node.getDynaAttr().put("editable", false);
		}
		else
		{
			node.getDynaAttr().put("editable", true);
		}
	}
	
	public static void setCanManageTightHoleUserFlag(CommandBeanTreeNode node, HttpServletRequest request) throws Exception
	{
		if (request != null) {
			UserSession userSession = UserSession.getInstance(request);
			if (userSession != null) {
				if(userSession.getCurrentUser().getCanManageTightHoleUsers() != null && userSession.getCurrentUser().getCanManageTightHoleUsers())
					node.getDynaAttr().put("canManageTightHoleUsers", 1);
			}
		}
	}
	
	public static void setCanSetTightHoleFlag(CommandBeanTreeNode node, HttpServletRequest request) throws Exception
	{
		if (request != null) {
			UserSession userSession = UserSession.getInstance(request);
			if (userSession != null) {
				if(userSession.getCurrentUser().getCanSetTightHole() != null && userSession.getCurrentUser().getCanSetTightHole())
					node.getDynaAttr().put("canSetTightHole", 1);
			}
		}
	}
	
	public static void updateOpTeams(CommandBeanTreeNode node) throws Exception
	{
		Operation operation = (Operation) node.getData();

		String concatenatedOpsTeams = String.valueOf(node.getDynaAttr().get("opsTeams"));				
		if (StringUtils.isNotBlank(concatenatedOpsTeams)) {
			if(!concatenatedOpsTeams.equalsIgnoreCase("_empty_")) {
				String opsTeams[] = concatenatedOpsTeams.split("\t"); // the default separator of ConcatenatedStringTreeNodeMultiSelect 
				if (opsTeams != null) {
					for (String opsTeam : opsTeams) {
						OpsTeamOperation opsTeamOperation = new OpsTeamOperation();
						opsTeamOperation.setOpsTeamUid(opsTeam);
						opsTeamOperation.setOperationUid(operation.getOperationUid());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(opsTeamOperation);
					}
				}
			}
		}
		
		String concatenatedOpsTeamsNotRequired = String.valueOf(node.getDynaAttr().get("opsTeamsNotRequired"));
		if (StringUtils.isNotBlank(concatenatedOpsTeamsNotRequired)) {
			String opsTeamsNotRequired[] = concatenatedOpsTeamsNotRequired.split("\t"); // the default separator of ConcatenatedStringTreeNodeMultiSelect 
			if (opsTeamsNotRequired != null) {
				for (String opsTeamNotRequired : opsTeamsNotRequired) {
					OpsTeamOperation opsTeamOperation = new OpsTeamOperation();
					opsTeamOperation.setOpsTeamUid(opsTeamNotRequired);
					opsTeamOperation.setOperationUid(operation.getOperationUid());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(opsTeamOperation);
				}
			}
		}	
	}
	
	public static void changeUOMTemplate(CommandBeanTreeNode node, HttpServletRequest request) throws Exception
	{
		Object value = node.getValue("uomTemplateUid");
		if(value != null && StringUtils.isNotBlank(value.toString())){
			UserSession userSession = UserSession.getInstance(request);
			userSession.setCurrentUOMTemplateUid(value.toString());
			UserUOMSelection.getThreadLocalInstance().synchronizeWith(userSession);
			if (node.getCommandBean().getFlexClientControl() != null) {
				node.getCommandBean().getFlexClientControl().setReloadAfterPageCancel();
				node.getCommandBean().getFlexClientControl().setForceLoadReferenceData(true);
			}
		}
	}
	public static void updateFirstDayProgress(CommandBean commandBean, Operation operation, UserSession session) throws Exception
	{
		if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DAILY_24HR_PROGRESS))){
			CommonUtil.getConfiguredInstance().updateFirstDay24HrsProgress(operation.getOperationUid(), operation.getSpudMdMsl(), commandBean, operation);
			
			if (operation.getSpudMdMsl() ==null) {
				Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
				
				if (wellbore !=null){
					CommonUtil.getConfiguredInstance().updateFirstDay24HrsProgress(operation.getOperationUid(), wellbore.getKickoffMdMsl(), commandBean, wellbore);
				}
			}
		}
	}
	
	public static void updateDatumRelatedContentAfterSave(CommandBeanTreeNode node, UserSession session, HttpServletRequest request) throws Exception
	{
		Operation operation = (Operation) node.getData();	
		if (StringUtils.isNotBlank((String)node.getDynaAttr().get("reportingDatumOffset"))) {
			// create new datum if present
			OpsDatum opsDatum = doSaveOpsDatum(node.getCommandBean(), node, session, request);
			
			// set the default datum and save the current operation again
			operation.setDefaultDatumUid(opsDatum.getOpsDatumUid());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(operation);
			
			// set current datum in session
			DatumManager.loadDatum();
			session.setCurrentUOMDatumUid(opsDatum.getOpsDatumUid(), true);
		}
		
		if(StringUtils.isNotBlank(operation.getUomTemplateUid())){
			session.setCurrentUOMTemplateUid(operation.getUomTemplateUid());
		}
		
		updateWellGroundLevelBasedOnDefaultDatum(operation.getOperationUid());
		
		node.getCommandBean().resetThreadLocalCurrentSelectedDatum();
	}
	
	/**
	 * Update well's ground level (glHeightMsl) based on operation's default datum
	 * only applicable if onoffshore == "on" and datum reference "GL"
	 * 
	 * @param operationUid
	 * @throws Exception
	 */
	public static void updateWellGroundLevelBasedOnDefaultDatum(String operationUid) throws Exception {
		List operation = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select wellUid, defaultDatumUid from Operation where operationUid = :operationUid", "operationUid", operationUid);
		if(operation.size() == 0) return;
		
		Object[] values = (Object[]) operation.get(0);
		String wellUid = (String) values[0];
		String defaultDatumUid = (String) values[1];
		
		OpsDatum opsDatum = DatumManager.getOpsDatum(defaultDatumUid);
		if(opsDatum == null) return;

		if(! "GL".equalsIgnoreCase(opsDatum.getDatumReferencePoint())) {
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update Well set glHeightMsl = null where wellUid = :wellUid", new String[]{"wellUid"}, new Object[]{wellUid}, QueryProperties.disableUOMAndDatumConversion());
			return;
		}
		
		List well = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select onOffShore from Well where wellUid = :wellUid", "wellUid", wellUid);
		if(well.size() == 0) return;
		
		if(! "ON".equals(well.get(0))) return;

		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update Well set glHeightMsl = :glHeightMsl where wellUid = :wellUid", new String[]{"glHeightMsl","wellUid"}, new Object[]{opsDatum.getOffsetMsl(), wellUid}, QueryProperties.disableUOMAndDatumConversion());
	}
	
	/*
	 * also shared by OperationDataNodeListener.java [ssjong]
	 */
	public static OpsDatum doSaveOpsDatum(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request) throws Exception {
		
		Operation operation = (Operation) node.getData();
		String reportingDatumOffset =getDynaAttr(node,"reportingDatumOffset");
		String datumType = getDynaAttr(node, "datumType");
		String datumReferencePoint = getDynaAttr(node, "datumReferencePoint");
		String offsetMsl = getDynaAttr(node, "offsetMsl");
		String entryType = getDynaAttr(node, "entryType");
		String slantAngle = getDynaAttr(node, "slantAngle");
		Boolean isGlReference = new Boolean(getDynaAttr(node, "isGlReference"));
		Double dblOffsetMsl = toDouble(offsetMsl, commandBean.getUserLocale());
		Double dblReportingDatumOffset = toDouble(reportingDatumOffset, commandBean.getUserLocale());
		Double dblSlantAngle = toDouble(slantAngle, commandBean.getUserLocale());
		Double dblSurfaceSlantLength = 0.0;
		if (StringUtils.isBlank(entryType) || entryType == null) entryType = "0";
		
		//If is reference from GL, datum type set to empty
		if(isGlReference) {
			if ("--".equalsIgnoreCase(datumType)) {
				datumType = "";
			}
		}
		
		//if (dblReportingDatumOffset > 0 && dblSlantAngle > 0) {
		if (StringUtils.isNotBlank(reportingDatumOffset) && StringUtils.isNotBlank(slantAngle) && "1".equals(entryType)) {
			Double reportingDatumOffsetRaw = 0.0;
			Double slantAngleRaw = 0.0;
			
			CustomFieldUom thisConverterField = new CustomFieldUom(commandBean, OpsDatum.class, "reportingDatumOffset");
			thisConverterField.setBaseValueFromUserValue(dblReportingDatumOffset);
			reportingDatumOffsetRaw = thisConverterField.getBasevalue();
			
			thisConverterField.setReferenceMappingField(OpsDatum.class, "slantAngle");
			thisConverterField.setBaseValueFromUserValue(dblSlantAngle);
			slantAngleRaw = thisConverterField.getBasevalue();
			
			//check angle unit is it in degree or radian, if in degree need to convert to radian for calculation
			
			double convertionFactor = 1.0;
			double thisSlantAngle;
			
			if ("Degree".equals(thisConverterField.getUomLabel())) convertionFactor = 1 / 57.2957549575;
			else if ("Gradian".equals(thisConverterField.getUomLabel())) convertionFactor = 0.015707963;
			
			thisSlantAngle = slantAngleRaw * convertionFactor;
			dblSurfaceSlantLength = reportingDatumOffsetRaw / Math.cos(thisSlantAngle);
			
			thisConverterField.setReferenceMappingField(OpsDatum.class, "surfaceSlantLength");			
			
			thisConverterField.setBaseValue(dblSurfaceSlantLength);
			dblSurfaceSlantLength = thisConverterField.getConvertedValue();
			
		}
		
		OpsDatum opsDatum = null;
		
		// check if the entered datum already exists
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OpsDatum where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and wellboreUid = :wellboreUid and wellUid = :wellUid and offsetMsl = :offsetMsl and datumCode = :datumCode and datumReferencePoint = :datumReferencePoint and reportingDatumOffset = :reportingDatumOffset and entryType = :entryType and slantAngle = :slantAngle and isGlReference = :isGlReference", 
				new String[] {"operationUid", "wellboreUid", "wellUid", "offsetMsl", "datumCode", "datumReferencePoint", "reportingDatumOffset", "entryType", "slantAngle", "isGlReference"}, 
				new Object[] {operation.getOperationUid(), operation.getWellboreUid(), operation.getWellUid(), dblOffsetMsl, datumType, datumReferencePoint, dblReportingDatumOffset, entryType, dblSlantAngle, isGlReference});
		if (list != null && list.size() > 0) {
			opsDatum = (OpsDatum) list.get(0);
		} else {
			// does not exist, create one
			String datumName = CustomFieldUom.format(commandBean, dblReportingDatumOffset, OpsDatum.class, "reportingDatumOffset").replace(" ", "") + " " + datumType;			
			opsDatum = new OpsDatum();
			opsDatum.setOperationUid(operation.getOperationUid());
			opsDatum.setWellboreUid(operation.getWellboreUid());
			opsDatum.setWellUid(operation.getWellUid());
			opsDatum.setRigInformationUid(operation.getRigInformationUid());
			opsDatum.setDatumName(datumName);
			opsDatum.setIsReference("1");
			if (StringUtils.isNotBlank(datumType)) {
				opsDatum.setDatumCode(datumType);
			}else {
				opsDatum.setDatumCode("");
			}
			opsDatum.setOffsetMsl(dblOffsetMsl);
			opsDatum.setDatumReferencePoint(datumReferencePoint);
			opsDatum.setReportingDatumOffset(dblReportingDatumOffset);
			opsDatum.setEntryType(entryType);
			opsDatum.setSlantAngle(dblSlantAngle);
			opsDatum.setSurfaceSlantLength(dblSurfaceSlantLength);
			opsDatum.setIsGlReference(isGlReference);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(opsDatum);
		}
		return opsDatum;
	}
	private static double toDouble(String text, Locale locale) {
		if (StringUtils.isNotBlank(text)) {
			try {
				return (Double) NumberUtils.parseNumber(text, Double.class, NumberFormat.getInstance(locale));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	static private String getDynaAttr(CommandBeanTreeNode node, String dynaAttrName) throws Exception {
		Object dynaAttrValue = node.getDynaAttr().get(dynaAttrName);
		if (dynaAttrValue != null) {
			return dynaAttrValue.toString();
		}
		return null;
	}
	
	public static void redirectToReportDaily(UserSession session, CommandBean commandBean,Operation operation, String currentPath, String tournetPath) throws Exception
	{
		// redirect to report daily
			String redirectToDepotWellOperationMapping = GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_MAP_WELL_OPERATION_TO_SERVER);
			if (StringUtils.isNotBlank(redirectToDepotWellOperationMapping) && StringUtils.equals(redirectToDepotWellOperationMapping, "1")) {
				commandBean.setRedirectUrl(CommonUtils.urlPathConcat(session.getApplicationContextPath(), "depotwelloperationmapping.html?fromWellOperation=1&newRecordInputMode=1"));
				commandBean.setRedirectMessage("Redirect to map well to server.");
			} else {
				// user who are not allowed to access un-QC'd data are not allowed to start new day
				if (StringUtils.isBlank(session.getCurrentUserAccessScope()) || QC_NOT_DONE.equals(session.getCurrentUserAccessScope())) {
					
					if (ModuleConstants.TOUR_MANAGEMENT.equalsIgnoreCase(session.getCurrentView()) && StringUtils.isNotBlank(tournetPath)) {
						if ("tourreportdaily.html".equals(tournetPath)) {
							commandBean.setRedirectUrl(CommonUtils.urlPathConcat(session.getApplicationContextPath(), tournetPath + "?newRecordInputMode=1"));
							commandBean.setRedirectMessage("Creating day 1 of " + operation.getOperationName());
						} else {
							commandBean.setRedirectUrl(CommonUtils.urlPathConcat(session.getApplicationContextPath(), tournetPath));
							commandBean.setRedirectMessage("Redirect to " + tournetPath);
						}
					} else {
						String redirectPage = GroupWidePreference.getValue(session.getCurrentGroupUid(), "newOperationRedirectPage");
						if(StringUtils.isNotBlank(redirectPage)){
							if("reportdaily.html".equals(redirectPage)){
								commandBean.setRedirectUrl(CommonUtils.urlPathConcat(session.getApplicationContextPath(), redirectPage + "?newRecordInputMode=1"));
								commandBean.setRedirectMessage("Creating day 1 of " + operation.getOperationName());
							}else{
								commandBean.setRedirectUrl(CommonUtils.urlPathConcat(session.getApplicationContextPath(), redirectPage));
								commandBean.setRedirectMessage("Redirect to " + redirectPage);
							}
						}else{						
							commandBean.setRedirectUrl(CommonUtils.urlPathConcat(session.getApplicationContextPath(), currentPath));
						}
					}
				} else {
					commandBean.setRedirectUrl(CommonUtils.urlPathConcat(session.getApplicationContextPath(),currentPath));
				}
			}		
	}
	
	/**
	 * Method to get operation name prefix is GWP autoOperationNamePrefix is set to yes
	 * @param userSelection 
	 * @param operation 
	 * @throws Exception
	 */
	
	public static String getOperationNamePrefix(UserSelectionSnapshot userSelection, Operation operation, Double gmtOffset) throws Exception
	{
		String getPrefixName = null;
			
		if ("1".equalsIgnoreCase(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoOperationNamePrefix")))
		{
			String prefixPattern = GroupWidePreference.getValue(userSelection.getGroupUid(), "operationNamePrefixPattern");			
			getPrefixName = CommonUtil.getConfiguredInstance().formatDisplayPrefix(prefixPattern, userSelection, operation, null, gmtOffset);
		}
		return getPrefixName;
	}
	
	/**
	 * Method to set actual costs for Santos Onshore
	 * @param commandBean 
	 * @param node
	 * @param operation 
	 * @throws Exception
	 */
	
	public static void setActualCosts(CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
		if (node.getData() instanceof Operation) {
			Operation operation = (Operation) node.getData();
			Double totalCost = 0.0; 
			String[] afeRefFields = new String[] {operation.getProjectNo(),operation.getSecondaryProjectNo(), operation.getOtherProjectNo()};
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, OperationActualCost.class, "costAmount");
			for (int i=0; i<afeRefFields.length ; i++) {
				Double operationActualCost = null;
				if (afeRefFields[i]!=null) {
					String queryString = "FROM OperationActualCost WHERE (isDeleted=false or isDeleted is null) AND operationUid=:operationUid and afeNumber=:afeNumber";
					List<OperationActualCost> actualCost = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"operationUid","afeNumber"}, new Object[]{operation.getOperationUid(), afeRefFields[i]});
					if (actualCost.size()>0) {
						OperationActualCost rec = actualCost.get(0);
						operationActualCost = rec.getCostAmount();
					}
				}
				node.getDynaAttr().put("operationActualCost.costAmount" + i, operationActualCost);
				if (operationActualCost!=null) totalCost += operationActualCost;
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@operationActualCost.costAmount" + i, thisConverter.getUOMMapping());				
				}				
			}
			node.getDynaAttr().put("totalCostAmount", totalCost);
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@totalCostAmount", thisConverter.getUOMMapping());				
			}				
		}
	}
	
	/**
	 * Method to save actual costs into OperationActualCost for Santos Onshore
	 * @param commandBean 
	 * @param node
	 * @param operation 
	 * @throws Exception
	 */
	public static void saveActualCosts(CommandBeanTreeNode node) throws Exception {
		
		if (node.getData() instanceof Operation) {
			if (node.getDynaAttr().containsKey("operationActualCost.costAmount0") || node.getDynaAttr().containsKey("operationActualCost.costAmount1") || node.getDynaAttr().containsKey("operationActualCost.costAmount2")) {
				Operation operation = (Operation) node.getData();
				
				//delete existing actual cost from operation_actual_cost table
				String queryString = "UPDATE OperationActualCost set isDeleted=1 WHERE operationUid=:operationUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(queryString, new String[]{"operationUid"}, new Object[] {operation.getOperationUid()});
				
				//save new record
				DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance(node.getCommandBean().getUserLocale());
				String[] afeRefFields = new String[] {operation.getProjectNo(),operation.getSecondaryProjectNo(), operation.getOtherProjectNo()};
				for (int i=0; i<afeRefFields.length ; i++) {
					if (!node.getDynaAttr().containsKey("operationActualCost.costAmount" + i)) continue;
					String cost = CommonUtils.null2EmptyString(node.getDynaAttr().get("operationActualCost.costAmount" + i));
					Double costAmount = null;
					if (cost!=null && StringUtils.isNotBlank(cost)) {
						if (StringUtils.isNotBlank(df.getDecimalFormatSymbols().getGroupingSeparator()+"")) {
							cost = cost.replaceAll(""+df.getDecimalFormatSymbols().getGroupingSeparator(), "");
						}
						if (StringUtils.isNotBlank(df.getDecimalFormatSymbols().getDecimalSeparator()+"")) {
							cost = cost.replace(""+df.getDecimalFormatSymbols().getDecimalSeparator(), ".");
						}
						costAmount = Double.parseDouble(cost);
						if (costAmount.isNaN()) costAmount = null;
					}
					
					if (StringUtils.isNotBlank(afeRefFields[i]) && costAmount!=null) {
						OperationActualCost actualCost = new OperationActualCost();
						actualCost.setAfeNumber(afeRefFields[i]);
						actualCost.setCostAmount(costAmount);
						actualCost.setOperationUid(operation.getOperationUid());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(actualCost);
					}
				}
			}
		}		
	}
	/**
	 * To auto calculate daySpentPriorToSpud by (spudDate - rigOnHireDate)
	 * @param operationUid
	 * @param thisConverter
	 * @return daysSpentPriorToSpud
	 * @throws Exception
	 */
	
	public static void updateDaysSpentPriorToSpud(String operationUid, CustomFieldUom thisConverter) throws Exception {
		
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			Double daysSpentPriorToSpud = 0.0;
			
			String queryString = "FROM Operation o " +
			"WHERE (o.isDeleted = false or o.isDeleted is null) " +
			"and o.operationUid=:thisOperationUid";
			List<Operation> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"thisOperationUid"}, new Object[]{operationUid}, qp);
			
			if (lstResult.size()>0) {
				if(lstResult.get(0)!=null)
				{
					Operation thisOperation = lstResult.get(0);
					//if RigOnHireDate and SpudDate are not null then do the calculation
					if (thisOperation.getSpudDate()!= null && thisOperation.getRigOnHireDate() != null){
						Calendar thisCalendar = Calendar.getInstance();
									
						//rigOnHire date
						thisCalendar.setTime(thisOperation.getRigOnHireDate());
						
						long rigOnHireDate = thisCalendar.getTimeInMillis() / 1000;
						
						//spud date
						thisCalendar.setTime(thisOperation.getSpudDate());
						
						long spudDate = (thisCalendar.getTimeInMillis() / 1000);
						//return daysSpentPriorToSpud value
						daysSpentPriorToSpud = (spudDate - rigOnHireDate) * 1.0;
						
						//get base value for daysSpentPriorToSpud 
						thisConverter.setBaseValueFromUserValue(daysSpentPriorToSpud);
						daysSpentPriorToSpud= thisConverter.getConvertedValue();
						thisOperation.setDaysSpentPriorToSpud(daysSpentPriorToSpud);
					}	
					else
					{
						thisOperation.setDaysSpentPriorToSpud(null);
					}
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisOperation, qp);
					
				}
				
			}
		}
		
	/**
	 * To check if the selected wellbore has operation or not (for copy from operation feature)
	 * @param wellboreUid
	 * @throws Exception
	 */	
	public static boolean isNewWellbore(String wellboreUid) throws Exception{
		String strSql = "FROM Operation WHERE wellboreUid = :wellboreUid and (isDeleted IS NULL OR isDeleted = '')";
		
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellboreUid", wellboreUid);				
		if (list.size()> 0) {
			return false;
		}	
		return true;
	}
	
	public static String wellCountry(String wellUid) throws Exception{
		String country = null;
		String strsql = "SELECT country FROM Well WHERE wellUid =: wellUid and (isDeleted is null or isDeleted = 0)";
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strsql, "wellUid", wellUid);
		if (result.size() > 0) {
			Object wellCountry = (Object) result.get(0);
			country = (String)wellCountry;
		}
		
		return country;
	}
	
	/**
	 * To check if the well name exist in WWO
	 * @param request
	 * @param response
	 * @throws Exception
	 */	
	public static void checkingWellNameExist(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String name = request.getParameter("name");
		boolean isNewRecord = request.getParameter("isNewRecord").equals("1")? true : false;
		boolean isDuplicated = false;
		UserSession session = UserSession.getInstance(request);
		String strResult = "<wellNameExist>";
		
		if(isValidForCheckingWellNameDuplicate(session, name, isNewRecord)) {
			isDuplicated = isWellNameDuplicated(name);
		}
		strResult += "<item name=\"duplicate\" value=\"" + isDuplicated + "\" /></wellNameExist>";
		
		response.getWriter().write(strResult);
	}
	
	/**
	 * To check if the well name is duplicate (for new record/existing)
	 * @param session
	 * @param name
	 * @param isNewRecord
	 * @throws Exception
	 */
	public static boolean isValidForCheckingWellNameDuplicate(UserSession session, String name, boolean isNewRecord) throws Exception{
		String wellUid 	= session.getCurrentWellUid();
		
		// editing an existing record
		if(! isNewRecord && StringUtils.isNotBlank(wellUid)) {
			Well currentWell = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellUid);	
			
			if(! StringUtils.equals(name, currentWell.getWellName())) {
				return true;
			}
		// else this is a new record.
		} else {
			return true;
		}
		return false;
	}
	
	/**
	 * To check if the well name got exist in the system
	 * @param wellName
	 * @throws Exception
	 */
	public static boolean isWellNameDuplicated(String wellName) throws Exception{
		String strSql = "FROM Well WHERE wellName = :wellName and (isDeleted IS NULL OR isDeleted = '')";
		
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellName", wellName);				
		if (list.size()> 0) {
			return true;
		}	
		return false;
	}
	
	
	/**
	 * To check if the wellbore name exist in WWO (in same well)
	 * @param request
	 * @param response
	 * @throws Exception
	 */	
	public static void checkingWellboreNameExist(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String name = request.getParameter("name");
		String parentWellUid = request.getParameter("parentWellUid");
		boolean isNewRecord = request.getParameter("isNewRecord").equals("1")? true : false;
		boolean isDuplicated = false;
		UserSession session = UserSession.getInstance(request);
		String strResult = "<wellboreNameExist>";
		
		if(isValidForCheckingWellboreNameDuplicate(session, name, isNewRecord)) {
			isDuplicated = isWellboreNameDuplicated(name, parentWellUid);
		}
		strResult += "<item name=\"duplicate\" value=\"" + isDuplicated + "\" /></wellboreNameExist>";
		
		response.getWriter().write(strResult);
	}
	
	/**
	 * To check if the wellbore name is duplicate (for new record/existing)
	 * @param session
	 * @param name
	 * @param isNewRecord
	 * @throws Exception
	 */
	public static boolean isValidForCheckingWellboreNameDuplicate(UserSession session, String name, boolean isNewRecord) throws Exception{
		String wellboreUid 	= session.getCurrentWellboreUid();
		
		// editing an existing record
		if(! isNewRecord && StringUtils.isNotBlank(wellboreUid)) {
			Wellbore currentWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, wellboreUid);	
			
			if(! StringUtils.equals(name, currentWellbore.getWellboreName())) {
				return true;
			}
		// else this is a new record.
		} else {
			return true;
		}
		return false;
	}
	
	/**
	 * To check if the wellbore name got exist in the same well
	 * @param wellboreName
	 * @param wellUid
	 * @throws Exception
	 */
	public static boolean isWellboreNameDuplicated(String wellboreName, String wellUid) throws Exception{
		String strSql = "FROM Wellbore WHERE wellboreName = :wellboreName and (isDeleted IS NULL OR isDeleted = '') and wellUid= :wellUid ";
		String[] paramsFields = {"wellboreName", "wellUid"};
		Object[] paramsValues = {wellboreName, wellUid};
		
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);				
		if (list.size()> 0) {
			return true;
		}	
		return false;
	}
	
	/**
	 * To check if the operation name exist in WWO (in same well, wellbore)
	 * @param request
	 * @param response
	 * @throws Exception
	 */	
	public static void checkingOperationNameExist(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String name = request.getParameter("name");
		String parentWellUid = request.getParameter("parentWellUid");
		String parentWellboreUid = request.getParameter("parentWellboreUid");
		boolean isNewRecord = request.getParameter("isNewRecord").equals("1")? true : false;
		boolean isDuplicated = false;
		UserSession session = UserSession.getInstance(request);
		String strResult = "<operationNameExist>";
		
		if(isValidForCheckingOperationNameDuplicate(session, name, isNewRecord)) {
			isDuplicated = isOperationNameDuplicated(name, parentWellUid, parentWellboreUid);
		}
		strResult += "<item name=\"duplicate\" value=\"" + isDuplicated + "\" /></operationNameExist>";
		
		response.getWriter().write(strResult);
	}
	
	/**
	 * To check if the operation name is duplicate (for new record/existing)
	 * @param session
	 * @param name
	 * @param isNewRecord
	 * @throws Exception
	 */
	public static boolean isValidForCheckingOperationNameDuplicate(UserSession session, String name, boolean isNewRecord) throws Exception{
		String operationUid = session.getCurrentOperationUid();
		
		// editing an existing record
		if(! isNewRecord && StringUtils.isNotBlank(operationUid)) {
			Operation currentOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);	
			
			if(! StringUtils.equals(name, currentOperation.getOperationName())) {
				return true;
			}
		// else this is a new record.
		} else {
			return true;
		}
		return false;
	}
	
	/**
	 * To check if the operation name got exist in the same wellbore and well
	 * @param operationName
	 * @param wellUid
	 * @param wellboreUid
	 * @throws Exception
	 */
	public static boolean isOperationNameDuplicated(String operationName, String wellUid, String wellboreUid) throws Exception{
		String strSql = "FROM Operation WHERE operationName = :operationName and (isDeleted IS NULL OR isDeleted = '') and wellUid= :wellUid and wellboreUid= :wellboreUid";
		String[] paramsFields = {"operationName", "wellUid", "wellboreUid"};
		Object[] paramsValues = {operationName, wellUid, wellboreUid};
		
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);				
		if (list.size()> 0) {
			return true;
		}	
		return false;
	}
	
	/**
	 * To redirct back to welloperation.html or operation.html based on GWP
	 * @param groupUid
	 * @throws Exception
	 */
	public static String getWWOHyperlinkRedirection(String groupUid) throws Exception{
		String operationPage = GroupWidePreference.getValue(groupUid, "wwoHyperlinkRedirection");
		if (StringUtils.isNotBlank(operationPage)){
			return operationPage;
		}
		return null;
	}
	
	/**
	 * To get the mobilization related op code
	 * @param currentOperationCode
	 * @throws Exception
	 */
	public static String getMobilizationRelatedOpCode(String currentOperationCode) throws Exception{
		String uri = "xml://mobilizationRelatedOpCode?key=code&value=label";
		Map<String, LookupItem> opCodeLookup = LookupManager.getConfiguredInstance().getLookup(uri, null, null);
		
		if (opCodeLookup.get(currentOperationCode)!=null) {
			return opCodeLookup.get(currentOperationCode).getValue().toString();
		}

		return null;
	}
	
	/**
	 * To calculate dryhole duration/cost
	 * @param operationUid
	 * @param groupUid
	 * @param spudDate
	 * @param dryHoleEndDate
	 * @throws Exception
	 */
	public static void calculateDaysSinceDryHole(String operationUid, String groupUid, Date spudDate, Date dryHoleEndDate) throws Exception{
		
		List<ReportDaily> days = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and reportType <> 'DGR' and operationUid=:operationUid", new String[] { "operationUid" }, new Object[] { operationUid });
		if (days.size()>0)
		{
			for( ReportDaily day:days){
				ReportDailyUtils.setReportDailyDryHole(day, spudDate, dryHoleEndDate);
			}
		}
	}
	
	/**
	 * To calculate spud to td duration for the whole operation report daily
	 * @param operationUid
	 * @param groupUid
	 * @param spudDate
	 * @param dryHoleEndDate
	 * @throws Exception
	 */
	public static void calculateSpudToTdDurationDaily(String operationUid, String groupUid, Date spudDate, Date tdDateTime) throws Exception{
		
		List<ReportDaily> days = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and reportType <> 'DGR' and operationUid=:operationUid", new String[] { "operationUid" }, new Object[] { operationUid });
		if (days.size()>0)
		{
			for( ReportDaily day:days){
				ReportDailyUtils.setReportDailySpudToTdDuration(day, spudDate, tdDateTime);
			}
		}
	}
	
	/** DELETE the LAR Setting **/
	public static void deleteLARmapping(String operationUid) throws Exception
	{
		if (StringUtils.isNotBlank(operationUid)) {
			List<DepotWellOperationMapping> dwoms = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from DepotWellOperationMapping where (isDeleted = false or isDeleted is null) and operationUid=:operationUid", new String[] { "operationUid" }, new Object[] { operationUid });
			if (dwoms.size()>0) {
				for (DepotWellOperationMapping dwom : dwoms) {
					List<DepotWitsmlLogRequest> dwlrs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from DepotWitsmlLogRequest where (isDeleted = false or isDeleted is null) and depotWellOperationMappingUid=:depotWellOperationMappingUid", new String[] { "depotWellOperationMappingUid" }, new Object[] { dwom.getDepotWellOperationMappingUid() });
					if (dwlrs.size()>0) {
						for (DepotWitsmlLogRequest dwlr : dwlrs) {
							List<RigStateProcessLog> rspls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from RigStateProcessLog where (isDeleted = false or isDeleted is null) and depotWitsmlLogRequestUid=:depotWitsmlLogRequestUid", new String[] { "depotWitsmlLogRequestUid" }, new Object[] { dwlr.getDepotWitsmlLogRequestUid() });
							if (rspls.size()>0) {
								for (RigStateProcessLog rspl : rspls) {
									ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE RigStateProcessLog SET isDeleted = true WHERE rigStateProcessLogUid = :rigStateProcessLogUid", new String[] { "rigStateProcessLogUid" }, new Object[] { rspl.getRigStateProcessLogUid() });
								}
							}
							ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE DepotWitsmlLogRequest SET isDeleted = true WHERE depotWitsmlLogRequestUid = :depotWitsmlLogRequestUid", new String[] { "depotWitsmlLogRequestUid" }, new Object[] { dwlr.getDepotWitsmlLogRequestUid() });
						}
					}
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE DepotWellOperationMapping SET isDeleted = true WHERE depotWellOperationMappingUid = :depotWellOperationMappingUid", new String[] { "depotWellOperationMappingUid" }, new Object[] { dwom.getDepotWellOperationMappingUid() });			
				}
			}
		}
	}
	
	//Ticket: 20030 - to calculate days lapsed for LTA event type
	public static Double calculateLastLtaDaysLapsed(String operationUid,UserSelectionSnapshot userSelection) throws Exception {
		List<Double> dayLapsedList = new ArrayList<Double>(); 
		if (!operationUid.isEmpty() ) {
			String[] paramsFields = {"operationUid"};
			Object[] paramsValues = {operationUid};
			String strSql = "FROM HseIncident " +
							"WHERE (isDeleted = FALSE OR isDeleted IS NULL) " +							
							"AND operationUid = :operationUid " + 
							"AND incidentCategory = 'LTA' "+
							"ORDER BY hseEventdatetime DESC";
			List<HseIncident>lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size() > 0){
				
					HseIncident thisHseIncident = lstResult.get(0);
								
					Date eventDateTime = thisHseIncident.getHseEventdatetime();
				    if (eventDateTime != null && StringUtils.isNotBlank(thisHseIncident.getDailyUid())) {
				  	    	
					    Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisHseIncident.getDailyUid());
					    if (daily != null) {
							Date dayDate = daily.getDayDate();
							if (dayDate != null) {	
								double daysLapsed = CommonUtil.getConfiguredInstance().calculateDaysSinceHseIncident(dayDate, eventDateTime, userSelection.getGroupUid());
								return daysLapsed;								
							}
						}
				    }
			}
			else
			{
				return 0.0;	
			}
		}
		else
		{
			return 0.0;
		}
		return 0.0;	
	}
}
