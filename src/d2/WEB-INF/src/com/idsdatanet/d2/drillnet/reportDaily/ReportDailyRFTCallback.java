package com.idsdatanet.d2.drillnet.reportDaily;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.stt.DefaultSTTActionCallback;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ReportDailyRFTCallback extends DefaultSTTActionCallback {
	public void afterSendToTownCompleted(UserSelectionSnapshot userSelection) {
	}
	
	public void afterRefreshFromTownCompleted(UserSelectionSnapshot userSelection) {
		try {
			CommonUtil.getConfiguredInstance().updateDailyCostFromCostNet(userSelection.getDailyUid());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
