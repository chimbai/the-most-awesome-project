package com.idsdatanet.d2.drillnet.report.schedule.corportatedrillingportal;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.report.DDRReportModule;
import com.idsdatanet.d2.drillnet.report.ReportJobParams;
import com.idsdatanet.d2.drillnet.report.ReportJobResult;
import com.idsdatanet.d2.drillnet.report.schedule.EmailConfiguration;

public class DailyReportExtractionService extends BaseDataTransferService{
	
	private static final String GMT_DATE_PATTERN="{system_date}";
	private static final String GMT_DATETIME_PATTERN="{system_datetime}";
	private static final String WELL_DATE_PATTERN="{well_date}";
	private static final String WELL_PATTERN="{wellname}";
	private static final String COUNTRY_PATTERN="{country}";
	private static final String RIG_PATTERN="{rigname}";
	
	private static final String EMAIL_LABEL_COUNTRY = "Country";
	private static final String EMAIL_LABEL_QC = "QC";

	private static final String EMAIL_LABEL_RIG = "Rig";
	private static final String DAY_KEY="day";
	private static final String COUNTRY_KEY="country";
	private static final String RIG_KEY="rig";
	private static final String NOT_QC_KEY = "qc";
	
	public static final int SUCCESS=1;
	public static final int ERROR=-1;
	public static final int MISSING=2;
	public static final int NOT_QCED = 3;
	
	
	private String systemDateFormat="yyyy-MM-dd";
	private String systemDateTimeFormat="yyyy-MM-dd kk:mm:ss";
	private String wellDateFormat="yyyy-MM-dd";
	private String fileNamePattern="ids-cdp_ddr_{date}_{time}";
	
	private String reportDateFormat="yyyyMMdd";
	private String reportTimeFormat="kkmmss";

	private List<Daily> validDays;
	private List<String> qcedFlags;
	private Map<String,List<Operation>> invalidList;
	private Map<String,Daily> notQCedDaily;

	private Map<String,DDRReportModule> operationToDDRList;
	
	//private DDRReportModule ddrReportModule;
	private boolean isRunning=false;

	public static DailyReportExtractionService getConfiguredInstance() throws Exception {
		return getSingletonInstance(DailyReportExtractionService.class);
	}
	
	public synchronized void run() throws Exception
	{
		if (this.getEnabled())
		{
			this.setRunning(true);
			this.validateDateForReportGeneration(new Date());
			for(Daily day:this.validDays)
			{
				this.runExtractionAndSendEmail(day,null);
			}
			if (this.hasDDRUnsend())
			{
				this.sendEmailRequirementMissingMandantory();
				this.sendEmailNoDay();
				this.sendEmailNotQCed();
			}
			this.setRunning(false);
		}
	}
	
	private DDRReportModule getDDRReportModule(Daily day) throws Exception
	{
		if (this.operationToDDRList!=null)
		{
			Operation op = ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
			return this.operationToDDRList.get(op.getOperationCode());
		}
		return null;
	}

	public synchronized Integer runExtractionAndSendEmail(Daily day, String[] emailList) throws Exception
	{
		try{
			ReportJobParams reportData=this.getDDRReportModule(day).generateDDRByDailyUid(day.getDailyUid(), this.getGenerateReportUserName(),null);
			reportData.waitOnThisObject();
			ReportJobResult jobResult=reportData.getLastReportJobResult();
			if (!jobResult.isError())
			{
				this.sendDailyEmail(day,jobResult,emailList);
				return SUCCESS;
			}
			else
			{
				this.sendErrorEmail(day);
				return ERROR;
			}
		}catch(Exception ex)
		{
			this.sendErrorEmail(day);
			this.getLogger().error(ex);
			return ERROR;
		}
	}
	
	public synchronized Integer runExtractionAndSendEmail(HttpServletRequest request, String[] emailList) throws Exception
	{
		UserSession userSession = UserSession.getInstance(request);
		Daily day=userSession.getCurrentDaily();
		Operation op=userSession.getCurrentOperation();
		boolean isDayQCed = isDailyQCed(day);
		boolean isMandantoryCompleted=this.validateOperation(op, null, false,false);
		if (isMandantoryCompleted)
			if (isDayQCed)
				return this.runExtractionAndSendEmail(day,emailList);
			else
				return NOT_QCED;
		else
			return MISSING;
	}
	
	private boolean isDailyQCed(Daily daily) throws Exception
	{
		ReportDaily reportDaily = ApplicationUtils.getConfiguredInstance().getReportDaily(daily.getDailyUid(), CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(daily.getOperationUid()));
		return this.getQcedFlags().contains(reportDaily.getQcFlag());
	}
	
	private void sendEmailRequirementMissingMandantory() throws Exception
	{
		EmailConfiguration emailConfig=new EmailConfiguration(this.getEmailConfigurations().get("missing"));
		
		Map<String,Object> mapMandantory=this.getMandantoryFieldMapping();
		if (mapMandantory!=null && mapMandantory.size()>0)
		{
			this.sendEmailNotMeetRequirement(emailConfig, "missing", mapMandantory,true);
		}
	}
	private void sendEmailNoDay() throws Exception {
		
		EmailConfiguration emailConfig=new EmailConfiguration(this.getEmailConfigurations().get("noday"));

		Map<String,Object> mapNoDay=this.getMissingDayMapping();
		if (mapNoDay!=null && mapNoDay.size()>0)
		{
			this.sendEmailNotMeetRequirement(emailConfig, "day", mapNoDay,true);
		}
	}
	private void sendEmailNotQCed() throws Exception {
		EmailConfiguration emailConfig=new EmailConfiguration(this.getEmailConfigurations().get("qc"));
		Map<String,Object> mapNotQCed = this.getNotQCMapping();
		if (mapNotQCed!=null && mapNotQCed.size()>0)
		{
			this.sendEmailNotMeetRequirement(emailConfig, "qc", mapNotQCed,false);
		}
	}
	
	private void sendEmailNotMeetRequirement(EmailConfiguration emailConfig, String mapKey, Object mapValue, boolean includeSupport) throws Exception
	{
		emailConfig.addContentMapping(mapKey, mapValue);
		emailConfig.setSubject(this.replaceDateTimePattern(new Date(),emailConfig.getSubject(),GMT_DATE_PATTERN,systemDateFormat));
		emailConfig.setEmailList(this.getEmailList(emailConfig));
		this.sendEmail(emailConfig);
	}
	
	private String[] getEmailList(EmailConfiguration emailConfig) throws Exception
	{
		if (emailConfig.getIncludeSupportEmail()!=null && emailConfig.getIncludeSupportEmail())
		{
			return (emailConfig.getSpecificEmailDistributionKey()!=null?this.getEmailListWithSupport(emailConfig.getSpecificEmailDistributionKey()):this.getEmailListWithSupport());
		}
		return emailConfig.getSpecificEmailDistributionKey()!=null?this.getEmailList(emailConfig.getSpecificEmailDistributionKey()):this.getEmailList();
	}
	
	private void sendErrorEmail(Daily day) throws Exception
	{
		String[] errorReceipient={this.getSupportEmail()};
		
		Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
		
		EmailConfiguration emailConfig=new EmailConfiguration(this.getErrorEmailConfiguration());
		emailConfig.addContentMapping("wellname",this.getCompleteOperationName(operation));
		emailConfig.addContentMapping("well_date", this.formatDate(day.getDayDate(), this.getWellDateFormat()));
		emailConfig.setEmailList(errorReceipient);
		this.sendEmail(emailConfig);
	}
	
	private void sendDailyEmail(Daily day, ReportJobResult reportJobResult,String[] emailList) throws Exception
	{
		String reportName=this.getFileNamePattern().replace("{date}",this.formatDate(new Date(), reportDateFormat)).replace("{time}",this.formatDate(new Date(),reportTimeFormat));
		String reportFileName=reportName+"."+this.getFileExtension(reportJobResult.getReportFilePath());
		ReportXml reportXml=this.createReportXml(day,reportFileName);
		File xmlFile=reportXml.writeToXML(new File(this.getXmlPath(),reportName+".xml"));

		Map<String, File> attachment = new HashMap<String,File>();
		attachment.put(xmlFile.getName(), xmlFile);
		attachment.put(reportFileName, new File(reportJobResult.getReportFilePath()));
		
		EmailConfiguration emailConfig=new EmailConfiguration(this.getValidEmailConfiguration());
		
		emailConfig.setEmailList(emailList==null?this.getEmailList(emailConfig):emailList);
		emailConfig.setSubject(this.constructEmailSubject(reportXml, day, emailConfig.getSubject()));
		emailConfig.setAttachment(attachment);
		this.sendEmail(emailConfig);
	}
	
	private ReportXml createReportXml(Daily day, String fileName) throws Exception
	{
		Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
		Well well=ApplicationUtils.getConfiguredInstance().getCachedWellByOperation(day.getOperationUid());
		Wellbore wellbore=ApplicationUtils.getConfiguredInstance().getCachedWellbore(day.getWellboreUid());
		String rigInformationUid=ApplicationUtils.getConfiguredInstance().getRigInformationUid(day.getOperationUid(), day.getDailyUid());
		RigInformation rig=(RigInformation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rigInformationUid);
		return new ReportXml(well.getCountry(), rig.getRigName(), this.getCompleteOperationName(operation),
				 fileName,this.formatDate(day.getDayDate(), wellDateFormat), wellbore.getGeoLocationCode());
	}
	
	private String constructEmailSubject(ReportXml reportXml, Daily day, String subject) throws Exception
	{
		subject = subject.replace(COUNTRY_PATTERN, reportXml.getCountry());
		subject = subject.replace(RIG_PATTERN, reportXml.getRigname());
		subject = subject.replace(WELL_PATTERN, reportXml.getWellname());
		subject = this.replaceDateTimePattern(day.getDayDate(),subject, WELL_DATE_PATTERN, wellDateFormat);
		subject = this.replaceDateTimePattern(new Date(),subject, GMT_DATETIME_PATTERN, systemDateTimeFormat);

		return subject;
	}
	
	private Map<String,Object> getMandantoryFieldMapping() throws Exception
	{
		Map<String, Object> map=new HashMap<String,Object>();
		
		List countryList=this.invalidList.get(COUNTRY_KEY);
		if (countryList!=null && countryList.size()>0)
		{
			map.put(EMAIL_LABEL_COUNTRY, this.convertListToMap(countryList));
		}
		List rigList=this.invalidList.get(RIG_KEY);
		if (rigList!=null && rigList.size()>0)
		{
			map.put(EMAIL_LABEL_RIG, this.convertListToMap(rigList));
		}
		return map;
	}
	
	private Map<String,Object> getNotQCMapping() throws Exception
	{
		List notQCList=this.invalidList.get(NOT_QC_KEY);
		if (notQCList!=null && notQCList.size()>0)
		{
			return this.convertListToMap(notQCList, this.notQCedDaily, this.getWellDateFormat());
		}
		return null;
	}
	
	private Map<String,Object> getMissingDayMapping() throws Exception
	{
		List noDayList=this.invalidList.get(DAY_KEY);
		if (noDayList!=null && noDayList.size()>0)
		{
			return this.convertListToMap(noDayList);
		}
		return null;
	}
	
	
	
	private boolean hasDDRUnsend(){
		if (this.invalidList!=null)
		{
			if (this.invalidList.size()>0)
			{
				for (Iterator iter = this.invalidList.entrySet().iterator(); iter.hasNext();) 
				{
					Map.Entry entry = (Map.Entry) iter.next();
					List list=(List)entry.getValue();
					if (list!=null && list.size()>0)
						return true;
				}
			}else
				return false;
		}
		return false;
	}
	
	public synchronized void validateDateForReportGeneration( Date now) throws Exception
	{
		List<Daily> generatableDays=CommonUtil.getConfiguredInstance().getActiveDaysForActiveOperationForReportAsAt(now);
		List<Operation> activeOperationNoDays=CommonUtil.getConfiguredInstance().getActiveOperationWithNoCurrentDay(now);
		validDays=new ArrayList<Daily>();
		invalidList=new HashMap<String,List<Operation>>();	
		notQCedDaily = new HashMap<String,Daily>();
		for (Daily day:generatableDays)
		{
			if (isOperationIncluded(day.getOperationUid()) && validateOperation(day,invalidList,true))
			{
				validDays.add(day);
			}
		}
		for (Operation op:activeOperationNoDays)
		{
			if (isOperationIncluded(op))
			{
				validateOperation(op,invalidList,true,false);
				addOperationUidToList(invalidList,DAY_KEY,op);
			}
		}
	}
	
	private boolean validateOperation(Operation operation, Map<String,List<Operation>> list, boolean addToList, boolean excludeRigValidate) throws Exception
	{
		Well well=ApplicationUtils.getConfiguredInstance().getCachedWellByOperation(operation.getOperationUid());
		String rigInformationUid=ApplicationUtils.getConfiguredInstance().getRigInformationUid(operation.getOperationUid(), null);
		if (StringUtils.isBlank(well.getCountry()) || StringUtils.isBlank(rigInformationUid))
		{
			if (StringUtils.isBlank(well.getCountry()))
				if (addToList)addOperationUidToList(list,COUNTRY_KEY,operation);
			if (StringUtils.isBlank(rigInformationUid))
				if (addToList && !excludeRigValidate)addOperationUidToList(list,RIG_KEY,operation);
			return false;
		}else{
			return true;
		}
	}
	private boolean isOperationIncluded(String operationUid) throws Exception
	{
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
		return isOperationIncluded(operation);
	}
	
	private boolean isOperationIncluded(Operation operation)
	{
		if (this.operationToDDRList!=null && this.operationToDDRList.get(operation.getOperationCode())!=null)
			return true;
		return false;
	}
	
	private boolean validateOperation(Daily day, Map<String,List<Operation>> list, boolean addToList) throws Exception
	{
		Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
		String rigInformationUid=ApplicationUtils.getConfiguredInstance().getRigInformationUid(day.getOperationUid(), day.getDailyUid());
		boolean result=true;
		if (StringUtils.isBlank(rigInformationUid))
		{
			result=false;
			if (addToList)addOperationUidToList(list,RIG_KEY,operation);
		}
		if (!isDailyQCed(day))
		{
			result = false;
			if (addToList)
			{
				this.notQCedDaily.put(day.getOperationUid(), day);
				addOperationUidToList(list,NOT_QC_KEY,operation);
			}
		}
		if (!this.validateOperation(operation, list, addToList, true))
			result=false;
		return result;
	}
	
	
	
	private boolean validateOperation(String operationUid, Map<String,List<Operation>> list, boolean excludeRigValidate) throws Exception
	{
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
		return this.validateOperation(operation, list,true, excludeRigValidate);
	}
	
	private void addOperationUidToList(Map<String,List<Operation>> list,String key, Operation operation)
	{
		if (list.get(key)==null)
			list.put(key, new ArrayList<Operation>());
	
		List al=list.get(key);
		if (!al.contains(operation))
			al.add(operation);
		list.put(key, al);
	}

	/*public void setDdrReportModule(DDRReportModule ddrReportModule) {
		this.ddrReportModule = ddrReportModule;
	}

	public DDRReportModule getDdrReportModule() {
		return ddrReportModule;
	}*/


	public void setValidDays(List<Daily> validDays) {
		this.validDays = validDays;
	}

	public List<Daily> getValidDays() {
		return validDays;
	}

	public void setInvalidList(Map<String,List<Operation>> invalidList) {
		this.invalidList = invalidList;
	}

	public Map<String,List<Operation>> getInvalidList() {
		return invalidList;
	}

	public void setSystemDateFormat(String systemDateFormat) {
		this.systemDateFormat = systemDateFormat;
	}

	public String getSystemDateFormat() {
		return systemDateFormat;
	}

	public void setWellDateFormat(String wellDateFormat) {
		this.wellDateFormat = wellDateFormat;
	}

	public String getWellDateFormat() {
		return wellDateFormat;
	}

	public void setSystemDateTimeFormat(String systemDateTimeFormat) {
		this.systemDateTimeFormat = systemDateTimeFormat;
	}

	public String getSystemDateTimeFormat() {
		return systemDateTimeFormat;
	}


	public void setFileNamePattern(String fileNamePattern) {
		this.fileNamePattern = fileNamePattern;
	}

	public String getFileNamePattern() {
		return fileNamePattern;
	}

	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public boolean isRunning() {
		return isRunning;
	}


	public void setReportDateFormat(String reportDateFormat) {
		this.reportDateFormat = reportDateFormat;
	}

	public String getReportDateFormat() {
		return reportDateFormat;
	}
	public String getReportTimeFormat() {
		return reportTimeFormat;
	}

	public void setReportTimeFormat(String reportTimeFormat) {
		this.reportTimeFormat = reportTimeFormat;
	}

	public void setQcedFlags(List<String> qcedFlags) {
		this.qcedFlags = qcedFlags;
	}

	public List<String> getQcedFlags() {
		return qcedFlags;
	}

	public void setOperationToDDRList(Map<String,DDRReportModule> operationToDDRList) {
		this.operationToDDRList = operationToDDRList;
	}

	public Map<String,DDRReportModule> getOperationToDDRList() {
		return operationToDDRList;
	}

	
	
	
}
