package com.idsdatanet.d2.drillnet.matrix;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
/**
 * Default Data Population Matrix Handler. This will be the default handler if no custom handler specified.
 * @author RYAU
 *
 */
public class DefaultMatrixHandler implements MatrixHandler{
	/**
	 * Query if current tableName has available data on current operationUid.
	 * User can give extra conditions to the query by supplying condition property in drillnet.xml.
	 * Condition includes "session.rigInformationUid", "session.wellboreUid" and so on.
	 * @param meta
	 * @param userSelection
	 * @return
	 * @throws Exception
	 */
	public void process(Matrix matrix, UserSelectionSnapshot userSelection) throws Exception {
		String tableName = matrix.getMetaDefinition().getTableName();
		
		if(StringUtils.isBlank(tableName)) return; // no processing without any table given.
		String classPathName = ApplicationUtils.getConfiguredInstance().getTablePackage();
		Class obj = Class.forName(classPathName + tableName);
		Constructor constructor = null;
		Object o = null;
		if(obj != null){
			try {
				constructor = obj.getDeclaredConstructor();
				if(constructor != null){
					o = constructor.newInstance();
				}
			}catch (InvocationTargetException x) {
	            x.printStackTrace();
	          } catch (NoSuchMethodException x) {
	            x.printStackTrace();
	          } catch (InstantiationException x) {
	            x.printStackTrace();
	          } catch (IllegalAccessException x) {
	            x.printStackTrace();
	          }
		}
		 
		Field f = null;
		if(o != null){
			try{
				f = obj.getDeclaredField("isIncomplete");
			}catch(NoSuchFieldException fieldE){
				
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		List<String> paramNames = new ArrayList<String>();
		List<Object> paramValues = new ArrayList<Object>();
		
		String sql = "";
		
		if ("ReportFiles".equalsIgnoreCase(tableName)) sql = "select distinct dailyUid from " + tableName + " where (isDeleted = false or isDeleted is null) and reportOperationUid = :operationUid";
		else if(f == null)sql = "select distinct dailyUid from " + tableName + " where (isDeleted = false or isDeleted is null) and operationUid = :operationUid";
		else if(f != null)sql = "select dailyUid,isIncomplete from " + tableName + " where (isDeleted = false or isDeleted is null) and operationUid = :operationUid";
		paramNames.add("operationUid");
		paramValues.add(userSelection.getOperationUid());
		
		// add extra condition if any
		String condition = matrix.getMetaDefinition().getCondition();
		if(StringUtils.isNotBlank(condition)) {
			sql += " and " + condition;
			sql = ApplicationUtils.getConfiguredInstance().replaceFilters(sql, paramNames, paramValues, userSelection);
		}
		
		if(f == null) {
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, paramValues);
			if(list != null && list.size() > 0) {
				for(String dailyUid : list) {
					matrix.addData(dailyUid,"N/A");
				}
			}
		}else{
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, paramValues);
			if(list != null && list.size() > 0) {
				Set<String> linkedHashSet = new LinkedHashSet<String>();
				for(Object[] daily : list) {
					String dailyUid = null;
					if(daily[0] != null) {
						dailyUid = (String) daily[0];
					}
					linkedHashSet.add(dailyUid);
				}
				for(String id: linkedHashSet) {
					String dailyUid = null;
					String isIncomplete = null;
					for(Object[] daily : list) {
						if(daily[0] != null) {
							dailyUid = (String) daily[0];
							if(id.equals(dailyUid)){
								if(daily[1] != null) {
									if(isIncomplete != "true") isIncomplete = String.valueOf(daily[1]);
								}
							}
						}
					}
					matrix.addData(id,isIncomplete);
				}
			}
		}
	}
}
