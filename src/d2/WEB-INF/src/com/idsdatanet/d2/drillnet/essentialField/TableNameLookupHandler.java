package com.idsdatanet.d2.drillnet.essentialField;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.hibernate.TableMapping;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class TableNameLookupHandler implements LookupHandler {
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> result = new TreeMap<String, LookupItem>();
		List<Class> list = TableMapping.getTableModel();
		for (Class<?> obj : list) {
			if(!obj.getSimpleName().equals("UnitMapping") && !obj.getSimpleName().equals("UomTemplate") && !obj.getSimpleName().equals("UomTemplateMapping")){
				String tableName = obj.getSimpleName();
				LookupItem item = new LookupItem(tableName, tableName);
				result.put(tableName, item);
			}
		}
		return result;
	}

}
