package com.idsdatanet.d2.drillnet.mudProperties;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.BhaComponent;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.MudRheology;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;

/**
 * All common utility related to MudProperties.
 * @author Fu
 *
 */

public class MudPropertiesUtil{
		
	/**
	 * Method used for auto - calculated on mud_pv field from Rheology reading figure - RPM600 & RPM300 (EDC usage ONLY)
	 * @param nodes
	 * @param session
	 * @param parent
	 * @throws Exception Standard Error Throwing Exception
	 * @return No Return
	 */
	public static void calculateMudPV(Collection<CommandBeanTreeNode> nodes, UserSession session, CommandBeanTreeNode parent) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);		
		String mudPropertiesUid=null;
		Double mudPv = 0.00;
		Double rpm600Reading = 0.00;
		Double rpm300Reading = 0.00;
		Integer counter600Rpm = 0;
		Integer counter300Rpm = 0;
		Double avgrpm600Reading = 0.00;
		Double avgrpm300Reading = 0.00;
		CustomFieldUom thisConverter=new CustomFieldUom(session.getUserLocale(),MudRheology.class, "rheologyReading");

		for(CommandBeanTreeNode node:nodes)
		{
			if (node.getData() instanceof MudRheology )
			{
				MudRheology mudRheology=(MudRheology) node.getData();
				Double val300 = 0.00;
				Double val600 = 0.00;
				if(mudRheology.getRpm()!= null){					
					if (mudRheology.getRpm().equals(600) && mudRheology.getRheologyReading()!= null){
						thisConverter.setBaseValueFromUserValue(mudRheology.getRheologyReading());
						val600 = thisConverter.getBasevalue();						
						rpm600Reading = rpm600Reading + val600;
						counter600Rpm = counter600Rpm + 1;
					}else if(mudRheology.getRpm().equals(300) && mudRheology.getRheologyReading()!= null){
						thisConverter.setBaseValueFromUserValue(mudRheology.getRheologyReading());
						val300 = thisConverter.getBasevalue();	
						rpm300Reading = rpm300Reading + val300;
						counter300Rpm = counter300Rpm + 1;
					} 
				}			
			}
		}
		
		if (counter600Rpm > 0){
			avgrpm600Reading = rpm600Reading / counter600Rpm;
		}
		if (counter300Rpm > 0){
			avgrpm300Reading = rpm300Reading / counter300Rpm;
		}
		
		if (counter600Rpm > 0 && counter300Rpm > 0){
			mudPv = (avgrpm600Reading - avgrpm300Reading) * 0.001;  // convert to base uom Pascalsecond			
		}else{
			mudPv = null;
		}

		if (parent.getData() instanceof MudProperties)
		{
			MudProperties mudProperties=(MudProperties)parent.getData();
			mudPropertiesUid=mudProperties.getMudPropertiesUid();
			
			if (mudPv != null){
				//set mudPV to parent node so that can be used for mudYp calculation if needed
				thisConverter.setReferenceMappingField(MudProperties.class, "mudPv");
				thisConverter.setBaseValue(mudPv);	
				mudProperties.setMudPv(thisConverter.getConvertedValue());
			}else{
				mudProperties.setMudPv(null);
			}
		}		
		
		// update mud_pv value back to parent node: mud properties
		String strSql1 = "UPDATE MudProperties SET mudPv =:mudPv WHERE mudPropertiesUid =:mudPropertiesUid";
		String[] paramsFields1 = {"mudPv", "mudPropertiesUid"};
		Object[] paramsValues1 = {mudPv, mudPropertiesUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
			
	}
	
	/**
	 * Method used for auto - calculated on mud_yp field from Rheology reading figure - RPM300 & field mud_pv (EDC usage ONLY)
	 * @param nodes
	 * @param session
	 * @param parent
	 * @throws Exception Standard Error Throwing Exception
	 * @return No Return
	 */
	public static void calculateMudYp(Collection<CommandBeanTreeNode> nodes, UserSession session, CommandBeanTreeNode parent) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);		
		String mudPropertiesUid=null;
		Double mudPv = null;
		Double rpm300Reading = 0.00;
		Integer counter300Rpm = 0;		
		Double avgrpm300Reading = 0.00;
		Double mudYp = 0.00;
		CustomFieldUom thisConverter=new CustomFieldUom(session.getUserLocale(),MudProperties.class, "mudPv");

		if (parent.getData() instanceof MudProperties)
		{
			MudProperties mudProperties=(MudProperties)parent.getData();
			mudPropertiesUid=mudProperties.getMudPropertiesUid();
			mudPv = mudProperties.getMudPv();
			if (mudPv != null){
				thisConverter.setBaseValueFromUserValue(mudPv);
				thisConverter.changeUOMUnit("Centipoise");
				mudPv = thisConverter.getConvertedValue();
			}			
		}
		
		for(CommandBeanTreeNode node:nodes)
		{
			if (node.getData() instanceof MudRheology )
			{
				MudRheology mudRheology=(MudRheology) node.getData();
				Double val300 = 0.00;				
				if(mudRheology.getRpm()!= null){
					if(mudRheology.getRpm().equals(300) && mudRheology.getRheologyReading()!= null){
						thisConverter.setReferenceMappingField(MudRheology.class, "rheologyReading");
						thisConverter.setBaseValueFromUserValue(mudRheology.getRheologyReading());
						val300 = thisConverter.getBasevalue();
						rpm300Reading = rpm300Reading + val300;
						counter300Rpm = counter300Rpm + 1;
					} 
				}			
			}
		}
		
		if (counter300Rpm > 0){
			avgrpm300Reading = rpm300Reading / counter300Rpm;
		}		
		
		if (mudPv != null && counter300Rpm > 0){
			mudYp = (avgrpm300Reading - mudPv) * 0.4788026;  // convert to base uom Pascal
		}else{
			mudYp = null;
		}
		
		// update mud_pv value back to parent node: mud properties
		String strSql1 = "UPDATE MudProperties SET mudYp =:mudYp WHERE mudPropertiesUid =:mudPropertiesUid";
		String[] paramsFields1 = {"mudYp", "mudPropertiesUid"};
		Object[] paramsValues1 = {mudYp, mudPropertiesUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
			
	}
	/**
	 * Method used calculate daily mud cost based on mud cost entered on mud screen or based on rig stock type (populate from fluid stock or rig stock screen)
	 * @param calculateMudCostFromRigStock
	 * @param stockType
	 * @param currentDate
	 * @param operationUid
	 * @throws Exception Standard Error Throwing Exception
	 * @return dailyMudCost
	 */
	
	public static Double calcDailyMudCost(Boolean calculateMudCostFromRigStock, String stockType, Date currentDate, String operationUid) throws Exception {
		Double dailyMudCost = 0.00;
		List lstResult = null;
			
		if(calculateMudCostFromRigStock){//calculate with rig stock type - fluids stock or mud stock 
			String strSql = "SELECT SUM(rs.dailycost) as totalmudcost FROM RigStock rs, Daily d WHERE " +
					"(rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
					"and d.dailyUid = rs.dailyUid AND d.dayDate = :todayDate AND rs.operationUid = :operationUid AND " +
					"rs.type=:stockType";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"todayDate", "operationUid", "stockType"}, new Object[] {currentDate, operationUid, stockType});

		
		}else {	//calculate with total mud cost enter from mud screen 		
			String strSql = "SELECT SUM(mud.mudCost) as totalmudcost FROM MudProperties mud, Daily d WHERE " +
					"(mud.isDeleted = false or mud.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
					"and d.dailyUid = mud.dailyUid AND d.dayDate = :todayDate AND mud.operationUid = :operationUid";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,  new String[] {"todayDate", "operationUid"}, new Object[] {currentDate, operationUid});	
		}
		
		if (lstResult !=null){
			Object a = (Object) lstResult.get(0);
			if (a != null) dailyMudCost = Double.parseDouble(a.toString());
		}
			
		
		return dailyMudCost;
	}
	
	/**
	 * Method used calculate cum. mud cost based on mud cost entered on mud screen or based on rig stock type (populate from fluid stock or rig stock screen)
	 * @param calculateMudCostFromRigStock
	 * @param stockType
	 * @param currentDate
	 * @param operationUid
	 * @throws Exception Standard Error Throwing Exception
	 * @return dailyMudCost
	 */
	public static Double calcCumMudCost(Boolean calculateMudCostFromRigStock, String stockType, Date currentDate, String operationUid) throws Exception {
		Double cumMudCost = 0.00;
		List lstResult = null;
		
		if(calculateMudCostFromRigStock){ //calculate with rig stock type - fluids stock or mud stock 
			String strSql = "SELECT SUM(rs.dailycost) as cummudcost FROM RigStock rs, Daily d WHERE " +
					"(rs.isDeleted = false or rs.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and " +
					"d.dailyUid = rs.dailyUid AND d.dayDate <= :todayDate AND rs.operationUid = :operationUid AND " +
					"rs.type=:stockType";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"todayDate", "operationUid", "stockType"}, new Object[] {currentDate, operationUid, stockType});

		}else {	//calculate with total mud cost enter from mud screen 		
			String strSql = "SELECT SUM(mud.mudCost) as totalmudcost FROM MudProperties mud, Daily d WHERE " +
					"(mud.isDeleted = false or mud.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) " +
					"and d.dailyUid = mud.dailyUid AND d.dayDate <= :todayDate AND mud.operationUid = :operationUid";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,  new String[] {"todayDate", "operationUid"}, new Object[] {currentDate, operationUid});	
		}
		
		if (lstResult !=null){
			Object a = (Object) lstResult.get(0);
			if (a != null) cumMudCost = Double.parseDouble(a.toString());
		}
			
		
		return cumMudCost;
	}
	
	public static void updateBHATotalWeights(String operationUid, String currentDailyUid, CustomFieldUom thisConverter) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		if (operationUid != null)
		{
			//find all bha run in this operation which needs to be updated. we need all bharuns to fulfill the criteria where bha date in is >, =, and < mud properties date
			String strSql = "SELECT bharunUid, dailyidIn, dailyidOut FROM Bharun WHERE operationUid = :operationUid AND (isDeleted = FALSE OR isDeleted IS NULL)";
			String[] paramsFields = {"operationUid"};
			Object[] paramsValues = {operationUid};
			List results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);

			if (results != null && results.size() > 0) {
				String bharunUid = null;
				Date bhaInDatetime = null;
				Date bhaOutDatetime = null;
				Date currentDate = null;
				if (currentDailyUid != null && !currentDailyUid.trim().equals("")) {
					Daily currentDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
					if (currentDaily != null) {
						currentDate = currentDaily.getDayDate();
					}
				}
				for(Object obj: results) {
					Object[] arr = (Object[])obj;
					bharunUid = null;
					bhaInDatetime = null;
					bhaOutDatetime = null;

					if (arr[0] != null && StringUtils.isNotBlank(arr[0].toString())) {
						bharunUid = arr[0].toString();
					}
					if (arr[1] != null && StringUtils.isNotBlank(arr[1].toString())) {
						Daily bhaDailyIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(arr[1].toString());
						if (bhaDailyIn != null) {
							bhaInDatetime = bhaDailyIn.getDayDate();
						}
					}
					if (arr[2] != null && StringUtils.isNotBlank(arr[2].toString())) {
						Daily bhaDailyOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(arr[2].toString());
						if (bhaDailyOut != null) {
							bhaOutDatetime = bhaDailyOut.getDayDate();
						}
					}
					
					Double buoyancyFactor = calculateBuoyancyFactor(operationUid, bhaInDatetime, bhaOutDatetime, currentDate, qp, thisConverter);
					Double totalWeightDry = calculateTotalWeightDry(bharunUid, qp, thisConverter);
					Double totalWeightBelowJarDry = calculateTotalWeightBelowJarDry(bharunUid, qp);
					Double totalWeightWet = null;
					Double totalWeightBelowJarWet = null;
					if (totalWeightDry != null && buoyancyFactor != null) {
						totalWeightWet = totalWeightDry * buoyancyFactor;
					}
					if (totalWeightBelowJarDry != null && buoyancyFactor != null) {
						totalWeightBelowJarWet = totalWeightBelowJarDry * buoyancyFactor;
					}
					updateTotalWeightsDryInBHA(totalWeightDry, totalWeightBelowJarDry, bharunUid, qp);
					
					//update bharundailysummary
					updateTotalWeightsWetInBHA(totalWeightWet, totalWeightBelowJarWet, bharunUid, currentDailyUid, qp);
				}
			}
		}
	}
	
	public static Double calculateBuoyancyFactor(String operationUid, Date bhaInDatetime, Date bhaOutDatetime, Date currentDatetime, QueryProperties qp, CustomFieldUom thisConverter) throws Exception {
		Double mudWeight = null;
		Double buoyancyFactor = null;

		List results = null;
		if (bhaInDatetime != null && bhaOutDatetime != null && currentDatetime != null) {
			String strSql2 = "SELECT m.mudWeight FROM MudProperties m, Daily d WHERE m.operationUid = d.operationUid AND m.operationUid = :operationUid AND m.dailyUid = d.dailyUid AND d.dayDate >= :bhaInDatetime AND d.dayDate <= :currentDatetime AND (m.isDeleted = FALSE OR m.isDeleted IS NULL) AND (d.isDeleted = FALSE OR d.isDeleted IS NULL) ORDER BY d.dayDate DESC, m.reportTime DESC";
			String[] paramsFields2 = {"operationUid", "bhaInDatetime", "currentDatetime"};
			Object[] paramsValues2 = {operationUid, bhaInDatetime, currentDatetime};

			results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp.setFetchFirstRowOnly());
		}
		else if (bhaInDatetime != null) {
			if (currentDatetime != null) {
				String strSql2 = "SELECT m.mudWeight FROM MudProperties m, Daily d WHERE m.operationUid = d.operationUid AND m.operationUid = :operationUid AND m.dailyUid = d.dailyUid AND d.dayDate >= :bhaInDatetime AND d.dayDate <= :currentDatetime AND (m.isDeleted = FALSE OR m.isDeleted IS NULL) AND (d.isDeleted = FALSE OR d.isDeleted IS NULL) ORDER BY d.dayDate DESC, m.reportTime DESC";
				String[] paramsFields2 = {"operationUid", "bhaInDatetime", "currentDatetime"};
				Object[] paramsValues2 = {operationUid, bhaInDatetime, currentDatetime};

				results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp.setFetchFirstRowOnly());
				
				if ((results != null && results.size() == 0) || results == null) {
					strSql2 = "SELECT m.mudWeight FROM MudProperties m, Daily d WHERE m.operationUid = d.operationUid AND m.operationUid = :operationUid AND m.dailyUid = d.dailyUid AND d.dayDate <= :bhaInDatetime AND (m.isDeleted = FALSE OR m.isDeleted IS NULL) AND (d.isDeleted = FALSE OR d.isDeleted IS NULL) ORDER BY d.dayDate DESC, m.reportTime DESC";
					paramsFields2 = new String[]{"operationUid", "bhaInDatetime"};
					paramsValues2 = new Object[]{operationUid, bhaInDatetime};
					results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp.setFetchFirstRowOnly());
				}
			}
			else {
				String strSql2 = "SELECT m.mudWeight FROM MudProperties m, Daily d WHERE m.operationUid = d.operationUid AND m.operationUid = :operationUid AND m.dailyUid = d.dailyUid AND d.dayDate <= :bhaInDatetime AND (m.isDeleted = FALSE OR m.isDeleted IS NULL) AND (d.isDeleted = FALSE OR d.isDeleted IS NULL) ORDER BY d.dayDate DESC, m.reportTime DESC";
				String[] paramsFields2 = {"operationUid", "bhaInDatetime"};
				Object[] paramsValues2 = {operationUid, bhaInDatetime};

				results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp.setFetchFirstRowOnly());
			}
		}

		if (results != null && results.size() > 0) {
			Object object = (Object) results.get(0);
			if (object != null) {
				mudWeight = Double.parseDouble(object.toString());

				//convert to ppg (us) before calc
				thisConverter.setReferenceMappingField(MudProperties.class, "mudWeight");
				thisConverter.changeUOMUnit("PoundsMassPerUSGallon");
				thisConverter.setBaseValue(mudWeight);
				mudWeight = thisConverter.getConvertedValue();
				
				if (mudWeight != null) {
					buoyancyFactor = (65.5 - mudWeight) / 65.5;
				}
			}
		}

		return buoyancyFactor;
	}
	
	public static Double calculateTotalWeightDry(String bharunUid, QueryProperties qp, CustomFieldUom thisConverter) throws Exception
	{
		Double bhaTotalWeightDry = null;
		//for all the bha components
		//get their weight and jointlength
		
		if (bharunUid != null) {
			String strSql2 = "SELECT weight, jointlength FROM BhaComponent WHERE bharunUid = :bharunUid AND (isDeleted = FALSE OR isDeleted IS NULL)";
			String[] paramsFields2 = {"bharunUid"};
			Object[] paramsValues2 = {bharunUid};

			List results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
		
			if (results != null && results.size() > 0) {
				Double weight = null;
				Double jointlength = null;
				for(Object obj: results) {
					Object[] arr = (Object[])obj;
				
					weight = null;
					jointlength = null;
					if (arr[0] != null && StringUtils.isNotBlank(arr[0].toString())) {
						weight = Double.parseDouble(arr[0].toString());
						thisConverter.setReferenceMappingField(BhaComponent.class, "weight");
						thisConverter.setBaseValueFromUserValue(weight);
						weight = thisConverter.getBasevalue();
					}
					if (arr[1] != null && StringUtils.isNotBlank(arr[1].toString())) {
						jointlength = Double.parseDouble(arr[1].toString());
						thisConverter.setReferenceMappingField(BhaComponent.class, "jointlength");
						thisConverter.setBaseValueFromUserValue(jointlength);
						jointlength = thisConverter.getBasevalue();
					}
					if (weight != null && jointlength != null) {
						//initialise for first loop and sum for the subsequent loop
						if (bhaTotalWeightDry == null) {
							bhaTotalWeightDry = weight * jointlength;
						}
						else {
							bhaTotalWeightDry += (weight * jointlength);
						}
					}
				}
			}
		}
		return bhaTotalWeightDry;
	}
	
	public static Double calculateTotalWeightBelowJarDry(String bharunUid, QueryProperties qp) throws Exception
	{
		Double weight = null;

		if (bharunUid != null) {
			Integer minimumSequence = 0;
			String strSql1 = "SELECT MIN(b.sequence) FROM BhaComponent b, LookupBhaComponent l WHERE l.lookupBhaComponentUid = b.type AND l.isJar = TRUE AND b.bharunUid = :bharunUid AND (b.isDeleted = FALSE or b.isDeleted IS NULL)";
			String[] paramsFields1 = {"bharunUid"};
			Object[] paramsValues1 = {bharunUid};
			List results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
			if (results.size() > 0) {
				Object object = (Object) results.get(0);
				if (object != null) {
					minimumSequence = Integer.parseInt(object.toString());
					
					String strSql2 = "SELECT SUM(b.weight * b.jointlength) FROM BhaComponent b WHERE b.sequence < :sequence AND b.bharunUid = :bharunUid AND (b.isDeleted = FALSE or b.isDeleted IS NULL)";
					String[] paramsFields2 = {"sequence", "bharunUid"};
					Object[] paramsValues2 = {minimumSequence, bharunUid};
					List results2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
					Object object2 = (Object) results2.get(0);
					if (object2 != null) {
						weight = Double.parseDouble(object2.toString());
					}
		 		}
			}
		}
		
		return weight;
	}

	public static void updateTotalWeightsDryInBHA(Double totalWeightDry, Double totalWeightBelowJarDry, String bharunUid, QueryProperties qp) throws Exception
	{
		String strSql1 = "UPDATE Bharun SET weightBhaTotalDry = :totalWeightDry, weightbelowjardry = :totalWeightBelowJarDry WHERE bharunUid =:thisBharunUid";
		String[] paramsFields1 = {"totalWeightDry", "totalWeightBelowJarDry", "thisBharunUid"};
		Object[] paramsValues1 = {totalWeightDry, totalWeightBelowJarDry, bharunUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
	}
	
	public static void updateTotalWeightsWetInBHA(Double totalWeightWet, Double totalWeightBelowJarWet, String bharunUid, String dailyUid, QueryProperties qp) throws Exception
	{
		String strSql1 = "UPDATE BharunDailySummary SET weightBhaTotalWet = :totalWeightWet, weightbelowjarwet = :totalWeightBelowJarWet WHERE bharunUid =:thisBharunUid AND dailyUid =:dailyUid";
		String[] paramsFields1 = {"totalWeightWet", "totalWeightBelowJarWet", "thisBharunUid", "dailyUid"};
		Object[] paramsValues1 = {totalWeightWet, totalWeightBelowJarWet, bharunUid, dailyUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
	}
}
