package com.idsdatanet.d2.drillnet.activity;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class BHIActivityCommandBeanListener extends ActivityCommandBeanListener {

	@Override
	protected void clientSpecificProcessAfterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
		
		UserSession getSession = UserSession.getInstance(request);
		String operationUid = null;
		
		if (getSession.getCurrentOperationUid() != null)
		{
			operationUid = getSession.getCurrentOperationUid();
			String querySql = "SELECT distinct incidentNumber FROM Activity WHERE (isDeleted = false OR isDeleted is null) " +						
							"AND (dayPlus <> 1 AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL)) " +
							"AND operationUid=:operationUid";						
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(querySql, "operationUid", operationUid);
			
			for (Object rec : lstResult) {
				if (rec != null)
				{
					int thisIncidentNumber = Integer.parseInt(rec.toString());
					if (!"".equals(thisIncidentNumber)) {
						this.calculateIncidentLevel(operationUid,thisIncidentNumber);
					}
				}
			}
		}
	}
	
	private void calculateIncidentLevel(String operationUid, int incidentNumber) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String querySql = "SELECT SUM(activityDuration) FROM Activity WHERE (isDeleted = false OR isDeleted is null) " +
				 "AND operationUid=:operationUid AND incidentNumber=:incidentNumber " +
				 "AND (dayPlus <> 1 AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL))";
		List lstSumDuration = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(querySql, new String[] {"operationUid", "incidentNumber"}, new Object[]{operationUid, incidentNumber}, qp);
		Object duration = (Object) lstSumDuration.get(0);
		Double sumDurationForThisIncidentNumber = Double.parseDouble(duration.toString());
		
		int incidentLevel = 0;
		
		// check the level and update the incident level of the activity with the same incident number (based on the spec given by BHI)
		if (sumDurationForThisIncidentNumber >= 0 && sumDurationForThisIncidentNumber <= 14400) {
			incidentLevel = 1;
		} else if (sumDurationForThisIncidentNumber >= 14401 && sumDurationForThisIncidentNumber <= 28800) {
			incidentLevel = 2;
		} else if (sumDurationForThisIncidentNumber >= 28801 && sumDurationForThisIncidentNumber <= 57600) {
			incidentLevel = 3;
		} else if (sumDurationForThisIncidentNumber >= 57601 && sumDurationForThisIncidentNumber <= 72000) {
			incidentLevel = 4;
		} else if (sumDurationForThisIncidentNumber >= 72001) {
			incidentLevel = 5;
		}
		
		if (incidentLevel > 0) {
			String updateSql = "UPDATE Activity SET incidentLevel=:incidentLevel WHERE (isDeleted = false OR isDeleted is null) " +
					"AND operationUid=:operationUid AND incidentNumber=:incidentNumber " +
					"AND (dayPlus <> 1 AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL))";
			String[] paramNames = {"incidentLevel","operationUid","incidentNumber"};
			Object[] paramValues = {incidentLevel, operationUid, incidentNumber};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(updateSql, paramNames, paramValues, qp);	
		} else {
			String updateSql = "UPDATE Activity SET incidentLevel=NULL WHERE (isDeleted = false OR isDeleted is null) " +
					"AND operationUid=:operationUid AND incidentNumber=:incidentNumber " +
					"AND (dayPlus <> 1 AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL))";
			String[] paramNames = {"operationUid","incidentNumber"};
			Object[] paramValues = {operationUid, incidentNumber};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(updateSql, paramNames, paramValues, qp);
		}
	}
}
