package com.idsdatanet.d2.drillnet.graph;

import java.awt.Color;
import java.awt.Font;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

import com.idsdatanet.d2.core.graph.BaseGraph;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class longestNPT extends BaseGraph {
	private String graphTitle=null;
	
	public String getGraphTitle() {
		return graphTitle;
	}

	public void setGraphTitle(String graphTitle) {
		this.graphTitle = graphTitle;
	}
	
	public Object paint() throws Exception
	{
		Locale locale = null;
		String opsType = this.getCurrentUserContext().getUserSelection().getOperationType();
		String opsUid = this.getCurrentUserContext().getUserSelection().getOperationUid();
		String dailyUid = this.getCurrentUserContext().getUserSelection().getDailyUid();
		if (this.getCurrentHttpRequest()!=null) {
			HttpServletRequest request = this.getCurrentHttpRequest();
			UserSession session = UserSession.getInstance(request);
			locale = session.getUserLocale();
		}
		
		this.setFileName("NPT_"+dailyUid+".jpg");
		
		return longestNPT.createChart(longestNPT.createDataset(opsUid, locale, dailyUid, opsType));
	}
	
	private static DefaultCategoryDataset createDataset(String opsUid, Locale locale, String dailyUid, String opsType) throws Exception 
	{
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		CustomFieldUom thisConverter = new CustomFieldUom(locale, Activity.class, "activity_duration");
        
		String row = "row";
		
		String selectReportDailySql = "SELECT reportDatetime FROM ReportDaily " +
		"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
		"AND dailyUid=:thisDailyUid ";
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(selectReportDailySql, "thisDailyUid", dailyUid);
		
		if(!lstResult.isEmpty()){
			Date thisReportDatetime = (Date) lstResult.get(0);
			String reportDailySql = "SELECT dailyUid FROM ReportDaily " +
			"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
			"AND operationUid=:operationUid " +
			"AND reportDatetime <=:thisReportDatetime " +
			"ORDER BY reportDatetime ASC";
			
			String[] reportDailyParamsFields = {"thisReportDatetime", "operationUid"};
			Object[] reportDailyParamsValues = {thisReportDatetime, opsUid};
			
			List<String> dailyLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(reportDailySql, reportDailyParamsFields, reportDailyParamsValues);
			
			if (!dailyLst.isEmpty())
			{
				String queryString = "SELECT rootCauseCode, sum(activityDuration) FROM Activity " +
	        	"WHERE internalClassCode IN ('TP', 'TU', 'IP', 'IU') " +
	        	"AND (isDeleted=false or isDeleted is null) " +
	            "AND (isSimop=false or isSimop is null) " +
	    		"AND (isOffline=false or isOffline is null) " +
	    		"AND (dayPlus IS NULL OR dayPlus=0) " +
				"AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') " + 
				"AND (dailyUid in (:thisDailyUid)) " +
	    		"GROUP BY rootCauseCode";
			
	        List<Object[]> rootCauseLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "thisDailyUid", dailyLst);
	        
		        if (!rootCauseLst.isEmpty())
				{
					for (Object[] obj : rootCauseLst)
					{
						String shortCode = (String) obj[0];
						Double duration = (Double) obj[1];
						if (thisConverter !=null)
						{
							thisConverter.setBaseValue(duration);
							duration = thisConverter.getConvertedValue();
						}
						
						String[] paramFields = {"thisShortCode","thisOperationType"};
						String[] paramValues = {shortCode, opsType};
						
						String queryString2 = "SELECT name FROM LookupRootCauseCode WHERE shortCode = :thisShortCode AND (isDeleted=false or isDeleted is null) AND (operationCode = :thisOperationType OR operationCode = '')";
						List<String> rcName = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString2, paramFields, paramValues);
						
						if(!rcName.isEmpty() && duration > 5)
							dataset.addValue(duration,row,rcName.get(0));
					}
				}
			}
		}
		
		return dataset;
	}
	
	private static JFreeChart createChart (DefaultCategoryDataset dataset) throws Exception
	{
		JFreeChart chart = ChartFactory.createBarChart(null, null, null, dataset, PlotOrientation.VERTICAL, false, false, false);
		chart.setBackgroundPaint(Color.WHITE);
		 
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		plot.setBackgroundPaint(Color.white);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		
	  // set the range axis to display integers only...
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        rangeAxis.setTickLabelFont(new Font("Arial", Font.PLAIN, 20));

        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setShadowVisible(false);
        renderer.setSeriesItemLabelGenerator(0,new StandardCategoryItemLabelGenerator("{2}",NumberFormat.getNumberInstance())); 
        renderer.setSeriesItemLabelsVisible(0, true);

        CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
        domainAxis.setMaximumCategoryLabelLines(2);
        domainAxis.setTickLabelFont(new Font("Arial", Font.PLAIN, 20));
        
		 
		return chart;
	}
	

	
}
