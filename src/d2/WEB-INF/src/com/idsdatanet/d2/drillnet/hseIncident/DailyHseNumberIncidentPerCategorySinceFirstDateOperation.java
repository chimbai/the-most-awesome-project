package com.idsdatanet.d2.drillnet.hseIncident;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class DailyHseNumberIncidentPerCategorySinceFirstDateOperation implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// To be overriden
	}
	
	// Ticket: 44400 - Change HSE Event Count to be Well-based (All until TODAY - reportDay)
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
	    String wellUid = userContext.getUserSelection().getWellUid();
	    Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
        Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
        if (daily == null) {
        	return;
        }
        Date todayDate = daily.getDayDate();
        Date startDate = operation.getStartDate();
        Calendar thisCalendar = Calendar.getInstance();
        
    	if(startDate!=null){
    		thisCalendar.setTime(startDate);
    	    thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
    	    thisCalendar.set(Calendar.MINUTE, 0);
    	    thisCalendar.set(Calendar.SECOND , 0);
    	    thisCalendar.set(Calendar.MILLISECOND , 0);
    	    startDate = thisCalendar.getTime();
    	}
        
        String strSql = "SELECT h.incidentCategory, SUM(h.numberOfIncidents) AS sumNumber FROM HseIncident h, Daily d " +
                "WHERE (h.isDeleted = false OR h.isDeleted IS NULL) " +
                "AND (d.isDeleted = false or d.isDeleted IS NULL) " +
                "AND h.wellUid = :wellUid " +
                "AND d.dailyUid = h.dailyUid " +
                "AND d.dayDate <= :todayDate " + 
                "AND (h.hseEventdatetime >= :startDate AND h.hseEventdatetime <= :todayDate) " +
                "GROUP BY h.incidentCategory";
        
        String[] paramsFields = {"todayDate", "startDate", "wellUid"};
        Object[] paramsValues = {todayDate, startDate, wellUid};
		
		List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (!lstResult.isEmpty()) {
			for (Object objResult: lstResult) {
				Object[] obj = (Object[]) objResult;
				String incidentCategory = (obj[0] != null) ? obj[0].toString() : "";
				String sumNumberOfIncidents = (obj[1] != null) ? obj[1].toString() : "";
				ReportDataNode thisReportNode = reportDataNode.addChild("numberIncidentsPerCategory");
				thisReportNode.addProperty("incidentCategory", incidentCategory);
				thisReportNode.addProperty("sumNumberOfIncidents", sumNumberOfIncidents);
			}
		}
	}	
}		
