package com.idsdatanet.d2.drillnet.keyPerformanceIndicator;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.KeyPerformanceIndicator;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class KeyPerformanceIndicatorDataNodeListener extends EmptyDataNodeListener{
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		 
		if (obj instanceof KeyPerformanceIndicator){ 
			
			KeyPerformanceIndicatorUtils.calc_npt_percent(session);
			
			
		}
	}
} 


