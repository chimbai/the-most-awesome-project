package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationEvent;

import com.idsdatanet.d2.core.model.OperationPlanMaster;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;
import com.idsdatanet.d2.core.web.security.BaseAclPermission;
import com.idsdatanet.d2.core.web.security.acl.ACLKey;
import com.idsdatanet.d2.core.web.security.acl.ACLKeyResolver;
import com.idsdatanet.d2.core.web.security.acl.AccessConditions;

public class DvdPlanLocker implements ActionManager, ACLKeyResolver, DataNodeAllowedAction {
	
	private static final String LOCK_DVD_PLAN = "lockDVDPlan";

	@Override
	public void resolveACLKey(ACLKey aclKey, AccessConditions accessConditions) throws Exception {
		UserSession session = accessConditions.getUserSession();
		
		if(session == null) {
			return;
		}
		
		String key = aclKey.getACLKey();
		if(StringUtils.isBlank(key)) {
			return;
		}
		
		if (LOCK_DVD_PLAN.equalsIgnoreCase(key)) {
			Boolean allowToLockDvdPlan = session.getCurrentUser().getAllowToLockDvdPlan();
			aclKey.setACLKeyValue(BooleanUtils.isTrue(allowToLockDvdPlan));
			return;
		}
		
		//Lock Plan should lock DVD screen only,  will not lock file manager tab
		if("filemanager".equalsIgnoreCase(key)) {
			return;
		}
		
		String sql = "from OperationPlanMaster where (isDeleted is null or isDeleted = false) and operationUid = :operationUid";
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"operationUid"}, new Object[] {session.getCurrentOperation().getOperationUid()});
		if(result != null && result.size() > 0) {
			OperationPlanMaster rec = (OperationPlanMaster) result.get(0);
			if(rec != null && BooleanUtils.isTrue(rec.getIsLocked())) {
				aclKey.setACLKeyValue(BaseAclPermission.ACL_KEY_READ.equalsIgnoreCase(aclKey.getACLKey()));
				return;
			}
		}
	}

	@Override
	public void refreshACLCacheIfAny(ApplicationEvent event) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if(StringUtils.isNotBlank(action) && action.equals(LOCK_DVD_PLAN)) {
			return new LockDvDPlanActionHandler();
		}
		return null;
	}

	@Override
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action,
			HttpServletRequest request) throws Exception {
		CommandBeanTreeNode dataNode = getOperationPlanMaster(node);
		if(dataNode != null) {
			OperationPlanMaster data = (OperationPlanMaster) dataNode.getData();
			if(data.getIsLocked() != null) {
				return !data.getIsLocked();
			}
		}
		return true;
	}
	
	private CommandBeanTreeNode getOperationPlanMaster(CommandBeanTreeNode node) throws Exception{
		if(node.getData() != null && node.getData() instanceof OperationPlanMaster) {
			return node;
		}else if(node.getParent() != null) {
			return getOperationPlanMaster(node.getParent());
		}
		return null;
	}
	
	private class LockDvDPlanActionHandler implements ActionHandler {
		@Override
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			if(node.isRoot()) {
				Map<String, CommandBeanTreeNode> children = node.getChild(OperationPlanMaster.class.getSimpleName());
				for(Map.Entry<String, CommandBeanTreeNode> childNode : children.entrySet()) {
					CommandBeanTreeNode dataNode = childNode.getValue();
					OperationPlanMaster data = (OperationPlanMaster) dataNode.getData();
					data.setIsLocked(!BooleanUtils.isTrue(data.getIsLocked()));
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(data);
				}
				
				if (commandBean.getFlexClientControl() != null) {
					commandBean.getFlexClientControl().setForceLoadReferenceData(true);
				}
			}
			
			return new ActionHandlerResponse(true);
		}
	}
	
	public void lockAllDvdPlans(CommandBeanTreeNode node) throws Exception{
		if(node == null || !node.isRoot()) {
			return;
		}
		
		Map<String, CommandBeanTreeNode> dvdPlanNodes = node.getChild(OperationPlanMaster.class.getSimpleName());
		
		if(dvdPlanNodes == null || dvdPlanNodes.size() == 0) {
			return;
		}
		
		for(Map.Entry<String, CommandBeanTreeNode> dvdPlanNode : dvdPlanNodes.entrySet()) {
			CommandBeanTreeNode dataNode = dvdPlanNode.getValue();
			OperationPlanMaster data = (OperationPlanMaster) dataNode.getData();
			data.setIsLocked(true);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(data);
		}
	}


}
