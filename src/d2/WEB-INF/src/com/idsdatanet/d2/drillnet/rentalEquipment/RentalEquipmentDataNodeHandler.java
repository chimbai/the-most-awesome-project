package com.idsdatanet.d2.drillnet.rentalEquipment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.RentalEquipment;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class RentalEquipmentDataNodeHandler implements DataNodeLoadHandler {

    public static String commonLookupType = "rental_equipment.status";

    @Override
    public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta arg0) {
        return true;
    }

    @Override
    public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
        
        if (meta.getTableClass().equals(RentalEquipment.class)) {
            
            String strSql = "FROM RentalEquipment WHERE (isDeleted is null or isDeleted = false) AND dailyUid = :dailyUid";
            String[] paramNames =  {"dailyUid"};
            Object[] paramValues = {userSelection.getDailyUid()};
                        
            List<RentalEquipment> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);          
            List<Object> output_maps = new ArrayList<Object>();
            
            Collections.sort(items, new RentalEquipmentComparator());
            for(Object  objResult: items){                
                RentalEquipment rentalEquipmentObj = (RentalEquipment) objResult;
                output_maps.add(rentalEquipmentObj);
            }
            return output_maps;
        }
        return null;
    }

    private class RentalEquipmentComparator implements Comparator<RentalEquipment> {
        public int compare(RentalEquipment r1, RentalEquipment r2) {
            int i;
            try {
                // Get CommonLookup Objects, order by Sequence ASC
                CommonLookup c1 = this.getCommonLookup(r1.getStatus());
                CommonLookup c2 = this.getCommonLookup(r2.getStatus());
                if (c1 == null || c2 == null) return 0;
                
                Integer s1 = c1.getSequence();
                Integer s2 = c2.getSequence();
                if (s1 == null || s2 == null) return 0;
                i = s1.compareTo(s2);
                if (i != 0) {
                    return i;
                }
                
                // Label the same, order by arrival date ASC
                Date d1 = r1.getArrivalDatetime();
                Date d2 = r2.getArrivalDatetime();
                if (d1 == null || d2 == null) return 0;
                i = d1.compareTo(d2);
                if (i != 0) {
                    return i;
                }
                /*
                // Arrival Date same, order by Equipment name ASC
                String e1 = WellNameUtil.getPaddedStr(r1.getEquipmentName());
                String e2 = WellNameUtil.getPaddedStr(r2.getEquipmentName());
                if (e1 == null || e2 == null) return 0;
                i = e1.compareTo(e2);
                if (i != 0) {
                    return i;
                }*/
                
                // Can add more if needed...
                
                return i;
            } catch(Exception e) {
                e.printStackTrace();
                return 0;
            }
        }
        
        private CommonLookup getCommonLookup(String shortCode) throws Exception {
            String strSql = "FROM CommonLookup WHERE (isDeleted is null or isDeleted = false) AND lookupTypeSelection = :lookupTypeSelection AND shortCode = :shortCode ORDER BY sequence ASC";
            String[] paramNames =  {"lookupTypeSelection","shortCode"};
            Object[] paramValues = {RentalEquipmentDataNodeHandler.commonLookupType, shortCode};
                        
            List<CommonLookup> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);          
            if(items.size() > 0) {
                return items.get(0);
            }
            return null;
        }
    }
}
