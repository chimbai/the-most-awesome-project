package com.idsdatanet.d2.drillnet.well;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellDataNodeAllowedAction implements DataNodeAllowedAction {
	
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		if (StringUtils.equals(targetClass, "Well")) {
			if (StringUtils.equals(action, "add")) return false; 
			if (StringUtils.equals(action, "pasteAsNew")) return false;
		}
		return true;
	}

}
