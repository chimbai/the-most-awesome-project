package com.idsdatanet.d2.drillnet.report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;

public class MultiWellRushmoreDPRReportModule extends MultiWellDPRReportModule {

	private Map<String, String> supportOperationTypes = null;
	
	public void setOperationTypeFilters(Map<String,String> operationTypes) {
		this.supportOperationTypes = operationTypes;
	}
	
	public Map<String, String> getOperationTypeFilters() {
		return this.supportOperationTypes;
	}
		
	@Override
	
	
	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// Constant used to obtain the list of operations
		if ("loadYearList".equals(invocationKey)) {
			
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");	
			
			LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
			String sql = "SELECT DISTINCT YEAR(op.startDate) FROM Operation op " +
			" WHERE (op.isDeleted = false or op.isDeleted is null)"+
			" ORDER BY YEAR(op.startDate) DESC";
			
			List<Integer> getList =  ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
			
			writer.startElement("Years");
			writer.addElement("year", "");
			writer.endElement();
			
			if(getList.size() > 0) {
				for (Integer objs : getList) {
					if (objs != null){
						result.put(objs.toString(), new LookupItem(objs.toString(),objs.toString()));
						writer.startElement("Years");
						writer.addElement("year", objs.toString());
						writer.endElement();
					}
				}
				writer.endElement();
				writer.close();
			}
		}
		
		else if ("flexMultiWellReportLoadOperationList".equals(invocationKey)) {
			UserSession userSession = UserSession.getInstance(request);
			// Use the existing operation lookup defined in the ADAT
			String year = request.getParameter("selectedyear");
			List<String> listOps = new ArrayList();
			Operations operationList = userSession.getCachedAllAccessibleOperations();
			if (year !=null && year !="") {
				listOps = this.getOperationFromYear(year);
			}
			if (operationList == null || operationList.size() == 0) return;
			
			List list = new ArrayList();
			for (Map.Entry<_Operation, _Operation> o : operationList.entrySet()) {
				_Operation operation = o.getValue();
				if (listOps.size()>0){
					if (listOps.contains(operation.getOperationUid())){
					if (this.supportOperationTypes==null || this.supportOperationTypes.containsKey(operation.getOperationCode())) {
						operation.setOperationName(CommonUtil.getConfiguredInstance().getCompleteOperationName(userSession.getCurrentGroupUid(), operation.getOperationUid()));
						list.add(operation);
					}
					}
				}else{
					if (this.supportOperationTypes==null || this.supportOperationTypes.containsKey(operation.getOperationCode())) {
						operation.setOperationName(CommonUtil.getConfiguredInstance().getCompleteOperationName(userSession.getCurrentGroupUid(), operation.getOperationUid()));
						list.add(operation);
					}	
				}
			}
			
			
			//sort operation list
			Collections.sort(list, new Comparator() {
				public int compare(Object o1, Object o2) {
					_Operation object1 = (_Operation) o1;
					_Operation object2 = (_Operation) o2;
					if (object1==null || object2==null) return 0;
					String op1 = WellNameUtil.getPaddedStr(object1.getOperationName());
					String op2 = WellNameUtil.getPaddedStr(object2.getOperationName());
					return (op1).compareTo(op2);
				}
			});
			
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
		    for (Iterator it = list.iterator(); it.hasNext();) {
		    	_Operation operation = (_Operation)it.next();
				writer.startElement("Operation");
				writer.addElement("operationUid", operation.getOperationUid());
				writer.addElement("operationName", operation.getOperationName());
				writer.endElement();
			}
			writer.endElement();
			writer.close();
		} else {
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);
		}
	}
	
	private List<String> getOperationFromYear(String year) throws Exception{
		List<String> operation = new ArrayList<String>();
		if (year !=null) {
			List<Operation> listx = ApplicationUtils.getConfiguredInstance().getDaoManager().find("From Operation WHERE (isDeleted is null or isDeleted = false)" +
									" AND YEAR(startDate)=" + year);
			
			for (Operation list2: listx){
				operation.add(list2.getOperationUid());
			}
			
		}
		return operation;
	}
}
