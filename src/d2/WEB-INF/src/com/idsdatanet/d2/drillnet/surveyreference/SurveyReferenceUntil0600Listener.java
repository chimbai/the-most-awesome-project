package com.idsdatanet.d2.drillnet.surveyreference;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class SurveyReferenceUntil0600Listener implements DataLoaderInterceptor {
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return query;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(meta.getTableClass().equals(SurveyStation.class)){
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null){
				return conditionClause.replace("{_daily_filter_}", "");
			}
			
			if(StringUtils.isNotBlank(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_NUMBER_OF_SURVEY_LINES)))
			{
				query.rowsToFetch = Integer.parseInt(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_NUMBER_OF_SURVEY_LINES).toString());
			}
			else
			{
				query.rowsToFetch = 5;
				
			}
			String thisFilter = "";
			
			//get today depth from daily report just to fetch station that less then last 0600 depth recorded
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(daily.getDayDate());
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			
			String strSql = "SELECT MAX(rd.depth0600MdMsl) as max0600Depth FROM ReportDaily rd, Daily d WHERE (rd.isDeleted = false or rd.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and rd.dailyUid = d.dailyUid AND d.dayDate <= :todayDate AND d.operationUid = :thisOperationUid";
			
			String[] paramsFields = {"todayDate", "thisOperationUid"};
			Object[] paramsValues = {calendar.getTime(), daily.getOperationUid()};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			
			if (conditionClause.indexOf("{_daily_filter_}") != -1 && !lstResult.isEmpty())
			{
				Object a = (Object) lstResult.get(0);
				Double max0600Depth = 0.00;
				if (a != null) max0600Depth = Double.parseDouble(a.toString());
				query.addParam("thisDepth", max0600Depth);
				thisFilter = "depthMdMsl <= :thisDepth";
			}
			else
			{
				query.rowsToFetch = 0;
			}
			return conditionClause.replace("{_daily_filter_}", thisFilter);

		}else{
			return null;
		}
	}	
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}	
}
