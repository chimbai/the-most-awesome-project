package com.idsdatanet.d2.drillnet.report;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.multiselect.TreeNodeMultiSelect;

public class ActivityReportSummaryModule extends DefaultReportModule{
	
	private Map<String,Integer> optionLimit;
	
	public void init(CommandBean commandBean){
		super.init(commandBean);
		if(commandBean instanceof BaseCommandBean){
			
			try {
				//commandBean.getRoot().getDynaAttr().put(WELL_FIELD, null);
				//commandBean.getRoot().getDynaAttr().put(FIELD_FIELD, null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
		
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		commandBean.getSystemMessage().clear();
		UserSelectionSnapshot userSelection=new UserSelectionSnapshot(session);
		
		Map<String,Object> reportOptions = this.getOptionAsReportOption(userSelection, request, commandBean);
		if (this.isMinimumOptionSelected(reportOptions))
			{
				if (this.validateReportOptionLimit(reportOptions))
				{
					userSelection.setCustomProperties(reportOptions);
					return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));
				}else{
					commandBean.getSystemMessage().addError("Some multi select option is over the selection limit", true);
				}
			}else{
				commandBean.getSystemMessage().addError("There is some selection that aren't selected at all", true);					
			}
			
		return null;
	}
		
	
	protected Map<String,Object> getOptionAsReportOption(UserSelectionSnapshot userSelection,HttpServletRequest request, CommandBean commandBean) throws Exception
	{		
		Map<String,List<String>> mapMultiSelect = commandBean.getRoot().getMultiSelect().getValues();
		
		Map<String,TreeNodeMultiSelect> multiSelectList = commandBean.getRoot().getMultiSelect().getMultiSelectDefinition();
		
		Map<String,Object> rptOption = new LinkedHashMap<String,Object>();
				
		if (multiSelectList != null)
		{
			for (Entry<String, TreeNodeMultiSelect> entry : multiSelectList.entrySet())
			{
				String field = entry.getKey();
				Map<String,LookupItem> obj=((BaseCommandBean)commandBean).getMultiSelectLookupMap(null,field);
				if (obj!=null)
				{
					Map<String,Object> multiResult = new LinkedHashMap<String,Object>();
					List<String> values = mapMultiSelect.get(field);
					if (values!=null)
					{
						for (String item : values)
						{
							LookupItem lookup = obj.get(item);
							if (lookup!=null)
								multiResult.put(item, lookup.getValue());
						}
					}
					rptOption.put(field, ReportOption.createMultiSelectReportOption(values,multiResult));
				}
			}
		}
		
		return rptOption;
	}
	
	private boolean isMinimumOptionSelected(Map<String,Object> reportOptions)
	{
		for (Map.Entry<String, Object> entry : reportOptions.entrySet())
		{
			ReportOption op = (ReportOption) entry.getValue();
			if (op!=null && op.getIsMultiOption() && op.getValues().size()==0)
				return false;
		}
		return true;
	}
	
	private boolean validateReportOptionLimit(Map<String,Object> reportOptions)
	{
		if (this.optionLimit!=null)
		{
			for (Map.Entry<String, Integer> entry : optionLimit.entrySet())
			{
				String field = entry.getKey();
				Integer limit = entry.getValue();
				ReportOption op = (ReportOption) reportOptions.get(field);
				if (op!=null && op.getIsMultiOption() && op.getValues().size()> limit)
					return false;
			}
		}
		return true;
	}
	
	
	public void setOptionLimit(Map<String,Integer> optionLimit) {
		this.optionLimit = optionLimit;
	}

	public Map<String,Integer> getOptionLimit() {
		return optionLimit;
	}	
}
