package com.idsdatanet.d2.drillnet.bharun;

import java.util.List;

import com.idsdatanet.d2.core.report.validation.CommandBeanReportValidator;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.AjaxAutoPopulateListener;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanConfiguration;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanReportDataListener;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataSummaryListener;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxActionHandler;

public class BharunCommandBeanConfig extends CommandBeanConfiguration {
	
	private Boolean dateTimeValidation = false;
	private Boolean TWcalculation = false;
	public boolean weightMassForce = false;
	
	public void init(BaseCommandBean commandBean) throws Exception{
	}
	
	public void setDateTimeValidation(Boolean value) {
		this.dateTimeValidation = value;
	}
	
	public void setTWcalculation(Boolean value) {
		this.TWcalculation = value;
	}
	
	public void setWeightMassForce(Boolean value){
		this.weightMassForce = value;
	}
	
	public DataNodeListener getDataNodeListener() throws Exception{
		BharunDataNodeListener listener = new BharunDataNodeListener();
		listener.setDateTimeValidation(this.dateTimeValidation);
		/* listener.setautoincrementBharunnumber(this.autoincrementBharunnumber); */
//		listener.settorquecalculation(this.torquecalculation);
		listener.setTWcalculation(this.TWcalculation);
		listener.setWeightMassForce(this.weightMassForce);
		return listener;
	}
	
	public CommandBeanListener getCommandBeanListener() throws Exception{
		return new com.idsdatanet.d2.drillnet.bharun.BharunCommandBeanListener();
	}
	
	public DataLoaderInterceptor getDataLoaderInterceptor() throws Exception{
		return new com.idsdatanet.d2.drillnet.bharun.BharunDataNodeListener();
	}
	
	public ActionManager getActionManager() throws Exception {return null;}

	public DataNodeAllowedAction getDataNodeAllowedAction() throws Exception{
		return new com.idsdatanet.d2.drillnet.bharun.BharunDataNodeAllowedAction();
	}
	
	public DataNodeLoadHandler getDataNodeLoadHandler() throws Exception {return null;}
	
	public SimpleAjaxActionHandler getSimpleAjaxActionHandler() throws Exception {return null;}
	
	public AjaxAutoPopulateListener getAjaxAutoPopulateListener() throws Exception {return null;}
	
	public CommandBeanReportDataListener getCommandBeanReportDataListener() throws Exception {return null;}
	
	public DataSummaryListener getDataSummaryListener() throws Exception {
		return new com.idsdatanet.d2.drillnet.bharun.BharunDataSummaryListener();
	}
	
	public List<CommandBeanReportValidator> getCommandBeanReportValidators() throws Exception {
		return null;
	}
}
