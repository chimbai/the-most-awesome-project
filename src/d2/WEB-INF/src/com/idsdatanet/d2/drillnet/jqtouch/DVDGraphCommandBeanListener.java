package com.idsdatanet.d2.drillnet.jqtouch;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DVDGraphCommandBeanListener extends EmptyCommandBeanListener {
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		StringBuilder sb = new StringBuilder();
		
		String operationUid = request.getParameter("operationUid");
		if (StringUtils.isNotBlank(operationUid)) {
			root.getDynaAttr().put("operationUid", StringUtils.replaceChars(operationUid, '.', '_'));
			
			Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
			root.getDynaAttr().put("operationName", operation.getOperationName());
			
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
			
			QueryProperties queryProperties = new QueryProperties();
			queryProperties.setUomConversionEnabled(false);
			List dailyLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and reportType=:reportType order by reportDatetime", new String[] {"operationUid", "reportType"}, new Object[] {operationUid, reportType}, queryProperties);
			
			if (dailyLists != null && dailyLists.size() > 0) {
				Double global_duration = 0.0;
				
				UserSession userSession = UserSession.getInstance(request, true);
				Locale locale = null;
				if (userSession != null && userSession.getUserLocale() != null) {
					locale = userSession.getUserLocale();
				} else {
					locale = request.getLocale();
				}
				CustomFieldUom thisConverter = new CustomFieldUom(locale, Activity.class, "depthMdMsl");
				root.getDynaAttr().put("depthUomSymbol", thisConverter.getUomSymbol());
				
				
				Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
				
				if (StringUtils.isBlank(wellbore.getParentWellboreUid())) {
					sb.append("[0.0, 0.0]");
				} else {
					Double kickOffMd = 0.0;
					if (wellbore.getKickoffMdMsl() != null) thisConverter.setBaseValue(wellbore.getKickoffMdMsl());
					kickOffMd = thisConverter.getConvertedValue();
					sb.append("[0.0, " + kickOffMd + "]");
				}
				
				for (Iterator i = dailyLists.iterator(); i.hasNext();) {
					ReportDaily reportDaily = (ReportDaily) i.next();
					
					String activityQuery = "from Activity where (isDeleted = false or isDeleted is null) and dailyUid=:dailyUid and operationUid = :operationUid AND (dayPlus=0 or dayPlus is null) AND (isOffline=0 or isOffline is null) AND (isSimop=0 or isSimop is null) order by endDatetime";
					String[] paramNames = {"dailyUid", "operationUid"};
					String[] paramValues = {reportDaily.getDailyUid(), operationUid};
					List activityLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(activityQuery, paramNames, paramValues, queryProperties);

					if (activityLists != null && activityLists.size() > 0) {
						for (Iterator it = activityLists.iterator(); it.hasNext();) {
							Activity activity = (Activity)it.next();
							Double duration = activity.getActivityDuration();
							//Double dayDepth 	= activity.getDepthMdMsl();
							Double dayDepth = null;
							
							if (activity.getDepthMdMsl() !=null) {
								thisConverter.setBaseValue(activity.getDepthMdMsl());
								dayDepth = thisConverter.getConvertedValue();
							}
							
							if(duration != null && dayDepth != null) {
								global_duration += (duration / 86400);
								sb.append(", [" + global_duration + ", " + dayDepth + "]");
							}
							
						}
					}
				}
			}
		}
		root.getDynaAttr().put("series", sb.toString());
	}

}
