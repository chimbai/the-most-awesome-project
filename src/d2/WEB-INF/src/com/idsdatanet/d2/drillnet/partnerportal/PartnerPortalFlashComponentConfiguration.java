package com.idsdatanet.d2.drillnet.partnerportal;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class PartnerPortalFlashComponentConfiguration extends AbstractFlashComponentConfiguration{

	public String getFlashComponentId(String targetSubModule) {
		return "d2_partner_portal";
	}

	public String getFlashvars(UserSession userSession,
			HttpServletRequest request, String sessionId, String targetSubModule)
			throws Exception {
		String baseUrl = "../../";
		
		return "d2Url=" + urlEncode(baseUrl);
	}

}
