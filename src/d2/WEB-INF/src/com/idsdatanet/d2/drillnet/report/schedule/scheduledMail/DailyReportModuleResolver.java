package com.idsdatanet.d2.drillnet.report.schedule.scheduledMail;

import java.util.Map;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.report.DDRReportModule;
/**
 * Class use for resolving which Report Module to be use for each operation type/type prefix
 * @author Moses
 *
 */
public class DailyReportModuleResolver extends DefaultBeanSupport{
	
	private Map<String,DDRReportModule> opTypePrefixMapping;
	private Map<String,DDRReportModule> specificOpTypeMapping;
	private DDRReportModule defaultDailyReportModule;
	
	
	public static DailyReportModuleResolver getConfiguredInstance() throws Exception {
		return getSingletonInstance(DailyReportModuleResolver.class);
	}

	public void setSpecificOpTypeMapping(Map<String,DDRReportModule> specificOpTypeMapping) {
		this.specificOpTypeMapping = specificOpTypeMapping;
	}

	public Map<String,DDRReportModule> getSpecificOpTypeMapping() {
		return specificOpTypeMapping;
	}

	public void setOpTypePrefixMapping(Map<String,DDRReportModule> opTypePrefixMapping) {
		this.opTypePrefixMapping = opTypePrefixMapping;
	}

	public Map<String,DDRReportModule> getOpTypePrefixMapping() {
		return opTypePrefixMapping;
	}

	public void setDefaultDailyReportModule(DDRReportModule defaultDailyReportModule) {
		this.defaultDailyReportModule = defaultDailyReportModule;
	}

	public DDRReportModule getDefaultDailyReportModule() {
		return defaultDailyReportModule;
	}
	/**
	 * Method for getting the report module by daily uid
	 * @param dailyUid
	 * @return DDR Report Module
	 * @throws Exception
	 */
	public DDRReportModule getReportModuleByDailyUid(String dailyUid) throws Exception
	{
		String operationCode = this.getOperationTypeByDailyUid(dailyUid);
		return this.getReportModuleByOperationType(operationCode);
	}
	
	/**
	 * Method for getting the report module by operation type
	 * @param operation code
	 * @return DDR Report Module
	 * @throws Exception
	 */
	public DDRReportModule getReportModuleByOperationType(String operationCode) throws Exception
	{
		if (this.specificOpTypeMapping!=null && this.specificOpTypeMapping.containsKey(operationCode))
			return this.specificOpTypeMapping.get(operationCode);
		
		String prefixOpCode = CommonUtils.parseForPrefixOperationType(operationCode);
		if (this.opTypePrefixMapping!=null && this.opTypePrefixMapping.containsKey(prefixOpCode))
			return this.opTypePrefixMapping.get(prefixOpCode);
		
		return this.defaultDailyReportModule;
	}
	
	/**
	 * get operation type by daily uid
	 * @param dailyUid
	 * @return String with operation code of the daily uid
	 * @throws Exception
	 */
	public static String getOperationTypeByDailyUid(String dailyUid) throws Exception
	{
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		if (daily !=null)
		{
			Operation op = ApplicationUtils.getConfiguredInstance().getCachedOperation(daily.getOperationUid());
			if (op!=null)
				return op.getOperationCode();
		}	
		return null;
	}
}
