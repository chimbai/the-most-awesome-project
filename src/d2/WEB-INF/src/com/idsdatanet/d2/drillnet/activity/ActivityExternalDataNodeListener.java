package com.idsdatanet.d2.drillnet.activity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.ActivityLessonTicketLink;
import com.idsdatanet.d2.core.model.ActivityExt;
import com.idsdatanet.d2.core.model.ActivityUnwantedEventLink;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.keyPerformanceIndicator.KeyPerformanceIndicatorUtils;
import com.idsdatanet.d2.drillnet.reportDaily.ReportDailyUtils;
import com.idsdatanet.d2.safenet.unwantedEvent.UnwantedEventFlexUtils;

public class ActivityExternalDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	
	// these values are defined in lookup.xml
	private final static String E_FAIL = "equipment failure";
	private final static String KICK = "kick";
	private String classCode;
	private String companyName;
	//private Boolean useActivityDate;
	private Boolean saveAllUserCode = true;
	private Boolean saveNPTCategoriesToRootCauseCode = false;
	private boolean autoPopulateLastHoleSize = false;
	private static final String DYN_ATTR_ACTIVITY_DATE = "activityDate";
	private Boolean displayCompanyRCMessageOnSystemMessage = false;
	private List<String> taskCodeListForDataEntrySystemMessage = null;
	private List<String> additionalInternalClassCodeForNptEvent = null;
	
	public void setClassCode(String value){
		this.classCode = value;
	}

	public void setCompanyName(String value){
		this.companyName = value;
	}
	
	public void setAutoPopulateLastHoleSize(boolean value){
		this.autoPopulateLastHoleSize = value;
	}
	
	/*public void setUseActivityDate(Boolean value){
		this.useActivityDate = value;
	}*/
	
	public void setSaveAllUserCode(Boolean value) {
		this.saveAllUserCode = value;
	}
	
	public void setAdditionalInternalClassCodeForNptEvent(
			List<String> additionalInternalClassCodeForNptEvent) {
		this.additionalInternalClassCodeForNptEvent = additionalInternalClassCodeForNptEvent;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		Calendar activityDate = Calendar.getInstance();
		
		if(object instanceof ActivityExt) {
//			ActivityUtils.populateBharunNumber(node, new UserSelectionSnapshot(session));
			
			String activityRsdUid = (String) PropertyUtils.getProperty(object, "activityExtUid");
			if ("".equals(activityRsdUid)) {
				PropertyUtils.setProperty(object, "activityExtUid", null);
			}

			Date thisActivityDate = null;
			if(node.getDynaAttr().get("activityDate") != null)
			{
				if(CommonDateParser.parse(node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE).toString()) != null)
				{
					thisActivityDate = CommonDateParser.parse(node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE).toString());
				}
				else
				{
					thisActivityDate = (Date) node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE);
				}
			}else{
				
				if (session.getCurrentDaily()!=null)
					thisActivityDate = session.getCurrentDaily().getDayDate();
					
			}
			
			//GET ACTIVITY DATE TO ASSIGN DAILYUID
			if(thisActivityDate != null && (commandBean.getOperatingMode() != BaseCommandBean.OPERATING_MODE_DEPOT))
			{
				activityDate.setTime(thisActivityDate);
				
				String thisOperationUid = PropertyUtils.getProperty(object, "operationUid").toString();
				Daily thisDaily = ApplicationUtils.getConfiguredInstance().getDailyByOperationAndDate(thisOperationUid, activityDate.getTime());
				if(thisDaily != null)
				{
					PropertyUtils.setProperty(object, "dailyUid", thisDaily.getDailyUid());
				}
				else
				{
					//CREATE DAY
					//Daily newDaily = new Daily();
					//newDaily.setOperationUid(thisOperationUid);
					//newDaily.setDayDate(activityDate);
					//newDaily.setDayNumber("TMP");	//TEMPORARY DAY NUMBER
					//ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newDaily);
										
					Daily newDaily = CommonUtil.getConfiguredInstance().createNewDaily(activityDate.getTime(), thisOperationUid, session, request);
					PropertyUtils.setProperty(object, "dailyUid", newDaily.getDailyUid());
					
					//CREATE REPORT_DAILY
					ReportDaily newReportDaily = new ReportDaily();
					newReportDaily.setOperationUid(thisOperationUid);
					newReportDaily.setDailyUid(newDaily.getDailyUid().toString());
					newReportDaily.setReportDatetime(activityDate.getTime());
					newReportDaily.setReportType(CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(thisOperationUid));
					newReportDaily.setReportNumber("-");
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newReportDaily);
				}
				

				//STAMP STARTDATETIME & ENDDATETIME WITH CURRENT ACTIVITY DATE
				if(PropertyUtils.getProperty(object, "startDatetime") != null)
				{
					Calendar startDatetime = Calendar.getInstance();
					startDatetime.setTime((Date) PropertyUtils.getProperty(object, "startDatetime"));
					startDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
					startDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
					startDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
					PropertyUtils.setProperty(object, "startDatetime", startDatetime.getTime());
				}
													
				Calendar endDatetime = Calendar.getInstance();
				endDatetime.setTime((Date) PropertyUtils.getProperty(object, "endDatetime"));
				endDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
				endDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
				endDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
				PropertyUtils.setProperty(object, "endDatetime", endDatetime.getTime());
				
				D2ApplicationEvent.refresh();
				
			}
			
			
			List<Date> endDatetimeList = new ArrayList<Date>();
			
			// get parent node and loop through child to get each end datetime
			CommandBeanTreeNode parentNode = node.getParent();
			for (Iterator i = parentNode.getList().values().iterator(); i.hasNext();) {
				Map items = (Map) i.next();
				
				for (Iterator j = items.values().iterator(); j.hasNext();) {
					CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
					if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(object.getClass())) {
						Boolean isDeleted = (Boolean) PropertyUtils.getProperty(childNode.getData(), "isDeleted"); // shouldn't need this, temporary fix for Flex client
						if (isDeleted == null || !isDeleted.booleanValue()) {
							endDatetimeList.add((Date) PropertyUtils.getProperty(childNode.getData(), "endDatetime"));
						}
					}
				}
			}
			Collections.sort(endDatetimeList, new ActivityUtils.ReversedDateComparator());
			commandBean.getRoot().getDynaAttr().put("endDatetime", endDatetimeList);
			
			Date start = (Date) PropertyUtils.getProperty(object, "startDatetime");
			Date end = (Date) PropertyUtils.getProperty(object, "endDatetime");
			
			// if user didn't enter start time, find the first smaller than the current end time.
			if(start == null) {
				DateFormat df = new SimpleDateFormat("kkmmss");
				
				boolean found = false;
				for(Date endDatetime : endDatetimeList) {
					Calendar currentEndDatetime = Calendar.getInstance();
					currentEndDatetime.setTime(end);
					
					Calendar cachedEndDatetime = Calendar.getInstance();
					cachedEndDatetime.setTime(endDatetime);
					
					if(df.format(cachedEndDatetime.getTime()).compareTo(df.format(currentEndDatetime.getTime()))==-1) {
						
						if(thisActivityDate!=null){
							activityDate.setTime(thisActivityDate);
						}
						cachedEndDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
						cachedEndDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
						cachedEndDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
						
												
						PropertyUtils.setProperty(object, "startDatetime", cachedEndDatetime.getTime());
						found = true;
						break;
					}
				}
				// if no matches, set to 00:00
				if(!found) {
					Calendar zero = Calendar.getInstance();
					zero.clear();
					zero.set(Calendar.HOUR_OF_DAY, 0);
					zero.set(Calendar.MINUTE, 0);
					zero.set(Calendar.SECOND, 0);
					zero.set(Calendar.MILLISECOND, 0);
					
					
					if(thisActivityDate!=null){
						activityDate.setTime(thisActivityDate);
					}
					zero.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
					zero.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
					zero.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
					
										
					PropertyUtils.setProperty(object, "startDatetime", zero.getTime());
				}
				start = (Date) PropertyUtils.getProperty(object, "startDatetime");
			}
			
			// check if start time greater than end time.
			if(start != null && end != null){
				if(start.getTime() > end.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "startDatetime", "Start time can not be greater than end time.");
					return;
				}
				
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
				
				/*
				long endTime = end.getTime();
				if (endTime == 86399000) { // if it is 23:59:59
					endTime = 86400000; // make it 24:00:00 just for duration calculation
				}
				Long duration = (endTime - start.getTime()) / 1000;
				*/
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityExt.class, "activityDuration");
				
				thisConverter.setBaseValue(thisDuration);
				
				PropertyUtils.setProperty(object, "activityDuration", (thisConverter.getConvertedValue()));
				
				/** PORT FROM TNP: Tour Stamping Feature **/
				String rigTourUid = CommonUtil.getConfiguredInstance().tourAssignmentBasedOnTime(session, start, false);
				if (StringUtils.isNotBlank(rigTourUid)) {
					PropertyUtils.setProperty(object, "rigTourUid", rigTourUid);
				}
				/** PORT FROM TNP: Tour Stamping Feature **/
				
			}
			
			//fix case where if first the record is select trouble, then user put RC, then if change to program, RC is hide then when save RC value still in data
			//clear RC record and company record if user select program or unprogram
//			String classCode = (String) PropertyUtils.getProperty(object, "classCode");
//			String internalCode = this.getInternalCode(classCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
//			PropertyUtils.setProperty(object, "internalClassCode", internalCode);
//			if (StringUtils.equalsIgnoreCase("P", internalCode) || StringUtils.equalsIgnoreCase("U", internalCode))
//			{
//				PropertyUtils.setProperty(object, "rootCauseCode", null);
//				PropertyUtils.setProperty(object, "equipmentCode", null);
//				PropertyUtils.setProperty(object, "subEquipmentCode", null);
//				PropertyUtils.setProperty(object, "lookupCompanyUid", null);
//				PropertyUtils.setProperty(object, "incidentNumber", null);
//				PropertyUtils.setProperty(object, "incidentLevel", null);
//				
//			}
			
//			if(StringUtils.equalsIgnoreCase("P", internalCode)|| StringUtils.equalsIgnoreCase("TU", internalCode)|| StringUtils.equalsIgnoreCase("TP", internalCode))
//			{
//				if (!this.saveAllUserCode) PropertyUtils.setProperty(object, "userCode", null);
//			}
//			
//			
//			if ("TP".equalsIgnoreCase(internalCode) || "TU".equalsIgnoreCase(internalCode) || 
//				(this.additionalInternalClassCodeForNptEvent!=null && this.additionalInternalClassCodeForNptEvent.contains(internalCode))) {
//				//insert NPT Event record to unwantedEvent table
//				String eventRef = (String) node.getDynaAttr().get("nptEventRef");
//				if (StringUtils.isNotBlank(eventRef)) {
//					List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from UnwantedEvent where (isDeleted = false or isDeleted is null) and type='nptEvent' and eventRef=:eventRef and operationUid=:operationUid", new String[]{"eventRef","operationUid"}, new Object[] {eventRef, session.getCurrentOperationUid()});
//					if (list.size()>0) {
//						UnwantedEvent ue = (UnwantedEvent) list.get(0);
//						PropertyUtils.setProperty(object, "nptEventUid", ue.getUnwantedEventUid());
//					} else {
//						//create new unwanted event if not found
//						UnwantedEvent ue = new UnwantedEvent();
//						ue.setEventRef(eventRef);
//						ue.setNameOfContractor((String) PropertyUtils.getProperty(object, "lookupCompanyUid"));
//						ue.setEventCause((String) PropertyUtils.getProperty(object, "rootCauseCode"));
//						ue.setType("nptEvent");
//						ue.setEventSummary("");
//						ue.setEventTitle("");
//						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ue);
//						PropertyUtils.setProperty(object, "nptEventUid", ue.getUnwantedEventUid());
//					}
//				}
//			} else {
//				PropertyUtils.setProperty(object, "nptEventUid", null);
//			}
			
			
//			if((StringUtils.isNotBlank((String) PropertyUtils.getProperty(object, "phaseCode")))){
//				String phaseCode = (String) PropertyUtils.getProperty(object, "phaseCode");
//				String regulatoryCode = this.getMPMCodePhase(phaseCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
//				PropertyUtils.setProperty(object, "regulatoryPhaseCode", regulatoryCode);
//				List<String> mpmCodes = null;
//				if(("P".equalsIgnoreCase(internalCode) || "U".equalsIgnoreCase(internalCode))){
//					if((StringUtils.isNotBlank((String) PropertyUtils.getProperty(object, "taskCode")))){
//						String taskCode = (String) PropertyUtils.getProperty(object, "taskCode");
//						mpmCodes = this.getMPMCodeTaskRoot(phaseCode,taskCode, session.getCurrentGroupUid(), session.getCurrentOperationType(), "task");
//						if(mpmCodes.size() > 0){
//							PropertyUtils.setProperty(object, "regulatoryCode", mpmCodes.get(0));
//							PropertyUtils.setProperty(object, "regulatorySubcode", mpmCodes.get(1));
//						}else{
//							PropertyUtils.setProperty(object, "regulatoryCode", null);
//							PropertyUtils.setProperty(object, "regulatorySubcode", null);
//						}
//					}
//				}else if (("TP".equalsIgnoreCase(internalCode) || "TU".equalsIgnoreCase(internalCode))){
//					if((StringUtils.isNotBlank((String) PropertyUtils.getProperty(object, "rootCauseCode")))){
//						String rootCauseCode = (String) PropertyUtils.getProperty(object, "rootCauseCode");
//						mpmCodes = this.getMPMCodeTaskRoot(phaseCode,rootCauseCode, session.getCurrentGroupUid(), session.getCurrentOperationType(), "root");
//						if(mpmCodes.size() > 0){
//							PropertyUtils.setProperty(object, "regulatoryCode", mpmCodes.get(0));
//							PropertyUtils.setProperty(object, "regulatorySubcode", mpmCodes.get(1));
//						}else{
//							PropertyUtils.setProperty(object, "regulatoryCode", null);
//							PropertyUtils.setProperty(object, "regulatorySubcode", null);
//						}
//					}
//				}
//			}else{
//				PropertyUtils.setProperty(object, "regulatoryPhaseCode", "");
//				PropertyUtils.setProperty(object, "regulatoryCode", "");
//				PropertyUtils.setProperty(object, "regulatorySubcode", "");
//			}
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if(! node.getInfo().isTemplateNode() && (object instanceof ActivityExt)) {
			
			// calculate activity duration.
			//This is for HTML version only when fully flex can remove this sewction
			/* Double activityDuration = (Double) PropertyUtils.getProperty(object, "activityDuration");
			
			if(activityDuration != null) {
				Double duration = null;
				// next day's activity
				Integer dayPlus = (Integer) PropertyUtils.getProperty(object, "dayPlus");
				if(dayPlus != null && dayPlus == 1) {
					duration = (Double) commandBean.getRoot().getDynaAttr().get("nextDayActivityDuration");
					
					if(duration == null) {
						commandBean.getRoot().getDynaAttr().put("nextDayActivityDuration", activityDuration);
						
					} else {
						duration += activityDuration;
						commandBean.getRoot().getDynaAttr().put("nextDayActivityDuration", duration);
					}
				} else {
					
					duration = (Double) commandBean.getRoot().getDynaAttr().get("activityDuration");
					if(duration == null) {
						commandBean.getRoot().getDynaAttr().put("activityDuration", activityDuration);
					} else {
						duration += activityDuration;
						commandBean.getRoot().getDynaAttr().put("activityDuration", duration);
					}
				}
			}*/
			
			String strInputMode  = (String) commandBean.getRoot().getDynaAttr().get("inputMode");
			if(StringUtils.isBlank(strInputMode))
			{
				commandBean.getRoot().getDynaAttr().put("inputMode", "all");
			}
			
			if("all".equalsIgnoreCase(commandBean.getRoot().getDynaAttr().get("inputMode").toString()))
			{
				node.getDynaAttr().put("editable", true);
			}
			else
			{
				node.getDynaAttr().put("editable", false);
			}
								
			//GET ACTIVITY DAY DATE FROM DAILYUID
			/*if(useActivityDate != null && useActivityDate == true)
			{
				String strDailyUid = PropertyUtils.getProperty(object, "dailyUid").toString();
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(strDailyUid);
				if(daily != null)
				{
					Date activityDate = daily.getDayDate();
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime(activityDate);
														
					node.getDynaAttr().put("activityDate", thisCalendar.getTime());
				}
			}*/
			this.populateDynAttrActivityDate(node, userSelection, true);
			
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		String newActivityRecordInputMode = null;
		if (request != null) {
			newActivityRecordInputMode = request.getParameter("newActivityRecordInputMode");
		}
		
		if (obj instanceof ActivityExt) {
			if (meta.getTableClass() == ActivityExt.class && "1".equals(newActivityRecordInputMode)) {
				commandBean.getRoot().addCustomNewChildNodeForInput(new ActivityExt());
			}
		}
		
		afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	
	/**
	 * Carry forward last activity codes
	 */
	/*
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception	{
		try {
			Object thisObject = node.getData();
			if (thisObject instanceof Activity) {
				CommandBeanTreeNodeImpl lastChildNode = null;
				
				// get the last activity node
				for (Map<String, CommandBeanTreeNode> map: node.getParent().getList().values()) {
					for (Iterator i = map.values().iterator(); i.hasNext(); ) {
						CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) i.next();
						if (!childNode.isTemplateNode() && childNode.getData() instanceof Activity) {
							lastChildNode = childNode;
						}
					}
				}
				if (lastChildNode != null) {
					Object lastObject = lastChildNode.getData();
					if (lastObject != null) {
						Activity lastActivity = (Activity) lastObject;
						Activity thisActivity = (Activity) thisObject;
						thisActivity.setClassCode(lastActivity.getClassCode());
						thisActivity.setPhaseCode(lastActivity.getPhaseCode());
						thisActivity.setTaskCode(lastActivity.getTaskCode());
						thisActivity.setRootCauseCode(lastActivity.getRootCauseCode());
						
						// dynamic fields
						node.getDynaField().getValues().put("mainmode", lastChildNode.getDynaField().getValues().get("mainmode"));
						node.getDynaField().getValues().put("submode1", lastChildNode.getDynaField().getValues().get("submode1"));
						node.getDynaField().getValues().put("submode2", lastChildNode.getDynaField().getValues().get("submode2"));
						node.getDynaField().getValues().put("submode3", lastChildNode.getDynaField().getValues().get("submode3"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
//	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
//		UnwantedEventFlexUtils ueUtils = new UnwantedEventFlexUtils();
//		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoRemoveUnwantedEventFromActivity"))) {
//			if (ueUtils.containsOldEvents(session)) { //check old unwanted events
//				if (ueUtils.unwantedEventCleanup(session)) { //do unwanted event clean up
//					commandBean.getSystemMessage().addInfo("Unwanted events have been cleaned up for the events that have been removed or changed to non-trouble event.", true, true);
//				}
//			}
//		}
//		
//		Object object = node.getData();
//		
//		if(autoPopulateLastHoleSize){
//			QueryProperties qp = new QueryProperties();		
//			qp.setUomConversionEnabled(false);
//			String operationUid = (String) PropertyUtils.getProperty(object,"operationUid");
//			String dailyUid = (String) PropertyUtils.getProperty(object,"dailyUid");
//			String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :selectedDailyUid AND reportType<>'DGR'";
//			String[] paramsFields = {"selectedOperationUid","selectedDailyUid"};
//			Object[] paramsValues = {operationUid,dailyUid};
//			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
//			ReportDaily thisReport = null;
//			String strSql2 = "FROM Activity WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :selectedDailyUid AND dayPlus = 0 AND (holeSize is not null OR holeSize != '') ORDER BY endDatetime DESC";
//			String[] paramsFields2 = {"selectedOperationUid","selectedDailyUid"};
//			Object[] paramsValues2 = {operationUid,dailyUid};
//			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
//			
//			if (!lstResult.isEmpty())
//			{
//				for(int i = 0; i < lstResult.size(); i++){
//					thisReport = (ReportDaily) lstResult.get(i);
//					if (!lstResult2.isEmpty())
//					{
//						Activity lastActivity = (Activity) lstResult2.get(0);
//						thisReport.setLastHolesize(lastActivity.getHoleSize());
//						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
//					}else{
//						thisReport.setLastHolesize(null);
//						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
//					}
//				}
//			}
//		}
//		
//	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		
		if (meta.getTableClass().equals(ActivityExt.class)) {
									
			//BY DEFAULT, LOAD DATA BY OPERATION
			String strSql = "FROM ActivityRsd WHERE operationUid = :operationUid AND groupUid = :groupUid AND (isDeleted IS NULL OR isDeleted = '') ORDER BY startDatetime";
			String[] paramNames =  {"operationUid", "groupUid"};
			Object[] paramValues = {userSelection.getOperationUid(), userSelection.getGroupUid()};
						
			List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			List<Object> output_maps = new ArrayList<Object>();
			
			for(Object objResult: items){
				ActivityExt activity = (ActivityExt) objResult;
				output_maps.add(activity);
			}
						
			return output_maps;
		}
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}
	
	private void populateDynAttrActivityDate(CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Boolean getDailyFromNode) throws Exception
	{
		String dailyUid = userSelection.getDailyUid();
		Date activityDate = null;
		if (getDailyFromNode && PropertyUtils.getProperty(node.getData(), "dailyUid")!=null)
		{
			dailyUid = (String) PropertyUtils.getProperty(node.getData(), "dailyUid");
		}
		if (StringUtils.isNotBlank(dailyUid))
		{
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
			if (daily!=null && daily.getDayDate()!=null)
				activityDate = daily.getDayDate();
		}
		node.getDynaAttr().put(DYN_ATTR_ACTIVITY_DATE, activityDate);
	}
	
	public void setSaveNPTCategoriesToRootCauseCode(
			Boolean saveNPTCategoriesToRootCauseCode) {
		this.saveNPTCategoriesToRootCauseCode = saveNPTCategoriesToRootCauseCode;
	}

	public Boolean getSaveNPTCategoriesToRootCauseCode() {
		return saveNPTCategoriesToRootCauseCode;
	}
	
	private String getInternalCode(String classCode, String groupUid, String operationCode) throws Exception{
		
		String internalCode = "";
		
		String strSql = "SELECT internalCode FROM LookupClassCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisClassCode AND (operationCode = :thisOperationCode OR operationCode = '') AND groupUid = :thisGroupUid";
		String[] paramsFields = {"thisClassCode", "thisGroupUid", "thisOperationCode"};
		Object[] paramsValues = {classCode, groupUid, operationCode};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			Object internalCodeResult = (Object) lstResult.get(0);
			if (internalCodeResult != null) internalCode = internalCodeResult.toString();
		}
		if (StringUtils.isBlank(internalCode)) internalCode = classCode;
		
		return internalCode;
	}
	
	private String getMPMCodePhase(String phaseCode, String groupUid, String operationCode) throws Exception{
		
		String mpmCode = "";
		
		String strSql = "SELECT regulatoryCode FROM LookupPhaseCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisPhaseCode AND (operationCode = :thisOperationCode OR operationCode = '') AND groupUid = :thisGroupUid";
		String[] paramsFields = {"thisPhaseCode", "thisGroupUid", "thisOperationCode"};
		Object[] paramsValues = {phaseCode, groupUid, operationCode};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			Object mpmCodeResult = (Object) lstResult.get(0);
			if (mpmCodeResult != null) mpmCode = mpmCodeResult.toString();
		}
		
		return mpmCode;
	}
	
	private List<String> getMPMCodeTaskRoot(String phaseCode, String shortCode, String groupUid, String operationCode,String codeType) throws Exception{
		List<String> mpmCode = new ArrayList<String>();
		String strSql = null;
		List lstResult = null;
		
		if(StringUtils.equalsIgnoreCase(codeType, "task")){
			strSql = "SELECT regulatoryCode,regulatorySubcode FROM LookupTaskCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisShortCode AND (operationCode = :thisOperationCode OR operationCode = '') AND (phaseShortCode = :thisPhaseShortCode) AND (isActive IS NULL OR isActive = 1 or isActive = '') AND groupUid = :thisGroupUid";
			String[] paramsFields = {"thisPhaseShortCode","thisShortCode", "thisGroupUid", "thisOperationCode"};
			Object[] paramsValues = {phaseCode, shortCode, groupUid, operationCode};
			if(strSql != null){
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			}
		}else if (StringUtils.equalsIgnoreCase(codeType, "root")){
			strSql = "SELECT regulatoryCode,regulatorySubcode FROM LookupRootCauseCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisShortCode AND (operationCode = :thisOperationCode OR operationCode = '') AND (isActive IS NULL OR isActive = 1 or isActive = '') AND groupUid = :thisGroupUid";
			String[] paramsFields = {"thisShortCode", "thisGroupUid", "thisOperationCode"};
			Object[] paramsValues = {shortCode, groupUid, operationCode};
			if(strSql != null){
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			}
		}
		
		if(lstResult != null){
			if (lstResult.size() > 0){
				Object[] mpmCodeResult = (Object[]) lstResult.get(0);
				if (mpmCodeResult != null) {
					if(mpmCodeResult[0] != null){
						mpmCode.add(mpmCodeResult[0].toString());
					}else{
						mpmCode.add("");
					}
					
					if(mpmCodeResult[1] != null){
						mpmCode.add(mpmCodeResult[1].toString());
					}else{
						mpmCode.add("");
					}
				}
			}
		}else{
			mpmCode.add("");
			mpmCode.add("");
		}

		return mpmCode;
	}

	public void setDisplayCompanyRCMessageOnSystemMessage(
			Boolean displayCOmpanyRCMessageOnSystemMessage) {
		this.displayCompanyRCMessageOnSystemMessage = displayCOmpanyRCMessageOnSystemMessage;
	}

	public Boolean getDisplayCompanyRCMessageOnSystemMessage() {
		return displayCompanyRCMessageOnSystemMessage;
	}

	public void setTaskCodeListForDataEntrySystemMessage(
			List<String> taskCodeListForDataEntrySystemMessage) {
		this.taskCodeListForDataEntrySystemMessage = taskCodeListForDataEntrySystemMessage;
	}
	
}