package com.idsdatanet.d2.drillnet.solidControlEquipment;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.SolidControlEquipment;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class SolidControlEquipmentCommandBeanListener extends EmptyCommandBeanListener {

	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);		

		for (Field field : SolidControlEquipment.class.getDeclaredFields()) {
			thisConverter.setReferenceMappingField(SolidControlEquipment.class, field.getName());
			if (thisConverter.isUOMMappingAvailable()) {
				commandBean.setCustomUOM(Shaker.class, field.getName(), thisConverter.getUOMMapping());
				commandBean.setCustomUOM(Centrifuge.class, field.getName(), thisConverter.getUOMMapping());
			}
		}
	
	}

}
