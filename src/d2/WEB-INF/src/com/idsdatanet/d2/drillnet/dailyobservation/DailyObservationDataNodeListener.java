package com.idsdatanet.d2.drillnet.dailyobservation;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DailyObservation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DailyObservationDataNodeListener extends EmptyDataNodeListener{
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		if(node.getData() instanceof DailyObservation) {
			DailyObservation o = (DailyObservation) node.getData();
			if(o.getDailyObservationUid() == null && o.getReportTime() == null && StringUtils.isNotBlank(o.getDailyUid())) {
				Calendar reportTime = Calendar.getInstance();
				reportTime.setTime(new Date());
				Daily day = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, o.getDailyUid());
				
				if(day != null) {
					Calendar reportingDate =  Calendar.getInstance();
					reportingDate.setTime(day.getDayDate());
					reportTime.set(Calendar.YEAR, reportingDate.get(Calendar.YEAR));
					reportTime.set(Calendar.MONTH, reportingDate.get(Calendar.MONTH));
					reportTime.set(Calendar.DAY_OF_MONTH, reportingDate.get(Calendar.DAY_OF_MONTH));
					o.setReportTime(reportTime.getTime());
				}
			}
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		if(DailyObservation.class.equals(meta.getTableClass())) {
			((DailyObservation) node.getData()).setRigInformationUid(userSelection.getRigInformationUid());
		}
	}
}
