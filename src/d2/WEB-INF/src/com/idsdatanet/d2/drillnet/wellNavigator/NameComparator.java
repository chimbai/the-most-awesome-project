package com.idsdatanet.d2.drillnet.wellNavigator;

import java.util.Comparator;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc._Well;

public class NameComparator implements Comparator {
	
	String _listType; 
	
	public NameComparator(String type) {
		_listType = type;
	}
	
	public int compare(Object _i1, Object _i2){
		String v1 = "";
		String v2 = "";
		
		if ("w".equals(_listType)) {
			v1 = WellNameUtil.getPaddedStr(((_Well)_i1).getWellName());
			v2 = WellNameUtil.getPaddedStr(((_Well)_i2).getWellName());
		} else if ("wb".equals(_listType)) {
			v1 = WellNameUtil.getPaddedStr(((Wellbore)_i1).getWellboreName());
			v2 = WellNameUtil.getPaddedStr(((Wellbore)_i2).getWellboreName());			
		} else if ("o".equals(_listType)) {
			v1 = WellNameUtil.getPaddedStr(((Operation)_i1).getOperationName());
			v2 = WellNameUtil.getPaddedStr(((Operation)_i2).getOperationName());			
		}
		
		return v1.compareTo(v2);
	}
}
