package com.idsdatanet.d2.drillnet.rigpump;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.EmptyDataSummary;
import com.idsdatanet.d2.core.web.mvc.Summary;
import com.idsdatanet.d2.core.web.mvc.SummaryInfo;
import com.idsdatanet.d2.core.web.mvc.SummaryUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class RigPumpDataSummaryListener  extends EmptyDataSummary {

private List<Summary> data = null;
	
	public void generate(HttpServletRequest request) throws Exception {
		this.data = new ArrayList<Summary>();
		UserSession session = UserSession.getInstance(request);
		String operation = session.getCurrentOperationUid();
		
		String strSql1 = "SELECT daily.dailyUid FROM RigPumpParam rigPumpParam, Daily daily WHERE (rigPumpParam.isDeleted = false or rigPumpParam.isDeleted is null) AND (daily.isDeleted = false or daily.isDeleted is null) AND daily.dailyUid = rigPumpParam.dailyUid AND daily.operationUid= :thisOperation GROUP BY daily.dayDate, daily.dailyUid ORDER BY daily.dayDate, daily.dailyUid";
		String[] paramsFields1 = {"thisOperation"};
		String[] paramsValues1 = {operation};
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);

		if (lstResult.size() > 0) {
			List<String> dailyUidList = new ArrayList();
			Summary summary = new Summary("Day # : ");
			for (Object objPump : lstResult) {
				String dailyUid = (String) objPump; // beware of the array index!!! make sure the first element in the query has to be dailyUid, unless changed
				dailyUidList.add(dailyUid);
			}
			
			Map<String, String> summaryMap = SummaryUtils.getConfiguredInstance().getDailyUidAndReportNumberWithReportTypeAware(dailyUidList, session, null);
			
			if (summaryMap != null && summaryMap.size() > 0) {
				for (Map.Entry<String, String> entry : summaryMap.entrySet()) {
					String dailyUid = entry.getKey();
					String reportNumber = entry.getValue();
					summary.addInfo(new SummaryInfo(reportNumber, dailyUid));
				}
				this.data.add(summary);
			}
		}
	}
	
	public List<Summary> getData() {
		return this.data;
	}
	
	public boolean isDefault() {
		return true;
	}
}
