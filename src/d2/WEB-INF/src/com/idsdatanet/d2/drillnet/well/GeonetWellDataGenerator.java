package com.idsdatanet.d2.drillnet.well;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.WeatherEnvironment;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class GeonetWellDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String thisReportType = null;
		
		String thisWellboreUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getWellboreUid().toString())) thisWellboreUid = userContext.getUserSelection().getWellboreUid().toString();		
		
		String thisOperationUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getOperationUid().toString())) thisOperationUid = userContext.getUserSelection().getOperationUid().toString();
			
		if (thisReportType==null) thisReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(thisOperationUid);

		String LastDailyUid = CommonUtil.getConfiguredInstance().getLastDailyUidInOperationByReportType(thisOperationUid,thisReportType);

		String strSql = "FROM ReportDaily WHERE (isDeleted is null or isDeleted = false) and operationUid =:thisOperationUid and dailyUid =:thisDailyUid";
		String[] paramsFields = {"thisOperationUid", "thisDailyUid"};
		Object[] paramsValues = {thisOperationUid, LastDailyUid};
		
		ReportDataNode thisReportNode = reportDataNode.addChild("ReportDaily");
		List<ReportDaily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "depthMdMsl");
		
		if (lstResult.size() > 0) {
			ReportDaily thisReportDaily = (ReportDaily) lstResult.get(0);
			
			if (thisReportDaily.getDepthMdMsl()!=null)
			{
				thisConverter.setBaseValue(thisReportDaily.getDepthMdMsl());
			//	Double depthMdMsl = thisConverter.getConvertedValue();
				thisReportNode.addProperty("depthMdMsl", thisConverter.formatOutputPrecision());
				if (thisConverter.isUOMMappingAvailable()){
					thisReportNode.addProperty("depthMdMslUom", thisConverter.getUomSymbol());
				}
			}
			
			if (thisReportDaily.getDepthTvdMsl()!=null)
			{
				thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "depthTvdMsl");
				thisConverter.setBaseValue(thisReportDaily.getDepthTvdMsl());
			//	Double depthTvdMsl = thisConverter.getConvertedValue();
				thisReportNode.addProperty("depthTvdMsl", thisConverter.formatOutputPrecision());
				if (thisConverter.isUOMMappingAvailable()){
					thisReportNode.addProperty("depthTvdMslUom", thisConverter.getUomSymbol());
				}
				thisConverter.removeDatumOffset();
			//	Double DepthTVDSS = thisConverter.getConvertedValue();
				thisReportNode.addProperty("depthTvdSS", thisConverter.formatOutputPrecision());
			}
		}
		thisReportNode = reportDataNode.addChild("Survey");
		thisReportNode.addProperty("maxAngleDepth", CommonUtil.getConfiguredInstance().getMaxDeviationDepth(userContext.getUserSelection()));
		
		thisReportNode = reportDataNode.addChild("WeatherEnvironment");
		//get latest seabedTemperatureAvg and seaTemperatureAvg for the current operation
		List<Object[]> lstResultW = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select w.seabedTemperatureAvg, w.seaTemperatureAvg FROM WeatherEnvironment w, Daily d WHERE (w.isDeleted is null or w.isDeleted = false) and (d.isDeleted is null or d.isDeleted = false) and w.operationUid =:thisOperationUid and w.dailyUid = d.dailyUid order by d.dayDate DESC, w.reportDatetime DESC", "thisOperationUid", thisOperationUid, qp);
		if (lstResultW.size() > 0) {
			Object[] we = lstResultW.get(0);
			
			if (we !=null){
				if (we[0] !=null){
					thisConverter.setReferenceMappingField(WeatherEnvironment.class, "seabedTemperatureAvg");
					thisConverter.setBaseValue(Double.parseDouble(we[0].toString()));
					Double seabedTemp = thisConverter.getConvertedValue();
					thisReportNode.addProperty("seabedTemperatureAvg", seabedTemp.toString());
					if (thisConverter.isUOMMappingAvailable()){
						thisReportNode.addProperty("seabedTemperatureAvgUom", thisConverter.getUomSymbol());
					}
				}
				
				if (we[1] !=null){
					thisConverter.setReferenceMappingField(WeatherEnvironment.class, "seaTemperatureAvg");
					thisConverter.setBaseValue(Double.parseDouble(we[1].toString()));	
					Double seaTemp = thisConverter.getConvertedValue();
					thisReportNode.addProperty("seaTemperatureAvg", seaTemp.toString());
					if (thisConverter.isUOMMappingAvailable()){
						thisReportNode.addProperty("seaTemperatureAvgUom", thisConverter.getUomSymbol());
					}
				}
			}
		}

	}
}
