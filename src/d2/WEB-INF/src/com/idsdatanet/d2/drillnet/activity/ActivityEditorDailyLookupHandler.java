package com.idsdatanet.d2.drillnet.activity;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ActivityEditorDailyLookupHandler implements LookupHandler{
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		UserSession session = UserSession.getInstance(request);
		String operationUid = session.getCurrentOperationUid();
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		List<ReportDaily> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportDaily WHERE (isDeleted=false or isDeleted is null) AND operationUid=:operationUid and reportType='DDR' order by reportDatetime", "operationUid", operationUid);
		for (ReportDaily rec : list) {
			String key = rec.getDailyUid();
			String value = rec.getReportNumber() + " (" + df.format(rec.getReportDatetime()) + ")";
			LookupItem item = new LookupItem(key, value);
			result.put(key, item);
		}
		return result;
	}
}
