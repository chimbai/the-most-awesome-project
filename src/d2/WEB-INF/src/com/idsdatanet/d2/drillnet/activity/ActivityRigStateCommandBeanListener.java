package com.idsdatanet.d2.drillnet.activity;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.CascadeLookupMeta;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.ActivityRsd;
import com.idsdatanet.d2.core.model.LookupClassCode;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RigStateRaw;
import com.idsdatanet.d2.core.model.RigTour;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.core.util.DepotUtils;
import com.idsdatanet.depot.witsml.rts.RTSAdapterListener;
import com.idsdatanet.depot.witsml.rts.RigStateData;


public class ActivityRigStateCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {

	private Boolean ignore24HourChecking = false;
	private String classCode = "TB";
	private Boolean clearEmptyDaily = false;
	private Boolean displayCompanyRCMessageOnSystemMessage = false;
	private Boolean showSystemMessageForActivity = true;
	protected RTSAdapterListener listeners = null;
	
	public void setListeners(RTSAdapterListener listeners) {
		this.listeners = listeners;
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		UserSession session = UserSession.getInstance(request);	
		this.recalculateTotalDuration(commandBean);
		
		// empty method to be extended for client specific usage
		this.clientSpecificProcessAfterProcessFormSubmission(commandBean, request);

	}
	
	protected void clientSpecificProcessAfterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
		// do nothing
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		double totalActivityDuration = this.recalculateTotalDuration(commandBean);
		String operationHour = "24";
		if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "act_show_warning_if_not_24")))
		{	
			String newActivityRecordInputMode = null;
			if (request != null) {
				newActivityRecordInputMode = request.getParameter("newActivityRecordInputMode");
				UserSession session = UserSession.getInstance(request);
				if (session.getCurrentOperation()!=null) {
					operationHour = ActivityUtils.getOperationHour(session.getCurrentOperation());
				}
			}
			
			if (!("1".equals(newActivityRecordInputMode))){
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityRsd.class, "activityDuration");
				thisConverter.setBaseValue(86400);
				if (StringUtils.isNotBlank(operationHour)) {
					Double totalHoursInSecond = Double.parseDouble(operationHour);
					if (!totalHoursInSecond.isNaN()) {
						totalHoursInSecond = totalHoursInSecond * 3600.0;
						thisConverter.setBaseValue(totalHoursInSecond);
					}
				}
				
				if (!thisConverter.getFormattedValue(totalActivityDuration).equals(thisConverter.getFormattedValue()) && !this.ignore24HourChecking){
					commandBean.getSystemMessage().addInfo("Total hours for activities in the day (" + thisConverter.getFormattedValue(totalActivityDuration) + ") is not " + operationHour + " hours.");
				}
			}
			
			if (isTimeOverlapped(commandBean)) {
				commandBean.getSystemMessage().addInfo("There is time overlapping / gaps between activities.");
			}
		}

		//SET DEFAULT INPUT MODE TO "ALL" IF NOT SELECTED
		String strInputMode  = (String) commandBean.getRoot().getDynaAttr().get("inputMode");
		if(StringUtils.isBlank(strInputMode))
		{
			commandBean.getRoot().getDynaAttr().put("inputMode", "all");
		}
		
		UserSession session = UserSession.getInstance(request);
		Operation currentOperation = session.getCurrentOperation();
		if (StringUtils.isNotBlank(currentOperation.getRigInformationUid())) {
			RigInformation rig = (RigInformation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, currentOperation.getRigInformationUid());
			List<RigTour> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from RigTour where rigInformationUid=:rigInformationUid and (isDeleted is null or isDeleted=0)", "rigInformationUid", currentOperation.getRigInformationUid());
			if (result == null || result.size() == 0) {
				commandBean.getSystemMessage().addError("There is no Rig Tour assigned to current Rig: " + rig.getRigName());
			}
		} else {
			commandBean.getSystemMessage().addError("Current operation has no rig assigned.");
		}
		List dwoms = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from DepotWellOperationMapping where wellUid=:wellUid and wellboreUid=:wellboreUid and (rigStateDefinitionUid IS NOT NULL AND rigStateDefinitionUid != '') and (isDeleted is null or isDeleted=0)", new String[] {"wellUid", "wellboreUid"}, new String[] {session.getCurrentWellUid(), session.getCurrentWellboreUid()});
		if (dwoms != null && dwoms.size() > 1) {
			commandBean.getSystemMessage().addError("There are more than one DepotWellOperationMapping being used in the current Wellbore. Please make sure there is only one mapping per wellbore.");
		}
	}
	
	private double recalculateTotalDuration(CommandBean commandBean) throws Exception {
		double totalActivityDuration = 0;
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityRsd.class, "activityDuration");
		
		for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ActivityRsd").values()) {
			ActivityRsd activity = (ActivityRsd) node.getData();
			
			if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {
				totalActivityDuration += activity.getActivityDuration();
			}
		}
		if (thisConverter.isUOMMappingAvailable()){
			commandBean.getRoot().setCustomUOM("@activityDuration", thisConverter.getUOMMapping());
		}
		commandBean.getRoot().getDynaAttr().put("activityDuration", totalActivityDuration);
		
		return totalActivityDuration;
	}
	
	private Boolean isTimeOverlapped(CommandBean commandBean) throws Exception {
		Date activityFirstStartDateTime = null;
		Date activityLastStartDateTime = null;
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityRsd.class, "activityDuration");
		Double activityDuration = 0.0;
		Double activityActualDuration = 0.0;
		for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ActivityRsd").values()) {
			ActivityRsd activity = (ActivityRsd) node.getData();
			if (activity.getIsDeleted() == null || !activity.getIsDeleted()) { 
				if (activity.getStartDatetime() != null && activity.getEndDatetime() != null) {						
					if (activityFirstStartDateTime == null) activityFirstStartDateTime = activity.getStartDatetime();	
					activityLastStartDateTime = activity.getEndDatetime();
					if (activity != null && activity.getActivityDuration() != null) {
						activityDuration += activity.getActivityDuration();
					}
				}
			}
		}
		
		thisConverter.setBaseValueFromUserValue(activityDuration);
		activityDuration=thisConverter.getBasevalue();
		int decimalPlaces = 1;
		BigDecimal bd = new BigDecimal(activityDuration);
		bd = bd.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
		activityDuration = bd.doubleValue();
		
		if (activityFirstStartDateTime != null && activityLastStartDateTime !=null) {	
			activityActualDuration = ActivityUtils.getActivityDurationInSecond(activityFirstStartDateTime, activityLastStartDateTime, thisConverter);
			if (activityActualDuration.compareTo(activityDuration) != 0) {
				return true;
			}
		}
		
		return false;
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		String strInputMode  = (String) commandBean.getRoot().getDynaAttr().get("inputMode");
		String autoPopulateDescription  = (String) request.getParameter("auto_populate_description");
		Boolean SysPass = true;
		if(StringUtils.isNotBlank(autoPopulateDescription)){	
			SysPass = false;
		}
		if ("classCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
		}
		if ("phaseCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
		}
		if ("taskCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
		}
		if ("userCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			SysPass = false;
		}
		if(StringUtils.isBlank(strInputMode))
		{
			commandBean.getRoot().getDynaAttr().put("inputMode", "all");
		}
		if(SysPass){		
			if("all".equalsIgnoreCase(commandBean.getRoot().getDynaAttr().get("inputMode").toString()))
			{
				commandBean.setSupportedAction("add", true);
				commandBean.setSupportedAction("copySelected", true);
				commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			}
			else
			{
				commandBean.setSupportedAction("add", false);
				commandBean.setSupportedAction("copySelected", false);
				commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			}
		}
		if(targetCommandBeanTreeNode != null && targetCommandBeanTreeNode.getData()!=null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(ActivityRsd.class)){
				if("phaseCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){ //endor - 20120309-0638-ekueh
					ActivityUtils.autoCalculateTripNumber(UserSession.getInstance(request), targetCommandBeanTreeNode);
				}
			}
		}
		
	}

	private List<String> getSelectedNodesUid(HttpServletRequest request) throws Exception
	{
		String value = request.getParameter("primaryKey");
		List list = new ArrayList();
		for (String str:value.split(","))
		{
			list.add(str);
		}
		return list;
	}
	
	private Map<String,LookupItem> getLookup(BaseCommandBean commandBean, HttpServletRequest request, String className, String field) throws Exception
	{
		UserSelectionSnapshot selection = new UserSelectionSnapshot(UserSession.getInstance(request));
		Object handler = null;
		if (commandBean.isCascadeLookup(className, field))
		{
			CascadeLookupMeta meta = commandBean.getCascadeLookupMeta(className, field);
			if (meta!=null)
			handler = meta.getLookupUriOrHandler();
		}
		if (commandBean.isLookup(className, field))
		{
			handler = commandBean.getLookup().get(className+"."+field);
		}
		if (handler !=null)
		{
			if (handler instanceof LookupHandler)
				return ((LookupHandler)handler).getLookup(commandBean, commandBean.getRoot(), selection, request, null);
			if (handler instanceof String)
				return LookupManager.getConfiguredInstance().getLookup(handler.toString(), selection, null);
		}
		return null;
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("activityByOperationList".equalsIgnoreCase(invocationKey)) {
			
			String activityUidSelected = request.getParameter("activityUidSelected");

			if(StringUtils.isNotBlank(activityUidSelected)){
				//change activity code
				
				String selectedActivityUid = activityUidSelected;
				
				String selectedSectionCode = request.getParameter("sectionSelected");
				String selectedClassCode = request.getParameter("classCodeSelected");
				String selectedPhaseCode = request.getParameter("phaseCodeSelected");
				String selectedTaskCode = request.getParameter("taskCodeSelected");
				
				if (StringUtils.isNotBlank(selectedActivityUid)) {
					for (String item : selectedActivityUid.split(",")) {
						
						String activityRsdUid = item.trim();
						
						if (StringUtils.isNotBlank(activityRsdUid)) {
							if (StringUtils.isNotBlank(selectedClassCode)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedClassCode)?null:selectedClassCode;
								String strSql = "UPDATE ActivityRsd SET classCode=:classCode where activityRsdUid=:activityRsdUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"classCode", "activityRsdUid"} , new Object[] {selectedCode, activityRsdUid});
							}
							
							if (StringUtils.isNotBlank(selectedPhaseCode)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedPhaseCode)?null:selectedPhaseCode;
								String strSql = "UPDATE ActivityRsd SET phaseCode=:phaseCode where activityRsdUid=:activityRsdUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"phaseCode", "activityRsdUid"} , new Object[] {selectedCode, activityRsdUid});
							}
							
							if (StringUtils.isNotBlank(selectedTaskCode)) {
								String selectedCode= null;
								selectedCode=("-").equalsIgnoreCase(selectedTaskCode)?null:selectedTaskCode;
								String strSql = "UPDATE ActivityRsd SET taskCode=:taskCode where activityRsdUid=:activityRsdUid";
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"taskCode", "activityRsdUid"} , new Object[] {selectedCode, activityRsdUid});
							}
						}
					}
				}

			}
			
			SimpleXmlWriter writer = new SimpleXmlWriter(response); 
	
			UserSession session = UserSession.getInstance(request);
			String operationUid = session.getCurrentOperationUid();
			String operationCode = session.getCurrentOperationType();
			String onOffShore = session.getCurrentWellOnOffShore();

			DateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			CustomFieldUom thisConverter = new CustomFieldUom(session.getUserLocale());
			writer.startElement("root");
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select d.dayDate, a.activityRsdUid, a.dailyUid, " +
					"a.startDatetime, a.endDatetime, a.depthMdMsl, a.activityDuration, a.classCode, a.phaseCode, a.activityDescription, " +
					"a.taskCode, a.rootCauseCode FROM ActivityRsd a, Daily d " +
					"WHERE (a.isDeleted is null or a.isDeleted = false) AND (d.isDeleted is null or d.isDeleted = false) AND " +
					"a.operationUid=:operationUid AND a.dailyUid=d.dailyUid ORDER by a.startDatetime, a.endDatetime", "operationUid", operationUid, qp);
			
			if (list.size() > 0){
				
				for (Object[] rec : list) {
					writer.addElement("dailyUid", (String) rec[2]);
					writer.addElement("startDateTime", CommonUtil.getConfiguredInstance().showTimeAs2400(rec[3]));
					writer.addElement("endDateTime", CommonUtil.getConfiguredInstance().showTimeAs2400(rec[4]));
						
					if (rec[5] !=null){
						thisConverter = new CustomFieldUom(session.getUserLocale(), ActivityRsd.class, "depthMdMsl");
						thisConverter.setBaseValue(Double.parseDouble(rec[5].toString()));
						writer.addElement("depthMdMsl",thisConverter.getFormattedValue());
					}else {
						writer.addElement("depthMdMsl","");
					}
					
					if (rec[6] !=null) {
						thisConverter = new CustomFieldUom(session.getUserLocale(), ActivityRsd.class, "activityDuration");
						thisConverter.setBaseValue(Double.parseDouble(rec[6].toString()));
						writer.addElement("duration", thisConverter.getFormattedValue());
					}else {
						writer.addElement("duration", "");
					}
					
					writer.addElement("classCode", (String) this.nullToEmptyString(rec[8]));
					writer.addElement("phaseCode", (String) this.nullToEmptyString(rec[9]));
					writer.addElement("description", (String) this.nullToEmptyString(rec[10]));
					writer.addElement("taskCode", (String) this.nullToEmptyString(rec[11]));
					writer.addElement("rootCauseCode", (String) this.nullToEmptyString(rec[12]));
					writer.endElement();
				}
				
				//get lookup class code
				List<LookupClassCode> lookupClassCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From LookupClassCode where " +
						"(operationCode=:operationCode or operationCode = '' or operationCode is null) and (isDeleted is null or isDeleted = false) " +
						"group by shortCode order by sequence", "operationCode", operationCode);

				writer.startElement("LookupClassCode");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupClassCode");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();

				if (lookupClassCode.size() > 0){
					for (LookupClassCode rec : lookupClassCode) {
						writer.startElement("LookupClassCode");
						writer.addElement("code", rec.getShortCode());
						writer.addElement("label", rec.getName() + " (" + rec.getShortCode() + ")");
						writer.endElement();
						
					}
				}
				
				//get lookup phase code
				List<LookupPhaseCode> lookupPhaseCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From LookupPhaseCode where " +
						"(operationCode=:operationCode or operationCode = '' or operationCode is null) and (isDeleted is null or isDeleted = false) " +
						"and (onOffShore=:onOffShore or onOffShore='' or onOffShore is null) group by shortCode order by name", new String[] {"operationCode", "onOffShore"}, new Object[] {operationCode, onOffShore});
				
				writer.startElement("LookupPhaseCode");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupPhaseCode");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();
				
				if (lookupPhaseCode.size() > 0){
					for (LookupPhaseCode rec : lookupPhaseCode) {
						writer.startElement("LookupPhaseCode");
						writer.addElement("code", rec.getShortCode());
						writer.addElement("label", rec.getName() + " (" + rec.getShortCode() + ")");
						writer.endElement();
						
					}
				}

				//get task code
				List<LookupTaskCode> lookupTaskCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From LookupTaskCode where " +
						"(operationCode=:operationCode or operationCode = '' or operationCode is null) and (isDeleted is null or isDeleted = false) " +
						"and (onOffShore=:onOffShore or onOffShore='' or onOffShore is null) group by shortCode order by sequence", new String[] {"operationCode", "onOffShore"}, new Object[] {operationCode, onOffShore});
				
				Collections.sort(lookupTaskCode, new LookupTaskCodeComparator());
				writer.startElement("LookupTaskCode");
				writer.addElement("code", "");
				writer.addElement("label", "");
				writer.endElement();
				
				writer.startElement("LookupTaskCode");
				writer.addElement("code", "-");
				writer.addElement("label", "-");
				writer.endElement();
				
				if (lookupTaskCode.size() > 0){
					for (LookupTaskCode rec : lookupTaskCode) {
						writer.startElement("LookupTaskCode");
						writer.addElement("code", rec.getShortCode());
						writer.addElement("label", rec.getName() + " (" + rec.getShortCode() + ")");
						writer.endElement();
						
					}
				}
				writer.endElement();
				writer.close();
			
			
			}
		
		}
		if ("redoFTR".equalsIgnoreCase(invocationKey)) {
			String activityRsdUid = request.getParameter("activityRsdUid");
			ActivityRsd actRsd = (ActivityRsd)  ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ActivityRsd.class, activityRsdUid);
			
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("uidWell", actRsd.getWellUid());
			data.put("uidWellbore", actRsd.getWellboreUid());
			data.put("uidOperation", actRsd.getOperationUid());
			
			Operation operation = (Operation)  ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, actRsd.getOperationUid());
			data.put("datumUid", operation.getDefaultDatumUid());
			
	//		UserSelectionSnapshot userSelection = new UserSelectionSnapshot();
	 //       userSelection.populdateDataAccordingToUserLogin("idsadmin");
			UserSession session = UserSession.getInstance(request);
			UserSelectionSnapshot userSelection = new UserSelectionSnapshot();
			userSelection.populdateDataAccordingToUserLogin(session.getUserName());
			data.put("userSelection", userSelection);
			data.put("isAddToActivity", "1".equals(GroupWidePreference.getValue(operation.getGroupUid(), "autopopulateMARActivityToActivity")));
			
			RigStateRaw rigState = (RigStateRaw)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigStateRaw.class, activityRsdUid);
			if (rigState!= null) {
				RigStateData rigStateData = new RigStateData();
				rigStateData.setId(activityRsdUid);
				rigStateData.setTimeZone(DepotUtils.getConfiguredInstance().getWellTimeZoneByWellUid(actRsd.getWellUid()));
				rigStateData.setState(rigState.getRigStateId());

				rigStateData.runStateDef(actRsd.getOperationUid());
					
				data.put("rec", rigStateData);
				
				this.listeners.activityEnded(data);
			}
			
		}
	}
	
	private String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}
	
	public void setIgnore24HourChecking(Boolean ignore24HourChecking) {
		this.ignore24HourChecking = ignore24HourChecking;
	}

	public Boolean getIgnore24HourChecking() {
		return ignore24HourChecking;
	}
	
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClearEmptyDaily(Boolean clearEmptyDaily) {
		this.clearEmptyDaily = clearEmptyDaily;
	}

	public Boolean getClearEmptyDaily() {
		return clearEmptyDaily;
	}

	public void setShowSystemMessageForActivity(Boolean showSystemMessageForActivity) {
		this.showSystemMessageForActivity = showSystemMessageForActivity;
	}

	public Boolean getShowSystemMessageForActivity() {
		return showSystemMessageForActivity;
	}
	
	public void setDisplayCompanyRCMessageOnSystemMessage(
			Boolean displayCompanyRCMessageOnSystemMessage) {
		this.displayCompanyRCMessageOnSystemMessage = displayCompanyRCMessageOnSystemMessage;
	}

	public Boolean getDisplayCompanyRCMessageOnSystemMessage() {
		return displayCompanyRCMessageOnSystemMessage;
	}

	private class LookupTaskCodeComparator implements  Comparator<LookupTaskCode>{
		public int compare(LookupTaskCode o1, LookupTaskCode o2){
			try{
				
				String s1 = WellNameUtil.getPaddedStr(o1.getShortCode());
				String s2 = WellNameUtil.getPaddedStr(o2.getShortCode());
				
				if (s1 == null || s2 == null) return 0;
				return s1.compareTo(s2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
		UserSession session = UserSession.getInstance(request);
		
		if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "disAllowSavingIfTotalHourMoreThan24"))) {
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityRsd.class, "activityDuration");
			//get operationHour set in operation screen 
			Double operationHour =  24.0;
			if (session.getCurrentOperation()!=null) {
				operationHour = Double.parseDouble(ActivityUtils.getOperationHour(session.getCurrentOperation()));
			}	
			
			Double totalActivityDuration = 0.0;
			Date newDate = new Date();
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ActivityRsd").values()) {
				ActivityRsd activity = (ActivityRsd) node.getData();
				
				// only calculate duration when it is save action 
				if(!node.getAtts().getAction().equals(Action.DELETE)) {
					if (activity != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {					
						//need to calculate activity duration as calculation not yet done
						Date start = ActivityUtils.getPopulatedStartTime(activity, commandBean);
						Date end = CommonUtil.getConfiguredInstance().setDate(activity.getEndDatetime(), newDate);
						
						Double activityDuration = ActivityUtils.getActivityDurationInHour(start, end, thisConverter);						
						totalActivityDuration += activityDuration;	
					}
				}
			}
			
			//show error message when total activity Hour more than operation Hour
			if (totalActivityDuration > operationHour){	
				if(this.showSystemMessageForActivity)
				{
					commandBean.getRoot().setDirty(true);
					commandBean.getSystemMessage().addError("Total Activity Hours exceeds " + ActivityUtils.getOperationHour(session.getCurrentOperation()) + "hrs. Please check and correct the record." ,true, true);
					commandBean.setAbortFormProcessing(true);
				}
			  	   
			}
		}
		
	}
	
	
	
		
}
