package com.idsdatanet.d2.drillnet.bitrun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class BitrunCommandBeanListener extends EmptyCommandBeanListener {
	private boolean _bharunLimitCheck = true;
	
	public void setBharunLimitCheck(boolean bharunLimitCheck) {
		this._bharunLimitCheck = bharunLimitCheck;
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		String action = request.getParameter("_action");
		if ("CopyBitRunNumber".equals(invocationKey)) {
			String strResult = "<bitrunNumber>";
			UserSession session = UserSession.getInstance(request);
			String operationUid = session.getCurrentOperationUid();
			
			List list = new ArrayList();
			if (action.equals("copy_bit_run_from_previous_run")) {
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select bitrunUid, bitrunNumber from Bitrun where (isDeleted = false or isDeleted is null) and operationUid = :operationUid order by bitrunNumber", "operationUid", operationUid);
				if (!list.isEmpty()){
					Collections.sort(list, new BitrunNumberComparator());
					for (Object obj : list) {
						Object[] _obj = (Object[]) obj;
						String bitrunUid = (String) _obj[0];
						String bitrunNumber = (String) _obj[1];
						strResult = strResult + "<item key=\"" + bitrunUid + "\" value=\"" + bitrunNumber + "\"/>";
					}
				}
			}
			strResult = strResult + "</bitrunNumber>";
			response.getWriter().write(strResult);
		}
		
		if ("checkBitrunNumberExist".equals(invocationKey)) {
			String name = request.getParameter("name");
			String bitrunUid = request.getParameter("bitrunUid");
			boolean isNewRecord = request.getParameter("isNewRecord").equals("1")? true : false;
			boolean isDuplicated = false;

			UserSession session = UserSession.getInstance(request);
			String operationUid = session.getCurrentOperationUid();
	
			String strResult = "<bitrunNumberExist>";
			isDuplicated = CommonUtil.getConfiguredInstance().isBitrunNumberDuplicate(name, operationUid, isNewRecord, bitrunUid);		
			strResult = strResult + "<item name=\"duplicate\" value=\"" + isDuplicated + "\" /></bitrunNumberExist>";
			response.getWriter().write(strResult);
		}
		return;
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		String action = request.getParameter("_action");
		String bitrunUid = "";
		if (StringUtils.isNotBlank(action) || action != null) {
			if (action.equals("copy_bit_run_from_previous_run")) {
				bitrunUid = request.getParameter(action);
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Bitrun where bitrunUid = :bitrunUid", "bitrunUid", bitrunUid);
				if (list != null && list.size() > 0) {
					Bitrun bitrun = (Bitrun) list.get(0);
					setPrimaryKeyToNull(Bitrun.class, bitrun);
					bitrun.setBharunUid(null);
					CommandBeanTreeNode bitrunNode = commandBean.getRoot().addCustomNewChildNodeForInput(bitrun);
					List<BitNozzle> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from BitNozzle where bitrunUid = :bitrunUid", "bitrunUid", bitrunUid);
					if (list2 != null && list2.size() > 0) {
						for (BitNozzle bitNozzle : list2) {
							setPrimaryKeyToNull(BitNozzle.class, bitNozzle);
							bitrunNode.addCustomNewChildNodeForInput(bitNozzle);
						}
					}
				}
				commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
			}
		}

		if (targetCommandBeanTreeNode !=null && targetCommandBeanTreeNode.getDataDefinition() != null) {
			if (Bitrun.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())) {
				if("bharunUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())) {
					
					Bitrun thisBitrun = (Bitrun) targetCommandBeanTreeNode.getData();
					bitrunUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "bitrunUid");
					String bharunUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "bharunUid");
					Bharun bharun = (Bharun) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Bharun.class, bharunUid);
					
					if (bharun != null && (bharun.getIsDeleted() == null || !bharun.getIsDeleted())) {
						
						// Check whether BHA Run already has a Bit Run associated (#14851)
						if(_bharunLimitCheck) //TODO re-implement the check if there is a way to check both existing records from DB AND the records from Add New at the same time
						{
							// Check existing data from DB (does not include new records that haven't been saved)
							String[] paramsFields = {"bharunUid", "bitrunUid"};
							Object[] paramsValues = {bharunUid, bitrunUid};
							String strSql = "FROM Bitrun WHERE (isDeleted = false OR isDeleted IS NULL) AND bharunUid = :bharunUid AND bitrunUid <> :bitrunUid";
							List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
							if (lstResult.size() > 0) {
								Bitrun otherBitrun = (Bitrun) lstResult.get(0);
								thisBitrun.setBharunUid(null);
								commandBean.getSystemMessage().addWarning("BHA Run #" + bharun.getBhaRunNumber().toString() + " already has been associated to Bit Run #" + otherBitrun.getBitrunNumber().toString() + ".");
								commandBean.getFlexClientControl().setReloadAfterPageCancel();
								return;
							}
							
							// Have to check the new records that are not saved as well (double checks the same day's Bitrun however)
							for(CommandBeanTreeNode node : targetCommandBeanTreeNode.getParent().getList().get("Bitrun").values()) {
								Bitrun bitrun = (Bitrun) node.getData();
								if(!thisBitrun.equals(bitrun)) {
									if((thisBitrun.getBharunUid()!=null) && (bitrun.getBharunUid()!=null)) {
										if(thisBitrun.getBharunUid().equals(bitrun.getBharunUid())) {
											thisBitrun.setBharunUid(null);
											commandBean.getSystemMessage().addWarning("BHA Run #" + bharun.getBhaRunNumber().toString() + " already has been associated to Bit Run #" + bitrun.getBitrunNumber().toString() + ".");
											return;
										}
									}
								}
							}
						}						
						thisBitrun.setDepthInMdMsl(bharun.getDepthInMdMsl());
						thisBitrun.setDepthOutMdMsl(bharun.getDepthOutMdMsl());
						thisBitrun.setDailyUidIn(bharun.getDailyidIn());
						thisBitrun.setDailyUidOut(bharun.getDailyidOut());
						thisBitrun.setTimeIn(bharun.getTimeIn());
						thisBitrun.setTimeOut(bharun.getTimeOut());
					}
				}
			}
		}
	}
	
	private void setPrimaryKeyToNull(Class clazz, Object object) throws Exception {
		String primaryKey = ApplicationUtils.getConfiguredInstance().getIdentifierPropertyName(clazz);
		PropertyUtils.setSimpleProperty(object, primaryKey, null);
	} 
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		
		String dailyUid = userSelection.getDailyUid();
		Boolean enabled = true;
		if (StringUtils.isNotBlank(dailyUid)) {
			String strSql = "FROM BharunDailySummary WHERE (isDeleted = false or isDeleted is null) and dailyUid =:currentDailyUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "currentDailyUid", dailyUid);
			if (lstResult.size() > 0){
				enabled = true;
			}else{
				enabled = false;
			}
			commandBean.setSupportedAction("add", enabled);
			commandBean.getRoot().getDynaAttr().put("isAllowAddBit", enabled);
			if (enabled == false){
				commandBean.getSystemMessage().addInfo("There is no BHA run for today. Please go to BHA run screen to add a new BHA run or carry from previous run.", true);
			}
		}
	}
	
	private class BitrunNumberComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try {
				Object in1 = o1[1];
				Object in2 = o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(in1.toString());
				String f2 = WellNameUtil.getPaddedStr(in2.toString());
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			} catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
}
