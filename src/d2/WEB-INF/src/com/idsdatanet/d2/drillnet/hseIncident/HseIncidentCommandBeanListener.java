package com.idsdatanet.d2.drillnet.hseIncident;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.rigUtilization.RigUtilizationUtils;

public class HseIncidentCommandBeanListener extends EmptyCommandBeanListener{
	@Override
	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		if (request!=null) {
			UserSession session = UserSession.getInstance(request);
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_DAYS_SINCE_LAST_LTI_BASED_ON_CONTRACT))) {
				RigUtilizationUtils.updateAllReportDailyDaysSinceLastLti(session.getCurrentGroupUid(), session.getCurrentRigInformationUid(), session.getCurrentDailyUid());
			}
		}
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
	
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
			if (request!=null){
				if(StringUtils.isNotBlank(request.getParameter("target_class"))){
					
					String hsePlanName = "";
					String hsePlanUid = "";
					
					if ("LaggingIndicator".equals(request.getParameter("target_class"))) hsePlanName = "Lagging Indicators";
					if ("LeadingIndicator".equals(request.getParameter("target_class"))) hsePlanName = "Leading Indicators";
					if ("OtherIndicator".equals(request.getParameter("target_class"))) hsePlanName = "Other Indicators";
					if ("DailyObservation".equals(request.getParameter("target_class"))) hsePlanName = "Daily Observation";
					
					UserSession session = UserSession.getInstance(request);
					UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
					
					Daily currentDay =  ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
					
					String[] paramsFields = {"rigInformationUid","dayDate", "planName"};
					Object[] paramsValues = {userSelection.getRigInformationUid(),currentDay.getDayDate(), hsePlanName};
					
					String strHsePlanSql ="SELECT hsePlanUid FROM HsePlan WHERE (isDeleted=false or isDeleted is null) and " +
							"rigInformationUid=:rigInformationUid and ((dateStart <=:dayDate and dateEnd >=:dayDate) or " +
							"(dateStart is null and dateEnd is null) or (dateStart <=:dayDate and dateEnd is null) or " +
							"(dateStart is null and dateEnd >=:dayDate)) and planName=:planName";
					
					List lstHsePlanResult= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strHsePlanSql, paramsFields, paramsValues);
					
					if (!lstHsePlanResult.isEmpty()) {
						hsePlanUid = lstHsePlanResult.get(0).toString();
						
						// populates hse incident category
						String strSql = "SELECT incidentCategory, sequence, description from HsePlanEvents where hsePlanUid=:hsePlanUid and (isDeleted=false or isDeleted is null)" +
								"order by sequence";
						List<Object[]> result= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "hsePlanUid", hsePlanUid);
						
						if (result != null)
						{
							for (Object[] rec: result) {
								
								if ("LaggingIndicator".equals(request.getParameter("target_class"))) {
									LaggingIndicator laggingIndicator = new LaggingIndicator();
									Object[] objIncident = (Object[]) rec;
									laggingIndicator.setIncidentCategory(objIncident[0].toString());
									if (objIncident[2] != null) {
										laggingIndicator.setHseShortdescription(objIncident[2].toString());
									}
									if (objIncident[1] != null) {
										laggingIndicator.setIncidentRefNumber(objIncident[1].toString());
									}
									laggingIndicator.setHseEventdatetime(currentDay.getDayDate());
									laggingIndicator.setSystemStatus("lagging");
									laggingIndicator.setRigInformationUid(userSelection.getRigInformationUid());
									commandBean.getRoot().addCustomNewChildNodeForInput(laggingIndicator);
									
								}else if ("LeadingIndicator".equals(request.getParameter("target_class"))) {
									LeadingIndicator leadingIndicator = new LeadingIndicator();
									Object[] objIncident = (Object[]) rec;
									leadingIndicator.setIncidentCategory(objIncident[0].toString());
									if (objIncident[2] != null) {
										leadingIndicator.setHseShortdescription(objIncident[2].toString());
									}
									if (objIncident[1] != null) {
										leadingIndicator.setIncidentRefNumber(objIncident[1].toString());
									}
									leadingIndicator.setHseEventdatetime(currentDay.getDayDate());	
									leadingIndicator.setSystemStatus("leading");
									leadingIndicator.setRigInformationUid(userSelection.getRigInformationUid());
									commandBean.getRoot().addCustomNewChildNodeForInput(leadingIndicator);
								}else if ("OtherIndicator".equals(request.getParameter("target_class"))) {
									OtherIndicator otherIndicator = new OtherIndicator();
									Object[] objIncident = (Object[]) rec;
									otherIndicator.setIncidentCategory(objIncident[0].toString());
									if (objIncident[2] != null) {
										otherIndicator.setHseShortdescription(objIncident[2].toString());
									}
									if (objIncident[1] != null) {
										otherIndicator.setIncidentRefNumber(objIncident[1].toString());
									}
									otherIndicator.setHseEventdatetime(currentDay.getDayDate());	
									otherIndicator.setSystemStatus("other");
									otherIndicator.setRigInformationUid(userSelection.getRigInformationUid());
									commandBean.getRoot().addCustomNewChildNodeForInput(otherIndicator);
								}else if ("DailyObservation".equals(request.getParameter("target_class"))) {
									DailyObservation dailyObservation = new DailyObservation();
									Object[] objIncident = (Object[]) rec;
									dailyObservation.setIncidentCategory(objIncident[0].toString());
									if (objIncident[2] != null) {
										dailyObservation.setHseShortdescription(objIncident[2].toString());
									}
									if (objIncident[1] != null) {
										dailyObservation.setIncidentRefNumber(objIncident[1].toString());
									}
									dailyObservation.setHseEventdatetime(currentDay.getDayDate());	
									dailyObservation.setSystemStatus("observation");
									dailyObservation.setRigInformationUid(userSelection.getRigInformationUid());
									commandBean.getRoot().addCustomNewChildNodeForInput(dailyObservation);
								}								
							}
							commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
							
						}
					}
				}

			}
			
		}
	
	}

}
