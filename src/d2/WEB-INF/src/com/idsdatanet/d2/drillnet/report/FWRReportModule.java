package com.idsdatanet.d2.drillnet.report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.report.BeanDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class FWRReportModule extends DefaultReportModule{
	private static final String FIELD_SELECTED_SECTIONS ="reportSelectedSections";
	private static final String FIELD_ADDITIONAL_INPUT_PARAM ="reportAdditionalInputParam";
	private static final String FWR_ADDITIONAL_SELECTION ="FWRAdditionalSelections";
	private static final String FIELD_CHILD_WELL_LIST ="parentChildFwr";
	
	private Map<String,fwrReportConfiguration> supportedSections = null;
	private static final String FWR_SECTIONS_MAP="FWRSelectionSections";
	private List<String> selectedBeans=null; 
	private Map<String,List> hiddenSectionForOperation;
	private Map additionalSelections;
	private List<String> operationList = null;
	//use for parent child fwr when more than 1 operation is selected 
	private Boolean isCombined = false;
	//private boolean validateOperationTypeByPrefix=false;
	
	public void setSupportedSections(Map<String,fwrReportConfiguration> sections){
		this.supportedSections = sections;
	}
	protected ReportOutputFile onLoadReportOutputFile(ReportOutputFile reportOutputFile){
			return reportOutputFile;
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		UserSession userSession=UserSession.getInstance(request);
		// String operationType=validateOperationTypeByPrefix?userSession.getCurrentOperationTypePrefix():userSession.getCurrentOperationType();
		String operationType=userSession.getCurrentOperationTypePrefix();
		List<String> hideList=null;
		if (hiddenSectionForOperation!=null)
		{
			hideList=hiddenSectionForOperation.get(operationType);
		}
		
		if ("flexFwrChildWellList".equals(invocationKey)) {			
			this.operationList = null;
			
			String currentWellUid = userSession.getCurrentWellUid();
			String currentOperationUid = userSession.getCurrentOperationUid();
			
			this.getCurrentWellOps(currentWellUid);
			if(operationList.contains(currentOperationUid)) operationList.remove(currentOperationUid);
			
			if(!operationList.isEmpty()){
				SimpleXmlWriter writer = new SimpleXmlWriter(response);
				writer.startElement("root");
				
				for (String operation : operationList)
				{
					String operationUid = String.valueOf(operation);
	
					if (ApplicationUtils.getConfiguredInstance().getCachedAllOperations().containsKey(operationUid))
					{
						writer.startElement("Operation");
						writer.addElement("operationUid", operationUid);
						String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(UserSession.getInstance(request).getCurrentGroupUid(), operationUid);
						writer.addElement("operationName", operationName);
						writer.endElement();
					}
				}
				writer.endElement();
				writer.close();
			}
		}		
		
		if ("flexFwrReportSectionList".equals(invocationKey)) {
			if( this.supportedSections!=null)
			{
				SimpleXmlWriter writer = new SimpleXmlWriter(response);
				writer.startElement("root");
				List list = new LinkedList(supportedSections.entrySet());
				Collections.sort(list, new Comparator() {
					public int compare(Object o1, Object o2) {
						fwrReportConfiguration object1 = (fwrReportConfiguration) ((Map.Entry) (o1)).getValue();
						fwrReportConfiguration object2 = (fwrReportConfiguration) ((Map.Entry) (o2)).getValue();
						if (object1==null || object1.getTitle()==null || object2==null || object2.getTitle()==null) return 0;
						return (object1.getTitle()).compareTo(object2.getTitle());
					}
				});
			    for (Iterator it = list.iterator(); it.hasNext();) {
			    	Map.Entry<String, Object> entry = (Map.Entry)it.next();
					fwrReportConfiguration sections=this.supportedSections.get(entry.getKey());
					if (!sections.getRequired()){
						boolean include=true;
						if (hideList!=null)
							if (hideList.contains(entry.getKey()))
								include=false;
						if (include){
							writer.startElement("FWRSection");
							writer.addElement("sectionBeanId", entry.getKey());
							writer.addElement("sectionLabel", sections.getTitle());
							writer.addElement("customProperty", sections.getCustomProperty());
							writer.addElement("isNumeric", sections.getIsNumeric().toString());
							writer.addElement("label", sections.getLabel());
							writer.endElement();
						}
					}
				}
				writer.endElement();
				writer.close();
			}
			
		} else {
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);
		}
	}
	private Map getSelectedSection(CommandBean commandBean) throws Exception {
		Object selected_sections = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_SECTIONS);
		if(selected_sections == null){
			return null;
		}
		if(StringUtils.isBlank(selected_sections.toString())){
			return null;
		}
		
		String[] id_list = selected_sections.toString().split(",");
		Map result = new HashMap();
		for(String id: id_list){
			result.put(id.trim(), null);
		}
		
		return result;
	}
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		//commandBean.getSystemMessage().clear();
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot( session );
		
		this.setIncludeOtherDocument("true".equals(commandBean.getRoot().getDynaAttr().get("includeOtherDocument")));		
				
		additionalSelections = new HashMap();
		if(commandBean.getRoot().getDynaAttr().get(FIELD_ADDITIONAL_INPUT_PARAM) != null){
			Object additionalOption = commandBean.getRoot().getDynaAttr().get(FIELD_ADDITIONAL_INPUT_PARAM);
			String[] option_list = additionalOption.toString().split(",");
			
			for(String id: option_list){
				String[] prop = id.split(":");
				if(prop.length == 2) additionalSelections.put(prop[0], prop[1]);
			}
			userSelection.setCustomProperties(additionalSelections);
			
		}					
		
		if (this.getSelectedSection(commandBean)!=null){
			this.getRequiredBeans(this.getSelectedSection(commandBean));
			Map runtimeContext=new HashMap();
			runtimeContext.put(FWR_ADDITIONAL_SELECTION, additionalSelections);
			runtimeContext.put(FWRReportModule.FWR_SECTIONS_MAP, this.getSelectedSection(commandBean));
			ReportJobParams reportJobData=this.getParametersForReportSubmission(session, request, commandBean);
			reportJobData.setRuntimeContext(runtimeContext);
			
			if(this.isParentChildFwr()){
				this.isCombined = false;
				// check GWP setting to decide whether combine operations for the chart / graph drawn				
				if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "combineOperationsForGraphAndChart")))
			    { 
			        runtimeContext.put("COMBINED_OPERATIONS_CHART", true);
			    }else{
			    	runtimeContext.put("COMBINED_OPERATIONS_CHART", false);
			    }
				List<String> opsList  = this.getSelectedOperations(commandBean, session);
				runtimeContext.put("OPERATION_HEADER", this.getCurrentWellOperation(session.getCurrentWellUid(),session.getCurrentGroupUid(), opsList));
				
				if(opsList != null && opsList.size() > 0){
					List<UserSelectionSnapshot> userSelectionList = new ArrayList<UserSelectionSnapshot>();
										
					userSelectionList = this.createMultipleOperationUserSelection(session, opsList);	
					for(int i=0; i < userSelectionList.size(); ++i){	
						userSelectionList.get(i).setCustomProperties(additionalSelections);
					}
					//if more than one operation selected, set is combined to true - in order to stamp _combined on file name
					if (opsList.size() > 1){
						this.isCombined = true;
					}
					//reportJobData.setMultiWellJob(true);				
					return super.submitReportJob(userSelectionList, reportJobData);
				}else{
					commandBean.getSystemMessage().addError("Warning : No Operation Selected", false, true);
					return null;
				}
			}else{
			return super.submitReportJob(userSelection ,reportJobData);
			}
			
		}else{
			commandBean.getSystemMessage().addError("Warning : No Section Selected", false, true);
			//commandBean.getSystemMessage().addWarning("Warning : No Section Selected");
			return null;
		}
		
	}
	
	public List<Operation> getOperationList(Map selectedOperations) throws Exception{
		List<Operation> list = null;
		String operationList = "";
		for(Object key: selectedOperations.keySet()){
			if(StringUtils.isNotEmpty(key.toString()) && StringUtils.isNotBlank(key.toString())){
				if(operationList.equals("")){
					operationList = "('" + key.toString() + "'";
				}else{
					operationList = operationList + ",'" + key.toString() + "'";
				}
			}
		}
		operationList += ")";
		
		list = ApplicationUtils.getConfiguredInstance().getDaoManager().find("From Operation where " +
				"(isDeleted is null or isDeleted = false) AND operationUid IN " + operationList);
		
		return list;
	}
	
	private void getRequiredBeans(Map refBeans){
		this.selectedBeans=null;
		for(String key: supportedSections.keySet()){
			fwrReportConfiguration requiredSections=this.supportedSections.get(key);
			if (requiredSections.getRequired() || refBeans.containsKey(key))
			{
				for(String val:requiredSections.getBeanForReportXml())
				{
					if (this.selectedBeans== null)
						this.selectedBeans= new ArrayList();
					if(!this.selectedBeans.contains(val))
						this.selectedBeans.add(val);
				}
			}
		}
	}
	
	private List<UserSelectionSnapshot> createMultipleOperationUserSelection(UserSession session, List<String> list) throws Exception {
		List<UserSelectionSnapshot> selections = new ArrayList<UserSelectionSnapshot>();
		for(String operationUid: list){
			String dailyUid = null;
			
			List<Daily> dailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM Daily WHERE operationUid='" + operationUid + "'");
			if (dailyList.size() > 0){
				dailyUid = dailyList.get(0).getDailyUid();
			}
			selections.add(UserSelectionSnapshot.getInstanceForCustomDaily(session, operationUid, dailyUid));
		}
		return selections;
	}	
	
	protected ReportDataGenerator getReportDataGenerator()
	{
		ReportDataGenerator generator=this.getDataGenerator();
		((BeanDataGenerator)generator).setUseSelectedModuleGeneratorBeansOnly(this.selectedBeans);
		return generator;
	}
	public void setHiddenSectionForOperation(
			Map<String,List> hiddenSectionForOperation) {
		this.hiddenSectionForOperation = hiddenSectionForOperation;
	}
	public Map<String,List> getHiddenSectionForOperation() {
		return hiddenSectionForOperation;
	}
	/*public void setValidateOperationTypeByPrefix(
			boolean validateOperationTypeByPrefix) {
		this.validateOperationTypeByPrefix = validateOperationTypeByPrefix;
	}
	public boolean isValidateOperationTypeByPrefix() {
		return validateOperationTypeByPrefix;
	}*/
	
	public void getParentWellboreOperation(String wellboreUid)  throws Exception{
		List<String> list = null;
		if(this.operationList == null) this.operationList = new ArrayList();
		
		if(StringUtils.isNotBlank(wellboreUid)){
			this.getCurrentWellboreOperation(wellboreUid);
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT parentWellboreUid FROM Wellbore WHERE (isDeleted='' OR isDeleted is null) AND wellboreUid = :wellboreUid", new String[] {"wellboreUid"} , new Object[] {wellboreUid} );
			if(list.size() > 0 && list != null){
				String parentWellboreUid = list.get(0);
				this.getParentWellboreOperation(parentWellboreUid);				
			}
		}		
	}
	
	public void getCurrentWellboreOperation(String wellboreUid) throws Exception{
		//to get operations of the current wellbore
		List<String> list = null;
		list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT operationUid FROM Operation WHERE (isDeleted='' OR isDeleted is null) AND wellboreUid = :wellboreUid", new String[] {"wellboreUid"} , new Object[] {wellboreUid} );
		if(list != null && list.size() > 0){
			for(String operationUid : list){
				if(!operationList.contains(operationUid)) operationList.add(operationUid);
			}
		}
	}
	
	public void getCurrentWellOps(String wellUid) throws Exception{
		//to get operations of the current Well
		this.operationList = new ArrayList();
		List<String> list = null; 
		list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT operationUid FROM Operation WHERE (isDeleted='' OR isDeleted is null) AND wellUid = :wellUid ORDER BY sysOperationStartDatetime", new String[] {"wellUid"} , new Object[] {wellUid} );
		if(list != null && list.size() > 0){
			for(String operationUid : list){
				operationList.add(operationUid);
			}
		}
	}
	
	public String getCurrentWellOperation(String wellUid, String groupUid , List opsList) throws Exception{
		//to get operations of the current Well
		String OperationList = "";
		List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT operationUid FROM Operation WHERE (isDeleted='' OR isDeleted is null) AND wellUid = :wellUid ORDER BY sysOperationStartDatetime", new String[] {"wellUid"} , new Object[] {wellUid} );
		if(list != null && list.size() > 0){
			for(String operationUid : list){
				if (opsList.contains(operationUid)){
					if (OperationList==""){
						OperationList = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, operationUid);
					}else{
						OperationList = OperationList + ", " + CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, operationUid);
					}
				}
			}
			return OperationList;
		}
		return OperationList;
	}
	
	public List<String> getSelectedOperations(CommandBean commandBean, UserSession session) throws Exception{
		Object selected_operations = commandBean.getRoot().getDynaAttr().get(FIELD_CHILD_WELL_LIST);
		List<String> result = new ArrayList();
		
		//include the current selected operation
		result.add(session.getCurrentOperationUid());
		
		if(selected_operations == null){
			return result;
		}
		if(StringUtils.isBlank(selected_operations.toString())){
			return result;
		}
		
		String[] id_list = selected_operations.toString().split(",");
		
		for(String id: id_list){
			if(!result.contains(id.trim())) result.add(id.trim());
		}
		
		return result;
	}
	
	protected String getReportDisplayNameOnCreation(ReportFiles reportFile, ReportTypes reportType, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) throws Exception {
		String file_name_pattern = null;
		if(reportType != null){
			file_name_pattern = reportType.getReportFilenamePattern();
		}
		
		if(StringUtils.isBlank(file_name_pattern)){
			return null;
		}
		
		if(this.isParentChildFwr()){
			//more than 1 selected operations, will stamp _combined after file name pattern
				if (this.isCombined){
					file_name_pattern = file_name_pattern + "_combined";
				}
			}

		return this.formatDisplayName(file_name_pattern, reportFile, operation, daily, reportDaily, userSelection, reportJobParams);
	}
	
}
