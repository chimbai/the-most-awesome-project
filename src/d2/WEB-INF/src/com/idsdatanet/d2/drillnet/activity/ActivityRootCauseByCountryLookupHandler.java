package com.idsdatanet.d2.drillnet.activity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.LookupRootCauseCode;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * Lookup handler to filter Root Cause Code by Operation Code as well as Country. Initial design for OMV Global.
 * 
 * @author mhchai
 */
public class ActivityRootCauseByCountryLookupHandler implements LookupHandler{	
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {

		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid());
		String country = null;
		if (well != null) {
			country = well.getCountry();
		}
		
		String operationCode = userSelection.getOperationType();
		
		boolean hasCountry = country != null && !StringUtils.isEmpty(country);
		List<LookupRootCauseCode> listRCResults = null; 
 		if (hasCountry) {
 			String strSql = "FROM LookupRootCauseCode WHERE (isDeleted = false OR isDeleted is null) " +
			"AND (operationCode = :operationCode OR operationCode IS NULL OR operationCode = '') AND (country = :country OR country IS NULL OR country = '') ORDER BY name";
 			
			String[] paramsFields = {"operationCode", "country"};
	 		Object[] paramsValues = {operationCode, country};
	 		listRCResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
	 		
 		}
 		else {
			String strSql = "FROM LookupRootCauseCode WHERE (isDeleted = false OR isDeleted is null) " +
 			"AND (operationCode = :operationCode OR operationCode IS NULL OR operationCode = '') AND (country IS NULL OR country = '') ORDER BY name";

			String[] paramsFields2 = {"operationCode"};
	 		Object[] paramsValues2 = {operationCode};
	 		listRCResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);
 		}
 		
 		if (listRCResults != null && listRCResults.size() > 0) {

 			LinkedHashMap<String, LookupItem> rcMatchesCountryAndOperationCode = new LinkedHashMap<String, LookupItem>();
 			LinkedHashMap<String, LookupItem> rcMatchesCountry = new LinkedHashMap<String, LookupItem>();
 			LinkedHashMap<String, LookupItem> rcMatchesNullOrEmptyCountry = new LinkedHashMap<String, LookupItem>();
 			
			for (LookupRootCauseCode rootCauseCode : listRCResults) {
				String lookupRootCauseCodeUid = rootCauseCode.getLookupRootCauseCodeUid();
				String key = rootCauseCode.getShortCode();
				String value = rootCauseCode.getName()+ " (" + rootCauseCode.getShortCode() + ")";
				
				LookupItem item = new LookupItem(key, value);
				
	 			boolean matchesCountry = rootCauseCode.getCountry() != null && rootCauseCode.getCountry().equals(country);
	 			boolean matchesOperationCode = (rootCauseCode.getOperationCode() != null && rootCauseCode.getOperationCode().equals(operationCode)) || !(rootCauseCode.getOperationCode() != null && !rootCauseCode.getOperationCode().trim().equals(""));
	 			boolean countryNullOrEmpty = !(rootCauseCode.getCountry() != null && !rootCauseCode.getCountry().trim().equals(""));
	 			item.setActive(rootCauseCode.getIsActive());
	 			
	 			item.setTableUid(lookupRootCauseCodeUid);
				if (hasCountry) {
		 			//logic for root cause selection
		 			//1) only show if operationCode & country match. hide the rest.
		 			//2) if return 0 from (1) only show if country matches and operationCode matches or operationCode is null or ''. hide the rest. [agreed by SA]
		 			//3) if return 0 from (2) only show if operationCode matches or operationCode is null or '' and country is null or ''
					if (matchesCountry && matchesOperationCode) {
						rcMatchesCountryAndOperationCode.put(key, item);
					}
					else if (matchesCountry) {
						//operationCode matching is already is hql
						rcMatchesCountry.put(key, item);
					}
					else if (countryNullOrEmpty) {
						//operationCode matching is already is hql
						rcMatchesNullOrEmptyCountry.put(key, item);
					}
				}
				else {
		 			//show all if reach here
					result.put(key, item);
				}
			}
			
			if (hasCountry) {
				if (!rcMatchesCountryAndOperationCode.isEmpty()) {
					result = rcMatchesCountryAndOperationCode;
				}
				else if (!rcMatchesCountry.isEmpty()) {
					result = rcMatchesCountry;
				}
				else if (!rcMatchesNullOrEmptyCountry.isEmpty()) {
					result = rcMatchesNullOrEmptyCountry;
				}
			}
		}
		
		return result;
	}
	
}
