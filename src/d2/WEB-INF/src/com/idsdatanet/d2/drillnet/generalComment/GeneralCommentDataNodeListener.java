package com.idsdatanet.d2.drillnet.generalComment;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.GeneralComment;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class GeneralCommentDataNodeListener extends EmptyDataNodeListener{
	private Boolean saveCommentDate = false;
	
	public void setsaveCommentDate(Boolean value){
		this.saveCommentDate = value;
	}
	public void getsaveCommentDate(Boolean value){
		this.saveCommentDate = value;
	}
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		
		if (obj instanceof GeneralComment){
			GeneralComment generalComment = (GeneralComment) obj;
			if(daily != null)
			{
				generalComment.setCommentDate(daily.getDayDate());
			}
		}
	}
	public void beforeDataNodeSaveOrUpdate (CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception{
		Object obj = node.getData();
		
		if(obj instanceof GeneralComment) {
			GeneralComment generalComment = (GeneralComment) obj;
			
			String dailyUid = null;
			if (saveCommentDate == true)
			{
				if (StringUtils.isNotBlank(generalComment.getDailyUid())) {
					
					dailyUid = generalComment.getDailyUid();
					Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
					generalComment.setCommentDate(daily.getDayDate());
				}
			}
		}
	}
}

	

