package com.idsdatanet.d2.drillnet.essentialField;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.hibernate.TableMapping;
import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.EssentialFields;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class FieldNameLookupHandler implements CascadeLookupHandler {	
	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, String key, LookupCache lookupCache)
			throws Exception {

		Map<String, LookupItem> result = new TreeMap<String, LookupItem>();
		String packagePath = ApplicationUtils.getConfiguredInstance().getTablePackage();
		Class obj = Class.forName(packagePath + key);
		Field[] fields = obj.getDeclaredFields();
		for (Field f : fields) {
			if(!f.getName().equals("isDeleted") && 
			!f.getName().equals("isSubmit") && 
			!f.getName().equals("lastEditDatetime") && 
			!f.getName().equals("lastEditUserUid") && 
			!f.getName().equals("recordOwnerUid") && 
			!f.getName().equals("sysDeleted") &&
			!f.getName().equals("isIncomplete")){
				String fieldName = f.getName();
				LookupItem item = new LookupItem(fieldName, fieldName);
				result.put(fieldName, item);
			}
		}
		return result;
	}

	@Override
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		return null;
	}

	@Override
	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		List<Class> list = TableMapping.getTableModel();
		for (Class<?> obj : list) {
			if(!obj.getSimpleName().equals("UnitMapping") && !obj.getSimpleName().equals("UomTemplate") && !obj.getSimpleName().equals("UomTemplateMapping")){
				String tableName = obj.getSimpleName();
				Map<String, LookupItem> childLookup = this.getCascadeLookup(commandBean, null, userSelection, request, tableName, null);
				if (childLookup.size()>0) {
					CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(tableName);
					cascadeLookupSet.setLookup(new LinkedHashMap(childLookup));
					result.add(cascadeLookupSet);
				}
			}
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);
		return result_in_array;
	}
}
