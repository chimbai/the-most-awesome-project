package com.idsdatanet.d2.drillnet.report;

import com.idsdatanet.d2.core.model.ReportFiles;

public interface ReportFileListener {
	public void onReportGenerated(ReportFiles reportFiles, BPAbstractReportModule reportModule);
	
	public void onUpdateReport(ReportFiles reportFiles);
}
