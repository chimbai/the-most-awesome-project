package com.idsdatanet.d2.drillnet.campaign;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Campaign;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CampaignOperationDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	// DataLoaderInterceptor
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		String customCondition 	= "(isDeleted = false or isDeleted is null)";
		customCondition += " and campaignUid = :customFilterCampaignUid";
		query.addParam("customFilterCampaignUid", userSelection.getCampaignUid()); // use userSelection whenever possible (instead of UserSession)
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
	// EmptyDataNodeListener  
	
	@Override
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof Campaign) {
			Campaign campaign = (Campaign) object;
			String campaignUid = campaign.getCampaignUid();
			Operation operation = this.getCampaignOperation(campaignUid);
			if (operation!=null) {
				Map<String, Object> wellFieldValues = PropertyUtils.describe(operation);
				for (Map.Entry entry : wellFieldValues.entrySet()) {
					setDynaAttr(node, "operation." + entry.getKey(), entry.getValue());
				}
			}
		}
	}

	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		if (object instanceof Campaign) {
			Campaign campaign = (Campaign) object;
			String campaignUid = campaign.getCampaignUid();
			Operation operation = this.getCampaignOperation(campaignUid);
			if (operation==null) operation = new Operation();
			
			for (String dynaAttrName : CommonUtil.getConfiguredInstance().getDynaAttrNames(commandBean, node, request, "@operation.", "Operation")) {
				CommonUtil.getConfiguredInstance().setProperty(commandBean.getUserLocale(), operation, dynaAttrName.substring(11), node.getDynaAttr().get(dynaAttrName.substring(1)));
			}
			operation.setOperationName(campaign.getCampaignName());
			operation.setIsCampaignOverviewOperation(true);
			operation.setOperationCode(MenuManager.CAMPAIGN_UID);
			operation.setWellboreFinalPurpose(MenuManager.CAMPAIGN_UID);
			operation.setWellboreUid(MenuManager.CAMPAIGN_UID);
			operation.setWellUid(MenuManager.CAMPAIGN_UID);
			operation.setOperationCampaignUid(campaignUid);
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(operation);
			/*
				SAVE COOPS
				1. save object
				2. ApplicationUtils.getConfiguredInstance().refreshCachedData();
				3. session.refreshCachedData();
				4. session.setCurrentCampaignUid(null);		
			*/
			
			// refresh sesison cached data
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
			
			if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				//session.setCurrentDailyUid(null);
				session.setCurrentOperationUid(operation.getOperationUid());
				//session.setCurrentWellboreUid("CAMPAIGN");
				//session.setCurrentWellUid("CAMPAIGN");
				//TODO : set campaignUid to session
				//session.setCurrentCampaignUid(campaignUid);
			}
			
			// reload html page
			if (commandBean.getFlexClientControl() != null) {
				commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			}
		}
	}
	@Override
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		if (object instanceof Campaign) {
			/*
				delete COOPS
				1. save object
				2. ApplicationUtils.getConfiguredInstance().refreshCachedData();
				3. session.refreshCachedData();
				4. session.setCurrentCampaignUid(null);		
			*/
			Campaign campaign = (Campaign) object;
			String campaignUid = campaign.getCampaignUid();
			Operation operation = this.getCampaignOperation(campaignUid);
			if (operation!=null) {
				operation.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(operation);
			}
			
			List<Operation> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Operation where (isCampaignOverviewOperation=false or isCampaignOverviewOperation is null) and operationCampaignUid=:campaignUid", "campaignUid", campaignUid);
			for (Operation ops:rs) {
				ops.setOperationCampaignUid(null);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ops);
			}
			
			//refresh
			ApplicationUtils.getConfiguredInstance().refreshCachedData();
			
			//TODO: set campaignUid to session
			//session.setCurrentCampaignUid(null);
			if (commandBean.getFlexClientControl() != null) {
				commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			}
			
		}
	}
	
	private Operation getCampaignOperation(String campaignUid) throws Exception {
		Operation operation = null;
		String strSql = "from Operation where (isDeleted=false or isDeleted is null) and operationCampaignUid=:campaignUid and isCampaignOverviewOperation=true";
		List<Operation> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "campaignUid", campaignUid);
		if (rs.size()>0) operation = rs.get(0);
		return operation;
	}
	
	@SuppressWarnings("unused")
	private String getDynaAttr(CommandBeanTreeNode node, String dynaAttrName) throws Exception {
		Object dynaAttrValue = node.getDynaAttr().get(dynaAttrName);
		if (dynaAttrValue != null) {
			return dynaAttrValue.toString();
		}
		return null;
	}

	private void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		}
	}
	
}
