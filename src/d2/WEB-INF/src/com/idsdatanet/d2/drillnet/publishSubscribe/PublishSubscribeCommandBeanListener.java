package com.idsdatanet.d2.drillnet.publishSubscribe;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;

import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.SysLogSendToTown;
import com.idsdatanet.d2.core.model.PublishSubscribe;
import com.idsdatanet.d2.core.stt.STTActionCallback;
import com.idsdatanet.d2.core.stt.STTComConfig;
import com.idsdatanet.d2.core.stt.STTDataDirection;
import com.idsdatanet.d2.core.stt.STTEngine;
import com.idsdatanet.d2.core.stt.STTJobConfig;
import com.idsdatanet.d2.core.stt.STTStatus;
import com.idsdatanet.d2.core.stt.STTTransactionType;
import com.idsdatanet.d2.core.stt.jaxb.SttStatus;
import com.idsdatanet.d2.core.uom.hibernate.tables.UomTemplate;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.util.xml.JAXBContextManager;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.SystemExceptionHandler;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.core.query.QueryResult;
import com.idsdatanet.depot.core.util.DepotHttpClient;
import com.idsdatanet.depot.core.util.DepotXmlReader;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


public class PublishSubscribeCommandBeanListener extends EmptyCommandBeanListener {
	
	public static final String METHOD_GET_WELL_LIST = "get_well_list";
	public static final String METHOD_GET_WELLBORE_LIST = "get_wellbore_list";
	public static final String METHOD_GET_OPERATION_LIST = "get_operation_list";
	public static final String METHOD_SUBCRIBE = "get_subcribe";
	public static final String METHOD_SUBCRIBE_ON = "setSubscribeOn";
	public static final String METHOD_SUBCRIBE_OFF = "setSubscribeOff";
	public static final String METHOD_TEST = "testing";
	
	private STTComConfig comConfig;
	private List<STTJobConfig> jobConfigs;
	
	public void setComConfig(STTComConfig comConfig) {
		this.comConfig = comConfig;
	}
	
	public void setJobConfigs(List<STTJobConfig> jobConfigs) {
		this.jobConfigs = jobConfigs;
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
		ImportExportServerProperties serverProperties = null;
		String serverPropertiesUid = (String) request.getParameter("serverPropertiesUid");
		QueryResult queryResult = new QueryResult();
		if (StringUtils.isNotBlank(serverPropertiesUid)) {
			serverProperties = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, serverPropertiesUid);
		}
		if (invocationKey.equals(METHOD_GET_WELL_LIST)) {	
			DepotHttpClient httpClient = new DepotHttpClient(serverProperties.getEndPoint() + "/webservice/welltransfer.html");
			httpClient.setBasicAuthentication(serverProperties.getUsername(), serverProperties.getPassword());
			Map<String, Object> params = new HashMap<>();
            params.put("request_key", "get_well");
            httpClient.invokeGetWithParams(params, queryResult);
            SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			if (queryResult.isSuccessful()) {
                String result = queryResult.getOutputString();
                Document doc = null;
                doc = (Document) DepotXmlReader.getConfiguredInstance().parseWithSAX(result);
                
                List<Element> list = doc.selectNodes("//root/well");
                if (list != null) {
					for (Iterator it=list.iterator(); it.hasNext();) {
						Element element = (Element)it.next();
						String uid = null;
						String name = "";
						Element uidElement = element.element("uid");
						if (uidElement != null) {
							uid = uidElement.getText();
						}
						Element codeElement = element.element("name");
						if (codeElement != null) {
							name = codeElement.getText();
						}
						writer.startElement("well");
						writer.addElement("uid", uid);
						writer.addElement("name", name);
						writer.endElement();
					}
                }
			}
			writer.endElement();
			writer.close();
		} else if (invocationKey.equals(METHOD_GET_WELLBORE_LIST)) {
			String wellUid = (String) request.getParameter("wellId");
			DepotHttpClient httpClient = new DepotHttpClient(serverProperties.getEndPoint() + "/webservice/welltransfer.html");
			httpClient.setBasicAuthentication(serverProperties.getUsername(), serverProperties.getPassword());
			Map<String, Object> params = new HashMap<>();
            params.put("request_key", "get_wellbore");
            params.put("wellUid", wellUid);
            httpClient.invokeGetWithParams(params, queryResult);
            SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			if (queryResult.isSuccessful()) {
                String result = queryResult.getOutputString();
                Document doc = null;
                doc = (Document) DepotXmlReader.getConfiguredInstance().parseWithSAX(result);
                
                List<Element> list = doc.selectNodes("//root/wellbore");
                if (list != null) {
					for (Iterator it=list.iterator(); it.hasNext();) {
						Element element = (Element)it.next();
						String uid = null;
						String name = "";
						Element uidElement = element.element("uid");
						if (uidElement != null) {
							uid = uidElement.getText();
						}
						Element codeElement = element.element("name");
						if (codeElement != null) {
							name = codeElement.getText();
						}
						writer.startElement("wellbore");
						writer.addElement("uid", uid);
						writer.addElement("name", name);
						writer.endElement();
					}
                }
			}
			writer.endElement();
			writer.close();
		} else if (invocationKey.equals(METHOD_GET_OPERATION_LIST)) {
			String wellUid = (String) request.getParameter("wellId");
			String wellboreUid = (String) request.getParameter("wellboreId");
			DepotHttpClient httpClient = new DepotHttpClient(serverProperties.getEndPoint() + "/webservice/welltransfer.html");
			httpClient.setBasicAuthentication(serverProperties.getUsername(), serverProperties.getPassword());
			Map<String, Object> params = new HashMap<>();
            params.put("request_key", "get_operation");
            params.put("wellUid", wellUid);
            params.put("wellboreUid", wellboreUid);
            
            httpClient.invokeGetWithParams(params, queryResult);
            SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			if (queryResult.isSuccessful()) {
                String result = queryResult.getOutputString();
                Document doc = null;
                doc = (Document) DepotXmlReader.getConfiguredInstance().parseWithSAX(result);
                
                List<Element> list = doc.selectNodes("//root/operation");
                if (list != null) {
					for (Iterator it=list.iterator(); it.hasNext();) {
						Element element = (Element)it.next();
						String uid = null;
						String name = "";
						String gid = null;
						Element uidElement = element.element("uid");
						if (uidElement != null) {
							uid = uidElement.getText();
						}
						Element codeElement = element.element("name");
						if (codeElement != null) {
							name = codeElement.getText();
						}
						Element gidElement = element.element("gid");
						if (gidElement != null) {
							gid = gidElement.getText();
						}
						writer.startElement("operation");
						writer.addElement("uid", uid);
						writer.addElement("name", name);
						writer.addElement("gid", gid);
						writer.endElement();
					}
                }
			}
			writer.endElement();
			writer.close();
		} else if (invocationKey.equals(METHOD_SUBCRIBE_ON)) {
			String publishSubscribeUid = (String) request.getParameter("publishSubscribeUid");
			PublishSubscribe ps = (PublishSubscribe) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(PublishSubscribe.class, publishSubscribeUid);
			ps.setIsActive(true);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ps);
			
			this.retrieveOperation(request, commandBean, ps.getOperationId(), ps.getClientGroupUid());
		} else if (invocationKey.equals(METHOD_SUBCRIBE_OFF)) {
			String publishSubscribeUid = (String) request.getParameter("publishSubscribeUid");
			PublishSubscribe ps = (PublishSubscribe) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(PublishSubscribe.class, publishSubscribeUid);
			ps.setIsActive(false);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ps);
		} else if (invocationKey.equals(METHOD_TEST)) {
			String url = (String) request.getParameter("url");
			url = url.substring(0, url.lastIndexOf("/publishsubscribe.html"));
			DepotHttpClient httpClient = new DepotHttpClient(serverProperties.getEndPoint() + "/webservice/welltransfer.html");
			httpClient.setBasicAuthentication(serverProperties.getUsername(), serverProperties.getPassword());
			Map<String, Object> params = new HashMap<>();
            params.put("request_key", "create_ps");
            params.put("host_url", url);
            
            String publishSubscribeUid = (String) request.getParameter("publishSubscribeUid");
			PublishSubscribe ps = (PublishSubscribe) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(PublishSubscribe.class, publishSubscribeUid);
			
			params.put("operationUid", ps.getOperationId());
			params.put("group_id", ps.getGroupUid());
			params.put("is_active", "1");
			
            httpClient.invokePostWithParams(params, queryResult);
            if(queryResult.getOutputString() != null && StringUtils.isNotBlank(queryResult.getOutputString())) {
				String jsonString = queryResult.getOutputString();
            }	
		}
	//	return;
	}
	
	public synchronized void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if ("1".equals(request.getParameter("subscribe"))) {
			String publishSubscribeUid = (String) request.getParameter("publishSubscribeUid");
			PublishSubscribe ps = (PublishSubscribe) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(PublishSubscribe.class, publishSubscribeUid);
			ps.setIsActive(true);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ps);
			
			ImportExportServerProperties serverProperties = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, ps.getServerPropertiesUid());
			this.sendtoCreatePSdata(request, serverProperties, "1");
			comConfig.setDestinationUrl(serverProperties.getEndPoint());
			this.retrieveOperation(request, commandBean, ps.getOperationId(), ps.getClientGroupUid()); 
		}else if ("0".equals(request.getParameter("subscribe"))) {
			String publishSubscribeUid = (String) request.getParameter("publishSubscribeUid");
			PublishSubscribe ps = (PublishSubscribe) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(PublishSubscribe.class, publishSubscribeUid);
			ps.setIsActive(false);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ps);
			
			ImportExportServerProperties serverProperties = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, ps.getServerPropertiesUid());
			this.sendtoCreatePSdata(request, serverProperties, "0");
			commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		}
	}
	
	public void sendtoCreatePSdata(HttpServletRequest request, ImportExportServerProperties serverProperties, String active) throws Exception {
		QueryResult queryResult = new QueryResult();
		String url = (String) request.getParameter("url");
		url = url.substring(0, url.lastIndexOf("/publishsubscribe.html"));
		DepotHttpClient httpClient = new DepotHttpClient(serverProperties.getEndPoint() + "/webservice/welltransfer.html");
		httpClient.setBasicAuthentication(serverProperties.getUsername(), serverProperties.getPassword());
		Map<String, Object> params = new HashMap<>();
        params.put("request_key", "create_ps");
        params.put("host_url", url);
        
        String publishSubscribeUid = (String) request.getParameter("publishSubscribeUid");
		PublishSubscribe ps = (PublishSubscribe) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(PublishSubscribe.class, publishSubscribeUid);
		
		params.put("operationUid", ps.getOperationId());
		params.put("group_id", ps.getGroupUid());
		params.put("is_active", active);
		
        httpClient.invokePostWithParams(params, queryResult);
        if(queryResult.getOutputString() != null && StringUtils.isNotBlank(queryResult.getOutputString())) {
			String jsonString = queryResult.getOutputString();
        }	
	}
	
	public void retrieveOperation(HttpServletRequest request, CommandBean commandBean, String operationUid, String groupUid) throws Exception {
		STTStatus status = new STTStatus();
		SysLogSendToTown sysLog = new SysLogSendToTown();
		
		UserSession userSession = UserSession.getInstance(request);
		boolean skipLookups = true;
		
		try {
			STTEngine.getConfiguredInstance().refresh(comConfig, jobConfigs, null, userSession, groupUid, null, null, operationUid, null, null, false, false, skipLookups, status, false); 
			if (status.hasError()) {
				sysLog.setStatus(STTStatus.FAIL);
				
				JAXBContext jc = JAXBContextManager.getContext("com.idsdatanet.d2.core.stt.jaxb");
				Unmarshaller m = jc.createUnmarshaller();
				SttStatus sttStatus = (SttStatus) m.unmarshal(new ByteArrayInputStream(status.toXMLString().getBytes()));
				for (String error : sttStatus.getErrors().getError()) {
					commandBean.getSystemMessage().addError("Refresh from town failed:\n" + error);
				}
				
			} else {
				sysLog.setStatus(STTStatus.OK);
				commandBean.getSystemMessage().addInfo("Refresh from town completed");
				
				List<String> changedRowPrimaryKeys = new ArrayList<String>();
				changedRowPrimaryKeys.add(operationUid);
				D2ApplicationEvent.publishTableChangedEvent(Operation.class, changedRowPrimaryKeys);
				
				ApplicationUtils.getConfiguredInstance().refreshCachedData();
				
				DatumManager.loadDatum();
				
				if (commandBean.getFlexClientControl() != null) {
					commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
				}
				
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, status.getUserSelection().getOperationUid());

				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from UomTemplate where (isDeleted = false or isDeleted is null) and groupUid = :groupUid and defaultTemplate = true", "groupUid", operation.getGroupUid());
				if (lstResult.size() > 0) {
					UomTemplate uomTemplate = (UomTemplate) lstResult.get(0);
					operation.setUomTemplateUid(uomTemplate.getUomTemplateUid());
				    ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(operation);
				}
				
				for (STTJobConfig jobConfig : jobConfigs) {
					if (jobConfig.getActionCallbacks() != null) {
						for (STTActionCallback actionCallback : jobConfig.getActionCallbacks()) {
							if (actionCallback != null) {
								actionCallback.afterRefreshFromTownCompleted(status.getUserSelection());
							}
						}
					}
				}
			}
		} catch (Exception e) {
			status.log("Caught exception:\n" + SystemExceptionHandler.printAsString(e));
			sysLog.setStatus(STTStatus.FAIL);
			commandBean.getSystemMessage().addError("Refresh from town failed:\n" + e.getMessage());
		} finally {
			// logging
			sysLog.setGroupUid(groupUid);
			sysLog.setOperationUid(operationUid);
			sysLog.setDailyUid(null);
			sysLog.setJobconfigs(STTJobConfig.jobConfigsToString(jobConfigs));
			sysLog.setTransactionDatetime(new Date());
			sysLog.setTransactionType(STTTransactionType.REFRESH_FROM_TOWN);
			sysLog.setDataDirection(STTDataDirection.INCOMING);
			sysLog.setIsScheduled(false);
			sysLog.setRemoteAddr(comConfig.getDestinationUrl());
			sysLog.setTransactionLog(status.getTransactionLog().toString());
			sysLog.setUserName(userSession.getUserName());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(sysLog);
			
			status.dispose();
		}
	}
	
	@Override
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection,	HttpServletRequest request) throws Exception {

	}
}
