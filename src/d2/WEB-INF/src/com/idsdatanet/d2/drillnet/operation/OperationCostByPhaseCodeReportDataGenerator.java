package com.idsdatanet.d2.drillnet.operation;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.CostCurrencyLookup;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class OperationCostByPhaseCodeReportDataGenerator implements ReportDataGenerator{

	public void disposeOnDataGeneratorExit() {		
		
	}

	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String currentDailyUid = userContext.getUserSelection().getDailyUid();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		if (currentDailyUid == null || currentOperationUid == null) return;

		//String phaseCode = "N/A";
		//String jobTypeCode = "N/A";
		//double thisDuration = 0.0;		
		//double thisCost = 0.0;
		//double totalDuration = 0.0;
		
		Daily currentDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (currentDaily==null) return;
		Date todayDate = currentDaily.getDayDate();
		
		if (reportType==null){
			reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(currentOperationUid);
		}
		
		//get conversion rates
		Map<String, Double> conversionRates = new HashMap<String, Double>();
		List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CostCurrencyLookup c, OperationAfe a " +
				"where (c.isDeleted = false or c.isDeleted is null) " +
				"and (a.isDeleted = false or a.isDeleted is null) " +
				"and c.costAfeMasterUid=a.costAfeMasterUid " +
				"and a.operationUid=:operationUid " +
				"order by c.isPrimaryCurrency desc", "operationUid", currentOperationUid);
		//List<CostCurrencyLookup> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CostCurrencyLookup where (isDeleted = false or isDeleted is null) and operationUid=:operationUid order by isPrimaryCurrency desc", "operationUid", currentOperationUid);
		for (Object[] rec : rs ) {
			CostCurrencyLookup costCurrencyLookup = (CostCurrencyLookup) rec[0]; 
			if (costCurrencyLookup.getCurrencySymbol()!=null && costCurrencyLookup.getConversionRate()!=null) {
				if (costCurrencyLookup.getConversionRate()>0.0) {
					conversionRates.put(costCurrencyLookup.getCurrencySymbol(), costCurrencyLookup.getConversionRate());
				}
			}
		}
		
		Map<String, Double> costByPhase = new HashMap<String, Double>();
		String strSql = "FROM Daily WHERE (isDeleted=false or isDeleted is null) AND dayDate<=:dayDate AND operationUid=:operationUid ORDER BY dayDate";
		List<Daily> dailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"dayDate","operationUid"}, new Object[]{todayDate, currentOperationUid}, qp);
		for (Daily daily : dailyList) {
			String dailyUid = daily.getDailyUid();
			Boolean hasCostData = false;
			Double unassignedCost = 0.0;
			strSql = "SELECT phaseCode, currency, sum(coalesce(itemCost,0.0) * coalesce(quantity,0.0)) FROM CostDailysheet " +
					"WHERE (isDeleted=false or isDeleted is null) " +
					"AND dailyUid=:dailyUid " +
					"GROUP BY phaseCode, currency";
			List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyUid, qp);
			for (Object[] rec : lstResult) {
				hasCostData = true;
				if (rec[2]==null) continue;
				Double thisTotal = Double.parseDouble(rec[2].toString());
				String phaseCode = (rec[0]==null?"":rec[0].toString());
				//convert total into primary currency
				if (rec[1]!=null) {
					if (conversionRates.containsKey(rec[1].toString())) {
						Double conversionRate = conversionRates.get(rec[1].toString());
						thisTotal = thisTotal / conversionRate;
					}
				}
				//put data into map
				if (StringUtils.isBlank(phaseCode)) {
					unassignedCost += thisTotal;
				} else {
					strSql = "FROM LookupPhaseCode WHERE (isDeleted is false or isDeleted is null) AND shortCode=:phaseCode and operationCode=:operationCode";
					List<LookupPhaseCode> phaseList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"phaseCode", "operationCode"}, new Object[]{phaseCode, userContext.getUserSelection().getOperationType()}, qp);
					if (phaseList.size()>0) {
						LookupPhaseCode lookupPhaseCode = phaseList.get(0);
						String jobTypeCode = (lookupPhaseCode.getJobTypeShortCode()==null?"":lookupPhaseCode.getJobTypeShortCode());
						String key = jobTypeCode + "::" + phaseCode;
						if (costByPhase.containsKey(key)) {
							Double phaseTotal = costByPhase.get(key) + thisTotal;
							costByPhase.put(key, phaseTotal);
						} else {
							costByPhase.put(key, thisTotal);
						}
					} else {
						unassignedCost += thisTotal;
					}
				}
			}
			if (!hasCostData) {
				//use getReportTypePriorityWhenDrllgNotExist to get type of operation
				
				if (reportType.equalsIgnoreCase("MGT")) {
				  reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(currentOperationUid);
				}
				
				strSql = "FROM ReportDaily WHERE (isDeleted=false or isDeleted is null) and dailyUid=:dailyUid and reportType=:reportType";
				List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"dailyUid", "reportType"}, new Object[]{dailyUid, reportType}, qp);
				if (reportDailyList.size()>0) {
					ReportDaily reportDaily = reportDailyList.get(0);
					if (reportDaily.getDaycost()!=null) {
						unassignedCost = reportDaily.getDaycost();
					}
				}
			}
			
			strSql = "SELECT jobTypeCode, phaseCode, sum(activityDuration) FROM Activity " +
					"WHERE (isDeleted=false or isDeleted is null) " +
					"AND (isSimop=false or isSimop is null) " +
					"AND (isOffline=false or isOffline is null) " +
					"AND (dayPlus=0 or dayPlus is null) " +
					"AND dailyUid=:dailyUid " +
					"GROUP BY jobTypeCode, phaseCode";
			Double totalDuration = 0.0;
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyUid, qp);
			for (Object[] rec : lstResult) {
				if (rec[2]!=null) {
					totalDuration += Double.parseDouble(rec[2].toString());
				}
			}
			if (totalDuration>0.0) {
				for (Object[] rec : lstResult) {
					if (rec[2]!=null) {
						String jobTypeCode = (rec[0]==null?"":rec[0].toString());
						String phaseCode = (rec[1]==null?"":rec[1].toString());
						Double thisDuration = Double.parseDouble(rec[2].toString());
						String key = jobTypeCode + "::" + phaseCode;
						Double thisTotal = unassignedCost * thisDuration / totalDuration;
						if (costByPhase.containsKey(key)) {
							Double phaseTotal = costByPhase.get(key) + thisTotal;
							costByPhase.put(key, phaseTotal);
						} else {
							costByPhase.put(key, thisTotal);
						}
					}
				}
			}
		}
		for (Map.Entry<String, Double> entry : costByPhase.entrySet()) {
			String key = entry.getKey();
			String[] codes = key.split("::");
			if (codes.length>1) {
				ReportDataNode thisReportNode = reportDataNode.addChild("OperationCostByPhaseCode");
				thisReportNode.addProperty("operationUid", currentOperationUid);
				thisReportNode.addProperty("dailyUid", currentDailyUid);
				thisReportNode.addProperty("jobTypeCode", codes[0]);
				thisReportNode.addProperty("phaseCode", codes[1]);	
				thisReportNode.addProperty("cost", entry.getValue() + "");
			}
		}
	}
}