package com.idsdatanet.d2.drillnet.bharun;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.BhaComponent;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LookupBhaComponent;
import com.idsdatanet.d2.core.model.LookupBhaComponentCatalog;
import com.idsdatanet.d2.core.model.LookupBhaComponentCatalogDetails;
import com.idsdatanet.d2.core.model.LookupBhaComponentSection;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.UOMManager;
import com.idsdatanet.d2.core.uom.mapping.UOMMapping;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class BharunCommandBeanListener extends EmptyCommandBeanListener {
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		String action = request.getParameter("_action");
		
		if ("checkBharunNumberExist".equals(invocationKey)) {
			String name = request.getParameter("name");
			String bharunUid = request.getParameter("bharunUid");
			boolean isNewRecord = request.getParameter("isNewRecord").equals("1")? true : false;
			boolean isDuplicated = false;

			UserSession session = UserSession.getInstance(request);
			String operationUid = session.getCurrentOperationUid();
	
			String strResult = "<bharunNumberExist>";
			isDuplicated = CommonUtil.getConfiguredInstance().isBhaRunNumberDuplicate(name, operationUid, isNewRecord, bharunUid);		
			strResult = strResult + "<item name=\"duplicate\" value=\"" + isDuplicated + "\" /></bharunNumberExist>";
			response.getWriter().write(strResult);
		}
		
		if ("checkBitrunNumberExist".equals(invocationKey)) {
			String name = request.getParameter("name");
			String bitrunUid = request.getParameter("bitrunUid");
			boolean isNewRecord = request.getParameter("isNewRecord").equals("1")? true : false;
			boolean isDuplicated = false;

			UserSession session = UserSession.getInstance(request);
			String operationUid = session.getCurrentOperationUid();
	
			String strResult = "<bitrunNumberExist>";
			isDuplicated = CommonUtil.getConfiguredInstance().isBitrunNumberDuplicate(name, operationUid, isNewRecord, bitrunUid);		
			strResult = strResult + "<item name=\"duplicate\" value=\"" + isDuplicated + "\" /></bitrunNumberExist>";
			response.getWriter().write(strResult);
		}
		
		if ("flexBhaRunNumber".equals(invocationKey)) {
			String strResult = "<bhaRunNumber>";
			UserSession session = UserSession.getInstance(request);
			String operationUid = session.getCurrentOperationUid();
			
			List list = new ArrayList();
			if (action.equals("continue_bha_from_previous_run")) {
				if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "allowCopyBHAWhenGotDateOut"))){
					list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select bharunUid, bhaRunNumber from Bharun where (isDeleted = false or isDeleted is null) and operationUid = :operationUid order by bhaRunNumber", "operationUid", operationUid);
				}else {
					String strCondition = BharunUtils.continueBharunNumberCondition(operationUid, session.getCurrentDailyUid());
					if (StringUtils.isNotBlank(strCondition)) {
						list = ApplicationUtils.getConfiguredInstance().getDaoManager().find("select bharunUid, bhaRunNumber from Bharun where bharunUid IN (" + strCondition +  ") order by bhaRunNumber");
					}
				}
				//list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Bharun where (isDeleted = false or isDeleted is null) and operationUid = :operationUid order by bhaRunNumber", "operationUid", operationUid);
				if (!list.isEmpty()){
					Collections.sort(list, new BharunNumberComparator());
					for (Object obj : list){
						Object[] _obj = (Object[]) obj;
						String bhaRunUid = (String) _obj[0];
						String bhaRunNumber = (String) _obj[1];
						
						strResult = strResult + "<item key=\"" + bhaRunUid + "\" value=\"" +  StringEscapeUtils.escapeXml(bhaRunNumber) + "\"/>";
					}
				}
			} else if (action.equals("continue_component_from_previous_run")) {
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select bhaRun.bharunUid, bhaRun.bhaRunNumber from Bharun bhaRun, BhaComponent bhaComponent where (bhaRun.isDeleted = false or bhaRun.isDeleted is null) and (bhaComponent.isDeleted = false or bhaComponent.isDeleted is null) and bhaRun.bharunUid = bhaComponent.bharunUid and bhaRun.operationUid = :operationUid group by bhaRun.bharunUid, bhaRun.bhaRunNumber", "operationUid", operationUid);
				if (!list.isEmpty()){		
					Collections.sort(list, new BharunNumberComparator());
					for (Object obj : list) {
						Object[] _obj = (Object[]) obj;
						String bhaRunUid = (String) _obj[0];
						String bhaRunNumber = (String) _obj[1];
						
						strResult = strResult + "<item key=\"" + bhaRunUid + "\" value=\"" +  StringEscapeUtils.escapeXml(bhaRunNumber) + "\"/>";
					}
				}
			} else if (action.equals("continue_bit_run_from_previous_run")) {
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select bhaRun.bharunUid, bhaRun.bhaRunNumber from Bharun bhaRun, Bitrun bitrun where (bhaRun.isDeleted = false or bhaRun.isDeleted is null) and bhaRun.bharunUid = bitrun.bharunUid and bhaRun.operationUid = :operationUid group by bhaRun.bharunUid, bhaRun.bhaRunNumber order by bhaRun.bhaRunNumber", "operationUid", operationUid);
				if (!list.isEmpty()){
					Collections.sort(list, new BharunNumberComparator());
					for (Object obj : list) {
						Object[] _obj = (Object[]) obj;
						String bhaRunUid = (String) _obj[0];
						String bhaRunNumber = (String) _obj[1];
						strResult = strResult + "<item key=\"" + bhaRunUid + "\" value=\"" +  StringEscapeUtils.escapeXml(bhaRunNumber) + "\"/>";
					}
				}
			}
			strResult = strResult + "</bhaRunNumber>";
			response.getWriter().write(strResult);
		}
		
		if ("getServiceLogList".equals(invocationKey)) {
			String serialNum = request.getParameter("serialNum");
			String currBharunUid = request.getParameter("bharunUid");
			BhaComponentServiceLogFlexRemoteUtils a = new BhaComponentServiceLogFlexRemoteUtils();
			a.getServiceLogList(request, response, serialNum, currBharunUid);
		}
		
		if ("saveNewServiceLog".equals(invocationKey)) {
			String serialNum = request.getParameter("serialNum");
			String descr = request.getParameter("descr");
			String dtChooser = request.getParameter("dtChooser");
			String chkBox = request.getParameter("chkBox");
			Boolean isReset = ("1".equals(chkBox))?true:false;
			String bharunUid = request.getParameter("bharunUid");
			String bhaComponentUid = request.getParameter("bhaComponentUid");
			String wellUid = request.getParameter("wellUid");
			String wellboreUid = request.getParameter("wellboreUid");
			BhaComponentServiceLogFlexRemoteUtils a = new BhaComponentServiceLogFlexRemoteUtils();
			a.saveNewServiceLog(request, response, serialNum, descr, dtChooser, isReset, bharunUid, bhaComponentUid, wellUid, wellboreUid);
		}
		
		if ("delServiceLog".equals(invocationKey)) {
			String inventoryServiceLogUid = request.getParameter("inventoryServiceLogUid");
			BhaComponentServiceLogFlexRemoteUtils a = new BhaComponentServiceLogFlexRemoteUtils();
			a.delServiceLog(request, response, inventoryServiceLogUid);
		}
		
		if ("getDailyHoursUsedLogList".equals(invocationKey)) {
			String bhaComponentUid = request.getParameter("bhaComponentUid");
			String bharunUid = request.getParameter("bharunUid");
			BhaComponentDailyHoursUsedLogFlexRemoteUtils a = new BhaComponentDailyHoursUsedLogFlexRemoteUtils();
			a.getDailyHoursUsedLogList(request, response, bhaComponentUid, bharunUid);
		}
		
		if ("saveNewDailyHoursUsedLog".equals(invocationKey)) {
			String descr = request.getParameter("descr");
			String txtHours = request.getParameter("txtHours");
			String cbDayDate = request.getParameter("cbDayDate");
			String bharunUid = request.getParameter("bharunUid");
			String bhaComponentUid = request.getParameter("bhaComponentUid");
			String wellUid = request.getParameter("wellUid");
			String wellboreUid = request.getParameter("wellboreUid");
			BhaComponentDailyHoursUsedLogFlexRemoteUtils a = new BhaComponentDailyHoursUsedLogFlexRemoteUtils();
			a.saveNewDailyHoursUsedLog(request, response, descr, txtHours, cbDayDate, bharunUid, bhaComponentUid, wellUid, wellboreUid);
		}
		
		if ("delDailyHoursUsedLog".equals(invocationKey)) {
			String bhaComponentHoursUsedUid = request.getParameter("bhaComponentHoursUsedUid");
			String bharunUid = request.getParameter("bharunUid");
			String bhaComponentUid = request.getParameter("bhaComponentUid");
			BhaComponentDailyHoursUsedLogFlexRemoteUtils a = new BhaComponentDailyHoursUsedLogFlexRemoteUtils();
			a.delDailyHoursUsedLog(request, response, bhaComponentHoursUsedUid, bharunUid, bhaComponentUid);
		}
		
		if ("loadCatalogDetails".equals(invocationKey)) {
			DecimalFormat uomFormatter = (DecimalFormat) DecimalFormat.getInstance(commandBean.getUserLocale());
			String uid = request.getParameter("lookupBhaComponentUid");
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("response");
			if (StringUtils.isNotBlank(uid)) {
				Map<String, UOMMapping> uomMappings = new HashMap<String, UOMMapping>();
				Map<String, CustomFieldUom> customFieldUOMs = new HashMap<String, CustomFieldUom>();
				LookupBhaComponent lookupBhaComponent = (LookupBhaComponent)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupBhaComponent.class, uid);
				if (lookupBhaComponent != null) {
					LookupBhaComponentSection componentSection = (LookupBhaComponentSection)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupBhaComponentSection.class, lookupBhaComponent.getLookupBhaComponentSectionUid());
					String hql = "from LookupBhaComponentCatalog where sectionType=:sectionType order by name";
					List<LookupBhaComponentCatalog> catalogs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "sectionType", componentSection.getSectionType());
					if (catalogs != null && catalogs.size() > 0) {
						String[] catalogParameters = componentSection.getCatalogParameters().split(",");
						writer.startElement("catalogs");
						for (LookupBhaComponentCatalog catalog : catalogs) {
							writer.startElement("catalog");
							writer.addElement("name", catalog.getName());
							hql = "from LookupBhaComponentCatalogDetails where lookupBhaComponentCatalogUid=:lookupBhaComponentCatalogUid";
							List<LookupBhaComponentCatalogDetails> catalogDetails = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "lookupBhaComponentCatalogUid", catalog.getLookupBhaComponentCatalogUid(), QueryProperties.disableUOMAndDatumConversion());
							if (catalogDetails != null && catalogDetails.size() > 0) {
								for (LookupBhaComponentCatalogDetails detail : catalogDetails) {
									writer.startElement("detail");
									for (String param : catalogParameters) {
										SimpleAttributes attr = new SimpleAttributes();
										String propertyName = param.trim();
										String strValue = "";
										Object value = PropertyUtils.getProperty(detail, propertyName);
										if (value != null) {
											UOMMapping uom = uomMappings.get(propertyName);
											if (uom == null) {
												uom = UOMManager.getCurrentClassPropertyMapping(BhaComponent.class, propertyName, false);
												uomMappings.put(propertyName, uom);
											}
											if (uom != null) {
												CustomFieldUom uomField = customFieldUOMs.get(propertyName);
												if (uomField == null) {
													uomField = new CustomFieldUom(commandBean, BhaComponent.class, propertyName, (Double)value);
												}
												attr.addAttribute("uom", uom.getUnitSymbol());
												uomFormatter.setMaximumFractionDigits(uom.getUnitOutputPrecision());
												uomFormatter.setMinimumFractionDigits(uom.getUnitOutputPrecision());
												strValue = CommonUtils.roundUpFormat(uomFormatter, uomField.getConvertedValue());
											} else {
												strValue = value.toString();
											}
										}
										writer.addElement(propertyName, strValue, attr);
									}
									writer.endElement();
								}
							}
							writer.endElement();
						}
						writer.endElement();
					}
				}
			}
			writer.endElement();
			writer.close();
		}
		
		return;
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		String action = request.getParameter("_action");
		String bhaRunUid = "";
		if (StringUtils.isNotBlank(action) || action!=null){
			if (action.equals("continue_component_from_previous_run")) {
				bhaRunUid = request.getParameter(action);
				loopAllNodeAndAppendComponentToNode(targetCommandBeanTreeNode, bhaRunUid);
				
				if (commandBean.getFlexClientControl() != null) {
					commandBean.getFlexClientControl().setReloadAfterPageCancel();
				}
			} else if (action.equals("continue_bit_run_from_previous_run")) {
				bhaRunUid = request.getParameter(action);
				loopAllNodeAndAppendBitrunToNode(targetCommandBeanTreeNode, bhaRunUid);
			}
		}
		commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
		
		if(targetCommandBeanTreeNode!=null){ 
			targetCommandBeanTreeNode.getNodeUserMessages().clearFieldMessages("bhaRunNumber");
			if ("isReRun".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				Boolean isReRun = (Boolean) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "isReRun");
				if (isReRun!=null && isReRun){
					targetCommandBeanTreeNode.getNodeUserMessages().addFieldWarning("bhaRunNumber", "Please include 'RR' with re-run number at the end of the Run #. For instance, '1RR1'.");
				}
			}
		}
	}
	
	private void loopAllNodeAndAppendComponentToNode(CommandBeanTreeNode targetNode, String bhaRunUid) throws Exception {
		if(targetNode.getDataDefinition().getTableClass().equals(Bharun.class)){
			List<BhaComponent> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from BhaComponent where bharunUid = :bhaRunUid and (isDeleted = false or isDeleted is null) order by sequence" , "bhaRunUid", bhaRunUid);
			if (list != null && list.size() > 0) {
				for (BhaComponent bhaComponent : list) {
					setPrimaryKeyToNull(BhaComponent.class, bhaComponent);
					bhaComponent.setTotalHoursUsed(null); // unset the value as Total Hours Used is only for sum of hours for each separate run. Ticket: 24129 
					targetNode.addCustomNewChildNodeForInput(bhaComponent);
				}
			}
		}
	}
	
	private void loopAllNodeAndAppendBitrunToNode(CommandBeanTreeNode targetNode, String bhaRunUid) throws Exception {
		if (targetNode.getDataDefinition().getTableClass().equals(Bharun.class)) {
			List<Bitrun> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Bitrun where bharunUid = :bhaRunUid", "bhaRunUid", bhaRunUid);
			if (list != null && list.size() > 0) {
				for (Bitrun bitrun : list) {
					List<BitNozzle> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from BitNozzle where bitrunUid = :bitrunUid", "bitrunUid", bitrun.getBitrunUid());
					setPrimaryKeyToNull(Bitrun.class, bitrun);
					CommandBeanTreeNode bitrunNode = targetNode.addCustomNewChildNodeForInput(bitrun);
					if (list2 != null && list2.size() > 0) {
						for (BitNozzle bitNozzle : list2) {
							setPrimaryKeyToNull(BitNozzle.class, bitNozzle);
							bitrunNode.addCustomNewChildNodeForInput(bitNozzle);
						}
					}
				}
			}
		}
	}
	
	private void setPrimaryKeyToNull(Class clazz, Object object) throws Exception {
		String primaryKey = ApplicationUtils.getConfiguredInstance().getIdentifierPropertyName(clazz);
		PropertyUtils.setSimpleProperty(object, primaryKey, null);
	}
	
	private class BharunNumberComparator implements Comparator<Object[]> {
		public int compare(Object[] o1, Object[] o2) {
			try {
				Object in1 = o1[1];
				Object in2 = o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(in1.toString());
				String f2 = WellNameUtil.getPaddedStr(in2.toString());
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			} catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
    	String operationUid="";
    	String dailyUid="";
		Boolean enabled = false;	
		Date Currentdate=null;
		if (userSelection.getOperationUid()!=null) {
			operationUid = userSelection.getOperationUid();
		}
		if (userSelection.getDailyUid()!=null) {
			dailyUid = userSelection.getDailyUid();
			String strSql2 = "FROM Daily WHERE (isDeleted = false or isDeleted is null) and operationUid =:operationUid and dailyUid=:dailyUid";
			String[] paramsFields2 = {"operationUid","dailyUid"};
			Object[] paramsValues2 = {operationUid,dailyUid};
			List <Daily> lsDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
			
			if (lsDaily.size() > 0) {
				for (Daily objResult2: lsDaily) {
					Currentdate = objResult2.getDayDate();
				}
			}
		}
		
		Daily yesterday = ApplicationUtils.getConfiguredInstance().getYesterday(operationUid, dailyUid);
		
		if (yesterday!=null) {
			String strSql = "Select bharun.dailyidOut,bharun.dailyidIn FROM Bharun bharun, BharunDailySummary bharunsummary, Daily d WHERE (bharun.isDeleted = false or bharun.isDeleted is null) and (bharunsummary.isDeleted = false or bharunsummary.isDeleted is null) and bharun.bharunUid=bharunsummary.bharunUid and bharun.operationUid =:operationUid and bharunsummary.dailyUid=d.dailyUid and d.dayDate < '" + Currentdate + "'";
			List <Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			
			if (lstResult.size() > 0) {
				for (Object[] objBha: lstResult) {
					String dailyidOut = (String) objBha[0];
					String dailyidIn = (String) objBha[1];
					String strSql1 = "FROM Daily WHERE (isDeleted = false or isDeleted is null) and operationUid =:operationUid and dailyUid=:dailyUid";
					String[] paramsFields = {"operationUid","dailyUid"};
					Object[] paramsValues = {operationUid,dailyidOut};
					List <Daily> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields, paramsValues);
					
					String strSql2 = "FROM Daily WHERE (isDeleted = false or isDeleted is null) and operationUid =:operationUid and dailyUid=:dailyUid";
					String[] paramsFields2 = {"operationUid","dailyUid"};
					Object[] paramsValues2 = {operationUid,dailyidIn};
					List <Daily> ls2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
					if (ls.size() > 0) {
						for (Daily objResult1: ls) {
							if (Currentdate!=null) {
								if (objResult1.getDayDate().compareTo(Currentdate) > 0 || objResult1.getDayDate() == null) {
									if (ls2.size() > 0) {
										for(Daily objResult2: ls2) {
											if (objResult2.getDayDate().compareTo(Currentdate) <= 0 || objResult2.getDayDate() == null) {
												enabled = true;
											}
										}
									} else {
										enabled = true;
									}
								}
							}
						}
					} else {
						if (ls2.size() > 0) {
							for (Daily objResult2: ls2) {
								if (Currentdate!=null) {
									if (objResult2.getDayDate().compareTo(Currentdate) <= 0 || objResult2.getDayDate() == null) {
										enabled = true;
									}
								}
							}
						} else {
							enabled = true;
						}
					}
				}
			}
		}
		commandBean.getRoot().getDynaAttr().put("hideContinueButton", enabled);
	}
}
