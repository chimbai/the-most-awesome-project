package com.idsdatanet.d2.drillnet.reportDaily;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.stt.STTTownSideCallback;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ReportDailySTTCallback implements STTTownSideCallback {
	
	public void afterSendToTownCompleted(UserSelectionSnapshot userSelection) {
		try {
			CommonUtil.getConfiguredInstance().updateDailyCostFromCostNet(userSelection.getDailyUid());
			CommonUtil.getConfiguredInstance().updateDailyIsDeleted(userSelection.getOperationUid());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
