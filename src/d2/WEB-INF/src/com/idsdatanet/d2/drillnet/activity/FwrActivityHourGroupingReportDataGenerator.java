package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class FwrActivityHourGroupingReportDataGenerator implements ReportDataGenerator {
	private Map<String, String> taskCodeList = null;
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		
		Map ad = userContext.getUserSelection().getCustomProperties();
		if(ad  != null && ad.size() > 0){
			this.taskCodeList = new HashMap();
			
			Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
			this.taskCodeList = this.getTaskCodeList(operation);
		}
		
		if(ad != null ){
			if(ad.containsKey("TimeAnalysis")){
				this.generateTimeAnalysisHourGrouping(userContext, reportDataNode, (String) ad.get("TimeAnalysis"));
			}
		
			if(ad.containsKey("TroubleTimeAnalysis")){
				this.generateTroubledTimeAnalysisGrouping(userContext, reportDataNode, (String) ad.get("TroubleTimeAnalysis"), false);
			}
			
			if(ad.containsKey("UnprogTimeAnalysis")){
				this.generateTroubledTimeAnalysisGrouping(userContext, reportDataNode, (String) ad.get("UnprogTimeAnalysis"), true);
			}
		}
		
	}

	public void generateTimeAnalysisHourGrouping(UserContext userContext, ReportDataNode reportDataNode, String minVal) throws Exception{
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		
		Double taskCodeDuration;
		Double totalMiscDuration = 0.0;
		String shortCode;
		String shortCodeName;
		int miscTaskCount = 0;
		Double minHrs = Double.parseDouble(minVal);
		minHrs = minHrs * 3600;
						
		//Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		
		//Map<String, String> taskCodeList = this.getTaskCodeList(operation);
		
		String strSql = "SELECT sum(a.activityDuration) AS totalDuration, a.taskCode FROM Activity a WHERE a.operationUid = :thisOperationUid AND (a.isDeleted = false OR a.isDeleted is null) AND (a.carriedForwardActivityUid = '' OR a.carriedForwardActivityUid IS NULL) AND (a.isSimop = '' OR a.isSimop IS NULL) AND (a.isOffline = '' OR a.isOffline IS NULL) AND a.dailyUid IN (SELECT dailyUid FROM Daily d WHERE d.operationUid = :thisOperationUid AND (d.isDeleted='' OR d.isDeleted IS NULL)) GROUP BY a.taskCode";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){			
			
			for(int i = 0; i < lstResult.size(); i++ ){
				Object[] activityRecord = (Object[]) lstResult.get(i);
				shortCode = "";
				shortCodeName = "";
				taskCodeDuration = 0.0;
				
				if (activityRecord[0] != null && StringUtils.isNotBlank(activityRecord[0].toString())){
					taskCodeDuration = Double.parseDouble(activityRecord[0].toString());
				}
				if (activityRecord[1] != null && StringUtils.isNotBlank(activityRecord[1].toString())) shortCode = activityRecord[1].toString();
				//if (activityRecord[2] != null && StringUtils.isNotBlank(activityRecord[2].toString())) shortCodeName = activityRecord[2].toString();
				if (this.taskCodeList.containsKey(shortCode)) shortCodeName = this.taskCodeList.get(shortCode);
				
				if(taskCodeDuration <= minHrs){
					ReportDataNode thisReportNode = reportDataNode.addChild("ActivityTimeAnalysisGroupingTimeWrap");
					taskCodeDuration = taskCodeDuration/3600;
					thisReportNode.addProperty("activityDuration", taskCodeDuration.toString());
					thisReportNode.addProperty("shortCode", shortCode);
					thisReportNode.addProperty("shortCodeName", shortCodeName);
					totalMiscDuration += taskCodeDuration;
					miscTaskCount++;					
				}
				else{
					//Convert to hours
					taskCodeDuration = taskCodeDuration/3600;
					ReportDataNode thisReportNode = reportDataNode.addChild("ActivityTimeAnalysisGrouping");
					thisReportNode.addProperty("activityDuration", taskCodeDuration.toString());
					thisReportNode.addProperty("shortCode", shortCode);
					thisReportNode.addProperty("shortCodeName", shortCodeName);
				}	
			}
			if(miscTaskCount > 0){
				String label = miscTaskCount + " events < " + minVal + "(hrs)";
				ReportDataNode thisReportNode = reportDataNode.addChild("ActivityTimeAnalysisGrouping");
				thisReportNode.addProperty("activityDuration", totalMiscDuration.toString());
				thisReportNode.addProperty("shortCode", "MISC");
				thisReportNode.addProperty("shortCodeName", label);
			}
		}
	}
	
	public void generateTroubledTimeAnalysisGrouping(UserContext userContext, ReportDataNode reportDataNode, String minVal, Boolean unprog) throws Exception{
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double taskCodeDuration;
		Double totalMiscDuration = 0.0;
		String shortCode;
		String shortCodeName;
		int miscTaskCount = 0;
		Double minHrs = Double.parseDouble(minVal);
		minHrs = minHrs * 3600;
		
		//Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		
		//Map<String, String> taskCodeList = this.getTaskCodeList(operation);	
		
		String queryCond = "";
		String reportDataNodeName = "";
		if (unprog){
			queryCond = "('P','TP')";
			reportDataNodeName = "ActivityUnprogTimeAnalysisGrouping";
		}else {
			queryCond = "('P','U')";
			reportDataNodeName = "ActivityTroubleTimeAnalysisGrouping";
		}
		
		String strSql = "SELECT sum(a.activityDuration) AS totalDuration, a.taskCode FROM Activity a WHERE a.operationUid = :thisOperationUid AND a.internalClassCode NOT IN " + queryCond + " AND (a.isDeleted = false or a.isDeleted is null) AND (a.carriedForwardActivityUid = '' OR a.carriedForwardActivityUid IS NULL) AND (a.isSimop = '' OR a.isSimop IS NULL) AND (a.isOffline = '' OR a.isOffline IS NULL) AND a.dailyUid IN (SELECT dailyUid FROM Daily d WHERE d.operationUid = :thisOperationUid AND (d.isDeleted='' OR d.isDeleted IS NULL)) GROUP BY a.taskCode";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
				
			for(int i = 0; i < lstResult.size(); i++ ){
				Object[] activityRecord = (Object[]) lstResult.get(i);
				shortCode = "";
				shortCodeName = "";
				taskCodeDuration = 0.0;
				
				if (activityRecord[0] != null && StringUtils.isNotBlank(activityRecord[0].toString())){
					taskCodeDuration = Double.parseDouble(activityRecord[0].toString());
				}
				if (activityRecord[1] != null && StringUtils.isNotBlank(activityRecord[1].toString())) shortCode = activityRecord[1].toString();
				if(this.taskCodeList.containsKey(shortCode)) shortCodeName = this.taskCodeList.get(shortCode);
				//if (activityRecord[2] != null && StringUtils.isNotBlank(activityRecord[2].toString())) shortCodeName = activityRecord[2].toString();
					
				if(taskCodeDuration <= minHrs){
					taskCodeDuration = taskCodeDuration/3600;
					ReportDataNode thisReportNode = reportDataNode.addChild("ActivityTroubleTimeAnalysisGroupingTimeWrap");
					thisReportNode.addProperty("activityDuration", taskCodeDuration.toString());
					thisReportNode.addProperty("shortCode", shortCode);
					thisReportNode.addProperty("shortCodeName", shortCodeName);
					totalMiscDuration += taskCodeDuration;
					miscTaskCount++;					
				}
				else{
					//Convert to hours
					taskCodeDuration = taskCodeDuration/3600;
					ReportDataNode thisReportNode = reportDataNode.addChild(reportDataNodeName);
					thisReportNode.addProperty("activityDuration", taskCodeDuration.toString());
					thisReportNode.addProperty("shortCode", shortCode);
					thisReportNode.addProperty("shortCodeName", shortCodeName);
				}	
			}
			if(miscTaskCount > 0){
				String label = miscTaskCount + " events < " + minVal + "(hrs)";
				ReportDataNode thisReportNode = reportDataNode.addChild(reportDataNodeName);
				thisReportNode.addProperty("activityDuration", totalMiscDuration.toString());
				thisReportNode.addProperty("shortCode", "MISC");
				thisReportNode.addProperty("shortCodeName", label);
			}
		
		}
	}
	
	public Map<String, String> getTaskCodeList(Operation operation) throws Exception{
		
		String operationalUnit = "";
		String operationalCode = "";
		String operationCode = "";
		String conditions = null;
		
		if(StringUtils.isNotBlank(operation.getOperationalCode())) operationalCode = "%" + operation.getOperationalCode() + "%";
		if(StringUtils.isNotBlank(operation.getOperationalUnit())) operationalUnit = operation.getOperationalUnit();
		if(StringUtils.isNotBlank(operation.getOperationCode())) operationCode = operation.getOperationCode();
		
		conditions = "ltc.operationalUnit = :operationalUnit AND (ltc.operationalCode like :operationalCode OR ltc.operationalCode = '' OR ltc.operationalCode is null)";
			
		if(StringUtils.isBlank(operationalCode) && StringUtils.isBlank(operationalUnit)) {
			conditions = "(ltc.operationalUnit = :operationalUnit OR ltc.operationalUnit is null) AND (ltc.operationalCode like :operationalCode OR ltc.operationalCode is null)";
		} 
		else if(operationalCode == null || StringUtils.isBlank(operationalCode)) {
			conditions = "ltc.operationalUnit = :operationalUnit AND (ltc.operationalCode like :operationalCode OR ltc.operationalCode is null)";
		}	
		
		List<Object[]> listTaskCodeResults = new ArrayList<Object[]>();
		Map<String, String> taskCodeList = new HashMap();
		
		String[] paramsFields = {"operationCode", "operationalUnit", "operationalCode"};		
 		Object[] paramsValues = {operationCode, operationalUnit, operationalCode};
		
		String strSql = "SELECT ltc.shortCode, ltc.name FROM LookupTaskCode ltc WHERE (ltc.isDeleted = false or ltc.isDeleted is null) AND " +
						conditions + " AND (ltc.operationCode = :operationCode or ltc.operationCode='') ORDER BY name";
		listTaskCodeResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (listTaskCodeResults.size() > 0) {
			for (Object[] listTaskCodeResult : listTaskCodeResults) {
				if (listTaskCodeResult[0] != null && listTaskCodeResult[1] != null) {
					
					String key = listTaskCodeResult[0].toString();
					String value = listTaskCodeResult[1].toString() + " (" + listTaskCodeResult[0].toString() + ")";
							
					taskCodeList.put(key, value);
					
				}
			}
		}
		
		return taskCodeList;
	}
	
}
