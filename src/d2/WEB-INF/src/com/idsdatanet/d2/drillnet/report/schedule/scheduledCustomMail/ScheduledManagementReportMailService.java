package com.idsdatanet.d2.drillnet.report.schedule.scheduledCustomMail;



import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.util.Map.Entry;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CostAfeMaster;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.costnet.CostNetConstants;
import com.idsdatanet.d2.drillnet.report.ManagementReportModule;
import com.idsdatanet.d2.drillnet.report.ReportAutoEmail;
import com.idsdatanet.d2.drillnet.report.ReportJobParams;
import com.idsdatanet.d2.drillnet.report.ReportJobResult;
import com.idsdatanet.d2.drillnet.report.schedule.EmailConfiguration;
import com.idsdatanet.d2.drillnet.report.schedule.corportatedrillingportal.BaseDataTransferService;

public class ScheduledManagementReportMailService extends BaseDataTransferService{
	
	
	private List<String> includeSpecificOperationType = null;
	private ManagementReportModule managementReportModule;
	private boolean attachReport = false;
	
	public static final int SUCCESS=1;
	public static final int ERROR=-1;
	
	private static final ReportAutoEmail ReportAutoEmail = null;
	private String reportDateFormat="dd MMM yyyy";
	
	
	private boolean isRunning=false;
	private boolean opsTeamBased=false;
	
	public void setIncludeSpecificOperationType(
			List<String> includeSpecificOperationType) {
		this.includeSpecificOperationType = includeSpecificOperationType;
	}

	public List<String> getIncludeSpecificOperationType() {
		return includeSpecificOperationType;
	}
	
	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public boolean isRunning() {
		return isRunning;
	}
	public void setOpsTeamBased(boolean opsTeamBased) {
		this.opsTeamBased = opsTeamBased;
	}

	public boolean opsTeamBased() {
		return opsTeamBased;
	}
	public void setReportDateFormat(String reportDateFormat) {
		this.reportDateFormat = reportDateFormat;
	}

	public String getReportDateFormat() {
		return reportDateFormat;
	}
	
	public void setManagementReportModule(ManagementReportModule managementReportModule){
		this.managementReportModule = managementReportModule;
	}
	
	public ManagementReportModule getManagementReportModule(){
		return managementReportModule;
	}
	
	public void setAttachReport(boolean attachReport){
		this.attachReport = attachReport;
	}
	
	public boolean getAttachReport(){
		return attachReport;
	}
	
	public synchronized void run() throws Exception
	{
		if (this.getEnabled())
		{
			this.setRunning(true);
			this.runExtractionAndSendEmail(null);
			this.setRunning(false);
		}
	}

	public synchronized Integer runExtractionAndSendEmail(String[] emailList) throws Exception
	{
		try{
			if (this.attachReport){
				ReportJobParams jobParams = this.managementReportModule.generateManagementReportOnCurrentSystemDatetime(this.getGenerateReportUserName(), ReportAutoEmail);
				jobParams.waitOnThisObject();
				List<ReportJobResult> reportJobResult = jobParams.getReportJobResult();
				this.sendDailyEmail(reportJobResult,emailList);
				
			}else{
				List<ReportJobResult> reportJobResult = null;
				this.sendDailyEmail(reportJobResult,emailList);
			}
			return SUCCESS;
		}catch(Exception ex)
		{
			this.sendErrorEmail();
			this.getLogger().error(ex);
			return ERROR;
		}
	}
	private void sendErrorEmail() throws Exception
	{
		String[] errorReceipient={this.getSupportEmail()};
		
		EmailConfiguration emailConfig=new EmailConfiguration(this.getErrorEmailConfiguration());
		emailConfig.clearContentMapping();
		emailConfig.setEmailList(errorReceipient);
		emailConfig.setSubject(emailConfig.getSubject());
		this.sendEmail(emailConfig);
	}
	//Function to get the user email address from screen
	private String[] getEmailList(EmailConfiguration emailConfig) throws Exception
	{
		if (emailConfig.getIncludeSupportEmail()!=null && emailConfig.getIncludeSupportEmail())
		{
			return (emailConfig.getSpecificEmailDistributionKey()!=null?this.getEmailListWithSupport(emailConfig.getSpecificEmailDistributionKey()):this.getEmailListWithSupport());
		}
		return emailConfig.getSpecificEmailDistributionKey()!=null?this.getEmailList(emailConfig.getSpecificEmailDistributionKey()):this.getEmailList();
	}
	//Function for send the auto management report email with management report as attachment in the email
	private void sendDailyEmail(List<ReportJobResult> reportJobResult, String[] emailList) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		List<Daily> list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(new Date());
		EmailConfiguration emailConfig=new EmailConfiguration(this.getValidEmailConfiguration());
		emailConfig.clearContentMapping();
		Boolean operationAssignedOpsteam = false;
		Map <String, Object> emailContentItemList = new HashMap <String, Object>();
		Map<String, File> attachment = new HashMap<String,File>();
		
		String strPlanTd="";
	
		String strPlanTvd="";
		String strProgress="";
		String strDepthMdMsl="";
		String strDepthTvdMsl="";
		String strLastCsgshoeMdMsl="";
		String strLastCsgshoeTvdMsl="";
		String strLastCsgsize="";
		String strLastHolesize="";
		String strDayCost="";
		String lookup="";
		String strAfe="";
		
		for(Daily day:list)
		{
			//For getting each day data to show in the email content
			Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(day.getOperationUid());
			ReportDaily thisReportDaily = this.reportDailyList(day.getOperationUid(), reportType, day.getDailyUid());
			RigInformation rig=(RigInformation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, thisReportDaily.getRigInformationUid());
			LookupCompany lookupCompany=(LookupCompany)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, rig.getRigManager());
			CustomFieldUom thisConverter = new CustomFieldUom(ApplicationConfig.getConfiguredInstance().getDefaultReportLocaleObject());
			
			qp.setUomDatumUid(operation.getDefaultDatumUid());
			String sql = "FROM Wellbore WHERE (isDeleted = false or isDeleted is null) AND wellboreUid = :thisWellboreUid";
			List<Wellbore> thisWellbore = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "thisWellboreUid", operation.getWellboreUid(),qp);
			
			String sql1 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND reportDailyUid = :reportDailyUid";
			List<ReportDaily> reportDailyRec = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql1, "reportDailyUid", thisReportDaily.getReportDailyUid(),qp);
		
			
			if(reportDailyRec.size() > 0){
				
				Double progress = null;
				Double depthMdMsl = null;
				Double depthTvdMsl = null;
				Double lastCsgshoeMdMsl = null;
				Double lastCsgshoeTvdMsl = null;
				Double lastCsgsize = null;
				Double lastHolesize = null;
				Double dayCost = null;
				String defaultUri = "xml://casingsection.casing_size?key=code&amp;value=label";
				
				if(reportDailyRec.get(0).getProgress() != null)
				{
					progress = reportDailyRec.get(0).getProgress();
					thisConverter.setReferenceMappingField(ReportDaily.class, "progress", operation.getUomTemplateUid());
					thisConverter.setBaseValue(progress);
					progress = thisConverter.getConvertedValue();
					strProgress = thisConverter.getFormattedValue(progress, true);
				}	
				
				if(reportDailyRec.get(0).getDepthMdMsl() != null)
				{
					depthMdMsl = reportDailyRec.get(0).getDepthMdMsl();
					thisConverter.setReferenceMappingField(ReportDaily.class, "depthMdMsl", operation.getUomTemplateUid());
					thisConverter.setBaseValue(depthMdMsl);
					depthMdMsl = thisConverter.getConvertedValue();
					strDepthMdMsl = thisConverter.getFormattedValue(depthMdMsl, true);
				}
				
				if(reportDailyRec.get(0).getDepthTvdMsl() != null)
				{
					depthTvdMsl = reportDailyRec.get(0).getDepthTvdMsl();
					thisConverter.setReferenceMappingField(ReportDaily.class, "depthTvdMsl", operation.getUomTemplateUid());
					thisConverter.setBaseValue(depthTvdMsl);
					depthTvdMsl = thisConverter.getConvertedValue();
					strDepthTvdMsl = thisConverter.getFormattedValue(depthTvdMsl, true);
				}
				
				if(reportDailyRec.get(0).getLastCsgshoeMdMsl() != null)
				{
					lastCsgshoeMdMsl = reportDailyRec.get(0).getLastCsgshoeMdMsl();
					thisConverter.setReferenceMappingField(ReportDaily.class, "lastCsgshoeMdMsl", operation.getUomTemplateUid());
					thisConverter.setBaseValue(lastCsgshoeMdMsl);
					lastCsgshoeMdMsl = thisConverter.getConvertedValue();
					strLastCsgshoeMdMsl = thisConverter.getFormattedValue(lastCsgshoeMdMsl, true);
				}
				
				if(reportDailyRec.get(0).getLastCsgshoeTvdMsl() != null)
				{
					lastCsgshoeTvdMsl = reportDailyRec.get(0).getLastCsgshoeTvdMsl();
					thisConverter.setReferenceMappingField(ReportDaily.class, "lastCsgshoeTvdMsl", operation.getUomTemplateUid());
					thisConverter.setBaseValue(lastCsgshoeTvdMsl);
					lastCsgshoeTvdMsl = thisConverter.getConvertedValue();
					strLastCsgshoeTvdMsl = thisConverter.getFormattedValue(lastCsgshoeTvdMsl, true);
				}
				
				if(reportDailyRec.get(0).getLastCsgsize() != null)
				{
					lastCsgsize = reportDailyRec.get(0).getLastCsgsize();
					thisConverter.setReferenceMappingField(ReportDaily.class, "lastCsgsize", operation.getUomTemplateUid());
					thisConverter.setBaseValue(lastCsgsize);
					lastCsgsize = thisConverter.getConvertedValue();
					strLastCsgsize = thisConverter.getFormattedValue();
					String[] csgSize = strLastCsgsize.split(" ");
					strLastCsgsize = csgSize[0].toString();
					
					if (defaultUri!=null) {
						Map<String, LookupItem> lookupList = LookupManager.getConfiguredInstance().getLookup(defaultUri, null, null);
						for(String value : lookupList.keySet()){
							if(value.equalsIgnoreCase(strLastCsgsize)){
								LookupItem lookupItem = lookupList.get(value);
								lookup = lookupItem.getValue().toString();
							}
						}	
					}
				}
				
				if(reportDailyRec.get(0).getLastHolesize() != null)
				{
					lastHolesize = reportDailyRec.get(0).getLastHolesize();
					thisConverter.setReferenceMappingField(ReportDaily.class, "lastHolesize", operation.getUomTemplateUid());
					thisConverter.setBaseValue(lastHolesize);
					lastHolesize = thisConverter.getConvertedValue();
					strLastHolesize = thisConverter.getFormattedValue(lastHolesize, true);
				}
				
				if(reportDailyRec.get(0).getDaycost() != null)
				{
					dayCost = reportDailyRec.get(0).getDaycost();
					thisConverter.setReferenceMappingField(ReportDaily.class, "dayCost");
					thisConverter.setBaseValue(dayCost);
					dayCost = thisConverter.getConvertedValue();
					strDayCost = thisConverter.getFormattedValue(dayCost, false);
				}
				
			}
			
			if(thisWellbore.size() > 0){
				
				Double PlannedTdMdMsl = null;
				Double PlannedTvdMdMsl = null;
				if(thisWellbore.get(0).getPlannedTdMdMsl() != null)
				{
					PlannedTdMdMsl = thisWellbore.get(0).getPlannedTdMdMsl();
					thisConverter.setReferenceMappingField(Wellbore.class, "plannedTdMdMsl", operation.getUomTemplateUid());
					thisConverter.setBaseValue(PlannedTdMdMsl);
					PlannedTdMdMsl = thisConverter.getConvertedValue();
					strPlanTd = thisConverter.getFormattedValue(PlannedTdMdMsl, true);
				}				
				if(thisWellbore.get(0).getPlannedTdTvdMsl() != null)
				{
					PlannedTvdMdMsl = thisWellbore.get(0).getPlannedTdTvdMsl();
					thisConverter.setReferenceMappingField(Wellbore.class, "plannedTdTvdMsl", operation.getUomTemplateUid());
					thisConverter.setBaseValue(PlannedTvdMdMsl);
					PlannedTvdMdMsl = thisConverter.getConvertedValue();
					strPlanTvd = thisConverter.getFormattedValue(PlannedTvdMdMsl, true);
				}	
			}
			
			
			thisConverter.setReferenceMappingField(Operation.class, "daysSpentPriorToSpud");
			
			thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysFromSpudOnOperation(operation.getOperationUid(), day.getDailyUid()));
			String daysFromSpud = thisConverter.getFormattedValue();
			String daysFromSpud_SinceOpsSpudDate  = thisConverter.getFormattedValue();
			thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysOnOperation(operation.getOperationUid(), day.getDailyUid()));
			String daysOnWell = thisConverter.getFormattedValue();
			Map <String, Object> operationItem = new HashMap <String, Object>();
			Map<String,Object> mapHseList=this.hseIncidentList(day);
			//if no afe added, value should be obtain from operation screen (manual entry), else must from costnet afe screen
			Double totalAfe = 0.0;
			Double afeOri = operation.getAfe();
			Double suppAfeOri = operation.getSecondaryafeAmt();
			Double afe = 0.0;
			
			getAfeData(operation);
			Double afeCost = operation.getAfe();
			Double suppAfeCost = operation.getSecondaryafeAmt();
			
			if(afeCost!=null){
				totalAfe = totalAfe + afeCost;
			}
			else if (afeOri!=null){
				totalAfe = totalAfe + afeOri;
				
			}
				
			
			if(suppAfeCost!=null){
				totalAfe = totalAfe + suppAfeCost;	
			}
			else if (suppAfeOri!=null){
				totalAfe = totalAfe + suppAfeOri;
			}		
			thisConverter.setReferenceMappingField(Operation.class, "afe");
			String totalAfeAmount;
			thisConverter.setBaseValue(totalAfe);
			totalAfeAmount = thisConverter.getFormattedValue(totalAfe, false);
			Double cumCost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(day.getDailyUid(), reportType);
			String cumCostValue;
			if (cumCost != null) {
				thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
				thisConverter.setBaseValue(cumCost);
				cumCostValue = thisConverter.getFormattedValue(cumCost, false);
			} else {
				cumCostValue = "";
			}
			
			if(operation.getAfe() != null)
			{
				afe = operation.getAfe();
				thisConverter.setReferenceMappingField(Operation.class, "afe");
				thisConverter.setBaseValue(afe);
				afe = thisConverter.getConvertedValue();
				strAfe = thisConverter.getFormattedValue(afe, false);
			}
			
			//Get the operationUid and operationName of those operations that have current daily day date
			List OperationNameList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select DISTINCT o.operationUid, o.operationName " +
					"From Operation o, Daily d WHERE (o.isDeleted is null or o.isDeleted = false) AND " +
					"(d.isDeleted is null or d.isDeleted = false) AND " +
					((this.includeSpecificOperationType!=null && this.includeSpecificOperationType.size()>0)?" o.operationCode in ('"+ StringUtils.join(this.includeSpecificOperationType.toArray(), "','") +"') AND ":"") + 
					"(o.operationUid=d.operationUid) AND (o.tightHole is null or o.tightHole = false) AND " +
					"d.dailyUid =:dailyUid", new String[] {"dailyUid"}, new Object[]{day.getDailyUid()});
			
			if(reportJobResult != null){
				for(int i=0; i<reportJobResult.size(); i++)
				{
					ReportJobResult rjr =  reportJobResult.get(i);
					String reportFileName=rjr.getReportFile().getDisplayName()+"."+this.getFileExtension(rjr.getReportFilePath());
					
					attachment.put(reportFileName, new File(rjr.getReportFilePath()));
				}
			}
			
			String completeOperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(day.getGroupUid(), operation.getOperationUid(), null, true);
			if(this.opsTeamBased){
				String accessibleOpsTeamOperation = "SELECT oto.operationUid FROM EmailList el, OpsTeam ot, OpsTeamOperation oto" +
					" WHERE (el.isDeleted=false OR el.isDeleted IS NULL)" +
					" AND (ot.isDeleted=false OR ot.isDeleted IS NULL)" +
					" AND (oto.isDeleted=false OR oto.isDeleted IS NULL)" +
					" AND oto.opsTeamUid=ot.opsTeamUid" +
					" AND el.emailListUid=ot.emailListUid" +
					" AND el.emailListKey=:emailListKey";
				String[] paramsFields = {"emailListKey"};
				Object[] paramsValues = {emailConfig.getSpecificEmailDistributionKey()};
				List lsAccessibleOpsTeamOperation = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(accessibleOpsTeamOperation, paramsFields, paramsValues);
				
				if(lsAccessibleOpsTeamOperation.size() > 0){
					for (Object operationUid : lsAccessibleOpsTeamOperation) {
							if(operationUid.equals(day.getOperationUid())){
								if (OperationNameList.size() > 0){
									operationAssignedOpsteam = true;
									for(Iterator i = OperationNameList.iterator(); i.hasNext(); ){
										
										Object[] obj_array = (Object[]) i.next();
										
										if (obj_array[0] != null && StringUtils.isNotBlank(obj_array[0].toString())){
											if(operationUid.equals(obj_array[0].toString()))
											{
												if (obj_array[1] != null && StringUtils.isNotBlank(obj_array[1].toString())){
													operationItem.put("operationName", obj_array[1].toString());
												}
												if (obj_array[0] != null && StringUtils.isNotBlank(obj_array[0].toString())){
													operationItem.put("operationUid", obj_array[1].toString());
												}
												
												operationItem.put("rigName", (rig.getRigName()!=null) ? rig.getRigName() : "");
												operationItem.put("totalAfe", totalAfeAmount);
												operationItem.put("spudDate", (operation.getSpudDate() != null) ? this.formatDate(operation.getSpudDate(), reportDateFormat) : "");
												operationItem.put("snrdrillsupervisor", (thisReportDaily.getSnrdrillsupervisor()!=null) ? thisReportDaily.getSnrdrillsupervisor() : "");
												operationItem.put("cumCost", cumCostValue);
												operationItem.put("reportNumber", (thisReportDaily.getReportNumber()!=null) ? thisReportDaily.getReportNumber() : "");
												operationItem.put("progress", (strProgress!=null) ? strProgress : "");
												operationItem.put("plannedTdMdMsl", (strPlanTd!=null) ? strPlanTd : "");
												operationItem.put("plannedTdTvdMsl", (strPlanTvd!=null) ? strPlanTvd : "");
												operationItem.put("reportCurrentStatus", (thisReportDaily.getReportCurrentStatus()!=null) ? thisReportDaily.getReportCurrentStatus() : "");
												operationItem.put("reportDatetime", (this.formatDate(thisReportDaily.getReportDatetime(), reportDateFormat)!=null) ? this.formatDate(thisReportDaily.getReportDatetime(), reportDateFormat) : "");
												operationItem.put("daysFromSpud", daysFromSpud);
												operationItem.put("reportPeriodSummary", (thisReportDaily.getReportPeriodSummary()!=null) ? thisReportDaily.getReportPeriodSummary() : "");
												operationItem.put("reportPlanSummary", (thisReportDaily.getReportPlanSummary()!=null) ? thisReportDaily.getReportPlanSummary() : "");
												operationItem.put("hseList", (mapHseList!=null) ? mapHseList : "");
												strPlanTd = "";
												strPlanTvd="";
												strProgress="";
												operationItem.put("afe", strAfe);
												if(lookupCompany!=null && lookupCompany.getCompanyName()!=null)
													operationItem.put("rigManager", lookupCompany.getCompanyName());
												else 
													operationItem.put("rigManager", "");
												//operationItem.put("rigManager", (lookupCompany.getCompanyName()!=null) ? lookupCompany.getCompanyName() : "");
												operationItem.put("daysOnWell", daysOnWell);
												operationItem.put("daycost", strDayCost);
												operationItem.put("depthMdMsl", strDepthMdMsl);
												operationItem.put("depthTvdMsl", strDepthTvdMsl);
												operationItem.put("lastCsgshoeMdMsl", strLastCsgshoeMdMsl);
												operationItem.put("lastCsgshoeTvdMsl", strLastCsgshoeTvdMsl);
												operationItem.put("cumcost", (thisReportDaily.getCumcost()!=null) ? thisReportDaily.getCumcost() : "");
												operationItem.put("lastCsgsize", lookup);
												operationItem.put("lastHolesize", strLastHolesize);
												operationItem.put("noteStatusafter0000", (thisReportDaily.getNoteStatusafter0000()!=null) ? thisReportDaily.getNoteStatusafter0000() : "");
												operationItem.put("last24hrsincident", (thisReportDaily.getLast24hrsincident()!=null) ? thisReportDaily.getLast24hrsincident() : "");
												operationItem.put("daysFromSpud_SinceOpsSpudDate", daysFromSpud_SinceOpsSpudDate);
												operationItem.put("completeOperationName", completeOperationName);
												strDepthMdMsl= "";
												strDepthTvdMsl= "";
												strLastCsgshoeMdMsl="";
												strLastCsgshoeTvdMsl ="";
												lookup="";
												strLastHolesize="";
												strDayCost ="";
												strAfe="";
											}
										}
										emailContentItemList.put(day.getOperationUid(), (operationItem!=null) ? operationItem : "");
										
									}
								}
							}
						
					}
					
				}
			}else{
				if (OperationNameList.size() > 0){
					for(Iterator i = OperationNameList.iterator(); i.hasNext(); ){
						
						Object[] obj_array = (Object[]) i.next();
						if (obj_array[1] != null && StringUtils.isNotBlank(obj_array[1].toString())){
								operationItem.put("operationName", obj_array[1].toString());
							}
						if (obj_array[0] != null && StringUtils.isNotBlank(obj_array[0].toString())){
							operationItem.put("operationUid", obj_array[1].toString());
						}		
						
						operationItem.put("rigName", (rig.getRigName()!=null) ? rig.getRigName() : "");
						operationItem.put("totalAfe", totalAfeAmount);
						operationItem.put("spudDate", (operation.getSpudDate() != null) ? this.formatDate(operation.getSpudDate(), reportDateFormat) : "");
						operationItem.put("snrdrillsupervisor", (thisReportDaily.getSnrdrillsupervisor()!=null) ? thisReportDaily.getSnrdrillsupervisor() : "");
						operationItem.put("cumCost", cumCostValue);
						operationItem.put("reportNumber", (thisReportDaily.getReportNumber()!=null) ? thisReportDaily.getReportNumber() : "");
						operationItem.put("progress", (strProgress!=null) ? strProgress : "");
						operationItem.put("plannedTdMdMsl", (strPlanTd!=null) ? strPlanTd : "");
						operationItem.put("plannedTdTvdMsl", (strPlanTvd!=null) ? strPlanTvd : "");
						operationItem.put("reportCurrentStatus", (thisReportDaily.getReportCurrentStatus()!=null) ? thisReportDaily.getReportCurrentStatus() : "");
						operationItem.put("reportDatetime", (this.formatDate(thisReportDaily.getReportDatetime(), reportDateFormat)!=null) ? this.formatDate(thisReportDaily.getReportDatetime(), reportDateFormat) : "");
						operationItem.put("daysFromSpud", daysFromSpud);
						operationItem.put("reportPeriodSummary", (thisReportDaily.getReportPeriodSummary()!=null) ? thisReportDaily.getReportPeriodSummary() : "");
						operationItem.put("reportPlanSummary", thisReportDaily.getReportPlanSummary());
						operationItem.put("hseList", (mapHseList!=null) ? mapHseList : "");
						emailContentItemList.put(day.getOperationUid(), (operationItem!=null) ? operationItem : "");
						strPlanTd = "";
						strPlanTvd="";
						strProgress="";
						operationItem.put("afe", strAfe);
						if(lookupCompany!=null && lookupCompany.getCompanyName()!=null)
							operationItem.put("rigManager", lookupCompany.getCompanyName());
						else 
							operationItem.put("rigManager", "");
						//operationItem.put("rigManager", (lookupCompany.getCompanyName()!=null) ? lookupCompany.getCompanyName() : "");
						operationItem.put("daysOnWell", daysOnWell);
						operationItem.put("daycost", strDayCost);
						operationItem.put("depthMdMsl", strDepthMdMsl);
						operationItem.put("depthTvdMsl", strDepthTvdMsl);
						operationItem.put("lastCsgshoeMdMsl", strLastCsgshoeMdMsl);
						operationItem.put("lastCsgshoeTvdMsl", strLastCsgshoeTvdMsl);
						operationItem.put("cumcost", (thisReportDaily.getCumcost()!=null) ? thisReportDaily.getCumcost() : "");
						operationItem.put("lastCsgsize", lookup);
						operationItem.put("lastHolesize", strLastHolesize);
						operationItem.put("noteStatusafter0000", (thisReportDaily.getNoteStatusafter0000()!=null) ? thisReportDaily.getNoteStatusafter0000() : "");
						operationItem.put("last24hrsincident", (thisReportDaily.getLast24hrsincident()!=null) ? thisReportDaily.getLast24hrsincident() : "");
						operationItem.put("daysFromSpud_SinceOpsSpudDate", daysFromSpud_SinceOpsSpudDate);
						operationItem.put("completeOperationName", completeOperationName);
						strDepthMdMsl= "";
						strDepthTvdMsl= "";
						strLastCsgshoeMdMsl="";
						strLastCsgshoeTvdMsl ="";
						lookup="";
						strLastHolesize="";
						strDayCost ="";
						strAfe="";
					}
					
				}
			}
		}
		if(list.size() > 0 && list!=null)
		{
			if(this.opsTeamBased)
			{
				if(operationAssignedOpsteam)
				{
					emailConfig.setEmailList(emailList==null?this.getEmailList(emailConfig):emailList);
					if (emailContentItemList.size() > 0 && emailContentItemList != null) emailConfig.addContentMapping("emailContentItemList", emailContentItemList);
					emailConfig.setAttachment(attachment);
					this.sendEmail(emailConfig);
				}
			}
			else
			{
				emailConfig.setEmailList(emailList==null?this.getEmailList(emailConfig):emailList);
				if (emailContentItemList.size() > 0 && emailContentItemList != null) emailConfig.addContentMapping("emailContentItemList", emailContentItemList);
				emailConfig.setAttachment(attachment);
				this.sendEmail(emailConfig);
			}
			
		}	
	}
	//To get the hse records from hse screen to show in email content
	private Map<String, Object> hseIncidentList (Daily day) throws Exception {
		
		String strSql ="FROM HseIncident WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid"; 
		String[] paramsFields = {"operationUid", "dailyUid"};
		String[] paramsValues = {day.getOperationUid(), day.getDailyUid()};						
		List<HseIncident> hseList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		Map<String, Object> hseItems = new HashMap<String, Object>();
		if (hseList.size() > 0) {
			for (HseIncident rec : hseList) {
				Map<String, Object> hseItem = new HashMap<String, Object>();
				hseItem.put("hseShortDescription", rec.getHseShortdescription());
				hseItems.put(rec.getHseIncidentUid(), hseItem);
			}
		}
		return hseItems;
	}
	//To get the report daily data to show in the email content
	private ReportDaily reportDailyList (String operationUid, String reportType, String dailyUid) throws Exception {
		
		String strSql ="FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND (reportType = :reportType)"; 
		String[] paramsFields = {"operationUid", "dailyUid", "reportType"};
		String[] paramsValues = {operationUid, dailyUid, reportType};						
		List<ReportDaily> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (rs.size() > 0) {
			return (ReportDaily) rs.get(0);
		} else {
			return null;
		}
	}
	
	//To get the afe data to show in email content
	public static void getAfeData(Operation operation) throws Exception
	{
		List<Object[]> afeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationAfe o, CostAfeMaster c where (o.isDeleted=false or o.isDeleted is null) and (c.isDeleted=false or c.isDeleted is null) and (o.afeType=:afeType) and o.costAfeMasterUid=c.costAfeMasterUid and o.operationUid=:operationUid", new String[] {"afeType", "operationUid"}, new Object[] {CostNetConstants.MASTER_AFE, operation.getOperationUid()});
		if (afeList.size()>0) {
			String afeNumber = "";
			Double afeTotal = null;
			for (Object[] rec : afeList) {
				CostAfeMaster costAfeMaster = (CostAfeMaster) rec[1];
				afeNumber += ("".equals(afeNumber)?"":"\n") + costAfeMaster.getAfeNumber();
				Double thisAfeTotal = CommonUtil.getConfiguredInstance().calculateAfeTotal(costAfeMaster.getCostAfeMasterUid(), operation.getOperationUid());
				if (thisAfeTotal!=null) {
					if (afeTotal==null) afeTotal = 0.0;
					afeTotal += thisAfeTotal;
				}
			}
			operation.setAfeNumber(afeNumber);
			operation.setAfe(afeTotal);			
		}
		
		afeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationAfe o, CostAfeMaster c where (o.isDeleted=false or o.isDeleted is null) and (c.isDeleted=false or c.isDeleted is null) and (o.afeType=:afeType or o.afeType='' or o.afeType is null) and o.costAfeMasterUid=c.costAfeMasterUid and o.operationUid=:operationUid", new String[] {"afeType", "operationUid"}, new Object[] {CostNetConstants.SUPPLEMENTARY_AFE, operation.getOperationUid()});
		if (afeList.size()>0) {
			String afeNumber = "";
			Double afeTotal = null;
			for (Object[] rec : afeList) {
				CostAfeMaster costAfeMaster = (CostAfeMaster) rec[1];
				afeNumber += ("".equals(afeNumber)?"":"\n") + costAfeMaster.getAfeNumber();
				Double thisAfeTotal = CommonUtil.getConfiguredInstance().calculateAfeTotal(costAfeMaster.getCostAfeMasterUid(), operation.getOperationUid());
				if (thisAfeTotal!=null) {
					if (afeTotal==null) afeTotal = 0.0;
					afeTotal += thisAfeTotal;
				}
			}
			operation.setSecondaryafeRef(afeNumber);
			operation.setSecondaryafeAmt(afeTotal);			
		}
	}
	
}