package com.idsdatanet.d2.drillnet.customRSR;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CustomRigStateRawDataLoaderInterceptor implements DataLoaderInterceptor{
	
	public void afterPropertiesSet() throws Exception {
    }
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String opsId = (String) commandBean.getRoot().getDynaAttr().get("opsID");
		if (StringUtils.isBlank(opsId)){
			commandBean.getRoot().getDynaAttr().put("opsID", userSelection.getOperationUid());
		}else {
			if (!opsId.equals(userSelection.getOperationUid())) {
				commandBean.getRoot().getDynaAttr().put("filter", "");
			}
		}
		String filter = (String) commandBean.getRoot().getDynaAttr().get("filter");
		String selecteddaydate = null;
		String selecteddayTime  = null;
		String selectedrigState = null;
		String selectedHide = null;
		if (StringUtils.isNotBlank(filter)){
			if (filter.equals("reset")) {
				commandBean.getRoot().getDynaAttr().put("daydate", null);
				commandBean.getRoot().getDynaAttr().put("dayTime", null);
				commandBean.getRoot().getDynaAttr().put("rigState", null);
				commandBean.getRoot().getDynaAttr().put("hideRec", null);
			}else {
				String[] filterField = filter.split(",");
				selecteddaydate = (String) filterField[0];
				selecteddayTime  = (String) filterField[1];
				selectedrigState = (String) filterField[2];
				selectedHide = (String) filterField[3];
				
				commandBean.getRoot().getDynaAttr().put("daydate", selecteddaydate);
				commandBean.getRoot().getDynaAttr().put("dayTime", selecteddayTime);
				commandBean.getRoot().getDynaAttr().put("rigState", selectedrigState);
				commandBean.getRoot().getDynaAttr().put("hideRec", selectedHide);
			}
		} else {
			commandBean.getRoot().getDynaAttr().put("daydate", null);
			commandBean.getRoot().getDynaAttr().put("dayTime", null);
			commandBean.getRoot().getDynaAttr().put("rigState", null);
			commandBean.getRoot().getDynaAttr().put("hideRec", null);
		}
		String customCondition = "";
		if ((StringUtils.isNotBlank(selecteddaydate)) && (StringUtils.isNotBlank(selecteddayTime))) {
			
			Date startTime = null;
			Date endTime = null;
			Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, selecteddaydate);
			if (daily != null) {
				Date date = daily.getDayDate();
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				cal.set(Calendar.HOUR_OF_DAY, this.convertToInt(selecteddayTime));
				startTime = cal.getTime();
				
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				endTime = cal.getTime();
	 		}
			
			if (startTime!=null) {
				customCondition = customCondition + " AND startTime between :minDate and :maxDate";
				query.addParam("minDate", startTime);
				query.addParam("maxDate", endTime);
			}
			
			if(StringUtils.isNotBlank(selectedrigState)) {
				customCondition = customCondition + " AND internalRigState=:selectedrigState";
				query.addParam("selectedrigState", selectedrigState);
			}
			
			if(StringUtils.isNotBlank(selectedHide)) {
				if (selectedHide.equals("0")) {
					customCondition = customCondition + " AND (hideRecord=false or hideRecord is null)";
				}
			}
		}else {
			customCondition = customCondition + " AND dailyUid='xxxx'";
		}
         
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public int convertToInt(String stringInt){
		if (NumberUtils.isNumber(stringInt)) return Integer.parseInt(stringInt);
		return (Integer) null;
	}
	
	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
