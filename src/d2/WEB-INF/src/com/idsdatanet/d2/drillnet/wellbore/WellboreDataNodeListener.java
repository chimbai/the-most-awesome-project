
//** changes made on this file should also be applied to well operation if needed.

package com.idsdatanet.d2.drillnet.wellbore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.SurveyReference;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.session.SystemSelectionFilter;
import com.idsdatanet.d2.drillnet.formation.FormationUtils;
import com.idsdatanet.d2.drillnet.operation.OperationUtils;
import com.idsdatanet.d2.drillnet.report.schedule.EmailConfiguration;
import com.idsdatanet.d2.drillnet.wellexplorer.WellExplorerConstant;
import com.idsdatanet.d2.safenet.environmentDischarges.EnvironmentDischargesUtil;

public class WellboreDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor, CommandBeanListener, DataNodeAllowedAction {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	private MailEngine mailEngine = null;
	private EmailConfiguration emailConfig = null;
	private Boolean wellborestart = false;	
	private Boolean wellboreend = false;

	public void setEmailConfig(EmailConfiguration emailConfig) {
		this.emailConfig = emailConfig;
	}

	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}
	
	public void setWellborestart(Boolean wellborestart){
		this.wellborestart = wellborestart;
	}
	
	public void setWellboreend(Boolean wellboreend){
		this.wellboreend = wellboreend;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if(object instanceof Wellbore) {
			Wellbore thisWellbore = (Wellbore) object;

			String wbParentWellboreUid = (String) thisWellbore.getParentWellboreUid();
			String wbWellboreUid = (String) thisWellbore.getWellboreUid();
			
			if(StringUtils.isNotBlank(wbParentWellboreUid) && StringUtils.isNotBlank(wbWellboreUid)) {
				// can't really pick something on an add new - so the wellbore uid will already be known
				if (wbWellboreUid.equals(wbParentWellboreUid)) {
					status.setFieldError(node, "parentWellboreUid", "Parent wellbore and current wellbore [" + thisWellbore.getWellboreName() + "] cannot be the same. This would introduce an infinite reference loop");
					status.setContinueProcess(false, true);
					return;
				}
			}

			// check if parent wellbore is part of this wellbore's child.
			if(ApplicationUtils.getConfiguredInstance().isWellboreParentCyclicReference(thisWellbore)) {
				status.setFieldError(node, "parentWellboreUid", "This parent wellbore seems to be part of this wellbore's child. Please choose another parent wellbore.");
				status.setContinueProcess(false, true);
				return;
			}
			
			//clear parent wellbore uid and kickoffmdmsl if wellbore type = initial or pilot 
			if("INITIAL".equals(thisWellbore.getWellboreType()) || "PILOT".equals(thisWellbore.getWellboreType())){
				thisWellbore.setKickoffMdMsl(null);
				thisWellbore.setKickoffTvdMsl(null);
				thisWellbore.setParentWellboreUid(null);
			}
			
			if (StringUtils.isNotBlank(thisWellbore.getParentWellboreUid())){
				Wellbore parentWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, thisWellbore.getParentWellboreUid());
				if(parentWellbore != null) {
					String OldWellUid = thisWellbore.getWellUid();
					String NewWellUid = parentWellbore.getWellUid();
					if (OldWellUid != NewWellUid)
					{
						String strSql = "FROM Operation WHERE wellUid = :wellUid AND wellboreUid = :wellboreUid AND (isDeleted IS NULL OR isDeleted = '') ";
						List Opslist = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,  new String[] {"wellUid", "wellboreUid"},  new String[] {OldWellUid, thisWellbore.getWellboreUid()});				
						for (Object Opsobject : Opslist) {
							Operation operation = (Operation) Opsobject;
							
							// update associated operation wellUid
							String strSql1 = "UPDATE Wellbore SET wellUid =:thisWellUid " +
							 "WHERE wellboreUid =:thisWellboreUid";
							String[] paramsFields = {"thisWellUid", "thisWellboreUid"};
							Object[] paramsValues = {parentWellbore.getWellUid(), thisWellbore.getWellboreUid()};
							ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields, paramsValues);
							
							// update associated operation wellUid
							String strSql2 = "UPDATE Operation SET wellUid =:thisWellUid " +
							 "WHERE operationUid =:thisOperationUid";
							String[] paramsFields2 = {"thisWellUid", "thisOperationUid"};
							Object[] paramsValues2 = {parentWellbore.getWellUid(), operation.getOperationUid()};
							ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields2, paramsValues2);
							
							// update associated daily's wellUid
							String strSql3 = "UPDATE Daily SET wellUid =:thisWellUid " +
							 "WHERE operationUid =:thisOperationUid";
							String[] paramsFields3 = {"thisWellUid", "thisOperationUid"};
							Object[] paramsValues3 = {parentWellbore.getWellUid(), operation.getOperationUid()};
							ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
						}
						
						thisWellbore.setWellUid(parentWellbore.getWellUid());
						
						// below may cause an unreferenced well, so check and clean
						deleteWellIfNoWellboreAssociated(OldWellUid,  thisWellbore.getWellboreUid());
					}
				}
			}
		}
	}
	
	private void deleteWellIfNoWellboreAssociated(String wellUid, String wellboreUid) throws Exception {
		if (StringUtils.isNotBlank(wellUid)) {
			List otherWellbores = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where (isDeleted = false or isDeleted is null) and wellUid = :wellUid", "wellUid", wellUid);
			if (otherWellbores.isEmpty()) {
				// no other wellbore associated with this well, mark well as deleted
				ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWell(wellUid);
			}
		}
	}

	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		
		//update first day progress when editing kickoff depth
		if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DAILY_24HR_PROGRESS))){
		
			Object object = node.getData();
			
			if(object instanceof Wellbore)
			{
				Wellbore thisWellbore = (Wellbore)object;
				
				if(StringUtils.isNotBlank(thisWellbore.getWellboreUid()))
				{
					String strSql = "Select o.operationUid FROM Operation o, Wellbore wb WHERE o.wellboreUid = wb.wellboreUid AND o.wellboreUid = :wellboreUid AND (o.isDeleted IS NULL OR o.isDeleted = '') AND (wb.isDeleted IS NULL OR wb.isDeleted = '')";
					List list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellboreUid", thisWellbore.getWellboreUid());				
					if (list2.size() > 0) {		
						for(Object a : list2){
							if (a!=null) CommonUtil.getConfiguredInstance().updateFirstDay24HrsProgress(a.toString(), thisWellbore.getKickoffMdMsl(), commandBean, thisWellbore);
						}
					}
						
				}
			}
		}
		
		if (node.getData() instanceof Wellbore) {
			Wellbore wellbore = (Wellbore) node.getData();
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "wellStartEndNotification")))
			{
				if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW && this.wellborestart) { 
					this.sendWellboreStartEndNotificationMail(commandBean, node, session, wellbore, "start"); 
				}else{
					if (wellbore.isPropertyModified("wellboreEnd") && BooleanUtils.isTrue(wellbore.getWellboreEnd()) && this.wellboreend){
						this.sendWellboreStartEndNotificationMail(commandBean, node, session, wellbore, "end");
					}
				}
			}
		
			if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				String redirectPage = "operation.html";
				//session.setCurrentWellboreUid(wellbore.getWellboreUid());
				boolean isFromWell = "1".equalsIgnoreCase(request.getParameter("fromWell"))?true:false;
				String copyFromOperationUid = (String) node.getDynaAttr().get("currentCopyFromOperationUid");
				commandBean.getRoot().getDynaAttr().remove("fromWell");
				commandBean.setRedirectUrl(CommonUtils.urlPathConcat(session.getApplicationContextPath(), redirectPage + "?newRecordInputMode=1&"+(isFromWell?"fromWell":"fromWellbore")+"=1&wellboreUid="+wellbore.getWellboreUid() + 
						(StringUtils.isNotBlank(copyFromOperationUid)?"&copyFromOperationUid="+copyFromOperationUid:"")));
				commandBean.setRedirectMessage("Creating Operation of " + wellbore.getWellboreName());
			}
			
			List<String> changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(wellbore.getWellboreUid());
			D2ApplicationEvent.publishTableChangedEvent(Wellbore.class, changedRowPrimaryKeys);
			
			changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(wellbore.getWellUid());
			D2ApplicationEvent.publishTableChangedEvent(Well.class, changedRowPrimaryKeys);
		
			// auto populate PrognosedTopTvdMsl and SampleTopTvdMsl from prognosedTopMdMsl and sampleTopMdMsl when Wellbore.wellboreShape = 'Vertical'
			FormationUtils.autoPopulateTvdFromMd(wellbore.getWellboreUid(), wellbore.getWellboreShape(), session, commandBean);
		}
		
		Daily selectedDaily = session.getCurrentDaily();
		if (selectedDaily != null) {
			session.setCurrentDailyUid(selectedDaily.getDailyUid());
		}
		ApplicationUtils.getConfiguredInstance().refreshCachedData();
		// if we just updated an existing record, reload the page for the well and datum drop down to reflect the change
		if (operationPerformed != BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
			if (commandBean.getFlexClientControl() != null) {
				commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
			}
		}
	}
	
	private void sendWellboreStartEndNotificationMail(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, Wellbore wellbore, String status) throws Exception{
		String[] emailList = ApplicationUtils.getConfiguredInstance().getCachedEmailList(session.getCurrentGroupUid(), emailConfig.getSpecificEmailDistributionKey());
		String supportAddr = ApplicationConfig.getConfiguredInstance().getSupportEmail();

		Map<String, Object> params = new HashMap<String, Object>();	
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellbore.getWellUid());
		params.put("wellName", well.getWellName());
		params.put("wellboreName", wellbore.getWellboreName());
		if(StringUtils.isEmpty(wellbore.getGeoLocationCode())){
			params.put("geoLocationCode", "-");
		}else{
			params.put("geoLocationCode", wellbore.getGeoLocationCode());	
		}
		if ("start".equals(status)){
			params.put("status", "started");
			this.emailConfig.setSubject("Wellbore Start Notification");
		}else{
			params.put("status", "ended");
			this.emailConfig.setSubject("Wellbore End Notification");
		}
		
		if (emailList.length!=0)
		this.mailEngine.sendMail(emailList, supportAddr, ApplicationConfig.getConfiguredInstance().getSupportEmail(),
		emailConfig.getSubject(), this.mailEngine.generateContentFromTemplate(emailConfig.getTemplateFile(), params));
	}

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "(isDeleted = false or isDeleted is null)";
		
		String wellExplorerWellUid = null;
		String wellExplorerWellboreUid = null;
		UserSession session = null;
		if (request != null) {
			wellExplorerWellUid = request.getParameter(WellExplorerConstant.WELL_UID);
			wellExplorerWellboreUid = request.getParameter(WellExplorerConstant.WELLBORE_UID);
			session = UserSession.getInstance(request);
		}
		
		String dynaAttrWellUid = (String) commandBean.getRoot().getDynaAttr().get("wellUid");
		if (StringUtils.isNotBlank(dynaAttrWellUid)) {
			if (!com.idsdatanet.d2.drillnet.constant.UI.SELECT_OPTION_ALL.equals(dynaAttrWellUid)) {
				query.addParam("customFilterWellUid", commandBean.getRoot().getDynaAttr().get("wellUid"));
				customCondition += " and wellUid = :customFilterWellUid";
			}
		} else {
			if (StringUtils.isNotBlank(wellExplorerWellboreUid) && session != null) {
				customCondition += " and wellboreUid = :customFilterWellboreUid";
				query.addParam("customFilterWellboreUid", wellExplorerWellboreUid);
				
				session.setCurrentWellboreUid(wellExplorerWellboreUid);
				ApplicationUtils.getConfiguredInstance().setLastActiveOperationAndDaily(session);
			} else if (StringUtils.isNotBlank(wellExplorerWellUid)) {
				customCondition += " and wellUid = :customFilterWellUid";
				query.addParam("customFilterWellUid", wellExplorerWellUid);
			} else {
				customCondition += " and wellboreUid = :customFilterWellboreUid";
				query.addParam("customFilterWellboreUid", userSelection.getWellboreUid());
			}
		}
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object object = node.getData();
		if(object instanceof Wellbore) {
			
			Wellbore thisWellbore = (Wellbore) object;
			
			List<Operation> opss = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from operation where (isDeleted = false or isDeleted is null) and wellboreUid=:wellboreUid", new String[] { "wellboreUid" }, new Object[] { thisWellbore.getWellboreUid() });
			if (opss.size()>0) {
				for (Operation ops : opss) {
					// Delete LAR Setting
					OperationUtils.deleteLARmapping(ops.getOperationUid());
				}
			}
			//cascade delete wellbore structure
			//ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWellbore(thisWellbore.getWellboreUid());
			ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWellbore(thisWellbore);
			
			session.setCurrentWellboreUid(null);
			session.getSystemSelectionFilter().setProperty(SystemSelectionFilter.WELLBORE_FILTER, null);
		}
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		Object obj = node.getData();
		
		if (obj instanceof Wellbore)
		{
			Wellbore thisWellbore = (Wellbore) obj;

			if ("1".equals(request.getParameter("fromWell")))
			{
				commandBean.getFlexClientControl().setDisableRootCancelButton(true);	
				commandBean.getFlexClientControl().setDisableCancelButton(Wellbore.class.getSimpleName(), true);
				thisWellbore.setWellUid(request.getParameter("wellUid"));
				node.getDynaAttr().put("disableWell", "1");
				
				if (StringUtils.isNotBlank(request.getParameter("copyFromOperationUid"))) {
					String sourceOperationUid = request.getParameter("copyFromOperationUid");
					node.getDynaAttr().put("currentCopyFromOperationUid", sourceOperationUid);
					Operation selectedOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, sourceOperationUid);
					if (selectedOperation!=null) {
						Wellbore parentWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, selectedOperation.getWellboreUid());
						if (parentWellbore!=null) {
							Wellbore newWellbore = (Wellbore) BeanUtils.cloneBean(parentWellbore);
							newWellbore.setWellboreUid(null);
							newWellbore.setWellboreEnd(false);
							newWellbore.setWellUid(request.getParameter("wellUid"));
							node.setData(newWellbore);
						}
					}
				}
			}else
			{
				String currentWellUid=UserSession.getInstance(request).getCurrentWellUid();
				if (currentWellUid!=null)
					thisWellbore.setWellUid(currentWellUid);
			}
			thisWellbore.setWellboreEnd(false);
		}
		
		
	}

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);

	}
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		
		if (object instanceof Wellbore) {
			
			Wellbore thisWellbore = (Wellbore)object;
			
			if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN){
				if("1".equals(request.getParameter("newRecordInputMode"))) commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
				
			}
			
			this.populateVerticalSectionAngle(commandBean, meta, node, userSelection, request);
			
			if(thisWellbore.getWellUid() != null || StringUtils.isNotBlank(thisWellbore.getWellUid()))
			{					
				if (isLastWellbore(thisWellbore.getWellUid())){
					node.getDynaAttr().put("isLastWellbore", "true");
				}
			}
		}
	}
	
	public boolean isLastWellbore(String wellUid) throws Exception{
		String strSql = "FROM Wellbore WHERE wellUid = :wellUid and (isDeleted IS NULL OR isDeleted = '')";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellUid", wellUid);				
		if (list.size() == 1) {
			return true;
		}	
		return false;
	}
	
	private void populateVerticalSectionAngle(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception 
	{
		Wellbore thisWellbore = (Wellbore) node.getData();
		String thisWellboreUid  = thisWellbore.getWellboreUid();
		//boolean isVSDFound = false;
		String strSql = "FROM SurveyReference WHERE (isDeleted = false or isDeleted is null) and wellboreUid = :thisWellboreUid and (statusIndicator='Current' or statusIndicator='Definitive')";
		String[] paramsFields = {"thisWellboreUid"};
		Object[] paramsValues = {thisWellboreUid};
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, SurveyReference.class, "verticalSectionAngle");
		
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);

		if (lstResult.size() > 0){
			SurveyReference tmpSurvey = (SurveyReference) lstResult.get(0);	
			node.getDynaAttr().put("directionalSurvey", tmpSurvey.getVerticalSectionAngle());
			//isVSDFound = true;
		}else{
			strSql ="FROM SurveyReference Where (isDeleted = false or isDeleted is null) and wellboreUid = :thisWellboreUid and statusIndicator='Survey'";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size()==1)
			{
				SurveyReference tmpSurvey = (SurveyReference) lstResult.get(0);	
				node.getDynaAttr().put("directionalSurvey", tmpSurvey.getVerticalSectionAngle());
				//isVSDFound = true;
			}
		}
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@directionalSurvey", thisConverter.getUOMMapping());
		}
		/*if (!isVSDFound)
			commandBean.getSystemMessage().addWarning("The Vertical Section Direction is Blank cause There isn't any Current/Definitive Survey"); */
	}
	
	public void beforeDataNodeDelete(CommandBean commandBean, 
			CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, 
			HttpServletRequest request) throws Exception{
		
		Object object = node.getData();
		
		if(object instanceof Wellbore)
		{
			Wellbore thisWellbore = (Wellbore)object;
			
			if(thisWellbore.getWellboreUid() != null || StringUtils.isNotBlank(thisWellbore.getWellboreUid()))
			{
				String strSql = "FROM Operation o, Wellbore wb WHERE o.wellboreUid = wb.wellboreUid AND o.wellboreUid = :wellboreUid AND (o.isDeleted IS NULL OR o.isDeleted = '') AND (wb.isDeleted IS NULL OR wb.isDeleted = '')";
				List list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellboreUid", thisWellbore.getWellboreUid());				
				if (list2.size() > 0) {					
					status.setContinueProcess(false);
					commandBean.getSystemMessage().addError("This Wellbore cannot be deleted, there is other Operation link to this Wellbore");
				}else {
					EnvironmentDischargesUtil.calculateTotalWellAfterWellOpsDeleted(commandBean, node, session);
				}
			}
		}
	}

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public boolean allowedAction(CommandBeanTreeNode node, UserSession session,
			String targetClass, String action, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		if (node.isRoot())
		{
			if ("add".equalsIgnoreCase(action))
			{
				if ("Wellbore".equalsIgnoreCase(targetClass))
				{
					Map<String, CommandBeanTreeNode> mapList = node.getChild(Wellbore.class.getSimpleName());
					for (Iterator iter = mapList.entrySet().iterator(); iter.hasNext();) 
					{
						Map.Entry entry = (Map.Entry) iter.next();
						CommandBeanTreeNode nodeWellbore = (CommandBeanTreeNode)entry.getValue();
						Object disableWell = nodeWellbore.getDynaAttr().get("disableWell");
						if (disableWell!=null && "1".equalsIgnoreCase(disableWell.toString()))
							return false;
					}
						
				}	
			}
		}
		return true;
	}
}
