package com.idsdatanet.d2.drillnet.basin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Basin;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class BasinDataNodeListener extends EmptyDataNodeListener {
	
	// these values are defined in lookup.xml
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof Basin)
		{
			Basin basin = (Basin) object;
			
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			List<Basin> basinList = ApplicationUtils.getConfiguredInstance().getDaoManager().find("From Basin where (isDeleted is null or isDeleted = false)",qp);
			if (basinList.size()==0)
			{
				basin.setIsDefault(true);
			}
		}
		
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		
		Object object = node.getData();
		if (object instanceof Basin)
		{
			Basin basin = (Basin) object;
			if (basin.getIsDefault()!=null && basin.getIsDefault())
			{
				String[] param = {"basinUid"};
				Object[] value = {basin.getBasinUid()};
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE Basin SET isDefault=false where basinUid!=:basinUid and isDefault = true and  (isDeleted is null or isDeleted = false)",param,value);
			}
		}
	}
	
	public void afterDataNodeProcessed(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, String action, int operationPerformed, boolean errorOccurred) throws Exception{
		
	}
	
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
	}
	
	
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object object = node.getData();
		if (object instanceof Basin)
		{
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			List<Basin> basinList = ApplicationUtils.getConfiguredInstance().getDaoManager().find("From Basin where (isDeleted is null or isDeleted = false)",qp);
			if (basinList.size()==1)
			{
				Basin basin = basinList.get(0);
			
				basin.setIsDefault(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(basin,qp);
			}
		}
	}
	
	
	
	
	
	
}
