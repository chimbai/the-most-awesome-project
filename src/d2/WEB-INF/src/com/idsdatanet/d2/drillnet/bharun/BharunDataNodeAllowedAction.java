package com.idsdatanet.d2.drillnet.bharun;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class BharunDataNodeAllowedAction  implements DataNodeAllowedAction {
	
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		if("add".equals(action)){
			if(! node.getInfo().isNewlyCreated()){
				if(! node.getInfo().isTemplateNode()){
					if("BharunDailySummary".equals(targetClass)) {
						if(Bharun.class.equals(node.getDataDefinition().getTableClass())){
							Map child = node.getChild("BharunDailySummary");
							if(child != null){
								if(child.size() > 0) return false;
							}
						}
					}
				}
			}
		}
		return true;
	}
}
