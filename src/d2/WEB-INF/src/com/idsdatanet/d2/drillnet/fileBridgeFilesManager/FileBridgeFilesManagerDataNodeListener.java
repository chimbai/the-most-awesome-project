package com.idsdatanet.d2.drillnet.fileBridgeFilesManager;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.filemanager.html.FileManagerDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class FileBridgeFilesManagerDataNodeListener extends FileManagerDataNodeListener {
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		super.afterDataNodeLoad(commandBean, meta, node, userSelection, request);
		//node.getDynaAttr().put("operationPage" , OperationUtils.getWWOHyperlinkRedirection(userSelection.getGroupUid()));
	}
}
