package com.idsdatanet.d2.drillnet.wellbore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.drillnet.operation.OperationUtils;

public class WellboreCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if (OperationUtils.CHECKING_WELLBORE_NAME_EXIST.equals(invocationKey)) {
			OperationUtils.checkingWellboreNameExist(request, response);
		}
		return;
	}
}
