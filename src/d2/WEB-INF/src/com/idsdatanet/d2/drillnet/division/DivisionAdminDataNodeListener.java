package com.idsdatanet.d2.drillnet.division;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.scheduler.ConfigurableScheduler;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DefaultDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DivisionAdminDataNodeListener extends DefaultDataNodeListener {
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		ConfigurableScheduler.getConfiguredInstance().rescheduleAllJobs();
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		ConfigurableScheduler.getConfiguredInstance().rescheduleAllJobs();
	}

}
