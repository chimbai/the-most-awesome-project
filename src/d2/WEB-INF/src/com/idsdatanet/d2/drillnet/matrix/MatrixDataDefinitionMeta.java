package com.idsdatanet.d2.drillnet.matrix;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;

import com.idsdatanet.d2.core.dataservices.blazeds.UrlMappingUtils;
import com.idsdatanet.d2.core.i18n.MultiLangLabelService;
import com.idsdatanet.d2.core.web.menu.MenuItemEx;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.BaseFormController;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSession;

/**
 * A simple class to hold meta data of matrix data definition
 * @author RYAU
 *
 */
public class MatrixDataDefinitionMeta {
	private static final String KEY_TABLE_NAME 		= "tableName";
	private static final String KEY_BEAN_NAME		= "beanName";
	private static final String KEY_TITLE		 	= "title";
	private static final String KEY_SECTION			= "section";
	private static final String KEY_URL				= "url";
	private static final String KEY_CONDITION		= "condition";
	private static final String KEY_CUSTOM_HANDLER	= "customHandler";
	
	private String tableName = null;
	private String beanName = null;
	private String title = null;
	private String section = null;
	private String url = null;
	private String condition = null;
	private MatrixHandler customHandler = null;
	private ApplicationContext appContext = null;
	private UserSession session = null;
	private Boolean isSummaryTable = false;
	private Boolean checkedOnce = false;
	
	public MatrixDataDefinitionMeta(Map<String, Object> def) throws Exception {
		setTableName((String) def.get(KEY_TABLE_NAME));
		setBeanName((String) def.get(KEY_BEAN_NAME));
		setTitle((String) def.get(KEY_TITLE));
		setSection((String) def.get(KEY_SECTION));
		setUrl((String) def.get(KEY_URL));
		setCondition((String) def.get(KEY_CONDITION));
		setCustomHandler((MatrixHandler) def.get(KEY_CUSTOM_HANDLER));
	}

	public MatrixDataDefinitionMeta(UserSession session, MenuItemEx menuItem, ApplicationContext appContext) throws Exception {
		Properties menuLabelProperties = MultiLangLabelService.getMenuLabelProperties(session.getUserLanguage());
		setBeanName(menuItem.getBeanName());
		setTitle(menuItem.getLabel(menuLabelProperties));
		setSection(menuItem.getDataMatrixSectionName());
		setUrl(menuItem.getUrl());
		this.appContext = appContext;
		this.session = session;
	}

	public String getBeanName() {
		if (this.condition==null && this.tableName==null && StringUtils.isNotBlank(this.beanName)) {
			this.setBeanTableNameDefinitions(this.beanName);
		}
		return this.beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		if (this.condition==null && this.tableName==null && StringUtils.isNotBlank(this.beanName)) {
			this.setBeanTableNameDefinitions(this.beanName);
		}
		this.condition = condition;
	}

	public String getTableName() {
		if (this.condition==null && this.tableName==null && StringUtils.isNotBlank(this.beanName)) {
			this.setBeanTableNameDefinitions(this.beanName);
		}
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public String getSection() {
		return this.section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setCustomHandler(MatrixHandler customHandler) {
		this.customHandler = customHandler;
	}
	
	public MatrixHandler getCustomHandler() {
		return this.customHandler;
	}
	
	public Boolean hasSummaryTable() {
		if (this.condition==null && this.tableName==null && StringUtils.isNotBlank(this.beanName)) {
			this.setBeanTableNameDefinitions(this.beanName);
		}
		return this.isSummaryTable;
	}

	
	private void setBeanTableNameDefinitions(String beanName) {
		if (StringUtils.isBlank(beanName)) return;
		try {
			if (this.appContext!=null) {
				Object bean = this.appContext.getBean(beanName);
				if (bean instanceof BaseFormController) {
					BaseFormController controller = (BaseFormController) bean;
					String commandBeanId = controller.getCommandBeanResolver()!=null?controller.getCommandBeanResolver().resolveBeanName(this.session):controller.getCommandBeanId();
					
					if (commandBeanId!=null) {
						if (this.appContext.getBean(commandBeanId) instanceof BaseCommandBean) {
							BaseCommandBean commandBean = (BaseCommandBean) this.appContext.getBean(commandBeanId);
							
							//if URL have tab specified, get the controller of that dynamic tab 
							if (StringUtils.isNotBlank(this.getUrl()) && this.getUrl().contains("?") && beanName.equals(this.beanName) && !checkedOnce) {
								checkedOnce = true;
								Map<String,String[]> maps = this.createParameterMap(this.getUrl().substring(this.getUrl().indexOf("?")));
								if (maps.containsKey(BaseCommandBean.REQUEST_PARAM_CURRENT_TAB)) {
									String value = maps.get(BaseCommandBean.REQUEST_PARAM_CURRENT_TAB)[0];
									
									String path = StringUtils.trim(this.getUrl());
									if(! path.startsWith("/")) path = "/" + path;
									path = path.substring(0, path.indexOf("?"));
									Object handler = UrlMappingUtils.getConfiguredInstance().getUrlMapping().getHandlerMap().get(path);
									if (handler!=null) {
										if (handler!=null && handler instanceof BaseFormController) {
											BaseFormController urlController = (BaseFormController) handler;
											String urlCommandBeanId = urlController.getCommandBeanResolver()!=null?urlController.getCommandBeanResolver().resolveBeanName(session):urlController.getCommandBeanId();
											if(this.appContext.getBean(urlCommandBeanId) instanceof BaseCommandBean) {
												BaseCommandBean urlCommandBean = (BaseCommandBean) this.appContext.getBean(urlCommandBeanId);
												Map<String,String> mapTab = urlCommandBean.getInfo().getDynamicTabUrl(session);
												if (value!=null && mapTab!=null && mapTab.containsKey(value)) {
													path = mapTab.get(value);
													if(!path.startsWith("/")) path = "/" + path;
													handler = UrlMappingUtils.getConfiguredInstance().getUrlMapping().getHandlerMap().get(path);
													if (handler!=null && handler instanceof BaseFormController) {
														urlController = (BaseFormController) handler;
														this.setBeanTableNameDefinitions(urlController.getBeanName());
														return;
													}
												}
											}
										}
									}
								}
							}
							if (commandBean.getAllDataDefinitionMeta()!=null && commandBean.getAllDataDefinitionMeta().size()>0) {
								TreeModelDataDefinitionMeta node = commandBean.getAllDataDefinitionMeta().get(0);
								this.tableName = node.getTableClass().getSimpleName();
								if ("ReportOutputFile".equals(this.tableName)) this.tableName = "ReportFiles";
								this.condition = node.getSummaryConditions();
								if (StringUtils.isNotBlank(this.condition)) this.condition = this.condition.replaceAll("summarytable.", "");
								this.isSummaryTable = node.getSummaryTable();
								this.beanName = beanName;
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	protected Map<String, String[]> createParameterMap(String query) throws Exception {
        if (StringUtils.isEmpty(query)) {
            return new HashMap<String, String[]>();
        }
        if (query.charAt(0) == '?') {
        	// remove leading '?' if required
            query = query.substring(1);
        }
        HashMap<String, String[]> parameters = new HashMap<String, String[]>();
        // cut along the different parameters
        String[] params = StringUtils.split(query, '&');
        for (int i = 0; i < params.length; i++) {
            String key = null;
            String value = null;
            // get key and value, separated by a '='
            int pos = params[i].indexOf('=');
            if (pos > 0) {
                key = params[i].substring(0, pos);
                value = params[i].substring(pos + 1);
            } else if (pos < 0) {
                key = params[i];
                value = "";
            }
            
            value = java.net.URLDecoder.decode(value, "UTF-8");
            
            // now make sure the values are of type String[]
            if (key != null) {
                String[] values = parameters.get(key);
                if (values == null) {
                    // this is the first value, create new array
                    values = new String[] {value};
                } else {
                    // append to the existing value array
                    String[] copy = new String[values.length + 1];
                    System.arraycopy(values, 0, copy, 0, values.length);
                    copy[copy.length - 1] = value;
                    values = copy;
                }
                parameters.put(key, values);
            }
        }
        return parameters;
    }
}
