package com.idsdatanet.d2.drillnet.activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


public class SupplierKpiNPTDataGenerator implements ReportDataGenerator {
	private List<String> serviceCode;
	//future enhancement: to consider a variable/property to be customisable to set the code ('RIGSOWNER', 'LOG_MWDFWD','LOG_MUDLOG','_MUD','_DRL_DIR','LOG_WIRELINE') 

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	public void setServiceCode(List<String> serviceCode)
	{
		if(serviceCode != null){
			this.serviceCode = new ArrayList<String>();
			for(String value: serviceCode){
				this.serviceCode.add(value.trim().toUpperCase());
			}
		}
	}
	
	public List<String> getServiceCode() {
		return this.serviceCode;
	}
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		String operationUid = userContext.getUserSelection().getOperationUid();
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
		
		Date startDate=null;
		Date endDate=null;
		
		if(operation.getSpudDate()!=null && operation.getRigOffHireDate()!=null)
		{
			startDate = operation.getSpudDate();
			endDate = operation.getRigOffHireDate();
		}
		if(operation.getSpudDate()!=null && operation.getRigOffHireDate()==null)
		{
			startDate = operation.getSpudDate();
			endDate = CommonUtil.getConfiguredInstance().getFirstOrLastDateFromOperation(operationUid, "DDR", "DESC");
		}
		if(operation.getSpudDate()==null && operation.getRigOffHireDate()!=null)
		{
			startDate = CommonUtil.getConfiguredInstance().getFirstOrLastDateFromOperation(operationUid, "DDR", "ASC");
			endDate = operation.getRigOffHireDate();
			
		}
		if(operation.getSpudDate()==null && operation.getRigOffHireDate()==null)
		{
			startDate = CommonUtil.getConfiguredInstance().getFirstOrLastDateFromOperation(operationUid, "DDR", "ASC");
			endDate = CommonUtil.getConfiguredInstance().getFirstOrLastDateFromOperation(operationUid, "DDR", "DESC");
		}
		
		int noOfDays = 0;
		
		//GEt spud date in date only
		Calendar thisStartDateTimeObj = Calendar.getInstance();
		Calendar thisStartDateObj = Calendar.getInstance();
		
		thisStartDateTimeObj.setTime(startDate);
		
		thisStartDateObj.set(Calendar.YEAR, thisStartDateTimeObj.get(Calendar.YEAR));
		thisStartDateObj.set(Calendar.MONTH, thisStartDateTimeObj.get(Calendar.MONTH));
		thisStartDateObj.set(Calendar.DAY_OF_MONTH, thisStartDateTimeObj.get(Calendar.DAY_OF_MONTH));
		thisStartDateObj.set(Calendar.HOUR_OF_DAY, 0);
		thisStartDateObj.set(Calendar.MINUTE, 0);
		thisStartDateObj.set(Calendar.SECOND, 0);
		thisStartDateObj.set(Calendar.MILLISECOND, 0);
		
		startDate = thisStartDateObj.getTime();
		
		Calendar thisEndDateTimeObj = Calendar.getInstance();
		Calendar thisEndDateObj = Calendar.getInstance();
		
		thisEndDateTimeObj.setTime(endDate);
		
		thisEndDateObj.set(Calendar.YEAR, thisEndDateTimeObj.get(Calendar.YEAR));
		thisEndDateObj.set(Calendar.MONTH, thisEndDateTimeObj.get(Calendar.MONTH));
		thisEndDateObj.set(Calendar.DAY_OF_MONTH, thisEndDateTimeObj.get(Calendar.DAY_OF_MONTH));
		thisEndDateObj.set(Calendar.HOUR_OF_DAY, 0);
		thisEndDateObj.set(Calendar.MINUTE, 0);
		thisEndDateObj.set(Calendar.SECOND, 0);
		thisEndDateObj.set(Calendar.MILLISECOND, 0);
		
		endDate = thisEndDateObj.getTime();
		
		if(this.getServiceCode() != null)
		{
			//GET SERVICECODE
			String strInCondition = "";
			
			for(String value:this.serviceCode)
			{
				strInCondition = strInCondition + "'" + value.toString() + "',";
			}
			
			strInCondition = StringUtils.substring(strInCondition, 0, -1);
			strInCondition = StringUtils.upperCase(strInCondition);
			
			String strSql = "SELECT a.lookupCompanyUid,lc.companyName, lc.dunsNumber,rd.rigInformationUid,SUM(a.activityDuration)/3600.00, rd.daycost, a.companyServiceProvided FROM Activity a,LookupCompany lc,ReportDaily rd "
				+ " WHERE a.companyServiceProvided IN (" + strInCondition + ") "
				+ " AND rd.dailyUid = a.dailyUid AND a.lookupCompanyUid = lc.lookupCompanyUid "
				+ " AND a.operationUid = :operationUid AND rd.reportType = 'DDR' AND a.classCode = 'NPT' "
				+ " AND (a.isDeleted IS NULL OR a.isDeleted = FALSE) AND (a.isSimop IS NULL OR a.isSimop = FALSE) AND (a.isOffline IS NULL OR a.isOffline = FALSE) "
				+ " AND (a.carriedForwardActivityUid='' OR a.carriedForwardActivityUid IS NULL) "
				+ " AND (lc.isDeleted IS NULL OR lc.isDeleted = FALSE) "
				+ " AND (rd.isDeleted IS NULL OR rd.isDeleted = FALSE) "
				+ " AND rd.reportDatetime >=:startDate AND rd.reportDatetime <=:endDate "
				+ " GROUP BY a.lookupCompanyUid, a.companyServiceProvided "
				+ " ORDER BY lc.companyName, a.companyServiceProvided";
				
				
				String[] paramNames = {"operationUid","startDate","endDate"};
				Object[] paramValues = {operationUid,startDate,endDate};
				List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramNames,paramValues);
				
				
				String companyUid = "";
				String companyUid2 = "";
				String companyServiceProvided = "";
				String companyServiceProvided2 = "";
				String dailyUid = "";
				String dailyUid2 = "";
				String rigUid = "";
				String strRoot= "";
				String strRoot2= "";
				String strTemp = "";
				double totalOperationHour = 0.0;
				double sumHour = 24;
				String companySvcProvided = "";
				
				
				CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Operation.class, "days_spent_prior_to_spud");
				/*
				Double daysFromSpud=CommonUtil.getConfiguredInstance().daysFromSpudOnOperationProcurement(operationUid);
				
				if (daysFromSpud!=null && thisConverter.isUOMMappingAvailable()) {
					thisConverter.setBaseValue(daysFromSpud);
					daysFromSpud = thisConverter.getConvertedValue();
				}

				
				if(daysFromSpud!=null && operation.getDaysSpuddedPreviousWell()!=null)
				{
					diffValue = daysFromSpud - operation.getDaysSpuddedPreviousWell();
				}
				if(daysFromSpud!=null && operation.getDaysSpuddedPreviousWell()==null)
				{
					diffValue = daysFromSpud;
				}
				if(diffValue < 0)
				{
					diffValue=0.0;
				}*/
				List<Object> resultTotal = new ArrayList();
				List dailyUidList = CommonUtil.getConfiguredInstance().getDailyForOperationProcurement(operationUid,"DDR", startDate, endDate);
				if(dailyUidList!=null && dailyUidList.size()>0)
				{
					noOfDays = dailyUidList.size();
					totalOperationHour = dailyUidList.size() * 24;
				}
				
				
				//else
					// throw new Exception("No day in operation");
				
				
				double avgDailyCost = 0.0;
				if(noOfDays>0)
				{
					thisConverter.setReferenceMappingField(ReportDaily.class,"dayCost");
					Double sumDailyCost = CommonUtil.getConfiguredInstance().calculateCummulativeDayCost(startDate,endDate,operation.getOperationUid());
					if(sumDailyCost==null){
						sumDailyCost = 0.0;
					}
					if (sumDailyCost!=null && thisConverter.isUOMMappingAvailable()) {
						thisConverter.setBaseValue(sumDailyCost);
						sumDailyCost = thisConverter.getConvertedValue();
					}
					avgDailyCost = sumDailyCost/noOfDays;
				}
					

				String sqlTotalHour = "SELECT p.crewCompany,lc.companyName, p.dailyUid, p.companyServiceProvided FROM LookupCompany lc, PersonnelOnSite p, ReportDaily rd "
					+ "WHERE p.companyServiceProvided IN (" + strInCondition + ") "
					+ "AND p.pax!=0 "
					+ "AND p.crewCompany = lc.lookupCompanyUid "
					+ "AND rd.dailyUid = p.dailyUid "
					+ "AND p.operationUid = :operationUid "
					+ "AND (p.isDeleted IS NULL OR p.isDeleted=FALSE) "
					+ "AND (lc.isDeleted IS NULL OR lc.isDeleted=FALSE) "
					+ "AND (rd.isDeleted IS NULL OR rd.isDeleted=FALSE) "
					+ "AND rd.reportDatetime >=:startDate AND rd.reportDatetime <=:endDate "
					+ "GROUP BY p.crewCompany,p.companyServiceProvided, p.dailyUid "
					+ "ORDER BY lc.companyName, p.crewCompany,p.companyServiceProvided";
				
			List<Object> totalHour = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlTotalHour,paramNames,paramValues);
			if (totalHour.size()>0){
					
				for (int i = 0;i < totalHour.size();i++)
					{	
						if(i+1 < totalHour.size())
						{
							
							Object[] obj = (Object[]) totalHour.get(i);
							Object[] obj2 = (Object[]) totalHour.get(i+1);
							companyUid = (obj[0]!=null)?obj[0].toString():"";
							dailyUid = ((obj[2]!=null)?obj[2].toString():"");
							companyServiceProvided = (obj[3]!=null)?obj[3].toString():"";
							
							companyUid2 = (obj2[0]!=null)?obj2[0].toString():"";
							dailyUid2 = ((obj2[2]!=null)?obj2[2].toString():"");
							companyServiceProvided2 = (obj2[3]!=null)?obj2[3].toString():"";
							
							if (StringUtils.equals(companyUid, companyUid2) && !(StringUtils.equals(dailyUid,dailyUid2)) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
							{
								sumHour+= 24;
							}
							else 
							{
								Object[] objTemp = new Object[3];
								objTemp[0] = companyUid;
								objTemp[1] = sumHour;
								objTemp[2] = companyServiceProvided;
								resultTotal.add(objTemp);
								sumHour=24;
							}
						}
						else
						{
							Object[] obj = (Object[]) totalHour.get(i);
							companyUid = (obj[0]!=null)?obj[0].toString():"";
							dailyUid = ((obj[2]!=null)?obj[2].toString():"");
							companyServiceProvided = (obj[3]!=null)?obj[3].toString():"";
							
							Object[] objTemp = new Object[3];
							objTemp[0] = companyUid;
							
							if(StringUtils.equals(companyUid, companyUid2) && !(StringUtils.equals(dailyUid, dailyUid2)) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
							{	
								sumHour+= 24;
								objTemp[1] = sumHour;
								
							}
							else if(StringUtils.equals(companyUid, companyUid2) && StringUtils.equals(dailyUid, dailyUid2) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
							{
								objTemp[1] = sumHour;
							}
							else
							{
								sumHour= 24;
								objTemp[1] = sumHour;
							}
							objTemp[2] = companyServiceProvided;
							resultTotal.add(objTemp);
						}
					}
				}
				
				
				
				//double opTotalHour = CommonUtil.getConfiguredInstance().calculateActivityDurationByOperation(userContext.getUserSelection().getOperationUid());
				
				String strRootCause = "SELECT lc.lookupCompanyUid,rd.rigInformationUid,lr.name,a.companyServiceProvided FROM Activity a,LookupCompany lc,ReportDaily rd,LookupRootCauseCode lr "
				+ "WHERE a.companyServiceProvided IN (" + strInCondition + ") "
				+ "AND rd.dailyUid = a.dailyUid AND a.lookupCompanyUid = lc.lookupCompanyUid AND a.rootCauseCode = lr.shortCode "
				+ "AND a.operationUid = :operationUid AND rd.reportType = 'DDR' AND a.classCode = 'NPT' "
				+ "AND (a.isDeleted IS NULL OR a.isDeleted=FALSE) AND (a.isSimop IS NULL OR a.isSimop=FALSE) AND (a.isOffline IS NULL OR a.isOffline=FALSE) "
				+ "AND (a.carriedForwardActivityUid='' OR a.carriedForwardActivityUid IS NULL) AND (lr.isDeleted IS NULL OR lr.isDeleted=FALSE) "
				+ "AND (lc.isDeleted IS NULL OR lc.isDeleted=FALSE) "
				+ "AND (rd.isDeleted IS NULL OR rd.isDeleted=FALSE) "
				+ "AND (lr.isActive IS NULL OR lr.isActive=TRUE) "
				+ "AND (lr.operationCode = :operationCode OR lr.operationCode IS NULL OR lr.operationCode = '') "
				+ "AND (lr.country = :country OR lr.country IS NULL OR lr.country = '') "
				+ "AND rd.reportDatetime >=:startDate AND rd.reportDatetime <=:endDate "
				+ "GROUP BY a.lookupCompanyUid,a.companyServiceProvided, a.rootCauseCode ORDER BY lc.companyName, a.companyServiceProvided, rd.rigInformationUid, lr.name";

				String strMaterialGroup = "SELECT lc.lookupCompanyUid,lcs.code,a.companyServiceProvided FROM Activity a,LookupCompany lc,ReportDaily rd,LookupCompanyService lcs "
				+ "WHERE a.companyServiceProvided IN (" + strInCondition + ") "
				+ "AND rd.dailyUid = a.dailyUid AND a.lookupCompanyUid = lc.lookupCompanyUid AND lc.lookupCompanyUid = lcs.lookupCompanyUid "
				+ "AND a.operationUid = :operationUid AND rd.reportType = 'DDR' AND a.classCode = 'NPT' "
				+ "AND (a.isDeleted IS NULL OR a.isDeleted=FALSE) AND (a.isSimop IS NULL OR a.isSimop=FALSE) AND (a.isOffline IS NULL OR a.isOffline=FALSE) AND (lcs.code IS NOT NULL) "
				+ "AND (a.carriedForwardActivityUid='' OR a.carriedForwardActivityUid IS NULL) "
				+ "AND (lc.isDeleted IS NULL OR lc.isDeleted=FALSE) "
				+ "AND (lcs.isDeleted IS NULL OR lcs.isDeleted=FALSE) "
				+ "AND (rd.isDeleted IS NULL OR rd.isDeleted=FALSE) "
				+ "AND rd.reportDatetime >=:startDate AND rd.reportDatetime <=:endDate "
				+ "GROUP BY a.lookupCompanyUid,a.companyServiceProvided, lcs.code ORDER BY lc.companyName,a.companyServiceProvided, lcs.code";
				
				Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(userContext.getUserSelection().getWellUid());
				
				String[] paramNamesRootCause = {"operationUid", "operationCode", "country","startDate","endDate"};
				Object[] paramValuesRootCause = {operationUid, operation.getOperationCode(), well.getCountry(),startDate,endDate};
				List<Object> rootCause = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strRootCause, paramNamesRootCause, paramValuesRootCause);
				List<Object> materialGroup = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strMaterialGroup, paramNames, paramValues);
				List<Object> rootCauseList = new ArrayList();
				List<Object> materialList = new ArrayList();
				Map<String,LookupItem> serviceLookup= LookupManager.getConfiguredInstance().getLookup("xml://company.service?key=code&amp;value=label", userContext.getUserSelection(), null);
				
				if (rootCause.size()>0)
				{
					for(int i = 0;i < rootCause.size();i++)
					{
						if(i+1 < rootCause.size())
						{
							Object[] obj = (Object[]) rootCause.get(i);
							Object[] obj2 = (Object[]) rootCause.get(i+1);
							companyUid = (obj[0]!=null)?obj[0].toString():"";
							strRoot = ((obj[2]!=null)?obj[2].toString():"");
							companyServiceProvided = (obj[3]!=null)?obj[3].toString():"";
							
							companyUid2 = (obj2[0]!=null)?obj2[0].toString():"";
							strRoot2 = ((obj2[2]!=null)?obj2[2].toString():"");
							companyServiceProvided2 = (obj2[3]!=null)?obj2[3].toString():"";
							
							
							if (StringUtils.equals(companyUid, companyUid2) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
							{
								if(StringUtils.isBlank(strTemp))
								strTemp = strTemp.concat(strRoot);
								else
								strTemp = strTemp.concat(", " + strRoot);
							}
							else 
							{
								Object[] objTemp = new Object[3];
								objTemp[0] = companyUid;
								
								if(i!=0){
									if(StringUtils.isBlank(strTemp))
										strTemp = strTemp.concat(strRoot);
									else
										strTemp = strTemp.concat(", " + strRoot);
									objTemp[1] = strTemp;
								}
								else
								objTemp[1] = strRoot;
								objTemp[2] = companyServiceProvided;
								rootCauseList.add(objTemp);
								strTemp = "";
							}
						}
						else
						{
							Object[] obj = (Object[]) rootCause.get(i);
							companyUid = (obj[0]!=null)?obj[0].toString():"";
							strRoot = ((obj[2]!=null)?obj[2].toString():"");
							companyServiceProvided = (obj[3]!=null)?obj[3].toString():"";
							Object[] objTemp = new Object[3];
							objTemp[0] = companyUid;
							if(StringUtils.equals(companyUid, companyUid2) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
							{
								if(StringUtils.isBlank(strTemp))
								strTemp = strTemp.concat(strRoot);
								else
								strTemp = strTemp.concat(", " + strRoot);
								
								objTemp[1] = strTemp;
								
							}
							else
							{
								objTemp[1] = strRoot;
							}
							objTemp[2] = companyServiceProvided;
							rootCauseList.add(objTemp);
						}
					}
				}
				strTemp="";
				if (materialGroup.size()>0)
				{
					for(int i = 0;i < materialGroup.size();i++)
					{
						if(i+1 < materialGroup.size())
						{
							Object[] obj = (Object[]) materialGroup.get(i);
							Object[] obj2 = (Object[]) materialGroup.get(i+1);
							companyUid = (obj[0]!=null)?obj[0].toString():"";
							strRoot = getLookupValue(serviceLookup,((obj[1]!=null)?obj[1].toString():""));
							companyServiceProvided = (obj[2]!=null)?obj[2].toString():"";
							
							companyUid2 = (obj2[0]!=null)?obj2[0].toString():"";
							strRoot2 = getLookupValue(serviceLookup,((obj2[1]!=null)?obj2[1].toString():""));
							companyServiceProvided2 = (obj2[2]!=null)?obj2[2].toString():"";
							
							if (StringUtils.equals(companyUid, companyUid2) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
							{
								if(StringUtils.isBlank(strTemp))
								strTemp = strTemp.concat(strRoot);
								else
									if (!StringUtils.isBlank(strRoot))
										strTemp = strTemp.concat(", " + strRoot);
							}
							else 
							{
								Object[] objTemp = new Object[3];
								objTemp[0] = companyUid;
								if(i!=0){
									if(StringUtils.isBlank(strTemp))
										strTemp = strTemp.concat(strRoot);
									else
										if (!StringUtils.isBlank(strRoot))
											strTemp = strTemp.concat(", " + strRoot);
									objTemp[1] = strTemp;
								}
								else
								objTemp[1] = strRoot;
								objTemp[2] = companyServiceProvided;
								materialList.add(objTemp);
								strTemp = "";
							}
						}
						else
						{
							Object[] obj = (Object[]) materialGroup.get(i);
							companyUid = (obj[0]!=null)?obj[0].toString():"";
							strRoot = getLookupValue(serviceLookup,((obj[1]!=null)?obj[1].toString():""));
							companyServiceProvided = (obj[2]!=null)?obj[2].toString():"";
							
							Object[] objTemp = new Object[3];
							objTemp[0] = companyUid;
							
							if(StringUtils.equals(companyUid, companyUid2) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
							{
								if(StringUtils.isBlank(strTemp))
								strTemp = strTemp.concat(strRoot);
								else
									if (!StringUtils.isBlank(strRoot))
										strTemp = strTemp.concat(", " + strRoot);
								
								objTemp[1] = strTemp;
							}
							else
							{
								objTemp[1] = strRoot;
							}
							objTemp[2] = companyServiceProvided;
							materialList.add(objTemp);
						}
					}
				} 
								

					
					String companyName;
					String dunsNumber;
					String rigName;
					String wellName;
					String material ="";
					String rootCauseStr= "";
					String dayCostUomSymbol = "";
					double totalHourCompany = 0;
					double totalHourNPT = 0; 
					double nptRelatedCost = 0;
					double nptCompany;
					double nptOperation;
					LookupItem lookupItem = null;
					
					thisConverter.setReferenceMappingField(ReportDaily.class,"dayCost");
					
					if( thisConverter.isUOMMappingAvailable() ){
						dayCostUomSymbol= thisConverter.getUomSymbol();
					}
					
					
					rigUid = operation.getRigInformationUid();
					
					
					for(Object o: lstResult)
					{
						totalHourCompany = 0;
						rootCauseStr = "";
						material = "";
						Object[] obj = (Object[]) o;
						companyUid = (obj[0]!=null)?obj[0].toString():"";
						companyName = (obj[1]!=null)?obj[1].toString():"";
						companyServiceProvided = (obj[6]!=null)?obj[6].toString():"";
						dunsNumber = (obj[2]!=null)?obj[2].toString():"";
						RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class,rigUid);
						rigName = rig.getRigName();
						wellName = well.getWellName();
						
						for(Object o2: resultTotal)
						{
							Object[] obj2 = (Object[]) o2;
							companyUid2 = (obj2[0]!= null)?obj2[0].toString():"";
							companyServiceProvided2 = (obj2[2]!= null)?obj2[2].toString():"";
							if(StringUtils.equals(companyUid,companyUid2) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
							{
								totalHourCompany = Double.parseDouble(obj2[1].toString());
								break;
							}
						}
						for(Object o2: rootCauseList)
						{
							Object[] obj2 = (Object[]) o2;
							companyUid2 = (obj2[0]!= null)?obj2[0].toString():"";
							companyServiceProvided2 = (obj2[2]!= null)?obj2[2].toString():"";
							if(StringUtils.equals(companyUid,companyUid2) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
							{
								rootCauseStr = obj2[1].toString();
								break;
							}
						}
						for(Object o2: materialList)
						{
							Object[] obj2 = (Object[]) o2;
							companyUid2 = (obj2[0]!= null)?obj2[0].toString():"";
							companyServiceProvided2 = (obj2[2]!= null)?obj2[2].toString():"";
							if(StringUtils.equals(companyUid,companyUid2) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
							{
								material = obj2[1].toString();
								break;
							}
						}
						DecimalFormat twoDigit = new DecimalFormat("#,##0.00");
						totalHourNPT = Double.valueOf(twoDigit.format(Double.parseDouble(obj[4].toString())));
						if(avgDailyCost != 0)
							nptRelatedCost = (avgDailyCost/24) * totalHourNPT;
						else
							nptRelatedCost = 0;
						if(totalHourCompany != 0)
							nptCompany = (totalHourNPT*100)/totalHourCompany;
						else
							nptCompany = 0;
						if(totalOperationHour!= 0)
							nptOperation = (totalHourNPT*100)/totalOperationHour;
						else
							nptOperation = 0;
						
						
					
						if(companyServiceProvided!=null)
						{
							lookupItem = serviceLookup.get(companyServiceProvided);
							if (lookupItem != null) {
								Object val = lookupItem.getValue();
								companySvcProvided = val != null ? val.toString() : "";
							}
						}
						
						ReportDataNode thisReportNode = reportDataNode.addChild("NPT");
					    thisReportNode.addProperty("supplierName", companyName);
					    thisReportNode.addProperty("serviceProvided", companySvcProvided);
					    thisReportNode.addProperty("supplierID", companyUid);
						if(!StringUtils.isBlank(dunsNumber))
							thisReportNode.addProperty("dunsNumber", dunsNumber);
						else
							thisReportNode.addProperty("dunsNumber", "");
					    thisReportNode.addProperty("rigName", rigName);
					    thisReportNode.addProperty("wellName", wellName);
						if(thisConverter.isUOMMappingAvailable() ){
							thisReportNode.addProperty("dayCostUomSymbol", thisConverter.getUomSymbol() );
						}
					    thisReportNode.addProperty("totalNumberOfHoursByCompany", Double.toString(totalHourCompany));
					    thisReportNode.addProperty("material", material);
					    thisReportNode.addProperty("rootCause", rootCauseStr);
					    thisReportNode.addProperty("totalNPTHour", Double.toString(totalHourNPT));
					    thisReportNode.addProperty("totalOperationHour", Double.toString(totalOperationHour));
					    thisReportNode.addProperty("avgDailyCost", Double.toString(avgDailyCost));
					    thisReportNode.addProperty("nptRelatedCost", Double.toString(nptRelatedCost));
					    thisReportNode.addProperty("nptCompany", Double.toString(nptCompany));
					    thisReportNode.addProperty("nptOperation", Double.toString(nptOperation));
					    
					} 
					
			}
			ReportDataNode thisReportNode = reportDataNode.addChild("DateRange");
			thisReportNode.addProperty("startDate", ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(startDate));
		    thisReportNode.addProperty("endDate", ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(endDate));
		}
	
	
	private String getLookupValue(Map<String,LookupItem> lookupList, Object lookupvalue) throws Exception {
		String retValue = "";
		
		if (lookupvalue !=null && StringUtils.isNotBlank(lookupvalue.toString())  && lookupList !=null){
			 LookupItem lookup = lookupList.get(lookupvalue.toString());
			if (lookup !=null)	retValue = lookup.getValue().toString();					
		} 
		
		return retValue;
	}
	
}

		
