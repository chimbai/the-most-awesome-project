package com.idsdatanet.d2.drillnet.formation;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


/**
 * All common utility related to Foramtion
 * @author Robin
 *
 */

public class FormationUtils {
	
	/**
	 * sort formation records based on the GWP selected
	 * @param userSelection
	 * @return
	 * @throws Exception
	 */
	
	public static String sortPreference(UserSelectionSnapshot userSelection, String viewType) throws Exception {
		
		if("drilling".equalsIgnoreCase(viewType) && StringUtils.isNotBlank(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_FORMATION_SORT_PREFERENCE))){
			return GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_FORMATION_SORT_PREFERENCE);
		}
		else if ("geology".equalsIgnoreCase(viewType) && StringUtils.isNotBlank(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_FORMATION_SORT_PREFERENCE_GEOL))){
			return GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_FORMATION_SORT_PREFERENCE_GEOL);
		}
		return null;
	}
	/**
	 * auto populate PrognosedTopTvdMsl and SampleTopTvdMsl from prognosedTopMdMsl and sampleTopMdMsl when Wellbore.wellboreShape = 'Vertical'
	 * @param wellbore
	 * @param session
	 * @param commandBean
	 * @throws Exception
	 */
	public static void autoPopulateTvdFromMd(String wellboreUid, String wellboreShape, UserSession session, CommandBean commandBean) throws Exception {
		if (wellboreShape!=null && wellboreShape.equalsIgnoreCase("vertical")){
			if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "geolformationautoPopulationTVD"))){
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
				String sqlFormation = "FROM Formation " +
						"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
						"AND wellboreUid=:wellboreUid";
				List<Formation> listFormation = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlFormation, "wellboreUid", wellboreUid);
				
				if(listFormation.size()>0){
					for(Formation thisFormation : listFormation){
						Double prognosedTopMdMsl = thisFormation.getPrognosedTopMdMsl();
						thisConverter.setReferenceMappingField(Formation.class, "prognosedTopMdMsl");
						if (prognosedTopMdMsl!=null && thisConverter.isUOMMappingAvailable()) {
							thisConverter.setBaseValueFromUserValue(prognosedTopMdMsl);
							thisConverter.addDatumOffset();
							prognosedTopMdMsl = thisConverter.getBasevalue();
						}
						thisConverter.setReferenceMappingField(Formation.class, "prognosedTopTvdMsl");
						if (prognosedTopMdMsl!=null){
							thisConverter.setBaseValue(prognosedTopMdMsl);
							prognosedTopMdMsl = thisConverter.getBasevalue();
							prognosedTopMdMsl = thisConverter.getConvertedValue();
							thisFormation.setPrognosedTopTvdMsl(thisConverter.getConvertedValue());
						}
						
						Double sampleTopMdMsl = thisFormation.getSampleTopMdMsl();
						thisConverter.setReferenceMappingField(Formation.class, "sampleTopMdMsl");
						if (sampleTopMdMsl!=null && thisConverter.isUOMMappingAvailable()) {
							thisConverter.setBaseValueFromUserValue(sampleTopMdMsl);
							thisConverter.addDatumOffset();
							sampleTopMdMsl = thisConverter.getBasevalue();
						}
						thisConverter.setReferenceMappingField(Formation.class, "sampleTopTvdMsl");
						if (sampleTopMdMsl!=null){
							thisConverter.setBaseValue(sampleTopMdMsl);
							thisFormation.setSampleTopTvdMsl(thisConverter.getConvertedValue());
						}
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisFormation);
					}
				}
			}
		}
	}
	
}
