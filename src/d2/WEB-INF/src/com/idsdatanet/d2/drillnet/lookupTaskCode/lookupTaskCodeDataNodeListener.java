package com.idsdatanet.d2.drillnet.lookupTaskCode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.model.LookupTaskCode;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class lookupTaskCodeDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	
	private static String SORTORDER = "ASC";
	
	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		String selectedOperationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
		String selectedPhaseCode = (String) commandBean.getRoot().getDynaAttr().get("phaseCodeFilter");
		String selectedOperationalUnit = (String) commandBean.getRoot().getDynaAttr().get("operationalUnitFilter");
		String selectedCommonFilter = (String) commandBean.getRoot().getDynaAttr().get("commonFilter");
		
		Object object = node.getData();
		if (object instanceof LookupTaskCode) {
			LookupTaskCode data = (LookupTaskCode) object;
			data.setOperationCode(selectedOperationCode);
			data.setPhaseShortCode(selectedPhaseCode);
			data.setOperationalUnit(selectedOperationalUnit);
			data.setCommonFilter(selectedCommonFilter);
		}
	}
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {

	}

	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
			
		if (meta.getTableClass().equals(LookupTaskCode.class)) {
			String selectedOperationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
			String selectedPhaseCode = (String) commandBean.getRoot().getDynaAttr().get("phaseCodeFilter");
			String selectedOperationalUnit = (String) commandBean.getRoot().getDynaAttr().get("operationalUnitFilter");
			String selectedCommonFilter = (String) commandBean.getRoot().getDynaAttr().get("commonFilter");
			if (selectedOperationCode==null) selectedOperationCode = "";
			
			String strSql = "FROM LookupTaskCode WHERE operationCode = :operationCode AND (isDeleted IS NULL OR isDeleted = FALSE)";
			List arrayParamNames = new ArrayList();
			List arrayParamValues = new ArrayList();
			
			arrayParamNames.add("operationCode");
			arrayParamValues.add(selectedOperationCode);

			if(StringUtils.isNotBlank(selectedOperationalUnit)){
				strSql = strSql + " AND operationalUnit =:operationalUnit ";
				arrayParamNames.add("operationalUnit");
				arrayParamValues.add(selectedOperationalUnit);
			}
			if(StringUtils.isNotBlank(selectedCommonFilter)){
				strSql = strSql + " AND commonFilter =:commonFilter ";
				arrayParamNames.add("commonFilter");
				arrayParamValues.add(selectedCommonFilter);
			}
			if(StringUtils.isNotBlank(selectedPhaseCode)){
				strSql = strSql + " AND phaseShortCode =:phaseShortCode ";
				arrayParamNames.add("phaseShortCode");
				arrayParamValues.add(selectedPhaseCode);
			}
			
			String[] paramNames =  (String[]) arrayParamNames.toArray(new String [arrayParamNames.size()]);
			Object[] paramValues = arrayParamValues.toArray();
			
			List<LookupTaskCode> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			List<Object> output_maps = new ArrayList<Object>();
			
			Collections.sort(items, new LookupTaskCodeComparator());
			
			for(Object objResult: items){
				LookupTaskCode thisLookupTaskCode = (LookupTaskCode) objResult;
				output_maps.add(thisLookupTaskCode);
			}
						
			return output_maps;
		}
	
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}
	
	private class LookupTaskCodeComparator implements Comparator<LookupTaskCode>{
		public int compare(LookupTaskCode o1, LookupTaskCode o2){
			try {				
				String f1 = WellNameUtil.getPaddedStr(o1.getShortCode().toString());
				String f2 = WellNameUtil.getPaddedStr(o2.getShortCode().toString());
				
				if (f1 == null || f2 == null) return 0;
				if ("DESC".equalsIgnoreCase(SORTORDER)) {
					return f2.compareTo(f1);
				} else {
					return f1.compareTo(f2);
				}				
			} catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
	}	
}
