package com.idsdatanet.d2.drillnet.partnerportal;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.ReportFiles;

public class BasePublicReportList {
	protected ReportFiles thisReportFiles;
	protected String operationName;
	protected String reportType;
	
	public void setData(ReportFiles reportFiles, String operationName){
		this.thisReportFiles = reportFiles;
		this.operationName = operationName;
		this.reportType = reportFiles.getReportType() ;
	}
	
	public String getReportDisplayName(){
		if(StringUtils.isNotBlank(this.thisReportFiles.getDisplayName())) return this.thisReportFiles.getDisplayName();
		return this.operationName + "_" + this.thisReportFiles.getReportDayNumber() + ".pdf";
	}
	
	public String getReportFilesUid(){
		return this.thisReportFiles.getReportFilesUid();
	}
	
	public Date getReportDate(){
		return this.thisReportFiles.getReportDayDate();
	}
}
