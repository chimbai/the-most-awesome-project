package com.idsdatanet.d2.drillnet.coiledTubing;

import java.util.List;
import java.util.Date;

import com.idsdatanet.d2.core.model.CoiledTubing;
import com.idsdatanet.d2.core.model.CoiledTubingDetails;
import com.idsdatanet.d2.core.model.CoiledTubingWallThickness;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class CoiledTubingUtils {
	
	//set the start date of the CT run to the earliest CT Log Date In in the coiled tubing details
	public static void setCoiledTubingStartDate(CoiledTubing coiledTubing) throws Exception
	{
		Date StartDate;
		String paramsFields = "thisCtUid";
 		Object paramsValues = new Object();
		paramsValues = coiledTubing.getCoiledTubingUid();
		String strSql = "FROM CoiledTubingDetails where coiledTubingUid = :thisCtUid and (isDeleted = false or isDeleted is null) order by logDateIn asc";
		String strSql2 = "FROM CoiledTubing where coiledTubingUid = :thisCtUid and (isDeleted = false or isDeleted is null)";
		List<CoiledTubingDetails> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		List<CoiledTubing> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);
		if (!list.isEmpty()) {
			CoiledTubingDetails thisCoiledTubingDetails = list.get(0);
			StartDate = thisCoiledTubingDetails.getLogDateIn();
			if(!list2.isEmpty())
			{
				CoiledTubing thisCoiledTubing = list2.get(0);
				thisCoiledTubing.setRunDateStart(StartDate);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisCoiledTubing);
			}
		}
	}
	
	//set the end date of the CT run to the latest CT Log Date Out in the coiled tubing details
	public static void setCoiledTubingEndDate(CoiledTubing coiledTubing) throws Exception
	{
		Date endDate;
		String paramsFields = "thisCtUid";
 		Object paramsValues = new Object();
		paramsValues = coiledTubing.getCoiledTubingUid();
		String strSql = "FROM CoiledTubingDetails where coiledTubingUid = :thisCtUid and (isDeleted = false or isDeleted is null) order by logDateOut desc";
		String strSql2 = "FROM CoiledTubing where coiledTubingUid = :thisCtUid and (isDeleted = false or isDeleted is null)";
		List<CoiledTubingDetails> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		List<CoiledTubing> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);
		if (!list.isEmpty()) {
			CoiledTubingDetails thisCoiledTubingDetails = list.get(0);
			endDate = thisCoiledTubingDetails.getLogDateOut();
			if(!list2.isEmpty())
			{
				CoiledTubing thisCoiledTubing = list2.get(0);
				thisCoiledTubing.setRunDateEnd(endDate);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisCoiledTubing);
			}
		}
	}
	
	//calculate the total of running footage of coiled tubing details and set the total in the CT Run Footage of CT run
	public static void setCoiledTubingFootage(CoiledTubing coiledTubing) throws Exception
	{
		String paramsFields = "thisCtUid";
 		Object paramsValues = new Object();
		paramsValues = coiledTubing.getCoiledTubingUid();
		String strSql = "FROM CoiledTubingDetails where coiledTubingUid = :thisCtUid and (isDeleted = false or isDeleted is null) and ftgRunDetail is not null";
		String strSql2 = "FROM CoiledTubing where coiledTubingUid = :thisCtUid and (isDeleted = false or isDeleted is null)";
		List<CoiledTubingDetails> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		List<CoiledTubing> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);
		if (!list.isEmpty()) {
			double count = 0.0;
			for (int i =0; i < list.size(); i++)
			{
				count = count + list.get(i).getFtgRunDetail();
			}
			if(!list2.isEmpty())
			{
				CoiledTubing thisCoiledTubing = list2.get(0);
				thisCoiledTubing.setFtgRun(count);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisCoiledTubing);
			}
		}
	}
	
	//calculate the total length of CT Wall Thickness and set it to the Total Length of coiled tubing details
	public static void setCoiledTubingDetailsTotalLength(CoiledTubingDetails coiledTubingDetails) throws Exception {
		String paramsFields = "thisCtDetailsUid";
 		Object paramsValues = new Object();
		paramsValues = coiledTubingDetails.getCoiledTubingDetailsUid();
		String strSql = "FROM CoiledTubingWallThickness where coiledTubingDetailsUid = :thisCtDetailsUid and (isDeleted = false or isDeleted is null) and wallLength is not null";
		String strSql2 = "FROM CoiledTubingDetails where coiledTubingDetailsUid = :thisCtDetailsUid and (isDeleted = false or isDeleted is null)";
		List<CoiledTubingWallThickness> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		List<CoiledTubingDetails> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);
		if (!list.isEmpty()) {
			double count = 0.0;
			for (int i =0; i < list.size(); i++)
			{
				count = count + list.get(i).getWallLength();
			}
			if(!list2.isEmpty())
			{
				CoiledTubingDetails thisCoiledTubingDetails = list2.get(0);
				thisCoiledTubingDetails.setTotalLength(count);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisCoiledTubingDetails);
			}
		}
		
	}
}
