package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;


public class BHIRootCauseLookupHandler implements LookupHandler{

	private String uriNptCategory;
	private String uriNptSubCategory;
	
	private Boolean useShortCategory=false;
	private Boolean useShortSubCategory=false;
	private String separator=":";
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		// TODO Auto-generated method stub
		Map<String,LookupItem> result = new LinkedHashMap<String,LookupItem>();
		if (StringUtils.isNotBlank(this.getUriNptCategory()))
		{
			Map<String,LookupItem> catList = LookupManager.getConfiguredInstance().getLookup(this.getUriNptCategory(), userSelection, null);
			for (Map.Entry<String, LookupItem> entry : catList.entrySet())
			{
				String catShortCode = entry.getKey();
				String catDescription = entry.getValue().getValue().toString();
				Map<String,LookupItem> subCatList = LookupManager.getConfiguredInstance().getCascadeLookup(this.getUriNptSubCategory(), userSelection,catShortCode , null);
				for (Map.Entry<String,LookupItem> entrySub : subCatList.entrySet())
				{
					String subCatShortCode = entrySub.getKey();
					String subCatDescription = entrySub.getValue().getValue().toString();
					
					String key = catShortCode+this.getSeparator()+subCatShortCode;
					String label = (useShortCategory?catShortCode:catDescription)+this.getSeparator()+(useShortSubCategory?subCatShortCode:subCatDescription);
					result.put(key, new LookupItem(key,label));
				}
			}
		}
		return result;
	}

	public void setUriNptCategory(String uriNptCategory) {
		this.uriNptCategory = uriNptCategory;
	}

	public String getUriNptCategory() {
		return uriNptCategory;
	}

	public void setUriNptSubCategory(String uriNptSubCategory) {
		this.uriNptSubCategory = uriNptSubCategory;
	}

	public String getUriNptSubCategory() {
		return uriNptSubCategory;
	}	

	public Boolean getUseShortCategory() {
		return useShortCategory;
	}

	public void setUseShortCategory(Boolean useShortCategory) {
		this.useShortCategory = useShortCategory;
	}

	public Boolean getUseShortSubCategory() {
		return useShortSubCategory;
	}

	public void setUseShortSubCategory(Boolean useShortSubCategory) {
		this.useShortSubCategory = useShortSubCategory;
	}

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	

}
