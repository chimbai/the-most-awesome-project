package com.idsdatanet.d2.drillnet.rigpump;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.RigPump;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;



public class RigPumpDataNodeLoadHandler implements DataNodeLoadHandler{

	public String reportNumber = null;
	
	public void setReportNumber (String value){
	reportNumber = value;
	}
	
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		if(Mud.class.equals(meta.getTableClass())) return true;
		else
		return false;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		
		if (meta.getTableClass().equals(Mud.class)) {
			List<Object> output_maps = new ArrayList<Object>();
			
			Object object = node.getData();
			
			if(object instanceof RigPump){
			RigPump thisRigPump = (RigPump)object;
			Mud data = new Mud();
			data.setRigPumpUid(thisRigPump.getRigPumpUid());
			}
			
			
			String strSql ="from MudProperties " +
				"where (isDeleted=false or isDeleted is null) " +
				"and dailyUid=:dailyUid " +
				"order by reportTime desc";
			List<MudProperties> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[]{"dailyUid"}, new Object[]{userSelection.getDailyUid()});
			
			if(list!=null && list.size()>0){
				MudProperties thisMudProperties = list.get(0);
				Object mud = new Mud();		
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
				thisConverter.setReferenceMappingField(MudProperties.class, "mudWeight");
				if (thisConverter.isUOMMappingAvailable()){
					commandBean.setCustomUOM(Mud.class,"mudWeight", thisConverter.getUOMMapping());
				}
					thisConverter.setReferenceMappingField(MudProperties.class, "depthMdMsl");
				if (thisConverter.isUOMMappingAvailable()){
					commandBean.setCustomUOM(Mud.class,"depthMdMsl", thisConverter.getUOMMapping());
				}
				
				PropertyUtils.copyProperties(mud, thisMudProperties);
				
				output_maps.add(mud);
				
			}
			return output_maps;
		}
		return null;
	}

}
