package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.OperationPlanDetail;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.security.acl.ACLKeyResolver;

public interface DynamicLookaheadDVDPlan {
	
	public void processDVDPlanPhaseActualData(List<String> planReferenceUids) throws Exception;
	
	public Double calculateActualDuration(OperationPlanPhase record, String operationUid) throws Exception;
	
	public Double calculateActualDepth(OperationPlanPhase record, String operationUid) throws Exception;
	
	public Double calculateActualDurationTask(OperationPlanTask record, String operationUid) throws Exception;
	
	public Double calculateActualDepthTask(OperationPlanTask record, String operationUid) throws Exception;
	
	public Double calculateActualDurationDetail(OperationPlanDetail record, String operationUid) throws Exception;
	
	public Double calculateActualDepthDetail(OperationPlanDetail record, String operationUid) throws Exception;
	
	public Date calculateActualStartDatetime(OperationPlanPhase record) throws Exception;
	
	public Date calculateActualEndDatetime(OperationPlanPhase record) throws Exception;
	
	public String calculateDayPlusMinus(Double actualDuration, Double plannedDuration) throws Exception;
	
	public Double calculateActualCost(OperationPlanPhase record, String operationUid, HttpServletRequest request) throws Exception;
	
	public Double calculateNptDuration(String operationPlanPhaseUid, String operationUid) throws Exception;
	
	public Double calculateNptDurationTask(String operationPlanTaskUid, String operationUid) throws Exception;
	
	public Double calculateNptDurationDetail(String operationPlanDetailUid, String operationUid) throws Exception;
	
}

