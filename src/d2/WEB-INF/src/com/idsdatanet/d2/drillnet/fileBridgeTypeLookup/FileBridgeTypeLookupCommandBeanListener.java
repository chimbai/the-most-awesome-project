package com.idsdatanet.d2.drillnet.fileBridgeTypeLookup;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.depot.d2.filebridge.util.FileBridgeUtils;

public class FileBridgeTypeLookupCommandBeanListener extends EmptyCommandBeanListener {

	@Override
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		String errorMsg = FileBridgeUtils.checkEnabledFileBridgeJob();
		if(StringUtils.isNotBlank(errorMsg)) {
			commandBean.getSystemMessage().addError(errorMsg, true);
		}
	}
}
