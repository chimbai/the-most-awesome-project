package com.idsdatanet.d2.drillnet.opsDatum;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class datumReferencePointLookupHandler implements LookupHandler {

	private String defaultUri;
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> lookupList = LookupManager.getConfiguredInstance().getLookup(this.defaultUri, userSelection, new LookupCache());
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String hql = "select onOffShore, country from Well where wellUid = :wellUid";
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "wellUid", userSelection.getWellUid());
		List<Object[]> mapList = new ArrayList();
		for (Map.Entry<String, LookupItem> lookupItem: lookupList.entrySet()) {
			mapList.add(new Object[] {lookupItem.getKey(), lookupItem.getValue()});
		}
		
		if(list.size() != 0)
		{
			for(Object[] obj: list){
				if ("ON".equals(obj[0]))
				{
					if("Australia".equals(obj[1]) || "AU".equals(obj[1])){
						LookupItem item = new LookupItem("AHD","AHD");
						result.put("AHD", item);
					}
					
					LookupItem item = new LookupItem("GL","GL");
					result.put("GL", item);
					
					item = new LookupItem("CHF","CHF");
					result.put("CHF", item);
					
					item = new LookupItem("MSL","MSL");
					result.put("MSL", item);
					
					item = new LookupItem("WH","WH");
					result.put("WH", item);
					
					if(userSelection.getOperationType().equals("SVC")||userSelection.getOperationType().equals("INTV")||userSelection.getOperationType().equals("WKO")){
						item = new LookupItem("TH","TH");
						result.put("TH", item);
					}

				}else{
					for(Iterator i = mapList.iterator(); i.hasNext(); ){
						Object[] obj_array = (Object[]) i.next();
						LookupItem item = (LookupItem) obj_array[1];
						if (!result.containsKey(item.getKey())) {
							if ("GL".equals(item.getKey())) continue;
							if ("AHD".equals(item.getKey())){
								if("Australia".equals(obj[1]) || "AU".equals(obj[1])){
									result.put(item.getKey(), item);
								}
							}else if("TH".equals(item.getKey())){
								if(userSelection.getOperationType().equals("SVC")||userSelection.getOperationType().equals("INTV")||userSelection.getOperationType().equals("WKO")){
									result.put(item.getKey(), item);
								}
							}else{
								result.put(item.getKey(), item);
							}
						}
					}
				}
			}
					
		}
//		for (Object[] obj : list) {
//			BasinFormation basinFormation = (BasinFormation) obj[0];
//			LookupItem item = new LookupItem(basinFormation.getName(), basinFormation.getName());
//			result.put(basinFormation.getName(), item);
//		}
		return result;
	}
	
	public void setDefaultUri(String defaultUri) {
		this.defaultUri = defaultUri;
	}
	
	public String getDefaultUri() {
		return this.defaultUri;
	}

}
