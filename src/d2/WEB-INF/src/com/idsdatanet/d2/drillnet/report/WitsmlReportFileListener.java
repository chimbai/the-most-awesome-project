package com.idsdatanet.d2.drillnet.report;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.depot.witsml.DepotPremierManager;
import com.idsdatanet.d2.drillnet.report.ReportFileListener;


/**
 * This class will act to call the Depot Witsml Manager and consequently send the data in DOR report to Premier Indo's sandbox  
 * @author nlau
 *
 */

public class WitsmlReportFileListener implements ReportFileListener{

	private String dailyUid = "";
	private String isPrivate = "";
	private String reportFilesUid = "";

	WitsmlReportFileListener(){}
		
	public void onReportGenerated(ReportFiles reportFiles, BPAbstractReportModule reportModule){
		
		if (reportFiles!= null)
		dailyUid = reportFiles.getDailyUid();
	    String isPrivate;
		try {
			isPrivate = this.getIsPrivate(reportFiles);
			
			if (StringUtils.isNotBlank(isPrivate)){

				if(reportModule!= null){
				/*If depotSyncPublicReport is true and is 'Public', trigger depot */
					if(reportModule.getDepotSyncPublicReport() && isPrivate.equals("false")){
					/*Trigger the depot job via the depot manager*/
						DepotPremierManager.getConfiguredInstance().postOpsReportEvent(dailyUid);
				
				}	
			  }
			}
		} catch (Exception e1) {
			
			e1.printStackTrace();
		}
		
	}
	
	
	public void onUpdateReport(ReportFiles reportFiles){
		//do nothing
	}
	
	public String getIsPrivate(ReportFiles reportFiles) throws Exception{
		
		if (reportFiles!= null)
			dailyUid = reportFiles.getDailyUid();
			reportFilesUid = reportFiles.getReportFilesUid();
			String reportType = "bpmigas";
			String sql = "select isPrivate from ReportFiles where (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid and reportFilesUid = :reportFilesUid  " + 
						 "and reportType='"+reportType+"' ";
			String[] paramNames = {"dailyUid", "reportFilesUid"};
			Object[] paramValues = {dailyUid, reportFilesUid};
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, paramValues);
			
			if (list != null && list.size() > 0) {
				Object value = list.get(0);
				isPrivate = value.toString();
				return isPrivate;
				 
			}
			return null;
			
	}
	
	
}
