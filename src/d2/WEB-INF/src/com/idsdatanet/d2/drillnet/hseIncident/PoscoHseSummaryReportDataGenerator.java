package com.idsdatanet.d2.drillnet.hseIncident;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * A client specific ReportDataGenerator for HseSummary section of DDR
 * @author Wong Joon Hui (jhwong)
 * @client posco_international_mm
 * @ticket 13106
 */
public class PoscoHseSummaryReportDataGenerator implements ReportDataGenerator {

	@Override
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		
		Date lastAbandonmentDate = null;
		Date lastFireDrillDate = null;
		
		Date dayDate = null;
		Date hseIncidentEventDatetime = null;
		String targetZeroDays = "";
		String groupUid = "";
		
		Long incidentLast24Hrs = null;
		
		Long positiveObservationCardsNo = null;
		Long correctiveObservationCardsNo = null;
		String positiveObservationCardsNoString = "";
		String correctiveObservationCardsNoString = "";
		String observationCardsString = "";
		
		Long totalObservationCards = null;
		Long pobCount = null;
		String participationString = "";
		
		Long step7ConversationsNo = null;
		Long interventionNo = null;
		
		String comments = "";
		
		Date lastBOPDate= null;

		// Get Last Abandonment Date - Operation level
		lastAbandonmentDate = this.getLastDateOfIncidentCategoryInOperation(userContext, "Abandon Drill");
		
		// Get Last Fire Drill Date - Operation level
		lastFireDrillDate = this.getLastDateOfIncidentCategoryInOperation(userContext, "Fire Drill");
		
		// Get Target Zero Days - Number of Days without Incident / Number of Days
		// (Number of Days - Number of Days with Incident)
		/*
		Integer totalDays = this.getTotalReportingDays(userContext);
		Integer incidentDays = this.getDistinctDatesWithIncident(userContext);
		targetZeroDays = Integer.toString(totalDays - incidentDays) + "/" + totalDays; 
		*/
		
		// Get Incident Last 24 Hours - Get all Incidents YESTERDAY (hseIncident.incidentCategory='Incidents')
		//incidentLast24Hrs = this.getIncidentLast24Hrs(userContext);
		
		//Get Incident Last 24 Hours
		incidentLast24Hrs = this.getNumberOfIncidentsOnReportingDay(userContext, "Incident Last 24 hours");
		
		// Get No. of Observation Cards
		positiveObservationCardsNo = this.getNumberOfIncidentsToday(userContext, "Positive Observation Cards");
		positiveObservationCardsNoString = (positiveObservationCardsNo == null) ? "-" : positiveObservationCardsNo.toString();
		correctiveObservationCardsNo = this.getNumberOfIncidentsToday(userContext, "Corrective Observation Cards");
		correctiveObservationCardsNoString = (correctiveObservationCardsNo == null) ? "-" : correctiveObservationCardsNo.toString();
		observationCardsString = positiveObservationCardsNoString + "/" + correctiveObservationCardsNoString;
		
		// Get % of Participation - Total Observation cards / Today POB count
		totalObservationCards = ((positiveObservationCardsNo == null) ? 0 : positiveObservationCardsNo) + ((correctiveObservationCardsNo == null) ? 0 : correctiveObservationCardsNo);
		pobCount = this.getPobCountToday(userContext);
		
		if (pobCount == null || pobCount == 0) { // division by zero
			participationString = "-";
		} else if (totalObservationCards == 0) { // 0 divide anything is 0
			participationString = "0";
		} else { // Normal Calculation
			Double participation = ((double) totalObservationCards / pobCount) * 100;
			DecimalFormat df = new DecimalFormat("#.00");
			// Precision Cut
			participationString = df.format(participation).toString() + "%";
		}
		// Get No. of STEP 7 Conversations
		step7ConversationsNo = this.getNumberOfIncidentsToday(userContext, "STEP 7 Conversations");
				
		// Get No. Intervention
		interventionNo = this.getNumberOfIncidentsToday(userContext, "Intervention");
		
		// Get Comments
		comments = this.getHseCommentsToday(userContext);
		
		// Get Last BOP date
		lastBOPDate= this.getFirstDateOfIncidentCategoryInOperation(userContext, "BOP Test");

		// Populate ReportDataGenerator
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		// Get Target Zero Days
		dayDate = this.getDayDateOfIncidentCategory(userContext, "Target Zero Days");
		hseIncidentEventDatetime = this.getFirstDateOfIncidentCategoryInOperation(userContext, "Target Zero Days");
		
		if(hseIncidentEventDatetime != null && dayDate != null) {
			Double daysLapsedInBase = this.getTargetZeroDaysLapsed(userContext, groupUid);
			Integer daysLapsed = (int) Math.round(daysLapsedInBase);
			targetZeroDays = Integer.toString(daysLapsed);
		} else {
			targetZeroDays = "-";
		}
		
		ReportDataNode hseSummaryReportDataNode = reportDataNode.addChild("HseSummary");
		hseSummaryReportDataNode.addProperty("operationUid", userContext.getUserSelection().getOperationUid());
		hseSummaryReportDataNode.addProperty("dailyUid", userContext.getUserSelection().getDailyUid());

		hseSummaryReportDataNode.addProperty("lastBOPDate", (lastBOPDate == null) ? "-" : dateFormat.format(lastBOPDate));
		hseSummaryReportDataNode.addProperty("lastAbandonment", (lastAbandonmentDate == null) ? "-" : dateFormat.format(lastAbandonmentDate));
		hseSummaryReportDataNode.addProperty("lastFireDrill", (lastFireDrillDate == null) ? "-" : dateFormat.format(lastFireDrillDate));
		hseSummaryReportDataNode.addProperty("targetZeroDays", targetZeroDays);
		hseSummaryReportDataNode.addProperty("incidentLast24Hrs", (incidentLast24Hrs == null) ? "-" : incidentLast24Hrs.toString());

		hseSummaryReportDataNode.addProperty("positiveObservationCardsNo", positiveObservationCardsNoString);
		hseSummaryReportDataNode.addProperty("correctiveObservationCardsNo", correctiveObservationCardsNoString);
		hseSummaryReportDataNode.addProperty("observationCardsNoString", observationCardsString);
		//
		hseSummaryReportDataNode.addProperty("totalObservationCards", (totalObservationCards == null) ? "0" : totalObservationCards.toString());
		hseSummaryReportDataNode.addProperty("pobCount", (pobCount == null) ? "0" : pobCount.toString());
		hseSummaryReportDataNode.addProperty("participationPC", participationString);
		
		hseSummaryReportDataNode.addProperty("step7ConversationsNo", (step7ConversationsNo == null) ? "-" : step7ConversationsNo.toString());
		hseSummaryReportDataNode.addProperty("interventionNo", (interventionNo == null) ? "-" : interventionNo.toString());
		
		hseSummaryReportDataNode.addProperty("comments", comments);
	}
	
	/**
	 * Get Last Date for provided Incident Category - Operation Level/Scope 
	 */
	private Date getLastDateOfIncidentCategoryInOperation(UserContext userContext, String incidentCategory) throws Exception {
		Daily todayDaily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, userContext.getUserSelection().getDailyUid());
		Date todayDate = todayDaily.getDayDate();
		QueryProperties qp = new QueryProperties();
		qp.setFetchFirstRowOnly();
		String strSql = "SELECT a.hseEventdatetime FROM HseIncident a, Daily b "
				+ "WHERE (a.isDeleted = false OR a.isDeleted IS NULL) AND (b.isDeleted = false OR b.isDeleted IS NULL) "
				+ "AND a.operationUid = :thisOperationUid AND a.dailyUid=b.dailyUid " 
				+ "AND b.dayDate <= :dayDate "
				+ "AND a.incidentCategory=:incidentCategory "
				+ "AND a.hseEventdatetime < :date "
				+ "AND a.numberOfIncidents >= 0 "
				+ "ORDER BY a.hseEventdatetime DESC";
		String[] paramsFields = {"thisOperationUid", "dayDate", "incidentCategory", "date"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid(), todayDate, incidentCategory, DateUtils.addDays(todayDate, 1)};
		List<Date> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (!lstResult.isEmpty()) {
			return lstResult.get(0);
		}
		return null;
	}
	
	/**
	 * Get First Date for provided Incident Category - Operation Level/Scope 
	 */
	private Date getFirstDateOfIncidentCategoryInOperation(UserContext userContext, String incidentCategory) throws Exception {
		Daily todayDaily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, userContext.getUserSelection().getDailyUid());
		Date todayDate = todayDaily.getDayDate();
		QueryProperties qp = new QueryProperties();
		qp.setFetchFirstRowOnly();
		String strSql = "SELECT a.hseEventdatetime FROM HseIncident a, Daily b "
					+ "WHERE (a.isDeleted = false OR a.isDeleted IS NULL) AND (b.isDeleted = false OR b.isDeleted IS NULL) "
					+ "AND a.operationUid = :thisOperationUid AND a.dailyUid=b.dailyUid " 
					+ "AND b.dayDate = :dayDate "
					+ "AND a.hseEventdatetime <=:dayDateForHseEventdatetime "
					+ "AND a.incidentCategory = :incidentCategory "
					+ "ORDER BY a.hseEventdatetime DESC";
		String[] paramsFields = {"thisOperationUid", "dayDate", "dayDateForHseEventdatetime","incidentCategory"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid(), todayDate, todayDate,incidentCategory};
		List<Date> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (!lstResult.isEmpty()) {
			return lstResult.get(0);
		}
		return null;
	}
	
	/**
	 * Get Day Date for provided Incident Category
	 */
	private Date getDayDateOfIncidentCategory(UserContext userContext, String incidentCategory) throws Exception {
		Daily todayDaily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, userContext.getUserSelection().getDailyUid());
		Date todayDate = todayDaily.getDayDate();
		QueryProperties qp = new QueryProperties();
		qp.setFetchFirstRowOnly();
		String strSql = "SELECT b.dayDate FROM HseIncident a, Daily b "
					+ "WHERE (a.isDeleted = false OR a.isDeleted IS NULL) AND (b.isDeleted = false OR b.isDeleted IS NULL) "
					+ "AND a.operationUid = :thisOperationUid AND a.dailyUid=b.dailyUid " 
					+ "AND b.dayDate = :dayDate "
					+ "AND a.incidentCategory = :incidentCategory "
					+ "ORDER BY b.dayDate ASC";
		String[] paramsFields = {"thisOperationUid", "dayDate", "incidentCategory"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid(), todayDate, incidentCategory};
		List<Date> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (!lstResult.isEmpty()) {
			return lstResult.get(0);
		}
		return null;
	}
	
	/**
	 * Get First Daily Date of Operation - Operation Level/Scope
	 */
	private Date getFirstDateOfOperation(UserContext userContext) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setFetchFirstRowOnly();
		String strSql = "SELECT dayDate FROM Daily "
				+ "WHERE (isDeleted = false OR isDeleted IS NULL) "
				+ "AND operationUid = :thisOperationUid "
				+ "ORDER BY dayDate";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid()};
		List<Date> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (!lstResult.isEmpty()) {
			return lstResult.get(0);
		}
		return null;
	}
	
	/**
	 * Get Last Incident Date - Operation Level/Scope
	 */
	private Long getSumOfIncidentDateInOperation(UserContext userContext, Date date) throws Exception {
		String strSql = "SELECT SUM(a.numberOfIncidents) FROM HseIncident a, Daily b "
				+ "WHERE (a.isDeleted = false OR a.isDeleted IS NULL) AND (b.isDeleted = false OR b.isDeleted IS NULL) "
				+ "AND a.operationUid = :thisOperationUid AND a.incidentCategory=:incidentCategory "
				+ "AND a.dailyUid=b.dailyUid AND b.dayDate <= :dayDate "
				+ "AND (a.hseEventdatetime >= :yesterday AND a.hseEventdatetime < :today) ORDER BY a.hseEventdatetime DESC";
		String[] paramsFields = {"thisOperationUid", "incidentCategory", "dayDate", "yesterday", "today"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid(), "Incidents", date, DateUtils.addDays(date, -1), date};
		List<Long> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (!lstResult.isEmpty()) {
			return lstResult.get(0);
		}
		return null;
	}
	
	/**
	 * Get Number of Distinct Dates with Incident - Operation Level/Scope
	 */
	private Integer getDistinctDatesWithIncident(UserContext userContext) throws Exception {
		Integer incidentCount = 0;
		List<Date> dateList = new LinkedList<>();
		
		Date firstDate = this.getFirstDateOfOperation(userContext);
		Daily todayDaily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, userContext.getUserSelection().getDailyUid());
		Date todayDate = todayDaily.getDayDate();
		String strSql = "SELECT DISTINCT a.hseEventdatetime FROM HseIncident a, Daily b "
				+ "WHERE (a.isDeleted = false OR a.isDeleted IS NULL) AND (b.isDeleted = false OR b.isDeleted IS NULL) "
				+ "AND a.operationUid = :thisOperationUid AND a.dailyUid=b.dailyUid " 
				+ "AND b.dayDate <= :dayDate "
				+ "AND a.incidentCategory=:incidentCategory "
				+ "AND a.hseEventdatetime < :date "
				+ "AND a.hseEventdatetime >= :firstDate "
				+ "AND a.numberOfIncidents > 0 "
				+ "ORDER BY a.hseEventdatetime DESC";
		String[] paramsFields = {"thisOperationUid", "dayDate", "incidentCategory", "date", "firstDate"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid(), todayDate, "Incidents", DateUtils.addDays(todayDate, 1), firstDate};
		List<Date> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		for (Date d : lstResult) {
			GregorianCalendar gregCal = new GregorianCalendar();
			gregCal.setTime(d);
			gregCal.set(Calendar.MILLISECOND, 0);
			gregCal.set(Calendar.SECOND, 0);
			gregCal.set(Calendar.MINUTE, 0);
			gregCal.set(Calendar.HOUR, 0);
			gregCal.set(Calendar.HOUR_OF_DAY, 0);
			if (dateList.contains(gregCal.getTime())) {
				continue;
			}
			dateList.add(gregCal.getTime());
			incidentCount++;
		}
		
		return incidentCount;
	}
	
	/**
	 * Get Total Reporting Days 
	 */
	private Integer getTotalReportingDays(UserContext userContext) throws Exception {
		Daily todayDaily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, userContext.getUserSelection().getDailyUid());
		Date todayDate = todayDaily.getDayDate();
		
		Date firstDate = this.getFirstDateOfOperation(userContext);
		
		long diff = todayDate.getTime() - firstDate.getTime();
		Integer days = 1 + ((diff != 0) ? (int) (diff / (1000 * 60 * 60 * 24)) : 0);
		return days; // should not ever be 0
	}
	
	/**
	 * Get Days Lapsed for Target Zero Days Incident Category
	 */
	private Double getTargetZeroDaysLapsed(UserContext userContext, String groupUid) throws Exception {
		double timeLapsed = 0;

		Date dayDate = this.getDayDateOfIncidentCategory(userContext, "Target Zero Days");
		Date hseIncidentEventDatetime = this.getFirstDateOfIncidentCategoryInOperation(userContext, "Target Zero Days");
		
		if ("0".equalsIgnoreCase(GroupWidePreference.getValue(groupUid, "calculateHseDayLapsedIncludeDayOfIncident"))){
			timeLapsed = 0;
		}else {
			timeLapsed = 86399999; // count the time lapse as at 23:59 of the day
		}
		
		double usecLapsed = dayDate.getTime() + timeLapsed - hseIncidentEventDatetime.getTime(); 
		double daysLapsedInBase = usecLapsed / (1000 * 60 * 60 * 24); 

		if (daysLapsedInBase < 0) {
			return 0.0;
		}
			
		return daysLapsedInBase;
	}
	
	/**
	 * Get Incident Last 24 Hrs - Operation Level/Scope
	 */
	
	/*
	private Long getIncidentLast24Hrs(UserContext userContext) throws Exception {
		Daily todayDaily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, userContext.getUserSelection().getDailyUid());
		Date todayDate = todayDaily.getDayDate();
		return this.getSumOfIncidentDateInOperation(userContext, todayDate);
	}
	*/
	
	/**
	 * Get SUM for provided Incident Category - Daily Level/Scope 
	 */
	private Long getNumberOfIncidentsToday(UserContext userContext, String incidentCategory) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setFetchFirstRowOnly();
		String strSql = "SELECT SUM(numberOfIncidents) FROM HseIncident "
				+ "WHERE (isDeleted = false OR isDeleted IS NULL) "
				+ "AND dailyUid = :dailyUid "
				+ "AND incidentCategory=:incidentCategory";
		String[] paramsFields = {"dailyUid", "incidentCategory"};
		Object[] paramsValues = {userContext.getUserSelection().getDailyUid(), incidentCategory};
		List<Long> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (!lstResult.isEmpty()) {
			return lstResult.get(0);
		}
		return null;
	}
	
	/**
	 * Get SUM for Number of Incident on Reporting Day
	 */
	private Long getNumberOfIncidentsOnReportingDay(UserContext userContext, String incidentCategory) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setFetchFirstRowOnly();
		String strSql = "SELECT SUM(a.numberOfIncidents) FROM HseIncident a, Daily b "
				+ "WHERE (a.isDeleted = FALSE OR a.isDeleted IS NULL) AND (b.isDeleted = FALSE OR b.isDeleted IS NULL) "
				+ "AND a.dailyUid = b.dailyUid "
				+ "AND a.operationUid = :thisOperationUid "
				+ "AND a.dailyUid = :dailyUid "
				+ "AND DATE(a.hseEventdatetime) = DATE(b.dayDate) "
				+ "AND a.incidentCategory = :incidentCategory";
		String[] paramsFields = {"thisOperationUid", "dailyUid", "incidentCategory"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid(), userContext.getUserSelection().getDailyUid(), incidentCategory};
		List<Long> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (!lstResult.isEmpty()) {
			return lstResult.get(0);
		}
		return null;
	}
	/**
	 * Get SUM for POB - Daily Level/Scope 
	 */
	private Long getPobCountToday(UserContext userContext) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setFetchFirstRowOnly();
		String strSql = "SELECT SUM(pax) FROM PersonnelOnSite WHERE (isDeleted = false OR isDeleted IS NULL) AND dailyUid = :dailyUid";
		String[] paramsFields = {"dailyUid"};
		Object[] paramsValues = {userContext.getUserSelection().getDailyUid()};
		List<Long> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (!lstResult.isEmpty()) {
			return lstResult.get(0);
		}
		return new Long("0");
	}
	
	/**
	 * Get HseComments from General Comments - Daily Level/Scope 
	 */
	private String getHseCommentsToday(UserContext userContext) throws Exception {
		String strSql = "SELECT comments FROM GeneralComment WHERE (isDeleted = false OR isDeleted IS NULL) AND category='hsecomment' AND dailyUid = :dailyUid ORDER BY sequence";
		String[] paramsFields = {"dailyUid"};
		Object[] paramsValues = {userContext.getUserSelection().getDailyUid()};
		List<String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (!lstResult.isEmpty()) {
			String str = "";
			int count = 0; 
			for (String s : lstResult) {
				if (StringUtils.isNotBlank(s)) {
					if (count != 0) {
						str += "\n";
					}
					str += s;
				}
				count++;
			}
			return str;
		}
		return "";
	}
	
	@Override
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}
	
}
