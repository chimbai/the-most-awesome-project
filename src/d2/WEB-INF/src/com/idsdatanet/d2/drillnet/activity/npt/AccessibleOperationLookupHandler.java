package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;

public class AccessibleOperationLookupHandler implements LookupHandler{

	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		Operations ops = UserSession.getInstance(request).getCachedAllAccessibleOperations();
		Map<String,LookupItem> lookupList = new LinkedHashMap<String,LookupItem>();
		for(_Operation _op:ops.values())
		{
			LookupItem li = new LookupItem(_op.getOperationUid(),_op.getOperationName());
			Map<String,String> filterProperties = new LinkedHashMap<String,String>();
			Operation op = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, _op.getOperationUid());
			filterProperties.put("operationArea",op!=null?op.getOperationArea():null);
			li.setFilterProperties(filterProperties);
			lookupList.put(_op.getOperationUid(), li);
		}
		return lookupList;
	}

}
