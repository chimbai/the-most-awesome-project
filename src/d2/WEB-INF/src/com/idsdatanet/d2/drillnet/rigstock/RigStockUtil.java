package com.idsdatanet.d2.drillnet.rigstock;

import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;

/**
 * All common utility related to Rig Stock
 *
 */

public class RigStockUtil {
	
	/**
	 * Method for save the stock records (FluidStock, SupplementaryStock)
	 * @param commandBean
	 * @param userSession
	 * @param aclManager
	 * @param request
	 * @param node
	 * @param key
	 * @throws Exception Standard Error Throwing Exception
	 * @return new ActionHandlerResponse
	 */
	public static class SaveStockActionHandler implements ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			Object obj = node.getData();
			if (obj instanceof FluidStock) {
				FluidStock FluidStock = (FluidStock) obj;

				if (FluidStock !=null) {
					RigStock RigStock = new RigStock();				
					PropertyUtils.copyProperties(RigStock, FluidStock);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(RigStock);
				}
			}
			
			if (obj instanceof SupplementaryStock) {
				SupplementaryStock SupplementaryStock = (SupplementaryStock) obj;

				if (SupplementaryStock !=null) {
					RigStock RigStock = new RigStock();				
					PropertyUtils.copyProperties(RigStock, SupplementaryStock);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(RigStock);
				}
			}
			
			if (obj instanceof MudStock) {
				MudStock mudStock = (MudStock) obj;

				if (mudStock !=null) {
					RigStock RigStock = new RigStock();				
					PropertyUtils.copyProperties(RigStock, mudStock);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(RigStock);
				}
			}
			
			return new ActionHandlerResponse(true);
		}
	}
	
	/**
	 * default rig stock type
	 */
	private static String defaultRigStockType = "rigbulkstock";
	
	/**
	 * Method for set the batch drill flag
	 * @param node
	 * @param userSession
	 * @throws Exception
	 */
	public static void setIsBatchDrillFlag(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		Operation op = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		if (op.getIsBatchDrill()!=null && op.getIsBatchDrill())
		{
			node.getDynaAttr().put("isBatch", "1");
		}
	}
	
	/**
	 * Method for save the Stock records (in beforeDataNodeSaveOrUpdate)
	 * @param commandBean
	 * @param node
	 * @param session
	 * @param request
	 * @param status
	 * @param stockType
	 * @param calculateUseable
	 * @param isCombined
	 * @throws Exception
	 */
	public static void saveRigStock(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, DataNodeProcessStatus status, CustomFieldUom thisConverter, String stockType, Boolean calculateUseable, Boolean isCombined, String itemCostLookupReference) throws Exception{
		Object object = node.getData();
		
		if (object instanceof RigStock) {
			
			if (isCombined){
				stockType = RigStockUtil.getRigStockType(node);
			}
			
			RigStock rigstock = (RigStock)object;
			String stockcode = rigstock.getStockCode();
			if (rigstock.getAmtStart() == null) {
				rigstock.setAmtStart(0.0);
			}
			if (rigstock.getAmtUsed() == null) {
				rigstock.setAmtUsed(0.0);
			}
			if (rigstock.getAmtDiscrepancy() == null) {
				rigstock.setAmtDiscrepancy(0.0);
			}
			//if itemCost was auto populated 
			if (StringUtils.isNotBlank(itemCostLookupReference)) {
				//reset itemcost in case item cost lookup table was changed
				rigstock.setItemcost(RigStockUtil.getItemCost(stockcode, itemCostLookupReference, session));
			}
			
			if (rigstock.getItemcost() == null) { 
				rigstock.setItemcost(0.0);
			}
			
			double dailyCost = 0.0;
			if (rigstock.getDiscountPct() == null) {
				dailyCost = rigstock.getItemcost() * rigstock.getAmtUsed();
			}else {
				dailyCost = rigstock.getItemcost() * rigstock.getAmtUsed() * (1-rigstock.getDiscountPct()/100);
			}
	
			rigstock.setDailycost(dailyCost);
			
			rigstock.setRigInformationUid(session.getCurrentRigInformationUid());
			
			if (StringUtils.isNotBlank(stockcode)) {
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select storedUnit from LookupRigStock where (isDeleted = false or isDeleted is null) and lookupRigStockUid = :stockCode", "stockCode", stockcode);
				if (lstResult.size() > 0) rigstock.setStoredUnit(lstResult.get(0).toString());
			} else {
				status.setContinueProcess(false, true);
				status.setFieldError(node, "stockBrand", "Stock Name is required.");
				return;
			}
			
			//save rig stock type if the rig stock is fluid stock
			if ( stockType != null && stockType != "" )
			{
				rigstock.setType(stockType);
			}else{
				String rigStockType = null;
				if (StringUtils.isNotBlank(stockcode)) {
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select type from LookupRigStock where (isDeleted = false or isDeleted is null) and lookupRigStockUid = :stockCode", "stockCode", stockcode);
					if (lstResult.size() > 0){
						if (lstResult.get(0) != null){
							rigStockType = lstResult.get(0).toString();						
							//if ("fluids".equalsIgnoreCase(rigStockType)) {
							//	rigstock.setType(rigStockType);
							//}
						}
						rigstock.setType(rigStockType);
					}	
				}
			}
			
			//SET CHECKING TO DISALLOW DATA SAVING FOR STOCK WITH NEGATIVE BALANCE
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(rigstock.getDailyUid());
			Double startingAmount = 0.0;
			Double currentBalance = 0.0;
			Double amountToday = rigstock.getAmtStart() - rigstock.getAmtUsed() + rigstock.getAmtDiscrepancy();
			
			Operation operation = session.getCurrentOperation();
			if (operation == null) {
				operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(rigstock.getOperationUid());
			}
			Boolean isBatchDrill = operation.getIsBatchDrill() != null? operation.getIsBatchDrill():Boolean.FALSE;
			//CHECK IF THIS RECORD IS THE FIRST RECORD FOR THIS OPERATION
			String isFirstDay = CommonUtil.getConfiguredInstance().getOperationStockcodeFirstDay(rigstock.getOperationUid(), rigstock.getStockCode(), daily.getDailyUid().toString());
			
			if (isBatchDrill)
			{
				if (rigstock.getAmtInitial()!=null)
					startingAmount = rigstock.getAmtInitial();
				else
					startingAmount = CommonUtil.getConfiguredInstance().getOperationStockcodePreviousBalance(rigstock.getOperationUid(), rigstock.getRigInformationUid(), stockcode, daily, stockType);
			}else{
				//IF YES, GET amtInitial
				if(isFirstDay.equals("1"))
				{
					if(rigstock.getAmtInitial() != null)
					{
						startingAmount = rigstock.getAmtInitial();
					}
				}
				//GET FROM PREVIOUS DAY
				else
				{
					startingAmount = CommonUtil.getConfiguredInstance().getOperationStockcodePreviousBalance(rigstock.getOperationUid(), rigstock.getRigInformationUid(), stockcode, daily, stockType);
				}
			}
			thisConverter.setBaseValue(startingAmount);
			String startingAmountStr = thisConverter.formatOutputPrecision();

            // Removing thousand separator
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(commandBean.getUserLocale());
            String thousandSeparator = Character.toString(formatter.getDecimalFormatSymbols().getGroupingSeparator());
            String decimalSeparator = Character.toString(formatter.getDecimalFormatSymbols().getDecimalSeparator());
            startingAmountStr = startingAmountStr.replace(thousandSeparator, "");
            startingAmountStr = startingAmountStr.replace(decimalSeparator, ".");

			startingAmount = Double.parseDouble(startingAmountStr);
			currentBalance = startingAmount + amountToday;
			//IF CURRENT BALANCE IS NEGATIVE
			if(currentBalance < 0){
				
				thisConverter.setBaseValue(currentBalance);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@currentBalance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("currentBalance", thisConverter.getConvertedValue());	
				if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_DEPOT) {
					String stockCode = (String) PropertyUtils.getProperty(node.getData(), "stockCode");
					String screenName = ((String) PropertyUtils.getProperty(node.getData(), "type")).equals(FluidStock.rigStockType) ? "Fluid Stock" : "Rig Stock";
					status.addWarning("[" + screenName + "] " + stockCode + ": Current Balance is negative.");
				} else {
					status.setFieldError(node, "stockCode", "Current Balance is negative.");
					status.setFieldError(node, "amtDiscrepancy", "Please adjust.");
					
					//BREAK SAVE PROCESS
					status.setContinueProcess(false, true);
					return;
				}
			}
			else
			{
				thisConverter.setBaseValue(currentBalance);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@currentBalance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("currentBalance", thisConverter.getConvertedValue());
			}
			
			//added flag in modules-context, default is to calculate useable
			if (calculateUseable == true) {
				Double dblRequiredMinimum = 0.0;
				Double dblUseable = 0.0;
				if(rigstock.getRequiredAmountMin() != null) dblRequiredMinimum = rigstock.getRequiredAmountMin();
				dblUseable = currentBalance - dblRequiredMinimum;
				rigstock.setUseable(dblUseable);
			}
			
			//save rigstock back
			//ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rigstock);
		}
	}
	
	/**
	 * Method for get the defined stock type (RigStock,FluidStock,SupplementaryStock)
	 * @param node
	 * @throws Exception
	 * @return stockType
	 */
	public static String getRigStockType(CommandBeanTreeNode node){
		return getRigStockType(node.getDataDefinition().getTableClass());
	}

	public static String getRigStockType(Class stockClass){
		if (stockClass.equals(RigStock.class)){
			return defaultRigStockType;
		}
		else if (stockClass.equals(FluidStock.class)){
			return FluidStock.rigStockType;
		}
		else if (stockClass.equals(SupplementaryStock.class)){
			return SupplementaryStock.rigStockType;
		}
		else if (stockClass.equals(MudStock.class)){
			return MudStock.rigStockType;
		}
		return null;
	}
	
	/**
	 * Method for load the auto calculation (e.g isFirstDay,previousBalance,currentBalance,cumRecvd,cumUsed,cumCost,totalDaily,rigStockType) Stock records (in afterDataNodeLoad)
	 * @param commandBean
	 * @param node
	 * @param userSelection
	 * @param request
	 * @param lastDayForFWR
	 * @param stockType
	 * @param isCombined
	 * @throws Exception
	 */
	public static void populateRigStock(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request,  CustomFieldUom thisConverter, String lastDayForFWR, String stockType, Boolean isCombined) throws Exception {
		Object object = node.getData();
		
		if (object instanceof RigStock) {
			if (isCombined){
				stockType = RigStockUtil.getRigStockType(node);
			}
			
			RigStock rigstock = (RigStock)object;

			RigStockUtil.setIsBatchDrillFlag(node, userSelection);
			
			if (rigstock.getAmtStart() == null) {
				rigstock.setAmtStart(0.0);
			}
			if (rigstock.getAmtUsed() == null) {
				rigstock.setAmtUsed(0.0);
			}
			if (rigstock.getAmtDiscrepancy() == null) {
				rigstock.setAmtDiscrepancy(0.0);
			}
			
			if (rigstock.getItemcost() == null) { 
				rigstock.setItemcost(0.0);
			}
			
			if (rigstock.getDiscountPct() == null) { 
				rigstock.setDiscountPct(0.0);
			}
			// get the current data
			String stockcode = rigstock.getStockCode();
			
			//populate stockBrand to dynamicAttribute @stockName
			node.getDynaAttr().put("stockName", RigStockUtil.getStockName(stockcode));
			
			// set 'amttoday' as the balance for today
			Double amttoday = rigstock.getAmtStart() - rigstock.getAmtUsed() + rigstock.getAmtDiscrepancy();
			
			// get the current date from the session
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(rigstock.getDailyUid());
			//Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			
			if(daily == null) return;
			String operationUid = userSelection.getOperationUid();
			String rigInformationUid = userSelection.getRigInformationUid();			
			String selectedDailyUid = null;
													
			// if 'stockcode' is available
			if (StringUtils.isNotBlank(stockcode)) {
				if (!"FWR".equals(lastDayForFWR)) 
				{
					// if request is not null, we use UserSession instead of UserSelectionSnapshot to get the dailyUid because when we do a copy/paste from yesterday, the UserSelectionSnapshot will contain yesterday's dailyUid
					selectedDailyUid = (request == null ? userSelection.getDailyUid() : UserSession.getInstance(request).getCurrentDailyUid());
					//check if today is the first day of operation or not to determine want to show the starting amount
					node.getDynaAttr().put("isFirstDay", CommonUtil.getConfiguredInstance().getOperationStockcodeFirstDay(operationUid, stockcode, selectedDailyUid));
				}
				
				// default 'previousBalance' to 0.0
				Double previousBalance = 0.0;
				
				// default 'currentBalance' as today's balance
				Double currentBalance = amttoday;
				thisConverter.setReferenceMappingField(RigStock.class, "amtInitial");

				if (rigstock.getAmtInitial()==null)
				{
					previousBalance = CommonUtil.getConfiguredInstance().getOperationStockcodePreviousBalance(operationUid, rigInformationUid, stockcode, daily, stockType);
					thisConverter.setBaseValue(previousBalance);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@previousBalance", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("previousBalance", thisConverter.getConvertedValue());
				}else{
					thisConverter.setBaseValueFromUserValue(rigstock.getAmtInitial());
					previousBalance = thisConverter.getBasevalue();
				}
				// set the currentBalance = previousBalance + today's balance, and assign into the dynamic variable
				currentBalance = previousBalance + amttoday;
				thisConverter.setBaseValue(currentBalance);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@currentBalance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("currentBalance", thisConverter.getConvertedValue());
			
				Double cumRecvd = 0.0;
				Double cumUsed = 0.0;
				Double cumCost = 0.0;
			
				thisConverter.setReferenceMappingField(RigStock.class, "amtStart");
				cumRecvd = CommonUtil.getConfiguredInstance().calculateCummulativeForRigStock(operationUid, stockcode, daily, "amtStart", stockType);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumRecvd", thisConverter.getUOMMapping());
				}
				//thisConverter.setBaseValueFromUserValue(cumRecvd);
				node.getDynaAttr().put("cumRecvd", cumRecvd);
				
				cumUsed = CommonUtil.getConfiguredInstance().calculateCummulativeForRigStock(operationUid, stockcode, daily, "amtUsed", stockType);
				
				thisConverter.setReferenceMappingField(RigStock.class, "amtUsed");
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumUsed", thisConverter.getUOMMapping());
				}
				//thisConverter.setBaseValueFromUserValue(cumUsed);
				node.getDynaAttr().put("cumUsed", cumUsed);
				
				thisConverter.setReferenceMappingField(RigStock.class, "dailycost");
				
				cumCost = CommonUtil.getConfiguredInstance().calculateCummulativeForRigStock(operationUid, stockcode, daily, "dailycost", stockType);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumCost", thisConverter.getUOMMapping());
				}
				//thisConverter.setBaseValueFromUserValue(cumCost);
				node.getDynaAttr().put("cumCost", cumCost);
				
			
				/*Double dailycost = (Double) PropertyUtils.getProperty(object, "dailycost");
				if (dailycost != null) {
					Double dailyTotal = null;
					dailyTotal = (Double) commandBean.getRoot().getDynaAttr().get("dailycost");
					if (dailyTotal == null) {
						commandBean.getRoot().getDynaAttr().put("dailycost", dailycost);
					} else {
						dailyTotal += dailycost;
						commandBean.getRoot().getDynaAttr().put("dailycost", dailyTotal);
					}
				}
				
				thisConverter.setReferenceMappingField(RigStock.class, "dailycost");
				Double totalDaily = (Double) commandBean.getRoot().getDynaAttr().get("dailycost");
				commandBean.getRoot().getDynaAttr().put("totalDaily", thisConverter.formatOutputPrecision(totalDaily == null ? 0 : totalDaily));
				if (thisConverter.isUOMMappingAvailable()){
					commandBean.getRoot().setCustomUOM("@totalDaily", thisConverter.getUOMMapping());
				}*/
				
			}
			// if no 'stockcode' available
			else {
				// assign 0 to both balances if no 'stockcode' is found
				thisConverter.setReferenceMappingField(RigStock.class, "amtInitial");
				thisConverter.setBaseValue(0);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@previousBalance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("previousBalance", thisConverter.getConvertedValue());
				
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@currentBalance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("currentBalance", thisConverter.getConvertedValue());
				
				thisConverter.setReferenceMappingField(RigStock.class, "amtStart");
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumRecvd", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumRecvd", thisConverter.getConvertedValue());
				
				thisConverter.setReferenceMappingField(RigStock.class, "amtUsed");
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumUsed", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumUsed", thisConverter.getConvertedValue());
				
				thisConverter.setReferenceMappingField(RigStock.class, "dailycost");
				
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumCost", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumCost", thisConverter.getConvertedValue());
				
				if (thisConverter.isUOMMappingAvailable()){
					commandBean.getRoot().setCustomUOM("@totalDaily", thisConverter.getUOMMapping());
				}
				commandBean.getRoot().getDynaAttr().put("totalDaily", thisConverter.getConvertedValue());
				
				
			}	
			//set rig stock type for calculation (Total Daily Cost and Total Cum. Cost)
			//String stockType = "rigbulkstock";
			/*
			if (StringUtils.isNotBlank(stockcode)) {
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select type from LookupRigStock where (isDeleted = false or isDeleted is null) and lookupRigStockUid = :stockCode", "stockCode", stockcode);
				if (lstResult.size() > 0){
					if (lstResult.get(0) != null){
						String rigStockType = lstResult.get(0).toString();
						if (StringUtils.isBlank(rigStockType)) {
							stockType = "rigbulkstock";
						} else {
							stockType = rigStockType;
						}
					}
				}	
			}*/
			commandBean.getRoot().getDynaAttr().put("rigStockType", stockType);
		}
	
	}
	
	/**
	 * Method for get the mud vendor company
	 * @param dailyUid
	 * @throws Exception
	 * @return nmudVendorCompayUid
	 */
	public static String getMudVendorCompany(String dailyUid) throws Exception{
		String mudVendorCompayUid = null;
		String sql = "SELECT mudVendorCompanyUid from ReportDaily where (isDeleted = false or isDeleted is null) and reportType='DDR' and dailyUid=:dailyUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "dailyUid", dailyUid);
		if (lstResult.size() > 0) {
			Object a = (Object) lstResult.get(0);
			if (a != null) mudVendorCompayUid = a.toString();
		}
		return mudVendorCompayUid;
	}
	
	/**
	 * Method for get stock name for dynamic attribute 
	 * @param lookupRigStockUid
	 * @throws Exception
	 */
	public static String getStockName(String lookupRigStockUid) throws Exception{
		String stockBrand = null;
		if (StringUtils.isNotBlank(lookupRigStockUid)) {
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select stockBrand from LookupRigStock where (isDeleted = false or isDeleted is null) and lookupRigStockUid =:stockCode", "stockCode", lookupRigStockUid);
			if (lstResult.size() > 0) {
				Object objStock = lstResult.get(0);
				stockBrand = objStock.toString();
				
			}
		}
		
		return stockBrand;
	}
	
	/**
	 * Method to get item cost based on the condition defined
	 * @param stockCode
	 * @param itemCostReferenceTable
	 * @param userSession
	 * @return itemCost
	 * @throws Exception
	 */
	public static Double getItemCost(String stockCode, String itemCostReferenceTable, UserSession userSession) throws Exception {
		Double itemCost = null;
		String strSql ="";		
		//get stockCost from LookupRigStock table as item cost 
		if("LookupRigStock".equalsIgnoreCase(itemCostReferenceTable)){
			strSql ="select stockCost from LookupRigStock where (isDeleted = false or isDeleted is null) and lookupRigStockUid = :stockCode";
		}else {	
			//get itemCost from LookupProductList table as item cost based on mud vendor and region selected.
			String mudVendorCompanyUid = null;
			String region = null;
			String strCondition= "";
			
			//get mud vendor from report daily screen		
			ReportDaily rd = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(userSession, userSession.getCurrentDailyUid());
			if (rd!=null) mudVendorCompanyUid = rd.getMudVendorCompanyUid();

			// get region from well table
			Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(userSession.getCurrentWellUid());
			if (well != null) {
				region = well.getRegion();
			}
			
			if (StringUtils.isNotBlank(mudVendorCompanyUid)) {
				strCondition = "mudVendorCompanyUid = '" + mudVendorCompanyUid + "'";
			}else {
				strCondition = "mudVendorCompanyUid is null";
			}
			
			if (StringUtils.isNotBlank(region)) {
				strCondition += " and region = '" + region + "'";
			}else {
				strCondition += " and region is null";
			}
			
			strSql ="select itemCost from LookupProductList where " +
					"(isDeleted = false or isDeleted is null) and stockUid = :stockCode and " 
					+ strCondition;
		}
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "stockCode", stockCode);
		if (lstResult.size() > 0) { 
			if (lstResult.get(0)!=null){
				itemCost = Double.parseDouble(lstResult.get(0).toString());						
			}
		}
		
		return itemCost;
	}
	
	/**
	 * Method for check stock if got saved before
	 * @param dailyUid
	 * @param stockCode
	 * @param type
	 * @param rigStockUid
	 * @throws Exception
	 */
	public static Boolean checkStockExist(String dailyUid, String stockCode, String type, String rigStockUid) throws Exception{
		if (rigStockUid !=null && !"".equals(rigStockUid)) return false;
		
		if (StringUtils.isNotBlank(stockCode)) {
			String[] paramsFields = {"stockCode", "dailyUid", "type"};
			Object[] paramsValues = {stockCode, dailyUid, type};
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select rigStockUid from RigStock where (isDeleted = false or isDeleted is null) and stockCode =:stockCode and dailyUid=:dailyUid and type=:type", paramsFields, paramsValues);
			if (lstResult.size() > 0) {			
				return true;		
			}
		}
		
		return false;
	}

	/**
	 * Method for get the stock detail from node given (RigStock, FluidStock, SupplementaryStock, MudStock)
	 * @param node
	 * @throws Exception
	 */
	public static String[] getStockClassDetail(CommandBeanTreeNode node) throws Exception{
		Object object = node.getData();
		String[] detail = new String[3];
		
		if (object instanceof RigStock) {
			RigStock rigstock = (RigStock) object;
			detail[0] = (rigstock.getStockCode()!=null ? rigstock.getStockCode():"");
			detail[1] = (rigstock.getRigStockUid()!=null ? rigstock.getRigStockUid():"");
			detail[2] = rigstock.getType();
		}
		else if (object instanceof FluidStock) {
			FluidStock fluidStock = (FluidStock) object;
			detail[0] = (fluidStock.getStockCode()!=null ? fluidStock.getStockCode():"");
			detail[1] = (fluidStock.getRigStockUid()!=null ? fluidStock.getRigStockUid():"");
			detail[2] = fluidStock.getType();
		}
		else if (object instanceof SupplementaryStock) {
			SupplementaryStock supplementaryStock = (SupplementaryStock) object;
			detail[0] = (supplementaryStock.getStockCode()!=null ? supplementaryStock.getStockCode():"");
			detail[1] = (supplementaryStock.getRigStockUid()!=null ? supplementaryStock.getRigStockUid():"");
			detail[2] = supplementaryStock.getType();
		}
		else if (object instanceof MudStock) {
			MudStock mudStock = (MudStock) object;
			detail[0] = (mudStock.getStockCode()!=null ? mudStock.getStockCode():"");
			detail[1] = (mudStock.getRigStockUid()!=null ? mudStock.getRigStockUid():"");
			detail[2] = mudStock.getType();
		}
		
		return detail;
	}
}
