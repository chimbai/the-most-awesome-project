package com.idsdatanet.d2.drillnet.activity;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class OMVOperationActivityDurationByPhaseCodeAndTaskCodeReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
		if("CMPLT".equalsIgnoreCase(userContext.getUserSelection().getOperationType())) {
			
			String strSql = "SELECT a.phaseCode FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND " + 
							"(a.isDeleted = false OR a.isDeleted IS NULL) AND a.dailyUid = d.dailyUid AND d.operationUid = :thisOperationUid AND " +
							"NOT(a.phaseCode is null or a.phaseCode = '') AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " +
							"GROUP BY a.phaseCode ORDER BY a.phaseCode";
			String[] paramsFields = {"thisOperationUid"};
			Object[] paramsValues = {userContext.getUserSelection().getOperationUid()};
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			String lastPhase = null;
			Boolean condition = false;
			if (lstResult.size() > 0){
				
				for(Object objResult: lstResult){
									
					if(objResult != null) {
						if("ph-6".equalsIgnoreCase(objResult.toString())) {
							condition = true;
							lastPhase = objResult.toString();
						} else if ("ph-9".equalsIgnoreCase(objResult.toString())) {
							condition = true;
							lastPhase = objResult.toString();
							break;
						}
					}
				}
			}
			
			if (condition){
				Double equipmentRun = this.sumDuration(userContext, lastPhase, userContext.getUserSelection().getOperationUid(), "EQU");
				CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
				String uomDuration = thisConverter.getUomSymbol();
				if (StringUtils.isNotBlank(equipmentRun.toString())) {
					ReportDataNode thisReportNode = reportDataNode.addChild("equipmentRun");
					thisReportNode.addProperty("phase", lastPhase);
					thisReportNode.addProperty("duration", equipmentRun.toString());	
					thisReportNode.addProperty("uomDuration", uomDuration);
				}
				
				Double perforationRun = this.sumDuration(userContext, lastPhase, userContext.getUserSelection().getOperationUid(), "PER");
				if (StringUtils.isNotBlank(perforationRun.toString())) {
					ReportDataNode thisReportNode = reportDataNode.addChild("perforationRun");
					thisReportNode.addProperty("phase", lastPhase);
					thisReportNode.addProperty("duration", perforationRun.toString());
					thisReportNode.addProperty("uomDuration", uomDuration);
				}
				
				Double formationTreatmentRun = this.sumDuration(userContext, lastPhase, userContext.getUserSelection().getOperationUid(), "FTR");
				if (StringUtils.isNotBlank(formationTreatmentRun.toString())) {
					ReportDataNode thisReportNode = reportDataNode.addChild("formationTreatmentRun");
					thisReportNode.addProperty("phase", lastPhase);
					thisReportNode.addProperty("duration", formationTreatmentRun.toString());	
					thisReportNode.addProperty("uomDuration", uomDuration);
				}
				
				Double conditioningRun = this.sumDuration(userContext, lastPhase, userContext.getUserSelection().getOperationUid(), "CNR");
				if (StringUtils.isNotBlank(conditioningRun.toString())) {
					ReportDataNode thisReportNode = reportDataNode.addChild("conditioningRun");
					thisReportNode.addProperty("phase", lastPhase);
					thisReportNode.addProperty("duration", conditioningRun.toString());	
					thisReportNode.addProperty("uomDuration", uomDuration);
				}
				
				Double bop = this.sumDuration(userContext, lastPhase, userContext.getUserSelection().getOperationUid(), "BOP");
				if (StringUtils.isNotBlank(bop.toString())) {
					ReportDataNode thisReportNode = reportDataNode.addChild("bop");
					thisReportNode.addProperty("phase", lastPhase);
					thisReportNode.addProperty("duration", bop.toString());	
					thisReportNode.addProperty("uomDuration", uomDuration);
				}
				
				Double other = this.sumOtherDuration(userContext, lastPhase, userContext.getUserSelection().getOperationUid());
				if (StringUtils.isNotBlank(other.toString())) {
					ReportDataNode thisReportNode = reportDataNode.addChild("other");
					thisReportNode.addProperty("phase", lastPhase);
					thisReportNode.addProperty("duration", other.toString());
					thisReportNode.addProperty("uomDuration", uomDuration);
				} 
			}
		}		
	}
	
	private Double sumDuration(UserContext userContext, String lastPhase, String operation, String jobTypeCode) throws Exception
	{
		//SUM duration based on condition

		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		Double totalDuration = 0.0;
		
		String strSql = "SELECT SUM(a.activityDuration) AS duration FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND " + 
		"(a.isDeleted = false OR a.isDeleted IS NULL) AND a.dailyUid = d.dailyUid AND d.operationUid = :thisOperationUid AND " +
		"a.phaseCode = :thisLastPhase AND a.jobTypeCode = :thisJobTypeCode AND a.classCode in ('PT','NPT') AND " +
		"((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL))";
		String[] paramsFields = {"thisOperationUid", "thisLastPhase", "thisJobTypeCode"};
		Object[] paramsValues = {operation, lastPhase, jobTypeCode};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if (lstResult.get(0) != null){
			totalDuration = Double.parseDouble(lstResult.get(0).toString());
			CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
			thisConverter.setBaseValue(totalDuration);
			totalDuration = thisConverter.getConvertedValue();
			return (Double) totalDuration;
		}else{
			return 0.0;
		}
	}
	
	private Double sumOtherDuration(UserContext userContext, String lastPhase, String operation) throws Exception
	{
		//SUM duration based on condition

		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		Double totalDuration = 0.0;
		
		String strSql = "SELECT SUM(a.activityDuration) AS duration FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND " + 
		"(a.isDeleted = false OR a.isDeleted IS NULL) AND a.dailyUid = d.dailyUid AND d.operationUid = :thisOperationUid AND " +
		"a.phaseCode = :thisLastPhase AND a.jobTypeCode not in ('EQU','PER','FTR','CNR','BOP') AND a.taskCode not in ('D/F') AND a.classCode in ('PT','NPT') AND " +
		"((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL))";
		String[] paramsFields = {"thisOperationUid", "thisLastPhase"};
		Object[] paramsValues = {operation, lastPhase};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if (lstResult.get(0) != null){
			totalDuration = Double.parseDouble(lstResult.get(0).toString());
			CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
			thisConverter.setBaseValue(totalDuration);
			totalDuration = thisConverter.getConvertedValue();
			return (Double) totalDuration;
		}else{
			return 0.0;
		}
	}
}
