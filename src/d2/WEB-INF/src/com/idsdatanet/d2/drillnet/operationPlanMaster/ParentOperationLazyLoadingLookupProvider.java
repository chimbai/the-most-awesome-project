package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LazyLoadingRequest;
import com.idsdatanet.d2.core.lookup.LazyLoadingResult;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupProviderForLazyLoading;
import com.idsdatanet.d2.core.lookup.LookupResolverForLazyLoading;
import com.idsdatanet.d2.core.lookup.LookupResolverForLookupMap;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.D2UserSelection;

public class ParentOperationLazyLoadingLookupProvider implements LookupProviderForLazyLoading{
	private class WellboreInfo{
		final String wellboreName;
		final String parentWellboreUid;
		
		WellboreInfo(String wellboreName, String parentWellboreUid){
			this.wellboreName = wellboreName;
			this.parentWellboreUid = parentWellboreUid;
		}
	}

	private class OperationInfo{
		final String operationUid;
		final String operationName;
		final String wellUid;
		final String wellboreUid;
		
		OperationInfo(String operationUid, String operationName, String wellUid, String wellboreUid){
			this.operationUid = operationUid;
			this.operationName = operationName;
			this.wellUid = wellUid;
			this.wellboreUid = wellboreUid;
		}
	}
	
	private class LookupItemHolder implements Comparable<LookupItemHolder>{
		final String compareName;
		final LookupItem lookupItem;
		
		LookupItemHolder(String compareName, LookupItem lookupItem){
			this.compareName = compareName;
			this.lookupItem = lookupItem;
		}

		@Override
		public int compareTo(LookupItemHolder o) {
			return this.compareName.compareTo(o.compareName);
		}
	}
	
	private class DataProvider{
		private final Map<String,String> wellNameMap = new HashMap<String,String>();
		private final Map<String,WellboreInfo> wellboreMap = new HashMap<String,WellboreInfo>();
		private final List<OperationInfo> operations = new ArrayList<OperationInfo>();
		
		@SuppressWarnings("rawtypes")
		DataProvider(List keys) throws Exception{
			// load operations from a list of operation id
			loadData(ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select operationUid, operationName, wellUid, wellboreUid from Operation where (isDeleted is null or isDeleted = false) and operationUid in (:keys)", "keys", keys));
		}
		
		@SuppressWarnings("unused")
		DataProvider(String operationUid) throws Exception{
			// load operations that belong to the same well but not including itself
			loadData(ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select operationUid, operationName, wellUid, wellboreUid from Operation where (isDeleted is null or isDeleted = false) and operationUid <> :operationUid1 and wellUid in (select wellUid from Operation where operationUid = :operationUid2)", new String[] {"operationUid1", "operationUid2"}, new Object[] {operationUid, operationUid}));
		}
		
		@SuppressWarnings("rawtypes")
		private void loadData(List rs) throws Exception {
			List<String> wellUidList = new ArrayList<String>();
			for(Object item: rs) {
				Object[] rec = (Object[]) item;
				OperationInfo operationInfo = new OperationInfo((String) rec[0], (String) rec[1], (String) rec[2], (String) rec[3]);
				this.operations.add(operationInfo);
				if(!wellUidList.contains(operationInfo.wellUid)){
					wellUidList.add(operationInfo.wellUid);
				}
			}

			// load well and wellbore related to the operations above, will be used later to resolve well name and hierarchical wellbore name
			List rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select W.wellUid, W.wellName, WB.wellboreUid, WB.wellboreName, WB.parentWellboreUid from Well W, Wellbore WB where W.wellUid = WB.wellUid and (W.isDeleted is null or W.isDeleted = false) and (WB.isDeleted is null or WB.isDeleted = false) and W.wellUid in (:wells)", "wells", wellUidList);
			for(Object item: rs2) {
				Object[] rec = (Object[]) item;
				String wellUid = (String) rec[0];
				String wellName = (String) rec[1];
				String wellboreUid = (String) rec[2];
				String wellboreName = (String) rec[3];
				String parentWellboreUid = (String) rec[4];
				if(!this.wellNameMap.containsKey(wellUid)) {
					this.wellNameMap.put(wellUid, wellName);
				}
				wellboreMap.put(wellboreUid, new WellboreInfo(wellboreName, parentWellboreUid));
			}
		}
		
		WellboreInfo getWellbore(String wellboreUid) {
			return this.wellboreMap.get(wellboreUid);
		}
		
		String getWellname(String wellUid) {
			return this.wellNameMap.get(wellUid);
		}
		
		List<OperationInfo> getOperations(){
			return this.operations;
		}
	}

	private int lazyLoadingPageSize = 0; // default to 0 to load all

	public void setLazyLoadingPageSize(int value) {
		this.lazyLoadingPageSize = value;
	}
	
	@Override
	public LazyLoadingResult getLookupForLazyLoading(URI uri, HttpServletRequest request) throws Exception {
		LazyLoadingRequest lazyLoadingRequest = new LazyLoadingRequest(request);
		DataProvider dataProvider = new DataProvider(lazyLoadingRequest.getCascadeParentField());
		String search = lazyLoadingRequest != null ? lazyLoadingRequest.getSearch() : null;
		List<LookupItemHolder> lookupItems = new ArrayList<LookupItemHolder>();
		
		for(OperationInfo operation: dataProvider.getOperations()) {
			String operationName = getOperationName(dataProvider.getWellname(operation.wellUid), operation.wellboreUid, operation.operationName, dataProvider);
			if (operationName != null) {
				if(search == null || operationName.indexOf(search) != -1) {
					// Maintain backward compatibility, the name is sorted with WellNameUtil.getPaddedStr()
					lookupItems.add(new LookupItemHolder(WellNameUtil.getPaddedStr(operationName), new LookupItem(operation.operationUid, operationName)));
				}
			}
		}		
		
		Collections.sort(lookupItems);
		
		int start = this.lazyLoadingPageSize > 0 ? lazyLoadingRequest.getPageNo() * this.lazyLoadingPageSize : 0;
		int end = this.lazyLoadingPageSize > 0 ? Math.min(start + this.lazyLoadingPageSize, lookupItems.size()) : lookupItems.size();
		
		LinkedHashMap<String, LookupItem> lookup = new LinkedHashMap<String, LookupItem>();
		for (int i=start; i < end; ++i) {
			LookupItem lookupItem = lookupItems.get(i).lookupItem;
			lookup.put(lookupItem.getKey(), lookupItem);
		}
		
		LazyLoadingResult result = new LazyLoadingResult();
		result.setHasMore(lookupItems.size() > end);
		result.setLookup(lookup);
		return result;
	}

	@Override
	public LookupResolverForLazyLoading getLookupResolverForLazyLoading(List<Object> keys, D2UserSelection userSelection, HttpServletRequest request, String lookupHandlerUri) throws Exception {
		DataProvider dataProvider = new DataProvider(keys);
		Map<String,LookupItem> result = new HashMap<String,LookupItem>();
		for(OperationInfo operation: dataProvider.getOperations()) {
			String operationName = getOperationName(dataProvider.getWellname(operation.wellUid), operation.wellboreUid, operation.operationName, dataProvider);
			if (operationName != null) {
				result.put(operation.operationUid, new LookupItem(operation.operationUid, operationName));
			}
		}
		return new LookupResolverForLookupMap(result);
	}

	private static String null2EmptyString(String value){
		if(value == null) return "";
		return value;
	}

	private static String getCascadeParentWellboreName(String wellboreUid, String name, String delim, DataProvider dataProvider, int depth) {
		if (depth > 5) {
			return "..." + delim + null2EmptyString(name);
		}
		WellboreInfo wb = dataProvider.getWellbore(wellboreUid);
		if(wb == null) return name;
		return getCascadeParentWellboreName(wb.parentWellboreUid, null2EmptyString(wb.wellboreName) + delim + null2EmptyString(name), delim, dataProvider, depth + 1);
	}
	
	// this method is copied from ParentOperationLookupHandler, trying to maintain all its logic but improve on loading speed
	private static String getOperationName(String wellName, String wellboreUid, String operationName, DataProvider dataProvider)  throws Exception {
		String hierarchicalWellbore = getCascadeParentWellboreName(wellboreUid, "", MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES, dataProvider, 0);
		if (hierarchicalWellbore!=null && !"".equals(hierarchicalWellbore)) {
			hierarchicalWellbore = hierarchicalWellbore.substring(0, hierarchicalWellbore.lastIndexOf(MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES));
		}
		if (hierarchicalWellbore!=null) {
			String label = operationName;
			if(! StringUtils.equals(operationName.trim(), hierarchicalWellbore.trim())) {
				String wellboreOperationName = hierarchicalWellbore + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + operationName;
				if(! StringUtils.equals(wellName.trim(), hierarchicalWellbore.trim())) {
					label = wellName + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + wellboreOperationName;
				} else {
					label = wellboreOperationName;
				}
			} else {
				if(! StringUtils.equals(wellName.trim(), hierarchicalWellbore.trim())) {
					label = wellName + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + operationName;
				}
			}
			return label;
		}
		return null;
	}
}
