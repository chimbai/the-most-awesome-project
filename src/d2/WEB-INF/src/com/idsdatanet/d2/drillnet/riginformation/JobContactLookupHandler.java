package com.idsdatanet.d2.drillnet.riginformation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class JobContactLookupHandler implements LookupHandler {
	
	private List<String> designation;
	
	public void setDesignation(List<String> designation)
	{
		if(designation != null){
			this.designation = new ArrayList<String>();
			for(String value: designation){
				this.designation.add(value.trim().toUpperCase());
			}
		}
	}
	
	public List<String> getDesignation() {
		return this.designation;
	}
		
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		String strInCondition = "";
		
		if(this.getDesignation() != null)
		{
			for(String value:this.designation)
			{
				strInCondition = strInCondition + "'" + value.toString() + "',";
			}
			strInCondition = StringUtils.substring(strInCondition, 0, -1);
			strInCondition = " and designation IN (" + StringUtils.upperCase(strInCondition) + ")";					
		}
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		String sql = "select pobMasterUid, pobName from PobMaster where (isDeleted = false or isDeleted is null) and groupUid =:groupUid" + strInCondition + " order by pobName";
		
		List<Object[]> jobContactResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "groupUid",userSelection.getGroupUid());
		if (jobContactResults.size() > 0) {
			for (Object[] jobContactResult : jobContactResults) {
				if (jobContactResult[0] != null && jobContactResult[1] != null) {
					result.put(jobContactResult[0].toString(), new LookupItem(jobContactResult[0].toString(), jobContactResult[1].toString()));
				}
			}
		}
		return result;
	}

}

