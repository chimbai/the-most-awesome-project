package com.idsdatanet.d2.drillnet.report;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.report.birt.ReportUtils;
import com.idsdatanet.d2.core.web.mvc.AbstractUOMFormatter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class DDRReportModuleListener implements ReportModuleListener {
	private ReportUtils reportUtils;
	private boolean dailyTypeByPriority=false;

	public void afterReportGenerated(UserSelectionSnapshot userSelection,
			ReportJobParams reportJobData, AbstractReportModule reportModule) throws Exception {
		// TODO Auto-generated method stub
		 
		this.reportUtils = this.getReportUtils(userSelection);
		
		if (reportJobData.isJoinReport() && reportJobData.isMultiCopiesJobEnd())
		{
			List<String> pdfs = new ArrayList<String>();
			List<ReportJobResult> results = reportJobData.getReportJobResult();
			if (results.size()>1)
			{
				for(ReportJobResult rs : results){
					File attachedFile = new File(rs.getReportFilePath());
					if(attachedFile.exists()) pdfs.add(rs.getReportFilePath());
				}
				
				String filePath = this.getOutputFileInLocalPath(results, userSelection, reportJobData.getPaperSize(), reportModule);
				String outputFilePath = com.idsdatanet.d2.drillnet.report.ReportUtils.getFullOutputFilePath(filePath);
				File file = new File(outputFilePath);
				if (!file.getParentFile().exists())
					file.getParentFile().mkdirs();
				reportUtils.concatPdfs(pdfs, outputFilePath);
				
				ReportFiles reportFiles = null;
				
				//save file
				if(reportJobData == null){
					return;
				}
				
				boolean newFile = false;
				
				List<Object> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportFiles where (isDeleted = false or isDeleted is null) and reportFile = :reportFile", "reportFile", filePath);
				if(rs.size() > 0){
					reportFiles = (ReportFiles) rs.get(0);
				}else{
					reportFiles = new ReportFiles();
					newFile = true;
				}
				
				Operation reportOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
				
				String report_type = reportJobData.getReportJobResult().get(0).getReportFile().getReportType();
				ReportTypes report_type_record = com.idsdatanet.d2.drillnet.report.ReportUtils.getConfiguredInstance().getReportType(userSelection.getGroupUid(), report_type, reportJobData.getPaperSize());
					
				reportFiles.setDateGenerated(new Date());
				reportFiles.setReportUserUid(userSelection.getUserUid());
				reportFiles.setReportOperationUid(userSelection.getOperationUid());				
				reportFiles.setDailyUid(userSelection.getDailyUid().concat(":combined"));
				reportFiles.setReportFile(filePath);
				reportFiles.setReportType(report_type);
				reportFiles.setGroupUid(userSelection.getGroupUid());
				reportFiles.setReportDayNumber(null);
				reportFiles.setReportDayDate(null);
				
				if(newFile){
					if (reportOperation == null || !BooleanUtils.isTrue(reportOperation.getTightHole())) { // tight hole should override GWP
						reportFiles.setIsPrivate(! "1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_NEWLY_GENERATED_REPORT_DEFAULT_PUBLIC)));
					}
				}
				
				reportFiles.setDisplayName(reportModule.getReportDisplayNameOnCreation(reportFiles, report_type_record, reportOperation, null, null, userSelection, reportJobData));

				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportFiles);
					
				ReportJobResult jobResult = reportJobData.getLastReportJobResult();
				if(jobResult != null){
					jobResult.setReportFile(reportFiles);
				}
				
			}
				
		}
	}

	private ReportUtils getReportUtils(UserSelectionSnapshot userSelection) throws Exception {
		if(AbstractUOMFormatter.isConfiguredInstanceExists()){
			return new ReportUtils(userSelection, AbstractUOMFormatter.getConfiguredInstance());
		}else{
			return new ReportUtils(userSelection, null);
		}
	}
	
	public void beforeReportJobStart(UserSelectionSnapshot userSelection,
			ReportJobParams reportJobData) throws Exception {
		
		
	}
	protected String getOutputFileInLocalPath(List<ReportJobResult> jobResults, UserSelectionSnapshot userSelection, String paperSize, AbstractReportModule reportModule) throws Exception{
		String operation_uid = userSelection.getOperationUid();
		String filename = "";
		if(this.getOutputFileName(jobResults,userSelection, paperSize,reportModule) != null)
			filename = this.getOutputFileName(jobResults,userSelection, paperSize,reportModule).trim().replace("\\", "_").replace("/", "_").replace("*", "_");
		
			
		return reportModule.getReportType(userSelection) + 
			(StringUtils.isBlank(operation_uid) ? "" : "/" + operation_uid)  + 
			"/" + filename;
	}

	protected String getOutputFileName(List<ReportJobResult> jobResults, UserSelectionSnapshot userSelection, String paperSize, AbstractReportModule reportModule) throws Exception{
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		
		return ( operation.getOperationName()) + (reportModule.getDifferentiateOutputFileByUomTemplate() ? " " + reportModule.getUomTemplateNameForOutputFile(userSelection) : "") + (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + " Combined." + reportModule.getOutputFileExtension();
	}
	protected String getDayNumbers(List<ReportJobResult> jobResults, String append)
	{
		String jobNumbers = null;
		for (ReportJobResult result : jobResults)
		{
			if (jobNumbers== null){
				jobNumbers = result.getReportFile().getReportDayNumber();
			}
			else{
				jobNumbers = jobNumbers+"_"+result.getReportFile().getReportDayNumber();
			}
		}
		return jobNumbers + append;
	}
	
	protected String getDailyType() {
		return null;
	}
	
	public boolean getDailyTypeByPriority() {
		return dailyTypeByPriority;
	}
	
	protected String getDailyType(UserSelectionSnapshot userSelection)  throws Exception{
		if (this.getDailyTypeByPriority())
		{
			return CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(userSelection.getOperationUid());
		}else{
			return null;
		}
	}
	
	protected ReportDaily getRelatedReportDaily(String dailyUid, String dailyType) throws Exception {
		return ApplicationUtils.getConfiguredInstance().getReportDaily(dailyUid, dailyType);
	}
	
	protected String getReportType(UserSelectionSnapshot userSelection) throws Exception {
		return null;
	}
	
	protected void beforeSaveReportEntry(JobContext jobContext, ReportFiles report) throws Exception{
		// do nothing
	}
	
	
}
