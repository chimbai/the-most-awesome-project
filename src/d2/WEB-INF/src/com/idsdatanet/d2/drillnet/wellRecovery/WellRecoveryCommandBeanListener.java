package com.idsdatanet.d2.drillnet.wellRecovery;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellRecoveryCommandBeanListener extends EmptyCommandBeanListener {
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		String action = request.getParameter("submitForServerSideProcess_action");
		if (action!=null) {
			if ("_recover_operation".equals(action)) {
				String operationUid = request.getParameter("submitForServerSideProcess_operationUid");
				if (operationUid!=null) {
					UserSession session = UserSession.getInstance(request);
					String user = session.getUserUid();
					Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
					String opUid = operation.getOperationUid();
					String wellboreUid = operation.getWellboreUid();
					String wellUid = operation.getWellUid();
					
					String strSql = "UPDATE Operation SET isDeleted = null,lastEditUserUid =:user,lastEditDatetime=:lastEditDatetime where operationUid=:opUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"opUid","user","lastEditDatetime"} , new Object[] {opUid,user,new Date()});
					String strSql2 = "UPDATE Wellbore SET isDeleted = null,lastEditUserUid =:user,lastEditDatetime=:lastEditDatetime where wellboreUid=:wellboreUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, new String[] {"wellboreUid","user","lastEditDatetime"} , new Object[] {wellboreUid,user,new Date()});
					String strSql3 = "UPDATE Well SET isDeleted = null,lastEditUserUid =:user,lastEditDatetime=:lastEditDatetime where wellUid=:wellUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, new String[] {"wellUid","user","lastEditDatetime"} , new Object[] {wellUid,user,new Date()});
					String strSql4 = "UPDATE Daily SET isDeleted = null,lastEditUserUid =:user,lastEditDatetime=:lastEditDatetime where operationUid=:opUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql4, new String[] {"opUid","user","lastEditDatetime"} , new Object[] {opUid,user,new Date()});
					
					session.refreshCachedData();
				}
			}
		}
	}
}
