package com.idsdatanet.d2.drillnet.wellManagement;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.hibernate.TableMapping;
import com.idsdatanet.d2.core.model.DepotWellOperationMapping;
import com.idsdatanet.d2.core.model.EdmSync;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;
import com.idsdatanet.depot.edm.wellboreoperationmapping.EDMDepotWellboreOperationMappingDataNodeListener;

public class WellManagementDataNodeListener extends EmptyDataNodeListener implements ActionManager, DataLoaderInterceptor {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	private DeleteHandler deleteHandler = new DeleteHandler();
	
	private List _excludedTables = null;
	
	public List getExcludedTables() {
		return _excludedTables;
	}

	public void setExcludedTables(List tables) {
		_excludedTables = tables;
	}

	private class DeleteHandler implements ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			Object data = node.getData();
			if (data instanceof Well) {
				Well well = (Well) data;
				deleteWell(well);
			}
			return new ActionHandlerResponse(true, false, null, BaseCommandBean.OPERATION_PERFORMED_DELETE);
		}
	}
	
	/**
	 * Delete well and all child tables
	 * @param operation
	 * @throws Exception
	 */
	private void deleteWell(Well well) throws Exception {
		String wellUid = well.getWellUid();
		//Delete edm sync record
		String sql = "FROM DepotWellOperationMapping WHERE (isDeleted IS NULL OR isDeleted=FALSE) AND wellUid = :wellUid";
		String[] paramsFields = {"wellUid"};
		Object[] paramsValues = {wellUid};
		List<DepotWellOperationMapping> lstResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		if (lstResults.size()>0) {
			for (DepotWellOperationMapping dwom : lstResults) {
				EDMDepotWellboreOperationMappingDataNodeListener listener = new EDMDepotWellboreOperationMappingDataNodeListener();
				listener.deleteOperationDataFromEdmSync(dwom);
			}
		}
		List<Class> all_tables = TableMapping.getTableModel();
		if (all_tables != null) {
			for (Class clazz : all_tables) {
				Map class_properties = PropertyUtils.describe(clazz.newInstance());
				if (class_properties.containsKey("wellUid")) {
					if (this._excludedTables!=null && 
							this._excludedTables.contains(clazz.getSimpleName())) {
						continue;
					}
					String hql = "delete from " + clazz.getName() + " where wellUid = :wellUid";
					String[] paramName = {"wellUid"};
					String[] paramValue = {wellUid};
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(hql, paramName, paramValue);
				}
			}
		}
	}
	
	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		// return delete handler
		if (node.getData() instanceof Well && action.equals(Action.DELETE)) {
			return deleteHandler;
		}
		return null;
	}

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		String wellNameFilter  = (String) commandBean.getRoot().getDynaAttr().get("wellNameFilter");
		if (StringUtils.isNotBlank(wellNameFilter)) {
			customCondition = "well_name like :wellNameCustomCondition";
			query.addParam("wellNameCustomCondition", wellNameFilter + "%");
		}
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
