package com.idsdatanet.d2.drillnet.report;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class LessonLearnedWithFilterReportModule extends DefaultReportModule {
	
	private Object filterLookup;
	public static String FILTER_LOOKUP_DYNAMIC_ATTRIBUTE="filterLookup";
	private String filterAttributeKey = "filterAttributeKey";

	public String getFilterAttributeKey() {
		return filterAttributeKey;
	}

	public void setFilterAttributeKey(String filterAttributeKey) {
		this.filterAttributeKey = filterAttributeKey;
	}

	public Object getFilterLookup() {
		return filterLookup;
	}

	public void setFilterLookup(Object filterLookup) {
		this.filterLookup = filterLookup;
	}
	
	private class FilterLookupHandler implements LookupHandler {	
		private String filterLookupDefinition;
		FilterLookupHandler(String filterLookupDefinition){
			this.filterLookupDefinition=filterLookupDefinition;
		}		
				
		public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache)
				throws Exception {
			return LookupManager.getConfiguredInstance().getLookup(this.filterLookupDefinition,userSelection, null);
		}
	}

	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		commandBean.getSystemMessage().clear();
		UserSelectionSnapshot userSelection=new UserSelectionSnapshot(session);
		String filterAttributeKeyValue = (String) commandBean.getRoot().getDynaAttr().get(FILTER_LOOKUP_DYNAMIC_ATTRIBUTE);
		
		if (!StringUtils.isBlank(filterAttributeKeyValue)) {
			userSelection.setCustomProperty(filterAttributeKey, filterAttributeKeyValue);
		}
		return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));	
	}
	
	public void init(CommandBean commandBean){
		super.init(commandBean);
		
		if(commandBean instanceof BaseCommandBean){
			Map lookup = new HashMap();
			lookup.put("@"+FILTER_LOOKUP_DYNAMIC_ATTRIBUTE, this.filterLookup instanceof LookupHandler ? (LookupHandler) this.filterLookup:new FilterLookupHandler(this.filterLookup.toString()));
			((BaseCommandBean) commandBean).setLookup(lookup);
		}
	}

}