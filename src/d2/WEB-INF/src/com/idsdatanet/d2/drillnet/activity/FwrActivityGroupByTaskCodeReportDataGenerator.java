package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class FwrActivityGroupByTaskCodeReportDataGenerator implements ReportDataGenerator {
	private Map<String, String> taskCodeList = null;
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation validation) throws Exception {
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		this.taskCodeList = this.getTaskCodeList(operation);
		
		this.generateTimeAnalysisHourGrouping(userContext, reportDataNode);
		
	}

	private void generateTimeAnalysisHourGrouping(UserContext userContext, ReportDataNode reportDataNode) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double taskCodeDuration;
		String shortCode;
		String shortCodeName;

		String strSql = "SELECT sum(a.activityDuration) AS totalDuration, a.taskCode "
				+ "FROM Activity a WHERE a.operationUid = :thisOperationUid "
				+ "AND (a.isDeleted = false OR a.isDeleted is null) "
				+ "AND (a.carriedForwardActivityUid = '' OR a.carriedForwardActivityUid IS NULL) "
				+ "AND (a.isSimop = '' OR a.isSimop IS NULL) AND (a.isOffline = '' OR a.isOffline IS NULL) "
				+ "AND a.dailyUid "
					+ "IN (SELECT dailyUid FROM Daily d WHERE d.operationUid = :thisOperationUid AND (d.isDeleted='' OR d.isDeleted IS NULL)) "
				+ "GROUP BY a.taskCode";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid()};
		
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (!lstResult.isEmpty()) {
			for (Object[] activityRecord : lstResult) {
				shortCode = "";
				shortCodeName = "";
				taskCodeDuration = 0.0;
				
				if (activityRecord[0] != null && StringUtils.isNotBlank(activityRecord[0].toString())) {
					taskCodeDuration = Double.parseDouble(activityRecord[0].toString());
				}
				if (activityRecord[1] != null && StringUtils.isNotBlank(activityRecord[1].toString())) 
					shortCode = activityRecord[1].toString();

				if (this.taskCodeList.containsKey(shortCode)) 
					shortCodeName = this.taskCodeList.get(shortCode);
				
					taskCodeDuration = taskCodeDuration/3600; //Convert to hours
					ReportDataNode thisReportNode = reportDataNode.addChild("ActivityHourGrouping");
					thisReportNode.addProperty("activityDuration", taskCodeDuration.toString());
					thisReportNode.addProperty("shortCode", shortCode);
					thisReportNode.addProperty("shortCodeName", shortCodeName);
			}
		}
	}
	
	public Map<String, String> getTaskCodeList(Operation operation) throws Exception{
		
		String operationCode = "";

		if(StringUtils.isNotBlank(operation.getOperationCode())) operationCode = operation.getOperationCode();
		
		List<Object[]> listTaskCodeResults = new ArrayList<Object[]>();
		Map<String, String> taskCodeList = new HashMap();
		
		String[] paramsFields = {"operationCode"};		
 		Object[] paramsValues = {operationCode};
		
		String strSql = "SELECT ltc.shortCode, ltc.name FROM LookupTaskCode ltc WHERE (ltc.isDeleted = false or ltc.isDeleted is null) AND " +
						 " (ltc.operationCode = :operationCode or ltc.operationCode='') ORDER BY ltc.name";
		listTaskCodeResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (listTaskCodeResults.size() > 0) {
			for (Object[] listTaskCodeResult : listTaskCodeResults) {
				if (listTaskCodeResult[0] != null && listTaskCodeResult[1] != null) {
					
					String key = listTaskCodeResult[0].toString();
					String value = listTaskCodeResult[1].toString() + " (" + listTaskCodeResult[0].toString() + ")";
							
					taskCodeList.put(key, value);
					
				}
			}
		}
		
		return taskCodeList;
	}
	
}
