package com.idsdatanet.d2.drillnet.activity;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @author dlee
 * This class help to mine data for activity duration for the selected daily group by rc code
 * for daily reporting use
 * 
 * configuration is set in ddr.xml
 */

public class DailyActivityDurationByRootCauseCodeReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "SELECT rootCauseCode, classCode, SUM(activityDuration) AS totalDuration, internalClassCode FROM Activity WHERE (isDeleted = false OR isDeleted IS NULL) AND dailyUid =:thisDailyUid AND " +
			"NOT(rootCauseCode is null or rootCauseCode = '') AND (dayPlus <> 1 AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL)) GROUP BY rootCauseCode, classCode, internalClassCode";
		String[] paramsFields = {"thisDailyUid"};
		Object[] paramsValues = {userContext.getUserSelection().getDailyUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			for(Object objResult: lstResult){
				//due to result is in array, cast the result to array first and read 1 by 1
				Object[] objDuration = (Object[]) objResult;
				
				String rcCode = "N/A";
				String classCode = "N/A";
				String internalClassCode = "N/A";
				Double totalDuration = 0.0;
				
				if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())) rcCode = objDuration[0].toString();
				
				if (objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())) classCode = objDuration[1].toString();
				
				if (objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())){
					totalDuration = Double.parseDouble(objDuration[2].toString());
					
					//assign unit to the value base on activity.activityDuration, this will do auto convert to user display unit + format
					CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
					thisConverter.setBaseValue(totalDuration);
					totalDuration = thisConverter.getConvertedValue();
				}
				if (objDuration[3] != null && StringUtils.isNotBlank(objDuration[3].toString())) internalClassCode = objDuration[3].toString();
				ReportDataNode thisReportNode = reportDataNode.addChild("code");
				thisReportNode.addProperty("name", rcCode);
				thisReportNode.addProperty("classCode", classCode);
				thisReportNode.addProperty("internalClassCode", internalClassCode);
				thisReportNode.addProperty("duration", totalDuration.toString());
				
							
			}			
		}		
	}
	
}
