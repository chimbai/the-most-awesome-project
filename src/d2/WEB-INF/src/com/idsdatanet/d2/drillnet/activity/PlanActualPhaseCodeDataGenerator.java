package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * Custom plan vs actual phase code data generator  
 * @author Jack
 *
 */
public class PlanActualPhaseCodeDataGenerator implements ReportDataGenerator{

	@Override
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String operationUid = userContext.getUserSelection().getOperationUid();
		String operationType = userContext.getUserSelection().getOperationType();
		String dailyUid = userContext.getUserSelection().getDailyUid();
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);		
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
		
		/*Bar Chart with Plan Phase Code*/
		
		String strSql = "SELECT opp.operationPlanPhaseUid, opp.sequence, opp.phaseCode, opp.p50Duration FROM OperationPlanMaster opm, OperationPlanPhase opp WHERE "
				+ "opm.operationPlanMasterUid = opp.operationPlanMasterUid AND opm.operationUid=:operationUid "
				+ "AND (opm.isDeleted is null or opm.isDeleted=false) AND (opp.isDeleted is null or opp.isDeleted=false) "
				+ "AND (opm.dvdPlanStatus=true and opm.dvdPlanStatus is not null) ORDER BY opp.sequence ";
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			
			for(Object objResult: lstResult){
				
				Integer sequence = 0;
				String phaseCode = null;
				Double p50Duration = 0.00;
				Object[] plannedObject = (Object[]) objResult;
				
				String operationPlanPhaseUid = plannedObject[0].toString();
				if(plannedObject[1]!=null && StringUtils.isNotBlank(plannedObject[1].toString())) sequence = Integer.parseInt(plannedObject[1].toString());
				if(plannedObject[2]!=null && StringUtils.isNotBlank(plannedObject[2].toString())) phaseCode = plannedObject[2].toString();
				if(plannedObject[3]!=null && StringUtils.isNotBlank(plannedObject[3].toString())) p50Duration = Double.parseDouble(plannedObject[3].toString());
				
				ReportDataNode thisReportNode = reportDataNode.addChild("PlanActualPhaseCode");
				
				thisConverter.setBaseValue(p50Duration);
				
				thisReportNode.addProperty("planPhaseUid", operationPlanPhaseUid);
				thisReportNode.addProperty("series", "Plan");
				thisReportNode.addProperty("stacknum", "s1");
				thisReportNode.addProperty("sequence", Integer.toString(sequence));
				thisReportNode.addProperty("internal_class_code", "PLAN");
				thisReportNode.addProperty("phase_code", phaseCode);
				thisReportNode.addProperty("phase_code_name", this.getPhaseName(phaseCode, operationType));
				thisReportNode.addProperty("duration", Double.toString(thisConverter.getConvertedValue()));
				thisReportNode.addProperty("duration_uom", thisConverter.getUomSymbol());
				thisReportNode.addProperty("start_datetime", "0");
				thisReportNode.addProperty("root_cause_code", "");
				thisReportNode.addProperty("sequence_phase", Integer.toString(sequence) + " - " + this.getPhaseName(phaseCode, operationType));
				
			}
		}
		
		String[] paramsFields = {"userDate", "operationUid"};
		Object[] paramsValues = {todayDate, operationUid};
		
		
		/*Bar Actual with Plan Reference*/
		
		List actualList = new ArrayList();
		
		strSql = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), a.rootCauseCode, min(a.startDatetime), a.planReference, opp.sequence FROM Activity a, Daily d, OperationPlanPhase opp "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND (a.rootCauseCode is null or a.rootCauseCode != 'WW') "
				+ "AND a.internalClassCode in ('P','U','IP','IU') "
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (opp.isDeleted is null or opp.isDeleted = false) "
				+ "AND a.planReference = opp.operationPlanPhaseUid "
				+ "GROUP BY opp.operationPlanPhaseUid ORDER by opp.sequence";
		
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			
			actualList.addAll(lstResult);
		}
		
		strSql = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), a.rootCauseCode, min(a.startDatetime), a.planReference, opp.sequence FROM Activity a, Daily d, OperationPlanPhase opp "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND (a.rootCauseCode is null or a.rootCauseCode != 'WW')"
				+ "AND a.internalClassCode in ('TP','TU') "
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (opp.isDeleted is null or opp.isDeleted = false) "
				+ "AND a.planReference = opp.operationPlanPhaseUid "
				+ "GROUP BY opp.operationPlanPhaseUid ORDER by opp.sequence";
		
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			
			actualList.addAll(lstResult);
		}
		
		strSql = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), a.rootCauseCode, min(a.startDatetime), a.planReference, opp.sequence FROM Activity a, Daily d, OperationPlanPhase opp "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND (a.rootCauseCode = 'WW')"
				+ "AND a.internalClassCode in ('P','U','IP','IU') "
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (opp.isDeleted is null or opp.isDeleted = false) "
				+ "AND a.planReference = opp.operationPlanPhaseUid "
				+ "GROUP BY opp.operationPlanPhaseUid ORDER by opp.sequence";
		
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			
			actualList.addAll(lstResult);			
		}
		
		strSql = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), a.rootCauseCode, min(a.startDatetime), a.planReference, opp.sequence FROM Activity a, Daily d, OperationPlanPhase opp "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND (a.rootCauseCode = 'WW')"
				+ "AND a.internalClassCode in ('TP','TU') "
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (opp.isDeleted is null or opp.isDeleted = false) "
				+ "AND a.planReference = opp.operationPlanPhaseUid "
				+ "GROUP BY opp.operationPlanPhaseUid ORDER by opp.sequence";
		
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			
			actualList.addAll(lstResult);			
		}
		
		for(Object actualObjResult: actualList){
			
			Integer sequence = 0;
			String internalClassCode = null;
			String phaseCode = null;
			String rootCause = null;
			Double duration = 0.00;
			Date startDatetime = null;
			String planReference = null;

			Object[] actualObject = (Object[]) actualObjResult;

			String activityUid = actualObject[0].toString();
			if (actualObject[1] != null && StringUtils.isNotBlank(actualObject[1].toString())) internalClassCode = actualObject[1].toString();
			if (actualObject[2] != null	&& StringUtils.isNotBlank(actualObject[2].toString())) phaseCode = actualObject[2].toString();
			if (actualObject[3] != null && StringUtils.isNotBlank(actualObject[3].toString())) duration = Double.parseDouble(actualObject[3].toString());
			if (actualObject[4] != null && StringUtils.isNotBlank(actualObject[4].toString())) rootCause = actualObject[4].toString();
			if (actualObject[5] != null && StringUtils.isNotBlank(actualObject[5].toString())) startDatetime = (Date) actualObject[5];
			if (actualObject[6] != null && StringUtils.isNotBlank(actualObject[6].toString())) planReference = actualObject[6].toString();
			if (actualObject[7] != null && StringUtils.isNotBlank(actualObject[7].toString())) sequence = Integer.parseInt(actualObject[7].toString());

			ReportDataNode thisReportNode = reportDataNode.addChild("PlanActualPhaseCode");

			thisReportNode.addProperty("activityUid", activityUid);
			
			thisConverter.setBaseValue(duration);
			
			if("WW".equals(rootCause)) { 
				thisReportNode.addProperty("series", "WOW");
			} else {
				if("TU".equals(internalClassCode)||"TP".equals(internalClassCode))
					thisReportNode.addProperty("series", "NPT");
				else
					thisReportNode.addProperty("series", "Productive");
			}
			thisReportNode.addProperty("stacknum", "s2");
			thisReportNode.addProperty("sequence", Integer.toString(sequence));
			thisReportNode.addProperty("internal_class_code", internalClassCode);
			thisReportNode.addProperty("phase_code", phaseCode);
			thisReportNode.addProperty("phase_code_name", this.getPhaseName(phaseCode, operationType));
			thisReportNode.addProperty("duration", Double.toString(thisConverter.getConvertedValue()));
			thisReportNode.addProperty("duration_uom", thisConverter.getUomSymbol());
			thisReportNode.addProperty("start_datetime", String.valueOf(startDatetime.getTime()));
			thisReportNode.addProperty("root_cause_code", rootCause);
			thisReportNode.addProperty("planPhaseUid", planReference);
			thisReportNode.addProperty("sequence_phase", Integer.toString(sequence) + " - " + this.getPhaseName(phaseCode, operationType));
		}
		
		
		
		/*Bar Actual Without Plan Reference*/
		
		List noPlanRefList = new ArrayList();
		
		strSql = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), a.rootCauseCode, min(a.startDatetime), a.planReference FROM Activity a, Daily d "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND (a.rootCauseCode is null or a.rootCauseCode != 'WW')"
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (a.planReference IS NULL OR a.planReference = '') "
				+ "GROUP BY a.classCode, a.phaseCode ORDER by min(a.startDatetime)";
		
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			
			noPlanRefList.addAll(lstResult);
		}
		
		strSql = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), a.rootCauseCode, min(a.startDatetime), a.planReference FROM Activity a, Daily d "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND (a.rootCauseCode = 'WW')"
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (a.planReference IS NULL OR a.planReference = '') "
				+ "GROUP BY a.classCode, a.phaseCode ORDER by min(a.startDatetime)";
		
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			
			noPlanRefList.addAll(lstResult);			
		}
		
		Collections.sort(noPlanRefList, new ActualTimeComparator());
		
		for(Object noPlanRefObjResult: noPlanRefList){
			
			Integer sequence = 0;
			String internalClassCode = null;
			String phaseCode = null;
			String rootCause = null;
			Double duration = 0.00;
			Date startDatetime = null;
			String planReference = null;

			Object[] noPlanObject = (Object[]) noPlanRefObjResult;

			String activityUid = noPlanObject[0].toString();
			if (noPlanObject[1] != null && StringUtils.isNotBlank(noPlanObject[1].toString())) internalClassCode = noPlanObject[1].toString();
			if (noPlanObject[2] != null	&& StringUtils.isNotBlank(noPlanObject[2].toString())) phaseCode = noPlanObject[2].toString();
			if (noPlanObject[3] != null && StringUtils.isNotBlank(noPlanObject[3].toString())) duration = Double.parseDouble(noPlanObject[3].toString());
			if (noPlanObject[4] != null && StringUtils.isNotBlank(noPlanObject[4].toString())) rootCause = noPlanObject[4].toString();
			if (noPlanObject[5] != null && StringUtils.isNotBlank(noPlanObject[5].toString())) startDatetime = (Date) noPlanObject[5];
			if (noPlanObject[6] != null && StringUtils.isNotBlank(noPlanObject[6].toString())) planReference = noPlanObject[6].toString();

			ReportDataNode thisReportNode = reportDataNode.addChild("PlanActualPhaseCode");

			thisReportNode.addProperty("activityUid", activityUid);
			
			thisConverter.setBaseValue(duration);
			
			if("WW".equals(rootCause)) { 
				thisReportNode.addProperty("series", "WOW NOREF");
			} else {
				if("TU".equals(internalClassCode)||"TP".equals(internalClassCode))
					thisReportNode.addProperty("series", "NPT NOREF");
				else
					thisReportNode.addProperty("series", "Productive NOREF");
			}
			thisReportNode.addProperty("stacknum", "s2");
			thisReportNode.addProperty("sequence", Integer.toString(sequence));
			thisReportNode.addProperty("internal_class_code", internalClassCode);
			thisReportNode.addProperty("phase_code", phaseCode);
			thisReportNode.addProperty("phase_code_name", this.getPhaseName(phaseCode, operationType));
			thisReportNode.addProperty("duration", Double.toString(thisConverter.getConvertedValue()));
			thisReportNode.addProperty("duration_uom", thisConverter.getUomSymbol());
			thisReportNode.addProperty("start_datetime", String.valueOf(startDatetime.getTime()));
			thisReportNode.addProperty("root_cause_code", rootCause);
			thisReportNode.addProperty("planPhaseUid", planReference);
			thisReportNode.addProperty("sequence_phase", this.getPhaseName(phaseCode, operationType));
		}
		
		
		
		/*
		 * Line chart
		 * 
		 */
		
		thisConverter.setReferenceMappingField(Operation.class, "days_spent_prior_to_spud");
		
		/*Line Chart with Plan Phase Code*/
		
		String strSql2 = "SELECT opp.operationPlanPhaseUid, opp.sequence, opp.phaseCode, opp.p50Duration FROM OperationPlanMaster opm, OperationPlanPhase opp WHERE "
				+ "opm.operationPlanMasterUid = opp.operationPlanMasterUid AND opm.operationUid=:operationUid "
				+ "AND (opm.isDeleted is null or opm.isDeleted=false) AND (opp.isDeleted is null or opp.isDeleted=false) "
				+ "AND (opm.dvdPlanStatus=true and opm.dvdPlanStatus is not null) ORDER BY opp.sequence ";
		
		List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "operationUid", operationUid, qp);
		
		if(lstResult2 != null && lstResult2.size() > 0){
			
			for(Object objResult: lstResult2){
				
				Integer sequence = 0;
				String phaseCode = null;
				Double p50Duration = 0.00;
				Object[] plannedObject = (Object[]) objResult;
				
				String operationPlanPhaseUid = plannedObject[0].toString();
				if(plannedObject[1]!=null && StringUtils.isNotBlank(plannedObject[1].toString())) sequence = Integer.parseInt(plannedObject[1].toString());
				if(plannedObject[2]!=null && StringUtils.isNotBlank(plannedObject[2].toString())) phaseCode = plannedObject[2].toString();
				if(plannedObject[3]!=null && StringUtils.isNotBlank(plannedObject[3].toString())) p50Duration = Double.parseDouble(plannedObject[3].toString());
				
				ReportDataNode thisReportNode = reportDataNode.addChild("PlanActualPhaseCodeDaysCumulative");
				
				thisConverter.setBaseValue(p50Duration);
				
				thisReportNode.addProperty("planPhaseUid", operationPlanPhaseUid);
				thisReportNode.addProperty("series", "Plan");
				thisReportNode.addProperty("sequence", Integer.toString(sequence));
				thisReportNode.addProperty("internal_class_code", "PLAN");
				thisReportNode.addProperty("phase_code", phaseCode);
				thisReportNode.addProperty("phase_code_name", this.getPhaseName(phaseCode, operationType));
				thisReportNode.addProperty("duration", Double.toString(thisConverter.getConvertedValue()));
				thisReportNode.addProperty("duration_uom", thisConverter.getUomSymbol());
				thisReportNode.addProperty("start_datetime", "0");
				thisReportNode.addProperty("sequence_phase", Integer.toString(sequence) + " - " + this.getPhaseName(phaseCode, operationType));
			}
		}

		
		/* Line Actual with Plan Reference*/
		
		strSql2 = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), 'Total (including WOW)' as series, min(a.startDatetime), a.planReference, opp.sequence FROM Activity a, Daily d, OperationPlanPhase opp "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (opp.isDeleted is null or opp.isDeleted=false) "
				+ "AND a.planReference = opp.operationPlanPhaseUid "
				+ "GROUP BY opp.operationPlanPhaseUid ORDER by opp.sequence";
		
		lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues, qp);
		
		if(lstResult2 != null && lstResult2.size() > 0){
			
			for(Object actualObjResult: lstResult2){
				
				Integer sequence = 0;
				String internalClassCode = null;
				String phaseCode = null;
				Double duration = 0.00;
				Date startDatetime = null;
				String series = null;
				String planReference = null;

				Object[] actualObject = (Object[]) actualObjResult;

				String activityUid = actualObject[0].toString();
				if (actualObject[1] != null && StringUtils.isNotBlank(actualObject[1].toString())) internalClassCode = actualObject[1].toString();
				if (actualObject[2] != null	&& StringUtils.isNotBlank(actualObject[2].toString())) phaseCode = actualObject[2].toString();
				if (actualObject[3] != null && StringUtils.isNotBlank(actualObject[3].toString())) duration = Double.parseDouble(actualObject[3].toString());
				if (actualObject[4] != null && StringUtils.isNotBlank(actualObject[4].toString())) series = actualObject[4].toString();
				if (actualObject[5] != null	&& StringUtils.isNotBlank(actualObject[5].toString())) startDatetime = (Date) actualObject[5];
				if (actualObject[6] != null && StringUtils.isNotBlank(actualObject[6].toString())) planReference = actualObject[6].toString();
				if (actualObject[7] != null && StringUtils.isNotBlank(actualObject[7].toString())) sequence = Integer.parseInt(actualObject[7].toString());
				
				ReportDataNode thisReportNode = reportDataNode.addChild("PlanActualPhaseCodeDaysCumulative");
				
				thisConverter.setBaseValue(duration);

				thisReportNode.addProperty("activityUid", activityUid);
				thisReportNode.addProperty("sequence", Integer.toString(sequence));
				thisReportNode.addProperty("series", series);
				thisReportNode.addProperty("internal_class_code", internalClassCode);
				thisReportNode.addProperty("phase_code", phaseCode);
				thisReportNode.addProperty("phase_code_name", this.getPhaseName(phaseCode, operationType));
				thisReportNode.addProperty("duration", Double.toString(thisConverter.getConvertedValue()));
				thisReportNode.addProperty("duration_uom", thisConverter.getUomSymbol());
				thisReportNode.addProperty("start_datetime", String.valueOf(startDatetime.getTime()));
				thisReportNode.addProperty("planPhaseUid", planReference);
				thisReportNode.addProperty("sequence_phase", Integer.toString(sequence) + " - " + this.getPhaseName(phaseCode, operationType));
			}
		}
		
		strSql2 = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), 'Total (excluding WOW)' as series, min(a.startDatetime), a.planReference, opp.sequence FROM Activity a, Daily d, OperationPlanPhase opp "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND (a.rootCauseCode is null or a.rootCauseCode != 'WW')"
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (opp.isDeleted is null or opp.isDeleted=false) "
				+ "AND a.planReference = opp.operationPlanPhaseUid "
				+ "GROUP BY opp.operationPlanPhaseUid ORDER by opp.sequence";
		
		lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues, qp);
		
		if(lstResult2 != null && lstResult2.size() > 0){
			
			for(Object actualObjResult: lstResult2){
				
				Integer sequence = 0;
				String internalClassCode = null;
				String phaseCode = null;
				Double duration = 0.00;
				Date startDatetime = null;
				String series = null;
				String planReference = null;

				Object[] actualObject = (Object[]) actualObjResult;

				String activityUid = actualObject[0].toString();
				if (actualObject[1] != null && StringUtils.isNotBlank(actualObject[1].toString())) internalClassCode = actualObject[1].toString();
				if (actualObject[2] != null	&& StringUtils.isNotBlank(actualObject[2].toString())) phaseCode = actualObject[2].toString();
				if (actualObject[3] != null && StringUtils.isNotBlank(actualObject[3].toString())) duration = Double.parseDouble(actualObject[3].toString());
				if (actualObject[4] != null && StringUtils.isNotBlank(actualObject[4].toString())) series = actualObject[4].toString();
				if (actualObject[5] != null	&& StringUtils.isNotBlank(actualObject[5].toString())) startDatetime = (Date) actualObject[5];
				if (actualObject[6] != null && StringUtils.isNotBlank(actualObject[6].toString())) planReference = actualObject[6].toString();
				if (actualObject[7] != null && StringUtils.isNotBlank(actualObject[7].toString())) sequence = Integer.parseInt(actualObject[7].toString());
				
				ReportDataNode thisReportNode = reportDataNode.addChild("PlanActualPhaseCodeDaysCumulative");
				
				thisConverter.setBaseValue(duration);

				thisReportNode.addProperty("activityUid", activityUid);
				thisReportNode.addProperty("sequence", Integer.toString(sequence));
				thisReportNode.addProperty("series", series);
				thisReportNode.addProperty("internal_class_code", internalClassCode);
				thisReportNode.addProperty("phase_code", phaseCode);
				thisReportNode.addProperty("phase_code_name", this.getPhaseName(phaseCode, operationType));
				thisReportNode.addProperty("duration", Double.toString(thisConverter.getConvertedValue()));
				thisReportNode.addProperty("duration_uom", thisConverter.getUomSymbol());
				thisReportNode.addProperty("start_datetime", String.valueOf(startDatetime.getTime()));
				thisReportNode.addProperty("planPhaseUid", planReference);
				thisReportNode.addProperty("sequence_phase", Integer.toString(sequence) + " - " + this.getPhaseName(phaseCode, operationType));
			}
		}
		
		strSql2 = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), 'NPT (excluding WOW)' as series, min(a.startDatetime), a.planReference, opp.sequence FROM Activity a, Daily d, OperationPlanPhase opp "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND (a.rootCauseCode is null or a.rootCauseCode != 'WW')"
				+ "AND a.internalClassCode in ('TP','TU')"
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (opp.isDeleted is null or opp.isDeleted=false) "
				+ "AND a.planReference = opp.operationPlanPhaseUid "
				+ "GROUP BY opp.operationPlanPhaseUid ORDER by opp.sequence";
		
		lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues, qp);
		
		if(lstResult2 != null && lstResult2.size() > 0){
			
			for(Object actualObjResult: lstResult2){
				
				Integer sequence = 0;
				String internalClassCode = null;
				String phaseCode = null;
				Double duration = 0.00;
				Date startDatetime = null;
				String series = null;
				String planReference = null;

				Object[] actualObject = (Object[]) actualObjResult;

				String activityUid = actualObject[0].toString();
				if (actualObject[1] != null && StringUtils.isNotBlank(actualObject[1].toString())) internalClassCode = actualObject[1].toString();
				if (actualObject[2] != null	&& StringUtils.isNotBlank(actualObject[2].toString())) phaseCode = actualObject[2].toString();
				if (actualObject[3] != null && StringUtils.isNotBlank(actualObject[3].toString())) duration = Double.parseDouble(actualObject[3].toString());
				if (actualObject[4] != null && StringUtils.isNotBlank(actualObject[4].toString())) series = actualObject[4].toString();
				if (actualObject[5] != null	&& StringUtils.isNotBlank(actualObject[5].toString())) startDatetime = (Date) actualObject[5];
				if (actualObject[6] != null && StringUtils.isNotBlank(actualObject[6].toString())) planReference = actualObject[6].toString();
				if (actualObject[7] != null && StringUtils.isNotBlank(actualObject[7].toString())) sequence = Integer.parseInt(actualObject[7].toString());
				
				ReportDataNode thisReportNode = reportDataNode.addChild("PlanActualPhaseCodeDaysCumulative");
				
				thisConverter.setBaseValue(duration);

				thisReportNode.addProperty("activityUid", activityUid);
				thisReportNode.addProperty("sequence", Integer.toString(sequence));
				thisReportNode.addProperty("series", series);
				thisReportNode.addProperty("internal_class_code", internalClassCode);
				thisReportNode.addProperty("phase_code", phaseCode);
				thisReportNode.addProperty("phase_code_name", this.getPhaseName(phaseCode, operationType));
				thisReportNode.addProperty("duration", Double.toString(thisConverter.getConvertedValue()));
				thisReportNode.addProperty("duration_uom", thisConverter.getUomSymbol());
				thisReportNode.addProperty("start_datetime", String.valueOf(startDatetime.getTime()));
				thisReportNode.addProperty("planPhaseUid", planReference);
				thisReportNode.addProperty("sequence_phase", Integer.toString(sequence) + " - " + this.getPhaseName(phaseCode, operationType));
			}
		}
		
		
		/*Line Actual Without Plan Reference*/
		
		strSql2 = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), 'NOREF - Total (including WOW)' as series, min(a.startDatetime), a.planReference FROM Activity a, Daily d "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (a.planReference IS NULL OR a.planReference = '') "
				+ "GROUP BY a.phaseCode ORDER by min(a.startDatetime)";
		
		lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues, qp);
		
		if(lstResult2 != null && lstResult2.size() > 0){
			
			for(Object noPlanObjResult: lstResult2){
				
				Integer sequence = 0;
				String internalClassCode = null;
				String phaseCode = null;
				Double duration = 0.00;
				Date startDatetime = null;
				String series = null;
				String planReference = null;

				Object[] noPlanObject = (Object[]) noPlanObjResult;

				String activityUid = noPlanObject[0].toString();
				if (noPlanObject[1] != null && StringUtils.isNotBlank(noPlanObject[1].toString())) internalClassCode = noPlanObject[1].toString();
				if (noPlanObject[2] != null	&& StringUtils.isNotBlank(noPlanObject[2].toString())) phaseCode = noPlanObject[2].toString();
				if (noPlanObject[3] != null && StringUtils.isNotBlank(noPlanObject[3].toString())) duration = Double.parseDouble(noPlanObject[3].toString());
				if (noPlanObject[4] != null && StringUtils.isNotBlank(noPlanObject[4].toString())) series = noPlanObject[4].toString();
				if (noPlanObject[5] != null	&& StringUtils.isNotBlank(noPlanObject[5].toString())) startDatetime = (Date) noPlanObject[5];
				if (noPlanObject[6] != null && StringUtils.isNotBlank(noPlanObject[6].toString())) planReference = noPlanObject[6].toString();
				
				ReportDataNode thisReportNode = reportDataNode.addChild("PlanActualPhaseCodeDaysCumulative");
				
				thisConverter.setBaseValue(duration);

				thisReportNode.addProperty("activityUid", activityUid);
				thisReportNode.addProperty("sequence", Integer.toString(sequence));
				thisReportNode.addProperty("series", series);
				thisReportNode.addProperty("internal_class_code", internalClassCode);
				thisReportNode.addProperty("phase_code", phaseCode);
				thisReportNode.addProperty("phase_code_name", this.getPhaseName(phaseCode, operationType));
				thisReportNode.addProperty("duration", Double.toString(thisConverter.getConvertedValue()));
				thisReportNode.addProperty("duration_uom", thisConverter.getUomSymbol());
				thisReportNode.addProperty("start_datetime", String.valueOf(startDatetime.getTime()));
				thisReportNode.addProperty("planPhaseUid", planReference);
				thisReportNode.addProperty("sequence_phase", this.getPhaseName(phaseCode, operationType));
			}
		}
		
		strSql2 = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), 'NOREF - Total (excluding WOW)' as series, min(a.startDatetime), a.planReference FROM Activity a, Daily d "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND (a.rootCauseCode is null or a.rootCauseCode != 'WW')"
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (a.planReference IS NULL OR a.planReference = '') "
				+ "GROUP BY a.phaseCode ORDER by min(a.startDatetime)";
		
		lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues, qp);
		
		if(lstResult2 != null && lstResult2.size() > 0){
			
			for(Object noPlanObjResult: lstResult2){
				
				Integer sequence = 0;
				String internalClassCode = null;
				String phaseCode = null;
				Double duration = 0.00;
				Date startDatetime = null;
				String series = null;
				String planReference = null;

				Object[] noPlanObject = (Object[]) noPlanObjResult;

				String activityUid = noPlanObject[0].toString();
				if (noPlanObject[1] != null && StringUtils.isNotBlank(noPlanObject[1].toString())) internalClassCode = noPlanObject[1].toString();
				if (noPlanObject[2] != null	&& StringUtils.isNotBlank(noPlanObject[2].toString())) phaseCode = noPlanObject[2].toString();
				if (noPlanObject[3] != null && StringUtils.isNotBlank(noPlanObject[3].toString())) duration = Double.parseDouble(noPlanObject[3].toString());
				if (noPlanObject[4] != null && StringUtils.isNotBlank(noPlanObject[4].toString())) series = noPlanObject[4].toString();
				if (noPlanObject[5] != null	&& StringUtils.isNotBlank(noPlanObject[5].toString())) startDatetime = (Date) noPlanObject[5];
				if (noPlanObject[6] != null && StringUtils.isNotBlank(noPlanObject[6].toString())) planReference = noPlanObject[6].toString();
				
				ReportDataNode thisReportNode = reportDataNode.addChild("PlanActualPhaseCodeDaysCumulative");
				
				thisConverter.setBaseValue(duration);

				thisReportNode.addProperty("activityUid", activityUid);
				thisReportNode.addProperty("sequence", Integer.toString(sequence));
				thisReportNode.addProperty("series", series);
				thisReportNode.addProperty("internal_class_code", internalClassCode);
				thisReportNode.addProperty("phase_code", phaseCode);
				thisReportNode.addProperty("phase_code_name", this.getPhaseName(phaseCode, operationType));
				thisReportNode.addProperty("duration", Double.toString(thisConverter.getConvertedValue()));
				thisReportNode.addProperty("duration_uom", thisConverter.getUomSymbol());
				thisReportNode.addProperty("start_datetime", String.valueOf(startDatetime.getTime()));
				thisReportNode.addProperty("planPhaseUid", planReference);
				thisReportNode.addProperty("sequence_phase", this.getPhaseName(phaseCode, operationType));
			}
		}
		
		strSql2 = "SELECT a.activityUid, a.internalClassCode, a.phaseCode, sum(a.activityDuration), 'NOREF - NPT (excluding WOW)' as series, min(a.startDatetime), a.planReference FROM Activity a, Daily d "
				+ "WHERE (a.isDeleted is null or a.isDeleted=false) AND (a.dayPlus is null OR a.dayPlus=0) "
				+ "AND (a.isOffline is null or a.isOffline=false) AND (a.isSimop is null or a.isSimop=false) "
				+ "AND (a.rootCauseCode is null or a.rootCauseCode != 'WW')"
				+ "AND a.internalClassCode in ('TP','TU')"
				+ "AND a.operationUid=:operationUid "
				+ "AND (d.isDeleted is null or d.isDeleted = false) "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND (a.planReference IS NULL OR a.planReference = '') "
				+ "GROUP BY a.phaseCode ORDER by min(a.startDatetime)";
		
		lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues, qp);
		
		if(lstResult2 != null && lstResult2.size() > 0){
			
			for(Object noPlanObjResult: lstResult2){
				
				Integer sequence = 0;
				String internalClassCode = null;
				String phaseCode = null;
				Double duration = 0.00;
				Date startDatetime = null;
				String series = null;
				String planReference = null;

				Object[] noPlanObject = (Object[]) noPlanObjResult;

				String activityUid = noPlanObject[0].toString();
				if (noPlanObject[1] != null && StringUtils.isNotBlank(noPlanObject[1].toString())) internalClassCode = noPlanObject[1].toString();
				if (noPlanObject[2] != null	&& StringUtils.isNotBlank(noPlanObject[2].toString())) phaseCode = noPlanObject[2].toString();
				if (noPlanObject[3] != null && StringUtils.isNotBlank(noPlanObject[3].toString())) duration = Double.parseDouble(noPlanObject[3].toString());
				if (noPlanObject[4] != null && StringUtils.isNotBlank(noPlanObject[4].toString())) series = noPlanObject[4].toString();
				if (noPlanObject[5] != null	&& StringUtils.isNotBlank(noPlanObject[5].toString())) startDatetime = (Date) noPlanObject[5];
				if (noPlanObject[6] != null && StringUtils.isNotBlank(noPlanObject[6].toString())) planReference = noPlanObject[6].toString();
				
				ReportDataNode thisReportNode = reportDataNode.addChild("PlanActualPhaseCodeDaysCumulative");
				
				thisConverter.setBaseValue(duration);

				thisReportNode.addProperty("activityUid", activityUid);
				thisReportNode.addProperty("sequence", Integer.toString(sequence));
				thisReportNode.addProperty("series", series);
				thisReportNode.addProperty("internal_class_code", internalClassCode);
				thisReportNode.addProperty("phase_code", phaseCode);
				thisReportNode.addProperty("phase_code_name", this.getPhaseName(phaseCode, operationType));
				thisReportNode.addProperty("duration", Double.toString(thisConverter.getConvertedValue()));
				thisReportNode.addProperty("duration_uom", thisConverter.getUomSymbol());
				thisReportNode.addProperty("start_datetime", String.valueOf(startDatetime.getTime()));
				thisReportNode.addProperty("planPhaseUid", planReference);
				thisReportNode.addProperty("sequence_phase", this.getPhaseName(phaseCode, operationType));
			}
		}
		
	}
	
	private class ActualTimeComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Object in1 = o1[5];
				Object in2 = o2[5];
				
				Date f1 = (Date) in1;
				Date f2 = (Date) in2;
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	private String getPhaseName(String phaseCode, String operationType) throws Exception {
				
		String phaseName= "";
		
		String strSql = "SELECT name FROM LookupPhaseCode WHERE (isDeleted is null OR isDeleted = false) AND shortCode = :thisPhaseCode AND (operationCode = :thisOperationType OR operationCode = '' OR operationCode is null)";
		String[] paramsFields = {"thisPhaseCode", "thisOperationType"};
		Object[] paramsValues = {phaseCode, operationType};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (lstResult.size()>0)
		{
			Object lookupPhaseCodeResult = (Object) lstResult.get(0);
			if(lookupPhaseCodeResult != null) phaseName = lookupPhaseCodeResult.toString();
		}
		
		return phaseName;
	}

	@Override
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

}
