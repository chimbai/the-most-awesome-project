package com.idsdatanet.d2.drillnet.holeSection;

import org.springframework.context.ApplicationEvent;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.web.security.acl.ACLKey;
import com.idsdatanet.d2.core.web.security.acl.ACLKeyResolver;
import com.idsdatanet.d2.core.web.security.acl.AccessConditions;

public class HoleSectionDataNodeAllowedAction implements  ACLKeyResolver {
	

	

	public void refreshACLCacheIfAny(ApplicationEvent event) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void resolveACLKey(ACLKey aclKey, AccessConditions accessConditions)
			throws Exception {
		// TODO Auto-generated method stub
		if ("1".equals(GroupWidePreference.getValue(accessConditions.getUserSession().getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_WELLBORE_PROPERTIES)))
			aclKey.setACLKeyValue(false);
	}
}
