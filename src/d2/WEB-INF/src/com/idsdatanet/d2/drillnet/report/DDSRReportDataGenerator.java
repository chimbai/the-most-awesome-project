package com.idsdatanet.d2.drillnet.report;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.GeneralComment;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.hibernate.tables.UomTemplate;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class DDSRReportDataGenerator implements ReportDataGenerator {
	private String uomTemplateName = null;
	
	public void setUomTemplateName(String uomTemplateName){
		this.uomTemplateName = uomTemplateName;
	}
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		
		if (getUOMTemplate(uomTemplateName, userSelection.getGroupUid())!=null){
			userSelection.setUomTemplateUid(getUOMTemplate(uomTemplateName, userSelection.getGroupUid()));
		}

		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		if (daily == null) return;
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, daily.getOperationUid());
		
		if (operation!=null){
			ReportDataNode child = reportDataNode.addChild("operation");
			
			Map<String,LookupItem> typeIntentLookup = LookupManager.getConfiguredInstance().getLookup("xml://well.typeIntent?key=code&amp;value=label", userContext.getUserSelection(), null);
			if (operation.getTypeIntent()!=null) child.addProperty("typeIntent", getLookupValue(typeIntentLookup, operation.getTypeIntent()));
			
			Map<String,LookupItem> wellTeamLookup = LookupManager.getConfiguredInstance().getLookup("xml://operation.wellTeam?key=code&amp;value=label", userContext.getUserSelection(), null);
			if (operation.getWellStatus()!=null) child.addProperty("wellStatus", getLookupValue(wellTeamLookup, operation.getWellStatus())); 
			
			if (operation.getAfe()!=null) {	
				child.addProperty("afe",  String.format("%.0f", operation.getAfe()));
			}
			
			Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, operation.getWellUid());
			if (well!=null){
				child.addProperty("wellName", well.getWellName()); 
				
				Map<String,LookupItem> assetTeamLookup = LookupManager.getConfiguredInstance().getLookup("xml://well.asset?key=code&amp;value=label", userContext.getUserSelection(), null);
				if (well.getAssetTeam()!=null) child.addProperty("assetTeam", getLookupValue(assetTeamLookup, well.getAssetTeam()));
			}
			
			Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
			if (wellbore!=null){
				Map<String,LookupItem> wellboreFinalPurposeLookup = LookupManager.getConfiguredInstance().getLookup("xml://wellbore.wellboreFinalPurpose?key=code&amp;value=label", userContext.getUserSelection(), null);
				if (wellbore.getWellboreFinalPurpose()!=null) child.addProperty("wellboreFinalPurpose", getLookupValue(wellboreFinalPurposeLookup, wellbore.getWellboreFinalPurpose())); 
			}
			
			//21813
			if (operation.getSpudDate() !=null) child.addProperty("spudDate", operation.getSpudDate().toString()); 
			if (operation.getDryHoleEndDateTime() !=null) child.addProperty("dryHoleEndDateTime", operation.getDryHoleEndDateTime().toString()); 
			if (operation.getRigOffHireDate() !=null) child.addProperty("rigOffHireDate", operation.getRigOffHireDate().toString()); 
			
			Double depthMdMslY = 0.0;
			Daily yesterday = ApplicationUtils.getConfiguredInstance().getYesterday(daily.getOperationUid(), daily.getDailyUid());
			
			if (yesterday!=null){
				String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid AND reportType <> 'DGR' ";
				String[] paramsFieldY = {"operationUid", "dailyUid"};
				Object[] paramsValueY = {yesterday.getOperationUid(), yesterday.getDailyUid()};
				List<ReportDaily> resultY = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldY, paramsValueY);
				
				if (resultY.size() > 0){
					for (ReportDaily rdY : resultY){
						if (rdY.getDepthMdMsl()!=null) {
							thisConverter.setReferenceMappingField(ReportDaily.class, "depthMdMsl");
							depthMdMslY = this.getConvertedUOM(thisConverter, rdY.getDepthMdMsl(), "Metre", true);
						}
					}
				}
			}
			
			String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid AND reportType <> 'DGR' ";
			String[] paramsFields = {"operationUid", "dailyUid"};
			Object[] paramsValues = {daily.getOperationUid(), daily.getDailyUid()};
			List<ReportDaily> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			if (result.size() > 0){
				for (ReportDaily rd : result){
					if(rd.getReportType().equals("CMPLT")) {
						child.addProperty("operatingCompanyRepDay", rd.getCompEngineer());
						child.addProperty("operatingCompanyRepNight", rd.getSentby());
					}
					else {
						child.addProperty("operatingCompanyRepDay", rd.getOperatingCompanyRepDay());
						child.addProperty("operatingCompanyRepNight", rd.getOperatingCompanyRepNight());
					}
					
					RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, rd.getRigInformationUid());
					if (rig!=null){
						child.addProperty("rigInformationUid", rig.getRigName());
						if (rig.getRigOwner()!=null){
							LookupCompany company = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, rig.getRigOwner());
							if (company!=null) child.addProperty("rigOwner", company.getCompanyName());
						}
					}
					
					if (rd.getMudengineerCo()!=null){
						LookupCompany mudCompany = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, rd.getMudengineerCo());
						if (mudCompany!=null) child.addProperty("mudengineerCo", mudCompany.getCompanyName());
					}
					
					if (rd.getDirectionalCo()!=null){
						LookupCompany dirCompany = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, rd.getDirectionalCo());
						if (dirCompany!=null) child.addProperty("directionalCo", dirCompany.getCompanyName());
					}		
					
					Double depthMdMsl = 0.0;
					if (rd.getDepthMdMsl()!=null) {
						thisConverter.setReferenceMappingField(ReportDaily.class, "depthMdMsl");
						depthMdMsl = this.getConvertedUOM(thisConverter, rd.getDepthMdMsl(), "Metre", true);
						child.addProperty("depthMdMsl", Double.toString(depthMdMsl));
					}
					
					Double prog = depthMdMsl - depthMdMslY;
					child.addProperty("prog", prog.toString());
					
					String operationSummary = null;
					
					if (StringUtils.isNotBlank(rd.getReportPeriodSummary())){
						operationSummary = rd.getReportPeriodSummary();
						child.addProperty("reportPeriodSummary", rd.getReportPeriodSummary());
					}
					
					if (StringUtils.isNotBlank(rd.getReportPlanSummary())){
						if (operationSummary!=null){
							operationSummary = operationSummary + "\n";
						}else{
							operationSummary = "";
						}
						operationSummary = operationSummary + rd.getReportPlanSummary();
						child.addProperty("reportPlanSummary", rd.getReportPlanSummary());
					}

					if (operationSummary==null){
						operationSummary = "";
					}
					child.addProperty("operationSummary", operationSummary);
					
					
					Double daysOnWell = null;
					thisConverter.setReferenceMappingField(Operation.class, "days_spent_prior_to_spud");
					thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysOnOperation(daily.getOperationUid(), daily.getDailyUid()));
					daysOnWell = thisConverter.getConvertedValue();
					child.addProperty("daysOnWell", daysOnWell.toString());
					
					String[] paramsFieldC = {"todayDate", "operationUid", "reportType"};
					Object[] paramsValueC = new Object[3]; 
					paramsValueC[0] = daily.getDayDate(); 
					paramsValueC[1] = daily.getOperationUid();
					paramsValueC[2] = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(daily.getOperationUid());
					
					strSql = "select sum(daycompletioncost) as cumcompletioncost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
					List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldC, paramsValueC);
					Object a = lstResult.get(0);
					Double cumcompletioncost = null;
					if (a != null) cumcompletioncost = Double.parseDouble(a.toString());
					
					strSql = "select sum(daytangiblecost) as cumtangiblecost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldC, paramsValueC);
					a = lstResult.get(0);
					Double cumtangiblecost = null;
					if (a != null) cumtangiblecost = Double.parseDouble(a.toString());
					
					strSql = "select sum(otherCost) as cumothercost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldC, paramsValueC);
					a = lstResult.get(0);
					Double cumothercost = null;
					if (a != null) cumothercost = Double.parseDouble(a.toString());
					
					strSql = "select sum(daytestcost) as cumtestcost from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldC, paramsValueC);
					a = lstResult.get(0);
					Double cumtestcost = null;
					if (a != null) {
						cumtestcost = Double.parseDouble(a.toString());
					}
					
					Double cumCost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(rd.getDailyUid(), rd.getReportType());
					Double cumtotalcost = null;
					if ((cumCost!=null) || (cumcompletioncost !=null) || (cumtangiblecost !=null) || (cumothercost !=null)) {
						cumtotalcost = 0.0;
						if (cumCost!=null) cumtotalcost += cumCost;
						if (cumcompletioncost!=null) cumtotalcost += cumcompletioncost;
						if (cumtangiblecost!=null) cumtotalcost += cumtangiblecost;
						if (cumothercost!=null) cumtotalcost += cumothercost;
						if (cumtestcost!=null) cumtotalcost += cumtestcost;
					}
					
					if (cumtotalcost!=null) {
						child.addProperty("cumtotalcost", String.format("%.0f", cumtotalcost));
					}
				}
			}
			
			strSql = "FROM GeneralComment WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid AND operationUid = :operationUid AND category='cctv'";
			String[] paramsFieldGc = {"operationUid", "dailyUid"};
			Object[] paramsValueGc = {daily.getOperationUid(), daily.getDailyUid()};
			List<GeneralComment> resultGc = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldGc, paramsValueGc);
			
			if (resultGc.size() > 0){
				String comments = "";
				for (GeneralComment gc : resultGc){
					comments = comments + gc.getComments() + "\n";
				}
				child.addProperty("comments", comments);
			}
			
			Double depthMdMsl = null;
			strSql = "FROM OperationPlanPhase WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid ORDER BY depthMdMsl DESC";
			String[] paramsFieldP = {"operationUid"};
			Object[] paramsValueP = {daily.getOperationUid()};
			List<OperationPlanPhase> resultP = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldP, paramsValueP);
			
			if (resultP.size() > 0){
				for (OperationPlanPhase opp : resultP){
					if (opp.getDepthMdMsl()!=null) {
						depthMdMsl = opp.getDepthMdMsl();
						break;
					}
				}
				if (depthMdMsl!=null){
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "depthMdMsl");
					child.addProperty("plan_depthMdMsl", Double.toString(this.getConvertedUOM(thisConverter, depthMdMsl, "Metre", true)));
				}
			}
			
			Double cumP50 = 0.00;
			boolean p50 = false;
			strSql = "FROM OperationPlanPhase WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid ORDER BY p50Duration DESC";
			String[] paramsFieldPD = {"operationUid"};
			Object[] paramsValuePD = {daily.getOperationUid()};
			List<OperationPlanPhase> resultPD = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldPD, paramsValuePD);
			
			if (resultPD.size() > 0){
				for (OperationPlanPhase oppD : resultPD){
					if (oppD.getP50Duration()!=null) {
						cumP50 = cumP50 + Double.valueOf(String.format("%.2f", oppD.getP50Duration()));
						p50 = true;
					}
				}
				if (p50 && cumP50!=null) child.addProperty("cumP50Duration", cumP50.toString());
			}		

		}
		
	}
	
	private String getLookupValue(Map<String,LookupItem> lookupList, Object lookupvalue) throws Exception {
		String retValue = "";
		
		if (lookupvalue !=null && StringUtils.isNotBlank(lookupvalue.toString())  && lookupList !=null){
			 LookupItem lookup = lookupList.get(lookupvalue.toString());
			if (lookup !=null)	retValue = lookup.getValue().toString();					
		} 
		
		return retValue;
	}
	
	private String getUOMTemplate(String uomTemplateName, String groupUid) throws Exception{
		if(uomTemplateName!=null){
			String strSql = "FROM UomTemplate WHERE (isDeleted = false or isDeleted is null) and name=:name and groupUid=:groupUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] { "name", "groupUid" }, new Object[] { uomTemplateName, groupUid });
			
			if (lstResult.size() > 0){
				UomTemplate defaultTemplate = (UomTemplate) lstResult.get(0);
				if (defaultTemplate!=null) {
					return defaultTemplate.getUomTemplateUid();
				}
			}
		}
		
		return null;
	}
	
	private Double getConvertedUOM(CustomFieldUom thisConverter, Double value, String unit, Boolean offset) throws Exception{
		if (thisConverter!=null){
			thisConverter.setBaseValueFromUserValue(value);
			thisConverter.changeUOMUnit(unit);
			if (offset) thisConverter.addDatumOffset();
			
			return thisConverter.getConvertedValue();
		}

		return null;
	}
}
