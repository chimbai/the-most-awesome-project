package com.idsdatanet.d2.drillnet.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.graph.BaseGraph;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class timeBreakdownPie extends BaseGraph {
	private String graphTitle=null;
	
	public String getGraphTitle() {
		return graphTitle;
	}

	public void setGraphTitle(String graphTitle) {
		this.graphTitle = graphTitle;
	}
	
	
	public Object paint() throws Exception
	{
		String opsUid = this.getCurrentUserContext().getUserSelection().getOperationUid();
		String dailyUid = this.getCurrentUserContext().getUserSelection().getDailyUid();
		
		this.setFileName("TB_"+dailyUid+".jpg");
		
		return timeBreakdownPie.createChart(timeBreakdownPie.createDataset(opsUid, dailyUid));
	}
	
	private static PieDataset createDataset(String opsUid, String dailyUid) throws Exception 
	{
		DefaultPieDataset dataset = new DefaultPieDataset();
		
		String selectReportDailySql = "SELECT reportDatetime FROM ReportDaily " +
		"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
		"AND dailyUid=:thisDailyUid ";
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(selectReportDailySql, "thisDailyUid", dailyUid);
		
		if(!lstResult.isEmpty()){
			Date thisReportDatetime = (Date) lstResult.get(0);
			String reportDailySql = "SELECT dailyUid FROM ReportDaily " +
			"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
			"AND operationUid=:operationUid " +
			"AND reportDatetime <=:thisReportDatetime " +
			"ORDER BY reportDatetime ASC";
			
			String[] reportDailyParamsFields = {"thisReportDatetime", "operationUid"};
			Object[] reportDailyParamsValues = {thisReportDatetime, opsUid};
			
			List<String> dailyLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(reportDailySql, reportDailyParamsFields, reportDailyParamsValues);
			
			if (!dailyLst.isEmpty())
			{
				String activitySql = "SELECT classCode, sum(activityDuration) " +
				"FROM Activity " +
				"WHERE (isDeleted is null or isDeleted=false) " +
				"AND (isSimop=false or isSimop is null) " +
				"AND (isOffline=false or isOffline is null) " +
				"AND (dayPlus IS NULL OR dayPlus=0) " +
				"AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') " + 
				"AND (dailyUid in (:thisDailyUid)) " +
				"GROUP BY classCode";
				
				List<Object[]> classCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(activitySql, "thisDailyUid", dailyLst);
				
				if (!classCode.isEmpty())
				{
					for (Object[] obj : classCode)
					{
						String clsCode = (String) obj[0];
						Double duration = (Double) obj[1];
						if (StringUtils.isBlank(clsCode))
							clsCode = "NULL";
						
						dataset.setValue(clsCode, duration);
					}
				}
			}
		}

		return dataset;
	}
	
	private static JFreeChart createChart (PieDataset dataset) throws Exception
	{
		JFreeChart chart = ChartFactory.createPieChart(null,dataset,false,true,false);
		chart.setBackgroundPaint(Color.WHITE);
		 
		 PiePlot plot = (PiePlot) chart.getPlot();
		 plot.setBackgroundPaint(Color.WHITE);
		 plot.setInteriorGap(0.04);
		 plot.setOutlineVisible(false);
		 
		 plot.setSectionOutlinesVisible(false);
		 plot.setBaseSectionOutlineStroke(new BasicStroke(2.0f));
		 
		// customise the section label appearance
		 PieSectionLabelGenerator generator = new StandardPieSectionLabelGenerator(
		            "{0}, {2}", new DecimalFormat("0"), new DecimalFormat("0.00%"));
		 plot.setLabelGenerator(generator);
		 plot.setLabelFont(new Font("Arial", Font.PLAIN, 45));
		 plot.setLabelLinkPaint(Color.BLACK);
		 plot.setLabelLinkStroke(new BasicStroke(2.0f));
		 plot.setLabelOutlineStroke(null);
		 plot.setLabelPaint(Color.BLACK);
		 plot.setLabelBackgroundPaint(null);
		 
		return chart;
	}
	
}
