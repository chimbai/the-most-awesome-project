package com.idsdatanet.d2.drillnet.kick;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.*;
import com.idsdatanet.d2.core.web.mvc.*;
import java.util.*;

public class KickLinkedInfo {
	private Activity linkedActivity = null;
	
	public KickLinkedInfo(Kick kick) throws Exception {
		if(kick.getActivityUid() != null){
			List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Activity where (isDeleted = false or isDeleted is null) and activityUid = :activityUid", "activityUid", kick.getActivityUid());
			if(! rs.isEmpty()) this.linkedActivity = (Activity) rs.get(0);
		}
	}

	public void dispose(){
		this.linkedActivity = null;
	}
	
	public Activity getLinkedActivity(){
		return this.linkedActivity;
	}
	
	public String getFormationName(Boolean wellboreBased) throws Exception {
		if(this.linkedActivity == null) return null;
		if(this.linkedActivity.getDepthMdMsl() == null) return null;
		
		if (wellboreBased){
			String[] paramsFields = { "wellboreUid", "activityDepth"};
			Object[] paramsValues = { this.linkedActivity.getWellboreUid(), this.linkedActivity.getDepthMdMsl()};
			
			QueryProperties qp = new QueryProperties();
			qp.setFetchFirstRowOnly();
				
			List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Formation where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid and sampleTopMdMsl <= :activityDepth order by sampleTopMdMsl desc", paramsFields, paramsValues, qp);
			
			
			
			if(rs.isEmpty()) return "N/A";
			
			Formation formation = (Formation) rs.get(0);
			return formation.getFormationName();
		}	
		else{

			String[] paramsFields = {"wellUid", "activityDepth"};
			Object[] paramsValues = {this.linkedActivity.getWellUid(), this.linkedActivity.getDepthMdMsl()};
		
		
			QueryProperties qp = new QueryProperties();
			qp.setFetchFirstRowOnly();
			
			List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Formation where (isDeleted = false or isDeleted is null) and sampleTopMdMsl <= :activityDepth and wellUid = :wellUid order by sampleTopMdMsl desc", paramsFields, paramsValues, qp);
		
		
		
			if(rs.isEmpty()) return "N/A";
		
			Formation formation = (Formation) rs.get(0);
			return formation.getFormationName();
		}
	}
}
