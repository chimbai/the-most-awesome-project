package com.idsdatanet.d2.drillnet.wellExternalCatalog;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * This lookup handler is for (most of) the filters on the WellExternalCatalog screen.
 * These get distinct values for WellName, uwi, uwbi, country and field based on your selection.
 * Fetches the filter values via dynaAttrs, and is re-run based on the bean-config property reloadLookupOnPageSubmit=true 
 * and where the filter fields perform the action onChange="submitCurrentNodeAndReload".
 * @author Sheldon James Cameron
 * 23 Aug 2022
 */
public class WellExternalCatalogLookupHandler implements LookupHandler  {
	private String column = null;
	
	@SuppressWarnings("unchecked")
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot selectionSnapshot, HttpServletRequest request, LookupCache cache) throws Exception {
		Map<String, LookupItem> result = cache.getFromCache(this.getClass().getName());
		if(result != null) return result;
		// set up some params and values
		ArrayList<String> params = new ArrayList<String>();
		ArrayList<Object> values = new ArrayList<Object>();
		// add the initial param/value of the selected column property defined for this lookup at the WEC bean-config
		params.add("column");
		values.add(this.column);
		// set up hql
		String hql = "SELECT DISTINCT "+this.column+" FROM WellExternalCatalog "
				+ "WHERE (isDeleted is null or isDeleted = 0) "
				+ "and (:column is not null or :column = '') ";

		// add any additional params to filter out other lookup values further based on the selected filter options
		// e.g., filter out the uwbi, country, field that is available for server A or wellName/uwi B.
		// grab all the filter values from the dynaAttrs
		String server = (String) commandBean.getRoot().getDynaAttr().get("server");
		String wellName = (String) commandBean.getRoot().getDynaAttr().get("wellName");
		String uwi = (String) commandBean.getRoot().getDynaAttr().get("uwi");
		String country = (String) commandBean.getRoot().getDynaAttr().get("country");
		String uwbi = (String) commandBean.getRoot().getDynaAttr().get("uwbi");
		String field = (String) commandBean.getRoot().getDynaAttr().get("field");
		// add further narrow down the filter options based on prior selected filters
		if(StringUtils.isNotBlank(server)) {
			hql += "AND importExportServerPropertiesUid=:serverUid ";
			params.add("serverUid");
			values.add(server);
		}
		if(StringUtils.isNotBlank(country)) {
			hql += "AND country=:country ";
			params.add("country");
			values.add(country);
		}
		if(StringUtils.isNotBlank(field)) {
			hql += "AND field=:field ";
			params.add("field");
			values.add(field);
		}
		if(StringUtils.isNotBlank(uwbi)) {
			hql += "AND uwbi=:uwbi ";
			params.add("uwbi");
			values.add(uwbi);
		}
		// for wellName/uwi lookups specifically (wellName and uwi column) skip them if the lookup is already populated 
		// e.g., you choose a uwi, wellName/uwi will be populated, 
		// and every other filter/column except wellName/uwi goes through here
		if((StringUtils.isNotBlank(wellName) && !"wellName".equalsIgnoreCase(this.column))
				&& (StringUtils.isNotBlank(uwi) && !"uwi".equalsIgnoreCase(this.column))) {
			hql += "AND wellName=:wellName ";
			params.add("wellName");
			values.add(wellName);
			hql += "AND uwi=:uwi ";
			params.add("uwi");
			values.add(uwi);
		}
		// add the last statement to order the lookup values
		hql += "ORDER BY :column ASC";
		// fetch and return
		List <Object> lsResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, params, values);
		result = new LinkedHashMap<String, LookupItem>();
		if (lsResult.size() > 0) {
			for (Object obj : lsResult) {
				result.put(obj.toString(), new LookupItem(obj.toString(), obj.toString()));
			}
		}
		
		return result;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}
}
