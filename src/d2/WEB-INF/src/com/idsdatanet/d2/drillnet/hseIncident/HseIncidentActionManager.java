package com.idsdatanet.d2.drillnet.hseIncident;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;

public class HseIncidentActionManager implements ActionManager {
	
	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if(com.idsdatanet.d2.core.web.mvc.Action.SAVE.equals(action)){
			
			Object object = node.getData();
			
			if (object instanceof LaggingIndicator) {
				return new SaveLaggingIndicatorActionHandler();
			}
			
			if (object instanceof LeadingIndicator) {
				return new SaveLeadingIndicatorActionHandler();
			}
			
			if (object instanceof OtherIndicator) {
				return new SaveOtherIndicatorActionHandler();
			}
			if (object instanceof DailyObservation) {
				return new SaveDailyObservationActionHandler();
			}
			
			return null;
			
		}else{
			return null;
		}
	}
	
	private class SaveLaggingIndicatorActionHandler implements ActionHandler{

		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {

				LaggingIndicator laggingIndicator = (LaggingIndicator) node.getData();
			
				if (laggingIndicator !=null) {
					
						HseIncident hseIncident = new HseIncident();
						
						PropertyUtils.copyProperties(hseIncident, laggingIndicator);
						hseIncident.setSystemStatus("lagging");
						
						if (laggingIndicator.getNumberOfIncidents() == null) {
							hseIncident.setNumberOfIncidents(1);
						}else {
							hseIncident.setNumberOfIncidents(laggingIndicator.getNumberOfIncidents());
						}
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(hseIncident);
	
				}
				
				
			
		return new ActionHandlerResponse(true);
		}
		
	}
	
	
	private class SaveLeadingIndicatorActionHandler implements ActionHandler{

		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {
	
			
				LeadingIndicator leadingIndicator = (LeadingIndicator) node.getData();
			
				if (leadingIndicator !=null) {
					
						HseIncident hseIncident = new HseIncident();
						
						PropertyUtils.copyProperties(hseIncident, leadingIndicator);
						hseIncident.setSystemStatus("leading");
						
						if (leadingIndicator.getNumberOfIncidents() == null) {
							hseIncident.setNumberOfIncidents(1);
						}else {
							hseIncident.setNumberOfIncidents(leadingIndicator.getNumberOfIncidents());
						}
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(hseIncident);
	
				}
				
			
		return new ActionHandlerResponse(true);
		}
		
	}
	
	private class SaveOtherIndicatorActionHandler implements ActionHandler{

		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {
	
			
				OtherIndicator otherIndicator = (OtherIndicator) node.getData();
			
				if (otherIndicator !=null) {
					
						HseIncident hseIncident = new HseIncident();
						
						PropertyUtils.copyProperties(hseIncident, otherIndicator);
						hseIncident.setSystemStatus("other");
						
						if (otherIndicator.getNumberOfIncidents() == null) {
							hseIncident.setNumberOfIncidents(1);
						}else {
							hseIncident.setNumberOfIncidents(otherIndicator.getNumberOfIncidents());
						}
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(hseIncident);
	
				}
				
			
		return new ActionHandlerResponse(true);
		}
		
	}
	private class SaveDailyObservationActionHandler implements ActionHandler{

		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {
	
			
				DailyObservation dailyObservation = (DailyObservation) node.getData();
			
				if (dailyObservation !=null) {
					
						HseIncident hseIncident = new HseIncident();
						
						PropertyUtils.copyProperties(hseIncident, dailyObservation);
						hseIncident.setSystemStatus("observation");
						
						if (dailyObservation.getNumberOfIncidents() == null) {
							hseIncident.setNumberOfIncidents(1);
						}else {
							hseIncident.setNumberOfIncidents(dailyObservation.getNumberOfIncidents());
						}
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(hseIncident);
	
				}
				
			
		return new ActionHandlerResponse(true);
		}
		
	}
}
