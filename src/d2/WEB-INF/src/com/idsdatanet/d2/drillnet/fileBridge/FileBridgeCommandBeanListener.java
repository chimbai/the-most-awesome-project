package com.idsdatanet.d2.drillnet.fileBridge;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.FileBridgeFiles;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.depot.d2.filebridge.util.FileBridgeUtils;

public class FileBridgeCommandBeanListener extends EmptyCommandBeanListener {

	public final String FILEBRIDGE_JOB_SERVICE_KEY = "fileBridgeJob"; 
	
	@Override
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		String errorMsg = FileBridgeUtils.checkEnabledFileBridgeJob();
		if(StringUtils.isNotBlank(errorMsg)) {
			commandBean.getSystemMessage().addError(errorMsg, true);
		}
	}
	
	@Override
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
		String fileBridgeFilesUid = (String) request.getParameter("fileBridgeFilesUid");
		if (StringUtils.isNotBlank(fileBridgeFilesUid)) {
			if (invocationKey.equals("reparse")) {
				// do reparse
			} else if (invocationKey.equals("reprocess")) {
				FileBridgeFiles fbf = (FileBridgeFiles) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(FileBridgeFiles.class, fileBridgeFilesUid);
				fbf.setDocparserDocumentId(null);
				fbf.setStatus("Uploaded");
				fbf.setRemark(null);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(fbf);
			} else if (invocationKey.equals("statusCheck")) {
				FileBridgeFiles fbf = (FileBridgeFiles) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(FileBridgeFiles.class, fileBridgeFilesUid);
				String hql = "FROM CommonLookup WHERE lookupTypeSelection = 'FileBridge.status' AND shortCode =:shortCode AND (isDeleted IS NULL OR isDeleted = FALSE) AND (isActive IS NULL OR isActive = TRUE)";
				CommonLookup lookup = (CommonLookup) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "shortCode", fbf.getStatus()).get(0);
				
				String strResult = "<status>";
				strResult += lookup.getLookupLabel() + "</status>";
				response.getWriter().write(strResult);
			} else if (invocationKey.equals("statusRemark")) {
				FileBridgeFiles fbf = (FileBridgeFiles) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(FileBridgeFiles.class, fileBridgeFilesUid);
				
				String strResultParserKey = "<parserKey>";
				String parserKey = fbf.getParserKey();
				String hql = null;
				strResultParserKey += parserKey;
				strResultParserKey += "</parserKey>";
				
				String strResultStatus = "<status>";
				String status = fbf.getStatus();
				hql = "FROM CommonLookup WHERE lookupTypeSelection = 'FileBridge.status' AND shortCode =:shortCode AND (isDeleted IS NULL OR isDeleted = FALSE) AND (isActive IS NULL OR isActive = TRUE)";
				CommonLookup lookup = (CommonLookup) ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "shortCode", status).get(0);
				strResultStatus += lookup.getLookupLabel() + "</status>";
				
				String strResultRemark = "<remark>";
				strResultRemark += fbf.getRemark() + "</remark>";
				
				String strResult = "<statusCheck>" + strResultParserKey + strResultStatus + strResultRemark + "</statusCheck>";
				response.getWriter().write(strResult);
			}	
		}
	}
	
}
