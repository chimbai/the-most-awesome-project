package com.idsdatanet.d2.drillnet.report.schedule.corportatedrillingportal;

import java.io.File;
import java.io.FileOutputStream;

import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ext.LexicalHandler;

import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;

public class ReportXml {
	
	// DTD & XML Constants
	private String REPORTS_DTD_TAG="reports";
	private String REPORT_ELEMENT_TAG="report";
	private String WELL_ELEMENT_TAG="well";
	private String COUNTRY_ELEMENT_TAG="country";
	private String RIG_ELEMENT_TAG="rig";
	private String DATE_ELEMENT_TAG="date";
	private String UWI_ELEMENT_TAG="uwi";
	private String FILENAME_ATTRIBUTE_TAG="filename";
	
	
	private String country;
	private String rigname;
	private String wellname;
	private String filename;
	private String date;
	private String uwi;
	
		
	public ReportXml(String country, String rigname, String wellname,
			String filename, String date, String uwi) {
		this.country = country;
		this.rigname = rigname;
		this.wellname = wellname;
		this.filename = filename;
		this.date = date;
		this.uwi = uwi;
}




	public String getCountry() {
		return country;
	}




	public void setCountry(String country) {
		this.country = country;
	}




	public String getRigname() {
		return rigname;
	}




	public void setRigname(String rigname) {
		this.rigname = rigname;
	}




	public String getWellname() {
		return wellname;
	}




	public void setWellname(String wellname) {
		this.wellname = wellname;
	}




	public String getFilename() {
		return filename;
	}




	public void setFilename(String filename) {
		this.filename = filename;
	}




	public String getDate() {
		return date;
	}




	public void setDate(String date) {
		this.date = date;
	}




	public String getUwi() {
		return uwi;
	}




	public void setUwi(String uwi) {
		this.uwi = uwi;
	}
	
	
	
	
	public File writeToXML(File file) throws Exception
	{
		FileOutputStream fos=new FileOutputStream(file);
		SimpleXmlWriter writer=new SimpleXmlWriter(fos);
		
		LexicalHandler lexicalHandler = (LexicalHandler) writer.getHandler();
		DeclHandler declHandler = (DeclHandler) writer.getHandler();
		
		lexicalHandler.startDTD(REPORTS_DTD_TAG, null, null);
		declHandler.elementDecl(REPORTS_DTD_TAG, "("+REPORT_ELEMENT_TAG+"+)");
		declHandler.elementDecl(REPORT_ELEMENT_TAG, "("+COUNTRY_ELEMENT_TAG+" , "+RIG_ELEMENT_TAG+" , "+WELL_ELEMENT_TAG+" , "+DATE_ELEMENT_TAG+" , "+UWI_ELEMENT_TAG+")");
		declHandler.attributeDecl(REPORT_ELEMENT_TAG, FILENAME_ATTRIBUTE_TAG, "CDATA", "#REQUIRED", null);
		declHandler.elementDecl(COUNTRY_ELEMENT_TAG, "(#PCDATA)");
		declHandler.elementDecl(RIG_ELEMENT_TAG, "(#PCDATA)");
		declHandler.elementDecl(WELL_ELEMENT_TAG, "(#PCDATA)");
		declHandler.elementDecl(DATE_ELEMENT_TAG, "(#PCDATA)");
		declHandler.elementDecl(UWI_ELEMENT_TAG, "(#PCDATA)");
		lexicalHandler.endDTD();
		
		writer.startElement(REPORTS_DTD_TAG);
		SimpleAttributes att=new SimpleAttributes();
		att.addAttribute(FILENAME_ATTRIBUTE_TAG, filename);
		writer.startElement(REPORT_ELEMENT_TAG, att);
			writer.addElement(COUNTRY_ELEMENT_TAG, country);
			writer.addElement(RIG_ELEMENT_TAG, rigname);
			writer.addElement(WELL_ELEMENT_TAG, wellname);
			writer.addElement(DATE_ELEMENT_TAG, date);
			writer.addElement(UWI_ELEMENT_TAG, uwi);
		writer.endElement();
		writer.endElement();
		
		writer.close();
		fos.close();
		return file;
	}
}
