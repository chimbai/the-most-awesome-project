package com.idsdatanet.d2.drillnet.activity;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;
/**
 * Example of implementation of DataNodeAllowedAction class
 * @author RYAU
 *
 */
public class ActivityDataNodeAllowedAction implements DataNodeAllowedAction {
	/**
	 * This method is to let you define your own logic of whether the current action can be displayed or not, according
	 * to your own business logic. You can access to database using <code>ApplicationUtils.java</code>.
	 * @param node The tree node that holds the current row of record.
	 * @param session Current <code>UserSession</code> object in the session, for you to retrieve current Welloperation, day or groupid.
	 * @param action Example : "add", "edit", "delete"...
	 */
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		if (StringUtils.equals(action, Action.ADD) || 
				StringUtils.equals(action, Action.COPY_SELECTED) || 
				StringUtils.equals(action, Action.IMPORT_FROM_CUSTOM_TEMPLATE) || 
				StringUtils.equals(action, Action.COPY_PASTE_FROM_YESTERDAY) || 
				StringUtils.equals(action, Action.COPY_PASTE_LAST_FROM_YESTERDAY) ||  
				StringUtils.equals(action, Action.PASTE_AS_NEW)) {
			if (session != null) {
				List<ReportDaily> drillnetReportDailyList = ActivityUtils.getDrillnetReportDaily(session.getCurrentDaily().getDailyUid());
				if (drillnetReportDailyList.isEmpty()) {
					return false;
				}
			}
		}
		return true;
	}
}
