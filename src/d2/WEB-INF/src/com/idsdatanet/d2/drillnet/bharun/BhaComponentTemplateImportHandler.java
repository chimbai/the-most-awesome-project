package com.idsdatanet.d2.drillnet.bharun;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.idsdatanet.d2.core.lookup.DbLookupDefinition;
import com.idsdatanet.d2.core.lookup.DbLookupProvider;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.templateimport.DefaultTemplateImportHandler;
import com.idsdatanet.d2.core.web.mvc.templateimport.EmptySlot;

public class BhaComponentTemplateImportHandler extends DefaultTemplateImportHandler{
	private Map<String, Object> _lookup = null;
	private Map<String, Map> cachedLookup = new HashMap<String, Map>();
	
	public void setLookup(Object lookup) {
		if (lookup instanceof Map) {
			this._lookup = (Map<String,Object>)lookup;
		}
	}
	
	public Object getLookup() {
		return this._lookup;
	}

	@Override
	protected void setupAdditionalBindingValues(int line,
			List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode) {
		
		if (this._lookup==null) return;
		if (bindingValue != null) {
			if (bindingValue.get(0) instanceof EmptySlot) {
				
			} else {
				try {
					
					for (Map.Entry<String, Object> entry : this._lookup.entrySet()) {
						String fieldName = entry.getKey();
						Object lookup = entry.getValue();
						if (lookup==null) continue;
						fieldName = fieldName.replaceAll("BhaComponent", "data");
						
						Map<String, LookupItem> lookup_map = null;

						if (this.cachedLookup.containsKey(fieldName)) {
							lookup_map = this.cachedLookup.get(fieldName);
						} else {
							if(lookup instanceof DbLookupDefinition)
							{
								lookup = (String) ((DbLookupDefinition) lookup).getObject();			
							}
							lookup_map = LookupManager.getConfiguredInstance().getLookup(lookup.toString(), new UserSelectionSnapshot(commandBean.getCurrentUserSession()), null);
							this.cachedLookup.put(fieldName, lookup_map);
						}
						
						if (lookup_map!=null) {
							for (int i=0; i<bindingProperties.size(); i++) {
								if (fieldName.equals(bindingProperties.get(i))) {
									Object fieldValue = bindingValue.get(i);
									if (fieldValue==null) break;
									if (!lookup_map.containsKey(fieldValue.toString())) {
										String value = fieldValue.toString().toUpperCase();
										for (Map.Entry<String, LookupItem> lookupEntry : lookup_map.entrySet()) {
											LookupItem item = lookupEntry.getValue();
											if (item.getValue()==null) continue;
											String lookupValue = item.getValue().toString().toUpperCase();
											if (value.equals(lookupValue)) {
												bindingValue.set(i, item.getKey());
											}
										}
									}
									break;	
								}
							}
							
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		
	}
	
	
	
}
