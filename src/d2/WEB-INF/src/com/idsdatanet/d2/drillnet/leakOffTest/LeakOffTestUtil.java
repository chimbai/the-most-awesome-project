package com.idsdatanet.d2.drillnet.leakOffTest;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.calculation.drillnet.LeakOffTestCalculation;
import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * All common utility related to Leak Off Test.
 * @author Moses
 *
 */

public class LeakOffTestUtil {
	
	/**
	 * Common Method for EMW Calculation
	 * @param commandBean
	 * @param object
	 * @throws Exception Standard Error Throwing Exception
	 * @return No Return
	 */
	public static void calculateEMW(CommandBean commandBean, Object object) throws Exception{
		
		if (object instanceof LeakOffTest) {
			LeakOffTest leakofftest = (LeakOffTest)object;
			Double emw = 0.00;
			Double leakOffPressure = 0.00;
			Double maxPressure = 0.00;
			Double pressure = 0.00;
			Double depth = 0.00;
			Double mudDensity = 0.00;
			String testType = null;
			
			if (leakofftest.getTestType() != null) testType = leakofftest.getTestType();
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			
			//Get Mud Density Value
			if(leakofftest.getMudDensity() != null) {
				mudDensity = CommonUtil.getConfiguredInstance().getBaseValue(thisConverter, LeakOffTest.class, "mudDensity", leakofftest.getMudDensity());			
				//convert to ppg for calculation, IDS base is Kilograms Per Cubic Metre						
				mudDensity = mudDensity / 119.8264;
			} else {
				mudDensity = 0.00;
			}
			
			if ("FIT".equalsIgnoreCase(testType) || "LOT".equalsIgnoreCase(testType))
			{
				String autoPopulateShoeDepth = GroupWidePreference.getValue(leakofftest.getGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_SHOE_DEPTH);
				
				if(autoPopulateShoeDepth.equalsIgnoreCase("1") && leakofftest.getShoeTvdMsl()!=null){
					Double shoeDepth = CommonUtil.getConfiguredInstance().getBaseValue(thisConverter, LeakOffTest.class, "shoeTvdMsl", leakofftest.getShoeTvdMsl());
					thisConverter.setReferenceMappingField(LeakOffTest.class, "testDepthTvdMsl");
					thisConverter.setBaseValue(shoeDepth);
					leakofftest.setTestDepthTvdMsl(thisConverter.getConvertedValue());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(leakofftest);
				}
				
				if (leakofftest.getTestDepthTvdMsl()!=null)
					depth = CommonUtil.getConfiguredInstance().getBaseValue(thisConverter, LeakOffTest.class, "testDepthTvdMsl", leakofftest.getTestDepthTvdMsl());
			}else{
				if (leakofftest.getTestDepthTvdMsl()!=null)
					depth = CommonUtil.getConfiguredInstance().getBaseValue(thisConverter, LeakOffTest.class, "testDepthTvdMsl", leakofftest.getTestDepthTvdMsl());
				else
				{
					if (leakofftest.getShoeTvdMsl()!=null)
						depth = CommonUtil.getConfiguredInstance().getBaseValue(thisConverter, LeakOffTest.class, "shoeTvdMsl", leakofftest.getShoeTvdMsl());
				}
			}
			depth = depth * 3.280839895013123;

			//If TestType = FIT, get maxPressure
			if ("FIT".equalsIgnoreCase(testType)) {
				
				if(leakofftest.getMaxPressure() != null)
				{
					maxPressure = CommonUtil.getConfiguredInstance().getBaseValue(thisConverter, LeakOffTest.class, "maxPressure", leakofftest.getMaxPressure());
					
					//convert to psi for calculation, IDS base is Pascal
					pressure = maxPressure / 6894.757;
				}
			} else {
			//Otherwise (LOT or null), get LeakOffPressure	
				if(leakofftest.getLeakOffPressure() != null)
				{
					leakOffPressure = CommonUtil.getConfiguredInstance().getBaseValue(thisConverter, LeakOffTest.class, "leakOffPressure", leakofftest.getLeakOffPressure());
					
					//convert to psi for calculation, IDS base is Pascal
					pressure = leakOffPressure / 6894.757;
				}
			}
			emw = LeakOffTestCalculation.calculateEMW(pressure, mudDensity, depth);
						
			thisConverter.setReferenceMappingField(LeakOffTest.class, "estimatedMudWeight");
			thisConverter.setBaseValue(emw);
			leakofftest.setEstimatedMudWeight(thisConverter.getConvertedValue());
			
			String strSqlUpdate = "UPDATE LeakOffTest SET estimatedMudWeight =:estimatedMudWeight WHERE leakOffTestUid =:leakOffTestUid";	
			String[] paramNames = {"estimatedMudWeight", "leakOffTestUid"};
			Object[] paramValues = {thisConverter.getConvertedValue(), leakofftest.getLeakOffTestUid()};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
		}
	}
	
	/**
	 * Method Use to populate Last LOT / FIT in Daily Screen
	 * @param commandBean
	 * @param object
	 * @param userSelection
	 * @param testType
	 * @param fieldToPopulate
	 * @param casingSize
	 * @throws Exception
	 * @return No Return 
	 */
	public static void populateLOTFIT(CommandBean commandBean, Object object, UserSelectionSnapshot userSelection,String testType,String fieldToPopulate, Double casingSize) throws Exception
	{
		Object obj = CommonUtil.getConfiguredInstance().lastFITLOT(userSelection.getOperationUid(), userSelection.getDailyUid(), testType, casingSize);
		if (obj ==null)
		{
			PropertyUtils.setProperty(object, fieldToPopulate, null);
		}
		if (obj instanceof LeakOffTest)
		{
			LeakOffTest leakofftest = (LeakOffTest) obj;
			LeakOffTestUtil.calculateEMW(commandBean, obj);
			CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, LeakOffTest.class, "estimatedMudWeight");
			thisConverter2.setBaseValueFromUserValue(leakofftest.getEstimatedMudWeight());
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, fieldToPopulate);
			thisConverter.setBaseValue(thisConverter2.getBasevalue());
			PropertyUtils.setProperty(object, fieldToPopulate, thisConverter.getConvertedValue());
		}
	}
	
	/**
	 * Method used to populate Test/Hole Depth in Daily Screen
	 * @param node
	 * @param userSelection
	 * @param casingSize
	 * @throws Exception
	 */
	public static void populateLotFitData(CustomFieldUom thisConverter, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Double casingSize) throws Exception
	{
		Date todayDate = null;
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		if (daily != null) 
		{
			todayDate = daily.getDayDate();
		}
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);
		//this is for getting the previous day latest test type value based on the time that has being selected in leak off test screen
		String strSql1 = "select lot.testType FROM LeakOffTest lot, Daily d WHERE (lot.isDeleted = false or lot.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and lot.operationUid=:currentOperationUid and d.dailyUid=lot.dailyUid and d.dayDate<=:dayDate order by d.dayDate desc, lot.testDateTime desc, lot.testDepthTvdMsl desc";
		String[] paramsFields1 = {"currentOperationUid","dayDate"};
		Object[] paramsValues1 = new Object[2]; paramsValues1[0] = userSelection.getOperationUid();  paramsValues1[1] = todayDate;
		List lstLatestLOT = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
		String testType="";
		if(lstLatestLOT.size()>0)
		{
			if(lstLatestLOT.get(0)!=null)
			{
				testType = lstLatestLOT.get(0).toString();
			}
		}	
	  Object obj = CommonUtil.getConfiguredInstance().lastFITLOT(userSelection.getOperationUid(), userSelection.getDailyUid(), testType, casingSize);
	  if (obj == null)
	  {
		  node.getDynaAttr().put("lastTestDepthTvdMsl", "");
		  node.getDynaAttr().put("lastHoleDepthMdMsl", "");
		  node.getDynaAttr().put("lastHoleDepthTvdMsl", "");
		  node.getDynaAttr().put("testType", "");
		  node.getDynaAttr().put("estimatedMudWeight", "");
	  }
	  if (obj instanceof LeakOffTest)
	  {
		  LeakOffTest leakofftest = (LeakOffTest) obj;
		  //get base value of TestDepthTvdMsl and set to lastTestDepthTvdMsl that declared in fieldToPopulate
		  if (leakofftest.getTestDepthTvdMsl() != null)
		  {
			  thisConverter.setReferenceMappingField(LeakOffTest.class, "testDepthTvdMsl");
			  thisConverter.setBaseValueFromUserValue(leakofftest.getTestDepthTvdMsl(), false);
			  if (thisConverter.isUOMMappingAvailable()){
				  node.setCustomUOM("@lastTestDepthTvdMsl", thisConverter.getUOMMapping());
			  }
			  node.getDynaAttr().put("lastTestDepthTvdMsl", thisConverter.getConvertedValue());
		  }
		  if (leakofftest.getHoleDepthMdMsl() != null)
		  {
			  thisConverter.setReferenceMappingField(LeakOffTest.class, "holeDepthMdMsl");
			  thisConverter.setBaseValueFromUserValue(leakofftest.getHoleDepthMdMsl(), false);
			  if (thisConverter.isUOMMappingAvailable()){
				  node.setCustomUOM("@lastHoleDepthMdMsl", thisConverter.getUOMMapping());
			  }
			  node.getDynaAttr().put("lastHoleDepthMdMsl", thisConverter.getConvertedValue());
		  }
		  if (leakofftest.getHoleDepthTvdMsl() != null)
		  {
			  thisConverter.setReferenceMappingField(LeakOffTest.class, "holeDepthTvdMsl");
			  thisConverter.setBaseValueFromUserValue(leakofftest.getHoleDepthTvdMsl(), false);
			  if (thisConverter.isUOMMappingAvailable()){
				  node.setCustomUOM("@lastHoleDepthTvdMsl", thisConverter.getUOMMapping());
			  }
			  node.getDynaAttr().put("lastHoleDepthTvdMsl", thisConverter.getConvertedValue());
		  }
		  if (leakofftest.getEstimatedMudWeight() != null)
		  {
			  thisConverter.setReferenceMappingField(LeakOffTest.class, "estimatedMudWeight");
			  thisConverter.setBaseValueFromUserValue(leakofftest.getEstimatedMudWeight(), false);
			  if (thisConverter.isUOMMappingAvailable()){
				  node.setCustomUOM("@lastEstimatedMudWeight", thisConverter.getUOMMapping());
			  }
			  node.getDynaAttr().put("lastEstimatedMudWeight", thisConverter.getConvertedValue());
		  }
		  if (testType != null)
		  {
			  node.getDynaAttr().put("testType", testType);
		  }
	  	}
	}
	
	/**
	 * Method Use to populate Dynamic Attribute value for Test Type field so that it can be used in Daily Screen for filtering either to show Last LOT field or Last FIT field in it
	 * @param node
	 * @param operationUid
	 * @param currentDailyUid
	 * @throws Exception
	 * @return No Return 
	 */
	public static void populateTestTypeDynaAttr(CommandBeanTreeNode node,String operationUid, String currentDailyUid) throws Exception
	{
		Date todayDate = null;
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (daily != null) 
		{
			todayDate = daily.getDayDate();
		}
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);
		//this is for getting the previous day latest test type value based on the time that has being selected in leak off test screen
		String strSql1 = "select lot.testType FROM LeakOffTest lot, Daily d WHERE (lot.isDeleted = false or lot.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and lot.operationUid=:currentOperationUid and d.dailyUid=lot.dailyUid and d.dayDate<=:dayDate order by d.dayDate desc, lot.testDateTime desc, lot.testDepthTvdMsl desc";
		String[] paramsFields1 = {"currentOperationUid","dayDate"};
		Object[] paramsValues1 = new Object[2]; paramsValues1[0] = operationUid;  paramsValues1[1] = todayDate;
		List lstLatestLOT = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
		String testType="";
		
		if(lstLatestLOT.size()>0)
		{
			if(lstLatestLOT.get(0)!=null)
			{
				testType = lstLatestLOT.get(0).toString();
				node.getDynaAttr().put("testType", testType);
			}
		}
		
	}
	/**
	 * Method Use to populate latest Mud Weight value to be show in Daily Screen
	 * @param node
	 * @param operationUid
	 * @param currentDailyUid
	 * @throws Exception
	 * @return No Return 
	 */
	public static void populateLatestMudWeight(CommandBean commandBean, Object object, CommandBeanTreeNode node,String operationUid, String currentDailyUid) throws Exception
	{
		Date todayDate = null;
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (daily != null) 
		{
			todayDate = daily.getDayDate();
		}
		QueryProperties qp = new QueryProperties();		
		qp.setUomConversionEnabled(false);
		Double mudWeight=null;
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "lastMw");
		
		//this is for getting the previous day latest mud weight value based on the time that has being selected in leak off test screen
		String strSql1 = "select lot.mudDensity FROM LeakOffTest lot, Daily d WHERE (lot.isDeleted = false or lot.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and lot.operationUid=:currentOperationUid and d.dailyUid=lot.dailyUid and d.dayDate<=:dayDate order by d.dayDate desc, lot.testDateTime desc, lot.testDepthTvdMsl desc";
		String[] paramsFields1 = {"currentOperationUid","dayDate"};
		Object[] paramsValues1 = new Object[2]; paramsValues1[0] = operationUid; paramsValues1[1] = todayDate;
		List lstLatestLOT = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
		

		if(lstLatestLOT.size()>0)
		{
			if(lstLatestLOT.get(0)!=null)
			{
				mudWeight = Double.parseDouble(lstLatestLOT.get(0).toString());
			}
			
		}
		
		if(thisConverter.isUOMMappingAvailable())
		{
			if(mudWeight!=null)
			{
				thisConverter.setBaseValue(mudWeight);
				mudWeight = thisConverter.getConvertedValue();
			}
			
		}
		
		PropertyUtils.setProperty(object, "lastMw", mudWeight);
	}
	
	/**
	 * Method to create or update LeakOffTest from Activity based on pre-defined free text remarks
	 * @param thisActivity
	 * @param fieldsMap
	 * @throws Exception
	 */
	public static void createLOTBasedOnActivityRemarks(UserSelectionSnapshot userSelection,Activity thisActivity, Map<String,Object>fieldsMap) throws Exception{
		if (fieldsMap !=null){
			String[] paramsFields = {"operationUid","dailyUid","activityUid"};
			Object[] paramsValues = {thisActivity.getOperationUid(),thisActivity.getDailyUid(),thisActivity.getActivityUid()};
			String strSql = "FROM LeakOffTest WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND dailyUid=:dailyUid AND activityUid=:activityUid";
			List<LeakOffTest> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			LeakOffTest leakOffTest = null;
			if(ls.size()==0){
				leakOffTest = new LeakOffTest();
				leakOffTest.setActivityUid(thisActivity.getActivityUid());
				leakOffTest.setDailyUid(thisActivity.getDailyUid());
			}else{
				leakOffTest = ls.get(0);
			}
			 
			if (leakOffTest!=null ){
				for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){
					leakOffTest.setTestType(thisActivity.getTaskCode());
					if (StringUtils.isNotBlank(fieldEntry.getKey())) PropertyUtils.setProperty(leakOffTest, fieldEntry.getKey(), fieldEntry.getValue());
				}
				
				Boolean isSubmit = leakOffTest.getIsSubmit();
				//if(isSubmit!=null && isSubmit) RevisionUtils.unSubmitRecordWithStaticRevisionLog(userSelection, leakOffTest, "Auto update from time codes");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(leakOffTest);
			}
		}		
	}
}
