package com.idsdatanet.d2.drillnet.wellexplorer;

/**
 * A constant class for well explorer screen. These variables are being used in
 * WellExplorerUtils, WellDataNodeListener, WellboreDataNodeListener, 
 * OperationDataNodeListener and ReportDailyDataNodeListener for easier maintenance.
 * @author RYAU
 *
 */
public interface WellExplorerConstant {
	public static final String TYPE_WELL = "well";
	public static final String TYPE_WELLBORE = "wellbore";
	public static final String TYPE_OPERATION = "operation";
	public static final String WELL_UID = "well_explorer_welluid";
	public static final String WELLBORE_UID = "well_explorer_wellboreuid";
	public static final String OPERATION_UID = "well_explorer_operationuid";
}