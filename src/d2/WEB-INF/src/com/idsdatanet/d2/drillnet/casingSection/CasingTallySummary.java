package com.idsdatanet.d2.drillnet.casingSection;


public class CasingTallySummary implements java.io.Serializable {

	private String type;
	private String casingSectionUid;
	private Double casingOd;
	private Double casingId;
	private String grade;
	private Double weight;
	private Double sectionTopMdMsl;
	private Double sectionBottomMdMsl;
	private Double sectionTopTvdMsl;
	private Double sectionBottomTvdMsl;
	private Integer numJoints;
	private String jointNumberText;
	private Double csgLength;
	private Double sectionNum;
	private String csgComment;
	private String groupKey;
	private String casingTallyUids;
	private Double nominalOd;
	private Double connectionSizeTop;
	private String threadType;
	private Double connectionSizeBottom;
	private String threadTypeBottom;
	private Double csgDrift;
	private Double topDepthCorrected;
	private String make;
	private Double csgConntorque;
	private Double csgTensileStrength;
	private Double csgCollapsePressure;
	private Double burstPressure;
	private Double cumLength;
	private Double cumWeightForce;
	private Double weightForce;
	private Double volumeDisplaced;
	private Double cumVolumeDisplaced;
	private Double makeUpTorqMin;
	private Double makeUpTorqOpt;
	private Double makeUpTorqMax;
	private Double csgNomid;
	private Double driftid;
	private Double wallThickness;
	private Double overallRunningLength;
	private Double avgcsgLength;
	private Double avgVolumeDisplaced;
	private Double avgholeVolume; 
	
	public CasingTallySummary() {
	}
	
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getCasingSectionUid() {
		return this.casingSectionUid;
	}

	public void setCasingSectionUid(String casingSectionUid) {
		this.casingSectionUid = casingSectionUid;
	}

	public Double getCasingOd() {
		return this.casingOd;
	}

	public void setCasingOd(Double casingOd) {
		this.casingOd = casingOd;
	}

	public Double getCasingId() {
		return this.casingId;
	}

	public void setCasingId(Double casingId) {
		this.casingId = casingId;
	}

	public String getGrade() {
		return this.grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Double getWeight() {
		return this.weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getSectionTopMdMsl() {
		return this.sectionTopMdMsl;
	}

	public void setSectionTopMdMsl(Double sectionTopMdMsl) {
		this.sectionTopMdMsl = sectionTopMdMsl;
	}

	public Double getSectionBottomMdMsl() {
		return this.sectionBottomMdMsl;
	}

	public void setSectionBottomMdMsl(Double sectionBottomMdMsl) {
		this.sectionBottomMdMsl = sectionBottomMdMsl;
	}

	public Double getSectionTopTvdMsl() {
		return this.sectionTopTvdMsl;
	}

	public void setSectionTopTvdMsl(Double sectionTopTvdMsl) {
		this.sectionTopTvdMsl = sectionTopTvdMsl;
	}

	public Double getSectionBottomTvdMsl() {
		return this.sectionBottomTvdMsl;
	}

	public void setSectionBottomTvdMsl(Double sectionBottomTvdMsl) {
		this.sectionBottomTvdMsl = sectionBottomTvdMsl;
	}

	public Integer getNumJoints() {
		return this.numJoints;
	}

	public void setNumJoints(Integer numJoints) {
		this.numJoints = numJoints;
	}
	
	public String getJointNumberText() {
		return this.jointNumberText;
	}

	public void setJointNumberText(String jointNumberText) {
		this.jointNumberText = jointNumberText;
	}

	public Double getCsgLength() {
		return this.csgLength;
	}

	public void setCsgLength(Double csgLength) {
		this.csgLength = csgLength;
	}

	public Double getSectionNum() {
		return this.sectionNum;
	}

	public void setSectionNum(Double sectionNum) {
		this.sectionNum = sectionNum;
	}

	public void setCsgComment(String csgComment) {
		this.csgComment = csgComment;
	}

	public String getCsgComment() {
		return csgComment;
	}

	public void setCasingTallyUids(String casingTallyUids) {
		this.casingTallyUids = casingTallyUids;
	}

	public String getCasingTallyUids() {
		return casingTallyUids;
	}

	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}

	public String getGroupKey() {
		return groupKey;
	}


	public void setConnectionSizeTop(Double connectionSizeTop) {
		this.connectionSizeTop = connectionSizeTop;
	}

	public Double getConnectionSizeTop() {
		return connectionSizeTop;
	}

	public void setNominalOd(Double nominalOd) {
		this.nominalOd = nominalOd;
	}

	public Double getNominalOd() {
		return nominalOd;
	}

	public void setThreadTypeBottom(String threadTypeBottom) {
		this.threadTypeBottom = threadTypeBottom;
	}

	public String getThreadTypeBottom() {
		return threadTypeBottom;
	}

	public void setConnectionSizeBottom(Double connectionSizeBottom) {
		this.connectionSizeBottom = connectionSizeBottom;
	}

	public Double getConnectionSizeBottom() {
		return connectionSizeBottom;
	}

	public void setThreadType(String threadType) {
		this.threadType = threadType;
	}

	public String getThreadType() {
		return threadType;
	}

	public void setCsgDrift(Double csgDrift) {
		this.csgDrift = csgDrift;
	}

	public Double getCsgDrift() {
		return csgDrift;
	}

	public void setTopDepthCorrected(Double topDepthCorrected) {
		this.topDepthCorrected = topDepthCorrected;
	}

	public Double getTopDepthCorrected() {
		return topDepthCorrected;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getMake() {
		return make;
	}

	public void setCsgConntorque(Double csgConntorque) {
		this.csgConntorque = csgConntorque;
	}

	public Double getCsgConntorque() {
		return csgConntorque;
	}

	public void setCsgTensileStrength(Double csgTensileStrength) {
		this.csgTensileStrength = csgTensileStrength;
	}

	public Double getCsgTensileStrength() {
		return csgTensileStrength;
	}

	public void setCsgCollapsePressure(Double csgCollapsePressure) {
		this.csgCollapsePressure = csgCollapsePressure;
	}

	public Double getCsgCollapsePressure() {
		return csgCollapsePressure;
	}

	public void setBurstPressure(Double burstPressure) {
		this.burstPressure = burstPressure;
	}

	public Double getBurstPressure() {
		return burstPressure;
	}

	public void setCumLength(Double cumLength) {
		this.cumLength = cumLength;
	}

	public Double getCumLength() {
		return cumLength;
	}

	public void setCumWeightForce(Double cumWeightForce) {
		this.cumWeightForce = cumWeightForce;
	}

	public Double getCumWeightForce() {
		return cumWeightForce;
	}

	public void setWeightForce(Double weightForce) {
		this.weightForce = weightForce;
	}

	public Double getWeightForce() {
		return weightForce;
	}

	public void setVolumeDisplaced(Double volumeDisplaced) {
		this.volumeDisplaced = volumeDisplaced;
	}

	public Double getVolumeDisplaced() {
		return volumeDisplaced;
	}

	public void setCumVolumeDisplaced(Double cumVolumeDisplaced) {
		this.cumVolumeDisplaced = cumVolumeDisplaced;
	}

	public Double getCumVolumeDisplaced() {
		return cumVolumeDisplaced;
	}
	
	public void setMakeUpTorqMin(Double makeUpTorqMin) {
		this.makeUpTorqMin = makeUpTorqMin;
	}

	public Double getMakeUpTorqMin() {
		return makeUpTorqMin;
	}
	
	public void setMakeUpTorqOpt(Double makeUpTorqOpt) {
		this.makeUpTorqOpt = makeUpTorqOpt;
	}

	public Double getMakeUpTorqOpt() {
		return makeUpTorqOpt;
	}
	
	public void setMakeUpTorqMax(Double makeUpTorqMax) {
		this.makeUpTorqMax = makeUpTorqMax;
	}

	public Double getMakeUpTorqMax() {
		return makeUpTorqMax;
	}
	
	public void setCsgNomid(Double csgNomid) {
		this.csgNomid = csgNomid;
	}

	public Double getCsgNomid() {
		return csgNomid;
	}
	
	public void setDriftid(Double driftid) {
		this.driftid = driftid;
	}

	public Double getDriftid() {
		return driftid;
	}
	
	public void setWallThickness(Double wallThickness) {
		this.wallThickness = wallThickness;
	}

	public Double getWallThickness() {
		return wallThickness;
	}
	
	public void setOverallRunningLength(Double overallRunningLength) {
		this.overallRunningLength = overallRunningLength;
	}

	public Double getOverallRunningLength() {
		return overallRunningLength;
	}
	
	public Double getAvgcsgLength() {
		return this.avgcsgLength;
	}

	public void setAvgcsgLength(Double avgcsgLength) {
		this.avgcsgLength = avgcsgLength;
	}
	
	public Double getAvgVolumeDisplaced() {
		return avgVolumeDisplaced;
	}

	public void setAvgVolumeDisplaced(Double avgVolumeDisplaced) {
		this.avgVolumeDisplaced = avgVolumeDisplaced;
	}
	
	public Double getAvgholeVolume() {
		return avgholeVolume;
	}

	public void setAvgholeVolume(Double avgholeVolume) {
		this.avgholeVolume = avgholeVolume;
	} 
}
