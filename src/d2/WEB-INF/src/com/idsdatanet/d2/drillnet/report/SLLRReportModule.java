package com.idsdatanet.d2.drillnet.report;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.JobServer;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.report.jasper.JsonJasperJob;
import com.idsdatanet.d2.core.report.jasper.JsonStringDataSorter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class SLLRReportModule extends DefaultReportModule {
	protected long reportJobTimeoutMilliSeconds = 300000;
	private int numberOfReportToDisplay = 20;
	private String reportFileName = "";
	private String reportDateFormat = "ddMMyyyy";
	private String reportTimeFormat = "HHmmss";
	private JsonStringDataSorter sorter = null;
	
	
	
	public void setSorter(JsonStringDataSorter sorter) {
		this.sorter = sorter;
	}

	public void setReportJobTimeoutMilliSeconds(long value){
		this.reportJobTimeoutMilliSeconds = value;
	}
	
	public int getNumberOfReportToDisplay() {
		return numberOfReportToDisplay;
	}

	public void setNumberOfReportToDisplay(int numberOfReportToDisplay) {
		this.numberOfReportToDisplay = numberOfReportToDisplay;
	}

	public void setReportDateFormat(String reportDateFormat) {
		this.reportDateFormat = reportDateFormat;
	}

	public String getReportDateFormat() {
		return reportDateFormat;
	}

	public void setReportTimeFormat(String reportTimeFormat) {
		this.reportTimeFormat = reportTimeFormat;
	}

	public String getReportTimeFormat() {
		return reportTimeFormat;
	}
	
	synchronized public SLLRReportJobParams submitReportJob(UserSession session, HttpServletRequest request) throws Exception {
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
		ReportJobParams params = new ReportJobParams();
		if(params.isUserSessionReportJob() && this.isUserSessionReportJobRunning()) throw new Exception("Current job is still running");
		
		return this._submitReportJob(userSelection, params, request);
	}
	
	synchronized protected SLLRReportJobParams _submitReportJob(UserSelectionSnapshot userSelection, ReportJobParams reportJobData, HttpServletRequest request) throws Exception {
		UserContext userContext = UserContext.getUserContext(userSelection);
		if(userContext == null) throw new Exception("UserContext must not be null");
		
		reportJobData.resetForNewJob();
		
		reportJobData.setPaperSize(this.defaultPaperSize);
		reportJobData.setLocalOutputFile(this.getOutputFileInLocalPath(userContext, reportJobData.getPaperSize()));
		String outputFile = ReportUtils.getFullOutputFilePath(reportJobData.getLocalOutputFile());
		if(StringUtils.isBlank(outputFile)) throw new Exception("Output file must not be null");
		
		String jrxmlSource = this.getJrxmlFileInLocalPath(userContext, reportJobData);
		if(jrxmlSource != null){
			jrxmlSource = ReportUtils.getFullXslFilePath(jrxmlSource);
			if(StringUtils.isBlank(jrxmlSource)) throw new Exception("Jrxml file must not be null");
		}
		
		this.beforeReportJobStart(userSelection);
		String outputFormat = this.getOutputFileExtension();
		D2Job job = JsonJasperJob.createJasperReportJob(jrxmlSource, outputFile, outputFormat, userContext, request, this.sorter);

		JobContext currentJob = JobServer.getConfiguredInstance().createJob(job, userContext);
		currentJob.setTimeout(this.reportJobTimeoutMilliSeconds);
		currentJob.setJobGroup(REPORT_JOB_POOL_NAME);
		currentJob.setJobListener(this);
		this.beforeReportJobSubmission(currentJob);

		this.reportJobs.put(currentJob, reportJobData);
		if(reportJobData.isUserSessionReportJob()) this.userSessionReportJob = currentJob;
		
		ReportJobResult jobResult = reportJobData.addReportJobResult();
		jobResult.setReportFilePath(outputFile);
		
		SLLRReportJobParams currentJobData = new SLLRReportJobParams();
		currentJobData.setJobContext(currentJob);
		currentJobData.setReportJobParams(reportJobData);
		
		JobServer.getConfiguredInstance().submitJob(currentJob);
		
		return currentJobData;
	}
	
	@Override
	protected void beforeReportJobStart(UserSelectionSnapshot userSelection) throws Exception {}
	
	@Override
	protected void beforeReportJobSubmission(JobContext job) throws Exception {
		Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(job.getUserContext().getUserSelection().getWellUid());
		job.setJobDescription("Generate " + super.getReportType() + " Report for " + (well == null ? "" : well.getWellName()));
	}
	
	@Override
	public String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
        String well_uid = userContext.getUserSelection().getWellUid();
        this.reportFileName = getReportFileName();
        return super.getReportType() + (StringUtils.isBlank(well_uid) ? "" : "/" + well_uid) + "/" + this.reportFileName + "." + this.getOutputFileExtension();
    }
	
	public String getJrxmlFileInLocalPath(UserContext userContext, ReportJobParams reportJobParams) throws Exception{
		int well_op_type = ReportUtils.WELL_OPERATION_TYPE_DEFAULT;
		String jrxml = ReportUtils.getConfiguredInstance().getXsl(userContext.getUserSelection().getGroupUid(), super.getReportType(), well_op_type, reportJobParams.getPaperSize(), reportJobParams.getLanguage());
		if(StringUtils.isBlank(jrxml)) throw new Exception("Jrxml file not found! [report type: " + null2EmptyString(super.getReportType()) + ", group id: " + null2EmptyString(userContext.getUserSelection().getGroupUid()) + ", paper size: " + null2EmptyString(reportJobParams.getPaperSize()) + ", language: " + null2EmptyString(reportJobParams.getLanguage()) + "]");
		return jrxml;
	}
	
	public String formatDate(Date date, String format) {
		DateFormat df = new SimpleDateFormat(format);
		return df.format(date);
	}
	
	public String getReportFileName() {
		return ReportUtils.replaceInvalidCharactersInFileName(super.getReportType() + "_" + this.formatDate(new Date(), this.reportDateFormat) + "_" + this.formatDate(new Date(), this.reportTimeFormat));
	}
	
	@Override
	protected String getReportDisplayNameOnCreation(ReportFiles reportFile, ReportTypes reportType, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) throws Exception {
		return this.reportFileName;
	}
}
