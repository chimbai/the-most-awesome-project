package com.idsdatanet.d2.drillnet.operationinformationcenter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ProductionPumpParamCompLog;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.session.ParameterizedQueryFilter;
import com.idsdatanet.d2.drillnet.operationinformationcenter.WellOverview;
import com.idsdatanet.d2.pronet.productionPump.ProductionPumpUtils;

public class WellOverviewDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{

    // Default Display Order. Needs to be able to be customized
    private String orderBy = "o.sysOperationLastDatetime DESC, w.field ASC, w.wellName ASC";
    
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
    
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		Object object = node.getData();
		if(object instanceof WellOverview) {
			WellOverview thisWellOverview = (WellOverview) object;
			String dailyUid = ApplicationUtils.getConfiguredInstance().getLastDailyUidInOperation(thisWellOverview.getOperationUid());
			if (dailyUid!=null){
				thisWellOverview.setDailyUid(dailyUid);
				
				Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, thisWellOverview.getOperationUid());
				
				QueryProperties qp = new QueryProperties();
				
				//set the query property to use this operation uomtemplate and datum
				qp.setUomDatumUid(operation.getDefaultDatumUid());
				qp.setUomTemplateUid(operation.getUomTemplateUid());
				
				String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :selectedDailyUid";
				String[] paramsFields = {"selectedDailyUid"};
				String[] paramsValues = {dailyUid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				
				ReportDaily thisReportDaily =  (ReportDaily) lstResult.get(0);
				
				//assign unit to the value base on UOM 
				
				if(thisReportDaily != null){
					CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
					thisConverter.setReferenceMappingField(ReportDaily.class, "downholeWaterLevelMdMsl");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("downholeWaterLevelMdMsl", thisConverter.getUOMMapping());
					}
					thisWellOverview.setDownholeWaterLevelMdMsl(thisReportDaily.getDownholeWaterLevelMdMsl());
				
					thisConverter.setReferenceMappingField(ReportDaily.class, "annulusPressure");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("annulusPressure", thisConverter.getUOMMapping());
					}
					thisWellOverview.setAnnulusPressure(thisReportDaily.getAnnulusPressure());
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "waterProduction");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("waterProduction", thisConverter.getUOMMapping());
					}
					thisWellOverview.setWaterProduction(thisReportDaily.getWaterProduction());
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "gasProduction");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("gasProduction", thisConverter.getUOMMapping());
					}
					thisWellOverview.setGasProduction(thisReportDaily.getGasProduction());
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "downholeGaugeReading");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("downholeGaugeReading", thisConverter.getUOMMapping());
					}
					thisWellOverview.setDownholeGaugeReading(thisReportDaily.getDownholeGaugeReading());
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "pumpSubmergenceMdMsl");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("pumpSubmergenceMdMsl", thisConverter.getUOMMapping());
					}
					thisWellOverview.setpumpSubmergenceMdMsl(thisReportDaily.getPumpSubmergenceMdMsl());
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "volumetricEfficiency");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("volumetricEfficiency", thisConverter.getUOMMapping());
					}
					thisWellOverview.setVolumetricEfficiency(thisReportDaily.getVolumetricEfficiency());

					thisConverter.setReferenceMappingField(ReportDaily.class, "oilProductionVolume");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("oilProductionVolume", thisConverter.getUOMMapping());
					}
					thisWellOverview.setOilProductionVolume(thisReportDaily.getOilProductionVolume());
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "oilCut");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("oilCut", thisConverter.getUOMMapping());
					}
					thisWellOverview.setOilCut(thisReportDaily.getOilCut());
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "totalOilWaterProd");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("totalOilWaterProd", thisConverter.getUOMMapping());
					}
					thisWellOverview.setTotalOilWaterProd(thisReportDaily.getTotalOilWaterProd());
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "temperature");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("temperature", thisConverter.getUOMMapping());
					}
					thisWellOverview.setTemperature(thisReportDaily.getTemperature());
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "downholeFluidLevelMdMsl");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("downholeFluidLevelMdMsl", thisConverter.getUOMMapping());
					}
					thisWellOverview.setDownholeFluidLevelMdMsl(thisReportDaily.getDownholeFluidLevelMdMsl());
					
					thisConverter.setReferenceMappingField(ProductionPumpParamCompLog.class, "topDepthMdMsl");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("topDepthMdMsl", thisConverter.getUOMMapping());
					}
					thisWellOverview.setTopDepthMdMsl(ProductionPumpUtils.getPSNData(thisReportDaily));
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "waterInjectionVolume");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("waterInjectionVolume", thisConverter.getUOMMapping());
					}
					thisWellOverview.setWaterInjectionVolume(thisReportDaily.getWaterInjectionVolume());
					
					thisWellOverview.setDowntimeComment(thisReportDaily.getDowntimeComment());
					thisWellOverview.setGeneralComment(thisReportDaily.getGeneralComment());
					thisWellOverview.setOpsComment(thisReportDaily.getOpsComment());
					thisWellOverview.setDaydate(thisReportDaily.getReportDatetime());
					thisConverter.dispose();
				}
			}
		}
		if(object instanceof Operation) {
			Operation thisOperation = (Operation) object;
			String dailyUid = ApplicationUtils.getConfiguredInstance().getLastDailyUidInOperation(thisOperation.getOperationUid());
			if(StringUtils.isNotBlank(dailyUid))
			{
				ReportDaily thisReportDaily = ApplicationUtils.getConfiguredInstance().getReportDailyByOperationType(dailyUid, thisOperation.getOperationCode());
				if(thisReportDaily != null){
					CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
					
					node.getDynaAttr().put("downholeWaterLevelMdMsl", thisReportDaily.getDownholeWaterLevelMdMsl());
					thisConverter.setReferenceMappingField(ReportDaily.class, "annulusPressure");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@annulusPressure", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("annulusPressure", thisReportDaily.getAnnulusPressure());
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "waterProduction");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@waterProduction", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("waterProduction", thisReportDaily.getWaterProduction());
					
					thisConverter.setReferenceMappingField(ReportDaily.class, "gasProduction");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@gasProduction", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("gasProduction", thisReportDaily.getGasProduction());
					
					node.getDynaAttr().put("downtimeComment", thisReportDaily.getDowntimeComment());
					node.getDynaAttr().put("generalComment", thisReportDaily.getGeneralComment());
					node.getDynaAttr().put("daydate", thisReportDaily.getReportDatetime());
					thisConverter.dispose();
				}
			}
			Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, thisOperation.getWellUid());
			node.getDynaAttr().put("field", well.getField());
			node.getDynaAttr().put("previousWellName", well.getPreviousWellName());
			node.getDynaAttr().put("wellName", well.getWellName());
		}
	}
	
	public Daily getLastDailyInWell(String wellUid) throws Exception {
		QueryProperties queryProperties = new QueryProperties();
		queryProperties.setFetchFirstRowOnly();
		
		List<Daily> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Daily where (isDeleted = false or isDeleted is null) and wellUid = :wellUid order by dayDate desc", "wellUid", wellUid, queryProperties);
		if(list.size() > 0) return ((Daily) list.get(0));
		return null;
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if ("WellOverview".equals(meta.getTableClass().getSimpleName())) {
			List<Object> result = new ArrayList<Object>();
			String queryString = "From Well w, Operation o WHERE " +
						"(w.isDeleted = '0' OR w.isDeleted IS NULL) " +
						"AND (o.isDeleted = '0' OR o.isDeleted IS NULL) " +
						"AND (o.operationCode='PRD') " +
						"AND (w.wellUid = o.wellUid) and w.groupUid in (:groupUid) ";
				
			BaseCommandBean bc = (BaseCommandBean) commandBean;
			String query = "";
			if (bc.getAlphabeticalIndex().isEnabled()) {
				String indexField = bc.getAlphabeticalIndex().getAlphabeticalIndexField();
				String currentIndex = bc.getAlphabeticalIndex().getCurrentAlphabeticalIndex();
				if (StringUtils.isNotBlank(currentIndex)) {
					if (!"ALL".equals(currentIndex) && !"".equals(currentIndex)) {
						if ("#".equals(currentIndex)) {
							for (char alphabet = 'A'; alphabet <= 'Z' ; alphabet++) {
								if (StringUtils.isEmpty(query)) query += "AND ( (" + indexField + " not like '"+alphabet+"%')"; 
								else query += " AND (" + indexField + " not like '"+alphabet+"%')";
							}
							query += ")";
						} else {
							query =  "AND (w.field like '" + currentIndex + "%')"; 
						}
					}
				}
			}
			
			//queryString = queryString + query + " ORDER BY w.field, w.wellName, o.sysOperationLastDatetime DESC";
			// Add customizability to orderBy since there is no use case & users might have different requirements regarding this.
			queryString = queryString + query + " ORDER BY " + this.orderBy;
			
			String[] paramNames = new String[] {"groupUid"};
			Object[] paramValues = new Object[] { userSelection.getAccessibleGroupUids() };
			QueryProperties qp = new QueryProperties();

			List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, qp);
			List<String> welllist = new ArrayList();
			if (lstResult.size() >0){
				for (Object[] rec : lstResult) {
					Well well = (Well) rec[0];
					Operation operation = (Operation) rec[1];
					WellOverview wo = new WellOverview(well.getWellUid(), well.getWellName(), well.getPreviousWellName(), well.getField());
					wo.setOperationUid(operation.getOperationUid());
					if (!welllist.contains(well.getWellUid())){
						welllist.add(well.getWellUid());
						result.add(wo);
					}
				}
				return result;
			}
		}	
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		return true;
	}
	
	@SuppressWarnings("unused")
	private class OperationComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Object in1 = o1[1];
				Object in2 = o2[1];
				
				Date f1 = (Date) in1;
				Date f2 = (Date) in2;
				
				if (f1 == null || f2 == null) return 0;
				return f2.compareTo(f1);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	

}
