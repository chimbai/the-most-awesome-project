package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;
/**
 * Example of implementation of DataNodeAllowedAction class
 * @author RYAU
 *
 */
public class DvdPlanDetailDataNodeAllowedAction implements DataNodeAllowedAction {
	/**
	 * This method is to let you define your own logic of whether the current action can be displayed or not, according
	 * to your own business logic. You can access to database using <code>ApplicationUtils.java</code>.
	 * @param node The tree node that holds the current row of record.
	 * @param session Current <code>UserSession</code> object in the session, for you to retrieve current Welloperation, day or groupid.
	 * @param action Example : "add", "edit", "delete"...
	 */
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		if (StringUtils.equals(action, Action.ADD) || 
				StringUtils.equals(action, Action.COPY_SELECTED) || 
				StringUtils.equals(action, Action.IMPORT_FROM_CUSTOM_TEMPLATE) || 
				StringUtils.equals(action, Action.COPY_PASTE_FROM_YESTERDAY) || 
				StringUtils.equals(action, Action.PASTE_AS_NEW) ||
				StringUtils.equals(action, Action.DELETE_SELECTED) ||
				StringUtils.equals(action, Action.DELETE)) {
			if (session != null) {
				String[] paramsFields = {"operationUid"};
				Object[] paramsValues = new Object[1]; paramsValues[0] = session.getCurrentOperationUid();					
				String strSql = "SELECT operationPlanMasterUid from OperationPlanMaster WHERE operationUid =:operationUid AND (isDeleted = false or isDeleted is null) ORDER BY lastEditDatetime DESC";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, QueryProperties.create().setFetchFirstRowOnly());
					
				if (lstResult.size() > 0){
					Object thisResult = (Object) lstResult.get(0);
						
					if (thisResult != null){
						return true;
					}else {
						return false;
					}
				}else {
					return false;
				}
			}
		}
		return true;
	}
}
