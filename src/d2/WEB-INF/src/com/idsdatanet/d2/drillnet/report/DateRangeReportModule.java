package com.idsdatanet.d2.drillnet.report;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DateRangeReportModule extends DefaultReportModule{

	
	
	public static String CUSTOM_PROPERTY_START_DATE = "CP_START_DATE";
	public static String CUSTOM_PROPERTY_END_DATE = "CP_END_DATE";
	public static String CUSTOM_PROPERTY_DATE_STRING = "CP_DATE_STRING";
	
	
	public void init(CommandBean commandBean){
		super.init(commandBean);
		if(commandBean instanceof BaseCommandBean){
			
			try {
				//commandBean.getRoot().getDynaAttr().put("startDate", null);
				//commandBean.getRoot().getDynaAttr().put("endDate", null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	protected void setStartEndDateToCustomProperty(UserSelectionSnapshot userSelection, Date startDate, Date endDate) throws Exception
	{
		userSelection.setCustomProperty(CUSTOM_PROPERTY_START_DATE, startDate);
		userSelection.setCustomProperty(CUSTOM_PROPERTY_END_DATE,endDate);
		String dateString=ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(startDate);
		if (!startDate.equals(endDate))
			dateString+= " - "+ ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(endDate);
		userSelection.setCustomProperty(CUSTOM_PROPERTY_DATE_STRING,dateString);
		
	}
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		commandBean.getSystemMessage().clear();
		UserSelectionSnapshot userSelection=new UserSelectionSnapshot(session);
		
		Date startDate = null;
		Date endDate = null;
		
		Object obj=commandBean.getRoot().getDynaAttr().get("startDate");
		if (obj != null)
			startDate = CommonDateParser.parse(obj.toString());
		
		obj=commandBean.getRoot().getDynaAttr().get("endDate");
		if (obj!= null )
			endDate = CommonDateParser.parse(obj.toString());
		
		if (startDate !=null)
		{
			if (endDate==null || endDate.after(startDate) || endDate.equals(startDate))
			{
				endDate=endDate==null?startDate:endDate;
				
				this.setStartEndDateToCustomProperty(userSelection, startDate, endDate);
				return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));
	
				
			}else if (endDate.before(startDate))
			{
				commandBean.getSystemMessage().addError("End Date must be After Start Date");
			}
			
		}else{
			commandBean.getSystemMessage().addError("Please Enter Start Date");
		}
		return null;
	}
	
	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception{
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		String dateString = (String) userContext.getUserSelection().getCustomProperties().get(CUSTOM_PROPERTY_DATE_STRING);
		if(super.getOperationRequired() && operation == null) throw new Exception("Operation must be selected");
				
		return (operation == null ? "" : operation.getOperationName()) + " " +dateString +" "+ (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
	}	
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		String operation_uid = userContext.getUserSelection().getOperationUid();
		if(super.getOperationRequired() && StringUtils.isBlank(operation_uid)){
			throw new Exception("Operation must be selected");
		}	
		
		
		
		
		return this.getReportType(userContext.getUserSelection()) + 
			(StringUtils.isBlank(operation_uid) ? "" : "/" + operation_uid) +  
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize));
	}
}
