package com.idsdatanet.d2.drillnet.wellControlStack;

import java.util.Comparator;
import java.util.Date;

import com.idsdatanet.d2.core.model.WellControlStack;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class WellControlStackComparator implements Comparator<CommandBeanTreeNode> {
    public int compare(CommandBeanTreeNode o1, CommandBeanTreeNode o2) {
        try {
            WellControlStack obj1 = (WellControlStack) o1.getData();
            WellControlStack obj2 = (WellControlStack) o2.getData();
            
            if (obj1 != null && obj2 != null) {
                Date obj1R = obj1.getRemoveDatetime();
                Date obj2R = obj2.getRemoveDatetime();
                if (obj1R != null && obj2R != null) {
                    if (obj1R.getTime() > obj2R.getTime()) {
                        return 1; 
                    }
                    if (obj1R.getTime() < obj2R.getTime()) {
                        return -1;
                    }
                    return 0;
                } else {
                    if (obj1R == null && obj2R == null) {
                        return 0;
                    }
                    if (obj1R != null) {
                        return -1;
                    }
                    return 1;
                }
            } else {
                if (obj1 == null && obj2 == null) {
                    return 0;
                }
                if (obj1 != null) {
                    return -1;
                }
                return 1;
            }
        } catch (Exception exception) {
            return 0;
        }
    }
}