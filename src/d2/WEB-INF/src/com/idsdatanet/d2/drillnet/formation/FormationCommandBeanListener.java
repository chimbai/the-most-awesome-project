package com.idsdatanet.d2.drillnet.formation;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CasingTally;
import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class FormationCommandBeanListener extends EmptyCommandBeanListener  {
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN || commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT){
			commandBean.getRoot().getDynaAttr().put("datumLabel", commandBean.getInfo().getCurrentDatumDisplayName());
		}
		
		if (StringUtils.isNotBlank(userSelection.getWellUid())) {
			Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
			if (StringUtils.isBlank(well.getBasin())) {
				commandBean.getSystemMessage().addWarning("No Basin is selected in Well Data");
			}
		}
		
		if (StringUtils.isNotBlank(GroupWidePreference.getValue(userSelection.getGroupUid(),GroupWidePreference.GWP_FORMATION_SHOW_LAST)))
		{
			String maxOutput = GroupWidePreference.getValue(userSelection.getGroupUid(),GroupWidePreference.GWP_FORMATION_SHOW_LAST);
			commandBean.getRoot().getDynaAttr().put("maxOutput", maxOutput);
		}
		
		Double lastSampleTopMdMsl = null;
		Double lastSampleTopTvdMsl = null;
		Double currSampleTopMdMsl = null;
		Double currSampleTopTvdMsl = null;
		Double thickness = null;
		Double tvdThickness = null;
		Formation prevFormation = null;
		
		for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("Formation").values()) {
			Formation formation = (Formation) node.getData();
			if (formation!=null){
				currSampleTopMdMsl = formation.getSampleTopMdMsl();
				
				if (BooleanUtils.isTrue(formation.getIsfault())){
					boolean needUpdate = false;
					if (formation.getThickness()!=null) needUpdate = true;
					formation.setThickness(null);
					if (needUpdate) {
						String strSqlUpdate = "UPDATE Formation SET thickness=null WHERE formationUid =:formationUid";	
						String[] paramNames = {"formationUid"};
						Object[] paramValues = {formation.getFormationUid()};
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
					}
				//	ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(formation);
				}else{
					if (prevFormation==null){
						if (formation.getSampleTopMdMsl()!=null){
							prevFormation = formation;
						}else{
							boolean needUpdate = false;
							if (formation.getThickness()!=null) needUpdate = true;
							formation.setThickness(null);
							if (needUpdate) {
								String strSqlUpdate = "UPDATE Formation SET thickness=null WHERE formationUid =:formationUid";	
								String[] paramNames = {"formationUid"};
								Object[] paramValues = {formation.getFormationUid()};
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
							}
						}
					}else{
						if (prevFormation.getSampleTopMdMsl()!=null) lastSampleTopMdMsl = prevFormation.getSampleTopMdMsl();
						if (currSampleTopMdMsl!=null && lastSampleTopMdMsl!=null){
							thickness = currSampleTopMdMsl - lastSampleTopMdMsl;
							boolean needUpdate = false;
							if (prevFormation.getThickness()!=thickness) needUpdate = true;
							prevFormation.setThickness(thickness);
					//		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(prevFormation);
							if (needUpdate) {
								String strSqlUpdate = "UPDATE Formation SET thickness=:thickness WHERE formationUid =:formationUid";	
								String[] paramNames = {"formationUid", "thickness"};
								Object[] paramValues = {prevFormation.getFormationUid(), thickness};
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
							}
							prevFormation = formation;
						}else{
							formation.setThickness(null);
							if (formation.getFormationUid()!=null) {
								String strSqlUpdate = "UPDATE Formation SET thickness=null WHERE formationUid =:formationUid";	
								String[] paramNames = {"formationUid"};
								Object[] paramValues = {formation.getFormationUid()};
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
							}
						}
					}
				}
			}
		}
		// 23567 - SET THICKNESS & THICKNESSTVD FOR LAST FORMATION = 0
		if (prevFormation != null){ 
			prevFormation.setThickness(null);
			String strSqlUpdate = "UPDATE Formation SET thickness=null WHERE formationUid =:formationUid";	
			String[] paramNames = {"formationUid"};
			Object[] paramValues = {prevFormation.getFormationUid()};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
		//	ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(prevFormation);
		}
		
		prevFormation = null;
		for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("Formation").values()) {
			Formation formation = (Formation) node.getData();
			if (formation!=null){
				currSampleTopTvdMsl = formation.getSampleTopTvdMsl();
				
				if (BooleanUtils.isTrue(formation.getIsfault())){
					formation.setThicknessTvd(null);
					String strSqlUpdate = "UPDATE Formation SET thicknessTvd=null WHERE formationUid =:formationUid";	
					String[] paramNames = {"formationUid"};
					Object[] paramValues = {formation.getFormationUid()};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
				//	ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(formation);
				}else{
					if (prevFormation==null){
						if (formation.getSampleTopTvdMsl()!=null){
							prevFormation = formation;
						}else{
							formation.setThicknessTvd(null);
						}
					}else{
						if (prevFormation.getSampleTopTvdMsl()!=null) lastSampleTopTvdMsl = prevFormation.getSampleTopTvdMsl();
						if (lastSampleTopTvdMsl != null && currSampleTopTvdMsl != null){
							tvdThickness = currSampleTopTvdMsl - lastSampleTopTvdMsl; 
							boolean needUpdate = false;
							if (prevFormation.getThicknessTvd()!=tvdThickness) needUpdate = true;
							prevFormation.setThicknessTvd(tvdThickness);
							// ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(prevFormation);
							if (needUpdate) {
								String strSqlUpdate = "UPDATE Formation SET thicknessTvd=:tvdThickness WHERE formationUid =:formationUid";	
								String[] paramNames = {"formationUid", "tvdThickness"};
								Object[] paramValues = {prevFormation.getFormationUid(), tvdThickness};
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
							}
							prevFormation = formation;
						}else{
							formation.setThicknessTvd(null);
							if (formation.getFormationUid()!=null) {
								String strSqlUpdate = "UPDATE Formation SET thicknessTvd=null WHERE formationUid =:formationUid";	
								String[] paramNames = {"formationUid"};
								Object[] paramValues = {formation.getFormationUid()};
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
							}
						}
					}
				}
			}
		}
		// 23567 - SET THICKNESS & THICKNESSTVD FOR LAST FORMATION = 0
		if (prevFormation != null){
			prevFormation.setThicknessTvd(null);
			String strSqlUpdate = "UPDATE Formation SET thicknessTvd=null WHERE formationUid =:formationUid";	
			String[] paramNames = {"formationUid"};
			Object[] paramValues = {prevFormation.getFormationUid()};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
			// ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(prevFormation);
		}	
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
		Map<String, CommandBeanTreeNode> children = commandBean.getRoot().getChild("Formation");
		Integer counter = 0;
		for (Map.Entry<String, CommandBeanTreeNode> entry : children.entrySet()) {
			CommandBeanTreeNode node = entry.getValue();
			Object data = node.getData();
			if (data instanceof Formation) {
				Formation formation = (Formation)data;				
				formation.setSequence(counter);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(formation);
				counter++;
			}
		}
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(targetCommandBeanTreeNode != null){
			if(Formation.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())){
				if("formationName".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					String formationName = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "formationName");	
					String basinUid = "";
					if (formationName != null) {	
						UserSession getSession = UserSession.getInstance(request);
						basinUid = getSession.getCurrentWell().getBasin();
						String eub = CommonUtil.getConfiguredInstance().getEubCode(formationName, basinUid );
						if (eub != null)
						{
							targetCommandBeanTreeNode.getDynaAttr().put("eubFormationCode", eub);
						}	
					}	
				}
			}
		}
	}
}
