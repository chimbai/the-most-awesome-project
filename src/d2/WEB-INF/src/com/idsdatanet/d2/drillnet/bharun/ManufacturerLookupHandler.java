package com.idsdatanet.d2.drillnet.bharun;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ManufacturerLookupHandler implements LookupHandler {
	
	private String code;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String sql = "select a.lookupCompanyUid, a.companyName, a.isActive from LookupCompany a, LookupCompanyService b where (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and a.lookupCompanyUid = b.lookupCompanyUid and b.code = :code order by a.companyName";
		List<Object[]> listCompanyResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { "code" }, new String[] { code });
		if (listCompanyResults.size() > 0) {
			for (Object[] listCompanyResult : listCompanyResults) {
				if (listCompanyResult[0] != null && listCompanyResult[1] != null) {
					LookupItem lookupItem = new LookupItem(listCompanyResult[0].toString(), listCompanyResult[1].toString());
					Boolean isActive = (Boolean)listCompanyResult[2];
					lookupItem.setActive(isActive);
					result.put(listCompanyResult[0].toString(), lookupItem);
				}
			}
		}
		return result;
	}

}

