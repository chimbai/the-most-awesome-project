package com.idsdatanet.d2.drillnet.report;

import java.awt.Font;
import java.awt.FontMetrics;
import java.io.File;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * Helper class to provide helping function for report related operation
 * @author ssjong
 *
 */
public class ReportUtils extends DefaultBeanSupport {
	public static final int WELL_OPERATION_TYPE_DEFAULT = 1;
	public static final int WELL_OPERATION_TYPE_ONSHORE = 2;
	public static final int WELL_OPERATION_TYPE_OFFSHORE = 3;
	
	private ConcurrentHashMap<String, ReportTypes> report_type_info = new ConcurrentHashMap<String, ReportTypes>();
	
	/**
	 * Overload function
	 * @param groupUid
	 * @param reportType
	 * @param wellOperationType
	 * @param paperSize
	 * @throws Exception
	 */
	public String getXsl(String groupUid, String reportType, int wellOperationType, String paperSize, String language) throws Exception {
		return getXsl(groupUid,reportType,wellOperationType,paperSize, language, null, null);
	}
	
	public String getXsl(String groupUid, String reportType, int wellOperationType, String paperSize) throws Exception {
		return getXsl(groupUid,reportType,wellOperationType,paperSize, "en", null, null);
	}
	
	/**
	 * Return the defined xsl for the specified report type and well operation type
	 * @param reportType
	 * @return
	 * @throws Exception
	*/
	
	public String getXsl(String groupUid, String reportType, int wellOperationType, String paperSize, String language, String opCo, String rigUsed) throws Exception {
		ReportTypes reportTypeInfo = this.getReportType(groupUid, reportType, paperSize, language, opCo, rigUsed);
		if(reportTypeInfo == null) return null;
		
		if(wellOperationType == WELL_OPERATION_TYPE_DEFAULT){
			return reportTypeInfo.getDefaultXsl();
		}else if(wellOperationType == WELL_OPERATION_TYPE_ONSHORE){
			return reportTypeInfo.getOnshoreXsl();
		}else if(wellOperationType == WELL_OPERATION_TYPE_OFFSHORE){
			return reportTypeInfo.getOffshoreXsl();
		}
		
		return null;
	}
	
	/**
	 * Overload function 
	 * @param groupUid
	 * @param reportType
	 * @param paperSize
	 * @throws Exception
	 */
	public ReportTypes getReportType(String groupUid, String reportType, String paperSize) throws Exception {
		return getReportType(groupUid,reportType,paperSize,null);
	}
	/**
	 * Load info for a report type
	 * @throws Exception
	 */
	public ReportTypes getReportType(String groupUid, String reportType, String paperSize, String opCo) throws Exception {
		return this.getReportType(groupUid, reportType, paperSize, "en", opCo, null);
	}
	
	public ReportTypes getReportType(String groupUid, String reportType, String paperSize, String language, String opCo,String rigUsed) throws Exception {
		String key = groupUid + "." + reportType + "." + (paperSize == null ? "" : paperSize) + "." + (language == null ? "" : language) + (opCo == null ? "" : opCo )+ "."+ (rigUsed == null ? "" : rigUsed);
		ReportTypes reportType_record = this.report_type_info.get(key);
		if(reportType_record != null) return reportType_record;
		
		List<?> list = null;
		String sqlLanguage = " AND (language = :language) ";
		String sqlPaperSize = " AND (paperSize = :paperSize) ";

		if(StringUtils.isBlank(language) || "en".equalsIgnoreCase(language)) {
			sqlLanguage = " AND (language IS NULL OR language = :language OR language = '') ";
		}
		
		if(StringUtils.isBlank(paperSize) || "A4".equalsIgnoreCase(paperSize)) {
			sqlPaperSize = " AND (paperSize IS NULL OR paperSize = :paperSize OR paperSize = '') ";
		}
		
		String sql = "from ReportTypes where groupUid = :groupUid and reportType = :reportType and (isDeleted = false or isDeleted is null) ";
		list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql + sqlPaperSize + sqlLanguage,  new String[] {"groupUid", "reportType", "paperSize","language"}, new Object[] {groupUid,reportType,paperSize,language});

		if(list.size() > 0) {
			for(Object o : list) {
				ReportTypes rec = (ReportTypes) o;
				if(StringUtils.isNotBlank(rigUsed) && StringUtils.isNotBlank(rec.getRigInformationUid()) && rec.getRigInformationUid().equals(rigUsed) &&
				   StringUtils.isNotBlank(opCo) && StringUtils.isNotBlank(rec.getLookupCompanyUid()) && rec.getLookupCompanyUid().equals(opCo)) {
					reportType_record = rec;
					break;
				}else if(StringUtils.isNotBlank(rigUsed) && StringUtils.isNotBlank(rec.getRigInformationUid()) && rec.getRigInformationUid().equals(rigUsed) &&
						 StringUtils.isBlank(opCo) && StringUtils.isBlank(rec.getLookupCompanyUid())) {
					reportType_record = rec;
					break;
				}else if(StringUtils.isBlank(rigUsed) && StringUtils.isBlank(rec.getRigInformationUid()) &&
						 StringUtils.isNotBlank(opCo) && StringUtils.isNotBlank(rec.getLookupCompanyUid()) && rec.getLookupCompanyUid().equals(opCo)) {
					reportType_record = rec;
					break;
				}else if(StringUtils.isBlank(rigUsed) && StringUtils.isBlank(rec.getRigInformationUid()) &&
						 StringUtils.isBlank(opCo) && StringUtils.isBlank(rec.getLookupCompanyUid())) {
					reportType_record = rec;
					break;
				}else if(StringUtils.isNotBlank(opCo) && StringUtils.isBlank(rec.getRigInformationUid())) {
					//rigUsed will always has value from screen, if no template with Rig is set, next to continue to look for any matching opCo
					if(StringUtils.isNotBlank(rec.getLookupCompanyUid()) && rec.getLookupCompanyUid().equals(opCo)) {
						reportType_record = rec;
						break;
					}else {
						//if has opCo selected but no matching reportType, pick default template
						if(StringUtils.isBlank(rec.getRigInformationUid()) && StringUtils.isBlank(rec.getLookupCompanyUid())) {
							reportType_record = rec;
						}
					}
				}else {
					//default template 
					if(StringUtils.isBlank(rec.getRigInformationUid()) && StringUtils.isBlank(rec.getLookupCompanyUid())) {
						reportType_record = rec;
					}
				}
			}
		}
		
		if(reportType_record != null) {
			this.report_type_info.put(key, reportType_record);
		}

		return reportType_record;
	}
	
	public static ReportUtils getConfiguredInstance() throws Exception {
		return getSingletonInstance(ReportUtils.class);
	}
	
	/**
	 * Clear local cache
	 *
	 */
	protected void onApplicationRefresh() throws Exception {
		this.report_type_info.clear();
	}
	
	/**
	 * Clear local resources before disposal
	 *
	 */
	protected void onApplicationExit() throws Exception {
		this.report_type_info = null;
	}
	
	public static String getFullOutputFilePath(String localPath) throws Exception {
		String dir = ApplicationConfig.getConfiguredInstance().getReportOutputPath();
		if(StringUtils.isBlank(dir)){
			return localPath;
		}else{
			return (new File(dir, localPath)).getAbsolutePath();
		}
	}
	
	public static String getFullXslFilePath(String localPath) throws Exception {
		if(StringUtils.isBlank(localPath)) throw new Exception("Xsl file (local path) is not specified");
		
		String dir = ApplicationConfig.getConfiguredInstance().getReportXSLPath();
		if(StringUtils.isBlank(dir)){
			return localPath;
		}else{
			return (new File(dir, localPath)).getAbsolutePath();
		}
	}

	public static String replaceInvalidCharactersInFileName(String name){
		if(name == null) return null;
		return name.trim().replaceAll("[/\\\\?%*:|<>\"]", "_");
	}
	/**
	 * get report daily qc flag
	 * @param operationUid
	 * @param dailyUid
	 * @throws Exception
	 */
	public static String getReportDailyQcFlag(String operationUid, String dailyUid) throws Exception {
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		String sql = "FROM ReportDaily rd, Daily d " +
				"WHERE (d.isDeleted = false or d.isDeleted is null) " +
				"AND (rd.isDeleted = false or rd.isDeleted is null) " +
				"AND rd.dailyUid=d.dailyUid " +
				"AND d.dailyUid=:dailyUid " +
				"AND rd.reportType=:reportType " +
				"AND rd.operationUid=:operationUid";		
		String[] paramsFields = {"dailyUid","reportType","operationUid"};
		Object[] paramsValues = {dailyUid, reportType, operationUid};
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		if (list.size()>0) {
			Object[] obj_array = list.get(0);
			ReportDaily reportDaily = (ReportDaily) obj_array[0]; 
			return reportDaily.getQcFlag();
		}
		
		return "";
	}
	
	/**
	 * Method to wrap text content into multiple records of single row text.
	 * For IADC Overflow purpose.
	 * @param txt : String text to wrap
	 * @param f : Font to use
	 * @param width : width of the Description column.
	 * @return List<String> : text wrapped & splitted into multirows.
	 * @throws Exception
	 */
	public static List<String> getWrappedText(String txt, Font f, int width) throws Exception {
		JLabel label = new JLabel();
		Font defaultFont = new Font("Arial", Font.PLAIN, 6);
		int containerWidth = 435;
		label.setFont(defaultFont);
		
		if( f != null ){
			label.setFont( f );
		}		
		
		FontMetrics fm = label.getFontMetrics(label.getFont());
		if( width > 0 ) containerWidth = width;

		BreakIterator boundary = BreakIterator.getWordInstance();
		boundary.setText(txt);

		StringBuffer trial = new StringBuffer();
		StringBuffer real = new StringBuffer();

		List<String> multirows = new ArrayList();
		
		int start = boundary.first();
		for (int end = boundary.next(); end != BreakIterator.DONE;
			start = end, end = boundary.next()) {
			String word = txt.substring(start,end); 
			
			//Replace new line char with space.
			if( word.equals("\n") ) word = " ";
			
			trial.append(word);
			int trialWidth = SwingUtilities.computeStringWidth(fm, trial.toString());
			if (trialWidth > containerWidth) {
				trial = new StringBuffer(word);
				multirows.add(real.toString());
				//real.append("<br>");				
				
				real = new StringBuffer("");
			}

			real.append(word);
		}
		multirows.add(real.toString());

		//real.append("</html>");

		//label.setText(real.toString());
		
		return multirows;
	}
	
}
