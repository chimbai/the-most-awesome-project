package com.idsdatanet.d2.drillnet.user;

import java.util.List;

import org.apache.commons.lang.BooleanUtils;

import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.model.UserSignature;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.infra.system.security.DefaultPasswordEncoder;
/**
 * Common Method for User
 * @author Moses
 *
 */
public class UserUtils {
	
	/**
	 * Method use to validate the user based on the password given
	 * @param userUid
	 * @param presentedPassword
	 * @param session
	 * @return return true as valid access false as invalid
	 * @throws Exception
	 */
	public static Boolean validateUser(String userUid, String presentedPassword, UserSession session) throws Exception {
		
		
		User user = (User) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(User.class, userUid);
		if (user!=null)
		{
			if (DefaultPasswordEncoder.isPasswordMatch(presentedPassword, user.getPassword()) && !BooleanUtils.isTrue(user.getIsblocked()))
				return true;
		}	
		return false;
	}
	
	public static Boolean hasSignature(String userUid) throws Exception
	{
		 String[] paramNames = {"userUid"};
	     Object[] paramValues = {userUid};
	     List<UserSignature> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM UserSignature WHERE (isDeleted is null or isDeleted = false) and userUid=:userUid", paramNames, paramValues);
	     if (result.size()>0)
	    	 return true;
	     return false;
	}
	
	public static void removeSignature(String userUid) throws Exception
	{
		 String[] paramNames = {"userUid"};
	     Object[] paramValues = {userUid};
	     ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE UserSignature set isDeleted = true WHERE userUid=:userUid", paramNames, paramValues);
	}
	
	public static void addSignature(String userUid, String tourSignature) throws Exception
	{
		UserSignature us = new UserSignature();
		us.setUserUid(userUid);
		us.setTourSignature(tourSignature);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(us);
	}
	
	public static String checkBooleanString(Boolean checkTxt) throws Exception
	{
		if(checkTxt!=null && checkTxt){
			return "1";
		}else if(checkTxt!=null && !checkTxt){
			return "0";
		}else{
			return "";
		}
	}

}