package com.idsdatanet.d2.drillnet.graph;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class DVDGraphDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		QueryProperties qp = new QueryProperties();
	    qp.setUomConversionEnabled(false);
	    
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		Double minDuration = 0.0;
		Double maxDuration = 0.0;
		Double minDepth = 0.0;
		Double maxDepth = 0.0;
		Double kickOffMdMsl = 0.0;
		CustomFieldUom durationConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Operation.class, "daysSpentPriorToSpud");
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "depthMdMsl");
		String currentWellboreUid = userContext.getUserSelection().getWellboreUid();
		String currentOperationPlanMasterUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(userContext.getUserSelection().getOperationUid());
		
		if(StringUtils.isNotBlank(currentWellboreUid) && currentWellboreUid != null){
			Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(currentWellboreUid);
			
			if(wellbore.getKickoffMdMsl() != null){
				thisConverter.setBaseValue(wellbore.getKickoffMdMsl());
				kickOffMdMsl = thisConverter.getConvertedValue();
			}
		}
		
		// Collect Planned Day vs Depth Data
		ReportDataNode thisReportNode = reportDataNode.addChild("operationPlanPhase");
		thisReportNode.addProperty("p10Duration","0.0");
		thisReportNode.addProperty("cumP10Duration","0.0");
		thisReportNode.addProperty("p50Duration","0.0");
		thisReportNode.addProperty("cumP50Duration","0.0");
		thisReportNode.addProperty("p90Duration","0.0");
		thisReportNode.addProperty("cumP90Duration","0.0");
		thisReportNode.addProperty("TLDuration","0.0");
		thisReportNode.addProperty("cumTLDuration","0.0");
		thisReportNode.addProperty("pmeanDuration","0.0");
		thisReportNode.addProperty("cumPmeanDuration","0.0");
		if(durationConverter.isUOMMappingAvailable() ){
			thisReportNode.addProperty("durationUomSymbol", durationConverter.getUomSymbol());
			thisReportNode.addProperty("durationUomLabel", durationConverter.getUomLabel());
		}
		thisReportNode.addProperty("depthMdMsl",kickOffMdMsl.toString());
		if(thisConverter.isUOMMappingAvailable() ){
			thisReportNode.addProperty("depthUomSymbol", thisConverter.getUomSymbol());
			thisReportNode.addProperty("depthUomLabel", thisConverter.getUomLabel());
		}
		
		String strSql = "SELECT p10Duration, p50Duration, p90Duration, tlDuration, pmeanDuration, depthMdMsl FROM OperationPlanPhase " +
						"WHERE (isDeleted = FALSE OR isDeleted IS NULL) " +
						"AND operationPlanMasterUid = :operationPlanMasterUid " +
						"ORDER BY sequence, phaseSequence";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationPlanMasterUid", currentOperationPlanMasterUid, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			
			Double cumP10Duration = 0.0;
			Double cumP50Duration = 0.0;
			Double cumP90Duration = 0.0;
			Double cumTLDuration = 0.0;
			Double cumPmeanDuration = 0.0;
			
			for(Object objResult: lstResult){
				//due to result is in array, cast the result to array first and read 1 by 1
				Object[] plannedObject = (Object[]) objResult;
				
				Double p10Duration = 0.0;
				Double p50Duration = 0.0;
				Double p90Duration = 0.0;
				Double TLDuration = 0.0;
				Double pmeanDuration = 0.0;
				Double depthMdMsl = 0.0;
				
				if(plannedObject[0]!=null && StringUtils.isNotBlank(plannedObject[0].toString())) p10Duration = Double.parseDouble(plannedObject[0].toString());
				if(plannedObject[1]!=null && StringUtils.isNotBlank(plannedObject[1].toString())) p50Duration = Double.parseDouble(plannedObject[1].toString());
				if(plannedObject[2]!=null && StringUtils.isNotBlank(plannedObject[2].toString())) p90Duration = Double.parseDouble(plannedObject[2].toString());
				if(plannedObject[3]!=null && StringUtils.isNotBlank(plannedObject[3].toString())) TLDuration = Double.parseDouble(plannedObject[3].toString());
				if(plannedObject[4]!=null && StringUtils.isNotBlank(plannedObject[4].toString())) pmeanDuration = Double.parseDouble(plannedObject[4].toString());
				if(plannedObject[5]!=null && StringUtils.isNotBlank(plannedObject[5].toString())) depthMdMsl = Double.parseDouble(plannedObject[5].toString());
				
				if(p10Duration != null){
					durationConverter.setBaseValue(p10Duration);
					p10Duration = durationConverter.getConvertedValue();
					cumP10Duration += p10Duration;
					if(cumP10Duration > maxDuration) maxDuration = cumP10Duration;
				}
				
				if(p50Duration != null){
					durationConverter.setBaseValue(p50Duration);
					p50Duration = durationConverter.getConvertedValue();
					cumP50Duration += p50Duration;
					if(cumP50Duration > maxDuration) maxDuration = cumP50Duration;
				}
				
				if(p90Duration != null){
					durationConverter.setBaseValue(p90Duration);
					p90Duration = durationConverter.getConvertedValue();
					cumP90Duration += p90Duration;
					if(cumP90Duration > maxDuration) maxDuration = cumP90Duration;
				}
				
				if(TLDuration != null){
					durationConverter.setBaseValue(TLDuration);
					TLDuration = durationConverter.getConvertedValue();
					cumTLDuration += TLDuration;
					if(cumTLDuration > maxDuration) maxDuration = cumTLDuration;
				}
				
				if(pmeanDuration != null){
					durationConverter.setBaseValue(pmeanDuration);
					pmeanDuration = durationConverter.getConvertedValue();
					cumPmeanDuration += pmeanDuration;
					if(cumPmeanDuration > maxDuration) maxDuration = cumPmeanDuration;
				}
				
				if(depthMdMsl != null){
					thisConverter.setBaseValue(depthMdMsl);
					depthMdMsl = thisConverter.getConvertedValue();
					if(depthMdMsl > maxDepth) maxDepth = depthMdMsl;
				}
				
				thisReportNode = reportDataNode.addChild("operationPlanPhase");
				thisReportNode.addProperty("p10Duration",p10Duration.toString());
				thisReportNode.addProperty("cumP10Duration",cumP10Duration.toString());
				thisReportNode.addProperty("p50Duration",p50Duration.toString());
				thisReportNode.addProperty("cumP50Duration",cumP50Duration.toString());
				thisReportNode.addProperty("p90Duration",p90Duration.toString());
				thisReportNode.addProperty("cumP90Duration",cumP90Duration.toString());
				thisReportNode.addProperty("TLDuration",TLDuration.toString());
				thisReportNode.addProperty("cumTLDuration",cumTLDuration.toString());
				thisReportNode.addProperty("pmeanDuration",pmeanDuration.toString());
				thisReportNode.addProperty("cumPmeanDuration",cumPmeanDuration.toString());
				if(durationConverter.isUOMMappingAvailable() ){
					thisReportNode.addProperty("durationUomSymbol", durationConverter.getUomSymbol());
					thisReportNode.addProperty("durationUomLabel", durationConverter.getUomLabel());
				}
				thisReportNode.addProperty("depthMdMsl",depthMdMsl.toString());
				if(thisConverter.isUOMMappingAvailable() ){
					thisReportNode.addProperty("depthUomSymbol", thisConverter.getUomSymbol());
					thisReportNode.addProperty("depthUomLabel", thisConverter.getUomLabel());
				}
			} 
		}

		// Collect Actual Day vs Depth Data
		thisReportNode = reportDataNode.addChild("activity");
		thisReportNode.addProperty("duration", "0.0");
		thisReportNode.addProperty("cumDuration", "0.0");
		if(durationConverter.isUOMMappingAvailable() ){
			thisReportNode.addProperty("durationUomSymbol", durationConverter.getUomSymbol());
			thisReportNode.addProperty("durationUomLabel", durationConverter.getUomLabel());
		}
		thisReportNode.addProperty("depthMdMsl",kickOffMdMsl.toString());
		if(thisConverter.isUOMMappingAvailable() ){
			thisReportNode.addProperty("depthUomSymbol", thisConverter.getUomSymbol());
			thisReportNode.addProperty("depthUomLabel", thisConverter.getUomLabel());
		}
		
		strSql = "SELECT a.activityDuration, a.depthMdMsl FROM Activity a, Daily d " +
			     "WHERE (a.isDeleted = FALSE OR a.isDeleted IS NULL) " +
				 "AND (d.isDeleted = FALSE OR d.isDeleted IS NULL) " +
				 "AND a.dailyUid = d.dailyUid " +
				 "AND d.dayDate <= :userDate " +
				 "AND a.operationUid = :thisOperationUid " +
				 "AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " +
				 "ORDER BY a.dayPlus, a.endDatetime";
		String[] paramsFields = {"userDate", "thisOperationUid"};
		Object[] paramsValues = {todayDate, userContext.getUserSelection().getOperationUid()};
		
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if (lstResult != null && lstResult.size() > 0){
			
			Double cumDuration = 0.0;
			
			for(Object objResult: lstResult){
				//due to result is in array, cast the result to array first and read 1 by 1
				Object[] activity = (Object[]) objResult;
					
				Double duration = 0.0;
				Double depthMdMsl = 0.0;
				
				if (activity[0] != null && StringUtils.isNotBlank(activity[0].toString())) duration = Double.parseDouble(activity[0].toString());
				if (activity[1] != null && StringUtils.isNotBlank(activity[1].toString())) depthMdMsl = Double.parseDouble(activity[1].toString());
				
				if(duration != null){
					durationConverter.setBaseValue(duration);
					duration = durationConverter.getConvertedValue();
					cumDuration += duration;
					if(cumDuration > maxDuration) maxDuration = cumDuration;
				}
				
				if(depthMdMsl != null){
					thisConverter.setBaseValue(depthMdMsl);
					depthMdMsl = thisConverter.getConvertedValue();
					if(depthMdMsl > maxDepth) maxDepth = depthMdMsl;
				}
				
				thisReportNode = reportDataNode.addChild("activity");
				thisReportNode.addProperty("duration", duration.toString());
				thisReportNode.addProperty("cumDuration", cumDuration.toString());
				if(durationConverter.isUOMMappingAvailable() ){
					thisReportNode.addProperty("durationUomSymbol", durationConverter.getUomSymbol());
					thisReportNode.addProperty("durationUomLabel", durationConverter.getUomLabel());
				}
				thisReportNode.addProperty("depthMdMsl", depthMdMsl.toString());
				if(thisConverter.isUOMMappingAvailable() ){
					thisReportNode.addProperty("depthUomSymbol", thisConverter.getUomSymbol());
					thisReportNode.addProperty("depthUomLabel", thisConverter.getUomLabel());
				}
			}
		}
		
		maxDepth = maxDepth + (maxDepth * 0.02);
		maxDuration = maxDuration + (maxDuration * 0.01);
		
		thisReportNode = reportDataNode.addChild("scaleValue");
		thisReportNode.addProperty("minDepth", minDepth.toString());
		thisReportNode.addProperty("maxDepth", maxDepth.toString());
		thisReportNode.addProperty("minDuration", minDuration.toString());
		thisReportNode.addProperty("maxDuration", maxDuration.toString());
	}
}
