package com.idsdatanet.d2.drillnet.personnelonsite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.PersonnelOnSite;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class PersonnelByCompanyBeanReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		String thisOperationUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getOperationUid().toString())) thisOperationUid = userContext.getUserSelection().getOperationUid().toString();
		
		Map<String, Integer> companyMap = new LinkedHashMap<String, Integer>();
		
		String strSql = "FROM PersonnelOnSite p, Daily d WHERE p.dailyUid = d.dailyUid " +
					    "AND (d.isDeleted IS NULL or d.isDeleted = false) AND (p.isDeleted IS NULL or p.isDeleted = false) " +
					    "AND p.operationUid = :thisOperationUid AND d.dailyUid=:dailyUid";
		String[] paramsFields = {"dailyUid", "thisOperationUid"};
		Object[] paramsValues = {daily.getDailyUid(), thisOperationUid};
		
		List<Object[]> pobList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		for(Object[] objArr : pobList) {
		    PersonnelOnSite pob = (PersonnelOnSite) objArr[0];
			String crewCompany = (pob.getCrewCompany() != null) ? pob.getCrewCompany() : "";
			Integer pax = (pob.getPax() != null) ? pob.getPax() : 0;
			if (StringUtils.isNotBlank(crewCompany)) { 
			    crewCompany = this.getCompanyName(crewCompany); // We'll end up with label, lookup n/a or not
			}
			
			if (companyMap.containsKey(crewCompany)) {
			    Integer currPax = companyMap.get(crewCompany);
			    if (pax != null) { companyMap.put(crewCompany, currPax + pax); }
			} else {
			    if (pax != null) { companyMap.put(crewCompany, pax); }
			}
		}
		
        List<Map.Entry<String, Integer>> entries = new ArrayList<Map.Entry<String, Integer>>(companyMap.entrySet());
        Collections.sort(entries, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return a.getKey().compareTo(b.getKey());
            }
        });
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : entries) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

		for (Map.Entry<String, Integer> company : sortedMap.entrySet()) {
		    ReportDataNode newNode = reportDataNode.addChild("PersonnelByCompany");
	        newNode.addProperty("name", company.getKey().toString());
	        newNode.addProperty("pax", company.getValue().toString());
		}
	}
	
	private String getCompanyName(String companyUid) throws Exception{
		String[] paramsFields = {"companyUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = companyUid;
		
		String strSql = "SELECT companyName FROM LookupCompany WHERE (isDeleted = false or isDeleted is null) and lookupCompanyUid =:companyUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (!lstResult.isEmpty()) {
			Object thisResult = (Object) lstResult.get(0);
			if (thisResult != null) {
				return thisResult.toString();
			} else {
				return companyUid;
			}
		} else {
			return companyUid;
		}
	}
}
