package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.report.ReportOption;

public class NPTOccurencesReportDataGenerator extends NPTReportDataGenerator{

	private String uriLookupCompany = "db://LookupCompany?key=lookupCompanyUid&value=companyName&cache=false&order=companyName";
	private Map<String,LookupItem> companyList = null;
	
	protected void generateActualData(UserContext userContext, Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		this.setCompanyList(LookupManager.getConfiguredInstance().getLookup(this.uriLookupCompany, userContext.getUserSelection(), null));
		List<NPTSummary> includedList = this.shouldIncludeWell(userContext, startDate, endDate);
		this.includeQueryData(includedList, reportDataNode, userContext.getUserSelection(), startDate, endDate);
	}
	
	private List<NPTSummary> generatedata(UserContext userContext, Date startDate, Date endDate, String includeWellUid) throws Exception
	{

		String[] paramNames = {"startDate","endDate","companyNPTClassCode","otherNPTClassCode","field","wellUid"};
		Object[] values = {startDate,endDate,this.getCompanyNPTClassCode(),this.getOtherNPTClassCode(),BHIQueryUtils.getReportOptionValue(userContext,BHIQueryUtils.CONSTANT_FIELD),includeWellUid};
		String sql = "SELECT w.wellUid,w.wellName, o.operationName, w.field, a.activityDescription, a.nptSubCategory, a.lookupCompanyUid, a.nptMainCategory, sum(a.activityDuration) as duration"+		
			" FROM Well w,Wellbore wb, Operation o, Activity a, Daily d "+ 
			" Where w.wellUid=wb.wellUid and wb.wellboreUid=o.wellboreUid"+
			" and d.operationUid=o.operationUid"+ 
			" and a.dailyUid=d.dailyUid"+
			" and (a.isDeleted is null or a.isDeleted = False)"+
			" and (d.isDeleted is null or d.isDeleted = False)"+
			" and (w.isDeleted is null or w.isDeleted = false) and (wb.isDeleted is null or wb.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false)"+
			" and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) "+
			" and (a.classCode=:companyNPTClassCode or a.classCode=:otherNPTClassCode)"+ 
			" and (a.startDatetime>=:startDate and a.endDatetime<=:endDate)"+ 						
			" and w.wellUid=:wellUid"+
			" and (w.field in (:field))"+
			" and (a.nptSubCategory != null and a.nptMainCategory != null and a.lookupCompanyUid != null)"+			
			" group by w.field, a.nptMainCategory, a.nptSubCategory, a.lookupCompanyUid, a.activityDescription"+
			" order by w.field, a.nptMainCategory, a.nptSubCategory, a.lookupCompanyUid, a.startDatetime";
		
		List<NPTSummary> includedList = new ArrayList<NPTSummary>();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, values, qp);
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
		for (Object[] item :result)
		{
			String wellUid = (String)item[0];
			
			NPTSummary npt = new NPTSummary((String)item[0],null, null, (String)item[6], null,null,null,null);			
			npt.setWellName((String)item[1]);
			npt.setOperationName((String)item[2]);		
			npt.setField((String)item[3]);			
			npt.setDuration(Double.parseDouble(String.valueOf(item[8])));
			thisConverter.setBaseValue(npt.getDuration());
			npt.setDuration(thisConverter.getConvertedValue());
			npt.setActivityDescription((String)item[4]);
			
			String nptMainCategory = this.getNptMainCategoryName((String) item[7], userContext);
			String nptSubCategory = this.getNptSubCategoryName((String) item[5], userContext);
			
			npt.setNptSubCategory(nptSubCategory);		
			npt.setNptMainCategory(nptMainCategory);			
			
			includedList.add(npt);
		}
		return includedList;
		
	}
	
	private List<NPTSummary> shouldIncludeWell(UserContext userContext, Date startDate, Date endDate) throws Exception
	{
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		ReportOption wells = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL):null;
		List<NPTSummary> result = new ArrayList<NPTSummary>();
		for (String wellUid : wells.getValues())
		{
			result.addAll(this.generatedata(userContext, startDate, endDate,wellUid));
		}
		return result;
	}

	private void includeQueryData(List<NPTSummary> nptSummary, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception
	{			
		ReportDataNode nodeChild = node.addChild("NPTOccurences");		
		//ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;

		List<String> fieldList = new ArrayList<String>();
		for (NPTSummary npt : nptSummary)
		{
			if (!fieldList.contains(npt.getField()))fieldList.add(npt.getField());
		}
		if (fieldList != null )
		{
			for (String fl : fieldList)
			{
				List<String> nptMain = new ArrayList<String>();
				for(NPTSummary npt : nptSummary)
				{
					if (fl.equalsIgnoreCase(npt.getField()))
					{
						if (!nptMain.contains(npt.getNptMainCategory()))
							nptMain.add(npt.getNptMainCategory());
					}
				}
				if (nptMain != null)
				{				 	
					for (String m1 : nptMain)
					{
						
						List<String> nptSub = new ArrayList<String>();
						for(NPTSummary npt : nptSummary)
						{
							if (m1.equalsIgnoreCase(npt.getNptMainCategory()))
							{
								if (!nptSub.contains(npt.getNptSubCategory() + ":" + npt.getLookupCompanyUid()))
									nptSub.add(npt.getNptSubCategory() + ":" + npt.getLookupCompanyUid());
							}
						}
						
						if (nptSub != null)
						{
							for (String s1 : nptSub)
							{
								ReportDataNode catChild = nodeChild.addChild("data");
								Double duration = 0.00;						
								String lookupCompanyUid = null;						
								String wellUid = null;
								String wellName = null;
								String activityDescription = null;
								String nptSubCategory = null;
								String nptMainCategory = null;
								String operationname = null;
								String field = null;
								LookupItem item = null;
								
								for (NPTSummary npt : nptSummary)
								{
									if ( (npt.getNptSubCategory() + ":" + npt.getLookupCompanyUid()).equalsIgnoreCase(s1) && fl.equalsIgnoreCase(npt.getField()) && m1.equalsIgnoreCase(npt.getNptMainCategory()))
									{								
										duration +=npt.getDuration();
										if (lookupCompanyUid == null)lookupCompanyUid = npt.getLookupCompanyUid();
										if (wellName == null)wellName = npt.getWellName();
										if (wellUid == null)wellUid = npt.getWellUid();
										if ( nptSubCategory == null) nptSubCategory = npt.getNptSubCategory();
										if ( nptMainCategory == null) nptMainCategory = npt.getNptMainCategory();
										if ( operationname == null) operationname = npt.getOperationName();
										if ( field == null) field = npt.getField();
										if (activityDescription == null)
										{
											activityDescription =npt.getActivityDescription();									
										}else{
											activityDescription = activityDescription + "\n" + npt.getActivityDescription();		
										}
										item = this.companyList.get(npt.getLookupCompanyUid());
									}
								}
														
								catChild.addProperty("welluid", wellUid);
								catChild.addProperty("wellname", wellName);
								catChild.addProperty("operationname", operationname);
								catChild.addProperty("field",field);			
								catChild.addProperty("activityDescription",activityDescription);			
								catChild.addProperty("nptSubCategory",nptSubCategory);	
								catChild.addProperty("nptMainCategory",nptMainCategory);
								catChild.addProperty("lookupCompanyUid", lookupCompanyUid);
								//catChild.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(lookupCompanyUid).toString());
								if (item !=null)
									catChild.addProperty("lookupCompanyUid_lookupLabel",item.getValue().toString());
								catChild.addProperty("NPTduration", String.valueOf(duration));	
							}
						}										
					}
				}				
			}
		}	
	}	
	
	private String getNptMainCategoryName(String nptMainCategory, UserContext userContext) throws Exception {
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://Activity.nptMainCategory?key=code&value=label", userSelection, null);
		if (lookupMap != null) {
			LookupItem lookupItem = lookupMap.get(nptMainCategory);
			if (lookupItem != null) {
				return (String) lookupItem.getValue();
			}
		}
		
		return nptMainCategory;
	}
	
	private String getNptSubCategoryName(String nptSubCategory, UserContext userContext) throws Exception {
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		CascadeLookupSet[] cascadeLookupNptSubCategory = LookupManager.getConfiguredInstance().getCompleteCascadeLookupSet("xml://Activity.nptSubCategory?key=code&amp;value=label", userSelection);
		
		for(CascadeLookupSet cascadeLookup : cascadeLookupNptSubCategory){
			Map<String, LookupItem> lookupList = cascadeLookup.getLookup();
			if (lookupList != null) {
				LookupItem lookupItem = lookupList.get(nptSubCategory);
				if (lookupItem != null) {
					return (String) lookupItem.getValue();
				}
			}					
		}		
		return nptSubCategory;
	}
		
	public void setUriLookupCompany(String uriLookupCompany) {
		this.uriLookupCompany = uriLookupCompany;
	}

	public String getUriLookupCompany() {
		return uriLookupCompany;
	}

	public void setCompanyList(Map<String,LookupItem> companyList) {
		this.companyList = companyList;
	}

	public Map<String,LookupItem> getCompanyList() {
		return companyList;
	}
}
