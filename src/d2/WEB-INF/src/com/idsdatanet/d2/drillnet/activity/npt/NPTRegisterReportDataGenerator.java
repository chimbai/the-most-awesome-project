package com.idsdatanet.d2.drillnet.activity.npt;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.idsdatanet.d2.infra.uom.OperationUomAndDatumSelector;
import com.idsdatanet.d2.infra.uom.OperationUomContext;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.report.ReportOption;

public class NPTRegisterReportDataGenerator extends NPTReportDataGenerator{


	private String uriLookupCompany = "db://LookupCompany?key=lookupCompanyUid&value=companyName&cache=false&order=companyName";
	private String rigLookupUri = "db://RigInformation?key=rigInformationUid&value=rigName&cache=false&order=rigName";
	private Map<String,LookupItem> rigLookup;
	private Map<String,LookupItem> companyList = null;
	private String uriNptCategory;
	private String uriNptSubCategory;
	
	
	protected void generateActualData(UserContext userContext, Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		this.setCompanyList(LookupManager.getConfiguredInstance().getLookup(this.uriLookupCompany, userContext.getUserSelection(), null));
		this.setRigLookup(LookupManager.getConfiguredInstance().getLookup(this.getRigLookupUri(), userContext.getUserSelection(), null));
		List<NPTSummary> includedList = this.shouldIncludeWell(userContext, startDate, endDate);
		this.includeQueryData(includedList, reportDataNode, userContext.getUserSelection(), startDate, endDate);
	}
	
	private List<NPTSummary> generatedata(UserContext userContext, Date startDate, Date endDate, String includeWellUid) throws Exception
	{
		OperationUomContext operationUomContext = new OperationUomContext(true, true);

		String[] paramNames = {"startDate","endDate","companyNPTClassCode","otherNPTClassCode","field","wellUid"};
		Object[] values = {startDate,endDate,this.getCompanyNPTClassCode(),this.getOtherNPTClassCode(),BHIQueryUtils.getReportOptionValue(userContext,BHIQueryUtils.CONSTANT_FIELD),includeWellUid};
		String sql = "SELECT o.operationUid, w.wellUid,w.wellName, o.operationName, w.field, w.country, a.activityDuration, a.sections, a.phaseCode, "+
			"a.startDatetime, a.nptMainCategory, a.nptSubCategory, a.lookupCompanyUid, a.activityDescription, o.rigInformationUid, a.incidentLevel, a.depthMdMsl, a.incidentNumber"+
			" FROM Well w,Wellbore wb, Operation o, Activity a, Daily d "+ 
			" Where w.wellUid=wb.wellUid and wb.wellboreUid=o.wellboreUid"+
			" and d.operationUid=o.operationUid"+ 
			" and a.dailyUid=d.dailyUid"+
			" and (a.isDeleted is null or a.isDeleted = False)"+
			" and (d.isDeleted is null or d.isDeleted = False)"+
			" and (w.isDeleted is null or w.isDeleted = false) and (wb.isDeleted is null or wb.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false)"+
			" and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) "+
			" and (a.classCode=:companyNPTClassCode or a.classCode=:otherNPTClassCode)"+ 
			" and (a.startDatetime>=:startDate and a.endDatetime<=:endDate)"+ 						
			" and w.wellUid=:wellUid"+
			" and (w.field in (:field))"+
			//" group by w.wellUid, w.field"+
			" order by w.field, w.wellName, d.dayDate, a.endDatetime, a.startDatetime";
		
		List<NPTSummary> includedList = new ArrayList<NPTSummary>();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, values, qp);
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
		for (Object[] item :result)
		{
			String operationUid = (String) item[0];
			String wellUid = (String) item[1];
			String wellname = (String) item[2];
			String operationName = (String) item[3];
			String field = (String) item[4];
			String country = (String) item[5];
			Double activityDuration = (Double) item[6];
			String sections = (String) item[7];
			String phaseCode = (String) item[8];
			Date startDatetime = (Date) item[9];
			//String nptMainCategory = (String) item[10];
			//String nptSubCategory = (String) item[11];
			String nptMainCategory = this.getNptMainCategoryName((String) item[10], userContext);
			String nptSubCategory = this.getNptSubCategoryName((String) item[11], userContext);
			
			String lookupCompanyUid = (String) item[12];
			String activityDescription = (String) item[13];
			String rigInformationUid = (String) item[14];
			String incidentLevel = String.valueOf(item[15]);
			Double depthMdMsl = (Double) item[16];
			String incidentNumber = String.valueOf(item[17]);
			
			operationUomContext.setOperationUid(operationUid);
			
			NPTSummary npt = new NPTSummary(wellUid, null, country,
					lookupCompanyUid, null, activityDuration, null, null);			
			npt.setWellName(wellname);
			npt.setOperationName(operationName);		
			npt.setField(field);
			npt.setCountry(country);
			npt.setDuration(activityDuration);
			thisConverter.setBaseValue(activityDuration);
			npt.setDuration(thisConverter.getConvertedValue());
			npt.setSections(sections);
			npt.setPhaseCode(phaseCode);
			npt.setStartDatetime(startDatetime);
			npt.setNptMainCategory(nptMainCategory);
			npt.setNptSubCategory(nptSubCategory);
			npt.setLookupCompanyUid(lookupCompanyUid);
			npt.setActivityDescription(activityDescription);
			npt.setRigInformationUid(rigInformationUid);
			npt.setIncidentLevel(incidentLevel);
			if(depthMdMsl==null){
				npt.setDepthMdMsl(0.00);
			}else{
				npt.setDepthMdMsl(depthMdMsl);
			}
			
			CustomFieldUom depthConverter = new CustomFieldUom((Locale)null, Activity.class, "depthMdMsl", operationUomContext);
			npt.setDepthMdMslUom(depthConverter.getUomSymbol());
			depthConverter.setBaseValue(npt.getDepthMdMsl());
			npt.setDepthMdMsl(depthConverter.getBasevalue());
			npt.setIncidentNumber(incidentNumber);
			
			includedList.add(npt);
		}
		return includedList;
		
	}
	
	private List<NPTSummary> shouldIncludeWell(UserContext userContext, Date startDate, Date endDate) throws Exception
	{
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		ReportOption wells = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL):null;
		List<NPTSummary> result = new ArrayList<NPTSummary>();
		for (String wellUid : wells.getValues())
		{
			result.addAll(this.generatedata(userContext, startDate, endDate,wellUid));
		}
		return result;
	}

	private void includeQueryData(List<NPTSummary> nptSummary, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception
	{
		//ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ReportDataNode nodeChild = node.addChild("NPTRegister");		
		Double totalNPT =0.00;
		for (NPTSummary npt : nptSummary)
		{	
			ReportDataNode catChild = nodeChild.addChild("data");
			catChild.addProperty("welluid", npt.getWellUid());
			catChild.addProperty("wellname", npt.getWellName());
			catChild.addProperty("operationname", npt.getOperationName());
			catChild.addProperty("field",npt.getField());
			catChild.addProperty("rigInformationUid",npt.getRigInformationUid());
			LookupItem rig = this.getRigLookup().get(npt.getRigInformationUid());
			if (rig !=null)
				catChild.addProperty("rigName",rig.getValue().toString());
			
			catChild.addProperty("sections",npt.getSections());
			catChild.addProperty("phasecode",npt.getPhaseCode());
			catChild.addProperty("startdatetime",dateFormat.format(npt.getStartDatetime()));
			catChild.addProperty("startdatetime_epoch",String.valueOf(npt.getStartDatetime().getTime()));
			catChild.addProperty("nptmaincategory",npt.getNptMainCategory());
			catChild.addProperty("nptsubcategory",npt.getNptSubCategory());
			//catChild.addProperty("lookupcompanyuid_lookupLabel",cos.getValue(npt.getLookupCompanyUid()).toString());
			catChild.addProperty("lookupcompanyuid",npt.getLookupCompanyUid());
			LookupItem item = this.companyList.get(npt.getLookupCompanyUid());
			if (item !=null)
				catChild.addProperty("lookupCompanyUid_lookupLabel",item.getValue().toString());
			catChild.addProperty("incidentlevel",npt.getIncidentLevel());
			catChild.addProperty("depthMdMsl",String.valueOf(npt.getDepthMdMsl()));
			catChild.addProperty("depthMdMslUom",npt.getDepthMdMslUom());
			catChild.addProperty("incidentNumber",npt.getIncidentNumber());
			
			//catChild.addProperty("activitydate",npt.getActivityDate().toString());
			//catChild.addProperty("activityduration",npt.getActivityDuration().toString());
			
			catChild.addProperty("NPTduration", String.valueOf(npt.getDuration()));
			totalNPT += npt.getDuration();
			
			catChild.addProperty("activitydescription",npt.getActivityDescription());
		}		
		nodeChild.addProperty("totalNPTDuration", totalNPT.toString());
	}

	public void setRigLookupUri(String rigLookupUri) {
		this.rigLookupUri = rigLookupUri;
	}

	public String getRigLookupUri() {
		return rigLookupUri;
	}

	public void setRigLookup(Map<String,LookupItem> rigLookup) {
		this.rigLookup = rigLookup;
	}

	public Map<String,LookupItem> getRigLookup() {
		return rigLookup;
	}
	
	private String getNptMainCategoryName(String nptMainCategory, UserContext userContext) throws Exception {
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://Activity.nptMainCategory?key=code&value=label", userSelection, null);
		if (lookupMap != null) {
			LookupItem lookupItem = lookupMap.get(nptMainCategory);
			if (lookupItem != null) {
				return (String) lookupItem.getValue();
			}
		}
		
		return nptMainCategory;
	}
	
	private String getNptSubCategoryName(String nptSubCategory, UserContext userContext) throws Exception {
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		CascadeLookupSet[] cascadeLookupNptSubCategory = LookupManager.getConfiguredInstance().getCompleteCascadeLookupSet("xml://Activity.nptSubCategory?key=code&amp;value=label", userSelection);
		  
		for(CascadeLookupSet cascadeLookup : cascadeLookupNptSubCategory){
			Map<String, LookupItem> lookupList = cascadeLookup.getLookup();
			if (lookupList != null) {
				LookupItem lookupItem = lookupList.get(nptSubCategory);
				if (lookupItem != null) {
					return (String) lookupItem.getValue();
				}
			}
		}
		return nptSubCategory;
	}
	
	public void setUriNptCategory(String uriNptCategory) {
		this.uriNptCategory = uriNptCategory;
	}

	public String getUriNptCategory() {
		return uriNptCategory;
	}
	
	public void setUriNptSubCategory(String uriNptSubCategory) {
		this.uriNptSubCategory = uriNptSubCategory;
	}

	public String getUriNptSubCategory() {
		return uriNptSubCategory;
	}
	
	public void setUriLookupCompany(String uriLookupCompany) {
		this.uriLookupCompany = uriLookupCompany;
	}

	public String getUriLookupCompany() {
		return uriLookupCompany;
	}

	public void setCompanyList(Map<String,LookupItem> companyList) {
		this.companyList = companyList;
	}

	public Map<String,LookupItem> getCompanyList() {
		return companyList;
	}

}
