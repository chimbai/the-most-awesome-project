package com.idsdatanet.d2.drillnet.hseIncident;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.WellControlComponentTest;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class DaysSinceHseIncidentByOpsDataGenerator implements ReportDataGenerator {

	private List<String> hseIncidentMarkers;
	private List<String> nextHseIncidentMarkers;
	private List<String> bopTest;
	private List<String> nextBopTest;
	private List<String> hseIncidentSum;
	
	public void setHseIncidentMarkers(List<String> hseIncidentMarkers){		
		if(hseIncidentMarkers != null){
			this.hseIncidentMarkers = new ArrayList<String>();
			for(String value: hseIncidentMarkers){
				this.hseIncidentMarkers.add(value);
			}
		}
	}
	
	public void setNextHseIncidentMarkers(List<String> nextHseIncidentMarkers){		
		if(nextHseIncidentMarkers != null){
			this.nextHseIncidentMarkers = new ArrayList<String>();
			for(String value: nextHseIncidentMarkers){
				this.nextHseIncidentMarkers.add(value);
			}
		}
	}
	public void setBopTest(List<String> bopTest){		
		if(bopTest != null){
			this.bopTest = new ArrayList<String>();
			for(String value: bopTest){
				this.bopTest.add(value);
			}
		}
	}
	
	public void setNextBopTest(List<String> nextBopTest){		
		if(nextBopTest != null){
			this.nextBopTest = new ArrayList<String>();
			for(String value: nextBopTest){
				this.nextBopTest.add(value);
			}
		}
	}
	
	public void setHseIncidentSum(List<String> hseIncidentSum){		
		if(hseIncidentSum != null){
			this.hseIncidentSum = new ArrayList<String>();
			for(String value: hseIncidentSum){
				this.hseIncidentSum.add(value);
			}
		}
	}
	
	public List<String> getHseIncidentMarkers() {
		return this.hseIncidentMarkers;
	}

	public List<String> getNextHseIncidentMarkers() {
		return this.nextHseIncidentMarkers;
	}
	
	public List<String> getBopTest() {
		return this.bopTest;
	}
	
	public List<String> getNextBopTest() {
		return this.nextBopTest;
	}
	
	public List<String> getHseIncidentSum() {
		return this.hseIncidentSum;
	}
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
		if(this.getHseIncidentMarkers() != null)
		{
			for(String value:this.hseIncidentMarkers)
			{
				//get the current date from the user selection
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
				if (daily == null) return;
				Date todayDate = daily.getDayDate();
				String currentOperationUid;
				if (userContext.getUserSelection().getRigInformationUid() != null){
					currentOperationUid = userContext.getUserSelection().getOperationUid();
				
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime(todayDate);
					thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
					thisCalendar.set(Calendar.MINUTE, 59);
					thisCalendar.set(Calendar.SECOND , 59);
					thisCalendar.set(Calendar.MILLISECOND , 59);
					
					String strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) and operationUid = :currentOperationUid AND hseEventdatetime <= :currentDate AND incidentCategory = :hseIncidentMarkers and (supportVesselInformationUid is null or supportVesselInformationUid = '') ORDER BY hseEventdatetime DESC";
					String[] paramsFields2 = {"currentOperationUid", "currentDate","hseIncidentMarkers"};
					Object[] paramsValues2 = {currentOperationUid, thisCalendar.getTime(), value.toString()};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);		
					
					if (lstResult.size() > 0){
						HseIncident hseIncident = (HseIncident) lstResult.get(0);
						
						CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "daysSinceLta");
	
						String incidentCategory = hseIncident.getIncidentCategory();
						Long eventDateTime = hseIncident.getHseEventdatetime().getTime();
	
						double daysSinceHseIncident = CommonUtil.getConfiguredInstance().calculateDaysSinceHseIncident(daily.getDayDate(), hseIncident.getHseEventdatetime(), userContext.getUserSelection().getGroupUid());
						thisConverter.setBaseValue(daysSinceHseIncident);				
						Double daysSinceIncident = thisConverter.getConvertedValue();	
						
						ReportDataNode thisReportNode = reportDataNode.addChild("DaysSinceHseIncidentByOps");
						thisReportNode.addProperty("incidentCategory", incidentCategory);
						thisReportNode.addProperty("daysSinceHseIncident", daysSinceIncident.toString());			
						SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
						thisReportNode.addProperty("hseEventDateTime", df.format(hseIncident.getHseEventdatetime()));					
						thisReportNode.addProperty("hseEventDateTimeEpochMS", eventDateTime.toString());
					}
				}
			}
		}		
		
		if(this.getNextHseIncidentMarkers() != null){	
			
			for(String value:this.nextHseIncidentMarkers){
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
				if (daily == null) return;
				Date todayDate = daily.getDayDate();
				
				String currentOperationUid;
				if (userContext.getUserSelection().getRigInformationUid() != null){
					currentOperationUid = userContext.getUserSelection().getOperationUid();
				
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime(todayDate);
					thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
					thisCalendar.set(Calendar.MINUTE, 59);
					thisCalendar.set(Calendar.SECOND , 59);
					thisCalendar.set(Calendar.MILLISECOND , 59);
					
					String strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) and operationUid = :currentOperationUid AND hseEventdatetime > :currentDate AND incidentCategory = :hseIncidentMarkers and (supportVesselInformationUid is null or supportVesselInformationUid = '') ORDER BY hseEventdatetime ASC";
					String[] paramsFields2 = {"currentOperationUid", "currentDate","hseIncidentMarkers"};
					Object[] paramsValues2 = {currentOperationUid, thisCalendar.getTime(), value.toString()};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);		
					
					if (lstResult.size() > 0){
						HseIncident hseIncident = (HseIncident) lstResult.get(0);
						Long eventDateTime = hseIncident.getHseEventdatetime().getTime();			
						
						ReportDataNode thisReportNode = reportDataNode.addChild("NextDaysSinceHseIncidentByOps");
						thisReportNode.addProperty("incidentCategory", hseIncident.getIncidentCategory());		
						SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
						thisReportNode.addProperty("hseEventDateTime", df.format(hseIncident.getHseEventdatetime()));					
						thisReportNode.addProperty("hseEventDateTimeEpochMS", eventDateTime.toString());
					}
				}
			}					
		}
		
		if(this.getBopTest() != null){	
			
			for(String value:this.bopTest){
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
				if (daily == null) return;
				Date todayDate = daily.getDayDate();
				
				if (userContext.getUserSelection().getWellUid() != null){
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime(todayDate);
					thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
					thisCalendar.set(Calendar.MINUTE, 59);
					thisCalendar.set(Calendar.SECOND , 59);
					thisCalendar.set(Calendar.MILLISECOND , 59);
					
					String strSql = "FROM WellControlComponentTest WHERE (isDeleted = false or isDeleted is null) and wellUid = :wellUid AND testDatetime <= :currentDate AND testType = :testType ORDER BY testDatetime DESC";
					String[] paramsFields2 = {"wellUid", "currentDate", "testType"};
					Object[] paramsValues2 = {daily.getWellUid(), thisCalendar.getTime(), value.toString()};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);		
					
					if (lstResult.size() > 0){
						WellControlComponentTest wcc = (WellControlComponentTest) lstResult.get(0);
						Long testDateTime = wcc.getTestDatetime().getTime();			
						
						ReportDataNode thisReportNode = reportDataNode.addChild("BopTest");	
						SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
						thisReportNode.addProperty("testType", value.toString());
						thisReportNode.addProperty("testDateTime", df.format(wcc.getTestDatetime()));					
						thisReportNode.addProperty("testDateTimeEpochMS", testDateTime.toString());
						
					}
					
				}
			}
		}
		
		if(this.getNextBopTest() != null){	
			
			for(String value:this.nextBopTest){
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
				if (daily == null) return;
				Date todayDate = daily.getDayDate();
				
				if (userContext.getUserSelection().getWellUid() != null){
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime(todayDate);
					thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
					thisCalendar.set(Calendar.MINUTE, 59);
					thisCalendar.set(Calendar.SECOND , 59);
					thisCalendar.set(Calendar.MILLISECOND , 59);
					
					String strSql = "FROM WellControlComponentTest WHERE (isDeleted = false or isDeleted is null) and wellUid = :wellUid AND nextWellControlTestDatetime >= :currentDate AND testType = :testType ORDER BY nextWellControlTestDatetime ASC";
					String[] paramsFields2 = {"wellUid", "currentDate", "testType"};
					Object[] paramsValues2 = {daily.getWellUid(), thisCalendar.getTime(), value.toString()};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);		
					
					if (lstResult.size() > 0){
						WellControlComponentTest wcc = (WellControlComponentTest) lstResult.get(0);
						Long nextWellControlTestDatetime = wcc.getNextWellControlTestDatetime().getTime();			
						
						ReportDataNode thisReportNode = reportDataNode.addChild("NextBopTest");	
						SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
						thisReportNode.addProperty("testType", value.toString());
						thisReportNode.addProperty("nextWellControlTestDatetime", df.format(wcc.getNextWellControlTestDatetime()));					
						thisReportNode.addProperty("nextWellControlTestDatetimeEpochMS", nextWellControlTestDatetime.toString());
						
					}
					
				}
			}
		}
		
		if (this.getHseIncidentSum()!=null){
			
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
			if (daily == null) return;
			Date todayDate = daily.getDayDate();
			
			Calendar thisCalender = Calendar.getInstance();
			thisCalender.setTime(todayDate);
			thisCalender.add(Calendar.DATE, 1);
			thisCalender.add(Calendar.SECOND, -1);
			Date nextDate = thisCalender.getTime();
		
			for(String value:this.hseIncidentSum){			
				String strSql = "SELECT h.incidentCategory, SUM(h.numberOfIncidents) AS sumNumber FROM HseIncident h, Daily d " +
		                "WHERE (h.isDeleted = false OR h.isDeleted IS NULL) " +
		                "AND (d.isDeleted = false or d.isDeleted IS NULL) " +
		                "AND h.wellUid = :wellUid " +
		                "AND d.dailyUid = h.dailyUid " +
		                "AND h.hseEventdatetime >= :todayDate " + 
		                "AND h.hseEventdatetime <= :nextDate " + 
		                "AND h.incidentCategory = :incidentCategory";
	        
				String[] paramsField = {"todayDate", "nextDate", "wellUid", "incidentCategory"};
				Object[] paramsValue = {todayDate, nextDate, daily.getWellUid(), value};
			
				List<Object> lstDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
			
				if (!lstDaily.isEmpty()) {
					for (Object objDailyResult: lstDaily) {
						Object[] objDaily = (Object[]) objDailyResult;
						String dailyNumberOfIncidents = (objDaily[1] != null) ? objDaily[1].toString() : "0";
						ReportDataNode thisReportNode = reportDataNode.addChild("HseIncidentSum");	
						thisReportNode.addProperty("incidentCategory", value.toString());	
						thisReportNode.addProperty("dailyNumberOfIncidents", dailyNumberOfIncidents);
					}
				}
			}
		}
	}
}