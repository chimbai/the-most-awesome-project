package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.text.DecimalFormat;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class NPTByRootCauseReportDataGenerator extends DateRangeReportDataGenerator{
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	
	protected void generateActualData(UserContext userContext,Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		// TODO Auto-generated method stub
		
		//set the unit to false so always get raw value 
		Map<String,Double> objData = new LinkedHashMap<String,Double>();
		Double TotalDuration =0.0;
		
		DecimalFormat nf = new DecimalFormat("#0.00");	
		
		String WellUid  = userContext.getUserSelection().getWellUid();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);		
		
		String wellName="";
		String strSql="select wellName from Well where (isDeleted is null or isDeleted = False) and wellUid=:thisWellUid";
		String[] paramsFields = {"thisWellUid"};
		Object[] paramsValues = {WellUid};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size()>0){
			for(Object item : lstResult)
			{
				if (item != null)
				{
					wellName = String.valueOf(item);
				}
			}
		}			
		ReportDataNode thisReportNode = reportDataNode.addChild("Rig");
		thisReportNode.addProperty("wellName", wellName);
		
		paramsFields = new String[]{"reportStartDate","reportEndDate","thisWellUid"};
		paramsValues = new Object[] {startDate,endDate,WellUid};
		
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
		
		//OPSNPTBKD
		 strSql = "select sum(a.activityDuration) as duration from Activity a,Operation b,Well c,Wellbore d,Daily e where a.operationUid=b.operationUid and " +
		                "b.wellboreUid=d.wellboreUid and d.wellUid=c.wellUid and a.dailyUid=e.dailyUid and (a.isDeleted is null or a.isDeleted = False) and " +
		                "(b.isDeleted is null or b.isDeleted = False) and (c.isDeleted is null or c.isDeleted = False) and (d.isDeleted is null or d.isDeleted = False) " +
		                "and (e.isDeleted is null or e.isDeleted = False) and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) " +
						"and a.startDatetime >=:reportStartDate and a.endDatetime <=:reportEndDate and a.wellUid=:thisWellUid and a.classCode='P'" ;
		
		TotalDuration += generateOPSNPTBKDData("OPS",objData,strSql,paramsFields,paramsValues,qp,nf);
		
	    strSql = "select sum(a.activityDuration) as duration from Activity a,Operation b,Well c,Wellbore d,Daily e where a.operationUid=b.operationUid and " +
        "b.wellboreUid=d.wellboreUid and d.wellUid=c.wellUid and a.dailyUid=e.dailyUid and (a.isDeleted is null or a.isDeleted = False) and (b.isDeleted is null or b.isDeleted = False) and " +
		"(c.isDeleted is null or c.isDeleted = False) and (d.isDeleted is null or d.isDeleted = False) and (e.isDeleted is null or e.isDeleted = False) and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) " +
		"and a.startDatetime >=:reportStartDate and a.endDatetime <=:reportEndDate and a.wellUid=:thisWellUid and a.classCode='NPT' and a.rootCauseCode in ('WEL','WOV','WOW','WPM','WWP') " ;

	    TotalDuration += generateOPSNPTBKDData("NPT",objData,strSql,paramsFields,paramsValues,qp,nf);		
	
		 strSql = "select sum(a.activityDuration) as duration from Activity a,Operation b,Well c,Wellbore d,Daily e where a.operationUid=b.operationUid and " +
	     "b.wellboreUid=d.wellboreUid and d.wellUid=c.wellUid and a.dailyUid=e.dailyUid and (a.isDeleted is null or a.isDeleted = False) and (b.isDeleted is null or b.isDeleted = False) and " +
		 "(c.isDeleted is null or c.isDeleted = False) and (d.isDeleted is null or d.isDeleted = False) and (e.isDeleted is null or e.isDeleted = False) and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) " +
		 "and a.startDatetime >=:reportStartDate and a.endDatetime <=:reportEndDate and a.wellUid=:thisWellUid and a.classCode='NPT' and a.rootCauseCode in ('B3P','BRH','BRT','BSV') " ;

		 TotalDuration += generateOPSNPTBKDData("BKD",objData,strSql,paramsFields,paramsValues,qp,nf);
		 
		 ReportDataNode thisReportSummaryNode = reportDataNode.addChild("ReportSummary");
		 thisReportSummaryNode.addProperty("OPSNPTBKD", nf.format(TotalDuration/3600.0));	//in hour				
		 
		 thisReportNode = reportDataNode.addChild("OPSNPTBKD");
		if (objData != null)
		{				
			for (Map.Entry<String, Double> entry : objData.entrySet())
			{	
				String Category = entry.getKey();
				Double duration = (Double) entry.getValue();
				
				ReportDataNode thisDataNode = thisReportNode.addChild("Data");
				thisDataNode.addProperty("category", Category);
				thisConverter.setBaseValue(duration);
				thisDataNode.addProperty("duration", String.valueOf(thisConverter.getConvertedValue()));
				thisDataNode.addProperty("uomlabel", thisConverter.getUomLabel());				
				thisDataNode.addProperty("Percentage", nf.format((Double.parseDouble(nf.format(duration))/TotalDuration) * 100));
			}
		}
		
		strSql = "select a.rootCauseCode,d.name,sum(a.activityDuration) as duration from Activity a,Operation b,Well c,LookupRootCauseCode d,Wellbore e,Daily f where a.operationUid=b.operationUid and " +
        "b.wellboreUid=e.wellboreUid and e.wellUid=c.wellUid and a.dailyUid=f.dailyUid and a.rootCauseCode = d.shortCode and (a.isDeleted is null or a.isDeleted = False) and (b.isDeleted is null or b.isDeleted = False) and " +
		"(c.isDeleted is null or c.isDeleted = False) and (d.isDeleted is null or d.isDeleted = False) and (e.isDeleted is null or e.isDeleted = False) and (f.isDeleted is null or f.isDeleted = False) and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) " +
		"and a.startDatetime >=:reportStartDate and a.endDatetime <=:reportEndDate and a.wellUid=:thisWellUid and a.classCode='NPT' and a.rootCauseCode in ('WEL','WOV','WOW','WPM','WWP') group by a.rootCauseCode,d.name" ;
		
		//strSql = "select a.rootCauseCode,d.Name,sum(a.activityDuration) as duration from Activity a,Operation b,Well c,LookupRootCauseCode d where a.operationUid=b.operationUid and " +
        //"b.wellUid=c.wellUid and a.rootCauseCode = d.shortCode and (a.isDeleted is null or a.isDeleted = False) and (b.isDeleted is null or b.isDeleted = False) and " +
		//"(c.isDeleted is null or c.isDeleted = False) and (d.isDeleted is null or d.isDeleted = False) and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) " +
		//"and a.startDatetime >=:reportStartDate and a.endDatetime <=:reportEndDate and a.wellUid=:thisWellUid and a.classCode='NPT' and a.rootCauseCode in ('WEL','WOV','WOW','WPM','WWP') group by a.rootCauseCode,d.name" ;
		
		TotalDuration =0.0;
		objData = new LinkedHashMap<String,Double>();
		TotalDuration = generateNPTByRootCause(objData,strSql,paramsFields,paramsValues,qp,nf);
		
		thisReportSummaryNode.addProperty("NPT", nf.format(TotalDuration/3600.0));	//in hour	
		
		thisReportNode = reportDataNode.addChild("NPT");
		if (objData != null)
		{				
			for (Map.Entry<String, Double> entry : objData.entrySet())
			{	
				String Category = entry.getKey();
				Double duration = (Double) entry.getValue();
				
				ReportDataNode thisDataNode = thisReportNode.addChild("Data");
				thisDataNode.addProperty("category", Category);
				
				thisConverter.setBaseValue(duration);
				thisDataNode.addProperty("duration", String.valueOf(thisConverter.getConvertedValue()));
				thisDataNode.addProperty("uomlabel", thisConverter.getUomLabel());	
				thisDataNode.addProperty("Percentage", nf.format((Double.parseDouble(nf.format(duration))/TotalDuration) * 100));
			}
		}
		
		strSql = "select a.rootCauseCode,d.name,sum(a.activityDuration) as duration from Activity a,Operation b,Well c,LookupRootCauseCode d,Wellbore e,Daily f where a.operationUid=b.operationUid and " +
        "b.wellboreUid=e.wellboreUid and e.wellUid=c.wellUid and a.dailyUid=f.dailyUid and a.rootCauseCode = d.shortCode and (a.isDeleted is null or a.isDeleted = False) and (b.isDeleted is null or b.isDeleted = False) and " +
		"(c.isDeleted is null or c.isDeleted = False) and (d.isDeleted is null or d.isDeleted = False) and (e.isDeleted is null or e.isDeleted = False) and (f.isDeleted is null or f.isDeleted = False) and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) " +
		"and a.startDatetime >=:reportStartDate and a.endDatetime <=:reportEndDate and a.wellUid=:thisWellUid and a.classCode='NPT' and a.rootCauseCode in ('B3P','BRH','BRT','BSV') group by a.rootCauseCode,d.name" ;
		
		//strSql = "select a.rootCauseCode,d.Name,sum(a.activityDuration) as duration from Activity a,Operation b,Well c,LookupRootCauseCode d where a.operationUid=b.operationUid and " +
        //"b.wellUid=c.wellUid and a.rootCauseCode = d.shortCode and (a.isDeleted is null or a.isDeleted = False) and (b.isDeleted is null or b.isDeleted = False) and " +
		//"(c.isDeleted is null or c.isDeleted = False) and (d.isDeleted is null or d.isDeleted = False) and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) " +
		//"and a.startDatetime >=:reportStartDate and a.endDatetime <=:reportEndDate and a.wellUid=:thisWellUid and a.classCode='NPT' and a.rootCauseCode in ('WEL','WOV','WOW','WPM','WWP') group by a.rootCauseCode,d.name" ;
		
		TotalDuration =0.0;
		objData = new LinkedHashMap<String,Double>();
		TotalDuration = generateNPTByRootCause(objData,strSql,paramsFields,paramsValues,qp,nf);
		
		thisReportSummaryNode.addProperty("BKD", nf.format(TotalDuration/3600.0));	//in hour	
		
		thisReportNode = reportDataNode.addChild("BKD");
		if (objData != null)
		{				
			for (Map.Entry<String, Double> entry : objData.entrySet())
			{	
				String Category = entry.getKey();
				Double duration = (Double) entry.getValue();
				
				ReportDataNode thisDataNode = thisReportNode.addChild("Data");
				thisDataNode.addProperty("category", Category);
				
				thisConverter.setBaseValue(duration);
				thisDataNode.addProperty("duration", String.valueOf(thisConverter.getConvertedValue()));
				
				thisDataNode.addProperty("uomlabel", thisConverter.getUomLabel());	
				thisDataNode.addProperty("Percentage", nf.format((Double.parseDouble(nf.format(duration))/TotalDuration) * 100));
			}
		}
		
		strSql="select b.operationName, sum(a.activityDuration) as duration from Activity a,Operation b,Well c,Wellbore d,Daily e where a.operationUid=b.operationUid " +
		       "and b.wellboreUid=d.wellboreUid and d.wellUid=c.wellUid and a.dailyUid=e.dailyUid and (a.isDeleted is null or a.isDeleted = False) and (b.isDeleted is null or b.isDeleted = False) and (c.isDeleted is null or c.isDeleted = False) " +
		       "and (d.isDeleted is null or d.isDeleted = False) and (e.isDeleted is null or e.isDeleted = False) and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='') and a.startDatetime >=:reportStartDate and a.endDatetime <=:reportEndDate and a.wellUid=:thisWellUid " +
		       "group by b.operationName";
		
		TotalDuration =0.0;
		Map<String,Double> objSummaryData = new LinkedHashMap<String,Double>();
		TotalDuration = generateSummaryActivity(objSummaryData,strSql,paramsFields,paramsValues,qp,nf);
		
		thisReportSummaryNode.addProperty("OpsSummary", nf.format(TotalDuration/3600.0));	//in hour
		
		thisReportNode = reportDataNode.addChild("OpsSummary");
		if (objData != null)
		{						
			
			for (Map.Entry<String, Double> entry : objSummaryData.entrySet())
			{	
				String operationName = entry.getKey();
				Double duration= (Double) entry.getValue();				
							
				ReportDataNode thisDataNode = thisReportNode.addChild("Data");
				thisDataNode.addProperty("wellName", wellName);
				thisDataNode.addProperty("operationName", operationName);
				
				thisConverter.setBaseValue(duration);
				thisDataNode.addProperty("duration", String.valueOf(thisConverter.getConvertedValue()));
				thisDataNode.addProperty("uomlabel", thisConverter.getUomLabel());	
				
				thisDataNode.addProperty("Percentage", nf.format((Double.parseDouble(nf.format(duration))/TotalDuration) * 100));
			}
		}
	}
	
	protected Double generateOPSNPTBKDData(String Category,Map<String,Double> objData,String Sql,String[] paramsFields,Object[] paramsValues,QueryProperties qp,DecimalFormat nf) throws Exception
	{	
		Double duration=0.0;
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(Sql, paramsFields, paramsValues, qp);
		if (lstResult.size()>0){
			for(Object item : lstResult)
			{
				if (item != null)
				{
					duration = 0.0;		
					duration = Double.parseDouble(String.valueOf(item));					
					
					objData.put(Category, duration);		
					
				}	
			}
		}	
		
		return Double.parseDouble(nf.format(duration));
	}
	
	protected Double generateNPTByRootCause(Map<String,Double> objNode,String Sql,String[] paramsFields,Object[] paramsValues,QueryProperties qp,DecimalFormat nf) throws Exception
	{
		Double totalduration = 0.0;
		Double duration = 0.0;
		String rootCauseCode="";
		String rootCauseDesc="";
		
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(Sql, paramsFields, paramsValues, qp);
		if (lstResult.size()>0){
			for(Object[] item : lstResult)
			{
				rootCauseCode = item[0].toString();
				rootCauseDesc = item[1].toString();
				
				duration = 0.0;		
				duration = Double.parseDouble(String.valueOf(item[2]));				
					
				objNode.put(rootCauseCode + " - " + rootCauseDesc , duration);	
				totalduration += Double.parseDouble(nf.format(duration));
			}	
		}
		
		return totalduration;
	}	
	
	protected Double generateSummaryActivity(Map<String,Double> objNode,String Sql,String[] paramsFields,Object[] paramsValues,QueryProperties qp,DecimalFormat nf) throws Exception
	{
		Double totalduration = 0.0;
		Double duration = 0.0;
		
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(Sql, paramsFields, paramsValues, qp);
		if (lstResult.size()>0){
			for(Object[] item : lstResult)
			{				
				duration = 0.0;		
				duration = Double.parseDouble(String.valueOf(item[1]));				
					
				objNode.put(item[0].toString() , duration);	
				totalduration += Double.parseDouble(nf.format(duration));
			}	
		}
		
		return totalduration;
	}	
}

