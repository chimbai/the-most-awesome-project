package com.idsdatanet.d2.drillnet.kick;

import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.web.mvc.*;
import com.idsdatanet.d2.core.model.*;
import com.idsdatanet.d2.core.uom.*;

public class DataNodeListener extends EmptyDataNodeListener {
	private Boolean wellboreBased = false;
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		if(node.getData() instanceof Kick){
			this.linkActivityInfo((Kick) node.getData(), node);
		}
	}
	
	private void linkActivityInfo(Kick kick, CommandBeanTreeNode node) throws Exception {
		if(kick == null) return;
		if(kick.getActivityUid() == null) return;
		
		KickLinkedInfo linkedInfo = new KickLinkedInfo(kick);
		
		Activity activity = linkedInfo.getLinkedActivity();
		if(activity != null){
			if(activity.getDepthMdMsl() != null){
				CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(), Activity.class, "depthMdMsl");
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@bitDepth", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("bitDepth", activity.getDepthMdMsl());
				//node.getDynaAttr().put("bitDepth", CustomFieldUom.format(node.getCommandBean(), activity.getDepthMdMsl(), Activity.class, "depthMdMsl"));
			}
		}
		
		node.getDynaAttr().put("formationName", linkedInfo.getFormationName(wellboreBased));
		
		linkedInfo.dispose();
	}

	public void setWellboreBased(boolean wellboreBased) {
		this.wellboreBased = wellboreBased;
	}

	public  boolean isWellboreBased() {
		return wellboreBased;
	}
}
