package com.idsdatanet.d2.drillnet.personnelonsite;

import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.model.Daily;

public class PersonnelByCompanyToDateReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		
		String thisOperationUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getOperationUid().toString())) thisOperationUid = userContext.getUserSelection().getOperationUid().toString();
		
		String strSql = "SELECT p.crewCompany, sum(p.pax) as totalPax FROM PersonnelOnSite p, Daily d WHERE p.dailyUid = d.dailyUid " +
					    "AND (d.isDeleted IS NULL or d.isDeleted = false) AND (p.isDeleted IS NULL or p.isDeleted = false) " +
					    "AND p.operationUid = :thisOperationUid AND d.dayDate <= :userDate GROUP BY p.crewCompany";
		String[] paramsFields = {"userDate", "thisOperationUid"};
		Object[] paramsValues = {todayDate, thisOperationUid};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (lstResult.size() > 0){	
			for(Object objResult: lstResult){
				Object[] objDuration = (Object[]) objResult;
				
				String crewCompany = "";
				String totalPax = "";
				
				if(objDuration[0] != null) crewCompany = objDuration[0].toString();
				if(objDuration[1] != null) totalPax = objDuration[1].toString();
				
				if(StringUtils.isNotBlank(this.getCompanyName(crewCompany))) crewCompany = this.getCompanyName(crewCompany);
				
				ReportDataNode thisReportNode = reportDataNode.addChild("PersonnelByCompany");
				thisReportNode.addProperty("crewCompany", crewCompany);
				thisReportNode.addProperty("totalPax", totalPax);
			}

		}
	}
	
	private String getCompanyName(String companyUid) throws Exception{
		String[] paramsFields = {"companyUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = companyUid;
		
		String strSql = "SELECT companyName FROM LookupCompany WHERE (isDeleted = false or isDeleted is null) and lookupCompanyUid =:companyUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (!lstResult.isEmpty())
		{
			Object thisResult = (Object) lstResult.get(0);
			
			if (thisResult != null) {
				return thisResult.toString();
			}else {
				return null;
			}
		}else {
			return null;
		}

	}
}