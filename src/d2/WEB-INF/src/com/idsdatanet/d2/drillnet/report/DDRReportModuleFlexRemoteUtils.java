package com.idsdatanet.d2.drillnet.report;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.view.FreemarkerUtils;

public class DDRReportModuleFlexRemoteUtils extends BlazeRemoteClassSupport{
	
	private Date timeReportFileCreated = null;
	
	public String getDDRReportFileList(String dailyUid, String operationUid, String reportType) {
		String result = "<root/>";
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		if (StringUtils.isBlank(dailyUid) || StringUtils.isBlank(operationUid) || StringUtils.isBlank(reportType)) return result;
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			// Populate DailyHoursUsedLog listing for loading in grid view
			String queryString = "SELECT b.reportFilesUid, b.isPrivate, b.displayName, b.reportUserUid, b.reportFile " +
								 "FROM ReportFiles b " +
								 "WHERE (b.isDeleted=false OR b.isDeleted is null) " +
								 "AND b.dailyUid = :dailyUid " +
								 "AND b.reportOperationUid = :operationUid " +
								 "AND b.reportType = :reportType ORDER BY b.dateGenerated";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"dailyUid", "operationUid", "reportType"}, new Object[] {dailyUid, operationUid, reportType});
			if (lstResult.size() > 0){
				for(Object objResult: lstResult){
					
					//due to result is in array, cast the result to array first and read 1 by 1
					Object[] obj = (Object[]) objResult;
					
					String reportFilesUid = "";
					String isPrivate = "";
					String displayName = "";
					String reportUserUid = "";
					String reportFile = "";
									
					if (obj[0] != null && StringUtils.isNotBlank(obj[0].toString())) reportFilesUid = obj[0].toString();
					if (obj[1] != null && StringUtils.isNotBlank(obj[1].toString())) isPrivate = obj[1].toString();
					if (obj[2] != null && StringUtils.isNotBlank(obj[2].toString())) displayName = obj[2].toString();
					if (obj[3] != null && StringUtils.isNotBlank(obj[3].toString())) reportUserUid = obj[3].toString();
					if (obj[4] != null && StringUtils.isNotBlank(obj[4].toString())) reportFile = obj[4].toString();
					
					File f = new File(ReportUtils.getFullOutputFilePath( reportFile ) );
					if( f.exists() ){
						this.timeReportFileCreated = new Date( f.lastModified());
						
						// Write the attributes into record
						SimpleAttributes attr = new SimpleAttributes();
						attr.addAttribute("reportFilesUid",reportFilesUid);
						attr.addAttribute("isPrivate", isPrivate);
						attr.addAttribute("displayName", displayName);
						attr.addAttribute("reportUserUid", getUserName( reportUserUid ) );
						attr.addAttribute("newlyGeneratedFlag", String.valueOf( getNewlyGeneratedFlag() ) );
						attr.addAttribute("timeReportFileCreatedText", String.valueOf( getTimeReportFileCreatedText() ) );
											
						//write the element record into the DDRReportFile
						writer.addElement("DDRReportFile", "", attr);
					}					
				}
			}
			
			writer.close();
			result = bytes.toString();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return "<root/>";
		}		
	}
	
	public boolean updateDDRReportFile( String reportFilesUid, Boolean isPrivate ) {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		try {					
			String strSql1 = "UPDATE ReportFiles SET isPrivate = :isPrivate WHERE reportFilesUid =:reportFilesUid";
			String[] paramsFields1 = {"reportFilesUid", "isPrivate"};
			Object[] paramsValues1 = {reportFilesUid, isPrivate};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Boolean delDDRReportFile(String reportFilesUid) {
		
		try {
			if(reportFilesUid != null){	
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);		
					
				String strSql1 = "FROM ReportFiles WHERE reportFilesUid =:reportFilesUid";
				String[] paramsFields1 = {"reportFilesUid"};
				Object[] paramsValues1 = {reportFilesUid};
				List<Object>lst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
				
				if( lst.size() > 0 ){
					ReportFiles reportFile = ( ReportFiles ) lst.get(0);
					this.delete( reportFile.getReportFilesUid(), reportFile.getReportFile());

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean getNewlyGeneratedFlag(){
		if(this.timeReportFileCreated == null) return false;
		return FreemarkerUtils.isDocumentNewlyGenerated(this.timeReportFileCreated);
	}
	
	public String getTimeReportFileCreatedText(){
		if(this.timeReportFileCreated == null) return null;
		return FreemarkerUtils.formatDurationSince(this.timeReportFileCreated);
	}
	
	private String getUserName(String userUid) throws Exception {
		User user = ApplicationUtils.getConfiguredInstance().getCachedUser(userUid);
		if(user == null) return "";
		if(user.getFname()!=null)
		{
			return user.getFname();
		}
		return user.getUserName();
	}
	
	protected final File getReportOutputFile(ReportFiles dbFile) throws Exception {
		return new File(ReportUtils.getFullOutputFilePath(dbFile.getReportFile()));
	}
	
	private void delete(String reportFilesUid, String reportFilePath) throws Exception {
		
		if (StringUtils.isNotBlank(reportFilePath)) {
			File reportFile = new File(ReportUtils.getFullOutputFilePath(reportFilePath));
			if(reportFile.exists()) reportFile.delete();
		}
		
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update ReportFiles set isDeleted = true where reportFilesUid = :reportFilesUid", new String[] { "reportFilesUid" }, new Object[] { reportFilesUid });
	}	
	
}
