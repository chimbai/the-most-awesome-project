package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @twlau
 * This class help to mine data for latest 7 days data
 * for 24 hr summary on report daily screen
 * 
 */

public class CumCostToDateWellReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date endDate = daily.getDayDate();
		String thisWellUid = daily.getWellUid();
		String thisOpsUid = daily.getOperationUid();
		CustomFieldUom costConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "daycost");
		
		Double totalCost = 0.0;

		String strSql = "FROM Operation WHERE (isDeleted = false or isDeleted is null) and wellUid = :thisWellUid";
		String[] paramsFields = {"thisWellUid"};
		Object[] paramsValues= {thisWellUid};
		List<Operation> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			for (Operation ops :lstResult){
				String currentOps = ops.getOperationUid();
				String queryString = "FROM Daily where (isDeleted=false or isDeleted is null) and operationUid=:operationUid AND dayDate<=:endDate order by dayDate desc";
				String[] paramsFields2 = {"operationUid", "endDate"};
				Object[] paramsValues2= {currentOps, endDate};
				List<Daily> dailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramsFields2, paramsValues2);
				if (dailyList.size()>0) {
					Double cumcost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(dailyList.get(0).getDailyUid(), null);
					if (cumcost!=null) totalCost += cumcost;
				}	
			}
		}
		costConverter.setBaseValue(totalCost);
		totalCost = costConverter.getConvertedValue();
		ReportDataNode thisReportNode = reportDataNode.addChild("CumCostWellToDate");
		thisReportNode.addProperty("cumCostWellToDate", totalCost.toString());
		
	}
	
}
