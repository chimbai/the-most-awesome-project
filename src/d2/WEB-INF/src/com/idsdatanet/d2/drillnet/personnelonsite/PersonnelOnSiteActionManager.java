package com.idsdatanet.d2.drillnet.personnelonsite;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.PersonnelOnSite;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;


public class PersonnelOnSiteActionManager implements ActionManager, DataNodeLoadHandler  {

	private boolean newdaycreated = false;

	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		if (meta.getTableClass().equals(ImportPOB.class)) return true;
		else return false;
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{		
		
		if (StringUtils.isBlank((String)commandBean.getRoot().getDynaAttr().get("importOption"))) {
			commandBean.getRoot().getDynaAttr().put("importOption", "append_pob");
		}
		
		return null;
	}
	
	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if(com.idsdatanet.d2.core.web.mvc.Action.SAVE.equals(action)){
			return new SaveImportPOBActionHandler();
			
		}else{
			return null;
		}
	}
	
	private class SaveImportPOBActionHandler implements ActionHandler{
		
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception{
			ImportPOB importPob = (ImportPOB) node.getData();
			String importOption = (String)commandBean.getRoot().getDynaAttr().get("importOption");
			if (importPob !=null) {
				if (importPob.getDayDate() != null) {
					String strSql = "Select dailyUid FROM Daily WHERE (isDeleted = false or isDeleted is null) and dayDate =:dayDate and operationUid =:operationUid";
					String[] paramsFields = {"dayDate", "operationUid"};
					Object[] paramsValues = {importPob.getDayDate(), userSession.getCurrentOperationUid()};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					PersonnelOnSite pob = new PersonnelOnSite();
					if (lstResult.size() > 0){
						newdaycreated = false;
						if ("overwrite_pob".equals(importOption)) {
							String strSql1 = "update PersonnelOnSite set isDeleted = true where dailyUid =:dailyUid and operationUid =:operationUid";
							String[] paramsFields1 = {"dailyUid", "operationUid"};
							Object[] paramsValues1 = {lstResult.get(0).toString(), userSession.getCurrentOperationUid()};
							ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1);
						}
						pob.setDailyUid(lstResult.get(0).toString());	
					}else {
						//create new daily
						Daily thisDaily = CommonUtil.getConfiguredInstance().createNewDaily(importPob.getDayDate(), userSession.getCurrentOperationUid(), userSession, request);
					
						newdaycreated = true;
						pob.setDailyUid(thisDaily.getDailyUid());
					}
					
					pob.setOperationUid(userSession.getCurrentOperationUid());
					pob.setSequence(importPob.getSequence());
					pob.setCrewCompany(importPob.getCrewCompany());
					pob.setPax(importPob.getPax());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(pob);
					commandBean.getSystemMessage().addWarning("<b>Personnel data imported successfully.</b>" ,true, true);
					if (newdaycreated) {
						commandBean.getSystemMessage().addWarning("The following days have also been automatically created: ", true, true);
						SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
						commandBean.getSystemMessage().addWarning("<b>" + df.format(importPob.getDayDate()) + "</b>" ,true, true);
					}
			
				}
			}
			//
			return new ActionHandlerResponse(true);
		}
	}
}
