package com.idsdatanet.d2.drillnet.partnerportal;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.dataservices.blazeds.BlazeRemoteClassSupport;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.FileManagerFiles;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.filemanager.FileManagerUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.view.FreemarkerUtils;
import com.idsdatanet.d2.drillnet.well.HierarchicalAccessibleOperationLookupHandler;

public class PartnerPortalFlexUtil extends BlazeRemoteClassSupport{

	SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat longdate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
	
	public String loadLookups() {
		String xml = "<root/>";
		try {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			String latestOperationUid = ApplicationUtils.getConfiguredInstance().getLatestAccessibleDrillNetOperation(this.getCurrentUserSession());
			writer.addElement("latestOperationUid", latestOperationUid);
			//collect operations
			SimpleAttributes atts = new SimpleAttributes();
			atts.addAttribute("key", "");
			atts.addAttribute("label", "");
			writer.addElement("operationList", "", atts);
			UserSelectionSnapshot userSelection = new  UserSelectionSnapshot();
			HierarchicalAccessibleOperationLookupHandler lookupHandler = new HierarchicalAccessibleOperationLookupHandler();
			Map<String, LookupItem> lookup = lookupHandler.getLookup(null, null, userSelection, this.getCurrentHttpRequest(), new LookupCache());
			for (Map.Entry<String, LookupItem> entry : lookup.entrySet()) {
				String key = entry.getValue().getKey();
				String label = (String) entry.getValue().getValue();
				atts = new SimpleAttributes();
				atts.addAttribute("key", key);
				atts.addAttribute("label", label);
				writer.addElement("operationList", "", atts);
			}
			
			//collect file extension
			atts = new SimpleAttributes();
			atts.addAttribute("key", "");
			atts.addAttribute("label", "All File Types");
			writer.addElement("fileExtension", "", atts);
			lookup = LookupManager.getConfiguredInstance().getLookup("xml://fileManager.fileType?key=key&value=label&cache=true", userSelection, new LookupCache());
			for (Map.Entry<String, LookupItem> entry : lookup.entrySet()) {
				String key = entry.getValue().getKey();
				String label = (String) entry.getValue().getValue();
				atts = new SimpleAttributes();
				atts.addAttribute("key", key);
				atts.addAttribute("label", label);
				writer.addElement("fileExtension", "", atts);
			}
			
			//collect category
			atts = new SimpleAttributes();
			atts.addAttribute("key", "");
			atts.addAttribute("label", "All Files");
			writer.addElement("category", "", atts);
			lookup = LookupManager.getConfiguredInstance().getLookup("xml://fileManager.category?key=key&value=label&cache=true", userSelection, new LookupCache());
			for (Map.Entry<String, LookupItem> entry : lookup.entrySet()) {
				String key = entry.getValue().getKey();
				String label = (String) entry.getValue().getValue();
				atts = new SimpleAttributes();
				atts.addAttribute("key", key);
				atts.addAttribute("label", label);
				writer.addElement("category", "", atts);
			}
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
			xml = "<root/>";
		}
		return xml;
	}
	
	public String loadFiles(String operationUid, String dateStr, String category, String fileExtension, String keywords, String fileName) {
		String xml = "<root/>";
		try {
			UserSelectionSnapshot userSelection = new  UserSelectionSnapshot();
			Map<String, LookupItem> lookup = LookupManager.getConfiguredInstance().getLookup("xml://fileManager.fileType?key=key&value=label&cache=true", userSelection, new LookupCache());
			String otherExtensionCondition = "";
			for (Map.Entry<String, LookupItem>item:lookup.entrySet()) {
				if (!"OTHER".equals(item)) {
					otherExtensionCondition += ("".equals(otherExtensionCondition)?"":" AND ") + " reportFile not like '%." + item.getKey() + "' ";
				}
			}
			
			QueryProperties queryProperties = new QueryProperties();
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			SimpleXmlWriter writer = new SimpleXmlWriter(bytes);
			writer.startElement("root");
			
			Date date = null;
			if (StringUtils.isNotBlank(dateStr)) {
				SimpleDateFormat sqlDate = new SimpleDateFormat("yyyy-MM-dd");
				date = sqlDate.parse(dateStr);
			}
			String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
			String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), operationUid);
			//DDR
			List<String>paramNames = new ArrayList();
			List<Object>values = new ArrayList();
			String queryString = "FROM ReportFiles " +
					"WHERE (isDeleted = false or isDeleted is null) " +
					"and reportOperationUid=:operationUid " +
					"AND reportType=:reportType " +
					"AND isPrivate = false " +
					(date!=null?"AND reportDayDate=:reportDate ":"") +
					(StringUtils.isNotBlank(fileExtension) && !"OTHER".equals(fileExtension)?"AND reportFile like :extension ":"") +
					(StringUtils.isNotBlank(fileExtension) && "OTHER".equals(fileExtension) && !"".equals(otherExtensionCondition)?"AND (" + otherExtensionCondition + ") ":"") +
					(StringUtils.isNotBlank(fileName)?"AND (displayName like :fileName or reportType like :fileName or reportFile like :fileName) ":"") +
					(StringUtils.isNotBlank(category) || StringUtils.isNotBlank(keywords)?"AND reportFilesUid='x' ":"") + 
					"ORDER BY reportDayDate";
			paramNames.add("operationUid");values.add(operationUid);
			paramNames.add("reportType");values.add(reportType);
			if (date!=null) {paramNames.add("reportDate");values.add(date);}
			if (StringUtils.isNotBlank(fileExtension) && !"OTHER".equals(fileExtension)) {paramNames.add("extension");values.add("%."+fileExtension);}
			if (StringUtils.isNotBlank(fileName)) {paramNames.add("fileName");values.add("%"+fileName+"%");}
			List<ReportFiles> reportFiles = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, values, queryProperties);
			for (ReportFiles file : reportFiles) {
				SimpleAttributes atts = this.getFileNode(file, operationName, "GROUP_A", "Daily Drilling Report", reportType);
				if (StringUtils.isNotBlank(fileName) && atts.getValue("displayName").toUpperCase().indexOf(fileName.toUpperCase())<0) continue;
				if (atts!=null) writer.addElement("file", "", atts);
			}

			//DGR
			paramNames.clear();
			values.clear();
			queryString = "FROM ReportFiles " +
					"WHERE (isDeleted = false or isDeleted is null) " +
					"and reportOperationUid=:operationUid " +
					"AND reportType='DGR' " +
					"AND isPrivate = false " +
					(date!=null?"AND reportDayDate=:reportDate ":"") +
					(StringUtils.isNotBlank(fileExtension) && !"OTHER".equals(fileExtension)?"AND reportFile like :extension ":"") +
					(StringUtils.isNotBlank(fileExtension) && "OTHER".equals(fileExtension) && !"".equals(otherExtensionCondition)?"AND (" + otherExtensionCondition + ") ":"") +
					(StringUtils.isNotBlank(fileName)?"AND (displayName like :fileName or reportType like :fileName or reportFile like :fileName) ":"") +
					(StringUtils.isNotBlank(category) || StringUtils.isNotBlank(keywords)?"AND reportFilesUid='x' ":"") + 
					"ORDER BY reportDayDate";
			paramNames.add("operationUid");values.add(operationUid);
			if (date!=null) {paramNames.add("reportDate");values.add(date);}
			if (StringUtils.isNotBlank(fileExtension) && !"OTHER".equals(fileExtension)) {paramNames.add("extension");values.add("%."+fileExtension);}
			if (StringUtils.isNotBlank(fileName)) {paramNames.add("fileName");values.add("%"+fileName+"%");}
			reportFiles = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, values, queryProperties);
			for (ReportFiles file : reportFiles) {
				SimpleAttributes atts = this.getFileNode(file, operationName, "GROUP_B", "Daily Geology Report", "DGR");
				if (StringUtils.isNotBlank(fileName) && atts.getValue("displayName").toUpperCase().indexOf(fileName.toUpperCase())<0) continue;
				if (atts!=null) writer.addElement("file", "", atts);
			}
			
			//Other Reports
			paramNames.clear();
			values.clear();
			queryString = "FROM ReportFiles " +
					"WHERE (isDeleted = false or isDeleted is null) " +
					"and reportOperationUid=:operationUid " +
					"AND (reportType <> :reportType AND reportType <> 'DGR' AND reportType <> 'MGT' AND reportType <> 'MGT_GEO') " +
					"AND isPrivate = false " +
					(date!=null?"AND reportDayDate=:reportDate ":"") +
					(StringUtils.isNotBlank(fileExtension) && !"OTHER".equals(fileExtension)?"AND reportFile like :extension ":"") +
					(StringUtils.isNotBlank(fileExtension) && "OTHER".equals(fileExtension) && !"".equals(otherExtensionCondition)?"AND (" + otherExtensionCondition + ") ":"") +
					(StringUtils.isNotBlank(fileName)?"AND (displayName like :fileName or reportType like :fileName or reportFile like :fileName) ":"") +
					(StringUtils.isNotBlank(category) || StringUtils.isNotBlank(keywords)?"AND reportFilesUid='x' ":"") + 
					"ORDER BY reportDayDate";
			paramNames.add("operationUid");values.add(operationUid);
			paramNames.add("reportType");values.add(reportType);
			if (date!=null) {paramNames.add("reportDate");values.add(date);}
			if (StringUtils.isNotBlank(fileExtension) && !"OTHER".equals(fileExtension)) {paramNames.add("extension");values.add("%."+fileExtension);}
			if (StringUtils.isNotBlank(fileName)) {paramNames.add("fileName");values.add("%"+fileName+"%");}
			reportFiles = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, values, queryProperties);
			for (ReportFiles file : reportFiles) {
				SimpleAttributes atts = this.getFileNode(file, operationName, "GROUP_C", "Other Reports", "OTHER");
				if (StringUtils.isNotBlank(fileName) && atts.getValue("displayName").toUpperCase().indexOf(fileName.toUpperCase())<0) continue;
				if (atts!=null) writer.addElement("file", "", atts);
			}
			
			//File Manager
			paramNames.clear();
			values.clear();
			otherExtensionCondition = otherExtensionCondition.replaceAll("reportFile", "fileName");
			queryString = "from FileManagerFiles " +
					"where (isDeleted = false or isDeleted is null) " +
					"and (popupAttachment = false or popupAttachment is null) " +
					"and isPrivate = false " +
					"and (attachmentMode = :attachmentModeDefault or attachmentMode = :attachmentModeFreeFolderShared or attachmentMode is null) " +
					"and attachToOperationUid = :operationUid " +
					(StringUtils.isNotBlank(fileName)?"AND (fileName like :fileName) ":"") +
					(StringUtils.isNotBlank(keywords)?"AND (description like :keywords) ":"") +
					(StringUtils.isNotBlank(category)?"AND (categoryPrimary like :category) ":"") +
					//(StringUtils.isNotBlank(fileExtension)?"AND fileName like :extension ":"") +
					(StringUtils.isNotBlank(fileExtension) && !"OTHER".equals(fileExtension)?"AND fileName like :extension ":"") +
					(StringUtils.isNotBlank(fileExtension) && "OTHER".equals(fileExtension) && !"".equals(otherExtensionCondition)?"AND (" + otherExtensionCondition + ") ":"") +
					"ORDER BY uploadedDatetime DESC";
			paramNames.add("attachmentModeDefault"); values.add(FileManagerUtils.ATTACHMENT_MODE_DEFAULT);
			paramNames.add("attachmentModeFreeFolderShared"); values.add(FileManagerUtils.ATTACHMENT_MODE_MULTI_FOLDER_SUPPORT_SHARED);
			paramNames.add("operationUid"); values.add(operationUid);
			if (StringUtils.isNotBlank(fileName)) {paramNames.add("fileName");values.add("%"+fileName+"%");}
			if (StringUtils.isNotBlank(keywords)) {paramNames.add("keywords");values.add("%"+keywords+"%");}
			if (StringUtils.isNotBlank(category)) {paramNames.add("category");values.add(category);}
			if (StringUtils.isNotBlank(fileExtension) && !"OTHER".equals(fileExtension)) {paramNames.add("extension");values.add("%."+fileExtension);}
			List<FileManagerFiles> fileList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, values);
			for (FileManagerFiles file : fileList) {
				SimpleAttributes atts = this.getFileNode(file, operationName, "GROUP_D", "Other Files", "fileManager");
				if (atts!=null) writer.addElement("file", "", atts);
			}
			
			writer.close();
			xml = bytes.toString("utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}
	
	private SimpleAttributes getFileNode(FileManagerFiles fileManagerFile, String operationName, String groupSort, String fileGroupName, String fileType) throws Exception {
		SimpleAttributes atts = null;
		String filePath = ApplicationConfig.getConfiguredInstance().getServerRootPath() + "WEB-INF/filemanager/"+
			fileManagerFile.getFileFullPathName() + "/" + fileManagerFile.getFileManagerFilesUid();
		File file = new File(filePath);

		if (file.exists()) {
			atts = new SimpleAttributes();
			atts.addAttribute("fileGroup", groupSort);
			atts.addAttribute("fileGroupName", fileGroupName);
			atts.addAttribute("fileType", fileType);
			atts.addAttribute("operationName", operationName);
			atts.addAttribute("displayName", fileManagerFile.getFileName());
			atts.addAttribute("paddedDisplayName", WellNameUtil.getPaddedStr(fileManagerFile.getFileName().toLowerCase()));
			Date lastModified = new Date(file.lastModified());
			//atts.addAttribute("reportDatetime", df.format(reportFile.getReportDayDate()));
			atts.addAttribute("fileDescription", fileManagerFile.getDescription());
			atts.addAttribute("fileAge", FreemarkerUtils.formatDurationSince(lastModified));
			
			Long fileLength = file.length();
			fileLength = fileLength / 1024;
			atts.addAttribute("fileSize", Math.round(fileLength) + "KB");
			atts.addAttribute("fileExtension", this.getFileExtension(fileManagerFile.getFileName()));
			atts.addAttribute("fileUid", fileManagerFile.getFileManagerFilesUid());
		}
		return atts;
		
	}
	
	private SimpleAttributes getFileNode(ReportFiles reportFile, String operationName, String groupSort, String fileGroupName, String fileType) throws Exception {
		SimpleAttributes atts = null;
		String filePath = ApplicationConfig.getConfiguredInstance().getServerRootPath() + "WEB-INF/report/pdf/"+reportFile.getReportFile();
		File file = new File(filePath);
		

		if (file.exists()) {
			atts = new SimpleAttributes();
			atts.addAttribute("fileGroup", groupSort);
			atts.addAttribute("fileGroupName", fileGroupName);
			atts.addAttribute("fileType", fileType);
			atts.addAttribute("operationName", operationName);
			atts.addAttribute("displayName", getFileName(reportFile, file));
			atts.addAttribute("paddedDisplayName", WellNameUtil.getPaddedStr(getFileName(reportFile, file).toLowerCase()));
			Date lastModified = new Date(file.lastModified());
			if (reportFile.getReportDayDate() != null){
				atts.addAttribute("reportDatetime", df.format(reportFile.getReportDayDate()));
				atts.addAttribute("reportDatetimeLong", longdate.format(reportFile.getReportDayDate()));
			}
			atts.addAttribute("fileDescription", "");
			atts.addAttribute("fileAge", FreemarkerUtils.formatDurationSince(lastModified));
			
			Long fileLength = file.length();
			fileLength = fileLength / 1024;
			atts.addAttribute("fileSize", Math.round(fileLength) + "KB");
			atts.addAttribute("fileExtension", this.getFileExtension(reportFile.getReportFile()));
			atts.addAttribute("fileUid", reportFile.getReportFilesUid());
		}
		return atts;
	}
	
	private String getFileName(ReportFiles reportFile, File file) throws Exception {
		if (StringUtils.isNotEmpty(reportFile.getDisplayName())) return reportFile.getDisplayName(); 
		if (StringUtils.isNotBlank(reportFile.getReportType())) {
			String queryString = "FROM ReportTypes WHERE (isDeleted=false or isDeleted is NULL) and reportType=:reportType";
			List<ReportTypes> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "reportType", reportFile.getReportType());
			if (list.size()>0) {
				ReportTypes reportType = list.get(0);
				String pattern = reportType.getReportFilenamePattern();
				if (StringUtils.isNotBlank(pattern)) {
					pattern = pattern.replace("${reporttype}", reportFile.getReportType()); 
					pattern = pattern.replace("${dayNumber}", reportFile.getReportDayNumber());
					pattern = pattern.replace("${operation}", CommonUtil.getConfiguredInstance().getCompleteOperationName(this.getCurrentUserSession().getCurrentGroupUid(), reportFile.getReportOperationUid()));
					pattern = pattern.replace("${uomTemplate}", "");
					pattern = pattern.replace("${date}", df.format(reportFile.getReportDayDate()));
					return pattern;
				}
			}
		}
		return reportFile.getReportType() + " - " + file.getName();
	}
	
	private String getFileExtension(String file){
		if(file == null) return null;
		int i = file.lastIndexOf(".");
		if(i != -1){
			return file.substring(i + 1);
		}else{
			return null;
		}
	}
	
}
