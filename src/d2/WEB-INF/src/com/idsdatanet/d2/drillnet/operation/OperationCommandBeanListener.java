package com.idsdatanet.d2.drillnet.operation;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.reportDaily.ReportDailyUtils;

public class OperationCommandBeanListener extends EmptyCommandBeanListener  {
	
	private List<String> fieldExcludeFromCarryOver = null;
	private Map<String, String> autoPopulateUOMTemplate = null;
	private Map<String, String> autoPopulateOperationHour = null;
	
	public void setFieldExcludeFromCarryOver(List<String> fieldExcludeFromCarryOver) {
		this.fieldExcludeFromCarryOver = fieldExcludeFromCarryOver;
	}
	public void setAutoPopulateUOMTemplate(Map<String, String> list){
		this.autoPopulateUOMTemplate = list;
	}
	public void setAutoPopulateOperationHour(Map<String, String> list){
		this.autoPopulateOperationHour = list;
	}
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN || commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT){
			commandBean.getRoot().getDynaAttr().put("datumLabel", commandBean.getInfo().getCurrentDatumDisplayName());
		}
		
		String dailyUid = userSelection.getDailyUid();
		
		if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CALCULATE_DAYS_SINCE_SPUD_WELL))){
			ReportDailyUtils.autoPopulateDaySinceSpud(dailyUid, commandBean);
		}
		
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(Operation.class)){
				if("wellUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					String wellUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "wellUid");	
					
					String strSql = "select onOffShore FROM Well WHERE (isDeleted = false or isDeleted is null) and wellUid = :thisWellUid";
					String[] paramsFields = {"thisWellUid"};
					Object[] paramsValues = {wellUid};
					
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					if (lstResult.size() > 0){
						String onOffShore = (String) lstResult.get(0);
						targetCommandBeanTreeNode.getDynaAttr().put("onOffShore", onOffShore);
						OperationUtils.setDefaultDatumReferencePoint(targetCommandBeanTreeNode);
					}
				}else if("uomTemplateUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					OperationUtils.changeUOMTemplate(targetCommandBeanTreeNode, request);
				
				}else if("operationalCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField()) || 
						"operationCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					UserSession session = UserSession.getInstance(request);
					UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
					Object object = targetCommandBeanTreeNode.getData();
					if (object instanceof Operation) {
						Operation operation = (Operation) object;
						String operationPrefix = OperationUtils.getOperationNamePrefix(userSelection, operation, null);
						if (StringUtils.isNotBlank(operationPrefix)) operation.setOperationName(operationPrefix);

						if (operation.getOperationCode() != null){
							String opCode = operation.getOperationCode();
							String relatedOpCode = OperationUtils.getMobilizationRelatedOpCode(opCode);
							if (relatedOpCode !=null){
								targetCommandBeanTreeNode.getDynaAttr().put("reportingDatumOffset", 0);
								targetCommandBeanTreeNode.getDynaAttr().put("offsetMsl", 0);
								targetCommandBeanTreeNode.getDynaAttr().put("datumReferencePoint", "MSL");
								targetCommandBeanTreeNode.getDynaAttr().put("datumType", "RT");
							}
						}
						
						if("operationCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField()))
						{
							
							if(this.autoPopulateUOMTemplate != null){
								for(String operationCode: this.autoPopulateUOMTemplate.keySet()){
										String uomTemplateUid = this.autoPopulateUOMTemplate.get(operationCode);
										if(targetCommandBeanTreeNode.getValue("operationCode").toString()!=""){
											if(operationCode.equalsIgnoreCase(targetCommandBeanTreeNode.getValue("operationCode").toString()))
											{
												OperationUtils.autoPopulateUOMTemplateBasedOnOperationType(targetCommandBeanTreeNode, uomTemplateUid);
											}
										}
									}
							}
							if(this.autoPopulateOperationHour != null){
								for(String operationCode: this.autoPopulateOperationHour.keySet()){
									    String operationHour = this.autoPopulateOperationHour.get(operationCode);
										if(targetCommandBeanTreeNode.getValue("operationCode").toString()!=""){
											if(operationCode.equalsIgnoreCase(targetCommandBeanTreeNode.getValue("operationCode").toString()))
											{
												OperationUtils.autoPopulateOperationHourBasedOnOperationType(targetCommandBeanTreeNode, operationHour);
											}
										}
									}
							}
							
						}
					}
					
				}else if ("@copyOperationFrom".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())) {
					UserSession session = UserSession.getInstance(request);
					String copyOperationUid = (String) targetCommandBeanTreeNode.getDynaAttr().get("copyOperationFrom");
	
					//create child node
					copyOperationData(commandBean, targetCommandBeanTreeNode, copyOperationUid, session);
					
				}

			}
		}

	}
	/**
	 * To copy selected operation data to current node
	 * @param commandBean
	 * @param targetNode
	 * @param operationUid
	 * @param session
	 * @throws Exception
	 */	
	public void copyOperationData(CommandBean commandBean, CommandBeanTreeNode targetNode, String operationUid, UserSession session) throws Exception {
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		UserSelectionSnapshot userSelection = UserSelectionSnapshot.getInstanceForCustomLogin(session.getCurrentUser().getUserName());
		if(targetNode.getDataDefinition().getTableClass().equals(Operation.class)){
			if (targetNode.getData() instanceof Operation){
				Operation operation = (Operation) targetNode.getData();
				
				if (StringUtils.isNotBlank(operationUid)){
					Operation selectedOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
					String defaultDatumUid = selectedOperation.getDefaultDatumUid();
					
					OpsDatum datum = DatumManager.getOpsDatum(defaultDatumUid);
					
					if (datum !=null){
						targetNode.getDynaAttr().put("reportingDatumOffset", datum.getReportingDatumOffset());
						targetNode.getDynaAttr().put("datumReferencePoint", datum.getDatumReferencePoint());
						targetNode.getDynaAttr().put("entryType", datum.getEntryType());
						targetNode.getDynaAttr().put("slantAngle", datum.getSlantAngle());
						targetNode.getDynaAttr().put("offsetMsl", datum.getOffsetMsl());
						
						if(datum.getIsGlReference() !=null && datum.getIsGlReference()){
							targetNode.getDynaAttr().put("isGlReference", "true");
						}else {
							targetNode.getDynaAttr().put("isGlReference", "");
						}
						
						targetNode.getDynaAttr().put("datumType", datum.getDatumCode());						
						PropertyUtils.copyProperties(operation, selectedOperation);
					}
					
					//after copy from selected operation, need to convert all Double value with UOM and Datum to current uom and datum. 
					//And set those excluded fields and primary key to null for creating a new record
					CommonUtil.getConfiguredInstance().setPropertyForNewCopiedRecord(commandBean, Operation.class, operation, thisConverter, this.fieldExcludeFromCarryOver);
					
					//set operation name prefix
					String operationPrefix = OperationUtils.getOperationNamePrefix(userSelection, operation, null);
					if (StringUtils.isNotBlank(operationPrefix)) operation.setOperationName(operationPrefix);
					//Tight Hole - value defaulted to 'Yes' (as when you add new)
					operation.setTightHole(false);
					//Display in Management Report - value defaulted to 'Yes' (as when you add new)
					operation.setDisplayInMgmtReport(true);
					//UOM Template - value defaulted to 'Onshore Drilling Metric' (as when you add new)
					OperationUtils.populateUomTemplateForNewOperation(targetNode, userSelection);
				} else {
					//Datum - set to blank (as no operation is selected, thus no data can be copied across)
					targetNode.getDynaAttr().put("reportingDatumOffset", null);
					targetNode.getDynaAttr().put("datumReferencePoint", null);
					targetNode.getDynaAttr().put("entryType", null);
					targetNode.getDynaAttr().put("slantAngle", null);
					targetNode.getDynaAttr().put("offsetMsl", null);
					targetNode.getDynaAttr().put("isGlReference", "");
					targetNode.getDynaAttr().put("datumType", null);
					//set operation name prefix
					String operationPrefix = OperationUtils.getOperationNamePrefix(userSelection, operation, null);
					if (StringUtils.isNotBlank(operationPrefix)) operation.setOperationName(operationPrefix);
					//Tight Hole - value defaulted to 'Yes' (as when you add new)
					operation.setTightHole(false);
					//Display in Management Report - value defaulted to 'Yes' (as when you add new)
					operation.setDisplayInMgmtReport(true);
					//UOM Template - value defaulted to 'Onshore Drilling Metric' (as when you add new)
					OperationUtils.populateUomTemplateForNewOperation(targetNode, userSelection);
				}
			}
		}
	}
			
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if (OperationUtils.CHECK_WELL_NAME_EXIST.equals(invocationKey)) {
			OperationUtils.checkWellNameExistence(request, response);
		} else if (OperationUtils.CHANGE_DATUM.equals(invocationKey)) {
			OperationUtils.changeDatum(commandBean, request, response);
		} else if (OperationUtils.CHECKING_OPERATION_NAME_EXIST.equals(invocationKey)) {
			OperationUtils.checkingOperationNameExist(request, response);
		}
		return;
	}
	
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		OperationUtils.setCustomUom(false, commandBean, userSelection);
	}


}
