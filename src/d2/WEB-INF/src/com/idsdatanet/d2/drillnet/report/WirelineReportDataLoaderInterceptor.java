package com.idsdatanet.d2.drillnet.report;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.WirelineRun;
import com.idsdatanet.d2.core.model.WirelineSuite;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WirelineReportDataLoaderInterceptor implements DataLoaderInterceptor {
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		
		//String suiteNumber = (String) commandBean.getRoot().getDynaAttr().get("suiteNumber");
		String suiteNumber = (String) userSelection.getCustomProperty("suiteNumber");		
		conditionClause = "";	
		
		if(WirelineSuite.class.equals(meta.getTableClass()) || WirelineRun.class.equals(meta.getTableClass())) {
			String suiteNumUid = "";
			String runNumUid = "";
			if(StringUtils.isNotBlank(suiteNumber)){
				
				if("ALL".equalsIgnoreCase(suiteNumber)) {
					conditionClause = "(isDeleted = false or isDeleted is null) and operationUid = session.operationUid";
				} else {
					String[] suiteAndRun = suiteNumber.split(":");
					if(suiteAndRun[0] != null) suiteNumUid = suiteAndRun[0];
					if(suiteAndRun.length > 1){
						if(suiteAndRun[1] != null) runNumUid = suiteAndRun[1];
					}				
					if (WirelineSuite.class.equals(meta.getTableClass())) {
						if(StringUtils.isNotBlank(suiteNumUid)) conditionClause = "(isDeleted = false or isDeleted is null) and operationUid = session.operationUid and wirelineSuiteUid = '" + suiteNumUid + "'";
					}
					
					if(WirelineRun.class.equals(meta.getTableClass())){
						//conditionClause = conditionClause + "wirelineSuiteUid = '" + suiteNumUid + "'";
						if(StringUtils.isNotBlank(runNumUid)) conditionClause = "(isDeleted = false or isDeleted is null) and operationUid = session.operationUid and wirelineRunUid = '" + runNumUid + "'";
					}
				}
			}else{
				conditionClause = "(isDeleted = false or isDeleted is null) and operationUid = session.operationUid";
			}
		}
		
		return conditionClause;
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

}
