package com.idsdatanet.d2.drillnet.hseIncident;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class HseIncidentDataNodeAllowedAction implements DataNodeAllowedAction {
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		
		String dailyUid = "";
		String sql = "";
		
		if (StringUtils.isNotBlank(session.getCurrentDailyUid())) dailyUid = session.getCurrentDailyUid();

		if(StringUtils.equals(targetClass, null)) {
			
			if (node.getDataDefinition().getTableClass().equals(LaggingIndicator.class)) {
				if(StringUtils.equals(action, "delete"))  {					
					if ("idsadmin".equals((String) session.getUserName())){ //only idsadmin can see the delete button
						return true;
					}else {
						return false;
					}
				}
					
			}
			
			if (node.getDataDefinition().getTableClass().equals(LeadingIndicator.class)) {
				if(StringUtils.equals(action, "delete"))  {
					if ("idsadmin".equals((String) session.getUserName())){//only idsadmin can see the delete button
						return true;
					}else {
						return false;
					}
				}
			}
			
			if (node.getDataDefinition().getTableClass().equals(OtherIndicator.class)) {
				if(StringUtils.equals(action, "delete"))  {
					if ("idsadmin".equals((String) session.getUserName())){//only idsadmin can see the delete button
						return true;
					}else {
						return false;
					}
				}
			}
			if (node.getDataDefinition().getTableClass().equals(DailyObservation.class)) {
				if(StringUtils.equals(action, "delete"))  {
					if ("idsadmin".equals((String) session.getUserName())){//only idsadmin can see the delete button
						return true;
					}else {
						return false;
					}
				}
			}
			
		}else if(StringUtils.equals(targetClass, "LaggingIndicator")) {
			sql = "select hseIncidentUid from HseIncident where (isDeleted = false or isDeleted is null) and dailyUid =:dailyUid and systemStatus='lagging'";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "dailyUid", dailyUid);
			if (!lstResult.isEmpty())
			{
				if(StringUtils.equals(action, "add"))  return false; 
				if(StringUtils.equals(action, "pasteAsNew"))  return false; 
			
			}
			
		}else if(StringUtils.equals(targetClass, "LeadingIndicator")) {
			sql = "select hseIncidentUid from HseIncident where (isDeleted = false or isDeleted is null) and dailyUid =:dailyUid and systemStatus='leading'";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "dailyUid", dailyUid);
			if (!lstResult.isEmpty())
			{
				if(StringUtils.equals(action, "add"))  return false; 
				if(StringUtils.equals(action, "pasteAsNew"))  return false; 
			
			}
			
		}else if(StringUtils.equals(targetClass, "OtherIndicator")) {
			sql = "select hseIncidentUid from HseIncident where (isDeleted = false or isDeleted is null) and dailyUid =:dailyUid and systemStatus='other'";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "dailyUid", dailyUid);
			if (!lstResult.isEmpty())
			{
				if(StringUtils.equals(action, "add"))  return false; 
				if(StringUtils.equals(action, "pasteAsNew"))  return false; 
			
			}
			
		}
		else if(StringUtils.equals(targetClass, "DailyObservation")) {
			sql = "select hseIncidentUid from HseIncident where (isDeleted = false or isDeleted is null) and dailyUid =:dailyUid and systemStatus='observation'";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "dailyUid", dailyUid);
			if (!lstResult.isEmpty())
			{
				if(StringUtils.equals(action, "add"))  return false; 
				if(StringUtils.equals(action, "pasteAsNew"))  return false; 
			
			}
			
		}
		
		return true;
	}
}
