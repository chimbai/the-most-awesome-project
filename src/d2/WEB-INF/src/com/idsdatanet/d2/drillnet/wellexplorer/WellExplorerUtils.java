package com.idsdatanet.d2.drillnet.wellexplorer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.cache.Wellbores;
import com.idsdatanet.d2.core.cache.Wells;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.util.SimpleTreeNode;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Well;
import com.idsdatanet.d2.core.web.mvc._Wellbore;
import com.idsdatanet.d2.core.web.session.SystemSelectionFilter;

/**
 * A utility for well explorer screen to generate tree menu.
 * @author RYAU
 *
 */
public class WellExplorerUtils extends DefaultBeanSupport {
	private Map<String, _Wellbore> cached_wellbore_with_no_parent = null;
	
	
	/**
	 * Get a singleton of this class object.
	 * @return
	 * @throws Exception
	 */
	public static WellExplorerUtils getConfiguredInstance() throws Exception {
		return getSingletonInstance(WellExplorerUtils.class);
	}
	
	/**
	 * Generate a tree node structure for Well Explorer to have a well, wellbore and operation structure for easier selection.
	 * @return
	 * @throws Exception
	 */
	public SimpleTreeNode getWellExplorerTreeMenu(UserSession session) throws Exception {
		SimpleTreeNode root = new SimpleTreeNode();
		//LinkedHashMap<String, _Well> wells = new LinkedHashMap<String, _Well>();
		Wells wells = null;
		
		String wellUid = session.getSystemSelectionFilter().getProperty(SystemSelectionFilter.WELL_FILTER);
		if(StringUtils.isNotBlank(wellUid)) {
			Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(wellUid);
			//wells.put(wellUid, new _Well(well.getWellUid(), well.getGroupUid(), well.getWellName(), well.getState(), well.getWorkinggroup(), well.getField()));
			wells = new Wells();
			wells.add(new _Well(well.getWellUid(), well.getGroupUid(), well.getWellName(), well.getState(), well.getWorkinggroup(), well.getField()));
		} else {
			//wells.putAll(ApplicationUtils.getConfiguredInstance().getCachedAllWells());
			wells = ApplicationUtils.getConfiguredInstance().getCachedAllWells();
		}
		
		if(wells != null) {
			for(_Well well : wells.values()) {
				SimpleTreeNode wellNode = this.setWellToTreeMenu(well);
				_Wellbore wellbore = null;
				String wellboreUid = session.getSystemSelectionFilter().getProperty(SystemSelectionFilter.WELLBORE_FILTER);
				if(StringUtils.isNotBlank(wellboreUid)) {
					wellbore = ApplicationUtils.getConfiguredInstance().getCachedAllWellbores().get(wellboreUid);
				} else {
					wellbore = WellExplorerUtils.getConfiguredInstance().getCachedWellboreWithNoParentWellboreByWellUid(well.getWellUid());
				}
				if(wellbore != null) {
					SimpleTreeNode wellboreNode = new SimpleTreeNode();
					if(StringUtils.isBlank(wellbore.getParentWellboreUid())) {
						wellboreNode = this.getWellboreTreeMenu(wellbore.getWellboreUid());
					}
					wellNode.addChild(wellboreNode);
				}
				root.addChild(wellNode);
			}
		}
		return root;
	}
	
	/**
	 * Find the given wellboreUid and search for wellbore child.  
	 * @param wellboreUid
	 * @return
	 * @throws Exception
	 */
	private SimpleTreeNode getWellboreTreeMenu(String wellboreUid) throws Exception {
		Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getCachedWellbore(wellboreUid);
		SimpleTreeNode wellboreNode = setWellboreToTreeMenu(wellbore);
		
		List<Wellbore> list = ApplicationUtils.getConfiguredInstance().getAllWellboresByParentWellboreUid(wellbore.getWellboreUid());
		for(Wellbore wellboreChild : list) {
			SimpleTreeNode wellboreChildNode = setWellboreToTreeMenu(wellboreChild);
			wellboreNode.addChild(wellboreChildNode);
			this.cascadeWellboreTreeMenu(wellboreChild.getWellboreUid(), wellboreChildNode);
			setOperationsToTreeMenuByWellbore(wellboreChild.getWellboreUid(), wellboreChildNode);
		}
		setOperationsToTreeMenuByWellbore(wellbore.getWellboreUid(), wellboreNode);

		return wellboreNode;
	}
	
	/**
	 * Cascade to search for wellbore parent of a given wellboreUid and tie them to a given MenuTreeNode
	 * @param wellboreUid
	 * @param node
	 * @throws Exception
	 */
	private void cascadeWellboreTreeMenu(String wellboreUid, SimpleTreeNode node) throws Exception {
		List<Wellbore> list = ApplicationUtils.getConfiguredInstance().getAllWellboresByParentWellboreUid(wellboreUid);
		if(list != null && list.size() > 0) {
			for(Wellbore wellbore : list) {
				SimpleTreeNode wellboreChildNode = setWellboreToTreeMenu(wellbore);
				node.addChild(wellboreChildNode);
				this.cascadeWellboreTreeMenu(wellbore.getWellboreUid(), wellboreChildNode);
				setOperationsToTreeMenuByWellbore(wellbore.getWellboreUid(), wellboreChildNode);
			}
		}
	}
	
	/**
	 * Return a wellbore with no parent wellboreUid given a wellUid.
	 * @param wellUid
	 * @return
	 * @throws Exception
	 */
	private _Wellbore getCachedWellboreWithNoParentWellboreByWellUid(String wellUid) throws Exception {
		return this.getCachedWellboreWithWellUidAndNoParentWellboreMap().get(wellUid);
	}
	
	/**
	 * Return a Map of wellbore list which has no parent wellbore bundle together with wellUid.
	 * @return
	 * @throws Exception
	 */
	private Map<String, _Wellbore> getCachedWellboreWithWellUidAndNoParentWellboreMap() throws Exception {
		if(this.cached_wellbore_with_no_parent != null) return this.cached_wellbore_with_no_parent;
		
		this.cached_wellbore_with_no_parent = new HashMap<String, _Wellbore>();
		Wellbores wellbores = ApplicationUtils.getConfiguredInstance().getCachedAllWellbores();
		for(_Wellbore wellbore : wellbores.values()) {
			if(StringUtils.isBlank(wellbore.getParentWellboreUid()))
				this.cached_wellbore_with_no_parent.put(wellbore.getWellUid(), wellbore);
		}
		return this.cached_wellbore_with_no_parent;
	}
	
	/**
	 * Set a well to a tree menu given a well object
	 * @param wellbore
	 * @return
	 */
	private SimpleTreeNode setWellToTreeMenu(_Well well) {
		SimpleTreeNode node = new SimpleTreeNode();
		node.setAttr("type", WellExplorerConstant.TYPE_WELL);
		node.setKey(well.getWellUid());
		node.setValue(well.getWellName());
		return node;
	}
	
	/**
	 * Set a wellbore to a tree menu given a wellbore object
	 * @param wellbore
	 * @return
	 */
	private SimpleTreeNode setWellboreToTreeMenu(Wellbore wellbore) {
		SimpleTreeNode node = new SimpleTreeNode();
		node.setAttr("type", WellExplorerConstant.TYPE_WELLBORE);
		node.setKey(wellbore.getWellboreUid());
		node.setValue(wellbore.getWellboreName());
		return node;
	}
	
	/**
	 * Generate a list of operations tie to given wellboreUid and add them to MenuTreeNode
	 * @param wellboreUid
	 * @param node
	 * @throws Exception
	 */
	private void setOperationsToTreeMenuByWellbore(String wellboreUid, SimpleTreeNode node) throws Exception {
		List<Operation> operations = ApplicationUtils.getConfiguredInstance().getOperationsByWellbore(wellboreUid);
		if(operations != null && operations.size() > 0) {
			for(Operation operation : operations) {
				SimpleTreeNode operationNode = new SimpleTreeNode();
				operationNode.setAttr("type", WellExplorerConstant.TYPE_OPERATION);
				operationNode.setKey(operation.getOperationUid());
				operationNode.setValue(operation.getOperationName());
				node.addChild(operationNode);
			}
		}
	}
	
	/**
	 * Clear cached data.
	 */
	protected synchronized void onApplicationRefresh() throws Exception {
		if(this.cached_wellbore_with_no_parent != null) this.cached_wellbore_with_no_parent.clear(); this.cached_wellbore_with_no_parent = null;
	}
	
	/**
	 * Clear reference to cached data.
	 */
	protected void onApplicationExit() throws Exception {
		if(this.cached_wellbore_with_no_parent != null) this.cached_wellbore_with_no_parent.clear(); this.cached_wellbore_with_no_parent = null;
	}
}
