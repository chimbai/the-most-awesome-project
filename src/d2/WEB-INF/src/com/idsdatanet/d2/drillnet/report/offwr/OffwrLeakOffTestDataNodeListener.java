package com.idsdatanet.d2.drillnet.report.offwr;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.leakOffTest.LeakOffTestDataNodeListener;

public class OffwrLeakOffTestDataNodeListener extends LeakOffTestDataNodeListener {
		
	@Override
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		super.afterDataNodeLoad(commandBean, meta, node, userSelection, request);
		
		Object object = node.getData();
		if (object instanceof LeakOffTest) {
			LeakOffTest leakofftest = (LeakOffTest) object;
			
			// Get ReportDaily for the record to add dynaAttr - Date / EpochMS
			QueryProperties qp = new QueryProperties();
			qp.setFetchFirstRowOnly();
			String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND dailyUid=:dailyUid AND reportType!='DGR'";		
			String[] paramsFields = {"dailyUid"};
			String[] paramsValues = {leakofftest.getDailyUid()};
			List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
			
			if (!reportDailyList.isEmpty()) {
				ReportDaily rd = reportDailyList.get(0);
				Date rdDatetime = rd.getReportDatetime();
				
				String formattedDate = dateFormat.format(rdDatetime); 
				long epochTime = rdDatetime.getTime();
				
				node.getDynaAttr().put("reportDaily.reportDatetime", formattedDate);
				node.getDynaAttr().put("reportDaily.reportDatetimeEpochMS", epochTime);
			}
		}
	}
}
