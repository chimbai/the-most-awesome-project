package com.idsdatanet.d2.drillnet.activity.kpi;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityCodingKpiMatrixPhaseCode;
import com.idsdatanet.d2.core.model.CustomFtrLink;
import com.idsdatanet.d2.core.model.FtrBopEquipment;
import com.idsdatanet.d2.core.model.FtrDrillLineEquipment;
import com.idsdatanet.d2.core.model.FtrRig;
import com.idsdatanet.d2.core.model.FtrDrilling;
import com.idsdatanet.d2.core.model.FtrTripping;
import com.idsdatanet.d2.core.model.FtrWorkoverCompletionSystem;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ActivityKpiUtil {
	
	//calculate the total activity duration in a set of phase codes with excluded tasks codes, not filtered by time range
	public static double calculateTotalDurationByPhaseCode(String kpiType, List operationUid) throws Exception
	{
		Double total = 0.0;
		
		String paramField = "thisKpiType";
 		String paramValue = kpiType;
		String strSql = "SELECT activityCodingKpiMatrixUid FROM ActivityCodingKpiMatrix where kpiType = :thisKpiType and (isDeleted = false or isDeleted is null)";
		List<String> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramField, paramValue);
		String kpiMatrixUid = null;
		
		if (!result.isEmpty())
			kpiMatrixUid = result.get(0).toString();
		
		if(kpiMatrixUid != null && StringUtils.isNotBlank(kpiMatrixUid))
		{
			String paramField2 = "thisKpiMatrixUid";
	 		String paramValue2 = kpiMatrixUid;
			String strSql2 = "FROM ActivityCodingKpiMatrixPhaseCode where activityCodingKpiMatrixUid = :thisKpiMatrixUid and (isDeleted = false or isDeleted is null)";
			List<ActivityCodingKpiMatrixPhaseCode> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramField2, paramValue2);
			
			if (!list2.isEmpty()) {
				//for each activty coding kpi matrix phase code, get its excluded task code
				for(ActivityCodingKpiMatrixPhaseCode lst1 : list2)
				{
					String paramField3 = "thisKpiPhaseUid";
			 		String paramValue3 = lst1.getActivityCodingKpiMatrixPhaseCodeUid();
					String strSql3 = "SELECT taskCode FROM ActivityCodingKpiMatrixTaskCodeExclusion where activityCodingKpiMatrixPhaseCodeUid = :thisKpiPhaseUid and (isDeleted = false or isDeleted is null)";
					List list3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramField3, paramValue3);
					
					List list4 = null;
					//calculate the sum of activity duration of the task code excluding the excluded task code
					String strSql4 = "SELECT SUM(activityDuration) FROM Activity " +
					"WHERE phaseCode = :thisKpiPhaseCode " + 
					"AND (isDeleted = false or isDeleted is null) " +
					"AND (operationUid in (:thisOpsUid)) " +
					"AND (isSimop=false or isSimop is null) " +
					"AND (isOffline=false or isOffline is null) " +
					"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null)";
					
					if(!list3.isEmpty())
					{		
						String[] paramField4 = {"thisKpiPhaseCode", "thisOpsUid", "thisExcludedTaskCode"};
						Object[] paramValue4 = {lst1.getPhaseCode(), operationUid, list3};
						strSql4 = strSql4 + (" AND (taskCode not in (:thisExcludedTaskCode))");
						list4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramField4, paramValue4);
					}
					else
					{
						String[] paramField4 = {"thisKpiPhaseCode", "thisOpsUid"};
						Object[] paramValue4 = {lst1.getPhaseCode(), operationUid};
						list4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramField4, paramValue4);
					}
					
					if(list4 != null && !list4.isEmpty())
					{
						if(list4.get(0) != null)
							total += Double.parseDouble(list4.get(0).toString());
					}
				}
			}else return 0;
		} else return 0;
		return total;
	}
	
	//calculate the total activity duration in a set of phase codes with excluded tasks codes, filtered by time range
	public static double calculateTotalDurationByPhaseCode(String kpiType, List operationUid, Date dateTimeFrom, Date dateTimeTo, QueryProperties qp) throws Exception
	{
		//retrieve the activities that are within the time range
		List<String> activityWithinDateTimeRange = getActivityInDateRange(operationUid, dateTimeFrom, dateTimeTo);
		Double total = 0.0;
		
		String paramField = "thisKpiType";
 		String paramValue = kpiType;
		String strSql = "SELECT activityCodingKpiMatrixUid FROM ActivityCodingKpiMatrix where kpiType = :thisKpiType and (isDeleted = false or isDeleted is null)";
		List<String> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramField, paramValue);
		String kpiMatrixUid = null;
		
		if (!result.isEmpty())
			kpiMatrixUid = result.get(0).toString();
		
		if(kpiMatrixUid != null && StringUtils.isNotBlank(kpiMatrixUid))
		{
			String paramField2 = "thisKpiMatrixUid";
	 		String paramValue2 = kpiMatrixUid;
			String strSql2 = "FROM ActivityCodingKpiMatrixPhaseCode where activityCodingKpiMatrixUid = :thisKpiMatrixUid and (isDeleted = false or isDeleted is null)";
			List<ActivityCodingKpiMatrixPhaseCode> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramField2, paramValue2);
			
			if (!list2.isEmpty()) {
				for(ActivityCodingKpiMatrixPhaseCode lst1 : list2)
				{
					String paramField3 = "thisKpiPhaseUid";
			 		String paramValue3 = lst1.getActivityCodingKpiMatrixPhaseCodeUid();
					String strSql3 = "SELECT taskCode FROM ActivityCodingKpiMatrixTaskCodeExclusion where activityCodingKpiMatrixPhaseCodeUid = :thisKpiPhaseUid and (isDeleted = false or isDeleted is null)";
					List list3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramField3, paramValue3);
					
					List<Activity> list4 = null;
					//calculate the sum of activity duration of the task code excluding the excluded task code and within the time range
					String strSql4 = "FROM Activity " +
					"WHERE phaseCode = :thisKpiPhaseCode " + 
					"AND (isDeleted = false or isDeleted is null) " +
					"AND (operationUid in (:thisOpsUid)) " +
					"AND (isSimop=false or isSimop is null) " +
					"AND (isOffline=false or isOffline is null) " +
					"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null)";
					
					if(!list3.isEmpty())
					{		
						if(activityWithinDateTimeRange == null)
						{
							String[] paramField4 = {"thisKpiPhaseCode", "thisOpsUid", "thisExcludedTaskCode"};
							Object[] paramValue4 = {lst1.getPhaseCode(), operationUid, list3};
							strSql4 = strSql4 + (" AND (taskCode not in (:thisExcludedTaskCode))");
							list4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramField4, paramValue4, qp);
						}
						else if(!activityWithinDateTimeRange.isEmpty())
						{
							String[] paramField4 = {"thisKpiPhaseCode", "thisOpsUid", "thisExcludedTaskCode", "thisRangeActUid"};
							Object[] paramValue4 = {lst1.getPhaseCode(), operationUid, list3, activityWithinDateTimeRange};
							strSql4 = strSql4 + (" AND (taskCode not in (:thisExcludedTaskCode)) AND (activityUid in (:thisRangeActUid))");
							list4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramField4, paramValue4, qp);
						}
					}
					else
					{
						if(activityWithinDateTimeRange == null)
						{
							String[] paramField4 = {"thisKpiPhaseCode", "thisOpsUid"};
							Object[] paramValue4 = {lst1.getPhaseCode(), operationUid};
							list4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramField4, paramValue4, qp);
						}
						else if(!activityWithinDateTimeRange.isEmpty())
						{
							String[] paramField4 = {"thisKpiPhaseCode", "thisOpsUid", "thisRangeActUid"};
							Object[] paramValue4 = {lst1.getPhaseCode(), operationUid, activityWithinDateTimeRange};
							strSql4 = strSql4 + (" AND (activityUid in (:thisRangeActUid))");
							list4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramField4, paramValue4, qp);
						}
					}
					
					if(list4 != null && !list4.isEmpty())
					{
						if(dateTimeTo != null && dateTimeFrom != null)
						{
							for(Activity act : list4)
							{
								Double dura = 0.0;
								if(act.getStartDatetime().before(dateTimeTo) && act.getEndDatetime().after(dateTimeTo))
								{
									Long duration = dateTimeTo.getTime() - act.getStartDatetime().getTime();
									duration = TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS);
									dura = Double.parseDouble(duration.toString());
								}
								else if(act.getStartDatetime().before(dateTimeFrom) && act.getEndDatetime().after(dateTimeFrom))
								{
									Long duration = act.getEndDatetime().getTime() - dateTimeFrom.getTime();
									duration = TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS);
									dura = Double.parseDouble(duration.toString());
								}
								else
									dura = Double.parseDouble(act.getActivityDuration().toString());
								
								total += dura;
							}
						}
						else if(dateTimeTo != null)//activity before
						{
							for(Activity act : list4)
							{
								Double dura = 0.0;
								if(act.getStartDatetime().before(dateTimeTo) && act.getEndDatetime().after(dateTimeTo))
								{
									Long duration = dateTimeTo.getTime() - act.getStartDatetime().getTime();
									duration = TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS);
									dura = Double.parseDouble(duration.toString());
								}
								else
									dura = Double.parseDouble(act.getActivityDuration().toString());
								
								total += dura;
							}
						}
						else if(dateTimeFrom != null)//activity after
						{
							for(Activity act : list4)
							{
								Double dura = 0.0;
								if(act.getStartDatetime().before(dateTimeFrom) && act.getEndDatetime().after(dateTimeFrom))
								{;
									Long duration = act.getEndDatetime().getTime() - dateTimeFrom.getTime();
									duration = TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS);
									dura = Double.parseDouble(duration.toString());
								}
								else
									dura = Double.parseDouble(act.getActivityDuration().toString());
								
								total += dura;
							}
						}
					}
				}
			}else return 0;
		} else return 0;
		return total;
	}
	
	//calculate the total depth progress in a set of phase codes with excluded tasks codes, filtered by time range
	public static double calculateTotalProgressByPhaseCode(String kpiType, List operationUid, Date startDateTime, Date endDateTime, QueryProperties qp) throws Exception
	{
		List<String> excludedActivity = new LinkedList<String>();
		List<String> activityWithinDateTimeRange = getActivityInDateRange(operationUid, startDateTime, endDateTime);
		List<String> phaseCodes = new LinkedList<String>();
		Double interval = 0.0;
		
		String paramField = "thisKpiType";
 		String paramValue = kpiType;
		String strSql = "SELECT activityCodingKpiMatrixUid FROM ActivityCodingKpiMatrix where kpiType = :thisKpiType and (isDeleted = false or isDeleted is null)";
		List<String> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramField, paramValue);
		String kpiMatrixUid = null;
		
		if (!result.isEmpty())
			kpiMatrixUid = result.get(0).toString();
		
		if(kpiMatrixUid != null && StringUtils.isNotBlank(kpiMatrixUid))
		{
			String paramField2 = "thisKpiMatrixUid";
	 		String paramValue2 = kpiMatrixUid;
			String strSql2 = "FROM ActivityCodingKpiMatrixPhaseCode where activityCodingKpiMatrixUid = :thisKpiMatrixUid and (isDeleted = false or isDeleted is null)";
			List<ActivityCodingKpiMatrixPhaseCode> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramField2, paramValue2);
			if (!list2.isEmpty()) {
				// to get activity that are to be excluded
				for(ActivityCodingKpiMatrixPhaseCode lst1 : list2)
				{
					phaseCodes.add(lst1.getPhaseCode());
					
					String paramField3 = "thisKpiPhaseUid";
			 		String paramValue3 = lst1.getActivityCodingKpiMatrixPhaseCodeUid();
					String strSql3 = "SELECT taskCode FROM ActivityCodingKpiMatrixTaskCodeExclusion where activityCodingKpiMatrixPhaseCodeUid = :thisKpiPhaseUid and (isDeleted = false or isDeleted is null)";
					List list3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramField3, paramValue3);
					
					if(!list3.isEmpty())
					{
					String[] paramFieldExclude = {"thisTaskCode","thisPhaseCode"};
					Object[] paramValueExclude = {list3, lst1.getPhaseCode()};
					String strSqlparamFieldExclude = "SELECT activityUid FROM Activity WHERE phaseCode = :thisPhaseCode AND (taskCode in (:thisTaskCode)) AND (isDeleted = false or isDeleted is null)";
					List<String> activityUidList =  ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlparamFieldExclude, paramFieldExclude, paramValueExclude);
					excludedActivity.addAll(activityUidList);
					}
				}

				String strSqlActivity = "FROM Activity " +
				"WHERE (phaseCode in (:thisKpiPhaseCode)) " + 
				"AND (isDeleted = false or isDeleted is null) " +
				"AND operationUid = :thisOpsUid " +
				"AND depthMdMsl is not null " +
				"AND (isSimop=false or isSimop is null) " +
				"AND (isOffline=false or isOffline is null) " +
				"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null)";
				
				for(Object ops : operationUid)
				{
					List<Activity> activityResult = null;
					
					if(!excludedActivity.isEmpty())
					{
						if(activityWithinDateTimeRange == null)
						{
							String[] paramFieldActivity2 = {"thisKpiPhaseCode","thisOpsUid", "thisActUid"};
							Object[] paramValueActivity2 = {phaseCodes, ops, excludedActivity};
							strSqlActivity = strSqlActivity + " AND (activityUid not in (:thisActUid))" +
							" order by endDatetime ASC";
							activityResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlActivity, paramFieldActivity2, paramValueActivity2, qp);
						} 
						else if(!activityWithinDateTimeRange.isEmpty())
						{
							String[] paramFieldActivity2 = {"thisKpiPhaseCode","thisOpsUid", "thisActUid", "thisRangeActUid"};
							Object[] paramValueActivity2 = {phaseCodes, ops, excludedActivity, activityWithinDateTimeRange};
							strSqlActivity = strSqlActivity + " AND (activityUid not in (:thisActUid)) AND (activityUid in (:thisRangeActUid))" +
							" order by endDatetime ASC";
							activityResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlActivity, paramFieldActivity2, paramValueActivity2, qp);
						}
					}
					else
					{
						if(activityWithinDateTimeRange == null)
						{
							String[] paramFieldActivity = {"thisKpiPhaseCode","thisOpsUid"};
							Object[] paramValueActivity = {phaseCodes, ops};
							strSqlActivity = strSqlActivity + " order by endDatetime ASC";
							activityResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlActivity, paramFieldActivity, paramValueActivity, qp);
						}
						else if(!activityWithinDateTimeRange.isEmpty())
						{
							String[] paramFieldActivity = {"thisKpiPhaseCode","thisOpsUid", "thisRangeActUid"};
							Object[] paramValueActivity = {phaseCodes, ops, activityWithinDateTimeRange};
							strSqlActivity = strSqlActivity + " AND (activityUid in (:thisRangeActUid)) order by endDatetime ASC";
							activityResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlActivity, paramFieldActivity, paramValueActivity, qp);
						}
					}
					
					if(activityResult != null && !activityResult.isEmpty())
					{
						for (int i = 0; i < activityResult.size(); i++)
						{
							if(activityResult.get(i).getDepthFromMdMsl() != null)
								interval += (activityResult.get(i).getDepthMdMsl() - activityResult.get(i).getDepthFromMdMsl());
							else
							{
								if(i != 0)
									interval += (activityResult.get(i).getDepthMdMsl() - activityResult.get(i - 1).getDepthMdMsl());
							}
						}
					}
				}
			} else return 0;
		} else return 0;
		return interval;
	}
	
	public static double calculateDryHoleDays(List opsUid, Date spudDate, Date dryHoleEnd, QueryProperties qp) throws Exception
	{
		Double dryHole = 0.0;
		List<String> activityWithinDateTimeRange = getActivityInDateRange(opsUid, spudDate, dryHoleEnd);
		
		String[] paramFieldsDryHole = {"thisOpsUid","thisRangeActUid"};
		Object[] paramValuesDryHole = {opsUid, activityWithinDateTimeRange};
		String strSqlDryHole = "FROM Activity " +
		"WHERE (isDeleted = false or isDeleted is null) " +
		"AND (isSimop=false or isSimop is null) " +
		"AND (isOffline=false or isOffline is null) " +
		"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
		"AND (operationUid in (:thisOpsUid)) " +
		"AND (activityUid in (:thisRangeActUid)) order by endDatetime ASC";
		
		if(activityWithinDateTimeRange != null && !activityWithinDateTimeRange.isEmpty())
		{
			List<Activity> listAct = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlDryHole, paramFieldsDryHole, paramValuesDryHole, qp);
			
			if(!listAct.isEmpty())
			{
				if(spudDate != null && dryHoleEnd != null)
				{
					Double dura = 0.0;
					for(Activity act : listAct)
					{
						if(act.getStartDatetime().before(dryHoleEnd) && act.getEndDatetime().after(dryHoleEnd))
						{
							Long duration = dryHoleEnd.getTime() - act.getStartDatetime().getTime();
							duration = TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS);
							dura = Double.parseDouble(duration.toString());
						}
						else if(act.getStartDatetime().before(spudDate) && act.getEndDatetime().after(spudDate))
						{
							Long duration = act.getEndDatetime().getTime() - spudDate.getTime();
							duration = TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS);
							dura = Double.parseDouble(duration.toString());
						}
						else
							dura = Double.parseDouble(act.getActivityDuration().toString());
						
						dryHole += dura;
					}
				}
			}
		}
		else
			return 0;
		
		return dryHole;
	}
	
	public static double calculateWowTime(List opsUid, Date dryHoleEnd, Boolean include, QueryProperties qp) throws Exception
	{
		Double wowTime = 0.0;
		List<String> activityWithinDateTimeRange = getActivityInDateRange(opsUid, null, dryHoleEnd);
		
		String[] paramFieldsWow = {"thisOpsUid","thisRangeActUid"};
		Object[] paramValuesWow = {opsUid, activityWithinDateTimeRange};
		String strSqlWow = "FROM Activity " +
		"WHERE (isDeleted = false or isDeleted is null) " +
		"AND (isSimop=false or isSimop is null) " +
		"AND (isOffline=false or isOffline is null) " +
		"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
		"AND (operationUid in (:thisOpsUid)) " +
		"AND (activityUid in (:thisRangeActUid)) " +
		"AND (internalClassCode = 'TP' or internalClassCode = 'TU')";
		
		if(include)
			strSqlWow += " AND rootCauseCode = 'WOW' order by endDatetime ASC";
		else
			strSqlWow += " AND rootCauseCode <> 'WOW' order by endDatetime ASC";
		
		if(activityWithinDateTimeRange != null && !activityWithinDateTimeRange.isEmpty())
		{
			List<Activity> listAct = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlWow, paramFieldsWow, paramValuesWow, qp);
			
			if(!listAct.isEmpty())
			{
				if(dryHoleEnd != null)
				{
					Double dura = 0.0;
					for(Activity act : listAct)
					{
						if(act.getStartDatetime().before(dryHoleEnd) && act.getEndDatetime().after(dryHoleEnd))
						{
							Long duration = dryHoleEnd.getTime() - act.getStartDatetime().getTime();
							duration = TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS);
							dura = Double.parseDouble(duration.toString());
						}
						else
							dura = Double.parseDouble(act.getActivityDuration().toString());
						
						wowTime += dura;
					}
				}
			}
		}
		
		return wowTime;
	}
	
	//retrieve activities that are in the time range and operations
	public static List getActivityInDateRange(List opsUid, Date dateTimeFrom, Date dateTimeTo) throws Exception
	{
		List result = null;
		if(dateTimeTo != null && dateTimeFrom != null)
		{
			String[] paramField = {"thisOpsUid", "thisDateTimeTo", "thisDateTimeFrom"};
			Object[] paramValue = {opsUid, dateTimeTo, dateTimeFrom};
			String strSql = "SELECT activityUid FROM Activity WHERE (operationUid in (:thisOpsUid)) AND startDatetime < :thisDateTimeTo AND endDatetime > :thisDateTimeFrom AND (isDeleted = false or isDeleted is null)";
			result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramField, paramValue);
		}
		else if(dateTimeTo != null)//activity before
		{
			String[] paramField = {"thisOpsUid", "thisDateTimeTo"};
			Object[] paramValue = {opsUid, dateTimeTo};
			String strSql = "SELECT activityUid FROM Activity WHERE (operationUid in (:thisOpsUid)) AND startDatetime < :thisDateTimeTo AND (isDeleted = false or isDeleted is null)";
			result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramField, paramValue);
		}
		else if(dateTimeFrom != null)//activity after
		{
			String[] paramField = {"thisOpsUid", "thisDateTimeFrom"};
			Object[] paramValue = {opsUid, dateTimeFrom};
			String strSql = "SELECT activityUid FROM Activity WHERE (operationUid in (:thisOpsUid)) AND endDatetime > :thisDateTimeFrom AND (isDeleted = false or isDeleted is null)";
			result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramField, paramValue);
		}
		
		return result;
	}
	
	public static void createFTRKPIBasedOnActivityRemarks(CommandBean commandBean,UserSelectionSnapshot userSelection,String moduleName,Object thisActivity, Map<String,Object>fieldsMap) throws Exception
	{
		String wellUid = null;
		String wellboreUid = null;
		String operationUid = null;
		String dailyUid = null;
		String activityUid = null;
		Double activityDuration = 0.0;
		Activity a;
		NextDayActivity nda;
		if (fieldsMap != null){
			if(thisActivity instanceof Activity){
				a = (Activity) thisActivity;
				wellUid= a.getWellUid();
				wellboreUid= a.getWellboreUid();
				operationUid = a.getOperationUid();
				dailyUid = a.getDailyUid();
				activityUid = a.getActivityUid();
				activityDuration = a.getActivityDuration();
			}else if (thisActivity instanceof NextDayActivity){
				nda = (NextDayActivity) thisActivity;
				wellUid= nda.getWellUid();
				wellboreUid= nda.getWellboreUid();
				operationUid = nda.getOperationUid();
				dailyUid = nda.getDailyUid();
				activityUid = nda.getActivityUid();
				activityDuration = nda.getActivityDuration();
			}
			String[] paramsFields = {"operationUid","dailyUid","activityUid"};
			Object[] paramsValues = {operationUid,dailyUid,activityUid};
			String strSql = "FROM " + moduleName + " WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND dailyUid=:dailyUid AND activityUid=:activityUid";
			String strSqlRemarksCode = "FROM CustomFtrLink WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid and dailyUid=:dailyUid and activityUid=:activityUid";
			List<Object> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			List<CustomFtrLink> lsRemarksCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlRemarksCode, paramsFields, paramsValues);
			String classPathName = ApplicationUtils.getConfiguredInstance().getTablePackage();
			CustomFieldUom thisConverter = null;
			CustomFieldUom activityConverter = null;
			Class ftr = Class.forName(classPathName + moduleName);
			Constructor constructor = null;
			Object o = null;
			if(ftr != null){
				try {
					constructor = ftr.getDeclaredConstructor();
					if(constructor != null){
						o = constructor.newInstance();
					}
				}catch (InvocationTargetException x) {
		            x.printStackTrace();
		          } catch (NoSuchMethodException x) {
		            x.printStackTrace();
		          } catch (InstantiationException x) {
		            x.printStackTrace();
		          } catch (IllegalAccessException x) {
		            x.printStackTrace();
		          }
			}

			if(ls.size()==0){
				if(o != null){
					PropertyUtils.setProperty(o, "wellUid", wellUid);
					PropertyUtils.setProperty(o, "wellboreUid",wellboreUid);
					PropertyUtils.setProperty(o, "operationUid",operationUid);
					PropertyUtils.setProperty(o, "dailyUid",dailyUid);
					PropertyUtils.setProperty(o, "activityUid", activityUid);
					activityConverter = new CustomFieldUom(commandBean,Activity.class,"activityDuration");
					activityConverter.setBaseValueFromUserValue(activityDuration);
					double basevalue = activityConverter.getBasevalue();
					CustomFieldUom durationConverter = new CustomFieldUom(commandBean,ftr,"metricDuration");
					durationConverter.setBaseValue(basevalue);
					PropertyUtils.setProperty(o, "metricDuration", durationConverter.getConvertedValue());
					if(lsRemarksCode.size() > 0){
						CustomFtrLink ftrRemarksCode = lsRemarksCode.get(0);
						PropertyUtils.setProperty(o, "ftrCode",ftrRemarksCode.getRemarksCode());
					}
				}
			}else{
				o = ls.get(0);
				activityConverter = new CustomFieldUom(commandBean,Activity.class,"activityDuration");
				activityConverter.setBaseValueFromUserValue(activityDuration);
				double basevalue = activityConverter.getBasevalue();
				CustomFieldUom durationConverter = new CustomFieldUom(commandBean,ftr,"metricDuration");
				durationConverter.setBaseValue(basevalue);
				PropertyUtils.setProperty(o, "metricDuration", durationConverter.getConvertedValue());
				if(lsRemarksCode.size() > 0){
					CustomFtrLink ftrRemarksCode = lsRemarksCode.get(0);
					PropertyUtils.setProperty(o, "ftrCode",ftrRemarksCode.getRemarksCode());
				}
			}
			
			Field f;
			
			if (o != null){
				for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
					if (StringUtils.isNotBlank(fieldEntry.getKey())) {
						if(fieldEntry.getValue() != null){
							try{
								f = ftr.getDeclaredField(fieldEntry.getKey());
								if(f.getType().getSimpleName().equals("Double")){
									Double d = Double.parseDouble(fieldEntry.getValue().toString());
									PropertyUtils.setProperty(o, fieldEntry.getKey(),d);
								}else if(f.getType().getSimpleName().equals("Date")){
									try {
										SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
										Date date = sdf.parse(fieldEntry.getValue().toString());
										PropertyUtils.setProperty(o, fieldEntry.getKey(),date);
									} catch (ParseException ex) {
									    ex.printStackTrace();
									}
								}else{
									PropertyUtils.setProperty(o, fieldEntry.getKey(), fieldEntry.getValue());
								}
							}catch (Exception e) {
								e.printStackTrace();
					        }
						}else if(fieldEntry.getValue() == null){
							PropertyUtils.setProperty(o, fieldEntry.getKey(), null);
						}
					}
				}						
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(o);
			}
		}		
	}
	
	public static void deleteFTRKPI(String activityUid) throws Exception{
		String[] tableNameList = {"FtrTripping","FtrBopEquipment","FtrDrillLineEquipment","FtrRig","FtrDrilling","FtrWorkoverCompletionSystem"};
		List<Object> ftrList = null;
		for (String tableName : tableNameList){
			String queryString = "from " + tableName +" where (isDeleted is null or isDeleted = FALSE) and activityUid = :activityUid";
			ftrList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
			if (ftrList != null && ftrList.size() > 0) {
				for (int i = 0; i < ftrList.size(); i++) {
					Object o = ftrList.get(i);
					PropertyUtils.setProperty(o,"isDeleted", true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(o);
				}
			}
		}
	}
	
	public static void triggerUpdateFtrDuration(CommandBean commandBean,Object thisActivity) throws Exception{
		String[] tableNameList = {"FtrTripping","FtrBopEquipment","FtrDrillLineEquipment","FtrRig","FtrDrilling","FtrWorkoverCompletionSystem"};
		List<Object> ftrList = null;
		String[] paramsFields = {"operationUid","dailyUid","activityUid"};
		String classPathName = ApplicationUtils.getConfiguredInstance().getTablePackage();
		Activity a = null;
		NextDayActivity nda = null;
		if(thisActivity instanceof Activity){
			a = (Activity) thisActivity;
		}else if(thisActivity instanceof NextDayActivity){
			nda = (NextDayActivity) thisActivity;
		}
		Object[] paramsValues = null;
		if(a != null)
		{
			paramsValues = new Object[]{a.getOperationUid(),a.getDailyUid(),a.getActivityUid()};
		}else if (nda != null){
			paramsValues = new Object[]{nda.getOperationUid(),nda.getDailyUid(),nda.getActivityUid()};
		}
		
		String strSql;
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean,Activity.class,"activityDuration");
		for (String tableName : tableNameList){
			strSql = "FROM " + tableName + " WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND dailyUid=:dailyUid AND activityUid=:activityUid";
			if(paramsValues != null){
				ftrList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			}
			
			if (ftrList != null && ftrList.size() > 0) {
				for (int i = 0; i < ftrList.size(); i++) {
					Object o = ftrList.get(i);
					if(a != null){
						thisConverter.setBaseValueFromUserValue(a.getActivityDuration());
						double basevalue = thisConverter.getBasevalue();
						Class ftr = Class.forName(classPathName + tableName);
						CustomFieldUom durationConverter = new CustomFieldUom(commandBean,ftr,"metricDuration");
						durationConverter.setBaseValue(basevalue);
						PropertyUtils.setProperty(o,"metricDuration", durationConverter.getConvertedValue());
					}else if (nda != null){
						thisConverter.setBaseValueFromUserValue(nda.getActivityDuration()); 
						double basevalue = thisConverter.getBasevalue();
						Class ftr = Class.forName(classPathName + tableName);
						CustomFieldUom durationConverter = new CustomFieldUom(commandBean,ftr,"metricDuration");
						durationConverter.setBaseValue(basevalue);
						PropertyUtils.setProperty(o,"metricDuration", durationConverter.getConvertedValue());
					}	
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(o);
				}
			}
		}
	}
}
