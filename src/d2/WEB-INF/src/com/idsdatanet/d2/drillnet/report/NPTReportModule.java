package com.idsdatanet.d2.drillnet.report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.multiselect.TreeNodeMultiSelect;



public class NPTReportModule extends DefaultReportModule{
	
	public static String CUSTOM_PROPERTY_START_DATE = "CP_START_DATE";
	public static String CUSTOM_PROPERTY_END_DATE = "CP_END_DATE";
	public static String CUSTOM_PROPERTY_DATE_STRING = "CP_DATE_STRING";
	public static String WELL_FIELD = "wells";
	public static String FIELD_FIELD = "field";

	
	private Map<String,Integer> optionLimit;
	
	public void init(CommandBean commandBean){
		super.init(commandBean);
		if(commandBean instanceof BaseCommandBean){
			
			try {
				//commandBean.getRoot().getDynaAttr().put(WELL_FIELD, null);
				//commandBean.getRoot().getDynaAttr().put(FIELD_FIELD, null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	private void customFilterInvokedWellList(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception
	{
		String strStart = request.getParameter("startDate");
		String strEnd = request.getParameter("endDate");
		String strOperationArea = request.getParameter("operationArea");
		String strField = request.getParameter("field");
		String strCountry = request.getParameter("country");
		String strOperationType = request.getParameter("operationType");
		
		Date startDate = null;
		Date endDate = null;
		
		if (StringUtils.isNotBlank(strStart))
		{
			startDate = new Date();
			startDate.setTime(Long.valueOf(strStart));
		}
		if (StringUtils.isNotBlank(strEnd))
		{
			endDate = new Date();
			endDate.setTime(Long.valueOf(strEnd));
		}
		
		List<String> paramName = new ArrayList<String>();
		List<Object> paramValues = new ArrayList<Object>();
		paramName.add("startDate");
		paramName.add("endDate");
		paramValues.add(startDate);
		paramValues.add(endDate);
		String additionalFilter = "";
		
		if (StringUtils.isNotBlank(strOperationArea))
		{
			paramName.add("operationArea");
			paramValues.add(Arrays.asList(strOperationArea.split(",")));
			additionalFilter +=" AND o.operationArea in (:operationArea)";
		}
		if (StringUtils.isNotBlank(strField))
		{
			paramName.add("field");
			paramValues.add(Arrays.asList(strField.split(",")));
			additionalFilter +=" AND w.field in (:field)";
		}
		if (StringUtils.isNotBlank(strCountry))
		{
			paramName.add("country");
			paramValues.add(Arrays.asList(strCountry.split(",")));
			additionalFilter +=" AND w.country in (:country)";
		}
		if (StringUtils.isNotBlank(strOperationType))
		{
			paramName.add("operationType");
			paramValues.add(Arrays.asList(strOperationType.split(",")));
			additionalFilter +=" AND o.operationCode in (:operationType)";
		}
		if (startDate!=null && endDate!=null)
		{
			List<Object[]> opList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select DISTINCT w.wellUid, w.wellName From Operation o, Well w, Daily d WHERE (w.isDeleted is null or w.isDeleted = false) AND(o.isDeleted is null or o.isDeleted = false) AND (d.isDeleted is null or d.isDeleted = false) AND  (w.wellUid=o.wellUid) and (o.operationUid=d.operationUid) AND d.dayDate >=:startDate AND d.dayDate<=:endDate " +additionalFilter+" order by w.wellName ", paramName, paramValues);
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			
			for (Object[] well : opList)
			{
				String wellUid = String.valueOf(well[0]);
				String wellName = String.valueOf(well[1]);
				if (ApplicationUtils.getConfiguredInstance().getCachedAllWells().containsKey(wellUid))
				{
					writer.startElement("op");
					writer.addElement("code", wellUid);
					writer.addElement("label", wellName);
					writer.endElement();
				}
			}
			writer.endElement();
			writer.close();
		}else{
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);				
		}
	}
	private void customFilterInvokedFieldList(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception
	{
		String strStart = request.getParameter("startDate");
		String strEnd = request.getParameter("endDate");
		String strOperationArea = request.getParameter("operationArea");
		String strCountry = request.getParameter("country");
		String strOperationType = request.getParameter("operationType");
		
		Date startDate = null;
		Date endDate = null;
		
		if (StringUtils.isNotBlank(strStart))
		{
			startDate = new Date();
			startDate.setTime(Long.valueOf(strStart));
		}
		if (StringUtils.isNotBlank(strEnd))
		{
			endDate = new Date();
			endDate.setTime(Long.valueOf(strEnd));
		}
		
		List<String> paramName = new ArrayList<String>();
		List<Object> paramValues = new ArrayList<Object>();
		paramName.add("startDate");
		paramName.add("endDate");
		paramValues.add(startDate);
		paramValues.add(endDate);
		String additionalFilter = "";
		
		if (StringUtils.isNotBlank(strOperationArea))
		{
			paramName.add("operationArea");
			paramValues.add(Arrays.asList(strOperationArea.split(",")));
			additionalFilter +=" AND o.operationArea in (:operationArea)";
		}
		if (StringUtils.isNotBlank(strCountry))
		{
			paramName.add("country");
			paramValues.add(Arrays.asList(strCountry.split(",")));
			additionalFilter +=" AND w.country in (:country)";
		}
		if (StringUtils.isNotBlank(strOperationType))
		{
			paramName.add("operationType");
			paramValues.add(Arrays.asList(strOperationType.split(",")));
			additionalFilter +=" AND o.operationCode in (:operationType)";
		}
		if (startDate!=null && endDate!=null)
		{
			List<String> fieldList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select DISTINCT w.field From Operation o, Well w, Daily d WHERE (w.field is not null and w.field!='')and (w.isDeleted is null or w.isDeleted = false) AND(o.isDeleted is null or o.isDeleted = false) AND (d.isDeleted is null or d.isDeleted = false) AND  (w.wellUid=o.wellUid) and (o.operationUid=d.operationUid) AND d.dayDate >=:startDate AND d.dayDate<=:endDate " +additionalFilter+" order by w.field ", paramName, paramValues);
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			
			for (String field : fieldList)
			{
				writer.startElement("field");
				writer.addElement("code", field);
				writer.addElement("label", field);
				writer.endElement();
			}
			writer.endElement();
			writer.close();
		}else{
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);				
		}
	}
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("flexNPTReportWellList".equals(invocationKey)) {
			this.customFilterInvokedWellList(request, response, commandBean, invocationKey);
		} else if ("flexNPTReportFieldList".equals(invocationKey)) {
			this.customFilterInvokedFieldList(request, response, commandBean, invocationKey);
		}else{
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);
		}
	}
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		commandBean.getSystemMessage().clear();
		UserSelectionSnapshot userSelection=new UserSelectionSnapshot(session);
		
		Date startDate = null;
		Date endDate = null;
		
		
		Object objStartDate=commandBean.getRoot().getDynaAttr().get("startDate");
		if (objStartDate != null)
		{
			startDate =new Date();
			startDate.setTime(Long.parseLong(objStartDate.toString()));
		}
		
		Object objEndDate=commandBean.getRoot().getDynaAttr().get("endDate");
		if (objEndDate!= null )
		{
			endDate = new Date();
			endDate.setTime(Long.parseLong(objEndDate.toString()));
		}
		
		if (startDate !=null)
		{
			if (endDate==null || endDate.after(startDate) || endDate.equals(startDate))
			{
				endDate=endDate==null?startDate:endDate;
				
				userSelection.setCustomProperty(CUSTOM_PROPERTY_START_DATE, startDate);
				userSelection.setCustomProperty(CUSTOM_PROPERTY_END_DATE,endDate);
				String dateString=ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(startDate);
				if (!startDate.equals(endDate))
					dateString+= " - "+ ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(endDate);
				userSelection.setCustomProperty(CUSTOM_PROPERTY_DATE_STRING,dateString);
				Map<String,Object> reportOptions = this.getOptionAsReportOption(userSelection, request, commandBean);
				if (this.isMinimumOptionSelected(reportOptions))
				{
					if (this.validateReportOptionLimit(reportOptions))
					{
						userSelection.setCustomProperties(reportOptions);
						return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));
					}else{
						commandBean.getSystemMessage().addError("Some multi select option is over the selection limit", true);
					}
				}else{
					commandBean.getSystemMessage().addError("There is some selection that aren't selected at all", true);					
				}
				
				
				
			}else if (endDate.before(startDate))
			{
				commandBean.getSystemMessage().addError("End Date must be After Start Date");
			}
			
		}else{
			commandBean.getSystemMessage().addError("Please Enter Start Date");
		}
		return null;
	}
	
	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception{
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		String dateString = (String) userContext.getUserSelection().getCustomProperties().get(CUSTOM_PROPERTY_DATE_STRING);
		if(super.getOperationRequired() && operation == null) throw new Exception("Operation must be selected");
				
		return dateString +" "+ (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
	}	
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		
		return this.getReportType(userContext.getUserSelection()) + 
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize));
	}
	
	protected Map<String,Object> getOptionAsReportOption(UserSelectionSnapshot userSelection,HttpServletRequest request, CommandBean commandBean) throws Exception
	{
		Map<String,Object> mapDyn = commandBean.getRoot().getDynaAttr();
		List entryDynList = new LinkedList(mapDyn.entrySet());
		Map mapLookup = ((BaseCommandBean) commandBean).getLookup();
		
		Map<String,List<String>> mapMultiSelect = commandBean.getRoot().getMultiSelect().getValues();
		List entryMultiList = new LinkedList(mapMultiSelect.entrySet());
		
		Map<String,TreeNodeMultiSelect> multiSelectList = commandBean.getRoot().getMultiSelect().getMultiSelectDefinition();
		
		Map<String,Object> rptOption = new LinkedHashMap<String,Object>();
		
		for (Iterator it = entryDynList.iterator(); it.hasNext();) {
			Map.Entry<String, Object> entry = (Map.Entry)it.next();
			String field = entry.getKey();
			Object value = entry.getValue();
			if (mapLookup.containsKey("@"+field))
			{
				Map<String, LookupItem> lookup = null;
				Object obj=mapLookup.get("@"+field);
				if (obj instanceof LookupHandler)
				{
					LookupHandler lh = (LookupHandler)obj;
					lookup = lh.getLookup(commandBean, commandBean.getRoot(), userSelection, request, null);
				}else
				{
					lookup = LookupManager.getConfiguredInstance().getLookup(obj.toString(), userSelection, null);
				}
				if (lookup!=null)
				{
					LookupItem item = lookup.get(value);
					if (item!=null)
						rptOption.put(field, ReportOption.createReportOption(value.toString(),item.getValue()));
				}
			}else{
				if (field.equalsIgnoreCase(WELL_FIELD))
				{
					List<String> val = Arrays.asList(value.toString().split(","));
					List<Well> wells = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From Well Where (isDeleted is null or isDeleted = false) and wellUid in (:wellUid)", "wellUid",val);
					Map<String,Object> multiResult = new LinkedHashMap<String,Object>();
					for (Well w :wells)
					{
						multiResult.put(w.getWellUid(), w.getWellName());
					}
					rptOption.put("@"+field,ReportOption.createMultiSelectReportOption(val, multiResult));
				}
				if (field.equalsIgnoreCase(FIELD_FIELD))
				{
					List<String> val = Arrays.asList(value.toString().split(","));
					Map<String,Object> multiResult = new LinkedHashMap<String,Object>();
					for (String f :val)
					{
						multiResult.put(f, f);
					}
					rptOption.put("@"+field,ReportOption.createMultiSelectReportOption(val, multiResult));
				}
			}
		}
		
		if (multiSelectList != null)
		{
			for (Entry<String, TreeNodeMultiSelect> entry : multiSelectList.entrySet())
			{
				String field = entry.getKey();
				Map<String,LookupItem> obj=((BaseCommandBean)commandBean).getMultiSelectLookupMap(null,field);
				if (obj!=null)
				{
					Map<String,Object> multiResult = new LinkedHashMap<String,Object>();
					List<String> values = mapMultiSelect.get(field);
					if (values!=null)
					{
						for (String item : values)
						{
							LookupItem lookup = obj.get(item);
							if (lookup!=null)
								multiResult.put(item, lookup.getValue());
						}
					}
					rptOption.put(field, ReportOption.createMultiSelectReportOption(values,multiResult));
				}
			}
		}
		
		
		return rptOption;
	}
	
	private boolean isMinimumOptionSelected(Map<String,Object> reportOptions)
	{
		for (Map.Entry<String, Object> entry : reportOptions.entrySet())
		{
			String field = entry.getKey();
			ReportOption op = (ReportOption) entry.getValue();
			if (op!=null && op.getIsMultiOption() && op.getValues().size()==0)
				return false;
		}
		return true;
	}
	
	private boolean validateReportOptionLimit(Map<String,Object> reportOptions)
	{
		if (this.optionLimit!=null)
		{
			for (Map.Entry<String, Integer> entry : optionLimit.entrySet())
			{
				String field = entry.getKey();
				Integer limit = entry.getValue();
				ReportOption op = (ReportOption) reportOptions.get(field);
				if (op!=null && op.getIsMultiOption() && op.getValues().size()> limit)
					return false;
			}
		}
		return true;
	}
	
	
	public void setOptionLimit(Map<String,Integer> optionLimit) {
		this.optionLimit = optionLimit;
	}

	public Map<String,Integer> getOptionLimit() {
		return optionLimit;
	}	
	
}
