package com.idsdatanet.d2.drillnet.casingSection;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CasingTally;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CasingSectionCommandBeanListener extends EmptyCommandBeanListener {
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);			
		thisConverter.setReferenceMappingField(CasingTally.class, "casingId");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "casingId", thisConverter.getUOMMapping());
		}
		
		thisConverter.setReferenceMappingField(CasingTally.class, "casingOd");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "casingOd", thisConverter.getUOMMapping());
		}
		
		thisConverter.setReferenceMappingField(CasingTally.class, "sectionBottomMdMsl");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "sectionBottomMdMsl", thisConverter.getUOMMapping());
		}
		
		thisConverter.setReferenceMappingField(CasingTally.class, "sectionTopMdMsl");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "sectionTopMdMsl", thisConverter.getUOMMapping());
		}
		
		thisConverter.setReferenceMappingField(CasingTally.class, "weight");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "weight", thisConverter.getUOMMapping());
		}
		
		thisConverter.setReferenceMappingField(CasingTally.class, "csgLength");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(CasingTallySummary.class, "csgLength", thisConverter.getUOMMapping());
		}
		
		thisConverter.dispose();
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		Map<String, CommandBeanTreeNode> children = commandBean.getRoot().getChild("CasingSection");
		for (Map.Entry<String, CommandBeanTreeNode> entry : children.entrySet()) {
			CommandBeanTreeNode node = entry.getValue();
			Object data = node.getData();
			if (data instanceof CasingSection) {
				CasingSection casingSection = (CasingSection)data;
				String hql = "from CasingTally where (isDeleted is null or isDeleted = 0) and casingSectionUid=:casingSectionUid order by sectionTopMdMsl asc";
				List<CasingTally> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "casingSectionUid", casingSection.getCasingSectionUid());
				Double counter = 0.0;
				if (result.size() > 0) {
					for (CasingTally casingTally : result) {
						casingTally.setEdmRunOrder(counter);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(casingTally);
						counter++;
					}
				}
			}
		}
	}
}
