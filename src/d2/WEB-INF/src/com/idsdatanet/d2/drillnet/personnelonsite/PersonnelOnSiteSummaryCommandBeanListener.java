package com.idsdatanet.d2.drillnet.personnelonsite;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class PersonnelOnSiteSummaryCommandBeanListener extends EmptyCommandBeanListener {
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
	
		Integer totalLocalPax = 0;
		Integer totalExpatPax = 0;
		
		String dailyUid = userSelection.getDailyUid();
		String strSql ="Select sum(pax) as localPax from PersonnelOnSite where nationality = 'local' and (isDeleted = false or isDeleted is null) and dailyUid =:dailyUid group by nationality";
		List lstLocalPaxResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", dailyUid);
		
		if (!lstLocalPaxResult.isEmpty())
		{
			Object thisResult = (Object) lstLocalPaxResult.get(0);
			
			if (thisResult != null) totalLocalPax = Integer.parseInt(thisResult.toString());
			
		}
		
		String strSql2 ="Select sum(pax) as expatPax from PersonnelOnSite where nationality = 'expat' and (isDeleted = false or isDeleted is null) and dailyUid =:dailyUid group by nationality";
		List lstExpatPaxResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "dailyUid", dailyUid);
		
		if (!lstExpatPaxResult.isEmpty())
		{
			Object thisResult = (Object) lstExpatPaxResult.get(0);
			
			if (thisResult != null) totalExpatPax = Integer.parseInt(thisResult.toString());
			
		}
		
		commandBean.getRoot().getDynaAttr().put("national", totalLocalPax);
		commandBean.getRoot().getDynaAttr().put("expatriate", totalExpatPax);
	}
}
