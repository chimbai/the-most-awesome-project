package com.idsdatanet.d2.drillnet.reportDaily;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @mstan
 * This class help to mine data for previous 7 days data
 * for cost sheet daily reporting use
 * 
 */

public class TotalWellNptReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get operation uid
		String wellUid = userContext.getUserSelection().getWellUid();
		Locale locale = userContext.getUserSelection().getLocale();
		Double nptDuration = 0.0;
		Double totalDuration = 0.0;
		//get total npt in the well
		
		
		String sql = "SELECT SUM(activityDuration) FROM Activity " +
		"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
		"AND wellUid = :thisWellUid " +
		"AND (isSimop=false OR isSimop is null) " +
 		"AND (isOffline=false OR isOffline is null) " +
 		"AND (dayPlus IS NULL OR dayPlus=0) " +
		"AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='')";
		
		List<Double> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "thisWellUid", wellUid, qp);
		
		if(!result.isEmpty())
			totalDuration = result.get(0);
		
		sql = sql + " AND internalClassCode IN ('TP', 'TU', 'IP', 'IU')";
		
		result  = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "thisWellUid", wellUid, qp);
		
		if(!result.isEmpty())
			nptDuration = result.get(0);
		
		//get total activity duration in the well
		
		//add to report
		
		if(totalDuration == null)
			totalDuration = 0.0;
		if(nptDuration == null)
			nptDuration = 0.0;
		
		CustomFieldUom thisConverter = new CustomFieldUom(locale, Activity.class, "activity_duration");
		if (thisConverter !=null)
		{
			thisConverter.setBaseValue(totalDuration);
			totalDuration = thisConverter.getConvertedValue();
			thisConverter.setBaseValue(nptDuration);
			nptDuration = thisConverter.getConvertedValue();
		}
		
		ReportDataNode thisReportNode = reportDataNode.addChild("TotalNptHour");
		thisReportNode.addProperty("nptDuration", nptDuration.toString());
		thisReportNode.addProperty("totalActivityDuration", totalDuration.toString());
	}
}
