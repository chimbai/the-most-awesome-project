package com.idsdatanet.d2.drillnet.activity;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class ActivityForChildReportDataGenerator implements ReportDataGenerator {
	
	private String defaultReportType;
	
	public void setReportType(String value){
		this.defaultReportType = value;
	}

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		
		if(this.defaultReportType != null){
			reportType = this.defaultReportType;
		}
		
		String currentWellboreUid = userContext.getUserSelection().getWellboreUid();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		
		//Use the strWellboreList to retrieve all of the operations using an IN clause
		List <Operation> lstOperation = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM Operation WHERE (isDeleted = false or isDeleted is null) and wellboreUid = '" + currentWellboreUid + "' and operationUid != '" + currentOperationUid + "'");
		if(lstOperation.size() > 0){
			
			for(Operation objOperation: lstOperation){
				
				    String operationuid="";
					operationuid=objOperation.getOperationUid();
					String strSql = "SELECT a.rootCauseCode, a.classCode, a.phaseCode, a.taskCode, a.activityDescription, a.depthMdMsl, a.activityDuration, a.startDatetime, a.endDatetime, a.wellboreUid, a.wellUid, a.dayPlus, a.dailyUid, a.isSimop, a.isOffline, a.operationUid FROM Daily d, Activity a " +
					"WHERE (d.isDeleted = false or d.isDeleted is null) AND (a.isDeleted = false or a.isDeleted is null) AND d.dailyUid = a.dailyUid " +
					"AND d.dayDate = :userDate AND a.operationUid = :thisoperationUid AND ((a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) order by a.startDatetime";
					String[] paramsFields = {"userDate", "thisoperationUid"};
					Object[] paramsValues = {todayDate, operationuid};
					
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
					
					if (lstResult.size() > 0){
						
						ReportDataNode ChildWellReportNode = reportDataNode.addChild("ChildWellActivity");
						ChildWellReportNode.addProperty("operationUid", operationuid);
						for(Object objActivity: lstResult)
						{
							Object[] childWellReport = (Object[]) objActivity;
							
							String classCode = "";
							String phaseCode = "";
							String taskCode = "";
							String rootCauseCode = "";
							String activityDescription = "";
							Double depthMdMsl = 0.0;
							Double activityDuration = 0.0;
							String wellboreUid = "";
							String wellUid = "";
							Integer dayPlus = 0;
							String dailyUid = "";
							Integer isSimop = 0;
							Integer isOffline = 0;
							String operationUid = "";
							String depthUomSymbol="";
							
							if (childWellReport[0] != null && StringUtils.isNotBlank(childWellReport[0].toString()))rootCauseCode = childWellReport[0].toString();
							
							if (childWellReport[1] != null && StringUtils.isNotBlank(childWellReport[1].toString())) classCode = childWellReport[1].toString();
							if (childWellReport[2] != null && StringUtils.isNotBlank(childWellReport[2].toString())) phaseCode = childWellReport[2].toString();
							if (childWellReport[3] != null && StringUtils.isNotBlank(childWellReport[3].toString())) taskCode = childWellReport[3].toString();
							if (childWellReport[4] != null && StringUtils.isNotBlank(childWellReport[4].toString())) activityDescription = childWellReport[4].toString();
							
							if (childWellReport[5] != null && StringUtils.isNotBlank(childWellReport[5].toString())){
								depthMdMsl = Double.parseDouble(childWellReport[5].toString());
								
								CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
								thisConverter.setReferenceMappingField(Activity.class, "depthMdMsl");
								if (thisConverter.isUOMMappingAvailable())
								{
									thisConverter.setBaseValue(depthMdMsl);
									depthUomSymbol=thisConverter.getUomSymbol();
									depthMdMsl = thisConverter.getConvertedValue();
								}
							}
							if (childWellReport[6] != null && StringUtils.isNotBlank(childWellReport[6].toString())) {
								activityDuration = Double.parseDouble(childWellReport[6].toString());
								
								CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activityDuration");
								if (thisConverter.isUOMMappingAvailable())
								{
									thisConverter.setBaseValue(activityDuration);
									activityDuration = thisConverter.getConvertedValue();
								}
							}
							
							Date thisstartDatetime = (Date) childWellReport[7];
							Long startDatetimeEpoch = thisstartDatetime.getTime();
							
							Date thisendDatetime = (Date) childWellReport[8];
							Long endDatetimeEpoch = thisendDatetime.getTime();
							
							if (childWellReport[9] != null && StringUtils.isNotBlank(childWellReport[9].toString())) wellboreUid = childWellReport[9].toString();
							if (childWellReport[10] != null && StringUtils.isNotBlank(childWellReport[10].toString())) wellUid = childWellReport[10].toString();
							if (childWellReport[11] != null && StringUtils.isNotBlank(childWellReport[11].toString())) dayPlus =  Integer.parseInt(childWellReport[11].toString());
							if (childWellReport[12] != null && StringUtils.isNotBlank(childWellReport[12].toString())) dailyUid =  childWellReport[12].toString();
							if (childWellReport[13] != null && StringUtils.isNotBlank(childWellReport[13].toString())) isSimop =  Integer.parseInt(childWellReport[13].toString());
							if (childWellReport[14] != null && StringUtils.isNotBlank(childWellReport[14].toString())) isOffline =  Integer.parseInt(childWellReport[14].toString());
							if (childWellReport[15] != null && StringUtils.isNotBlank(childWellReport[15].toString())) operationUid =  childWellReport[15].toString();
							
							ReportDataNode thisReportNode = reportDataNode.addChild("ActivityForChild");
							
							thisReportNode.addProperty("rootCauseCode", rootCauseCode);
							thisReportNode.addProperty("classCode", classCode);
							thisReportNode.addProperty("phaseCode", phaseCode);
							thisReportNode.addProperty("taskCode", taskCode);
							thisReportNode.addProperty("activityDescription", activityDescription);
							thisReportNode.addProperty("depthMdMsl", depthMdMsl.toString());
							thisReportNode.addProperty("activityDuration", activityDuration.toString());
							thisReportNode.addProperty("startDatetime", startDatetimeEpoch.toString());
							thisReportNode.addProperty("endDatetime", endDatetimeEpoch.toString());
							thisReportNode.addProperty("wellboreUid", wellboreUid);
							thisReportNode.addProperty("wellUid", wellUid);
							thisReportNode.addProperty("dayPlus", dayPlus.toString());
							thisReportNode.addProperty("dailyUid", dailyUid);
							thisReportNode.addProperty("isSimop", isSimop.toString());
							thisReportNode.addProperty("isOffline", isOffline.toString());
							thisReportNode.addProperty("operationUid", operationUid);
							thisReportNode.addProperty("depthUomSymbol", depthUomSymbol);
						}
					}
			}
		}		
	}
}
