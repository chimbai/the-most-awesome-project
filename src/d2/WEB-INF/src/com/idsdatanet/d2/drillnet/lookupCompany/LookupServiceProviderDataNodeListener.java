package com.idsdatanet.d2.drillnet.lookupCompany;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.LookupCompanyService;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LookupServiceProviderDataNodeListener extends EmptyDataNodeListener {
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof LookupCompany) {
			LookupCompany myCompany = (LookupCompany)object;
			String myCompanyName = myCompany.getCompanyName();
			
			if(myCompanyName != null ){
				
				String strSql = "FROM LookupCompany WHERE (isDeleted = false or isDeleted is null) and lookupCompanyUid <> :lookupCompanyUid AND companyName = :companyName";
				String[] paramsFields = {"lookupCompanyUid", "companyName"};
				String[] paramsValues = {(myCompany.getLookupCompanyUid()==null?"":myCompany.getLookupCompanyUid()), myCompanyName};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (lstResult.size() > 0){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "companyName", "This Company Name already exist.");
				}
			}
			myCompany.setIsNewCompany(null);
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		
		if(object instanceof LookupCompany) {
			LookupCompany myCompany = (LookupCompany)object;

			if(myCompany!=null){
				String serviceCode = "NCR_SVCCO";
				String strSql = "FROM LookupCompanyService WHERE (isDeleted = false or isDeleted is null) and lookupCompanyUid = :lookupCompanyUid AND code = :code";
				String[] paramsFields = {"lookupCompanyUid", "code"};
				String[] paramsValues = {(myCompany.getLookupCompanyUid()==null?"":myCompany.getLookupCompanyUid()), serviceCode};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (lstResult.size() > 0){
				}else{
					//add new service
					LookupCompanyService companyService = new LookupCompanyService();
					companyService.setLookupCompanyUid(myCompany.getLookupCompanyUid());
					companyService.setCode(serviceCode);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(companyService);
				}
				
			}

		}
	}
}
