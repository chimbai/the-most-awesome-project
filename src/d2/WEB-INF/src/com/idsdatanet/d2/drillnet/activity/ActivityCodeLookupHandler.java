package com.idsdatanet.d2.drillnet.activity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.ActivityDescriptionMatrix;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityCodeLookupHandler implements LookupHandler{

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {

		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();

 		String strSql = "from ActivityDescriptionMatrix where (isDeleted = false or isDeleted is null)";
 		String[] paramsFields = {};
 		Object[] paramsValues = {};
 		List<ActivityDescriptionMatrix> listActivityRemark = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
 		
 		if (listActivityRemark.size() > 0) {
 			for (ActivityDescriptionMatrix activityDescriptionMatrix : listActivityRemark) {
 				String clsCode = activityDescriptionMatrix.getClassShortCode();
				String phsCode = activityDescriptionMatrix.getPhaseShortCode();
				String tskCode = activityDescriptionMatrix.getTaskShortCode();
				String usrCode = activityDescriptionMatrix.getUserShortCode();
				String combinString = clsCode + ":" + phsCode + ":" + tskCode + ":" + usrCode;
				
 				LookupItem item = new LookupItem(combinString, combinString);
 				result.put(combinString, item);
 			}
 		}
		return result;
		
	}
	
}
