package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class DateRangeReportDataGenerator implements ReportDataGenerator {
	
	public static String CUSTOM_PROPERTY_START_DATE = "CP_START_DATE";
	public static String CUSTOM_PROPERTY_END_DATE = "CP_END_DATE";
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		
		Date startDate = null;
		Date endDate = null;
		
		Object objStart = userContext.getUserSelection().getCustomProperty(CUSTOM_PROPERTY_START_DATE);
		Object objEnd = userContext.getUserSelection().getCustomProperty(CUSTOM_PROPERTY_END_DATE);
		
		startDate = objStart!=null?(Date)objStart:this.getCurrentDate(userContext);
		endDate = objEnd!=null?(Date)objEnd:startDate;
		
		startDate = DateUtils.truncate(startDate, Calendar.DAY_OF_MONTH);
		endDate = new Date(DateUtils.truncate(endDate, Calendar.DAY_OF_MONTH).getTime()+86399999);
		
		generateActualData(userContext,startDate, endDate, reportDataNode);
		
		
	}

	private Date getCurrentDate(UserContext userContext) throws Exception
	{
		Daily currentDaily=null;
		String dailyUid = userContext.getUserSelection().getDailyUid();
		if (dailyUid!=null)
			currentDaily=ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		
		if (currentDaily!=null)
			return currentDaily.getDayDate();
		
		return new Date();
		
	}
	
	
	protected void generateActualData(UserContext userContext,Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		
	}
	
	

}
