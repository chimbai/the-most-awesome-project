package com.idsdatanet.d2.drillnet.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils; 
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.multiselect.ConcatenatedStringTreeNodeMultiSelect;

public class FilterableReportModule extends DefaultReportModule {
	
	private Boolean isFilterLookupMultiSelect=false;
	private Boolean isMultiSelectJobMerge=false;
	private Object filterLookup;
	public Object getFilterLookup() {
		return filterLookup;
	}

	public void setFilterLookup(Object filterLookup) {
		this.filterLookup = filterLookup;
	}

	public static String FILTER_LOOKUP_DYNAMIC_ATTRIBUTE="filterLookup";
	public static String FILTER_ATTRIBUTE_KEY="filterAttributeKey";
	public static String FILTER_ATTRIBUTE_LABEL="filterAttributeLabel";
	private String warningMessage;
	
	private class FilterLookupHandler implements LookupHandler {	
		private String filterLookupDefinition;
		FilterLookupHandler(String filterLookupDefinition){
			this.filterLookupDefinition=filterLookupDefinition;
		}		
				
		public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache)
				throws Exception {
			return LookupManager.getConfiguredInstance().getLookup(this.filterLookupDefinition,userSelection, null);
			
		}
	}
	
	private Map<String, LookupItem> getFilterLookupMap(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		if (this.filterLookup instanceof LookupHandler)
		{
			return ((LookupHandler)this.filterLookup).getLookup(commandBean, null, userSelection, request, null);
		}else{
			return LookupManager.getConfiguredInstance().getLookup(this.filterLookup.toString(),userSelection, null);
		}
	}
	
	//override the report generating function, if got multiple day selected construct the List of UserSelectionSnapShot and generate report	
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		commandBean.getSystemMessage().clear();
		UserSelectionSnapshot userSelection=new UserSelectionSnapshot(session);
		if (this.getIsFilterLookupMultiSelect())
		{
			List<String> filterAttributeKeyList = commandBean.getRoot().getMultiSelect().getValues().get("@"+FILTER_LOOKUP_DYNAMIC_ATTRIBUTE);
			
			if (filterAttributeKeyList!=null && filterAttributeKeyList.size()>0) {
				if (!this.getIsMultiSelectJobMerge())
				{
					List<UserSelectionSnapshot> selectionList=new ArrayList<UserSelectionSnapshot>();
					for (String key:filterAttributeKeyList)
					{
						UserSelectionSnapshot selection=new UserSelectionSnapshot(session);
						
						String label=(String) this.getFilterLookupMap(commandBean, userSelection, request).get(key).getValue();
						selection.setCustomProperty(FILTER_ATTRIBUTE_KEY, key);
						selection.setCustomProperty(FILTER_ATTRIBUTE_LABEL,label);
						selectionList.add(selection);
					}
					return super.submitReportJob(selectionList, this.getParametersForReportSubmission(session, request, commandBean));
				}else{
					String filterAttributeLabel=null;
					for (Object key:filterAttributeKeyList)
					{
						String label=(String) this.getFilterLookupMap(commandBean, userSelection, request).get(key).getValue();
						filterAttributeLabel+=filterAttributeLabel==null?label:(" "+label);
						
					}
					userSelection.setCustomProperty(FILTER_ATTRIBUTE_KEY, filterAttributeKeyList);
					userSelection.setCustomProperty(FILTER_ATTRIBUTE_LABEL,filterAttributeLabel);		
					return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));
				}
			}else
			{
				commandBean.getSystemMessage().addWarning(this.getWarningMessage());
				return null;
			}	
		}else{
			String filterAttributeKey = (String) commandBean.getRoot().getDynaAttr().get(FILTER_LOOKUP_DYNAMIC_ATTRIBUTE);
			if (!StringUtils.isBlank(filterAttributeKey)) {
				userSelection.setCustomProperty(FILTER_ATTRIBUTE_KEY, filterAttributeKey);
				userSelection.setCustomProperty(FILTER_ATTRIBUTE_LABEL,this.getFilterLookupMap(commandBean, userSelection, request).get(filterAttributeKey).getValue());
				return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));
			}else
			{
				commandBean.getSystemMessage().addWarning(this.getWarningMessage());
				return null;
			}	
		}
			
	}
	
	//override
	protected ReportJobParams submitReportJobWithEmail(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		return submitReportJobWithGenerateAndSent(session, request, commandBean);
	}
	
	public void init(CommandBean commandBean){
		super.init(commandBean);
		
		if(commandBean instanceof BaseCommandBean){
			Map lookup = new HashMap();
			if (this.getIsFilterLookupMultiSelect())
			{
				ConcatenatedStringTreeNodeMultiSelect multi=new ConcatenatedStringTreeNodeMultiSelect();
				if (this.filterLookup instanceof LookupHandler)
					multi.setLookupHandler((LookupHandler)this.filterLookup);
				else
					multi.setLookup(this.filterLookup.toString());
				
				lookup.put("@"+FILTER_LOOKUP_DYNAMIC_ATTRIBUTE, multi);
				((BaseCommandBean) commandBean).setMultiSelect(lookup);
			}else{
				
				lookup.put("@"+FILTER_LOOKUP_DYNAMIC_ATTRIBUTE, this.filterLookup instanceof LookupHandler ? (LookupHandler) this.filterLookup:new FilterLookupHandler(this.filterLookup.toString()));
				((BaseCommandBean) commandBean).setLookup(lookup);

			}
		}
	}

	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception{
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
			
		if(super.getOperationRequired() && operation == null) throw new Exception("Operation must be selected");
		
		
		if(operation == null) throw new Exception("Unable to generate output file name");
		
		return (operation == null ? "" : operation.getOperationName()) + " " + (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
	}	
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		String operation_uid = userContext.getUserSelection().getOperationUid();
		if(super.getOperationRequired() && StringUtils.isBlank(operation_uid)){
			throw new Exception("Operation must be selected");
		}	
		
		Object obj = userContext.getUserSelection().getCustomProperties().get(FILTER_ATTRIBUTE_KEY);
		String filterAttributeKey=null;
		if (this.getIsFilterLookupMultiSelect() && this.getIsMultiSelectJobMerge())
		{
			filterAttributeKey="multi";
		}else{
			filterAttributeKey=obj.toString();
		}
		
		return this.getReportType(userContext.getUserSelection()) + 
			(StringUtils.isBlank(operation_uid) ? "" : "/" + operation_uid) + 
			(StringUtils.isBlank(filterAttributeKey) ? "" : "/" + filterAttributeKey) + 
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize));
	}
	
	
	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}

	public String getWarningMessage() {
		return warningMessage;
	}

	public void setIsFilterLookupMultiSelect(Boolean isFilterLookupMultiSelect) {
		this.isFilterLookupMultiSelect = isFilterLookupMultiSelect;
	}

	public Boolean getIsFilterLookupMultiSelect() {
		return isFilterLookupMultiSelect;
	}

	public void setIsMultiSelectJobMerge(Boolean isMultiSelectJobMerge) {
		this.isMultiSelectJobMerge = isMultiSelectJobMerge;
	}

	public Boolean getIsMultiSelectJobMerge() {
		return isMultiSelectJobMerge;
	}	
}
