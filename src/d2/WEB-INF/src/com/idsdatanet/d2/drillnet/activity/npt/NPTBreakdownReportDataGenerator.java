package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.report.ReportOption;

public class NPTBreakdownReportDataGenerator extends NPTReportDataGenerator{


	
	protected void generateActualData(UserContext userContext, Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		List<NPTSummary> includedList = this.shouldIncludeWell(userContext, startDate, endDate);
		this.includeQueryData(includedList, reportDataNode, userContext.getUserSelection(), startDate, endDate);
	}
	
	private List<NPTSummary> generatedata(UserContext userContext, Date startDate, Date endDate, String includeWellUid) throws Exception
	{

		String[] paramNames = {"startDate","endDate","companyNPTClassCode","otherNPTClassCode","field","wellUid"};
		Object[] values = {startDate,endDate,this.getCompanyNPTClassCode(),this.getOtherNPTClassCode(),BHIQueryUtils.getReportOptionValue(userContext,BHIQueryUtils.CONSTANT_FIELD),includeWellUid};
		String sql = "SELECT DISTINCT w.wellUid,w.wellName, o.operationName, w.field, w.country, sum(a.activityDuration) as duration, wb.wellboreName, wb.wellboreUid"+		
			" FROM Well w,Wellbore wb, Operation o, Activity a, Daily d "+ 
			" Where w.wellUid=wb.wellUid and wb.wellboreUid=o.wellboreUid"+
			" and d.operationUid=o.operationUid"+ 
			" and a.dailyUid=d.dailyUid"+
			" and (a.isDeleted is null or a.isDeleted = False)"+
			" and (d.isDeleted is null or d.isDeleted = False)"+
			" and (w.isDeleted is null or w.isDeleted = false) and (wb.isDeleted is null or wb.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false)"+
			" and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) "+
			" and (a.classCode=:companyNPTClassCode or a.classCode=:otherNPTClassCode)"+ 
			" and (a.startDatetime>=:startDate and a.endDatetime<=:endDate)"+ 						
			" and w.wellUid=:wellUid"+
			" and (w.field in (:field))"+
			" group by w.field, w.wellUid, wb.wellboreUid"+
			" order by w.field, w.wellUid, wb.wellboreUid";
		
		List<NPTSummary> includedList = new ArrayList<NPTSummary>();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, values, qp);
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
		for (Object[] item :result)
		{
			String wellUid = (String)item[0];
			
			NPTSummary npt = new NPTSummary((String)item[0],null, (String)item[4], null, null,null,null,null);			
			npt.setWellName((String)item[1]);
			npt.setOperationName((String)item[2]);		
			npt.setField((String)item[3]);			
			npt.setDuration(Double.parseDouble(String.valueOf(item[5])));
			thisConverter.setBaseValue(npt.getDuration());
			npt.setDuration(thisConverter.getConvertedValue());
			npt.setWellboreName((String)item[6]);	
			npt.setWellboreUid((String)item[7]);	
			
			includedList.add(npt);
		}
		return includedList;
		
	}
	
	private List<NPTSummary> shouldIncludeWell(UserContext userContext, Date startDate, Date endDate) throws Exception
	{
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		ReportOption wells = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL):null;
		List<NPTSummary> result = new ArrayList<NPTSummary>();
		for (String wellUid : wells.getValues())
		{
			result.addAll(this.generatedata(userContext, startDate, endDate,wellUid));
		}
		return result;
	}

	private void includeQueryData(List<NPTSummary> nptSummary, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception
	{			
		ReportDataNode nodeChild = node.addChild("NPTBreakdown");		
		Double totalNPT =0.00;
		for (NPTSummary npt : nptSummary)
		{	
			ReportDataNode catChild = nodeChild.addChild("data");
			catChild.addProperty("welluid", npt.getWellUid());
			catChild.addProperty("wellname", npt.getWellName());
			catChild.addProperty("wellborename", npt.getWellboreName());
			catChild.addProperty("wellboreuid", npt.getWellboreUid());
			catChild.addProperty("operationname", npt.getOperationName());
			catChild.addProperty("field",npt.getField());			
			catChild.addProperty("NPTduration", String.valueOf(npt.getDuration()));
			totalNPT += npt.getDuration();
		}		
		nodeChild.addProperty("totalNPTDuration", String.valueOf(totalNPT));
	}	
}
