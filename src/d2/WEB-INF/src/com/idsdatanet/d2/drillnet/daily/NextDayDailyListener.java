package com.idsdatanet.d2.drillnet.daily;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class NextDayDailyListener  implements DataLoaderInterceptor {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		Daily tomorrowDaily = ApplicationUtils.getConfiguredInstance().getTomorrow(userSelection.getOperationUid(), userSelection.getDailyUid());
		if (tomorrowDaily!=null) {
			customCondition = "dailyUid = :tomorrowDailyUid ";
			query.addParam("tomorrowDailyUid", tomorrowDaily.getDailyUid());
		} else {
			customCondition = " 1 = 2";
		}
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean arg0, UserSelectionSnapshot arg1, HttpServletRequest arg2, TreeModelDataDefinitionMeta arg3, DataDefinitionHQLQuery arg4, CommandBeanTreeNode arg5) throws Exception {
		return null;
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
}
