package com.idsdatanet.d2.drillnet.reportDaily;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @author 
 * This class help to mine data for previous 7 days data
 * for cost sheet daily reporting use
 * 
 */

public class Previous7DaysReportDailyDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		
		Double prev_depth = 0.00;
		Double current_depth = 0.00;
		
		//get operation uid
		String operationUid = userContext.getUserSelection().getOperationUid();			
		
		//get current day date
		String currentDailyUid = userContext.getUserSelection().getDailyUid();
		String thisOpsReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		Daily currentDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (currentDaily==null) return;
		Date today_date = currentDaily.getDayDate();
		String todayDailyUid = currentDaily.getDailyUid();
		
		ReportDaily currentReportDaily = ApplicationUtils.getConfiguredInstance().getReportDaily(todayDailyUid, thisOpsReportType);
		current_depth = currentReportDaily.getDepthMdMsl();
		
		String sql = "SELECT depthMdMsl FROM ReportDaily " +
		"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
		"AND operationUid = :thisOpsUid " +
		"AND reportDatetime < :thisDate " +
		"ORDER BY reportDatetime DESC";
		
		String[] paramsFields = {"thisOpsUid", "thisDate"};
		Object[] paramsValues = {operationUid, today_date};
		//get current depth
		//current_depth=currentDaily.getDepthMdMsl();		
		//ReportDaily currentDepth = ApplicationUtils.getConfiguredInstance().getReportDailyByOperationType(currentDailyUid, operationUid);
		//if (currentDepth==null) return;
		//current_depth = currentDepth.getDepthMdMsl();
		
		List <Double> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		
		if (!lstResult.isEmpty()){
			int dayCount = lstResult.size();
			if (dayCount >= 7)
				prev_depth = lstResult.get(6);
			else
				prev_depth = lstResult.get(dayCount - 1);
		}
		
		String DepthUomSymbol="";
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
		thisConverter.setReferenceMappingField(ReportDaily.class, "depthMdMsl");
		
		if (thisConverter.isUOMMappingAvailable())
		{
			DepthUomSymbol=thisConverter.getUomSymbol();
		}
		
		DateFormat df = new SimpleDateFormat("dd MMM yyyy");
		ReportDataNode thisReportNode = reportDataNode.addChild("Prev7daysReportDaily");
		thisReportNode.addProperty("operationUid", operationUid);
		thisReportNode.addProperty("currentDate", df.format(today_date).toString());
		
		String currentDepthStr = null;
		String prevDepthStr = null;
		
		if(current_depth != null)
			currentDepthStr = thisConverter.formatOutputPrecision(current_depth);
		
		if(prev_depth != null)
			prevDepthStr = thisConverter.formatOutputPrecision(prev_depth);
		
		if(current_depth != null)
			thisReportNode.addProperty("currentDepth", currentDepthStr.replace(",", ""));
		else
			thisReportNode.addProperty("currentDepth", "0.0");
		
		if(prev_depth != null)
			thisReportNode.addProperty("prevDepth", prevDepthStr.replace(",", ""));
		else
			thisReportNode.addProperty("prevDepth", "0.0");
		
		thisReportNode.addProperty("depthUomSymbol", DepthUomSymbol);
		
	}
}
