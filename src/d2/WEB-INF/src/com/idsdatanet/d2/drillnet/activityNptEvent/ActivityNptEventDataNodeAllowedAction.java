package com.idsdatanet.d2.drillnet.activityNptEvent;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ActivityNptEventDataNodeAllowedAction implements DataNodeAllowedAction {

	public boolean allowedAction(CommandBeanTreeNode node, UserSession session,
			String targetClass, String action, HttpServletRequest request)
			throws Exception {
		
		if ("Activity".equals(targetClass) || node.getData() instanceof Activity) {
			return false;
		}
		
		return true;
	}

}
