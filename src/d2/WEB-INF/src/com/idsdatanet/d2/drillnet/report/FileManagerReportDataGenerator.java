package com.idsdatanet.d2.drillnet.report;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.FileManagerFiles;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.BaseFormController;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class FileManagerReportDataGenerator extends DefaultBeanSupport implements ReportDataGenerator {

	private HashMap<String, BaseCommandBean> controllerToCommandBean = new HashMap<String, BaseCommandBean> ();
	
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub

	}

	public void generateData(UserContext userContext, ReportDataNode reportNode,
			String reportType, ReportValidation validation) throws Exception {
		UserSelectionSnapshot selection = userContext.getUserSelection();
		
		ReportDataNode currentModuleReportNode = null;
		String currentController = null;
		
		List<FileManagerFiles> files = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From FileManagerFiles "
				+ "Where ( isDeleted = false or isDeleted = null) "
				+ "and attachToModule is not null "
				+ "and attachToDaily=:dailyUid "
				+ "order by attachToModule, fileName", "dailyUid", selection.getDailyUid());
		
		for (FileManagerFiles file: files){
			String module = file.getAttachToModule();
			if (StringUtils.isBlank(currentController) || (StringUtils.isNotBlank(currentController) && currentController != file.getAttachToModule())){
				BaseCommandBean cb = this.getCommandBean(file.getAttachToModule(), selection);
				if (cb != null){
					currentModuleReportNode = reportNode.addChild("Modules");
					currentModuleReportNode.addProperty("title", cb.getInfo().getTitle());
				}
			}
			if (currentModuleReportNode != null)
				currentModuleReportNode.addChild(file);
		}

	}

	public BaseCommandBean getCommandBean(String controllerId, UserSelectionSnapshot userSelection) throws Exception{
		BaseCommandBean cb = this.controllerToCommandBean.get(controllerId);
		if (cb != null)
			return cb;
		
		Object beanController = this.getApplicationContext().getBean(controllerId,BaseFormController.class);
		if (beanController instanceof BaseFormController){
			BaseFormController controller = (BaseFormController) beanController;
			String commandBeanId = null;
			if (controller.getCommandBeanResolver()!=null){
				commandBeanId = controller.getCommandBeanResolver().resolveBeanName(userSelection);
			}else{
				commandBeanId = controller.getCommandBeanId();
			}
			if (StringUtils.isNotBlank(commandBeanId)){
				Object beanBaseCommandBean = this.getApplicationContext().getBean(commandBeanId,BaseCommandBean.class);
				if (beanBaseCommandBean instanceof BaseCommandBean){
					BaseCommandBean baseCommandBean = (BaseCommandBean) beanBaseCommandBean;
					this.controllerToCommandBean.put(controllerId, baseCommandBean);
					return baseCommandBean;
				}
			}
		}
		return null;
	}

}
