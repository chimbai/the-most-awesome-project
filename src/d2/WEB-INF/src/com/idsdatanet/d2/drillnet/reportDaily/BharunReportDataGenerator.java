package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;




public class BharunReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub

		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily==null) return;
	
		if (daily!=null) {
			String queryString = "FROM Bharun b, Operation o " +
					"WHERE (b.isDeleted=false or b.isDeleted is null) " +
					"AND (o.isDeleted=false or o.isDeleted is null) " +
					"AND b.operationUid=o.operationUid " +
					"AND b.operationUid=:operationUid " +
					"ORDER BY b.depthInMdMsl DESC";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"operationUid"}, new Object[] {userContext.getUserSelection().getOperationUid()});
			for (Object[] rec : list) {
				Boolean show = false;
				Bharun bharun = (Bharun) rec[0];
				if (bharun.getDailyidIn()!=null && StringUtils.isNotBlank(bharun.getDailyidIn())){
					Daily bharun_in = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, bharun.getDailyidIn());
					if (bharun_in!=null){
						if (bharun_in.getDayDate().getTime() <= daily.getDayDate().getTime()){
							show = true;
						}
					}
				}

				if (show){
					show = false;
					if (bharun.getDailyidOut()!=null && StringUtils.isNotBlank(bharun.getDailyidOut())){
						Daily bharun_out = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, bharun.getDailyidOut());
						if (bharun_out!=null){
							if (bharun_out.getDayDate().getTime() >= daily.getDayDate().getTime()){
								show = true;
							}
						}
					}else{
						show = true;
					}
				}
				
				if (show){
					ReportDataNode thisReportNode = reportDataNode.addChild("Bharun");
					thisReportNode.addProperty("bhaType", bharun.getBhaType());
					thisReportNode.addProperty("drillStringSummaryConnectionType", bharun.getDrillStringSummaryConnectionType());	
					
					CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Bharun.class, "bhaTotalLength");
					if (thisConverter !=null && bharun.getBhaTotalLength()!=null){	
						thisConverter.setBaseValue(bharun.getBhaTotalLength());						
						thisReportNode.addProperty("bhaTotalLength", thisConverter.formatOutputPrecision());
					}else{
						thisReportNode.addProperty("bhaTotalLength", "");
					}
					
					thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Bharun.class, "drillStringSummaryMaxOd");
					if (thisConverter !=null && bharun.getDrillStringSummaryMaxOd()!=null){	
						thisConverter.setBaseValue(bharun.getDrillStringSummaryMaxOd());						
						thisReportNode.addProperty("drillStringSummaryMaxOd", thisConverter.formatOutputPrecision());
					}else{
						thisReportNode.addProperty("drillStringSummaryMaxOd", "");
					}
					
					thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Bharun.class, "depthInMdMsl");
					if (thisConverter !=null && bharun.getDepthInMdMsl()!=null){	
						thisConverter.setBaseValue(bharun.getDepthInMdMsl());						
						thisReportNode.addProperty("depthInMdMsl", thisConverter.formatOutputPrecision());
					}else{
						thisReportNode.addProperty("depthInMdMsl", "");
					}
				}
				
			}
		}
		
	}
	
}
