package com.idsdatanet.d2.drillnet.lookupProductList;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.LookupProductList;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LookupProductListDataLoaderInterceptor implements DataLoaderInterceptor {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "(isDeleted = false or isDeleted is null) and ";
		
		String strVendorUid = (String) commandBean.getRoot().getDynaAttr().get("vendorCompany");
		String strRegion = (String) commandBean.getRoot().getDynaAttr().get("wellRegion");
		
		if (meta.getTableClass().equals(LookupProductList.class)) {
			if (StringUtils.isNotBlank(strVendorUid)) {
				customCondition += "mudVendorCompanyUid = :companyUid";
				query.addParam("companyUid", strVendorUid);	
			}else {
				customCondition += "mudVendorCompanyUid is null";
			}
			
			if (StringUtils.isNotBlank(strRegion)) {
				customCondition += " and region = :region";
				query.addParam("region", strRegion);	
			}else {
				customCondition += " and region is null";
			}
		}
				
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
}
