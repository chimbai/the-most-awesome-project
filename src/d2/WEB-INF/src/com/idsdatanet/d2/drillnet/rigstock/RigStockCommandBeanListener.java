package com.idsdatanet.d2.drillnet.rigstock;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.common.util.ExcelImportTemplateManager;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.witsml.wellit.rigstock.WellITPortBulkRob;
import com.idsdatanet.depot.witsml.wellit.rigstock.WellITRigStockScreenImport;
import com.idsdatanet.depot.witsml.wellit.rigstock.WellITRigStockUtils;


/* for sites which are using cascade lookup on stock name and unit (ticket 20110308-0915-scwong), please follow the field mapping as below:
 *  - map Stock Name column to @stockName
 *  - map Unit to stockCode (to keep the lookupRigStockUid in order to calculate the balance and cost) 
 *  - can refer to override commandbean in santos_csm_rig_stock under deployment folder and rigStock_alt_p1.xml
 */


public class RigStockCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {
	private String stockType;
    private Boolean isCombined = false;

    private String itemCostLookupReference;
	
    private ExcelImportTemplateManager excelImportTemplateManager = null;

    @Override
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
        String action = request.getParameter("action");
        if (StringUtils.equals(ExcelImportTemplateManager.IMPORT_TEMPLATE_EXCEL, action)) {
            this.excelImportTemplateManager.importExcelFile((BaseCommandBean) commandBean, this, request, request.getParameter(ExcelImportTemplateManager.KEY_TEMPLATE), request.getParameter(ExcelImportTemplateManager.KEY_FILE), "RigStock");
        }else if (StringUtils.equals("importWellIT",action)) {
        	this.importWellIT(request,commandBean,targetCommandBeanTreeNode);
        }else if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigStock.class) || targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(FluidStock.class) || targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(SupplementaryStock.class)) || targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(MudStock.class)) {
			CustomFieldUom thisConverter = new CustomFieldUom(targetCommandBeanTreeNode.getCommandBean(), RigStock.class, "amt_start");
			if("stockCode".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				stockCodeChanged(request, commandBean, targetCommandBeanTreeNode, thisConverter);
			}
		}
	}

    public void stockCodeChanged(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node, CustomFieldUom thisConverter) throws Exception {
		if(Action.DELETE.equals(node.getAtts().getAction())) return; //why do we need to test for "delete" ? ssjong - 04/may/2009
		
		RigStock thisRigStock = (RigStock) node.getData();
		if (isCombined){
			stockType = RigStockUtil.getRigStockType(node);
		}
		UserSession userSession = UserSession.getInstance(request);
		
		//show the correct unit as well
		String stockcode = thisRigStock.getStockCode();
		
		if (StringUtils.isNotBlank(stockcode)) {
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select storedUnit from LookupRigStock where (isDeleted = false or isDeleted is null) and lookupRigStockUid = :stockCode", "stockCode", stockcode);
			if (lstResult.size() > 0) thisRigStock.setStoredUnit(lstResult.get(0).toString());
		}
		
		//check if today is the first day of operation or not to determine want to show the starting amount
		node.getDynaAttr().put("isFirstDay", CommonUtil.getConfiguredInstance().getOperationStockcodeFirstDay(userSession.getCurrentOperationUid(), thisRigStock.getStockCode(), userSession.getCurrentDailyUid()));
		RigStockUtil.setIsBatchDrillFlag(node, new UserSelectionSnapshot(UserSession.getInstance(request)));
		// default 'previousBalance' to 0.0
		Double previousBalance = 0.0;
						
		// get the current date from the session
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSession.getCurrentDailyUid());
		
		previousBalance = CommonUtil.getConfiguredInstance().getOperationStockcodePreviousBalance(userSession.getCurrentOperationUid(), userSession.getCurrentRigInformationUid(), stockcode, daily, this.stockType);
		thisConverter.setBaseValue(previousBalance);
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@previousBalance", thisConverter.getUOMMapping());
		}
		node.getDynaAttr().put("previousBalance", thisConverter.getConvertedValue());	

		// auto populate item cost from table configured in command bean 
		if (StringUtils.isNotBlank(stockcode)) {
			thisRigStock.setItemcost(RigStockUtil.getItemCost(stockcode, this.itemCostLookupReference, userSession));
		}
	}	

	@Override
    public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		String stockType = (String) commandBean.getRoot().getDynaAttr().get("rigStockType");
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RigStock.class, "dailycost");
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		
		Double totalDaily = CommonUtil.getConfiguredInstance().calculateDailyRigStockCost(userSelection.getOperationUid(), stockType, daily);
		Double cumTotal = CommonUtil.getConfiguredInstance().calculateCummulativeRigStockCost(userSelection.getOperationUid(), stockType, daily);
		commandBean.getRoot().getDynaAttr().put("totalDaily", thisConverter.formatOutputPrecision(totalDaily == null ? 0 : totalDaily));
		commandBean.getRoot().getDynaAttr().put("cumTotalCost", thisConverter.formatOutputPrecision(cumTotal == null ? 0 : cumTotal));
		if (thisConverter.isUOMMappingAvailable()){
			commandBean.getRoot().setCustomUOM("@totalDaily", thisConverter.getUOMMapping());
			commandBean.getRoot().setCustomUOM("@cumTotalCost", thisConverter.getUOMMapping());
		}
	}
	
    @Override
    public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
        if (ExcelImportTemplateManager.IMPORT_TEMPLATE_EXCEL.equalsIgnoreCase(invocationKey)) {
            this.excelImportTemplateManager.uploadFile(request, response, ExcelImportTemplateManager.KEY_FILE);
        }
        return;
    }

    public void setType(String type) {
		this.stockType = type;
	}
	
    public void setIsCombined(Boolean type) {
		this.isCombined = type;
	}
	
	/*
	 * To identify item cost should auto populate from which table
	 * value can be set as LookupRigStock , default (if property not set) will get item cost from Lookup Product List Table 
	 */
	public void setItemCostLookupReference(String value)
	{
		this.itemCostLookupReference = value;
	}

    public ExcelImportTemplateManager getExcelImportTemplateManager() {
        return excelImportTemplateManager;
    }

    public void setExcelImportTemplateManager(ExcelImportTemplateManager excelImportTemplateManager) {
        this.excelImportTemplateManager = excelImportTemplateManager;
    }

    public String getStockType() {
        return stockType;
    }

    public void setStockType(String stockType) {
        this.stockType = stockType;
    }

    public Boolean getIsCombined() {
        return isCombined;
    }
    
    private void importWellIT(HttpServletRequest request, CommandBean commandBean,CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
    	Object dataList = new WellITRigStockScreenImport().getDataFromWellIT(request,commandBean);
    	String supportVesselInformationUid = null;
    	this.importPortBulkRob(request,(ArrayList<Object>) dataList, commandBean, targetCommandBeanTreeNode);
	}
    
    private void importPortBulkRob(HttpServletRequest request, ArrayList<Object> dataList,CommandBean commandBean,CommandBeanTreeNode targetCommandBeanTreeNode) {
    	try {
    		commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
    		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RigStock.class, "amt_start");
        	CommandBeanTreeNodeImpl targetNode = (CommandBeanTreeNodeImpl) commandBean.getRoot();
    		UserSession userSession = UserSession.getInstance(request);
    		String dailyUid = userSession.getCurrentDailyUid();
    		String strSql = "FROM RigStock WHERE dailyUid = :dailyUid AND type = 'rigbulkstock' AND (supportVesselInformationUid IS NULL OR supportVesselInformationUid = '') AND (isDeleted IS NULL or isDeleted = FALSE) ORDER BY sequence DESC";
    		String[] paramsFields = {"dailyUid"};
    		Object[] paramsValues = {dailyUid};
    		List<RigStock> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			int sequence = 0;
			if(lstResult.size() > 0) {
				sequence = lstResult.get(0).getSequence();
			}
			
        	for(Object o : dataList) {
        		if (o instanceof WellITPortBulkRob) {
        			WellITPortBulkRob data = (WellITPortBulkRob) o;
	        		String lookupRigStockUid = null;
	        		String[] paramsFields2 = {"stockCode","storedUnit"};
	        		Object[] paramsValues2 = {data.getBulkName(),data.getUnit()};
	    			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select lookupRigStockUid from LookupRigStock where (isDeleted = false or isDeleted is null) and stockBrand LIKE :stockCode and storedUnit LIKE :storedUnit and (type IS NULL or type = '' or type = 'rigbulkstock')", paramsFields2, paramsValues2);
	    			if (lstResult2.size() > 0) {
	    				Object objStock = lstResult2.get(0);
	    				lookupRigStockUid = objStock.toString();
	    			}
	        		RigStock rigStock = new RigStock();
	        		sequence += 1;
	        		if(lookupRigStockUid == null) {
	        			rigStock.setStockCode(data.getBulkName());
	        			rigStock.setStoredUnit(data.getUnit());
	        		}else{
	        			rigStock.setStockCode(lookupRigStockUid);
	        			rigStock.setStoredUnit(data.getUnit());
	        		}
	        		rigStock.setSequence(sequence);
	        		rigStock.setAmtStart(data.getReceivedQuantity());
	        		rigStock.setAmtUsed(data.getConsumedQuantity());
	        		rigStock.setType("rigbulkstock");
	        		rigStock.setOperationUid(userSession.getCurrentOperation().getOperationUid());
	        		rigStock.setRigInformationUid(userSession.getCurrentOperation().getRigInformationUid());
	        		rigStock.setDailyUid(userSession.getCurrentDailyUid());
	        		CommandBeanTreeNodeImpl node = (CommandBeanTreeNodeImpl) targetNode.addCustomNewChildNodeForInput(rigStock);
	        		BaseCommandBean baseCommandBean = (BaseCommandBean) commandBean;
	        		for (DataNodeListener listener : baseCommandBean.getInternalSequencedDataNodeListeners()) {
                        listener.afterTemplateNodeCreated(baseCommandBean, targetNode.getChildDataDefinition(rigStock.getClass()), node, baseCommandBean.getCurrentUserSelectionSnapshot(), request);
                    }
	        		Double amount = WellITRigStockUtils.getInitialAmount(rigStock);
	        		if (!WellITRigStockUtils.checkIsPreviousRigStockInSameOperation(rigStock)) { 
	        			rigStock.setAmtInitial(amount);
	        		}
	        		if (amount == null) {
	        			amount = 0.0;
	        		}
	        		rigStock.setAmtDiscrepancy(data.getInventoryQuantity() - amount - data.getReceivedQuantity() + data.getConsumedQuantity());
	        		stockCodeChanged(request, commandBean, node, thisConverter);
	        		node.getAtts().setAction(Action.SAVE);
	        		node.getAtts().setEditMode(true);
	                node.setNewlyCreated(true);
	                node.setDirty(true);
        		}
        	}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
}
