package com.idsdatanet.d2.drillnet.wellExternalCatalog;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WellExternalCatalogServerLookupHandler implements LookupHandler  {
	private String column = null;
	@SuppressWarnings("unchecked")
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot selectionSnapshot, HttpServletRequest request, LookupCache cache) throws Exception {
		Map<String, LookupItem> result = cache.getFromCache(this.getClass().getName());
		if(result != null) return result;
		String hql = "SELECT serverPropertiesUid, "+this.column+" FROM ImportExportServerProperties "
				+ "WHERE (isDeleted is null or isDeleted = 0) "
				+ "and dataTransferType=:dataTransferType ORDER BY serviceName asc";
		
		List <Object[]> lsResult = ApplicationUtils.getConfiguredInstance()
				.getDaoManager().findByNamedParam(hql, "dataTransferType", "EXT_WELL_CATALOG");
		result = new LinkedHashMap<String, LookupItem>();
		
		if (lsResult.size() > 0) {
			for (Object[] obj : lsResult) {
				result.put(obj[0].toString(), new LookupItem(obj[0].toString(), obj[1].toString()));
			}
		}
		return result;
	}
	
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
}
