package com.idsdatanet.d2.drillnet.report;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WellboreBasedReportModule extends DefaultReportModule {
	
	@Override
	protected List<ReportFiles> loadSourceReportFilesFromDB(UserSelectionSnapshot userSelection, String reportType) throws Exception {
		List<Object> param_values = new ArrayList<>();
		List<String> param_names = new ArrayList<>();
		String query = null;
		
		// Get current operation's wellboreUid & get operation list
		String operationUidList = this.getOperationListString(userSelection);

		param_names.add("reportType"); param_values.add(reportType);
		query = "FROM ReportFiles WHERE reportOperationUid IN (" + operationUidList + ") AND reportType = :reportType AND (isDeleted = false or isDeleted is null) ORDER BY dateGenerated";
		return ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, param_names, param_values);
	}
	
	@Override
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		String wellboreUid = userContext.getUserSelection().getWellboreUid();
			
		return this.getReportType(userContext.getUserSelection()) + 
			(StringUtils.isBlank(wellboreUid) ? "" : "/" + wellboreUid) + 
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize));
	}
	
	@Override
	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception{
		Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(userContext.getUserSelection().getWellboreUid());
		Date currentdate = new Date();
		String formattedDate = ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(currentdate);
		return (wellbore == null ? "" : wellbore.getWellboreName()) + " " + formattedDate + " " + (StringUtils.isBlank(paperSize) ? "" : "(" + paperSize + ")") + "." + this.getOutputFileExtension();
	}
	
	@Override
	protected Comparator getReportOutputFilesComparator() {
		return new WellboreReportOutputFilesComparator();
	}
	
	private class WellboreReportOutputFilesComparator implements Comparator<Object> {
		public int compare(Object o2, Object o1) {
			ReportOutputFile f1 = (ReportOutputFile) o1;
			ReportOutputFile f2 = (ReportOutputFile) o2;
			
			if(f1.getTimeReportFileCreated() != null && f2.getTimeReportFileCreated() != null){
				return f1.getTimeReportFileCreated().compareTo(f2.getTimeReportFileCreated());
			}else if(f1.getTimeReportFileCreated() != null){
				return 1;
			}else if(f2.getTimeReportFileCreated() != null){
				return -1;
			}else{
				return 0;
			}
		}
	}
	
	private String getOperationListString(UserSelectionSnapshot userSelection) throws Exception {
		String strSql = "SELECT DISTINCT operationUid FROM Operation "
				+ "WHERE (isDeleted = false OR isDeleted IS NULL) "
				+ "AND wellboreUid = :wellboreUid";
		String[] paramsFields = {"wellboreUid"};
		Object[] paramsValues = {userSelection.getWellboreUid()};
		List<String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		String rv = "";
		if (!lstResult.isEmpty()) {
			StringBuilder strBuilder = new StringBuilder();
			for (String operationUid : lstResult) {
				strBuilder.append("'");
				strBuilder.append(operationUid);
				strBuilder.append("'");
				strBuilder.append(",");
			}
			rv = strBuilder.toString();
			rv = rv.substring(0, rv.length() - 1);
		}
		return rv;
	}

}
