package com.idsdatanet.d2.drillnet.activity;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Campaign;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityByTimeBaseDataLoaderInterceptor implements DataLoaderInterceptor {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private String defaultReportingPeriod = "0_24";
	
	public void setDefaultReportingPeriod(String value){
		this.defaultReportingPeriod = value;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		if (request != null) {
			if (Activity.class.equals(meta.getTableClass())) {
				return conditionClause + (StringUtils.isNotBlank(conditionClause) ? " AND " : "") + "((dayPlus IS NULL) OR dayPlus = '0')";
			}
		} else {
			if (Activity.class.equals(meta.getTableClass())) {
				String timeBase = this.getCampaignReportingPeriod(userSelection.getOperationUid()); //0000-2400 >> 0_24 (format: [start_hr]_[duration])

				if (timeBase==null || StringUtils.isBlank(timeBase)){
					timeBase = defaultReportingPeriod;
				}
				
				Date start = null;
				Date end = null;
				Boolean nextTime = false;
				
				if (StringUtils.isNotBlank(timeBase)){
					String[] time = timeBase.split("_");
					
					if (time.length>0 && time.length==2){
						Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
						
						if (daily == null) return "1==2";
						
						Calendar thisCalendar = Calendar.getInstance();
						thisCalendar.setTime(daily.getDayDate());
						thisCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
						start = thisCalendar.getTime();
						
						Daily tomorrow = ApplicationUtils.getConfiguredInstance().getTomorrow(userSelection.getOperationUid(), userSelection.getDailyUid());
						
						if (tomorrow != null) {
							List<ReportDaily> drillnetReportDailyList = ActivityUtils.getDrillnetReportDaily(tomorrow.getDailyUid());
							
							if (!drillnetReportDailyList.isEmpty()) {
								thisCalendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(time[1]));
								end = thisCalendar.getTime();
								
								conditionClause = conditionClause + (StringUtils.isNotBlank(conditionClause) ? " AND " : "") + "((dayPlus IS NULL) OR dayPlus = '0')";
								nextTime = true;
								
								conditionClause = conditionClause.replace(CUSTOM_CONDITION_MARKER, "(dailyUid =:dailyUid OR dailyUid=:nextDailyUid)");
								query.addParam("dailyUid", daily.getDailyUid());
								query.addParam("nextDailyUid", tomorrow.getDailyUid());	
							}
						}
						
						if (end==null){
							thisCalendar.setTime(daily.getDayDate());
							thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
							thisCalendar.set(Calendar.MINUTE, 59);
							thisCalendar.set(Calendar.SECOND , 59);
							thisCalendar.set(Calendar.MILLISECOND , 59);
							end = thisCalendar.getTime();
						}
						
						conditionClause = conditionClause + (StringUtils.isNotBlank(conditionClause) ? " AND " : "") + "(endDatetime >:startDate AND startDatetime <:endDate)";
						
						query.addParam("startDate", start);
						query.addParam("endDate", end);
						
					}
				}
				
				if (!nextTime){
					conditionClause = conditionClause.replace(CUSTOM_CONDITION_MARKER, "dailyUid = session.dailyUid");
				}
			
			}
			if (NextDayActivity.class.equals(meta.getTableClass())) {
				return "1 = 2";
			}
		}
		return conditionClause;
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String getCampaignReportingPeriod(String operationUid) throws Exception{
		Operation op = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
		
		if(op!=null && StringUtils.isNotBlank(op.getOperationCampaignUid())){
			Campaign campaign = (Campaign) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Campaign.class, op.getOperationCampaignUid());
			
			if (campaign!=null){
				return campaign.getReportingPeriod();
			}
		}
		
		return null;
	}
}
