package com.idsdatanet.d2.drillnet.operation;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.stt.DefaultSTTActionCallback;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class OperationRTFCallback extends DefaultSTTActionCallback {
	public void afterSendToTownCompleted(UserSelectionSnapshot userSelection) {
	}
	
	public void afterRefreshFromTownCompleted(UserSelectionSnapshot userSelection) {
		try {
			//update sys_operation_start_datetime and sys_operation_last_datetime in operation table
			CommonUtil.getConfiguredInstance().setStartAndLastOperationDate(userSelection.getOperationUid());
			
			//cascade delete process
			if (ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid()) == null) {
				//null = cannot find this well = deleted
				ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWell(userSelection.getWellUid());
			}
			//cascade delete wellbore
			else if (ApplicationUtils.getConfiguredInstance().getCachedWellbore(userSelection.getWellboreUid()) == null) {
				//null = cannot find this wellbore = deleted
				ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWellbore(userSelection.getWellboreUid());
			}
			else if (ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid()) == null) {
				//null = cannot find this operation = deleted
				ApplicationUtils.getConfiguredInstance().CascadeDeleteDailyWhenDeleteOperation(userSelection.getOperationUid());
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
