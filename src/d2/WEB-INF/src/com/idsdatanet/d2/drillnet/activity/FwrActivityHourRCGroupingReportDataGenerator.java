package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class FwrActivityHourRCGroupingReportDataGenerator implements ReportDataGenerator {
	private Map<String, String> rootCodeList = null;
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		
		Map ad = userContext.getUserSelection().getCustomProperties();
		if(ad  != null && ad.size() > 0){
			this.rootCodeList = new HashMap();
			
			Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
			this.rootCodeList = this.getRootCodeList(operation);
		}
		
		if(ad != null ){
			if(ad.containsKey("TroubleTimeRCAnalysis")){
				this.generateTroubledTimeAnalysisRCGrouping(userContext, reportDataNode, (String) ad.get("TroubleTimeRCAnalysis"), false, "TroubleTimeRCAnalysis");
			}
			if(ad.containsKey("TroubleTimeRCAnalysisByCompany")){
				this.generateTroubledTimeAnalysisRCGrouping(userContext, reportDataNode, (String) ad.get("TroubleTimeRCAnalysisByCompany"), false, "TroubleTimeRCAnalysisByCompany");
			}
		}
	}

	public void generateTroubledTimeAnalysisRCGrouping(UserContext userContext, ReportDataNode reportDataNode, String minVal, Boolean unprog, String key) throws Exception{
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double rootCodeDuration;
		Double totalMiscDuration = 0.0;
		String shortCode;
		String shortCodeName;
		String lookupCompanyUid;
		String companyName;
		int miscTaskCount = 0;
		Double minHrs = Double.parseDouble(minVal);
		minHrs = minHrs * 3600;
		
		String queryCond = "";
		String reportDataNodeName = "";
		
		if(key=="TroubleTimeRCAnalysis"){
			
			if (unprog){
				queryCond = "('P','TP')";
				reportDataNodeName = "ActivityUnprogTimeAnalysisRCGrouping";
			}else {
				queryCond = "('P','U')";
				reportDataNodeName = "ActivityTroubleTimeAnalysisRCGrouping";
			}
			
			String strSql = "SELECT sum(a.activityDuration) AS totalDuration, a.rootCauseCode FROM Activity a WHERE a.operationUid = :thisOperationUid AND a.internalClassCode NOT IN " + queryCond + " AND (a.isDeleted = false or a.isDeleted is null) AND (a.carriedForwardActivityUid = '' OR a.carriedForwardActivityUid IS NULL) AND (a.isSimop = '' OR a.isSimop IS NULL) AND (a.isOffline = '' OR a.isOffline IS NULL) AND a.dailyUid IN (SELECT dailyUid FROM Daily d WHERE d.operationUid = :thisOperationUid AND (d.isDeleted='' OR d.isDeleted IS NULL)) GROUP BY a.rootCauseCode";
			String[] paramsFields = {"thisOperationUid"};
			Object[] paramsValues = {userContext.getUserSelection().getOperationUid()};
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			if (lstResult.size() > 0){
					
				for(int i = 0; i < lstResult.size(); i++ ){
					Object[] activityRecord = (Object[]) lstResult.get(i);
					shortCode = "";
					shortCodeName = "";
					rootCodeDuration = 0.0;
					
					if (activityRecord[0] != null && StringUtils.isNotBlank(activityRecord[0].toString())){
						rootCodeDuration = Double.parseDouble(activityRecord[0].toString());
					}
					if (activityRecord[1] != null && StringUtils.isNotBlank(activityRecord[1].toString())) shortCode = activityRecord[1].toString();
					if(this.rootCodeList.containsKey(shortCode)) shortCodeName = this.rootCodeList.get(shortCode);
					//if (activityRecord[2] != null && StringUtils.isNotBlank(activityRecord[2].toString())) shortCodeName = activityRecord[2].toString();
						
					if(rootCodeDuration <= minHrs){
						rootCodeDuration = rootCodeDuration/3600;
						ReportDataNode thisReportNode = reportDataNode.addChild("ActivityTroubleTimeAnalysisRCGroupingTimeWrap");
						thisReportNode.addProperty("activityDuration", rootCodeDuration.toString());
						thisReportNode.addProperty("shortCode", shortCode);
						thisReportNode.addProperty("shortCodeName", shortCodeName);
						totalMiscDuration += rootCodeDuration;
						miscTaskCount++;					
					}
					else{
						//Convert to hours
						rootCodeDuration = rootCodeDuration/3600;
						ReportDataNode thisReportNode = reportDataNode.addChild(reportDataNodeName);
						thisReportNode.addProperty("activityDuration", rootCodeDuration.toString());
						thisReportNode.addProperty("shortCode", shortCode);
						thisReportNode.addProperty("shortCodeName", shortCodeName);
					}	
				}
				if(miscTaskCount > 0){
					String label = miscTaskCount + " events < " + minVal + "(hrs)";
					ReportDataNode thisReportNode = reportDataNode.addChild(reportDataNodeName);
					thisReportNode.addProperty("activityDuration", totalMiscDuration.toString());
					thisReportNode.addProperty("shortCode", "MISC");
					thisReportNode.addProperty("shortCodeName", label);
				}
			
			}
		}
		
		if(key=="TroubleTimeRCAnalysisByCompany"){
			if (unprog){
				queryCond = "('P','TP')";
				reportDataNodeName = "ActivityUnprogTimeAnalysisRCCompanyGrouping";
			}else {
				queryCond = "('P','U')";
				reportDataNodeName = "ActivityTroubleTimeAnalysisRCCompanyGrouping";
			}
			String strSql2 = "SELECT sum(a.activityDuration) AS totalDuration, a.lookupCompanyUid" +
					" FROM Activity a" +
					" WHERE a.operationUid = :thisOperationUid AND a.internalClassCode NOT IN " + queryCond + 
					" AND (a.isDeleted = false or a.isDeleted is null)" +
					" AND (a.carriedForwardActivityUid = '' OR a.carriedForwardActivityUid IS NULL)" +
					" AND (a.isSimop = '' OR a.isSimop IS NULL) AND (a.isOffline = '' OR a.isOffline IS NULL)" +
					" AND a.dailyUid IN (SELECT dailyUid FROM Daily d WHERE d.operationUid = :thisOperationUid AND (d.isDeleted='' OR d.isDeleted IS NULL))" +
					" GROUP BY a.lookupCompanyUid";
			String[] paramsFields2 = {"thisOperationUid"};
			Object[] paramsValues2 = {userContext.getUserSelection().getOperationUid()};
			
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
			if (lstResult2.size() > 0){
					
				for(int i = 0; i < lstResult2.size(); i++ ){
					Object[] activityRecord = (Object[]) lstResult2.get(i);
					lookupCompanyUid = "";
					companyName = "No Party Assigned";
					rootCodeDuration = 0.0;
					
					if (activityRecord[0] != null && StringUtils.isNotBlank(activityRecord[0].toString())){
						rootCodeDuration = Double.parseDouble(activityRecord[0].toString());
					}
					if (activityRecord[1] != null && StringUtils.isNotBlank(activityRecord[1].toString())) {
						lookupCompanyUid = activityRecord[1].toString();
						
						String strSql3 = "SELECT companyName FROM LookupCompany WHERE (isDeleted = false or isDeleted is null) AND lookupCompanyUid = :lookupCompanyUid";
						String[] paramsFields3 = {"lookupCompanyUid"};
						Object[] paramsValues3 = {lookupCompanyUid};
						List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3, qp);
						if(lstResult3.size() > 0){
							companyName = lstResult3.get(0).toString();
						}
					}
						
					if(rootCodeDuration <= minHrs){
						rootCodeDuration = rootCodeDuration/3600;
						ReportDataNode thisReportNode = reportDataNode.addChild("ActivityTroubleTimeAnalysisRCCompanyGroupingTimeWrap");
						thisReportNode.addProperty("activityDuration", rootCodeDuration.toString());
						thisReportNode.addProperty("lookupCompanyUid", lookupCompanyUid);
						thisReportNode.addProperty("companyName", companyName);
						totalMiscDuration += rootCodeDuration;
						miscTaskCount++;					
					}
					else{
						//Convert to hours
						rootCodeDuration = rootCodeDuration/3600;
						ReportDataNode thisReportNode = reportDataNode.addChild(reportDataNodeName);
						thisReportNode.addProperty("activityDuration", rootCodeDuration.toString());
						thisReportNode.addProperty("lookupCompanyUid", lookupCompanyUid);
						thisReportNode.addProperty("companyName", companyName);
					}	
				}
				if(miscTaskCount > 0){
					String label = miscTaskCount + " events < " + minVal + "(hrs)";
					ReportDataNode thisReportNode = reportDataNode.addChild(reportDataNodeName);
					thisReportNode.addProperty("activityDuration", totalMiscDuration.toString());
					thisReportNode.addProperty("lookupCompanyUid", "MISC");
					thisReportNode.addProperty("companyName", label);
				}
			
			}
		}
	}
	
	public Map<String, String> getRootCodeList(Operation operation) throws Exception{
		
		String operationCode = "";

		if(StringUtils.isNotBlank(operation.getOperationCode())) operationCode = operation.getOperationCode();
		
		List<Object[]> listRootCodeResults = new ArrayList<Object[]>();
		Map<String, String> rootCodeList = new HashMap();
		
		String[] paramsFields = {"operationCode"};		
 		Object[] paramsValues = {operationCode};
		
		String strSql = "SELECT lrcc.shortCode, lrcc.name FROM LookupRootCauseCode lrcc WHERE (lrcc.isDeleted = false or lrcc.isDeleted is null) AND " +
						 " (lrcc.operationCode = :operationCode or lrcc.operationCode='') ORDER BY lrcc.name";
		listRootCodeResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (listRootCodeResults.size() > 0) {
			for (Object[] listRootCodeResult : listRootCodeResults) {
				if (listRootCodeResult[0] != null && listRootCodeResult[1] != null) {
					
					String key = listRootCodeResult[0].toString();
					String value = listRootCodeResult[1].toString() + " (" + listRootCodeResult[0].toString() + ")";
							
					rootCodeList.put(key, value);
					
				}
			}
		}
		
		return rootCodeList;
	}
	
}
