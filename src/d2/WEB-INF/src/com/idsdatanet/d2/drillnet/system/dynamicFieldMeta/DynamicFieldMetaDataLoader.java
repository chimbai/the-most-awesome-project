package com.idsdatanet.d2.drillnet.system.dynamicFieldMeta;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.DynamicFieldMeta;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class DynamicFieldMetaDataLoader implements DataNodeLoadHandler{
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if(meta.getTableClass().equals(DynamicFieldMeta.class)){
			return this.loadDynamicFieldMeta(node, userSelection);
		}else{
			return null;
		}
	}
	
	private List<Object> loadDynamicFieldMeta(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception{
		
		String selectedTableName  = (String)node.getDynaAttr().get("tableUid");
		
		List<Object> param_values = new ArrayList<Object>();
		List<String> param_names = new ArrayList<String>();
		
		if(StringUtils.isNotBlank(selectedTableName)){
			param_names.add("tableName"); 
			param_values.add(selectedTableName);
		}	
		else{
			param_names.add("tableName"); 
			param_values.add("");
		}
		
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM DynamicFieldMeta WHERE (isDeleted = false or isDeleted is null) and tableName=:tableName", param_names, param_values);;
		List<Object> output_maps = new ArrayList<Object>();
		
		for(Iterator i = items.iterator(); i.hasNext(); ){
			Object obj_array = (Object) i.next();
			DynamicFieldMeta thisMeta = (DynamicFieldMeta) obj_array;
			output_maps.add(thisMeta);
		}
	
		return output_maps;	
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		return true;
	}
}
