package com.idsdatanet.d2.drillnet.personnelonsite;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.ExcelImportTemplateManager;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.PersonnelOnSite;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.pobMaster.PobMasterUtils;
import com.idsdatanet.depot.witsml.wellit.personnelonsite.WellITPersonnelOnSiteScreenImport;
import com.idsdatanet.depot.witsml.wellit.personnelonsite.WellITPobRobSummary;

public class PersonnelOnSiteCommandBeanListener extends EmptyCommandBeanListener implements CommandBeanListener {
    
    public static final String IMPORT_TEMPLATE_EXCEL = "importTemplateExcel";
    
    private ExcelImportTemplateManager excelImportTemplateManager = null;

    public ExcelImportTemplateManager getExcelImportTemplateManager() {
        return excelImportTemplateManager;
    }

    public void setExcelImportTemplateManager(ExcelImportTemplateManager excelImportTemplateManager) {
        this.excelImportTemplateManager = excelImportTemplateManager;
    }

	private class ImportPOBComparator implements Comparator<CommandBeanTreeNode> {
		public int compare(CommandBeanTreeNode o1, CommandBeanTreeNode o2) {
			try{
				ImportPOB pob1 = (ImportPOB) o1.getData();
				ImportPOB pob2 = (ImportPOB) o2.getData();
				Date date1 = pob1.getDayDate();
				Date date2 = pob2.getDayDate();
				
				if(date1 == null && date2 == null){
					return 0;
				}else if(date1 == null){
					return -1;
				}else if(date2 == null){
					return 1;
				}else{
					return date1.compareTo(date2);
				}
			}catch(Exception e){
				Log logger = LogFactory.getLog(this.getClass());
				logger.error(e.getMessage(), e);
				return 0;
			}
		}
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		
		//calculate rig capacity
		String rigInformationUid = userSelection.getRigInformationUid();
		String[] paramsFields = {"thisRigInformationUid"};
		Object[] paramsValues = {rigInformationUid};
		
		//String strSql = "SELECT df.fieldValueDouble from DynamicField df, RigInformation rig WHERE (rig.isDeleted = false or rig.isDeleted is null) and df.fieldKey= :thisRigInformationUid AND df.fieldUid = 'rig_information.paxmax' AND rig.rigInformationUid = :thisRigInformationUid";
		String strSql = "SELECT paxmax from RigInformation WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid = :thisRigInformationUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		Double rigMaxPax = 0.0;
		
		if (!lstResult.isEmpty()) {
			Object thisResult = lstResult.get(0);
			if (thisResult != null) rigMaxPax = Double.parseDouble(thisResult.toString());
			commandBean.getRoot().getDynaAttr().put("rigMaxPax", rigMaxPax);
			Integer totalPax = 0;
			if (commandBean.getRoot().getDynaAttr().get("pax") != null) {
				totalPax = (Integer)commandBean.getRoot().getDynaAttr().get("pax");
			}
			Double rigCapacity = 0.0;
			if (rigMaxPax > 0 && totalPax > 0) {				
				rigCapacity = (totalPax / rigMaxPax) * 100;
			}
			//format the value
			String formatPattern = GroupWidePreference.getValue(userSelection.getGroupUid(), "pobCapacityFormat");
			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(commandBean.getUserLocale());
			formatter.applyPattern(formatPattern);
			commandBean.getRoot().getDynaAttr().put("rigCapacity", formatter.format(rigCapacity));
		}
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		if (daily == null) return;
		Date reportDate = daily.getDayDate();

		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		if (operation == null) return;
		Date opsStartDate = operation.getStartDate();
		
		if (reportDate == null && opsStartDate == null) return;
		strSql = "Select sum(p.totalWorkingHours) from PersonnelOnSite p, Daily d " +
					"where (p.isDeleted = false or p.isDeleted is null) " +
					"and (d.isDeleted = false or d.isDeleted is null) " +
					"and p.operationUid = :operationUid " +
					"and d.operationUid = :operationUid " +
					"and p.dailyUid = d.dailyUid " +
					"and d.dayDate >= :opsStartDate " +
					"and d.dayDate <= :reportDate";
		String[] paramsFields1 = {"operationUid", "opsStartDate", "reportDate"};
		Object[] paramsValues1 = {userSelection.getOperationUid(), opsStartDate, reportDate};
		List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1);
		
		Double totalManhoursUntilNow = 0.0;
		if (!lstResult1.isEmpty()) {
			Object thisResult = lstResult1.get(0);
			if (thisResult != null) totalManhoursUntilNow = Double.parseDouble(thisResult.toString());
		}
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, PersonnelOnSite.class, "total_working_hours");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.getRoot().setCustomUOM("@totalManhoursUntilNow", thisConverter.getUOMMapping());
			thisConverter.setBaseValue(totalManhoursUntilNow);
			thisConverter.changeUOMUnit(thisConverter.getUOMMapping().getUnitID());
			totalManhoursUntilNow = thisConverter.getConvertedValue();
			commandBean.getRoot().getDynaAttr().put("totalManhoursUntilNow", totalManhoursUntilNow);
		}
		//need to refresh for the month POB import so the newly created day will appear on the day drop down
		D2ApplicationEvent.refresh();
	}

	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		commandBean.getRoot().sortChild("ImportPOB", new ImportPOBComparator());
	}

	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
	}

	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
        String action = request.getParameter("action");
        if (StringUtils.equals(IMPORT_TEMPLATE_EXCEL, action)) {
            this.excelImportTemplateManager.importExcelFile((BaseCommandBean) commandBean, this, request, request.getParameter(ExcelImportTemplateManager.KEY_TEMPLATE), request.getParameter(ExcelImportTemplateManager.KEY_FILE), "PersonnelOnSite");
        }else if (StringUtils.equals("importWellIT",action)) {
        	this.importWellIT(request,commandBean,targetCommandBeanTreeNode);
        }
    }

	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
		if ("collectPersonnelList".equalsIgnoreCase(invocationKey)) {
			UserSelectionSnapshot userSelection = new UserSelectionSnapshot(UserSession.getInstance(request));
			Map<String,Map<String,LookupItem>> lookupMap= new HashMap<>();
			lookupMap.put("PersonnelOnSite.crewPosition",commandBean.getLookupMap(null, "PersonnelOnSite", "crewPosition"));
			PobMasterUtils.writePersonnelListResponse(new SimpleXmlWriter(response), userSelection);
		} else if ("importTemplateExcel".equalsIgnoreCase(invocationKey)) {
            this.excelImportTemplateManager.uploadFile(request, response, ExcelImportTemplateManager.KEY_FILE);
		}
		return;
	}
	
    private void importWellIT(HttpServletRequest request, CommandBean commandBean,CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
    	Object dataList = new WellITPersonnelOnSiteScreenImport().getDataFromWellIT(request,commandBean);
    	this.importPobRobSummary(request,(ArrayList<Object>)dataList,commandBean,targetCommandBeanTreeNode);
	}
    
    private void importPobRobSummary(HttpServletRequest request, ArrayList<Object> dataList,CommandBean commandBean,CommandBeanTreeNode targetCommandBeanTreeNode) {
    	try {
    		commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
        	CommandBeanTreeNodeImpl targetNode = (CommandBeanTreeNodeImpl) commandBean.getRoot();
    		UserSession userSession = UserSession.getInstance(request);
    		String dailyUid = userSession.getCurrentDailyUid();
    		String strSql = "FROM PersonnelOnSite WHERE dailyUid = :dailyUid AND (isDeleted IS NULL OR isDeleted = 0) ORDER BY sequence DESC";
			String[] paramsFields = {"dailyUid"};
			Object[] paramsValues = {dailyUid};
			List<PersonnelOnSite> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			int sequence = 0;
			if(lstResult.size() > 0) {
				sequence = lstResult.get(0).getSequence();
			}
    		strSql = "select lookupCompanyUid,companyName FROM LookupCompany WHERE (isDeleted IS NULL or isDeleted = 0)";
			List<Object> lookupCompanyList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			Map<String,String> lookupCompanyMap = new HashMap<>();
			for(Object o: lookupCompanyList) {
				Object[] obj = (Object[]) o;
				String lookupCompanyUid = obj[0].toString();
				String lookupCompanyName = obj[1].toString().toLowerCase();
				lookupCompanyMap.put(lookupCompanyName, lookupCompanyUid);
			}
			
        	for(Object o : dataList) {
        		if (o instanceof WellITPobRobSummary) {
        			WellITPobRobSummary data = (WellITPobRobSummary) o;
	        		PersonnelOnSite pob = new PersonnelOnSite();
	        		sequence += 1;
	        		String crewCompany = "";
	        		if(lookupCompanyMap.get(data.getCompanyName().toLowerCase()) != null) {
	        			crewCompany = lookupCompanyMap.get(data.getCompanyName().toLowerCase());
	        		}else {
	        			crewCompany = data.getCompanyName();
	        		}
	        		String crewPosition = data.getTaskName();
	        		int pax = data.getPob();
	        		pob.setSequence(sequence);
	        		pob.setCrewCompany(crewCompany);
	        		pob.setCrewPosition(crewPosition);
	        		pob.setPax(pax);
	        		CommandBeanTreeNodeImpl node = (CommandBeanTreeNodeImpl) targetNode.addCustomNewChildNodeForInput(pob);
	        		node.getAtts().setEditMode(true);
	                node.setNewlyCreated(true);
	                node.setDirty(true);
        		}
        	}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
}

