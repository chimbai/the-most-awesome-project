package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;
import com.idsdatanet.d2.core.web.mvc._Well;

public class ParentOperationLookupHandler extends ParentOperationLazyLoadingLookupProvider implements CascadeLookupHandler {
	


	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, String key, LookupCache lookupCache)
					throws Exception {
		
		
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT) {
			Map<String, LookupItem> cascadeLookup = new HashMap<String, LookupItem>();
			
			String sql = "from Operation where (isDeleted is null or isDeleted = false) and wellUid in (select wellUid from Operation where operationUid = :operationUid)";
			
			List<Operation> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", key);
			if(result != null) {
				for(Operation o: result) {
					_Operation _operation = new _Operation(o.getOperationUid(), o.getOperationName(), o.getWellUid(), o.getWellboreUid(), o.getOperationalCode(), o.getRigInformationUid(), o.getOperationCampaignUid(), o.getIsCampaignOverviewOperation(), o.getSysOperationSubclass(), o.getSysOperationStartDatetime(), o.getSysOperationLastDatetime());
					String operationName = this.getOperationName(o.getWellUid(), o.getWellboreUid(), _operation);
					cascadeLookup.put(o.getOperationUid(), new LookupItem(o.getOperationUid(), operationName));
				}
				return cascadeLookup;
			}
		}
		
		return null;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		
		
		List selected = new ArrayList();
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		Collection<_Operation> allOperations = UserSession.getInstance(request).getCachedAllAccessibleOperations().values();
		for (_Operation operation: allOperations) {
			if (operation.getWellUid()==null) continue;
			CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(operation.getOperationUid());
			List<LookupItem> lookupItems = new ArrayList<LookupItem>();
			for (_Operation rec: allOperations) {
				if (rec.getWellUid() == null) continue;
				if (rec.getWellUid().equals(operation.getWellUid()) && !rec.getOperationUid().equals(operation.getOperationUid())) {
					String operationName = this.getOperationName(rec.getWellUid(), rec.getWellboreUid(), rec);
					if (operationName!=null) {
						lookupItems.add(new LookupItem(rec.getOperationUid(), operationName));
					}
				}
			}
			
			LinkedHashMap<String, LookupItem> lookup = new LinkedHashMap<String, LookupItem>();
			Collections.sort(lookupItems, new LookupItemComparator());
			for (LookupItem lookupItem : lookupItems) {
				lookup.put(lookupItem.getKey(), lookupItem);
			}

			cascadeLookupSet.setLookup(lookup);
			result.add(cascadeLookupSet);
			selected.clear();
			selected.add(operation.getOperationUid());
		}
		
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);
		return result_in_array;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		
	Collection<_Operation> allOperations = null;
		
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT) {
			allOperations = ApplicationUtils.getConfiguredInstance().getCachedAllOperations().values();
		}else {
			allOperations = UserSession.getInstance(request).getCachedAllAccessibleOperations().values();
		}
		
		if(allOperations == null) {
			return null;
		}
		
		List<LookupItem> lookupItems = new ArrayList<LookupItem>();
		for (_Operation rec: allOperations) {
			if (rec.getWellUid() == null) continue;
			if (rec.getWellUid().equals(userSelection.getWellUid()) && !rec.getOperationUid().equals(userSelection.getOperationUid())) {
				String operationName = this.getOperationName(rec.getWellUid(), rec.getWellboreUid(), rec);
				if (operationName!=null) {
					lookupItems.add(new LookupItem(rec.getOperationUid(), operationName));
				}
			}
		}
		
		LinkedHashMap<String, LookupItem> lookup = new LinkedHashMap<String, LookupItem>();
		Collections.sort(lookupItems, new LookupItemComparator());
		for (LookupItem lookupItem : lookupItems) {
			lookup.put(lookupItem.getKey(), lookupItem);
		}
		
		return lookup;
	}
	
	private String getOperationName(String wellUid, String wellboreUid, _Operation operation)  throws Exception {
		String hierarchicalWellbore = ApplicationUtils.getConfiguredInstance().getCascadeParentWellboreName(wellboreUid, MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES);
		if (hierarchicalWellbore!=null && !"".equals(hierarchicalWellbore)) {
			hierarchicalWellbore = hierarchicalWellbore.substring(0, hierarchicalWellbore.lastIndexOf(MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES));
		}
		_Well well = ApplicationUtils.getConfiguredInstance().getCachedAllWells().get(wellUid);
		String wellName = "";
		if (well!=null) wellName = well.getWellName();
		if (operation!=null && hierarchicalWellbore!=null) {
			String operationName = operation.getOperationName();
			String label = operationName;
			if(! StringUtils.equals(operationName.trim(), hierarchicalWellbore.trim())) {
				String wellboreOperationName = hierarchicalWellbore + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + operationName;
				if(! StringUtils.equals(wellName.trim(), hierarchicalWellbore.trim())) {
					label = wellName + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + wellboreOperationName;
				} else {
					label = wellboreOperationName;
				}
			} else {
				if(! StringUtils.equals(wellName.trim(), hierarchicalWellbore.trim())) {
					label = wellName + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + operationName;
				}
			}
			return label;
			
		}
		return null;
	}
	
	private class LookupItemComparator implements Comparator<LookupItem> {
		
		public int compare(LookupItem v1, LookupItem v2) {
			if (v1 == null || v1.getValue() == null || v2 == null || v2.getValue() == null || StringUtils.equals((String)v1.getValue(), (String)v2.getValue())) {
				return 0;
			}
			return WellNameUtil.getPaddedStr((String)v1.getValue()).compareTo(WellNameUtil.getPaddedStr((String)v2.getValue()));
		}
	}
}