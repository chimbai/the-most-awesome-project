package com.idsdatanet.d2.drillnet.activity;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @author dlee
 * This class help to mine data for activity duration for the operation up to the selected date group by phase code
 * for daily reporting use
 * 
 * configuration is set in ddr.xml
 */

public class OperationActivityDurationByPhaseCodeAndPlanReferenceToDateReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		String prevPhase = null;
		String prevPhaseName = null;
		String prevPlanReferenceUid = null;
		String prevPlanName = null;
		Double prevDuration = 0.0;
		Double prevRunningTotalDurationDay = 0.0;
		Double prevRunningTotalDuration = 0.0;
		Date prevStartDate = null;
		Date prevEndDate = null;
		long prevStartDateVal = 0;
		long prevEndDateVal = 0;
		String sequence = null;
		
		String phaseCode = "N/A";
		String phaseName = "N/A";
		String planName = "N/A";
		String planReferenceUid = "N/A";
		String planReferenceDescription = "N/A";
		Double runningTotalDuration = 0.0;
		Double currentRunningTotalDuration = 0.0;
		Double currentRunningTotalDurationDay = 0.0;
		Double duration = 0.0;
		Double minDepth = 0.0;
		Double prevMinDepth = 0.0;
		Double maxDepth = 0.0;
		String prevMinDepthDisplay = "";
		String minDepthDisplay = "";
		String maxDepthDisplay = "";
		Date startDate = null;
		Date endDate = null;
		long startDateVal = 0;
		long endDateVal = 0;
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		// get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if(daily == null) return;
		Date todayDate = daily.getDayDate();
		
		String strSql = "SELECT a.phaseCode, a.activityDuration, a.depthMdMsl, d.dayDate, a.planReference " + 
				"FROM Daily d, Activity a WHERE (d.isDeleted = false OR d.isDeleted IS NULL) AND (a.isDeleted = false OR a.isDeleted IS NULL) " + 
				"AND a.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid AND " +
				"NOT(a.phaseCode IS NULL OR a.phaseCode = '') AND (a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) ORDER BY a.startDatetime";
		
		String[] paramsFields = {"userDate", "thisOperationUid"};
		Object[] paramsValues = {todayDate, userContext.getUserSelection().getOperationUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if(lstResult.size() > 0) {
			//unit converter to convert + format value
			CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
			
			for(Object objResult: lstResult) {
				Object[] objDuration = (Object[]) objResult;
				
				if(objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())) {
					phaseCode = objDuration[0].toString();
					String strSql2 = "SELECT name FROM LookupPhaseCode WHERE (isDeleted is null OR isDeleted = false) AND shortCode = :thisPhaseCode AND (operationCode = :thisOperationType OR operationCode = '')";
					String[] paramsFields2 = {"thisPhaseCode", "thisOperationType"};
					Object[] paramsValues2 = {phaseCode, userContext.getUserSelection().getOperationType().toString()};
					
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				     if(lstResult2.size()>0) {
				      Object lookupPhaseCodeResult = (Object) lstResult2.get(0);
				      if(lookupPhaseCodeResult != null) phaseName = lookupPhaseCodeResult.toString();
				     }
				}
				
				if(objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())) {
					duration = Double.parseDouble(objDuration[1].toString());
					thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
					thisConverter.setBaseValue(duration);
					duration = thisConverter.getConvertedValue();
					runningTotalDuration = runningTotalDuration + Double.parseDouble(objDuration[1].toString());
				}
				
				if(objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())) {
					minDepth = Double.parseDouble(objDuration[2].toString());
					
					//assign unit to the value base on activity.depthMdMsl, this will do auto convert to user display unit + format
					thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");
					thisConverter.setBaseValue(minDepth);
					minDepth = thisConverter.getConvertedValue();
					minDepthDisplay = thisConverter.getFormattedValue();
				}
				
				if(objDuration[3] != null && StringUtils.isNotBlank(objDuration[3].toString())) {
					startDate = (Date) objDuration[3];
					endDate = startDate;
					
					startDateVal = startDate.getTime();
					endDateVal = startDateVal;
				}
				
				if(objDuration[4] != null) {
					if(StringUtils.isNotBlank(objDuration[4].toString())){
						planReferenceUid = objDuration[4].toString();
						String strSql2 = "SELECT sequence, phaseName FROM OperationPlanPhase WHERE (isDeleted is null OR isDeleted = false) AND operationPlanPhaseUid = :thisPlanReference AND operationUid = :thisOperationUid";
						String[] paramsFields2 = {"thisPlanReference", "thisOperationUid"};
						Object[] paramsValues2 = {planReferenceUid, userContext.getUserSelection().getOperationUid()};
						
						List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
					     if(lstResult2.size()>0) {
					      for(Object planReferenceResult: lstResult2) {
					    	  Object[] res = (Object[]) planReferenceResult;
						      if(res[1] != null) planReferenceDescription = res[1].toString();
						      if(res[0] != null) sequence = res[0].toString();
						      planName = sequence + " " + planReferenceDescription;
					      }
					     }
					}else {
						planReferenceUid = "";
						planName = "";
					}
				}
				
				// first phase
				if(prevPhase == null) {
					prevPhase = phaseCode;
					prevPhaseName = phaseName;
					
					if(prevPlanReferenceUid == null) {
						prevPlanReferenceUid = planReferenceUid;
						prevPlanName = planName;
						prevDuration = duration;
						prevMinDepth = minDepth;
						prevMinDepthDisplay = minDepthDisplay;
						
						prevStartDate = startDate;
						prevEndDate = endDate;
						prevStartDateVal = startDateVal;
						prevEndDateVal = endDateVal;
						
						//for running total calculation need to be outside the test cause the total is always needed in every record
						//assign unit to current running total
						prevRunningTotalDuration = runningTotalDuration;
						
						//do another value for day base, do this before the value got converted, cause here is always seconds
						prevRunningTotalDurationDay = prevRunningTotalDuration / 86400;
						
						thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
						thisConverter.setBaseValue(prevRunningTotalDuration);
						prevRunningTotalDuration = thisConverter.getConvertedValue();
					}
				} else {
					if(phaseCode.equalsIgnoreCase(prevPhase)) {
						if(prevPlanReferenceUid.equals(planReferenceUid)) {
							if(prevMinDepth > minDepth) {
								maxDepth = prevMinDepth;
								//assign unit to the value base on activity.depthMdMsl, this will do auto convert to user display unit + format
								thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");
								thisConverter.setBaseValue(maxDepth);
								maxDepth = thisConverter.getConvertedValue();
								minDepthDisplay = thisConverter.getFormattedValue();
								prevMinDepthDisplay = minDepthDisplay;
							}else {
								maxDepth = minDepth;
								//assign unit to the value base on activity.depthMdMsl, this will do auto convert to user display unit + format
								thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");
								thisConverter.setBaseValue(maxDepth);
								maxDepth = thisConverter.getConvertedValue();
								minDepthDisplay = thisConverter.getFormattedValue();
								prevMinDepthDisplay = minDepthDisplay;
							}
							prevDuration = prevDuration + duration;
							
							prevEndDate = endDate;
							prevEndDateVal = endDateVal;
							//for running total calculation need to be outside the test cause the total is always needed in every record
							//assign unit to current running total
							prevRunningTotalDuration = prevRunningTotalDuration + runningTotalDuration;
							
							//do another value for day base, do this before the value got converted, cause here is always seconds
							prevRunningTotalDurationDay = prevRunningTotalDuration / 86400;
							
							thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
							thisConverter.setBaseValue(prevRunningTotalDuration);
							prevRunningTotalDuration = thisConverter.getConvertedValue();
						}else {
							addToReportDataNode(reportDataNode, prevPhase, prevPhaseName, prevPlanReferenceUid, prevPlanName, prevDuration, prevRunningTotalDuration, prevRunningTotalDurationDay, 
									minDepth, minDepthDisplay, maxDepth, prevMinDepthDisplay, prevStartDateVal, prevStartDate, prevEndDateVal, prevEndDate);
							prevPhase = phaseCode;
							prevPhaseName = phaseName;
							prevPlanReferenceUid = planReferenceUid;
							prevPlanName = planName;
							prevDuration = duration;
							prevMinDepth = minDepth;
							prevMinDepthDisplay = minDepthDisplay;
							
							prevStartDate = startDate;
							prevEndDate = endDate;
							prevStartDateVal = startDateVal;
							prevEndDateVal = endDateVal;
							
							//for running total calculation need to be outside the test cause the total is always needed in every record
							//assign unit to current running total
							prevRunningTotalDuration = runningTotalDuration;
							
							//do another value for day base, do this before the value got converted, cause here is always seconds
							prevRunningTotalDurationDay = prevRunningTotalDuration / 86400;
							
							thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
							thisConverter.setBaseValue(prevRunningTotalDuration);
							prevRunningTotalDuration = thisConverter.getConvertedValue();
						}
					} else {
						addToReportDataNode(reportDataNode, prevPhase, prevPhaseName, prevPlanReferenceUid, prevPlanName, prevDuration, prevRunningTotalDuration, prevRunningTotalDurationDay, 
								minDepth, minDepthDisplay, maxDepth, prevMinDepthDisplay, prevStartDateVal, prevStartDate, prevEndDateVal, prevEndDate);
						prevPhase = phaseCode;
						prevPhaseName = phaseName;
						prevPlanReferenceUid = planReferenceUid;
						prevPlanName = planName;
						prevDuration = duration;
						prevMinDepth = minDepth;
						prevMinDepthDisplay = minDepthDisplay;
						
						prevStartDate = startDate;
						prevEndDate = endDate;
						prevStartDateVal = startDateVal;
						prevEndDateVal = endDateVal;
						
						//for running total calculation need to be outside the test cause the total is always needed in every record
						//assign unit to current running total
						prevRunningTotalDuration = runningTotalDuration;
						
						//do another value for day base, do this before the value got converted, cause here is always seconds
						prevRunningTotalDurationDay = prevRunningTotalDuration / 86400;
						
						thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
						thisConverter.setBaseValue(prevRunningTotalDuration);
						prevRunningTotalDuration = thisConverter.getConvertedValue();
					}
				}
			}
			
			if(prevPhase != null) {
				addToReportDataNode(reportDataNode, prevPhase, prevPhaseName, prevPlanReferenceUid, prevPlanName, prevDuration, prevRunningTotalDuration, prevRunningTotalDurationDay, 
						minDepth, minDepthDisplay, maxDepth, prevMinDepthDisplay, prevStartDateVal, prevStartDate, prevEndDateVal, prevEndDate);
			}
		}		
	}

	private void addToReportDataNode(ReportDataNode reportDataNode, String phase, String phaseName, String planReferenceUid, String planName,
			Double totalDuration, Double currentRunningTotalDuration, Double currentRunningTotalDurationDay,
			Double minDepth, String minDepthDisplay, Double maxDepth, String maxDepthDisplay, long startDateVal,
			Date startDate, long endDateVal, Date endDate) {
		
		//In BIRT Report - If raw value needed for calculation, need to manually add property (eg. minDepthVal_raw)
		ReportDataNode thisReportNode = reportDataNode.addChild("code");
		thisReportNode.addProperty("name", phase);
		thisReportNode.addProperty("phaseName", phaseName);
		thisReportNode.addProperty("planReferenceUid", planReferenceUid);
		thisReportNode.addProperty("planReferenceDescription", planName);
		thisReportNode.addProperty("duration", totalDuration.toString());	
		thisReportNode.addProperty("runningTotalDuration", currentRunningTotalDuration.toString());
		thisReportNode.addProperty("runningTotalDurationDay", currentRunningTotalDurationDay.toString());				
		thisReportNode.addProperty("minDepthVal", minDepth.toString());
		thisReportNode.addProperty("minDepthDisplay", minDepthDisplay);
		thisReportNode.addProperty("maxDepthVal", maxDepth.toString());
		thisReportNode.addProperty("maxDepthDisplay", maxDepthDisplay);
		thisReportNode.addProperty("startDate", String.valueOf(startDateVal));
		thisReportNode.addProperty("startDateDisplay", startDate.toString());
		thisReportNode.addProperty("endDate", String.valueOf(endDateVal));
		thisReportNode.addProperty("endDateDisplay", endDate.toString());
	}
}
