package com.idsdatanet.d2.drillnet.cementJob;

import java.lang.reflect.Field;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.model.CementJob;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CementStage;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CementJobCommandBeanListener extends EmptyCommandBeanListener  {
	private Boolean casingSizeBasedOnHoleSize = false;	
	
	public void setCasingSizeBasedOnHoleSize(Boolean value) {
		this.casingSizeBasedOnHoleSize = value;
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(CementJob.class)){
				if("previousCasingSectionUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					String casingUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "previousCasingSectionUid");	
					CementJobUtils.getCasingSection(targetCommandBeanTreeNode, casingUid, "previousCasingSection");
				}else if("casingSectionUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					String casingUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "casingSectionUid");
					CementJobUtils.getCasingSection(targetCommandBeanTreeNode, casingUid, "casingSection");
					CementJobUtils.getRatHole(targetCommandBeanTreeNode, casingUid, "casingSection");
				}else if("holeSize".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					if (this.casingSizeBasedOnHoleSize){
						Double holeSize = (Double) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "holeSize");
						UserSelectionSnapshot userSelection= new UserSelectionSnapshot(UserSession.getInstance(request));
						if(holeSize != null){
							if (StringUtils.isNotBlank(holeSize.toString())) {
								CementJobUtils.populateCasingSizeBasedOnHoleSize(targetCommandBeanTreeNode, userSelection, holeSize, userSelection.getOperationUid());
								CementJobUtils.populatePrevCasingSizeBasedOnHoleSize(targetCommandBeanTreeNode, userSelection, holeSize, userSelection.getOperationUid());
							}else{
								targetCommandBeanTreeNode.getDynaAttr().put("casingSizeBasedOnHoleSize", null);
								targetCommandBeanTreeNode.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", null);
							}
						}else{
							targetCommandBeanTreeNode.getDynaAttr().put("casingSizeBasedOnHoleSize", null);
							targetCommandBeanTreeNode.getDynaAttr().put("prevCasingSizeBasedOnHoleSize", null);
						}
					}	
				}
			}
		}
		//commandBean.getRoot().setDirty(true); why are we setting this ? ssjong
	}
	
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		CustomFieldUom converter = new CustomFieldUom(commandBean);
		
		// set units for standard fields in CasingSection.class
		for (Field field : CasingSection.class.getDeclaredFields()) {
			converter.setReferenceMappingField(CasingSection.class, field.getName());
			if (converter.isUOMMappingAvailable()) {
				commandBean.setCustomUOM(CementJob.class, "@casingSection_" + field.getName(), converter.getUOMMapping());
			}
			
			if (converter.isUOMMappingAvailable()) {
				commandBean.setCustomUOM(CementJob.class, "@previousCasingSection_" + field.getName(), converter.getUOMMapping());
			}
		}
		
		for (Field field : CasingSection.class.getDeclaredFields()) {
			converter.setReferenceMappingField(CasingSection.class, field.getName());
			if (converter.isUOMMappingAvailable()) {
				commandBean.setCustomUOM(CementStage.class, "@csg_cementStage_" + field.getName(), converter.getUOMMapping());
			}
		}
		
		for (Field field : MudProperties.class.getDeclaredFields()) {
			converter.setReferenceMappingField(MudProperties.class, field.getName());
			if (converter.isUOMMappingAvailable()) {
				commandBean.setCustomUOM(CementJob.class, "@mudProperties_" + field.getName(), converter.getUOMMapping());
			}
		}
		
		for (Field field : Well.class.getDeclaredFields()) {
			converter.setReferenceMappingField(Well.class, field.getName());
			if (converter.isUOMMappingAvailable()) {
				commandBean.setCustomUOM(CementJob.class, "@well_" + field.getName(), converter.getUOMMapping());
			}
		}
		
		for (Field field : Operation.class.getDeclaredFields()) {
			converter.setReferenceMappingField(Operation.class, field.getName());
			if (converter.isUOMMappingAvailable()) {
				commandBean.setCustomUOM(CementJob.class, "@operation_" + field.getName(), converter.getUOMMapping());
			}
		}
		
		
	}
	
}
