package com.idsdatanet.d2.drillnet.activity;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @author - for carl to try
 * This class help to mine data for activity duration for the selected daily group by phase code
 * for daily reporting use
 * 
 * configuration is set in ddr.xml
 */

public class DailyActivityDurationByTaskCodeReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "SELECT taskCode, SUM(activityDuration) AS totalDuration FROM Activity WHERE (isDeleted = false OR isDeleted IS NULL) AND dailyUid =:thisDailyUid AND " +
			"((dayPlus IS NULL OR dayPlus=0) AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL)) GROUP BY taskCode";
		String[] paramsFields = {"thisDailyUid"};
		Object[] paramsValues = {userContext.getUserSelection().getDailyUid()};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			for(Object objResult: lstResult){
				//due to result is in array, cast the result to array first and read 1 by 1
				Object[] objDuration = (Object[]) objResult;
				
				//default class to program, cause some record might have no class code
				String taskCode = "N/A";
				Double totalDuration = 0.0;
				String taskName = "N/A";
				
				if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())) {
					taskCode = objDuration[0].toString();
					String strSql2 = "SELECT name FROM LookupTaskCode WHERE (isDeleted is null OR isDeleted = false) AND shortCode = :thisTaskCode AND (operationCode = :thisOperationType OR operationCode = '')";
					String[] paramsFields2 = {"thisTaskCode", "thisOperationType"};
					Object[] paramsValues2 = {taskCode, userContext.getUserSelection().getOperationType().toString()};
					
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
					if (lstResult2.size()>0)
					{
						Object lookupTaskCodeResult = (Object) lstResult2.get(0);
						if(lookupTaskCodeResult != null) taskName = lookupTaskCodeResult.toString();
					}
				}
				if (objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())){
					totalDuration = Double.parseDouble(objDuration[1].toString());
					
					//assign unit to the value base on activity.activityDuration, this will do auto convert to user display unit + format
					CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
					thisConverter.setBaseValue(totalDuration);
					totalDuration = thisConverter.getConvertedValue();
				}
				
				ReportDataNode thisReportNode = reportDataNode.addChild("code");
				thisReportNode.addProperty("name", taskCode);
				thisReportNode.addProperty("duration", totalDuration.toString());
				thisReportNode.addProperty("taskName", taskName);
			}			
		}
		
	}
	
}
