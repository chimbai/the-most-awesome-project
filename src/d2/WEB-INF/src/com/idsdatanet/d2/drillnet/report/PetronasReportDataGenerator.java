package com.idsdatanet.d2.drillnet.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.GasReadings;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.LookupRigStock;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.commonLookup.CommonLookupUtil;

public class PetronasReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		List<String> bhaList = new ArrayList<String>();
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
		Map<String,LookupItem> bitSizeLookup= LookupManager.getConfiguredInstance().getLookup("xml://bitsize?key=code&amp;value=label", userContext.getUserSelection(), null);
		Map<String,LookupItem> bitTypeLookup= LookupManager.getConfiguredInstance().getLookup("xml://bitrun.bit_type?key=code&amp;value=label", userContext.getUserSelection(), null);
		
		List<Bharun> resultBha = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From Bharun Where (isDeleted is null or isDeleted = false) and operationUid=:operationUid ORDER BY depthInMdMsl DESC", "operationUid",userSelection.getOperationUid());
		if (resultBha.size()>0){
			Integer countBit = 1;
			
			for(Bharun bha : resultBha){
				if(bha.getBharunUid()!=null){
					//bhaList.add(bha.getBharunUid());
					//if (bhaList.size()>0){
						
						List<Bitrun> resultBit = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From Bitrun Where (isDeleted is null or isDeleted = false) and bharunUid=:bharunUid ", "bharunUid", bha.getBharunUid());					

						if (resultBit.size()>0){
							for (Bitrun bit : resultBit){
								Boolean proceed = false;
								
								if (bit.getDailyUidIn()!=null){
									
									Daily bitDailyIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(bit.getDailyUidIn());
									if (bitDailyIn!=null && bitDailyIn.getDayDate().getTime()<= todayDate.getTime()){
											
										if (bit.getDailyUidOut()!=null){
											Daily bitDailyOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(bit.getDailyUidOut());
											
											if (bitDailyOut!=null && bitDailyOut.getDayDate().getTime()>=todayDate.getTime()){								
												proceed = true;
											}else if(bitDailyOut==null){
												proceed = true;
											}
										}				
									}
								}
								
								if (proceed){
									ReportDataNode child = reportDataNode.addChild("Bitrun");
									child.addProperty("count", countBit.toString());
									child.addProperty("bitrunNumber", bit.getBitrunNumber());	
									
									Bharun bharun = (Bharun) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Bharun.class, bit.getBharunUid());					
									if (bharun!=null){
										child.addProperty("bharunUid", bharun.getBhaRunNumber());

										if (bharun.getDepthInMdMsl()!=null){
											thisConverter.setReferenceMappingField(Bharun.class, "depthInMdMsl");
											child.addProperty("depthInMdMsl", thisConverter.formatOutputPrecision(bharun.getDepthInMdMsl()));
										}
										
										if(bharun.getDepthOutMdMsl()!=null){
											thisConverter.setReferenceMappingField(Bharun.class, "depthOutMdMsl");
											child.addProperty("depthOutMdMsl",  thisConverter.formatOutputPrecision(bharun.getDepthOutMdMsl()));
										}
									}
									
									String bitDiameter="";
									if (bit.getBitDiameter()!=null){
										thisConverter.setReferenceMappingField(Bitrun.class, "bitDiameter");
										thisConverter.setBaseValueFromUserValue(bit.getBitDiameter());
										bitDiameter = thisConverter.getFormattedValue();
										child.addProperty("bitDiameter", getLookupValue(bitSizeLookup,bitDiameter.replace(thisConverter.getUomSymbol(), "").trim()));
									}					
									
									if (bit.getMake()!=null){							
										LookupCompany lookupCompany = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, bit.getMake());
										if (lookupCompany!=null && lookupCompany.getCompanyName()!=null) child.addProperty("make", lookupCompany.getCompanyName());
									}
									
									if (bit.getBitType()!=null){
										child.addProperty("bitType", getLookupValue(bitTypeLookup,bit.getBitType()));
									}
									child.addProperty("serialNumber", bit.getSerialNumber());
									child.addProperty("model", bit.getModel());
									
									if(bit.getTfa()!=null){
										thisConverter.setReferenceMappingField(Bitrun.class, "tfa");
										child.addProperty("tfa", thisConverter.formatOutputPrecision(bit.getTfa()));
									}
									
									String condOther = "";
									if(bit.getCondFinalOther()!=null){
										Integer count = 0;
										String[] other = bit.getCondFinalOther().split("\t");
										 for (int i = 0; i < other.length; i++) {
											 if (count>0) condOther= condOther + ";" + other[i].toString();
											 else condOther= condOther + other[i].toString();
											 count++;
										 }
				
									}
									child.addProperty("IODL", bit.getCondFinalInner() + "/" + bit.getCondFinalOuter() + "/" + bit.getCondFinalDull() + "/" + bit.getCondFinalLocation());
									child.addProperty("BGOR", bit.getCondFinalBearing() + "/" + bit.getCondFinalGauge() + "/" + condOther + "/" + bit.getCondFinalReason());
									child.addProperty("condFinalInner", bit.getCondFinalInner());
									child.addProperty("condFinalOuter", bit.getCondFinalOuter());
									child.addProperty("condFinalDull", bit.getCondFinalDull());
									child.addProperty("condFinalLocation", bit.getCondFinalLocation());
									child.addProperty("condFinalBearing", bit.getCondFinalBearing());
									child.addProperty("condFinalGauge", bit.getCondFinalGauge());
									child.addProperty("condFinalOther", bit.getCondFinalOther());
									child.addProperty("condFinalReason", bit.getCondFinalReason());
									
									
									List<DrillingParameters> resultDp = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM DrillingParameters WHERE (isDeleted is null or isDeleted = false) AND bharunUid =:bharunUid ORDER BY depthTopMdMsl DESC", "bharunUid", bit.getBharunUid());
									
									if (resultDp.size()>0){
										for (DrillingParameters dp : resultDp){
											if (dp.getWobMinForce()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "wobMinForce");
												child.addProperty("wobMinForce", thisConverter.formatOutputPrecision(dp.getWobMinForce()));
											}
											
											if(dp.getWobMaxForce()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "wobMaxForce");
												child.addProperty("wobMaxForce", thisConverter.formatOutputPrecision(dp.getWobMaxForce()));
											}
											
											if(dp.getSurfaceRpmMin()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "surfaceRpmMin");
												child.addProperty("surfaceRpmMin", thisConverter.formatOutputPrecision(dp.getSurfaceRpmMin()));
											}
											
											if(dp.getSurfaceRpmMax()!=null){
												thisConverter.setReferenceMappingField(DrillingParameters.class, "surfaceRpmMax");
												child.addProperty("surfaceRpmMax", thisConverter.formatOutputPrecision(dp.getSurfaceRpmMax()));
											}
											
											break;
										}
									}
									
									countBit++;
								}
							}
						}
					//}
				}
			}
		}
		
		
		
		
		List<MudProperties> resultMud = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM MudProperties WHERE (isDeleted is null or isDeleted = false) AND dailyUid =:dailyUid ORDER BY reportTime DESC", "dailyUid", daily.getDailyUid());
		Integer countMud = 1;

		if (resultMud.size()>0){
			Map<String,LookupItem> mudTypeLookup= LookupManager.getConfiguredInstance().getLookup("xml://mud_properties.mud_type?key=code&amp;value=label", userContext.getUserSelection(), null);
			
			for (MudProperties mud : resultMud){
				ReportDataNode child = reportDataNode.addChild("MudCheck");
				child.addProperty("count", countMud.toString());
				
				if (mud.getCumMudCost()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudCost");	
					child.addProperty("cumMudCost", thisConverter.formatOutputPrecision(mud.getCumMudCost()));
				}
				
				if (mud.getMudCost()!=null){
					child.addProperty("mudCost", thisConverter.formatOutputPrecision(mud.getMudCost()));
				}
				
				if(mud.getMudType()!=null)	child.addProperty("mudType", getLookupValue(mudTypeLookup, mud.getMudType()));
				
				if(mud.getMudTypeSystem()!=null){
					String lookupKey = CommonLookupUtil.checkForCommonLookupKey("mudProperties.mudTypeSystem");
					Map<String, LookupItem> lookup = LookupManager.getConfiguredInstance().getLookup(lookupKey, userSelection, null);
					if (lookup.containsKey(mud.getMudTypeSystem())){
						LookupItem lookupDescItem = lookup.get(mud.getMudTypeSystem());
						
						child.addProperty("mudTypeSystem_lookup", lookupDescItem.getValue().toString());
					}
				}
				
				child.addProperty("mudTypeSystem", mud.getMudTypeSystem());
				child.addProperty("reportTime", mud.getReportTime()!=null?mud.getReportTime().toString():"");
				
				if (mud.getDepthMdMsl()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "depthMdMsl");
					child.addProperty("depthMdMsl", thisConverter.formatOutputPrecision(mud.getDepthMdMsl()));
				}
				
				if (mud.getMudTestTemperature()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudTestTemperature");
					child.addProperty("mudTestTemperature", thisConverter.formatOutputPrecision(mud.getMudTestTemperature()));
				}
				
				if(mud.getMudWeight()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudWeight");
					child.addProperty("mudWeight", thisConverter.formatOutputPrecision(mud.getMudWeight()));
				}
				
				if(mud.getMudFv()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudFv");
					child.addProperty("mudFv", thisConverter.formatOutputPrecision(mud.getMudFv()));
				}
				
				if(mud.getMudPv()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudPv");
					child.addProperty("mudPv", thisConverter.formatOutputPrecision(mud.getMudPv()));
				}
				
				if( mud.getMudYp()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudYp");
					child.addProperty("mudYp", thisConverter.formatOutputPrecision(mud.getMudYp()));
				}
				
				if(mud.getMudGel10s()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudGel10s");
					child.addProperty("mudGel10s", thisConverter.formatOutputPrecision(mud.getMudGel10s()));
				}
				
				if (mud.getMudGel10m()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudGel10m");
					child.addProperty("mudGel10m",  thisConverter.formatOutputPrecision(mud.getMudGel10m()));
				}
				
				if (mud.getMudGel30m()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudGel30m");
					child.addProperty("mudGel30m",  thisConverter.formatOutputPrecision(mud.getMudGel30m()));
				}
				
				if (mud.getMudExcessLimeDensity()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudExcessLimeDensity");
					child.addProperty("mudExcessLimeDensity", thisConverter.formatOutputPrecision(mud.getMudExcessLimeDensity()));
				}
				
				if (mud.getMudPh()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudPh");
					child.addProperty("mudPh", thisConverter.formatOutputPrecision(mud.getMudPh()));
				}
				
				if (mud.getMudMfVolume()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudMfVolume");
					child.addProperty("mudMfVolume", thisConverter.formatOutputPrecision(mud.getMudMfVolume()));
				}
				
				if (mud.getKplusConcentration()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "kplusConcentration");
					child.addProperty("kplusConcentration", thisConverter.formatOutputPrecision(mud.getKplusConcentration()));
				}
				
				if (mud.getSandTxt()!=null){
					child.addProperty("sandTxt", mud.getSandTxt());
				}
				
				if(mud.getMudDissolvedSolidsConcentration()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudDissolvedSolidsConcentration");
					child.addProperty("mudDissolvedSolidsConcentration", thisConverter.formatOutputPrecision(mud.getMudDissolvedSolidsConcentration()));
				}
				
				if(mud.getMudOil()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudOil");
					child.addProperty("mudOil", thisConverter.formatOutputPrecision(mud.getMudOil()));
				}
				
				if(mud.getRetortH2oPcVolpervol()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "retortH2oPcVolpervol");
					child.addProperty("retortH2oPcVolpervol", thisConverter.formatOutputPrecision(mud.getRetortH2oPcVolpervol()));
				}
				
				if (mud.getCaclMud()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "caclMud");
					child.addProperty("caclMud", thisConverter.formatOutputPrecision(mud.getCaclMud()));
				}
				
				if (mud.getEcd()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "ecd");
					child.addProperty("ecd", thisConverter.formatOutputPrecision(mud.getEcd()));
				}
				
				if (mud.getMudApiFl()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudApiFl");
					child.addProperty("mudApiFl", thisConverter.formatOutputPrecision(mud.getMudApiFl()));
				}
				
				if (mud.getHthpFl()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "hthpFl");
					child.addProperty("hthpFl", thisConverter.formatOutputPrecision(mud.getHthpFl()));
				}
				
				if (mud.getMudApiCake()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudApiCake");
					child.addProperty("mudApiCake", thisConverter.formatOutputPrecision(mud.getMudApiCake()));
				}
				
				if (mud.getMudMbtDensity()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudMbtDensity");
					child.addProperty("mudMbtDensity", thisConverter.formatOutputPrecision(mud.getMudMbtDensity()));
				}
				
				if (mud.getMudPmVolume()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudPmVolume");
					child.addProperty("mudPmVolume", thisConverter.formatOutputPrecision(mud.getMudPmVolume()));
				}
				
				if (mud.getMudPfVolume()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudPfVolume");
					child.addProperty("mudPfVolume", thisConverter.formatOutputPrecision(mud.getMudPfVolume()));
				}
				
				if (mud.getCalciumChlorideConcentration()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "calciumChlorideConcentration");
					child.addProperty("calciumChlorideConcentration", thisConverter.formatOutputPrecision(mud.getCalciumChlorideConcentration()));
				}
				
				if (mud.getPolymerDensity()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "polymerDensity");
					child.addProperty("polymerDensity", thisConverter.formatOutputPrecision(mud.getPolymerDensity()));
				}
				
				if (mud.getMudNacl()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudNacl");
					child.addProperty("mudNacl", thisConverter.formatOutputPrecision(mud.getMudNacl()));
				}
				
				if (mud.getWaterOilRatio()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "waterOilRatio");
					child.addProperty("waterOilRatio", thisConverter.formatOutputPrecision(mud.getWaterOilRatio()));
				}
				
				if (mud.getHgsWtDensity()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "hgsWtDensity");
					child.addProperty("hgsWtDensity", thisConverter.formatOutputPrecision(mud.getHgsWtDensity()));
				}
				
				if (mud.getLgsWtDensity()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "lgsWtDensity");
					child.addProperty("lgsWtDensity", thisConverter.formatOutputPrecision(mud.getLgsWtDensity()));
				}
				
				if (mud.getMudH2o()!=null){
					thisConverter.setReferenceMappingField(MudProperties.class, "mudH2o");
					child.addProperty("mudH2o", thisConverter.formatOutputPrecision(mud.getMudH2o()));
				}
				
				child.addProperty("owratio", mud.getOwratio());
				child.addProperty("mudEngineerSummary", mud.getMudEngineerSummary());
				
				countMud++;
			}
		}
		
		List<GasReadings> resultGas = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM GasReadings WHERE (isDeleted is null or isDeleted = false) AND dailyUid =:dailyUid ORDER BY totalgasvalue DESC", "dailyUid", daily.getDailyUid());
		Double totalTrip = 0.00;
		Double totalConnection = 0.00;
		Double totalBackground = 0.00;
		Integer countTrip = 0;
		Integer countConnection= 0;
		Integer countBackground= 0;
		Boolean trip = true;
		Boolean connection = true;
		Boolean background = true;
		
		if (resultGas.size()>0){
			ReportDataNode child = reportDataNode.addChild("GasReadings");
			
			for (GasReadings mud : resultGas){
				
				if ("Trip".equals(mud.getGastype())){
					if (trip){
						child.addProperty("max_totalgasvalue_trip", mud.getTotalgasvalue()!=null?mud.getTotalgasvalue().toString():"");
						trip = false;
					}
					
					if (mud.getTotalgasvalue()!=null){
						totalTrip = totalTrip + mud.getTotalgasvalue();
						countTrip ++;
					}
					
				}else if ("Connection".equals(mud.getGastype())){
					if (connection){
						child.addProperty("max_totalgasvalue_connection", mud.getTotalgasvalue()!=null?mud.getTotalgasvalue().toString():"");
						connection = false;
					}
					
					if (mud.getTotalgasvalue()!=null){
						totalConnection = totalConnection + mud.getTotalgasvalue();
						countConnection ++;
					}
				}else if ("Background".equals(mud.getGastype())){
					if (background){
						child.addProperty("max_totalgasvalue_background", mud.getTotalgasvalue()!=null?mud.getTotalgasvalue().toString():"");
						background = false;
					}
					
					if (mud.getTotalgasvalue()!=null){
						totalBackground = totalBackground + mud.getTotalgasvalue();
						countBackground ++;
					}
				}					
			}
			
			
			if (countTrip>0) {
				totalTrip = totalTrip/countTrip;
			}
			
			if (countConnection>0) {
				totalConnection = totalConnection/countConnection;
			}
			
			if (countBackground>0) {
				totalBackground = totalBackground/countBackground;
			}
			
			child.addProperty("totalgasvalue_ave_trip", totalTrip.toString());
			child.addProperty("totalgasvalue_ave_connection", totalConnection.toString());
			child.addProperty("totalgasvalue_ave_background", totalBackground.toString());
			
		}
		
		List<RigStock> resultStock = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM RigStock WHERE (isDeleted is null or isDeleted = false) AND dailyUid =:dailyUid ORDER BY sequence ", "dailyUid", daily.getDailyUid());
		ReportDataNode child = reportDataNode.addChild("RigStock");
		
		if (resultStock.size()>0){
			int diff = 0;
			if (resultStock.size()>11){
				diff = resultStock.size() - 11;
			}
			int seq = 1 - diff;
			
			for (RigStock stock : resultStock){
				
				LookupRigStock lookupRigStock = (LookupRigStock) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigStock.class, stock.getStockCode());
				if (lookupRigStock!=null)	child.addProperty("stockCode_"+seq, lookupRigStock.getStockBrand());
				
				child.addProperty("storedUnit_"+seq, stock.getStoredUnit());
				
				if (stock.getStockCode()!=null){
					Double balance = this.getBalance(stock, daily, daily.getOperationUid(), thisConverter);
					child.addProperty("currentBalance_"+seq, balance.toString());
				}
				
				child.addProperty("amtUsed_"+seq, stock.getAmtUsed().toString());
				child.addProperty("amtStart_"+seq, stock.getAmtStart().toString());
				child.addProperty("amtDiscrepancy_"+seq, stock.getAmtDiscrepancy().toString());
				
				seq++;
			}
		}else{
			for (int seq=1; seq<12; seq++){
				child.addProperty("stockCode_"+seq, "");
				child.addProperty("storedUnit_"+seq, "");
				child.addProperty("currentBalance_"+seq, "");
				child.addProperty("amtUsed_"+seq, "");
				child.addProperty("amtStart_"+seq, "");
				child.addProperty("amtDiscrepancy_"+seq, "");
			}
		}
	}
	
	private String getLookupValue(Map<String,LookupItem> lookupList, Object lookupvalue) throws Exception {
		String retValue = "";
		
		if (lookupvalue !=null && StringUtils.isNotBlank(lookupvalue.toString())  && lookupList !=null){
			 LookupItem lookup = lookupList.get(lookupvalue.toString());
			if (lookup !=null)	retValue = lookup.getValue().toString();					
		} 
		
		return retValue;
	}
	
	private Double getBalance(RigStock rigstock, Daily daily, String operationUid, CustomFieldUom thisConverter) throws Exception{

		String stockcode = rigstock.getStockCode();
		Double amtstart = rigstock.getAmtStart();
		Double amtused = rigstock.getAmtUsed();
		Double amtadjust = rigstock.getAmtDiscrepancy();
		Double trftorig = rigstock.getTrfToRig();
		Double trftobeach = rigstock.getTrfToBeach();
		String supportVesselInformationUid = rigstock.getSupportVesselInformationUid(); 
	
		if (amtstart == null) amtstart = 0.0;
		if (amtused == null) amtused = 0.0;
		if (amtadjust == null) amtadjust = 0.0;
		if (trftorig == null) trftorig = 0.0;
		if (trftobeach == null) trftobeach = 0.0;
		
		Double amttoday = amtstart - amtused + amtadjust - trftorig - trftobeach;
		
		if (StringUtils.isNotBlank(stockcode)){
			
			Double previousBalance = 0.0;
			Double currentBalance = amttoday;
			
			thisConverter.setReferenceMappingField(RigStock.class, "amtInitial");

			if (rigstock.getAmtInitial()==null){
				previousBalance = CommonUtil.getConfiguredInstance().getOperationSupportVesselStockcodePreviousBalance(operationUid, supportVesselInformationUid, stockcode, daily);
			}else{
				thisConverter.setBaseValueFromUserValue(rigstock.getAmtInitial());
				previousBalance = thisConverter.getBasevalue();
			}
			
			currentBalance = previousBalance + amttoday;
			thisConverter.setBaseValue(currentBalance);
			return thisConverter.getConvertedValue();
			
		}else{
			thisConverter.setBaseValue(0);
			return thisConverter.getConvertedValue();
		} 
	}
}
