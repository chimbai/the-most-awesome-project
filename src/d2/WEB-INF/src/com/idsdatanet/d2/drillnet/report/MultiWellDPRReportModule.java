package com.idsdatanet.d2.drillnet.report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;

/**
 * Extending the default report module to handle multiple well reports
 * 
 * @author William
 *
 */
public class MultiWellDPRReportModule extends DefaultReportModule {
	private static final String FIELD_SELECTED_OPS = "reportSelectedOperations";
	private static final String FIELD_SELECTED_YEAR = "yearSelected";
	protected boolean useDayForReference = false;
	private Date selectedDay = null;
	
	public void setUseDayForReference(boolean value){
		this.useDayForReference = value;
	}
	
	public boolean getUseDayForReference(){
		return this.useDayForReference;
	}
	
	protected void beforeSaveReportEntry(JobContext jobContext, ReportFiles report) throws Exception{
		if (useDayForReference && selectedDay != null){
			report.setReportDayDate(selectedDay);
			report.setReportOperationUid(null);
		}
	}
	
	protected String parseDisplayNameParam(String key, ReportFiles reportFiles, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) {
		try{
			if(FILE_NAME_PATTERN_DATE.equalsIgnoreCase(key)){
				if(reportFiles.getReportDayDate() != null){
					return null2EmptyString(ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(reportFiles.getReportDayDate()));
				}else{
					return super.parseDisplayNameParam(key, reportFiles, operation, daily, reportDaily, userSelection, reportJobParams);
				}
			} else {
				return super.parseDisplayNameParam(key, reportFiles, operation, daily, reportDaily, userSelection, reportJobParams);
			}
		} catch(Exception e){
			return null;
		}
	}
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern("yyyy-MM-dd");
		if (useDayForReference) {
			return this.getReportType(userContext.getUserSelection()) + 
			"/" + sdf.format(selectedDay) +
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize));
		} else {
			return this.getReportType(userContext.getUserSelection()) + 
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize));
		}
	}
	
	/**
	 * Method to obtain years selected in the report page
	 * 
	 * @param commandBean
	 * @return
	 * @throws Exception
	 
	protected List<String> getSelectedYear(CommandBean commandBean,UserSession session) throws Exception {
		
		Object selected_year = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_YEAR);
		if(selected_year == null){
			return getAllAvailableOperations(session);
		}
		if(StringUtils.isBlank(selected_year.toString())){
			return getAllAvailableOperations(session);
		}
		
		String[] operation_list = selected_year.toString().split(",");
		List<String> result = new ArrayList<String>();
		for(String id: operation_list){
			result.add(id.trim());
		}
		return result;
	}
*/
	/**
	 * Helper method to obtain all the operation uids selected in the report page
	 * 
	 * @param commandBean
	 * @return
	 * @throws Exception
	 */
	protected List<String> getSelectedOperations(CommandBean commandBean,UserSession session) throws Exception {
		Object selected_operations = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_OPS);
		if(selected_operations == null){
			return getAllAvailableOperations(session);
		}
		if(StringUtils.isBlank(selected_operations.toString())){
			return getAllAvailableOperations(session);
		}
		
		String[] id_list = selected_operations.toString().split(",");
		List<String> result = new ArrayList<String>();
		for(String id: id_list){
			result.add(id.trim());
		}
		return result;
	}
	
	/**
	 * Helper method to return all available operations if none are selected
	 * 
	 * @param session
	 * @return
	 * @throws Exception
	 */
	private List<String> getAllAvailableOperations(UserSession session) throws Exception {
		Operations availableOperations = session.getCachedAllAccessibleOperations();
		List<String> result = new ArrayList<String>();
		for (_Operation op:availableOperations.values()){
			result.add(op.getOperationUid());
		}
		return result;
	}
	
	/**
	 * Set report job to use the multi-well setting
	 */
	protected ReportJobParams getParametersForReportSubmission(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		ReportJobParams params = super.getParametersForReportSubmission(session, request, commandBean);
		params.setMultiWellJob(true);
		return params;
	}

	/**
	 * Process the data before submitting the report for generation
	 */
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		List<String> list = this.getSelectedOperations(commandBean,session);
	//	List<String> list2 = this.getSelectedYear(commandBean,session);
		
		if (list != null && list.size() > 0) {
			if (session.getCurrentDaily() != null){
				selectedDay = session.getCurrentDaily().getDayDate();
			}
			List<UserSelectionSnapshot> selections = new ArrayList<UserSelectionSnapshot>();
			for(String operationUid: list){
				String dailyUid = null;
				if (session.getCurrentDaily() != null){
					Date currentDate = session.getCurrentDaily().getDayDate();
					SimpleDateFormat sdf = new SimpleDateFormat();
					sdf.applyPattern("yyyy-MM-dd");
					List<Daily> dailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().find("FROM Daily WHERE operationUid='" + 
							operationUid + "' and dayDate LIKE '" + sdf.format(currentDate) + "%'");
					if (dailyList.size() > 0){
						dailyUid = dailyList.get(0).getDailyUid();
					}
				}
				selections.add(UserSelectionSnapshot.getInstanceForCustomDaily(session, operationUid,dailyUid));
			}
			return super.submitReportJob(selections, this.getParametersForReportSubmission(session, request, commandBean));
			
		} else {
			return super.submitReportJob(UserSelectionSnapshot.getInstanceForCustomDaily(session, session.getCurrentDailyUid()), this.getParametersForReportSubmission(session, request, commandBean));		
		}
	}
}
