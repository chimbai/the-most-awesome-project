package com.idsdatanet.d2.drillnet.report;


import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.JobListener;
import com.idsdatanet.d2.core.job.JobServer;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.SystemReportUtils;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxActionHandler;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxParameter;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;
import com.idsdatanet.d2.core.web.webservice.SimpleXmlResponse;
import com.idsdatanet.depot.witsml.DepotPremierManager;

public abstract class BPAbstractReportModule implements DataNodeLoadHandler, CommandBeanListener, SimpleAjaxActionHandler, JobListener, ActionManager, DisposableBean, DataNodeListener {
	public static final String CUSTOM_ACTION_SYNCHRONOUS_GENERATE_REPORT = "synchronousGenerateReport";
	private static final String FIELD_EMAIL_ADDRESS ="emailAddress";
	public String emailAddress = null;
	public String systemMsg = null;
	private ReportAutoEmail emailConfiguration=null;
	
	private ReportFileListener reportFilesListener = null;
	public ReportFileListener getReportFilesListener() {
		return reportFilesListener;
	}

	public void setReportFilesListener(ReportFileListener reportFilesListener) {
		this.reportFilesListener = reportFilesListener;
	}

	
	private class ReportOutputFilesComparator implements Comparator<Object> {
		public int compare(Object o2, Object o1) {
			ReportOutputFile f1 = (ReportOutputFile) o1;
			ReportOutputFile f2 = (ReportOutputFile) o2;
			
			if(f1.getDayDate() != null && f2.getDayDate() != null){
				return f1.getDayDate().compareTo(f2.getDayDate());
			}else if(f1.getDayDate() != null){
				return 1;
			}else if(f2.getDayDate() != null){
				return -1;
			}else{
				return 0;
			}
		}
	}
	
	private class UpdateReportActionHandler implements ActionHandler {
		UpdateReportActionHandler(){
		}
		
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception{
			if(node.getData() instanceof ReportOutputFile){
				try{
					this.update((ReportOutputFile) node.getData());
					return new ActionHandlerResponse(true, false);
				}catch(Exception e){
					e.printStackTrace();
					return new ActionHandlerResponse(false, false, e.getMessage());
				}
			}else{
				return new ActionHandlerResponse(false, false, "Unknown object, expecting " + ReportOutputFile.class.getName());
			}
		}
		
		private void update(ReportOutputFile report) throws Exception {
			
			ReportFiles reportFile = (ReportFiles) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ReportFiles.class, report.getReportFilesUid());
			reportFile.setIsPrivate(report.getIsPrivate());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportFile);
			if (BPAbstractReportModule.this.reportFilesListener != null)
			{
				BPAbstractReportModule.this.reportFilesListener.onUpdateReport(reportFile);
			}
		}
	}
	
	private class SynchronousGenerateReport implements ActionHandler {
		private BPAbstractReportModule parent = null;
		
		SynchronousGenerateReport(BPAbstractReportModule parent){
			this.parent = parent;
		}
		
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception{
			this.parent.submitReportJob(userSession, request, commandBean);
			int count = 0;
			while(this.parent.isUserSessionReportJobRunning()){
				++count;
				if(count > 600) break; // max run for approximately 5 minutes
				Thread.sleep(500);
			}
			
			if(! this.parent.isUserSessionReportJobRunning()){
				if(this.parent.userSessionReportJob.getJobResponse().isSuccess()){
					return new ActionHandlerResponse(true, true);
				}else{
					return new ActionHandlerResponse(false, true, "Failed to generate report [" + this.parent.userSessionReportJob.getJobResponse().getResponseMessage() + "]");
				}
			}else{
				return new ActionHandlerResponse(false, true, "Failed to generate report [still generating in progress]");
			}
		}
	}
	
	private class DeleteReportActionHandler implements ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception{
			if(node.getData() instanceof ReportOutputFile){
				try{
					this.delete((ReportOutputFile) node.getData());
					return new ActionHandlerResponse(true, false);
				}catch(Exception e){
					e.printStackTrace();
					return new ActionHandlerResponse(false, false, e.getMessage());
				}
			}else{
				return new ActionHandlerResponse(false, false, "Unknown object, expecting " + ReportOutputFile.class.getName());
			}
		}
		
		private void delete(ReportOutputFile report) throws Exception {
			String reportFilePath = report.getReportFilePath();
			if (StringUtils.isNotBlank(reportFilePath)) {
				File reportFile = new File(ReportUtils.getFullOutputFilePath(report.getReportFilePath()));
				if(reportFile.exists()) reportFile.delete();
			}
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update ReportFiles set isDeleted = true where reportFilesUid = :reportFilesUid", new String[] { "reportFilesUid" }, new Object[] { report.getReportFilesUid() });
		}
	}

	protected ConcurrentHashMap<JobContext,ReportJobParams> reportJobs = new ConcurrentHashMap<JobContext,ReportJobParams>();
	protected JobContext userSessionReportJob = null; // this is for report job instigated from user interaction (gui), this module can handle multiple jobs (some maybe from scheduler)
	private Log logger = LogFactory.getLog(this.getClass());
	private MailEngine mailEngine = null;
	private long reportJobTimeoutMilliSeconds = 300000; // default to 5 minutes
	private Locale reportLocale = null;
	private boolean showAllDays = false;
	private boolean dailyTypeByPriority=false;
	private String reportDateChooserSortOrder = "ASC";
	private BPReportModuleListener reportModuleListener = null;
	public ReportFiles reportGeneratedFiles = null;
	public boolean generateAndDownload = false;
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if(meta.getTableClass().equals(ReportOutputFile.class)){
			return this.loadReportOutputFiles(userSelection, request);
		}else{
			return null;
		}
	}

	public void setShowAllDays(boolean value){
		this.showAllDays = value;
	}
	
	public boolean isShowAllDays() {
		return this.showAllDays;
	}
	
	public String getReportDateChooserSortOrder() {
		return reportDateChooserSortOrder;
	}

	public void setReportDateChooserSortOrder(String reportDateChooserSortOrder) {
		this.reportDateChooserSortOrder = reportDateChooserSortOrder;
	}

	public void setMailEngine(MailEngine value){
		this.mailEngine = value;
	}
	
	public void setReportJobTimeoutMilliSeconds(long value){
		this.reportJobTimeoutMilliSeconds = value;
	}
	
	public void setReportLocale(String code) throws Exception {
		String[] codes = code.split("_");
		if(codes.length != 2) throw new Exception("Invalid locale code: " + code);
		this.reportLocale = new Locale(codes[0], codes[1]);
	}
	
	private Locale getReportLocaleObject() throws Exception {
		if(this.reportLocale != null) return this.reportLocale;
		return ApplicationConfig.getConfiguredInstance().getDefaultReportLocaleObject();
	}
	
	protected boolean isUserSessionReportJobRunning(){
		if(this.userSessionReportJob == null) return false;
		if(this.userSessionReportJob.disposed()) return false;
		if(this.userSessionReportJob.isTimedout()) return false;
		
		if(this.userSessionReportJob.isStarted()){
			return this.userSessionReportJob.isRunning();	
		}else{
			return true;
		}
	}

	public void setReportModuleListener(BPReportModuleListener value){
		this.reportModuleListener = value;
	}
	
	public void restartRefreshChecking(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, SimpleAjaxParameter simpleAjax) throws Exception{
		if("generateReport".equals(simpleAjax.getAction())){
			simpleAjax.setResponse("Generating Report. Please wait...", "", true, this.getSimpleAjaxRefreshInterval(), null);
		}else if("generateAndSent".equals(simpleAjax.getAction())){
			simpleAjax.setResponse("Generating Report. Please wait...", "", true, this.getSimpleAjaxRefreshInterval(), null);
		}else{
			simpleAjax.setResponse(null, "Unknow command: " + simpleAjax.getAction(), false, this.getSimpleAjaxRefreshInterval(), null);
		}
	}
	
	public void submitAction(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, SimpleAjaxParameter simpleAjax) throws Exception{
		if("generateReport".equals(simpleAjax.getAction())){
			try{
				this.submitReportJob(userSession, request, commandBean);
				simpleAjax.setResponse("Generating Report. Please wait...", "", true, this.getSimpleAjaxRefreshInterval(), null);
			}catch(Exception e){
				this.logger.error(e.getMessage(),e);
				simpleAjax.setResponse(null, "Error encountered when creating report: " + e.getMessage(), false, this.getSimpleAjaxRefreshInterval(), null);
			}
		}else if("generateAndSent".equals(simpleAjax.getAction())){
			try{
				this.submitReportJob(userSession, request, commandBean);
				simpleAjax.setResponse("Generating Report. Please wait...", "", true, this.getSimpleAjaxRefreshInterval(), null);
			}catch(Exception e){
				this.logger.error(e.getMessage(),e);
				simpleAjax.setResponse(null, "Error encountered when creating report: " + e.getMessage(), false, this.getSimpleAjaxRefreshInterval(), null);
			}
		}else{
			simpleAjax.setResponse(null, "Unknow command: " + simpleAjax.getAction(), false, this.getSimpleAjaxRefreshInterval(), null);
		}
	}
	
	public boolean isHandlerForSimpleAjaxAction(String action){
		return "generateReport".equals(action);
	}
	
	synchronized public boolean isContinueRefreshChecking(){
		return this.isUserSessionReportJobRunning();
	}
	
	synchronized public void collectRefreshInstruction(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, SimpleAjaxParameter simpleAjax) throws Exception{
		if(this.userSessionReportJob == null || this.userSessionReportJob.disposed()){
			simpleAjax.setRefreshInstruction(SimpleAjaxParameter.REFRESH_INSTRUCTION_STOP_CHECKING, null);
		}else{
			if(! this.isUserSessionReportJobRunning()){
				if(this.userSessionReportJob.isTimedout()){
					simpleAjax.setRefreshInstruction(SimpleAjaxParameter.REFRESH_INSTRUCTION_STOP_CHECKING, "Timeout error while generating report");
				}else if(this.userSessionReportJob.getJobResponse().isSuccess()){
					simpleAjax.setRefreshInstruction(SimpleAjaxParameter.REFRESH_INSTRUCTION_RELOAD_SCREEN, null);
				}else{
					simpleAjax.setRefreshInstruction(SimpleAjaxParameter.REFRESH_INSTRUCTION_STOP_CHECKING, "Error encountered while generating report");
				}
			}else{
				simpleAjax.setRefreshInstruction(SimpleAjaxParameter.REFRESH_INSTRUCTION_CHECK_AGAIN, null);
			}
		}
	}
	
	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if((node.getData() instanceof ReportOutputFile)){
			if(com.idsdatanet.d2.core.web.mvc.Action.DELETE.equals(action)){
				return new DeleteReportActionHandler();
			}else if(com.idsdatanet.d2.core.web.mvc.Action.SAVE.equals(action)){
				return new UpdateReportActionHandler();
			}else{
				return null;
			}
		}else{
			if(CUSTOM_ACTION_SYNCHRONOUS_GENERATE_REPORT.equals(action)){
				return new SynchronousGenerateReport(this);
			}
		}
		
		return null;
	}
	
	protected int getSimpleAjaxRefreshInterval(){
		return -1;
	}
	
	protected void beforeSaveReportEntry(JobContext jobContext, ReportFiles report) throws Exception{
		// do nothing
	}
	
	private ReportJobParams popReportJobData(JobContext job){
		ReportJobParams reportJobData = this.reportJobs.get(job);
		if(reportJobData != null) this.reportJobs.remove(job);
		return reportJobData;
	}
	
	synchronized public void jobEnded(JobContext job) throws Exception{
		ReportJobParams reportJobData = this.popReportJobData(job);
		if(reportJobData == null) return;
		
		if(job.getJobResponse().isSuccess()){
			ReportFiles reportFiles = null;
			//BPAbstractReportModule reportModule = null;
			boolean newFile = false;
			
			List<Object> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportFiles where (isDeleted = false or isDeleted is null) and reportFile = :reportFile", "reportFile", reportJobData.getLocalOutputFile());
			if(rs.size() > 0){
				reportFiles = (ReportFiles) rs.get(0);
			}else{
				reportFiles = new ReportFiles();
				newFile = true;
			}
			
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(job.getUserContext().getUserSelection().getDailyUid());
			if(daily != null){
				reportFiles.setReportDayDate(daily.getDayDate());
			}
			
			ReportDaily reportDaily = this.getRelatedReportDaily(job.getUserContext().getUserSelection().getDailyUid(), this.getDailyType());
			if(reportDaily != null){
				reportFiles.setReportDayNumber(reportDaily.getReportNumber());
			}else{
				reportDaily = ApplicationUtils.getConfiguredInstance().getReportDailyByOperationType(job.getUserContext().getUserSelection().getDailyUid(), job.getUserContext().getUserSelection().getOperationType());
				if (reportDaily != null) 
				{
					reportFiles.setReportDayNumber(reportDaily.getReportNumber());
				}
			}
					
			Operation reportOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(job.getUserContext().getUserSelection().getOperationUid());
			
			String opCo = null;
			if(reportOperation!=null){
				opCo = reportOperation.getOpCo();
			}
			
			String report_type = this.getReportType(job.getUserContext().getUserSelection());
			ReportTypes report_type_record = ReportUtils.getConfiguredInstance().getReportType(job.getUserContext().getUserSelection().getGroupUid(), report_type, reportJobData.getPaperSize(), opCo);
			
			reportFiles.setDateGenerated(new Date());
			reportFiles.setReportUserUid(job.getUserContext().getUserSelection().getUserUid());
			reportFiles.setReportOperationUid(job.getUserContext().getUserSelection().getOperationUid());
			reportFiles.setDailyUid(job.getUserContext().getUserSelection().getDailyUid());
			reportFiles.setReportFile(reportJobData.getLocalOutputFile());
			reportFiles.setReportType(report_type);
			reportFiles.setGroupUid(job.getUserContext().getUserSelection().getGroupUid());
			
			if(newFile){
				if (reportOperation == null || !BooleanUtils.isTrue(reportOperation.getTightHole())) { // tight hole should override GWP
					reportFiles.setIsPrivate(! "1".equals(GroupWidePreference.getValue(job.getUserContext().getUserSelection().getGroupUid(), GroupWidePreference.GWP_NEWLY_GENERATED_REPORT_DEFAULT_PUBLIC)));
				}
			}
			
			this.beforeSaveReportEntry(job, reportFiles);
			
			reportFiles.setDisplayName(this.getReportDisplayNameOnCreation(reportFiles, report_type_record, reportOperation, daily, reportDaily, job.getUserContext().getUserSelection(), reportJobData));
			if (this.reportFilesListener != null)this.reportFilesListener.onReportGenerated(reportFiles, this);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(reportFiles);
			
			ReportJobResult jobResult = reportJobData.getLastReportJobResult();
			if(jobResult != null){
				jobResult.setReportFile(reportFiles);
			}
			
			this._afterReportGenerated(reportFiles, job, reportJobData);
			if (this.reportModuleListener!=null)
				this.reportModuleListener.afterReportGenerated(job.getUserContext().getUserSelection(), reportJobData,this);
			
			this.reportGeneratedFiles = reportFiles;
		}
		
		if(this.continueMultiCopiesJob(reportJobData) == null){
			reportJobData.notifyWaitingThreadOnThisObject();
		}
	}
	
	private void _afterReportGenerated(ReportFiles reportFile, JobContext jobContext, ReportJobParams reportJobData){
		try{
			if(reportJobData.getReportAutoEmail() != null){
				this.sendEmailAfterReportGenerated(reportJobData.getReportAutoEmail(), reportFile, jobContext, reportJobData.getEmailContentMapping());
			}
			
			this.afterReportGenerated(reportFile, jobContext);
		}catch(Exception e){
			this.logger.error(e.getMessage(), e);
		}
	}
	
	protected void sendEmailAfterReportGenerated(ReportAutoEmail autoEmail, ReportFiles reportFile, JobContext jobContext, Map emailContentMapping) throws Exception {
		if(autoEmail == null) return;
		if(! autoEmail.isSendEmailAfterReportGeneration()) return;
		if (autoEmail.isUseEmailList()){
			if(StringUtils.isBlank(autoEmail.getEmailList())){
				this.logger.error("Unable to send mail after generated report [" + reportFile.getReportFile() + "] because email list is empty");
				this.systemMsg = "Unable to send mail after generated report because email list is empty";
				return;
			}
		}else{
			if(StringUtils.isBlank(autoEmail.getEmailListKey()) && !autoEmail.isOpsTeamBased()){
				this.logger.error("Unable to send mail after generated report [" + reportFile.getReportFile() + "] because email list key is empty");
				return;
			}
		}
		
		if(StringUtils.isBlank(autoEmail.getEmailContentTemplateFile())){
			this.logger.error("Unable to send mail after generated report [" + reportFile.getReportFile() + "] because email template file is not defined");
			return;
		}
		if(this.mailEngine == null){
			this.logger.error("Unable to send mail after generated report [" + reportFile.getReportFile() + "] because MailEngine is null");
			this.systemMsg = "Unable to send mail after generated report because MailEngine is null";
			return;
		}
		File outputFile = this.getReportOutputFile(reportFile);
		if(! outputFile.exists()){
			this.logger.error("Unable to send mail after generated report [" + reportFile.getReportFile() + "] because physical report file not found");
			this.systemMsg = "Unable to send mail after generated report because physical report file not found";
			return;
		}
		
		String[] emailList = null;
		if (autoEmail.isUseEmailList()){
			if (autoEmail.getEmailList() != null) { 
				String emailAddress = null;
				emailAddress = autoEmail.getEmailList().toString();
				emailList =emailAddress.split(",");
			}
		}else{
			if (autoEmail.isOpsTeamBased()) {
				List<String> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select e.emailListKey from EmailList e, OpsTeam t, OpsTeamOperation o where e.emailListUid = t.emailListUid and t.opsTeamUid = o.opsTeamUid and o.operationUid = :operationUid and (e.isDeleted = false or e.isDeleted is null) and (t.isDeleted = false or t.isDeleted is null) and (o.isDeleted = false or o.isDeleted is null)", "operationUid", jobContext.getUserContext().getUserSelection().getOperationUid());
				if (results != null && !results.isEmpty()) {
					Set<String> uniqueAddresses = new HashSet<String>();
					for (String emailListKey : results) {
						String[] addresses = ApplicationUtils.getConfiguredInstance().getCachedEmailList(jobContext.getUserContext().getUserSelection().getGroupUid(), emailListKey);
						if (addresses != null) { 
							uniqueAddresses.addAll(Arrays.asList(addresses));
						}
					}
					emailList = uniqueAddresses.toArray(new String[uniqueAddresses.size()]);
				}
			} else {
				emailList = ApplicationUtils.getConfiguredInstance().getCachedEmailList(jobContext.getUserContext().getUserSelection().getGroupUid(), autoEmail.getEmailListKey());
			}
		}
		
		if(emailList != null && emailList.length > 0){
			String email_text = this.mailEngine.generateContentFromTemplate(autoEmail.getEmailContentTemplateFile(), emailContentMapping);
			
			//to form the file name of the attachment 
			String attachmentName = reportFile.getDisplayName();
			if (StringUtils.isBlank(attachmentName)) {
				attachmentName = outputFile.getName();
			}
			else {
				String fileExt = "";
				int lastIndex = reportFile.getReportFile().lastIndexOf(".");
				fileExt = reportFile.getReportFile().substring(lastIndex);
				attachmentName = attachmentName + fileExt;
			}
			
			if("PRISM".equals(reportFile.getReportType())){
				Map<String, InputStreamSource> attachment = new HashMap<String, InputStreamSource>();
				attachment.put(attachmentName, new FileSystemResource(outputFile));	
				
				String[] dailyReport = CommonUtil.getConfiguredInstance().getDailyReportGenerated(jobContext.getUserContext().getUserSelection().getOperationUid(), jobContext.getUserContext().getUserSelection().getDailyUid()) ;
				if (dailyReport != null){
					attachment.put(dailyReport[0], new FileSystemResource(dailyReport[1]));
				}				
				this.mailEngine.sendMail(emailList, ApplicationConfig.getConfiguredInstance().getSupportEmail(), ApplicationConfig.getConfiguredInstance().getSupportEmail(), autoEmail.getEmailSubject(), email_text, attachment);
			}
			else {
				this.mailEngine.sendMail(emailList, ApplicationConfig.getConfiguredInstance().getSupportEmail(), ApplicationConfig.getConfiguredInstance().getSupportEmail(), autoEmail.getEmailSubject(), email_text, attachmentName, new FileSystemResource(outputFile));
			}
			this.systemMsg = "Report has been successfully sent to the recipient(s)";
		}else{
			this.logger.error("Unable to send mail after generated report [" + reportFile.getReportFile() + "] because email list is empty");
			this.systemMsg = "Unable to send mail after generated report [" + reportFile.getReportFile() + "] because email list is empty";
		}
	}
	
	protected void afterReportGenerated(ReportFiles reportFile, JobContext jobContext){
		// do nothing, to be subclassed
	}
	
	/**
	 * don't have to do anything at the moment, error should be logged already
	 */
	synchronized public void jobErrorOccured(JobContext job){
		ReportJobParams reportJobData = this.popReportJobData(job);
		if(reportJobData == null) return;
		
		ReportJobResult result = reportJobData.getLastReportJobResult();
		if(result != null){
			result.setIsError(true);
			result.setErrorMsg(job.getJobResponse().getResponseMessage());
		}
		
		if(reportJobData.isThreadWaiting()){
			reportJobData.notifyWaitingThreadOnThisObject();
		}else{
			reportJobData.dispose();
		}
	}
	
	public void jobStarted(JobContext job) throws Exception{
		// do nothing
	}
	
	synchronized public void jobTimedOut(JobContext job) throws Exception{
		ReportJobParams reportJobData = this.popReportJobData(job);
		if(reportJobData == null) return;
		reportJobData.dispose();
	}
	
	protected UserSelectionSnapshot getUserSelectionForReportSubmission(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		return new UserSelectionSnapshot(session);
	}
	
	synchronized public ReportJobParams submitReportJob(List<UserSelectionSnapshot> list) throws Exception {
		return this.submitReportJob(list, null);
	}
	
	synchronized public ReportJobParams submitReportJob(List<UserSelectionSnapshot> list, ReportJobParams params) throws Exception {
		if(list == null) return null;
		if(list.isEmpty()) return null;
		
		if(params == null) params = new ReportJobParams();
		if (params.isMultiWellJob()){
			params.setMultiWellList(list);
			UserSelectionSnapshot userSelection = list.get(0);
			if(userSelection == null) return null;
			return this._submitReportJob(userSelection, params);
		} else {
			params.setMultiCopiesJob(list);
			return this.continueMultiCopiesJob(params);
		}
	}
	
	synchronized public ReportJobParams submitReportJobWithEmail(List<UserSelectionSnapshot> list, ReportJobParams params) throws Exception {
		if(list == null) return null;
		if(list.isEmpty()) return null;
		
		if(params == null) params = new ReportJobParams();
		if (params.isMultiWellJob()){
			params.setMultiWellList(list);
			UserSelectionSnapshot userSelection = list.get(0);
			if(userSelection == null) return null;
			
			return this._submitReportJob(userSelection, params);
		} else {
			ReportAutoEmail config = this.getEmailConfiguration();
			
			ReportAutoEmail autoEmail = new ReportAutoEmail();
			autoEmail.setEmailList(this.emailAddress);
			autoEmail.setUseEmailList(true);
			if (config!=null && config.getEmailSubject()!=null){
				autoEmail.setEmailSubject(config.getEmailSubject());
			}else{
				autoEmail.setEmailSubject("IDS Report");
			}
			if (config!=null && config.getEmailContentTemplateFile()!=null){
				autoEmail.setEmailContentTemplateFile(config.getEmailContentTemplateFile());
			}else{
				autoEmail.setEmailContentTemplateFile("report/MgtReportEmailBody.ftl");
			}
			if (config!=null && config.getEmailListKey()!=null){
				autoEmail.setEmailListKey(config.getEmailListKey());
			}else{
				autoEmail.setEmailListKey("DefaultEmailList");
			}
			autoEmail.setSendEmailAfterReportGeneration(true);
			autoEmail.setOpsTeamBased(false);
			
			params.setReportAutoEmail(autoEmail);
			params.setMultiCopiesJob(list);
			return this.continueMultiCopiesJob(params);
		}
	}
	
	private ReportJobParams continueMultiCopiesJob(ReportJobParams reportJobData) throws Exception {
		UserSelectionSnapshot userSelection = reportJobData.getNextMultiCopiesJob();
		if(userSelection == null) return null;
		return this._submitReportJob(userSelection, reportJobData);
	}
	
	protected ReportJobParams getParametersForReportSubmission(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		return null;
	}
	
	synchronized protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		return this.submitReportJob(this.getUserSelectionForReportSubmission(session, request, commandBean), this.getParametersForReportSubmission(session, request, commandBean));
	}

	synchronized public ReportJobParams submitReportJob(UserSelectionSnapshot userSelection) throws Exception {
		return this.submitReportJob(userSelection, null);
	}
	
	synchronized public ReportJobParams submitReportJob(UserSelectionSnapshot userSelection, ReportJobParams params) throws Exception {
		if(params == null) params = new ReportJobParams();
		if(params.isUserSessionReportJob() && this.isUserSessionReportJobRunning()) throw new Exception("Current job is still running");
		
		return this._submitReportJob(userSelection, params);
	}

	
	protected void beforeReportJobStart(UserSelectionSnapshot userSelection) throws Exception {
		// do nothing, to be override by subclass
	}
	
	private ReportJobParams _submitReportJob(UserSelectionSnapshot userSelection, ReportJobParams reportJobData) throws Exception {
		SystemReportUtils.setDefaultReportLocale(userSelection, getReportLocaleObject());
		
		UserContext userContext = UserContext.getUserContext(userSelection);
		if(userContext == null) throw new Exception("UserContext must not be null");
		
		reportJobData.resetForNewJob();
		
		reportJobData.setLocalOutputFile(this.getOutputFileInLocalPath(userContext, reportJobData.getPaperSize()));
		String output_file = ReportUtils.getFullOutputFilePath(reportJobData.getLocalOutputFile());
		if(StringUtils.isBlank(output_file)) throw new Exception("Output file must not be null");
		
		String source_xsl = this.getXslFileInLocalPath(userContext, reportJobData);
		if(source_xsl != null){
			source_xsl = ReportUtils.getFullXslFilePath(source_xsl);
			if(StringUtils.isBlank(source_xsl)) throw new Exception("Xsl file must not be null");
		}

		this.beforeReportJobStart(userSelection);
		if(this.reportModuleListener != null) this.reportModuleListener.beforeReportJobStart(userSelection, reportJobData);

		ArrayList<ReportDataGenerator> reportDataGenerators = new ArrayList<ReportDataGenerator>();
		ReportDataGenerator generator = this.getReportDataGenerator();
		if(generator != null) reportDataGenerators.add(generator);
		if(reportJobData.getAdditionalReportDataGenerator() != null) reportDataGenerators.add(reportJobData.getAdditionalReportDataGenerator());
		if(reportDataGenerators.size() == 0) throw new Exception("ReportDataGenerator must be specified");
		
		ReportDataGenerator[] generatorsList = new ReportDataGenerator[reportDataGenerators.size()];
		reportDataGenerators.toArray(generatorsList);
		
		D2Job job = this.createReportJob(source_xsl, output_file, generatorsList, this.getReportType(userContext.getUserSelection()), userContext,reportJobData.getRuntimeContext(),reportJobData);
		
		JobContext currentJob = JobServer.getConfiguredInstance().createJob(job, userContext);
		currentJob.setTimeout(this.reportJobTimeoutMilliSeconds);
		this.beforeReportJobSubmission(currentJob);
		
		currentJob.setJobListener(this);
		
		this.reportJobs.put(currentJob, reportJobData);
		
		if(reportJobData.isUserSessionReportJob()) this.userSessionReportJob = currentJob;

		ReportJobResult jobResult = reportJobData.addReportJobResult();
		jobResult.setReportFilePath(output_file);
		
		JobServer.getConfiguredInstance().submitJob(currentJob);

		return reportJobData;
	}
	
	protected abstract D2Job createReportJob(String xsl, String output, ReportDataGenerator[] generators, String reportType, UserContext userContext, Map runtimeContext, ReportJobParams reportJobData) throws Exception;
	
	protected void beforeReportJobSubmission(JobContext job) throws Exception {
		// subclass can overwrite this method to do customization
	}
	protected abstract boolean getDifferentiateOutputFileByUomTemplate();
	
	protected abstract String getOutputFileExtension() throws Exception ;
	
	protected abstract String getUomTemplateNameForOutputFile(UserSelectionSnapshot userSelection);
	
	protected abstract String getXslFileInLocalPath(UserContext userContext, ReportJobParams reportJobParams) throws Exception;
	
	protected abstract String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception;
	
	protected abstract ReportDataGenerator getReportDataGenerator() throws Exception;
	
	protected abstract String getReportType(UserSelectionSnapshot userSelection) throws Exception;
	
	protected abstract Boolean getDepotSyncPublicReport();
	
	//protected abstract ReportFileListener getReportFilesListener();
	
	protected abstract String getDailyType();
	
	protected Comparator getReportOutputFilesComparator(){
		return new ReportOutputFilesComparator();
	}
	
	protected String generateReportDownloadFilePath(ReportFiles reportFiles) throws Exception {
		
		String path = null;
		if(reportFiles != null) {
			path = reportFiles.getReportFile() + "?handlerId=ddr&reportId=" + reportFiles.getReportFilesUid();
			return path;
		}
		return null;
		
	}
	
	protected abstract String getReportDisplayName(ReportFiles reportFile, Operation operation, UserSelectionSnapshot userSelection) throws Exception;
	
	protected abstract String getReportDisplayNameOnCreation(ReportFiles reportFiles, ReportTypes reportType, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) throws Exception;
	
	private String null2EmptyString(String value){
		if(value == null) return "";
		return value;
	}
	
	protected ReportDaily getRelatedReportDaily(String dailyUid, String dailyType) throws Exception {
		return ApplicationUtils.getConfiguredInstance().getReportDaily(dailyUid, dailyType);
	}
	
	protected ReportOutputFile onLoadReportOutputFile(ReportOutputFile reportOutputFile){
		return reportOutputFile;
	}

	protected abstract List<ReportFiles> loadSourceReportFilesFromDB(UserSelectionSnapshot userSelection, String reportType) throws Exception ;
	
	protected final File getReportOutputFile(ReportFiles dbFile) throws Exception {
		return new File(ReportUtils.getFullOutputFilePath(dbFile.getReportFile()));
	}
	
	public List<Object> loadReportOutputFiles(UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		return this.loadReportOutputFiles(userSelection, this.getReportType(userSelection), request);	
	}
	
	protected String getDailyType(UserSelectionSnapshot userSelection)  throws Exception{
		if (this.getDailyTypeByPriority())
		{
			return CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(userSelection.getOperationUid());
		}else{
			return null;
		}
	}
	
	protected List<ReportDaily> loadReportDaily(UserSelectionSnapshot userSelection) throws Exception {
		String dailyType=this.getDailyType(userSelection);
		return CommonUtil.getConfiguredInstance().getDailyForOperation(userSelection.getOperationUid(), dailyType==null?this.getDailyType():dailyType);
	}
	
	protected List<Object> loadReportOutputFiles(UserSelectionSnapshot userSelection, String reportType, HttpServletRequest request) throws Exception{
		List<ReportFiles> db_files = this.loadSourceReportFilesFromDB(userSelection, reportType);
		List<Object> output_files = new ArrayList<Object>();
		
		List<ReportDaily> reportDailyList = this.loadReportDaily(userSelection);
		
		ApplicationUtils util = ApplicationUtils.getConfiguredInstance();
		
		if(this.showAllDays){
			Map<String, List<ReportFiles>> mapped_db_files = new HashMap<String, List<ReportFiles>>();
			for(ReportFiles rec: db_files){
				List<ReportFiles> daily_list = mapped_db_files.get(rec.getDailyUid());
				if(daily_list == null){
					daily_list = new ArrayList<ReportFiles>();
					mapped_db_files.put(rec.getDailyUid(), daily_list);
				}
				String[] arr = rec.getDailyUid().split(":");
				if (arr.length > 1){
					if(arr[arr.length-1].equals("combined")){
						ReportDaily newReportDaily = (ReportDaily) new ReportDaily();
						newReportDaily.setDailyUid(rec.getDailyUid());
						reportDailyList.add(newReportDaily);
					}
				}
				daily_list.add(rec);
			}
			
			Operation operation = util.getCachedOperation(userSelection.getOperationUid());
			UserSession session = UserSession.getInstance(request);
			
			if (reportDailyList != null) {
				for(ReportDaily reportDaily: reportDailyList){
					if (session.withinAccessScope(reportDaily)) {
						try{
							ReportOutputFile output_file = null;
							List<ReportFiles> daily_list = mapped_db_files.get(reportDaily.getDailyUid());
							if (daily_list != null) {
								File outputFile = null;
								
								int reportCount = 0;
								// check for output files existence
								for (ReportFiles reportFile : daily_list) {
									outputFile = this.getReportOutputFile(reportFile);
									if (outputFile.exists()) {
										Daily daily = util.getCachedDaily(reportFile.getDailyUid());
										
										if(StringUtils.isNotBlank(reportFile.getDailyUid())){
											String[] arr = reportFile.getDailyUid().split(":");
											if (arr.length > 1){
												if(arr[arr.length-1].equals("combined")){
													daily = new Daily();
													daily.setDailyUid(reportFile.getDailyUid());
												}
											}
										}
										
										operation = (reportFile.getReportOperationUid() != null ? util.getCachedOperation(reportFile.getReportOperationUid()) : null);
										String display_name = this.getReportDisplayName(reportFile, operation, userSelection);
										output_file = this.onLoadReportOutputFile(new ReportOutputFile(reportFile, operation, daily, display_name, outputFile));
										output_file.setOperationUid( reportFile.getReportOperationUid() );
										output_file.setReportType( reportType );
										reportCount++;
										//break;
									}
									}
								if ( output_file != null ){
									output_file.setNumberOfReport( reportCount );
								}
							}
							if (output_file == null) output_file = this.onLoadReportOutputFile(ReportOutputFile.getInstanceWithNoActualReport(operation, reportDaily));
							if (output_file != null) output_files.add(output_file);
						}catch(Exception e){
							this.logger.error("Error encountered when creating ReportOutputFile: " + e.getMessage(), e);
						}
					}
				}
			}
		}else{
			for(ReportFiles reportFile: db_files){
				try{
					if(StringUtils.isNotBlank(reportFile.getReportFile())){
						File outputFile = this.getReportOutputFile(reportFile);
						if(outputFile.exists()){
							Daily daily = util.getCachedDaily(reportFile.getDailyUid());
							if(reportFile.getDailyUid()!=null){
								String[] arr = reportFile.getDailyUid().split(":");
								if (arr.length > 1){
									if(arr[arr.length-1].equals("combined")){
										daily = new Daily();
										daily.setDailyUid(reportFile.getDailyUid());
									}
								}
							}
							Operation operation = (reportFile.getReportOperationUid() != null ? util.getCachedOperation(reportFile.getReportOperationUid()) : null);
							String display_name = this.getReportDisplayName(reportFile, operation, userSelection);
							ReportOutputFile output_file = this.onLoadReportOutputFile(new ReportOutputFile(reportFile, operation, daily, display_name, outputFile));
							if(output_file != null) output_files.add(output_file);
						}else{
							this.logger.error("Physical output file not found. [ReportFilesUid: " + reportFile.getReportFilesUid() + ", Output file: " + null2EmptyString(reportFile.getReportFile()) + "]");
						}
					}
				}catch(Exception e){
					this.logger.error("Error encountered when creating ReportOutputFile: " + e.getMessage(), e);
				}				
			}
		}
		
		Comparator comparator = this.getReportOutputFilesComparator();
		if(comparator != null) Collections.sort(output_files, comparator);
		
		return output_files;
	}

	public void destroy(){
		this.mailEngine = null;
		this.logger = null;
		this.reportJobs = null;
		this.userSessionReportJob = null;
		this.reportLocale = null;
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
	}

	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
	}
	
	public void init(CommandBean commandBean){
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	}
	
	public synchronized void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		String emailList=(String)commandBean.getRoot().getDynaAttr().get("emailAddress");
		this.emailAddress = emailList;
		
		if("1".equals(request.getParameter("custom_button_generate_report"))){
			try{
				this.submitReportJob(UserSession.getInstance(request), request, commandBean);
			}catch(Exception e){
				this.logger.error(e.getMessage(), e);
				commandBean.getSystemMessage().addError("Error when creating report: " + e.getMessage());
			}
		}else if("1".equals(request.getParameter("custom_button_generate_and_sent"))){
			try{
				this.submitReportJobWithEmail(UserSession.getInstance(request), request, commandBean);
			}catch(Exception e){
				this.logger.error(e.getMessage(), e);
				commandBean.getSystemMessage().addError("Error when creating report: " + e.getMessage());
			}		
		}else if("1".equals(request.getParameter("custom_button_generate_report_and_download"))){
			try{
				this.generateAndDownload = true;
				this.submitReportJob(UserSession.getInstance(request), request, commandBean);
			}catch(Exception e){
				this.logger.error(e.getMessage(), e);
				commandBean.getSystemMessage().addError("Error when creating report: " + e.getMessage());
			}
		}
	}

	synchronized protected ReportJobParams submitReportJobWithEmail(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		return this.submitReportJobWithEmail(this.getUserSelectionForReportSubmission(session, request, commandBean), this.getParametersForReportSubmission(session, request, commandBean));
	}
	
	synchronized public ReportJobParams submitReportJobWithEmail(UserSelectionSnapshot userSelection, ReportJobParams params) throws Exception {
		if(params == null) params = new ReportJobParams();
		if(params.isUserSessionReportJob() && this.isUserSessionReportJobRunning()) throw new Exception("Current job is still running");
		
		ReportAutoEmail config = this.getEmailConfiguration();
		
		ReportAutoEmail autoEmail = new ReportAutoEmail();
		autoEmail.setEmailList(this.emailAddress);
		autoEmail.setUseEmailList(true);
		if (config!=null && config.getEmailSubject()!=null){
			autoEmail.setEmailSubject(config.getEmailSubject());
		}else{
			autoEmail.setEmailSubject("IDS Report");
		}
		if (config!=null && config.getEmailContentTemplateFile()!=null){
			autoEmail.setEmailContentTemplateFile(config.getEmailContentTemplateFile());
		}else{
			autoEmail.setEmailContentTemplateFile("report/MgtReportEmailBody.ftl");
		}
		if (config!=null && config.getEmailListKey()!=null){
			autoEmail.setEmailListKey(config.getEmailListKey());
		}else{
			autoEmail.setEmailListKey("DefaultEmailList");
		}
		autoEmail.setSendEmailAfterReportGeneration(true);
		autoEmail.setOpsTeamBased(false);
		
		params.setReportAutoEmail(autoEmail);
		
		return this._submitReportJob(userSelection, params);
	}	
	
	public synchronized ReportJobStatus getReportJobStatus(){
		if(this.userSessionReportJob == null || this.userSessionReportJob.disposed()){
			return new ReportJobStatus(ReportJobStatus.STATUS_NO_JOB);
		}else{
			if(! this.isUserSessionReportJobRunning()){
				if(this.userSessionReportJob.isTimedout()){
					return new ReportJobStatus(ReportJobStatus.STATUS_JOB_ERROR, "Timeout error while generating report");
				}else if(this.userSessionReportJob.getJobResponse().isSuccess()){
					return new ReportJobStatus(ReportJobStatus.STATUS_JOB_DONE);
				}else{
					return new ReportJobStatus(ReportJobStatus.STATUS_JOB_ERROR, this.userSessionReportJob.getJobResponse().getResponseMessage());
				}
			}else{
				return new ReportJobStatus(ReportJobStatus.STATUS_JOB_RUNNING);
			}
		}
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if("check_report_status".equals(invocationKey)){
			ReportJobStatus status = this.getReportJobStatus();
			if(status.getStatus() == ReportJobStatus.STATUS_NO_JOB){
				SimpleXmlResponse.send(response, true, "no_job", null);
			}else if(status.getStatus() == ReportJobStatus.STATUS_JOB_DONE){
				if(this.generateAndDownload){
					if(this.reportGeneratedFiles != null) {
						String path = this.generateReportDownloadFilePath(this.reportGeneratedFiles);
						SimpleXmlResponse.send(response, true, "job_done", null, path);
						this.generateAndDownload = false;
						this.reportGeneratedFiles = null;
					} else {
						SimpleXmlResponse.send(response, true, "no_job", null);
					}
				} else {
					SimpleXmlResponse.send(response, true, "job_done", null);
				}
			}else if(status.getStatus() == ReportJobStatus.STATUS_JOB_ERROR){
				SimpleXmlResponse.send(response, true, "job_error", status.getResponseMessage());
			}else if(status.getStatus() == ReportJobStatus.STATUS_JOB_RUNNING){
				SimpleXmlResponse.send(response, true, "job_running", null);
			}else{
				SimpleXmlResponse.send(response, true, "job_unknown", null);
			}
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
	}

	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		//if depotSyncPublicReport is flagged true in the customization
		String isPrivate = null;
		Object object = node.getData();
		
		if (object instanceof ReportOutputFile) {
			ReportOutputFile report = (ReportOutputFile) object;
			String dailyUid = report.getDailyUid();
			String operationUid = session.getCurrentOperationUid();
			String reportFilesUid = report.getReportFilesUid();
			String reportType = "bpmigas";
			//isPrivate = report.getIsPrivate().toString();
			String sql = "select isPrivate from ReportFiles where (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid and reportOperationUid = :operationUid and reportFilesUid = :reportFilesUid " + 
					 	 "and reportType='"+reportType+"' ";
			String[] paramNames = {"dailyUid", "operationUid", "reportFilesUid"};
			Object[] paramValues = {dailyUid, operationUid, reportFilesUid};
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, paramValues);
			
			if (list != null && list.size() > 0) {
				Object value = list.get(0);
				isPrivate = value.toString();
				
				//Boolean test = this.getDepotSyncPublicReport();
				
				/*If depotSyncPublicReport is true and is 'Public', trigger depot */
				if(this.getDepotSyncPublicReport() && isPrivate.equals("false")){
					
					
					/*Trigger the depot job via the depot manager*/
					DepotPremierManager.getConfiguredInstance().postOpsReportEvent(dailyUid);
					
					
				} 
				
			}
		
		}
	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
	}

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
	}
	
	public void beforeDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception{
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
	}

	public void onDataNodeCarryForwardFromYesterday(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception{
	}

	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{
	}
	
	public void onDataNodePasteLastAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{
	}

	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
	}
	
	public void afterDataNodeProcessed(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, String action, int operationPerformed, boolean errorOccurred) throws Exception{
	}

	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession userSession, CommandBeanTreeNode parent) throws Exception {
		// do nothing
	}

	public void setDailyTypeByPriority(boolean dailyTypeByPriority) {
		this.dailyTypeByPriority = dailyTypeByPriority;
	}

	public boolean getDailyTypeByPriority() {
		return dailyTypeByPriority;
	}
	
	public void setEmailConfiguration(ReportAutoEmail emailConfiguration) {
		this.emailConfiguration = emailConfiguration;
	}

	public ReportAutoEmail getEmailConfiguration() {
		return emailConfiguration;
	}
}
