package com.idsdatanet.d2.drillnet.activity;

import java.util.Map;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;

public class OperationCodeDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
	// TODO Auto-generated method stub
		String uri = "xml://operationtype?key=code&amp;value=label";
		Map<String, LookupItem> operationTypeLookup = LookupManager.getConfiguredInstance().getLookup(uri, userContext.getUserSelection(), null);
		
		ReportDataNode thisReportNode = reportDataNode.addChild("OperationCodeList");
		
		ReportDataNode childNode = thisReportNode.addChild("LookupItem");
		//add dummy record for general listing 
		childNode.addProperty("code", "AAAAA");
		childNode.addProperty("label", "General");	
		
		for(Map.Entry<String, LookupItem> item: operationTypeLookup.entrySet()){
			ReportDataNode temp = thisReportNode.addChild("LookupItem");
			temp.addProperty("code", item.getValue().getKey().toString());
			temp.addProperty("label", item.getValue().getValue().toString());	
		}
	}		

}
