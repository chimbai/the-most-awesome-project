package com.idsdatanet.d2.drillnet.drillingparameters;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.DepotWellOperationMapping;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.LogCurveDefinition;
import com.idsdatanet.d2.core.model.SchedulerTemplate;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.core.DepotConstants;
import com.idsdatanet.depot.core.DepotContext;
import com.idsdatanet.depot.core.mapping.DepotMappingModule;
import com.idsdatanet.depot.core.mapping.MappingAggregator;
import com.idsdatanet.depot.core.query.QueryResult;
import com.idsdatanet.depot.core.soap.SoapQuery;
import com.idsdatanet.depot.core.soap.SoapQueryDelegate;
import com.idsdatanet.depot.core.util.DepotUtils;
import com.idsdatanet.depot.witsml.dataobject.log.LogCurveDataSource;
import com.idsdatanet.depot.witsml.dataobject.log.LogDataBean;

public class DrillingParametersCommandBeanListenerForDepot extends EmptyCommandBeanListener implements ApplicationContextAware {
	public class WitsmlLogCurve {
		SchedulerTemplate template;
		LogCurveDefinition logCurveDef = null;
		DepotWellOperationMapping wellMapping = null;
		LogCurveDataSource dataSource = null;
		
		public WitsmlLogCurve(LogCurveDefinition logCurveDef, DepotWellOperationMapping dwom, LogCurveDataSource dataSource, SchedulerTemplate template) throws Exception {
			this.logCurveDef = logCurveDef;
			this.dataSource = dataSource;
			this.dataSource.setLogCurveDefinition(logCurveDef);
			
			this.wellMapping = dwom;
			this.template = template;
		}
		
		public LogCurveDefinition getLogCurveDef() {
			return logCurveDef;
		}
		
		public void setLogCurveDef(LogCurveDefinition logCurveDef) {
			this.logCurveDef = logCurveDef;
		}
		
		public DepotWellOperationMapping getWellMapping() {
			return wellMapping;
		}
		
		public void setWellMapping(DepotWellOperationMapping wellMapping) {
			this.wellMapping = wellMapping;
		}

		public LogCurveDataSource getDataSource() {
			return dataSource;
		}

		public void setDataSource(LogCurveDataSource dataSource) {
			this.dataSource = dataSource;
		}

		public SchedulerTemplate getTemplate() {
			return template;
		}

		public void setTemplate(SchedulerTemplate template) {
			this.template = template;
		}		
	}
	
	private ApplicationContext applicationContext;
	
	public void setApplicationContext(ApplicationContext appContext) {
		this.applicationContext = appContext;
	}
	
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		String witsmlImportKey = request.getParameter("witsmlImportKey");
		if (StringUtils.isNotBlank(witsmlImportKey) && StringUtils.equals(witsmlImportKey, "LogDrillingParameters")) {
			String importParameters = request.getParameter("importParameters");
			if (StringUtils.isNotBlank(importParameters)) {
				String[] args = importParameters.split("[.]");
				String schedulerTemplateUid = args[0];
				String dwomUid = args[1];
				String logCurveUid = args[2];
				
				SchedulerTemplate schedulerTemplate = (SchedulerTemplate)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SchedulerTemplate.class, schedulerTemplateUid);
				DepotWellOperationMapping dwom = (DepotWellOperationMapping)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(DepotWellOperationMapping.class, dwomUid);
				LogCurveDefinition logCurveDef = (LogCurveDefinition)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LogCurveDefinition.class, logCurveUid);
				LogCurveDataSource dataSource = (LogCurveDataSource)this.getApplicationContext().getBean(logCurveDef.getLogSoapQueryId());
				commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
				this.queryWitsmlLogAndPopulateToScreen(new WitsmlLogCurve(logCurveDef, dwom, dataSource, schedulerTemplate), commandBean, UserSession.getInstance(request));
			} else {
				commandBean.getSystemMessage().addError("Import Parameters are not configured.");
			}
		}
	}

	private void queryWitsmlLogAndPopulateToScreen(WitsmlLogCurve witsmlLogCurve, CommandBean commandBean, UserSession session) throws Exception {
		SchedulerTemplate schedulerTemplate = witsmlLogCurve.getTemplate();
		DepotWellOperationMapping dwom = witsmlLogCurve.getWellMapping();
		LogCurveDataSource dataSource = witsmlLogCurve.getDataSource();
		ImportExportServerProperties serverProperties = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, schedulerTemplate.getImportExportServerPropertiesUid());
		MappingAggregator mappingAggregator = DepotMappingModule.getConfiguredInstance().getMappingAggregator(DepotConstants.WITSML, dataSource.getMappingAggregatorId());
		
		QueryResult qResult = new QueryResult();
		qResult.setDepotType(DepotConstants.WITSML);
		qResult.setDepotVersion(mappingAggregator.getStoreVersion());
		qResult.setTransType(DepotConstants.GET_FROM_STORE);
		qResult.setClientId(DepotUtils.SCHEDULER_USER_AGENT);
		qResult.setDepotObjectId(mappingAggregator.getDataObjectId());
		
		DepotContext depotContext = new DepotContext(null, mappingAggregator.getStoreVersion(), mappingAggregator.getDefaultNamespace());
		depotContext.setWellUid(dwom.getDepotWellUid());
		depotContext.setWellboreUid(dwom.getDepotWellboreUid());
		
		if (dataSource instanceof SoapQuery) {
			final List<Object> records = new ArrayList<Object>();
			SoapQuery soapQuery = (SoapQuery)dataSource;
			soapQuery.setJobProperties(serverProperties, mappingAggregator, dwom, session.getCurrentDaily().getDayDate(), depotContext, schedulerTemplate.getSchedulerType(), mappingAggregator.getDataObjectId());
			soapQuery.invoke(qResult, new SoapQueryDelegate() {
				public void beforeDataSave(Object logDataBean) {
					LogDataBean dataBean = (LogDataBean)logDataBean;
					DrillingParameters drillingParameters = (DrillingParameters)dataBean.getData();
					if(drillingParameters.getStartTime() != null)
						records.add(drillingParameters);
				}
				public void afterDataSave(Object drillingParameters) {
					// Do nothing
				}
				public boolean canSave() {
					return false;
				}
			});
			if (qResult.isSuccessful()) {
				if (records.size() > 0) {
					/** link bha with drilling params [ticket:40403]**/
					String bharunUid = null;
					
					String sql ="select b.bharunUid from BharunDailySummary bds, Bharun b " +
									"where bds.dailyUid=:dailyUid " +
									"and (bds.isDeleted is null or bds.isDeleted=0) " +
									"and (b.isDeleted is null or b.isDeleted=0) " +
									"and (b.dailyidOut is null or b.dailyidOut='') " +
									"and b.bharunUid=bds.bharunUid " +
									"order by b.bhaRunNumber DESC" ; //get the highest bharun number, assuming it to be the latest
					List<String> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "dailyUid", session.getCurrentDailyUid());
					if (result != null && result.size() > 0){
						bharunUid = result.get(0);
					}
					
					//if (bharun != null && bharunUid == null) bharunUid = bharun.getBharunUid(); //get the last bharun found if all bharun has been uninstalled
					/** link bha with drilling params **/
					
					for (Object drillParams : records) {
						DrillingParameters dp = (DrillingParameters)drillParams;
						if(bharunUid != null) dp.setBharunUid(bharunUid);/** link bha with drilling params **/
						CommandBeanTreeNodeImpl node = (CommandBeanTreeNodeImpl)commandBean.getRoot().addCustomNewChildNodeForInput(dp);
						node.getAtts().setAction(Action.SAVE);
						node.getAtts().setEditMode(true);
						node.setNewlyCreated(true);
						node.setDirty(true);
						node.getAtts().setFocusFlag(true);
					}
				} else {
					commandBean.getSystemMessage().addWarning("No data available on " + serverProperties.getServiceName());
				}
			} else {
				commandBean.getSystemMessage().addError("Failed to query data on " + serverProperties.getServiceName() + " with errors: " + qResult.getAllMessages());
			}
		} else {
			commandBean.getSystemMessage().addError("Log Curve Data Source is not an instance of SoapQuery.");
		}
	}
}
