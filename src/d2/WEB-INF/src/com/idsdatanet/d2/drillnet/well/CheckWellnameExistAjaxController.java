package com.idsdatanet.d2.drillnet.well;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CheckWellnameExistAjaxController extends AbstractController {
	
	public class Result {
		private Boolean isDuplicate = false;
		public void setDuplicate(Boolean value) {
			this.isDuplicate = value;
		}
		public Boolean isDuplicate() {
			return this.isDuplicate;
		}
	}

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelMap map = new ModelMap();
		Result result = new Result();
		String name = request.getParameter("name");
		String parentWellUid = request.getParameter("parentId");
		String operationType = request.getParameter("operationType");
		boolean isNewRecord = request.getParameter("isNewRecord").equals("1")? true : false;
		UserSession session = UserSession.getInstance(request);
		
		if(CommonUtil.getConfiguredInstance().isValidForCheckingWellNameDuplicate(session, name, parentWellUid, operationType, isNewRecord)) {
			result.setDuplicate(CommonUtil.getConfiguredInstance().isWellNameDuplicate(name, operationType, parentWellUid));
		}
		
		map.addAttribute("result", result);
		return new ModelAndView("drillnet/checkWellnameExist", "command", map);
	}
}
