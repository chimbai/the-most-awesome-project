package com.idsdatanet.d2.drillnet.activity.npt;

public class NPTLessonTicket implements java.io.Serializable {
	
	private String wellUid;
	private String wellName;
	private String operationUid;
	private String operationName;
	private String country;
	private String opCo;
	private String field;
	private String sections;
	private String lessonTitle;
	private String descrEventRisk;
	private String rootCauseDescription;
	private String descrLesson;
	private String eventProbability;
	private String nptSubCategory;
	private String nptMainCategory;
	private String eventImpact;	
	private String incidentLevel;
	private String descrPostevent;
	private String descrContigency;
		
	public void setOpCo(String opCo) {
		this.opCo = opCo;
	}
	public String getOpCo() {
		return opCo;
	}
	
	public NPTLessonTicket(String wellUid, String operationUid, String country,
			String opCo, String lessonTitle, String field) {
		this.wellUid = wellUid;
		this.operationUid = operationUid;
		this.country = country;
		this.opCo = opCo;
		this.lessonTitle = lessonTitle;	
		this.field = field;		
	}
	
	public void setLessonTitle(String lessonTitle) {
		this.lessonTitle = lessonTitle;
	}
	public String getLessonTitle() {
		return lessonTitle;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry() {
		return country;
	}
	
	public void setDescrEventRisk(String descrEventRisk) {
		this.descrEventRisk = descrEventRisk;
	}
	public String getDescrEventRisk() {
		return descrEventRisk;
	}
	
	public void setWellUid(String wellUid) {
		this.wellUid = wellUid;
	}
	public String getWellUid() {
		return wellUid;
	}
	
	public void setOperationUid(String operationUid) {
		this.operationUid = operationUid;
	}
	public String getOperationUid() {
		return operationUid;
	}
	
	public void setRootCauseDescription(String rootCauseDescription) {
		this.rootCauseDescription = rootCauseDescription;
	}
	public String getRootCauseDescription() {
		return rootCauseDescription;
	}
	
	public void setDescrLesson(String descrLesson) {
		this.descrLesson = descrLesson;
	}	
	public String getDescrLesson() {
		return descrLesson;
	}
	
	public void setEventProbability(String eventProbability) {
		this.eventProbability = eventProbability;
	}
	public String getEventProbability() {
		return eventProbability;
	}
	
	public void setEventImpact(String eventImpact) {
		this.eventImpact = eventImpact;
	}
	public String getEventImpact() {
		return eventImpact;
	}
	
	public void setWellName(String wellName) {
		this.wellName = wellName;
	}
	public String getWellName() {
		return wellName;
	}
	
	public void setIncidentLevel(String incidentLevel) {
		this.incidentLevel = incidentLevel;
	}
	public String getIncidentLevel() {
		return incidentLevel;
	}
	
	public void setDescrPostevent(String descrPostevent) {
		this.descrPostevent = descrPostevent;
	}
	public String getDescrPostevent() {
		return descrPostevent;
	}
	
	public void setField(String field) {
		this.field = field;
	}
	public String getField() {
		return field;
	}
	
	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}
	public String getOperationName() {
		return operationName;
	}
	
	public void setDescrContigency(String descrContigency) {
		this.descrContigency = descrContigency;
	}
	public String getDescrContigency() {
		return descrContigency;
	}
	
	public void setSections(String sections) {
		this.sections = sections;
	}
	public String getSections() {
		return sections;
	}
	
	public void setNptSubCategory(String nptSubCategory) {
		this.nptSubCategory = nptSubCategory;
	}
	public String getNptSubCategory() {
		return nptSubCategory;
	}
	
	public void setNptMainCategory(String nptMainCategory) {
		this.nptMainCategory = nptMainCategory;
	}
	public String getNptMainCategory() {
		return nptMainCategory;
	}	
}
