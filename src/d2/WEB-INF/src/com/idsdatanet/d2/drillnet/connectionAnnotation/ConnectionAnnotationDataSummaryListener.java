package com.idsdatanet.d2.drillnet.connectionAnnotation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.query.generators.MSSQLDatabaseManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.EmptyDataSummary;
import com.idsdatanet.d2.core.web.mvc.Summary;
import com.idsdatanet.d2.core.web.mvc.SummaryInfo;
import com.idsdatanet.d2.core.web.mvc.SummaryUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ConnectionAnnotationDataSummaryListener extends EmptyDataSummary {
	private List<Summary> data = null;
	
	public void generate(HttpServletRequest request) throws Exception {
		this.data = new ArrayList<Summary>();
		UserSession session = UserSession.getInstance(request);
		
		String strSql1 = "SELECT d.dailyUid FROM RigStateRaw r, Daily d WHERE DATE(r.startTime)=DATE(d.dayDate) AND r.operationUid=:operationUid AND d.operationUid=r.operationUid " +
						"AND r.internalRigState IN ('22','28','31','36','38','40','41') AND (r.isDeleted=FALSE OR r.isDeleted IS NULL) AND (r.isDeleted=FALSE OR r.isDeleted IS NULL) " +
						"GROUP BY DATE(r.startTime) ORDER BY DATE(r.startTime)";
		//IF MSSQL 
		if (!MSSQLDatabaseManager.getConfiguredInstance().isMySql) {
			strSql1 = "SELECT d.dailyUid FROM RigStateRaw r, Daily d WHERE CONVERT(DATE, r.startTime)=CONVERT(DATE, d.dayDate) AND r.operationUid=:operationUid AND d.operationUid=r.operationUid " +
					"AND r.internalRigState IN ('22','28','31','36','38','40','41') AND (r.isDeleted=FALSE OR r.isDeleted IS NULL) AND (r.isDeleted=FALSE OR r.isDeleted IS NULL) " +
					"GROUP BY d.dailyUid,d.dayDate ORDER BY d.dayDate";
		}
		
		String[] paramsFields1 = {"operationUid"};
		String[] paramsValues1 = {session.getCurrentOperationUid()};
		List<String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);

		if (lstResult.size() > 0){
			List<String> dailyUidList = new ArrayList();
			for (String dailyUid : lstResult) {
				dailyUidList.add(dailyUid);
			}
			
			Map<String, String> summaryMap = SummaryUtils.getConfiguredInstance().getDailyUidAndReportNumberWithReportTypeAware(dailyUidList, session, null);
			
			if (summaryMap != null && summaryMap.size() > 0) {
				for (Map.Entry<String, String> entry : summaryMap.entrySet()) {
					String dailyUid = entry.getKey();
					String reportNumber = entry.getValue();
					
					Summary summary = new Summary();
					summary.addInfo(new SummaryInfo(reportNumber, dailyUid));
					this.data.add(summary);
				}
			}
		}
	}
	
	public List<Summary> getData() {
		return this.data;
	}
	
	public boolean isDefault() {
		return true;
	}
}
