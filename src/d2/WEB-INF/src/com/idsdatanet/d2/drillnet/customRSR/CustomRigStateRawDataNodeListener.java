package com.idsdatanet.d2.drillnet.customRSR;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.KpiThreshold;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.RigStateRaw;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CustomRigStateRawDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if (obj instanceof RigStateRaw)
		{
			RigStateRaw rsr = (RigStateRaw) obj;
			
			DateFormat datetimeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DateFormat durationformat = new SimpleDateFormat("HH:mm:ss");
			Date start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rsr.getStartTime().toString());
			String startTime = datetimeformat.format(start);
			Date end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rsr.getEndTime().toString());
			String endTime = datetimeformat.format(end);
			
			long diff = rsr.getEndTime().getTime() - rsr.getStartTime().getTime();		
			Calendar triggertime = Calendar.getInstance();
        	triggertime.setTimeInMillis(diff);
        	
			node.getDynaAttr().put("startTime", startTime);
			node.getDynaAttr().put("endTime", endTime);
			node.getDynaAttr().put("duration", durationformat.format(triggertime.getTime()));
		}
	
	}
	
	@Override
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		if (meta.getTableClass().equals(RigStateRaw.class)) {
			String opsId = (String) commandBean.getRoot().getDynaAttr().get("opsID");
			if (StringUtils.isBlank(opsId)){
				commandBean.getRoot().getDynaAttr().put("opsID", userSelection.getOperationUid());
			}else {
				if (!opsId.equals(userSelection.getOperationUid())) {
					commandBean.getRoot().getDynaAttr().put("filter", "");
				}
			}
			String filter = (String) commandBean.getRoot().getDynaAttr().get("filter");
			String selecteddaydate = null;
			String selecteddayTime  = null;
			String selectedrigState = null;
			String selectedHide = null;
			if (StringUtils.isNotBlank(filter)){
				if (filter.equals("reset")) {
					commandBean.getRoot().getDynaAttr().put("daydate", null);
					commandBean.getRoot().getDynaAttr().put("dayTime", null);
					commandBean.getRoot().getDynaAttr().put("rigState", null);
					commandBean.getRoot().getDynaAttr().put("hideRec", null);
				}else {
					String[] filterField = filter.split(",");
					selecteddaydate = (String) filterField[0];
					selecteddayTime  = (String) filterField[1];
					selectedrigState = (String) filterField[2];
					selectedHide = (String) filterField[3];
					
					commandBean.getRoot().getDynaAttr().put("daydate", selecteddaydate);
					commandBean.getRoot().getDynaAttr().put("dayTime", selecteddayTime);
					commandBean.getRoot().getDynaAttr().put("rigState", selectedrigState);
					commandBean.getRoot().getDynaAttr().put("hideRec", selectedHide);
				}
			} else {
				commandBean.getRoot().getDynaAttr().put("daydate", null);
				commandBean.getRoot().getDynaAttr().put("dayTime", null);
				commandBean.getRoot().getDynaAttr().put("rigState", null);
				commandBean.getRoot().getDynaAttr().put("hideRec", null);
			}
			
			if ((StringUtils.isNotBlank(selecteddaydate)) && (StringUtils.isNotBlank(selecteddayTime))) {
				// Staring SQL Query
				String strSql = "FROM RigStateRaw WHERE operationUid = :operationUid AND (isDeleted IS NULL OR isDeleted = '')";
				List<String> paramNames= new ArrayList<String>();
				List<Object> paramValues= new ArrayList<Object>();
				paramNames.add("operationUid");
				paramValues.add(userSelection.getOperationUid());
				
				Date startTime = null;
				Date endTime = null;
				Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, selecteddaydate);
				if (daily != null) {
					Date date = daily.getDayDate();
					Calendar cal = Calendar.getInstance();
					cal.setTime(date);
					cal.set(Calendar.HOUR_OF_DAY, this.convertToInt(selecteddayTime));
					startTime = cal.getTime();
					
					cal.set(Calendar.MINUTE, 59);
					cal.set(Calendar.SECOND, 59);
					endTime = cal.getTime();
		 		}
				
				if (startTime!=null) {
					strSql = strSql + " AND startTime between :minDate and :maxDate";
					paramNames.add("minDate");
					paramValues.add(startTime);
					
					paramNames.add("maxDate");
					paramValues.add(endTime);
				}
				
				if(StringUtils.isNotBlank(selectedrigState)) {
					strSql = strSql + " AND internalRigState=:selectedrigState";
					paramNames.add("selectedrigState");
					paramValues.add(selectedrigState);
				}
				
				if(StringUtils.isNotBlank(selectedHide)) {
					if (selectedHide.equals("0")) {
						strSql = strSql + " AND (hideRecord=false or hideRecord is null)";
					}
				}
							
				List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
				List<Object> output_maps = new ArrayList<Object>();
				
				Operation ops = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, userSelection.getOperationUid());
				
				String rigUid = null;
				rigUid = ops.getRigInformationUid();
				
				Map<String, KpiThreshold> kpi = new LinkedHashMap<String, KpiThreshold>();
				String strSql2 = "FROM KpiThreshold WHERE rigInformationUid = :rigInformationUid AND (isDeleted IS NULL OR isDeleted = '')";
				List<KpiThreshold> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "rigInformationUid", rigUid);
				if (list.size() > 0) {
					for (KpiThreshold kpit : list)
					{
						kpi.put(kpit.getInternalRigState(), kpit);
					}
				}
				
				for(Object objResult: items){
					RigStateRaw rsr = (RigStateRaw) objResult;
					if (kpi.containsKey(rsr.getInternalRigState())) {
						if (checkValid(kpi.get(rsr.getInternalRigState()),  rsr)) {
							output_maps.add(rsr);
						}
					}else {
						output_maps.add(rsr);
					}
				}
							
				return output_maps;
			}
		}
		return null;
	}

	@Override
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	public boolean checkValid(KpiThreshold kpi, RigStateRaw rsr) throws Exception{
		long seconds = ((rsr.getEndTime().getTime()-rsr.getStartTime().getTime())/1000);
    	
    	if ((kpi.getBottomDuration() <=seconds) && (kpi.getTopDuration() >=seconds))
    	{
    		return true;
    	}
		return false;
	}
	
	public int convertToInt(String stringInt){
		if (NumberUtils.isNumber(stringInt)) return Integer.parseInt(stringInt);
		return (Integer) null;
	}
}
