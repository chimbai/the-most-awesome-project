package com.idsdatanet.d2.drillnet.casingSection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CasingSectionDataLoaderInterceptor implements DataLoaderInterceptor {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private static final String CUSTOM_FROM_MARKER = "{_custom_from_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		if(StringUtils.isNotBlank(fromClause) && fromClause.indexOf(CUSTOM_FROM_MARKER) < 0) return null;
		
		String currentClass	= meta.getTableClass().getSimpleName();
		String customFrom = "";
		
		if(currentClass.equals("CasingComponent") ) {
			customFrom = "CasingTally";			
			return fromClause.replace(CUSTOM_FROM_MARKER, customFrom);
		}
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, 
			UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, 
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		/*
		String casingSectionUid = null;
		Object object = parentNode.getData();
		if (request != null) {
			if (object instanceof CasingSection)
			{		
				CasingSection CasingSection = (CasingSection) object;
				casingSectionUid = CasingSection.getCasingSectionUid();
			}
		}*/
		
		String currentClass		= meta.getTableClass().getSimpleName();
		String customCondition 	= "";
	
		if(currentClass.equals("CasingComponent") ) {
			customCondition = "(isDeleted = false or isDeleted is null) ";
			///customCondition += "and casingSectionUid= :casingSectionUid ";
			customCondition += "and isCasingComponent= true";
			//query.addParam("casingSectionUid", casingSectionUid);
			
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		return null;
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

}
