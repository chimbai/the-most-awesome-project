package com.idsdatanet.d2.drillnet.marineProperties;

import java.util.List;

import com.idsdatanet.d2.core.model.MarineProperties;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class MarinePropertiesUtil {
	
	public static void calculateAllVariableDeckLoadMarginFromRigInformationSaveOrUpdate(CommandBean commandBean, UserSession session, RigInformation rigInformation) throws Exception {
		String queryString = "FROM MarineProperties WHERE (isDeleted=false or isDeleted is null) and rigInformationUid=:rigInformationUid";
		List<MarineProperties> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "rigInformationUid", rigInformation.getRigInformationUid());
		for (MarineProperties marineProperties : list) {
			MarinePropertiesUtil.calculateVariableDeckLoadMargin(commandBean, session, rigInformation, marineProperties);
		}
	}
	

	public static void calculateVariableDeckLoadMargin(CommandBean commandBean, UserSession session, RigInformation rigInformation, MarineProperties marineProperties) throws Exception {
		if (marineProperties==null) return;
		
		// calculate Variable Deck Load Margin = Max Variable Deck Load Current Variable Deck Load
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, MarineProperties.class, "variableDeckLoad");
		
		Double variableDeckLoad = null;
		if (marineProperties.getVariableDeckLoad()!=null) {
			thisConverter.setBaseValueFromUserValue(marineProperties.getVariableDeckLoad());
			variableDeckLoad = thisConverter.getBasevalue();
		}
		
		Double deckloadmax = null;
		if (rigInformation!=null && rigInformation.getDeckloadmax()!=null) {
			thisConverter.setReferenceMappingField(RigInformation.class, "deckloadmax");
			thisConverter.setBaseValueFromUserValue(rigInformation.getDeckloadmax());
			deckloadmax = thisConverter.getBasevalue();
			
			thisConverter.setReferenceMappingField(MarineProperties.class, "variableDeckLoadMax");
			thisConverter.setBaseValue(deckloadmax);
			marineProperties.setVariableDeckLoadMax(thisConverter.getConvertedValue());
		} else {
			marineProperties.setVariableDeckLoadMax(null);
		}
		
		if (variableDeckLoad!=null && deckloadmax!=null) {
			thisConverter.setReferenceMappingField(MarineProperties.class, "variableDeckLoadMargin");
			Double variableDeckLoadMargin = deckloadmax - variableDeckLoad;
			thisConverter.setBaseValue(variableDeckLoadMargin);
			marineProperties.setVariableDeckLoadMargin(thisConverter.getConvertedValue());
		} else {
			marineProperties.setVariableDeckLoadMargin(null);
		}
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(marineProperties);

	}
}
