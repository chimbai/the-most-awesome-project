package com.idsdatanet.d2.drillnet.activity;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

import java.util.Date;
import java.util.List;


public class WellBasedActivityDurationByClassCodeToDateReportDataGenerator implements ReportDataGenerator {
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		// get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String currentWellUid = userContext.getUserSelection().getWellUid();
		String strOperationList = "";
		String strBit = "";
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		
		ReportDataNode thisReportNode = reportDataNode.addChild("code");
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");	
		
		if(daily!=null && StringUtils.isNotBlank(currentOperationUid)){
			Double dailyNptCost = 0.00;
			Double dailyNptDuration = getActivityDuration(currentOperationUid,daily.getDailyUid(),thisConverter,true);
			Double dailyTotalDuration = getActivityDuration(currentOperationUid,daily.getDailyUid(),thisConverter,false);
			Double dailyCost = getDailyCost(daily.getDailyUid(), thisConverter);
			if (dailyTotalDuration>0) dailyNptCost = dailyNptDuration/dailyTotalDuration*dailyCost;
			
			thisReportNode.addProperty("DailyNptDuration", dailyNptDuration.toString());
			thisReportNode.addProperty("DailyNPTCost", dailyNptCost.toString());
		}
		
		if(StringUtils.isNotBlank(currentWellUid)) {
			List <Operation> lstOperation = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Operation WHERE (isDeleted = false or isDeleted is null) and wellUid=:wellUid", "wellUid", currentWellUid);
			if(lstOperation!=null && lstOperation.size() > 0){
				strBit = "";
				for(Operation objOperation: lstOperation){
					strOperationList = strOperationList + strBit + "'" + objOperation.getOperationUid() + "'";
					strBit = ",";
				}
			}
			
			if(StringUtils.isNotBlank(strOperationList)) {
				String strSql = "Select dailyUid, operationUid FROM Daily WHERE (isDeleted = false or isDeleted is null) AND operationUid IN (" + strOperationList + ") and dayDate <= :userDate ";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "userDate", todayDate);
				
				Double cumDailyNptDuration = 0.00;
				Double cumDailyNPTCost = 0.00;
				
				if(lstResult!=null && lstResult.size()>0){
					for(Object objResult: lstResult){
						Object[] obj = (Object[]) objResult;
						
						if (obj!=null && obj[0] != null && obj[1] != null) {
							Double dailyNptCost = 0.00;
							Double dailyNptDuration = getActivityDuration(obj[1].toString(),obj[0].toString(),thisConverter,true);
							Double dailyTotalDuration = getActivityDuration(obj[1].toString(),obj[0].toString(),thisConverter,false);
							Double dailyCost = getDailyCost(obj[0].toString(), thisConverter);
							if (dailyTotalDuration>0) dailyNptCost = dailyNptDuration/dailyTotalDuration*dailyCost;
							
							cumDailyNptDuration = cumDailyNptDuration + dailyNptDuration;
							cumDailyNPTCost = cumDailyNPTCost + dailyNptCost;		
						}										
					}					
				}		
				thisReportNode.addProperty("CumDailyNptDuration", cumDailyNptDuration.toString());
				thisReportNode.addProperty("CumDailyNPTCost", cumDailyNPTCost.toString());
			}
		}
	}
	
	
	public Double getActivityDuration(String currentOperationUid, String dailyUid, CustomFieldUom thisConverter, Boolean classCodeTpTu) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double totalDuration = 0.00;
		String classCode = "";
		if(classCodeTpTu){
			classCode = "AND (a.internalClassCode='TP' or a.internalClassCode = 'TU')";
		}
		
		String strSql = "SELECT SUM(a.activityDuration) as totalDuration FROM Activity a " +
				"WHERE (a.isDeleted = false or a.isDeleted is null) " +
				classCode +	" AND a.dailyUid = :dailyUid AND a.operationUid=:operationUid AND (a.dayPlus <> 1 AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " ;
		String[] paramsFields = {"dailyUid" ,"operationUid"};
		Object[] paramsValues = {dailyUid, currentOperationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult!=null && lstResult.size() > 0){			
			Object thisResult = (Object) lstResult.get(0);	
			
			if (thisResult != null) {
				totalDuration = Double.parseDouble(thisResult.toString());
				thisConverter.setReferenceMappingField(Activity.class, "activity_duration");
				thisConverter.setBaseValue(totalDuration);
				totalDuration = thisConverter.getConvertedValue();						
			}				
		}
		
		return totalDuration;
	}
	
	public Double getDailyCost(String currentDailyUid, CustomFieldUom thisConverter) throws Exception{	
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND dailyUid = :thisDailyUid";
		List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisDailyUid", currentDailyUid, qp);
		
		Double totalCost = 0.00;
		if(reportDailyList!=null && reportDailyList.size()>0){
			Double daytangiblecost =  0.00;
			Double daycost =  0.00;
			Double otherCost =  0.00;
			if(reportDailyList.get(0).getDaytangiblecost()!=null) daytangiblecost = reportDailyList.get(0).getDaytangiblecost();
			if(reportDailyList.get(0).getOtherCost()!=null) otherCost = reportDailyList.get(0).getOtherCost();
			if(reportDailyList.get(0).getDaycost()!=null) daycost = reportDailyList.get(0).getDaycost();
			totalCost = daytangiblecost + daycost + otherCost ;
			thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
			thisConverter.setBaseValue(totalCost);
			totalCost = thisConverter.getConvertedValue();	
		}

		return totalCost;
	}
}