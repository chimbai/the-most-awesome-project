package com.idsdatanet.d2.drillnet.report;

import java.io.File;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.web.mvc.view.FreemarkerUtils;

public class ReportOutputFile {
	private String reportFilesUid = null;
	private Date dayDate = null;
	private String reportDayNumber = null;
	private String reportFilePath = null;
	private String reportDisplayName = null;
	private String operationName = null;
	private Date timeReportFileCreated = null;
	private String reportUserUid = null;
	private boolean associated_daily_available = false;
	private Boolean is_private = null;
	private String dailyUid = null;
	private String fileExtension = null;
	private boolean withoutReportFile = false;
	private List<ReportFiles> multipleRecords = null;
	private String extraProperties = null;
	private String opsTeamUid = null;
	private int numberOfReport = 0;
	private String reportType = null;
	private String operationUid = null;
	private String qcFlag = null;
	
	public ReportOutputFile(ReportFiles reportFile, Operation operation, Daily daily, String displayName, File outputFile) throws Exception {
		if(reportFile != null){
			this.reportFilesUid = reportFile.getReportFilesUid();
			this.reportFilePath = reportFile.getReportFile();
			this.reportUserUid = reportFile.getReportUserUid();
			this.dayDate = reportFile.getReportDayDate();
			this.reportDayNumber = reportFile.getReportDayNumber();
			this.is_private = (reportFile.getIsPrivate() == null ? true : reportFile.getIsPrivate());
			this.fileExtension = this.getFileExtension(reportFile.getReportFile());
			this.extraProperties = reportFile.getExtraProperties();
			this.opsTeamUid = reportFile.getOpsTeamUid();
		}
		
		this.reportDisplayName = displayName;
		
		if(outputFile != null){
			this.timeReportFileCreated = new Date(outputFile.lastModified());
		}
		
		if(operation != null) this.operationName = operation.getOperationName();
		
		if (daily != null) {
			this.associated_daily_available = true;
			this.dailyUid = daily.getDailyUid();
			if(operation != null) {
				this.qcFlag = ReportUtils.getReportDailyQcFlag(operation.getOperationUid(), daily.getDailyUid());
			}
		}
	}
	
	public static ReportOutputFile getInstanceWithNoActualReport(Operation operation, ReportDaily reportDaily) throws Exception{
		ReportOutputFile me = new ReportOutputFile(null, operation, null, null, null);
		
		me.withoutReportFile = true;
		me.associated_daily_available = true;
		me.dailyUid = reportDaily.getDailyUid();
		me.reportDayNumber = reportDaily.getReportNumber();
		me.dayDate = reportDaily.getReportDatetime();
		
		return me;
	}
	
	public static ReportOutputFile getInstanceWithMultipleCopies(Operation operation, ReportDaily reportDaily, List<ReportFiles> dbRecords) throws Exception {
		ReportOutputFile me = new ReportOutputFile(null, operation, null, null, null);

		me.associated_daily_available = true;
		me.dailyUid = reportDaily.getDailyUid();
		me.reportDayNumber = reportDaily.getReportNumber();
		me.dayDate = reportDaily.getReportDatetime();
		me.multipleRecords = dbRecords;
		
		return me;
	}
	
	private String getFileExtension(String file){
		if(file == null) return null;
		int i = file.lastIndexOf(".");
		if(i != -1){
			return file.substring(i + 1);
		}else{
			return null;
		}
	}
	
	public boolean isAssociatedDailyAvailable(){
		return this.associated_daily_available;
	}
	
	public String getReportFilesUid(){
		return this.reportFilesUid;
	}
	
	public String getOperationName(){
		return this.operationName;
	}
	
	public String getFileExtension(){
		return this.fileExtension;
	}
	
	public String getReportFilePath(){
		return this.reportFilePath;
	}
	
	public Date getDayDate(){
		return this.dayDate;
	}
	
	public String getReportNumber() {
		return this.reportDayNumber;
	}
	
	public String getReportDisplayName(){
		return this.reportDisplayName;
	}
	
	public Date getTimeReportFileCreated(){
		return this.timeReportFileCreated;
	}
	
	public String getTimeReportFileCreatedText(){
		if(this.timeReportFileCreated == null) return null;
		return FreemarkerUtils.formatDurationSince(this.timeReportFileCreated);
	}
	
	public boolean getNewlyGeneratedFlag(){
		if(this.timeReportFileCreated == null) return false;
		return FreemarkerUtils.isDocumentNewlyGenerated(this.timeReportFileCreated);
	}
	
	public String getReportUserUid(){
		return this.reportUserUid;
	}
	
	public Boolean getIsPrivate(){
		return this.is_private;
	}
	
	public void setIsPrivate(Boolean value){
		this.is_private = value;
	}

	public String getDailyUid() {
		return dailyUid;
	}
	
	public boolean isWithoutActualReport(){
		return this.withoutReportFile;
	}
	
	public List<ReportFiles> getMultipleCopies(){
		return this.multipleRecords;
	}

	public String getExtraProperties() {
		return extraProperties;
	}

	public String getOpsTeamUid() {
		return opsTeamUid;
	}
	
	public void setNumberOfReport( int value ){
		this.numberOfReport = value;
	}
	
	public int getNumberOfReport( ){
		return this.numberOfReport;
	}
	
	public void setReportType( String type ){
		this.reportType = type;
	}
	
	public String getReportType( ){
		return this.reportType;
	}
	
	public void setOperationUid( String operationUid ){
		this.operationUid = operationUid;
	}
	
	public String getOperationUid( ){
		return this.operationUid;
	}
	public void setQcFlag (String qcFlag){
		this.qcFlag = qcFlag;
	}
	
	public String getQcFlag (){
		return this.qcFlag;
	}
}
