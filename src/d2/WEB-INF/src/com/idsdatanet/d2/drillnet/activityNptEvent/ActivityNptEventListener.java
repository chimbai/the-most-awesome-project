package com.idsdatanet.d2.drillnet.activityNptEvent;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityNptEventListener  implements DataLoaderInterceptor {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	@SuppressWarnings("unchecked")
	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String currentClass		= meta.getTableClass().getSimpleName();
		String customCondition 	= "";
		
		if(currentClass.equals("Activity")) {
			if (StringUtils.isNotBlank(userSelection.getOperationUid())) {
				List<String> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select dailyUid from Daily where (isDeleted = false or isDeleted is null) and operationUid = :operationUid ", "operationUid", userSelection.getOperationUid());
				if (rs != null && !rs.isEmpty()) {
					String delimiter = "";
					for (int i = 0; i < rs.size(); i++) {
						customCondition += delimiter;
						customCondition += "dailyUid = :dailyUid" + i;
						delimiter = " or ";
						query.addParam("dailyUid" + i, rs.get(i));
						customCondition += " and (dayPlus IS NULL OR dayPlus='0') ";
					}
				}
			}
			if (StringUtils.isBlank(customCondition)) {
				customCondition = "1 = 2";
			}
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		return null;
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean arg0, UserSelectionSnapshot arg1, HttpServletRequest arg2, TreeModelDataDefinitionMeta arg3, DataDefinitionHQLQuery arg4, CommandBeanTreeNode arg5) throws Exception {
		return null;
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		String currentClass	= meta.getTableClass().getSimpleName();
		if(!currentClass.equals("UnwantedEvent")) return null;
		orderByClause = "eventRef + 0 ASC, eventRef";
		return orderByClause;
	}
	
}
