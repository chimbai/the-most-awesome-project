package com.idsdatanet.d2.drillnet.surveyreference;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.*;
import com.idsdatanet.d2.core.model.*;

/**
 * 
 * ***********************************************************
 * THIS CLASS IS OLD NPD CODE AND IS NO LONGER USED - jman
 * ***********************************************************
 * 
 */
public class NPDReportListener implements DataLoaderInterceptor {
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return query;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(meta.getTableClass().equals(SurveyStation.class)){
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null){
				return conditionClause.replace("{_daily_filter_}", "");
			}
			
			/*Calendar calendar = Calendar.getInstance();
			calendar.setTime(daily.getDayDate());
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			
			query.addParam("daily_d1", calendar.getTime());

			calendar.add(Calendar.DAY_OF_MONTH, 1);
			query.addParam("daily_d2", calendar.getTime());*/

			query.addParam("thisDailyUid", daily.getDailyUid());
			//return conditionClause.replace("{_daily_filter_}", "readingDatetime >= :daily_d1 and readingDatetime < :daily_d2");
			return conditionClause.replace("{_daily_filter_}", "dailyUid = :thisDailyUid");
		}else{
			return null;
		}
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
}
