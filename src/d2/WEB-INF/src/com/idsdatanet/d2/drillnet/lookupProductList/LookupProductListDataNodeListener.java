package com.idsdatanet.d2.drillnet.lookupProductList;


import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.LookupProductList;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LookupProductListDataNodeListener extends EmptyDataNodeListener{
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof LookupProductList) {

			LookupProductList lookupProductList = (LookupProductList) obj;
			
			String strVendorUid = (String) commandBean.getRoot().getDynaAttr().get("vendorCompany");
			String strRegion = (String) commandBean.getRoot().getDynaAttr().get("wellRegion");
			
			if (StringUtils.isNotBlank(strVendorUid)) {
				lookupProductList.setMudVendorCompanyUid(strVendorUid);
			}
			
			if (StringUtils.isNotBlank(strRegion)) {
				lookupProductList.setRegion(strRegion);
			}

			
			if(StringUtils.isNotBlank(lookupProductList.getMudVendorCompanyUid()) ){
				Map<String,LookupItem> vendorList =  commandBean.getLookupMap(node, LookupProductList.class.getSimpleName(), "mudVendorCompanyUid");
				LookupItem item = vendorList.get(lookupProductList.getMudVendorCompanyUid());
				if (item==null)
				{			
					System.out.print(item);
					status.setContinueProcess(false, true);
					status.setFieldError(node, "mudVendorCompanyUid", "This Vendor Name does not exist. Please re-select.");
					
				}
			}			
		}	
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
			Object obj = node.getData();
		if (obj instanceof LookupProductList) {

			LookupProductList lookupProductList = (LookupProductList) obj;
			
			String strVendorUid = (String) commandBean.getRoot().getDynaAttr().get("vendorCompany");
			String strRegion = (String) commandBean.getRoot().getDynaAttr().get("wellRegion");
			
			if (StringUtils.isNotBlank(strVendorUid)) {
				lookupProductList.setMudVendorCompanyUid(strVendorUid);
			}
			
			if (StringUtils.isNotBlank(strRegion)) {
				lookupProductList.setRegion(strRegion);
			}

		}
	}
}
