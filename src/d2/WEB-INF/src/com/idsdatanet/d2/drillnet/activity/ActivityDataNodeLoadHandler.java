package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityDataNodeLoadHandler implements DataNodeLoadHandler {

	private boolean isNonSimop = false;
	private boolean isNonOffline = false;
	private boolean isSimop = false;
	private boolean isOffline = false;
	private boolean isAux = false;
	
	public void setIsNonSimop(Boolean value) {
		this.isNonSimop = value;
	}
	
	public void setIsNonOffline(Boolean value) {
		this.isNonOffline = value;
	}
	
	public void setIsSimop(Boolean value) {
		this.isSimop = value;
	}
	
	public void setIsOffline(Boolean value) {
		this.isOffline = value;
	}
	
	public void setIsAux(Boolean value) {
		this.isAux = value;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		if ("Activity".equals(meta.getTableClass().getSimpleName())) {
			return true;
		}
		if ("NextDayActivity".equals(meta.getTableClass().getSimpleName())) {
			return true;
		}
		return false;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		
		if ("Activity".equals(meta.getTableClass().getSimpleName())) {
			List<Object> result = new ArrayList<Object>();
			QueryProperties qp = new QueryProperties();
			
			List<String> param_names = new ArrayList<String>();
			List<Object> param_values = new ArrayList<Object>();
			param_names.add("dailyUid");
			param_values.add(userSelection.getDailyUid());
			
			Date previousEnd = null;
			int count=1;
			boolean dayPlus = false;
			
			if (request!=null){
				dayPlus=true;
			}else{
				dayPlus=false;
			}
			
			String queryString = "FROM Activity a, Daily d " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					(dayPlus ? "AND (a.dayPlus IS NULL OR a.dayPlus = '0') ":"")+
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is NULL) " +
					(this.isNonOffline ? "AND (a.isOffline=false or a.isOffline is null) ":"" )+
					(this.isNonSimop ? "AND (a.isSimop=false or a.isSimop is null) ":"") +
					(this.isSimop ? "AND a.isSimop = '1' " :"") +
					(this.isOffline ? "AND a.isOffline = '1' " :"") +
					(this.isAux ? "AND a.derrickType = 'aux' ":"AND (a.derrickType is null or a.derrickType='') ") +
					"AND a.dailyUid=d.dailyUid " +
					"AND d.dailyUid=:dailyUid " +
					"ORDER BY a.startDatetime";
			List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, param_names, param_values, qp);
			for (Object[] rec : lstResult) {
				
				Activity act = (Activity) rec[0];
				
				if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(),"populateMissingActivityGaps"))){
					
					if (count==1){
						previousEnd = act.getEndDatetime();
					}else{
						if (previousEnd.getTime()!=act.getStartDatetime().getTime()){
							Activity newAct = new Activity();
							newAct.setActivityUid(act.getDailyUid()+"_new" + count);
							newAct.setStartDatetime(previousEnd);
							newAct.setEndDatetime(act.getStartDatetime());
							newAct.setObject("true");
							
							Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(newAct.getStartDatetime(), newAct.getEndDatetime());			
							CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");			
							thisConverter.setBaseValue(thisDuration);		
							newAct.setActivityDuration(thisConverter.getConvertedValue());
							
							result.add(newAct);
							previousEnd = act.getEndDatetime();
						}else{
							previousEnd = act.getEndDatetime();
						}
					}
				}
					
				result.add(rec[0]);
				count++;
			}
			
			return result;
		}else if ("NextDayActivity".equals(meta.getTableClass().getSimpleName())) {
			List<Object> result = new ArrayList<Object>();
			QueryProperties qp = new QueryProperties();
			
			List<String> param_names = new ArrayList<String>();
			List<Object> param_values = new ArrayList<Object>();
			param_names.add("dailyUid");
			param_values.add(userSelection.getDailyUid());
			
			Date previousEnd = null;
			int count=1;
			
			String queryString = "FROM NextDayActivity a, Daily d " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (a.dayPlus = '1') " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is NULL) " +
					(this.isNonOffline ? "AND (a.isOffline=false or a.isOffline is null) ":"" )+
					(this.isNonSimop ? "AND (a.isSimop=false or a.isSimop is null) ":"") +
					(this.isSimop ? "AND a.isSimop = '1' " :"") +
					(this.isOffline ? "AND a.isOffline = '1' " :"") +
					(this.isAux ? "AND a.derrickType = 'aux' ":"AND (a.derrickType is null or a.derrickType='') ") +
					"AND a.dailyUid=d.dailyUid " +
					"AND d.dailyUid=:dailyUid " +
					"ORDER BY a.startDatetime";
			List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, param_names, param_values, qp);
			for (Object[] rec : lstResult) {
				
				NextDayActivity act = (NextDayActivity) rec[0];
				
				if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(),"populateMissingActivityGaps"))){
					
					if (count==1){
						previousEnd = act.getEndDatetime();
					}else{
						if (previousEnd.getTime()!=act.getStartDatetime().getTime()){
							NextDayActivity newAct = new NextDayActivity();
							newAct.setActivityUid(act.getDailyUid()+"_newN" + count);
							newAct.setStartDatetime(previousEnd);
							newAct.setEndDatetime(act.getStartDatetime());
							newAct.setObject("true");
							newAct.setDayPlus(1);
							
							Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(newAct.getStartDatetime(), newAct.getEndDatetime());			
							CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");			
							thisConverter.setBaseValue(thisDuration);		
							newAct.setActivityDuration(thisConverter.getConvertedValue());
							
							result.add(newAct);
							previousEnd = act.getEndDatetime();
						}else{
							previousEnd = act.getEndDatetime();
						}
					}
				}
			
				result.add(rec[0]);
				count++;
			}
			
			return result;
		}
		return null;
	}

}
