package com.idsdatanet.d2.drillnet.customRSR;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CustomRigStateRawDataNodeAllowedAction implements DataNodeAllowedAction {

	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if (obj instanceof RigInformation)
		{
			if (StringUtils.equals(action, "delete")) return false;
			if (StringUtils.equals(action, "edit")) return false;
		}
		return true;
	}
}
