package com.idsdatanet.d2.drillnet.wellExternalCatalog;

import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindException;

import com.idsdatanet.d2.core.web.mvc.AbstractGenericWebServiceCommandBean;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseFormController;

import net.sf.json.JSONObject;

/**
 * This class gets loaded and used by the WellExternalCatalog screen (wellExternalCatalog.js).
 * Handles $.ajax calls to query specific things such as the server name in the Import As New popup,
 * or fetching data to show on the View Raw Data popup.
 * 16 Aug 2022
 */
public class WellExternalCatalogWebserviceCommandBean extends AbstractGenericWebServiceCommandBean { 
	
	public void init(BaseFormController controller){
		controller.setExcludesAclCheck(true);
	}
	
	/**
	 * This will be called upon the $.ajax request from wellExternalCatalog.js whenever the "Import as New" or "View Raw Data" buttons' "onclick" callback fires. 
	 * (under ViewRawDataButtonField.prototype.getFieldRenderer and ImportActualButtonField.prototype.showPopup),
	 * This either fetches the ImportExportServerProperties serviceName for "Import as New" dialog for show in the "Import All From X Server" button,
	 * or fetches the data to show on the "View Raw Data" dialog (which includes the WellExternalCatalog.sourceData).
	 */
	public void process(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		String hql = null;
		ArrayList<String> params = new ArrayList<String>();
		ArrayList<Object> values = new ArrayList<Object>();
		// for "View Raw Data"
		if(request.getParameter("method").equals("viewRaw"))
			this.viewRawData(hql, params, values, request, response);
	}

	/**
	 * For "View Raw Data" action. Fetches the sourceData for display on the popup.
	 * @param hql
	 * @param params
	 * @param values
	 * @param request
	 * @param response
	 * @throws Exception 
	 */
	private void viewRawData(String hql, ArrayList<String> params, ArrayList<Object> values, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		hql = "SELECT sourceData FROM WellExternalCatalog WHERE (isDeleted is null or isDeleted=0) "
				+ "AND wellExternalCatalogUid=:wecUid";
		params.add("wecUid");
		values.add(request.getParameter("wecUid"));
		// fetch
		List<?> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, params, values);
		if(result == null || result.isEmpty()) return;
		// organize the data properly for wellExternalCatalog.js to display
		String sourceData = (String) result.get(0);
		JSONObject responseToSendBack = new JSONObject();
		responseToSendBack.put("success", true); // assume true
		responseToSendBack.put("sourceData", sourceData);
		// change/add the values if sourceData is empty
		if(StringUtils.isBlank(sourceData)) {
			responseToSendBack.put("errorMsg","No source data found for this External Well Catalog!");
			responseToSendBack.put("success", false);
		}
		
		Writer writer = response.getWriter();
		writer.write(responseToSendBack.toString());
		writer.close();
	}
}