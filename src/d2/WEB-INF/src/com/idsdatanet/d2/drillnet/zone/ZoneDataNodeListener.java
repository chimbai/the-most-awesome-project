package com.idsdatanet.d2.drillnet.zone;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.infra.scheduler.ConfigurableScheduler;
import com.idsdatanet.d2.core.model.Zone;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.DefaultDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ZoneDataNodeListener extends DefaultDataNodeListener{
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if (obj instanceof Zone)
		{
			Zone zone = (Zone) obj;
			if (zone.getHasDaylightSaving()!=null && zone.getHasDaylightSaving())
			{
				if (zone.getDaylightSavingStartTime() == null || zone.getDaylightSavingEndTime() == null || zone.getAdjustedMinutes() == null)
				{
					status.setContinueProcess(false, true);
					status.setFieldError(node, "hasDaylightSaving", "Start & End of Day Month and Minutes need to be set if Zone has Daylight Saving");
				}
			}else{
				zone.setDaylightSavingStartTime(null);
				zone.setDaylightSavingEndTime(null);
				zone.setAdjustedMinutes(null);
				
			}
		}
		super.beforeDataNodeSaveOrUpdate(commandBean, node, session, status, request);
	}
	
	private Boolean validateDateFormat(String value) 
	{
		try{
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
			Date d = df.parse(value + " " + calendar.get(Calendar.YEAR));
			System.out.println(d);
		}catch(Exception ex)
		{
			return false;
		}
		return true;
	}
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		ConfigurableScheduler.getConfiguredInstance().rescheduleAllJobs();
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		ConfigurableScheduler.getConfiguredInstance().rescheduleAllJobs();
	}
}
