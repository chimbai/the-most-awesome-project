package com.idsdatanet.d2.drillnet.mudVolume;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.MudVolumes;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class MudVolumesCmpltCarryOverDataNodeListener implements DataNodeLoadHandler{

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		String currentClass = meta.getTableClass().getSimpleName();
		if ("MudVolumes".equals(currentClass)) {
			return true;
		}
		return false;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		String currentClass = meta.getTableClass().getSimpleName();
		if ("MudVolumes".equals(currentClass)) {
			String queryString = "FROM MudVolumes WHERE (isDeleted=false or isDeleted is null) and dailyUid=:dailyUid ";

			List result = new ArrayList();
			List<MudVolumes> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", userSelection.getDailyUid());
			for (MudVolumes rec : list) {
				rec.setPumpedVolume(0.0);
				rec.setVolumeLosses(0.0);
				result.add(rec);
			}
			return result;
		}		
		return null;
	}

}
