package com.idsdatanet.d2.drillnet.hseIncident;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class DaysSinceLastHseIncidentDataGenerator implements ReportDataGenerator {
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
			// TODO Auto-generated method stub
		
				//get the current date from the user selection
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
				if (daily == null) return;
				Date todayDate = daily.getDayDate();
				String currentOperationUid = userContext.getUserSelection().getOperationUid();
				
				if (userContext.getUserSelection().getRigInformationUid() != null){
				
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime(todayDate);
					thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
					thisCalendar.set(Calendar.MINUTE, 59);
					thisCalendar.set(Calendar.SECOND , 59);
					thisCalendar.set(Calendar.MILLISECOND , 59);
							
					String strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) AND hseEventdatetime <= :currentDate AND incidentCategory IN (select hseCategory from LookupIncidentCategory where isLosttime ='1') AND operationUid=:operationUid ORDER BY hseEventdatetime DESC";
					String[] paramsFields2 = {"currentDate","operationUid"};
					Object[] paramsValues2 = {thisCalendar.getTime(),currentOperationUid};
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2);										
					
					if (lstResult.size() > 0){
						HseIncident hseIncident = (HseIncident) lstResult.get(0);
						
						CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "daysSinceLta");
								
					    Long eventDateTime = hseIncident.getHseEventdatetime().getTime();
	
						double daysSinceLastHseIncident = CommonUtil.getConfiguredInstance().calculateDaysSinceHseIncident(daily.getDayDate(), hseIncident.getHseEventdatetime(), userContext.getUserSelection().getGroupUid());
						thisConverter.setBaseValue(daysSinceLastHseIncident);
						Double daysSinceLastIncident = thisConverter.getConvertedValue();					
						
						ReportDataNode thisReportNode = reportDataNode.addChild("DaysSinceLastHseIncident");
					
						thisReportNode.addProperty("incidentUid", hseIncident.getHseIncidentUid());
						thisReportNode.addProperty("incidentCategory", hseIncident.getIncidentCategory());
						thisReportNode.addProperty("daysSinceHseIncident", daysSinceLastIncident.toString());					
						thisReportNode.addProperty("hseEventDateTime", hseIncident.getHseEventdatetime().toString());					
						thisReportNode.addProperty("hseEventDateTimeEpochMS", eventDateTime.toString());
						thisReportNode.addProperty("rigInformationUid", hseIncident.getRigInformationUid());
		
					}
				}
			
		}

	
}

