package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DailyShiftInfo;
import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.LookupRigEquipmentList;
import com.idsdatanet.d2.core.model.MudVolumeDetails;
import com.idsdatanet.d2.core.model.MudVolumes;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.RigCompressors;
import com.idsdatanet.d2.core.model.RigContainers;
import com.idsdatanet.d2.core.model.RigEngine;
import com.idsdatanet.d2.core.model.RigGenerators;
import com.idsdatanet.d2.core.model.RigHydrTongs;
import com.idsdatanet.d2.core.model.RigMobileWinch;
import com.idsdatanet.d2.core.model.RigPowerUnitHydrTongs;
import com.idsdatanet.d2.core.model.RigPowerUnitRotaryTable;
import com.idsdatanet.d2.core.model.RigPreventorControlUnit;
import com.idsdatanet.d2.core.model.RigPump;
import com.idsdatanet.d2.core.model.RigRotaryTable;
import com.idsdatanet.d2.core.model.RigSubstructure;
import com.idsdatanet.d2.core.model.RigSwivel;
import com.idsdatanet.d2.core.model.StockMovement;
import com.idsdatanet.d2.core.model.TourWireline;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.mudVolume.MudVolumeUtil;

public class OMVDailyReportDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		UserSelectionSnapshot userSelection = userContext.getUserSelection();

		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		if (daily == null) return;
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, daily.getOperationUid());
		
		if (operation!=null){
			ReportDataNode child = reportDataNode.addChild("DDR");
			
			//Map<String,LookupItem> bitSizeLookup= LookupManager.getConfiguredInstance().getLookup("xml://bitsize?key=code&amp;value=label", userContext.getUserSelection(), null);
			//Map<String,LookupItem> bitTypeLookup= LookupManager.getConfiguredInstance().getLookup("xml://bitrun.bit_type?key=code&amp;value=label", userContext.getUserSelection(), null);
			//Map<String,LookupItem> bhaComponentTypeLookup= LookupManager.getConfiguredInstance().getLookup("xml://bhacomponent.equipment_type_lookup?key=code&amp;value=label&amp;active=isActive", userContext.getUserSelection(), null);
			
			Calendar thisCalendar = Calendar.getInstance();
			thisCalendar.setTime(daily.getDayDate());
			thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
			thisCalendar.set(Calendar.MINUTE, 59);
			thisCalendar.set(Calendar.SECOND , 59);
			thisCalendar.set(Calendar.MILLISECOND , 59);
			
			//HSSE
			if (daily.getDailyUid()!=null){
				String strSql = "FROM DailyShiftInfo WHERE (isDeleted = false or isDeleted is null) AND dailyUid= :dailyUid ORDER BY startDatetime";
				String[] paramsFieldShift = {"dailyUid"};
				Object[] paramsValueShift = {daily.getDailyUid()};
				List<DailyShiftInfo> resultShift = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldShift, paramsValueShift);
				
				
				if(resultShift.size()>0){
					for (DailyShiftInfo shift : resultShift){
						Integer shiftCount = 1;
						child = reportDataNode.addChild("HSSE");
						child.addProperty("shiftLabel", shift.getShiftLabel());
						child.addProperty("hseShortdescription", "");
						
						strSql = "FROM HseIncident WHERE (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid AND hseEventdatetime >= :dayDate AND hseEventdatetime <= :currentDate AND incidentCategory = :incidentCategory ORDER BY sequence, hseEventdatetime";
						String[] paramsField = {"dailyUid", "currentDate", "dayDate", "incidentCategory" };
						Object[] paramsValue = {daily.getDailyUid(), thisCalendar.getTime(), daily.getDayDate(), "Tool Box Meeting"};
						List<HseIncident> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
						
						if(result.size()>0){				
							for(HseIncident hse : result){
								String shiftLabel = "";
								Date hseEventdatetime =  this.setReportDatetime(daily.getDayDate(), hse.getHseEventdatetime());
								
								if (hse.getHseEventdatetime()!=null) {	
									Date startDatetime = this.setReportDatetime(daily.getDayDate(), shift.getStartDatetime());
									Date endDatetime = this.setReportDatetime(daily.getDayDate(), shift.getEndDatetime());
									
									if ((hseEventdatetime.getTime()>= startDatetime.getTime()) && (hseEventdatetime.getTime() < endDatetime.getTime()) ){
										shiftLabel = shift.getShiftLabel();	
										//break;
										if (shiftCount>1){
											child = reportDataNode.addChild("HSSE");
										}
										shiftCount++;
										child.addProperty("shiftLabel", shiftLabel);
										child.addProperty("hseShortdescription", hse.getHseShortdescription());	
									}
								}				
							}
							
						}
					}
				}
			}
			
			//activity
			if (daily.getDailyUid()!=null){
				List<Activity> activities = getActivityForShift(userContext.getUserSelection(), daily);
				Double totalDuration = 0.00;
				Double cumDuration = 0.00;
				Integer count = 1;
				Integer countLabel = 1;
				String stringLabel = "";
				
				if( activities.size() > 0 ){
					for( Activity activity : activities ){
						if (stringLabel.equals(activity.getRigTourUid())){
							count = countLabel;
						}
						stringLabel = activity.getRigTourUid();
						countLabel = count;
						child = reportDataNode.addChild("Activity");
						
						child.addProperty("shiftLabel", countLabel.toString()+ "_"+activity.getRigTourUid());
						child.addProperty("duration", this.formatData(thisConverter, activity.getActivityDuration(), Activity.class, "activityDuration"));
						child.addProperty("ops", (activity.getTaskCode() != null?activity.getTaskCode():""));
						child.addProperty("activityDescription", (activity.getActivityDescription() != null?activity.getActivityDescription():""));
						if (activity.getActivityDuration()!=null){
							totalDuration = totalDuration + activity.getActivityDuration();
						}
						child.addProperty("additionalRemarks", (activity.getAdditionalRemarks() != null?activity.getAdditionalRemarks():""));
						count++;
					}
				}else{
					child = reportDataNode.addChild("Activity");
					child.addProperty("shiftLabel", "");
				}
				
				
				String strSql = "SELECT SUM(COALESCE(activityDuration,0.00))/3600 FROM Activity WHERE (isDeleted = false or isDeleted is null) "+
						"AND (isSimop=FALSE or isSimop IS NULL) AND (isOffline=FALSE or isOffline IS NULL) AND dayPlus='0' "+
						"AND operationUid=:operationUid AND startDatetime <= :reportDatetime ORDER BY startDatetime ";
				String[] paramsFieldAct = {"operationUid", "reportDatetime"}; 
				Object[] paramsValueAct= {daily.getOperationUid(), thisCalendar.getTime()};
				List<Object> resultAct = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldAct, paramsValueAct);
				
				if(resultAct.size()>0){
					Object thisResult = (Object) resultAct.get(0);
					if (thisResult!=null) {
						cumDuration = (Double) thisResult;
					}
				}
				child = reportDataNode.addChild("ActivityDuration");
				child.addProperty("totalDuration", this.formatData(thisConverter, totalDuration, Operation.class, "plannedDuration"));
				child.addProperty("cumDuration", this.formatData(thisConverter, cumDuration, Operation.class, "plannedDuration"));
			}
			
			//equipment tracking - Y
			if (operation.getRigInformationUid()!=null){
				Boolean gotRecordY = false;
				
				String strSql = "FROM RigMobileWinch WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				String[] paramsField = {"rigInformationUid"};
				Object[] paramsValue = {operation.getRigInformationUid()};
				List<RigMobileWinch> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result.size()>0){
					for(RigMobileWinch tbl : result){
						if (tbl.getWinchNumber()!=null){
							LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getWinchNumber());
							
							if (list!=null && list.getEquipmentNumber()!=null){
								String[] eq = list.getEquipmentNumber().split(" ");
								if (eq.length==2){
									child = reportDataNode.addChild("EquipmentTrackingY");
									child.addProperty("equipment", eq[0]);
									child.addProperty("equipment_number", eq[1]);
									child.addProperty("type", "RigMobileWinch");
									gotRecordY = true;
									if (tbl.getRunDuration()!=null) {		
										String runTime = tbl.getRunDuration() + this.formatUnit(thisConverter, RigMobileWinch.class, "runDuration");
										child.addProperty("runTime", runTime);
									}
								}
							}						
						}							
					}
				}
				
				strSql = "FROM RigPump WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				List<RigPump> result1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result1.size()>0){
					for(RigPump tbl : result1){
						if (tbl.getLookupRigEquipmentListUid()!=null){
							LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getLookupRigEquipmentListUid());
							
							if (list!=null && list.getEquipmentNumber()!=null){
								String[] eq = list.getEquipmentNumber().split(" ");
								if (eq.length==2){
									child = reportDataNode.addChild("EquipmentTrackingY");
									child.addProperty("equipment", eq[0]);
									child.addProperty("equipment_number", eq[1]);
									child.addProperty("type", "RigPump");
									gotRecordY = true;
									if (tbl.getRunDuration()!=null){
										String runTime = tbl.getRunDuration() + this.formatUnit(thisConverter, RigPump.class, "runDuration");
										child.addProperty("runTime", runTime);
									}
								}
							}
							
						}							
					}
				}
				
				
				strSql = "FROM RigPowerUnitHydrTongs WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				List<RigPowerUnitHydrTongs> result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result2.size()>0){
					for(RigPowerUnitHydrTongs tbl : result2){
						if (tbl.getPuTongsNumber()!=null){
							LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getPuTongsNumber());
							
							if (list!=null && list.getEquipmentNumber()!=null){
								String[] eq = list.getEquipmentNumber().split(" ");
								if (eq.length==2){
									child = reportDataNode.addChild("EquipmentTrackingY");
									child.addProperty("equipment", eq[0]);
									child.addProperty("equipment_number", eq[1]);
									child.addProperty("type", "RigPowerUnitHydrTongs");
									gotRecordY = true;
									if (tbl.getRunDuration()!=null){
										String runTime = tbl.getRunDuration() + this.formatUnit(thisConverter, RigPowerUnitHydrTongs.class, "runDuration");
										child.addProperty("runTime", runTime);
									}
								}
							}
						}							
					}
				}
				
				
				strSql = "FROM RigPowerUnitRotaryTable WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				List<RigPowerUnitRotaryTable> result3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result3.size()>0){
					for(RigPowerUnitRotaryTable tbl : result3){
						if (tbl.getPuRtNumber()!=null){
							LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getPuRtNumber());
							
							if (list!=null && list.getEquipmentNumber()!=null){
								String[] eq = list.getEquipmentNumber().split(" ");
								if (eq.length==2){
									child = reportDataNode.addChild("EquipmentTrackingY");
									child.addProperty("equipment", eq[0]);
									child.addProperty("equipment_number", eq[1]);
									child.addProperty("type", "RigPowerUnitRotaryTable");
									gotRecordY = true;
									if (tbl.getRunDuration()!=null){
										String runTime = tbl.getRunDuration() + this.formatUnit(thisConverter, RigPowerUnitRotaryTable.class, "runDuration");
										child.addProperty("runTime", runTime);
									}
								}
							}
						}							
					}
				}
				
				
				strSql = "FROM RigGenerators WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				List<RigGenerators> result4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result4.size()>0){
					for(RigGenerators tbl : result4){
						if (tbl.getLookupRigEquipmentListUid()!=null){
							LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getLookupRigEquipmentListUid());
							
							if (list!=null && list.getEquipmentNumber()!=null){
								String[] eq = list.getEquipmentNumber().split(" ");
								if (eq.length==2){
									child = reportDataNode.addChild("EquipmentTrackingY");
									child.addProperty("equipment", eq[0]);
									child.addProperty("equipment_number", eq[1]);
									child.addProperty("type", "RigGenerators");
									gotRecordY = true;
									if (tbl.getRunDuration()!=null){
										String runTime = tbl.getRunDuration() + this.formatUnit(thisConverter, RigGenerators.class, "runDuration");
										child.addProperty("runTime", runTime);
									}
								}
							}
						}							
					}
				}
				
				strSql = "FROM RigCompressors WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				List<RigCompressors> result5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result5.size()>0){
					for(RigCompressors tbl : result5){
						if (tbl.getLookupRigEquipmentListUid()!=null){
							LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getLookupRigEquipmentListUid());
							
							if (list!=null && list.getEquipmentNumber()!=null){
								String[] eq = list.getEquipmentNumber().split(" ");
								if (eq.length==2){
									child = reportDataNode.addChild("EquipmentTrackingY");
									child.addProperty("equipment", eq[0]);
									child.addProperty("equipment_number", eq[1]);
									child.addProperty("type", "RigCompressors");
									gotRecordY = true;
									if (tbl.getRunDuration()!=null){
										String runTime = tbl.getRunDuration() + this.formatUnit(thisConverter, RigCompressors.class, "runDuration");
										child.addProperty("runTime", runTime);
									}
								}
							}
						}							
					}
				}
				
				if (!gotRecordY){
					child = reportDataNode.addChild("EquipmentTrackingY");
					child.addProperty("equipment", "");
					child.addProperty("equipment_number", "");
					child.addProperty("type", "");		
				}
			}
			
			//equipment tracking - B
			if (operation.getRigInformationUid()!=null){
				Boolean gotRecordB = false;
				
				String strSql = "FROM TourWireline WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				String[] paramsField = {"rigInformationUid"};
				Object[] paramsValue = {operation.getRigInformationUid()};
				List<TourWireline> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result.size()>0){
					for(TourWireline tbl : result){
						if (tbl.getReelNumber()!=null){
							child = reportDataNode.addChild("EquipmentTrackingB");
							String[] eq = tbl.getReelNumber().replaceFirst(" ", "::").split("::");
							if (eq.length==1){
								child.addProperty("equipment", eq[0]);
							}
							if (eq.length==2){		
								child.addProperty("equipment", eq[0]);
								child.addProperty("equipment_number", eq[1]);						
							}	
							child.addProperty("type", "TourWireline");	
							gotRecordB = true;
						}							
					}
				}
				
				strSql = "FROM RigEngine WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				List<RigEngine> result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result2.size()>0){
					for(RigEngine tbl : result2){
						if (tbl.getEngineNumber()!=null){
							child = reportDataNode.addChild("EquipmentTrackingB");
							String[] eq = tbl.getEngineNumber().replaceFirst(" ", "::").split("::");
							if (eq.length==1){
								child.addProperty("equipment", eq[0]);
							}
							if (eq.length==2){
								child.addProperty("equipment", eq[0]);
								child.addProperty("equipment_number", eq[1]);					
							}	
							child.addProperty("type", "RigEngine");	
							gotRecordB = true;
						}							
					}
				}
				
				strSql = "FROM RigPreventorControlUnit WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				List<RigPreventorControlUnit> result3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result3.size()>0){
					for(RigPreventorControlUnit tbl : result3){
						if (tbl.getPcuNumber()!=null){
							LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getPcuNumber());
							
							if (list!=null && list.getEquipmentNumber()!=null){
								String[] eq = list.getEquipmentNumber().split(" ");
								if (eq.length==2){
									child = reportDataNode.addChild("EquipmentTrackingB");
									child.addProperty("equipment", eq[0]);
									child.addProperty("equipment_number", eq[1]);
									child.addProperty("type", "RigPreventorControlUnit");	
									gotRecordB = true;
								}
							}
						}							
					}
				}
				
				strSql = "FROM RigHydrTongs WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				List<RigHydrTongs> result4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result4.size()>0){
					for(RigHydrTongs tbl : result4){
						if (tbl.getTongsNumber()!=null){
							LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getTongsNumber());
							
							if (list!=null && list.getEquipmentNumber()!=null){
								String[] eq = list.getEquipmentNumber().split(" ");
								if (eq.length==2){
									child = reportDataNode.addChild("EquipmentTrackingB");

									if (StringUtils.isNumeric(eq[1].toString())){
										child.addProperty("equipment", eq[0]);
										child.addProperty("equipment_number", eq[1]);
									}else{
										child.addProperty("equipment", list.getEquipmentNumber());
									}
									
									child.addProperty("type", "RigHydrTongs");		
									gotRecordB = true;
								}
							}
						}							
					}
				}
				
				strSql = "FROM RigRotaryTable WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				List<RigRotaryTable> result5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result5.size()>0){
					for(RigRotaryTable tbl : result5){
						if (tbl.getRtNumber()!=null){
							LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getRtNumber());
							
							if (list!=null && list.getEquipmentNumber()!=null){
								String[] eq = list.getEquipmentNumber().split(" ");
								if (eq.length==2){
									child = reportDataNode.addChild("EquipmentTrackingB");
									child.addProperty("equipment", eq[0]);
									child.addProperty("equipment_number", eq[1]);
									child.addProperty("type", "RigRotaryTable");
									gotRecordB = true;
								}
							}
						}							
					}
				}
				
				strSql = "FROM RigSubstructure WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				List<RigSubstructure> result6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result6.size()>0){
					for(RigSubstructure tbl : result6){
						if (tbl.getSubstructureNumber()!=null){
							LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getSubstructureNumber());
							
							if (list!=null && list.getEquipmentNumber()!=null){
								String[] eq = list.getEquipmentNumber().split(" ");
								if (eq.length==2){
									child = reportDataNode.addChild("EquipmentTrackingB");
									child.addProperty("equipment", eq[0]);
									child.addProperty("equipment_number", eq[1]);
									child.addProperty("type", "RigSubstructure");	
									gotRecordB = true;
								}
							}
						}							
					}
				}
				
				strSql = "FROM RigSwivel WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid ";
				List<RigSwivel> result7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result7.size()>0){
					for(RigSwivel tbl : result7){
						if (tbl.getSwivelNumber()!=null){
							LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getSwivelNumber());
							
							if (list!=null && list.getEquipmentNumber()!=null){
								String[] eq = list.getEquipmentNumber().split(" ");
								if (eq.length==2){
									child = reportDataNode.addChild("EquipmentTrackingB");
									child.addProperty("equipment", eq[0]);
									child.addProperty("equipment_number", eq[1]);
									child.addProperty("type", "RigSwivel");		
									gotRecordB = true;
								}
							}
						}							
					}
				}
				
				if (!gotRecordB){
					child = reportDataNode.addChild("EquipmentTrackingB");
					child.addProperty("equipment", "");
					child.addProperty("equipment_number", "");
					child.addProperty("type", "");		
				}
			}
			
			//equipment tracking - P
			if (operation.getRigInformationUid()!=null){
				
				child = reportDataNode.addChild("EquipmentTrackingContainers");
				child.addProperty("equipment", "MCO");
				child.addProperty("serial", this.getSerialNumberList(operation.getRigInformationUid(), "Crew Trailer"));
				
				String strSql = "SELECT type FROM RigContainers WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid AND type <> 'Crew Trailer' GROUP BY type";
				String[] paramsField = {"rigInformationUid"};
				Object[] paramsValue = {operation.getRigInformationUid()};
				List<Object> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if(result.size()>0){
					for(Object rc : result){
						String serial = "";
						child = reportDataNode.addChild("EquipmentTrackingContainers");	
						
						if (rc!=null){		
							child.addProperty("equipment", this.getContainerCode(operation.getRigInformationUid(), rc.toString()));
							serial = this.getSerialNumberList(operation.getRigInformationUid(), rc.toString());
						}
						child.addProperty("serial", serial);
					}
				}
				
				strSql = "SELECT SUM(COALESCE(maxVolumeCapacity,0.00)) FROM RigTanks WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid";
				String[] paramsFieldRt = {"rigInformationUid"};
				Object[] paramsValueRt = {operation.getRigInformationUid()};
				List<Object> resultRt = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldRt, paramsValueRt);
				
				if(resultRt.size()>0){
					Object thisResult = (Object) resultRt.get(0);
					child = reportDataNode.addChild("EquipmentTrackingTanks");
					if (thisResult!=null) child.addProperty("sumMaxCapacity", thisResult.toString());				
				}
			}
			
			HashMap<String, Integer> dailyShiftInfo = new HashMap<String, Integer>();
			String queryString = "FROM DailyShiftInfo WHERE (isDeleted=false or isDeleted is null) and dailyUid=:dailyUid ORDER BY startDatetime";
			List<DailyShiftInfo> dailyShiftList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", daily.getDailyUid());
			int count = 1;
			
			for (DailyShiftInfo ds : dailyShiftList) {
				dailyShiftInfo.put(ds.getShiftLabel(), count);
				count++;
			}
			
			if (daily.getDailyUid()!=null){
				
				queryString = "SELECT formationUid FROM MudVolumes WHERE (isDeleted=false or isDeleted is null) and dailyUid=:dailyUid AND checkTime IS NOT NULL AND (formationUid <> '' AND formationUid IS NOT NULL ) GROUP BY formationUid";
				List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", daily.getDailyUid());
				
				if (list.size()>0){ 
					thisConverter.setReferenceMappingField(MudVolumeDetails.class, "volume");
					
					for (Object mv : list) {
						
						if (mv!=null){
							child = reportDataNode.addChild("MudVolumes");
							
							Formation formation = (Formation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Formation.class, mv.toString());					
							
							if (formation!=null){
								child.addProperty("formationName", formation.getFormationName());
							}
							Double cumTotal = this.calculateCumVolume(daily.getOperationUid(), daily.getDayDate(), "loss", mv.toString());
							if (cumTotal!=null){
								thisConverter.setBaseValue(cumTotal);
								child.addProperty("shiftCumLoss", this.formatData(thisConverter, thisConverter.getConvertedValue(), MudVolumeDetails.class, "volume"));
							}
							
							for (String i : dailyShiftInfo.keySet()) {
							      child.addProperty("column_"+dailyShiftInfo.get(i), i);
							}
							
							String strSql = "FROM MudVolumes WHERE (isDeleted = false or isDeleted is null) AND dailyUid=:dailyUid AND checkTime IS NOT NULL AND formationUid=:formationUid ORDER BY checkTime";
							String[] paramsField = {"dailyUid", "formationUid"};
							Object[] paramsValue = {daily.getDailyUid(), mv.toString()};
							List<MudVolumes> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
							Double totalLosses = 0.00;
							String shift = "";
							
							if(result.size()>0){	
								for(MudVolumes mvs : result){
									
									if(mvs.getCheckTime()!=null){		
										String shiftLabel = this.getShift(daily.getDailyUid(), mvs.getCheckTime());
										
										if(!shift.equals(shiftLabel)){
											totalLosses = 0.00;
										}
										
										if (StringUtils.isNotBlank(shiftLabel)){
											int column = dailyShiftInfo.get(shiftLabel);
											child.addProperty("shiftLabel_"+column, shiftLabel);
											
											totalLosses = totalLosses + MudVolumeUtil.calculateTotalVolumeFor(daily.getOperationUid(), daily.getDailyUid(), mvs.getMudVolumesUid(), "loss");							
											if (totalLosses!=null){
												thisConverter.setBaseValue(totalLosses);												
												child.addProperty("shiftLoss_"+column, this.formatData(thisConverter, thisConverter.getConvertedValue(), MudVolumeDetails.class, "volume"));
											}
											shift = shiftLabel;
										}
										
									}
									
								}
							}
							
							
						}
						
					}
				}else{
					count = 1;
					child = reportDataNode.addChild("MudVolumes");
					child.addProperty("formationName", "");
					for (String i : dailyShiftInfo.keySet()) {
					      child.addProperty("column_"+dailyShiftInfo.get(i), i);
					}
					for (DailyShiftInfo ds : dailyShiftList) {
						child.addProperty("shiftLabel_"+count, ds.getShiftLabel());
					}
				}
			}
			
			if (daily.getDailyUid()!=null){
				
				queryString = "SELECT category FROM StockMovement WHERE (isDeleted=false or isDeleted is null) AND dailyUid=:dailyUid AND movementDatetime IS NOT NULL GROUP BY category";
				List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", daily.getDailyUid());
				
				if (list.size()>0){
					for (Object mv : list) {
						
						if (mv!=null){
							child = reportDataNode.addChild("Logistics");	
							String category = "";
							
							if ("fluid".equals( mv.toString())){
								category = "SWT";
							}else if("pipe".equals( mv.toString())){
								category = "RT";
							}
							child.addProperty("category", category);
							
							for (String i : dailyShiftInfo.keySet()) {
							      child.addProperty("column_"+dailyShiftInfo.get(i), i);
							      child.addProperty("count_"+dailyShiftInfo.get(i), i+":");
							}
							
							String strSql = "FROM StockMovement WHERE (isDeleted = false or isDeleted is null) AND dailyUid=:dailyUid AND movementDatetime IS NOT NULL AND category=:category";
							String[] paramsField = {"dailyUid", "category"};
							Object[] paramsValue = {daily.getDailyUid(), mv.toString()};
							List<StockMovement> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
												
							if(result.size()>0){
								String direction = "";
								int column = 0;
								
								for(StockMovement sm : result){
									if(sm.getMovementDatetime()!=null){
										String shiftLabel = this.getShift(daily.getDailyUid(), sm.getMovementDatetime());
										
										if (StringUtils.isNotBlank(shiftLabel)){
											
											column = dailyShiftInfo.get(shiftLabel);
											child.addProperty("shiftLabel_"+column, shiftLabel);	
											
											DailyShiftInfo dsi = this.getDailyShiftInfo(daily.getDailyUid(), shiftLabel) ;
											
											if(dsi!=null){
												direction = shiftLabel + ": " + this.getStockMovementByShift(daily.getDailyUid(), mv.toString(), dsi.getStartDatetime(), dsi.getEndDatetime());
											}
												
											child.addProperty("count_"+column, direction);
										}
									}
								}					
							}
						}
					}
				}else{
					child = reportDataNode.addChild("Logistics");	
					child.addProperty("category", "");
					
					for (String i : dailyShiftInfo.keySet()) {
					      child.addProperty("column_"+dailyShiftInfo.get(i), i);
					      child.addProperty("count_"+dailyShiftInfo.get(i), i+":");
					}
				}
				
			}
		}
		
	}
	
	private DailyShiftInfo getDailyShiftInfo(String dailyUid, String shiftLabel) throws Exception{
		
		String queryString = "FROM DailyShiftInfo WHERE (isDeleted=false or isDeleted is null) and shiftLabel=:shiftLabel and dailyUid=:dailyUid ORDER BY startDatetime";
		String[] paramsField = {"dailyUid", "shiftLabel"};
		Object[] paramsValue = {dailyUid, shiftLabel};
		List<DailyShiftInfo> dailyShiftList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramsField, paramsValue);
		
		if (dailyShiftList.size()>0){
			DailyShiftInfo shift = (DailyShiftInfo) dailyShiftList.get(0);
			
			if (shift!=null){
				return shift;
			}
		}
		
		return null;
	}
	
	private String getStockMovementByShift(String dailyUid, String category, Date startDatetime, Date endDatetime) throws Exception{
		String direction = "";
		int count_ab = 0;
		int count_an = 0;
		
		String strSql = "FROM StockMovement WHERE (isDeleted = false or isDeleted is null) AND dailyUid=:dailyUid AND movementDatetime IS NOT NULL AND category=:category " +
						"AND movementDatetime >=:startDatetime AND movementDatetime < :endDatetime ";
		String[] paramsField = {"dailyUid", "category", "startDatetime", "endDatetime"};
		Object[] paramsValue = {dailyUid, category, startDatetime, endDatetime};
		List<StockMovement> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
							
		if(result.size()>0){
			for(StockMovement sm : result){
				if ("sentoff".equals(sm.getDirection())){
					count_ab ++;
				}else if ("received".equals(sm.getDirection())){
					count_an++;
				}
			}
			
			if (count_ab>0){
				direction = Integer.toString(count_ab) + " x ab.";
			}
			if (count_an>0){
				direction = (count_ab>0?direction + " ":"") + Integer.toString(count_an) + " x an.";
			}
		}
		
		return direction;
	}
	public double calculateCumVolume(String operationUid, Date reportDatetime, String type, String formationUid) throws Exception
	{
		String[] paramsFields = {"reportDatetime", "operationUid", "formationUid", "type"};
		Object[] paramsValues = new Object[4]; 
		paramsValues[0] = reportDatetime; 
		paramsValues[1] = operationUid; 
		paramsValues[2] = formationUid;
		paramsValues[3] = type;
		
		String strSql = "select sum(mvd.volume) as totalVolume from MudVolumeDetails mvd, MudVolumes mv where (mvd.isDeleted = false or mvd.isDeleted is null) and (mv.isDeleted = false or mv.isDeleted is null) and mvd.type = :type " + 
						"and mv.dailyUid IN (SELECT dailyUid FROM ReportDaily where (isDeleted = false or isDeleted is null) AND operationUid=:operationUid AND reportDatetime <=:reportDatetime ) "+
						"and mv.operationUid = :operationUid and mv.formationUid =:formationUid and mvd.mudVolumesUid = mv.mudVolumesUid ";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		Object a = (Object) lstResult.get(0);
		Double totalVolume = 0.00;
		if (a != null) totalVolume = Double.parseDouble(a.toString());
		return totalVolume;
	}
	
	private String getContainerCode(String rigInformationUid, String type) throws Exception{
		String code = "";
		String strSql = "FROM RigContainers WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid AND type=:type";
		String[] paramsFieldRc = {"rigInformationUid", "type"};
		Object[] paramsValueRc = {rigInformationUid, type};
		List<RigContainers> resultRc = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldRc, paramsValueRc);
		
		if(resultRc.size()>0){
			for(RigContainers tbl : resultRc){
				if (tbl.getLookupRigEquipmentListUid()!=null){
					LookupRigEquipmentList list = (LookupRigEquipmentList) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigEquipmentList.class, tbl.getLookupRigEquipmentListUid());
					
					if (list!=null && list.getEquipmentNumber()!=null){
						String[] eq = list.getEquipmentNumber().split(" ");
						if (eq[0]!=null){
							code = eq[0].toString();
						}
					}
				}
				break;
			}
		}
		return code;
	}
	
	private String getShift(String dailyUid, Date checkTime) throws Exception{
		String shiftlabel = "";
		
		String queryString = "FROM DailyShiftInfo WHERE (isDeleted=false or isDeleted is null) AND startDatetime <=:checkTime AND endDatetime > :checkTime and dailyUid=:dailyUid ORDER BY startDatetime";
		String[] paramsField = {"dailyUid", "checkTime"};
		Object[] paramsValue = {dailyUid, checkTime};
		List<DailyShiftInfo> dailyShiftList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramsField, paramsValue);
		
		if (dailyShiftList.size()>0){
			DailyShiftInfo shift = (DailyShiftInfo) dailyShiftList.get(0);
			
			if (shift!=null){
				shiftlabel = shift.getShiftLabel();
			}
		}
		
		return shiftlabel;
	}
	
	private String getSerialNumberList(String rigInformationUid, String type) throws Exception{
		String serial = "";
		String strSql = "FROM RigContainers WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid=:rigInformationUid AND type=:type";
		String[] paramsFieldRc = {"rigInformationUid", "type"};
		Object[] paramsValueRc = {rigInformationUid, type};
		List<RigContainers> resultRc = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFieldRc, paramsValueRc);
		
		if(resultRc.size()>0){
			int count = 1;
			for(RigContainers tbl : resultRc){
				if (count==1){
					serial = tbl.getSerialNumber();
				}else{
					serial = serial + ", "+ tbl.getSerialNumber();
				}
				count ++;
			}
		}
		return serial;
	}
	
	private Date setReportDatetime(Date dayDate, Date datetime){
		
		Calendar time = Calendar.getInstance();
		time.setTime(datetime);
		
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTime(dayDate);
		thisCalendar.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
		thisCalendar.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
		thisCalendar.set(Calendar.SECOND , time.get(Calendar.SECOND));
		thisCalendar.set(Calendar.MILLISECOND , time.get(Calendar.MILLISECOND));
		
		Date a = thisCalendar.getTime();
		return thisCalendar.getTime();
	}
	
	private String getLookupValue(Map<String,LookupItem> lookupList, Object lookupvalue) throws Exception {
		String retValue = "";
		
		if (lookupvalue !=null && StringUtils.isNotBlank(lookupvalue.toString())  && lookupList !=null){
			 LookupItem lookup = lookupList.get(lookupvalue.toString());
			if (lookup !=null)	retValue = lookup.getValue().toString();					
		} 
		
		return retValue;
	}
	
	private Double getConvertedUOM(CustomFieldUom thisConverter, Double value, String unit, Boolean offset) throws Exception{
		if (thisConverter!=null){
			thisConverter.setBaseValueFromUserValue(value);
			thisConverter.changeUOMUnit(unit);
			if (offset) thisConverter.addDatumOffset();
			
			return thisConverter.getConvertedValue();
		}

		return null;
	}
	
	private String getCommonLookup(String lookupTypeSelection, String shortCode) throws Exception{
		String lookupLabel = "";
		
		String strSql = "FROM CommonLookup Where (isDeleted is null or isDeleted = false) and lookupTypeSelection=:lookupTypeSelection AND shortCode=:shortCode ";
		String[] paramsField = {"lookupTypeSelection", "shortCode"};
		Object[] paramsValue = {lookupTypeSelection, shortCode};
		List<CommonLookup> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
		
		if (result.size() > 0){
			for (CommonLookup cl : result){
				lookupLabel = cl.getLookupLabel();
			}
		}
		return lookupLabel;
	}
	
	private List<Activity> getActivityForShift( UserSelectionSnapshot userSelection, Daily d) throws Exception {
		List<Activity> result = new ArrayList();

		String queryString = "FROM DailyShiftInfo WHERE (isDeleted=false or isDeleted is null) and dailyUid=:dailyUid ORDER BY startDatetime";
		List<DailyShiftInfo> dailyShiftList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", d.getDailyUid());
		for (DailyShiftInfo ds : dailyShiftList) {

			if (ds.getStartDatetime()!=null && ds.getEndDatetime()!=null) {
				Date startDatetime = this.setReportDatetime(d.getDayDate(), ds.getStartDatetime());
				Date endDatetime = this.setReportDatetime(d.getDayDate(), ds.getEndDatetime());
				
				queryString = "FROM Activity WHERE (isDeleted=false or isDeleted is null) " +
						"AND (isOffline=false or isOffline is null) " +
						"AND (isSimop=false or isSimop is null) " +
						"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
						"AND (dayPlus=0 or dayPlus is null) " +
						"AND dailyUid=:dailyUid " +
						"ORDER by startDatetime";
				List<Activity> activityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", d.getDailyUid());
				for (Activity activity : activityList) {
					Date activityStart = this.setReportDatetime(d.getDayDate(), activity.getStartDatetime());
					
					if ((activityStart.getTime()>= startDatetime.getTime()) && (activityStart.getTime() < endDatetime.getTime())){
						activity.setRigTourUid(ds.getShiftLabel());
						result.add(activity);
					}	
				}
								
			}
		}
		return result;
	}
	
	public int xformatTimeInt(Date newDate){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		
		String Hour=String.valueOf(calendar.get(Calendar.HOUR));
		String Min=String.valueOf(calendar.get(Calendar.MINUTE));
		
		if (Hour.length()==1)
		{
			if (Hour.equals("0")){
				Hour="00";
			}else{
				Hour="0"+Hour;
			}
		}
		if (Min.length()==1)
		{
			Min="0"+Min;
		}
		if (Integer.parseInt(Hour + Min) == 2359)
			return 2400;
		else
			return Integer.parseInt(Hour + Min);
	}
	
	public Activity getShiftActivity(CustomFieldUom thisConverter, Activity activity, DailyShiftInfo ds, Date shiftStartDatetime, Date shiftEndDatetime) throws Exception {
		int shiftStartTime = this.formatTimeInt(shiftStartDatetime);
		int shiftEndTime = this.formatTimeInt(shiftEndDatetime);
		int actStartTime = this.formatTimeInt(activity.getStartDatetime());
		int actEndTime = this.formatTimeInt(activity.getEndDatetime());
		
		if (shiftEndTime <= actStartTime || actEndTime<=shiftStartTime) {
			return null; 
		} else if (shiftStartTime<=actStartTime && actEndTime<=shiftEndTime) {
			activity.setRigTourUid(ds.getShiftLabel());
			return activity;
		}
		Date newStartTime = activity.getStartDatetime();
		Date newEndTime = activity.getEndDatetime();
		if (actStartTime<shiftStartTime) newStartTime = shiftStartDatetime;
		if (shiftEndTime<actEndTime) newEndTime = shiftEndDatetime;
		actStartTime = this.formatTimeInt(newStartTime);
		actEndTime = this.formatTimeInt(newEndTime);
		if (actEndTime<=actStartTime) return null;
		
		Activity newActivity = new Activity();
		PropertyUtils.copyProperties(newActivity, activity);
		newActivity.setActivityUid(activity.getActivityUid()+Math.random());
		Date startDatetime = CommonUtil.getConfiguredInstance().stampTimeFieldWithDate(activity.getDailyUid(), newStartTime);
		newActivity.setStartDatetime(startDatetime);
		Date endDatetime = CommonUtil.getConfiguredInstance().stampTimeFieldWithDate(activity.getDailyUid(), newEndTime);
		newActivity.setEndDatetime(endDatetime);
		
		Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(newActivity.getStartDatetime(), newActivity.getEndDatetime());
		thisConverter.setBaseValue(thisDuration);
		newActivity.setActivityDuration(thisConverter.getConvertedValue());
		newActivity.setRigTourUid(ds.getShiftLabel());
		return newActivity;
	}

	private String formatData(CustomFieldUom thisConverter, Double value, Class className, String fieldName) throws Exception {
		if (value!=null) {
			if (thisConverter!=null) {
				thisConverter.setReferenceMappingField(className, fieldName);
				if (thisConverter.isUOMMappingAvailable()) {
					return thisConverter.formatOutputPrecision(value);
				}				
			}
			return String.valueOf(value);
		}		
		return "";
	}
	
	private String formatUnit(CustomFieldUom thisConverter, Class className, String fieldName) throws Exception {
		if (thisConverter!=null) {
			thisConverter.setReferenceMappingField(className, fieldName);
			if (thisConverter.isUOMMappingAvailable()) {
				return " " + thisConverter.getUomSymbol();
			}				
		}	
			
		return "";
	}
	
	private String formatDataUom(CustomFieldUom thisConverter, Double value, Class className, String fieldName, Boolean uom, Boolean space) throws Exception {
		if (value!=null) {
			if (thisConverter!=null) {
				thisConverter.setReferenceMappingField(className, fieldName);
				if (thisConverter.isUOMMappingAvailable()) {
					return thisConverter.formatOutputPrecision(value) + (space?" ":"") + (uom?thisConverter.getUomSymbol():"");
				}				
			}
			return String.valueOf(value);
		}		
		return "";
	}
	
	@SuppressWarnings("deprecation")
	public int formatTimeInt(Date newDate){
		String Hour=String.valueOf(newDate.getHours());
		String Min=String.valueOf(newDate.getMinutes());
		
		if (Hour.length()==1)
		{
			if (Hour.equals("0")){
				Hour="00";
			}else{
				Hour="0"+Hour;
			}
		}
		if (Min.length()==1)
		{
			Min="0"+Min;
		}
		if (Integer.parseInt(Hour + Min) == 2359)
			return 2400;
		else
			return Integer.parseInt(Hour + Min);
	}
}
