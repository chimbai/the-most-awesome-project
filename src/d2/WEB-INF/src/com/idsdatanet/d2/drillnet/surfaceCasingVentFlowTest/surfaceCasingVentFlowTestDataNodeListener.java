
package com.idsdatanet.d2.drillnet.surfaceCasingVentFlowTest;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.SurfaceCasingVentFlowTest;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class surfaceCasingVentFlowTestDataNodeListener extends EmptyDataNodeListener {
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		Object obj = node.getData();
		
		if(obj instanceof SurfaceCasingVentFlowTest){
			SurfaceCasingVentFlowTest thisSurfaceCasingVentFlowTest = (SurfaceCasingVentFlowTest) obj;
			
			if (thisSurfaceCasingVentFlowTest.getResolutionDateTime() != null &&  thisSurfaceCasingVentFlowTest.getTestDateTime() != null) {
				if(thisSurfaceCasingVentFlowTest.getResolutionDateTime().getTime() < thisSurfaceCasingVentFlowTest.getTestDateTime().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "resolutionDateTime", "Resolution Date should be on or after Test Date");
					return;
				}
			}
		}
		
	}
	
}
