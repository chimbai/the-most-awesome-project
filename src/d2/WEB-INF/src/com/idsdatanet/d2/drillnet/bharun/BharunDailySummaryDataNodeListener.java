package com.idsdatanet.d2.drillnet.bharun;
import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.BhaComponent;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class BharunDailySummaryDataNodeListener extends EmptyDataNodeListener {
	
	public void onDataNodeCarryForwardFromYesterday(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception{
		//on carry record data need to make sure we need to carry BHArun, bit or components or not
		
		Object object = node.getData();
		
		if (object instanceof BharunDailySummary) {
			BharunDailySummary thisBHADaily = (BharunDailySummary)object;
			
			//need to take the BHA run and check if this is same operation, if same no need to copy run level data over, if not need to replicate 
			String BharunUid = thisBHADaily.getBharunUid();
			String strSql;
			
			QueryProperties qp = QueryProperties.disableUOMAndDatumConversion();
			
			strSql = "FROM Bharun a WHERE (a.isDeleted = false or a.isDeleted is null) and a.bharunUid = :BharunUid";
			String[] paramsFields3 = {"BharunUid"};
			String[] paramsValues3 = {BharunUid};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields3, paramsValues3, qp);
			if (lstResult.size() > 0)
			{
				Bharun thisBharun = (Bharun) lstResult.get(0);
				
				//if the bha is not from the same operation then busy time, need to replicate run and all data over
				if (!session.getCurrentOperationUid().equalsIgnoreCase(thisBharun.getOperationUid())) {
					Bharun newBharun = (Bharun) BeanUtils.cloneBean(thisBharun);
					newBharun.setBharunUid(null);
					newBharun.setOperationUid(null);
					newBharun.setWellUid(null);
					newBharun.setWellboreUid(null);
					 
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newBharun,qp);
					
					//set the new bharun id to this bha daily object
					thisBHADaily.setBharunUid(newBharun.getBharunUid());
					
					//copy the bit data, need to loop just in case 1 bha have multiple bit
					strSql = "FROM Bitrun WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
					List lstResultBit = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", BharunUid, qp);
					Bitrun thisBitrun, newBitrun;
					if (lstResultBit.size() > 0){
						for(Object objBitrun: lstResultBit){
							thisBitrun = (Bitrun) objBitrun;
							newBitrun = (Bitrun) BeanUtils.cloneBean(thisBitrun);
							newBitrun.setBitrunUid(null);
							newBitrun.setOperationUid(null);
							newBitrun.setBharunUid(newBharun.getBharunUid());
							newBitrun.setWellUid(null);
							newBitrun.setWellboreUid(null);
							
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newBitrun,qp);
							
							//copy the bit nozzle data
							strSql = "FROM BitNozzle WHERE (isDeleted = false or isDeleted is null) and bitrunUid =:BitrunUid";
							List lstResultBitNozzle = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BitrunUid", thisBitrun.getBitrunUid(), qp);
							BitNozzle thisNozzle, newBitNozzle;
							if (lstResultBitNozzle.size() > 0){
								for(Object objBitNozzle: lstResultBitNozzle){
									thisNozzle = (BitNozzle) objBitNozzle;
									newBitNozzle = (BitNozzle) BeanUtils.cloneBean(thisNozzle);
									newBitNozzle.setBitNozzleUid(null);
									newBitNozzle.setBitrunUid(newBitrun.getBitrunUid());
									
									ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newBitNozzle,qp);
								}
							}
						}
					}//end copy bit data
					
					//copy bha component
					strSql = "FROM BhaComponent WHERE (isDeleted = false or isDeleted is null) and bharunUid =:BharunUid";
					List lstResultBhaComponent = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "BharunUid", BharunUid, qp);
					BhaComponent thisBhaComponent, newBhaComponent;
					if (lstResultBhaComponent.size() > 0){
						for(Object objBhaComponent: lstResultBhaComponent){
							thisBhaComponent = (BhaComponent) objBhaComponent;
							newBhaComponent = (BhaComponent) BeanUtils.cloneBean(thisBhaComponent);
							newBhaComponent.setBhaComponentUid(null);
							newBhaComponent.setBharunUid(newBharun.getBharunUid());
							newBhaComponent.setWellUid(null);
							newBhaComponent.setWirelineRunUid(null);	
							
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newBhaComponent,qp);
						}
					}//end copy bha component
					
				}//end same operation check
			}
			
			//set Bit Daily value in bharun_daily_summary when start new day carry over record bharun_daily_summary
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "copyCumulativeValueFromDrillParam"))) {
				
				String strSql2 = "FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :BharunUid AND dailyUid = :DailyUid";
				String[] paramsFields2 = {"BharunUid","DailyUid"};
				String[] paramsValues2 = {BharunUid,thisBHADaily.getDailyUid()};
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
				if (lstResult2.size() > 0)
				{
					CommonUtil.getConfiguredInstance().copyValueFromDrilParam(thisBHADaily.getDailyUid());
				}
				else 
				{							
					thisBHADaily.setProgress(null);
					thisBHADaily.setDuration(null);
					thisBHADaily.setIADCDuration(null);
					thisBHADaily.setKrevs(null);
					thisBHADaily.setSlideHours(null);
					thisBHADaily.setDurationRotated(null);
					thisBHADaily.setDurationCirculated(null);
					thisBHADaily.setPercentSlide(null);
					thisBHADaily.setPercentRotated(null);
					thisBHADaily.setPercentCirculated(null);
					thisBHADaily.setHsi(null);
					
					thisBHADaily.setWobMinForce(null);
					thisBHADaily.setWobMaxForce(null);							
					thisBHADaily.setSurfaceRpmMin(null);
					thisBHADaily.setSurfaceRpmMax(null);
					thisBHADaily.setFlowRateMin(null);
					thisBHADaily.setFlowRateMax(null);
					thisBHADaily.setStandpipePressureMin(null);
					thisBHADaily.setStandpipePressureMax(null);
					thisBHADaily.setStringWeightRotaryMinForce(null);
					thisBHADaily.setStringWeightRotaryMaxForce(null);
					thisBHADaily.setPickupWeightMinForce(null);
					thisBHADaily.setPickupWeightMaxForce(null);
					thisBHADaily.setSlackOffWeightMinForce(null);
					thisBHADaily.setSlackOffWeightMaxForce(null);
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisBHADaily, qp);
				}			
			}
		}	
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		//re-calculate and save the bit depth drilled in & out and bit hours in & out 
		if (obj instanceof BharunDailySummary) {
			CommonUtil.getConfiguredInstance().calcCumulativeBitProgressDuration(session.getCurrentDailyUid(), session.getCurrentOperationUid());
		}
	}
}