package com.idsdatanet.d2.drillnet.witsmlLink;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.WitsmlLink;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

/**
 * 
 * @author RYAU
 *
 */
public class WitsmlLinkDataNodeListener extends EmptyDataNodeListener {
	private static final String WITSML_SEPARATOR = "__witsml__separator__";
	
	@Override
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {

		Object object = node.getData();
		if (object instanceof WitsmlLink) {
			WitsmlLink witsmlLink = (WitsmlLink) object;
			
			String temp = witsmlLink.getWitsmlWellName();
			String[] uidAndName = temp.split(WITSML_SEPARATOR);
			witsmlLink.setWitsmlWellUid(uidAndName[0]);
			witsmlLink.setWitsmlWellName(uidAndName[1]);
			
			temp = witsmlLink.getWitsmlWellboreName();
			uidAndName = temp.split(WITSML_SEPARATOR);
			witsmlLink.setWitsmlWellboreUid(uidAndName[0]);
			witsmlLink.setWitsmlWellboreName(uidAndName[1]);
		}
	}
}
