package com.idsdatanet.d2.drillnet.bitrun;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class BitrunandDrillingParametersFwrDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		Map<String,LookupItem> bitSizeLookup= LookupManager.getConfiguredInstance().getLookup("xml://bitsize?key=code&amp;value=label", userContext.getUserSelection(), null);
		Map<String,LookupItem> mudTypeLookup= LookupManager.getConfiguredInstance().getLookup("xml://mud_properties.mud_type?key=code&amp;value=label", userContext.getUserSelection(), null);
		Map<String,LookupItem> bitTypeLookup= LookupManager.getConfiguredInstance().getLookup("xml://bitrun.bit_type?key=code&amp;value=label", userContext.getUserSelection(), null);
			
		String concatBitNozzle;
		
		//Bitrun and Bharun
		String sql = "select distinct A.bharunUid,A.wellUid,A.wellboreUid,A.operationUid,A.depthInMdMsl,A.depthOutMdMsl,B.bitrunUid,B.bitrunNumber," +
		             "B.bitDiameter,B.make,B.model,B.bitStatus,B.bitType,B.tfa,B.comment,B.condFinalInner,B.condFinalOuter,B.condFinalDull,B.condFinalLocation," +
		             "B.condFinalBearing,B.condFinalGauge,B.condFinalOther,B.condFinalReason,B.serialNumber from Bharun A, Bitrun B,DrillingParameters C where (A.isDeleted=false or A.isDeleted is null) " +
		             "and (B.isDeleted=false or B.isDeleted is null) and (C.isDeleted=false or C.isDeleted is null) and A.bharunUid = B.bharunUid and A.operationUid=:operationUid and A.bharunUid=C.bharunUid " +
		             "order by A.depthInMdMsl,A.depthOutMdMsl";
		
		List<Object[]> lstData = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql ,"operationUid", userContext.getUserSelection().getOperationUid());
		
		if (lstData.size()>0){
			ReportDataNode bitrunDP = reportDataNode.addChild("bitrunDrillingParameter");
			ReportDataNode currentDataNode = null;
			
			String bitDiameter="";
			
			CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale());
						
			for(Object[] item : lstData){
				
				String bharunUid = item[0].toString();
				String bitrunUid = null2EmptyString(item[6]);
				
				currentDataNode = bitrunDP.addChild("ReportInfo");
				
				currentDataNode.addProperty("bharunUid", bharunUid);
				currentDataNode.addProperty("wellUid", item[1].toString());
				currentDataNode.addProperty("wellboreUid", item[2].toString());
				currentDataNode.addProperty("operationUid", item[3].toString());
								
				generateUomField(currentDataNode,Bharun.class,thisConverter,"depthInMdMsl",item[4],true);
				generateUomField(currentDataNode,Bharun.class,thisConverter,"depthOutMdMsl",item[5],true);
				
				currentDataNode.addProperty("bitrunUid", bitrunUid);
				currentDataNode.addProperty("bitrunNumber", null2EmptyString(item[7]));
				
				if (item[8]!=null){
					thisConverter.setReferenceMappingField(Bitrun.class, "bitDiameter");
					thisConverter.setBaseValueFromUserValue(Double.parseDouble(item[8].toString()));
					bitDiameter = thisConverter.getFormattedValue();
					
					currentDataNode.addProperty("bitDiameter", getLookupValue(bitSizeLookup,bitDiameter.replace(thisConverter.getUomSymbol(), "").trim()));
				}	
					
				currentDataNode.addProperty("make", getCompanyLookupValue(item[9]));
				currentDataNode.addProperty("model", null2EmptyString(item[10]));
				currentDataNode.addProperty("bitStatus", null2EmptyString(item[11]));
				currentDataNode.addProperty("bitType", getLookupValue(bitTypeLookup,item[12]));
				
				generateUomField(currentDataNode,Bitrun.class,thisConverter,"tfa",item[13],false);
				
				currentDataNode.addProperty("comment", null2EmptyString(item[14]));
				currentDataNode.addProperty("condFinalInner", null2EmptyString(item[15]));
				currentDataNode.addProperty("condFinalOuter", null2EmptyString(item[16]));
				currentDataNode.addProperty("condFinalDull", null2EmptyString(item[17]));
				currentDataNode.addProperty("condFinalLocation", null2EmptyString(item[18]));
				currentDataNode.addProperty("condFinalBearing", null2EmptyString(item[19]));
				currentDataNode.addProperty("condFinalGauge", null2EmptyString(item[20]));
				currentDataNode.addProperty("condFinalOther", null2EmptyString(item[21]));
				currentDataNode.addProperty("condFinalReason", null2EmptyString(item[22]));
				currentDataNode.addProperty("serialNumber", null2EmptyString(item[23]));
				//bharun_daily_summary
				String dailyUid="";
				
				sql = "Select A.dailyUid,A.hsi FROM BharunDailySummary A,Daily B WHERE A.dailyUid=B.dailyUid and A.bharunUid=:bharunUid and (A.isDeleted=false or A.isDeleted is null) " +
					  "and (B.isDeleted=false or B.isDeleted is null) order by B.dayDate";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"bharunUid", bharunUid);
				if (lstResult.size()>0){
					Object[] obj = (Object[])lstResult.get(0);
					
					dailyUid = obj[0].toString();
					generateUomField(currentDataNode,BharunDailySummary.class,thisConverter,"hsi",obj[1],false);
					//generateUomField(currentDataNode,BharunDailySummary.class,thisConverter,"duration",obj[2],false);
				}
				
				sql = "Select SUM(A.duration) AS totalDuration FROM BharunDailySummary A,Daily B WHERE A.dailyUid=B.dailyUid and A.bharunUid=:bharunUid and (A.isDeleted=false or A.isDeleted is null) " +
				  "and (B.isDeleted=false or B.isDeleted is null)";
			    lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"bharunUid", bharunUid);
			    if (lstResult.size()>0){
			    	//refer to @totalDuration of BharunDataNodeListener
			    	Object obj = (Object)lstResult.get(0);
			    	generateUomField(currentDataNode,BharunDailySummary.class,thisConverter,"duration",obj,false,true);
			    }
				
				//BitNozzle
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM BitNozzle WHERE bitrunUid=:bitrunUid and (isDeleted=false or isDeleted is null) order by nozzleNumber", "bitrunUid", bitrunUid);
				
				concatBitNozzle = "";
				for(Object data : lstResult){
									
					BitNozzle bitNozzle = (BitNozzle) data;
					
					if (!concatBitNozzle.equals("")) concatBitNozzle +=System.getProperty("line.separator");
					 
					 String nozzleSize="";
					 
					 if (bitNozzle.getNozzleSize()!=null) { 
						 DecimalFormat df = new DecimalFormat("#");
						 nozzleSize = df.format(bitNozzle.getNozzleSize());
					 }
					
					concatBitNozzle += null2EmptyString(bitNozzle.getNozzleQty()) + " x " + null2EmptyString(nozzleSize);
				}
				currentDataNode.addProperty("bitNozzle", concatBitNozzle);
				
				
				//drilling parameters							
				sql = "Select A.ropAvg,A.wobMin,A.wobAvg,A.wobMax,A.surfaceRpmMax,A.surfaceRpmAvg,A.surfaceRpmMin,A.flowMin,A.flowAvg,A.flowMax,A.pressureMin,A.pressureAvg,A.pressureMax,A.krevs " +
				      "FROM DrillingParameters A,Daily B WHERE A.dailyUid=B.dailyUid and A.bharunUid=:bharunUid and (A.isDeleted=false or A.isDeleted is null) and (B.isDeleted=false or B.isDeleted is null) order by B.dayDate";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"bharunUid", bharunUid);
				if (lstResult.size()>0){					
					Object[] obj = (Object[])lstResult.get(0);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"RopAvg",obj[0],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"WobMin",obj[1],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"WobAvg",obj[2],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"WobMax",obj[3],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"SurfaceRpmMax",obj[4],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"SurfaceRpmAvg",obj[5],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"SurfaceRpmMin",obj[6],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"FlowMin",obj[7],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"FlowAvg",obj[8],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"FlowMax",obj[9],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"PressureMin",obj[10],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"PressureAvg",obj[11],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"PressureMax",obj[12],false);
					generateUomField(currentDataNode,DrillingParameters.class,thisConverter,"Krevs",obj[13],false);
				}		
				
				//Mud Properties
				
				sql = "Select distinct A.dailyUid,A.mudType,A.mudWeight,A.mudFv,A.mudPv,A.mudYp FROM MudProperties A, BharunDailySummary B, Daily C " +
				      "WHERE A.dailyUid=B.dailyUid and A.dailyUid=C.dailyUid and B.bharunUid=:bharunUid and (A.isDeleted=false or A.isDeleted is null) " +
				      "and (B.isDeleted=false or B.isDeleted is null) order by C.dayDate";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"bharunUid", bharunUid);
				if (lstResult.size()>0){
					
					Object[] obj = (Object[])lstResult.get(0);			
					currentDataNode.addProperty("MudType", getLookupValue(mudTypeLookup,obj[1]));
					generateUomField(currentDataNode,MudProperties.class,thisConverter,"MudWeight",obj[2],false);
					generateUomField(currentDataNode,MudProperties.class,thisConverter,"MudFv",obj[3],false);
					generateUomField(currentDataNode,MudProperties.class,thisConverter,"MudPv",obj[4],false);
					generateUomField(currentDataNode,MudProperties.class,thisConverter,"MudYp",obj[5],false);
				}		
			}
		}
	}
	
	private String null2EmptyString(Object data){
		
		if (data ==null)
			return "";
		else
			return data.toString();
	}
	

	private void generateUomField(ReportDataNode node,Class refClass,CustomFieldUom thisConverter,String dataField, Object data,boolean isDatumField) throws Exception{
		generateUomField(node,refClass,thisConverter,dataField, data,isDatumField, false);
	}
	
	private void generateUomField(ReportDataNode node,Class refClass,CustomFieldUom thisConverter,String dataField, Object data,boolean isDatumField, boolean isBaseValue ) throws Exception{
		
		String rawValue = "";
		String dataUomSymbol = "";
		String formattedvalue = "";
		String displayValue="";
		
		thisConverter.setReferenceMappingField(refClass, dataField);
		if (thisConverter.isUOMMappingAvailable()) dataUomSymbol = thisConverter.getUomSymbol().toString();
		
		if (data != null)
		{
			if (thisConverter.isUOMMappingAvailable()){
				if (!isBaseValue) {
					thisConverter.setBaseValueFromUserValue(Double.parseDouble(data.toString()));
				}
				else {
					thisConverter.setBaseValue(Double.parseDouble(data.toString()));
				}
				if (isDatumField) thisConverter.addDatumOffset();
				rawValue =  Double.toString(thisConverter.getConvertedValue()) ;
				formattedvalue = thisConverter.getFormattedValue();	
				displayValue = formattedvalue.replace(dataUomSymbol, "").trim() ;
			}
			else{
				displayValue = data.toString();
			}			
		}				
		
		node.addProperty(dataField, displayValue);
		node.addProperty(dataField + "_RawValue", rawValue);
		node.addProperty(dataField + "UomSymbol", dataUomSymbol);		
		if (StringUtils.isNotBlank(formattedvalue))
			node.addProperty(dataField + "WithUomSymbol", formattedvalue);
		else
			node.addProperty(dataField + "WithUomSymbol", displayValue);
	}
	
	private String getLookupValue(Map<String,LookupItem> lookupList, Object lookupvalue) throws Exception {
		String retValue = "";
		
		if (lookupvalue !=null && StringUtils.isNotBlank(lookupvalue.toString())  && lookupList !=null){
			 LookupItem lookup = lookupList.get(lookupvalue.toString());
			if (lookup !=null)	retValue = lookup.getValue().toString();					
		} 
		
		return retValue;
	}
	
	private String getCompanyLookupValue(Object lookupvalue) throws Exception {
		String retValue = "";
		
		if (lookupvalue !=null && StringUtils.isNotBlank(lookupvalue.toString())){
			String sql = "SELECT a.companyName FROM LookupCompany a, LookupCompanyService b " +
			"WHERE a.lookupCompanyUid = b.lookupCompanyUid AND (a.isDeleted IS NULL OR a.isDeleted = '') " +
			"AND (b.isDeleted IS NULL OR b.isDeleted = '') AND b.code ='BITS' AND a.lookupCompanyUid =:lookupCompanyUid";	
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"lookupCompanyUid", lookupvalue.toString());
			if (lstResult.size()>0){
				retValue = (String)lstResult.get(0);
			}
		} 
		
		return retValue;
	}
}
