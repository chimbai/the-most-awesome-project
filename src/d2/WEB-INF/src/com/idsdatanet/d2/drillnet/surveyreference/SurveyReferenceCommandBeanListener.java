package com.idsdatanet.d2.drillnet.surveyreference;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.*;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.*;

public class SurveyReferenceCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {
	int isVSDZeroCount = 0;
	int isVSDNullCount = 0;
	private class SurveyStationComparator implements Comparator<CommandBeanTreeNode>{
		public int compare(CommandBeanTreeNode o1, CommandBeanTreeNode o2){
			try{
				Object in1 = o1.getData();
				Object in2 = o2.getData();
				
				SurveyStation f1 = (SurveyStation) in1;
				SurveyStation f2 = (SurveyStation) in2;
				
				if(f1.getDepthMdMsl() != null && f2.getDepthMdMsl() != null){
					return f1.getDepthMdMsl().compareTo(f2.getDepthMdMsl());
				}else if(f1.getDepthMdMsl() != null){
					return 1;
				}else if(f2.getDepthMdMsl() != null){
					return -1;
				}else{
					return 0;
				}
			}catch(Exception e){
				//e.printStackTrace();
				return 0;
			}
		}
	}
	
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		CustomFieldUom uom_depthTvdSubsea = new CustomFieldUom(commandBean, SurveyStation.class, "depthTvdMsl");
		if(uom_depthTvdSubsea.isUOMMappingAvailable()){
			commandBean.setCustomUOM(SurveyStation.class, "@depthTvdSubsea", uom_depthTvdSubsea.getUOMMapping(false));
			commandBean.setCustomUOM(SurveyStation.class, "@depthTvdMslSubsea", uom_depthTvdSubsea.getUOMMapping(false));
		}
		
		boolean isVSDValid = this.isSurveyVSDValid(commandBean, userSelection);
		if (!isVSDValid){
			commandBean.getSystemMessage().addWarning("The VSD set for All survey is different in this Wellbore");
		}
		if (isVSDNullCount > 0 && isVSDZeroCount > 0){
			commandBean.getSystemMessage().addWarning("One or more survey Vertical Section Direction is in zero degs or blank");
			isVSDZeroCount = 0;
			isVSDNullCount = 0;
		}else if(isVSDZeroCount > 0){
			commandBean.getSystemMessage().addWarning("One or more survey Vertical Section Direction is in zero degs");
			isVSDZeroCount = 0;
		}else if(isVSDNullCount > 0){
			commandBean.getSystemMessage().addWarning("One or more survey Vertical Section Direction is blank");
			isVSDNullCount = 0;
		}
	}


	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception
	{
		commandBean.getRoot().getDynaAttr().remove("allowContinue");
		boolean allowContinue = this.isStatusDuplicate(commandBean);
		
		if (!allowContinue)
		{
			commandBean.getSystemMessage().addError("The Survey Reference you intend to save contain more than 1 Current/Definitive or Both");
			commandBean.getRoot().getDynaAttr().put("allowContinue", "1");
		}
		
	}
	
	private boolean isSurveyVSDValid(CommandBean commandBean, UserSelectionSnapshot userSelection) throws Exception
	{
		int isSurveyVSDValid = 0;
		String[] params = {"thisWellboreUid"};
		Object[] values = {userSelection.getWellboreUid()};
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		List<SurveyReference> surveyReferenceList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM SurveyReference WHERE (isDeleted = false or isDeleted is null) and wellboreUid =:thisWellboreUid",params,values, qp); 
		boolean hasStatus = false;
		Double vsdValue = null;
		boolean isStarted = false;
		for (SurveyReference survey : surveyReferenceList)
		{
			if ("Current".equalsIgnoreCase(survey.getStatusIndicator()) || "Definitive".equalsIgnoreCase(survey.getStatusIndicator()))
				hasStatus = true;
			
			if(survey.getVerticalSectionAngle()!=null){
				if (!isStarted)
				{
					vsdValue = survey.getVerticalSectionAngle();
					isStarted=true;
				}else
				{
					if(vsdValue!=null){
						if (!vsdValue.equals(survey.getVerticalSectionAngle()))
							isSurveyVSDValid = isSurveyVSDValid + 1;
							//return false;
						else
							vsdValue = survey.getVerticalSectionAngle();
					}else{
						isSurveyVSDValid = isSurveyVSDValid + 1;
					}
				}
				
				if (survey.getVerticalSectionAngle() == 0)
					isVSDZeroCount = isVSDZeroCount + 1;
			}else{
				isVSDNullCount = isVSDNullCount + 1;
				
				if (!isStarted){
					isStarted=true;
					if(vsdValue!=null){
						// check if the previous VSD is null
						if (!vsdValue.equals(null))
							isSurveyVSDValid = isSurveyVSDValid + 1;
							//return false;
						else
							vsdValue = null;
					}
				}else{
					if(vsdValue!=null){
						// check if the previous VSD is null
						if (!vsdValue.equals(null))
							isSurveyVSDValid = isSurveyVSDValid + 1;
							//return false;
						else
							vsdValue = null;
					}
				}
			}
		}
		if (isSurveyVSDValid > 0)
			return false;
		else
			return true;
	}
	
	private boolean isStatusDuplicate(CommandBean commandBean) throws Exception
	{
		Collection<CommandBeanTreeNode> surveyReferenceList = commandBean.getRoot().getList().get(SurveyReference.class.getSimpleName()).values();
		int counter = 0;
		for ( CommandBeanTreeNode nodeReference : surveyReferenceList)
		{
			if (nodeReference.getAtts().getAction().equalsIgnoreCase(Action.SAVE))
			{
				SurveyReference survey = (SurveyReference) nodeReference.getData();
				if ("Current".equalsIgnoreCase(survey.getStatusIndicator()) || "Definitive".equalsIgnoreCase(survey.getStatusIndicator()))
				{
					counter++;
				}
			}
		}
		return counter<=1;
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		UserSession session = UserSession.getInstance(request);
		//this.setCurrentStatusIndicator(session.getCurrentWellboreUid());
	}
	/*
	private void setCurrentStatusIndicator(String wellboreUid) throws Exception {
		if (wellboreUid==null) return;
		String surveyUid = getCurrentSurveyUid(wellboreUid);
		if (surveyUid !=null)
		{
			String queryString = "FROM SurveyReference WHERE (isDeleted=false or isDeleted is null) and wellboreUid =:wellboreUid";
			List<SurveyReference> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "wellboreUid", wellboreUid);
			for (SurveyReference rec : rs) {
				if (surveyUid.equals(rec.getSurveyReferenceUid())) {
					rec.setStatusIndicator("Current");
				} else {
					rec.setStatusIndicator("Definitive");
				}
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
			}
		}
		
	}
	
	public String getCurrentSurveyUid(String wellboreUid) throws Exception {
		Date lastEditDate = null;
		String currentSurveyReferenceUid = null;

		String queryString = "FROM SurveyReference " +
				"where (isDeleted=false or isDeleted is null) " +
				"and wellboreUid=:wellboreUid " +
				"and (statusIndicator='Current' and statusIndicator is not null)"; //check active
		List<SurveyReference> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "wellboreUid", wellboreUid);
		if (rs.size()==1) {
			//only one active plan
			SurveyReference rec = rs.get(0);
			currentSurveyReferenceUid = rec.getSurveyReferenceUid();
		} else if (rs.size()>1) {
			//move than 1 active plan
			for (SurveyReference rec: rs) {
				if (lastEditDate==null) lastEditDate = rec.getLastEditDatetime();
				if (currentSurveyReferenceUid==null) currentSurveyReferenceUid = rec.getSurveyReferenceUid();
				if (rec.getLastEditDatetime().compareTo(lastEditDate)>0) {
					lastEditDate = rec.getLastEditDatetime();
					currentSurveyReferenceUid = rec.getSurveyReferenceUid();
				}
				
				//check detail
				Date detailDate = this.surveyStationGreaterDate(rec.getSurveyReferenceUid(), lastEditDate);
				if (detailDate.compareTo(lastEditDate) > 0) {
					lastEditDate = detailDate;
					currentSurveyReferenceUid = rec.getSurveyReferenceUid();
				}
			}
		} else {
			//inactive plans
			queryString = "FROM SurveyReference " +
					"where (isDeleted=false or isDeleted is null) " +
					"and wellboreUid=:wellboreUid " +
					"and (statusIndicator='Definitive' or statusIndicator is null or statusIndicator='')";
			rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "wellboreUid", wellboreUid);
			if (rs.size()==1) {
				//only one inactive plan
				SurveyReference rec = rs.get(0);
				currentSurveyReferenceUid = rec.getSurveyReferenceUid();
			} else if (rs.size()>1) {
				//move than 1 inactive plan
				for (SurveyReference rec: rs) {
					if (lastEditDate==null) lastEditDate = rec.getLastEditDatetime();
					if (currentSurveyReferenceUid==null) currentSurveyReferenceUid = rec.getSurveyReferenceUid();
					if (rec.getLastEditDatetime().compareTo(lastEditDate)>0) {
						lastEditDate = rec.getLastEditDatetime();
						currentSurveyReferenceUid = rec.getSurveyReferenceUid();
					}
					
					//check detail
					Date detailDate = this.surveyStationGreaterDate(rec.getSurveyReferenceUid(), lastEditDate);
					if (detailDate.compareTo(lastEditDate) > 0) {
						lastEditDate = detailDate;
						currentSurveyReferenceUid = rec.getSurveyReferenceUid();
					}
				}
			}
		}
		return currentSurveyReferenceUid;
	}
	
	private Date surveyStationGreaterDate(String surveyReferenceUid, Date currentDate) throws Exception {
		String queryString = "FROM SurveyStation where (isDeleted=false or isDeleted is null) and surveyReferenceUid=:surveyReferenceUid order by lastEditDatetime DESC";
		List<SurveyStation> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "surveyReferenceUid", surveyReferenceUid);
		for (SurveyStation rec : rs) {
			if (rec.getLastEditDatetime().compareTo(currentDate) > 0) {
				currentDate = rec.getLastEditDatetime(); 
			}		
		}
		return currentDate;
	}
	*/
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		// to do calculate survey station data 
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition()!=null)
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(SurveyReference.class)) {
				commandBean.getFlexClientAdaptor().setReloadAfterPageCancel();
				
				SurveyReferenceUtils.calculateMinimumCurvatureMethod(targetCommandBeanTreeNode);
			}	
		}
	}
}
