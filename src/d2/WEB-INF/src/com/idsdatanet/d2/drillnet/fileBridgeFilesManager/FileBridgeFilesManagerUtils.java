package com.idsdatanet.d2.drillnet.fileBridgeFilesManager;

import java.sql.Connection;
import java.sql.Statement;
import java.util.LinkedHashMap;

import javax.sql.DataSource;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.SystemExceptionHandler;

public class FileBridgeFilesManagerUtils extends DefaultBeanSupport {
	private DataSource dataSource = null;
	private FileBridgeFilesArchive archive = null;
	private Log log = LogFactory.getLog(this.getClass());
	public static FileBridgeFilesManagerUtils configuredInstance = null;
	
	public void setDataSource(DataSource dataSource){
		this.dataSource = dataSource;
	}
	
	public void setArchive(FileBridgeFilesArchive fileBridgeFilesArchive){
		this.archive = fileBridgeFilesArchive;
	}
	
	public FileBridgeFilesArchive getArchive(){
		if(this.archive == null) this.archive = new FileBridgeFilesArchive();
		return this.archive;
	}
	
	public static FileBridgeFilesManagerUtils getConfiguredInstance() throws Exception {
		if(configuredInstance == null) configuredInstance = getSingletonInstance(FileBridgeFilesManagerUtils.class); 
		return configuredInstance;
	}
	
	public CachedRowSet query(String query, QueryProperties qp) throws Exception{
		Connection connection = null; 
		Statement statement = null;
		RowSetFactory factory = RowSetProvider.newFactory();
		CachedRowSet rowset = factory.createCachedRowSet();
		
		try {
			if (this.dataSource == null){
				throw new Exception("Database connection NOT found");					
			}
			
			if(qp != null) {
				query += " LIMIT " + qp.getFetchStartRow() + ", " + qp.getFetchTotalRows();
			}
			//final long startTime = System.currentTimeMillis();
			connection = this.dataSource.getConnection();
			statement = connection.createStatement();
			rowset.populate(statement.executeQuery(query));
			//final long endTime = System.currentTimeMillis();
			//System.out.println("QUERY: " + (endTime - startTime));
		}catch (Exception e) {
			log.error(SystemExceptionHandler.printAsString(e));			
		}finally{
			if (connection !=null) connection.close();
			if (statement !=null) statement.close();
		}
		
		return rowset;
	}
	
	public static LinkedHashMap<String, LookupItem> getWellOpsLookupSet() {
		return null;
	}
	
	public static LinkedHashMap<String, LookupItem> getParserKeyLookupSet() {
		return null;
	}
	
	/*public static LinkedHashMap<String, LookupItem> getStatusFilterLookupSet() {
		return null;
	}*/
}
