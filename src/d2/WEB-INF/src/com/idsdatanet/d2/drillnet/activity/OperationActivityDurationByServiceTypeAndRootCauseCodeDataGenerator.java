package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

import edu.emory.mathcs.backport.java.util.Collections;


public class OperationActivityDurationByServiceTypeAndRootCauseCodeDataGenerator implements ReportDataGenerator {
	public int numberTop;
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void setNumberTop(int numberTop)
	{
		if(numberTop != 0){
			this.numberTop = numberTop;
		}
	}
	
	public int getNumberTop() {
		return this.numberTop;
	}
	
	//@SuppressWarnings("unused")
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
		String operationUid = userContext.getUserSelection().getOperationUid();
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String[] paramsFields = {"userDate", "operationUid"};
		Object[] paramsValues = {todayDate, operationUid};
			
			String strSql = "SELECT a.activityUid, a.classCode, a.internalClassCode, a.rootCauseCode, a.lookupCompanyUid, a.activityDuration FROM Activity a, Daily d " +
					"WHERE (a.isDeleted is null or a.isDeleted = false) AND (d.isDeleted is null or d.isDeleted = false) " +
					"AND a.operationUid = :operationUid AND d.dailyUid = a.dailyUid AND d.dayDate <= :userDate " +
					"AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL)) " +
					"GROUP BY a.activityUid ORDER BY a.startDatetime, a.endDatetime";
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			
			String activityRecord = null;
			String classCode = null;
			String internalCode = null;
			String rootCauseCode = null;
			String companyUid = null;
			String category = null;
			double duration = 0.0;
			String unwantedEventUid = null;
			String eventRef = null;
			List<Object> CategoryList = new ArrayList();
			List<Object> EventList = new ArrayList();
			
			if (lstResult.size() > 0){

				for (int i=0; i<lstResult.size(); i++){	
					Object[] obj = (Object[]) lstResult.get(i);
					Object[] objTemp = new Object[10];
					boolean categorised = false;
					
					activityRecord = (obj[0]!=null)?obj[0].toString():"";
					classCode = (obj[1]!=null)?obj[1].toString():"";
					internalCode = (obj[2]!=null)?obj[2].toString():"";
					rootCauseCode = (obj[3]!=null)?obj[3].toString():"";
					companyUid = (obj[4]!=null)?obj[4].toString():"";
					if (obj[5]!=null && StringUtils.isNotBlank(obj[5].toString())){
						duration = Double.parseDouble(obj[5].toString());
						CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
						thisConverter.setBaseValue(duration);
						duration = thisConverter.getConvertedValue();
					}else
						duration = 0.0;
						
					String[] field = {"activityRecord"};
					Object[] values = {activityRecord};
							
					if (internalCode!=null && StringUtils.isNotBlank(internalCode)){
						if (rootCauseCode!=null && StringUtils.isNotBlank(rootCauseCode)){
							if(!categorised){
								if("TP".equalsIgnoreCase(internalCode) || "TU".equalsIgnoreCase(internalCode) || "IP".equalsIgnoreCase(internalCode) || "IU".equalsIgnoreCase(internalCode)){	
									if("ME".equalsIgnoreCase(rootCauseCode) || "WW".equalsIgnoreCase(rootCauseCode)){
										category = "NPT Weather";
										categorised = true;
									}
								}
							}
							if("TP".equalsIgnoreCase(internalCode) || "TU".equalsIgnoreCase(internalCode)){
								String[] fieldCompany = {"lookupCompanyUid"};
								Object[] valuesCompany = {companyUid};
								if(!categorised){
									if("HC".equalsIgnoreCase(rootCauseCode) || "LC".equalsIgnoreCase(rootCauseCode) || "WC".equalsIgnoreCase(rootCauseCode)){
										String strUncontrollable = "SELECT companyType FROM LookupCompany WHERE (isDeleted is null or isDeleted = false) AND lookupCompanyUid = :lookupCompanyUid";
										List<String> lstUncontrollable = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strUncontrollable, fieldCompany, valuesCompany, qp);
										if (lstUncontrollable.size() > 0){
											for(String objUncontrollable: lstUncontrollable){
												String companyType = (objUncontrollable!=null)?objUncontrollable.toString():"";
												if(!"operating_company".equalsIgnoreCase(companyType)){
													category = "NPT Uncontrollable";
													categorised = true;
												}
											}
										}else{
											category = "NPT Uncontrollable";
											categorised = true;
										}
									}
								}
								if(!categorised){
									String strCompanyType = "SELECT companyType FROM LookupCompany WHERE (isDeleted is null or isDeleted = false) AND lookupCompanyUid = :lookupCompanyUid";
									List<String> lstCompanyType = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strCompanyType, fieldCompany, valuesCompany, qp);
									if (lstCompanyType.size() > 0){		
										for(String objCompanyType: lstCompanyType){
											String companyType = (objCompanyType!=null)?objCompanyType.toString():"";
											if("rig_contractor".equalsIgnoreCase(companyType)){
												category = "NPT Rig Contractor";
												categorised = true;
											}else if("operating_company".equalsIgnoreCase(companyType)){
												category = "NPT INPEX";
												categorised = true;
											}else if("service_company".equalsIgnoreCase(companyType)){
												category = "NPT Service Contractor";
												categorised = true;
											}
										}
									}
								}
							}
						}
						if(!categorised){
							if("P".equalsIgnoreCase(internalCode) || "U".equalsIgnoreCase(internalCode) || "IP".equalsIgnoreCase(internalCode) || "IU".equalsIgnoreCase(internalCode)){
								category = "Productive";
								categorised = true;
							}
						}	
					}
							
					if(!categorised){
						category = "NPT Others";
						categorised = true;
					}
							
						String strNptEvent = "SELECT unwantedEventUid, eventRef FROM UnwantedEvent WHERE unwantedEventUid IN (SELECT nptEventUid FROM Activity WHERE activityUid =:activityRecord)";
						List lstResultNptEvent = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strNptEvent, field, values, qp);
						if (lstResultNptEvent.size() > 0){
							for(Object objNptEvent: lstResultNptEvent){
								Object[] objEvent = (Object[]) objNptEvent;
								unwantedEventUid = (objEvent[0]!=null)?objEvent[0].toString():"";
								eventRef = (objEvent[1]!=null)?objEvent[1].toString():"";
							}
						}else{
							unwantedEventUid = "";
							eventRef = "";
						}
								
							objTemp[0] = activityRecord;
							objTemp[1] = rootCauseCode;
							objTemp[2] = duration;
							objTemp[3] = category;
							if (unwantedEventUid!=null){
								objTemp[4] = unwantedEventUid;
								objTemp[5] = eventRef;
							}
							CategoryList.add(objTemp);
							EventList.add(objTemp);
				}
				
				
				Collections.sort(CategoryList, new CategoryComparator());
	
				double sumDuration = 0.0;
				double categoryDuration = 0.0;
				List<Object> removeList = new ArrayList();
				List<Object> EventToBeGroupTemp = new ArrayList();
				List<Object> EventCategoryList = new ArrayList();
				
				if(CategoryList.size()>0){
					for (int current=0; current<CategoryList.size(); current++){
						Object[] currentObj = (Object[]) CategoryList.get(current);
							category = (currentObj[3]!=null)?currentObj[3].toString():"";
							categoryDuration = (currentObj[2]!=null)?Double.parseDouble(currentObj[2].toString()):0.0;
							sumDuration = categoryDuration;
							for (int next=current+1; next<CategoryList.size(); next++){
								Object[] nextObj = (Object[]) CategoryList.get(next);
								if(nextObj[3].equals(currentObj[3])){
									category = (nextObj[3]!=null)?nextObj[3].toString():"";
									categoryDuration = (nextObj[2]!=null)?Double.parseDouble(nextObj[2].toString()):0.0;
									sumDuration += categoryDuration;
									removeList.add(nextObj);
								}
								if(next==CategoryList.size()-1){
									for(Object objRemove: removeList){
										Object[] remove = (Object[]) objRemove;
										CategoryList.remove(remove);
									}
								}
							}
						ReportDataNode thisReportNode = reportDataNode.addChild("cumTimeBreakdownPerCategory");
						thisReportNode.addProperty("category", category);
						thisReportNode.addProperty("sumDuration", Double.toString(sumDuration));
							CategoryList.remove(currentObj);
							current=-1;
							removeList.clear();
					}
				}
				
				String eventUid = null;
				String eventName = null;
				if(EventList.size()>0){
					for (int current=0; current<EventList.size(); current++){
						Object[] currentObj = (Object[]) EventList.get(current);
							categoryDuration = (currentObj[2]!=null)?Double.parseDouble(currentObj[2].toString()):0.0;
							category = (currentObj[3]!=null)?currentObj[3].toString():"";
							eventUid = (currentObj[4]!=null)?currentObj[4].toString():"";
							eventName = (currentObj[5]!=null)?currentObj[5].toString():"";
							sumDuration = categoryDuration;
							if(eventUid!=null && StringUtils.isNotBlank(eventUid)){
								for (int next=current+1; next<EventList.size(); next++){
									Object[] nextObj = (Object[]) EventList.get(next);
									if(next!=current){
										if(nextObj[3].equals(currentObj[3]) && nextObj[4].equals(currentObj[4])){
											categoryDuration = (nextObj[2]!=null)?Double.parseDouble(nextObj[2].toString()):0.0;
											sumDuration += categoryDuration;
											removeList.add(nextObj);
										}
									}
								}
								Object[] objTemp = new Object[4];
								objTemp[0] = eventUid;
								objTemp[1] = eventName;
								objTemp[2] = category;
								objTemp[3] = sumDuration;
								EventToBeGroupTemp.add(objTemp);
								EventCategoryList.add(objTemp);
									ReportDataNode thisReportNode = reportDataNode.addChild("nptEventsTimeBreakdown");
									thisReportNode.addProperty("nptEventUid", eventUid);
									thisReportNode.addProperty("nptEvent", eventName);
									thisReportNode.addProperty("category", category);
									thisReportNode.addProperty("sumDuration", Double.toString(sumDuration));
							}
							EventList.remove(currentObj);
							for(Object objRemove: removeList){
								Object[] remove = (Object[]) objRemove;
								EventList.remove(remove);
							}
							current=-1;
							removeList.clear();
					}
				}
				
				double durationPerEvent = 0.0;
				String currentEventUid = null;
				String nextEventUid = null;
				List<Object> SumEvent = new ArrayList();
				List<Object> event = new ArrayList();
				
				if(EventToBeGroupTemp.size()>0){
					for (int current=0; current < EventToBeGroupTemp.size(); current++){
						Object[] currentObj = (Object[]) EventToBeGroupTemp.get(current);
							currentEventUid = (currentObj[0]!=null)?currentObj[0].toString():"";
							durationPerEvent = (currentObj[3]!=null)?Double.parseDouble(currentObj[3].toString()):0.0;
							for (int next=current+1; next < EventToBeGroupTemp.size(); next++){
								Object[] nextObj = (Object[]) EventToBeGroupTemp.get(next);
								nextEventUid = (nextObj[0]!=null)?nextObj[0].toString():"";
								if(current!=next){
									if(nextEventUid.equals(currentEventUid)){
										double nextDuration = (nextObj[3]!=null)?Double.parseDouble(nextObj[3].toString()):0.0;
										durationPerEvent += nextDuration;
										removeList.add(nextObj);
									}
								}
							}
							Object[] temp = new Object[2];
							temp[0] = currentEventUid;
							temp[1] = durationPerEvent;
							SumEvent.add(temp);
						
							EventToBeGroupTemp.remove(currentObj);
								for(Object objRemove: removeList){
									Object[] remove = (Object[]) objRemove;
									EventToBeGroupTemp.remove(remove);
								}
							current=-1;
							removeList.clear();
					}
				}
					
					
				Collections.sort(SumEvent, Collections.reverseOrder(new ItemLocationComparator()));
				
				int numberTop = 0;
				if(SumEvent.size()>this.getNumberTop())
					numberTop = this.getNumberTop();
				else
					numberTop = 5;
				
				int top5Count = 0;
				String nptEventName = null;
				String nptCategoryName = null;
				double nptCategoryDuration = 0.0;
				double otherEventDuration = 0.0;
				double totalOtherDuration = 0.0;
				double totalDurationPerEvent = 0.0;
				
				Object[] maxDuration = null;
				
				if(SumEvent.size()>0){
					maxDuration = (Object[]) SumEvent.get(0);
					for (int i=0; i < SumEvent.size(); i++){
						Object[] obj1 = (Object[]) SumEvent.get(i);
							String nptEventUid1 = (obj1[0]!=null)?obj1[0].toString():"";
							totalDurationPerEvent = (obj1[1]!=null)?Double.parseDouble(obj1[1].toString()):0.0;
							top5Count++;
							if(top5Count>numberTop){
								otherEventDuration = (obj1[1]!=null)?Double.parseDouble(obj1[1].toString()):0.0;
								totalOtherDuration += otherEventDuration;
								if(i==SumEvent.size()-1){
									Object[] objTemp = new Object[5];
									objTemp[0] = "";
									objTemp[1] = "Other";
									objTemp[2] = totalOtherDuration;
									objTemp[3] = "CategoryOthers";
									objTemp[4] = totalOtherDuration;
									event.add(objTemp);
								}
							}else if(top5Count<=numberTop){
								for (int j=0; j < EventCategoryList.size(); j++){
									Object[] obj2 = (Object[]) EventCategoryList.get(j);
										String nptEventUid2 = (obj2[0]!=null)?obj2[0].toString():"";
										if(nptEventUid2.equals(nptEventUid1)){
											nptEventName = (obj2[1]!=null)?obj2[1].toString():"";
											nptCategoryName = (obj2[2]!=null)?obj2[2].toString():"";
											nptCategoryDuration = (obj2[3]!=null)?Double.parseDouble(obj2[3].toString()):0.0;
												Object[] objTemp = new Object[5];
												objTemp[0] = nptEventUid1;
												objTemp[1] = nptEventName;
												objTemp[2] = totalDurationPerEvent;
												objTemp[3] = nptCategoryName;
												objTemp[4] = nptCategoryDuration;
												event.add(objTemp);
										}
								}
							}
					}
					for(Object e: event){
						Object[] objEvent = (Object[]) e;
						ReportDataNode thisReportNode = reportDataNode.addChild("totalNptEventsTimeBreakdown");
						thisReportNode.addProperty("nptEventUid", objEvent[0].toString());
						thisReportNode.addProperty("nptEvent", objEvent[1].toString());
						thisReportNode.addProperty("totalDurationByEvent", objEvent[2].toString());
						thisReportNode.addProperty("category", objEvent[3].toString());
						thisReportNode.addProperty("sumDuration", objEvent[4].toString());
						thisReportNode.addProperty("maxDuration",maxDuration[1].toString());
					}
				}
			}
		
		Double cumCost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(userContext.getUserSelection().getDailyUid(), "DDR");
		
		String strSqlActivityDuration = "SELECT a.classCode, SUM(a.activityDuration) FROM Activity a, Daily d " +
				"WHERE (a.isDeleted is null or a.isDeleted = false) " +
				"AND (d.isDeleted is null or d.isDeleted = false) " +
				"AND a.operationUid = :operationUid " +
				"AND d.dailyUid = a.dailyUid " +
				"AND d.dayDate = :userDate " +
				"AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL))";
		
		List<Object> lstResultActivityDuration = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlActivityDuration, paramsFields, paramsValues, qp);
		
		double todayDuration = 0.0;
		
		if (lstResultActivityDuration.size() > 0){				
			for(Object result: lstResultActivityDuration){
				Object[] objDuration = (Object[]) result;
				if (objDuration[1]!=null && StringUtils.isNotBlank(objDuration[1].toString())){
					todayDuration = Double.parseDouble(objDuration[1].toString());
					CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
					thisConverter.setBaseValue(todayDuration);
					todayDuration = thisConverter.getConvertedValue();
				}
				ReportDataNode thisReportNodeTotal = reportDataNode.addChild("dailyTimeBreakdown");
				thisReportNodeTotal.addProperty("todayDuration", Double.toString(todayDuration));
			}
		}
		
		
		String strSqlTotal = "SELECT a.classCode, SUM(a.activityDuration) FROM Activity a, Daily d " +
				"WHERE (a.isDeleted is null or a.isDeleted = false) " +
				"AND (d.isDeleted is null or d.isDeleted = false) " +
				"AND a.operationUid = :operationUid " +
				"AND d.dailyUid = a.dailyUid " +
				"AND d.dayDate <= :userDate " +
				"AND ((a.dayPlus IS NULL OR a.dayPlus=0) AND (a.carriedForwardActivityUid IS NULL OR a.carriedForwardActivityUid='') AND (a.isSimop <> 1 OR a.isSimop IS NULL) AND (a.isOffline <> 1 OR a.isOffline IS NULL))";
		
		List<Object> lstResultTotal = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlTotal, paramsFields, paramsValues, qp);
		
		double totalDuration = 0.0;
	
		if (lstResultTotal.size() > 0){				
			for(Object result: lstResultTotal){
				Object[] objDuration = (Object[]) result;
				if (objDuration[1]!=null && StringUtils.isNotBlank(objDuration[1].toString())){
					totalDuration = Double.parseDouble(objDuration[1].toString());
					CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
					thisConverter.setBaseValue(totalDuration);
					totalDuration = thisConverter.getConvertedValue();
				}
				ReportDataNode thisReportNodeTotal = reportDataNode.addChild("totalTimeBreakdown");
				thisReportNodeTotal.addProperty("totalDuration", Double.toString(totalDuration));
				if(cumCost!=null){
					thisReportNodeTotal.addProperty("cumCost", Double.toString(cumCost));
				}
			}
		}
		
	}
	
	private class ItemLocationComparator implements Comparator<Object[]>{
		@Override
		public int compare(Object[] o1, Object[] o2){
			// TODO Auto-generated method stub
			try{
				double in1 = (double) o1[1];
				double in2 = (double) o2[1];
				return Double.compare(in1,in2);
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
			
		}
	}
	
	private class CategoryComparator implements Comparator<Object[]>{
		@Override
		public int compare(Object[] o1, Object[] o2){
			// TODO Auto-generated method stub
			try{
				String in1 = o1[3].toString();
				String in2 = o2[3].toString();
				return in1.compareTo(in2);
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}

}

		
