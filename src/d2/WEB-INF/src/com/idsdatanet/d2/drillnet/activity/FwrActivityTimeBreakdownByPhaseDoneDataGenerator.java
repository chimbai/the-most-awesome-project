package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class FwrActivityTimeBreakdownByPhaseDoneDataGenerator implements ReportDataGenerator {
	private Map<String, String> phaseCodeList = null;
	private Map<String, String> classCodeList = null;
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		
		this.phaseCodeList = new HashMap();
		this.classCodeList = new HashMap();
		
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		this.phaseCodeList = this.getPhaseCodeList(operation);
		this.classCodeList = this.getClassCodeList(operation);
		
		this.generateTimeBreakdownByPhaseDone(userContext, reportDataNode);

		
	}

	public void generateTimeBreakdownByPhaseDone(UserContext userContext, ReportDataNode reportDataNode) throws Exception{
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
			
		Double activityDuration;
		String phaseCode;
		String phaseCodeName;
		String classCode;
		String classCodeName;				
	
		String strSql = "SELECT SUM(coalesce(a.activityDuration,0.0)) AS actDuration, a.phaseCode, a.classCode FROM Activity a WHERE a.operationUid = :thisOperationUid AND (a.isDeleted = false OR a.isDeleted is null) AND (a.carriedForwardActivityUid = '' OR a.carriedForwardActivityUid IS NULL) AND (a.isSimop = '' OR a.isSimop IS NULL) AND (a.isOffline = '' OR a.isOffline IS NULL) AND a.dailyUid IN (SELECT dailyUid FROM Daily d WHERE d.operationUid = :thisOperationUid AND (d.isDeleted='' OR d.isDeleted IS NULL)) GROUP BY a.phaseCode, a.classCode order by a.startDatetime DESC";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){				
			for(int i = 0; i < lstResult.size(); i++ ){
				Object[] activityRecord = (Object[]) lstResult.get(i);
				phaseCode ="";
				phaseCodeName ="";
				classCode ="";
				classCodeName ="";
				activityDuration = 0.0;
				
				if (activityRecord[0] != null && StringUtils.isNotBlank(activityRecord[0].toString())){
					activityDuration = Double.parseDouble(activityRecord[0].toString());
				}
				
				if (activityRecord[1] != null && StringUtils.isNotBlank(activityRecord[1].toString())) phaseCode = activityRecord[1].toString();
				if (this.phaseCodeList.containsKey(phaseCode)) phaseCodeName = this.phaseCodeList.get(phaseCode);
				
				if (activityRecord[2] != null && StringUtils.isNotBlank(activityRecord[2].toString())) classCode = activityRecord[2].toString();
				if (this.classCodeList.containsKey(classCode)) classCodeName = this.classCodeList.get(classCode);

					//Convert to hours
					activityDuration = activityDuration/3600;
					ReportDataNode thisReportNode = reportDataNode.addChild("Activity");
					thisReportNode.addProperty("activityDuration", activityDuration.toString());
					thisReportNode.addProperty("phaseCode", phaseCode);
					thisReportNode.addProperty("phaseCodeName", phaseCodeName);
					thisReportNode.addProperty("classCode", classCode);
					thisReportNode.addProperty("classCodeName", classCodeName);
			}
		}
	}
	
	public Map<String, String> getPhaseCodeList(Operation operation) throws Exception{
		String operationCode = "";
		if(StringUtils.isNotBlank(operation.getOperationCode())) operationCode = operation.getOperationCode();
		
		List<Object[]> listPhaseCodeResults = new ArrayList<Object[]>();
		Map<String, String> phaseCodeList = new HashMap();
		
		String[] paramsFields = {"operationCode"};		
 		Object[] paramsValues = {operationCode};
		
		String strSql = "SELECT lpc.shortCode, lpc.name FROM LookupPhaseCode lpc WHERE (lpc.isDeleted = false or lpc.isDeleted is null) " 
						+ " AND (lpc.operationCode = :operationCode or lpc.operationCode='') ORDER BY name";
		listPhaseCodeResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (listPhaseCodeResults.size() > 0) {
			for (Object[] listPhaseCodeResult : listPhaseCodeResults) {
				if (listPhaseCodeResult[0] != null && listPhaseCodeResult[1] != null) {
					
					String key = listPhaseCodeResult[0].toString();
					String value = listPhaseCodeResult[1].toString() + " (" + listPhaseCodeResult[0].toString() + ")";			
					
					phaseCodeList.put(key, value);
					
				}
			}
		}
		
		return phaseCodeList;
	}

	public Map<String, String> getClassCodeList(Operation operation) throws Exception{
		String operationCode = "";
		if(StringUtils.isNotBlank(operation.getOperationCode())) operationCode = operation.getOperationCode();
		
		List<Object[]> listClassCodeResults = new ArrayList<Object[]>();
		Map<String, String> classCodeList = new HashMap();
		
		String[] paramsFields = {"operationCode"};		
 		Object[] paramsValues = {operationCode};
		
		String strSql = "SELECT lcc.shortCode, lcc.name FROM LookupClassCode lcc WHERE (lcc.isDeleted = false or lcc.isDeleted is null) "
						+ " AND (lcc.operationCode = :operationCode or lcc.operationCode='') ORDER BY name";
		listClassCodeResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (listClassCodeResults.size() > 0) {
			for (Object[] listClassCodeResult : listClassCodeResults) {
				if (listClassCodeResult[0] != null && listClassCodeResult[1] != null) {
					
					String key = listClassCodeResult[0].toString();
					String value = listClassCodeResult[1].toString() + " (" + listClassCodeResult[0].toString() + ")";
							
					classCodeList.put(key, value);
					
				}
			}
		}
		
		return classCodeList;
	}
	
}
