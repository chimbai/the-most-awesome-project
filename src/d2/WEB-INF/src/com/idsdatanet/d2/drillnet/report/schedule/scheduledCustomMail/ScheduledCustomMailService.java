package com.idsdatanet.d2.drillnet.report.schedule.scheduledCustomMail;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CostAfeMaster;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.GeneralComment;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.costnet.CostNetConstants;
import com.idsdatanet.d2.drillnet.report.DDRReportModule;
import com.idsdatanet.d2.drillnet.report.ReportJobParams;
import com.idsdatanet.d2.drillnet.report.ReportJobResult;
import com.idsdatanet.d2.drillnet.report.schedule.EmailConfiguration;
import com.idsdatanet.d2.drillnet.report.schedule.corportatedrillingportal.BaseDataTransferService;
import com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.DailyReportModuleResolver;

public class ScheduledCustomMailService extends BaseDataTransferService{
	
	private static final String REPORT_DATETIME="{reportDate}";
	private static final String OPERATION="{operationName}";
	private static final String REPORT_NUMBER="{reportNumber}";
	
	public static final int SUCCESS=1;
	public static final int ERROR=-1;
	
	private String reportDateFormat="dd MMM yyyy";
	private String reportDateTimeFormat="dd MMM yyyy HH:mm";
	
	private DDRReportModule ddrReportModule;
	private boolean isRunning=false;
	
	private Boolean includeAllOperationsWithoutAFE = false;
	private String daysFromSpudByDateCalcType = null;
	private List<String> includeSpecificHseIncidentCategory = null;
	
	public void setDaysFromSpudByDateCalcType(String daysFromSpudByDateCalcType)
	{
		this.daysFromSpudByDateCalcType = daysFromSpudByDateCalcType;
	}
	
	public String getDaysFromSpudByDateCalcType()
	{
		return daysFromSpudByDateCalcType;
	}
	
	public void setDdrReportModule(DDRReportModule ddrReportModule) {
		this.ddrReportModule = ddrReportModule;
	}

	public DDRReportModule getDdrReportModule() {
		return ddrReportModule;
	}

	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void setReportDateFormat(String reportDateFormat) {
		this.reportDateFormat = reportDateFormat;
	}

	public String getReportDateFormat() {
		return reportDateFormat;
	}
	
	public synchronized void run() throws Exception
	{
		if (this.getEnabled())
		{
			this.setRunning(true);
			List<Daily> list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(new Date());
			if (list != null) {
				if (!this.getIncludeAllOperationsWithoutAFE()) {
					for(Daily day:list)
					{
						Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
						if (StringUtils.isNotBlank(operation.getOpCo())) {
							LookupCompany lookupCompany=(LookupCompany)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, operation.getOpCo());
							if (lookupCompany != null) {
								if (lookupCompany.getAutoEmailFlag() !=null && lookupCompany.getAutoEmailFlag()) { 
									this.runExtractionAndSendEmail(day,null);
								}
							}
						}
					}
				}
				else {
					//for Wintershall, all operations in 1 mail, no AFE info, should always be only 1 day date from all dailies
					EmailConfiguration emailConfig=new EmailConfiguration(this.getValidEmailConfiguration());
					emailConfig.clearContentMapping();
					Map<String, File> attachment = new HashMap<String,File>();
					Map<String,Object> all = new HashMap<String,Object>();
					String subject = "";
					ArrayList<String> operationNameList = new ArrayList<String>();
					ArrayList<String> reportNumberList = new ArrayList<String>();
					ArrayList<String> reportDateList = new ArrayList<String>();
					int c = 0;
					for(Daily day:list)
					{
						Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
						if (StringUtils.isNotBlank(operation.getOpCo())) {
							LookupCompany lookupCompany=(LookupCompany)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, operation.getOpCo());
							if (lookupCompany != null) {
								if (lookupCompany.getAutoEmailFlag() !=null && lookupCompany.getAutoEmailFlag()) {
									DDRReportModule module = DailyReportModuleResolver.getConfiguredInstance().getReportModuleByOperationType(operation.getOperationCode());
									ReportJobResult reportJobResult = this.runExtraction(day, module);
									if (reportJobResult != null && !reportJobResult.isError()) {
										
										Well well=ApplicationUtils.getConfiguredInstance().getCachedWell(day.getWellUid());
										String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(day.getOperationUid());
										ReportDaily thisReportDaily = this.reportDailyList(day.getOperationUid(), reportType, day.getDailyUid());
										
										subject = formatDate(thisReportDaily.getReportDatetime(),reportDateFormat);
										operationNameList.add(this.getCompleteOperationName(operation));
										reportNumberList.add(thisReportDaily.getReportNumber());
										reportDateList.add(this.formatDate(thisReportDaily.getReportDatetime(), reportDateFormat));
										
										RigInformation rig=(RigInformation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, thisReportDaily.getRigInformationUid());
										
										CustomFieldUom thisConverter = new CustomFieldUom(ApplicationConfig.getConfiguredInstance().getDefaultReportLocaleObject(), Operation.class, "daysSpentPriorToSpud");
										thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysFromSpudOnOperation(operation.getOperationUid(), day.getDailyUid()));
										String daysFromSpud = thisConverter.getFormattedValue();
										
										//Map<String,Object> mapHseList=this.hseIncidentList(day);
										Map<String,Object> mapGenCommentHSEList=this.hseGeneralCommentList(day);
										Map<String,Object> mapGenCommentList=this.generalCommentList(day);
										Map<String,Object> mapContent= new HashMap<String,Object>();
										mapContent.put("country", well.getCountry());
										mapContent.put("operationType", operation.getOperationCode());
										mapContent.put("operationName",this.getCompleteOperationName(operation));
										mapContent.put("reportDatetime", this.formatDate(thisReportDaily.getReportDatetime(), reportDateFormat));
										mapContent.put("reportNumber", thisReportDaily.getReportNumber());
										mapContent.put("rigName", rig.getRigName());
										mapContent.put("reportPeriodSummary", thisReportDaily.getReportPeriodSummary());
										mapContent.put("reportCurrentStatus", thisReportDaily.getReportCurrentStatus());
										mapContent.put("reportPlanSummary", thisReportDaily.getReportPlanSummary());
										mapContent.put("spudDate", (operation.getSpudDate() != null) ? this.formatDate(operation.getSpudDate(), reportDateFormat) : "");
										mapContent.put("snrdrillsupervisor", thisReportDaily.getSnrdrillsupervisor());
										mapContent.put("daysFromSpud", daysFromSpud);
										mapContent.put("last24hrsincident", (StringUtils.isNotBlank(thisReportDaily.getLast24hrsincident())) ? thisReportDaily.getLast24hrsincident() : "");
										//mapContent.put("hseDetails", mapHseList);
										mapContent.put("genCommentsHse", mapGenCommentHSEList);
										mapContent.put("genComments", mapGenCommentList);
										all.put(String.valueOf(c), mapContent);
										c++;
										String reportFileName=reportJobResult.getReportFile().getDisplayName()+"."+this.getFileExtension(reportJobResult.getReportFilePath());
										attachment.put(reportFileName, new File(reportJobResult.getReportFilePath()));
									}
								}
							}
						}
					}
					subject = "Daily Summary Report " + subject;
					//String subject = emailConfig.getSubject();
					//subject = subject.replace(REPORT_DATETIME, reportDateList.toString());
					//subject = subject.replace(REPORT_NUMBER, reportNumberList.toString());
					//subject = subject.replace(OPERATION, operationNameList.toString());
					
					emailConfig.addContentMapping("content", all);
					emailConfig.setEmailList(this.getEmailList(emailConfig));
					emailConfig.setSubject(subject);
					emailConfig.setAttachment(attachment);
					if (!all.isEmpty()) {
						this.sendEmail(emailConfig);	
					}
				}
			}
			this.setRunning(false);
		}
	}

	public synchronized Integer runExtractionAndSendEmail(Daily day, String[] emailList) throws Exception
	{
		try{
			ReportJobParams reportData=ddrReportModule.generateDDRByDailyUid(day.getDailyUid(), this.getGenerateReportUserName(),null);
			reportData.waitOnThisObject();
			ReportJobResult jobResult=reportData.getLastReportJobResult();
			if (!jobResult.isError())
			{
				this.sendDailyEmail(day,jobResult,emailList);
				return SUCCESS;
			}
			else
			{
				this.sendErrorEmail(day);
				return ERROR;
			}
		}catch(Exception ex)
		{
			this.sendErrorEmail(day);
			this.getLogger().error(ex);
			return ERROR;
		}
	}
	
	public synchronized ReportJobResult runExtraction(Daily day, DDRReportModule reportModule) throws Exception
	{
		try{
			ReportJobParams reportData=reportModule.generateDDRByDailyUid(day.getDailyUid(), this.getGenerateReportUserName(),null);
			reportData.waitOnThisObject();
			ReportJobResult jobResult=reportData.getLastReportJobResult();
			return jobResult;
		}catch(Exception ex)
		{
			this.sendErrorEmail(day);
			this.getLogger().error(ex);
			return null;
		}
	}
	
	private String[] getEmailList(EmailConfiguration emailConfig) throws Exception
	{
		if (emailConfig.getIncludeSupportEmail()!=null && emailConfig.getIncludeSupportEmail())
		{
			return (emailConfig.getSpecificEmailDistributionKey()!=null?this.getEmailListWithSupport(emailConfig.getSpecificEmailDistributionKey()):this.getEmailListWithSupport());
		}
		return emailConfig.getSpecificEmailDistributionKey()!=null?this.getEmailList(emailConfig.getSpecificEmailDistributionKey()):this.getEmailList();
	}
	
	private void sendErrorEmail(Daily day) throws Exception
	{
		String[] errorReceipient={this.getSupportEmail()};
		
		Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(day.getOperationUid());
		ReportDaily thisReportDaily = this.reportDailyList(day.getOperationUid(), reportType, day.getDailyUid());
		RigInformation rig=(RigInformation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, thisReportDaily.getRigInformationUid());
		
		EmailConfiguration emailConfig=new EmailConfiguration(this.getErrorEmailConfiguration());
		emailConfig.clearContentMapping();
		emailConfig.addContentMapping("operationName",this.getCompleteOperationName(operation));
		emailConfig.addContentMapping("reportDatetime", this.formatDate(thisReportDaily.getReportDatetime(), reportDateFormat));
		emailConfig.addContentMapping("reportNumber", thisReportDaily.getReportNumber());
		emailConfig.addContentMapping("rigName", rig.getRigName());
		emailConfig.setEmailList(errorReceipient);
		emailConfig.setSubject(this.constructEmailSubject(this.reportDailyList(day.getOperationUid(), reportType, day.getDailyUid()), day, emailConfig.getSubject()));
		this.sendEmail(emailConfig);
	}
	
	private void sendDailyEmail(Daily day, ReportJobResult reportJobResult,String[] emailList) throws Exception
	{
		Well well=ApplicationUtils.getConfiguredInstance().getCachedWell(day.getWellUid());
		Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());		
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(day.getOperationUid());
		ReportDaily thisReportDaily = this.reportDailyList(day.getOperationUid(), reportType, day.getDailyUid());
		RigInformation rig=(RigInformation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, thisReportDaily.getRigInformationUid());
		LookupCompany lookupCompany=(LookupCompany)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, rig.getRigManager());
		
		CustomFieldUom thisConverter = new CustomFieldUom(ApplicationConfig.getConfiguredInstance().getDefaultReportLocaleObject(), Operation.class, "daysSpentPriorToSpud");
		thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysFromSpudOnOperation(operation.getOperationUid(), day.getDailyUid()));
		String daysFromSpud = thisConverter.getFormattedValue();
		thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysFromSpudOnOperationByDate(operation.getOperationUid(), day.getDailyUid(), this.daysFromSpudByDateCalcType));
		String daysFromSpud_SinceOpsSpudDate  = thisConverter.getFormattedValue();
		thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysOnOperation(operation.getOperationUid(), day.getDailyUid()));
		String daysOnWell = thisConverter.getFormattedValue();
		
		//if no afe added, value should be obtain from operation screen (manual entry), else must from costnet afe screen
		Double totalAfe = 0.0;
		Double afeOri = operation.getAfe();
		Double suppAfeOri = operation.getSecondaryafeAmt();
		Double dayCost = null;
		String strDayCost="";
		
		getAfeData(operation);
		Double afeCost = operation.getAfe();
		Double suppAfeCost = operation.getSecondaryafeAmt();
		Double afe =0.0;
		Double secondaryAfe=0.0;
		
		Double depthMdMsl = null;
		String strDepthMdMsl = "";
		Double depthTvdMsl = null;
		String strDepthTvdMsl="";
		Double lastCsgshoeMdMsl = null;
		String strLastCsgshoeMdMsl="";
		Double lastCsgshoeTvdMsl = null;
		String strLastCsgshoeTvdMsl="";
		Double progress = null;
		String strProgress="";
		Double lastCsgsize = null;
		String strLastCsgsize="";
		String lookup="";
		Double lastHolesize = null;
		String strLastHolesize="";
		
		String defaultUri = "xml://casingsection.casing_size?key=code&amp;value=label";
		
		if(afeCost!=null){
			totalAfe = totalAfe + afeCost;	
			afe = afeCost;
		}
		else if (afeOri!=null){
			totalAfe = totalAfe + afeOri;
			afe = afeOri;
		}
			
		String afeAmount;
		if (afe != null) {
			thisConverter.setReferenceMappingField(Operation.class, "afe");
			thisConverter.setBaseValue(afe);
			afeAmount = thisConverter.getFormattedValue(afe, false);
		} else {
			afeAmount = "";
		}
		
		if(suppAfeCost!=null){
			totalAfe = totalAfe + suppAfeCost;	
			secondaryAfe = suppAfeCost;
		}
		else if (suppAfeOri!=null){
			totalAfe = totalAfe + suppAfeOri;
			secondaryAfe = suppAfeOri;
		}		
		
		String secondaryAfeAmount;
		if (secondaryAfe != null) {
			thisConverter.setReferenceMappingField(Operation.class, "secondaryafeAmt");
			thisConverter.setBaseValue(secondaryAfe);
			secondaryAfeAmount = thisConverter.getFormattedValue(secondaryAfe, false);
		} else {
			secondaryAfeAmount = "";
		}
		
		String totalAfeAmount;
		thisConverter.setBaseValue(totalAfe);
		totalAfeAmount = thisConverter.getFormattedValue(totalAfe, false);
		Double cumCost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(day.getDailyUid(), reportType);
		String cumCostValue;
		if (cumCost != null) {
			thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
			thisConverter.setBaseValue(cumCost);
			cumCostValue = thisConverter.getFormattedValue(cumCost, false);
		} else {
			cumCostValue = "";
		}
		
		if(thisReportDaily.getDaycost() != null)
		{
			dayCost = thisReportDaily.getDaycost();
			thisConverter.setReferenceMappingField(ReportDaily.class, "dayCost");
			thisConverter.setBaseValue(dayCost);
			dayCost = thisConverter.getConvertedValue();
			strDayCost = thisConverter.getFormattedValue(dayCost, false);
		}
		
		if(thisReportDaily.getDepthMdMsl() != null)
		{
			depthMdMsl = thisReportDaily.getDepthMdMsl();
			thisConverter.setReferenceMappingField(ReportDaily.class, "depthMdMsl", operation.getUomTemplateUid());
			thisConverter.setBaseValue(depthMdMsl);
			depthMdMsl = thisConverter.getConvertedValue();
			strDepthMdMsl = thisConverter.getFormattedValue(depthMdMsl, true);
		}
		
		if(thisReportDaily.getDepthTvdMsl() != null)
		{
			depthTvdMsl = thisReportDaily.getDepthTvdMsl();
			thisConverter.setReferenceMappingField(ReportDaily.class, "depthTvdMsl", operation.getUomTemplateUid());
			thisConverter.setBaseValue(depthTvdMsl);
			depthTvdMsl = thisConverter.getConvertedValue();
			strDepthTvdMsl = thisConverter.getFormattedValue(depthTvdMsl, true);
		}
		
		if(thisReportDaily.getLastCsgshoeMdMsl() != null)
		{
			lastCsgshoeMdMsl = thisReportDaily.getLastCsgshoeMdMsl();
			thisConverter.setReferenceMappingField(ReportDaily.class, "lastCsgshoeMdMsl", operation.getUomTemplateUid());
			thisConverter.setBaseValue(lastCsgshoeMdMsl);
			lastCsgshoeMdMsl = thisConverter.getConvertedValue();
			strLastCsgshoeMdMsl = thisConverter.getFormattedValue(lastCsgshoeMdMsl, true);
		}
		
		if(thisReportDaily.getLastCsgshoeTvdMsl() != null)
		{
			lastCsgshoeTvdMsl = thisReportDaily.getLastCsgshoeTvdMsl();
			thisConverter.setReferenceMappingField(ReportDaily.class, "lastCsgshoeTvdMsl", operation.getUomTemplateUid());
			thisConverter.setBaseValue(lastCsgshoeTvdMsl);
			lastCsgshoeTvdMsl = thisConverter.getConvertedValue();
			strLastCsgshoeTvdMsl = thisConverter.getFormattedValue(lastCsgshoeTvdMsl, true);
		}
		
		if(thisReportDaily.getProgress() != null)
		{
			progress = thisReportDaily.getProgress();
			thisConverter.setReferenceMappingField(ReportDaily.class, "progress", operation.getUomTemplateUid());
			thisConverter.setBaseValue(progress);
			progress = thisConverter.getConvertedValue();
			strProgress = thisConverter.getFormattedValue(progress, true);
		}
		
		if(thisReportDaily.getLastCsgsize() != null)
		{
			lastCsgsize = thisReportDaily.getLastCsgsize();
			thisConverter.setReferenceMappingField(ReportDaily.class, "lastCsgsize", operation.getUomTemplateUid());
			thisConverter.setBaseValue(lastCsgsize);
			lastCsgsize = thisConverter.getConvertedValue();
			strLastCsgsize = thisConverter.getFormattedValue();
			String[] csgSize = strLastCsgsize.split(" ");
			strLastCsgsize = csgSize[0].toString();
			
			if (defaultUri!=null) {
				Map<String, LookupItem> lookupList = LookupManager.getConfiguredInstance().getLookup(defaultUri, null, null);
				for(String value : lookupList.keySet()){
					if(value.equalsIgnoreCase(strLastCsgsize)){
						LookupItem lookupItem = lookupList.get(value);
						lookup = lookupItem.getValue().toString();
					}
				}	
			}
		}
		
		if(thisReportDaily.getLastHolesize() != null)
		{
			lastHolesize = thisReportDaily.getLastHolesize();
			thisConverter.setReferenceMappingField(ReportDaily.class, "lastHolesize", operation.getUomTemplateUid());
			thisConverter.setBaseValue(lastHolesize);
			lastHolesize = thisConverter.getConvertedValue();
			strLastHolesize = thisConverter.getFormattedValue(lastHolesize, true);
		}
		
		String reportFileName=reportJobResult.getReportFile().getDisplayName()+"."+this.getFileExtension(reportJobResult.getReportFilePath());
		
		Map<String, File> attachment = new HashMap<String,File>();
		attachment.put(reportFileName, new File(reportJobResult.getReportFilePath()));
		
		EmailConfiguration emailConfig=new EmailConfiguration(this.getValidEmailConfiguration());
		
		Map<String,Object> mapHseList=this.hseIncidentList(day);
		Map<String,Object> mapGenCommentHSEList=this.hseGeneralCommentList(day);
		Map<String,Object> mapGenCommentList=this.generalCommentList(day);
		emailConfig.clearContentMapping();
		emailConfig.addContentMapping("country", well.getCountry());
		emailConfig.addContentMapping("operationName",this.getCompleteOperationName(operation));
		emailConfig.addContentMapping("reportDatetime", this.formatDate(thisReportDaily.getReportDatetime(), reportDateFormat));
		emailConfig.addContentMapping("reportNumber", thisReportDaily.getReportNumber());
		emailConfig.addContentMapping("rigName", rig.getRigName());
		emailConfig.addContentMapping("rigManager", (lookupCompany.getCompanyName()!=null) ? lookupCompany.getCompanyName() : "");
		emailConfig.addContentMapping("daysOnWell", daysOnWell);
		emailConfig.addContentMapping("reportPeriodSummary", (thisReportDaily.getReportPeriodSummary()!=null) ? thisReportDaily.getReportPeriodSummary() : "");
		emailConfig.addContentMapping("reportCurrentStatus", (thisReportDaily.getReportCurrentStatus()!=null) ? thisReportDaily.getReportCurrentStatus() : "");
		emailConfig.addContentMapping("reportPlanSummary", (thisReportDaily.getReportPlanSummary()!=null) ? thisReportDaily.getReportPlanSummary() : "");
		emailConfig.addContentMapping("afe", afeAmount);
		emailConfig.addContentMapping("supAfe", secondaryAfeAmount);
		emailConfig.addContentMapping("totalAfe", totalAfeAmount);
		emailConfig.addContentMapping("spudDate", (operation.getSpudDate() != null) ? this.formatDate(operation.getSpudDate(), reportDateFormat) : "");
		emailConfig.addContentMapping("snrdrillsupervisor", thisReportDaily.getSnrdrillsupervisor());
		emailConfig.addContentMapping("daycost", strDayCost);
		emailConfig.addContentMapping("cumCost", cumCostValue);
		emailConfig.addContentMapping("daysFromSpud", daysFromSpud);
		emailConfig.addContentMapping("daysFromSpud_SinceOpsSpudDate", daysFromSpud_SinceOpsSpudDate);
		emailConfig.addContentMapping("last24hrsincident", (StringUtils.isNotBlank(thisReportDaily.getLast24hrsincident())) ? thisReportDaily.getLast24hrsincident() : "");
		emailConfig.addContentMapping("depthMdMsl", strDepthMdMsl);
		emailConfig.addContentMapping("depthTvdMsl", strDepthTvdMsl);
		emailConfig.addContentMapping("lastCsgshoeMdMsl", strLastCsgshoeMdMsl);
		emailConfig.addContentMapping("lastCsgshoeTvdMsl", strLastCsgshoeTvdMsl);
		emailConfig.addContentMapping("progress", (strProgress!=null) ? strProgress : "");
		emailConfig.addContentMapping("lastCsgsize", lookup);
		emailConfig.addContentMapping("lastHolesize", strLastHolesize);
		
		if (mapHseList != null && mapHseList.size() > 0) emailConfig.addContentMapping("hseDetails", mapHseList);
		if (mapGenCommentHSEList != null && mapGenCommentHSEList.size() > 0) emailConfig.addContentMapping("genCommentsHse", mapGenCommentHSEList);
		if (mapGenCommentList != null && mapGenCommentList.size() > 0) emailConfig.addContentMapping("genComments", mapGenCommentList);
		emailConfig.setEmailList(emailList==null?this.getEmailList(emailConfig):emailList);
		emailConfig.setSubject(this.constructEmailSubject(this.reportDailyList(day.getOperationUid(), reportType, day.getDailyUid()), day, emailConfig.getSubject()));
		emailConfig.setAttachment(attachment);
		this.sendEmail(emailConfig);
	}
	
	private ReportDaily reportDailyList (String operationUid, String reportType, String dailyUid) throws Exception {
		Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setUomDatumUid(operation.getDefaultDatumUid());
		
		String strSql ="FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND (reportType = :reportType)"; 
		String[] paramsFields = {"operationUid", "dailyUid", "reportType"};
		String[] paramsValues = {operationUid, dailyUid, reportType};						
		List<ReportDaily> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues,qp);
		
		if (rs != null && rs.size() > 0) {
			return (ReportDaily) rs.get(0);
		} else {
			return null;
		}
	}
	
	private Map<String, Object> hseIncidentList (Daily day) throws Exception {
		
		String strSql ="FROM HseIncident WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid"; 
		if(includeSpecificHseIncidentCategory!=null && includeSpecificHseIncidentCategory.size()>0){
			strSql += " AND incidentCategory IN ('"+ StringUtils.join(this.includeSpecificHseIncidentCategory.toArray(), "','") +"')";
		}
		String[] paramsFields = {"operationUid", "dailyUid"};
		String[] paramsValues = {day.getOperationUid(), day.getDailyUid()};						
		List<HseIncident> hseList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		Map<String, Object> hseItems = new HashMap<String, Object>();
		Date dayDate = day.getDayDate();
		if (hseList != null && hseList.size() > 0) {
			for (HseIncident rec : hseList) {
				
				String numberOfDays = null;
				if (dayDate != null) {	
					double daysLapsed = CommonUtil.getConfiguredInstance().calculateDaysSinceHseIncident(dayDate, rec.getHseEventdatetime(), day.getGroupUid());
					
					//assign unit to the value base on daily days_since_lta unit
					CustomFieldUom thisConverter = new CustomFieldUom(ApplicationConfig.getConfiguredInstance().getDefaultReportLocaleObject(), ReportDaily.class, "days_since_lta");
					thisConverter.setBaseValue(daysLapsed);	
					numberOfDays = thisConverter.getFormattedValue();
				}
				
				Map<String, Object> hseItem = new HashMap<String, Object>();
				hseItem.put("sequence", rec.getSequence());
				hseItem.put("incidentCategory", rec.getIncidentCategory());
				hseItem.put("hseShortDescription", rec.getHseShortdescription());
				hseItem.put("numberOfIncidents", rec.getNumberOfIncidents());
				hseItem.put("hseEventDatetime", this.formatDate(rec.getHseEventdatetime(),reportDateTimeFormat));
				hseItem.put("daysLapsed", numberOfDays);
				hseItem.put("description", rec.getDescription());
				hseItems.put(rec.getHseIncidentUid(), hseItem);
			}
		}
		return hseItems;
	}
	
	private Map<String, Object> hseGeneralCommentList (Daily day) throws Exception {
		
		String strSql ="FROM GeneralComment WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND category = 'hseStatistic'"; 
		String[] paramsFields = {"operationUid", "dailyUid"};
		String[] paramsValues = {day.getOperationUid(), day.getDailyUid()};						
		List<GeneralComment> genCommentList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		Map<String, Object> genCommentItems = new HashMap<String, Object>();
		if (genCommentList != null && genCommentList.size() > 0) {
			for (GeneralComment rec : genCommentList) {
				Map<String, Object> genCommentItem = new HashMap<String, Object>();
				genCommentItem.put("comments", rec.getComments());
				genCommentItems.put(rec.getGeneralCommentUid(), genCommentItem);
			}
		}
		return genCommentItems;
	}
	
	private Map<String, Object> generalCommentList (Daily day) throws Exception {
		
		String strSql ="FROM GeneralComment WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND category <> 'hseStatistic'"; 
		String[] paramsFields = {"operationUid", "dailyUid"};
		String[] paramsValues = {day.getOperationUid(), day.getDailyUid()};						
		List<GeneralComment> genCommentList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		Map<String, Object> genCommentItems = new HashMap<String, Object>();
		if (genCommentList != null && genCommentList.size() > 0) {
			for (GeneralComment rec : genCommentList) {
				Map<String, Object> genCommentItem = new HashMap<String, Object>();
				genCommentItem.put("comments", rec.getComments());
				genCommentItem.put("category", rec.getCategory());
				genCommentItems.put(rec.getGeneralCommentUid(), genCommentItem);
			}
		}
		return genCommentItems;
	}

	private String constructEmailSubject(ReportDaily rd, Daily day, String subject) throws Exception
	{
		Operation currentOperation 	= (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, day.getOperationUid());
			
		subject = this.replaceDateTimePattern(rd.getReportDatetime(),subject, REPORT_DATETIME, reportDateFormat);
		subject = subject.replace(REPORT_NUMBER, rd.getReportNumber());		
		subject = subject.replace(OPERATION, this.getCompleteOperationName(currentOperation));
		
		return subject;
	}
	
	public static void getAfeData(Operation operation) throws Exception
	{
		List<Object[]> afeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationAfe o, CostAfeMaster c where (o.isDeleted=false or o.isDeleted is null) and (c.isDeleted=false or c.isDeleted is null) and (o.afeType=:afeType) and o.costAfeMasterUid=c.costAfeMasterUid and o.operationUid=:operationUid", new String[] {"afeType", "operationUid"}, new Object[] {CostNetConstants.MASTER_AFE, operation.getOperationUid()});
		if (afeList != null && afeList.size()>0) {
			String afeNumber = "";
			Double afeTotal = null;
			for (Object[] rec : afeList) {
				CostAfeMaster costAfeMaster = (CostAfeMaster) rec[1];
				afeNumber += ("".equals(afeNumber)?"":"\n") + costAfeMaster.getAfeNumber();
				Double thisAfeTotal = CommonUtil.getConfiguredInstance().calculateAfeTotal(costAfeMaster.getCostAfeMasterUid(), operation.getOperationUid());
				if (thisAfeTotal!=null) {
					if (afeTotal==null) afeTotal = 0.0;
					afeTotal += thisAfeTotal;
				}
			}
			operation.setAfeNumber(afeNumber);
			operation.setAfe(afeTotal);			
		}
		
		afeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM OperationAfe o, CostAfeMaster c where (o.isDeleted=false or o.isDeleted is null) and (c.isDeleted=false or c.isDeleted is null) and (o.afeType=:afeType or o.afeType='' or o.afeType is null) and o.costAfeMasterUid=c.costAfeMasterUid and o.operationUid=:operationUid", new String[] {"afeType", "operationUid"}, new Object[] {CostNetConstants.SUPPLEMENTARY_AFE, operation.getOperationUid()});
		if (afeList != null && afeList.size()>0) {
			String afeNumber = "";
			Double afeTotal = null;
			for (Object[] rec : afeList) {
				CostAfeMaster costAfeMaster = (CostAfeMaster) rec[1];
				afeNumber += ("".equals(afeNumber)?"":"\n") + costAfeMaster.getAfeNumber();
				Double thisAfeTotal = CommonUtil.getConfiguredInstance().calculateAfeTotal(costAfeMaster.getCostAfeMasterUid(), operation.getOperationUid());
				if (thisAfeTotal!=null) {
					if (afeTotal==null) afeTotal = 0.0;
					afeTotal += thisAfeTotal;
				}
			}
			operation.setSecondaryafeRef(afeNumber);
			operation.setSecondaryafeAmt(afeTotal);			
		}
	}
	

	public Boolean getIncludeAllOperationsWithoutAFE() {
		return includeAllOperationsWithoutAFE;
	}

	public void setIncludeAllOperationsWithoutAFE(Boolean includeAllOperationsWithoutAFE) {
		this.includeAllOperationsWithoutAFE = includeAllOperationsWithoutAFE;
	}

	public List<String> getIncludeSpecificHseIncidentCategory() {
		return includeSpecificHseIncidentCategory;
	}

	public void setIncludeSpecificHseIncidentCategory(
			List<String> includeSpecificHseIncidentCategory) {
		this.includeSpecificHseIncidentCategory = includeSpecificHseIncidentCategory;
	}
}