package com.idsdatanet.d2.drillnet.report.offwr;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * A client specific ReportDataGenerator for GeneralComment section for OFFWR
 * @author Wong Joon Hui (jhwong)
 * @client d2_choc_my
 * @ticket 12517
 */
public class OffwrGeneralCommentReportDataGenerator implements ReportDataGenerator {

	@Override
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		String wellboreUid = userContext.getUserSelection().getWellboreUid();
		String strSql = "SELECT DISTINCT b.reportDatetime FROM GeneralComment a, ReportDaily b "
				+ "WHERE (a.isDeleted = false or a.isDeleted is null) AND (b.isDeleted = false or b.isDeleted is null) "
				+ "AND a.dailyUid=b.dailyUid AND b.reportType!='DGR' AND a.category='gencomment' "
				+ "AND a.wellboreUid=:wellboreUid "
				+ "ORDER BY b.reportDatetime";
		String[] paramsFields = {"wellboreUid"};
		Object[] paramsValues = {wellboreUid};
		List<Date> reportDatetimeList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		for (Date date: reportDatetimeList) {
			List<String> commentList = this.getGeneralCommentsOnDate(wellboreUid, date);
			String comment = "";

			int count = 0; 
			String newLine = "\n";
			for (String s : commentList) {
				if (StringUtils.isNotBlank(s)) {
					if (count != 0) {
						comment += newLine + newLine;
					}
					comment += s;
				}
				count++;
			}
			
			ReportDataNode generalCommentReportDataNode = reportDataNode.addChild("GeneralComment");
			generalCommentReportDataNode.addProperty("reportDatetimeEpochMS", String.valueOf(date.getTime()));
			generalCommentReportDataNode.addProperty("reportDatetime", dateFormat.format(date));
			generalCommentReportDataNode.addProperty("comment", comment);
		}
	}
	
	private List<String> getGeneralCommentsOnDate(String wellboreUid, Date date) throws Exception {
		String strSql = "SELECT a.comments FROM GeneralComment a, ReportDaily b "
				+ "WHERE (a.isDeleted = false or a.isDeleted is null) AND (b.isDeleted = false or b.isDeleted is null) "
				+ "AND a.dailyUid=b.dailyUid AND b.reportType!='DGR' AND a.category='gencomment' "
				+ "AND a.wellboreUid=:wellboreUid AND b.reportDatetime=:reportDatetime "
				+ "ORDER BY a.sequence, b.reportNumber";
		String[] paramsFields = {"wellboreUid", "reportDatetime"};
		Object[] paramsValues = {wellboreUid, date};
		return ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
	}
	
	@Override
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}
	
}
