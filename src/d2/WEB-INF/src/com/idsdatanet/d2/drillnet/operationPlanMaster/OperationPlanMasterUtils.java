package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.util.List;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.PlanOperationTime;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class OperationPlanMasterUtils {
	/**
	 * Method to get the plan operation time by phase short code
	 * @param phaseShortCode
	 * @param operationUid
	 * @param qp
	 * @return List<PlanOperationTime>;
	 */
	public static List<PlanOperationTime> getPlanOperationTimeListByShortCode(String phaseShortCode, String operationUid, QueryProperties qp) throws Exception {
		//get list plan operation time
		List<PlanOperationTime> lookup = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From PlanOperationTime where " +
				"(isDeleted is null or isDeleted = false) " +
				"and phaseShortCode =:phaseShortCode and operationUid=:operationUid order by sequence", new String[] {"operationUid", "phaseShortCode"}, new Object[] {operationUid, phaseShortCode}, qp);
		
		if (lookup.size() > 0){
			return lookup;
		}
		
		return null;
	}
	
	/**
	 * Method to get the lookup phase code name by short code
	 * @param shortCode
	 * @param operationUid
	 * @param onOffShore
	 * @return string;
	 */
	public static String getLookupPhaseCodeName(String shortCode, String operationCode, String onOffShore) throws Exception {
		//get lookup phase code
		List<LookupPhaseCode> lookupPhaseCode = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From LookupPhaseCode where " +
				"(operationCode=:operationCode or operationCode = '' or operationCode is null) and (isDeleted is null or isDeleted = false) " +
				"and (onOffShore=:onOffShore or onOffShore='' or onOffShore is null) and shortCode =:shortCode  group by shortCode order by name", new String[] {"operationCode", "onOffShore", "shortCode"}, new Object[] {operationCode, onOffShore, shortCode});
		
		if (lookupPhaseCode.size() > 0){
			for (LookupPhaseCode rec : lookupPhaseCode) {
				return rec.getName();	
			}
		}

		return "";
	}

	/**
	 * Method to calculate the planned operation hour
	 * @param operationHours
	 * @param contingencyPercentage
	 * @param thisConverter
	 * @return Double;
	 */
	
	public static Double getPlannedOperationHours(Double operationHours, Double contingencyPercentage, CustomFieldUom thisConverter) throws Exception {
		thisConverter.setReferenceMappingField(PlanOperationTime.class, "operationHours");
		
		Double hour = operationHours ;
		thisConverter.setBaseValueFromUserValue(hour);
		hour = thisConverter.getBasevalue();
		Double percentage = 1.0 + (contingencyPercentage/100) ;
		
		thisConverter.setReferenceMappingField(PlanOperationTime.class, "plannedOperationHours");
		thisConverter.setBaseValue(hour*percentage);

		return thisConverter.getConvertedValue();
	}
	
}


