package com.idsdatanet.d2.drillnet.solidControlEquipment;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.model.SolidControlEquipment;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;


public class SolidControlEquipmentActionManager implements ActionManager {

	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if(com.idsdatanet.d2.core.web.mvc.Action.SAVE.equals(action)){
			
			Object object = node.getData();
			
			if (object instanceof Centrifuge) {
				return new SaveCentrifugeActionHandler();
			}
			
			if (object instanceof Shaker) {
				return new SaveShakerActionHandler();
			}
			
			return null;
		} else {
			return null;
		}
	}
	private class SaveCentrifugeActionHandler implements ActionHandler{
		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {
			
			Centrifuge centrifuge = (Centrifuge) node.getData();
			
			if (centrifuge != null) {
				SolidControlEquipment solidControlEquipment = new SolidControlEquipment();
				PropertyUtils.copyProperties(solidControlEquipment, centrifuge);
				solidControlEquipment.setEquipmentType("centrifuge");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(solidControlEquipment);
			}
			
			return new ActionHandlerResponse(true);
		}
	}
	private class SaveShakerActionHandler implements ActionHandler{
		public ActionHandlerResponse process(BaseCommandBean commandBean,
				UserSession userSession, AclManager aclManager,
				HttpServletRequest request, CommandBeanTreeNode node, String key)
				throws Exception {
			
			Shaker shaker = (Shaker) node.getData();
			
			if (shaker != null) {
				SolidControlEquipment solidControlEquipment = new SolidControlEquipment();
				PropertyUtils.copyProperties(solidControlEquipment, shaker);
				solidControlEquipment.setEquipmentType("shaker");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(solidControlEquipment);
			}
			
			return new ActionHandlerResponse(true);
		}
	}
}