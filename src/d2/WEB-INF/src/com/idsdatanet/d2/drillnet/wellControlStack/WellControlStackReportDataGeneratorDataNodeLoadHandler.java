package com.idsdatanet.d2.drillnet.wellControlStack;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.WellControl;
import com.idsdatanet.d2.core.model.WellControlComponent;
import com.idsdatanet.d2.core.model.WellControlStack;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WellControlStackReportDataGeneratorDataNodeLoadHandler implements DataNodeLoadHandler {

    private static final String REPORT_DATE = "report_date";
    
    @Override
    public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
        if ("WellControlStack".equals(meta.getTableClass().getSimpleName())) {
            return true;
        } else if ("WellControl".equals(meta.getTableClass().getSimpleName())) {
            return true;
        } else if ("WellControlComponent".equals(meta.getTableClass().getSimpleName())) {
            return true;
        } else if ("WellControlComponentTest".equals(meta.getTableClass().getSimpleName())) {
            return true;
        }
        return false;
    }

    @Override
    public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
        Date todayDate = (Date) userSelection.getCustomProperty(REPORT_DATE);
        if (todayDate == null) {
            Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
            if (daily != null) todayDate = daily.getDayDate();
        }
        String wellUid = userSelection.getWellUid();
        if ("WellControlStack".equals(meta.getTableClass().getSimpleName())) {
            return this.getWellControlStackList(todayDate, wellUid);
        } else if ("WellControl".equals(meta.getTableClass().getSimpleName())) {
            return this.getWellControlList(node, todayDate, wellUid);
        } else if ("WellControlComponent".equals(meta.getTableClass().getSimpleName())) {
            return this.getWellControlComponentList(node, todayDate, wellUid);
        } else if ("WellControlComponentTest".equals(meta.getTableClass().getSimpleName())) {
            return this.getWellControlComponentTestList(node, todayDate, wellUid);
        }
        return null;
    }
    
    private List<Object> getWellControlStackList(Date todayDate, String wellUid) throws Exception {        
        String queryString = "FROM WellControlStack WHERE (isDeleted=false OR isDeleted IS NULL) " 
            + "AND DATE(installDatetime) <= DATE(" + ":todayDate" + ") AND (DATE(removeDatetime) >= :todayDate OR removeDatetime IS NULL) " 
            + "AND wellUid = :wellUid";
        String[] paramNames = {"todayDate","wellUid"};
        Object[] paramValues = {todayDate, wellUid};
        List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
        if (lstResult.size() > 0) { 
            return lstResult;
        } else {
            return null;
        }
    }
    
    private List<Object> getWellControlList(CommandBeanTreeNode node, Date todayDate, String wellUid) throws Exception {
        Object object = node.getData();
        if (object instanceof WellControlStack) {
            WellControlStack wcs = (WellControlStack) object;
            
            String queryString = "FROM WellControl WHERE (isDeleted=false OR isDeleted IS NULL) "  
                    + "AND wellUid = :wellUid AND wellControlStackUid = :wellControlStackUid";
            String[] paramNames = {"wellUid", "wellControlStackUid"};
            Object[] paramValues = {wellUid, wcs.getWellControlStackUid()};
            List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
            if (lstResult.size() > 0) { 
                return lstResult;
            } else {
                return null;
            }
        }
        return null;
    }
    
    private List<Object> getWellControlComponentList(CommandBeanTreeNode node, Date todayDate, String wellUid) throws Exception {
        Object object = node.getData();
        if (object instanceof WellControl) {
            WellControl wc = (WellControl) object;
            
            String queryString = "FROM WellControlComponent WHERE (isDeleted=false OR isDeleted IS NULL) " 
                    + "AND DATE(installDatetime) <= DATE(" + ":todayDate" + ") AND (DATE(removeDatetime) >= :todayDate OR removeDatetime IS NULL) " 
                    + "AND wellUid = :wellUid AND wellControlUid = :wellControlUid";
            String[] paramNames = {"todayDate", "wellUid", "wellControlUid"};
            Object[] paramValues = {todayDate, wellUid, wc.getWellControlUid()};
            List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
            if (lstResult.size() > 0) { 
                return lstResult;
            } else {
                return null;
            }
        }
        return null;
    }
    
    private List<Object> getWellControlComponentTestList(CommandBeanTreeNode node, Date todayDate, String wellUid) throws Exception {
        Object object = node.getData();
        if (object instanceof WellControlComponent) {
            WellControlComponent wcc = (WellControlComponent) object;
            
            String queryString = "FROM WellControlComponentTest WHERE (isDeleted=false OR isDeleted IS NULL) " 
                + "AND DATE(testDatetime) <= DATE(" + ":todayDate" + ") " 
                + "AND wellUid = :wellUid AND wellControlComponentUid = :wellControlComponentUid "
                + "ORDER BY testDatetime DESC";
            String[] paramNames = {"todayDate", "wellUid", "wellControlComponentUid"};
            Object[] paramValues = {todayDate, wellUid, wcc.getWellControlComponentUid()};
            QueryProperties queryProperties = new QueryProperties();
            queryProperties.setFetchFirstRowOnly();
            List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, queryProperties);
            if (lstResult.size() > 0) { 
                List<Object> outputMap = new ArrayList<Object>();
                outputMap.add(lstResult.get(0));
                return outputMap;
            } else {
                return null;
            }
        }
        return null;
    }
}