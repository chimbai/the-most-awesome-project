package com.idsdatanet.d2.drillnet.surveyreference;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.*;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.*;
import java.util.*;
import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;

public class DDRReportListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return query;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(meta.getTableClass().equals(SurveyStation.class)){
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null){
				return conditionClause.replace("{_daily_filter_}", "");
			}
			
			if(StringUtils.isNotBlank(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_NUMBER_OF_SURVEY_LINES)))
			{
				query.rowsToFetch = Integer.parseInt(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_NUMBER_OF_SURVEY_LINES).toString());
			}
			else
			{
				query.rowsToFetch = 5;
				
			}
			String thisFilter = "";
			
			//GWP - if showcurrentdaysurveyonly - is set to yes
			if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "showcurrentdaysurveyonly"))){
				query.addParam("thisDailyUid", daily.getDailyUid());
				thisFilter = "dailyUid = :thisDailyUid";
				return conditionClause.replace("{_daily_filter_}", thisFilter);
			}			
			//use back midnight depth logic to control
			else{
				//get today depth from daily report just to fetch station that less then last midnight depth recorded
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(daily.getDayDate());
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
				
				String strSql = "SELECT MAX(rd.depthMdMsl) as maxMidNightDepth FROM ReportDaily rd, Daily d WHERE (rd.isDeleted = false or rd.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and rd.dailyUid = d.dailyUid AND d.dayDate <= :todayDate AND d.operationUid = :thisOperationUid";
				
				String[] paramsFields = {"todayDate", "thisOperationUid"};
				Object[] paramsValues = {calendar.getTime(), daily.getOperationUid()};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				
				if (conditionClause.indexOf("{_daily_filter_}") != -1 && !lstResult.isEmpty())
				{
					Object a = (Object) lstResult.get(0);
					Double maxMidNightDepth = 0.00;
					if (a != null) maxMidNightDepth = Double.parseDouble(a.toString());
					query.addParam("thisDepth", maxMidNightDepth);
					thisFilter = "depthMdMsl <= :thisDepth";
				}
				else
				{
					query.rowsToFetch = 0;
				}
				return conditionClause.replace("{_daily_filter_}", thisFilter);
			}
		}else if (meta.getTableClass().equals(SurveyReference.class)){
			if(StringUtils.isNotBlank(conditionClause) && conditionClause.indexOf("{_status_filter_}") < 0) return null;
			
			String condition="";
			List<SurveyReference> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM SurveyReference WHERE (isDeleted is null or isDeleted = false) and wellboreUid=:wellboreUid", "wellboreUid", userSelection.getWellboreUid());
			String surveyReferenceUid = filterSurveyToBeRetrieve(list);
			if (StringUtils.isNotBlank(surveyReferenceUid))
			{
				condition = (conditionClause.replace("{_status_filter_}","").trim().length()>0?" or ":"")+"surveyReferenceUid =:thisSurveyReferenceUid";
				query.addParam("thisSurveyReferenceUid", surveyReferenceUid);
				
			}	
			return conditionClause.replace("{_status_filter_}", condition);
		}else{
			return null;
		}
	}

	private String filterSurveyToBeRetrieve(List<SurveyReference> list)
	{
		String[] status = {"Definitive","Current"};
		int counter=0;
		String surveyReferenceUid = null;
		for (SurveyReference reference : list)
		{
			if (StringUtils.isNotBlank(reference.getStatusIndicator()) && ArrayUtils.contains(status, reference.getStatusIndicator()))
			{
				surveyReferenceUid = reference.getSurveyReferenceUid();
				counter++;
			}
		}
		if (counter>0 && counter==1)
		{
			return surveyReferenceUid;
		}
		if(counter==0)
		{
			surveyReferenceUid=null;
			for (SurveyReference reference : list)
			{
				if (StringUtils.isNotBlank(reference.getStatusIndicator()) && "Survey".equalsIgnoreCase(reference.getStatusIndicator()))
				{
					surveyReferenceUid = reference.getSurveyReferenceUid();
					counter++;
				}
			}
			if (counter>0 && counter==1)
			{
				return surveyReferenceUid;
			}
		}
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof SurveyStation){
			
			SurveyStation thisSurveyStation = (SurveyStation) object;
			
			if(thisSurveyStation.getDepthTvdMsl()!=null){
				
				//get TVDSS for VNG Norway only 
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, SurveyStation.class, "depthTvdMsl");
				thisConverter.setBaseValueFromUserValue(thisSurveyStation.getDepthTvdMsl(), false);
				thisConverter.removeDatumOffset();
		 		node.getDynaAttr().put("tvdSS", thisConverter.getConvertedValue());
				if (thisConverter.isUOMMappingAvailable()) node.setCustomUOM("@tvdSS", thisConverter.getUOMMapping());  
				  
			}
		} else if (object instanceof SurveyReference){
			QueryProperties qp = new QueryProperties();		
			qp.setUomConversionEnabled(false);
			
			Double depthMdMslValue = null;
			Double inclinationAngle = null;
			
			Date todayDate = null;
			Daily currentDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			todayDate = currentDaily.getDayDate();
			
			if(todayDate != null) {
				String SurveyStationSql = "SELECT ss.inclinationAngle, ss.depthMdMsl FROM SurveyStation ss, SurveyReference sr, ReportDaily rd " +
	 					"WHERE (ss.isDeleted='' or ss.isDeleted is null) AND (sr.isDeleted='' or sr.isDeleted is null) AND (rd.isDeleted='' or rd.isDeleted is null) " +
	 					"AND ss.surveyReferenceUid = sr.surveyReferenceUid AND (sr.isPlanned is null or sr.isPlanned !='1') AND ss.dailyUid = rd.dailyUid " +
	 					"AND sr.operationUid =:operationUid AND rd.reportDatetime <=:todayDate ORDER BY ss.inclinationAngle DESC, ss.depthMdMsl DESC";
	 			
	 			String[] params = {"operationUid","todayDate"};
	 			Object[] paramsValue = {currentDaily.getOperationUid(), todayDate};
	 			
	 			List surveyStationResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(SurveyStationSql, params, paramsValue, qp.setFetchFirstRowOnly());
				
		 		if (surveyStationResult.size()>0) {
					Object[] surveyResult = (Object[]) surveyStationResult.get(0);
					if (surveyResult != null) {
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
						
						if(surveyResult[0] != null) {
							thisConverter.setReferenceMappingField(SurveyStation.class, "inclinationAngle");
							thisConverter.setBaseValue(Double.parseDouble(surveyResult[0].toString()));
							node.getDynaAttr().put("SurveyStation.maxIncUpToCurrentDate", thisConverter.getConvertedValue());
							if (thisConverter.isUOMMappingAvailable())
								node.setCustomUOM("@SurveyStation.maxIncUpToCurrentDate", thisConverter.getUOMMapping());				
						}
						if(surveyResult[1] != null) {
							thisConverter.setReferenceMappingField(SurveyStation.class, "depthMdMsl");
							thisConverter.setBaseValue(Double.parseDouble(surveyResult[1].toString()));
							node.getDynaAttr().put("SurveyStation.depthMdMslOfMaxIncUpToCurrentDate", thisConverter.getConvertedValue());
							if (thisConverter.isUOMMappingAvailable())
								node.setCustomUOM("@SurveyStation.depthMdMslOfMaxIncUpToCurrentDate", thisConverter.getUOMMapping());
						}
					}	
				}
			}
		}
	}
	
}