package com.idsdatanet.d2.drillnet.formation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.BasinFormation;
import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.model.GeologyDescription;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.geonet.geologyDescription.GeologyDescriptionCommandBeanListener;

public class FormationDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor{
	private static final String TVDSS_REFERENCE_FIELD = "sampleTopTvdMsl";
	private static final String MDSS_REFERENCE_FIELD = "sampleTopMdMsl";
	private static final String PROG_TVD_MSL_REFERENCE_FIELD = "prognosedTopTvdMsl";
	private static final String DIFF_FROM_PROG_REFERENCE_FIELD = "diffFromProg";
	private static final String LOG_TOP_TVD_MSL_REFERENCE_FIELD = "logTopTvdMsl";
	private static final String DYN_ATTR_PROG_TVD = "prognosedTopTvdMsl";
	private static final String DYN_ATTR_ACTUAL_TVD = "sampleTopTvdMsl";
	
	private static final String HIGH = "High";
	private static final String LOW = "Low";
	
	private String viewType;
	private String noBasin = null;
	
	public void setViewType(String value){
		this.viewType = value;
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof Formation) {
			Formation thisFormation = (Formation) object;
				
			int invert = (this.isInvert(userSelection.getGroupUid()) ? -1 : 1);
			
			Double prognosedTvd = null;
			Double actualTvd = null;
			if (thisFormation.getPrognosedTopTvdMsl() != null) {
				// - depth to datum reference point (without offset to msl)
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, PROG_TVD_MSL_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getPrognosedTopTvdMsl(), false);
				thisConverter.removeDatumOffsetToUserReferencePoint();
				prognosedTvd = thisConverter.getBasevalue()* invert;
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@progTvdMsl", thisConverter.getUOMMapping(false));
				}
				
				node.getDynaAttr().put("progTvdMsl", thisConverter.getConvertedValue() * invert);
				
				// - depth to subsea
				thisConverter = new CustomFieldUom(commandBean, Formation.class, PROG_TVD_MSL_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getPrognosedTopTvdMsl(), false);
				thisConverter.removeDatumOffset();
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@progTvdMslSubsea", thisConverter.getUOMMapping(false));
				}
				
				node.getDynaAttr().put("progTvdMslSubsea", thisConverter.getConvertedValue() * invert);
			}
			
			if (thisFormation.getSampleTopTvdMsl() != null) {	
				// - depth to datum reference point (without offset to msl)
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, TVDSS_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getSampleTopTvdMsl(), false);
				thisConverter.removeDatumOffsetToUserReferencePoint();
				actualTvd = thisConverter.getBasevalue()* invert;
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@tvdMsl", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("tvdMsl", thisConverter.getConvertedValue() * invert);
				
				// - depth to subsea
				thisConverter = new CustomFieldUom(commandBean, Formation.class, TVDSS_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getSampleTopTvdMsl(), false);
				thisConverter.removeDatumOffset();
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@tvdMslSubsea", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("tvdMslSubsea", thisConverter.getConvertedValue() * invert);
				
			}
			
			if (prognosedTvd!=null && actualTvd!=null){
				Double different = prognosedTvd - actualTvd;
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, PROG_TVD_MSL_REFERENCE_FIELD);
				thisConverter.setBaseValue(different);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@diffTvd", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("diffTvd", thisConverter.getConvertedValue());
				
				//for GeoNet's absolute Diff Tvd and High/Low Status
				thisConverter = new CustomFieldUom(commandBean, Formation.class, DIFF_FROM_PROG_REFERENCE_FIELD);
				thisConverter.setBaseValue(Math.abs(different));
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@absDiffTvd", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("absDiffTvd", thisConverter.getConvertedValue());
				
				//Geonet's High/Low Calculation\
				Double progTvdRt = thisFormation.getPrognosedTopTvdMsl();
				Double tvdRt = thisFormation.getSampleTopTvdMsl();
				if(progTvdRt != null && tvdRt != null){
					Double progDepthTvd = Double.valueOf((Double) progTvdRt);
					Double actualDepthTvd = Double.valueOf((Double) tvdRt);
					Double diffProg = actualDepthTvd - progDepthTvd;
					thisConverter = new CustomFieldUom(commandBean, Formation.class, DIFF_FROM_PROG_REFERENCE_FIELD);
					thisConverter.setBaseValue(diffProg);
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@diffFromProg", thisConverter.getUOMMapping(false));
					}
					node.getDynaAttr().put("diffFromProg", thisConverter.getConvertedValue());
				}
				
				//check comparison
				if (different.compareTo(Double.valueOf(0)) < 0) {
					node.getDynaAttr().put("diffHighLowStatus", LOW);
				}
				else if (different.compareTo(Double.valueOf(0)) > 0) {
					node.getDynaAttr().put("diffHighLowStatus", HIGH);
				}

			}
			
						
			if (thisFormation.getSampleTopMdMsl() != null) {	
				// - depth to datum reference point (without offset to msl)
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, MDSS_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getSampleTopMdMsl(), false);
				thisConverter.removeDatumOffsetToUserReferencePoint();
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@mdMsl", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("mdMsl", thisConverter.getConvertedValue() * invert);
				
				// - depth to subsea
				thisConverter = new CustomFieldUom(commandBean, Formation.class, MDSS_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getSampleTopMdMsl(), false);
				thisConverter.removeDatumOffset();
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@mdMslSubsea", thisConverter.getUOMMapping(false));
				}
				node.getDynaAttr().put("mdMslSubsea", thisConverter.getConvertedValue() * invert);
			}
			
			if (thisFormation.getLogTopTvdMsl() != null) {
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, LOG_TOP_TVD_MSL_REFERENCE_FIELD);
				thisConverter.setBaseValueFromUserValue(thisFormation.getLogTopTvdMsl(), false);
				thisConverter.removeDatumOffset();
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@logTopTvdMslSubsea", thisConverter.getUOMMapping(false));
				}
				
				node.getDynaAttr().put("logTopTvdMslSubsea", thisConverter.getConvertedValue() * invert);
			}
			
			checkIfBasinSelected(node, userSelection.getWellUid(), node.getInfo().isFirstNodeInList()); // only check once per list (to improve performance)
			
			//get the eub formation code for formation name
			if (thisFormation.getFormationName() != null && userSelection.getWellUid() != null) {	
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
				String basinUid = "";
				basinUid = well.getBasin();
				String eub = CommonUtil.getConfiguredInstance().getEubCode(thisFormation.getFormationName(), basinUid);
				if (eub != null)
				{
					node.getDynaAttr().put("eubFormationCode", eub);
				}	
			}
			
			Wellbore thisWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, userSelection.getWellboreUid());
			String wellboreShape = thisWellbore.getWellboreShape();
			if (wellboreShape != null){
				node.getDynaAttr().put("wellboreShape", wellboreShape);
			}
		}
	}
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		if (object instanceof Formation) {
			Formation formation = (Formation) object;
			this.autoPopulateFormationDepthToGeologyDescription(formation.getFormationUid());
			
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_FORMATION_NAME))){
				String reportType = "DGR";
						
				if (formation.getOperationUid() != null) 
				{
					String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid AND reportType = :thisReportType";
					String[] paramsFields = {"selectedOperationUid", "thisReportType"};
					String[] paramsValues = {formation.getOperationUid(), reportType};
					List<ReportDaily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					if (lstResult.size() > 0)
					{
						
						for (ReportDaily selectedDDRReportDaily : lstResult)
						{
							Double depthMdMsl = null;
							
							String strSql1 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid AND dailyUid =:dailyUid AND reportType = 'DDR'";
							String[] paramsFields1 = {"selectedOperationUid",  "dailyUid"};
							String[] paramsValues1 = {selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid()};
							List<ReportDaily> lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
							if (lstResult1.size() > 0) {
								if (lstResult1.get(0).getDepthMdMsl() != null)
								{
									depthMdMsl = Double.parseDouble(lstResult1.get(0).getDepthMdMsl().toString());
									String strSql2 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid and sampleTopMdMsl <=:depthMdMsl order by sampleTopMdMsl desc";
									String[] paramsFields2 = {"selectedOperationUid",  "depthMdMsl"};
									Object[] paramsValues2 = {selectedDDRReportDaily.getOperationUid(), depthMdMsl};
									List<Formation> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
									if (lstResult1.size() > 0)
									{
										if (lstResult2.size() > 0)
										{
											Formation formationdata = (Formation) lstResult2.get(0);
											if (formationdata.getSampleTopMdMsl() != null)
											{
												if (formationdata.getSampleTopMdMsl() <= lstResult1.get(0).getDepthMdMsl())
												{
													String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
													String[] paramsFields3 = {"formationName", "operationUid", "dailyUid", "reportType"};
													String[] paramsValues3 = {formationdata.getFormationName(), selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
													ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
												}
											}
											else
											{
												String formationName = null;
												String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
												String[] paramsFields3 = {"formationName", "operationUid", "dailyUid", "reportType"};
												String[] paramsValues3 = {formationName, selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
												ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
											}
										}
										else
										{
											String formationName = null;
											String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
											String[] paramsFields3 = {"formationName", "operationUid", "dailyUid", "reportType"};
											String[] paramsValues3 = {formationName, selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
											ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
										}
									}
								}
								else
								{
									String formationName = null;
									String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
									String[] paramsFields3 = {"formationName", "operationUid", "dailyUid", "reportType"};
									String[] paramsValues3 = {formationName, selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
									ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
								}
							}
						}
						
					}
				}
			}
			//auto populate PrognosedTopTvdMsl and SampleTopTvdMsl from prognosedTopMdMsl and sampleTopMdMsl when Wellbore.wellboreShape = 'Vertical'
			Wellbore thisWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, formation.getWellboreUid());
			FormationUtils.autoPopulateTvdFromMd(thisWellbore.getWellboreUid(), thisWellbore.getWellboreShape(), session, commandBean);
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		if(object instanceof Formation) {
			Formation formation = (Formation) object;
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_FORMATION_NAME))){
				String reportType = "DGR";
						
				if (formation.getFormationUid() != null) 
				{
					String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid AND reportType = :thisReportType";
					String[] paramsFields = {"selectedOperationUid", "thisReportType"};
					String[] paramsValues = {formation.getOperationUid(), reportType};
					List<ReportDaily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					if (lstResult.size() > 0)
					{
						
						for (ReportDaily selectedDDRReportDaily : lstResult)
						{
							Double depthMdMsl = null;
							
							String strSql1 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid AND dailyUid =:dailyUid AND reportType = 'DDR'";
							String[] paramsFields1 = {"selectedOperationUid",  "dailyUid"};
							String[] paramsValues1 = {selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid()};
							List<ReportDaily> lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
							if (lstResult1.size() > 0 && lstResult1.get(0).getDepthMdMsl() != null)
							{
								depthMdMsl = Double.parseDouble(lstResult1.get(0).getDepthMdMsl().toString());
								String strSql2 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) and operationUid = :selectedOperationUid and sampleTopMdMsl <=:depthMdMsl order by sampleTopMdMsl desc";
								String[] paramsFields2 = {"selectedOperationUid",  "depthMdMsl"};
								Object[] paramsValues2 = {selectedDDRReportDaily.getOperationUid(), depthMdMsl};
								List<Formation> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
								if (lstResult1.size() > 0)
								{
									if (lstResult2.size() > 0)
									{
										Formation formationdata = (Formation) lstResult2.get(0);
										if (formationdata.getSampleTopMdMsl() != null)
										{
											if (formationdata.getSampleTopMdMsl() <= lstResult1.get(0).getDepthMdMsl())
											{
												String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
												String[] paramsFields3 = {"formationName", "operationUid", "dailyUid", "reportType"};
												String[] paramsValues3 = {formationdata.getFormationName(), selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
												ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
											}
										}
										else
										{
											String formationName = null;
											String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
											String[] paramsFields3 = {"formationName", "operationUid", "dailyUid", "reportType"};
											String[] paramsValues3 = {formationName, selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
											ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
										}
									}
									else
									{
										String formationName = null;
										String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
										String[] paramsFields3 = {"formationName", "operationUid", "dailyUid", "reportType"};
										String[] paramsValues3 = {formationName, selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
										ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
									}
								}
							}
							else
							{
								String formationName = null;
								String strSql3 = "UPDATE ReportDaily SET formation =:formationName WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND reportType =:reportType";
								String[] paramsFields3 = {"formationName", "operationUid", "dailyUid", "reportType"};
								String[] paramsValues3 = {formationName, selectedDDRReportDaily.getOperationUid(), selectedDDRReportDaily.getDailyUid(), reportType};
								ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, paramsFields3, paramsValues3);
							}
						}
						
					}
				}
			}
		}
		
	}
	
	private void autoPopulateFormationDepthToGeologyDescription(String formationUid) throws Exception
	{
		
		if (StringUtils.isNotBlank(formationUid))
		{
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			qp.setDatumConversionEnabled(false);
			List<GeologyDescription> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM GeologyDescription WHERE (isDeleted is null or isDeleted = false) and formationName = :formationUid","formationUid",formationUid,qp );
			if (results.size()>0)
			{
				GeologyDescription descr = results.get(0);
				Formation formation = GeologyDescriptionCommandBeanListener.getFormation(formationUid, false);
				if (formation !=null && formation.getSampleTopMdMsl()!=null)
				{
					descr.setTopDepthMdMsl(formation.getSampleTopMdMsl());
				}
	
				if (formation!=null && formation.getSampleTopTvdMsl()!=null)
				{
					descr.setTopSubseaTvd(formation.getSampleTopTvdMsl());
				}
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(descr, qp);
			}
			
		}
	}
	
	private boolean isInvert(String groupUid) throws Exception {
		return "negative".equals(GroupWidePreference.getValue(groupUid, "structureformationgeol"));	
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof Formation) {
			
			Formation thisFormation = (Formation)object;
			String formationName = getDynaAttr(node, "name");
			
			
			// validate as much as possible here, if failed, return immediately, before anything get created below
			if (StringUtils.isNotBlank(formationName)) {
				
				String basinUid = null;
				if (StringUtils.isNotBlank(session.getCurrentWellUid())) {
					Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, session.getCurrentWellUid());
					if (StringUtils.isNotBlank(well.getBasin())) {
						basinUid = well.getBasin();
					}else {
						status.setContinueProcess(false, true);
						status.setFieldError(node, "formationName", "No Basin is selected in Well Data");
						return;
					}
				}
				
				String[] paramsFields = {"formationName", "basinUid"};
		 		Object[] paramsValues = new Object[2];
				paramsValues[0] = formationName;
				paramsValues[1] = basinUid;
				
				String strSql = "FROM BasinFormation where name = :formationName and basinUid = :basinUid and (isDeleted = false or isDeleted is null)";
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields,paramsValues);			
				if (list != null && list.size() > 0) {
					status.setContinueProcess(false, true);
					status.setDynamicAttributeError(node, "name", "The formation name '" + formationName + "' already exists in database");
					return;
				}
				
				BasinFormation thisBasinFormation = CommonUtil.getConfiguredInstance().createBasinFormation(formationName, basinUid);
				commandBean.getSystemMessage().addInfo("A new formation has been created with minimal information from the formation tops screen. Please make sure that the Basin Formation module is reviewed and relevant details are added in.");
				thisFormation.setFormationName(thisBasinFormation.getName());
			}
			
			int invert = (this.isInvert(session.getCurrentGroupUid()) ? -1 : 1);
			
			//check if GWP is set to calc TVD base on TVDSS enter - for geology only
			//if ("geology".equalsIgnoreCase(viewType) && "0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "geolformationcalcsubseatvd")) && node.getDynaAttr().get("tvdMsl") != null) {
			if ("geology".equalsIgnoreCase(viewType) && "0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "geolformationcalcsubseatvd"))) {
				if(node.getDynaAttr().get("tvdMslSubsea")!=null && StringUtils.isNotBlank(node.getDynaAttr().get("tvdMslSubsea").toString())){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, TVDSS_REFERENCE_FIELD);
					thisConverter.setBaseValueFromUserValue(Double.valueOf(node.getDynaAttr().get("tvdMslSubsea").toString()) * invert, false);
					thisConverter.addDatumOffset();
					//thisConverter.addDatumOffsetToUserReferencePoint(); // add datum to user entered value, because user enter the value in "reporting ref point" (LAT, etc)
					thisFormation.setSampleTopTvdMsl(thisConverter.getConvertedValue());
				}
				else thisFormation.setSampleTopTvdMsl(null);
			}
			
			//check if GWP is set to calc MD base on MDSS enter - for geology only
			if ("geology".equalsIgnoreCase(viewType) && "0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "geolformationcalcsubseamd")) && node.getDynaAttr().get("mdMsl") != null) {	
				if(node.getDynaAttr().get("mdMsl")!=null && StringUtils.isNotBlank(node.getDynaAttr().get("mdMsl").toString())){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, MDSS_REFERENCE_FIELD);
					thisConverter.setBaseValueFromUserValue(Double.valueOf(node.getDynaAttr().get("mdMsl").toString()) * invert, false);
					thisConverter.addDatumOffsetToUserReferencePoint(); // add datum to user entered value, because user enter the value in "reporting ref point" (LAT, etc)
					thisFormation.setSampleTopMdMsl(thisConverter.getConvertedValue());
				}
				else thisFormation.setSampleTopMdMsl(null);
			}
			
			//check if GWP is set to calc Prog TVD base on Prog TVDSS enter - for geology only
			//if ("geology".equalsIgnoreCase(viewType) && "0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "geolformationcalcProgsubseatvd")) && node.getDynaAttr().get("progTvdMsl") != null) {
			if ("geology".equalsIgnoreCase(viewType) && "0".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "geolformationcalcProgsubseatvd"))) {
				if(node.getDynaAttr().get("progTvdMslSubsea")!=null && StringUtils.isNotBlank(node.getDynaAttr().get("progTvdMslSubsea").toString())){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, PROG_TVD_MSL_REFERENCE_FIELD);
					
					thisConverter.setBaseValueFromUserValue(Double.valueOf(node.getDynaAttr().get("progTvdMslSubsea").toString()) * invert, false);
					thisConverter.addDatumOffset();
					//thisConverter.addDatumOffsetToUserReferencePoint();
					
					thisFormation.setPrognosedTopTvdMsl(thisConverter.getConvertedValue());
				}
				else thisFormation.setPrognosedTopTvdMsl(null);
			}
			
			// sjcameron 22857 - do not run if operating mode is DEPOT.
			// In this mode the well/basin will be null and will throw null exceptions
			if(commandBean.getOperatingMode() != BaseCommandBean.OPERATING_MODE_DEPOT) {
				//dfchan 21946 - insert basin uid here
				thisFormation.setBasinUid(session.getCurrentWell().getBasin());
			}
		}
	}
	
	
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		checkIfBasinSelected(node, userSelection.getWellUid(), true);
		
		Wellbore thisWellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, userSelection.getWellboreUid());
		String wellboreShape = thisWellbore.getWellboreShape();
		if (wellboreShape != null){
			node.getDynaAttr().put("wellboreShape", wellboreShape);
		}
	}
	
	private void checkIfBasinSelected (CommandBeanTreeNode node, String wellUid, boolean doChecking) throws Exception {
		if(doChecking){
			this.noBasin = null;
			if (StringUtils.isNotBlank(wellUid)) {
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellUid);
				if (StringUtils.isBlank(well.getBasin())) {
					this.noBasin = "1";
				}
			}
		}

		if(this.noBasin != null){
			node.getDynaAttr().put("noBasin", this.noBasin);
		}
	}
	
	private String getDynaAttr(CommandBeanTreeNode node, String dynaAttrName) throws Exception {
		Object dynaAttrValue = node.getDynaAttr().get(dynaAttrName);
		if (dynaAttrValue != null) {
			return dynaAttrValue.toString();
		}
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		return null;
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		String gwpSorting = FormationUtils.sortPreference(userSelection, viewType);
		return gwpSorting;
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean arg0, UserSelectionSnapshot arg1, HttpServletRequest arg2, TreeModelDataDefinitionMeta arg3, DataDefinitionHQLQuery arg4, CommandBeanTreeNode arg5) throws Exception {
		return null;
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
}
