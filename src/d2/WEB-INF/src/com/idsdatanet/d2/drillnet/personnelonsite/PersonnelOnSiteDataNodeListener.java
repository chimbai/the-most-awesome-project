package com.idsdatanet.d2.drillnet.personnelonsite;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.PersonnelOnSite;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class PersonnelOnSiteDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	
	public boolean TotalPax = false;
	
	public void setTotalPax(Boolean TotalPax){
		this.TotalPax = TotalPax;
	}
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof PersonnelOnSite) {
			PersonnelOnSite personnelonsite = (PersonnelOnSite)object;
			Integer pax = personnelonsite.getPax();
			Double workhour = personnelonsite.getWorkingHours();
			Double overhour = personnelonsite.getOvertimeHours();
			Double totalhour = personnelonsite.getTotalWorkingHours();
			
			if(workhour!=null)
			{
				if(workhour>24.00){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "workingHours", "Record cannot be saved due to working hours exceeded 24 hours");
					return;
				}
				else if(workhour<0.00){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "workingHours", "Record cannot be saved due to working hours cannot be negative");
					return;
				}
			}
			
			if(personnelonsite.getArrivedDate()!= null && personnelonsite.getDepartedDate()!= null){
				if(personnelonsite.getArrivedDate().getTime() > personnelonsite.getDepartedDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "departedDate", "Record cannot be saved due to the date departed is earlier than date of arrival");
					return;
				}
			}
			
			if (pax == null) {
				personnelonsite.setPax(1);
				pax = 1;
			}
			
			if ("hours_per_pax".equals(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "pobAlwaysAutoCalculate"))){
				if (totalhour == null) {
					double defaultWorkingHours = Double.parseDouble(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "pobDefaultWorkinghours").toString());
					totalhour = defaultWorkingHours * pax;
					workhour = defaultWorkingHours;
					
				}else {
					workhour = totalhour / pax;
				}
				
			}else if ("total_hours".equals(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "pobAlwaysAutoCalculate"))) {
				if (workhour == null) {
					double defaultWorkingHours = Double.parseDouble(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "pobDefaultWorkinghours").toString());
					totalhour = defaultWorkingHours * pax;
					workhour = defaultWorkingHours;
					
				}else {
					totalhour = workhour * pax;
				}
			}
			personnelonsite.setWorkingHours(workhour);
			personnelonsite.setTotalWorkingHours(totalhour);
			
			
			if (overhour == null) {
				personnelonsite.setOvertimeHours(0.0);
			}
			
			Date start = personnelonsite.getArrivedDate();
			
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(session.getCurrentDailyUid());
			if (daily == null) return;
			Date dailyStart = daily.getDayDate();
			
			if(start != null && dailyStart != null){
				if(start.after(dailyStart)){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "arrivedDate", "Date In cannot be later than the report date.");
					return;
				}
			}
		}

	}

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (! node.getInfo().isTemplateNode() && object instanceof PersonnelOnSite)
		{	
			Integer pax = (Integer) PropertyUtils.getProperty(object, "pax");
			if (pax != null) {
				Integer totalPax = null;
				totalPax = (Integer) commandBean.getRoot().getDynaAttr().get("pax");
				if (totalPax == null) {
					commandBean.getRoot().getDynaAttr().put("pax", pax);
				} else {
					totalPax += pax;
					commandBean.getRoot().getDynaAttr().put("pax", totalPax);
				}
			}
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, PersonnelOnSite.class, "total_working_hours");
			Double totalworkinghours = (Double) PropertyUtils.getProperty(object, "totalWorkingHours");
			if (totalworkinghours != null) {
				Double totalhours = null;
				totalhours = (Double) commandBean.getRoot().getDynaAttr().get("totalWorkingHours");
				if (totalhours == null) {
					commandBean.getRoot().getDynaAttr().put("totalWorkingHours", totalworkinghours);
					//thisConverter.setBaseValueFromUserValue(totalworkinghours);
					if (thisConverter.isUOMMappingAvailable()) commandBean.getRoot().setCustomUOM("@totalManHours", thisConverter.getUOMMapping());
					commandBean.getRoot().getDynaAttr().put("totalManHours", totalworkinghours);
				} else {
					totalhours += totalworkinghours;
					commandBean.getRoot().getDynaAttr().put("totalWorkingHours", totalhours);
					//thisConverter.setBaseValueFromUserValue(totalhours);
					if (thisConverter.isUOMMappingAvailable()) commandBean.getRoot().setCustomUOM("@totalManHours", thisConverter.getUOMMapping());
					commandBean.getRoot().getDynaAttr().put("totalManHours", totalhours);
				}
			}
			
			//19608
			if(TotalPax) {
				//get total green hands
				String[] paramsFields = {"crewStatus", "dailyUid"};
				Object[] paramsValues = new Object[2]; paramsValues[0] = "GH"; paramsValues[1]= userSelection.getDailyUid();
				Double totalGreenHands = 0.0;
				
				String strSql = "SELECT SUM(pax) FROM PersonnelOnSite WHERE (isDeleted = false or isDeleted is null) and crewStatus =:crewStatus and dailyUid =: dailyUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (!lstResult.isEmpty()) {
					Object a = (Object) lstResult.get(0);
					
					if (a != null) totalGreenHands = Double.parseDouble(a.toString());
					commandBean.getRoot().getDynaAttr().put("totalGreenHands", totalGreenHands);
				}
				
				//get total absentees
				String[] paramsFields1 = {"crewStatus", "dailyUid"};
				Object[] paramsValues1 = new Object[2]; paramsValues1[0] = "ABS"; paramsValues1[1]= userSelection.getDailyUid();
				Double totalAbsentees = 0.0;
				
				String strSql1 = "SELECT SUM(pax) FROM PersonnelOnSite WHERE (isDeleted = false or isDeleted is null) and crewStatus =:crewStatus and dailyUid =: dailyUid";
				List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1);
				
				if (!lstResult1.isEmpty()) {
					Object b = (Object) lstResult1.get(0);
					
					if (b != null) totalAbsentees = Double.parseDouble(b.toString());
					commandBean.getRoot().getDynaAttr().put("totalAbsentees", totalAbsentees);
				}
				
				//get total shortages
				String[] paramsFields2 = {"crewStatus", "dailyUid"};
				Object[] paramsValues2 = new Object[2]; paramsValues2[0] = "ST"; paramsValues2[1]= userSelection.getDailyUid();
				Double totalShortages = 0.0;
				
				String strSql2 = "SELECT SUM(pax) FROM PersonnelOnSite WHERE (isDeleted = false or isDeleted is null) and crewStatus =:crewStatus and dailyUid =: dailyUid";
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				
				if (!lstResult2.isEmpty()) {
					Object c = (Object) lstResult2.get(0);
					
					if (c != null) totalShortages = Double.parseDouble(c.toString());
					commandBean.getRoot().getDynaAttr().put("totalShortages", totalShortages);
				}
			}
		}
	}
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		if(commandBean.getFlexClientControl() != null){
			commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		if(commandBean.getFlexClientControl() != null){
			commandBean.getFlexClientControl().setReloadParentHtmlPage();
		}
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<Object> loadChildNodes(CommandBean arg0,
			TreeModelDataDefinitionMeta arg1, CommandBeanTreeNode arg2,
			UserSelectionSnapshot arg3, Pagination arg4, HttpServletRequest arg5)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
