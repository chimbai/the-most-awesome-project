package com.idsdatanet.d2.drillnet.wellControlStack;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.WellControlComponent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActiveBopStackSummaryDataNodeLoadHandler implements DataNodeLoadHandler {

    @Override
    public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
        if ("WellControlComponent".equals(meta.getTableClass().getSimpleName())) {
            return true;
        }
        return false;
    }

    @Override
    public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
        String wellUid = userSelection.getWellUid();
        if ("WellControlComponent".equals(meta.getTableClass().getSimpleName())) {
            return this.getWellControlComponentList(node, wellUid);
        }
        return null;
    }
    
    private List<Object> getWellControlComponentList(CommandBeanTreeNode node, String wellUid) throws Exception {
        String queryString = "FROM WellControlStack wcs, WellControl wc, WellControlComponent wcc "
        		+ "WHERE (wcs.isDeleted = false OR wcs.isDeleted IS NULL) "
        		+ "AND (wc.isDeleted = false OR wc.isDeleted IS NULL) "
        		+ "AND (wcc.isDeleted = false OR wcc.isDeleted IS NULL) " 
        		+ "AND wcs.wellUid = :wellUid "
        		+ "AND (wcs.removeDatetime IS NULL OR wcs.removeDatetime = '') "
        		+ "AND wc.wellControlStackUid = wcs.wellControlStackUid "
        		+ "AND wcc.wellControlUid = wc.wellControlUid "
        		+ "AND (wcc.removeDatetime IS NULL OR wcc.removeDatetime = '') "
        		+ "ORDER BY wc.sequence ASC, wcc.ramPosition DESC, wcc.sequence ASC";
        String[] paramNames = {"wellUid"};
        Object[] paramValues = {wellUid};
        List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
        List<Object> componentList = new ArrayList<Object>();
        if (lstResult.size() > 0) {
            for (Object[] lst : lstResult) {
                        WellControlComponent wcc = (WellControlComponent) lst[2];
                        componentList.add(wcc);
            }
        }
        return componentList;
    }
    
}