package com.idsdatanet.d2.drillnet.well;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ExtendedOperationLookupHandler implements LookupHandler {	
		
	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
			
		if (userSelection.getWellUid()!=null)
		{
			String sql = "select operationUid, operationName from Operation where (isDeleted = false or isDeleted is null) and wellUid = :welluid ORDER BY operationName";
			
			List<Object []> operationResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "welluid", userSelection.getWellUid());
			if (operationResults.size() > 0) {
				for (Object[] operationResult : operationResults) {
					if (operationResult != null) {										    	 
						  result.put(operationResult[0].toString(), new LookupItem(operationResult[0].toString(), operationResult[1].toString()));
					}			
				}
			}
		}
		return result;
	}
}

