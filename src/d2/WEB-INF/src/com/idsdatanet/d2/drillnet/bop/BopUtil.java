package com.idsdatanet.d2.drillnet.bop;

import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Bop;
import com.idsdatanet.d2.core.model.BopLog;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class BopUtil {

	public static void populateCasingSectionData(CommandBean commandBean, CommandBeanTreeNode node, String casingSectionUid) throws Exception {
		
		CasingSection casingSection = null;
		if (StringUtils.isNotBlank(casingSectionUid)) {
			casingSection = (CasingSection) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(CasingSection.class, casingSectionUid);
		}
		if (casingSection==null) casingSection = new CasingSection(); //to set existing all dynamic field to blank
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		Map<String, Object> fieldValues = PropertyUtils.describe(casingSection);
		for (Map.Entry<String, Object> entry : fieldValues.entrySet()) {
			setDynaAttr(node, thisConverter, casingSection.getClass(), entry.getKey(), entry.getValue());
		}
	}

	public static void setDynaAttr(CommandBeanTreeNode node, CustomFieldUom converter, Class thisClass, String fieldName, Object value) throws Exception {
		node.getDynaAttr().put(thisClass.getSimpleName() + "." + fieldName, value);
		converter.setReferenceMappingField(thisClass, fieldName);
		if (converter.isUOMMappingAvailable()) {
			node.setCustomUOM("@" + thisClass.getSimpleName() + "." + fieldName, converter.getUOMMapping());
		}

	}
	
	/**
	 * Method to create or update BOP from Activity based on pre-defined free text remarks
	 * @param thisActivity
	 * @param fieldsMap
	 * @throws Exception
	 */
	public static void createBopBasedOnActivityRemarks(UserSelectionSnapshot userSelection,String moduleName,Activity thisActivity, Map<String,Object>fieldsMap) throws Exception{
		if (fieldsMap !=null){
			
			String[] paramsFields = {"operationUid","installDate"};
			Object[] paramsValues = {thisActivity.getOperationUid(),thisActivity.getStartDatetime()};
			String strSql = "FROM Bop WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND operationUid=:operationUid AND installDate=:installDate";
			List<Bop> ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			Bop bop = null;
			BopLog bopLog = null;
			if(ls.size()==0){
				if(thisActivity.getStartDatetime()!=null){
					bop = new Bop();
					bop.setInstallDate(thisActivity.getStartDatetime());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bop);
				}
			}else{
				bop = ls.get(0);			
			}
			 
			if ("bop".equalsIgnoreCase(moduleName)){
				
				for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
					if (StringUtils.isNotBlank(fieldEntry.getKey())) PropertyUtils.setProperty(bop, fieldEntry.getKey(), fieldEntry.getValue());
				}
				Boolean isSubmit = bop.getIsSubmit();
				//if(isSubmit!=null && isSubmit)  RevisionUtils.unSubmitRecordWithStaticRevisionLog(userSelection, bop, "Auto update from time codes");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bop);
				
			}else if ("bopLog".equalsIgnoreCase(moduleName)){
				paramsFields = new String[]{"bopUid","testingDatetime"};
				paramsValues = new Object[]{bop.getBopUid(),thisActivity.getStartDatetime()};
				strSql = "FROM BopLog WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND bopUid=:bopUid AND testingDatetime=:testingDatetime";
				List<BopLog> lsBogLog = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if(lsBogLog.size()==0){
					if(thisActivity.getStartDatetime()!=null){
						bopLog = new BopLog();
						bopLog.setBopUid(bop.getBopUid());
						bopLog.setTestingDatetime(thisActivity.getStartDatetime());
					}
				}else{
					bopLog = lsBogLog.get(0);			
				}
					
				for (Map.Entry<String, Object> fieldEntry : fieldsMap.entrySet()){					
					if (StringUtils.isNotBlank(fieldEntry.getKey())) PropertyUtils.setProperty(bopLog, fieldEntry.getKey(), fieldEntry.getValue());
				}
				Boolean isSubmit = bopLog.getIsSubmit();
				//if(isSubmit!=null && isSubmit)  RevisionUtils.unSubmitRecordWithStaticRevisionLog(userSelection, bopLog, "Auto update from time codes");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bopLog);
				
			}
			
		}				
	}
}
