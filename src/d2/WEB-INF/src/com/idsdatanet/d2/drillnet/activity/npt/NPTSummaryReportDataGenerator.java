package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.report.ReportOption;
import com.idsdatanet.d2.core.util.WellNameUtil;

public class NPTSummaryReportDataGenerator extends NPTReportDataGenerator{

	private String uriLookupCompany = "db://LookupCompany?key=lookupCompanyUid&value=companyName&cache=false&order=companyName";
	private String uriSections = "xml://Activity.sections?key=code&value=label";
	
	private Map<String,LookupItem> sectionList = null;
	private Map<String,LookupItem> companyList = null;
	
	protected void generateActualData(UserContext userContext, Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		this.setCompanyList(LookupManager.getConfiguredInstance().getLookup(this.uriLookupCompany, userContext.getUserSelection(), null));
		this.setSectionList(LookupManager.getConfiguredInstance().getLookup(this.uriSections, userContext.getUserSelection(), null));
		List<NPTSummary> includedList = this.shouldIncludeWell(userContext, startDate, endDate);
		this.includeQueryData(includedList, reportDataNode, userContext.getUserSelection(), startDate, endDate);
		this.populateDistinctWellboreByField(reportDataNode, userContext.getUserSelection(), startDate, endDate, userContext);
	}
	
	private List<NPTSummary> generatedata(UserContext userContext, Date startDate, Date endDate, String includeWellUid) throws Exception
	{

		String[] paramNames = {"startDate","endDate","companyNPTClassCode","otherNPTClassCode","wellUid"};
		Object[] values = {startDate,endDate,this.getCompanyNPTClassCode(),this.getOtherNPTClassCode(),includeWellUid};
		String sql = "SELECT DISTINCT w.wellUid,w.wellName, o.operationName, w.field, w.country, a.phaseCode, a.sections, a.nptSubCategory, a.lookupCompanyUid, a.nptMainCategory, sum(a.activityDuration) as duration, wb.wellboreName"+		
			" FROM Well w,Wellbore wb, Operation o, Activity a, Daily d "+ 
			" Where w.wellUid=wb.wellUid and wb.wellboreUid=o.wellboreUid"+
			" and d.operationUid=o.operationUid"+ 
			" and a.dailyUid=d.dailyUid"+
			" and (a.isDeleted is null or a.isDeleted = False)"+
			" and (d.isDeleted is null or d.isDeleted = False)"+
			" and (w.isDeleted is null or w.isDeleted = false) and (wb.isDeleted is null or wb.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false)"+
			" and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) "+
			" and (a.classCode=:companyNPTClassCode or a.classCode=:otherNPTClassCode)"+ 
			" and (a.startDatetime>=:startDate and a.endDatetime<=:endDate)"+ 						
			" and w.wellUid=:wellUid"+			
			" group by w.field, a.phaseCode, a.sections, a.nptSubCategory, a.lookupCompanyUid, wb.wellboreUid"+
			" order by w.field, a.phaseCode, a.sections, a.nptSubCategory, a.lookupCompanyUid, wb.wellboreUid";
		
		List<NPTSummary> includedList = new ArrayList<NPTSummary>();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, values, qp);
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
				
		for (Object[] item :result)
		{
			String wellUid = (String)item[0];
			
			NPTSummary npt = new NPTSummary((String)item[0],null, (String)item[4], (String)item[8], null,null,null,null);			
			npt.setWellName((String)item[1]);
			npt.setOperationName((String)item[2]);		
			npt.setField((String)item[3]);			
			npt.setDuration(Double.parseDouble(String.valueOf(item[10])));
			thisConverter.setBaseValue(npt.getDuration());
			npt.setDuration(thisConverter.getConvertedValue());
			npt.setPhaseCode((String)item[5]);		
			npt.setSections((String)item[6]);	
			
			String nptMainCategory = this.getNptMainCategoryName((String)item[9], userContext);
			String nptSubCategory = this.getNptSubCategoryName((String)item[7], userContext);
			
			npt.setNptSubCategory(nptSubCategory);		
			npt.setNptMainCategory(nptMainCategory);
			npt.setWellboreName((String)item[11]);	
			
			includedList.add(npt);
		}
				
		return includedList;
		
	}
	
	private List<NPTSummary> shouldIncludeWell(UserContext userContext, Date startDate, Date endDate) throws Exception
	{
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		ReportOption wells = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL):null;
		List<NPTSummary> result = new ArrayList<NPTSummary>();
		for (String wellUid : wells.getValues())
		{
			result.addAll(this.generatedata(userContext, startDate, endDate,wellUid));
		}
		return result;
	}

	private void includeQueryData(List<NPTSummary> nptSummary, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception
	{			
		ReportDataNode nodeChild = node.addChild("NPTSummary");		
		
		for (NPTSummary npt : nptSummary)
		{	
			ReportDataNode catChild = nodeChild.addChild("data");
			
			catChild.addProperty("welluid", npt.getWellUid());
			catChild.addProperty("wellname", npt.getWellName());
			catChild.addProperty("operationname", npt.getOperationName());
			catChild.addProperty("field",npt.getField());			
			catChild.addProperty("phaseCode",npt.getPhaseCode());	
			catChild.addProperty("sections",npt.getSections());	
			LookupItem section = this.sectionList.get(npt.getSections());
			if (section !=null)
				catChild.addProperty("sections_lookupLabel",section.getValue().toString());
			catChild.addProperty("nptSubCategory",npt.getNptSubCategory());	
			catChild.addProperty("nptMainCategory",npt.getNptMainCategory());	
			catChild.addProperty("lookupCompanyUid",npt.getLookupCompanyUid());
			LookupItem item = this.companyList.get(npt.getLookupCompanyUid());
			if (item !=null)
				catChild.addProperty("lookupCompanyUid_lookupLabel",item.getValue().toString());
			catChild.addProperty("NPTduration", String.valueOf(npt.getDuration()));	
			catChild.addProperty("wellboreName",npt.getWellboreName());
			catChild.addProperty("sectionsPadded",WellNameUtil.getPaddedStr(npt.getSections()));			
		}				
	}
	
	
	private String getNptMainCategoryName(String nptMainCategory, UserContext userContext) throws Exception {
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://Activity.nptMainCategory?key=code&value=label", userSelection, null);
		if (lookupMap != null) {
			LookupItem lookupItem = lookupMap.get(nptMainCategory);
			if (lookupItem != null) {
				return (String) lookupItem.getValue();
			}
		}
		
		return nptMainCategory;
	}
	
	private String getNptSubCategoryName(String nptSubCategory, UserContext userContext) throws Exception {
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		CascadeLookupSet[] cascadeLookupNptSubCategory = LookupManager.getConfiguredInstance().getCompleteCascadeLookupSet("xml://Activity.nptSubCategory?key=code&amp;value=label", userSelection);
		
		for(CascadeLookupSet cascadeLookup : cascadeLookupNptSubCategory){
			Map<String, LookupItem> lookupList = cascadeLookup.getLookup();
			if (lookupList != null) {
				LookupItem lookupItem = lookupList.get(nptSubCategory);
				if (lookupItem != null) {
					return (String) lookupItem.getValue();
				}
			}					
		}		
		return nptSubCategory;
	}
	
	private void populateDistinctWellboreByField(ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end, UserContext userContext) throws Exception
	{
		//to populate Distinct Wellbore Name, Wellbore Uid, BY FIELD
		ReportOption fields = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_FIELD)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_FIELD):null;	
		ReportDataNode nodeChild2 = node.addChild("Field_Wellbore");	
		
		for (String fieldName : fields.getValues()){
			String[] paramVars = {"startDate","endDate","companyNPTClassCode","otherNPTClassCode","field","wellUid"};
			Object[] paramValues = {start,end,this.getCompanyNPTClassCode(),this.getOtherNPTClassCode(),fieldName,BHIQueryUtils.getReportOptionValue(userContext,BHIQueryUtils.CONSTANT_WELL)};
			String sql = "SELECT DISTINCT wb.wellboreName, wb.wellboreUid"+			
			" FROM Well w,Wellbore wb, Operation o, Activity a, Daily d "+ 
			" Where w.wellUid=wb.wellUid and wb.wellboreUid=o.wellboreUid"+
			" and d.operationUid=o.operationUid"+ 
			" and a.dailyUid=d.dailyUid"+
			" and (a.isDeleted is null or a.isDeleted = False)"+
			" and (d.isDeleted is null or d.isDeleted = False)"+
			" and (w.isDeleted is null or w.isDeleted = false) and (wb.isDeleted is null or wb.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false)"+
			" and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) "+
			" and (a.classCode=:companyNPTClassCode or a.classCode=:otherNPTClassCode)"+ 
			" and (a.startDatetime>=:startDate and a.endDatetime<=:endDate)"+ 						
			" and w.wellUid in (:wellUid)"+
			" and (w.field =:field)"+
			" group by wb.wellboreUid"+
			" order by wb.wellboreUid";
		
			List<Object[]> query = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramVars , paramValues);			
			String lWellboreUid = "";
			String lWellboreName = "";
			
			for (Object[] item :query)			
			{
		
				String wellboreName = (String) item[0];	
				String wellboreUid = (String) item[1];
				
				if (wellboreUid != null ){
					if (lWellboreName == ""){
						lWellboreName = wellboreName;	
						lWellboreUid = wellboreUid;
					}else{		
						lWellboreName = lWellboreName + ", " + wellboreName;
						lWellboreUid = lWellboreUid + ", " + wellboreUid ;
					}					
				}	
			}
			
			//Create another report data node for populated DISTINCT Field , Wellbore	
			ReportDataNode catChild2 = nodeChild2.addChild("data");
			catChild2.addProperty("Field", (String) fieldName);
			catChild2.addProperty("wellboreNames", (String) lWellboreName);		
			catChild2.addProperty("wellboreUids", (String) lWellboreUid);	
		}	
	}

	public void setUriLookupCompany(String uriLookupCompany) {
		this.uriLookupCompany = uriLookupCompany;
	}

	public String getUriLookupCompany() {
		return uriLookupCompany;
	}

	public void setCompanyList(Map<String,LookupItem> companyList) {
		this.companyList = companyList;
	}

	public Map<String,LookupItem> getCompanyList() {
		return companyList;
	}

	public void setUriSections(String uriSections) {
		this.uriSections = uriSections;
	}

	public String getUriSections() {
		return uriSections;
	}

	public void setSectionList(Map<String,LookupItem> sectionList) {
		this.sectionList = sectionList;
	}

	public Map<String,LookupItem> getSectionList() {
		return sectionList;
	}
}
