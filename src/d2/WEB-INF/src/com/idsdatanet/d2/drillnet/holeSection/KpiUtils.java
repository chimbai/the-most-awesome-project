package com.idsdatanet.d2.drillnet.holeSection;

import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;

public class KpiUtils {
		
	public static void handleConversion(CommandBean commandBean,Object object,String kpiUnit) throws Exception{
		Class kpiObjectClass = Class.forName(object.getClass().getName());
		String uom = (String) PropertyUtils.getProperty(object, "uom");
		if(StringUtils.isNotBlank(uom)){
			String uomType = getUomType(uom,kpiUnit);
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			if(uomType!=null && StringUtils.isNotBlank(uomType)){
				clearFields(object);
				Double target = (Double) PropertyUtils.getProperty(object, "target");
				Double aim = (Double) PropertyUtils.getProperty(object, "aim");
				if ("currency".equals(uomType)){
					thisConverter = getUomConverter(thisConverter, "targetCurrency", uom, target,kpiObjectClass);
					PropertyUtils.setProperty(object, "targetCurrency", thisConverter.getBasevalue());
								
					thisConverter = getUomConverter(thisConverter, "aimCurrency", uom, aim,kpiObjectClass);
					PropertyUtils.setProperty(object, "aimCurrency", thisConverter.getBasevalue());
				}
				if ("length".equals(uomType)){
					thisConverter = getUomConverter(thisConverter, "targetLength", uom, target,kpiObjectClass);
					PropertyUtils.setProperty(object, "targetLength", thisConverter.getBasevalue());
								
					thisConverter = getUomConverter(thisConverter, "aimLength", uom, aim,kpiObjectClass);
					PropertyUtils.setProperty(object, "aimLength", thisConverter.getBasevalue());
				}
				if ("time".equals(uomType)){
					thisConverter = getUomConverter(thisConverter, "targetTime", uom, target,kpiObjectClass);
					PropertyUtils.setProperty(object, "targetTime", thisConverter.getBasevalue());
								
					thisConverter = getUomConverter(thisConverter, "aimTime", uom, aim,kpiObjectClass);
					PropertyUtils.setProperty(object, "aimTime", thisConverter.getBasevalue());
				}
				if ("velocity".equals(uomType)){
					thisConverter = getUomConverter(thisConverter, "targetVelocity", uom, target,kpiObjectClass);
					PropertyUtils.setProperty(object, "targetVelocity", thisConverter.getBasevalue());
								
					thisConverter = getUomConverter(thisConverter, "aimVelocity", uom, aim,kpiObjectClass);
					PropertyUtils.setProperty(object, "aimVelocity", thisConverter.getBasevalue());
				}
			}
		}
	}
	
	private static void clearFields(Object object) throws Exception{
		PropertyUtils.setProperty(object, "targetCurrency", null);
		PropertyUtils.setProperty(object, "aimCurrency", null);
		PropertyUtils.setProperty(object, "targetLength", null);
		PropertyUtils.setProperty(object, "aimLength", null);
		PropertyUtils.setProperty(object, "targetTime", null);
		PropertyUtils.setProperty(object, "aimTime", null);
		PropertyUtils.setProperty(object, "targetVelocity", null);
		PropertyUtils.setProperty(object, "aimVelocity", null);
	}
	
	private static String getUomType(String uom,String kpiUnit) throws Exception{
		if (StringUtils.isNotBlank(uom)){
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select shortCode, lookupLabel, description From CommonLookup Where (isDeleted is null or isDeleted = false) and lookupTypeSelection=:lookupTypeSelection", "lookupTypeSelection", kpiUnit);
			
			for (Object[] obj : list){
				if (uom.equals((String)obj[0])) return (String)obj[2];
			}
		}
		return null;
	}
	
	public static CustomFieldUom getUomConverter (CustomFieldUom thisConverter, String field, String uom, Double value,Class kpiObjectClass) throws Exception{
		thisConverter.setReferenceMappingField(kpiObjectClass, field);
		if(thisConverter.isUOMMappingAvailable()){
			thisConverter.changeUOMUnit(uom);
			thisConverter.setBaseValueFromUserValue(value);
		}
		return thisConverter;
	}
}

