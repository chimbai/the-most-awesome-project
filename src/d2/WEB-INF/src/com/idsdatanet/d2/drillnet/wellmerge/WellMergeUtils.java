package com.idsdatanet.d2.drillnet.wellmerge;

import java.util.List;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class WellMergeUtils {
	@SuppressWarnings("unchecked")
	public static List<Well> getWellList() throws Exception {
		String hql = "FROM Well WHERE (isDeleted is null or isDeleted = false) ORDER BY wellName, wellUid";
		List<Well> wellList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
		if (wellList != null && wellList.size() > 0) {
			return wellList;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static List<Wellbore> getWellboreList() throws Exception {
		String hql = "FROM Wellbore WHERE (isDeleted is null or isDeleted = false) ORDER BY wellboreName, wellboreUid";
		List<Wellbore> wellboreList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
		if (wellboreList != null && wellboreList.size() > 0) {
			return wellboreList;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Wellbore> getWellboreList(String wellUid) throws Exception {
		String hql = "FROM Wellbore WHERE (isDeleted is null or isDeleted = false) AND wellUid=:wellUid ORDER BY wellboreName, wellboreUid";
		List<Wellbore> wellboreList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[]{"wellUid"}, new Object[]{wellUid});
		if (wellboreList != null && wellboreList.size() > 0) {
			return wellboreList;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Operation> getOperationList(String wellboreUid) throws Exception {
		String hql = "FROM Operation WHERE (isDeleted is null or isDeleted = false) AND wellboreUid=:wellboreUid ORDER BY operationName, operationUid";
		List<Operation> operationList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[]{"wellboreUid"}, new Object[]{wellboreUid});
		if (operationList != null && operationList.size() > 0) {
			return operationList;
		}
		return null;
	}
}
