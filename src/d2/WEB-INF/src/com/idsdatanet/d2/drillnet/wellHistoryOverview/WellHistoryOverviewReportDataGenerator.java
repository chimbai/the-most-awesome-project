package com.idsdatanet.d2.drillnet.wellHistoryOverview;

import java.io.File;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.operationinformationcenter.DDRReportList;
import com.idsdatanet.d2.drillnet.operationinformationcenter.DGRReportList;

public class WellHistoryOverviewReportDataGenerator implements ReportDataGenerator {
	
	private String defaultReportType;
	
	public void setReportType(String value){
		this.defaultReportType = value;
	}

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
	    Map<String,LookupItem> campaignLookup=null;
	    Map<String,LookupItem> operationCodeLookup=null;
	    Map<String,LookupItem> operationalUnitLookup=null;
	    CascadeLookupSet[] cascadeLookupCategory=null;
	    String strIntvEvent="";
	    String strIntvEventTmp="";
	    String strOpType="";
	    
		if(this.defaultReportType != null){
			reportType = this.defaultReportType;
		}
		
		String currentWellUid = userContext.getUserSelection().getWellUid();
		String currentUserUid = userContext.getUserSelection().getUserUid();
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String selectedField = "";
		String selectedCond = "";
		String selecteddataUid = "";
		Integer selectedOutputRecord = 0; 
		String wellName="";
		String campaignName="";
		String viewFilter="";
		String selectedOpType = "";
		
		String selectedScreenOutputFilter = (String) userContext.getUserSelection().getCustomProperty("cp_ScreenOutput");
		if (selectedScreenOutputFilter !=null)
		{
			if (selectedScreenOutputFilter.equalsIgnoreCase("last10")) {
				selectedOutputRecord =10;
				viewFilter = "LAST 10";
			}
		}
		
		String selectedOperationFilter = (String) userContext.getUserSelection().getCustomProperty("cp_OperationFilter");
		if (selectedOperationFilter !=null) 
		{				
			if (selectedOperationFilter.equalsIgnoreCase("campaign"))
			{	
				selecteddataUid = (String) userContext.getUserSelection().getCustomProperty("cp_operationCampaignUid");
				if (selecteddataUid == null) selecteddataUid = "";
				
				selectedField = "operationCampaignUid";
				selectedCond = "o.operationCampaignUid = :operationCampaignUid";
				
				String strSql="select campaignName from Campaign where (isDeleted is null or isDeleted = False) and campaignUid=:thisCampaginUid";
				String[] paramsFields = {"thisCampaginUid"};
				Object[] paramsValues = {selecteddataUid};
				
				List lstData = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				if (lstData.size()>0){
					for(Object item : lstData)
					{
						if (item != null)
						{
							campaignName = String.valueOf(item);
						}
					}
				}
			}
			else if (selectedOperationFilter.equalsIgnoreCase("well"))
			{
				selecteddataUid = (String) userContext.getUserSelection().getCustomProperty("cp_wellUid");
				if (selecteddataUid == null) selecteddataUid = "";
				selectedField = "wellUid";
				selectedCond = "o.wellUid = :wellUid";	
				
				String strSql="select wellName from Well where (isDeleted is null or isDeleted = False) and wellUid=:thisWellUid";
				String[] paramsFields = {"thisWellUid"};
				Object[] paramsValues = {selecteddataUid};
				
				List lstData = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				if (lstData.size()>0){
					for(Object item : lstData)
					{
						if (item != null)
						{
							wellName = String.valueOf(item);
						}
					}
				}
			}				
		}
		
		String selectedOperationCode = (String) userContext.getUserSelection().getCustomProperty("cp_operationCodeFilter");
		if (StringUtils.isNotBlank(selectedOperationCode)){
			selectedOpType = "o.operationCode = '" + selectedOperationCode + "'";
		}
		
		if(selecteddataUid.equalsIgnoreCase("showall"))
		{
			viewFilter = "ALL";
			selectedField = "";
			selectedCond = "";
		}		
		
		if (!selectedCond.equalsIgnoreCase("")) selectedCond = " and " + selectedCond;
		
		if (!selectedOpType.equalsIgnoreCase("")) selectedOpType = " and " + selectedOpType;
		
		//get the all operation under selected well
		String sql = "SELECT o.operationUid,o.operationName,o.operationSummary,o.operationCampaignUid,o.operationCode,o.operationalUnit,o.operationalCode,o.sysOperationLastDatetime FROM Operation o WHERE (o.isDeleted = FALSE OR o.isDeleted IS NULL) AND " +
		             "(((o.tightHole = FALSE OR o.tightHole IS NULL) AND (o.isCampaignOverviewOperation = FALSE OR o.isCampaignOverviewOperation IS NULL) " +
		             "AND (o.operationUid NOT IN (SELECT a.operationUid FROM OpsTeamOperation a, OpsTeam e WHERE e.opsTeamUid = a.opsTeamUid AND " +
		             "(a.isDeleted = false OR a.isDeleted IS NULL) AND (e.isPublic = false OR e.isPublic IS NULL)) OR o.operationUid IN " +
		             "(SELECT b.operationUid FROM OpsTeamOperation b, OpsTeamUser c WHERE b.opsTeamUid = c.opsTeamUid AND (b.isDeleted = FALSE OR b.isDeleted is null) AND " +
		             "(c.isDeleted = FALSE OR c.isDeleted IS NULL) AND c.userUid = :userUid))) OR (o.tightHole = TRUE AND o.operationUid IN (SELECT d.operationUid FROM " +
		             "TightHoleUser d WHERE (d.isDeleted = FALSE OR d.isDeleted IS NULL) AND d.userUid = :userUid)))" + selectedCond + selectedOpType + " GROUP BY o.operationUid,o.operationName,o.operationSummary,o.operationCampaignUid," +
		             "o.operationCode,o.operationalUnit,o.operationalCode,o.sysOperationLastDatetime order by o.sysOperationLastDatetime DESC" ;
		
		String[] paramsFields = null;
		Object[] paramsValues = null;
		if (StringUtils.isNotBlank(selectedField)){
			paramsFields = new String[]{"userUid", selectedField};
			paramsValues = new Object[]{currentUserUid, selecteddataUid}; 
		}else{
			paramsFields = new String[]{"userUid"};
			paramsValues = new Object[]{currentUserUid}; 
		}
			
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
		
		if (lstResult.size()>0){
			
			ReportDataNode currentDataNode = reportDataNode.addChild("ReportInfo");
			currentDataNode.addProperty("campaignName", campaignName);
			currentDataNode.addProperty("wellname", wellName);
			currentDataNode.addProperty("welluid", currentWellUid);
			currentDataNode.addProperty("viewFilter", viewFilter);
						
			String operationUid="";	
			String operationName="";	
			String operationSummary = "";
			String operationCode = "";
			currentDataNode = reportDataNode.addChild("WellHistorySummary");
			
			campaignLookup = LookupManager.getConfiguredInstance().getLookup("db://Campaign?key=campaignUid&value=campaignName&cache=false", userContext.getUserSelection(), null);
			operationCodeLookup = LookupManager.getConfiguredInstance().getLookup("xml://operationtype?key=code&value=label", userContext.getUserSelection(), null);
			operationalUnitLookup = LookupManager.getConfiguredInstance().getLookup("xml://well.operationalUnit?key=code&value=label", userContext.getUserSelection(), null);
			cascadeLookupCategory = LookupManager.getConfiguredInstance().getCompleteCascadeLookupSet("xml://well.operationalCode?key=code&value=label", userContext.getUserSelection());
			
			strOpType = getLookupValue(operationCodeLookup, selectedOperationCode);
			currentDataNode.addProperty("selectedOperationCode", strOpType);
			
			String strSql = "select Min(d.reportDatetime) as startDate,Max(d.reportDatetime) as EndDate from ReportDaily d WHERE (d.isDeleted = FALSE OR d.isDeleted IS NULL) AND d.operationUid =:operationUid GROUP BY d.operationUid";
			List<Object[]> lstReportDaily = null;
			
			Integer RecordCounter=0;
			for(Object[] item : lstResult)
			{
				operationUid = item[0].toString();				
				operationName = item[1].toString();
				if (item[6] !=null)
					operationCode = item[4].toString();
				else
					operationCode = "";
				
				ReportDataNode childDataNode = currentDataNode.addChild("Data");
				childDataNode.addProperty("operationUid", operationUid);
				childDataNode.addProperty("operationName", operationName);
				
				if (item[2] !=null)
					operationSummary = item[2].toString();
				else
					operationSummary = "";
				childDataNode.addProperty("operationSummary", operationSummary);
									
				childDataNode.addProperty("operationCampaign", getLookupValue(campaignLookup, item[3]));
			
				lstReportDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid, qp);
				if (lstReportDaily.size()>0){
					Object[] reportDailyObj = lstReportDaily.get(0);
					
					childDataNode.addProperty("startDate", reportDailyObj[0].toString());
					childDataNode.addProperty("endDate", reportDailyObj[1].toString());
				}
				else{
					childDataNode.addProperty("startDate", null);
					childDataNode.addProperty("endDate", null);
				}
			
				
				if (item[7] !=null)
					childDataNode.addProperty("sysOperationLastDatetime", item[7].toString());
				else
					childDataNode.addProperty("sysOperationLastDatetime", "");
				
				strIntvEvent = "";
				strIntvEventTmp = "";
				
				strIntvEvent = getLookupValue(operationCodeLookup, operationCode);
				strIntvEventTmp = getLookupValue(operationalUnitLookup, item[5]);
				if(StringUtils.isNotBlank(strIntvEventTmp))	strIntvEvent = strIntvEvent + "-" + strIntvEventTmp;
				strIntvEventTmp = getcascadeLookupValue(cascadeLookupCategory, item[6]);
				if(StringUtils.isNotBlank(strIntvEventTmp))	strIntvEvent = strIntvEvent + "-" + strIntvEventTmp;				
				childDataNode.addProperty("InterventionEvent", strIntvEvent);
				
				String strFiles="";
				strFiles= getDDRReportList(operationUid, operationName,operationCode,userContext.getUserSelection()) ;
				strFiles = getDGRReportList(strFiles,operationUid, operationName,userContext.getUserSelection());
				strFiles = getLessonTicketList(strFiles, operationUid,userContext.getUserSelection());
								
				childDataNode.addProperty("ReportList", strFiles);
				
				//load file manager file list 
				Integer maxNumberFilesToShow = this.getMaxUploadedFileToShow(userContext.getUserSelection());
				String fileMangerList = "";
				fileMangerList = CommonUtil.getConfiguredInstance().loadFileManagerFileListForOIC(operationUid, maxNumberFilesToShow);
				if (StringUtils.isNotBlank(fileMangerList)) {
					childDataNode.addProperty("fileManagerList", SplitFile(fileMangerList));
				}
				else
					childDataNode.addProperty("fileManagerList", "");
				
				
				RecordCounter++;
				if (selectedOutputRecord >0 && RecordCounter >= selectedOutputRecord) break;
			}	
		}				
	}
	
	private String getLookupValue(Map<String,LookupItem> lookupList, Object lookupvalue) throws Exception {
		String retValue = "";
		
		if (lookupvalue !=null && StringUtils.isNotBlank(lookupvalue.toString())  && lookupList !=null){
			 LookupItem lookup = lookupList.get(lookupvalue.toString());
			if (lookup !=null)	retValue = lookup.getValue().toString();					
		} 
		
		return retValue;
	}
	
	private String getcascadeLookupValue(CascadeLookupSet[] cascadeLookupCategory, Object lookupvalue) throws Exception {
		  
		for(CascadeLookupSet cascadeLookup : cascadeLookupCategory){
			Map<String, LookupItem> lookupList = cascadeLookup.getLookup();
			if (lookupList != null) {
				LookupItem lookupItem = lookupList.get(lookupvalue);
				if (lookupItem != null)return (String) lookupItem.getValue();				
			}
		}
		return "";
	}
	
	private Integer getMaxUploadedFileToShow(UserSelectionSnapshot userSelection) throws Exception {
		String maxFilesNumber = GroupWidePreference.getValue(userSelection.getGroupUid(), "maxUploadedFilesToShowInOIC");
		Integer intMaxFilesNumber = 9;
		if(maxFilesNumber != null){
			intMaxFilesNumber = Integer.parseInt(maxFilesNumber) - 1;
		}
		return intMaxFilesNumber;
	} 
	
	private String SplitFile(String fileList) throws Exception{
		String Filebuffer= "";
		
		String[] files = fileList.split(",");
		for (String strfiles : files) {
			
			String[] file = strfiles.split("&");
			for (String strfile : file) {
				if (strfile.toLowerCase().indexOf("filename=")>=0)
				{					
					if (StringUtils.isNotBlank(Filebuffer))Filebuffer += System.getProperty("line.separator");
					Filebuffer = Filebuffer + strfile.substring(9);;
					break;
				}
			}			
		}			
		return Filebuffer;	
	}
	
	private int getMaxReportToShow(UserSelectionSnapshot userSelection) throws Exception {
		String maxReportNumber = GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_MAX_REPORT_TO_SHOW_IN_OIC);
		int intMaxReportNumber = 6;
		if(maxReportNumber != null){
			intMaxReportNumber = Integer.parseInt(maxReportNumber) - 1;
		}
		return intMaxReportNumber;
	}
	
	private String getDGRReportList(String strFileList, String operationUid, String operationName,UserSelectionSnapshot userSelection) throws Exception {
		int intMaxReportNumber = this.getMaxReportToShow(userSelection);
		String strReportName="";
		
		Integer intCounter = 0;
		String[] paramsFields = {"thisOperation"};
		String[] paramsValues = {operationUid};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation AND (rd.reportType='DGR') AND (rf.reportType='DGR') ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues);
		
		for(Iterator i = items.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			DGRReportList dgr = new DGRReportList();
			ReportFiles thisReportFile = (ReportFiles) obj_array[0];
			dgr.setData(thisReportFile, operationName);
			File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());
			
			if(report_file.exists()){
				strReportName = dgr.getReportDisplayName();
				if (StringUtils.isNotBlank(strFileList))strFileList += System.getProperty("line.separator");
				strFileList = strFileList + strReportName;				
				intCounter++;
				if (intCounter > intMaxReportNumber) break;
			}
		}
		
		return strFileList;
	}
	
	private String getDDRReportList(String operationUid, String operationName,String operationCode,  UserSelectionSnapshot userSelection) throws Exception {
		int intMaxReportNumber = this.getMaxReportToShow(userSelection);
	
		String strFileList="", strReportName="";
		Integer intCounter = 0;
		String thisReportType = "DDR";
		String[] paramsFields = {"thisOperation", "reportDailyReportType", "reportFilesReportType"};
		String[] paramsValues = {operationUid, thisReportType, thisReportType};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rd.reportType=:reportDailyReportType and rf.reportType=:reportFilesReportType ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues);
					
		if(items.size() < 1)
		{
			Map<String, String> reportTypePriorityWhenDrllgNotExist = CommonUtils.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist();
			
			String strReportTypes = reportTypePriorityWhenDrllgNotExist.get(operationCode);
			if (strReportTypes != null) {
				String[] reportTypes = strReportTypes.split("[,]");
				for (String reportType : reportTypes) {
					String[] paramsValues2 = {operationUid, reportType, reportType};
					thisReportType = reportType;
					items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rd.reportType=:reportDailyReportType and rf.reportType=:reportFilesReportType ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues2);
					
					if(items.size() > 0)
					{
						break;
					}
				}
			}
		}
				
		for(Iterator i = items.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			DDRReportList ddr = new DDRReportList();
			ReportFiles thisReportFile = (ReportFiles) obj_array[0];
			ddr.setData(thisReportFile, operationName);
						
			File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());
			
			if(report_file.exists()){
				strReportName = ddr.getReportDisplayName();
				if (StringUtils.isNotBlank(strFileList))strFileList += System.getProperty("line.separator");
				strFileList = strFileList + strReportName;	
				
				intCounter++;
				if (intCounter > intMaxReportNumber) break;
			}
		}
		return strFileList;
	}
	
	
	private String getLessonTicketList(String strFileList, String operationUid, UserSelectionSnapshot userSelection) throws Exception {
		
		String[] paramsFields = {"thisOperation"};
		String[] paramsValues = {operationUid};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select d.dayNumber, d.dailyUid, d.dayDate, l.lessonTicketUid, l.lessonTitle" +
				" FROM Daily d, LessonTicket l WHERE (d.isDeleted = false or d.isDeleted is null) and " +
				"(l.isDeleted = false or l.isDeleted is null) and d.operationUid=:thisOperation AND " +
				"l.dailyUid=d.dailyUid order by d.dayDate DESC", paramsFields, paramsValues);
		
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
		
		for(Iterator i = items.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
				
			String dDate = df.format(obj_array[2]);			
			String dayDate = URLEncoder.encode(dDate, "utf-8");
			String lessonTicketTitle = URLEncoder.encode(this.nullToEmptyString(obj_array[4]), "utf-8");
			
			if (StringUtils.isNotBlank(strFileList))strFileList += System.getProperty("line.separator");
			strFileList = strFileList + "[" + dayDate  + "] " + lessonTicketTitle;
		}
		
		return strFileList;

	}
	
	private String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}
}
