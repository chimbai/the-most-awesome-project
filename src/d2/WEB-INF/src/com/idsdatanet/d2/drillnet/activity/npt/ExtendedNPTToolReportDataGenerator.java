package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.report.ReportOption;

public class ExtendedNPTToolReportDataGenerator extends NPTReportDataGenerator{

	protected void generateActualData(UserContext userContext, Date startDate, Date endDate, ReportDataNode reportDataNode) throws Exception
	{
		List<NPTToolSummary> includedList  = shouldIncludeNPTSummary(userContext, startDate, endDate,this.getUseCountry());
		includeQueryData(includedList,reportDataNode,userContext.getUserSelection(),startDate,endDate);
	}
		
	public static List<NPTToolSummary> generatedata(UserContext userContext, Date startDate, Date endDate, String includeWellUid, Boolean useCountry) throws Exception
	{
		List<NPTToolSummary> includedList = new ArrayList<NPTToolSummary>();
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String sql = "SELECT DISTINCT w.wellUid, o.operationUid, w.country,"+BHIQueryUtils.getQuerySQL(useCountry)+", b.bharunUid, b.timeIn, b.timeOut, b.depthInMdMsl, b.depthOutMdMsl, b.toolCategory, b.directionalCompany, b.toolSize ,d.dailyUid, d.dayDate,a.sections, sum(a.activityDuration) as duration"+
			" FROM Well w,Wellbore wb, Operation o, Bharun b, Daily d, Activity a"+
			" Where w.wellUid=wb.wellUid and wb.wellboreUid=o.wellboreUid"+
			" and d.operationUid=o.operationUid"+ 
			" and o.operationUid=b.operationUid"+ 
			" and d.dailyUid=a.dailyUid"+
			" and (w.isDeleted is null or w.isDeleted = false) and (wb.isDeleted is null or wb.isDeleted = false) and (o.isDeleted is null or o.isDeleted = false)"+
			" and (d.isDeleted is null or d.isDeleted = False)"+ 
			" and (b.isDeleted is null or b.isDeleted = False)"+
			" and (a.isDeleted is null or a.isDeleted = False)"+
			" and (a.startDatetime>=b.timeIn)and (a.endDatetime<=b.timeOut)"+
			" and (a.internalClassCode='TP' or a.internalClassCode='TU') " +
			" and (a.carriedForwardActivityUid is null or a.carriedForwardActivityUid ='' ) "+
			" and (d.dayDate>=:startDate and d.dayDate<=:endDate)"+
			" and a.depthMdMsl is not null"+
			" and (b.toolCategory in (:toolCategory))"+
			" and (b.toolSize in (:toolSize))"+
			" and (b.directionalCompany in (:companyName))"+
			BHIQueryUtils.getQueryFilter(useCountry)+
			" and w.wellUid=:wellUid"+
			" group by o.operationUid,b.bharunUid,d.dailyUid, a.sections"+
			" order by b.directionalCompany, d.dayDate, b.toolCategory";
		String[] paramNames = {"startDate","endDate","toolCategory","companyName","toolSize",BHIQueryUtils.getQueryParamName(useCountry),"wellUid"};
		Object[] values = {startDate,endDate,BHIQueryUtils.getReportOptionValue(userContext, BHIQueryUtils.CONSTANT_TOOL_CATEGORY),BHIQueryUtils.getReportOptionValue(userContext, BHIQueryUtils.CONSTANT_COMPANY),BHIQueryUtils.getReportOptionValue(userContext, BHIQueryUtils.CONSTANT_TOOL_SIZE,true),BHIQueryUtils.getQueryParamValue(userContext, useCountry),includeWellUid};
		
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, values,qp);
		
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
		if (result.size()>0)
		{
			for (Object[] item:result)
			{
				String wellUid = (String)item[0];
				String operationUid = (String)item[1];
				String country = (String)item[2];
				String bharunUid = (String)item[4];
				Date timeIn = (Date)item[5];
				Date timeOut = (Date)item[6];
				Double depthFromMdMsl = (Double) item[7];
				Double depthToMdMsl = (Double) item[8];
				String toolCategory = (String) item[9];
				String directionalCompany = (String) item[10];
				Double toolSize =(Double) item[11];
				String dailyUid =(String) item[12];
				Date dayDate =(Date) item[13];
				String section = (String) item[14];
				Double duration = (Double) item[15];
				
				thisConverter.setBaseValue(duration);
				duration = thisConverter.getConvertedValue();
				
				NPTToolSummary nptTool = new NPTToolSummary(wellUid, operationUid, country,	directionalCompany, null, duration,	dailyUid, dayDate);
				BHIQueryUtils.setCountryOrArea(nptTool, useCountry, (String)item[3]);
				nptTool.setDepthFromMdMsl(depthFromMdMsl);
				nptTool.setBharunUid(bharunUid);
				nptTool.setDepthToMdMsl(depthToMdMsl);
				nptTool.setTimeIn(timeIn);
				nptTool.setTimeOut(timeOut);
				nptTool.setToolCategory(toolCategory);
				nptTool.setToolSize(toolSize);
				nptTool.setSections(section);
				
				includedList.add(nptTool);
					
			}
		}
		return includedList;
	}
	
	private void includeQueryData(List<NPTToolSummary> toolsNPT, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception
	{
		ReportOption options = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_QUERIES)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_QUERIES):null;
		if (options.getValues().contains("Q7"))
			this.includeNPTPerToolType(toolsNPT, node, userSelection, start, end);
	}
	
	private void includeNPTPerToolType(List<NPTToolSummary> toolsNPT, ReportDataNode node, UserSelectionSnapshot userSelection, Date start, Date end) throws Exception
	{
		
		ReportDataNode nodeNPTPerToolType = node.addChild("NPTPerToolType"); 
		
		ReportOption cats = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_REPORT_GENERATION_CATEGORY):null;
		ReportOption cos = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_COMPANY):null;
		ReportOption toolCats = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_TOOL_CATEGORY)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_TOOL_CATEGORY):null;
		
		if (cats!=null){
			for (String cat : cats.getValues()){
				ReportDataNode child = nodeNPTPerToolType.addChild(cats.getValue(cat).toString().replace(" ", "_"));
				if (cos!=null){
					for (String coName : cos.getValues()){
						if (toolCats!=null){
							for (String toolCat : toolCats.getValues()){
								List<Date> dates = DateRangeUtils.getDateList(start, end, false, cat);
								for (Date date: dates){
									Double duration =0.00;
									String formatLabel = DateRangeUtils.formatDateLabel(date, cat,true);
									for (NPTToolSummary tool : toolsNPT)
									{
										String formatLabelData = DateRangeUtils.formatDateLabel(tool.getDayDate(), cat,true);
										if (tool.getLookupCompanyUid().equals(coName) && tool.getToolCategory().equalsIgnoreCase(toolCat) && formatLabel.equalsIgnoreCase(formatLabelData))
											duration += tool.getDuration();
									}
									ReportDataNode catChild = child.addChild("data");
									catChild.addProperty("lookupCompanyUid", coName);
									catChild.addProperty("lookupCompanyUid_lookupLabel", cos.getValue(coName).toString());
									catChild.addProperty("toolCategory", toolCat);
									catChild.addProperty("toolCategory_lookupLabel", toolCats.getValue(toolCat).toString());
									catChild.addProperty("label", formatLabel);
									catChild.addProperty("duration", duration.toString());
								}
							}
						}
					}
				}
			}
		}
		
	}
	protected static List<NPTToolSummary> shouldIncludeNPTSummary(UserContext userContext, Date startDate, Date endDate, Boolean useOperationCode) throws Exception
	{
		UserSelectionSnapshot userSelection = userContext.getUserSelection();
		ReportOption wells = userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL)!=null? (ReportOption) userSelection.getCustomProperty(BHIQueryUtils.CONSTANT_WELL):null;
		List<NPTToolSummary> result = new ArrayList<NPTToolSummary>();
		for (String wellUid : wells.getValues())
		{
			result.addAll(generatedata(userContext, startDate, endDate,wellUid,useOperationCode));
		}
		return result;
	}
}
