package com.idsdatanet.d2.drillnet.wellHistoryOverview;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.WellHead;
import com.idsdatanet.d2.core.model.WellHeadDetail;
import com.idsdatanet.d2.core.model.WellHeadDetailLog;
import com.idsdatanet.d2.core.model.WellHeadSection;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WellHeadHistoryOverviewDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{

	private String  currentRef= "";
	private Integer autoSeqNumber = 0;
		
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		String selectedScreenOutputFilter = (String) commandBean.getRoot().getDynaAttr().get("ScreenOutput");
		if (selectedScreenOutputFilter==null) selectedScreenOutputFilter = "";
		node.getDynaAttr().put("screenOption", selectedScreenOutputFilter);
		
		// a trick to only load used Operation object (initially load as String, only load the real object when needed)
		if((obj instanceof String) && meta.getTableClass() == WellHeadDetailLog.class){
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM WellHeadDetailLog WHERE wellHeadDetailLogUid=:wellHeadDetailLogUid", "wellHeadDetailLogUid", obj.toString());
			obj = lstResult.get(0);
			node.setData(obj);
		}
		
		String nodeRef = "";
		
		if (obj instanceof WellHeadDetailLog){
			WellHeadDetailLog wellheadDetailLog = (WellHeadDetailLog) obj;
			
			String wellHeadDetailUid = wellheadDetailLog.getWellHeadDetailUid();
			nodeRef = wellheadDetailLog.getOperationUid();
		
			if (StringUtils.isNotBlank(wellHeadDetailUid)) {
				WellHeadDetail wellheadDetaill = (WellHeadDetail) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(WellHeadDetail.class, wellHeadDetailUid);
				
				if (wellheadDetaill !=null) {

					CustomFieldUom thisConverter = new CustomFieldUom(userSelection.getLocale());
					
					node.getDynaAttr().put("type", wellheadDetaill.getDetailType());
					node.getDynaAttr().put("manufacturer", wellheadDetaill.getManufacturer());
					node.getDynaAttr().put("serialNo", wellheadDetaill.getSerialNo());
					node.getDynaAttr().put("partNo", wellheadDetaill.getPartNo());
					node.getDynaAttr().put("comment", wellheadDetaill.getComment());
					node.getDynaAttr().put("status", wellheadDetaill.getStatus());
										
					if (wellheadDetaill.getCheckDateTime()!=null)
					{	
						SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
						node.getDynaAttr().put("CheckDateTime", sdf.format(wellheadDetaill.getCheckDateTime()));
					}
					
					generateUomField(node,thisConverter,"topPressureRating", wellheadDetaill.getTopPressureRating());
					generateUomField(node,thisConverter,"size", wellheadDetaill.getSize());
								
					
					String wellheadSectionUid = wellheadDetaill.getWellHeadSectionUid();
					
					if (StringUtils.isNotBlank(wellheadSectionUid)) {
						WellHeadSection wellheadSection = (WellHeadSection) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(WellHeadSection.class, wellheadSectionUid);
						if (wellheadSection !=null) {
														
							nodeRef = nodeRef + wellheadSection.getSectionCode() + wellheadSection.getWellHeadUid();
							
							if (!currentRef.equalsIgnoreCase(nodeRef))
							{
								currentRef = nodeRef.toLowerCase();
								autoSeqNumber = 1;
							}
							else
								autoSeqNumber = autoSeqNumber + 1;
							
							node.getDynaAttr().put("autoSeqNumber", autoSeqNumber);
							node.getDynaAttr().put("sectionCode", wellheadSection.getSectionCode() + " - " + wellheadSection.getDescription());
							node.getDynaAttr().put("sectionDesc", wellheadSection.getDescription());
					
							String wellheadUid = wellheadSection.getWellHeadUid();
							WellHead wellhead = (WellHead) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(WellHead.class, wellheadUid);
							
							if (wellhead !=null) {
								node.getDynaAttr().put("wellHeadRef", wellhead.getReferenceNo());
								node.getDynaAttr().put("wellHeadUid", wellhead.getWellHeadUid());
								
								if (wellhead.getOperationUid()!=null){
									Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, wellhead.getOperationUid());
									node.getDynaAttr().put("operationName", operation.getOperationName());
								}
							}
							
						}
					}
					
				}
			}
		}
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		return true;
	}

	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
	
		boolean showLatest = false;
		
		String selectedScreenOutputFilter = (String) commandBean.getRoot().getDynaAttr().get("ScreenOutput");
			
		if (selectedScreenOutputFilter !=null){				
				if (selectedScreenOutputFilter.equalsIgnoreCase("latest")) showLatest =true;
		}
	
		
		if (meta.getTableClass().equals(WellHeadDetailLog.class)) {
		
			String sqlFilter = "";
			String[] paramsFields = null;
			Object[] paramsValues = null;	
			
			if (showLatest){					
				String WellHeadUid= "";
				/*String sql = "select a.wellHeadUid from WellHead a,Operation b where a.operationUid = b.operationUid and (a.isDeleted is null or a.isDeleted = false) " +
							 "and (b.isDeleted is null or b.isDeleted = false) and a.wellboreUid =:wellboreUid order by a.installDateTime desc,b.sysOperationStartDatetime desc";
				*/			 
				
				String sql = "select wellHeadUid from WellHead where (isDeleted is null or isDeleted = false) and wellboreUid =:wellboreUid order by installDateTime desc";
			
				List lstData = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql ,"wellboreUid", userSelection.getWellboreUid());
				
				if (lstData.size()>0){
					for(Object item : lstData){
						if (item != null){
							WellHeadUid = String.valueOf(item);
						}
						break;
					}
				}
				sqlFilter = "a.wellHeadUid=:wellHeadUid";
				paramsFields = new String[] {"wellHeadUid"};
				paramsValues = new Object[] {WellHeadUid};	
			}	
			else{
				sqlFilter = "d.wellboreUid=:wellboreUid";
				paramsFields = new String[] {"wellboreUid"};
				paramsValues = new Object[] {userSelection.getWellboreUid()};				
			}
			
			String sql = "select d.wellHeadDetailLogUid,d.wellHeadDetailUid from WellHead a, WellHeadSection b, WellHeadDetail c, WellHeadDetailLog d where (a.isDeleted = false or a.isDeleted is null) and " +
					"(b.isDeleted = false or b.isDeleted is null) and (c.isDeleted = false or c.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and " +
					"a.wellHeadUid = b.wellHeadUid and b.wellHeadSectionUid = c.wellHeadSectionUid and c.wellHeadDetailUid = d.wellHeadDetailUid and " + sqlFilter +
					" group by d.wellHeadDetailLogUid order by d.operationUid,a.referenceNo, b.sectionCode, c.sequence, d.serviceDateTime DESC";
		
			List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql ,paramsFields, paramsValues);
			
			List<Object> output_maps = new ArrayList<Object>();
		
			ArrayList filterList = new ArrayList<String>();
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] item = (Object[])i.next();
				String logUid = item[0].toString();
				String wellHeadDetailUid =  item[1].toString(); 				
			
				if (showLatest){
					if (filterList.contains(wellHeadDetailUid)) 
						continue;
					else
						filterList.add(wellHeadDetailUid);
				}
				output_maps.add(logUid);
			}
			return output_maps;
		}
		return null;
	}

	private void generateUomField(CommandBeanTreeNode node,CustomFieldUom thisConverter,String DataField, Object Data ) throws Exception{
		
		String formattedvalue = "";
		
		thisConverter.setReferenceMappingField(WellHeadDetail.class, DataField);
		
		if (Data != null)
		{
			if (thisConverter.isUOMMappingAvailable()){
				thisConverter.setBaseValueFromUserValue(Double.parseDouble(Data.toString()));
				formattedvalue = thisConverter.getFormattedValue();	
			}
			else{
				formattedvalue = Data.toString();
			}			
		}				
		
		node.getDynaAttr().put(DataField + "WithUomSymbol", formattedvalue);
	}
}
