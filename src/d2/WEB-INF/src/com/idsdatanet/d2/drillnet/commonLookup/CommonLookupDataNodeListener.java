package com.idsdatanet.d2.drillnet.commonLookup;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CommonLookupDataNodeListener extends EmptyDataNodeListener {
	
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession session, CommandBeanTreeNode parent) throws Exception
	{
		if ("CommonLookupDepot".equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted())
		{
			Object obj = parent.getData();
			if (obj instanceof CommonLookup)
			{
				CommonLookup cl = (CommonLookup) obj;
				CommonLookupUtil.updateCommonLookupDepotString(cl.getCommonLookupUid());
			}
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if(obj instanceof CommonLookup){
			CommonLookup thisCommonLookup = (CommonLookup) obj;
			String selectedLookupTypeSelection = null;
			
			if(commandBean.getRoot().getDynaAttr().get("lookupTypeSelection")!=null && !commandBean.getRoot().getDynaAttr().get("lookupTypeSelection").toString().isEmpty()){
				selectedLookupTypeSelection = (String) commandBean.getRoot().getDynaAttr().get("lookupTypeSelection");
				thisCommonLookup.setLookupTypeSelection(selectedLookupTypeSelection);
			}else{
				status.addError("Please select Lookup Type before add new record");
				status.setContinueProcess(false);
			}
		}
	}
	
}
