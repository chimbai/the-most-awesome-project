package com.idsdatanet.d2.drillnet.wellexplorer;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellExplorerFlashComponentConfiguration extends AbstractFlashComponentConfiguration {
	public String getFlashvars(UserSession userSession, HttpServletRequest request, String sessionId, String targetSubModule) throws Exception{
		String serviceUrl = "../../webservice/wellexplorerservice.html";
		String reportDailyDDRUrl = "../../reportdaily.html";
		String reportDailyDGRUrl = "../../reportdailydgr.html";
		
		return "d2Url=" + urlEncode(serviceUrl)
		+ "&baseUrl=" + urlEncode(userSession.getClientBaseUrl())
		+ "&reportDailyDDRUrl=" + urlEncode(reportDailyDDRUrl)
		+ "&reportDailyDGRUrl=" + urlEncode(reportDailyDGRUrl);
	}
	
	public String getFlashComponentId(String targetSubModule){
		return "wellexplorer";
	}
}
