package com.idsdatanet.d2.drillnet.opsteam;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.OpsTeam;
import com.idsdatanet.d2.core.model.OpsTeamUser;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class OpsTeamDataNodeListener extends EmptyDataNodeListener {
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		if (request != null) {
			node.getDynaAttr().put("currentView", UserSession.getInstance(request).getCurrentView());
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		if (request != null) {
			node.getDynaAttr().put("currentView", UserSession.getInstance(request).getCurrentView());
		}
	}
	
	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		if (node.getData() instanceof OpsTeam) {
			if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW || operationPerformed == BaseCommandBean.OPERATING_MODE_WITSML) {
				List<String> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT u.userUid FROM User u WHERE (u.isDeleted = false OR u.isDeleted IS NULL) AND u.userName = :userName AND u.groupUid = :groupUid", new String[] {"userName", "groupUid"}, new Object[] {"idsadmin", session.getCurrentGroupUid()});
				if (rs != null && !rs.isEmpty()) {
					OpsTeamUser u = new OpsTeamUser();
					u.setOpsTeamUid(((OpsTeam) node.getData()).getOpsTeamUid());
					u.setUserUid(rs.get(0));
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(u);
				}
			}
		}
	}
	
	@Override
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		if (node.getData() instanceof OpsTeam) {
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE OpsTeamOperation SET isDeleted = true WHERE opsTeamUid = :opsTeamUid", new String[] {"opsTeamUid"}, new Object[] {((OpsTeam) node.getData()).getOpsTeamUid()});
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE OpsTeamUser SET isDeleted = true WHERE opsTeamUid = :opsTeamUid", new String[] {"opsTeamUid"}, new Object[] {((OpsTeam) node.getData()).getOpsTeamUid()});
			
			commandBean.getRoot().resetPersistedDynamicAttribute("groupUid");
			commandBean.getRoot().resetPersistedDynamicAttribute("opsTeamUid");
		}
	}

}
