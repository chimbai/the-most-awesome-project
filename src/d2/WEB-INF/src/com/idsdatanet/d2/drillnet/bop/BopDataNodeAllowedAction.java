package com.idsdatanet.d2.drillnet.bop;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.Bop;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class BopDataNodeAllowedAction implements DataNodeAllowedAction {

	public boolean allowedAction(CommandBeanTreeNode node, UserSession session,
			String targetClass, String action, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "singleActiveBopInstalled"))) {
			if(StringUtils.equals(targetClass, "Bop")) {
					String rigInformationUid = session.getCurrentRigInformationUid();
					Boolean enabled = true;
					if (StringUtils.isNotBlank(rigInformationUid)) {
						String strSql = "FROM Bop WHERE (isDeleted = false or isDeleted is null) and rigInformationUid =:rigInformationUid";
						List <Bop> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigInformationUid", rigInformationUid);
						if (lstResult.size() > 0 && lstResult!=null){
							
							for(Bop objValue:lstResult)
							{
								if(objValue.getRemoveDate()==null)
								{
									enabled = false;
								}
							}
						}
					}
					
					return enabled;
				}
				
			
		}
		return true;
	}
	
}

