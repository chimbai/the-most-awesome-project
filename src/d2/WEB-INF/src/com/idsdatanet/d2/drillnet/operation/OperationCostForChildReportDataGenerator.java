package com.idsdatanet.d2.drillnet.operation;

import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class OperationCostForChildReportDataGenerator implements ReportDataGenerator{

	private String defaultReportType;
	
	public void setReportType(String value){
		this.defaultReportType = value;
	}
	
	public void disposeOnDataGeneratorExit() {		
		
	}
	
	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String currentDailyUid = userContext.getUserSelection().getDailyUid();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		if (currentDailyUid == null || currentOperationUid == null) return;

		
		Daily currentDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (currentDaily==null) return;
		Date todayDate = currentDaily.getDayDate();
		
		if(this.defaultReportType != null){
			reportType = this.defaultReportType;
		}
		
		String currentWellboreUid = userContext.getUserSelection().getWellboreUid();
		String currencyUomSymbol = null;
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Operation.class, "afeOtherCost");
		
		if (thisConverter.isUOMMappingAvailable())
		{		
			currencyUomSymbol=thisConverter.getUomSymbol();			
		}
		
		String sql = "FROM Daily WHERE (isDeleted = false or isDeleted is null) and dayDate =:todayDate " +
				"and wellboreUid =:wellboreUid and operationUid !=:currentOperationUid";
		
		List <Daily> lstDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"todayDate", "wellboreUid", "currentOperationUid"}, new Object[] {todayDate, currentWellboreUid, currentOperationUid});
		if(lstDaily.size() > 0){

			for(Daily objDaily: lstDaily){
				
				Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(objDaily.getOperationUid());
				
					if (operation !=null){
						ReportDataNode ChildWellOperationCostNode = reportDataNode.addChild("ChildWellOperationCost");
						ChildWellOperationCostNode.addProperty("operationUid", nullToEmptyString(operation.getOperationUid()));
						ChildWellOperationCostNode.addProperty("afeOtherCost", nullToEmptyString(operation.getAfeOtherCost()));
						ChildWellOperationCostNode.addProperty("operationBudget", nullToEmptyString(operation.getOperationBudget()));
						ChildWellOperationCostNode.addProperty("plannedDuration", nullToEmptyString(operation.getPlannedDuration()));
						ChildWellOperationCostNode.addProperty("operationVesselHourlyCost", nullToEmptyString(operation.getOperationVesselHourlyCost()));
						ChildWellOperationCostNode.addProperty("currencyUomSymbol", nullToEmptyString(currencyUomSymbol));
						
						String actualHourToDate = null;
						String strSql = "select sum(activityDuration) from Activity WHERE (isDeleted = false or isDeleted is null) " +
								"AND (isSimop=false or isSimop is null) " +
								"AND (isOffline=false or isOffline is null) " +
								"AND (dayPlus IS NULL OR dayPlus=0) " +
								"AND dailyUid=:dailyUid";
						List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", objDaily.getDailyUid(), qp);
						
						if (lstResult.size() > 0){
							Object a = lstResult.get(0);
							if (a!=null){
								Double actualDurationToDate = Double.parseDouble(a.toString());
								thisConverter.setReferenceMappingField(Activity.class, "activityDuration");
								if (thisConverter.isUOMMappingAvailable())
								{
									thisConverter.setBaseValue(actualDurationToDate);
									actualHourToDate = thisConverter.formatOutputPrecision();
								}
							}
						}
						
						ChildWellOperationCostNode.addProperty("actualHourToDate", nullToEmptyString(actualHourToDate));
						Double dayToDate = (CommonUtil.getConfiguredInstance().daysOnOperation(operation.getOperationUid(), objDaily.getDailyUid()) / (3600.0*24.0));
						//thisConverter.changeUOMUnit("Day");
						//String actualDayToDate = thisConverter.formatOutputPrecision(dayToDate);
						ChildWellOperationCostNode.addProperty("actualDayToDate", nullToEmptyString(dayToDate));
						//ChildWellOperationCostNode.addProperty("actualDayToDate", nullToEmptyString(actualDayToDate));
						
					}	
			}
			
		}			
		
	}
	
	private String nullToEmptyString(Object value) {
		if (value==null) return "";
		return value.toString();
	}
}