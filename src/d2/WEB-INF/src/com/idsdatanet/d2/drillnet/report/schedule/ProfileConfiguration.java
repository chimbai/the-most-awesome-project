package com.idsdatanet.d2.drillnet.report.schedule;

import java.util.HashMap;
import java.util.Map;

public class ProfileConfiguration {

	private String subject;
	private String templateFile;
	private Boolean validEmail=false;
	private Boolean errorEmail=false;
	private String[] emailList;
	private Boolean includeSupportEmail;
	
	private Map<String,Object> contentMapping= new HashMap<String,Object>();
	
	public ProfileConfiguration(){
		
	}
	
	public ProfileConfiguration (ProfileConfiguration config)
	{
		this.subject=config.subject;
		this.templateFile=config.templateFile;
		this.validEmail=config.validEmail;
		this.errorEmail=config.errorEmail;
		this.contentMapping=config.contentMapping;
		this.emailList=config.emailList;
	}
	
	public void setEmailList(String[] emailList) {
		this.emailList = emailList;
	}
	public String[] getEmailList() {
		return emailList;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSubject() {
		return subject;
	}
	public void setTemplateFile(String templateFile) {
		this.templateFile = templateFile;
	}
	public String getTemplateFile() {
		return templateFile;
	}
	public void addContentMapping (String key, Object value)
	{
		this.contentMapping.put(key,value);
	}	
	public Map<String,Object> getContentMapping()
	{
		return this.contentMapping;
	}
	public void setIncludeSupportEmail(Boolean includeSupportEmail) {
		this.includeSupportEmail = includeSupportEmail;
	}
	public Boolean getIncludeSupportEmail() {
		return includeSupportEmail;
	}
	public void clearContentMapping () {
		this.contentMapping= new HashMap<String,Object>();
	}
	public void setValidEmail(Boolean validEmail) {
		this.validEmail = validEmail;
	}
	public Boolean getValidEmail() {
		return validEmail;
	}
	public void setErrorEmail(Boolean errorEmail) {
		this.errorEmail = errorEmail;
	}
	public Boolean getErrorEmail() {
		return errorEmail;
	}
}
