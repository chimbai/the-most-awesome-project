package com.idsdatanet.d2.drillnet.activity;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityRsd;
import com.idsdatanet.d2.core.model.ActivityExt;
import com.idsdatanet.d2.core.model.ActivityMar;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.ftr.FTRUtils;
import com.idsdatanet.d2.infra.uom.OperationUomContext;

/**
 * ActivityUtil to populate BHA run number based on activity depth.
 * Method to get parent wellbore operation based on current selected wellbore.
 * @author Moses, Jackson
 *
 */
public class ActivityUtils {
	
	public static class ReversedDateComparator implements Comparator<java.util.Date>{
		public int compare(Date d1, Date d2){
			if(d1 != null && d2 != null){
				if(d1.getTime() > d2.getTime()) return -1;
				if(d1.getTime() < d2.getTime()) return 1;
				return 0;
			}else{
				if(d1 == null && d2 == null) return 0;
				if(d1 != null) return -1;
				return 1;
			}
		}
	}
	
	private final static String E_FAIL = "equipment failure";
	private final static String KICK = "kick";
	private static List<String> operationList = null;
	
	public static void populateBharunNumber(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(),GroupWidePreference.GWP_AUTO_POPULATE_BHA_NUMBER))){
			Object obj = node.getData();
			if (obj instanceof Activity)
			{
				Activity act = (Activity) obj;
				String bhaNumber = null;
//				CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(),Activity.class,"depthMdMsl");
//				Double depthMdMsl = null;
//				String bhaNumber = null;
//				if (act.getDepthMdMsl()!=null)
//				{
//					thisConverter.setBaseValueFromUserValue(act.getDepthMdMsl());
//					depthMdMsl = thisConverter.getBasevalue();
//				}
//				String[] paramNames = {"startTime","endTime","operationUid", "depthMdMsl"};
//				Object[] paramValues = {act.getStartDatetime(),act.getEndDatetime(),act.getOperationUid(),depthMdMsl};
				String[] paramNames = {"operationUid", "activityUid"};
			    Object[] paramValues = {act.getOperationUid(), act.getActivityUid()};
				QueryProperties qp = new QueryProperties();
				qp.setDatumConversionEnabled(false);
				qp.setUomConversionEnabled(false);
//				List<Bharun> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From Bharun Where (isDeleted is null or isDeleted = false) and timeIn<=:startTime and timeOut>=:endTime and (depthInMdMsl<=:depthMdMsl and depthOutMdMsl>=:depthMdMsl) and operationUid=:operationUid",paramNames, paramValues,qp);
				List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select b.bhaRunNumber " +
					      "From Bharun b, Activity a " +
					      "Where (b.isDeleted is null or b.isDeleted = false) " +
					      "and (a.isDeleted is null or a.isDeleted = false) " +
					      "and b.timeIn<=a.startDatetime and b.timeOut>=a.endDatetime " +
					      "and (b.depthInMdMsl<=a.depthMdMsl " +
					      "and b.depthOutMdMsl>=a.depthMdMsl) " +
					      "and b.operationUid=:operationUid " +
					      "and a.activityUid=:activityUid ",paramNames, paramValues,qp);
				if (result.size()>0)
					bhaNumber =  result.get(0).toString();
				
				node.getDynaAttr().put("bhaNumber",bhaNumber);
				act.setBhaNumber(bhaNumber);
				
			}
		}
	}
	public static void populateActivityBharunNumber(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(),GroupWidePreference.GWP_AUTO_POPULATE_BHA_NUMBER))){
			Object obj = node.getData();
			if (obj instanceof Bharun)
			{
				Bharun bharun = (Bharun) obj;
				Double depthIn = bharun.getDepthInMdMsl();
				Double depthOut = bharun.getDepthOutMdMsl();
				CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean(),Bharun.class,"depthInMdMsl");
				if (depthIn!=null)
				{	
					thisConverter.setBaseValueFromUserValue(depthIn);
					depthIn = thisConverter.getBasevalue();
				}
				thisConverter.setReferenceMappingField(Bharun.class, "depthOutMdMsl");
				if (depthOut!=null)
				{	
					thisConverter.setBaseValueFromUserValue(depthOut);
					depthOut = thisConverter.getBasevalue();
				}
				String[] paramNames = {"bhaNumber","timeIn","timeOut","operationUid", "depthInMdMsl","depthOutMdMsl"};	
				Object[] paramValues = {bharun.getBhaRunNumber(),bharun.getTimeIn(),bharun.getTimeOut(),bharun.getOperationUid(),depthIn,depthOut};

				String sql = "UPDATE Activity set bhaNumber =:bhaNumber WHERE (isDeleted is null or isDeleted = false) and startDatetime>=:timeIn and endDatetime<=:timeOut and (depthMdMsl>=:depthInMdMsl and depthMdMsl<=:depthOutMdMsl) and operationUid=:operationUid";
				QueryProperties qp = new QueryProperties();
				qp.setDatumConversionEnabled(false);
				qp.setUomConversionEnabled(false);
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sql, paramNames, paramValues,qp);
			}
		}
	}
	
	/**
	 * Method to get parent wellbore operation. 
	 * @param wellboreUid from current selected operation
	 * @return
	 * @throws Exception
	 */
	public static List<String> getParentWellboreOperationList(String wellboreUid) throws Exception {
		
		operationList = new ArrayList();
		
		getParentWellboreOperation(wellboreUid);
		return operationList;
	}
	
	/**
	 * Method to get parent wellbore operation
	 * @param wellboreUid from current selected operation
	 * @throws Exception
	 */
	public static void getParentWellboreOperation(String wellboreUid)  throws Exception{
		List<String> list = null;
		if(operationList == null) operationList = new ArrayList();
		
		if(StringUtils.isNotBlank(wellboreUid)){
			getCurrentWellboreOperation(wellboreUid);
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT parentWellboreUid FROM Wellbore WHERE (isDeleted='' OR isDeleted is null) AND wellboreUid = :wellboreUid", new String[] {"wellboreUid"} , new Object[] {wellboreUid} );
			if(list.size() > 0 && list != null){
				String parentWellboreUid = list.get(0);
				getParentWellboreOperation(parentWellboreUid);				
			}
		}
	}
	
	/**
	 * Method to get operation uid based on current wellbore
	 * @param wellboreUid
	 * @throws Exception
	 */
	public static void getCurrentWellboreOperation(String wellboreUid) throws Exception{
		//to get operations of the current wellbore
		List<String> list = null;
		list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT operationUid FROM Operation WHERE (isDeleted='' OR isDeleted is null) AND wellboreUid = :wellboreUid", new String[] {"wellboreUid"} , new Object[] {wellboreUid} );
		if(list != null && list.size() > 0){
			for(String operationUid : list){
				if(!operationList.contains(operationUid)) operationList.add(operationUid);
			}
		}
	}
	/**
	 * Method to get operation Hour
	 * @param operation
	 * @throws Exception
	 * @return operationHour
	 */
	public static String getOperationHour(Operation operation) throws Exception{
		String operationHour = "24";
		
		if (operation != null) {
			if (StringUtils.isNotBlank(operation.getOperationHour())) {
				operationHour = operation.getOperationHour();
			}
		}
		
		return operationHour;
	}
	/**
	 * Method to get a list of end date time 
	 * @param commandBean
	 * @return endDatetimeList
	 */
	public static List<Date> getEndDatetimeList (CommandBean commandBean) throws Exception{
	
		List<Date> endDatetimeList = new ArrayList<Date>();
		
		for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("Activity").values()) {
			Activity activity = (Activity) node.getData();
			
			if (activity != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {					
				endDatetimeList.add((Date) activity.getEndDatetime());	
			}

		}
		Collections.sort(endDatetimeList, new ReversedDateComparator());
		
		return endDatetimeList;
	}
	
	/**
	 * Method to get a start time, populate start time based on end time 
	 * This method will set the date to 1970-01-01 as time needed to be used for calculation
	 * @param activity
	 * @param commandBean
	 * @return startTime
	 */
	public static Date getPopulatedStartTime(Activity activity, CommandBean commandBean) throws Exception {
			
		Date newDate = new Date();
		
		if(activity !=null){
			Date start = (Date) activity.getStartDatetime();
			Date end = (Date) activity.getEndDatetime();
			
			Date startTime = start;
			
			if(start == null) {
				DateFormat df = new SimpleDateFormat("kkmmss");
				
				boolean found = false;
				for(Date endDatetime : ActivityUtils.getEndDatetimeList(commandBean)) {
					Calendar currentEndDatetime = Calendar.getInstance();
					currentEndDatetime.setTime(end);
					
					Calendar cachedEndDatetime = Calendar.getInstance();
					cachedEndDatetime.setTime(endDatetime);
					
					if(df.format(cachedEndDatetime.getTime()).compareTo(df.format(currentEndDatetime.getTime()))==-1) {						
						
						startTime = CommonUtil.getConfiguredInstance().setDate(cachedEndDatetime.getTime(), newDate);		
						found = true;
						break;
					}
				}
				// if no matches, set to 00:00
				if(!found) {
					Calendar zero = Calendar.getInstance();
					zero.clear();
					zero.set(Calendar.HOUR_OF_DAY, 0);
					zero.set(Calendar.MINUTE, 0);
					zero.set(Calendar.SECOND, 0);
					zero.set(Calendar.MILLISECOND, 0);
					
					startTime = CommonUtil.getConfiguredInstance().setDate(zero.getTime(), newDate);		
				}
			}else {
				startTime = CommonUtil.getConfiguredInstance().setDate(start, newDate);		
			}
			
			return startTime;
		}
		
		return null;
	}
	
	public static Date getPopulatedStartTime(ActivityRsd activity, CommandBean commandBean) throws Exception {
		
		Date newDate = new Date();
		
		if(activity !=null){
			Date start = (Date) activity.getStartDatetime();
			Date end = (Date) activity.getEndDatetime();
			
			Date startTime = start;
			
			if(start == null) {
				DateFormat df = new SimpleDateFormat("kkmmss");
				
				boolean found = false;
				for(Date endDatetime : ActivityUtils.getEndDatetimeList(commandBean)) {
					Calendar currentEndDatetime = Calendar.getInstance();
					currentEndDatetime.setTime(end);
					
					Calendar cachedEndDatetime = Calendar.getInstance();
					cachedEndDatetime.setTime(endDatetime);
					
					if(df.format(cachedEndDatetime.getTime()).compareTo(df.format(currentEndDatetime.getTime()))==-1) {						
						
						startTime = CommonUtil.getConfiguredInstance().setDate(cachedEndDatetime.getTime(), newDate);		
						found = true;
						break;
					}
				}
				// if no matches, set to 00:00
				if(!found) {
					Calendar zero = Calendar.getInstance();
					zero.clear();
					zero.set(Calendar.HOUR_OF_DAY, 0);
					zero.set(Calendar.MINUTE, 0);
					zero.set(Calendar.SECOND, 0);
					zero.set(Calendar.MILLISECOND, 0);
					
					startTime = CommonUtil.getConfiguredInstance().setDate(zero.getTime(), newDate);		
				}
			}else {
				startTime = CommonUtil.getConfiguredInstance().setDate(start, newDate);		
			}
			
			return startTime;
		}
		
		return null;
	}
	
	public static Date getPopulatedStartTime(ActivityExt activity, CommandBean commandBean) throws Exception {
		
		Date newDate = new Date();
		
		if(activity !=null){
			Date start = (Date) activity.getStartDatetime();
			Date end = (Date) activity.getEndDatetime();
			
			Date startTime = start;
			
			if(start == null) {
				DateFormat df = new SimpleDateFormat("kkmmss");
				
				boolean found = false;
				for(Date endDatetime : ActivityUtils.getEndDatetimeList(commandBean)) {
					Calendar currentEndDatetime = Calendar.getInstance();
					currentEndDatetime.setTime(end);
					
					Calendar cachedEndDatetime = Calendar.getInstance();
					cachedEndDatetime.setTime(endDatetime);
					
					if(df.format(cachedEndDatetime.getTime()).compareTo(df.format(currentEndDatetime.getTime()))==-1) {						
						
						startTime = CommonUtil.getConfiguredInstance().setDate(cachedEndDatetime.getTime(), newDate);		
						found = true;
						break;
					}
				}
				// if no matches, set to 00:00
				if(!found) {
					Calendar zero = Calendar.getInstance();
					zero.clear();
					zero.set(Calendar.HOUR_OF_DAY, 0);
					zero.set(Calendar.MINUTE, 0);
					zero.set(Calendar.SECOND, 0);
					zero.set(Calendar.MILLISECOND, 0);
					
					startTime = CommonUtil.getConfiguredInstance().setDate(zero.getTime(), newDate);		
				}
			}else {
				startTime = CommonUtil.getConfiguredInstance().setDate(start, newDate);		
			}
			
			return startTime;
		}
		
		return null;
	}

	public static Date getPopulatedStartTime(ActivityMar activity, CommandBean commandBean) throws Exception {
		
		Date newDate = new Date();
		
		if(activity !=null){
			Date start = (Date) activity.getStartDatetime();
			Date end = (Date) activity.getEndDatetime();
			
			Date startTime = start;
			
			if(start == null) {
				DateFormat df = new SimpleDateFormat("kkmmss");
				
				boolean found = false;
				for(Date endDatetime : ActivityUtils.getEndDatetimeList(commandBean)) {
					Calendar currentEndDatetime = Calendar.getInstance();
					currentEndDatetime.setTime(end);
					
					Calendar cachedEndDatetime = Calendar.getInstance();
					cachedEndDatetime.setTime(endDatetime);
					
					if(df.format(cachedEndDatetime.getTime()).compareTo(df.format(currentEndDatetime.getTime()))==-1) {						
						
						startTime = CommonUtil.getConfiguredInstance().setDate(cachedEndDatetime.getTime(), newDate);		
						found = true;
						break;
					}
				}
				// if no matches, set to 00:00
				if(!found) {
					Calendar zero = Calendar.getInstance();
					zero.clear();
					zero.set(Calendar.HOUR_OF_DAY, 0);
					zero.set(Calendar.MINUTE, 0);
					zero.set(Calendar.SECOND, 0);
					zero.set(Calendar.MILLISECOND, 0);
					
					startTime = CommonUtil.getConfiguredInstance().setDate(zero.getTime(), newDate);		
				}
			}else {
				startTime = CommonUtil.getConfiguredInstance().setDate(start, newDate);		
			}
			
			return startTime;
		}
		
		return null;
	}
	/**
	 * Method to calculate activity duration in hour
	 * @param activity
	 * @param thisConverter
	 * @return activityDuration
	 */
	public static Double getActivityDurationInHour(Date start, Date end, CustomFieldUom thisConverter) throws Exception {
		
		Double activityDurationInHour = 0.0;

		Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
		thisConverter.setBaseValue(thisDuration);		
		thisConverter.changeUOMUnit("Hour");
		activityDurationInHour = thisConverter.getConvertedValue();
		return activityDurationInHour;

	}
	
	/**
	 * Method to calculate activity duration in second
	 * @param activity
	 * @param thisConverter
	 * @return activityDuration
	 */
	public static Double getActivityDurationInSecond(Date start, Date end, CustomFieldUom thisConverter) throws Exception {
		
		Double activityDurationInSecond = 0.0;

		Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
		thisConverter.setBaseValue(thisDuration);		
		thisConverter.changeUOMUnit("Second");
		activityDurationInSecond = thisConverter.getConvertedValue();
		return activityDurationInSecond;

	}
	
	/**
	 * Method to calculate total activity duration by internal class code
	 * @param classCode
	 * @param operationUid
	 * @param qp 
	 * @param planReferenceUids (optional for plan operation time)
	 * @return totalDuration
	 */
	
	public static Double getActivityTotalDurationByInternalClassCode(String classCode, String operationUid, QueryProperties qp, String[] planReferenceUids) throws Exception {
		
		Double totalDuration = 0.0;
		String queryString = "SELECT d.dailyUid, d.dayDate, SUM(coalesce(a.activityDuration,0.0)) FROM Daily d, Activity a " +
			"WHERE (d.isDeleted=false or d.isDeleted is null) " +
			"AND (a.isDeleted=false or a.isDeleted is null) " +
			"AND (a.isSimop=false or a.isSimop is null) " +
			"AND (a.isOffline=false or a.isOffline is null) " +
			"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
			"AND a.dailyUid=d.dailyUid " +
			"AND d.operationUid=:operationUid " +
			"AND a.internalClassCode=:internalClassCode " ;
		
			if (planReferenceUids!=null)
				queryString += "AND a.planReference in ('"+StringUtils.join(planReferenceUids,"','")+"') " ;
			
			queryString += "AND a.activityDuration > 0.0 " +
			"GROUP BY d.dailyUid, d.dayDate " +
			"ORDER BY d.dayDate";
		
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"operationUid", "internalClassCode"}, new Object[] {operationUid, classCode}, qp);
		for (Object[] rec : list) {
			Double duration = (Double) rec[2];
			totalDuration += duration;
		}

		return totalDuration;
	}
	
	/**
	 * FOR NOBLE - auto calculate trip number =  ticket: 20120309-0638-ekueh
	 * @param session
	 * @param node
	 */
	public static void autoCalculateTripNumber(UserSession session, CommandBeanTreeNode node) throws Exception {
		if (node.getData() instanceof Activity || node.getData() instanceof NextDayActivity) {
			Object object = node.getData();
			Map<String, Object> fieldValues = PropertyUtils.describe(object);
			Integer tripNumber = null;
			if (fieldValues.containsKey("phaseCode")) {
				String phaseCode = (String) fieldValues.get("phaseCode");
				String queryString = "FROM LookupPhaseCode WHERE (isDeleted=false or isDeleted is null) " +
						"and (operationCode=:operationCode or operationCode='' or operationCode=null) " +
						"AND shortCode=:phaseCode " +
						"ORDER BY isActive DESC, operationCode DESC";
				List<LookupPhaseCode> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"operationCode", "phaseCode"}, new Object[]{session.getCurrentOperationType(), phaseCode});
				if (list.size()>0) {
					LookupPhaseCode phase = list.get(0);
					Boolean isTrip = BooleanUtils.isTrue(phase.getIsTrip());
					if (isTrip && session.getCurrentDaily()!=null) {
						tripNumber = 0;
						Date dayDate = session.getCurrentDaily().getDayDate();
						if (fieldValues.get("startDatetime")!=null) { //get startDatetime from activity if the activity listing by operation. eg Activity Batch Coding
							Calendar startDatetime = Calendar.getInstance();
							startDatetime.setTime((Date) fieldValues.get("startDatetime"));
							startDatetime.set(Calendar.MILLISECOND, 0);
							startDatetime.set(Calendar.SECOND, 0);
							startDatetime.set(Calendar.MINUTE, 0);
							startDatetime.set(Calendar.HOUR_OF_DAY, 0);
							if (startDatetime.getTimeInMillis()>0) {
								dayDate = startDatetime.getTime();
							}
						}
						queryString = "SELECT MAX(a.tripNumber) FROM ReportDaily rd, Activity a, Daily d, Operation o, Wellbore wb, Well w " +
								"WHERE (rd.isDeleted=false or rd.isDeleted is null) " +
								"AND (d.isDeleted=false or d.isDeleted is null) " +
								"AND (a.isDeleted=false or a.isDeleted is null) " +
								"AND (w.isDeleted=false or w.isDeleted is null) " +
								"AND (wb.isDeleted=false or wb.isDeleted is null) " +
								"AND (o.isDeleted=false or o.isDeleted is null) " +
								"AND rd.dailyUid=d.dailyUid " +
								"AND d.dailyUid=a.dailyUid " +
								"AND d.operationUid=o.operationUid " +
								"AND o.wellboreUid=wb.wellboreUid " +
								"AND wb.wellUid=w.wellUid " +
								"AND d.dayDate<:dayDate " +
								"AND rd.rigInformationUid=:rigInformationUid ";
						List<Object> tripList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"dayDate", "rigInformationUid"}, new Object[]{dayDate, session.getCurrentRigInformationUid()});
						if (tripList.size()>0 && tripList.get(0)!=null) {
							tripNumber = Integer.parseInt(tripList.get(0).toString());
							
						}
						tripNumber += 1;
					}
				}
				
			}
			PropertyUtils.setProperty(object, "tripNumber", tripNumber);
		}
	}
	
	public static Map<String,Object> getActivityRemarkParameterValues(CommandBean commandBean,String activityUid,OperationUomContext opUomContext) throws Exception{
		return FTRUtils.getActivityRemarkCustomParameterValues(commandBean,activityUid,null,"targetedFieldName",false,false, opUomContext);
	}
	
	/*public static String getModuleParameterDetailFieldType(String moduleParameterDetailUid,String key) throws Exception{
		
		String queryString = "Select targetFieldName from ModuleParameterDetail where (isDeleted is null or isDeleted = 0) and moduleParameterDetailUid = :moduleParameterDetailUid and parameterKey = :parameterKey";
		
		List<Object> paramList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"moduleParameterDetailUid","parameterKey"},new Object[]{moduleParameterDetailUid,key});
		if (paramList.size() >0){
			
			if (paramList.get(0) !=null){
				String targetFieldName = paramList.get(0).toString();
				
				String[] fieldToken = targetFieldName.split("[.]");
				if (fieldToken.length==2){
					String tableName = fieldToken[0];
					String fieldName = fieldToken[1];
					
					if (StringUtils.isNotBlank(tableName)) {
						String tableClassName = ApplicationUtils.getConfiguredInstance().getTablePackage() + CommonUtils.beanCapitalize(tableName);
						Class<?> hibernateClass = Class.forName(tableClassName);
						
						Type dataType= ApplicationUtils.getConfiguredInstance().getFieldTypeFromTable(hibernateClass, fieldName);
						String typeName = dataType.getReturnedClass().getName();
						if ( (typeName.equalsIgnoreCase("java.lang.double"))|| (typeName.equalsIgnoreCase("java.lang.float"))){
							return "double";
						}else if ((typeName.equalsIgnoreCase("java.lang.integer"))|| (typeName.equalsIgnoreCase("java.lang.long"))){
							return "integer";
						}
						
					}	
				}			
			}
		}	
		
		return "string"; 
	}*/
	
	public static int hasActivityDescription(String activityUid,String customParameterDetailUid) throws Exception{
		/*String queryString = "Select A.activityDescriptionMatrixUid from ActivityDescriptionMatrix A, ModuleParameterDetail B, ModuleParameterLink C where (A.isDeleted is null or A.isDeleted =0) " + 
		                     "and (B.isDeleted is null or B.isDeleted =0) and (C.isDeleted is null or C.isDeleted =0) and A.activityDescriptionMatrixUid=B.activityDescriptionMatrixUid " +
		                     "and B.moduleParameterDetailUid=C.moduleParameterDetailUid and C.activityUid = :activityUid group by A.activityDescriptionMatrixUid";*/
		String queryString = "Select B.customDescriptionMatrixUid from ActivityDescriptionMatrix A, CustomDescriptionMatrix B, CustomParameterDetail C, CustomParameterLink D " +
		                     " where (A.isDeleted is null or A.isDeleted =0) and (B.isDeleted is null or B.isDeleted =0) and (C.isDeleted is null or C.isDeleted =0) and (D.isDeleted is null or D.isDeleted =0) " +
				"AND a.hasCustomCode = FALSE " +
				"AND A.activityDescriptionMatrixUid=B.activityDescriptionMatrixUid " +
                "and B.customDescriptionMatrixUid = C.customDescriptionMatrixUid " +
                "and C.customParameterDetailUid=D.customParameterDetailUid and D.activityUid = :activityUid group by B.customDescriptionMatrixUid";
		List<Object> idList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
		if (idList.size()>0){
			if(idList.get(0)!=null){
				String customDescriptionMatrixUid = idList.get(0).toString();
				
				/*queryString = "Select moduleParameterDetailUid from ModuleParameterDetail where (isDeleted is null or isDeleted =0) " + 
                			  "and activityDescriptionMatrixUid = :activityDescriptionMatrixUid and moduleParameterDetailUid = :moduleParameterDetailUid ";*/
				queryString = "Select customParameterDetailUid from CustomParameterDetail where (isDeleted is null or isDeleted = 0) " + 
          			  "and customDescriptionMatrixUid = :customDescriptionMatrixUid and customParameterDetailUid = :customParameterDetailUid ";
				idList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString,new String[]{"customDescriptionMatrixUid","customParameterDetailUid"},new Object[]{customDescriptionMatrixUid,customParameterDetailUid});
				if(idList.size()>0) 
					return 1;
				else
					return -1;
			}
		}		
		return 0;
	}
	
	/*public static void autoRemoveTargetModuleByActivityUid(UserSelectionSnapshot selection,String activityUid) throws Exception{
		
		if (StringUtils.isNotBlank(activityUid)){		
			
			List<String> tableList = new ArrayList<String>();
			
			//set auto created record's activityUid to null
			String queryString = "select B.targetFieldName from ModuleParameterLink A,ModuleParameterDetail B where A.moduleParameterDetailUid= B.moduleParameterDetailUid and " +
			                     "(A.isDeleted is null or A.isDeleted =0) and (B.isDeleted is null or B.isDeleted =0) and A.activityUid = :activityUid and B.paramType <>'inputonly'";
			List<Object[]> targetFieldNameLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
			
			if (targetFieldNameLst.size()>0){
				
				String targetFieldName = "";
				String tableName = "";
				
				for (Object item : targetFieldNameLst){					
					if (item!=null){									
						targetFieldName = item.toString();
						String[] fieldToken = targetFieldName.split("[.]");
						if (fieldToken.length==2){
							// ignore second index - field
							tableName = fieldToken[0];						
							if (StringUtils.isNotBlank(tableName)) {
								
								if (!tableList.contains(tableName)){
									
									tableList.add(tableName);
									
									queryString = "from " + tableName + " where (isDeleted is null or isDeleted=0) and activityUid = :activityUid";
									List<Object> objectlst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
									if (objectlst.size()>0){
										Object object =  objectlst.get(0);
										
										//Boolean isSubmit = (Boolean) PropertyUtils.getProperty(object, "isSubmit");
										//if(isSubmit!=null && isSubmit) RevisionUtils.unSubmitRecordWithStaticRevisionLog(selection, object, "Auto update from time codes");
										
										PropertyUtils.setProperty(object, "activityUid",null);
										ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(object);
									}
								}						
							}
						}							
					}				
				}	
				
				//set is_deleted for each moduleParameterLink record to true
				queryString = "from ModuleParameterLink where (isDeleted is null or isDeleted =0) and activity_uid = :activityUid";
				List<ModuleParameterLink> ModuleParameterLinkLst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "activityUid", activityUid);
				for (ModuleParameterLink moduleParameterLink : ModuleParameterLinkLst){	
					moduleParameterLink.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(moduleParameterLink);
				}
			}			
			
		}
	}*/
	
	//Ticket No: 26839 jkong
	//moved from activityCommandBeanListener
	//To allow reportDailyCommandBeanListener to call this method
	public static double recalculateTotalDuration(CommandBean commandBean,String dailyUid,String operationUid) throws Exception {
		double totalActivityDuration = 0;
		double troubleDuration = 0; //endor 20120309-0659-ekueh
		double totalActivityGapDuration = 0;
		
		//If this method is called by reportDailyCommandBean
		if(commandBean.getBeanName().equals("reportDailyCommandBean") ||
				commandBean.getBeanName().equals("reportDailyCmpltRCommandBean") ||
				commandBean.getBeanName().equals("reportDailyDIRCommandBean") ||
				commandBean.getBeanName().equals("reportDailyWKOCommandBean") ||
				commandBean.getBeanName().equals("reportDailyWellServiceCommandBean") ||
				commandBean.getBeanName().equals("reportDailyDeCmpltCommandBean") ||
				commandBean.getBeanName().equals("reportDailyDeepenCommandBean") ||
				commandBean.getBeanName().equals("reportDailyRMCommandBean") ||
				commandBean.getBeanName().equals("reportDailyFracRCommandBean") ||
				commandBean.getBeanName().equals("reportDailyPNACommandBean"))
		{
			List<Activity> list = null;
			
			if(StringUtils.isNotBlank(dailyUid) && StringUtils.isNotBlank(operationUid)){
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Activity WHERE (isDeleted='' OR isDeleted is null) AND (carriedForwardActivityUid='' OR carriedForwardActivityUid is null) AND (dayPlus = 0 OR dayPlus is null) AND dailyUid = :dailyUid AND operationUid = :operationUid", new String[] {"dailyUid","operationUid"}, new Object[] {dailyUid,operationUid} );
				if(list.size() > 0 && list != null){
					for(Activity activity : list)
					{
						if (BooleanUtils.isTrue(activity.getIsSimop()) || BooleanUtils.isTrue(activity.getIsOffline())) continue; // exclude from calculation
						
						if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {
							totalActivityDuration += activity.getActivityDuration();
						
							if ("TP".equals(activity.getInternalClassCode()) || "TU".equals(activity.getInternalClassCode())) { //endor 20120309-0659-ekueh
								troubleDuration += activity.getActivityDuration();
							}
						}
					}
				}
			}
		}
		//if this method is called by activityCommandBean
		else
		{
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
		
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("Activity").values()) {
				if(node.getData() instanceof Activity) {
				
					Activity activity = (Activity) node.getData();
				
					if (BooleanUtils.isTrue(activity.getIsSimop()) || BooleanUtils.isTrue(activity.getIsOffline())) continue; // exclude from calculation
				
					if (activity.getObject()!=null && "true".equals(activity.getObject())){
						totalActivityGapDuration += activity.getActivityDuration();
					}else{
						if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {
							totalActivityDuration += activity.getActivityDuration();
						
							if ("TP".equals(activity.getInternalClassCode()) || "TU".equals(activity.getInternalClassCode())) { //endor 20120309-0659-ekueh
								troubleDuration += activity.getActivityDuration();
							}
						}
					}
				}
			}
			if (thisConverter.isUOMMappingAvailable()){
				commandBean.getRoot().setCustomUOM("@activityDuration", thisConverter.getUOMMapping());
				commandBean.getRoot().setCustomUOM("@troubleDuration", thisConverter.getUOMMapping());  //endor 20120309-0659-ekueh
				commandBean.getRoot().setCustomUOM("@activityGapDuration", thisConverter.getUOMMapping());
			}
			commandBean.getRoot().getDynaAttr().put("activityDuration", totalActivityDuration);
			commandBean.getRoot().getDynaAttr().put("troubleDuration", troubleDuration);  //endor 20120309-0659-ekueh
			commandBean.getRoot().getDynaAttr().put("activityGapDuration", totalActivityGapDuration);
		
            // Combined (Including SIMOP)
            double totalActivityDurationCombined = 0;
            for (CommandBeanTreeNode node : commandBean.getRoot().getList().get("Activity").values()) {
            	if(node.getData() instanceof Activity) {
	            	Activity activity = (Activity) node.getData();
	                if (BooleanUtils.isTrue(activity.getIsOffline()))
	                    continue; // exclude from calculation
	                if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {
	                    totalActivityDurationCombined += activity.getActivityDuration();
	                }
            	}
            }
            if (thisConverter.isUOMMappingAvailable()) {
                commandBean.getRoot().setCustomUOM("@activityDurationCombined", thisConverter.getUOMMapping());
            }
            commandBean.getRoot().getDynaAttr().put("activityDurationCombined", totalActivityDurationCombined);

			double totalSimopActivityDuration = 0;
			double troubleSimopDuration = 0; //endor 20120309-0659-ekueh
		
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("Activity").values()) {
				if(node.getData() instanceof Activity) {
				
					Activity activity = (Activity) node.getData();
			
					if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted()) ) {
						if (BooleanUtils.isTrue(activity.getIsSimop()))
						{
							totalSimopActivityDuration += activity.getActivityDuration();
						
							if ("TP".equals(activity.getInternalClassCode()) || "TU".equals(activity.getInternalClassCode())) { //endor 20120309-0659-ekueh
								troubleSimopDuration += activity.getActivityDuration();
							}
						}
					}
				}
			}
			if ("simop_activityController".equals(commandBean.getControllerBeanName())){
				totalActivityDuration = totalSimopActivityDuration;
				troubleDuration = troubleSimopDuration;
			}
			if (thisConverter.isUOMMappingAvailable()){
				commandBean.getRoot().setCustomUOM("@activitySimopDuration", thisConverter.getUOMMapping());
				commandBean.getRoot().setCustomUOM("@troubleActivitySimopDuration", thisConverter.getUOMMapping()); //endor 20120309-0659-ekueh
			}
			commandBean.getRoot().getDynaAttr().put("activitySimopDuration", totalSimopActivityDuration);
			commandBean.getRoot().getDynaAttr().put("troubleActivitySimopDuration", troubleDuration); //endor 20120309-0659-ekueh
		
			double totalOfflineActivityDuration = 0;
		
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("Activity").values()) {
				if(node.getData() instanceof Activity) { 
					Activity activity = (Activity) node.getData();
				
					if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted()) ) {
						if (BooleanUtils.isTrue(activity.getIsOffline()))
						{
							totalOfflineActivityDuration += activity.getActivityDuration();
						}
					}
				}
			}
			if ("offlineActivityController".equals(commandBean.getControllerBeanName())) totalActivityDuration = totalOfflineActivityDuration;
			if (thisConverter.isUOMMappingAvailable()){
				commandBean.getRoot().setCustomUOM("@activityOfflineDuration", thisConverter.getUOMMapping());
			}
			commandBean.getRoot().getDynaAttr().put("activityOfflineDuration", totalOfflineActivityDuration);
		
			double totalNextDayActivityDuration = 0;
			double totalNextDayTroubleDuration = 0; //endor 20120309-0659-ekueh
			double totalNextDayActivityGapDuration = 0;
			int cutOverGapHours = 600;
			Boolean proceedGap = true;
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("NextDayActivity").values()) {
				NextDayActivity activity = (NextDayActivity) node.getData();
			
				if (BooleanUtils.isTrue(activity.getIsSimop()) || BooleanUtils.isTrue(activity.getIsOffline())) continue; // exclude from calculation
			
				if (activity.getObject()!=null && "true".equals(activity.getObject()) && proceedGap){
					int startHours = formatTimeInt(activity.getStartDatetime());
					
					if (startHours < cutOverGapHours){
						int endHours = formatTimeInt(activity.getEndDatetime());
						
						if (endHours > cutOverGapHours){
							Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(updateActivityDateTime(activity.getStartDatetime(), cutOverGapHours, false),updateActivityDateTime(activity.getEndDatetime(), (cutOverGapHours/100), true));			
							CustomFieldUom thisConverterGap = new CustomFieldUom(commandBean, Activity.class, "activityDuration");			
							thisConverterGap.setBaseValue(thisDuration);		

							if (thisDuration!=null){
								totalNextDayActivityGapDuration = totalNextDayActivityGapDuration + thisConverterGap.getConvertedValue();
								proceedGap = false;
							}			
						}else{
							totalNextDayActivityGapDuration += activity.getActivityDuration();
						}
					}else{
						proceedGap = false;
					}
				}else if (activity.getObject()!=null && "true".equals(activity.getObject()) && !proceedGap){
					//exclude to add after cutOverGapHours
				}else{
					if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {
						totalNextDayActivityDuration += activity.getActivityDuration();
						if ("TP".equals(activity.getInternalClassCode()) || "TU".equals(activity.getInternalClassCode())) { //endor 20120309-0659-ekueh
							totalNextDayTroubleDuration += activity.getActivityDuration();
						}
					}
				}
			}
		
			if (thisConverter.isUOMMappingAvailable()){
				commandBean.getRoot().setCustomUOM("@nextDayActivityDuration", thisConverter.getUOMMapping());
				commandBean.getRoot().setCustomUOM("@nextDayTroubleDuration", thisConverter.getUOMMapping()); //endor 20120309-0659-ekueh
				commandBean.getRoot().setCustomUOM("@nextDayActivityGapDuration", thisConverter.getUOMMapping());
			}
			//thisConverter.setBaseValueFromUserValue(totalNextDayActivityDuration);
		
			commandBean.getRoot().getDynaAttr().put("nextDayActivityDuration", totalNextDayActivityDuration);
			commandBean.getRoot().getDynaAttr().put("nextDayTroubleDuration", totalNextDayTroubleDuration); //endor 20120309-0659-ekueh
			commandBean.getRoot().getDynaAttr().put("nextDayActivityGapDuration", totalNextDayActivityGapDuration);
			
            // Combined (Including SIMOP)
            double totalNextDayActivityDurationCombined = 0;
            for (CommandBeanTreeNode node : commandBean.getRoot().getList().get("NextDayActivity").values()) {
                NextDayActivity activity = (NextDayActivity) node.getData();
                if (BooleanUtils.isTrue(activity.getIsOffline()))
                    continue; // exclude from calculation
                if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {
                    totalNextDayActivityDurationCombined += activity.getActivityDuration();
                }
            }
            if (thisConverter.isUOMMappingAvailable()) {
                commandBean.getRoot().setCustomUOM("@nextDayActivityDurationCombined", thisConverter.getUOMMapping());
            }
            commandBean.getRoot().getDynaAttr().put("nextDayActivityDurationCombined", totalNextDayActivityDurationCombined);

			//Calculate Next Day SIMOP Activity Duration
			double totalNextDaySimopActivityDuration = 0;
			double totalNextDayTroubleSimopActivityDuration = 0; //endor 20120309-0659-ekueh
		
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("NextDayActivity").values()) {
				NextDayActivity activity = (NextDayActivity) node.getData();
			
				if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted()) ) {
					if (BooleanUtils.isTrue(activity.getIsSimop()))
					{
						totalNextDaySimopActivityDuration += activity.getActivityDuration();
						if ("TP".equals(activity.getInternalClassCode()) || "TU".equals(activity.getInternalClassCode())) { //endor 20120309-0659-ekueh
							totalNextDayTroubleSimopActivityDuration += activity.getActivityDuration();
						}
					}
				}
			}
			if (thisConverter.isUOMMappingAvailable()){
				commandBean.getRoot().setCustomUOM("@nextDayActivitySimopDuration", thisConverter.getUOMMapping());
				commandBean.getRoot().setCustomUOM("@nextDayTroubleActivitySimopDuration", thisConverter.getUOMMapping()); //endor 20120309-0659-ekueh
			}
			commandBean.getRoot().getDynaAttr().put("nextDayActivitySimopDuration", totalNextDaySimopActivityDuration);
			commandBean.getRoot().getDynaAttr().put("nextDayTroubleActivitySimopDuration", totalNextDayTroubleSimopActivityDuration); //endor 20120309-0659-ekueh
		
			//Calculate Next Day Offline Activity Duration
			double totalNextDayOfflineActivityDuration = 0;
		
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("NextDayActivity").values()) {
				NextDayActivity activity = (NextDayActivity) node.getData();
			
				if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted()) ) {
					if (BooleanUtils.isTrue(activity.getIsOffline()))
					{
						totalNextDayOfflineActivityDuration += activity.getActivityDuration();
					}
				}
			}
			if (thisConverter.isUOMMappingAvailable()){
				commandBean.getRoot().setCustomUOM("@nextDayActivityOfflineDuration", thisConverter.getUOMMapping());
			}
			commandBean.getRoot().getDynaAttr().put("nextDayActivityOfflineDuration", totalNextDayOfflineActivityDuration);
		
		}
		
		return totalActivityDuration;
	}
	
	//Ticket No: 26839 jkong
	//moved from activityCommandBeanListener
	//To allow reportDailyCommandBeanListener to call this method
	public static Boolean isTimeOverlapped(CommandBean commandBean,String dailyUid,String operationUid) throws Exception {
		Date activityFirstStartDateTime = null;
		Date activityLastStartDateTime = null;
		Double activityActualDuration = 0.0;
		Double activityDuration = 0.0;
		
		//If this method is called by reportDailyCommandBean
		if(commandBean.getBeanName().equals("reportDailyCommandBean") ||
			commandBean.getBeanName().equals("reportDailyCmpltRCommandBean") ||
			commandBean.getBeanName().equals("reportDailyDIRCommandBean") ||
			commandBean.getBeanName().equals("reportDailyWKOCommandBean") ||
			commandBean.getBeanName().equals("reportDailyWellServiceCommandBean") ||
			commandBean.getBeanName().equals("reportDailyDeCmpltCommandBean") ||
			commandBean.getBeanName().equals("reportDailyDeepenCommandBean") ||
			commandBean.getBeanName().equals("reportDailyFracRCommandBean"))
		{
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
			List<Activity> list = null;
			
			if(StringUtils.isNotBlank(dailyUid) && StringUtils.isNotBlank(operationUid)){
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Activity WHERE (isDeleted='' OR isDeleted is null) AND (carriedForwardActivityUid='' OR carriedForwardActivityUid is null) AND (isSimop='' OR isSimop is null) AND (isOffline='' OR isOffline is null) AND (dayPlus = 0 OR dayPlus is null) AND dailyUid = :dailyUid AND operationUid = :operationUid order by startDatetime", new String[] {"dailyUid","operationUid"}, new Object[] {dailyUid,operationUid} );
				if(list.size() > 0 && list != null){
					for(Activity activity : list) {
						if (activity.getIsDeleted() == null || !activity.getIsDeleted()) { 
							if (activity.getCarriedForwardActivityUid() == null || StringUtils.isEmpty(activity.getCarriedForwardActivityUid())) {
								if (activity.getStartDatetime() != null && activity.getEndDatetime() != null) {						
									if (activityFirstStartDateTime == null) activityFirstStartDateTime = activity.getStartDatetime();					
										activityLastStartDateTime = activity.getEndDatetime();
										if (activity != null && activity.getActivityDuration() != null) {
											activityDuration += activity.getActivityDuration();
										}
								}
							}
						}
					}
				}
			}
		
			thisConverter.setBaseValueFromUserValue(activityDuration);
			activityDuration=thisConverter.getBasevalue();
			int decimalPlaces = 1;
			BigDecimal bd = new BigDecimal(activityDuration);
			bd = bd.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
			activityDuration = bd.doubleValue();
			//activityDuration = (double) Math.ceil(activityDuration);
		
			if (activityFirstStartDateTime != null && activityLastStartDateTime !=null) {	
				//activityActualDuration = ActivityUtils.getActivityDurationInHour(activityFirstStartDateTime, activityLastStartDateTime, thisConverter);
				activityActualDuration = ActivityUtils.getActivityDurationInSecond(activityFirstStartDateTime, activityLastStartDateTime, thisConverter);
				if (activityActualDuration.compareTo(activityDuration) != 0) {
					return true;
				}
			}
		
			Date nextDayActivityFirstStartDateTime = null;
			Date nextDayActivityLastStartDateTime = null;
			Double nextDayActivityActualDuration = 0.0;
			Double nextDayActivityDuration = 0.0;
	
			thisConverter = new CustomFieldUom(commandBean, NextDayActivity.class, "activityDuration");
			
			if(StringUtils.isNotBlank(dailyUid) && StringUtils.isNotBlank(operationUid)){
				list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Activity WHERE (isDeleted='' OR isDeleted is null) AND (carriedForwardActivityUid='' OR carriedForwardActivityUid is null) AND (isSimop='' OR isSimop is null) AND (isOffline='' OR isOffline is null) AND dayPlus = 1 AND dailyUid = :dailyUid AND operationUid = :operationUid order by startDatetime", new String[] {"dailyUid","operationUid"}, new Object[] {dailyUid,operationUid} );
				if(list.size() > 0 && list != null){
					for(Activity nextDayActivity : list) {
						if (nextDayActivity.getIsDeleted() == null || !nextDayActivity.getIsDeleted()) {				
							if (nextDayActivity.getStartDatetime() != null || nextDayActivity.getEndDatetime() != null) {
								if (nextDayActivityFirstStartDateTime == null) nextDayActivityFirstStartDateTime = nextDayActivity.getStartDatetime();
								nextDayActivityLastStartDateTime = nextDayActivity.getEndDatetime();
								if (nextDayActivity != null && nextDayActivity.getActivityDuration() != null) {
									nextDayActivityDuration += nextDayActivity.getActivityDuration();
								}
							}
						}
					}
				}
			}
			
			thisConverter.setBaseValueFromUserValue(nextDayActivityDuration);
			nextDayActivityDuration = thisConverter.getBasevalue();
			BigDecimal nbd = new BigDecimal(nextDayActivityDuration);
			nbd = nbd.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
			nextDayActivityDuration = nbd.doubleValue();
			
			if ((nextDayActivityFirstStartDateTime != null) && (nextDayActivityLastStartDateTime != null))
		    {
				nextDayActivityActualDuration = ActivityUtils.getActivityDurationInSecond(nextDayActivityFirstStartDateTime, nextDayActivityLastStartDateTime, thisConverter);
				if (nextDayActivityActualDuration.compareTo(nextDayActivityDuration) != 0) {
					return true;
				}
			}
		
			return false;
		}
		//if this method is called by activityCommandBean
		else
		{
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("Activity").values()) {
				Activity activity = (Activity) node.getData();
				if (activity.getIsDeleted() == null || !activity.getIsDeleted()) { 
					if (activity.getStartDatetime() != null && activity.getEndDatetime() != null) {						
						if (activityFirstStartDateTime == null) activityFirstStartDateTime = activity.getStartDatetime();					
						activityLastStartDateTime = activity.getEndDatetime();
						if (activity != null && activity.getActivityDuration() != null) {
							activityDuration += activity.getActivityDuration();
						}
					}
				}
			}
		
			thisConverter.setBaseValueFromUserValue(activityDuration);
			activityDuration=thisConverter.getBasevalue();
			int decimalPlaces = 1;
			BigDecimal bd = new BigDecimal(activityDuration);
			bd = bd.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
			activityDuration = bd.doubleValue();
		
			if (activityFirstStartDateTime != null && activityLastStartDateTime !=null) {	
				activityActualDuration = ActivityUtils.getActivityDurationInSecond(activityFirstStartDateTime, activityLastStartDateTime, thisConverter);
				if (activityActualDuration.compareTo(activityDuration) != 0) {
					return true;
				}
			}
		
			Date nextDayActivityFirstStartDateTime = null;
			Date nextDayActivityLastStartDateTime = null;
			Double nextDayActivityActualDuration = 0.0;
			Double nextDayActivityDuration = 0.0;
			
			thisConverter = new CustomFieldUom(commandBean, NextDayActivity.class, "activityDuration");
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("NextDayActivity").values()) {
				NextDayActivity nextDayActivity = (NextDayActivity) node.getData();
			
				if (nextDayActivity.getIsDeleted() == null || !nextDayActivity.getIsDeleted()) {				
					if (nextDayActivity.getStartDatetime() != null || nextDayActivity.getEndDatetime() != null) {
						if (nextDayActivityFirstStartDateTime == null) nextDayActivityFirstStartDateTime = nextDayActivity.getStartDatetime();
						nextDayActivityLastStartDateTime = nextDayActivity.getEndDatetime();
						if (nextDayActivity != null && nextDayActivity.getActivityDuration() != null) {
							nextDayActivityDuration += nextDayActivity.getActivityDuration();
						}
					}
				}
			}
			
			thisConverter.setBaseValueFromUserValue(nextDayActivityDuration);
			nextDayActivityDuration = thisConverter.getBasevalue();
			BigDecimal nbd = new BigDecimal(nextDayActivityDuration);
			nbd = nbd.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
			nextDayActivityDuration = nbd.doubleValue();
			
			if ((nextDayActivityFirstStartDateTime != null) && (nextDayActivityLastStartDateTime != null))
		    {
				nextDayActivityActualDuration = ActivityUtils.getActivityDurationInSecond(nextDayActivityFirstStartDateTime, nextDayActivityLastStartDateTime, thisConverter);
				if (nextDayActivityActualDuration.compareTo(nextDayActivityDuration) != 0) {
					return true;
				}
			}
		
			return false;
		}
	}
	
	public static Boolean submodeIsKickOrEquimentFailure(CommandBean commandBean,String dailyUid,String operationUid) throws Exception {
		String submode1 = null;
		String submode2 = null;
		String submode3 = null;
		String[] paramNames = {"dailyUid", "operationUid"};	
		Object[] paramValues = {dailyUid, operationUid};

		String sql = "SELECT submode1, submode2, submode3 FROM Activity WHERE dailyUid =:dailyUid AND operationUid=:operationUid AND (isDeleted is null or isDeleted = false)";
		List res = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, paramValues);
		if(res.size() > 0 && res != null) {
			for(Object submodeObj : res) {
				Object[] sub1 = (Object[]) submodeObj;
				if(sub1[0] != null && StringUtils.isNotBlank(sub1[0].toString())) {
					if(E_FAIL.equals(sub1[0].toString()) || KICK.equals(sub1[0].toString())) {
						submode1 = sub1[0].toString();
					}
				}
				
				if(sub1[1] != null && StringUtils.isNotBlank(sub1[1].toString())) {
					if(E_FAIL.equals(sub1[1].toString()) || KICK.equals(sub1[1].toString())) {
						submode2 = sub1[1].toString();
					}
				}
				
				if(sub1[2] != null && StringUtils.isNotBlank(sub1[2].toString())) {
					if(E_FAIL.equals(sub1[2].toString()) || KICK.equals(sub1[2].toString())) {
						submode3 = sub1[2].toString();
					}
				}
			}
			if(E_FAIL.equals(submode1) || KICK.equals(submode1) || E_FAIL.equals(submode2) || KICK.equals(submode2) || E_FAIL.equals(submode3) || KICK.equals(submode3)) {
				return true;
			}
		}
		return false;
	}
	
	public static Boolean isSubmodeEmpty(CommandBean commandBean,String dailyUid,String operationUid) throws Exception {
		String submode1 = null;
		String submode2 = null;
		String submode3 = null;
		
		String[] paramNames = {"dailyUid", "operationUid"};	
		Object[] paramValues = {dailyUid, operationUid};

		String sql = "SELECT submode1, submode2, submode3 FROM Activity WHERE dailyUid =:dailyUid AND operationUid=:operationUid AND (isDeleted is null or isDeleted = false)";
		List res = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, paramValues);
		if(res.size() > 0 && res != null) {
			for(Object submodeObj : res) {
				Object[] sub = (Object[]) submodeObj;
				if(sub[0] != null && StringUtils.isNotBlank(sub[0].toString())) {
					submode1 = sub[0].toString();
				}
				if(sub[1] != null && StringUtils.isNotBlank(sub[1].toString())) {
					submode2 = sub[1].toString();
				}
				if(sub[2] != null && StringUtils.isNotBlank(sub[2].toString())) {
					submode3 = sub[2].toString();
				}
			}
			
			if((StringUtils.isNotBlank(submode1)) || (StringUtils.isNotBlank(submode2)) || (StringUtils.isNotBlank(submode3))) {
				return true;
			}
		}
		return false;
	}
	
	public static List<ReportDaily> getDrillnetReportDaily(String dailyUid) throws Exception {
		if(StringUtils.isBlank(dailyUid)) {
			return null;
		}
		List<ReportDaily> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportDaily WHERE (isDeleted = false OR isDeleted IS NULL) AND dailyUid=:dailyUid AND reportType != 'DGR'", "dailyUid", dailyUid);
		return list;
	}
	
	public static Date updateActivityDateTime(Date activityDateTime, int hour, boolean setTime){
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTime(activityDateTime);
		
		if (setTime){
			thisCalendar.set(Calendar.HOUR_OF_DAY, hour);
			thisCalendar.set(Calendar.MINUTE, 0);
			thisCalendar.set(Calendar.SECOND , 0);
			thisCalendar.set(Calendar.MILLISECOND , 0);
		}
	
		return thisCalendar.getTime();
	}
	
	public static int formatTimeInt(Date newDate){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		
		String Hour=String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		String Min=String.valueOf(calendar.get(Calendar.MINUTE));
		
		if (Hour.length()==1)
		{
			if (Hour.equals("0")){
				Hour="00";
			}else{
				Hour="0"+Hour;
			}
		}
		if (Min.length()==1)
		{
			Min="0"+Min;
		}
		if (Integer.parseInt(Hour + Min) == 2359)
			return 2400;
		else
			return Integer.parseInt(Hour + Min);
	}
	
}
