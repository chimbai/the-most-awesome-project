package com.idsdatanet.d2.drillnet.well;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.OpsTeam;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class OpsTeamLookupHandler implements LookupHandler {
	
	// get the ops teams that the current user has access to, order by the team name
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> results = new LinkedHashMap<String, LookupItem>();
		List<OpsTeam> opsTeams = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT t FROM OpsTeam t, OpsTeamUser u WHERE t.opsTeamUid = u.opsTeamUid AND (t.isDeleted = false or t.isDeleted is null) AND (u.isDeleted = false or u.isDeleted is null) AND u.userUid = :userUid ORDER BY t.name ASC", "userUid", userSelection.getUserUid());
		if (opsTeams != null && !opsTeams.isEmpty()) {
			for (OpsTeam opsTeam : opsTeams) {
				results.put(opsTeam.getOpsTeamUid(), new LookupItem(opsTeam.getOpsTeamUid(), opsTeam.getName()));
			}
		}
		return results;
	}

}
