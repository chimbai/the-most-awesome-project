package com.idsdatanet.d2.drillnet.cementJob;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupProvider;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CasingSummary;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * Custom lookuphandler for cementjob casingSectionUid - Special handling for Tapered Casing Screen.
 * @author Fu
 *
 */
public class CementJobCasingSummaryCasingOdLookupHandler implements LookupHandler {
	private URI xmlLookup = null;
	private LookupProvider lookupProvider = null;

	public void setXmlLookup(URI xmlLookup) {
		this.xmlLookup = xmlLookup;
	}
	
	public URI getXmlLookup() {
		return this.xmlLookup;
	}

	public LookupProvider getLookupProvider() {
		return lookupProvider;
	}

	public void setLookupProvider(LookupProvider lookupProvider) {
		this.lookupProvider = lookupProvider;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		Map<String, LookupItem> casingLookup = this.getLookupProvider().getLookup(this.getXmlLookup(), userSelection);
		
		List<CasingSection> csgList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and operationUid = :operationUid order by installStartDate", "operationUid", userSelection.getOperationUid());
		if(csgList != null && csgList.size() > 0){
			for(CasingSection casingSection : csgList) {
				String casingSectionUid = casingSection.getCasingSectionUid();
				String casingSizeLabel = "";
				int count = 0;
				
				//based on casingSectionUid, filter all related casing summary records' casingOd to populate into the lookup items
				List<CasingSummary> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSummary where (isDeleted = false or isDeleted is null) and casingSectionUid = :casingSectionUid and (casingOd is not null and casingOd != '') ORDER BY casingOd DESC", "casingSectionUid", casingSectionUid);
				if((list != null && list.size() > 0) && (casingLookup != null)) {
					for(CasingSummary casingSummary : list) {
						CustomFieldUom thisConverter = new CustomFieldUom((Locale) null, CasingSummary.class, "casingOd");
						String strCasingOd = thisConverter.formatOutputPrecision(casingSummary.getCasingOd());
						
						if(StringUtils.isNotBlank(strCasingOd) && casingLookup.containsKey(strCasingOd)) {
							LookupItem lookupItem = casingLookup.get(strCasingOd);
							if(lookupItem != null)
							{
								if (count == 0){
									casingSizeLabel = lookupItem.getValue().toString();				
								}else{
									casingSizeLabel = casingSizeLabel + " x " + lookupItem.getValue().toString();
								}
								count ++;
							}
						}
					}
					if (count > 0){
						result.put(casingSectionUid, new LookupItem(casingSectionUid, casingSizeLabel));
					}					
				}								
			}
		}		
		return result;
	}
}