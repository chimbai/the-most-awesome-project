package com.idsdatanet.d2.drillnet.wellControlStack;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class WellControlStackUtils {
    
    protected static Double getWellGroundLevel(String wellUid) throws Exception {
        Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, wellUid);
        if (well != null && "ON".equals(well.getOnOffShore())) {
            Double gl = well.getGlHeightMsl();
            return gl;
        }
        return null;
    }
    
    protected static Date stripTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        date = cal.getTime();
        return date;
    }
    
    protected static Date getMinDailyDayDateFromWellUid(String wellUid) throws Exception {
        String queryString = "FROM Daily WHERE (isDeleted=FALSE OR isDeleted IS NULL) AND wellUid = :wellUid ORDER BY dayDate ASC";
        String[] paramNames = {"wellUid"};
        Object[] paramValues = {wellUid};
        QueryProperties queryProperties = new QueryProperties();
        queryProperties.setFetchFirstRowOnly();
        List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, queryProperties);
        
        if (lstResult.size() > 0) {
            Daily thisDaily = (Daily) lstResult.get(0);
            return thisDaily.getDayDate();
        }
        return null;
    }
    
    protected static Date getMaxDailyDayDateFromWellUid(String wellUid) throws Exception {
        String queryString = "FROM Daily WHERE (isDeleted=FALSE OR isDeleted IS NULL) AND wellUid = :wellUid ORDER BY dayDate DESC";
        String[] paramNames = {"wellUid"};
        Object[] paramValues = {wellUid};
        QueryProperties queryProperties = new QueryProperties();
        queryProperties.setFetchFirstRowOnly();
        List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, queryProperties);
        
        if (lstResult.size() > 0) {
            Daily thisDaily = (Daily) lstResult.get(0);
            Date dayDate = thisDaily.getDayDate();
            return dayDate;
        }
        return null;
    }
}