package com.idsdatanet.d2.drillnet.weatherEnvironment;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.MarineProperties;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.WeatherEnvironment;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WeatherEnvironmentDataNodeListener extends EmptyDataNodeListener {
	
	private boolean reportWeatherDirectionInAzimuth = false;
	
	public void setReportWeatherDirectionInAzimuth(boolean value){
		this.reportWeatherDirectionInAzimuth = value;
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof WeatherEnvironment) {
			WeatherEnvironment thisWeatherEnv = (WeatherEnvironment) object;
			// TODO this is not time zone sensitive, fix this
			thisWeatherEnv.setReportDatetime(CommonUtil.getConfiguredInstance().currentDateTime(userSelection.getGroupUid(), userSelection.getWellUid()));
			
			//GET ONOFFSHORE STATUS WHEN ADD NEW RECORD
			if (StringUtils.isNotBlank(userSelection.getWellUid())) { 
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
				node.getDynaAttr().put("onOffShore", well.getOnOffShore());
			}
					
			if (StringUtils.isNotBlank(userSelection.getRigInformationUid())) {
				RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, userSelection.getRigInformationUid());
				if (rig!=null) {
					node.getDynaAttr().put("rigType", rig.getRigSubType());
				}
			}
			
			if(reportWeatherDirectionInAzimuth){
				reportWeatherDirectionInAzimuthSetUom(commandBean,node);
			}
			// Marine
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, MarineProperties.class, "pitchAngle");
			node.setCustomUOM("@marineProperties.pitchAngle", thisConverter.getUOMMapping());
			thisConverter = new CustomFieldUom(commandBean, MarineProperties.class, "rollAngle");
			node.setCustomUOM("@marineProperties.rollAngle", thisConverter.getUOMMapping());
			thisConverter = new CustomFieldUom(commandBean, MarineProperties.class, "heaveTravel");
			node.setCustomUOM("@marineProperties.heaveTravel", thisConverter.getUOMMapping());
		}
		
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof WeatherEnvironment) {
			WeatherEnvironment thisWeatherEnv = (WeatherEnvironment) object;
			
			if (StringUtils.isNotBlank(thisWeatherEnv.getWellUid())) {
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, thisWeatherEnv.getWellUid());
				node.getDynaAttr().put("onOffShore", well.getOnOffShore());
			}
				
			if (StringUtils.isNotBlank(userSelection.getRigInformationUid())) {
				RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, userSelection.getRigInformationUid());
				if (rig!=null) {
					node.getDynaAttr().put("rigType", rig.getRigSubType());
				}
			}
			
			if(reportWeatherDirectionInAzimuth){
				reportWeatherDirectionInAzimuthSetUom(commandBean,node);
				if (thisWeatherEnv.getWaveDirection() != null) {
					double waveDirection = Math.round(thisWeatherEnv.getWaveDirection());
					node.getDynaAttr().put("waveDirectionAzimuth", azimuthString(Double.toString(waveDirection)));
				}
				if (thisWeatherEnv.getWaveDirection2400() != null) {
					double waveDirection2400 = Math.round(thisWeatherEnv.getWaveDirection2400());
					node.getDynaAttr().put("waveDirection2400Azimuth", azimuthString(Double.toString(waveDirection2400)));
				}
				if (thisWeatherEnv.getWavedirDaymax() != null) {
					double wavedirDaymax = Math.round(thisWeatherEnv.getWavedirDaymax());
					node.getDynaAttr().put("wavedirDaymaxAzimuth", azimuthString(Double.toString(wavedirDaymax)));
				}
				if (thisWeatherEnv.getSwellDirection() != null) {
					double swellDirection = Math.round(thisWeatherEnv.getSwellDirection());
					node.getDynaAttr().put("swellDirectionAzimuth", azimuthString(Double.toString(swellDirection)));
				}
				if (thisWeatherEnv.getSwellDirection2400() != null) {
					double swellDirection2400 = Math.round(thisWeatherEnv.getSwellDirection2400());
					node.getDynaAttr().put("swellDirection2400Azimuth", azimuthString(Double.toString(swellDirection2400)));
				}
				if (thisWeatherEnv.getSwelldirDaymax() != null) {
					double swelldirDaymax = Math.round(thisWeatherEnv.getSwelldirDaymax());
					node.getDynaAttr().put("swelldirDaymaxAzimuth", azimuthString(Double.toString(swelldirDaymax)));
				}
				if (thisWeatherEnv.getWindDirection() != null) {
					double windDirection = Math.round(thisWeatherEnv.getWindDirection());
					node.getDynaAttr().put("windDirectionAzimuth", azimuthString(Double.toString(windDirection)));
				}
				if (thisWeatherEnv.getWindDirection2400() != null) {
					double windDirection2400 = Math.round(thisWeatherEnv.getWindDirection2400());
					node.getDynaAttr().put("windDirection2400Azimuth", azimuthString(Double.toString(windDirection2400)));
				}
				if (thisWeatherEnv.getWinddirDaymax() != null) {
					double winddirDaymax = Math.round(thisWeatherEnv.getWinddirDaymax());
					node.getDynaAttr().put("winddirDaymaxAzimuth", azimuthString(Double.toString(winddirDaymax)));
				}
				if (thisWeatherEnv.getSurfacecurrentdir() != null) {
					double surfacecurrentdir = Math.round(thisWeatherEnv.getSurfacecurrentdir());
					node.getDynaAttr().put("surfacecurrentdirAzimuth", azimuthString(Double.toString(surfacecurrentdir)));
				}
				if (thisWeatherEnv.getSurfaceCurrentDirection2400() != null) {
					double surfaceCurrentDirection2400 = Math.round(thisWeatherEnv.getSurfaceCurrentDirection2400());
					node.getDynaAttr().put("surfaceCurrentDirection2400Azimuth", azimuthString(Double.toString(surfaceCurrentDirection2400)));
				}
				if (thisWeatherEnv.getSurfaceCurrentDirectionDaymax() != null) {
					double surfaceCurrentDirectionDaymax = Math.round(thisWeatherEnv.getSurfaceCurrentDirectionDaymax());
					node.getDynaAttr().put("surfaceCurrentDirectionDaymaxAzimuth", azimuthString(Double.toString(surfaceCurrentDirectionDaymax)));
				}
			}
			// Marine
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, MarineProperties.class, "pitchAngle");
			node.setCustomUOM("@marineProperties.pitchAngle", thisConverter.getUOMMapping());
			thisConverter = new CustomFieldUom(commandBean, MarineProperties.class, "rollAngle");
			node.setCustomUOM("@marineProperties.rollAngle", thisConverter.getUOMMapping());
			thisConverter = new CustomFieldUom(commandBean, MarineProperties.class, "heaveTravel");
			node.setCustomUOM("@marineProperties.heaveTravel", thisConverter.getUOMMapping());
			
			String sql = "from MarineProperties where (isDeleted = false or isDeleted is null) and weatherEnvironmentUid = :weatherEnvironmentUid";
			List mpList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "weatherEnvironmentUid", thisWeatherEnv.getWeatherEnvironmentUid());
			if(mpList.size() > 0){
				MarineProperties mp = (MarineProperties) mpList.get(0);
				node.getDynaAttr().put("marineProperties.pitchAngle", mp.getPitchAngle());
				node.getDynaAttr().put("marineProperties.rollAngle", mp.getRollAngle());
				node.getDynaAttr().put("marineProperties.heaveTravel", mp.getHeaveTravel());
			}else {
				node.getDynaAttr().put("marineProperties.pitchAngle", "");
				node.getDynaAttr().put("marineProperties.rollAngle", "");
				node.getDynaAttr().put("marineProperties.heaveTravel", "");
			}
		}
	}

	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		if (object instanceof WeatherEnvironment) {
			WeatherEnvironment thisWeatherEnv = (WeatherEnvironment) object;
			Object pitchAngle = node.getDynaAttr().get("marineProperties.pitchAngle");
			Object rollAngle = node.getDynaAttr().get("marineProperties.rollAngle");
			Object heaveTravel =node.getDynaAttr().get("marineProperties.heaveTravel");
			
			String sql = "from MarineProperties where (isDeleted = false or isDeleted is null) and weatherEnvironmentUid = :weatherEnvironmentUid";
			List mpList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "weatherEnvironmentUid", thisWeatherEnv.getWeatherEnvironmentUid());
			MarineProperties mp = null;
			if(mpList.size() > 0){
				mp = (MarineProperties) mpList.get(0);
			}else {
				mp = new MarineProperties();
				mp.setWeatherEnvironmentUid(thisWeatherEnv.getWeatherEnvironmentUid());
			}
			
			if (pitchAngle!=null) {
				if(!(pitchAngle.toString().isEmpty())) {
					mp.setPitchAngle(Double.parseDouble(pitchAngle.toString()));
				} else {
					mp.setPitchAngle(null);
				}
			} else {
				mp.setPitchAngle(null);
			}
			
			if (rollAngle!=null) {
				if(!(rollAngle.toString().isEmpty())) {
					mp.setRollAngle(Double.parseDouble(rollAngle.toString()));
				} else {
					mp.setRollAngle(null);
				}
			} else {
				mp.setRollAngle(null);
			}
			
			if (heaveTravel!=null) {
				if(!(heaveTravel.toString().isEmpty())) {
					mp.setHeaveTravel(Double.parseDouble(heaveTravel.toString()));
				} else {
					mp.setHeaveTravel(null);
				}
			} else {
				mp.setHeaveTravel(null);
			}
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mp);
		}
	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof WeatherEnvironment) {
			WeatherEnvironment thisWeatherEnv = (WeatherEnvironment) object;
			if (thisWeatherEnv.getReportDatetime() != null) {
				if(session.getCurrentDaily() != null) {
					thisWeatherEnv.setReportDatetime(CommonUtil.getConfiguredInstance().setDate(thisWeatherEnv.getReportDatetime(),session.getCurrentDaily().getDayDate()));
				}
			}
			if ("1".equals(GroupWidePreference.getValue(UserSession.getInstance(request).getCurrentGroupUid(), "autoCalculateAirTemperatureAvg"))) {
				if (thisWeatherEnv.getAirTemperatureMax() != null && thisWeatherEnv.getAirTemperatureMin() != null) thisWeatherEnv.setAirTemperatureAvg((thisWeatherEnv.getAirTemperatureMax() + thisWeatherEnv.getAirTemperatureMin())/2);
				else
				{
					if (thisWeatherEnv.getAirTemperatureMax() != null)thisWeatherEnv.setAirTemperatureAvg(thisWeatherEnv.getAirTemperatureMax());
					else if (thisWeatherEnv.getAirTemperatureMin() != null)thisWeatherEnv.setAirTemperatureAvg(thisWeatherEnv.getAirTemperatureMin());
					else thisWeatherEnv.setAirTemperatureAvg(null);
				}
			}
			
			if(reportWeatherDirectionInAzimuth){
				double waveDirection = 0;
				if (node.getDynaAttr().get("waveDirectionAzimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("waveDirectionAzimuth").toString())) {
					String text = (String) node.getDynaAttr().get("waveDirectionAzimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "waveDirectionAzimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						waveDirection = Double.valueOf(text);
						thisWeatherEnv.setWaveDirection(waveDirection);
					}
				}else{
					thisWeatherEnv.setWaveDirection(null);
				}
				double waveDirection2400 = 0;
				if (node.getDynaAttr().get("waveDirection2400Azimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("waveDirection2400Azimuth").toString())) {
					String text = (String) node.getDynaAttr().get("waveDirection2400Azimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "waveDirection2400Azimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						waveDirection2400 = Double.valueOf(text);
						thisWeatherEnv.setWaveDirection2400(waveDirection2400);
					}
				}else{
					thisWeatherEnv.setWaveDirection2400(null);
				}
				double wavedirDaymax = 0;
				if (node.getDynaAttr().get("wavedirDaymaxAzimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("wavedirDaymaxAzimuth").toString())) {
					String text = (String) node.getDynaAttr().get("wavedirDaymaxAzimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "wavedirDaymaxAzimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						wavedirDaymax = Double.valueOf(text);
						thisWeatherEnv.setWavedirDaymax(wavedirDaymax);
					}
				}else{
					thisWeatherEnv.setWavedirDaymax(null);
				}
				double swellDirection = 0;
				if (node.getDynaAttr().get("swellDirectionAzimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("swellDirectionAzimuth").toString())) {
					String text = (String) node.getDynaAttr().get("swellDirectionAzimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "swellDirectionAzimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						swellDirection = Double.valueOf(text);
						thisWeatherEnv.setSwellDirection(swellDirection);
					}
				}else{
					thisWeatherEnv.setSwellDirection(null);
				}
				double swellDirection2400 = 0;
				if (node.getDynaAttr().get("swellDirection2400Azimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("swellDirection2400Azimuth").toString())) {
					String text = (String) node.getDynaAttr().get("swellDirection2400Azimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "swellDirection2400Azimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						swellDirection2400 = Double.valueOf(text);
						thisWeatherEnv.setSwellDirection2400(swellDirection2400);
					}
				}else{
					thisWeatherEnv.setSwellDirection2400(null);
				}
				double swelldirDaymax = 0;
				if (node.getDynaAttr().get("swelldirDaymaxAzimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("swelldirDaymaxAzimuth").toString())) {
					String text = (String) node.getDynaAttr().get("swelldirDaymaxAzimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "swelldirDaymaxAzimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						swelldirDaymax = Double.valueOf(text);
						thisWeatherEnv.setSwelldirDaymax(swelldirDaymax);
					}
				}else{
					thisWeatherEnv.setSwelldirDaymax(null);
				}
				double windDirection = 0;
				if (node.getDynaAttr().get("windDirectionAzimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("windDirectionAzimuth").toString())) {
					String text = (String) node.getDynaAttr().get("windDirectionAzimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "windDirectionAzimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						windDirection = Double.valueOf(text);
						thisWeatherEnv.setWindDirection(windDirection);
					}
				}else{
					thisWeatherEnv.setWindDirection(null);
				}
				double windDirection2400 = 0;
				if (node.getDynaAttr().get("windDirection2400Azimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("windDirection2400Azimuth").toString())) {
					String text = (String) node.getDynaAttr().get("windDirection2400Azimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "windDirection2400Azimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						windDirection2400 = Double.valueOf(text);
						thisWeatherEnv.setWindDirection2400(windDirection2400);
					}
				}else{
					thisWeatherEnv.setWindDirection2400(null);
				}
				double winddirDaymax = 0;
				if (node.getDynaAttr().get("winddirDaymaxAzimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("winddirDaymaxAzimuth").toString())) {
					String text = (String) node.getDynaAttr().get("winddirDaymaxAzimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "winddirDaymaxAzimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						winddirDaymax = Double.valueOf(text);
						thisWeatherEnv.setWinddirDaymax(winddirDaymax);
					};
				}else{
					thisWeatherEnv.setWinddirDaymax(null);
				}
				double surfacecurrentdir = 0;
				if (node.getDynaAttr().get("surfacecurrentdirAzimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("surfacecurrentdirAzimuth").toString())) {
					String text = (String) node.getDynaAttr().get("surfacecurrentdirAzimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "surfacecurrentdirAzimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						surfacecurrentdir = Double.valueOf(text);
						thisWeatherEnv.setSurfacecurrentdir(surfacecurrentdir);
					}
				}else{
					thisWeatherEnv.setSurfacecurrentdir(null);
				}
				double surfaceCurrentDirection2400 = 0;
				if (node.getDynaAttr().get("surfaceCurrentDirection2400Azimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("surfaceCurrentDirection2400Azimuth").toString())) {
					String text = (String) node.getDynaAttr().get("surfaceCurrentDirection2400Azimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "surfaceCurrentDirection2400Azimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						surfaceCurrentDirection2400 = Double.valueOf(text);
						thisWeatherEnv.setSurfaceCurrentDirection2400(surfaceCurrentDirection2400);
					}
				}else{
					thisWeatherEnv.setSurfaceCurrentDirection2400(null);
				}
				double surfaceCurrentDirectionDaymax = 0;
				if (node.getDynaAttr().get("surfaceCurrentDirectionDaymaxAzimuth") != null && StringUtils.isNotBlank(node.getDynaAttr().get("surfaceCurrentDirectionDaymaxAzimuth").toString())) {
					String text = (String) node.getDynaAttr().get("surfaceCurrentDirectionDaymaxAzimuth");
					String[] split = text.split("\\.");
					if(split[0].length() > 3 || split[0].contains("-")) {
						status.setDynamicAttributeError(node, "surfaceCurrentDirectionDaymaxAzimuth", "You have entered invalid data or have not filled in a required field. Please correct the error and try again.");
						status.setContinueProcess(false, true);
					}else{
						surfaceCurrentDirectionDaymax = Double.valueOf(text);
						thisWeatherEnv.setSurfaceCurrentDirectionDaymax(surfaceCurrentDirectionDaymax);
					}
				}else{
					thisWeatherEnv.setSurfaceCurrentDirectionDaymax(null);
				}
				if(!status.isProceed()){
					commandBean.getFlexClientControl().setReloadAfterPageCancel();
				}
			}
			Object pitchAngle = node.getDynaAttr().get("marineProperties.pitchAngle");
			Object rollAngle = node.getDynaAttr().get("marineProperties.rollAngle");
			Object heaveTravel =node.getDynaAttr().get("marineProperties.heaveTravel");
			if (pitchAngle!=null && "-".equals(pitchAngle.toString())) {
				status.setDynamicAttributeError(node, "marineProperties.pitchAngle", "This field requires a number (Double)");
				status.setContinueProcess(false, true);
			}
			if (rollAngle!=null && "-".equals(rollAngle.toString())) {
				status.setDynamicAttributeError(node, "marineProperties.rollAngle", "This field requires a number (Double)");
				status.setContinueProcess(false, true);
			}
			if (heaveTravel!=null && "-".equals(heaveTravel.toString())) {
				status.setDynamicAttributeError(node, "marineProperties.heaveTravel", "This field requires a number (Double)");
				status.setContinueProcess(false, true);
			}
		}
	}
	
	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{
		if(reportWeatherDirectionInAzimuth){
			Object object = targetNode.getData();
			reportWeatherDirectionInAzimuthSetUom(commandBean,targetNode);
			if (object instanceof WeatherEnvironment) {
				WeatherEnvironment weather = (WeatherEnvironment) object;
				if(weather.getWaveDirection() != null) setDynaAttr(targetNode, "waveDirectionAzimuth", Math.round(weather.getWaveDirection()));
				if(weather.getWaveDirection2400() != null) setDynaAttr(targetNode, "waveDirection2400Azimuth", Math.round(weather.getWaveDirection2400()));
				if(weather.getWavedirDaymax() != null) setDynaAttr(targetNode, "wavedirDaymaxAzimuth", Math.round(weather.getWavedirDaymax()));
				if(weather.getSwellDirection() != null) setDynaAttr(targetNode, "swellDirectionAzimuth", Math.round(weather.getSwellDirection()));
				if(weather.getSwellDirection2400() != null) setDynaAttr(targetNode, "swellDirection2400Azimuth", Math.round(weather.getSwellDirection2400()));
				if(weather.getSwelldirDaymax() != null) setDynaAttr(targetNode, "swelldirDaymaxAzimuth", Math.round(weather.getSwelldirDaymax()));
				if(weather.getWindDirection() != null) setDynaAttr(targetNode, "windDirectionAzimuth", Math.round(weather.getWindDirection()));
				if(weather.getWindDirection2400() != null) setDynaAttr(targetNode, "windDirection2400Azimuth", Math.round(weather.getWindDirection2400()));
				if(weather.getWinddirDaymax() != null) setDynaAttr(targetNode, "winddirDaymaxAzimuth", Math.round(weather.getWinddirDaymax()));
				if(weather.getSurfacecurrentdir() != null) setDynaAttr(targetNode, "surfacecurrentdirAzimuth", Math.round(weather.getSurfacecurrentdir()));
				if(weather.getSurfaceCurrentDirection2400() != null) setDynaAttr(targetNode, "surfaceCurrentDirection2400Azimuth", Math.round(weather.getSurfaceCurrentDirection2400()));
				if(weather.getSurfaceCurrentDirectionDaymax() != null) setDynaAttr(targetNode, "surfaceCurrentDaymaxAzimuth", Math.round(weather.getSurfaceCurrentDirectionDaymax()));
			}
		}
		
		if (StringUtils.isNotBlank(userSession.getCurrentWellUid())) {
			Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSession.getCurrentWellUid());
			if (well!=null) setDynaAttr(targetNode, "onOffShore", well.getOnOffShore());
		}
		
	}
	
	private void reportWeatherDirectionInAzimuthSetUom(CommandBean commandBean, CommandBeanTreeNode node) throws Exception{
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "waveDirection");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@waveDirectionAzimuth", thisConverter.getUOMMapping());
		}
		thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "swelldirDaymax");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@waveDirection2400Azimuth", thisConverter.getUOMMapping());
		}
		thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "wavedirDaymax");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@wavedirDaymaxAzimuth", thisConverter.getUOMMapping());
		}
		thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "swellDirection");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@swellDirectionAzimuth", thisConverter.getUOMMapping());
		}
		thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "swellDirection2400");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@swellDirection2400Azimuth", thisConverter.getUOMMapping());
		}
		thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "swelldirDaymax");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@swelldirDaymaxAzimuth", thisConverter.getUOMMapping());
		}
		thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "windDirection");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@windDirectionAzimuth", thisConverter.getUOMMapping());
		}
		thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "windDirection2400");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@windDirection2400Azimuth", thisConverter.getUOMMapping());
		}
		thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "winddirDaymax");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@winddirDaymaxAzimuth", thisConverter.getUOMMapping());
		}
		thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "surfacecurrentdir");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@surfacecurrentdirAzimuth", thisConverter.getUOMMapping());
		}
		thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "surfaceCurrentDirection2400");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@surfaceCurrentDirection2400Azimuth", thisConverter.getUOMMapping());
		}
		thisConverter = new CustomFieldUom(commandBean, WeatherEnvironment.class, "surfaceCurrentDirectionDaymax");
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@surfaceCurrentDirectionDaymaxAzimuth", thisConverter.getUOMMapping());
		}
	}
	
	private String azimuthString(String text){
		String[] whole = text.split("\\.");
		if(whole[0].length() == 1){
			text = "00" + whole[0];
		}else if(whole[0].length() == 2){
			text = "0" + whole[0];
		}else{
			text = whole[0].toString();
		}
		return text;
	}
	
	private void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		}
	}
}