package com.idsdatanet.d2.drillnet.lookupRigEquipmentList;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.LookupRigEquipmentList;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LookupRigEquipmentListDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		if (obj instanceof LookupRigEquipmentList) {
			LookupRigEquipmentList thisLookupRigEquipmentList = (LookupRigEquipmentList) obj;
			String lookupUid = thisLookupRigEquipmentList.getLookupRigEquipmentListUid();
			updateEquipmentLookupValue(lookupUid);
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object obj = node.getData();
		if (obj instanceof LookupRigEquipmentList) {
			LookupRigEquipmentList thisLookupRigEquipmentList = (LookupRigEquipmentList) obj;
			String lookupUid = thisLookupRigEquipmentList.getLookupRigEquipmentListUid();
			updateEquipmentLookupValue(lookupUid);
		}
	}
	
	private void updateEquipmentLookupValue(String lookupUid) throws Exception {
		
		if (lookupUid !=null || !StringUtils.isEmpty(lookupUid)){
			String sql = "SELECT makeModel,type,serialNumber,manufacturerUid,componentNumber,maxVolumeCapacity,name,equipmentNumber FROM LookupRigEquipmentList WHERE lookupRigEquipmentListUid=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			
			String makeModel ="";
			String type ="";
			String serialNumber ="";
			String manufacturerUid ="";
			String componentNumber = "";
			Double maxVolumeCapacity = null;
			String name ="";
			String equipmentNumber="";
			String companyName="";
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"lookupUid", lookupUid);
			
			
			if (lstResult.size()>0){
				Object[] a = (Object[]) lstResult.get(0);
				
				if (a[0] != null) {
					makeModel = a[0].toString();
				}
				if (a[1] != null) {
					type = a[1].toString();
				}
				if (a[2] != null) {
					serialNumber = a[2].toString();
				}
				if (a[3] != null) {
					manufacturerUid = a[3].toString();
					List <String> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select companyName from LookupCompany where (isDeleted = false or isDeleted is null) and lookupCompanyUid = :companyLookupUid", "companyLookupUid", manufacturerUid);
					if (lstResult2.size() > 0) {
						companyName = lstResult2.get(0);
					}
				}
				if (a[4] != null) {
					componentNumber = a[4].toString();
				}
				if (a[5] != null) {
					maxVolumeCapacity = (Double) a[5];
				}
				if (a[6] != null) {
					name = a[6].toString();
				}
				if (a[7] != null) {
					equipmentNumber = a[7].toString();
				}
			}
			

			String strSql = "UPDATE RigMobileWinch SET makeModel=:makeModel,type=:type,serialNumber=:serialNumber,lastEditDatetime=:lastEditDatetime WHERE winchNumber=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, new String[] {"makeModel", "type","serialNumber","lastEditDatetime","lookupUid"} , new Object[] {makeModel, type, serialNumber,new Date(),lookupUid});
			
			String strSql2 = "UPDATE RigPump SET unitNumber=:unitNumber,manufacturer=:manufacturer,manufacturerUid=:manufacturerUid,unitModel=:makeModel,pumpType=:type,unitSerialNumber=:serialNumber,unitName=:unitName,lastEditDatetime=:lastEditDatetime WHERE lookupRigEquipmentListUid=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, new String[] {"unitNumber","manufacturer","manufacturerUid","makeModel", "type","serialNumber","unitName","lastEditDatetime","lookupUid"} , new Object[] {equipmentNumber,companyName,manufacturerUid,makeModel, type, serialNumber,name,new Date(),lookupUid});
			
			String strSql3 = "UPDATE RigPreventorControlUnit SET makeModel=:makeModel,type=:type,serialNumber=:serialNumber,lastEditDatetime=:lastEditDatetime WHERE pcuNumber=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql3, new String[] {"makeModel", "type","serialNumber","lastEditDatetime","lookupUid"} , new Object[] {makeModel, type, serialNumber,new Date(),lookupUid});
			
			String strSql4 = "UPDATE RigPowerUnitHydrTongs SET makeModel=:makeModel,type=:type,serialNumber=:serialNumber,lastEditDatetime=:lastEditDatetime WHERE puTongsNumber=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql4, new String[] {"makeModel", "type","serialNumber","lastEditDatetime","lookupUid"} , new Object[] {makeModel, type, serialNumber,new Date(),lookupUid});
			
			String strSql5 = "UPDATE RigHydrTongs SET makeModel=:makeModel,type=:type,serialNumber=:serialNumber,lastEditDatetime=:lastEditDatetime WHERE tongsNumber=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql5, new String[] {"makeModel", "type","serialNumber","lastEditDatetime","lookupUid"} , new Object[] {makeModel, type, serialNumber,new Date(),lookupUid});
			
			String strSql6 = "UPDATE RigPowerUnitRotaryTable SET makeModel=:makeModel,type=:type,serialNumber=:serialNumber,lastEditDatetime=:lastEditDatetime WHERE puRtNumber=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql6, new String[] {"makeModel", "type","serialNumber","lastEditDatetime","lookupUid"} , new Object[] {makeModel, type, serialNumber,new Date(),lookupUid});
			
			String strSql7 = "UPDATE RigRotaryTable SET makeModel=:makeModel,type=:type,serialNumber=:serialNumber,lastEditDatetime=:lastEditDatetime WHERE rtNumber=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql7, new String[] {"makeModel", "type","serialNumber","lastEditDatetime","lookupUid"} , new Object[] {makeModel, type, serialNumber,new Date(),lookupUid});
			
			String strSql8 = "UPDATE RigGenerators SET generatorNumber=:generatorNumber,makeModel=:makeModel,type=:type,serialNumber=:serialNumber,engineNumber=:engineNumber,lastEditDatetime=:lastEditDatetime WHERE lookupRigEquipmentListUid=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql8, new String[] {"generatorNumber","makeModel", "type","serialNumber","engineNumber","lastEditDatetime","lookupUid"} , new Object[] {equipmentNumber,makeModel, type, serialNumber,componentNumber,new Date(),lookupUid});
			
			String strSql9 = "UPDATE RigContainers SET serialNumber=:serialNumber,type=:type,lastEditDatetime=:lastEditDatetime WHERE lookupRigEquipmentListUid=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql9, new String[] {"serialNumber","type","lastEditDatetime","lookupUid"} , new Object[] {serialNumber,type,new Date(),lookupUid});
			
			String strSql10 = "UPDATE RigTanks SET serialNumber=:serialNumber,maxVolumeCapacity=:maxVolumeCapacity,lastEditDatetime=:lastEditDatetime WHERE tankNumber=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql10, new String[] {"serialNumber","maxVolumeCapacity","lastEditDatetime","lookupUid"} , new Object[] {serialNumber,maxVolumeCapacity,new Date(),lookupUid});
			
			String strSql11 = "UPDATE RigSubstructure SET serialNumber=:serialNumber,lastEditDatetime=:lastEditDatetime WHERE substructureNumber=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql11, new String[] {"serialNumber","lastEditDatetime","lookupUid"} , new Object[] {serialNumber,new Date(),lookupUid});
			
			String strSql12 = "UPDATE RigCompressors SET compressorNumber=:compressorNumber,serialNumber=:serialNumber,lastEditDatetime=:lastEditDatetime WHERE lookupRigEquipmentListUid=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql12, new String[] {"compressorNumber","serialNumber","lastEditDatetime","lookupUid"} , new Object[] {equipmentNumber,serialNumber,new Date(),lookupUid});
			
			String strSql13 = "UPDATE RigSwivel SET serialNumber=:serialNumber,lastEditDatetime=:lastEditDatetime WHERE swivelNumber=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql13, new String[] {"serialNumber","lastEditDatetime","lookupUid"} , new Object[] {serialNumber,new Date(),lookupUid});
		} 
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		if ("LookupRigEquipmentList".equals(meta.getTableClass().getSimpleName())) {
			String equipmentType = (String) commandBean.getRoot().getDynaAttr().get("equipmentType");
			if (StringUtils.isNotBlank(equipmentType)) {
				if (StringUtils.isNotBlank(conditionClause)) conditionClause += " AND "; 
				conditionClause += " equipmentCategory=:equipmentCategory ";
				query.addParam("equipmentCategory", equipmentType);
				
			}
			
			return conditionClause;
		}
		return null;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
