package com.idsdatanet.d2.drillnet.lookupRigEquipmentList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.LookupRigEquipmentList;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class LookupRigEquipmentListCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {

    @Override
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		if("equipmentCategory".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
			equipmentCategoryReset(request, commandBean, targetCommandBeanTreeNode);
		}
	}

    public void equipmentCategoryReset(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	
    	if(node.getData() instanceof LookupRigEquipmentList){
    		LookupRigEquipmentList thisLookupRigEquipmentList = (LookupRigEquipmentList) node.getData();
    		String lookupUid = thisLookupRigEquipmentList.getLookupRigEquipmentListUid();
    		
    		String sql = "SELECT componentNumber,maxVolumeCapacity FROM LookupRigEquipmentList WHERE lookupRigEquipmentListUid=:lookupUid AND (isDeleted IS NULL OR isDeleted = false)";
			
			String componentNumber = "";
			Double maxVolumeCapacity = null;
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,"lookupUid", lookupUid);
			
			if (lstResult.size()>0){
				Object[] a = (Object[]) lstResult.get(0);
				
				if (a[0] != null) {
					componentNumber = a[0].toString();
				}
				if (a[1] != null) {
					maxVolumeCapacity = (Double) a[1];
				}
			}
    		String equipmentCategory = thisLookupRigEquipmentList.getEquipmentCategory();
    		
    		if (!StringUtils.equals(equipmentCategory, "rigGenerator")) {
    			thisLookupRigEquipmentList.setComponentNumber(null);
    		} else {
    			thisLookupRigEquipmentList.setComponentNumber(componentNumber);
    		}
    		
    		if (!StringUtils.equals(equipmentCategory, "rigTank")) {
    			thisLookupRigEquipmentList.setMaxVolumeCapacity(null);
    		} else {
    			thisLookupRigEquipmentList.setMaxVolumeCapacity(maxVolumeCapacity);
    		}
		}
	}
}
