package com.idsdatanet.d2.drillnet.hseIncident;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class PetronasHseNumberIncidentPerCategoryDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// To be overriden
	}
	
	// Ticket: 44400 - Change HSE Event Count to be Well-based (All until TODAY - reportDay)
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
	    String wellUid = userContext.getUserSelection().getWellUid();
        Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
        if (daily == null) {
        	return;
        }
        Date todayDate = daily.getDayDate();
        
        Calendar thisCalender = Calendar.getInstance();
		thisCalender.setTime(todayDate);
		thisCalender.add(Calendar.DATE, 1);
		thisCalender.add(Calendar.SECOND, -1);
		Date nextDate = thisCalender.getTime();
		
        String strSql = "SELECT h.incidentCategory, SUM(h.numberOfIncidents) AS sumNumber FROM HseIncident h, Daily d " +
                "WHERE (h.isDeleted = false OR h.isDeleted IS NULL) " +
                "AND (d.isDeleted = false or d.isDeleted IS NULL) " +
                "AND h.wellUid = :wellUid " +
                "AND d.dailyUid = h.dailyUid " +
                "AND h.hseEventdatetime <= :todayDate " + 
                "AND h.incidentCategory IN (SELECT hseCategory FROM LookupIncidentCategory WHERE (isDeleted = false OR isDeleted IS NULL) AND isSafety ='1') " +
                "GROUP BY h.incidentCategory";
        
        String[] paramsFields = {"todayDate", "wellUid"};
        Object[] paramsValues = {nextDate, wellUid};
		
		List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (!lstResult.isEmpty()) {
			for (Object objResult: lstResult) {
				Object[] obj = (Object[]) objResult;
				String incidentCategory = (obj[0] != null) ? obj[0].toString() : "";
				String sumNumberOfIncidents = (obj[1] != null) ? obj[1].toString() : "0";
				ReportDataNode thisReportNode = reportDataNode.addChild("numberIncidentsPerCategory");
				thisReportNode.addProperty("incidentCategory", incidentCategory);
				thisReportNode.addProperty("sumNumberOfIncidents", sumNumberOfIncidents);
				
				strSql = "SELECT h.incidentCategory, SUM(h.numberOfIncidents) AS sumNumber FROM HseIncident h, Daily d " +
			                "WHERE (h.isDeleted = false OR h.isDeleted IS NULL) " +
			                "AND (d.isDeleted = false or d.isDeleted IS NULL) " +
			                "AND h.wellUid = :wellUid " +
			                "AND d.dailyUid = h.dailyUid " +
			                "AND h.hseEventdatetime > :todayDate " + 
			                "AND h.hseEventdatetime <= :nextDate " + 
			                "AND h.incidentCategory = :incidentCategory";
		        
				String[] paramsField = {"todayDate", "nextDate", "wellUid", "incidentCategory"};
				Object[] paramsValue = {todayDate, nextDate, wellUid, incidentCategory};
				
				List<Object> lstDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsField, paramsValue);
				
				if (!lstDaily.isEmpty()) {
					for (Object objDailyResult: lstDaily) {
						Object[] objDaily = (Object[]) objDailyResult;
						String dailyNumberOfIncidents = (objDaily[1] != null) ? objDaily[1].toString() : "0";
						thisReportNode.addProperty("dailyNumberOfIncidents", dailyNumberOfIncidents);
					}
				}
			}
		}else{
			ReportDataNode thisReportNode = reportDataNode.addChild("numberIncidentsPerCategory");
			thisReportNode.addProperty("incidentCategory", "");
		}
	}	
}		
