package com.idsdatanet.d2.drillnet.bharun;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.report.validation.CommandBeanReportValidator;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class BharunFWRReportValidation implements CommandBeanReportValidator {
	public void validate(CommandBean commandBean, ReportValidation reportValidation, String reportType, UserSelectionSnapshot userSelection) throws Exception {
		if(commandBean.getRoot() == null) return;
		Map<String, CommandBeanTreeNode> bharun_list = commandBean.getRoot().getList().get(Bharun.class.getSimpleName());
		if(bharun_list == null) return;
		
		for(CommandBeanTreeNode bharun_node : bharun_list.values()){
			if(! bharun_node.getInfo().isTemplateNode()){
				Bharun bharun = (Bharun) bharun_node.getData();
				if(StringUtils.isBlank(bharun.getDailyidIn()) && StringUtils.isNotBlank(bharun.getDailyidOut())){
					reportValidation.addError("BHA Run #" + bharun.getBhaRunNumber() + " does not have a date for Date In. This BHA may appear out of sequence in the report.");
				}else if ((StringUtils.isNotBlank(bharun.getDailyidIn()) && StringUtils.isBlank(bharun.getDailyidOut()))){
					reportValidation.addError("BHA Run #" + bharun.getBhaRunNumber() + " does have a date for Date In but does not have a date for Date Out. This BHA may appear out of sequence in the report.");
				} else if ((StringUtils.isBlank(bharun.getDailyidIn()) && StringUtils.isBlank(bharun.getDailyidOut()))){
					reportValidation.addError("BHA Run #" + bharun.getBhaRunNumber() + " does not have a date for Date In and Date Out. This BHA may appear out of sequence in the report.");
				}
			}
		}
	}
}
