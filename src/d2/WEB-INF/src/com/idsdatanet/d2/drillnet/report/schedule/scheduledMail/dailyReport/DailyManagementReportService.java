package com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.dailyReport;

import com.idsdatanet.d2.drillnet.report.ReportManager;

public class DailyManagementReportService extends AbstractReportService{
	private ReportManager reportManager = null;
	
	synchronized public void run() throws Exception
	{
		this.reportManager.generateManagementReportOnCurrentSystemDatetime(this.getReportAutoEmail());
	}
	
	public void setReportManager(ReportManager value){
		this.reportManager = value;
	}
	
}
