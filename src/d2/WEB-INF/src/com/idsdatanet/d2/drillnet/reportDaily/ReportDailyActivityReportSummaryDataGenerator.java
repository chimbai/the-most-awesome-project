package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.report.ReportOption;

public class ReportDailyActivityReportSummaryDataGenerator implements ReportDataGenerator {
	
	private String defaultReportType;
	
	public void setReportType(String value){
		this.defaultReportType = value;
	}

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
		if(this.defaultReportType != null){
			reportType = this.defaultReportType;
		}
		
		//String currentWellUid = userContext.getUserSelection().getWellUid();
		String currentUserUid = userContext.getUserSelection().getUserUid();
		
		ArrayList<String> wellArray = null;
		Map<String, Object> wellMultiSelectList = userContext.getUserSelection().getCustomProperties(); 
		if (wellMultiSelectList != null)
		{
			for (Entry<String, Object> entry : wellMultiSelectList.entrySet())
			{
				String field = entry.getKey();
				if (field.equalsIgnoreCase("@wells"))
				{
					ReportOption rptOption = (ReportOption) entry.getValue();
					wellArray  = (ArrayList<String>) rptOption.getValues();							
				}
			}
		}
		
		//return if well list is null 
		if (wellArray==null) return;
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		for(String wellUid : wellArray){
								
			//get the all operation under selected well
			String sql = "SELECT A.wellUid,A.wellName,o.operationUid,o.operationName,Min(d.reportDatetime) as startDate,Max(d.reportDatetime) as EndDate FROM Well A, Wellbore B,Operation o, ReportDaily d " +
			             "WHERE (o.isDeleted = FALSE OR o.isDeleted IS NULL) AND A.wellUid = B.wellUid and B.wellboreUid = o.wellboreUid and o.operationUid = d.operationUid " +
			             "and (A.isDeleted=FALSE or A.isDeleted IS NULL)and (B.isDeleted=FALSE or B.isDeleted IS NULL) and (d.isDeleted=FALSE or d.isDeleted IS NULL) and " +
			             "(((o.tightHole = FALSE OR o.tightHole IS NULL) AND (o.isCampaignOverviewOperation = FALSE OR o.isCampaignOverviewOperation IS NULL) " +
			             "AND (o.operationUid NOT IN (SELECT a.operationUid FROM OpsTeamOperation a, OpsTeam e WHERE e.opsTeamUid = a.opsTeamUid AND " +
			             "(a.isDeleted = false OR a.isDeleted IS NULL) AND (e.isPublic = false OR e.isPublic IS NULL)) OR o.operationUid IN " +
			             "(SELECT b.operationUid FROM OpsTeamOperation b, OpsTeamUser c WHERE b.opsTeamUid = c.opsTeamUid AND (b.isDeleted = FALSE OR b.isDeleted is null) AND " +
			             "(c.isDeleted = FALSE OR c.isDeleted IS NULL) AND c.userUid = :userUid))) OR (o.tightHole = TRUE AND o.operationUid IN (SELECT d.operationUid FROM " +
			             "TightHoleUser d WHERE (d.isDeleted = FALSE OR d.isDeleted IS NULL) AND d.userUid = :userUid))) and d.reportType<>'DGR' AND o.wellUid =:wellUid  GROUP BY A.wellName,A.wellUid,o.operationUid,o.operationName " +
			             "order by A.wellName";
			
			String[] paramsFields = {"userUid", "wellUid"};
			Object[] paramsValues = {currentUserUid, wellUid}; 
			List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
		
			if (lstResult.size()>0){
			
				ReportDataNode wellDataNode = reportDataNode.addChild("Well");
				ReportDataNode opsDataNode = reportDataNode.addChild("Operation");
				ReportDataNode reportDailyDataNode = reportDataNode.addChild("ReportDaily");
				
				String currWellUid="";		
				String operationUid="";
				String operationName="";
				String WellName="";
				ReportDataNode currNode = null; 
				String EventSummary = "";
						
				for(Object[] item : lstResult)
				{
					wellUid = item[0].toString();
					WellName = item[1].toString();
					
					if (!currWellUid.equalsIgnoreCase(wellUid))
					{
						currNode  = wellDataNode.addChild("Data");
						currNode.addProperty("wellUid", wellUid);
						currNode.addProperty("wellName", WellName);
						
						currWellUid = wellUid;
					}
				
					operationUid = item[2].toString();
					operationName = item[3].toString();
					currNode  = opsDataNode.addChild("Data");
					currNode.addProperty("wellUid", wellUid);
					currNode.addProperty("WellName", WellName);
					currNode.addProperty("operationUid", operationUid);
					currNode.addProperty("operationName", operationName);
					currNode.addProperty("StartDate", item[4].toString());
					currNode.addProperty("EndDate", item[5].toString());
									
					sql = "select reportDailyUid,operationUid,reportNumber,reportDatetime,reportPeriodSummary from ReportDaily where (isDeleted=FALSE or isDeleted IS NULL) and reportType<>'DGR' and operationUid =:operationUid order by reportDatetime"; 
					paramsFields = new String[]{"operationUid"};
					paramsValues =new Object[] {operationUid}; 
					List <Object[]>lstSummary = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
									
					for(Object[] dataitem : lstSummary)
					{						
						currNode  = reportDailyDataNode.addChild("Data");
						currNode.addProperty("reportDailyUID", dataitem[0].toString());
						currNode.addProperty("operationUid", dataitem[1].toString());
						currNode.addProperty("operationName", operationName);
						currNode.addProperty("reportNumber", dataitem[2].toString());
						currNode.addProperty("reportDatetime", dataitem[3].toString());
					
						if (dataitem[4] != null && StringUtils.isNotBlank(dataitem[4].toString()))
							EventSummary = dataitem[4].toString();
						else
							EventSummary = "";
						currNode.addProperty("reportPeriodSummary", EventSummary);
					}		
				}	
			}
		}
	}
}