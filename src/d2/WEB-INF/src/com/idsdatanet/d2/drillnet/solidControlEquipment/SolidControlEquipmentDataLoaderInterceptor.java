package com.idsdatanet.d2.drillnet.solidControlEquipment;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class SolidControlEquipmentDataLoaderInterceptor implements DataLoaderInterceptor{
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String currentClass		= meta.getTableClass().getSimpleName();
		String customCondition 	= "";
		String equipmentType = "";
		
		if(currentClass.equals("Centrifuge")) equipmentType = "centrifuge";
		if(currentClass.equals("Shaker")) equipmentType = "shaker";
		

		if(currentClass.equals("Centrifuge") || currentClass.equals("Shaker")) {
			customCondition = "(isDeleted = false or isDeleted is null) and dailyUid=:currentDailyUid and group_uid=:groupUid ";
			customCondition += "and equipmentType=:equipmentType";
			query.addParam("currentDailyUid", userSelection.getDailyUid());
			query.addParam("groupUid", userSelection.getGroupUid());
			query.addParam("equipmentType", equipmentType);
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		return null;
	}
	public DataDefinitionHQLQuery generateHQL(CommandBean arg0,
			UserSelectionSnapshot arg1, HttpServletRequest arg2,
			TreeModelDataDefinitionMeta arg3, DataDefinitionHQLQuery arg4,
			CommandBeanTreeNode arg5) throws Exception {
		return null;
	}
	public String generateHQLFromClause(String arg0, CommandBean arg1,
			UserSelectionSnapshot arg2, HttpServletRequest arg3,
			TreeModelDataDefinitionMeta arg4, CommandBeanTreeNode arg5)
			throws Exception {
		return null;
	}
	public String generateHQLOrderByClause(String arg0, CommandBean arg1,
			UserSelectionSnapshot arg2, HttpServletRequest arg3,
			TreeModelDataDefinitionMeta arg4, CommandBeanTreeNode arg5)
			throws Exception {
		return null;
	}
}
