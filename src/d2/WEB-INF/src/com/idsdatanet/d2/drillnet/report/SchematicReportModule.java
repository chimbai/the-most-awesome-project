package com.idsdatanet.d2.drillnet.report;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.UserRequestContext;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.schematics.report.LastestSchematicReportListener;

public class SchematicReportModule extends DefaultReportModule{
	
	
	private static final String LATEST_REPORT_FLAG = "latestReport";

	private static final String FIELD_SELECTED_DATE = "";
	private Boolean hasLatestFlag=false;
	private Boolean generateOnDaily=false;
	
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		
		Date reportDate = null;
		String dailyUid=null;
		
		UserSelectionSnapshot userSelection=new UserSelectionSnapshot(session);
		if (hasLatestFlag){	
			Object dateParam = commandBean.getRoot().getDynaAttr().get(FIELD_SELECTED_DATE);
			if(isEmptyString(dateParam)) dateParam = LATEST_REPORT_FLAG;
			if(LATEST_REPORT_FLAG.equals(dateParam)){
				reportDate = new Date();
			}else{
				reportDate = CommonDateParser.parse(dateParam.toString());
			}
			userSelection.setCustomProperty(LastestSchematicReportListener.IS_SCHEMATIC_DAILY,false);
			userSelection.setCustomProperty(LastestSchematicReportListener.SCHEMATIC_DATE,reportDate);
		}else
		{
			List<Daily> result=ApplicationUtils.getConfiguredInstance().getDaysInOperation(session.getCurrentOperationUid());
			for(Daily d:result){
				if (dailyUid==null)
				{
					dailyUid=d.getDailyUid();
					reportDate=d.getDayDate();
				}else{
					if (d.getDayDate().after(reportDate))
					{
						dailyUid=d.getDailyUid();
						reportDate=d.getDayDate();
					}
				}
			}

			if (dailyUid!=null){
				if (generateOnDaily){
					dailyUid= session.getCurrentDailyUid();
				}
			}
			else
				dailyUid= session.getCurrentDailyUid();
			
			userSelection=UserSelectionSnapshot.getInstanceForCustomDaily(session, dailyUid);
			userSelection.setCustomProperty(LastestSchematicReportListener.IS_SCHEMATIC_DAILY,this.getDailyRequired());
		}
		return super.submitReportJob(userSelection,this.getParametersForReportSubmission(session, request, commandBean));
	}
	
	
	
	private boolean isEmptyString(Object value){
		if(value == null) return true;
		return StringUtils.isBlank(value.toString());
	}
	
	protected ReportOutputFile onLoadReportOutputFile(ReportOutputFile reportOutputFile){
		if(reportOutputFile.isAssociatedDailyAvailable()){
			try {
				// exclude this report from the list if the related reportDaily is outside the access scope
				ReportDaily reportDaily = getRelatedReportDaily(reportOutputFile.getDailyUid(), this.getDailyType());
				UserSession userSession = UserRequestContext.getThreadLocalInstance().getUserSessionOptional();
				if (userSession != null && !userSession.withinAccessScope(reportDaily)) {
					return null;
				}
			} catch (Exception e) {
				// if we can't figure out the access scope, bail
			}
			return reportOutputFile;
		}else{
			return null;
		}
	}

	public Boolean getHasLatestFlag() {
		return hasLatestFlag;
	}
	public void setHasLatestFlag(Boolean hasLatestFlag) {
		this.hasLatestFlag = hasLatestFlag;
	}
	
	public Boolean getGenerateOnDaily(){
		return generateOnDaily;
	}
	public void setGenerateOnDaily(Boolean generateOnDaily){
		this.generateOnDaily = generateOnDaily;
	}
}
