package com.idsdatanet.d2.drillnet.report;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WirelineSuiteReportModule extends DefaultReportModule{
	
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		commandBean.getSystemMessage().clear();
		
		Boolean selectionOption = isTrue(commandBean.getRoot().getDynaAttr().get("hideChart"));
		
		UserSelectionSnapshot userSelection = UserSelectionSnapshot.getInstanceForCustomDaily(session, session.getCurrentDailyUid());
		userSelection.setCustomProperty("hideChart", selectionOption);
		ReportJobParams reportJobData=this.getParametersForReportSubmission(session, request, commandBean);
		
		Map runtimeContext=new HashMap();
		runtimeContext.put("WirelineSuiteHideChart", selectionOption);
		
		reportJobData.setRuntimeContext(runtimeContext);
		
		return super.submitReportJob(userSelection, reportJobData);
		
	}
	
	private Boolean isTrue(Object value){
		if (value==null)
			return true;
		if (value.equals("1") || value.equals("true"))
			return true;
			return false;
	}

}
