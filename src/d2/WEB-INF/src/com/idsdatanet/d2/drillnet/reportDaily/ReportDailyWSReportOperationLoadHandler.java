package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.report.OperationLoadHandler;

public class ReportDailyWSReportOperationLoadHandler implements OperationLoadHandler {

	public List getOperationList(Date startDate, Date endDate) throws Exception {
		if (startDate !=null && endDate !=null) {
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select DISTINCT o.operationUid " +
					"From Operation o, Daily d, ReportDaily g WHERE (o.isDeleted is null or o.isDeleted = false) AND " +
					"(d.isDeleted is null or d.isDeleted = false) AND (g.isDeleted is null or g.isDeleted = false) AND " +
					"(o.operationUid=d.operationUid) AND (d.dailyUid=g.dailyUid) AND d.dayDate >=:startDate AND " +
					"d.dayDate<=:endDate", new String[] {"startDate","endDate"}, new Object[]{startDate, endDate});
			return list;
		}
		return null;
	}
}