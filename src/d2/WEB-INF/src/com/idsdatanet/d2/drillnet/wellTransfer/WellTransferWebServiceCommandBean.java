package com.idsdatanet.d2.drillnet.wellTransfer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;

import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.PublishSubscribe;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.publishsubscribe.PublishSubscribeManager;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.AbstractGenericWebServiceCommandBean;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


public class WellTransferWebServiceCommandBean extends AbstractGenericWebServiceCommandBean {
	private static final String GET_WELL = "get_well";
	private static final String GET_WELLBORE = "get_wellbore";
	private static final String GET_OPERATION = "get_operation";
	private static final String CREATE_PS = "create_ps";

	@Override
	public void process(HttpServletRequest request,	HttpServletResponse response, BindException bindError) throws Exception {
		String request_key = request.getParameter("request_key");
		String wellUid = request.getParameter("wellUid");
		String wellboreUid = request.getParameter("wellboreUid");
		JSONObject root = new JSONObject();
		if (StringUtils.isBlank(request_key)) {
			this.responseWithErrors(response, "Parameter request_key is not provided.");
		}
		if (request_key.equals(GET_WELL)) {
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			for (String[] obj : this.getWells()) {
				writer.startElement("well");
				writer.addElement("uid", obj[0]);
				writer.addElement("name", obj[1]);
				writer.endElement();
			}
			writer.endElement();
			writer.close();	
		} else if (request_key.equals(GET_WELLBORE)) {
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			for (String[] obj : this.getWellbores(wellUid)) {
				writer.startElement("wellbore");
				writer.addElement("uid", obj[0]);
				writer.addElement("name", obj[1]);
				writer.endElement();
			}
			writer.endElement();
			writer.close();	
			
		} else if (request_key.equals(GET_OPERATION)) {
			SimpleXmlWriter writer = new SimpleXmlWriter(response);
			writer.startElement("root");
			for (String[] obj : this.getOperations(wellUid, wellboreUid)) {
				writer.startElement("operation");
				writer.addElement("uid", obj[0]);
				writer.addElement("name", obj[1]);
				writer.addElement("gid", obj[2]);
				writer.endElement();
			}
			writer.endElement();
			writer.close();
		} else if (request_key.equals(CREATE_PS)) {
			String host_url = request.getParameter("host_url");
			String operationUid = request.getParameter("operationUid");
			String groupId = request.getParameter("group_id");
			String isActive = request.getParameter("is_active");
			
			String sql = "from ImportExportServerProperties where (isDeleted = false or isDeleted is null) "
					+ "and dataTransferType='subscribe' and  endPoint='"+ host_url + "'";
			
			List<ImportExportServerProperties> iesp = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
			String iespUid = null;
			if (iesp.size()>0) {
				ImportExportServerProperties ies = (ImportExportServerProperties) iesp.get(0);
				iespUid = ies.getServerPropertiesUid();
			}else {
				ImportExportServerProperties newiesp = new ImportExportServerProperties();
				newiesp.setEndPoint(host_url);
				newiesp.setServiceName(groupId);
				newiesp.setDataTransferType("subscribe");
				newiesp.setDepotVersion("1.0");
				newiesp.setUsername("ps_user");
				newiesp.setPassword("ps123");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newiesp);
				iespUid = newiesp.getServerPropertiesUid();
			}
	
			String psSQL = "from PublishSubscribe where (isDeleted = false or isDeleted is null) "
					+ "AND type='publisher' AND operationId='"+operationUid+"' AND serverPropertiesUid='" + iespUid + "'";
			
			List<PublishSubscribe> psList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(psSQL);
			
			if (psList.size()>0) {
				PublishSubscribe ps = (PublishSubscribe) psList.get(0);
				if (isActive.equals("1")) ps.setIsActive(true);
				else ps.setIsActive(false);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ps);
			}else {
				Operation operation = (Operation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
				Well well = (Well)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, operation.getWellUid());
				Wellbore wellbore = (Wellbore)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
				
				PublishSubscribe newPS = new PublishSubscribe();
				newPS.setServerPropertiesUid(iespUid);
				newPS.setWellId(well.getWellUid());
				newPS.setWellName(well.getWellName());
				newPS.setWellboreId(wellbore.getWellboreUid());
				newPS.setWellboreName(wellbore.getWellboreName());
				newPS.setOperationId(operation.getOperationUid());
				newPS.setOperationName(operation.getOperationName());
				newPS.setType(PublishSubscribeManager.PUBLISHER);
				newPS.setClientGroupUid(groupId);
				if (isActive.equals("1")) newPS.setIsActive(true);
				else newPS.setIsActive(false);
				newPS.setGroupUid(operation.getGroupUid());
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newPS);
			}

			this.responseSuccess(response, root);
		} 	
	}
	
	private void responseSuccess(HttpServletResponse response, JSONObject data) throws Exception {
		JSONObject result = new JSONObject();
		result.put("success", true);
		result.put("response", data);
		response.getWriter().write(result.toString());
	}
	
	private void responseWithErrors(HttpServletResponse response, String errors) throws Exception {
		JSONObject result = new JSONObject();
		result.put("success", false);
		result.put("errors", errors);
		response.getWriter().write(result.toString());
	}
	
	private List<String[]> getWells() throws Exception {
		List<String[]> results = new ArrayList<String[]>();
		List<Well> wells = ApplicationUtils.getConfiguredInstance().getDaoManager().find("from Well w where (w.isDeleted = false or w.isDeleted is null)");
		if (wells != null && wells.size() > 0) {
			for (Well well : wells) {
				String wellName	= well.getWellName();
				results.add(new String[] {well.getWellUid(), wellName});
			}
		}
		Collections.sort(results, new OperationComparator());
		return results;	
	}
	
	private List<String[]> getWellbores(String wellUid) throws Exception {
		List<String[]> results = new ArrayList<String[]>();
		List<Wellbore> wellbores = ApplicationUtils.getConfiguredInstance().getDaoManager().find("from Wellbore where (isDeleted = false or isDeleted is null) and wellUid='" + wellUid + "'");
		if (wellbores != null && wellbores.size() > 0) {
			for (Wellbore wellbore : wellbores) {
				String wellboreName	= wellbore.getWellboreName();
				results.add(new String[] {wellbore.getWellboreUid(), wellboreName});
			}
		}
		Collections.sort(results, new OperationComparator());
		return results;	
	}
	
	private List<String[]> getOperations(String wellUid, String wellboreUid) throws Exception {
		List<String[]> results = new ArrayList<String[]>();
		List<Operation> operations = ApplicationUtils.getConfiguredInstance().getDaoManager().find("from Operation where (isDeleted = false or isDeleted is null) and wellUid='" + wellUid + "' and wellboreUid='" + wellboreUid + "'");
		if (operations != null && operations.size() > 0) {
			for (Operation operation : operations) {
				String operationName = operation.getOperationName();
				results.add(new String[] {operation.getOperationUid(), operationName, operation.getGroupUid()});
			}
		}
		Collections.sort(results, new OperationComparator());
		return results;	
	}
	
	private class OperationComparator implements Comparator<String[]> {
		public int compare(String[] v1, String[] v2) {
			if (v1 == null || v1[1] == null || v2 == null || v2[1] == null || StringUtils.equals((String)v1[1], (String)v2[1])) {
				return 0;
			}
			return WellNameUtil.getPaddedStr((String)v1[1]).compareTo(WellNameUtil.getPaddedStr((String)v2[1]));
		}
	}
	
	private String getShortName(String name) {
		if(name.length() > 30) {
			return name.substring(0, 30) + "...";
		} else {
			return name;
		}
	}
}
