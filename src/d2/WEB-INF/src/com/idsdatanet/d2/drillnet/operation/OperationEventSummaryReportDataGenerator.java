package com.idsdatanet.d2.drillnet.operation;

import java.util.List;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class OperationEventSummaryReportDataGenerator implements ReportDataGenerator {
	
	private String defaultReportType;
	
	public void setReportType(String value){
		this.defaultReportType = value;
	}

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
		if(this.defaultReportType != null){
			reportType = this.defaultReportType;
		}

		String currentWellUid = userContext.getUserSelection().getWellUid();
		String currentUserUid = userContext.getUserSelection().getUserUid();
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String wellName="";
		String strSql="select wellName from Well where (isDeleted is null or isDeleted = False) and wellUid=:thisWellUid";
		String[] paramsFields = {"thisWellUid"};
		Object[] paramsValues = {currentWellUid};
		
		List lstData = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstData.size()>0){
			for(Object item : lstData)
			{
				if (item != null)
				{
					wellName = String.valueOf(item);
				}
			}
		}
		
		//get the all operation under selected well
		String sql = "SELECT o.operationUid,o.operationName,o.operationSummary,Min(d.reportDatetime) as startDate,Max(d.reportDatetime) as EndDate FROM Operation o, ReportDaily d WHERE (o.isDeleted = FALSE OR o.isDeleted IS NULL) AND " +
		             "o.operationUid = d.operationUid and (d.isDeleted=FALSE or d.isDeleted IS NULL) and (((o.tightHole = FALSE OR o.tightHole IS NULL) AND (o.isCampaignOverviewOperation = FALSE OR o.isCampaignOverviewOperation IS NULL) " +
		             "AND (o.operationUid NOT IN (SELECT a.operationUid FROM OpsTeamOperation a, OpsTeam e WHERE e.opsTeamUid = a.opsTeamUid AND " +
		             "(a.isDeleted = false OR a.isDeleted IS NULL) AND (e.isPublic = false OR e.isPublic IS NULL)) OR o.operationUid IN " +
		             "(SELECT b.operationUid FROM OpsTeamOperation b, OpsTeamUser c WHERE b.opsTeamUid = c.opsTeamUid AND (b.isDeleted = FALSE OR b.isDeleted is null) AND " +
		             "(c.isDeleted = FALSE OR c.isDeleted IS NULL) AND c.userUid = :userUid))) OR (o.tightHole = TRUE AND o.operationUid IN (SELECT d.operationUid FROM " +
		             "TightHoleUser d WHERE (d.isDeleted = FALSE OR d.isDeleted IS NULL) AND d.userUid = :userUid))) and d.reportType<>'DGR' AND o.wellUid=:wellUid  GROUP BY o.operationUid,o.operationName,o.operationSummary " ;
		           //  "ORDER BY Max(d.report_datetime), Min(d.report_datetime) DESC";
			
		paramsFields = new String[]{"userUid", "wellUid"};
		paramsValues = new Object[]{currentUserUid, currentWellUid}; 
		List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
		
		if (lstResult.size()>0){
			
			ReportDataNode currentDataNode = reportDataNode.addChild("ReportInfo");
			currentDataNode.addProperty("wellname", wellName);
			currentDataNode.addProperty("welluid", currentWellUid);
					
			currentDataNode = reportDataNode.addChild("ReportSummary");
			
			for(Object[] item : lstResult)
			{
				ReportDataNode childDataNode = currentDataNode.addChild("Data");
				childDataNode.addProperty("operationUid", item[0].toString());
				childDataNode.addProperty("operationName", item[1].toString());
				
				if (item[2] == null)
					childDataNode.addProperty("EventSummary", "");
				else
					childDataNode.addProperty("EventSummary", item[2].toString());
				
				childDataNode.addProperty("startDate", item[3].toString());
				childDataNode.addProperty("endDate", item[4].toString());					
			}	
		}			
	}
}
