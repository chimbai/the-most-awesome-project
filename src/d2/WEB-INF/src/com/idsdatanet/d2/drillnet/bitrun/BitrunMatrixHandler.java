package com.idsdatanet.d2.drillnet.bitrun;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.matrix.DefaultMatrixHandler;
import com.idsdatanet.d2.drillnet.matrix.Matrix;
import com.idsdatanet.d2.drillnet.matrix.MatrixHandler;

public class BitrunMatrixHandler extends DefaultMatrixHandler {

	public void process(Matrix matrix, UserSelectionSnapshot userSelection) throws Exception {
		boolean f = false;
		try {
			Bitrun.class.getDeclaredField("isIncomplete");
			f = true;
		} catch (NoSuchFieldException e) {
			f = false;
		}
		String sql = "";
		if(f) {
			sql = "SELECT dailySummary.dailyUid,bitrun.bitrunUid from Bharun bharun, BharunDailySummary dailySummary, Bitrun bitrun WHERE " +
					"(bharun.isDeleted = false or bharun.isDeleted is null) " +
					"AND (bitrun.isDeleted = false or bitrun.isDeleted is null) " +
					"AND (dailySummary.isDeleted = false or dailySummary.isDeleted is null) " +
					"AND bharun.bharunUid=dailySummary.bharunUid AND bitrun.bharunUid=dailySummary.bharunUid " +
					"AND bharun.bharunUid=bitrun.bharunUid AND bitrun.operationUid = :operationUid";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", userSelection.getOperationUid());
			
			if(list != null && list.size() > 0) {
				Set<String> linkedHashSet = new LinkedHashSet<String>();
				String queryString = "";
				for(Object[] daily : list) {
					String dailyUid = null;
					if(daily[0] != null) {
						dailyUid = (String) daily[0];
					}
					if(daily[1] != null) {
						queryString = queryString + "'" + (String) daily[1] + "',";
					}
					linkedHashSet.add(dailyUid);
				}
				queryString = StringUtils.substring(queryString, 0, -1);
				if(queryString != "") { 
					sql = "select bitrun.bitrunUid,bitrun.isIncomplete from Bitrun bitrun where (bitrun.isDeleted = false or bitrun.isDeleted is null) and bitrun.bitrunUid IN (" + queryString + ")";
					List<Object[]> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
					for(String id: linkedHashSet) {
						String dailyUid = null;
						String bitrunUid = null;
						String isIncomplete = null;
						for(Object[] daily : list) {
							if(daily[0] != null) {
								dailyUid = (String) daily[0];
								if(id.equals(dailyUid)){
									if(daily[1] != null) {
										bitrunUid = (String) daily[1];
										for(Object[] bit : list2){
											String bitUid = (String) bit[0];
											if(bitrunUid.equals(bitUid)) {
												if(bit[1] != null){
													if(isIncomplete != "true") isIncomplete = String.valueOf(bit[1]);
												}
											}
										}
									}
								}
							}
						}
						matrix.addData(id,isIncomplete);
					}
				}
			}
		}else {
			sql = "SELECT distinct dailySummary.dailyUid from Bharun bharun, BharunDailySummary dailySummary, Bitrun bitrun WHERE " +
					"(bharun.isDeleted = false or bharun.isDeleted is null) " +
					"AND (bitrun.isDeleted = false or bitrun.isDeleted is null) " +
					"AND (dailySummary.isDeleted = false or dailySummary.isDeleted is null) " +
					"AND bharun.bharunUid=dailySummary.bharunUid AND bitrun.bharunUid=dailySummary.bharunUid " +
					"AND bharun.bharunUid=bitrun.bharunUid AND bitrun.operationUid = :operationUid";
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", userSelection.getOperationUid());
			
			if(list.size() > 0) {
				for(String dailyUid : list) {
					matrix.addData(dailyUid,"N/A");
				}
			}
		}
	}
}