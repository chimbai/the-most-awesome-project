package com.idsdatanet.d2.drillnet.wellExternalCatalog;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.WellExternalCatalog;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.core.DepotConstants;
import com.idsdatanet.depot.edm.externalwellsource.EDMExternalWellSourceImporter;
import com.idsdatanet.depot.wellview.WellViewImporter;
import com.idsdatanet.depot.wellview.WellViewIndexer;

import edu.emory.mathcs.backport.java.util.Arrays;
import net.sf.json.JSONObject;
	
public class WellExternalCatalogManager extends DefaultBeanSupport implements InitializingBean  {
	
	public static WellExternalCatalogManager configuredInstance = null;
	/**
	 * Set up the singleton getter/initializer for all classes to be able to fetch.
	 * @return WellExternalCatalogManager
	 * @throws Exception
	 */
	public static WellExternalCatalogManager getConfiguredInstance() throws Exception{
		if (configuredInstance == null) configuredInstance = getSingletonInstance(WellExternalCatalogManager.class);
		return configuredInstance;

	}

	/**
	 * Call by WellExternalCatalogDataNodeListener.onCustomFilterInvoked. Fetches the WEC and its respective server properties.
	 * Depending on when the user click on either "Import All From Server" or "Import Well/Wellbore Only" (invocationKey), it will run either
	 * this.forwardToImporter() or this.importWellWellbore(). Will check if server is setup, else throws error and sets a error system message.
	 * @param wec
	 * @param commandBean
	 * @param invocationKey
	 * @throws Exception
	 */
	public void importWEC(WellExternalCatalog wec, CommandBean commandBean, String importType) throws Exception {
		ImportExportServerProperties iesp = this.fetchServerProperties(wec.getImportExportServerPropertiesUid());
		this.checkIesp(iesp, wec, commandBean); // throws error if IESP is null and creates a system message
		// depending on the request, run either a job for import all, or process the WW directly and redirect to Operation/Daily creation screen
		if("importAll".equals(importType)) 
			WellExternalCatalogManager.getConfiguredInstance().forwardToImporter(iesp, wec);
		if("importWWOnly".equals(importType)) 
			WellExternalCatalogManager.getConfiguredInstance().importWellWellbore(iesp, wec, (BaseCommandBean) commandBean);
	}
	
	/**
	 * Call by WellExternalCatalogDataNodeListener.onCustomFilterInvoked when invocationKey="importSelected".
	 * If the user is multi-selecting WECs to import, we will perform the following:
	 * 1. Grab the WECUids request param set in wellExternalCatalog.js
	 * 2. Fetch WECs with server setups, fetch their respective ImportExportServerProperties, and use this.forwardToImporter() to submit the jobs
	 * @param request
	 * @param commandBean
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void importSelectedWECs(HttpServletRequest request, CommandBean commandBean) throws Exception {
		// Fetch the comma separate list of WEC uids that have been selected.
		ArrayList<Object> WECUids = new ArrayList<>(Arrays.asList(request.getParameter("WECUids").split(",")));
		// prepare hql to fetch the selected WECs with server setup
		String hqlGetWECs = "FROM WellExternalCatalog WHERE (isDeleted=0 OR isDeleted IS NULL) "
				+ "AND importExportServerPropertiesUid IN (SELECT serverPropertiesUid FROM ImportExportServerProperties "
				+ "WHERE (isDeleted=0 OR isDeleted IS NULL)) "
				+ "AND wellExternalCatalogUid IN (";
		// prepare params and append hqls with the uids we need to check for.
		ArrayList<String> params = new ArrayList<String>();
		String delimeter = "";
		for(int i=0; i<WECUids.size(); i++) {
			String param = "WEC"+i;
			params.add(param);
			hqlGetWECs += delimeter+":"+param;
			delimeter = ",";
		}
		hqlGetWECs += ") ORDER BY wellName";
		// get wecs that have a server setup, and fetch their respective ImportExportServerProperties
		List<WellExternalCatalog >wecs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hqlGetWECs, params, WECUids);
		if (wecs == null || wecs.size() <= 0) return; // skip if nothing
		for(WellExternalCatalog wec : wecs) {
			ImportExportServerProperties iesp = this.fetchServerProperties(wec.getImportExportServerPropertiesUid());
			this.forwardToImporter(iesp, wec);
		}
	}
	
	/**
	 * Called when the user uses the "Index External Wells" popup feature to index WECs on demand. 
	 * Grab the extra configurations and server uid from the request, and forward to the relevant classes based on the server (depot version).
	 * @param request
	 * @param commandBean
	 * @throws Exception
	 */
	public void indexExternalWells(HttpServletRequest request, CommandBean commandBean) throws Exception {
		String extraConfig = request.getParameter("extraConfig");
		String serverUid = request.getParameter("serverUid");
		ImportExportServerProperties iesp = this.fetchServerProperties(serverUid);
		switch (iesp.getDepotVersion()) {
		    case DepotConstants.WELLVIEW: //for Chevron WellView
		    	WellViewIndexer.getConfiguredInstance().indexOnDemand(extraConfig, iesp);
				break;
		    case DepotConstants.DLCWELLHEADER: //for OMV HIP
		    	break;
			default:
		}
	}
	
	/**
	 * Upon choosing this WEC record to import, we simply switch to the specified classes to start the import process, 
	 * based on the data_transfer_type of the WEC's ImportExportServerProperties.
	 * If you have a new data_transfer_type shortcode, define it as a new case and fire your next method in the import process.
	 * @param serverProperties
	 * @param commandBean 
	 * @param wecr
	 * @throws Exception
	 */
	public void forwardToImporter(ImportExportServerProperties serverProperties, WellExternalCatalog wec) throws Exception {
		// grab the EXT_WELL_CATALOG depot version to check
		switch (serverProperties.getDepotVersion()) {
		    case DepotConstants.WELLVIEW: //for Chevron WellView
		    	WellViewImporter.getConfiguredInstance().importWellSource(serverProperties, wec);
				break;
		    case DepotConstants.DLCWELLHEADER: //for OMV HIP
		    	EDMExternalWellSourceImporter wellSouceImporter = new EDMExternalWellSourceImporter();
		    	wellSouceImporter.importWellSource(serverProperties, wec);
		    	break;
			default:
		}
	}
	
	/**
	 * If "Import Well/Wellbore Only" is selected as an import option, we will forward import request to a non-job related class.
	 * It should only import the WW information this singular WEC catalogs through DEPOT, either directly from the WEC.sourceData or other sources.
	 * Basically the same as WellExternalCatalogDataNodeListener.forwardToImporter but different classes/methods used.
	 * @param serverProperties
	 * @param wec
	 * @throws Exception
	 */
	public void importWellWellbore(ImportExportServerProperties serverProperties, 
			WellExternalCatalog wec, BaseCommandBean commandBean) throws Exception {
		// grab the EXT_WELL_CATALOG depot version to check
		switch (serverProperties.getDepotVersion()) {
		 	case DepotConstants.WELLVIEW: //for Chevron WellView
		    	WellViewImporter.getConfiguredInstance().importWWOnly(serverProperties, wec, commandBean);
				break;
		 	case DepotConstants.DLCWELLHEADER: //for OMV HIP
		    	break;
			default:
		}
	}

	/**
	 * Throws a system message in the case that the depot_version is not set correctly for the server setup.
	 * We need to return a JSON object as we set that as the "outputType" in our wellExternalCatalog.js ImportActualButtonField.prototype.checkServerSetup
	 * @param request 
	 * @param response 
	 * @throws Exception 
	 */
	public void checkDepotVersion(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ImportExportServerProperties iesp = 
				(ImportExportServerProperties) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(
						ImportExportServerProperties.class, request.getParameter("serverUid"));
		JSONObject status = new JSONObject();
		// in any case the importExportServerPropertiesUid of the WEC is not found in ImportExportServerProperties
		if(iesp == null) {
			String msg = "Server setup issue! No server found for this ImportExportServerPropertiesUid: "
					+ request.getParameter("serverUid");
			status.put("success", false);
			status.put("msg", msg);
			response.getWriter().write(status.toString());
			response.getWriter().close();
			return;
		}
		
		try {
			// will throw NoSuchFieldException if the DepotConstants does not have the EXT_WELL_CATALOG depotVersion
			DepotConstants.class.getField(iesp.getDepotVersion().toUpperCase()); 
			status.put("success", true);
			status.put("isIdsAdmin", UserSession.getInstance(request).getCurrentUser().getIdsUser());
		} catch (NoSuchFieldException e) {
			// else we have to send an error system message and prevent further action
			String msg = "Server setup issue: DEPOT Version \""+iesp.getDepotVersion()+"\" for this server is not correct. "
					+ "\nPlease check for the correct \"Version\" has been set or has been configured properly.";
			status.put("success", false);
			status.put("msg", msg);
		} finally {
			response.getWriter().write(status.toString());
			response.getWriter().close();
		}
	}
	
	/**
	 * Fetch the ImportExportServerProperties you need via uid. Can be used by anyone that gets this WellExternalCatalogManager's instance.
	 * @param serverUid
	 * @return ImportExportServerProperties
	 * @throws Exception
	 */
	public ImportExportServerProperties fetchServerProperties(String serverUid) throws Exception {
		return (ImportExportServerProperties) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(
						ImportExportServerProperties.class, serverUid);
	}
	/**
	 * Fetch the WellExternalCatalog you need via uid. Can be used by anyone that gets this WellExternalCatalogManager's instance.
	 * @param wecUid
	 * @return WellExternalCatalog
	 * @throws Exception
	 */
	public WellExternalCatalog fetchWEC(String wecUid) throws Exception {
		return (WellExternalCatalog) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(
				WellExternalCatalog.class, wecUid);
	}
	/**
	 * Checks if the ImportExportServerProperties we fetched for their respective WellExternalCatalog is null.
	 * If so it will throw exceptions and set a error system message.
	 * @param iesp
	 * @param wec
	 * @param commandBean
	 * @throws Exception
	 */
	private void checkIesp(ImportExportServerProperties iesp, WellExternalCatalog wec, CommandBean commandBean) throws Exception {
		if (iesp != null) return;
		String errMsg = "No ImportExportServerProperties Server Setup found for this Well External Catalog record:\n "
				+ "Well: "+wec.getWellName()+"\n "
				+ "Wellbore: "+wec.getWellboreName();
		commandBean.getSystemMessage().addError(errMsg);
		throw new Exception(errMsg); 
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
	}
}
