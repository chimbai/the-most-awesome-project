package com.idsdatanet.d2.drillnet.bharun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.LookupBhaComponent;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class BhacomponentLookupHandler implements LookupHandler {	
	
	private String defaultUri = null;
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		List<Object[]> mapList = new ArrayList();
		
		if (this.defaultUri!=null) {
			Map<String, LookupItem> lookupList = LookupManager.getConfiguredInstance().getLookup(this.defaultUri, userSelection, new LookupCache());
		
			for (Map.Entry<String, LookupItem> lookupItem: lookupList.entrySet()) {
				mapList.add(new Object[] {lookupItem.getKey(), lookupItem.getValue()});
			}
		}

		String operationCode = userSelection.getOperationType();
		String sql = "from LookupBhaComponent where (isDeleted = false or isDeleted is null) and (operationCode=null or operationCode='' or operationCode=:operationCode) order by type";
		List<LookupBhaComponent> lookupBhaComponentList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationCode", operationCode);
		if (lookupBhaComponentList.size() > 0) {
			for (LookupBhaComponent lookupBhaComponent : lookupBhaComponentList) {
				if (lookupBhaComponent.getLookupBhaComponentUid() != null && lookupBhaComponent.getType() != null) {
					LookupItem lookupItem = new LookupItem(lookupBhaComponent.getLookupBhaComponentUid(), lookupBhaComponent.getType());
					lookupItem.setActive(lookupBhaComponent.getIsActive());
					// supply witsml lookup
					if (StringUtils.isNotBlank(lookupBhaComponent.getWitsmlLookupKey())) {
						lookupItem.addDepotValue("witsml", lookupBhaComponent.getWitsmlLookupKey());
					}
					// supply image key lookup
					if (StringUtils.isNotBlank(lookupBhaComponent.getImageKey()))
					{
						lookupItem.addDepotValue("schematic", lookupBhaComponent.getImageKey());
					}
					mapList.add(new Object[] {lookupBhaComponent.getLookupBhaComponentUid(), lookupItem});
				}
			}
		}
		
		//Sorting by BHA component
		Collections.sort(mapList, new BhaComponentComparator());
		for(Iterator i = mapList.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			LookupItem item = (LookupItem) obj_array[1];
			if (!result.containsKey(item.getKey())) {
				result.put(item.getKey(), item);
			}
		}
		
		return result;
	}

	public void setDefaultUri(String defaultUri) {
		this.defaultUri = defaultUri;
	}
	
	public String getDefaultUri() {
		return this.defaultUri;
	}

	private class BhaComponentComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				LookupItem in1 = (LookupItem) o1[1];
				LookupItem in2 = (LookupItem) o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(in1.getValue().toString().toLowerCase());
				String f2 = WellNameUtil.getPaddedStr(in2.getValue().toString().toLowerCase());
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
}
