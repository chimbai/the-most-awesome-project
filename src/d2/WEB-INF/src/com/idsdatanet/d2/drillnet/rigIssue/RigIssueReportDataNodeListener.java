package com.idsdatanet.d2.drillnet.rigIssue;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.RigIssue;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class RigIssueReportDataNodeListener extends EmptyDataNodeListener {

	@Override
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof RigIssue){
			RigIssue thisRigIssue = (RigIssue) object;
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			
			
			if (daily != null) {
				Date dayDate = daily.getDayDate();
				if (thisRigIssue.getDateRaised().compareTo(dayDate)<=0){
					node.getDynaAttr().put("isHide", false);
				}else node.getDynaAttr().put("isHide", true);
				
				Calendar thisCalendar = Calendar.getInstance();
	 			thisCalendar.setTime(dayDate);
	 			thisCalendar.add(Calendar.DATE, 1);

				String queryString = "SELECT ril.rigIssueLogUid, ril.comment, ril.issueStatus, ril.logDateTime " +
						"FROM RigIssueLog ril " +
						"WHERE (ril.isDeleted = false or ril.isDeleted is null) " +
						"and ril.logDateTime < :dayDate " +
						"and ril.rigIssueUid = :rigIssueUid " +
						"order by ril.logDateTime desc";
				String[] paramNames = {"dayDate", "rigIssueUid"};
				Object[] paramValues = {thisCalendar.getTime(), thisRigIssue.getRigIssueUid()};
				List<Object[]> rigIssueLogList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
				
				if(rigIssueLogList.size()>0){
					Object[] rigIssueLog = rigIssueLogList.get(0);
					String rigIssueLogUid = null;
					String comment = null;
					String issueStatus = null;
					Date logDateTime = null;
					
					if(rigIssueLog[0]!=null)
						rigIssueLogUid = rigIssueLog[0].toString();
					if(rigIssueLog[1]!=null)
						comment = rigIssueLog[1].toString();
					if(rigIssueLog[2]!=null)
						issueStatus = rigIssueLog[2].toString();
					if(rigIssueLog[3]!=null)
						logDateTime = (Date) rigIssueLog[3];
					
					if(issueStatus.equalsIgnoreCase("closed")){
						if(this.formatDate(logDateTime).equals(this.formatDate(dayDate))){
							node.getDynaAttr().put("rigIssueLogUid", rigIssueLogUid);
							node.getDynaAttr().put("comment", comment);
							node.getDynaAttr().put("issueStatus", issueStatus);
						}
					}else{
						node.getDynaAttr().put("rigIssueLogUid", rigIssueLogUid);
						node.getDynaAttr().put("comment", comment);
						node.getDynaAttr().put("issueStatus", issueStatus);
					}
				}
			}
		}
		
	}
	
	public Date formatDate(Date dayDate){
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTime(dayDate);
		thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
		thisCalendar.set(Calendar.MINUTE, 0);
		thisCalendar.set(Calendar.SECOND , 0);
		thisCalendar.set(Calendar.MILLISECOND , 0);
		dayDate = thisCalendar.getTime();
		return dayDate;
	}
}
