package com.idsdatanet.d2.drillnet.report;

import java.util.List;

public class fwrReportConfiguration {
	private List<String> beanForReportXml = null;
	private String title=null;
	private Boolean required=false;
	private String customProperty=null;
	private String label=null;
	private Boolean isNumeric= false;	
	
	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public void setTitle(String title){
		this.title=title;
	}
	
	public String getTitle()
	{
		return this.title;
	}
	
	public void setBeanForReportXml(List<String> value){
		this.beanForReportXml = value;
	}
	
	public List<String> getBeanForReportXml(){
		return this.beanForReportXml;
	}
	
	public void setCustomProperty(String customProperty){
		this.customProperty = customProperty;
	}
	
	public String getCustomProperty(){
		return this.customProperty;
	}
	
	public void setLabel(String label){
		this.label = label;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public void setIsNumeric(Boolean isNumeric){
		this.isNumeric = isNumeric;
	}
	
	public Boolean getIsNumeric(){
		return this.isNumeric;
	}
	
}
