package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ProductionPumpParam;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ProductionPumpLookupHandler implements LookupHandler{

	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		Map<String,LookupItem> lookupList = new LinkedHashMap<String,LookupItem>();
		
		List<Object> param_values = new ArrayList<Object>();
		List<String> param_names = new ArrayList<String>();
		
		param_names.add("wellUid"); 
		param_values.add(userSelection.getWellUid());
		
		List<ProductionPumpParam> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ProductionPumpParam where wellUid = :wellUid and (isDeleted = false or isDeleted is null) order by installDatetime", param_names, param_values); 
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
		Date today = new Date();
		if (daily!=null) today = daily.getDayDate();
		
		Calendar thisCalendar = Calendar.getInstance();
		
		for(ProductionPumpParam productionPumpParam:list)
		{
			thisCalendar.setTime(productionPumpParam.getInstallDatetime());
			thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
			thisCalendar.set(Calendar.MINUTE, 0);
			thisCalendar.set(Calendar.SECOND , 0);
			thisCalendar.set(Calendar.MILLISECOND , 0);
			Date installDate = thisCalendar.getTime();
			
			Date removeDate = new Date();
			if (productionPumpParam.getRemoveDatetime()!=null){
				thisCalendar.setTime(productionPumpParam.getRemoveDatetime());
				thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
				thisCalendar.set(Calendar.MINUTE, 0);
				thisCalendar.set(Calendar.SECOND , 0);
				thisCalendar.set(Calendar.MILLISECOND , 0);
				removeDate = thisCalendar.getTime();
			}
			Boolean allowAdd = false;
			if (productionPumpParam.getInstallDatetime()!=null && productionPumpParam.getRemoveDatetime()!=null){
				if ((today.compareTo(installDate)  >= 0) && (today.compareTo(removeDate)  <= 0)){
					allowAdd = true;
				}
			}else{
				if (today.compareTo(installDate)  >= 0){
					allowAdd = true;
				}
			}
			if (allowAdd){
				LookupItem li = new LookupItem(productionPumpParam.getProductionPumpParamUid() , productionPumpParam.getPumpNum());
				Map<String,String> filterProperties = new LinkedHashMap<String,String>();
				filterProperties.put("pumpLocation",productionPumpParam.getPumpLocation());
				li.setFilterProperties(filterProperties);
				lookupList.put(productionPumpParam.getProductionPumpParamUid(), li);
			}
		}
		return lookupList;
	}

}
