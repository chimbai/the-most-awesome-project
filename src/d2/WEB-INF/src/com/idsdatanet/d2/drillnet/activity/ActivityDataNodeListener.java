package com.idsdatanet.d2.drillnet.activity;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.CascadeLookupMeta;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ActivityLessonTicketLink;
import com.idsdatanet.d2.core.model.ActivityUnwantedEventLink;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.GoldboxAuditTransaction;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.UnwantedEvent;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.keyPerformanceIndicator.KeyPerformanceIndicatorUtils;
import com.idsdatanet.d2.drillnet.operationPlanMaster.DynamicLookaheadDVDPlan;
import com.idsdatanet.d2.drillnet.reportDaily.ReportDailyUtils;
import com.idsdatanet.d2.safenet.unwantedEvent.UnwantedEventFlexUtils;
import com.idsdatanet.depot.goldbox.field.util.ActRofCodeUtil;
import com.idsdatanet.depot.goldbox.field.util.ErrorCodeUtil;

public class ActivityDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {
	
	// these values are defined in lookup.xml
	private final static String E_FAIL = "equipment failure";
	private final static String KICK = "kick";
	private String classCode;
	private String companyName;
	//private Boolean useActivityDate;
	private Boolean saveAllUserCode = true;
	private Boolean saveNPTCategoriesToRootCauseCode = false;
	private boolean autoPopulateLastHoleSize = false;
	private static final String DYN_ATTR_ACTIVITY_DATE = "activityDate";
	private Boolean displayCompanyRCMessageOnSystemMessage = false;
	private List<String> taskCodeListForDataEntrySystemMessage = null;
	private List<String> additionalInternalClassCodeForNptEvent = null;
	private DynamicLookaheadDVDPlan dvdPlanHandler = null;
	
	
	public void setDvdPlanHandler(DynamicLookaheadDVDPlan dvdPlanHandler) {
		this.dvdPlanHandler = dvdPlanHandler;
	}

	public void setClassCode(String value){
		this.classCode = value;
	}

	public void setCompanyName(String value){
		this.companyName = value;
	}
	
	public void setAutoPopulateLastHoleSize(boolean value){
		this.autoPopulateLastHoleSize = value;
	}
	
	/*public void setUseActivityDate(Boolean value){
		this.useActivityDate = value;
	}*/
	
	public void setSaveAllUserCode(Boolean value) {
		this.saveAllUserCode = value;
	}

	public void setAdditionalInternalClassCodeForNptEvent(
			List<String> additionalInternalClassCodeForNptEvent) {
		this.additionalInternalClassCodeForNptEvent = additionalInternalClassCodeForNptEvent;
	}
	
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession session, CommandBeanTreeNode parent) throws Exception {
		if (("Activity".equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted()) || ("NextDayActivity".equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted()))
		{		
			KeyPerformanceIndicatorUtils.calc_npt_percent(session); 
		}

		if (("Activity".equalsIgnoreCase(className)) && status.isAnyNodeSavedOrDeleted())
		{
			boolean updateDvdSummary = false;

			if(status.getSavedNodes() != null && status.getSavedNodes().size() > 0){
				LOOP:
				for(CommandBeanTreeNode node:status.getSavedNodes()){
					if(node.getData() instanceof Activity){
						Activity thisActivity = (Activity)node.getData();
						
						// Update DVD Summary only if it is not Offline and Simop Activity
						boolean isOffline = false;
						boolean isSimop = false;
						
						if(thisActivity.getIsOffline() != null){
							isOffline = thisActivity.getIsOffline();
						}
						
						if(thisActivity.getIsSimop() != null){
							isSimop = thisActivity.getIsSimop();
						}
						
						if(!isOffline && !isSimop){
							updateDvdSummary = true;
							break LOOP;
						}	
					}
				}
			}
			
			if(status.getDeletedNodes() != null && status.getDeletedNodes().size() > 0){
				LOOP:
				for(CommandBeanTreeNode node:status.getDeletedNodes()){
					if(node.getData() instanceof Activity){
						Activity thisActivity = (Activity)node.getData();
						
						// Update DVD Summary only if it is not Offline and Simop Activity
						boolean isOffline = false;
						boolean isSimop = false;
						
						if(thisActivity.getIsOffline() != null){
							isOffline = thisActivity.getIsOffline();
						}
						
						if(thisActivity.getIsSimop() != null){
							isSimop = thisActivity.getIsSimop();
						}
						
						if(!isOffline && !isSimop){
							updateDvdSummary = true;
							break LOOP;
						}
					}
				}
			}
			
			if(this.dvdPlanHandler != null) {
				this.dvdPlanHandler.processDVDPlanPhaseActualData((ArrayList)parent.getCommandBean().getReferenceData().get("postProcessPlanReferences"));
			}
			
			if(updateDvdSummary){
				CommonUtil.getConfiguredInstance().deleteExistingDvdSummary("daily", session.getCurrentDailyUid());
				CommonUtil.getConfiguredInstance().createDvdSummary(session.getCurrentOperationUid(), session.getCurrentDailyUid());
			}
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		Calendar activityDate = Calendar.getInstance();
		
		if (this.getSaveNPTCategoriesToRootCauseCode())
		{
			this.setNPTCategoriesToRootCauseCode(node);
		}
		
		if(object instanceof Activity || object instanceof NextDayActivity) {
			Boolean setToBlank = false;
			
			if (PropertyUtils.getProperty(object, "object")!=null && "true".equals(PropertyUtils.getProperty(object, "object"))){
				setToBlank = true;
			}
				
			if(setToBlank){
				PropertyUtils.setProperty(object, "activityUid", null);
				PropertyUtils.setProperty(object, "object", null);
			}
			
			ActivityUtils.populateBharunNumber(node, new UserSelectionSnapshot(session));
			
			String activityUid = (String) PropertyUtils.getProperty(object, "activityUid");
			if ("".equals(activityUid)) {
				PropertyUtils.setProperty(object, "activityUid", null);
			}

			if(PropertyUtils.getProperty(object, "incidentNumber") == null)
			{
				PropertyUtils.setProperty(object, "incidentLevel", null);
			}
			
			Date thisActivityDate = null;
			if(node.getDynaAttr().get("activityDate") != null)
			{
				if(CommonDateParser.parse(node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE).toString()) != null)
				{
					thisActivityDate = CommonDateParser.parse(node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE).toString());
				}
				else
				{
					thisActivityDate = (Date) node.getDynaAttr().get(DYN_ATTR_ACTIVITY_DATE);
				}
			}else{
				
				if (session.getCurrentDaily()!=null)
					thisActivityDate = session.getCurrentDaily().getDayDate();
					
			}
			
			//GET ACTIVITY DATE TO ASSIGN DAILYUID
			if(thisActivityDate != null && (commandBean.getOperatingMode() != BaseCommandBean.OPERATING_MODE_DEPOT))
			{
				activityDate.setTime(thisActivityDate);
				
				String thisOperationUid = PropertyUtils.getProperty(object, "operationUid").toString();
				Daily thisDaily = ApplicationUtils.getConfiguredInstance().getDailyByOperationAndDate(thisOperationUid, activityDate.getTime());
				if(thisDaily != null)
				{
					PropertyUtils.setProperty(object, "dailyUid", thisDaily.getDailyUid());
				}
				else
				{
					//CREATE DAY
					//Daily newDaily = new Daily();
					//newDaily.setOperationUid(thisOperationUid);
					//newDaily.setDayDate(activityDate);
					//newDaily.setDayNumber("TMP");	//TEMPORARY DAY NUMBER
					//ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newDaily);
										
					Daily newDaily = CommonUtil.getConfiguredInstance().createNewDaily(activityDate.getTime(), thisOperationUid, session, request);
					PropertyUtils.setProperty(object, "dailyUid", newDaily.getDailyUid());
					
					//CREATE REPORT_DAILY
					ReportDaily newReportDaily = new ReportDaily();
					newReportDaily.setOperationUid(thisOperationUid);
					newReportDaily.setDailyUid(newDaily.getDailyUid().toString());
					newReportDaily.setReportDatetime(activityDate.getTime());
					newReportDaily.setReportType(CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(thisOperationUid));
					newReportDaily.setReportNumber("-");
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newReportDaily);
				}
				

				//STAMP STARTDATETIME & ENDDATETIME WITH CURRENT ACTIVITY DATE
				if(PropertyUtils.getProperty(object, "startDatetime") != null)
				{
					Calendar startDatetime = Calendar.getInstance();
					startDatetime.setTime((Date) PropertyUtils.getProperty(object, "startDatetime"));
					startDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
					startDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
					startDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
					PropertyUtils.setProperty(object, "startDatetime", startDatetime.getTime());
				}
													
				Calendar endDatetime = Calendar.getInstance();
				endDatetime.setTime((Date) PropertyUtils.getProperty(object, "endDatetime"));
				endDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
				endDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
				endDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
				PropertyUtils.setProperty(object, "endDatetime", endDatetime.getTime());
				
				D2ApplicationEvent.refresh();
				
			}
			
			
			List<Date> endDatetimeList = new ArrayList<Date>();
			
			// get parent node and loop through child to get each end datetime
			CommandBeanTreeNode parentNode = node.getParent();
			for (Iterator i = parentNode.getList().values().iterator(); i.hasNext();) {
				Map items = (Map) i.next();
				
				for (Iterator j = items.values().iterator(); j.hasNext();) {
					CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
					if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(object.getClass())) {
						Boolean isDeleted = (Boolean) PropertyUtils.getProperty(childNode.getData(), "isDeleted"); // shouldn't need this, temporary fix for Flex client
						if (isDeleted == null || !isDeleted.booleanValue()) {
							endDatetimeList.add((Date) PropertyUtils.getProperty(childNode.getData(), "endDatetime"));
						}
					}
				}
			}
			Collections.sort(endDatetimeList, new ActivityUtils.ReversedDateComparator());
			commandBean.getRoot().getDynaAttr().put("endDatetime", endDatetimeList);
			
			Date start = (Date) PropertyUtils.getProperty(object, "startDatetime");
			Date end = (Date) PropertyUtils.getProperty(object, "endDatetime");
			
			// if user didn't enter start time, find the first smaller than the current end time.
			if(start == null) {
				DateFormat df = new SimpleDateFormat("kkmmss");
				
				boolean found = false;
				for(Date endDatetime : endDatetimeList) {
					Calendar currentEndDatetime = Calendar.getInstance();
					currentEndDatetime.setTime(end);
					
					Calendar cachedEndDatetime = Calendar.getInstance();
					cachedEndDatetime.setTime(endDatetime);
					
					if(df.format(cachedEndDatetime.getTime()).compareTo(df.format(currentEndDatetime.getTime()))==-1) {
						
						if(thisActivityDate!=null){
							activityDate.setTime(thisActivityDate);
						}
						cachedEndDatetime.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
						cachedEndDatetime.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
						cachedEndDatetime.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
						
												
						PropertyUtils.setProperty(object, "startDatetime", cachedEndDatetime.getTime());
						found = true;
						break;
					}
				}
				// if no matches, set to 00:00
				if(!found) {
					Calendar zero = Calendar.getInstance();
					zero.clear();
					zero.set(Calendar.HOUR_OF_DAY, 0);
					zero.set(Calendar.MINUTE, 0);
					zero.set(Calendar.SECOND, 0);
					zero.set(Calendar.MILLISECOND, 0);
					
					
					if(thisActivityDate!=null){
						activityDate.setTime(thisActivityDate);
					}
					zero.set(Calendar.YEAR, activityDate.get(Calendar.YEAR));
					zero.set(Calendar.MONTH, activityDate.get(Calendar.MONTH));
					zero.set(Calendar.DAY_OF_MONTH, activityDate.get(Calendar.DAY_OF_MONTH));
					
										
					PropertyUtils.setProperty(object, "startDatetime", zero.getTime());
				}
				start = (Date) PropertyUtils.getProperty(object, "startDatetime");
			}
			
			// check if start time greater than end time.
			if(start != null && end != null){
				if(start.getTime() > end.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "startDatetime", "Start time can not be greater than end time.");
					return;
				}
				
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
				
				/*
				long endTime = end.getTime();
				if (endTime == 86399000) { // if it is 23:59:59
					endTime = 86400000; // make it 24:00:00 just for duration calculation
				}
				Long duration = (endTime - start.getTime()) / 1000;
				*/
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "activityDuration");
				
				thisConverter.setBaseValue(thisDuration);
				
				PropertyUtils.setProperty(object, "activityDuration", (thisConverter.getConvertedValue()));
				
				/** PORT FROM TNP: Tour Stamping Feature **/
				String rigTourUid = CommonUtil.getConfiguredInstance().tourAssignmentBasedOnTime(session, start, false);
				if (StringUtils.isNotBlank(rigTourUid)) {
					PropertyUtils.setProperty(object, "rigTourUid", rigTourUid);
				}
				/** PORT FROM TNP: Tour Stamping Feature **/
				
			}
			
			if (object instanceof NextDayActivity) {
				PropertyUtils.setProperty(object, "dayPlus", 1);
			}
			else {
				PropertyUtils.setProperty(object, "dayPlus", 0);
			}
			
			//fix case where if first the record is select trouble, then user put RC, then if change to program, RC is hide then when save RC value still in data
			//clear RC record and company record if user select program or unprogram
			String classCode = (String) PropertyUtils.getProperty(object, "classCode");
			String internalCode = this.getInternalCode(classCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
			PropertyUtils.setProperty(object, "internalClassCode", internalCode);
			if (StringUtils.equalsIgnoreCase("P", internalCode) || StringUtils.equalsIgnoreCase("U", internalCode))
			{
				PropertyUtils.setProperty(object, "rootCauseCode", null);
				PropertyUtils.setProperty(object, "equipmentCode", null);
				PropertyUtils.setProperty(object, "subEquipmentCode", null);
				PropertyUtils.setProperty(object, "lookupCompanyUid", null);
				PropertyUtils.setProperty(object, "incidentNumber", null);
				PropertyUtils.setProperty(object, "incidentLevel", null);
				
			}
			
			if(StringUtils.equalsIgnoreCase("P", internalCode)|| StringUtils.equalsIgnoreCase("TU", internalCode)|| StringUtils.equalsIgnoreCase("TP", internalCode))
			{
				if (!this.saveAllUserCode) PropertyUtils.setProperty(object, "userCode", null);
			}
			
			
			if ("TP".equalsIgnoreCase(internalCode) || "TU".equalsIgnoreCase(internalCode) || 
				(this.additionalInternalClassCodeForNptEvent!=null && this.additionalInternalClassCodeForNptEvent.contains(internalCode))) {
				//insert NPT Event record to unwantedEvent table
				String eventRef = (String) node.getDynaAttr().get("nptEventRef");
				if (StringUtils.isNotBlank(eventRef)) {
					List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from UnwantedEvent where (isDeleted = false or isDeleted is null) and type='nptEvent' and eventRef=:eventRef and operationUid=:operationUid", new String[]{"eventRef","operationUid"}, new Object[] {eventRef, session.getCurrentOperationUid()});
					if (list.size()>0) {
						UnwantedEvent ue = (UnwantedEvent) list.get(0);
						PropertyUtils.setProperty(object, "nptEventUid", ue.getUnwantedEventUid());
					} else {
						//create new unwanted event if not found
						UnwantedEvent ue = new UnwantedEvent();
						ue.setEventRef(eventRef);
						ue.setNameOfContractor((String) PropertyUtils.getProperty(object, "lookupCompanyUid"));
						ue.setEventCause((String) PropertyUtils.getProperty(object, "rootCauseCode"));
						ue.setType("nptEvent");
						ue.setEventSummary("");
						ue.setEventTitle("");
						
						//21239 autopopulate service field on activityNPT (UnwantedEvent.contractorType in commonlookup)
						List list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT a.code FROM LookupCompanyService a, CommonLookup b where a.code = b.shortCode and (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and a.lookupCompanyUid=:lookupCompanyUid and b.lookupTypeSelection = 'unwantedEvent.contractorType'", new String[]{"lookupCompanyUid"}, new Object[] {(String) PropertyUtils.getProperty(object, "lookupCompanyUid")});
						if(list2.size()>0) {
							ue.setContractorType((String) list2.get(0));
						}//end 21239
						
						if ("simop_activityController".equals(commandBean.getControllerBeanName()))
						{	
							ue.setIsSimop(true);
						}
						
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ue);
						PropertyUtils.setProperty(object, "nptEventUid", ue.getUnwantedEventUid());
					}
				}
			} else {
				PropertyUtils.setProperty(object, "nptEventUid", null);
			}
			
			if (!"P".equalsIgnoreCase(internalCode) && !"TP".equalsIgnoreCase(internalCode)) {
				PropertyUtils.setProperty(object, "planTaskReference",null);
				PropertyUtils.setProperty(object, "planDetailReference",null);
				PropertyUtils.setProperty(object, "isWithinScope",null);
			}
			
			if((StringUtils.isNotBlank((String) PropertyUtils.getProperty(object, "phaseCode")))){
				String phaseCode = (String) PropertyUtils.getProperty(object, "phaseCode");
				String regulatoryCode = this.getMPMCodePhase(phaseCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
				PropertyUtils.setProperty(object, "regulatoryPhaseCode", regulatoryCode);
				
				String internalPhaseCode  = this.getPhaseInternalCode(phaseCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
				PropertyUtils.setProperty(object, "internalPhaseCode", internalPhaseCode); 
				
				List<String> mpmCodes = null;
				if(("P".equalsIgnoreCase(internalCode) || "U".equalsIgnoreCase(internalCode))){
					if((StringUtils.isNotBlank((String) PropertyUtils.getProperty(object, "taskCode")))){
						String taskCode = (String) PropertyUtils.getProperty(object, "taskCode");
						mpmCodes = this.getMPMCodeTaskRoot(phaseCode,taskCode, session.getCurrentGroupUid(), session.getCurrentOperationType(), "task");
						if(mpmCodes.size() > 0){
							PropertyUtils.setProperty(object, "regulatoryCode", mpmCodes.get(0));
							PropertyUtils.setProperty(object, "regulatorySubcode", mpmCodes.get(1));
						}else{
							PropertyUtils.setProperty(object, "regulatoryCode", null);
							PropertyUtils.setProperty(object, "regulatorySubcode", null);
						}
					}
				}else if (("TP".equalsIgnoreCase(internalCode) || "TU".equalsIgnoreCase(internalCode))){
					if((StringUtils.isNotBlank((String) PropertyUtils.getProperty(object, "rootCauseCode")))){
						String rootCauseCode = (String) PropertyUtils.getProperty(object, "rootCauseCode");
						mpmCodes = this.getMPMCodeTaskRoot(phaseCode,rootCauseCode, session.getCurrentGroupUid(), session.getCurrentOperationType(), "root");
						if(mpmCodes.size() > 0){
							PropertyUtils.setProperty(object, "regulatoryCode", mpmCodes.get(0));
							PropertyUtils.setProperty(object, "regulatorySubcode", mpmCodes.get(1));
						}else{
							PropertyUtils.setProperty(object, "regulatoryCode", null);
							PropertyUtils.setProperty(object, "regulatorySubcode", null);
						}
					}
				}
			}else{
				PropertyUtils.setProperty(object, "regulatoryPhaseCode", "");
				PropertyUtils.setProperty(object, "regulatoryCode", "");
				PropertyUtils.setProperty(object, "regulatorySubcode", "");
			}
			
			if((StringUtils.isNotBlank((String) PropertyUtils.getProperty(object, "taskCode")))){
				String taskCode = (String) PropertyUtils.getProperty(object, "taskCode");
				String internalTaskCode  = this.getTaskInternalCode(taskCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
				PropertyUtils.setProperty(object, "internalTaskCode", internalTaskCode); 
			}
			
			if((StringUtils.isNotBlank((String) PropertyUtils.getProperty(object, "userCode")))){
				String userCode = (String) PropertyUtils.getProperty(object, "userCode");
				String internalUserCode  = this.getUserInternalCode(userCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
				PropertyUtils.setProperty(object, "internalUserCode", internalUserCode); 
			}
		
			if((StringUtils.isNotBlank((String) PropertyUtils.getProperty(object, "rootCauseCode")))){
				String rcCode = (String) PropertyUtils.getProperty(object, "rootCauseCode");
				String internalRCCode  = this.getRootCauseInternalCode(rcCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
				PropertyUtils.setProperty(object, "internalRootCauseCode", internalRCCode); 
			}
			
			if((StringUtils.isNotBlank((String) PropertyUtils.getProperty(object, "jobTypeCode")))){
				String jtCode = (String) PropertyUtils.getProperty(object, "jobTypeCode");
				String internalJTCode  = this.getRootCauseInternalCode(jtCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
				PropertyUtils.setProperty(object, "internalJobTypeCode", internalJTCode); 
			}
			
			//Set isOffline="YES" upon SAVE on activity simop screen
			if ("simop_activityController".equals(commandBean.getControllerBeanName()))
			{	
				PropertyUtils.setProperty(object, "isSimop", Boolean.parseBoolean("true"));
			}		
			
			//Set isOffline="YES" upon SAVE on activity offline screen
			if ("offlineActivityController".equals(commandBean.getControllerBeanName()))
			{	
				PropertyUtils.setProperty(object, "isOffline", Boolean.parseBoolean("true"));
			}

            // Set isOffline="YES" upon SAVE on activity simop screen
            if ("activityAuxDerrickController".equals(commandBean.getControllerBeanName())) {
                PropertyUtils.setProperty(object, "derrickType", "aux");
            }

			//Set company when classCode is match with the property in modules context
			if(StringUtils.isNotBlank(this.classCode) && StringUtils.isNotBlank(this.companyName))
			{
				if (StringUtils.equalsIgnoreCase(this.classCode, classCode))
				{
					PropertyUtils.setProperty(object, "lookupCompanyUid", this.companyName);
				}
			}
			
			//check if GWP set to when code is Trouble, user must select a company
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "actMustEnterCompanyIfTrouble"))){
				this.updateCompanyWarning(commandBean, node, status, session);
			}
			
			//Stamp activity code uid
			this.stampActivityCodeUid(commandBean, node);
			
		}
		
		if(object instanceof Activity) {
			if(this.dvdPlanHandler != null) {
				Activity rec = (Activity) object;
				if(rec.isPropertyModified("planReference")) {
					Activity oldData = (Activity) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Activity.class, rec.getActivityUid());
					this.registerPostProcessPlanReferences(commandBean, oldData.getPlanReference());
				}
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		if (object instanceof Activity || object instanceof NextDayActivity) {
			this.updateRootCauseCodeWarning(node,request, commandBean);
			
/*			Boolean showMsg = false;
			String submode1 = (String) PropertyUtils.getProperty(object, "submode1");
			if (submode1 != null){
				if (E_FAIL.equals(submode1) || KICK.equals(submode1)) {
					showMsg= true;
				}
			}
			String submode2 = (String) PropertyUtils.getProperty(object, "submode2");
			if (submode2 != null){
				if (E_FAIL.equals(submode2) || KICK.equals(submode2)) {
					showMsg= true;
				}
			}
			String submode3 = (String) PropertyUtils.getProperty(object, "submode3");
			if (submode3 != null){
				if (E_FAIL.equals(submode3) || KICK.equals(submode3)) {
					showMsg= true;
				}
			}*/
			
			String taskCode = (String) PropertyUtils.getProperty(object, "taskCode");
			
			if (this.taskCodeListForDataEntrySystemMessage !=null && this.taskCodeListForDataEntrySystemMessage.contains(taskCode)){
				commandBean.getSystemMessage().addInfo("If there is data related to air package / cavitation / rig gas water activity, please go to the relevant screen (Air Package Summary/Cavitation/Rig Gas-Water Rates) for data entry.", true, true);					
			}
	
			
/*			if (showMsg) commandBean.getSystemMessage().addInfo("Kick and/or Equipment Failure details must be added for the purpose of the NPD report" ,true, true); 
			
			if ((StringUtils.isNotBlank(submode1)) || (StringUtils.isNotBlank(submode2)) || (StringUtils.isNotBlank(submode3))) {
			//if ((submode1 != null) || (submode2 != null) || (submode3 != null)) {
				commandBean.getSystemMessage().addWarning("Changes on submode (from Kick and/or Equipment Failure to others) might affect Kick and/or Equipment Failure details shown in the respective screens", true, true); 
			}*/
				
			String thisClassCode = (String) PropertyUtils.getProperty(object, "classCode");
			String thisActivityUid = (String) PropertyUtils.getProperty(object, "activityUid");
			
			//AUTO CREATE UNWANTED EVENT FOR EACH NPT ACTIVITY
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CREATE_UNWANTEDEVENTS))){
				String strSql = "SELECT DISTINCT internalCode FROM LookupClassCode WHERE (isDeleted = FALSE OR isDeleted IS NULL) AND (operationCode = :thisOperationCode OR operationCode = '') " +
					"AND groupUid = :thisGroupUid AND shortCode = :thisShortCode";
				String[] paramsFields = {"thisOperationCode","thisGroupUid","thisShortCode"};
				Object[] paramsValues = {session.getCurrentOperationType(),session.getCurrentGroupUid(),thisClassCode};
				List ls = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				if(ls.size() > 0)
				{
					//USE IDS INTERNAL CLASS CODE TO CHECK IF THIS IS NPT ACTIVITY
					if(ls.get(0) != null && ("TP".equalsIgnoreCase(ls.get(0).toString()) || "TU".equalsIgnoreCase(ls.get(0).toString())))
					{
						//CHECK IF UE HAS BEEN CREATED FOR THIS NPT ACTIVITY
						String strSql2 = "FROM UnwantedEvent WHERE activityUid = :thisActivityUid AND type='UE' AND (isDeleted = FALSE OR isDeleted IS NULL)";
						List ls2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "thisActivityUid", thisActivityUid);
						if(ls2.size() < 1)
						{
							UnwantedEvent newUnwantedEvent = new UnwantedEvent();
							Date thisDate = new Date();
							DateFormat dateFormatter = new SimpleDateFormat("yyyyMMddhhmmssSSSSSSSS");
							newUnwantedEvent.setActivityUid(thisActivityUid);
							newUnwantedEvent.setEventRef("UE" + dateFormatter.format(thisDate));
							newUnwantedEvent.setEventSummary("");
							newUnwantedEvent.setEventTitle("");
							newUnwantedEvent.setEventNptCode("");
							newUnwantedEvent.setOperationUid(session.getCurrentOperationUid());
							newUnwantedEvent.setDailyUid(session.getCurrentDailyUid());
							newUnwantedEvent.setType("UE");
							
							if ("simop_activityController".equals(commandBean.getControllerBeanName()))
							{	
								newUnwantedEvent.setIsSimop(true);
							}

							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newUnwantedEvent);
							
							ActivityUnwantedEventLink newlink = new ActivityUnwantedEventLink();
							newlink.setUnwantedEventUid(newUnwantedEvent.getUnwantedEventUid());
							newlink.setActivityUid(thisActivityUid);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newlink);
							
							commandBean.getSystemMessage().addInfo("Unwanted events have been created automatically for the events that have been classified as trouble event.", true, true);
						}
					}
				}
				
			}
			
			if(autoPopulateLastHoleSize){
				String operationUid = (String) PropertyUtils.getProperty(object,"operationUid");
				String dailyUid = (String) PropertyUtils.getProperty(object,"dailyUid");
				String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :selectedDailyUid AND reportType<>'DGR'";
				String[] paramsFields = {"selectedOperationUid","selectedDailyUid"};
				Object[] paramsValues = {operationUid,dailyUid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				ReportDaily thisReport = null;
				String strSql2 = "FROM Activity WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :selectedDailyUid AND dayPlus = 0 AND (holeSize is not null OR holeSize != '') ORDER BY endDatetime DESC";
				String[] paramsFields2 = {"selectedOperationUid","selectedDailyUid"};
				Object[] paramsValues2 = {operationUid,dailyUid};
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
				if (!lstResult.isEmpty())
				{
					for(int i = 0; i < lstResult.size(); i++){
						thisReport = (ReportDaily) lstResult.get(i);
						if (!lstResult2.isEmpty())
						{
							Activity lastActivity = (Activity) lstResult2.get(0);
							thisReport.setLastHolesize(lastActivity.getHoleSize());
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
						}else{
							thisReport.setLastHolesize(null);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
						}
					}
				}
			}
		}
		if (object instanceof Activity)
		{
			// EFC Vessel Cost - 20100321-1223-ekhoo 
			Activity thisActivity = (Activity) object;
			CommonUtil.getConfiguredInstance().UpdateReportDailyEFC(thisActivity.getOperationUid(),thisActivity.getDailyUid());
		}
		
		UnwantedEventFlexUtils ueUtils = new UnwantedEventFlexUtils();
		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoRemoveUnwantedEventFromActivity"))) {
			if (ueUtils.containsOldEvents(session)) { //check old unwanted events
				if (ueUtils.unwantedEventCleanup(session)) { //do unwanted event clean up
					commandBean.getSystemMessage().addInfo("Unwanted events have been cleaned up for the events that have been removed or changed to non-trouble event.", true, true);
				}
			}
		}
		
		if (object instanceof Activity || object instanceof NextDayActivity) {
			String thisActivityUid = (String) PropertyUtils.getProperty(object, "activityUid");
			Double thisHoleSize = (Double) PropertyUtils.getProperty(object, "holeSize");
			
			String strSql = "select lessonTicketUid FROM ActivityLessonTicketLink WHERE (isDeleted = false or isDeleted is null) and activityUid =:activityUid";
			String[] paramsFields = {"activityUid"};
			Object[] paramsValues = {thisActivityUid};
			List<String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size() > 0) {
				for (String lessonTicketUid : lstResult){
					LessonTicket ll = (LessonTicket) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LessonTicket.class, lessonTicketUid);
						if (ll != null) {
							ll.setHoleSize(thisHoleSize);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(ll);
						}
				}
			}
		}
		
		commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		
		// empty method to be extended for client specific usage
		//this.clientSpecificProcessAfterDataNodeSaveOrUpdate(node);
	}
	/*
	protected void clientSpecificProcessAfterDataNodeSaveOrUpdate(CommandBeanTreeNode node) throws Exception{
		// do nothing
	}*/
	
	public void afterDataNodeProcessed(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, String action, int operationPerformed, boolean errorOccurred) throws Exception{
		Object object = node.getData();
		
		if (object instanceof Activity) {
			//calculate drilling ahead average rop - 20100111-0509-asim
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_CALC_DRILLING_AHEAD_AVG_ROP))) {
				Activity activity = (Activity) object;
				String dailyUid = activity.getDailyUid();
				ReportDailyUtils.calculateDrillingAheadAvgRop(node, session.getCurrentOperationUid(), dailyUid);				
			}
			
			if(this.dvdPlanHandler != null) {
				this.registerPostProcessPlanReferences(commandBean, ((Activity) object).getPlanReference());
			}
		}
	}
	
	private void updateRootCauseCodeWarning(CommandBeanTreeNode node, HttpServletRequest request, CommandBean commandBean) throws Exception {
		String classCode = null;
		String internalCode = null;
		String rootCauseCode = null;
		String rootcauseMessage="";
		if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN)
		{
			UserSession userSession=UserSession.getInstance(request);
			rootcauseMessage = GroupWidePreference.getValue(userSession.getCurrentGroupUid(),"messageAlertUserWhenRootCauseEmpty");
		
			if (rootcauseMessage==null) rootcauseMessage = "";
			if(node.getData() instanceof Activity){
				Activity activity = (Activity) node.getData();
				classCode = activity.getClassCode();
				internalCode = this.getInternalCode(classCode, userSession.getCurrentGroupUid(), userSession.getCurrentOperationType());
				rootCauseCode = activity.getRootCauseCode();
			}else if(node.getData() instanceof NextDayActivity){
				NextDayActivity activity = (NextDayActivity) node.getData();
				classCode = activity.getClassCode();
				internalCode = this.getInternalCode(classCode, userSession.getCurrentGroupUid(), userSession.getCurrentOperationType());
				rootCauseCode = activity.getRootCauseCode();
			}
			if(!this.displayCompanyRCMessageOnSystemMessage) {
				if (("TP".equalsIgnoreCase(internalCode) || "TU".equalsIgnoreCase(internalCode)) && (StringUtils.isBlank(rootCauseCode))) {
					//set the message here
						node.getNodeUserMessages().clearFieldMessages("rootCauseCode");
						node.getDynaAttr().put("rcMessage", rootcauseMessage);
						node.getNodeUserMessages().addFieldError("rootCauseCode", rootcauseMessage);
				}
				else
				{
					node.getNodeUserMessages().clearFieldMessages("rootCauseCode");
				}
			}
		}
	}
	
	private void updateUserCodeWarning(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception {
		String classCode = null;
		String internalCode = null;
		String userCode = null;
		if(node.getData() instanceof Activity){
			Activity activity = (Activity) node.getData();
			classCode = activity.getClassCode();
			internalCode = this.getInternalCode(classCode, userSelection.getGroupUid(), userSelection.getOperationType());
			userCode = activity.getUserCode();
		}else if(node.getData() instanceof NextDayActivity){
			NextDayActivity activity = (NextDayActivity) node.getData();
			classCode = activity.getClassCode();
			internalCode = this.getInternalCode(classCode, userSelection.getGroupUid(), userSelection.getOperationType());
			userCode = activity.getUserCode();
		}
		if ("U".equalsIgnoreCase(internalCode) && (StringUtils.isBlank(userCode))) {
			//set the message here
				node.getNodeUserMessages().clearFieldMessages("userCode");
				node.getDynaAttr().put("ucMessage", "Activity records that has been classified as UNPROGRAMMED must have the U/COS code documented.");
				node.getNodeUserMessages().addFieldError("userCode", "Activity records that has been classified as UNPROGRAMMED must have the U/COS code documented.");
			
		}else{
			node.getNodeUserMessages().clearFieldMessages("userCode");
		}
	}
	
	private void updateCompanyWarning(CommandBean commandBean, CommandBeanTreeNode node, DataNodeProcessStatus status, UserSession session) throws Exception {
		String classCode = null;
		String internalCode = null;
		String company = null;
		if(node.getData() instanceof Activity){
			Activity activity = (Activity) node.getData();
			classCode = activity.getClassCode();
			internalCode = this.getInternalCode(classCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
			company = activity.getLookupCompanyUid();
		}else if(node.getData() instanceof NextDayActivity){
			NextDayActivity activity = (NextDayActivity) node.getData();
			classCode = activity.getClassCode();
			internalCode = this.getInternalCode(classCode, session.getCurrentGroupUid(), session.getCurrentOperationType());
			company = activity.getLookupCompanyUid();
		}
		if (("TP".equalsIgnoreCase(internalCode) || "TU".equalsIgnoreCase(internalCode)) && (StringUtils.isBlank(company))) {
			//set the message here
			if(this.displayCompanyRCMessageOnSystemMessage) {
				commandBean.getSystemMessage().addError("Activity records that has been classified as TROUBLE must have the related company documented.");
			} else {
				node.getNodeUserMessages().clearFieldMessages("lookupCompanyUid");
				node.getDynaAttr().put("lookupCompanyUid", "Activity records that has been classified as TROUBLE must have the related company documented.");
				node.getNodeUserMessages().addFieldError("lookupCompanyUid", "Activity records that has been classified as TROUBLE must have the related company documented.");
			}
			status.setContinueProcess(false, true);
		}
	}
	
	private void setNPTCategoriesToRootCauseCode(CommandBeanTreeNode node) throws Exception
	{
		Object obj = node.getData();
		if (obj instanceof Activity || obj instanceof NextDayActivity)
		{
			String rootCauseCode = null;
			if (obj instanceof Activity)
			{
				Activity act = (Activity) obj;
				if (StringUtils.isNotBlank(act.getNptMainCategory()) && StringUtils.isNotBlank(act.getNptSubCategory()))
				{
					rootCauseCode = act.getNptMainCategory()+":"+act.getNptSubCategory();
					act.setRootCauseCode(rootCauseCode);
				}
			}
			if (obj instanceof NextDayActivity)
			{
				NextDayActivity nAct = (NextDayActivity) obj;
				if (StringUtils.isNotBlank(nAct.getNptMainCategory()) && StringUtils.isNotBlank(nAct.getNptSubCategory()))
				{
					rootCauseCode = nAct.getNptMainCategory()+":"+nAct.getNptSubCategory();
					nAct.setRootCauseCode(rootCauseCode);
				}
			}
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if(! node.getInfo().isTemplateNode() && (object instanceof Activity || object instanceof NextDayActivity)) {
			updateRootCauseCodeWarning(node, request, commandBean);
			updateUserCodeWarning(node, userSelection);
			if (object instanceof Activity || object instanceof NextDayActivity) {
				
				node.getDynaAttr().put("currentOperationType", userSelection.getOperationType());
				
				ActivityUtils.populateBharunNumber(node, userSelection);
				
				String activityUid = (String) PropertyUtils.getProperty(object, "activityUid");
				String[] paramsFields = {"activityUid"};
				Object[] paramsValues = {activityUid};
				
				String strSql = "SELECT count(*) as rec_count from ActivityLessonTicketLink WHERE (isDeleted = false or isDeleted is null) and activityUid= :activityUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				Integer llCount = 0;
				if (!lstResult.isEmpty())
				{
					Object thisResult = (Object) lstResult.get(0);
					
					if (thisResult != null) llCount = Integer.parseInt(thisResult.toString());
					node.getDynaAttr().put("lessonTicketRecordFound", llCount.toString() + " lesson ticket (s) available");
				}

				strSql = "SELECT lessonTicketUid from ActivityLessonTicketLink WHERE (isDeleted = false or isDeleted is null) and activityUid= :activityUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				String lessonTicketUid = "";
				if (!lstResult.isEmpty())
				{
					Object thisResult = (Object) lstResult.get(0);				
					if (thisResult != null) lessonTicketUid =  thisResult.toString();
					node.getDynaAttr().put("lessonTicketUid", lessonTicketUid);
				}
				
				Integer incidentNumber = 0;
				Boolean isDeleted = (Boolean) PropertyUtils.getProperty(object, "isDeleted");
				Boolean ofInterestValue = (Boolean) PropertyUtils.getProperty(object, "ofInterest");
				Integer incidentNum = (Integer) PropertyUtils.getProperty(object, "incidentNumber");
				Boolean ofInterest = false;
				if (isDeleted == null || !isDeleted) { 
					if (incidentNum!=null)		
						incidentNumber = incidentNum;		
					if (ofInterestValue!=null)		
						ofInterest = ofInterestValue;
					if (ofInterest || incidentNumber  > 0  ) {						
						node.getDynaAttr().put("showLessonLearnedLink", "1");	
					}
					else
						node.getDynaAttr().put("showLessonLearnedLink", "0");	
				}
				
				Integer numLL = 0;			
				if(StringUtils.isNotBlank(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_NUMBER_OF_LESSON_TICKET_ALLOWED)))
				{				
					numLL = Integer.parseInt(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_NUMBER_OF_LESSON_TICKET_ALLOWED).toString());
					if (numLL > llCount)
					{				
						node.getDynaAttr().put("showAddLessonTicketLink", "1");	
					}
					else
					{
						node.getDynaAttr().put("showAddLessonTicketLink", "0");	
					}
				}
				else
					node.getDynaAttr().put("showAddLessonTicketLink", "1");	
			}
			
			// calculate activity duration.
			//This is for HTML version only when fully flex can remove this sewction
			/* Double activityDuration = (Double) PropertyUtils.getProperty(object, "activityDuration");
			
			if(activityDuration != null) {
				Double duration = null;
				// next day's activity
				Integer dayPlus = (Integer) PropertyUtils.getProperty(object, "dayPlus");
				if(dayPlus != null && dayPlus == 1) {
					duration = (Double) commandBean.getRoot().getDynaAttr().get("nextDayActivityDuration");
					
					if(duration == null) {
						commandBean.getRoot().getDynaAttr().put("nextDayActivityDuration", activityDuration);
						
					} else {
						duration += activityDuration;
						commandBean.getRoot().getDynaAttr().put("nextDayActivityDuration", duration);
					}
				} else {
					
					duration = (Double) commandBean.getRoot().getDynaAttr().get("activityDuration");
					if(duration == null) {
						commandBean.getRoot().getDynaAttr().put("activityDuration", activityDuration);
					} else {
						duration += activityDuration;
						commandBean.getRoot().getDynaAttr().put("activityDuration", duration);
					}
				}
			}*/
			
			String strInputMode  = (String) commandBean.getRoot().getDynaAttr().get("inputMode");
			if(StringUtils.isBlank(strInputMode))
			{
				commandBean.getRoot().getDynaAttr().put("inputMode", "all");
			}
			
			if("all".equalsIgnoreCase(commandBean.getRoot().getDynaAttr().get("inputMode").toString()))
			{
				node.getDynaAttr().put("editable", true);
			}
			else
			{
				node.getDynaAttr().put("editable", false);
			}
								
			//GET ACTIVITY DAY DATE FROM DAILYUID
			/*if(useActivityDate != null && useActivityDate == true)
			{
				String strDailyUid = PropertyUtils.getProperty(object, "dailyUid").toString();
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(strDailyUid);
				if(daily != null)
				{
					Date activityDate = daily.getDayDate();
					Calendar thisCalendar = Calendar.getInstance();
					thisCalendar.setTime(activityDate);
														
					node.getDynaAttr().put("activityDate", thisCalendar.getTime());
				}
			}*/
			this.populateDynAttrActivityDate(node, userSelection, true);
			
			if (object instanceof Activity) {  
				Activity thisActivity = (Activity) node.getData();
				if (request != null) {
					String qat = request.getParameter("qat");
					if ("1".equals(qat)) {
						node.getDynaAttr().put("showQAT", "1");	
						
						String strSql = "FROM GoldboxAuditTransaction WHERE tableName='Activity' AND recordPrimaryKey=:key AND "
								+ "goldboxIndexUid IN (SELECT goldboxIndexUid FROM GoldboxIndex WHERE operationUid=:operationUid)";
						String[] paramsFields = {"key", "operationUid"};
						Object[] paramsValues = {thisActivity.getActivityUid(), thisActivity.getOperationUid()};
						List<GoldboxAuditTransaction> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
						if (lstResult.size() >0) {
							GoldboxAuditTransaction gat = (GoldboxAuditTransaction) lstResult.get(0);
							node.getDynaAttr().put("status", gat.getStatus());	
							node.getDynaAttr().put("errorMsg", ErrorCodeUtil.getErrorMsgLabel(gat, userSelection.getUserLanguage()));	
						}
							node.getDynaAttr().put("rofCode", ActRofCodeUtil.getRofCode(thisActivity));	
					}
				}else {
					node.getDynaAttr().put("showQAT", "0");	
				}
			}
		}
		
		this.loadLessonTicketList(node, userSelection);
		this.loadPlannedLessonTicketRecord(node, userSelection);
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		String newActivityRecordInputMode = null;
		if (request != null) {
			newActivityRecordInputMode = request.getParameter("newActivityRecordInputMode");
		}
		
		if (obj instanceof Activity) {
			if (meta.getTableClass() == Activity.class && "1".equals(newActivityRecordInputMode)) {
				commandBean.getRoot().addCustomNewChildNodeForInput(new Activity());
			}
		}
		
		afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	
	/**
	 * Carry forward last activity codes
	 */
	/*
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception	{
		try {
			Object thisObject = node.getData();
			if (thisObject instanceof Activity) {
				CommandBeanTreeNodeImpl lastChildNode = null;
				
				// get the last activity node
				for (Map<String, CommandBeanTreeNode> map: node.getParent().getList().values()) {
					for (Iterator i = map.values().iterator(); i.hasNext(); ) {
						CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) i.next();
						if (!childNode.isTemplateNode() && childNode.getData() instanceof Activity) {
							lastChildNode = childNode;
						}
					}
				}
				if (lastChildNode != null) {
					Object lastObject = lastChildNode.getData();
					if (lastObject != null) {
						Activity lastActivity = (Activity) lastObject;
						Activity thisActivity = (Activity) thisObject;
						thisActivity.setClassCode(lastActivity.getClassCode());
						thisActivity.setPhaseCode(lastActivity.getPhaseCode());
						thisActivity.setTaskCode(lastActivity.getTaskCode());
						thisActivity.setRootCauseCode(lastActivity.getRootCauseCode());
						
						// dynamic fields
						node.getDynaField().getValues().put("mainmode", lastChildNode.getDynaField().getValues().get("mainmode"));
						node.getDynaField().getValues().put("submode1", lastChildNode.getDynaField().getValues().get("submode1"));
						node.getDynaField().getValues().put("submode2", lastChildNode.getDynaField().getValues().get("submode2"));
						node.getDynaField().getValues().put("submode3", lastChildNode.getDynaField().getValues().get("submode3"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		UnwantedEventFlexUtils ueUtils = new UnwantedEventFlexUtils();
		if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoRemoveUnwantedEventFromActivity"))) {
			if (ueUtils.containsOldEvents(session)) { //check old unwanted events
				if (ueUtils.unwantedEventCleanup(session)) { //do unwanted event clean up
					commandBean.getSystemMessage().addInfo("Unwanted events have been cleaned up for the events that have been removed or changed to non-trouble event.", true, true);
				}
			}
		}
		
		Object object = node.getData();
		
		if(autoPopulateLastHoleSize){
			String operationUid = (String) PropertyUtils.getProperty(object,"operationUid");
			String dailyUid = (String) PropertyUtils.getProperty(object,"dailyUid");
			String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :selectedDailyUid AND reportType<>'DGR'";
			String[] paramsFields = {"selectedOperationUid","selectedDailyUid"};
			Object[] paramsValues = {operationUid,dailyUid};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			ReportDaily thisReport = null;
			String strSql2 = "FROM Activity WHERE (isDeleted = false or isDeleted is null) AND operationUid = :selectedOperationUid AND dailyUid = :selectedDailyUid AND dayPlus = 0 AND (holeSize is not null OR holeSize != '') ORDER BY endDatetime DESC";
			String[] paramsFields2 = {"selectedOperationUid","selectedDailyUid"};
			Object[] paramsValues2 = {operationUid,dailyUid};
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
			
			if (!lstResult.isEmpty())
			{
				for(int i = 0; i < lstResult.size(); i++){
					thisReport = (ReportDaily) lstResult.get(i);
					if (!lstResult2.isEmpty())
					{
						Activity lastActivity = (Activity) lstResult2.get(0);
						thisReport.setLastHolesize(lastActivity.getHoleSize());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
					}else{
						thisReport.setLastHolesize(null);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisReport);
					}
				}
			}
		}
		commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		
		if (meta.getTableClass().equals(Activity.class)) {
									
			//BY DEFAULT, LOAD DATA BY OPERATION
			String strSql = "FROM Activity WHERE operationUid = :operationUid AND groupUid = :groupUid AND (isDeleted IS NULL OR isDeleted = '') ORDER BY startDatetime";
			String[] paramNames =  {"operationUid", "groupUid"};
			Object[] paramValues = {userSelection.getOperationUid(), userSelection.getGroupUid()};
						
			List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);			
			List<Object> output_maps = new ArrayList<Object>();
			
			for(Object objResult: items){
				Activity activity = (Activity) objResult;
				output_maps.add(activity);
			}
						
			return output_maps;
		}
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		UserSession userSession = UserSession.getInstance(request);
		if ("1".equals(GroupWidePreference.getValue(userSession.getCurrentGroupUid(), "activityJobTypeDefaultGetOperationType"))) {
			Object object = node.getData();
			if (object instanceof Activity || object instanceof NextDayActivity) {
				node.getDynaAttr().put("editable", true);
				PropertyUtils.setProperty(object, "jobTypeCode", userSelection.getOperationType());
				this.populateDynAttrActivityDate(node, userSelection,false);
			}
		}
	}
	
	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{	//on paste as new record data with initial amount will be set to null
		
		Object object = targetNode.getData();
		if (object instanceof Activity || object instanceof NextDayActivity) {
			targetNode.getDynaAttr().put("editable", true);
			this.populateDynAttrActivityDate(targetNode, new UserSelectionSnapshot(userSession),false);
		}
	}
	
	//Ticket: 21230 (OP-8283)
	public void onDataNodePasteLastAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{	//on paste as new record data with initial amount will be set to null
		
		Object object = targetNode.getData();
		if (object instanceof Activity || object instanceof NextDayActivity) {
			Activity activity = (Activity) object;
			activity.setStartDatetime(null);
			activity.setEndDatetime(null);
			activity.setActivityDuration(null);
			activity.setPlanTaskReference(null);
			activity.setPlanDetailReference(null);
			activity.setRootCauseCode(null);
			activity.setLookupCompanyUid(null);
			activity.setNptEventUid(null);
			activity.setIsWithinScope(null);
			activity.setActivityDescription(null);
			activity.setMainmode(null);
			activity.setSubmode1(null);
		}
	}

	private void populateDynAttrActivityDate(CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Boolean getDailyFromNode) throws Exception
	{
		
		
		String dailyUid = userSelection.getDailyUid();
		Date activityDate = null;
		if (getDailyFromNode && PropertyUtils.getProperty(node.getData(), "dailyUid")!=null)
		{
			dailyUid = (String) PropertyUtils.getProperty(node.getData(), "dailyUid");
		}
		if (StringUtils.isNotBlank(dailyUid))
		{
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
			if (daily!=null && daily.getDayDate()!=null)
				activityDate = daily.getDayDate();
		}
		node.getDynaAttr().put(DYN_ATTR_ACTIVITY_DATE, activityDate);
	}
	
	public void setSaveNPTCategoriesToRootCauseCode(
			Boolean saveNPTCategoriesToRootCauseCode) {
		this.saveNPTCategoriesToRootCauseCode = saveNPTCategoriesToRootCauseCode;
	}

	public Boolean getSaveNPTCategoriesToRootCauseCode() {
		return saveNPTCategoriesToRootCauseCode;
	}
	
	private String getInternalCode(String classCode, String groupUid, String operationCode) throws Exception{
		
		String internalCode = "";
		
		String strSql = "SELECT internalCode FROM LookupClassCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisClassCode AND (operationCode = :thisOperationCode OR operationCode = '' OR operationCode IS NULL) AND groupUid = :thisGroupUid";
		String[] paramsFields = {"thisClassCode", "thisGroupUid", "thisOperationCode"};
		Object[] paramsValues = {classCode, groupUid, operationCode};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			Object internalCodeResult = (Object) lstResult.get(0);
			if (internalCodeResult != null) internalCode = internalCodeResult.toString();
		}
		if (StringUtils.isBlank(internalCode)) internalCode = classCode;
		
		return internalCode;
	}
	
	private String getPhaseInternalCode(String phaseCode, String groupUid, String operationCode) throws Exception{
		String internalCode = "";
		String strSql = "SELECT internalCode FROM LookupPhaseCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisPhaseCode AND (operationCode = :thisOperationCode OR operationCode = '' OR operationCode IS NULL) AND groupUid = :thisGroupUid";
		
		String[] paramsFields = {"thisPhaseCode", "thisOperationCode", "thisGroupUid"};
		Object[] paramsValues = {phaseCode,  operationCode, groupUid}; 

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (lstResult.size() > 0){
			Object internalCodeResult = (Object) lstResult.get(0);
			if (internalCodeResult != null) internalCode = internalCodeResult.toString();
		}
		if (StringUtils.isBlank(internalCode)) internalCode = phaseCode;
		
		return internalCode;
	}
	
	private String getTaskInternalCode(String taskCode, String groupUid, String operationCode) throws Exception{
		String internalCode = "";
		String strSql = "SELECT internalCode FROM LookupTaskCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisCode AND (operationCode = :thisOperationCode OR operationCode = '' OR operationCode IS NULL) AND groupUid = :thisGroupUid";
		
		String[] paramsFields = {"thisCode", "thisOperationCode", "thisGroupUid"};
		Object[] paramsValues = {taskCode,  operationCode, groupUid}; 

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (lstResult.size() > 0){
			Object internalCodeResult = (Object) lstResult.get(0);
			if (internalCodeResult != null) internalCode = internalCodeResult.toString();
		}
		if (StringUtils.isBlank(internalCode)) internalCode = taskCode;
		
		return internalCode;
	}
	
	private String getUserInternalCode(String userCode, String groupUid, String operationCode) throws Exception{
		String internalCode = "";
		String strSql = "SELECT internalCode FROM LookupUserCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisCode AND (operationCode = :thisOperationCode OR operationCode = '' OR operationCode IS NULL) AND groupUid = :thisGroupUid";
		
		String[] paramsFields = {"thisCode", "thisOperationCode", "thisGroupUid"};
		Object[] paramsValues = {userCode,  operationCode, groupUid}; 

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (lstResult.size() > 0){
			Object internalCodeResult = (Object) lstResult.get(0);
			if (internalCodeResult != null) internalCode = internalCodeResult.toString();
		}
		if (StringUtils.isBlank(internalCode)) internalCode = userCode;
		
		return internalCode;
	}
	
	private String getRootCauseInternalCode(String userCode, String groupUid, String operationCode) throws Exception{
		String internalCode = "";
		String strSql = "SELECT internalCode FROM LookupRootCauseCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisCode AND (operationCode = :thisOperationCode OR operationCode = '' OR operationCode IS NULL) AND groupUid = :thisGroupUid";
		
		String[] paramsFields = {"thisCode", "thisOperationCode", "thisGroupUid"};
		Object[] paramsValues = {userCode,  operationCode, groupUid}; 

		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (lstResult.size() > 0){
			Object internalCodeResult = (Object) lstResult.get(0);
			if (internalCodeResult != null) internalCode = internalCodeResult.toString();
		}
		if (StringUtils.isBlank(internalCode)) internalCode = userCode;
		
		return internalCode;
	}
	
	private String getMPMCodePhase(String phaseCode, String groupUid, String operationCode) throws Exception{
		
		String mpmCode = "";
		
		String strSql = "SELECT regulatoryCode FROM LookupPhaseCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisPhaseCode AND (operationCode = :thisOperationCode OR operationCode = '') AND groupUid = :thisGroupUid";
		String[] paramsFields = {"thisPhaseCode", "thisGroupUid", "thisOperationCode"};
		Object[] paramsValues = {phaseCode, groupUid, operationCode};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (lstResult.size() > 0){
			Object mpmCodeResult = (Object) lstResult.get(0);
			if (mpmCodeResult != null) mpmCode = mpmCodeResult.toString();
		}
		
		return mpmCode;
	}
	
	private List<String> getMPMCodeTaskRoot(String phaseCode, String shortCode, String groupUid, String operationCode,String codeType) throws Exception{
		List<String> mpmCode = new ArrayList<String>();
		String strSql = null;
		List lstResult = null;
		
		if(StringUtils.equalsIgnoreCase(codeType, "task")){
			strSql = "SELECT regulatoryCode,regulatorySubcode FROM LookupTaskCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisShortCode AND (operationCode = :thisOperationCode OR operationCode = '') AND (phaseShortCode = :thisPhaseShortCode) AND (isActive IS NULL OR isActive = 1 or isActive = '') AND groupUid = :thisGroupUid";
			String[] paramsFields = {"thisPhaseShortCode","thisShortCode", "thisGroupUid", "thisOperationCode"};
			Object[] paramsValues = {phaseCode, shortCode, groupUid, operationCode};
			if(strSql != null){
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			}
		}else if (StringUtils.equalsIgnoreCase(codeType, "root")){
			strSql = "SELECT regulatoryCode,regulatorySubcode FROM LookupRootCauseCode WHERE (isDeleted is null or isDeleted = false) AND shortCode = :thisShortCode AND (operationCode = :thisOperationCode OR operationCode = '') AND (isActive IS NULL OR isActive = 1 or isActive = '') AND groupUid = :thisGroupUid";
			String[] paramsFields = {"thisShortCode", "thisGroupUid", "thisOperationCode"};
			Object[] paramsValues = {shortCode, groupUid, operationCode};
			if(strSql != null){
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			}
		}
		
		if(lstResult != null){
			if (lstResult.size() > 0){
				Object[] mpmCodeResult = (Object[]) lstResult.get(0);
				if (mpmCodeResult != null) {
					if(mpmCodeResult[0] != null){
						mpmCode.add(mpmCodeResult[0].toString());
					}else{
						mpmCode.add("");
					}
					
					if(mpmCodeResult[1] != null){
						mpmCode.add(mpmCodeResult[1].toString());
					}else{
						mpmCode.add("");
					}
				}
			}
		}else{
			mpmCode.add("");
			mpmCode.add("");
		}

		return mpmCode;
	}

	public void setDisplayCompanyRCMessageOnSystemMessage(
			Boolean displayCOmpanyRCMessageOnSystemMessage) {
		this.displayCompanyRCMessageOnSystemMessage = displayCOmpanyRCMessageOnSystemMessage;
	}

	public Boolean getDisplayCompanyRCMessageOnSystemMessage() {
		return displayCompanyRCMessageOnSystemMessage;
	}

	public void setTaskCodeListForDataEntrySystemMessage(
			List<String> taskCodeListForDataEntrySystemMessage) {
		this.taskCodeListForDataEntrySystemMessage = taskCodeListForDataEntrySystemMessage;
	}
	
	public void beforeDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception
	{
		Object object=node.getData();
		//cascade delete the record of activity lesson ticket link and lesson ticket table when delete from activity screen
		if (object instanceof Activity || object instanceof NextDayActivity)
		{
			String activityUid = (String) PropertyUtils.getProperty(object, "activityUid");
			String lessonTicketUid = "";
			String[] paramsFields = {"activityUid"};
			Object[] paramsValues = {activityUid};
			
			String strSql = "FROM ActivityLessonTicketLink WHERE (isDeleted = false or isDeleted is null) and activityUid= :activityUid";
			List<ActivityLessonTicketLink> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty())
			{
				for(ActivityLessonTicketLink item : lstResult)
				{
					item.setIsDeleted(true);
					if(lessonTicketUid==""){
						lessonTicketUid = "'"+item.getLessonTicketUid()+"'";
					}else{
						lessonTicketUid += ",'"+item.getLessonTicketUid()+"'";
					}
					
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(item);
				}
			}
			
			if (lessonTicketUid!=""){
				String strSql2 = "FROM LessonTicket WHERE (isDeleted = false or isDeleted is null) and lessonTicketUid in ("+lessonTicketUid+")";
				List<LessonTicket> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql2);
				if (!lstResult2.isEmpty())
				{
					for(LessonTicket item2 : lstResult2)
					{
						item2.setIsDeleted(true);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(item2);
					}
				}
			}
		}
	}
	
	private void stampActivityCodeUid(CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
		
		Object obj = node.getData();
		if (obj instanceof Activity || obj instanceof NextDayActivity)
		{
			CascadeLookupMeta meta;
			String classCode = (String) PropertyUtils.getProperty(obj, "classCode");
			String phaseCode = (String) PropertyUtils.getProperty(obj, "phaseCode");
			String taskCode = (String) PropertyUtils.getProperty(obj, "taskCode");
			String rootCauseCode = (String) PropertyUtils.getProperty(obj, "rootCauseCode");
			String rootCauseSubCode = (String) PropertyUtils.getProperty(obj, "rootCauseSubCode");
			String userCode = (String) PropertyUtils.getProperty(obj, "userCode");
			String jobTypeCode = (String) PropertyUtils.getProperty(obj, "jobTypeCode");
			
			if (StringUtils.isNotBlank(classCode)) {
				PropertyUtils.setProperty(obj, "lookupClassCodeUid", this.getActivityCodesUid(commandBean, node, classCode, "classCode", "lookupClassCodeUid", null));
			} else {
				PropertyUtils.setProperty(obj, "lookupClassCodeUid", null);
			}
			
			if (StringUtils.isNotBlank(phaseCode)) {
				meta = commandBean.getCascadeLookupMeta("Activity", "phaseCode");
				if (meta != null && "jobTypeCode".equalsIgnoreCase(meta.getParentFieldName())) {
					PropertyUtils.setProperty(obj, "lookupPhaseCodeUid", this.getActivityCodesUid(commandBean, node, phaseCode, "phaseCode", "lookupPhaseCodeUid", jobTypeCode));
				} else {
					PropertyUtils.setProperty(obj, "lookupPhaseCodeUid", this.getActivityCodesUid(commandBean, node, phaseCode, "phaseCode", "lookupPhaseCodeUid", null));
				}
			} else {
				PropertyUtils.setProperty(obj, "lookupPhaseCodeUid", null);
			}
			
			if (StringUtils.isNotBlank(taskCode)) {
				meta = commandBean.getCascadeLookupMeta("Activity", "taskCode");
				if (meta != null && "phaseCode".equalsIgnoreCase(meta.getParentFieldName())) {
					PropertyUtils.setProperty(obj, "lookupTaskCodeUid", this.getActivityCodesUid(commandBean, node, taskCode, "taskCode", "lookupTaskCodeUid", phaseCode));
				} else if (meta != null && "jobTypeCode".equalsIgnoreCase(meta.getParentFieldName())) {
					PropertyUtils.setProperty(obj, "lookupTaskCodeUid", this.getActivityCodesUid(commandBean, node, taskCode, "taskCode", "lookupTaskCodeUid", jobTypeCode));
				} else {
					PropertyUtils.setProperty(obj, "lookupTaskCodeUid", this.getActivityCodesUid(commandBean, node, taskCode, "taskCode", "lookupTaskCodeUid", null));
				}
			} else {
				PropertyUtils.setProperty(obj, "lookupTaskCodeUid", null);
			}
			
			if (StringUtils.isNotBlank(rootCauseCode)) {
				meta = commandBean.getCascadeLookupMeta("Activity", "rootCauseCode");
				if (meta != null && "jobTypeCode".equalsIgnoreCase(meta.getParentFieldName())) {
					PropertyUtils.setProperty(obj, "lookupRootCauseCodeUid", this.getActivityCodesUid(commandBean, node, rootCauseCode, "rootCauseCode", "lookupRootCauseCodeUid", jobTypeCode));
				} else {
					PropertyUtils.setProperty(obj, "lookupRootCauseCodeUid", this.getActivityCodesUid(commandBean, node, rootCauseCode, "rootCauseCode", "lookupRootCauseCodeUid", null));
				}
			} else {
				PropertyUtils.setProperty(obj, "lookupRootCauseCodeUid", null);
			}
			
			if (StringUtils.isNotBlank(rootCauseCode)) {
				meta = commandBean.getCascadeLookupMeta("Activity", "rootCauseSubCode");
				if (meta != null && "jobTypeCode".equalsIgnoreCase(meta.getParentFieldName())) {
					PropertyUtils.setProperty(obj, "lookupRootCauseSubCodeUid", this.getActivityCodesUid(commandBean, node, rootCauseSubCode, "rootCauseSubCode", "lookupRootCauseSubCodeUid", jobTypeCode));
				} else {
					PropertyUtils.setProperty(obj, "lookupRootCauseSubCodeUid", this.getActivityCodesUid(commandBean, node, rootCauseSubCode, "rootCauseSubCode", "lookupRootCauseSubCodeUid", null));
				}
			} else {
				PropertyUtils.setProperty(obj, "lookupRootCauseSubCodeUid", null);
			}
			
			if (StringUtils.isNotBlank(userCode)) {
				meta = commandBean.getCascadeLookupMeta("Activity", "userCode");
				if (meta != null && "taskCode".equalsIgnoreCase(meta.getParentFieldName())) {
					PropertyUtils.setProperty(obj, "lookupUserCodeUid", this.getActivityCodesUid(commandBean, node, userCode, "userCode", "lookupUserCodeUid", taskCode));
				} else {
					PropertyUtils.setProperty(obj, "lookupUserCodeUid", this.getActivityCodesUid(commandBean, node, userCode, "userCode", "lookupUserCodeUid", null));
				}
			} else {
				PropertyUtils.setProperty(obj, "lookupUserCodeUid", null);
			}
			
			if (StringUtils.isNotBlank(jobTypeCode)) {
				PropertyUtils.setProperty(obj, "lookupJobTypeUid", this.getActivityCodesUid(commandBean, node, jobTypeCode, "jobTypeCode", "lookupJobTypeUid", null));
			} else {
				PropertyUtils.setProperty(obj, "lookupJobTypeUid", null);
			}
		}
	}
	
	private String getActivityCodesUid(CommandBean commandBean, CommandBeanTreeNode node, String activityCode, String fieldName, String lookupKey, String parentKey) throws Exception {
		
		String activityCodesLookupUid = null;
		Map<String, LookupItem> activityCodesLookup = commandBean.getLookupMap(node, "Activity", fieldName);
		if (activityCodesLookup != null && activityCodesLookup.size() > 0) {
			LookupItem lookupItem = activityCodesLookup.get(activityCode);
			if (lookupItem != null) {
				activityCodesLookupUid = lookupItem.getTableUid();
			}
		} else {
			if (parentKey != null) {
				activityCodesLookup = commandBean.getCascadeLookupMap(node, "Activity", fieldName, parentKey);
			} else {
				activityCodesLookup = commandBean.getCascadeLookupMap(node, "Activity", fieldName, null);
			}
			if (activityCodesLookup != null && activityCodesLookup.size() > 0) {
				LookupItem lookupItem = activityCodesLookup.get(activityCode);
				if (lookupItem != null) {
					activityCodesLookupUid = lookupItem.getTableUid();
				}
			}
		}
		return activityCodesLookupUid;
		
	}
	
	private void registerPostProcessPlanReferences(CommandBean commandBean, String planReference) throws Exception {
		ArrayList<String> postProcessPlanReferences = (ArrayList<String>) commandBean.getReferenceData().get("postProcessPlanReferences");
		if(postProcessPlanReferences == null) {
			postProcessPlanReferences = new ArrayList<String>();
			commandBean.getReferenceData().put("postProcessPlanReferences", postProcessPlanReferences);
		}
		
		if(!postProcessPlanReferences.contains(planReference)) {
			postProcessPlanReferences.add(planReference);
		}
	}
	
	private void loadPlannedLessonTicketRecord(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception {
		// TODO Auto-generated method stub
		
		Object object = node.getData();
		if (object instanceof Activity || object instanceof NextDayActivity)
		{
			String plannedLessonTicketInfo = "";
			String phaseCode = (String) PropertyUtils.getProperty(object, "phaseCode");
			String taskCode = (String) PropertyUtils.getProperty(object, "taskCode");
			String wellUid = userSelection.getWellUid();
			
			String[] paramsFields = {"phaseCode", "taskCode", "wellUid"};
			String[] paramsValues = {phaseCode, taskCode, wellUid};
			List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT lessonTicketUid, lessonTicketNumberPrefix, lessonTicketNumber FROM LessonTicket l " +
					"WHERE (l.isDeleted = FALSE OR l.isDeleted IS NULL) " +
					"AND l.phaseCode = :phaseCode " + 
					"AND l.taskCode = :taskCode " + 
					"AND l.wellUid = :wellUid " + 
					"AND l.lessonReportType = 'plan'", paramsFields, paramsValues);
			
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();
				
				String lessonTicketPrefix = "", lessonTicketNumber = "";
				
				if (StringUtils.isNotBlank((String) obj_array[1]))
					lessonTicketPrefix = obj_array[1].toString();
				
				if (StringUtils.isNotBlank((String) obj_array[2]))
					lessonTicketNumber = "PLL - " + lessonTicketPrefix + " - " + obj_array[2].toString();
								
				String lessonTicketUid = URLEncoder.encode(obj_array[0].toString(), "utf-8");
				String lessonTicketTitle = URLEncoder.encode(lessonTicketNumber, "utf-8");
				
				if (StringUtils.isBlank(plannedLessonTicketInfo)) {
					plannedLessonTicketInfo = "lessonticketuid=" + lessonTicketUid + "&lessontitle=" + lessonTicketTitle;
				} else {
					plannedLessonTicketInfo = plannedLessonTicketInfo + ",lessonticketuid=" + lessonTicketUid + "&lessontitle=" + lessonTicketTitle;
				}
			}
			node.getDynaAttr().put("plannedLessonTicketList", plannedLessonTicketInfo);
		}
	}
	
	private void loadLessonTicketList(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception {
		
		Object object = node.getData();
		if (object instanceof Activity || object instanceof NextDayActivity)
		{			
			String activityUid = (String) PropertyUtils.getProperty(object, "activityUid");
			
			if (!activityUid.isEmpty()) {
				String lessonTicketInfo = "";
				String[] paramsFields = {"activityUid"};
				String[] paramsValues = {activityUid};
				List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT l.lessonTicketUid, l.lessonTicketNumberPrefix, l.lessonTicketNumber " +
						"FROM ActivityLessonTicketLink a, LessonTicket l " +
						"WHERE (a.isDeleted = FALSE OR a.isDeleted IS NULL) " +
						"AND (l.isDeleted = FALSE OR l.isDeleted IS NULL) " +
						"AND l.lessonTicketUid = a.lessonTicketUid " +
						"AND a.activityUid = :activityUid", paramsFields, paramsValues);
				
				for(Iterator i = items.iterator(); i.hasNext(); ){
					Object[] obj_array = (Object[]) i.next();
					
					String lessonTicketPrefix = "", lessonTicketNumber = "";
					
					if (StringUtils.isNotBlank((String) obj_array[1]))
						lessonTicketPrefix = obj_array[1].toString();
					
					if (StringUtils.isNotBlank((String) obj_array[2]))
						lessonTicketNumber = lessonTicketPrefix + " - " + obj_array[2].toString();
					
					String lessonTicketUid = URLEncoder.encode(obj_array[0].toString(), "utf-8");
					String lessonTicketTitle = URLEncoder.encode(lessonTicketNumber, "utf-8");
					
					if (StringUtils.isBlank(lessonTicketInfo)) {
						lessonTicketInfo = "lessonticketuid=" + lessonTicketUid + "&lessontitle=" + lessonTicketTitle;
					} else {
						lessonTicketInfo = lessonTicketInfo + ",lessonticketuid=" + lessonTicketUid + "&lessontitle=" + lessonTicketTitle;
					}
				}
				node.getDynaAttr().put("lessonTicketList", lessonTicketInfo);
			}
		}
	}
	
}
