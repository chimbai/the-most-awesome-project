package com.idsdatanet.d2.drillnet.activity;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


public class PtVSNptDataGenerator implements ReportDataGenerator{
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		QueryProperties qp = new QueryProperties();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		
		String strSql = "SELECT SUM(activityDuration)/3600.00 AS duration, operationUid, wellUid " +
				"FROM Activity " +
				"WHERE isDeleted IS NULL " +
				"AND (dayPlus IS NULL OR dayPlus=0) " +
				"AND (isSimop <> 1 OR isSimop IS NULL) " +
				"AND (isOffline <> 1 OR isOffline IS NULL) " +
				"AND (internalClassCode <> 'TP' OR internalClassCode <> 'TU') " +
				"AND operationUid = :currentOperationUid ";
		String[] paramsFields = {"currentOperationUid"};
		Object[] paramsValues = {currentOperationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues,qp);
		
		if (lstResult.size() > 0){
			for(Object objResult: lstResult){
				Object[] objDuration = (Object[]) objResult;
				ReportDataNode thisReportNode = reportDataNode.addChild("ActivityDuration");
				String operationUid = "";
				String wellUid = "";
				Double activityDuration = 0.0;
				String type = "PT";
				
				if (objDuration[0] != null && StringUtils.isNotBlank(objDuration[0].toString())){ activityDuration = Double.parseDouble(objDuration[0].toString());}
				if (objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())){ operationUid = objDuration[1].toString();}
				if (objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())){ wellUid = objDuration[2].toString();}
				
				thisReportNode.addProperty("operationUid", operationUid.toString());
				thisReportNode.addProperty("wellUid", wellUid.toString());
				thisReportNode.addProperty("activityDuration", activityDuration.toString());
				thisReportNode.addProperty("type", type.toString());
			}
					
		}
		String strSql2 = "SELECT SUM(activityDuration)/3600.00 AS duration, operationUid, wellUid " +
				"FROM Activity " +
				"WHERE isDeleted IS NULL " +
				"AND (dayPlus IS NULL OR dayPlus=0) " +
				"AND (isSimop <> 1 OR isSimop IS NULL) " +
				"AND (isOffline <> 1 OR isOffline IS NULL) " +
				"AND (internalClassCode = 'TP' OR internalClassCode = 'TU') " +
				"AND operationUid = :currentOperationUid ";
		String[] paramsFields2 = {"currentOperationUid"};
		Object[] paramsValues2 = {currentOperationUid};
		List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
		
		if (lstResult2.size() > 0){
			for(Object objRes: lstResult2){
				Object[] objDur = (Object[]) objRes;
				ReportDataNode thisReportNode = reportDataNode.addChild("ActivityDuration");
				String operationUid = "";
				String wellUid = "";
				Double activityDuration = 0.0;
				String type = "NPT";
				
				if (objDur[0] != null && StringUtils.isNotBlank(objDur[0].toString())){ activityDuration = Double.parseDouble(objDur[0].toString());}
				if (objDur[1] != null && StringUtils.isNotBlank(objDur[1].toString())){ operationUid = objDur[1].toString();}
				if (objDur[2] != null && StringUtils.isNotBlank(objDur[2].toString())){ wellUid = objDur[2].toString();}
				
				thisReportNode.addProperty("operationUid", operationUid.toString());
				thisReportNode.addProperty("wellUid", wellUid.toString());
				thisReportNode.addProperty("activityDuration", activityDuration.toString());
				thisReportNode.addProperty("type", type.toString());
			}
					
		}
	}
}