package com.idsdatanet.d2.drillnet.lookupTaskCode;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class lookupTaskCodeMultiSelectGroupingHandler implements LookupHandler {
	
	private String orderBy;
	
	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	// get common look up type of grouping purpose from common lookup
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> results = new LinkedHashMap<String, LookupItem>();
		String sql = "SELECT shortCode, lookupLabel FROM CommonLookup WHERE lookupTypeSelection='GroupingPurpose' AND (isDeleted = false or isDeleted is null) ORDER BY :order";
		List<Object[]> groupingPurpose = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"order"}, new String[]{orderBy});
		if (groupingPurpose.size() > 0) {
			for (Object[] groupingPurposes : groupingPurpose) {
				if (groupingPurposes[0] != null && groupingPurposes[1] != null) {
					String lookupLabel = groupingPurposes[1].toString();
					
					LookupItem lookupItem = new LookupItem(groupingPurposes[0].toString(), lookupLabel);
					results.put(groupingPurposes[0].toString(), lookupItem);
				}
			}
		}
		return results;
	}
}