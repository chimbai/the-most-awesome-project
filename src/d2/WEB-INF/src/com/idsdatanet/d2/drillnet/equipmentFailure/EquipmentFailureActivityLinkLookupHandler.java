package com.idsdatanet.d2.drillnet.equipmentFailure;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class EquipmentFailureActivityLinkLookupHandler implements LookupHandler{
	
	private Boolean showAllEquipmentCode = false;
	
	public Boolean getShowAllEquipmentCode() {
		return showAllEquipmentCode;
	}


	public void setShowAllEquipmentCode(Boolean showAllEquipmentCode) {
		this.showAllEquipmentCode = showAllEquipmentCode;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		String dailyUid = userSelection.getDailyUid();
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		List<Activity> list = null;
		if (this.showAllEquipmentCode) {
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Activity WHERE (isDeleted=false or isDeleted is null) AND dailyUid=:dailyUid and (equipmentCode!='' and equipmentCode is not null) and (dayPlus IS NULL OR dayPlus=0) order by startDatetime, endDatetime", "dailyUid", dailyUid);
		} else {
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Activity WHERE (isDeleted=false or isDeleted is null) AND dailyUid=:dailyUid and (submode1='equipment failure' or submode2='equipment failure' or submode3='equipment failure') and (dayPlus IS NULL OR dayPlus=0) order by startDatetime, endDatetime", "dailyUid", dailyUid);
		}
		for (Activity rec : list) {
			String key = rec.getActivityUid();
			String endDateTime = df.format(rec.getEndDatetime());
			if (("23:59").equalsIgnoreCase(endDateTime)) {
				endDateTime = "24:00";
			}
			String value = df.format(rec.getStartDatetime()) + "-" + endDateTime + " -- " + rec.getActivityDescription();
			LookupItem item = new LookupItem(key, value);
			result.put(key, item);
		}
		return result;
	}
}
