package com.idsdatanet.d2.drillnet.corrosionRingProperties;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CorrosionRingProperties;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * All common utility related to CorrosionRingProperties.
 * @author Fu
 *
 */

public class CorrosionRingPropertiesUtil{
	/**
	 * Method used to auto - populate mud type label from mudMedium field of mudProperties - for EDC Usage
	 * @param corrosionRing
	 * @param userSelection
	 * @param qp
	 * @throws Exception Standard Error Throwing Exception
	 * @return String
	 */

	public static String populateMudType(CorrosionRingProperties corrosionRing, UserSelectionSnapshot userSelection, QueryProperties qp) throws Exception {		
		Date dateIn = null;
		Date dateOut = null;
		String wellboreUid = "";
		String cmbMudType = "";
		
		if (corrosionRing.getInstallDateTime()!= null){
			dateIn = corrosionRing.getInstallDateTime();
			dateOut = corrosionRing.getRemoveDateTime();
			wellboreUid = corrosionRing.getWellboreUid();
			
			//set corrosion's dateIn & dateOut to be only DATE value, initialise time to be 00:00:00
			Calendar thisCalendar = Calendar.getInstance();
			if (dateIn != null){					
			    thisCalendar.setTime(dateIn);
			    thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
			    thisCalendar.set(Calendar.MINUTE, 0);
			    thisCalendar.set(Calendar.SECOND , 0);
			    thisCalendar.set(Calendar.MILLISECOND , 0);
			    dateIn = thisCalendar.getTime();
			}
						    
		    if (dateOut != null){
		    	thisCalendar.setTime(dateOut);
			    thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
			    thisCalendar.set(Calendar.MINUTE, 0);
			    thisCalendar.set(Calendar.SECOND , 0);
			    thisCalendar.set(Calendar.MILLISECOND , 0);
			    dateOut = thisCalendar.getTime();
			}			        
		   	
		    if (dateIn != null && dateOut == null){			    	
		    	//Query mud type which fall in range of dateIn until the last day with Mud Properties for specific wellboreUid
				String[] paramsFields = {"dateIn", "wellboreUid"};
				Object[] paramsValues = {dateIn, wellboreUid};
				
				String strSql = "select DISTINCT m.mudMedium from MudProperties m, Daily d " +
						"where (m.isDeleted is null or m.isDeleted = false) " +
						"and (d.isDeleted is null or d.isDeleted = false) " +
						"and (m.dailyUid = d.dailyUid) " +
						"and (m.mudMedium is not null and m.mudMedium != '') " +
						"and m.wellboreUid = :wellboreUid " +
						"and d.dayDate >= :dateIn order by d.dayDate";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues,qp);
				if (lstResult.size() > 0) {
					for (Object mudType : lstResult) {
						if (mudType != null) {
							String mudTypeLabel = getMudMediumLabel(mudType.toString(), userSelection);
							if (StringUtils.isEmpty(cmbMudType)){
								cmbMudType = mudTypeLabel;
							}else{
								cmbMudType = cmbMudType + " , " + mudTypeLabel;
							}
						}
					}
				}
		    }else if (dateIn != null && dateOut != null){
		    	//Query mud type which fall in range of dateIn & dateOut
				String[] paramsFields = {"dateIn", "dateOut", "wellboreUid"};
				Object[] paramsValues = {dateIn, dateOut, wellboreUid};
				
				String strSql = "select DISTINCT m.mudMedium from MudProperties m, Daily d " +
						"where (m.isDeleted is null or m.isDeleted = false) " +
						"and (d.isDeleted is null or d.isDeleted = false) " +
						"and (m.dailyUid = d.dailyUid) " +
						"and (m.mudMedium is not null and m.mudMedium != '') " +
						"and m.wellboreUid = :wellboreUid " +
						"and d.dayDate >= :dateIn and d.dayDate <= :dateOut order by d.dayDate";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues,qp);
				if (lstResult.size() > 0) {
					for (Object mudType : lstResult) {
						if (mudType != null) {
							String mudTypeLabel = getMudMediumLabel(mudType.toString(), userSelection);
							if (StringUtils.isEmpty(cmbMudType)){
								cmbMudType = mudTypeLabel;
							}else{
								cmbMudType = cmbMudType + " , " + mudTypeLabel;
							}
						}
					}
				}
		    }else{
		    	// assign empty string if both conditions are not matched
		    	cmbMudType = "";
		    }		   
		}
		return cmbMudType;
	}
	
	/**
	 * Method used for retrieving mudProperties's mud medium xml lookup item value - for EDC Usage
	 * @param mudMediumCode
	 * @param userSelection 
	 * @throws Exception Standard Error Throwing Exception
	 * @return String
	 */
	public static String getMudMediumLabel(String mudMediumCode, UserSelectionSnapshot userSelection) throws Exception {		
		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://WaterBasedMudType?key=code&value=label", userSelection, null);
		if (lookupMap != null) {
			LookupItem lookupItem = lookupMap.get(mudMediumCode);
			if (lookupItem != null) {
				return (String) lookupItem.getValue();
			}
		}		
		return mudMediumCode;
	}
		
	/**
	 * Method used for calculating weight loss based on initial weight & final weight - for EDC Usage
	 * @param initialWeight
	 * @param finalWeight
	 * @param thisConverter
	 * @throws Exception Standard Error Throwing Exception
	 * @return void
	 */
	public static double calcWeightLoss(Double initialWeight, Double finalWeight, CustomFieldUom thisConverter) throws Exception {		
		Double weightLoss = 0.00;
		
		if (initialWeight!=null && finalWeight!=null){
			thisConverter.setReferenceMappingField(CorrosionRingProperties.class, "initialWeight");
			thisConverter.setBaseValueFromUserValue(initialWeight);
			initialWeight=thisConverter.getBasevalue();
			
			thisConverter.setReferenceMappingField(CorrosionRingProperties.class, "finalWeight");
			thisConverter.setBaseValueFromUserValue(finalWeight);
			finalWeight=thisConverter.getBasevalue();
			
			//calculation formula
			weightLoss = initialWeight - finalWeight;					
		}
		return weightLoss;
	}
	
	/**
	 * Method used for calculating corrosion rate based on weight loss, K Factor & Exposure Hrs - for EDC Usage
	 * @param exposureDuration
	 * @param kFactor
	 * @param weightLoss
	 * @param thisConverter 
	 * @throws Exception Standard Error Throwing Exception
	 * @return double
	 */
	public static double calcCorrosionRate(Double exposureDuration, Double kFactor, Double weightLoss, CustomFieldUom thisConverter) throws Exception {
		Double corrosionRate = 0.00;		
		
		if (exposureDuration != null){
			thisConverter.setReferenceMappingField(CorrosionRingProperties.class, "exposureDuration");
			thisConverter.setBaseValueFromUserValue(exposureDuration);
			exposureDuration=thisConverter.getBasevalue();
			exposureDuration=exposureDuration / 3600; // converted to hours(hrs) for calculation
		}else{
			exposureDuration = 0.00;
		}
		
		if (kFactor != null){
			thisConverter.setReferenceMappingField(CorrosionRingProperties.class, "kFactor");
			thisConverter.setBaseValueFromUserValue(kFactor);
			kFactor=thisConverter.getBasevalue();					
		}else{
			kFactor = 0.00;
		}
		
		if (weightLoss != null){
			thisConverter.setReferenceMappingField(CorrosionRingProperties.class, "weightLoss");
			thisConverter.setBaseValueFromUserValue(weightLoss);
			weightLoss=thisConverter.getBasevalue();	
			weightLoss = weightLoss / 0.0010; // converted to gram(g) for calculation
		}else{
			weightLoss = 0.00;
		}
		
		corrosionRate = (weightLoss * kFactor)/ exposureDuration;
		corrosionRate = corrosionRate * 1.547181E-07; // converted to IDS base value - KilogramPerSquareMeterSecond after result calculation
		
		return corrosionRate;		 
	}
}
