package com.idsdatanet.d2.drillnet.rigstock;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.LookupRigStock;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class FluidStockLookupHandler implements LookupHandler {

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		String mudVendorCompanyUid = null;
		String wellRegion = null;
		String strCondition = "";
		
		//get mud vendor from report daily screen
		String sql = "select mudVendorCompanyUid from ReportDaily where (isDeleted = false or isDeleted is null) and dailyUid=:dailyUid and reportType='DDR'";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "dailyUid", userSelection.getDailyUid());
		if (lstResult.size() > 0) {
			Object a = (Object) lstResult.get(0);
			if (a != null) mudVendorCompanyUid = a.toString();
		}
		
		// get region from well table
		Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(userSelection.getWellUid());
		if (well != null) {
			wellRegion = well.getRegion();
		}
		
		if (StringUtils.isNotBlank(mudVendorCompanyUid)) {
			strCondition = "mudVendorCompanyUid = '" + mudVendorCompanyUid + "'";
		}else {
			strCondition = "mudVendorCompanyUid is null";
		}
		
		if (StringUtils.isNotBlank(wellRegion)) {
			strCondition += " and region = '" + wellRegion + "'";
		}else {
			strCondition += " and region is null";
		}

		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();

		String strSql = "select stockUid from LookupProductList where (isDeleted = false or isDeleted is null) and " + strCondition + " order by sequence";
		List lstResults = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
		if (lstResults.size() > 0) {
			for (Object obj : lstResults) {
				if (obj != null) {

					LookupRigStock lookupRigStock = (LookupRigStock) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupRigStock.class, obj.toString());
					
					String stockBrand = "";
					String storedUnit = "";
					
					if (lookupRigStock !=null){
						stockBrand = lookupRigStock.getStockBrand();
						storedUnit = lookupRigStock.getStoredUnit();
					}
					String lookupLabel = stockBrand + " (" +  storedUnit + ")";
					result.put(obj.toString(), new LookupItem(obj.toString(), lookupLabel));
				}
			}
		}
			
		
				
		return result;
	}
}
