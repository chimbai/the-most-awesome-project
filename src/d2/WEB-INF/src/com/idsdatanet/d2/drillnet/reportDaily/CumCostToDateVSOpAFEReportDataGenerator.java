package com.idsdatanet.d2.drillnet.reportDaily;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @twlau
 * This class help to mine data for latest 7 days data
 * for 24 hr summary on report daily screen
 * 
 */

public class CumCostToDateVSOpAFEReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		CustomFieldUom costConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "daycost");
		
		Double afe = 0.0;
		Double secondaryAfe = 0.0;
		if(StringUtils.isNotBlank(currentOperationUid) && currentOperationUid != null){
			Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(currentOperationUid);
			if(operation.getAfe() != null){
				costConverter.setBaseValue(operation.getAfe());
				afe = costConverter.getConvertedValue();
			}
			
			if(operation.getSecondaryafeAmt() != null){
				costConverter.setBaseValue(operation.getSecondaryafeAmt());
				secondaryAfe = costConverter.getConvertedValue();
			}
		}
		
		Double authorizedCost = 0.0;
		authorizedCost = afe + secondaryAfe;
		
		String dailyReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(userContext.getUserSelection().getOperationUid());
		Double cumCostToDate = CommonUtil.getConfiguredInstance().calculateCummulativeCost(daily.getDailyUid(), dailyReportType);	
		if(cumCostToDate != null && cumCostToDate > 0.0){
			costConverter.setBaseValue(cumCostToDate);
			cumCostToDate = costConverter.getConvertedValue();
			
			if(authorizedCost > 0.0){
				ReportDataNode thisReportNode = reportDataNode.addChild("node");
				thisReportNode.addProperty("series", "cumCostToDate");
				thisReportNode.addProperty("amount", cumCostToDate.toString());
				thisReportNode.addProperty("authorizedCost", authorizedCost.toString());
				
				if(cumCostToDate < authorizedCost){
					Double balance = authorizedCost - cumCostToDate;
					thisReportNode = reportDataNode.addChild("node");
					thisReportNode.addProperty("series", "balance");
					thisReportNode.addProperty("amount", balance.toString());
					thisReportNode.addProperty("authorizedCost", authorizedCost.toString());
				}
			}
		}
		else{
			if(authorizedCost > 0.0){
				ReportDataNode thisReportNode = reportDataNode.addChild("node");
				thisReportNode.addProperty("series", "authorizedCost");
				thisReportNode.addProperty("amount", authorizedCost.toString());
				thisReportNode.addProperty("authorizedCost", authorizedCost.toString());
			}
		}
		
		
	}
}
