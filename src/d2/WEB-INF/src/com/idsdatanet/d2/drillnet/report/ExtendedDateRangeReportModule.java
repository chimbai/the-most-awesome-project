package com.idsdatanet.d2.drillnet.report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.JobContext;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.report.MultipleUserSelectionSnapshot;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class ExtendedDateRangeReportModule extends DefaultReportModule {
	
	public final String FIELD_IS_DISTINCT_REPORT = "isDistinctReport";
	public final String FIELD_START_DATE = "startDate";
	public final String FIELD_END_DATE = "endDate";
	public final String FIELD_LATEST ="isLatest";
	public final String FIELD_OPERATION="operations";
	
	public static String CUSTOM_PROPERTY_DATE_STRING = "CP_DATE_STRING";
	
	private Integer latestDays = 7;
	
	private OperationLoadHandler operationLoadHandler;
	
	public void init(CommandBean commandBean) {
		super.init(commandBean);
	}
	
	public Object parseRequestParam(HttpServletRequest request, String field, Class type) {
		String value = request.getParameter(field);
		if (type == Boolean.class) {
			if (StringUtils.isBlank(value)) {
				return false;
			}
			if (value.equalsIgnoreCase("true")||value.equalsIgnoreCase("1")) {
				return true;
			}
			return false;
		}
		if (type == Date.class) {
			if (StringUtils.isBlank(value)) {
				return null;
			}
			return CommonDateParser.parse(value);
		}
		if (type == String.class){
			return value;
		}
		return null;
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
		if ("flexDateRangeReportLoadOperationList".equals(invocationKey)) {
			Date startDate = null;
			Date endDate = null;
			
			Boolean isLatest = (Boolean) this.parseRequestParam(request, FIELD_LATEST, Boolean.class);
			if (isLatest) {
				endDate = new Date();
				startDate  = DateUtils.addDays(endDate, - (this.getLatestDays() - 1));
			} else {
				startDate = (Date) this.parseRequestParam(request, FIELD_START_DATE, Date.class);
				endDate = (Date) this.parseRequestParam(request, FIELD_END_DATE, Date.class);
				if (endDate == null) {
					endDate = startDate;
				}
			}
			
			if (startDate != null && endDate != null) {
				List<String> list = null;
				
				if (this.getOperationLoadHandler() != null) {
					list = this.getOperationLoadHandler().getOperationList(startDate, endDate);
				} else {
					list = this.getOperationFromDateRange(startDate, endDate);
				}
				
				SimpleXmlWriter writer = new SimpleXmlWriter(response);
				writer.startElement("root");
				
				for (String operation : list) {
					String operationUid = String.valueOf(operation);
					if (ApplicationUtils.getConfiguredInstance().getCachedAllOperations().containsKey(operationUid)) {
						writer.startElement("op");
						writer.addElement("code", operationUid);
						String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(UserSession.getInstance(request).getCurrentGroupUid(), operationUid);
						writer.addElement("label", operationName);
						writer.endElement();
					}
				}
				writer.endElement();
				writer.close();
			}
		} else {
			super.onCustomFilterInvoked(request, response, commandBean, invocationKey);
		}
	}
	
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		commandBean.getSystemMessage().clear();
		
		Boolean isLatest = "1".equals(commandBean.getRoot().getDynaAttr().get(FIELD_LATEST))||"true".equals(commandBean.getRoot().getDynaAttr().get(FIELD_LATEST));
		Boolean isDistinct = "1".equals(commandBean.getRoot().getDynaAttr().get(FIELD_IS_DISTINCT_REPORT))||"true".equals(commandBean.getRoot().getDynaAttr().get(FIELD_IS_DISTINCT_REPORT));

		List<String> operationList = this.getSelectedOperation(commandBean);
		
		if (operationList == null || (operationList != null && operationList.size() == 0)) {
			throw new Exception("No data fit the selected criteria therefore unable to generate report.");
		}
		
		Date startDate = null;
		Date endDate = null;
		
		if (isLatest) {
			endDate = new Date();
			startDate  = DateUtils.addDays(endDate, - (this.getLatestDays() - 1));
		} else {
			Object obj=commandBean.getRoot().getDynaAttr().get(FIELD_START_DATE);
			if (obj != null) {
				startDate = CommonDateParser.parse(obj.toString());
			}
			obj=commandBean.getRoot().getDynaAttr().get(FIELD_END_DATE);
			if (obj != null) {
				endDate = CommonDateParser.parse(obj.toString());
			}
			if (endDate == null) {
				endDate=startDate;
			}
		}
		
		if (startDate != null && endDate != null) {
			if (endDate.after(startDate) || endDate.equals(startDate)) {
				if (isDistinct) {
					return super.submitReportJob(this.createMultipleOperationUserSelection(session.getUserName(), operationList, startDate, endDate,isDistinct),this.getParametersForReportSubmission(session, request, commandBean));
				}
				return super.submitReportJob(this.createUserSelectionList(session.getUserName(), operationList, startDate, endDate,isDistinct),this.getParametersForReportSubmission(session, request, commandBean));
			} else if (endDate.before(startDate)) {
				commandBean.getSystemMessage().addError("End Date must be After Start Date");
			}
		} else {
			commandBean.getSystemMessage().addError("Please Enter Start Date");
		}
		return null;
	}
	
	private UserSelectionSnapshot createUserSelectionList(String login, List<String> operations, Date start, Date end, Boolean isDistinctReport) throws Exception {
		MultipleUserSelectionSnapshot userSelection = new MultipleUserSelectionSnapshot(UserSelectionSnapshot.getInstanceForCustomLogin(login));
		userSelection.addCopies(this.createMultipleOperationUserSelection(login, operations,start,end,isDistinctReport));
		this.setStartEndDateToCustomProperty(userSelection, start, end, isDistinctReport);
		return userSelection;
	}
	
	private List<UserSelectionSnapshot> createMultipleOperationUserSelection(String userLogin, List<String> list, Date start, Date end, Boolean isDistinctReport) throws Exception {
		List<UserSelectionSnapshot> result = new ArrayList<>();
		for (String operationUid: list) {
			UserSelectionSnapshot userSelection  = UserSelectionSnapshot.getInstanceForCustomOperation(userLogin, operationUid);
			this.setStartEndDateToCustomProperty(userSelection, start, end,isDistinctReport);
			result.add(userSelection);
		}
		return result;
	}
	
	private void setStartEndDateToCustomProperty(UserSelectionSnapshot userSelection, Date start, Date end,	Boolean isDistinctReport) throws Exception {
		userSelection.setCustomProperty(FIELD_IS_DISTINCT_REPORT,isDistinctReport);
		userSelection.setCustomProperty(FIELD_START_DATE, start);
		userSelection.setCustomProperty(FIELD_END_DATE,end);
		String dateString = ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(start);
		if (!start.equals(end)) {
			dateString += " - "+ ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(end);
		}
		userSelection.setCustomProperty(CUSTOM_PROPERTY_DATE_STRING,dateString);
	}

	private List getSelectedOperation(CommandBean cb) throws Exception {
		Object obj = cb.getRoot().getDynaAttr().get(FIELD_OPERATION);
		if (obj == null) {
			return null;
		}
		if (StringUtils.isBlank(obj.toString())) {
			return null;
		}
		if (obj.toString().equals("null")) {
			return null;
		}
		return Arrays.asList(obj.toString().split(","));
	}
	
	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception {
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		String dateString = (String) userContext.getUserSelection().getCustomProperties().get(CUSTOM_PROPERTY_DATE_STRING);
		if (super.getOperationRequired() && operation == null) {
			throw new Exception("Operation must be selected");
		}
		return (operation == null ? "" : operation.getOperationName()) + " " + dateString + " " + (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
	}
	
	protected void beforeSaveReportEntry(JobContext jobContext, ReportFiles report) throws Exception {
		if (!this.isDistinctReport(jobContext.getUserContext().getUserSelection())) {
			report.setReportOperationUid(null);
		}
	}
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception {
		String report_type = this.getReportType(userContext.getUserSelection());
		if (this.isDistinctReport(userContext.getUserSelection())) {
			Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
			if (operation == null) {
				throw new Exception("Operation must be selected");
			}
			String name = operation.getOperationName() + " " + (userContext.getUserSelection().getCustomProperty(CUSTOM_PROPERTY_DATE_STRING)) + 
				(StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
			return report_type + "/" + operation.getOperationUid() + "/" + ReportUtils.replaceInvalidCharactersInFileName(name);
		}
		String name = report_type + " " + (userContext.getUserSelection().getCustomProperty(CUSTOM_PROPERTY_DATE_STRING)) + 
				(StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
		return report_type + "/" + ReportUtils.replaceInvalidCharactersInFileName(name);
	}

	public void setOperationLoadHandler(OperationLoadHandler operationLoadHandler) {
		this.operationLoadHandler = operationLoadHandler;
	}

	public OperationLoadHandler getOperationLoadHandler() {
		return operationLoadHandler;
	}

	public void setLatestDays(Integer latestDays) {
		this.latestDays = latestDays;
	}

	public Integer getLatestDays() {
		return latestDays;
	}
	private List <String> getOperationFromDateRange(Date start, Date end) throws Exception {
		List<String> list = null;
		if (start != null && end != null) {
			list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select DISTINCT o.operationUid " +
					"From Operation o, Daily d WHERE (o.isDeleted is null or o.isDeleted = false) AND " +
					"(d.isDeleted is null or d.isDeleted = false) AND " +
					"(o.operationUid=d.operationUid) AND d.dayDate >=:startDate AND " +
					"d.dayDate<=:endDate", new String[] {"startDate","endDate"}, new Object[]{start, end});
		}
		return list;

	}
	private boolean isDistinctReport(UserSelectionSnapshot userSelection) {
		Object value = userSelection.getCustomProperty(FIELD_IS_DISTINCT_REPORT);
		if (value instanceof Boolean) return (Boolean) value;
		if (value == null) return false;
		return false;
	}
	protected String getReportDisplayNameOnCreation(ReportFiles reportFile, ReportTypes reportType, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) throws Exception {
		String file_name_pattern = null;
		if (reportType != null) {
			file_name_pattern = reportType.getReportFilenamePattern();
		}
		if (StringUtils.isBlank(file_name_pattern)) {
			return null;
		}

		// look for <distinct></distinct> or <combined></combined>, both must be specified if used
		if (file_name_pattern.indexOf("<distinct>") != -1) {
			if (this.isDistinctReport(userSelection)) {
				file_name_pattern = this.getFileNamePattern(file_name_pattern, "distinct");
			} else {
				file_name_pattern = this.getFileNamePattern(file_name_pattern, "combined");
			}
		}
		return this.formatDisplayName(file_name_pattern, reportFile, operation, daily, reportDaily, userSelection, reportJobParams);
	}
	
	private String getFileNamePattern(String pattern, String subType) {
		int start = pattern.indexOf("<" + subType + ">");
		if(start == -1) return pattern;
		int end = pattern.indexOf("</" + subType + ">");
		if(end == -1) return pattern;
		return pattern.substring(start + subType.length() + 2, end);
	}
}
