package com.idsdatanet.d2.drillnet.wellHistoryOverview;

import java.io.File;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.query.representation.DatabaseQuery;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.session.ParameterizedQueryFilter;
import com.idsdatanet.d2.drillnet.operationinformationcenter.DDRReportList;
import com.idsdatanet.d2.drillnet.operationinformationcenter.DGRReportList;
import com.idsdatanet.d2.core.uom.CustomFieldUom;

public class WellHistoryOverviewDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{
			
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		Object object = node.getData();

		// a trick to only load used Operation object (initially load as String, only load the real object when needed)
		if((object instanceof String) && meta.getTableClass() == Operation.class){
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Operation WHERE operationUid=:thisOperationUid", "thisOperationUid", object.toString());
			object = lstResult.get(0);
			node.setData(object);
		}
		
		if(object instanceof Operation) {
			
			Operation thisOperation = (Operation) object;
			
			/// retrieve operation start and end dates
			String strSql = "SELECT MIN(reportDatetime), MAX(reportDatetime) FROM ReportDaily where (isDeleted=false or isDeleted is null) and operationUid=:operationUid";
			List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", thisOperation.getOperationUid());
			if (!result.isEmpty()) {
				Object[] rec = result.get(0);
				node.getDynaAttr().put("startDate", rec[0]);
				node.getDynaAttr().put("endDate", rec[1]);
			}
			
			// only load the report list when parent node (Operation) is visible (in a page) in order to improve performance
			this.loadDDRReportList(node, thisOperation, userSelection);
			this.loadDGRReportList(node, thisOperation, userSelection);
			this.loadLessonTicketList(node, thisOperation, userSelection);
		
			//load file manager file list 
			Integer maxNumberFilesToShow = this.getMaxUploadedFileToShow(userSelection);
			String fileMangerList = CommonUtil.getConfiguredInstance().loadFileManagerFileListForOIC(thisOperation.getOperationUid(), maxNumberFilesToShow);
			if (StringUtils.isNotBlank(fileMangerList)) {
				node.getDynaAttr().put("fileManagerList", fileMangerList);
			}
						
			//GET TODAY'S RIG
			String dailyUid = ApplicationUtils.getConfiguredInstance().getLastDailyUidInOperation(thisOperation.getOperationUid());
			if(StringUtils.isNotBlank(dailyUid))
			{
				//get report daily object to fill out information from it
				ReportDaily thisReportDaily = ApplicationUtils.getConfiguredInstance().getReportDailyByOperationType(dailyUid, thisOperation.getOperationCode());
				
				if(thisReportDaily != null){
					String strSql2= "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and reportDailyUid = :thisReportDailyUid";
					String[] paramsFields = {"thisReportDailyUid"};
					String[] paramsValues = {thisReportDaily.getReportDailyUid()};				
				
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields, paramsValues);
					
					if (!lstResult.isEmpty())
					{
						ReportDaily selectedReportDaily =  (ReportDaily) lstResult.get(0);
						if (StringUtils.isNotBlank(selectedReportDaily.getRigInformationUid())) node.getDynaAttr().put("todayRig", selectedReportDaily.getRigInformationUid());
						else node.getDynaAttr().put("todayRig", thisOperation.getRigInformationUid());
					}
					else
					{
						node.getDynaAttr().put("todayRig", thisOperation.getRigInformationUid());
					}
				}
			}
			
			//GET EVENT NAME
			String strIntvEvent = this.getLookupValue(commandBean, node, node.getData().getClass().getSimpleName(), "operationCode", thisOperation.getOperationCode(), false, "");
			if(thisOperation.getOperationalUnit() != null && StringUtils.isNotBlank(this.getLookupValue(commandBean, node, node.getData().getClass().getSimpleName(), "operationalUnit", thisOperation.getOperationalUnit(), false, ""))) 
			{
				strIntvEvent = strIntvEvent + "-" + this.getLookupValue(commandBean, node, node.getData().getClass().getSimpleName(), "operationalUnit", thisOperation.getOperationalUnit(), false, "");
			}
			if(thisOperation.getOperationalCode() != null && StringUtils.isNotBlank(this.getLookupValue(commandBean, node, node.getData().getClass().getSimpleName(), "operationalCode", thisOperation.getOperationalCode(), true, thisOperation.getOperationalUnit()))) 
			{
				strIntvEvent = strIntvEvent + "-" + this.getLookupValue(commandBean, node, node.getData().getClass().getSimpleName(), "operationalCode", thisOperation.getOperationalCode(), true, thisOperation.getOperationalUnit());
			}
			node.getDynaAttr().put("interventionEvent", strIntvEvent);
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Operation.class, "daysSpentPriorToSpud");
			if(StringUtils.isNotBlank(dailyUid) || dailyUid!=null){
				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysOnOperation(thisOperation.getOperationUid(), dailyUid));
			}
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@daysOnWell", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("daysOnWell", thisConverter.getConvertedValue());
		}
	}
	
	private int getMaxReportToShow(UserSelectionSnapshot userSelection) throws Exception {
		String maxReportNumber = GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_MAX_REPORT_TO_SHOW_IN_OIC);
		int intMaxReportNumber = 6;
		if(maxReportNumber != null){
			intMaxReportNumber = Integer.parseInt(maxReportNumber) - 1;
		}
		return intMaxReportNumber;
	}
	
	private void loadDGRReportList(CommandBeanTreeNode parentNode, Operation operation, UserSelectionSnapshot userSelection) throws Exception {
		int intMaxReportNumber = this.getMaxReportToShow(userSelection);
		
		Integer intCounter = 0;
		String[] paramsFields = {"thisOperation"};
		String[] paramsValues = {operation.getOperationUid()};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation AND (rd.reportType='DGR') AND (rf.reportType='DGR') ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues);
		
		for(Iterator i = items.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			DGRReportList dgr = new DGRReportList();
			ReportFiles thisReportFile = (ReportFiles) obj_array[0];
			dgr.setData(thisReportFile, operation.getOperationName());
			File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());
			
			if(report_file.exists()){
				parentNode.addChild(DGRReportList.class.getSimpleName(), dgr);
				intCounter++;
				if (intCounter > intMaxReportNumber) break;
			}
		}
	}

	private void loadDDRReportList(CommandBeanTreeNode parentNode, Operation operation, UserSelectionSnapshot userSelection) throws Exception {
		int intMaxReportNumber = this.getMaxReportToShow(userSelection);
		
		Integer intCounter = 0;
		String thisReportType = "DDR";
		String[] paramsFields = {"thisOperation", "reportDailyReportType", "reportFilesReportType"};
		String[] paramsValues = {operation.getOperationUid(), thisReportType, thisReportType};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rd.reportType=:reportDailyReportType and rf.reportType=:reportFilesReportType ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues);
					
		if(items.size() < 1)
		{
			Map<String, String> reportTypePriorityWhenDrllgNotExist = CommonUtils.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist();
			String operationCode = operation.getOperationCode();
			String strReportTypes = reportTypePriorityWhenDrllgNotExist.get(operationCode);
			if (strReportTypes != null) {
				String[] reportTypes = strReportTypes.split("[,]");
				for (String reportType : reportTypes) {
					String[] paramsValues2 = {operation.getOperationUid(), reportType, reportType};
					thisReportType = reportType;
					items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rd.reportType=:reportDailyReportType and rf.reportType=:reportFilesReportType ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues2);
					
					if(items.size() > 0)
					{
						break;
					}
				}
			}
		}
				
		for(Iterator i = items.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			DDRReportList ddr = new DDRReportList();
			ReportFiles thisReportFile = (ReportFiles) obj_array[0];
			ddr.setData(thisReportFile, operation.getOperationName());
						
			File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());
			
			if(report_file.exists()){
				parentNode.addChild(DDRReportList.class.getSimpleName(),ddr);
				
				intCounter++;
				if (intCounter > intMaxReportNumber) break;
			}
		}
	}
	
	private void loadLessonTicketList(CommandBeanTreeNode node, Operation operation, UserSelectionSnapshot userSelection) throws Exception {
	
		String lessonTicketInfo = "";
		String[] paramsFields = {"thisOperation"};
		String[] paramsValues = {operation.getOperationUid()};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select d.dayNumber, d.dailyUid, d.dayDate, l.lessonTicketUid, l.lessonTitle" +
				" FROM Daily d, LessonTicket l WHERE (d.isDeleted = false or d.isDeleted is null) and " +
				"(l.isDeleted = false or l.isDeleted is null) and d.operationUid=:thisOperation AND " +
				"l.dailyUid=d.dailyUid order by d.dayDate DESC", paramsFields, paramsValues);
		
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
		
		for(Iterator i = items.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			
			String dayNum= URLEncoder.encode(this.nullToEmptyString(obj_array[0]), "utf-8");
			String dailyUid = URLEncoder.encode(obj_array[1].toString(), "utf-8");		
			String dDate = df.format(obj_array[2]);			
			String dayDate = URLEncoder.encode(dDate, "utf-8");
			String lessonTicketUid = URLEncoder.encode(obj_array[3].toString(), "utf-8");
			String lessonTicketTitle = URLEncoder.encode(this.nullToEmptyString(obj_array[4]), "utf-8");
			
			if (StringUtils.isBlank(lessonTicketInfo)) {
				lessonTicketInfo = "daynum=" + dayNum + "&dailyuid=" + dailyUid + "&daydate=" + dayDate  + "&lessonticketuid=" + lessonTicketUid + "&lessontitle=" + lessonTicketTitle;
			}else {
				lessonTicketInfo = lessonTicketInfo + ",daynum=" + dayNum + "&dailyuid=" + dailyUid + "&daydate=" + dayDate  + "&lessonticketuid=" + lessonTicketUid + "&lessontitle=" + lessonTicketTitle;
			}

		}
		node.getDynaAttr().put("lessonTicketList", lessonTicketInfo);

	}
	
	private Integer getMaxUploadedFileToShow(UserSelectionSnapshot userSelection) throws Exception {
		String maxFilesNumber = GroupWidePreference.getValue(userSelection.getGroupUid(), "maxUploadedFilesToShowInOIC");
		Integer intMaxFilesNumber = 9;
		if(maxFilesNumber != null){
			intMaxFilesNumber = Integer.parseInt(maxFilesNumber) - 1;
		}
		return intMaxFilesNumber;
	}

	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if (meta.getTableClass().equals(Operation.class)) {
			// retrieve all the operations that are not being assigned to a team that the current user not belongs to
			/*List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT o.operationUid, " +
					"MAX(d.dayDate) FROM Operation o, Daily d WHERE (o.isDeleted = '0' OR o.isDeleted IS NULL) AND " +
					"(d.isDeleted = '0' OR d.isDeleted IS NULL) AND o.operationUid = d.operationUid AND " +
					"(((o.tightHole = '0' OR o.tightHole IS NULL) AND (o.operationUid NOT IN " +
					"(SELECT a.operationUid FROM OpsTeamOperation a WHERE (a.isDeleted = '0' OR a.isDeleted IS NULL)) " +
					"OR o.operationUid IN (SELECT b.operationUid FROM OpsTeamOperation b, OpsTeamUser c " +
					"WHERE b.opsTeamUid = c.opsTeamUid AND (b.isDeleted = '0' OR b.isDeleted is null) AND " +
					"(c.isDeleted = '0' OR c.isDeleted IS NULL) AND c.userUid = :userUid))) OR (o.tightHole = '1' AND o.operationUid IN" +
					" (SELECT d.operationUid FROM TightHoleUser d WHERE (d.isDeleted = '0' OR d.isDeleted IS NULL) AND d.userUid = :userUid)))" +
					" GROUP BY o.operationUid", "userUid", UserSession.getInstance(request).getUserUid());
			List<Object> output_maps = new ArrayList<Object>();
			
			Collections.sort(items, new OperationComparator());
			
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();
				output_maps.add(obj_array[0]);
			}
			return output_maps;*/
			
			/* get filter value; if filter value is null, then return no result */
			String selectedField = "";
			String selectedCond = "";
			String selectedOpType = "";
			String selecteddataUid = "";
			Integer selectedOutputRecord = 0; 
			
			String selectedScreenOutputFilter = (String) commandBean.getRoot().getDynaAttr().get("ScreenOutput");
			if (selectedScreenOutputFilter !=null)
			{
				if (selectedScreenOutputFilter.equalsIgnoreCase("last10")) selectedOutputRecord =10;
			}
			else
				selectedScreenOutputFilter ="";
			
			String selectedOperationFilter = (String) commandBean.getRoot().getDynaAttr().get("OperationFilter");
			if (selectedOperationFilter !=null) 
			{				
				if (selectedOperationFilter.equalsIgnoreCase("campaign"))
				{	
					selecteddataUid = (String) commandBean.getRoot().getDynaAttr().get("operationCampaignUid");
					if (selecteddataUid == null) selecteddataUid = "";
					
					selectedField = "operationCampaignUid";
					selectedCond = "o.operationCampaignUid = :operationCampaignUid";
				}
				else if (selectedOperationFilter.equalsIgnoreCase("well"))
				{
					selecteddataUid = (String) commandBean.getRoot().getDynaAttr().get("wellUid");
					if (selecteddataUid == null) selecteddataUid = "";
					selectedField = "wellUid";
					selectedCond = "AND o.wellUid = :wellUid ";					
				}			
				
			}
			
			String selectedOperationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
			if (StringUtils.isNotBlank(selectedOperationCode)){
				selectedOpType = "AND o.operationCode = '" + selectedOperationCode + "'";
			}
			
			if (selectedScreenOutputFilter == "" || selectedCond == "")  return null;
			
			if(selecteddataUid.equalsIgnoreCase("showall"))
			{
				selectedField = "";
				selectedCond = "";
			}
			
			//if (!selectedCond.equalsIgnoreCase("")) selectedCond += " and ";
			
			List<Object> output_maps = new ArrayList<Object>();
			List<Object> output_maps2 = new ArrayList<Object>();
			List result = new ArrayList();
			UserSession userSession = UserSession.getInstance(request);
				
			String allOperationsNotAssignedToOpsTeam = "o.operationUid NOT IN (SELECT a.operationUid FROM OpsTeamOperation a, OpsTeam e WHERE " + 
			"e.opsTeamUid = a.opsTeamUid AND (a.isDeleted = false OR a.isDeleted IS NULL) AND (e.isPublic = false OR e.isPublic IS NULL)) OR ";
			if (BooleanUtils.isTrue(userSession.getCurrentUser().getAccessToOpsTeamOperationsOnly())) {
				allOperationsNotAssignedToOpsTeam = "";
			}
			
			String queryString = "SELECT DISTINCT o.operationUid FROM Operation o";
			
			// query filter
			DatabaseQuery queryFilter = userSession.getSystemSelectionFilter().getQueryFilter(ModuleConstants.WELL_MANAGEMENT,"Operation", "o");
			if (StringUtils.isNotBlank(queryFilter.getParentTablesString())) {
				queryString += ", " + queryFilter.getParentTablesString();
			}
			String conditionClause = queryFilter.getConditionClause(userSession);
			if (queryFilter instanceof ParameterizedQueryFilter){
				queryString += " WHERE ";
			}
			
			if (StringUtils.isNotBlank(conditionClause)) {
				queryString += conditionClause + " AND ";
			}
			
			queryString += "(o.isDeleted = '0' OR o.isDeleted IS NULL) AND " + 
				"(((o.tightHole = '0' OR o.tightHole IS NULL) AND (o.isCampaignOverviewOperation = '0' OR o.isCampaignOverviewOperation IS NULL) " + 
				"AND (" + allOperationsNotAssignedToOpsTeam + "o.operationUid IN (SELECT b.operationUid FROM OpsTeamOperation b, OpsTeamUser c " + 
				"WHERE b.opsTeamUid = c.opsTeamUid AND (b.isDeleted = '0' OR b.isDeleted is null) AND " + 
				"(c.isDeleted = '0' OR c.isDeleted IS NULL) AND c.userUid = :userUid))) OR (o.tightHole = '1' AND o.operationUid IN " + 
				"(SELECT d.operationUid FROM TightHoleUser d WHERE (d.isDeleted = '0' OR d.isDeleted IS NULL) AND d.userUid = :userUid))) " +
				 selectedCond + selectedOpType +
				" GROUP BY o.operationUid ORDER BY o.sysOperationLastDatetime DESC";
			String[] paramNames;
			Object[] paramValues;
			if (queryFilter instanceof ParameterizedQueryFilter){
				ParameterizedQueryFilter pQueryFilter = (ParameterizedQueryFilter)queryFilter;
				if (selectedField==""){
					paramNames = (String[]) ArrayUtils.addAll(pQueryFilter.getParameterNames(),new String[] { "userUid" });
					paramValues = ArrayUtils.addAll(pQueryFilter.getParameterValues(), new Object[] { userSession.getUserUid()});
				}
				else{
					paramNames = (String[]) ArrayUtils.addAll(pQueryFilter.getParameterNames(),new String[] { "userUid", selectedField });
					paramValues = ArrayUtils.addAll(pQueryFilter.getParameterValues(), new Object[] { userSession.getUserUid() , selecteddataUid });
				}
			} else {
				
				if (selectedField==""){
					paramNames = new String[] { "userUid" };
					paramValues = new Object[] { userSession.getUserUid()};
				}
				else{
					paramNames = new String[] { "userUid", selectedField };
					paramValues = new Object[] { userSession.getUserUid() , selecteddataUid };
				}
			}			
			
			Integer RecordCounter=0;
			List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
			if (!items.isEmpty()){
				for(Iterator i = items.iterator(); i.hasNext(); ){
					String operationUid = (String)i.next();
					
					List items2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select operationUid, MAX(reportDatetime)" +
							"from ReportDaily where (isDeleted = '0' OR isDeleted IS NULL) and operationUid = :operationUid GROUP BY operationUid", 
							"operationUid", operationUid);
					
					if (!items2.isEmpty()) {
						for(Iterator i2 = items2.iterator(); i2.hasNext(); ){
							Object[] obj_array = (Object[]) i2.next();
							result.add(obj_array);
							//output_maps.add(obj_array[0]);
						}
					}else {
						output_maps2.add(operationUid);
					}
					
					// return number of record
					RecordCounter++;
					if (selectedOutputRecord >0 && RecordCounter >= selectedOutputRecord) break;
						
				}
					
				//DO NOT SORT AGAIN AS THE OPERATION LIST HAS BEEN SORTED DESCENDING IN THE ABOVE QUERY
				//Collections.sort(result, new OperationComparator());
				
				for(Iterator i3 = result.iterator(); i3.hasNext(); ){
					Object[] obj_array = (Object[]) i3.next();
					output_maps.add(obj_array[0]);
				}
				
				for(Iterator i4 = output_maps2.iterator(); i4.hasNext(); ){
					output_maps.add(i4.next());
				}
				
				return output_maps;
				
			}
			
		}
		
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		return true;
	}
	
	@SuppressWarnings("unused")
	private class OperationComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Object in1 = o1[1];
				Object in2 = o2[1];
				
				Date f1 = (Date) in1;
				Date f2 = (Date) in2;
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	private String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}
	
	private String getLookupValue(CommandBean commandBean, CommandBeanTreeNode node, String className, String field, String value, Boolean isCascaded, String key) throws Exception
	{
		Map<String, LookupItem> thisLookupMap = commandBean.getLookupMap(node, className, field);
		if(isCascaded) 
		{
			thisLookupMap = commandBean.getCascadeLookupMap(node, className, field, key);
		}
		
		if(thisLookupMap != null) 
		{
			LookupItem thisLookupItem = thisLookupMap.get(value);
			if(thisLookupItem != null)
			{
				return thisLookupItem.getValue().toString();
			}
		}
						
		return "";
	}
	
}
