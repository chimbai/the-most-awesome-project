package com.idsdatanet.d2.drillnet.drillLine;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.BhaComponent;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DrillPipeTally;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.TourWireline;
import com.idsdatanet.d2.core.model.TourWirelineDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;

/**
 * 
 * @author Kwong
 *
 */

public class DrillLineUtils {

	private static Double FEET = 0.30480000000000003340608;
	private static Double LB_PER_FEET = 1.48816; //pound per foot
	private static Double PPG = 119.8264;
	private static Double LBS = 0.4535924; //pounds
	
	private static String resetDailyWearUserCode = "10.2.1";
	private static String resetCummulativeWearUserCode = "10.2.2";
	
	/**
	 * Calculate Slipped Length and Present Length
	 * @param tourWirelineDetail
	 * @param thisConverter
	 * @throws Exception
	 */
	public static Double calculateSlipAndCut(TourWirelineDetail tourWirelineDetail, CustomFieldUom thisConverter, Double prevPresentLength) throws Exception {
		if (tourWirelineDetail==null) return null;
		
		Double slippedLength = null;
		Double presentLength = null;
		Double lengthWireInDerrick = 0.00;
		Double totalCutOffLength = 0.00;
		Double originalSpoolLength = 0.00;
		
		if(prevPresentLength==null){
			TourWireline wireline = (TourWireline) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(TourWireline.class, tourWirelineDetail.getTourWirelineUid());
			if (wireline!=null && wireline.getOriginalSpoolLength()!=null) {
				thisConverter.setReferenceMappingField(TourWireline.class, "originalSpoolLength");
				thisConverter.setBaseValueFromUserValue(wireline.getOriginalSpoolLength());
				originalSpoolLength = thisConverter.getBasevalue();
				prevPresentLength = originalSpoolLength;
			}else{
				prevPresentLength = 0.00;
			}
		}
		
		if (tourWirelineDetail.getLengthWireInDerrick()!=null) {
			thisConverter.setReferenceMappingField(TourWirelineDetail.class, "lengthWireInDerrick");
			if (thisConverter.isUOMMappingAvailable()) {
				thisConverter.setBaseValueFromUserValue(tourWirelineDetail.getLengthWireInDerrick());
				lengthWireInDerrick = thisConverter.getBasevalue();
			} else {
				lengthWireInDerrick = tourWirelineDetail.getLengthWireInDerrick();
			}
		}
		
		if (tourWirelineDetail.getTotalCutOffLength()!=null) {
			thisConverter.setReferenceMappingField(TourWirelineDetail.class, "totalCutOffLength");
			if (thisConverter.isUOMMappingAvailable()) {
				thisConverter.setBaseValueFromUserValue(tourWirelineDetail.getTotalCutOffLength());
				totalCutOffLength = thisConverter.getBasevalue();
			} else {
				totalCutOffLength = tourWirelineDetail.getTotalCutOffLength();
			}
		}
		
		// calculate slippedLength
		if(lengthWireInDerrick!=null && totalCutOffLength!=null)
			slippedLength = lengthWireInDerrick + totalCutOffLength;
		
		thisConverter.setReferenceMappingField(TourWirelineDetail.class, "slippedLength");
		if (slippedLength!=null && thisConverter.isUOMMappingAvailable()) {
			thisConverter.setBaseValue(slippedLength);
			tourWirelineDetail.setSlippedLength(thisConverter.getConvertedValue());
		} else {
			tourWirelineDetail.setSlippedLength(slippedLength);
		}
		
		// calculate presentLength
		if(prevPresentLength!=null || totalCutOffLength!=null){
			presentLength = prevPresentLength - totalCutOffLength;
			prevPresentLength = presentLength;
		}
		
		thisConverter.setReferenceMappingField(TourWirelineDetail.class, "currentLength");
		if (presentLength!=null && thisConverter.isUOMMappingAvailable()) {
			thisConverter.setBaseValue(presentLength);
			tourWirelineDetail.setCurrentLength(thisConverter.getConvertedValue());
		} else {
			tourWirelineDetail.setCurrentLength(presentLength);
		}
		
		
		/*calculate for slip and cut 
		if (tourWirelineDetail.getLengthWireInDerrick()!=null) {
			thisConverter.setReferenceMappingField(TourWirelineDetail.class, "lengthWireInDerrick");
			if (thisConverter.isUOMMappingAvailable()) {
				thisConverter.setBaseValueFromUserValue(tourWirelineDetail.getLengthWireInDerrick());
				slippedLength = thisConverter.getBasevalue();
			} else {
				slippedLength = tourWirelineDetail.getLengthWireInDerrick();
			}
		}
		
		TourWireline wireline = (TourWireline) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(TourWireline.class, tourWirelineDetail.getTourWirelineUid());
		if (wireline!=null && wireline.getOriginalSpoolLength()!=null) {
			thisConverter.setReferenceMappingField(TourWireline.class, "originalSpoolLength");
			thisConverter.setBaseValueFromUserValue(wireline.getOriginalSpoolLength());
			presentLength = thisConverter.getConvertedValue();
		}
		
		if (tourWirelineDetail.getCutOffLength()!=null) {
			if (slippedLength==null) slippedLength = 0.0;
			if (presentLength==null) presentLength = 0.0;
			thisConverter.setReferenceMappingField(TourWirelineDetail.class, "cutOffLength");
			if (thisConverter.isUOMMappingAvailable()) {
				thisConverter.setBaseValueFromUserValue(tourWirelineDetail.getCutOffLength());
				slippedLength = slippedLength + thisConverter.getBasevalue();
				presentLength = presentLength - thisConverter.getBasevalue();
			} else {
				slippedLength = slippedLength + tourWirelineDetail.getCutOffLength();
				presentLength = presentLength - tourWirelineDetail.getCutOffLength();
			}
		}
		
		thisConverter.setReferenceMappingField(TourWirelineDetail.class, "slippedLength");
		if (slippedLength!=null && thisConverter.isUOMMappingAvailable()) {
			thisConverter.setBaseValue(slippedLength);
			tourWirelineDetail.setSlippedLength(thisConverter.getConvertedValue());
		} else {
			tourWirelineDetail.setSlippedLength(slippedLength);
		}
		
		thisConverter.setReferenceMappingField(TourWirelineDetail.class, "currentLength");
		if (presentLength!=null && thisConverter.isUOMMappingAvailable()) {
			thisConverter.setBaseValue(presentLength);
			tourWirelineDetail.setCurrentLength(thisConverter.getConvertedValue());
		} else {
			tourWirelineDetail.setCurrentLength(presentLength);
		}
		*/
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(tourWirelineDetail);
		
		return prevPresentLength;
	}
	
	/**
	 * Calculate Slipped Length and Present Length
	 * @param tourWireline
	 * @param thisConverter
	 * @throws Exception
	 */
	public static void calculateSlipAndCut(TourWireline tourWireline, CustomFieldUom thisConverter) throws Exception {
		if (tourWireline==null) return;
		
		String queryString = "FROM TourWirelineDetail WHERE (isDeleted=false or isDeleted is null) and tourWirelineUid=:tourWirelineUid";
		List<TourWirelineDetail> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "tourWirelineUid", tourWireline.getTourWirelineUid());
		for (TourWirelineDetail detail : list) {
			Double prevPresentLength = null;
			prevPresentLength = calculateSlipAndCut(detail, thisConverter, prevPresentLength);
		}
		
	}
	
	
	/**
	 *  calculate Wear/Trips 
	 *  @param tourWirelineDetail
	 *  @param thisConverter
	 *  @throws Exception
	 *  
	 *  formula: RTTM = (Wp * D * (Lp + D) + (2 * D) * (2 * Wb + Wc)) / (5280 * 2000);
		* RTTM = Round Trip Ton-Miles
		* Wp = buoyed weight of drill pipe in lb/ft
		* D = hole measured depth in ft
		* Lp = Average Length per stand of drill pipe in ft
		* Wb = weight of traveling block in lb
		* Wc = buoyed weight of BHA lbs (drill collar + heavy weight dril pipe + BHA) in mud minus the buoyed weight of the same length of drill pipe in lb
		* 2000 = number of pounds in one ton
		* 5280 = number of feet in one mile
	 *  
	 */
	public static void calculateWearTripSinceLastCut(TourWirelineDetail tourWirelineDetail, CustomFieldUom thisConverter) throws Exception {
		if (tourWirelineDetail!=null) {
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			String dailyUid = tourWirelineDetail.getDailyUid();
			
			Date startDatetime = null;
			Date endDatetime = null;
			TourWireline wireline = (TourWireline) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(TourWireline.class, tourWirelineDetail.getTourWirelineUid());
			if (wireline!=null) {
				startDatetime = wireline.getInstallDateTime();
				endDatetime = wireline.getUninstallDateTime();
			}
			
			Double dpLength = 0.0;
			Double dpWeight = 0.0;
			Double BF = calculateBuoyancyFactor(dailyUid, startDatetime, endDatetime, qp); //Buoyancy Factor
			
			List<String> paramNames = new LinkedList<String>();
			List<Object> paramValues = new LinkedList<Object>();
			paramNames.add("dailyUid"); paramValues.add(dailyUid);
			paramNames.add("startDatetime"); paramValues.add(startDatetime);
			if (endDatetime!=null) {
				paramNames.add("endDatetime"); paramValues.add(endDatetime);
			}
			String queryString = "FROM Bharun b, DrillPipeDaily d, DrillPipeTally t " +
					"WHERE (d.isDeleted=false or d.isDeleted is null) " +
					"AND (t.isDeleted=false or t.isDeleted is null) " +
					"AND (b.isDeleted=false or b.isDeleted is null) " +
					"AND b.bharunUid=d.bharunUid " +
					"AND (b.timeIn>=:startDatetime) " +
					(endDatetime!=null?"AND (b.timeOut<=:endDatetime or b.timeOut is null) ":"") +
					"AND t.drillPipeDailyUid=d.drillPipeDailyUid " +
					"AND d.dailyUid=:dailyUid ";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, qp);
			for (Object[] rec : list) {
				DrillPipeTally tally = (DrillPipeTally) rec[2];
				
				if (tally.getJointLength()!=null) dpLength += tally.getJointLength(); 
				if (tally.getWeight()!=null) dpWeight += tally.getWeight(); 
			}
			dpWeight = dpWeight / LB_PER_FEET; //lb/ft
			dpLength = dpLength / FEET; //ft
			
			Double Wp = (dpWeight) * BF;
			Double Lp = dpLength;
			
			queryString = "FROM Activity WHERE (isDeleted=false or isDeleted is null) " +
					"AND (isSimop=false or isSimop is null) " +
					"AND (isOffline=false or isOffline is null) " +
					"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
					"AND dailyUid=:dailyUid " +
					"AND startDatetime>=:startDatetime " +
					(endDatetime!=null?"AND endDatetime<=:endDatetime ":"") +
					"ORDER BY startDatetime";
			Double cumDistance = 0.0;
			List<Activity> actvityList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, qp);
			for (Activity activity:actvityList) {
				Double distance = null;
				if (activity.getStringFromDepthMdMsl()!=null && activity.getStringToDepthMdMsl()!=null) {
					if (activity.getStringToDepthMdMsl() > activity.getStringFromDepthMdMsl()) {
						distance = activity.getStringToDepthMdMsl() - activity.getStringFromDepthMdMsl();
					} else {
						distance = activity.getStringFromDepthMdMsl() - activity.getStringToDepthMdMsl();
					}
				}
				if (resetDailyWearUserCode.equals(activity.getUserCode()) || resetCummulativeWearUserCode.equals(activity.getUserCode())) { //reset daily wear
					cumDistance = 0.0;
				}
				if (distance!=null) {
					cumDistance += distance;
				}
			}
			cumDistance = cumDistance / FEET; 
			Double D = cumDistance;

			Double Wb = 0.0;
			queryString = "FROM RigInformation WHERE (isDeleted=false or isDeleted is null) and rigInformationUid=:rigInformationUid";
			List<RigInformation> rigList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "rigInformationUid", wireline.getRigInformationUid(), qp);
			if (rigList.size()>0) {
				RigInformation rig = rigList.get(0);
				if (rig.getTravelingBlockWeight()!=null) Wb = rig.getTravelingBlockWeight();
			}
			Wb = Wb / LBS; //lbs
			Double Wc = DrillLineUtils.calculateBhaBuoyedWeight(dailyUid, startDatetime, endDatetime, BF, qp);
			Wc = Wc / LBS;
			
			Double RTTM = (Wp * D * (Lp + D) + (2 * D) * (2 * Wb + Wc)) / (5280 * 2000);
			RTTM = RTTM / 2.0;
			thisConverter.setReferenceMappingField(TourWirelineDetail.class, "dailyWear");
			if (thisConverter.isUOMMappingAvailable()) {
				thisConverter.setBaseValue(RTTM);
				tourWirelineDetail.setDailyWear(thisConverter.getConvertedValue());
			}
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(tourWirelineDetail);
		}
		
	}
	
	/**
	 * calculate accumulated wear/trips
	 * @param tourWirelineUid
	 * @throws Exception
	 */
	public static void calculateAccumulatedWear(String tourWirelineUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Double accumulated = 0.0;
		String queryString = "FROM TourWirelineDetail w, Daily d " +
				"WHERE (w.isDeleted=false or w.isDeleted is null) " +
				"AND (d.isDeleted=false or d.isDeleted is null) " +
				"AND w.dailyUid=d.dailyUid " +
				"AND w.tourWirelineUid=:tourWirelineUid " +
				"ORDER BY d.dayDate";
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "tourWirelineUid", tourWirelineUid, qp);
		for (Object[] rec : list) {
			TourWirelineDetail detail = (TourWirelineDetail) rec[0];
			
			if (resetCumulativeWearTrip(detail.getDailyUid())) accumulated = 0.0; //reset cumulative
			
			if (detail.getNewDailyWear()!=null) {
				accumulated += detail.getNewDailyWear();
			} else {
				if (detail.getDailyWear()!=null) {
					accumulated += detail.getDailyWear();
				}
			}
			detail.setAccumulativeDailyWear(accumulated);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(detail, qp);
		}
	}
	
	public static Boolean resetCumulativeWearTrip(String dailyUid) throws Exception {
		String queryString = "FROM Activity WHERE (isDeleted=false or isDeleted is null) " +
				"AND (isSimop=false or isSimop is null) " +
				"AND (isOffline=false or isOffline is null) " +
				"AND (carriedForwardActivityUid='' or carriedForwardActivityUid is null) " +
				"AND dailyUid=:dailyUid " +
				"AND userCode=:userCode " +
				"ORDER BY startDatetime";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"dailyUid", "userCode"}, new Object[]{dailyUid, resetCummulativeWearUserCode});
		if (!list.isEmpty()) return true;
		return false;
	}
	
	
	public static void createTourWirelineDetail(CommandBean commandBean, UserSession session) throws Exception {
		if (session==null || session.getCurrentDailyUid()==null) return;
		
		Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, session.getCurrentDailyUid());
		if (daily!=null && daily.getDayDate()!=null) {
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			Date dayDate = daily.getDayDate();
			Calendar startDatetime = Calendar.getInstance();
			startDatetime.setTime(dayDate);
			startDatetime.add(Calendar.DATE, 1);
			
			String queryString = "FROM TourWireline " +
					" WHERE (isDeleted=false or isDeleted is null) " +
					" AND rigInformationUid=:rigInformationUid " +
					" AND (installDateTime<:startDatetime) " +
					" AND (uninstallDateTime>=:endDatetime or uninstallDateTime is null) ";
			String[] paramNames = {"rigInformationUid", "startDatetime", "endDatetime"};
			Object[] paramValues = {session.getCurrentRigInformationUid(), startDatetime.getTime(), dayDate};
			List<TourWireline> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
			for (TourWireline wireline : list) {
				
				queryString = "FROM TourWirelineDetail WHERE (isDeleted=false or isDeleted is null) " +
						"AND tourWirelineUid=:tourWirelineUid and dailyUid=:dailyUid ";
				List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"tourWirelineUid","dailyUid"}, new Object[]{wireline.getTourWirelineUid(), session.getCurrentDailyUid()});
				if (result.isEmpty()) {
					TourWirelineDetail tourWirelineDetail = new TourWirelineDetail();
					tourWirelineDetail.setDailyUid(session.getCurrentDailyUid());
					tourWirelineDetail.setTourWirelineUid(wireline.getTourWirelineUid());
					tourWirelineDetail.setWellboreUid(session.getCurrentWellboreUid());
					tourWirelineDetail.setWellUid(session.getCurrentWellUid());
					tourWirelineDetail.setOperationUid(session.getCurrentOperationUid());
					tourWirelineDetail.setRigInformationUid(session.getCurrentRigInformationUid());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(tourWirelineDetail);
					
					Double prevPresentLength = null;
					prevPresentLength = calculateSlipAndCut(tourWirelineDetail, thisConverter, prevPresentLength);
					calculateWearTripSinceLastCut(tourWirelineDetail, thisConverter);
					calculateAccumulatedWear(wireline.getTourWirelineUid());
				}
				
			}
			
		}
		
	}

	public static Double calculateBuoyancyFactor(String dailyUid, Date startDatetime, Date endDatetime, QueryProperties qp) throws Exception {
		
		Double BF = 1.0;
		Boolean sameStartDay = false;
		Boolean sameEndDay = false;
		
		Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
		if (daily!=null) {
			if (DateUtils.isSameDay(startDatetime, daily.getDayDate())) sameStartDay = true;
			if (endDatetime!=null && DateUtils.isSameDay(endDatetime, daily.getDayDate())) sameEndDay = true;
		}
		
		List<String> paramNames = new LinkedList<String>();
		List<Object> paramValues = new LinkedList<Object>();
		String queryString = "FROM MudProperties WHERE (isDeleted=false or isDeleted is null) " +
				"AND dailyUid=:dailyUid " +
				(sameStartDay?"AND reportTime>=:startDatetime ":"") +
				(sameEndDay?"AND reportTime<=:endDatetime ":"") +
				"order by reportTime desc";
		paramNames.add("dailyUid");
		paramValues.add(dailyUid);
		if (sameStartDay) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(startDatetime);
			cal.set(1970, 0, 1);
			paramNames.add("startDatetime");
			paramValues.add(cal.getTime());
		}
		if (sameEndDay) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(endDatetime);
			cal.set(1970, 0, 1);
			paramNames.add("endDatetime");
			paramValues.add(cal.getTime());
		}
		
		List<MudProperties> mudList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, qp);
		if (mudList.size()>0) {
			MudProperties mp = mudList.get(0);
			if (mp.getMudWeight()!=null) {
				BF = (65.5 - (mp.getMudWeight() / PPG)) / 65.5;
			}
		}
		
		return BF;
	}
	
	public static Double calculateBhaBuoyedWeight(String dailyUid, Date startDatetime, Date endDatetime, Double buoyancyFactor, QueryProperties qp) throws Exception {

		Double bhaWeight = 0.0;

		List<String> paramNames = new LinkedList<String>();
		List<Object> paramValues = new LinkedList<Object>();
		paramNames.add("dailyUid"); paramValues.add(dailyUid);
		paramNames.add("timeIn"); paramValues.add(startDatetime);
		if (endDatetime!=null) {
			paramNames.add("timeOut"); paramValues.add(endDatetime);
		}
		String queryString = "FROM Bharun b, BhaComponent c, BharunDailySummary d " +
				"WHERE (d.isDeleted=false or d.isDeleted is null) " +
				"AND (c.isDeleted=false or c.isDeleted is null) " +
				"AND (b.isDeleted=false or b.isDeleted is null) " +
				"AND b.bharunUid=d.bharunUid " +
				"AND b.bharunUid=c.bharunUid " +
				"AND (b.timeIn>=:timeIn) " +
				(endDatetime!=null?"AND (b.timeOut<=:timeOut or b.timeOut is null) ":"") +
				"AND d.dailyUid=:dailyUid ";
		List<Object[]>list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, qp);
		for (Object[] rec : list) {
			BhaComponent component = (BhaComponent) rec[1];
			if (component.getWeight()!=null && component.getJointlength()!=null) bhaWeight += component.getWeight() * component.getJointlength();
		}
		//bhaWeight = bhaWeight / LBS; // lbs
		Double Wc = (bhaWeight * buoyancyFactor); 
		return Wc; //return in base value
	}
	
	public static Double calculateDrillPipeBuoyedWeight(String dailyUid, Date startDatetime, Date endDatetime, Double buoyancyFactor, QueryProperties qp) throws Exception {
		
		Double dpWeight = 0.0;
		
		List<String> paramNames = new LinkedList<String>();
		List<Object> paramValues = new LinkedList<Object>();
		paramNames.add("dailyUid"); paramValues.add(dailyUid);
		paramNames.add("startDatetime"); paramValues.add(startDatetime);
		if (endDatetime!=null) {
			paramNames.add("endDatetime"); paramValues.add(endDatetime);
		}
		String queryString = "FROM Bharun b, DrillPipeDaily d, DrillPipeTally t " +
				"WHERE (d.isDeleted=false or d.isDeleted is null) " +
				"AND (t.isDeleted=false or t.isDeleted is null) " +
				"AND (b.isDeleted=false or b.isDeleted is null) " +
				"AND b.bharunUid=d.bharunUid " +
				"AND (b.timeIn>=:startDatetime) " +
				(endDatetime!=null?"AND (b.timeOut<=:endDatetime or b.timeOut is null) ":"") +
				"AND t.drillPipeDailyUid=d.drillPipeDailyUid " +
				"AND d.dailyUid=:dailyUid ";
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, qp);
		for (Object[] rec : list) {
			DrillPipeTally tally = (DrillPipeTally) rec[2];
			if (tally.getWeight()!=null) dpWeight += tally.getWeight(); 
		}
		//dpWeight = dpWeight / LB_PER_FEET; //lb/ft
		
		Double Wp = (dpWeight) * buoyancyFactor;

		return Wp; //return in base value
	}
	
	
	public static void calculateWearTripSinceLastCut(String dailyUid, CustomFieldUom thisConverter) throws Exception {
		String queryString = "FROM TourWirelineDetail WHERE (isDeleted=false or isDeleted is null) and dailyUid=:dailyUid";
		List<TourWirelineDetail> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "dailyUid", dailyUid);
		for (TourWirelineDetail tourWirelineDetail:list) {
			DrillLineUtils.calculateWearTripSinceLastCut(tourWirelineDetail, thisConverter);
			DrillLineUtils.calculateAccumulatedWear(tourWirelineDetail.getTourWirelineUid());
		}
	}
	
	/**
	 * to calculate TourWirelineDetail.wearSinceLastCut for related TourWirelineDetail record
	 * @param session
	 * @param status
	 * @throws Exception
	 * @author Robin
	 */
	public static void calculateWearSinceLastCut(UserSession session, ChildClassNodesProcessStatus status, CommandBeanTreeNode parent) throws Exception {
		CustomFieldUom thisConverter = new CustomFieldUom(session.getUserLocale());
		Double prevWearSinceLastCut = null;
		Double wearSinceLastCut = null;
		Double cumWearSinceLastCut = 0.00;
		Date prevLastCutDate = null;
		Double prevPresentLength = null;
		
		DateFormat df = new SimpleDateFormat();
		String[] datePattern = {"yyyy-MM-dd"};
		
		Object parentData = parent.getData();
		if (parentData instanceof TourWireline){
			TourWireline tourWireline = (TourWireline) parentData;
			String tourWirelineUid = tourWireline.getTourWirelineUid();
			
			String queryString = "FROM TourWirelineDetail " +
					"WHERE (isDeleted=false or isDeleted is null) " +
					"AND tourWirelineUid=:tourWirelineUid " +
					"ORDER BY checkDateTime";
			String[] paramNames = {"tourWirelineUid"};
			Object[] paramValues = {tourWirelineUid};
			List<TourWirelineDetail> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues);
			
			for (TourWirelineDetail tourWirelineDetail : list) {
				Double dailyWear = null;
				if (tourWirelineDetail.getDailyWear()!=null) {
					thisConverter.setReferenceMappingField(TourWirelineDetail.class, "dailyWear");
					if (thisConverter.isUOMMappingAvailable()) {
						thisConverter.setBaseValueFromUserValue(tourWirelineDetail.getDailyWear());
						dailyWear = thisConverter.getBasevalue();
					} else {
						dailyWear = tourWirelineDetail.getDailyWear();
					}
				}
				
				Date checkDateTime = null;
				if (tourWirelineDetail.getCheckDateTime()!=null) {
					checkDateTime = tourWirelineDetail.getCheckDateTime();
				}
				
				Date lastCutDate = null;
				if (tourWirelineDetail.getLastCutDate()!=null) {
					lastCutDate = tourWirelineDetail.getLastCutDate();
				}
				
				if(lastCutDate!=null && prevLastCutDate!=null){
					if(!lastCutDate.equals(prevLastCutDate)){
						prevWearSinceLastCut = null;
						prevLastCutDate = null;
					}
				}else{
					prevLastCutDate = DateUtils.parseDate("1970-01-01", datePattern);
				}
				
				if(checkDateTime!=null && lastCutDate!=null){
					if(checkDateTime.after(lastCutDate) || checkDateTime.equals(lastCutDate)){
						if(prevWearSinceLastCut!=null && dailyWear!=null){
							wearSinceLastCut = dailyWear + prevWearSinceLastCut;
						}else{
							wearSinceLastCut = dailyWear;
						}
						
						prevLastCutDate = lastCutDate;
					}
				}else if(checkDateTime!=null && lastCutDate==null){
					if(prevWearSinceLastCut!=null && dailyWear!=null){
						wearSinceLastCut = dailyWear + prevWearSinceLastCut;
					}else{
						wearSinceLastCut = dailyWear;
					}
				}else{
					wearSinceLastCut = dailyWear;
				}
				
				prevWearSinceLastCut = wearSinceLastCut;
				
				if(dailyWear!=null)
					cumWearSinceLastCut += dailyWear;
				
				thisConverter.setReferenceMappingField(TourWirelineDetail.class, "wearSinceLastCut");
				if (wearSinceLastCut!=null && thisConverter.isUOMMappingAvailable()) {
					thisConverter.setBaseValue(wearSinceLastCut);
					tourWirelineDetail.setWearSinceLastCut(thisConverter.getConvertedValue());
				} else {
					tourWirelineDetail.setWearSinceLastCut(wearSinceLastCut);
				}
				
				thisConverter.setReferenceMappingField(TourWirelineDetail.class, "cumWearSinceLastCut");
				if (cumWearSinceLastCut!=null && thisConverter.isUOMMappingAvailable()) {
					thisConverter.setBaseValue(cumWearSinceLastCut);
					tourWirelineDetail.setAccumulativeDailyWear(thisConverter.getConvertedValue());
				} else {
					tourWirelineDetail.setAccumulativeDailyWear(cumWearSinceLastCut);
				}
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(tourWirelineDetail);
				
				prevPresentLength = calculateSlipAndCut(tourWirelineDetail, thisConverter, prevPresentLength);
			}
		}
	}
	
}
