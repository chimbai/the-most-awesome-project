package com.idsdatanet.d2.drillnet.report;

import java.util.Map;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;

public class FwrCustomInputReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
		// TODO Auto-generated method stub
		
		Map ad = userContext.getUserSelection().getCustomProperties();
	
		if(ad != null ){
			if(ad.containsKey("FrontCover_ReportHeader")){
				this.generateSimpleReportData(userContext, reportDataNode, (String) ad.get("FrontCover_ReportHeader"), "ReportHeader", "reportHeader");
			}
			if(ad.containsKey("FrontCover_ReportCompiler")){
				this.generateSimpleReportData(userContext, reportDataNode, (String) ad.get("FrontCover_ReportCompiler"), "ReportCompiler", "reportCompiler");
			}
			if(ad.containsKey("FrontCover_DateOfIssue")){
				this.generateSimpleReportData(userContext, reportDataNode, (String) ad.get("FrontCover_DateOfIssue"), "DateOfIssue", "dateOfIssue");
			}
		}
		
	}

	public void generateSimpleReportData(UserContext userContext, ReportDataNode reportDataNode, String value, String title, String field) throws Exception{
		ReportDataNode thisReportNode = reportDataNode.addChild(title);
		thisReportNode.addProperty(field, value.toString());
	}
	
	
}
