package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class SelectiveFieldLookupHandler implements LookupHandler{

	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		List<String> result = ApplicationUtils.getConfiguredInstance().getDaoManager().find("SELECT DISTINCT(field) from Well Where (isDeleted is null or isDeleted = false) and (field <> '') order by field");
		Map<String,LookupItem> lookupList = new LinkedHashMap<String,LookupItem>();
		if (result.size()>0)
		{
			for(String field:result)
				lookupList.put(field, new LookupItem(field,field));
		}
		return lookupList;
	}

}
