package com.idsdatanet.d2.drillnet.report.schedule.scheduledMail;

import com.idsdatanet.d2.drillnet.report.DefaultReportModule;
import com.idsdatanet.d2.drillnet.report.ReportAutoEmail;

public interface ReportService {
	public void setSendEmailAfterReportGeneration(boolean value);
	
	public void setEmailDistributionListKey(String value);
	
	public void setEmailBodyTemplateFile(String value);
	
	public void setEmailSubject(String value);
	
	public void setOpsTeamBased(boolean opsTeamBased) ;
	
	public ReportAutoEmail getReportAutoEmail();
	
	public void setDefaultReportModule(DefaultReportModule defaultReportModule);

	public DefaultReportModule getDefaultReportModule();
	
	public void run() throws Exception;
}
