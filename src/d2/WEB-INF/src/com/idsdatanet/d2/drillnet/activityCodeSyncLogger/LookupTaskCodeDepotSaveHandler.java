package com.idsdatanet.d2.drillnet.activityCodeSyncLogger;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;


public class LookupTaskCodeDepotSaveHandler extends EmptyDataNodeListener {
	
	private List<String> excludedOnUpdate = null;
	
	public void setExcludedOnUpdate(List<String> excludedOnUpdate){
		this.excludedOnUpdate = excludedOnUpdate;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
			
		// excluding fields
		if (excludedOnUpdate.size() > 0) {
			Map dataProperties = BeanUtils.describe(object);
			for (String field : excludedOnUpdate) {
				String fieldParts[] = field.split("[.]");
				if (fieldParts[0].equals(object.getClass().getSimpleName())) {
					if (dataProperties.containsKey(fieldParts[1])) {
						String id = BeanUtils.getProperty(object, ApplicationUtils.getConfiguredInstance().getIdentifierPropertyName(object.getClass()));
						Object bean = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(object.getClass(), id);
						if (bean != null) {
							//log.info("Field '" + fieldParts[1] + "' excluded...");
							Object value = PropertyUtils.getProperty(bean, fieldParts[1]);
							PropertyUtils.setProperty(object, fieldParts[1], value);
						}
					}
				}
			}
		}
	}
}