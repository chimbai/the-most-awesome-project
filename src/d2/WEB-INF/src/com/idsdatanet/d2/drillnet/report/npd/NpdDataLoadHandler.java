package com.idsdatanet.d2.drillnet.report.npd;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.config.gwp.SystemSettingsManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.depot.npd.OperationCodeConversion;
import com.idsdatanet.depot.npd.OperationMappingEntry;

public class NpdDataLoadHandler implements DataNodeLoadHandler {
	
	/**
	 * Returns a list of Activities with codes which do not have a match in the proprietary code mappings.
	 * 
	 * @param commandBean
	 * @param meta
	 * @param node
	 * @param userSelection
	 * @param pagination
	 * @param request
	 * @return possible object is {@link List<Object> }
	 * @throws Exception
	 * @author Jackey
	 */
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		
		String unmappedExists = "false";
		List<Object> unmappedActs = new ArrayList<Object>();
		
		// retrieve and iterate list of Activities of current DailyUid
		String dailyUid = userSelection.getDailyUid();
		List<Activity> allActs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam
			("from Activity where (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid AND ((dayPlus IS NULL OR dayPlus=0) AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL)) ORDER BY dayPlus, startDatetime", "dailyUid", dailyUid);
		
		for (Activity act : allActs) {
			OperationMappingEntry entry = OperationCodeConversion.getConfiguredInstance().getMapping(act);
			//gwp for Proprietary Code get from Activity screen or npdOperationMapping
			if("0".equals(SystemSettingsManager.getConfiguredInstance().getValue(act.getGroupUid(), "useNpdFromActivity")))
			{
				if (entry != null && entry.getMainOp() != null && entry.getSubOp() != null){
					// the codes for this Activity have a valid mapping - do nothing
				} else {
					// no mapping found - add Activity to unmapped list
					unmappedActs.add(act);
					unmappedExists = "true";
				}
			}
			
		}
		
		// indicate to front-end if any unmapped Activity exists, so it knows to display Activity section or not
		node.getDynaAttr().put("unmappedActivityExists", unmappedExists);
		
		if ("true".equals(unmappedExists)) {
			
			// display sys message only if there are no others (eg. if there are identical Activity code conflicts)
			if (commandBean.getSystemMessage().getTotalMessages() == 0) {
				
				commandBean.getSystemMessage().addInfo
				(	"There are Activity codes which have no valid proprietary code mapping. " +
					"Please make the correct Main Op / Sub Op selections for each Activity...\n\n" +
					"When successfully submitted, the data will only be temporarily stored in the system. " +
					"An email will automatically be sent to the relevant parties for verification " +
					"before the mapping(s) are added on permanent basis.", true);
			}
			return unmappedActs;
		}
		else return null;
	}

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}
}