package com.idsdatanet.d2.drillnet.report;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class dgrReportDataNodeAllowedAction implements DataNodeAllowedAction {
	
	public boolean allowedAction(CommandBeanTreeNode node, UserSession session, String targetClass, String action, HttpServletRequest request) throws Exception {
		String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid =:thisDailyUid AND reportType='DGR'";
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisDailyUid", session.getCurrentDailyUid());
		
		if (lstResult.size() > 0){
			return true;
		}else{
			return false;
		}
	}

}
