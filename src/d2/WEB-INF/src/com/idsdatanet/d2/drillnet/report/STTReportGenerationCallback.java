package com.idsdatanet.d2.drillnet.report;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.stt.STTTownSideCallback;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class STTReportGenerationCallback implements STTTownSideCallback {
	
	private ReportManager reportManager = null;
	private String emailListKey = null;
	private String emailSubject = null;
	private String emailBodyTemplateFile = null;
	private boolean sendEmailAfterReportGeneration = true;
	private boolean opsTeamBased = false;
	
	public void setSendEmailAfterReportGeneration(boolean value) {
		this.sendEmailAfterReportGeneration = value;
	}
	
	public void setReportManager(ReportManager value) {
		this.reportManager = value;
	}
	
	public void setEmailDistributionListKey(String value) {
		this.emailListKey = value;
	}
	
	public void setEmailBodyTemplateFile(String value) {
		this.emailBodyTemplateFile = value;
	}
	
	public void setEmailSubject(String value) {
		this.emailSubject = value;
	}
	
	public void setOpsTeamBased(boolean opsTeamBased) {
		this.opsTeamBased = opsTeamBased;
	}

	public void afterSendToTownCompleted(UserSelectionSnapshot userSelection) {
		String dailyUid = userSelection.getDailyUid();
		if (StringUtils.isBlank(dailyUid)) {
			return; // fail silently
		}
		
		ReportAutoEmail autoEmail = new ReportAutoEmail();
		autoEmail.setEmailListKey(this.emailListKey);
		autoEmail.setEmailSubject(this.emailSubject);
		autoEmail.setEmailContentTemplateFile(this.emailBodyTemplateFile);
		autoEmail.setSendEmailAfterReportGeneration(this.sendEmailAfterReportGeneration);
		autoEmail.setOpsTeamBased(this.opsTeamBased);
		
		try {
			reportManager.generateDDRByDailyUid(dailyUid, autoEmail);
		} catch (Exception e) {
			Log logger = LogFactory.getLog(this.getClass());
			logger.error(e.getMessage(), e);
		}
	}

}
