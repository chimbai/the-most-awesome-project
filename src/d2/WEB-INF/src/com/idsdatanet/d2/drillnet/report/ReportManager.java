package com.idsdatanet.d2.drillnet.report;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;

public class ReportManager extends DefaultBeanSupport {
	private DDRReportModule ddrReportModule = null;
	private ManagementReportModule mgtReportModule = null;
	private DefaultReportModule defaultReportModule = null;
	private String generateReportUserName = "idsadmin";
	
	public static ReportManager getConfiguredInstance() throws Exception {
		return getSingletonInstance(ReportManager.class);
	}

	public void setGenerateReportUserName(String value){
		this.generateReportUserName = value;
	}

	public void setManagementReportModule(ManagementReportModule value){
		this.mgtReportModule = value;
	}
	
	public void setDDRReportModule(DDRReportModule value){
		this.ddrReportModule = value;
	}
			
	public void generateDDROnCurrentSystemDatetime() throws Exception {
		this.generateDDROnCurrentSystemDatetime(null);
	}

	public void generateDDROnCurrentSystemDatetime(ReportAutoEmail reportAutoEmail) throws Exception {
		this.ddrReportModule.generateDDROnCurrentSystemDatetime(this.generateReportUserName, reportAutoEmail);
	}
	
	public void generateManagementReportOnCurrentSystemDatetime() throws Exception {
		this.generateManagementReportOnCurrentSystemDatetime(null);
	}

	public void generateManagementReportOnCurrentSystemDatetime(ReportAutoEmail reportAutoEmail) throws Exception {
		this.mgtReportModule.generateManagementReportOnCurrentSystemDatetime(this.generateReportUserName, reportAutoEmail);
	}
	
	public void generateDefaultReportOnCurrentSystemDatetime() throws Exception {
		this.generateDefaultReportOnCurrentSystemDatetime(null, null, null);
	}

	public void generateDefaultReportOnCurrentSystemDatetime(ReportAutoEmail reportAutoEmail, String defaultReportType, DefaultReportModule defaultReportModule) throws Exception {
		if(defaultReportModule != null)
		{
			this.defaultReportModule = defaultReportModule;
		}
		
		this.defaultReportModule.generateDefaultReportOnCurrentSystemDatetime(this.generateReportUserName, reportAutoEmail, defaultReportType);
	}

	public void generateDefaultReportOnCurrentSystemDatetimeByDepotWellOperation(ReportAutoEmail reportAutoEmail, String defaultReportType, DefaultReportModule defaultReportModule, Daily daily) throws Exception {
		if(defaultReportModule != null)
		{
			this.defaultReportModule = defaultReportModule;
		}
		
		this.defaultReportModule.generateDefaultReportOnCurrentSystemDatetimeByDepotWellOperation(this.generateReportUserName, reportAutoEmail, defaultReportType, daily);
	}
	
	public void generateDDRByDailyUid(String dailyUid, ReportAutoEmail reportAutoEmail) throws Exception {
		this.ddrReportModule.generateDDRByDailyUid(dailyUid, this.generateReportUserName, reportAutoEmail);
	}
}
