package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * 
 * @hkling
 * This class help to mine data for latest 7 days' 24 hr summary
 * for daily management reporting use
 * 
 */

public class LatestDaysReportPeriodSummaryReportDataGenerator implements ReportDataGenerator {
	private Integer defaultDayNo = null;
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}
	
	public void generateData(UserContext userContext,ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		//get current day date
		String currentDailyUid = userContext.getUserSelection().getDailyUid();
		Daily currentDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(currentDailyUid);
		if (currentDaily==null) return;
		Date todayDate = currentDaily.getDayDate();
		
		//get operation uid
		String operationUid = userContext.getUserSelection().getOperationUid();	
		String currentOperationReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);				
		String sql = "SELECT d.dayDate, rd.reportPeriodSummary FROM ReportDaily rd, Daily d " +
					 "WHERE (d.isDeleted IS NULL OR d.isDeleted IS FALSE) " +
					 "AND (rd.isDeleted IS NULL OR rd.isDeleted IS FALSE) " +
					 "AND rd.operationUid = :thisOpsUid " +
					 "AND rd.dailyUid = d.dailyUid " +
					 "AND d.dayDate <= :userDate " +
					 "AND (rd.reportType = :thisReportType) "+
					 "AND (rd.reportPeriodSummary <> '' AND rd.reportPeriodSummary IS NOT NULL) " +
					 "ORDER BY d.dayDate DESC";
		
		String[] paramsFields = {"thisOpsUid","userDate", "thisReportType"};
		Object[] paramsValues = {operationUid, todayDate, currentOperationReportType};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		
		if (!lstResult.isEmpty()){
			
			// Collect expected number of results
			List objList = new ArrayList();
			int count = 0;
			for(Object objResult : lstResult){
				if(count < this.getDefaultDayNo()){
					objList.add(objResult);
					count++;
				}
				else{
					break;
				}
			}
			
			Collections.sort(objList, new ResultComparator());
			
			for (Object objResult : objList){
				Object[] obj = (Object[]) objResult;
				
				ReportDataNode thisReportNode = reportDataNode.addChild("report");
				thisReportNode.addProperty("reportDate",obj[0].toString());
				thisReportNode.addProperty("reportPeriodSummary",obj[1].toString());
			}	
		}
	}
	
	public void setDefaultDayNo(Integer defaultDayNo) {
		this.defaultDayNo = defaultDayNo;
	}

	public Integer getDefaultDayNo() {
		return defaultDayNo;
	}

	private class ResultComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Object in1 = o1[0];
				Object in2 = o2[0];
				
				Date f1 = (Date) in1;
				Date f2 = (Date) in2;
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				//e.printStackTrace();
				return 0;
			}
		}
	}
}
		