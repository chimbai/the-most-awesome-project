package com.idsdatanet.d2.drillnet.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.graph.BaseGraph;
import com.idsdatanet.d2.core.graph.DualXGraph;
import com.idsdatanet.d2.core.graph.X2YSeries;
import com.idsdatanet.d2.core.graph.X2YSeriesCollection;
import com.idsdatanet.d2.core.model.LeakOffTest;
import com.idsdatanet.d2.core.model.LeakOffTestDetail;
import com.idsdatanet.d2.core.model.LeakOffTestPumpOffDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class leakofftest extends BaseGraph {
	
	private String leakOffTestUid = null;
	private NumberFormat formatter = new DecimalFormat("#,##0.0");
	private String graphTitle=null;
	private String groupUid = null;
	
	public void setLeakOffTestUid(String value) {
		leakOffTestUid = value;
	}
	
	public String getLeakOffTestUid() {
		return leakOffTestUid;
	}
	
	public void setGroupUid(String value) {
		groupUid = value;
	}
	
	public String getGroupUid() {
		return groupUid;
	}
	
	public Object paint() throws Exception {
		//String groupUid = null;
		Locale locale = null;
		
		if (this.getCurrentHttpRequest()!=null) {
			HttpServletRequest request = this.getCurrentHttpRequest();
			UserSession session = UserSession.getInstance(request);
			groupUid = session.getCurrentGroupUid();
			locale = session.getUserLocale();
			this.formatter = (DecimalFormat) DecimalFormat.getInstance(locale);
			this.formatter.setMinimumFractionDigits(1);
			this.formatter.setMaximumFractionDigits(1);
			this.leakOffTestUid = CommonUtils.null2EmptyString(request.getParameter("uid"));
			if ("".equals(this.leakOffTestUid)) {
				Object uid = this.getCurrentUserContext().getUserSelection().getCustomProperty("leakOffTestUid");
				if (uid!=null) this.leakOffTestUid = (String) uid;
			}
		} else {
			groupUid = this.getCurrentUserContext().getUserSelection().getGroupUid();
			locale = this.getCurrentUserContext().getUserSelection().getLocale();
			Object uid = (String) this.getCurrentUserContext().getUserSelection().getCustomProperty("leakOffTestUid");
			if (uid!=null) this.leakOffTestUid = (String) uid;
			
		}
		
		
		this.setFileName("LOT_"+leakOffTestUid+".jpg");
		
		
		CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale());
		thisConverter.setReferenceMappingField(LeakOffTestDetail.class, "mudVolumePump");
		String volumePumped = "Volume Pumped (" + thisConverter.getUomSymbol() + ")";
		thisConverter.setReferenceMappingField(LeakOffTestPumpOffDetail.class, "timeDuration");
		String time = "Time (" + thisConverter.getUomSymbol() + ")";
		thisConverter.setReferenceMappingField(LeakOffTestDetail.class, "recordedPressure");
		String pressure = "Pressure (" + thisConverter.getUomSymbol() + ")";
		String operationName = "";

		LeakOffTest leakOffTest = (LeakOffTest) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LeakOffTest.class, this.leakOffTestUid);
		if (leakOffTest==null) {
			return this.emptyChart(null, "", volumePumped, pressure); 
		} else {
			operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, leakOffTest.getOperationUid());
			
			String queryString = "FROM LeakOffTestDetail WHERE (isDeleted=false or isDeleted is null) and leakOffTestUid=:leakOffTestUid";
			List<LeakOffTestDetail> lotDetail = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "leakOffTestUid", this.leakOffTestUid);
			
			queryString = "FROM LeakOffTestPumpOffDetail WHERE (isDeleted=false or isDeleted is null) and leakOffTestUid=:leakOffTestUid";
			List<LeakOffTestPumpOffDetail> pumpOff = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "leakOffTestUid", this.leakOffTestUid);
			if (pumpOff.size()>0 && lotDetail.size()>0) {
				//2 axis-x
				X2YSeriesCollection x2y=this.collectX2Dataset();
				DualXGraph g =  DualXGraph.createDualXGraph(this.getPath(), graphTitle, operationName, volumePumped, time, pressure, false, x2y);
				
				g.setHeight(this.getHeight());
				g.setWidth(this.getWidth());
				g.setFormatX1(formatter);
				g.setFormatX2(formatter);
				g.setFormatY(formatter);				
				g.Paint();
				g.getOutputPath(this.getFileName());
				
				return null;
			} else {
				
				return this.emptyChart(this.collectDataset(), operationName, volumePumped, pressure);
			}
			
		}
		
		
	}
	
	public String getGraphTitle() {
		return graphTitle;
	}

	public void setGraphTitle(String graphTitle) {
		this.graphTitle = graphTitle;
	}

	private JFreeChart emptyChart(XYSeriesCollection dataset, String header, String xLabel, String yLabel) throws Exception {
		if (dataset==null) {
			dataset = new XYSeriesCollection();
			XYSeries series = new XYSeries("");
			series.add(0, 0);
			dataset.addSeries(series);
		}
		
		Boolean lotGraphWithCasingTest = "1".equals(GroupWidePreference.getValue(groupUid, "lotGraphWithCasingTest"));
		NumberAxis xAxis = new NumberAxis(xLabel);
		xAxis.setAutoRangeIncludesZero(false);
		NumberAxis yAxis = new NumberAxis(yLabel);
		yAxis.setAutoRangeIncludesZero(false);
		XYItemRenderer renderer = new StandardXYItemRenderer();
		renderer.setSeriesStroke(0, stroke);		
		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
		JFreeChart chart = new JFreeChart(graphTitle, JFreeChart.DEFAULT_TITLE_FONT, plot, lotGraphWithCasingTest);
		chart.addSubtitle(new TextTitle(header));
		chart.setBackgroundPaint(Color.lightGray);
		chart.getXYPlot().getRenderer().setSeriesPaint(0, Color.BLUE);
        
        if(lotGraphWithCasingTest)
        {
        	XYDataset casingPressureTestDataset = (XYDataset) this.casingPressureTestLine();
	        plot.setDataset(1, casingPressureTestDataset);
	        XYItemRenderer casingPressureTestRenderer = new StandardXYItemRenderer();
	        casingPressureTestRenderer.setSeriesPaint(0, Color.BLACK); //Planned/P10 Curve -- Dashes -- #9999cc
	        plot.setRenderer(1, casingPressureTestRenderer);
	        
        }
        
        ((NumberAxis) chart.getXYPlot().getRangeAxis()).setNumberFormatOverride(formatter);
        ((NumberAxis) chart.getXYPlot().getDomainAxis()).setNumberFormatOverride(formatter);

        return chart;
	}
	
	// dashed line
	private BasicStroke stroke = new java.awt.BasicStroke(1f,
		        BasicStroke.CAP_SQUARE,
		        BasicStroke.JOIN_MITER,
		        10.0f,
		        new float[] {2f, 3f, 2f, 3f},
		        0f
		    );
	
	private XYSeriesCollection collectDataset() throws Exception {
		XYSeriesCollection dataset = new XYSeriesCollection();
		String queryString = "FROM LeakOffTestDetail WHERE (isDeleted=false or isDeleted is null) and leakOffTestUid=:leakOffTestUid order by mudVolumePump";
		String testTypeQueryString = "SELECT testType FROM LeakOffTest WHERE leakOffTestUid=:leakOffTestUid";
		List<String> testType = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(testTypeQueryString,"leakOffTestUid", this.leakOffTestUid);
		List<LeakOffTestDetail> details = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "leakOffTestUid", this.leakOffTestUid);
		LeakOffTestDetail prev=null;
		String title="Graph";
		String testTypeTitle = testType.get(0);
		if (testType != null && testType.size() > 0)
		{
			if (testTypeTitle != null)
			{
				if (testTypeTitle.equals("LOT"))
				{
					title="Leak Off Test";
				}
				else if (testTypeTitle.equals("FIT"))
				{
					title="Formation Integrity Test";
				}
				else if (StringUtils.isBlank(testTypeTitle))
				{
					title="Graph";
				}
			}
			else
			{
				title="Graph";
			}
		}
		/*for (LeakOffTestDetail rec : details) {
			if (rec.getMudVolumePump()!=null && rec.getRecordedPressure()!=null) {
				if (prev!=null) {
					if (prev.getRecordedPressure()>rec.getRecordedPressure()) {
						title="Leak Off Test";
					}
				}
				prev=rec;
			}
		}*/
		XYSeries series=new XYSeries(title);
		for (LeakOffTestDetail rec : details) {
			if (rec.getMudVolumePump()!=null && rec.getRecordedPressure()!=null) {
				series.add(rec.getMudVolumePump(), rec.getRecordedPressure());
				prev=rec;
			}
		}
		dataset.addSeries(series);
		this.graphTitle=this.graphTitle==null?title:graphTitle;
		return dataset;
	}

	private X2YSeriesCollection collectX2Dataset() throws Exception {
		X2YSeriesCollection dataset=new X2YSeriesCollection();
		double lastPressure = 0.0;
		String queryString = "FROM LeakOffTestDetail WHERE (isDeleted=false or isDeleted is null) and leakOffTestUid=:leakOffTestUid order by mudVolumePump";
		String testTypeQueryString = "SELECT testType FROM LeakOffTest WHERE leakOffTestUid=:leakOffTestUid";
		List<String> testType = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(testTypeQueryString,"leakOffTestUid", this.leakOffTestUid);
		List<LeakOffTestDetail> details = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "leakOffTestUid", this.leakOffTestUid);
		LeakOffTestDetail prev=null;
		String title="Graph";
		/*for (LeakOffTestDetail rec : details) {
			if (rec.getMudVolumePump()!=null && rec.getRecordedPressure()!=null) {
				if (prev!=null) {
					if (prev.getRecordedPressure()>rec.getRecordedPressure()) {
						title="Leak Off Test";
						break;
					}
				}
				prev=rec;
			}
		}*/
		String testTypeTitle = testType.get(0);
		if (testType != null && testType.size() > 0)
		{
			if (testTypeTitle != null)
			{
				if (testTypeTitle.equals("LOT"))
				{
					title="Leak Off Test";
				}
				else if (testTypeTitle.equals("FIT"))
				{
					title="Formation Integrity Test";
				}
				else if (StringUtils.isBlank(testTypeTitle))
				{
					title="Graph";
				}
			}
			else
			{
				title="Graph";
			}
		}
		X2YSeries series=new X2YSeries(title);
		if (details.size()>0) {
			if (((LeakOffTestDetail)details.get(0)).getMudVolumePump()!=0.00)
				series.addXY(0.0, 0.0);
		}
		for (LeakOffTestDetail rec : details) {
			if (rec.getMudVolumePump()!=null && rec.getRecordedPressure()!=null) {
				double volume = rec.getMudVolumePump();
				double pressure = rec.getRecordedPressure();
				series.addXY(volume, pressure);
				lastPressure = pressure;
				prev=rec;
			}
		}
		queryString = "FROM LeakOffTestPumpOffDetail WHERE (isDeleted=false or isDeleted is null) and leakOffTestUid=:leakOffTestUid order by timeDuration";
		List<LeakOffTestPumpOffDetail> pumpOff = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "leakOffTestUid", this.leakOffTestUid);
		if (pumpOff.size()>0)
		{
			if (((LeakOffTestPumpOffDetail)pumpOff.get(0)).getTimeDuration()!=0.00)
				series.addX2Y(0.0, lastPressure);
		}
		for (LeakOffTestPumpOffDetail rec : pumpOff) {
			if (rec.getTimeDuration()!=null && rec.getRecordedPressure()!=null) {
				double time = rec.getTimeDuration();
				double pressure = rec.getRecordedPressure();
				series.addX2Y(time, pressure);
			}
		}
		this.graphTitle=this.graphTitle==null?title:graphTitle;		
		dataset.addSeries(series);
		return dataset;
	}
	
	private XYSeriesCollection casingPressureTestLine() throws Exception {
		
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries casingPressureTestSeries=new XYSeries("Casing Test");
		String sql = "FROM LeakOffTest WHERE (isDeleted=false or isDeleted is null) and leakOffTestUid=:leakOffTestUid";
		List lotList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "leakOffTestUid", this.leakOffTestUid);
		if(lotList.size() > 0) {
			LeakOffTest thisLot = (LeakOffTest) lotList.get(0);
			if (thisLot.getMaxCasingVolPumped() != null && thisLot.getCasingPressure() != null) {
				casingPressureTestSeries.add(0.0, 0.0);
				
				casingPressureTestSeries.add(thisLot.getMaxCasingVolPumped(), thisLot.getCasingPressure());
			}
		}
		dataset.addSeries(casingPressureTestSeries);
		return dataset;
		
	}
}
