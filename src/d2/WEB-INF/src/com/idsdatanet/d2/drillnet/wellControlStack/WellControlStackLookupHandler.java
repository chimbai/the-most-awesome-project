package com.idsdatanet.d2.drillnet.wellControlStack;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class WellControlStackLookupHandler implements LookupHandler{	
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();		
		if(request != null) {
			UserSession session = UserSession.getInstance(request);
			
			if(session.getCurrentWell().getCountry().equalsIgnoreCase("AT")) {
			    String sql ="FROM CommonLookup where  (isDeleted = false or isDeleted is null) and lookupTypeSelection = 'wellControl.wellControlType.AT' order by sequence";
				List<CommonLookup> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
				if (list.size() > 0) { 
					for (CommonLookup lookuplist : list) {
						LookupItem newItem = new LookupItem(lookuplist.getShortCode(),lookuplist.getLookupLabel());
						result.put(lookuplist.getShortCode(), newItem);
					}
				}
			}
			else {
				String sql ="FROM CommonLookup where  (isDeleted = false or isDeleted is null) and lookupTypeSelection = 'wellControl.wellControlType' order by sequence";
				List<CommonLookup> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
				if (list.size() > 0) { 
					for (CommonLookup lookuplist : list) {
						LookupItem newItem = new LookupItem(lookuplist.getShortCode(),lookuplist.getLookupLabel());
						result.put(lookuplist.getShortCode(), newItem);
					}
				}
			}
		}
		return result;
	}
	
}
