package com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.dailyReport;

import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.drillnet.report.DefaultReportModule;
import com.idsdatanet.d2.drillnet.report.ReportManager;

public class DefaultReportService extends AbstractReportService{
	private ReportManager reportManager = null;
	private String defaultReportType = null;
	private DefaultReportModule defaultReportModule = null;
	private Boolean reportByDepotWellOperation = false;
	
	synchronized public void run() throws Exception
	{
		if(reportByDepotWellOperation) {
			
			List<Daily> dailies = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(new Date());
			
			for (Daily daily : dailies)
			{
				this.reportManager.generateDefaultReportOnCurrentSystemDatetimeByDepotWellOperation(this.getReportAutoEmail(), defaultReportType, defaultReportModule, daily);
			}
			
		} else {
			this.reportManager.generateDefaultReportOnCurrentSystemDatetime(this.getReportAutoEmail(), this.defaultReportType, this.defaultReportModule);
		}
	}
	
	public void setReportManager(ReportManager value){
		this.reportManager = value;
	}
	
	public void setDefaultReportType(String value){
		this.defaultReportType = value;
	}
	
	public void setDefaultReportModule(DefaultReportModule value){
		this.defaultReportModule = value;
	}
	
	public void setReportByDepotWellOperation(Boolean reportByDepotWellOperation) {
		this.reportByDepotWellOperation = reportByDepotWellOperation;
	}
}
