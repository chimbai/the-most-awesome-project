package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.CostAfeMasterVault;
import com.idsdatanet.d2.core.model.OperationPlanMasterVault;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class SantosDvdPlanListingDataNodeListener extends EmptyDataNodeListener {

	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof OperationPlanMasterVault) {
			OperationPlanMasterVault dvdplan = (OperationPlanMasterVault) node.getData();
			String queryString = "FROM CostAfeMasterVault WHERE (isDeleted=false or isDeleted is null) and operationPlanMasterVaultUid=:operationPlanMasterVaultUid";
			List<CostAfeMasterVault> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationPlanMasterVaultUid", dvdplan.getOperationPlanMasterVaultUid());
			String vaultAfe = "";
			for (CostAfeMasterVault data : list) {
				vaultAfe += ("".equals(vaultAfe)?"":",") + data.getCostAfeMasterVaultUid() + "::" + data.getAfeNumber();
			}
			node.getDynaAttr().put("vaultAfe", vaultAfe);
		}
	}

}
