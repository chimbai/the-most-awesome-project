package com.idsdatanet.d2.drillnet.mudVolume;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.HoleVolumes;
import com.idsdatanet.d2.core.model.MudVolumeDetails;
import com.idsdatanet.d2.core.model.MudVolumes;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;



public class MudVolumeDataNodeListener extends EmptyDataNodeListener {
	
	private List<String> excludeFromSegmentVolumeCalculation;
	private Boolean recalculateLoses = false;
	
	//18115
	private Boolean calcumlosses = false;
	
	public void setcalcumlosses(Boolean calcumlosses){
		this.calcumlosses = calcumlosses;
	}
		
	public void setExcludeFromSegmentVolumeCalculation(List<String> serviceCode)
	{
		if(serviceCode != null){
			this.excludeFromSegmentVolumeCalculation = new ArrayList<String>();
			for(String value: serviceCode){
				this.excludeFromSegmentVolumeCalculation.add(value.trim().toUpperCase());
			}
		}
	}
	
	private HashMap<String, String> getExcludeFromSegmentVolumeCalculation() {
		HashMap<String, String> thisMap = new HashMap<String, String>();
		
		for(String value: this.excludeFromSegmentVolumeCalculation)
		{
			thisMap.put(value.toLowerCase(), value.toLowerCase());
		}
		
		return thisMap;
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		String mudPropertiesUid = null;
		if (request !=null){
			mudPropertiesUid = request.getParameter("mudPropertiesUid");
			
		}
		
		if (obj instanceof MudVolumes) {
			MudVolumes thisMudVolumes = (MudVolumes) obj;
			
			if(meta.getTableClass() == MudVolumes.class && StringUtils.isNotBlank(mudPropertiesUid)){
				thisMudVolumes.setMudPropertiesUid(mudPropertiesUid);
				commandBean.getRoot().addCustomNewChildNodeForInput(thisMudVolumes);
			}
		}
	}
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		 
		if (object instanceof MudVolumes) {
			// get the current dailyUid and operationUid from the session
			 MudVolumes thisMudVol = (MudVolumes) object;
			 String dailyUid = thisMudVol.getDailyUid();
			 
			// MudVolumeUtil.getRelatedInfo(node, userSelection, request);
			//String dailyUid = userSelection.getDailyUid();
			
			if (StringUtils.isNotBlank(dailyUid)) {
				String operationUid = userSelection.getOperationUid();
				String wellUid = userSelection.getWellUid();
				
				//sql for totalVolume
				Double totalVolume = MudVolumeUtil.calculateTotalVolumeFor(operationUid, dailyUid, thisMudVol.getMudVolumesUid(), "vol");
				//sql for totalLosses 
				Double totalLosses = MudVolumeUtil.calculateTotalVolumeFor(operationUid, dailyUid, thisMudVol.getMudVolumesUid(), "loss");
				//sql for totalLosses crescent
				Double totalLosses1 = getLosses(operationUid, dailyUid, thisMudVol.getMudVolumesUid(), "loss","'loss_dh','loss_surface','loss_shaker','loss_dumped','loss_centrifuge'");
				//sql for surfaceLosses crescent
				Double surfaceLosses = getLosses(operationUid, dailyUid, thisMudVol.getMudVolumesUid(), "loss","'loss_surface','loss_shaker','loss_dumped','loss_centrifuge'");
				//sql for dhLosses crescent
				Double dhLosses = getLosses(operationUid, dailyUid, thisMudVol.getMudVolumesUid(), "loss","'loss_dh'");
				//sql for totalGains		
				Double totalGains = MudVolumeUtil.calculateTotalVolumeFor(operationUid, dailyUid, thisMudVol.getMudVolumesUid(), "gain");
				//sql for totalAddition for d2 osa
				Double totalAdditions = MudVolumeUtil.calculateTotalVolumeFor(operationUid, dailyUid, thisMudVol.getMudVolumesUid(), "addition");
				//sql for totalMudLostToHole - for cnrl
				Double totalMudLostToHole = MudVolumeUtil.calculateTotalVolumeFor(operationUid, dailyUid, thisMudVol.getMudVolumesUid(), "loss", "loss_dh");
				//sql for totalMudLostSurface - for cnrl
				Double totalMudLostToSurface = MudVolumeUtil.calculateTotalVolumeFor(operationUid, dailyUid, thisMudVol.getMudVolumesUid(), "loss", "loss_surface");
				
				
				
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, MudVolumeDetails.class, "volume");
				
				this.setDynAttr(node, thisConverter, totalVolume, "totalVolume");
				this.setDynAttr(node, thisConverter, totalLosses, "totalLosses");
				this.setDynAttr(node, thisConverter, totalLosses1, "totalLosses1");
				this.setDynAttr(node, thisConverter, surfaceLosses, "surfaceLosses");
				this.setDynAttr(node, thisConverter, dhLosses, "dhLosses");
				this.setDynAttr(node, thisConverter, totalGains, "totalGains");
				this.setDynAttr(node, thisConverter, totalAdditions, "totalAdditions");
				this.setDynAttr(node, thisConverter, totalMudLostToHole, "totalMudLostToHole");
				this.setDynAttr(node, thisConverter, totalMudLostToSurface, "totalMudLostToSurface");
				
				//cumulative losses volumes	for d2 wko		
				Double cumulativePumped = MudVolumeUtil.calculateCumulativeLossesVolumeFor(operationUid, dailyUid, "loss", "loss_surface");
				Double cumulativeDumped = MudVolumeUtil.calculateCumulativeLossesVolumeFor(operationUid, dailyUid, "loss", "loss_dumped");
				Double cumulativeDownholeLosses = MudVolumeUtil.calculateCumulativeLossesVolumeFor(operationUid, dailyUid, "loss", "loss_dh");
				Double cumulativeOther = MudVolumeUtil.calculateCumulativeLossesVolumeFor(operationUid, dailyUid, "loss", "loss_other");		
				this.setDynAttr(node, thisConverter, cumulativePumped, "cumulativePumped");
				this.setDynAttr(node, thisConverter, cumulativeDumped, "cumulativeDumped");
				this.setDynAttr(node, thisConverter, cumulativeDownholeLosses, "cumulativeDownholeLosses");
				this.setDynAttr(node, thisConverter, cumulativeOther, "cumulativeOther");
				
				//18115
				if(calcumlosses) {
					Double cumulativeLosses = MudVolumeUtil.calculateCumulativeLossesVolume(operationUid, dailyUid, "loss", thisMudVol);
					this.setDynAttr(node, thisConverter, cumulativeLosses, "cumulativeLosses");
				}
				
				
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
				if (daily == null) return;
				Date todayDate = daily.getDayDate();
				Double cummSurface = getCummMudLost(operationUid,todayDate,thisConverter,"'shakers','loss_cleaner','loss_pits','loss_tripping','evaporation','loss_centrifuge','dewatering','spill'");
				node.getDynaAttr().put("cumulativeSurface", cummSurface);
				
				//cumulative losses in well for crescent
				Double cummSurfaceLoss = getCumMudLost(wellUid,todayDate,thisConverter,"'loss_surface','loss_shaker','loss_dumped','loss_centrifuge'");
				node.getDynaAttr().put("cummSurfaceLoss", cummSurfaceLoss);
				Double cummDhLoss = getCumMudLost(wellUid,todayDate,thisConverter,"'loss_dh'");
				node.getDynaAttr().put("cummDhLoss", cummDhLoss);

			}
		}
	}
	
	private void setDynAttr(CommandBeanTreeNode node, CustomFieldUom thisConverter, double baseValue, String dynAttrName) throws Exception
	{
		thisConverter.setBaseValue(baseValue);
		if (thisConverter.isUOMMappingAvailable()) {
			node.setCustomUOM("@"+dynAttrName, thisConverter.getUOMMapping());
		}
		node.getDynaAttr().put(dynAttrName, thisConverter.getConvertedValue());
	}
	

	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof MudVolumes) {
			if(StringUtils.isNotBlank(UserSession.getInstance(request).getCurrentRigInformationUid())){
				//auto set the rigUid form current rig
				MudVolumes thisMudVolumes = (MudVolumes) object;
				thisMudVolumes.setRigInformationUid(UserSession.getInstance(request).getCurrentRigInformationUid());
			}
		}
		
		else if(object instanceof MudVolumeDetails) {			
			MudVolumeDetails thisMudVolumeDetails = (MudVolumeDetails) object;
			if ("loss".equals(thisMudVolumeDetails.getType()))
				recalculateLoses=true;
		}
		
		else if (object instanceof HoleVolumes)
		{
			HoleVolumes holeVolumes = (HoleVolumes) object;
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, HoleVolumes.class, "segmentLength");
			
			Double dblSegmentLength = 0.0;
			if(holeVolumes.getSegmentLength() != null)
			{
				thisConverter.setBaseValueFromUserValue(holeVolumes.getSegmentLength(), false);
				dblSegmentLength = thisConverter.getBasevalue();
			}	
			
			Double dblSegmentId = 0.0;
			if(holeVolumes.getSegmentId() != null)
			{
				thisConverter.setReferenceMappingField(HoleVolumes.class, "segmentId");
				thisConverter.setBaseValueFromUserValue(holeVolumes.getSegmentId(), false);
				dblSegmentId = thisConverter.getBasevalue();
			}
				
			//CALCULATE SEGMENT CAPACITY
			Double dblSegmentCapacityLength = Math.PI * (dblSegmentId / 2) * (dblSegmentId / 2);
			thisConverter.setReferenceMappingField(HoleVolumes.class, "segmentCapacityLength");
			thisConverter.setBaseValue(dblSegmentCapacityLength);
			//holeVolumes.setSegmentCapacityLength(thisConverter.getConvertedValue());		
			
			//CALCULATE SEGMENT VOLUME
			HashMap<String, String> thisExcludeFromSegmentVolumeCalc = this.getExcludeFromSegmentVolumeCalculation();
					
			if(thisExcludeFromSegmentVolumeCalc.get(holeVolumes.getSegmentDescription().toLowerCase()) == null)
			{
				Double dblSegmentVolume = Math.PI * (dblSegmentId / 2) * (dblSegmentId / 2) * dblSegmentLength;
				thisConverter.setReferenceMappingField(HoleVolumes.class, "segmentVolume");
				thisConverter.setBaseValue(dblSegmentVolume);
				//holeVolumes.setSegmentVolume(thisConverter.getConvertedValue());		
			}
			else
			{
				//holeVolumes.setSegmentVolume(0.0);
			}
		}
	}
	public void afterChildClassNodesProcessed(String className, ChildClassNodesProcessStatus status, UserSession session, CommandBeanTreeNode parent) throws Exception{
		if ("MudVolumeDetails".equalsIgnoreCase(className) && status.isAnyNodeSavedOrDeleted())
		{
			String operationUid = session.getCurrentOperationUid();
			String dailyUid = session.getCurrentDailyUid();
			if(StringUtils.isBlank(dailyUid)){
				dailyUid = (String) PropertyUtils.getProperty(parent.getData(), "dailyUid");
			}
			this.calculateCirculatingVolume(status.getUpdatedNodes(), session, parent);			
			if(recalculateLoses) MudVolumeUtil.updateCumLossesVolumeToDateForOperation(operationUid, dailyUid);
		}
	}
	
	public void calculateCirculatingVolume(Collection<CommandBeanTreeNode> nodes, UserSession session, CommandBeanTreeNode parent) throws Exception{
		String mudVolumesUid=null;
		if (parent.getData() instanceof MudVolumes)
		{
			MudVolumes mudVolumes=(MudVolumes)parent.getData();
			mudVolumesUid=mudVolumes.getMudVolumesUid();
			
			Double activeVolume=this.calculateTotalVolumeByLabel(nodes, session, "vol_active");
			Double holeVolume=this.calculateTotalVolumeByLabel(nodes, session, "vol_hole");
			Double circulatingVolume=activeVolume+holeVolume;
			
			
			QueryProperties qp=new QueryProperties();
			qp.setUomConversionEnabled(false);
			String strSql1 = "UPDATE MudVolumes SET activeVolume =:activeVolume, holeVolume =:holeVolume, circulatingVolume =:circulatingVolume WHERE mudVolumesUid =:mudVolumesUid";
			String[] paramsFields = {"activeVolume", "holeVolume","circulatingVolume","mudVolumesUid"};
			Object[] paramsValues = {activeVolume, holeVolume,circulatingVolume,mudVolumesUid};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields, paramsValues,qp);
		}
	}
	
	public Double calculateTotalVolumeByLabel(Collection<CommandBeanTreeNode> nodes, UserSession session, String label) throws Exception{
		Double totalVolume=0.00;
		
		CustomFieldUom thisConverter=new CustomFieldUom(session.getUserLocale(),MudVolumeDetails.class,"volume");
		
		for (CommandBeanTreeNode node:nodes)
		{
			MudVolumeDetails detail = (MudVolumeDetails) node.getData();
			if ( label.equalsIgnoreCase(detail.getLabel()) && detail.getVolume()!=null )
			{
				thisConverter.setBaseValueFromUserValue(detail.getVolume());
				totalVolume+=thisConverter.getBasevalue();
			}
		}
		
		return totalVolume;
	}
	
	// the function below only applicable for Pronet, thus, only enable in MudVolumePronetDataNodeListener
	/*public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		String operationUid = session.getCurrentOperationUid();
		this.updateCumulativeVolumeDHGainandDHLoss(node, operationUid, commandBean);
		Object object = node.getData();
		
		if(object instanceof MudVolumes) {
			MudVolumes thisMudVolumes = (MudVolumes) object;
			String fieldName = "";
			if (thisMudVolumes.getCheckTime() != null){
				fieldName = "checkTime";
			}
			else
			{
				fieldName = "sequence";
			}
			CustomFieldUom thisConverter=new CustomFieldUom(commandBean,MudVolumes.class,"pumpedVolume");
			MudVolumeUtil.calculateVolumePumpedandLosses(session.getCurrentDailyUid(), session.getCurrentOperationUid(), thisConverter,"pumpedVolume", "cumPumpedVolume", fieldName);
			thisConverter.setReferenceMappingField(MudVolumes.class, "volumeLosses");
			MudVolumeUtil.calculateVolumePumpedandLosses(session.getCurrentDailyUid(), session.getCurrentOperationUid(), thisConverter,"volumeLosses", "cumVolumeLosses", fieldName);
		}
		
	}*/
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		String operationUid = session.getCurrentOperationUid();
		this.updateCumulativeVolumeDHGainandDHLoss(node, operationUid, commandBean);
		MudVolumeUtil.updateCumLossesVolumeToDateForOperation(operationUid, session.getCurrentDailyUid());
	}
	
	/**
	 * Common Method to update loss_dh and gain_dh volume after calculate
	 * @param node
	 * @param operationUid
	 * @param commandBean
	 * @throws Exception
	 * */
	public void updateCumulativeVolumeDHGainandDHLoss(CommandBeanTreeNode node, String operationUid, CommandBean commandBean)throws Exception {
		
		//assign unit to the value base on volume unit
		CustomFieldUom thisConverter=new CustomFieldUom(commandBean,MudVolumeDetails.class,"volume");
		Object object = node.getData();		
		if (object instanceof MudVolumeDetails) {
			MudVolumeDetails thisMudVolumeDetails = (MudVolumeDetails) object;
			MudVolumeUtil.calcCumulativeVolumeType(thisMudVolumeDetails.getMudVolumesUid(), operationUid, thisConverter, "dhloss", "loss","loss_dh");
			MudVolumeUtil.calcCumulativeVolumeType(thisMudVolumeDetails.getMudVolumesUid(), operationUid, thisConverter, "dhgain", "gain", "gain_dh");
		}
	}
	
	public Double getCummMudLost(String currentOperationUid, Date todayDate, CustomFieldUom thisConverter, String label) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		Double totalVolume = 0.00;
		
		String strSql = "SELECT SUM(b.volume) as totalLoss FROM MudVolumes a, MudVolumeDetails b, Daily d " +
				"WHERE (a.isDeleted = false or a.isDeleted is null) AND (b.isDeleted = false or b.isDeleted is null) AND (d.isDeleted = false or d.isDeleted is null) " +
				"AND b.label IN (" + label + ") " +
				"AND a.mudVolumesUid=b.mudVolumesUid AND d.dailyUid = a.dailyUid AND d.dayDate <= :userDate AND a.operationUid=:operationUid " ;
		String[] paramsFields = {"userDate" ,"operationUid"};
		Object[] paramsValues = {todayDate, currentOperationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult!=null && lstResult.size() > 0){			
			Object thisResult = (Object) lstResult.get(0);	
			
			if (thisResult != null) {
				totalVolume = Double.parseDouble(thisResult.toString());
				thisConverter.setReferenceMappingField(MudVolumeDetails.class, "volume");
				thisConverter.setBaseValue(totalVolume);
				totalVolume = thisConverter.getConvertedValue();						
			}				
		}
		
		return totalVolume;
	}
	
	//get totalLosses for multiple selected label/location
	public static double getLosses(String operationUid, String dailyUid, String mudVolumesUid, String type, String label) throws Exception
	{
		String[] paramsFields = {"dailyUid", "operationUid", "mudVolumeUid"};
		Object[] paramsValues = new Object[3]; paramsValues[0] = dailyUid; paramsValues[1] = operationUid; paramsValues[2] = mudVolumesUid;
		String strSql = "SELECT SUM(mvd.volume) as totalVolume FROM MudVolumeDetails mvd, MudVolumes mv where (mvd.isDeleted = false or mvd.isDeleted is null) and (mv.isDeleted = false or mv.isDeleted is null)"
				+ "and mvd.type = '"+ type +"'"
				+ "and mvd.label IN ("+ label +")" 
				+ "and mvd.mudVolumesUid = mv.mudVolumesUid "
				+ "and mv.dailyUid = :dailyUid "
				+ "and mv.operationUid = :operationUid "
				+ "and mv.mudVolumesUid =:mudVolumeUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		Object a = (Object) lstResult.get(0);
		Double totalVolume = 0.00;
		if (a != null) totalVolume = Double.parseDouble(a.toString());
		return totalVolume;
	}
	
	//get CumMudLost for all Operation in Well
	public Double getCumMudLost(String currentWellUid, Date todayDate, CustomFieldUom thisConverter, String label) throws Exception{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		Double totalVolume = 0.00;
		
		String strSql = "SELECT SUM(b.volume) as totalLoss FROM MudVolumes a, MudVolumeDetails b, Daily d " +
				"WHERE (a.isDeleted = false or a.isDeleted is null) AND (b.isDeleted = false or b.isDeleted is null) "
				+ "AND (d.isDeleted = false or d.isDeleted is null) "
				+ "AND b.label IN (" + label + ") "
				+ "AND a.mudVolumesUid=b.mudVolumesUid "
				+ "AND d.dailyUid = a.dailyUid "
				+ "AND d.dayDate <= :userDate "
				+ "AND a.wellUid=:wellUid " ;
		String[] paramsFields = {"userDate" ,"wellUid"};
		Object[] paramsValues = {todayDate, currentWellUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult!=null && lstResult.size() > 0){			
			Object thisResult = (Object) lstResult.get(0);	
			
			if (thisResult != null) {
				totalVolume = Double.parseDouble(thisResult.toString());
				thisConverter.setReferenceMappingField(MudVolumeDetails.class, "volume");
				thisConverter.setBaseValue(totalVolume);
				totalVolume = thisConverter.getConvertedValue();						
			}				
		}
		
		return totalVolume;
	}
	
}
