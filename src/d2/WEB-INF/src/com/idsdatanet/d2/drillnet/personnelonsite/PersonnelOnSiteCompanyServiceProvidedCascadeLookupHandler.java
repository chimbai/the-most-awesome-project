package com.idsdatanet.d2.drillnet.personnelonsite;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * Get a list of Service Provided of the Company defined in Company Lookup
 * 
 * @author mhchai
 *
 */
public class PersonnelOnSiteCompanyServiceProvidedCascadeLookupHandler implements CascadeLookupHandler {

	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();

		Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://company.service?key=code&amp;value=label", userSelection, null);

		String sql = "SELECT lcs.code FROM LookupCompany lc, LookupCompanyService lcs WHERE (lc.isDeleted = false OR lc.isDeleted is null) AND (lcs.isDeleted = false OR lcs.isDeleted is null) " +
				"AND lcs.lookupCompanyUid = lc.lookupCompanyUid AND lc.lookupCompanyUid = :thisLookupCompanyUid " +
				"ORDER BY lcs.code";
		List lsResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,
				new String[] {"thisLookupCompanyUid"}, new String[] {key});

		if (lsResults.size() > 0) {
			for (Object lsResult : lsResults) {
				String shortCode = lsResult.toString();
				if (lookupMap.get(shortCode) != null) {
					result.put(shortCode, lookupMap.get(shortCode));	
				}
			}
		}
		
		return result;
	}

	/**
	 * To fire SQL query once for all combination, and create the cascade lookup
	 */
	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		CascadeLookupSet cascadeLookupSet = null;

		//Get full company.service list from lookup.xml (to get the full description (from value) later)
		//Map<String, LookupItem> lookupMap = LookupManager.getConfiguredInstance().getLookup("xml://company.service?key=code&amp;value=label", userSelection, null);

		String sql = "SELECT lcs.lookupCompanyUid, lcs.code, service.lookupLabel FROM LookupCompany lc, LookupCompanyService lcs, CommonLookup service " +
				"WHERE (lc.isDeleted = false OR lc.isDeleted is null) AND (lcs.isDeleted = false OR lcs.isDeleted is null) AND (service.isDeleted = false OR service.isDeleted is null)" +
				"AND lc.lookupCompanyUid = lcs.lookupCompanyUid " + 
				"AND service.shortCode = lcs.code AND service.lookupTypeSelection='company.service' " +
				"ORDER BY 1, 2";
		List<?> lsResults = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		Map<String, LookupItem> resultMap = new LinkedHashMap<String, LookupItem>();

		if (lsResults.size() > 0) {
			for (Object lsResult : lsResults) {
				if (lsResult != null) {
					Object[] code = (Object[]) lsResult;
					String lookupCompanyUid = (String) code[0];
					String serviceCode = (String) code[1];
					String label = (String) code[2];
					
					boolean found = false;
					for(CascadeLookupSet cascade : result) {
						if(cascade.getKey().equals(lookupCompanyUid)) {
							found = true;
							cascade.getLookup().put(serviceCode, new LookupItem(serviceCode, label));
							break;
						}
					}
					
					if(!found) {
						cascadeLookupSet = new CascadeLookupSet(lookupCompanyUid);
						resultMap = new LinkedHashMap<String, LookupItem>();
						resultMap.put(serviceCode, new LookupItem(serviceCode, label));
						cascadeLookupSet.setLookup(new LinkedHashMap<String, LookupItem>(resultMap));
						result.add(cascadeLookupSet);
					}
				}
			}
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		return null;
	}


}
