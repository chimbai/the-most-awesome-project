package com.idsdatanet.d2.drillnet.customRSR;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.menu.MenuDaySelectionItem;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CustomRSRDayDateLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot selectionSnapshot, HttpServletRequest request, LookupCache cache) throws Exception {
		Map<String, LookupItem> result = cache.getFromCache(this.getClass().getName());
		if(result != null) return result;
		
		UserSession session = null;
		String hql = "SELECT DATE(startTime), COUNT(rigStateRawUid) FROM RigStateRaw WHERE operationUid=:operationUid AND isSlicedByTour=0 GROUP BY DATE(startTime) ORDER BY DATE(startTime)";
		List <Object[]> rigStateRaws = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "operationUid", selectionSnapshot.getOperationUid());
		result = new LinkedHashMap<String, LookupItem>();
		
		DateFormat datetimeformat = new SimpleDateFormat("yyyy-MM-dd");
		
		for(Object[] rigStateRaw : rigStateRaws){
			String reportDatetime = rigStateRaw[0].toString();
			Date rawStartTime = new SimpleDateFormat("yyyy-MM-dd").parse(reportDatetime);
			
			hql = "SELECT reportNumber, DATE(reportDatetime), dailyUid FROM ReportDaily WHERE reportDatetime = :reportDatetime and operationUid=:operationUid AND isDeleted IS NULL AND sysDeleted IS NULL";
			List <Object[]> reportDailys = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"reportDatetime", "operationUid"}, new Object[] {rawStartTime, selectionSnapshot.getOperationUid()});
			
			String reportNumber = "";
			String dailyUid = "";
			if(reportDailys.size() > 0){
				Object[] reportDaily = reportDailys.get(0);
				reportNumber = reportDaily[0].toString();
				dailyUid = reportDaily[2].toString();
				
				String label = (StringUtils.isBlank(reportNumber) ? " - " : "#" + reportNumber + " ") + datetimeformat.format(rawStartTime);
				result.put(dailyUid, new LookupItem(dailyUid, label));
			}
		}
		
		return result;
	}
}
