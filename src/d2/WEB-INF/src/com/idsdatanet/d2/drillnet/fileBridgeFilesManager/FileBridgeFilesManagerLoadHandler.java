package com.idsdatanet.d2.drillnet.fileBridgeFilesManager;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.CachedRowSet;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.FileBridgeFiles;
import com.idsdatanet.d2.core.model.FileBridgeTypeLookup;
import com.idsdatanet.d2.core.model.FileManagerFiles;
import com.idsdatanet.d2.core.web.filemanager.FileManagerUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.infra.filemanager.html.FileManagerLoadHandler;
import com.idsdatanet.d2.core.web.mvc.TreeNodeData;

public class FileBridgeFilesManagerLoadHandler extends FileManagerLoadHandler {
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		
		UserSession userSession = UserSession.getInstance(request);
		List<Object> results = new ArrayList<Object>();
		//System.out.println("Timer STARTS");
		//final long startTime = System.currentTimeMillis();
		FileBridgeFilesManagerUtils.getConfiguredInstance().getArchive().load(commandBean, userSession, userSelection, pagination);
		
		final CachedRowSet resultSet = FileBridgeFilesManagerUtils.getConfiguredInstance().getArchive().getRecordSet();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String fileName = null;
		String operationUid = null;
		String fileBridgeFilesUid = null;
		Date reportDatetime = null;
		Date dayDate = null;
		String dayNumber = null;
		String status = null;
		String remark = null;
		String dailyUid = null;
		String uploadedUserUid = null;
		Date createdDatetime = null;
		
		while(resultSet.next()) {
			fileName = resultSet.getString("file_name");
			operationUid = resultSet.getString("operation_uid");
			fileBridgeFilesUid = resultSet.getString("file_bridge_files_uid");
			reportDatetime = resultSet.getString("report_datetime") != null ? dateFormat.parse(resultSet.getString("report_datetime")) : null;
			dayDate = resultSet.getString("day_date") != null ? dateFormat.parse(resultSet.getString("day_date")) : null;
			dayNumber = resultSet.getString("day_number");
			status = resultSet.getString("lookup_label");
			remark = resultSet.getString("remark");
			dailyUid = resultSet.getString("daily_uid");
			uploadedUserUid = resultSet.getString("uploaded_user_uid");
			createdDatetime = resultSet.getString("created_datetime") != null ? dateFormat.parse(resultSet.getString("created_datetime")) : null;
			
			FileManagerFiles file = getFileManagerFilesByFBFUid (fileBridgeFilesUid);
			TreeNodeData data = new TreeNodeData(file);
			Boolean attachedFileExists = true;
			if(file != null) {
				File attachedFile = FileManagerUtils.getAttachedFile(file);
				if(!attachedFile.exists()) {
					attachedFileExists = false;
				} 
			}

			this.getType(fileBridgeFilesUid, data);
			this.getDayInfo(reportDatetime, dayDate, dayNumber, data);
			String completeOperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), operationUid, null, true);

			if (StringUtils.isNotBlank(completeOperationName)){
				data.setDynamicAttribute("completeOperationName", completeOperationName);
			}

			data.setDynamicAttribute("fileName", fileName);
			data.setDynamicAttribute("attachedFileExists", attachedFileExists.toString());
			data.setDynamicAttribute("fileSize", formatSize(file));
			data.setDynamicAttribute("status", status);
			data.setDynamicAttribute("fbfStatusRemark", remark);
			data.setDynamicAttribute("fileBridgeFilesUid", fileBridgeFilesUid);
			data.setDynamicAttribute("operationUid", operationUid);
			data.setDynamicAttribute("dailyUid", dailyUid);
			data.setDynamicAttribute("uploadedUserUid", uploadedUserUid);
			data.setDynamicAttribute("createdDatetime", (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(createdDatetime));
			results.add(data);
			
		}
		resultSet.release();
		
		if (pagination!=null && pagination.isEnabled()) {
			commandBean.getInfo().getPagination().setTotalRows(FileBridgeFilesManagerUtils.getConfiguredInstance().getArchive().getTotalRecordCount());
			commandBean.getInfo().getPagination().setRowsPerPage(pagination.getRowsPerPage());
			commandBean.getInfo().getPagination().setCurrentPage(pagination.getCurrentPage());
			commandBean.getInfo().getPagination().setEnabled(pagination.isEnabled());
		}
		//final long endTime = System.currentTimeMillis();
		//System.out.println("Total execution time: " + (endTime - startTime));
		return results;
	}
	
	protected FileManagerFiles getFileManagerFilesByFBFUid(String fbfUid) throws Exception{
		String key = "FileBridgeFiles:" + fbfUid;
		String hql = "FROM FileManagerFiles WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND externalKey1 = :key";
		List<FileManagerFiles> files = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "key", key);
		return files.size() > 0 ? files.get(0) : null;
	}

	private void getDayInfo (Date reportDatetime, Date dayDate, String dayNumber, TreeNodeData data) throws Exception {
		String strReportDatetime = null;
		String dayName = null;
		if (StringUtils.isEmpty(dayNumber)) dayNumber = "-";
		if(reportDatetime != null && dayDate != null){		
			strReportDatetime = new SimpleDateFormat("yyyy-MM-dd").format(reportDatetime);
			dayName = "#" + dayNumber + " (" +  new SimpleDateFormat("dd MMM yyyy").format(dayDate) + ")";
		}
		data.setDynamicAttribute("reportDatetime", strReportDatetime);
		data.setDynamicAttribute("dayName", dayName);
	}
	
	private void getType (String fileBridgeFilesUid, TreeNodeData data) throws Exception {
		FileBridgeFiles fbf = (FileBridgeFiles) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(FileBridgeFiles.class, fileBridgeFilesUid);
		String hql = "FROM FileBridgeTypeLookup WHERE parserKey=:parserKey AND (isDeleted IS NULL OR isDeleted = FALSE)";
		List<?> keys = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "parserKey", fbf.getParserKey());
		if(keys.size() > 0 && keys.get(0) != null)
			data.setDynamicAttribute("parserKey", ((FileBridgeTypeLookup) keys.get(0)).getName());
	}
	
}
