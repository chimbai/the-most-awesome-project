package com.idsdatanet.d2.drillnet.report;

import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public interface ReportModuleListener {
	public void beforeReportJobStart(UserSelectionSnapshot userSelection, ReportJobParams reportJobData) throws Exception;
	
	public void afterReportGenerated(UserSelectionSnapshot userSelection, ReportJobParams reportJobData, AbstractReportModule reportModule) throws Exception;
	
}
