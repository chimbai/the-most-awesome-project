package com.idsdatanet.d2.drillnet.well;

import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.model.SiteLocation;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

/**
 * Formatter for the site location. Formats the location based on
 * the pattern described in the the sitenet.xml
 * 
 * @author William
 */
public class WellLocationFormatter extends DefaultBeanSupport {

	private Map<String,String> formatByType = null;
	
	/**
	 * Main method for formatting a location, takes a type and location 
	 * and produces a string based on the defined pattern
	 * 
	 * @param locationType
	 * @param location
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String formatLocation(String locationType,SiteLocation location) throws Exception{
		
		String groupName = formatByType.get(locationType);
		if (groupName != null && groupName.indexOf("%") != -1){
			StringWriter returnValue = new StringWriter();
			// Split string on % character
			String[] objArray = groupName.split("%");
			for (int i=0;i < objArray.length;i++){
				String currentString = objArray[i];
				if (i % 2 == 0){
					returnValue.append(currentString);
				} else {
					if (currentString.indexOf(":") != -1){
						String[] lookupFields = currentString.split("\\:");
						String localField = lookupFields[0];
						
						String[] lookups = lookupFields[1].split("\\.");
						String lookupTable = lookups[0];
						String lookupField = lookups[1];
						
						String sqlLookup = "SELECT " + lookupField + " FROM " + lookupTable +
							" WHERE " + localField + " ='" + String.valueOf(PropertyUtils.getProperty(location, localField)) + "'";
						List<Object> results = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sqlLookup);
						returnValue.append(String.valueOf(results.get(0)));
					} else {
						returnValue.append(String.valueOf(PropertyUtils.getProperty(location, currentString)));
					}
				}
			}
			return returnValue.toString();
		} else {
			return groupName;
		}
	}
	
	public Map<String, String> getFormatByType() {
		return formatByType;
	}

	public void setFormatByType(Map<String, String> formatByType) {
		this.formatByType = formatByType;
	}
	
	public static WellLocationFormatter getConfiguredInstance() throws Exception {
		return getSingletonInstance(WellLocationFormatter.class);
	}
	
}
