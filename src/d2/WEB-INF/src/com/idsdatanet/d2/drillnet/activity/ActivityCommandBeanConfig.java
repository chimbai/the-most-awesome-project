package com.idsdatanet.d2.drillnet.activity;

import java.util.List;

import com.idsdatanet.d2.core.report.validation.CommandBeanReportValidator;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.AjaxAutoPopulateListener;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanConfiguration;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanReportDataListener;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataSummaryListener;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxActionHandler;
import com.idsdatanet.d2.ftr.FTRDataNodeListener;

public class ActivityCommandBeanConfig extends CommandBeanConfiguration {
	
	private Boolean saveAllUserCode = true;
	private List<String> taskCodeListForDataEntrySystemMessage = null;
	
	public void setSaveAllUserCode(Boolean value) {
		this.saveAllUserCode = value;
	}

	public void setTaskCodeListForDataEntrySystemMessage(
			List<String> taskCodeListForDataEntrySystemMessage) {
		this.taskCodeListForDataEntrySystemMessage = taskCodeListForDataEntrySystemMessage;
	}
	
	public void init(BaseCommandBean commandBean) throws Exception{
	}
	
	public DataNodeListener getDataNodeListener() throws Exception{
		//ActivityDataNodeListener listener = new ActivityDataNodeListener();
		//ActivityFTRDataNodeListener listener = new ActivityFTRDataNodeListener();
		FTRDataNodeListener listener = new FTRDataNodeListener();
		listener.setSaveAllUserCode(this.saveAllUserCode);
		listener.setTaskCodeListForDataEntrySystemMessage(this.taskCodeListForDataEntrySystemMessage);

		return listener;
	}
	
	public CommandBeanListener getCommandBeanListener() throws Exception{
		//return new com.idsdatanet.d2.drillnet.activity.ActivityCommandBeanListener();
		//return new com.idsdatanet.d2.drillnet.activity.ActivityFTRCommandBean();
		return new com.idsdatanet.d2.ftr.FTRCommandBean();
	}
	
	public DataLoaderInterceptor getDataLoaderInterceptor() throws Exception{
		return new com.idsdatanet.d2.drillnet.activity.ActivityDataLoaderInterceptor();
	}
	
	public ActionManager getActionManager() throws Exception{
		return new com.idsdatanet.d2.drillnet.activity.ActivityActionManager();
	}

	public DataNodeAllowedAction getDataNodeAllowedAction() throws Exception{
		return new com.idsdatanet.d2.drillnet.activity.ActivityDataNodeAllowedAction();
	}
	
	public DataNodeLoadHandler getDataNodeLoadHandler() throws Exception {return null;}
	
	public SimpleAjaxActionHandler getSimpleAjaxActionHandler() throws Exception {return null;}
	
	public AjaxAutoPopulateListener getAjaxAutoPopulateListener() throws Exception {return null;}
	
	public CommandBeanReportDataListener getCommandBeanReportDataListener() throws Exception {return null;}
	
	public DataSummaryListener getDataSummaryListener() throws Exception {return null;}
	
	public List<CommandBeanReportValidator> getCommandBeanReportValidators() throws Exception {
		return null;
	}
}
