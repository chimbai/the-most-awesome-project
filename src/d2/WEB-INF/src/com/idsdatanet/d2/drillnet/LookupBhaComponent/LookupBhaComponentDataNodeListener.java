package com.idsdatanet.d2.drillnet.LookupBhaComponent;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.LookupBhaComponent;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.witsml.BhaComponentLookup;

public class LookupBhaComponentDataNodeListener extends EmptyDataNodeListener{
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		/*
		Object object = node.getData();
		
		if(object instanceof LookupBhaComponent) {
			LookupBhaComponent bhaComponent = (LookupBhaComponent)object;
			String name = bhaComponent.getType();
			
			if(name != null ){
				
				String strSql = "FROM LookupBhaComponent WHERE (isDeleted = false or isDeleted is null) and lookupBhaComponentUid <> :lookupBhaComponentUid AND type = :type";
				String[] paramsFields = {"lookupBhaComponentUid", "type"};
				String[] paramsValues = {(bhaComponent.getLookupBhaComponentUid()==null?"":bhaComponent.getLookupBhaComponentUid()), name};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				if (lstResult.size() > 0){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "type", "This BHA component already exist.");
				}
			}
		}*/
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		BhaComponentLookup.getConfiguredInstance().refresh();
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		BhaComponentLookup.getConfiguredInstance().refresh();
	}
	

	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof LookupBhaComponent) {
			LookupBhaComponent rec = (LookupBhaComponent) node.getData();
			String operationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCode"); 
			rec.setOperationCode(operationCode);
			String componentType = (String) commandBean.getRoot().getDynaAttr().get("componentType"); 
			rec.setComponentType(componentType);
		}
	}
}
