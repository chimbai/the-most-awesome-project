//** changes made on this file should also be applied to well operation if needed.

package com.idsdatanet.d2.drillnet.well;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Basin;
import com.idsdatanet.d2.core.model.GeneralComment;
import com.idsdatanet.d2.core.model.GloryHole;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.Platform;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.SiteLocation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.Datum;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.session.SystemSelectionFilter;
import com.idsdatanet.d2.drillnet.operation.OperationUtils;
import com.idsdatanet.d2.drillnet.report.schedule.EmailConfiguration;
import com.idsdatanet.d2.drillnet.wellexplorer.WellExplorerConstant;

public class WellDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor  {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	private MailEngine mailEngine = null;
	private EmailConfiguration emailConfig = null;	
	private Boolean wellstart = false;	
	private Boolean wellend = false;
	
	public void setWellstart(Boolean wellstart){
		this.wellstart = wellstart;
	}
	
	public void setWellend(Boolean wellend){
		this.wellend = wellend;
	}

	public void setEmailConfig(EmailConfiguration emailConfig) {
		this.emailConfig = emailConfig;
	}

	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof GeneralComment) {
			GeneralComment thisGeneralComment = (GeneralComment)object;
			thisGeneralComment.setCommentType("well_comment");
			thisGeneralComment.setDailyUid(null);
			if (StringUtils.isBlank(thisGeneralComment.getCommentUser())) {
				if (session!=null && session.getCurrentUser()!=null) {
					thisGeneralComment.setCommentUser(session.getCurrentUser().getFname());
				}
			}
		}
		
		if(object instanceof Well) {
			Well thisWell = (Well)object;
			
			String wellPlatformName = (String) node.getDynaAttr().get("platformName");
			String wellOpCo = (String) node.getDynaAttr().get("opCoName");
			String wellBasin = (String) node.getDynaAttr().get("name");
			
			// validate uniqueness here, if failed, return immediately, before anything get created below
			if (StringUtils.isNotBlank(wellPlatformName)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Platform where (isDeleted = false or isDeleted is null) and platformName = :platformName", "platformName", wellPlatformName);
				if (list.size() > 0) {
					status.setDynamicAttributeError(node, "platformName", "The platform '" + wellPlatformName + "' already exists in database");
					status.setContinueProcess(false, true);
				}
			}
			
			if (StringUtils.isNotBlank(wellOpCo)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupCompany where (isDeleted = false or isDeleted is null) and companyName = :companyName", "companyName", wellOpCo);
				if (list.size() > 0) {
					status.setDynamicAttributeError(node, "opCoName", "The company '" + wellOpCo + "' already exists in database");
					status.setContinueProcess(false, true);
				}
			}
			
			if (StringUtils.isNotBlank(wellBasin)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Basin where (isDeleted = false or isDeleted is null) and name = :name", "name", wellBasin);
				if (list.size() > 0) {
					status.setDynamicAttributeError(node, "name", "The basin '" + wellBasin + "' already exists in database");
					status.setContinueProcess(false, true);
				}
			}
			
			if (status.isProceed()) {
				if (StringUtils.isNotBlank(wellPlatformName)) {
					Platform thisPlatform = CommonUtil.getConfiguredInstance().createPlatform(wellPlatformName);
					thisWell.setPlatformUid(thisPlatform.getPlatformUid());
					commandBean.getSystemMessage().addInfo("A new platform has been created with minimal information from the Well Properties screen. Please make sure that the Platform module is reviewed and relevant details are added in.");	
				}
				
				if (StringUtils.isNotBlank(wellOpCo)) {
					LookupCompany thisLookupCompany = CommonUtil.getConfiguredInstance().createLookupCompany(wellOpCo);
					thisWell.setOpCo(thisLookupCompany.getLookupCompanyUid());
					commandBean.getSystemMessage().addInfo("A new company has been created with minimal information from the Well screen. Please make sure that the company lookup module is reviewed and relevant details are added in.");	
				}
				
				if (StringUtils.isNotBlank(wellBasin)) {
					Basin thisBasin = CommonUtil.getConfiguredInstance().createBasin(wellBasin);
					thisWell.setBasin(thisBasin.getBasinUid());
					commandBean.getSystemMessage().addInfo("A new basin has been created with minimal information from the Well screen. Please make sure that the Basin module is reviewed and relevant details are added in.");	
				}
			}
			
			//uwi for Bottom Hole Location
			
			//check whether locationtype is expanded. 
			String isCreateNew;
			isCreateNew = (String) node.getDynaAttr().get("locationTypeCreateNew");
			if(StringUtils.isNotBlank(isCreateNew)){
				node.getDynaAttr().put("uwiString", "");
			}
			
			String uwiSiteLocationUid = thisWell.getUwi();
			SiteLocation location = null;
			if (StringUtils.isNotBlank(uwiSiteLocationUid)){
				location = (SiteLocation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SiteLocation.class,uwiSiteLocationUid);
			}
			
			String uwiType = (String) node.getDynaAttr().get("locationType");
			String uwiString = (String) node.getDynaAttr().get("uwiString");
			
			if (StringUtils.isNotBlank(uwiType)){
				if (StringUtils.isNotBlank(uwiString)){
					if (location == null){
						location = new SiteLocation();
					}
					
					if (validateLocationInput(commandBean, node, status, uwiString, node.getDynaAttr().get("locationType").toString(), false))
					{
						if (parseLocationStringByType(uwiString, uwiType, location, false)){
							location.setLocationType(uwiType);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(location);						
							//thisWell.setUwi(location.getSiteLocationUid());
						}
					}										
				} else {
					String ntsTest = (String) node.getDynaAttr().get("ntsQuarter");
					String dlsTest = (String) node.getDynaAttr().get("dlsLsd");
					String fpsTest = (String) node.getDynaAttr().get("fpsSection");
					
					if (StringUtils.isNotBlank(ntsTest) || StringUtils.isNotBlank(dlsTest) || StringUtils.isNotBlank(fpsTest)){
						if (location == null){
							location = new SiteLocation();
						}
						if (!node.getDynaAttr().get("locationException").toString().matches("[0-9A-HJ-NP-Z]{2}") )
						{
							commandBean.getSystemMessage().addInfo("Location Exception allowed character [0-9A-HJ-NP-Z] only");	
							status.setDynamicAttributeError(node, "locationException", "Plese enter character [0-9A-HJ-NP-Z]");
							status.setContinueProcess(false, true);
						}
						if ("dls".equals(uwiType)){
							location.setSurveyType("1");
							location.setLocationException((String) node.getDynaAttr().get("locationException"));						
							location.setDlsLsd((String) node.getDynaAttr().get("dlsLsd"));
							location.setDlsSection((String) node.getDynaAttr().get("dlsSection"));
							location.setDlsTownship((String) node.getDynaAttr().get("dlsTownship"));
							location.setDlsRange((String) node.getDynaAttr().get("dlsRange"));
							location.setDlsMeridian((String) node.getDynaAttr().get("dlsMeridian"));
							location.setEventSequence((String) node.getDynaAttr().get("eventSequence"));	
						} else if ("nts".equals(uwiType)){
							location.setSurveyType("2");
							location.setLocationException((String) node.getDynaAttr().get("locationException"));						
							location.setNtsQuarter((String) node.getDynaAttr().get("ntsQuarter"));
							location.setNtsUnit((String) node.getDynaAttr().get("ntsUnit"));
							location.setNtsBlock((String) node.getDynaAttr().get("ntsBlock"));
							location.setNtsMapNo((String) node.getDynaAttr().get("ntsMapNo"));
							location.setNtsMapLot((String) node.getDynaAttr().get("ntsMapLot"));
							location.setNtsMapPoint((String) node.getDynaAttr().get("ntsMapPoint"));
							location.setEventSequence((String) node.getDynaAttr().get("eventSequence"));
						}else if ("fps".equals(uwiType)){
							location.setSurveyType("3");
							location.setLocationException((String) node.getDynaAttr().get("locationException"));													
							location.setFpsSection((String) node.getDynaAttr().get("fpsSection"));
							location.setFpsGrid((String) node.getDynaAttr().get("fpsGrid"));
							location.setFpsLatitude((String) node.getDynaAttr().get("fpsLatitude"));
							location.setFpsLatitudeMin((String) node.getDynaAttr().get("fpsLatitudeMin"));
							location.setFpsLongitude((String) node.getDynaAttr().get("fpsLongitude"));
							location.setFpsLongitudeMin((String) node.getDynaAttr().get("fpsLongitudeMin"));
							location.setEventSequence((String) node.getDynaAttr().get("eventSequence"));
						}
						location.setLocationType(uwiType);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(location);
						//thisWell.setUwi(location.getSiteLocationUid());
					}else{
						//if locationtype is selected, parameters is mandatory
						status.setDynamicAttributeError(node, "uwiString", "Parameters must be entered.");
						status.setContinueProcess(false, true);
					}
				}
			}else
			{
				if (location == null){
					location = new SiteLocation();
				}
				//dls
				location.setSurveyType(null);
				location.setLocationException(null);						
				location.setDlsLsd(null);
				location.setDlsSection(null);
				location.setDlsTownship(null);
				location.setDlsRange(null);
				location.setDlsMeridian(null);
				location.setEventSequence(null);	
				//nts
				location.setSurveyType(null);
				location.setLocationException(null);						
				location.setNtsQuarter(null);
				location.setNtsUnit(null);
				location.setNtsBlock(null);
				location.setNtsMapNo(null);
				location.setNtsMapLot(null);
				location.setNtsMapPoint(null);
				location.setEventSequence(null);
				//fps
				location.setSurveyType(null);
				location.setLocationException(null);													
				location.setFpsSection(null);
				location.setFpsGrid(null);
				location.setFpsLatitude(null);
				location.setFpsLatitudeMin(null);
				location.setFpsLongitude(null);
				location.setFpsLongitudeMin(null);
				location.setEventSequence(null);
				
				location.setLocationType("");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(location);
				//thisWell.setUwi(location.getSiteLocationUid());
			}
			
			//uwi for Surface Legal Location
			//check whether locationtype is expanded. 
			isCreateNew = (String) node.getDynaAttr().get("locationTypeSurfaceCreateNew");
			if(StringUtils.isNotBlank(isCreateNew)){
				node.getDynaAttr().put("uwiStringSurface", "");
			}
			
			uwiSiteLocationUid = thisWell.getSurfaceLegalUwi();
			location = null;
			if (StringUtils.isNotBlank(uwiSiteLocationUid)){
				location = (SiteLocation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SiteLocation.class,uwiSiteLocationUid);
			}
			
			String uwiTypeSurface = (String) node.getDynaAttr().get("locationTypeSurface");
			String uwiStringSurface = (String) node.getDynaAttr().get("uwiStringSurface");
			if (StringUtils.isNotBlank(uwiTypeSurface)){
				if (StringUtils.isNotBlank(uwiStringSurface)){
					if (location == null){
						location = new SiteLocation();
					}
					if (validateLocationInput(commandBean, node, status, uwiStringSurface,node.getDynaAttr().get("locationTypeSurface").toString(),true))
					{
						if (parseLocationStringByType(uwiStringSurface, uwiTypeSurface, location, true)){
							location.setLocationType(uwiTypeSurface);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(location);						
							thisWell.setSurfaceLegalUwi(location.getSiteLocationUid());
						}
					}										
				} else {
					String ntsTest = (String) node.getDynaAttr().get("ntsQuarterSurface");
					String dlsTest = (String) node.getDynaAttr().get("dlsLsdSurface");
					String fpsTest = (String) node.getDynaAttr().get("fpsSectionSurface");
					
					if (StringUtils.isNotBlank(ntsTest) || StringUtils.isNotBlank(dlsTest) || StringUtils.isNotBlank(fpsTest)){
						if (location == null){
							location = new SiteLocation();
						}
						if ("dls".equals(uwiTypeSurface)){
							location.setSurveyType("1");
							location.setLocationException((String) node.getDynaAttr().get("locationExceptionSurface"));						
							location.setDlsLsd((String) node.getDynaAttr().get("dlsLsdSurface"));
							location.setDlsSection((String) node.getDynaAttr().get("dlsSectionSurface"));
							location.setDlsTownship((String) node.getDynaAttr().get("dlsTownshipSurface"));
							location.setDlsRange((String) node.getDynaAttr().get("dlsRangeSurface"));
							location.setDlsMeridian((String) node.getDynaAttr().get("dlsMeridianSurface"));
						} else if ("nts".equals(uwiTypeSurface)){
							location.setSurveyType("2");
							location.setLocationException((String) node.getDynaAttr().get("locationExceptionSurface"));						
							location.setNtsQuarter((String) node.getDynaAttr().get("ntsQuarterSurface"));
							location.setNtsUnit((String) node.getDynaAttr().get("ntsUnitSurface"));
							location.setNtsBlock((String) node.getDynaAttr().get("ntsBlockSurface"));
							location.setNtsMapNo((String) node.getDynaAttr().get("ntsMapNoSurface"));
							location.setNtsMapLot((String) node.getDynaAttr().get("ntsMapLotSurface"));
							location.setNtsMapPoint((String) node.getDynaAttr().get("ntsMapPointSurface"));
						}else if ("fps".equals(uwiTypeSurface)){
							location.setSurveyType("3");
							location.setLocationException((String) node.getDynaAttr().get("locationExceptionSurface"));													
							location.setFpsSection((String) node.getDynaAttr().get("fpsSectionSurface"));
							location.setFpsGrid((String) node.getDynaAttr().get("fpsGridSurface"));
							location.setFpsLatitude((String) node.getDynaAttr().get("fpsLatitudeSurface"));
							location.setFpsLatitudeMin((String) node.getDynaAttr().get("fpsLatitudeMinSurface"));
							location.setFpsLongitude((String) node.getDynaAttr().get("fpsLongitudeSurface"));
							location.setFpsLongitudeMin((String) node.getDynaAttr().get("fpsLongitudeMinSurface"));
						}
						location.setLocationType(uwiTypeSurface);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(location);
						thisWell.setSurfaceLegalUwi(location.getSiteLocationUid());
					}else{
						//if locationtype is selected, parameters is mandatory
						status.setDynamicAttributeError(node, "uwiStringSurface", "Parameters must be entered.");
						status.setContinueProcess(false, true);
					}
				}
			}else{
				if (location == null){
					location = new SiteLocation();
				}
				//dls
				location.setSurveyType(null);
				location.setLocationException(null);						
				location.setDlsLsd(null);
				location.setDlsSection(null);
				location.setDlsTownship(null);
				location.setDlsRange(null);
				location.setDlsMeridian(null);
				//nts
				location.setSurveyType(null);
				location.setLocationException(null);						
				location.setNtsQuarter(null);
				location.setNtsUnit(null);
				location.setNtsBlock(null);
				location.setNtsMapNo(null);
				location.setNtsMapLot(null);
				location.setNtsMapPoint(null);
				//fps
				location.setSurveyType(null);
				location.setLocationException(null);													
				location.setFpsSection(null);
				location.setFpsGrid(null);
				location.setFpsLatitude(null);
				location.setFpsLatitudeMin(null);
				location.setFpsLongitude(null);
				location.setFpsLongitudeMin(null);

				location.setLocationType("");
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(location);
				thisWell.setSurfaceLegalUwi(location.getSiteLocationUid());
				
			}
		}
	}
	
	
	public static boolean parseLocationStringByType(String locationString, String type, SiteLocation siteLocation, boolean ES) {
		if (StringUtils.isBlank(locationString)) return false;
		if (siteLocation == null) siteLocation = new SiteLocation();
		
		if ("dls".equals(type)){		
			siteLocation.setSurveyType(locationString.substring(0, 1));
			siteLocation.setLocationException(locationString.substring(1, 3));
			siteLocation.setDlsLsd(locationString.substring(4, 6));
			siteLocation.setDlsSection(locationString.substring(7, 9));
			siteLocation.setDlsTownship(locationString.substring(10, 13));
			siteLocation.setDlsRange(locationString.substring(14, 16));
			siteLocation.setDlsMeridian(locationString.substring(16, 18));	
			if (!ES){
				siteLocation.setEventSequence(locationString.substring(19, 21));
			}				
			return true;
		} else if ("nts".equals(type)){
			
			siteLocation.setSurveyType(locationString.substring(0, 1));
			siteLocation.setLocationException(locationString.substring(1, 3));
			siteLocation.setNtsQuarter(locationString.substring(4, 5));
			siteLocation.setNtsUnit(locationString.substring(6, 9));
			siteLocation.setNtsBlock(locationString.substring(10, 11));
			siteLocation.setNtsMapNo(locationString.substring(12, 15));
			siteLocation.setNtsMapLot(locationString.substring(16, 17));
			siteLocation.setNtsMapPoint(locationString.substring(18, 20));
			if (!ES){
				siteLocation.setEventSequence(locationString.substring(21, 23));	
			}			
			return true;
		}else if ("fps".equals(type)){
			siteLocation.setSurveyType(locationString.substring(0, 1));
			siteLocation.setLocationException(locationString.substring(1, 3));
			siteLocation.setFpsGrid(locationString.substring(4, 5));
			siteLocation.setFpsSection(locationString.substring(6, 8));		
			siteLocation.setFpsLatitude(locationString.substring(9, 12));
			siteLocation.setFpsLatitudeMin(locationString.substring(13, 15));
			siteLocation.setFpsLongitude(locationString.substring(16, 19));
			siteLocation.setFpsLongitudeMin(locationString.substring(20, 22));
			if (!ES){
				siteLocation.setEventSequence(locationString.substring(23, 25));
			}	
				
			return true;
		}else if (("gps".equals(type) || "nad83".equals(type) || "nad27".equals(type))){
			String[] gpsCoors = locationString.split(" ");
			siteLocation.setFpsLatitude(gpsCoors[0]);
			siteLocation.setFpsLatitudeMin(gpsCoors[1]);
			siteLocation.setGpsLatitudeSeconds(gpsCoors[2]);
			siteLocation.setFpsLongitude(gpsCoors[3]);
			siteLocation.setFpsLongitudeMin(gpsCoors[4]);
			siteLocation.setGpsLongitudeSeconds(gpsCoors[5]);
			
			return true;
		}
		return false;
	}
	
	
	public static boolean validateLocationInput(CommandBean commandBean,CommandBeanTreeNode node,DataNodeProcessStatus status, String uwiString, String type, boolean ES) throws Exception{
		boolean isValid = true;
		
		if (StringUtils.isNotBlank(type)){
			String locationString = uwiString;
			if (StringUtils.isNotBlank(locationString)){
				if ("dls".equals(type)){
					if (ES){
						if (!locationString.matches("[1-1]{1}[0-9A-HJ-NP-Z]{2}/[0-9]{2}-[0-9]{2}-[0-9]{3}-[0-9]{2}[A-Z]{1}[0-9]{1}")){
							commandBean.getSystemMessage().addError("DLS Locations must be in the form '102/16-36-126-34W1'");
							isValid = false;
						}
					}
					else
					{
						if (!locationString.matches("[1-1]{1}[0-9A-HJ-NP-Z]{2}/[0-9]{2}-[0-9]{2}-[0-9]{3}-[0-9]{2}[A-Z]{1}[0-9]{1}/[0-9]{2}")){
							commandBean.getSystemMessage().addError("DLS Locations must be in the form '102/16-36-126-34W1/09'");
							isValid = false;
						}
					}
					
				} else if ("nts".equals(type)){
					if (ES) {
						if (!locationString.matches("[2-2]{1}[0-9A-HJ-NP-Z]{2}/[A-Z]{1}-[0-9]{3}-[A-Z]{1}/[0-9]{3}-[A-Z]{1}-[0-9]{2}")){
							commandBean.getSystemMessage().addError("NTS Locations must be in the form '203/A-060-E/094-L-05'");
							isValid = false;
						}
					}
					else
					{
						if (!locationString.matches("[2-2]{1}[0-9A-HJ-NP-Z]{2}/[A-Z]{1}-[0-9]{3}-[A-Z]{1}/[0-9]{3}-[A-Z]{1}-[0-9]{2}/[0-9]{2}")){
							commandBean.getSystemMessage().addError("NTS Locations must be in the form '203/A-060-E/094-L-05/08'");
							isValid = false;
						}
					}
					
				} else if ("fps".equals(type)){
					if (ES) {
						if (!locationString.matches("[3-3]{1}[0-9A-HJ-NP-Z]{2}/[A-Z]{1}-[0-9]{2}-[0-9]{3}.[0-9]{2}-[0-9]{3}.[0-9]{2}")){
							commandBean.getSystemMessage().addError("FPS Locations must be in the form '302/P-80-089.20-120.30'");
							isValid = false;
						}			
					}
					else
					{
						if (!locationString.matches("[3-3]{1}[0-9A-HJ-NP-Z]{2}/[A-Z]{1}-[0-9]{2}-[0-9]{3}.[0-9]{2}-[0-9]{3}.[0-9]{2}/[0-9]{2}")){
							commandBean.getSystemMessage().addError("FPS Locations must be in the form '302/P-80-089.20-120.30/02'");
							isValid = false;
						}
					}
					
				} else if (("gps".equals(type) || "nad83".equals(type) || "nad27".equals(type))){
					String[] gpsCoors = locationString.split(" ");
					if (gpsCoors != null && gpsCoors.length == 6) {
						for (int i=0;i < gpsCoors.length;i++){
							if (!NumberUtils.isNumber(gpsCoors[i])){
								isValid = false;
								break;
							}
						}
					} else {
						isValid = false;
					}
					if (!isValid){
						commandBean.getSystemMessage().addError("GPS Locations must be in the form '117 45 20.1 22 15 6.99'");
					}
				}
			}		
		} 
		
		if (!isValid){
			status.setContinueProcess(false, true);
		}
		return isValid;
	}
	
	/*
	static private String getDynaAttr(CommandBeanTreeNode node, String dynaAttrName) throws Exception {
		Object dynaAttrValue = node.getDynaAttr().get(dynaAttrName);
		if (dynaAttrValue != null) {
			return dynaAttrValue.toString();
		}
		return null;
	}
	*/
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		ApplicationUtils.getConfiguredInstance().refreshCachedData();
		
		Object obj = node.getData();
		if(obj instanceof Well) {
			Well well =(Well) obj;
			List<String> changedRowPrimaryKeys = new ArrayList<String>();
			changedRowPrimaryKeys.add(well.getWellUid());
			D2ApplicationEvent.publishTableChangedEvent(Well.class, changedRowPrimaryKeys);
			
			session.setCurrentRigOnOffShoreByWellUid(well.getWellUid());
			
			// if we just updated an existing record, reload the page for the well and datum drop down to reflect the change
			if (operationPerformed != BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				if (commandBean.getFlexClientControl() != null) {
					commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
				}
			}
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "wellStartEndNotification")))
			{
				if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW && this.wellstart) { 
					this.sendWellStartEndNotificationMail(commandBean, node, session, well,"start"); 
				}else{
				if (well.isPropertyModified("isWellEnd") && BooleanUtils.isTrue(well.getIsWellEnd()) && this.wellend){
					this.sendWellStartEndNotificationMail(commandBean, node, session, well,"end");
					}
				}
			}
			
			if (operationPerformed == BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW) {
				String redirectPage = "wellbore.html";
				//session.setCurrentWellUid(well.getWellUid());
				String copyFromOperationUid = (String) node.getDynaAttr().get("currentCopyFromOperationUid");
				
				commandBean.setRedirectUrl(CommonUtils.urlPathConcat(session.getApplicationContextPath(), redirectPage + "?newRecordInputMode=1&fromWell=1&wellUid="+well.getWellUid() + 
						(StringUtils.isNotBlank(copyFromOperationUid)?"&copyFromOperationUid="+copyFromOperationUid:"")));
				commandBean.setRedirectMessage("Creating Wellbore of " + well.getWellName());
			}
		}
	}

	private void sendWellStartEndNotificationMail(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, Well well, String status) throws Exception{
		String[] emailList = ApplicationUtils.getConfiguredInstance().getCachedEmailList(session.getCurrentGroupUid(), emailConfig.getSpecificEmailDistributionKey());
		String supportAddr = ApplicationConfig.getConfiguredInstance().getSupportEmail();

		Map<String, Object> params = new HashMap<String, Object>();		
		
		params.put("wellName", well.getWellName());
		params.put("country", well.getCountry());
		if ("start".equals(status)){
			params.put("status", "started");
			this.emailConfig.setSubject("Well Start Notification");
		}else{
			params.put("status", "ended");
			this.emailConfig.setSubject("Well End Notification");
		}
		
		if (emailList.length!=0)
		this.mailEngine.sendMail(emailList, supportAddr, ApplicationConfig.getConfiguredInstance().getSupportEmail(),
		emailConfig.getSubject(), this.mailEngine.generateContentFromTemplate(emailConfig.getTemplateFile(), params));
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "(isDeleted = false or isDeleted is null)";
		
		String wellExplorerWellUid = null;
		UserSession session = null;
		if (request != null) {
			wellExplorerWellUid = request.getParameter(WellExplorerConstant.WELL_UID);
			session = UserSession.getInstance(request);
		}
		
		if (meta.getTableClass().equals(Well.class)) {
			if (StringUtils.isNotBlank(wellExplorerWellUid) && session != null) {
				query.addParam("customFilterWellUid", wellExplorerWellUid);
				customCondition += " and wellUid = :customFilterWellUid";
				
				// Well Explorer - set current wellUid and set last active wellbore, operation and day of the selected well
				session.setCurrentWellUid(wellExplorerWellUid);
				ApplicationUtils.getConfiguredInstance().setLastActiveWellboreAndOperationAndDaily(session);
			} else {
				customCondition += " and wellUid = :wellUid";
				query.addParam("wellUid", userSelection.getWellUid());
			}
		}
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object object = node.getData();
		
		if(object instanceof Well)
		{
			Well thisWell = (Well) object;
			
			List<Operation> opss = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from operation where (isDeleted = false or isDeleted is null) and wellUid=:wellUid", new String[] { "wellUid" }, new Object[] { thisWell.getWellUid() });
			if (opss.size()>0) {
				for (Operation ops : opss) {
					// Delete LAR Setting
					OperationUtils.deleteLARmapping(ops.getOperationUid());
				}
			}
			
			//cascade delete well structure
			//ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWell(thisWell.getWellUid());
			ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWell(thisWell);
			
			session.setCurrentWellUid(null);
			session.getSystemSelectionFilter().setProperty(SystemSelectionFilter.WELL_FILTER, null);
			session.getSystemSelectionFilter().setProperty(SystemSelectionFilter.WELLBORE_FILTER, null);
		}
	}

	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
	/**
	 * Method to load the dynamic variables required for UWI handling
	 */
	public void loadUWIFields(SiteLocation location,CommandBeanTreeNode node, String surface) throws Exception{
		String uwiType = location.getLocationType();
		
		node.getDynaAttr().put("locationType"+surface+"CreateNew", "");
		
		if ("dls".equals(uwiType)){
			node.getDynaAttr().put("surveyType"+surface, location.getSurveyType());
			node.getDynaAttr().put("locationException"+surface,location.getLocationException());
			node.getDynaAttr().put("eventSequence"+surface,location.getEventSequence());		
			node.getDynaAttr().put("dlsLsd"+surface,location.getDlsLsd());
			node.getDynaAttr().put("dlsSection"+surface,location.getDlsSection());
			node.getDynaAttr().put("dlsTownship"+surface,location.getDlsTownship());
			node.getDynaAttr().put("dlsRange"+surface,location.getDlsRange());
			node.getDynaAttr().put("dlsMeridian"+surface,location.getDlsMeridian());
		} else if ("nts".equals(uwiType)){
			node.getDynaAttr().put("surveyType"+surface,"2");
			node.getDynaAttr().put("locationException"+surface,location.getLocationException());
			node.getDynaAttr().put("eventSequence"+surface,location.getEventSequence());
			node.getDynaAttr().put("ntsQuarter"+surface,location.getNtsQuarter());
			node.getDynaAttr().put("ntsUnit"+surface,location.getNtsUnit());
			node.getDynaAttr().put("ntsBlock"+surface,location.getNtsBlock());
			node.getDynaAttr().put("ntsMapNo"+surface,location.getNtsMapNo());
			node.getDynaAttr().put("ntsMapLot"+surface,location.getNtsMapLot());
			node.getDynaAttr().put("ntsMapPoint"+surface,location.getNtsMapPoint());		
		}else if ("fps".equals(uwiType)){
			node.getDynaAttr().put("surveyType"+surface,"3");
			node.getDynaAttr().put("locationException"+surface,location.getLocationException());
			node.getDynaAttr().put("eventSequence"+surface,location.getEventSequence());	
			node.getDynaAttr().put("fpsSection"+surface,location.getFpsSection());
			node.getDynaAttr().put("fpsGrid"+surface,location.getFpsGrid());
			node.getDynaAttr().put("fpsLatitude"+surface,location.getFpsLatitude());
			node.getDynaAttr().put("fpsLatitudeMin"+surface,location.getFpsLatitudeMin());
			node.getDynaAttr().put("fpsLongitude"+surface,location.getFpsLongitude());
			node.getDynaAttr().put("fpsLongitudeMin"+surface,location.getFpsLongitudeMin());
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof Well) {
			
			Well thisWell = (Well)object;
			
			//set the uwi for Bottom Hole Location
			if (StringUtils.isNotBlank(thisWell.getUwi())){
				String siteLocationUid = thisWell.getUwi();
				SiteLocation location = (SiteLocation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SiteLocation.class, siteLocationUid);
				
				if(location != null)
				{
					node.getDynaAttr().put("locationType",location.getLocationType());
					node.getDynaAttr().put("uwiString",WellLocationFormatter.getConfiguredInstance().formatLocation(location.getLocationType(), location));
					loadUWIFields(location,node,"");
				}
								
			}
			//set the uwi for Surface Legal Location
			if (StringUtils.isNotBlank(thisWell.getSurfaceLegalUwi())){
				String siteLocationUid = thisWell.getSurfaceLegalUwi();
				SiteLocation location = (SiteLocation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SiteLocation.class, siteLocationUid);
				
				if(location != null)
				{
					node.getDynaAttr().put("locationTypeSurface",location.getLocationType());
					node.getDynaAttr().put("uwiStringSurface",WellLocationSurfaceFormatter.getConfiguredInstance().formatLocation(location.getLocationType(), location));
					loadUWIFields(location,node,"Surface");
				}
			}
			
			
			//To get KBelevation, spud date and rig release date
			if(thisWell.getWellUid() != null || StringUtils.isNotBlank(thisWell.getWellUid()))
			{
				String defaultDatumUid = CommonUtil.getConfiguredInstance().getKBelevation(thisWell.getWellUid());
				if (defaultDatumUid !=null)
				{
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, OpsDatum.class, "reportingDatumOffset");
					Double kbElevation = 0.00;
					String datumReferencePoint = "MSL";
					String datumCode = "";
					String datumLabel = "";
					
					OpsDatum datum = DatumManager.getOpsDatum(defaultDatumUid);
					if (datum !=null)
					{
						if(datum.getReportingDatumOffset()!=null && datum.getOffsetMsl()!=null){
							kbElevation = datum.getReportingDatumOffset() + datum.getOffsetMsl();
							thisConverter.setBaseValue(kbElevation);
							datumLabel = thisConverter.getFormattedValue();
						}
						if(datum.getDatumCode() != null || StringUtils.isNotBlank(datum.getDatumCode())) {
							datumCode = datum.getDatumCode();
						}
						if(datum.getDatumReferencePoint() != null || StringUtils.isNotBlank(datum.getDatumReferencePoint())) {
							datumReferencePoint = datum.getDatumReferencePoint();
						}
					}
					datumLabel = datumLabel + " " + datumCode + " " +  datumReferencePoint;
					node.getDynaAttr().put("kbElevation", datumLabel);								
				}			
			}
			Datum currentDatum = node.getCommandBean().getInfo().getCurrentDatum();
			if(currentDatum != null){
				node.getDynaAttr().put("currentDatumType", currentDatum.getDatumCode());
			}			
			Double waterdepth = 0.00;
			Double kbGroundDistance = 0.00;
			Double casingFlangeElevation = 0.00;
			Double kbCasingFlangeDistance = 0.00;
			Double tubingHeadElevation = 0.00;
			Double kbTubingHeadDistance = 0.00;
			Double datum = 0.00;
			Double offMSL = 0.00;
			Double groundElevation = 0.00;
			
			Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
			if (operation != null) {
				OpsDatum opsdatum = DatumManager.getOpsDatum(operation.getDefaultDatumUid());
				if(opsdatum != null) {
					if (opsdatum.getReportingDatumOffset() != null) {
						datum = opsdatum.getReportingDatumOffset();
						if (opsdatum.getOffsetMsl() != null) {
						    offMSL = opsdatum.getOffsetMsl();
						}
					}
				}
			}
			
			
			//KB-Ground Distance
			if (thisWell.getWaterDepth()!= null)	waterdepth = thisWell.getWaterDepth();
			if (thisWell.getGlHeightMsl()!=null)    groundElevation = thisWell.getGlHeightMsl();
			CustomFieldUom thisConverterField = new CustomFieldUom(commandBean,Well.class, "waterDepth");
			thisConverterField.setBaseValueFromUserValue(waterdepth);
			waterdepth = thisConverterField.getBasevalue();
			
			thisConverterField.setReferenceMappingField(Well.class, "glHeightMsl");
			thisConverterField.setBaseValueFromUserValue(groundElevation);
			groundElevation = thisConverterField.getBasevalue();;
			
			//kbGroundDistance = datum - waterdepth;
			kbGroundDistance = (datum + offMSL) - groundElevation;
			if (kbGroundDistance !=null)
			{
				thisConverterField.setBaseValue(kbGroundDistance);
				if (thisConverterField.isUOMMappingAvailable()){
					node.setCustomUOM("@kbGroundDistance", thisConverterField.getUOMMapping());
				}
				node.getDynaAttr().put("kbGroundDistance", thisConverterField.getConvertedValue());
			}
			
			//KB-Casing Flange Distance
			if (thisWell.getCasingFlangeElevation()!= null)	casingFlangeElevation = thisWell.getCasingFlangeElevation();
			thisConverterField.setReferenceMappingField(Well.class, "casingFlangeElevation");
		
			thisConverterField.setBaseValueFromUserValue(casingFlangeElevation);
			casingFlangeElevation = thisConverterField.getBasevalue();
			
			kbCasingFlangeDistance = (datum + offMSL) - casingFlangeElevation;
			if (kbCasingFlangeDistance !=null)
			{
				thisConverterField.setBaseValue(kbCasingFlangeDistance);
				if (thisConverterField.isUOMMappingAvailable()){
					node.setCustomUOM("@kbCasingFlangeDistance", thisConverterField.getUOMMapping());
				}
				node.getDynaAttr().put("kbCasingFlangeDistance", thisConverterField.getConvertedValue());
			}
			
			//KB-Tubing Head Distance
			if (thisWell.getTubingHeadElevation()!= null)	tubingHeadElevation = thisWell.getTubingHeadElevation();
			thisConverterField.setReferenceMappingField(Well.class, "tubingHeadElevation");
		
			thisConverterField.setBaseValueFromUserValue(tubingHeadElevation);
			tubingHeadElevation = thisConverterField.getBasevalue();
			
			kbTubingHeadDistance= (datum + offMSL) - tubingHeadElevation;
			if (kbTubingHeadDistance !=null)
			{
				thisConverterField.setBaseValue(kbTubingHeadDistance);
				if (thisConverterField.isUOMMappingAvailable()){
					node.setCustomUOM("@kbTubingHeadDistance", thisConverterField.getUOMMapping());
				}
				node.getDynaAttr().put("kbTubingHeadDistance", thisConverterField.getConvertedValue());
			}
			
			//GET DRILLING OPERATION DATA
			String strSql = "FROM Operation WHERE operationCode = 'DRLLG' AND wellUid = :wellUid AND (isDeleted IS NULL OR isDeleted = '')";
			List list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellUid", thisWell.getWellUid());
			String thisOperationUid = "";
			if (list2.size() > 0) {
				Operation thisOperation = (Operation) list2.get(0);
				thisOperationUid = thisOperation.getOperationUid();
				setDynaAttr(node, "spudDate", thisOperation.getSpudDate());
				setDynaAttr(node, "rigOffHireDate", thisOperation.getRigOffHireDate());
				
			}
			
			//GET DRILLING OPERATION REPORT DAILY DATA
			if(StringUtils.isNotBlank(thisOperationUid))
			{
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "depthMdMsl");
				String thisCurrentMD = CommonUtil.getConfiguredInstance().getCurrentMD(thisOperationUid);
				if (thisCurrentMD != null)
				{
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("depthMdMsl", thisConverter.getUOMMapping());
						thisConverter.setBaseValue(Double.parseDouble(thisCurrentMD.toString()));
					}
					setDynaAttr(node, "depthMdMsl", thisConverter.getFormattedValue());
				}
				
				thisConverter.setReferenceMappingField(ReportDaily.class, "depthTvdMsl");
				String thisCurrentTVD = CommonUtil.getConfiguredInstance().getCurrentTVD(thisOperationUid);
				if (thisCurrentTVD != null)
				{
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("depthTvdMsl", thisConverter.getUOMMapping());
						thisConverter.setBaseValue(Double.parseDouble(thisCurrentTVD.toString()));
					}
					setDynaAttr(node, "depthTvdMsl", thisConverter.getFormattedValue());
				}
							
			}
			
			String gloryHoleUid = thisWell.getGloryHoleUid();
			if (StringUtils.isNotBlank(gloryHoleUid)) {
				GloryHole gloryHole = (GloryHole) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(GloryHole.class, gloryHoleUid);
				if (gloryHole!=null) {
					Map<String, Object> gloryHoleFieldValues = PropertyUtils.describe(gloryHole);
					for (Map.Entry gloryHoleFieldEntry : gloryHoleFieldValues.entrySet()) {
						String fieldName = gloryHoleFieldEntry.getKey().toString();
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, GloryHole.class, fieldName);
						if (thisConverter.isUOMMappingAvailable()){
							if (gloryHoleFieldEntry.getValue()!=null) {
								thisConverter.setBaseValue((Double)gloryHoleFieldEntry.getValue());
								setDynaAttr(node, "gloryHole." + fieldName, thisConverter.getFormattedValue());
							}
						} else {
							setDynaAttr(node, "gloryHole." + fieldName, gloryHoleFieldEntry.getValue());
						}
					}
				}
			}
		}
	}

	private static void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		}
	}

	@Override
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof Well) {
			Well well = (Well) node.getData();
			well.setOnOffShore(GroupWidePreference.getValue(userSelection.getGroupUid(), "defaultOnOffShore"));
			node.getDynaAttr().put("currentDatumType", GroupWidePreference.getValue(userSelection.getGroupUid(), "setDefaultDatumReferencePoint"));
			Basin defaultBasin = CommonUtil.getConfiguredInstance().getDefaultBasin();
			well.setIsWellEnd(false);
			if (defaultBasin !=null)
				well.setBasin(defaultBasin.getBasinUid());
		} else if (node.getData() instanceof GeneralComment) {
			GeneralComment gc = (GeneralComment) node.getData();
			UserSession session = UserSession.getInstance(request);
			if (session!=null) {
				if (session.getCurrentUser()!=null) gc.setCommentUser(session.getCurrentUser().getFname());
			}
			gc.setCommentDate(new Date());
		}
	}

	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	public void beforeDataNodeDelete(CommandBean commandBean, 
			CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, 
			HttpServletRequest request) throws Exception{
		
		Object object = node.getData();
		
		if(object instanceof Well)
		{
			Well thisWell = (Well)object;
			
			if(thisWell.getWellUid() != null || StringUtils.isNotBlank(thisWell.getWellUid()))
			{
				String strSql = "FROM Wellbore wb, Operation o WHERE o.wellboreUid = wb.wellboreUid AND wb.wellUid = :wellUid AND (wb.isDeleted IS NULL OR wb.isDeleted = '') AND (o.isDeleted IS NULL OR o.isDeleted = '')";
				List list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellUid", thisWell.getWellUid());				
				if (list2.size() > 0) {					
					status.setContinueProcess(false);
					commandBean.getSystemMessage().addError("This Well cannot be deleted, there is other Wellbore/Operation link to this Well.");
				}
			}
		}
	}
	
}
