package com.idsdatanet.d2.drillnet.rigpump;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.idsdatanet.d2.core.model.RigPumpParam;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.matrix.DefaultMatrixHandler;
import com.idsdatanet.d2.drillnet.matrix.Matrix;
import com.idsdatanet.d2.drillnet.matrix.MatrixHandler;

public class RigPumpMatrixHandler extends DefaultMatrixHandler {

	public void process(Matrix matrix, UserSelectionSnapshot userSelection) throws Exception {
		/*String sql = "SELECT DISTINCT rpp.dailyUid from RigPump rp, RigPumpParam rpp WHERE " +
				"(rp.isDeleted = false or rp.isDeleted is null) " +
				"AND (rpp.isDeleted = false or rpp.isDeleted is null) " +
				"AND rp.rigPumpUid=rpp.rigPumpUid and rpp.operationUid = :operationUid";*/
		boolean f = false;
		try {
			RigPumpParam.class.getDeclaredField("isIncomplete");
			f = true;
		} catch (NoSuchFieldException e) {
			f = false;
		}
		String sql = "";
		if(f) {
			sql =  "SELECT dailyUid,isIncomplete from RigPumpParam WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", userSelection.getOperationUid());
			if(list != null && list.size() > 0) {
				Set<String> linkedHashSet = new LinkedHashSet<String>();
				for(Object[] daily : list) {
					String dailyUid = null;
					if(daily[0] != null) {
						dailyUid = (String) daily[0];
					}
					linkedHashSet.add(dailyUid);
				}
				for(String id: linkedHashSet) {
					String dailyUid = null;
					String isIncomplete = null;
					for(Object[] daily : list) {
						if(daily[0] != null) {
							dailyUid = (String) daily[0];
							if(id.equals(dailyUid)){
								if(daily[1] != null) {
									if(isIncomplete != "true") isIncomplete = String.valueOf(daily[1]);
								}
							}
						}
					}
					matrix.addData(id,isIncomplete);
				}
			}
		}else {
			sql =  "SELECT DISTINCT dailyUid from RigPumpParam WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid";
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", userSelection.getOperationUid());
	
			if(list.size() > 0) {
				for(String dailyUid : list) {
					matrix.addData(dailyUid,"N/A");
				}
			}
		}
	}
}
