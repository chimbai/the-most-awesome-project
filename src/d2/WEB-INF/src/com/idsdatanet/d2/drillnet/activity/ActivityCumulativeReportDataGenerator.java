package com.idsdatanet.d2.drillnet.activity;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class ActivityCumulativeReportDataGenerator implements ReportDataGenerator{
	@Override
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {
			// TODO Auto-generated method stub
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			String strSql = "from Activity where wellUid=:wellUid and "+ 
					"wellboreUid=:wellboreUid and operationUid=:operationUid "+
					"and (isSimop=false or isSimop is null) "+
					"and (isOffline=false or isOffline is null) "+
					"and (isDeleted=false or isDeleted is null) "+
					"order by endDatetime";
			String[] paramsFields = {"wellUid","wellboreUid","operationUid"};
			Object[] paramsValues = {userContext.getUserSelection().getWellUid(),
									userContext.getUserSelection().getWellboreUid(),
									userContext.getUserSelection().getOperationUid()
									};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
			
			//Get current selected date and convert to epoch
			Daily currentDate = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
			Date currentDateStr = currentDate.getDayDate();
			Long currentDatetimeEpoch = currentDateStr.getTime() + 86399000;
			
			if(lstResult.size() >= 1) {
				for(Object objResult:lstResult) {
					
					//Initialize object
					Activity activity = (Activity) objResult;
					
					//Initialize variables
					String rootCauseCode = "";
					String classCode="";
					Double activityDuration = 0.0;
					Long startDatetimeEpoch = (long) 0;
					Long endDatetimeEpoch = (long) 0;
					String activityDescription = "";
					String wellUid = "";
					String wellboreUid="";
					String operationUid="";
					Integer dayPlus = 0;
					
					//Validation and checking
					if(activity.getRootCauseCode()!=null && StringUtils.isNotBlank(activity.getRootCauseCode())) {
						rootCauseCode = activity.getRootCauseCode().toString();
					}
					
					if(activity.getClassCode()!=null && StringUtils.isNotBlank(activity.getClassCode())) {
						classCode = activity.getClassCode().toString();
					}
					
					if(activity.getActivityDuration()!=null && StringUtils.isNotBlank(activity.getActivityDuration().toString())) {
						activityDuration = activity.getActivityDuration();
					}
					
					if(activity.getStartDatetime()!=null && StringUtils.isNotBlank(activity.getStartDatetime().toString())) {
						startDatetimeEpoch = activity.getStartDatetime().getTime();
					}
					
					if(activity.getEndDatetime()!=null && StringUtils.isNotBlank(activity.getEndDatetime().toString())) {
						endDatetimeEpoch = activity.getEndDatetime().getTime();
					}
					
					if(activity.getActivityDescription()!=null && StringUtils.isNotBlank(activity.getActivityDescription())) {
						activityDescription = activity.getActivityDescription();
					}
					
					if(activity.getWellUid()!=null && StringUtils.isNotBlank(activity.getWellUid())) {
						wellUid = activity.getWellUid();
					}
					
					if(activity.getWellboreUid()!=null && StringUtils.isNotBlank(activity.getWellboreUid())) {
						wellboreUid = activity.getWellboreUid();
					}
					
					if(activity.getOperationUid()!=null && StringUtils.isNotBlank(activity.getOperationUid())) {
						operationUid = activity.getOperationUid();
					}
					
					if(activity.getDayPlus()!=null && StringUtils.isNotBlank(activity.getDayPlus().toString())) {
						dayPlus = activity.getDayPlus();
					}
					
					if(endDatetimeEpoch<=currentDatetimeEpoch) {
						//Generate XML module
						ReportDataNode thisReportNode = reportDataNode.addChild("ActivityCumulative");
						thisReportNode.addProperty("rootCauseCode", rootCauseCode);
						thisReportNode.addProperty("classCode", classCode);
						thisReportNode.addProperty("activityDuration", activityDuration.toString());
						thisReportNode.addProperty("startDatetimeEpoch", startDatetimeEpoch.toString());
						thisReportNode.addProperty("endDatetimeEpoch", endDatetimeEpoch.toString());
						thisReportNode.addProperty("activityDescription", activityDescription);
						thisReportNode.addProperty("wellUid", wellUid);
						thisReportNode.addProperty("wellboreUid", wellboreUid);
						thisReportNode.addProperty("operationUid", operationUid);
						thisReportNode.addProperty("dayPlus", dayPlus.toString());
					}
				}
			}
		}
	}

	
	
