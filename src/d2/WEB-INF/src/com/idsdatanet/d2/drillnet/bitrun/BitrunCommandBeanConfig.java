package com.idsdatanet.d2.drillnet.bitrun;

import java.util.List;

import com.idsdatanet.d2.core.report.validation.CommandBeanReportValidator;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.AjaxAutoPopulateListener;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanConfiguration;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanReportDataListener;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataSummaryListener;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxActionHandler;
import com.idsdatanet.d2.drillnet.bharun.BharunDataNodeListener;

public class BitrunCommandBeanConfig extends CommandBeanConfiguration {
	
	private Boolean dateTimeValidation = false;
	
	//p2
	private Boolean calcumduration = false;
	public void setcalcumduration(Boolean value) {
		this.calcumduration = value;
	}
	
	public void setDateTimeValidation(Boolean value) {
		this.dateTimeValidation = value;
	}
	
	public void init(BaseCommandBean commandBean) throws Exception{
	}
	
	public DataNodeListener getDataNodeListener() throws Exception{
		BharunDataNodeListener listener = new BharunDataNodeListener();
		listener.setDateTimeValidation(this.dateTimeValidation);
		listener.setcalcumduration(this.calcumduration);
		return listener;
		//return new com.idsdatanet.d2.drillnet.bharun.BharunDataNodeListener();
	}
	
	public CommandBeanListener getCommandBeanListener() throws Exception{
		return new com.idsdatanet.d2.drillnet.bitrun.BitrunCommandBeanListener();
	}
	
	public DataLoaderInterceptor getDataLoaderInterceptor() throws Exception{
		return new com.idsdatanet.d2.drillnet.bitrun.BitrunDataLoaderInterceptor();
	}
	
	public ActionManager getActionManager() throws Exception {return null;}

	public DataNodeAllowedAction getDataNodeAllowedAction() throws Exception{
		return null;
	}
	
	public DataNodeLoadHandler getDataNodeLoadHandler() throws Exception {return null;}
	
	public SimpleAjaxActionHandler getSimpleAjaxActionHandler() throws Exception {return null;}
	
	public AjaxAutoPopulateListener getAjaxAutoPopulateListener() throws Exception {return null;}
	
	public CommandBeanReportDataListener getCommandBeanReportDataListener() throws Exception {return null;}
	
	public DataSummaryListener getDataSummaryListener() throws Exception {
		return new com.idsdatanet.d2.drillnet.bitrun.BitrunDataSummaryListener();
	}
	
	public List<CommandBeanReportValidator> getCommandBeanReportValidators() throws Exception {
		return null;
	}
}
