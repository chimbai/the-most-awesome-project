package com.idsdatanet.d2.drillnet.rigstock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class MudVendorLookupHandler implements LookupHandler {

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
	
		//get mud vendor from report daily screen
		String sql = "SELECT DISTINCT(mudVendorCompanyUid) from ReportDaily where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and reportType='DDR' and mudVendorCompanyUid<>''";
		String[] paramNames =  {"operationUid"};
		Object[] paramValues = {userSelection.getOperationUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramNames, paramValues);			
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		List<Object[]> mapList = new ArrayList();
		
		if (lstResult.size() > 0) {
			for (Object obj : lstResult) {
				if (obj != null) {
					LookupCompany lookupcomp = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, obj.toString());
					String companyName = "";
					String companyUid = "";					
					if (lookupcomp !=null){
						companyName = lookupcomp.getCompanyName();
						companyUid = lookupcomp.getLookupCompanyUid();
					}					
					mapList.add(new Object[] {companyUid, new LookupItem(companyUid, companyName)});
				}
			}
		}	
		
		//Sorting by BHA component
		Collections.sort(mapList, new MudVendorComparator());
		for(Iterator i = mapList.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			LookupItem item = (LookupItem) obj_array[1];
			if (!result.containsKey(item.getKey())) {
				result.put(item.getKey(), item);
			}
		}
		
		return result;
	}
	
	private class MudVendorComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				LookupItem in1 = (LookupItem) o1[1];
				LookupItem in2 = (LookupItem) o2[1];
				
				String f1 = in1.getValue().toString();
				String f2 = in2.getValue().toString();
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
}
