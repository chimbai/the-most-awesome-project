package com.idsdatanet.d2.drillnet.mudProperties;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.CasingTally;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.MudRheology;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class MudPropertiesCommandBeanListener extends EmptyCommandBeanListener  {
	
	private String rigStockTypeForCalcMudCost = null;
	
	public void setRigStockTypeForCalcMudCost(String stockType) {
		this.rigStockTypeForCalcMudCost = stockType;
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{

			// get the current dailyUid and operationUid from the session
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null) return;
			
			Date currentDate = daily.getDayDate();
			String operationUid = daily.getOperationUid();
			String stockType = this.rigStockTypeForCalcMudCost;
			Boolean calculateMudCostFromRigStock = false;
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			
			//if there is stockType set from bean property and when GWP is set to YES will get cost from rig stock / fluid stock  else will use mud properties cost
			if (StringUtils.isNotBlank(stockType) || "1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "calcMudCostFromMudChemical"))){
				calculateMudCostFromRigStock = true; 
				//stockType is default to fluids when calcMudCostFromMudChemical = yes and no stock type set from bean property  
				if (StringUtils.isBlank(stockType)){
					stockType = "fluids";
				}
				thisConverter.setReferenceMappingField(RigStock.class, "dailycost");
				
			}else {
				//to get mud cost from mud properties screen 
				calculateMudCostFromRigStock = false;
				stockType = null;
				thisConverter.setReferenceMappingField(MudProperties.class, "mudCost");
			}
	
			Double cumMudCost = 0.00;
			Double dailyMudCost = 0.00;
			
			//Calculate cum mud cost 
			cumMudCost = MudPropertiesUtil.calcCumMudCost(calculateMudCostFromRigStock, stockType, currentDate, operationUid);
			thisConverter.setBaseValue(cumMudCost);
			
			if (thisConverter.isUOMMappingAvailable()){
				commandBean.getRoot().setCustomUOM("@cumMudCost", thisConverter.getUOMMapping());
			}
			commandBean.getRoot().getDynaAttr().put("cumMudCost", thisConverter.getConvertedValue());
			
			// Calculate daily mud cost		
			dailyMudCost = MudPropertiesUtil.calcDailyMudCost(calculateMudCostFromRigStock, stockType, currentDate, operationUid);

			thisConverter.setBaseValue(dailyMudCost);
			if (thisConverter.isUOMMappingAvailable()){
				commandBean.getRoot().setCustomUOM("@dailyMudCost", thisConverter.getUOMMapping());
			}
			
			commandBean.getRoot().getDynaAttr().put("dailyMudCost", thisConverter.getConvertedValue());
			
			this.updateDailyMudCost(daily.getDailyUid(), dailyMudCost);

			
			//calculate the cumulative shaker cost
			
			//variable declaration
			Double cumshakercost = 0.00;
			//query statement to sum up the shaker cost till the selected date
			
			String[] paramsFields = {"todayDate", "operationUid"};
			Object[] paramsValues = new Object[2]; paramsValues[0] = currentDate; paramsValues[1] = operationUid;
			
			String strSql = "SELECT SUM(mud.shakerCost) as totalshakercost FROM MudProperties mud, Daily d WHERE (mud.isDeleted = false or mud.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and d.dailyUid = mud.dailyUid AND d.dayDate <= :todayDate AND mud.operationUid = :operationUid";
			List lstResultShakerCost = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			//query result and assign to cumshakercost
			Object ResultShakerCost = (Object) lstResultShakerCost.get(0);
			if (ResultShakerCost != null) cumshakercost = Double.parseDouble(ResultShakerCost.toString()) ;
			
			//get the field mapping of the shaker cost and assign the cumshakercost to it
			thisConverter.setReferenceMappingField(MudProperties.class, "shakerCost");		
			thisConverter.setBaseValue(cumshakercost);
			if (thisConverter.isUOMMappingAvailable()){
				commandBean.getRoot().setCustomUOM("@cumshakercost", thisConverter.getUOMMapping());
			}
			commandBean.getRoot().getDynaAttr().put("cumshakercost", thisConverter.getConvertedValue());
			
	}
	
	//Ticket: 20091210-1021-ylu 
	private void updateDailyMudCost(String dailyUid, Double dailyMudCost) throws Exception { 
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String strSql1 = "UPDATE ReportDaily SET mudcost =:dailyMudCost, totalmudcost =:dailyMudCost WHERE dailyUid =:dailyUid";
		String[] paramsFields1 = {"dailyMudCost","dailyUid"};
		Object[] paramsValues1 = {dailyMudCost, dailyUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);


	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		Map<String, CommandBeanTreeNode> children = commandBean.getRoot().getChild("MudProperties");
		for (Map.Entry<String, CommandBeanTreeNode> entry : children.entrySet()) {
			CommandBeanTreeNode node = entry.getValue();
			Object data = node.getData();
			if (data instanceof MudProperties) {
				MudProperties mudProperties = (MudProperties)data;
				String hql = "from MudRheology where (isDeleted is null or isDeleted = 0) and mudPropertiesUid=:mudPropertiesUid order by rpm asc";
				List<MudRheology> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "mudPropertiesUid", mudProperties.getMudPropertiesUid());
				Double counter = 0.0;
				if (result.size() > 0) {
					for (MudRheology mudRheology : result) {
						mudRheology.setEdmSequenceNo(counter);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(mudRheology);
						counter++;
					}
				}
			}
		}
	}
}
