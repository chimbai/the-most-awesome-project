package com.idsdatanet.d2.drillnet.cementJob;

import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * Custom lookuphandler for cementjob casingUid.
 * @author RYAU
 *
 */
public class CementJobCasingOdLookupHandler implements CascadeLookupHandler {
	private URI xmlLookup = null;
	
	public void setXmlLookup(URI xmlLookup) {
		this.xmlLookup = xmlLookup;
	}
	
	public URI getXmlLookup() {
		return this.xmlLookup;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		Map<String, LookupItem> casingLookup = LookupManager.getConfiguredInstance().getLookup(this.getXmlLookup().toString(), userSelection, null);
		
		//check parent wellbore
		List<Wellbore> parentWellboreList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid", "wellboreUid", userSelection.getWellboreUid());
		if(parentWellboreList != null && parentWellboreList.size() > 0) {
			for(Wellbore wellbore : parentWellboreList) {
				String[] paramsFields = {"parentWellboreUid", "wellboreUid"};
				Object[] paramsValues = {wellbore.getParentWellboreUid(), wellbore.getWellboreUid()};
				List<CasingSection> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and (wellboreUid = :parentWellboreUid or wellboreUid = :wellboreUid) and casingOd > 0 order by casingOd", paramsFields, paramsValues);
				if((list != null && list.size() > 0) && (casingLookup != null)) {
					for(CasingSection casingSection : list) {
						CustomFieldUom thisConverter = new CustomFieldUom((Locale) null, CasingSection.class, "casingOd");
						String strCasingOd 			 = thisConverter.formatOutputPrecision(casingSection.getCasingOd());
						String casingSectionUid 	 = casingSection.getCasingSectionUid();
						
						if(StringUtils.isNotBlank(strCasingOd) && casingLookup.containsKey(strCasingOd)) {
							LookupItem lookupItem = casingLookup.get(strCasingOd);
							if(lookupItem != null)
							{
								result.put(casingSectionUid, new LookupItem(casingSectionUid, lookupItem.getValue(), lookupItem.getKey()));
							}
						}
					}
				}
			}
		}else{
			List<CasingSection> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and casingOd > 0 order by casingOd", "operationUid", userSelection.getOperationUid());
			if((list != null && list.size() > 0) && (casingLookup != null)) {
				for(CasingSection casingSection : list) {
					CustomFieldUom thisConverter = new CustomFieldUom((Locale) null, CasingSection.class, "casingOd");
					String strCasingOd 			 = thisConverter.formatOutputPrecision(casingSection.getCasingOd());
					String casingSectionUid 	 = casingSection.getCasingSectionUid();
					
					if(StringUtils.isNotBlank(strCasingOd) && casingLookup.containsKey(strCasingOd)) {
						LookupItem lookupItem = casingLookup.get(strCasingOd);
						if(lookupItem != null)
						{
							result.put(casingSectionUid, new LookupItem(casingSectionUid, lookupItem.getValue(), lookupItem.getKey()));
						}
					}
				}
			}
		}
		
		return result;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		String sql ="select wellboreUid FROM Wellbore WHERE (isDeleted = false OR isDeleted is null)";
		List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		for (String wellboreUid : list) {
			Map<String, LookupItem> childLookup = this.getCascadeLookup(commandBean, null, userSelection, request, wellboreUid, null);
			if (childLookup.size()>0) {
				CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(wellboreUid);
				cascadeLookupSet.setLookup(new LinkedHashMap(childLookup));
				result.add(cascadeLookupSet);
			}
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
	}

	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, String key, LookupCache lookupCache)
			throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		CustomFieldUom thisConverter = new CustomFieldUom((Locale) null, CasingSection.class, "casingOd");
		Map<String, LookupItem> casingLookup = LookupManager.getConfiguredInstance().getLookup(this.getXmlLookup().toString(), userSelection, null);
		
	    String sql ="from CasingSection where (isDeleted = false or isDeleted is null) and wellboreUid = :wellboreUid and casingOd > 0 order by casingOd";
		List<CasingSection> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"wellboreUid"}, new String[] { key });
		if((list != null && list.size() > 0) && (casingLookup != null)) {
			for(CasingSection casingSection : list) {
				String strCasingOd 			 = thisConverter.formatOutputPrecision(casingSection.getCasingOd());
				String casingSectionUid 	 = casingSection.getCasingSectionUid();
				
				if(StringUtils.isNotBlank(strCasingOd) && casingLookup.containsKey(strCasingOd)) {
					LookupItem lookupItem = casingLookup.get(strCasingOd);
					if(lookupItem != null)
					{
						result.put(casingSectionUid, new LookupItem(casingSectionUid, lookupItem.getValue(), lookupItem.getKey()));
					}
				}
			}
		}
		return result;
	}
}