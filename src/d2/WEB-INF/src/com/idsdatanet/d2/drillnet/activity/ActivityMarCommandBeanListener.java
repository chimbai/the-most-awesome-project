package com.idsdatanet.d2.drillnet.activity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.ActivityMar;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RigTour;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.witsml.ActivityMergeUtils;

public class ActivityMarCommandBeanListener extends EmptyCommandBeanListener {
	private Boolean ignore24HourChecking = false;
	private Boolean showSystemMessageForActivity = true;
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		double totalActivityDuration = this.recalculateTotalDuration(commandBean);
		String operationHour = "24";
		if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "act_show_warning_if_not_24")))
		{	
			String newActivityRecordInputMode = null;
			if (request != null) {
				newActivityRecordInputMode = request.getParameter("newActivityRecordInputMode");
				UserSession session = UserSession.getInstance(request);
				if (session.getCurrentOperation()!=null) {
					operationHour = ActivityUtils.getOperationHour(session.getCurrentOperation());
				}
			}
			
			if (!("1".equals(newActivityRecordInputMode))){
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityMar.class, "activityDuration");
				thisConverter.setBaseValue(86400);
				if (StringUtils.isNotBlank(operationHour)) {
					Double totalHoursInSecond = Double.parseDouble(operationHour);
					if (!totalHoursInSecond.isNaN()) {
						totalHoursInSecond = totalHoursInSecond * 3600.0;
						thisConverter.setBaseValue(totalHoursInSecond);
					}
				}
				
				if (!thisConverter.getFormattedValue(totalActivityDuration).equals(thisConverter.getFormattedValue()) && !this.ignore24HourChecking){
					commandBean.getSystemMessage().addInfo("Total hours for activities in the day (" + thisConverter.getFormattedValue(totalActivityDuration) + ") is not " + operationHour + " hours.");
				}
			}
			
			if (isTimeOverlapped(commandBean)) {
				commandBean.getSystemMessage().addInfo("There is time overlapping / gaps between activities.");
			}
		}
		
		UserSession session = UserSession.getInstance(request);
		Operation currentOperation = session.getCurrentOperation();
		if (StringUtils.isNotBlank(currentOperation.getRigInformationUid())) {
			RigInformation rig = (RigInformation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, currentOperation.getRigInformationUid());
			List<RigTour> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from RigTour where rigInformationUid=:rigInformationUid and (isDeleted is null or isDeleted=0)", "rigInformationUid", currentOperation.getRigInformationUid());
			if (result == null || result.size() == 0) {
				commandBean.getSystemMessage().addError("There is no Rig Tour assigned to current Rig: " + rig.getRigName());
			}
		} else {
			commandBean.getSystemMessage().addError("Current operation has no rig assigned.");
		}
		List dwoms = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from DepotWellOperationMapping where (isDeleted is null or isDeleted=0) and wellUid=:wellUid and wellboreUid=:wellboreUid and (rigStateDefinitionUid IS NOT NULL AND rigStateDefinitionUid != '')", new String[] {"wellUid", "wellboreUid"}, new String[] {session.getCurrentWellUid(), session.getCurrentWellboreUid()});
		if (dwoms != null && dwoms.size() > 1) {
			commandBean.getSystemMessage().addError("There are more than one DepotWellOperationMapping being used in the current Wellbore. Please make sure there is only one mapping per wellbore.");
		}
	}
	
	private double recalculateTotalDuration(CommandBean commandBean) throws Exception {
		double totalActivityDuration = 0;
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityMar.class, "activityDuration");
		
		for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ActivityMar").values()) {
			ActivityMar activity = (ActivityMar) node.getData();
			
			if (activity != null && activity.getActivityDuration() != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {
				totalActivityDuration += activity.getActivityDuration();
			}
		}
		if (thisConverter.isUOMMappingAvailable()){
			commandBean.getRoot().setCustomUOM("@activityDuration", thisConverter.getUOMMapping());
		}
		commandBean.getRoot().getDynaAttr().put("activityDuration", totalActivityDuration);
		
		return totalActivityDuration;
	}
	
	private Boolean isTimeOverlapped(CommandBean commandBean) throws Exception {
		Date activityFirstStartDateTime = null;
		Date activityLastStartDateTime = null;
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityMar.class, "activityDuration");
		Double activityDuration = 0.0;
		Double activityActualDuration = 0.0;
		for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ActivityMar").values()) {
			ActivityMar activity = (ActivityMar) node.getData();
			if (activity.getIsDeleted() == null || !activity.getIsDeleted()) { 
				if (activity.getStartDatetime() != null && activity.getEndDatetime() != null) {						
					if (activityFirstStartDateTime == null) activityFirstStartDateTime = activity.getStartDatetime();	
					activityLastStartDateTime = activity.getEndDatetime();
					if (activity != null && activity.getActivityDuration() != null) {
						activityDuration += activity.getActivityDuration();
					}
				}
			}
		}
		
		thisConverter.setBaseValueFromUserValue(activityDuration);
		activityDuration=thisConverter.getBasevalue();
		int decimalPlaces = 1;
		BigDecimal bd = new BigDecimal(activityDuration);
		bd = bd.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
		activityDuration = bd.doubleValue();
		
		if (activityFirstStartDateTime != null && activityLastStartDateTime !=null) {	
			activityActualDuration = ActivityUtils.getActivityDurationInSecond(activityFirstStartDateTime, activityLastStartDateTime, thisConverter);
			if (activityActualDuration.compareTo(activityDuration) != 0) {
				return true;
			}
		}
		
		return false;
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
		if ("reprocessData".equalsIgnoreCase(invocationKey)) {
			UserSession session = UserSession.getInstance(request);
			UserSelectionSnapshot userSelection = commandBean.getCurrentUserSelectionSnapshot();
			
			String hql = "delete from ActivityMar where dailyUid=:dailyUid";
			String[] paramName = {"dailyUid"};
			String[] paramValue = {session.getCurrentDailyUid()};
			// permanent delete
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(hql, paramName, paramValue);
			
			String hql1 = "delete from Activity where dailyUid=:dailyUid and isRsd=true";
			String[] paramName1 = {"dailyUid"};
			String[] paramValue1 = {session.getCurrentDailyUid()};
			// permanent delete
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(hql1, paramName1, paramValue1);
			
			ActivityMergeUtils.populateMarAct(session.getCurrentDailyUid(), true);
		}else if ("clearData".equalsIgnoreCase(invocationKey)) {
				UserSession session = UserSession.getInstance(request);
				String hql = "delete from ActivityMar where dailyUid=:dailyUid";
				String[] paramName = {"dailyUid"};
				String[] paramValue = {session.getCurrentDailyUid()};
				// permanent delete
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(hql, paramName, paramValue);
			
		}
	}
	
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
		UserSession session = UserSession.getInstance(request);
		
		if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "disAllowSavingIfTotalHourMoreThan24"))) {
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ActivityMar.class, "activityDuration");
			//get operationHour set in operation screen 
			Double operationHour =  24.0;
			if (session.getCurrentOperation()!=null) {
				operationHour = Double.parseDouble(ActivityUtils.getOperationHour(session.getCurrentOperation()));
			}	
			
			Double totalActivityDuration = 0.0;
			Date newDate = new Date();
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("ActivityMar").values()) {
				ActivityMar activity = (ActivityMar) node.getData();
				
				// only calculate duration when it is save action 
				if(!node.getAtts().getAction().equals(Action.DELETE)) {
					if (activity != null && (activity.getIsDeleted() == null || !activity.getIsDeleted())) {					
						//need to calculate activity duration as calculation not yet done
						Date start = ActivityUtils.getPopulatedStartTime(activity, commandBean);
						Date end = CommonUtil.getConfiguredInstance().setDate(activity.getEndDatetime(), newDate);
						
						Double activityDuration = ActivityUtils.getActivityDurationInHour(start, end, thisConverter);						
						totalActivityDuration += activityDuration;	
					}
				}
			}
			
			//show error message when total activity Hour more than operation Hour
			if (totalActivityDuration > operationHour){	
				if(this.showSystemMessageForActivity)
				{
					commandBean.getRoot().setDirty(true);
					commandBean.getSystemMessage().addError("Total Activity Hours exceeds " + ActivityUtils.getOperationHour(session.getCurrentOperation()) + "hrs. Please check and correct the record." ,true, true);
					commandBean.setAbortFormProcessing(true);
				}
			  	   
			}
		}
		
	}
	
	
	
		
}
