package com.idsdatanet.d2.drillnet.dailyUidMaintenance;

import java.util.List;

import com.idsdatanet.d2.core.spring.DefaultBeanSupport;


public class DailyUidMaintenanceSetting extends DefaultBeanSupport{
	private List<String> ddrSetting = null;
	private List<String> dgrSetting = null;
	
	public static DailyUidMaintenanceSetting getConfiguredInstance() throws Exception {
		return getSingletonInstance(DailyUidMaintenanceSetting.class);
	}
	
	public void setDdrSetting(List<String> ddrSetting) {
		this.ddrSetting = ddrSetting;
	}
	
	public List<String> getDdrSetting() {
		return ddrSetting;
	}
	
	public void setDgrSetting(List<String> dgrSetting) {
		this.dgrSetting = dgrSetting;
	}
	
	public List<String> getDgrSetting() {
		return dgrSetting;
	}
}
