package com.idsdatanet.d2.drillnet.marineProperties;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.ExcelImportTemplateManager;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class MarinePropertiesCommandBeanListener extends EmptyCommandBeanListener {

    private ExcelImportTemplateManager excelImportTemplateManager = null;

    public ExcelImportTemplateManager getExcelImportTemplateManager() {
        return excelImportTemplateManager;
    }

    public void setExcelImportTemplateManager(ExcelImportTemplateManager excelImportTemplateManager) {
        this.excelImportTemplateManager = excelImportTemplateManager;
    }

    @Override
    public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from RigInformation where rigInformationUid = :id","id",userSelection.getRigInformationUid());
		if(rs.size() > 0){
			RigInformation rig = (RigInformation) rs.get(0);
			Integer num = rig.getNumsupport();
			if(num != null && num > 0){
				commandBean.setRowsOnCreate(com.idsdatanet.d2.core.model.MarineSupport.class, num);
			}
		}
	}

    @Override
    public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
        String action = request.getParameter("action");
        if (StringUtils.equals(ExcelImportTemplateManager.IMPORT_TEMPLATE_EXCEL, action)) {
            this.excelImportTemplateManager.importExcelFile((BaseCommandBean) commandBean, this, request, request.getParameter(ExcelImportTemplateManager.KEY_TEMPLATE), request.getParameter(ExcelImportTemplateManager.KEY_FILE), new String[] { "MarineProperties", "MarineSupport" });
        }
    }

    @Override
    public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
        if (ExcelImportTemplateManager.IMPORT_TEMPLATE_EXCEL.equalsIgnoreCase(invocationKey)) {
            this.excelImportTemplateManager.uploadFile(request, response, ExcelImportTemplateManager.KEY_FILE);
        }
        return;
    }
}
