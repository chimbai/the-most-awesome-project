package com.idsdatanet.d2.drillnet.user;

import java.io.StringWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.AclGroup;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * @author Carl Anderson
 *
 * Cascade lookup for taking in a key which is an aclGroupUid and finding all the users with that aclGroupUid in UserAclGroup
 */
public class UserCascadeLookupHandler implements CascadeLookupHandler {

	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
			
		// Get all the relevant userUids
		String test = "SELECT DISTINCT uag.userUid FROM UserAclGroup uag WHERE uag.aclGroupUid='" + key + "' AND (isDeleted = false or isDeleted is null)";
		List<String> userUids = ApplicationUtils.getConfiguredInstance().getDaoManager().find(test);
		if(userUids != null && userUids.size() > 0){
			StringWriter strSql = new StringWriter();
			strSql.append("FROM User WHERE userUid IN(");
			
			// Iterate through the uid and retrieve the users
			Iterator<String> iter = userUids.iterator();
			while(iter.hasNext()){
				strSql.append("'" + iter.next() + "'");
				
				if(iter.hasNext()){
					strSql.append(", ");
				}
			}
			
			strSql.append(") AND (isDeleted = false or isDeleted is null)");
			
			// Retrieve the list of users
			List<User> users = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql.toString());
			if(users != null && users.size() > 0){
				for(User user : users){
					result.put(user.getUserUid(), new LookupItem(user.getUserUid(), user.getFname()));
				}
			}
			
		}
		
		
		return result;
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		//List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		CascadeLookupSet[] result = null;
		
		String strSql = "FROM AclGroup WHERE (isDeleted = false or isDeleted is null)";
		List<AclGroup> aclGroups = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql.toString());
		if(aclGroups != null && aclGroups.size() > 0){
			result = new CascadeLookupSet[aclGroups.size()];
			for(int i = 0;i < aclGroups.size();i++){
				AclGroup aclGroup = aclGroups.get(i);
				CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(aclGroup.getAclGroupUid());
				cascadeLookupSet.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, aclGroup.getAclGroupUid(), null)));
				result[i] = cascadeLookupSet;
			
			}
		}
		
		return result;
	}

	
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		return null;
	}

}
