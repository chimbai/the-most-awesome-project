package com.idsdatanet.d2.drillnet.report;

import com.idsdatanet.d2.core.model.ReportFiles;

public class ReportJobResult {
	private ReportFiles reportFile = null;
	private String reportFilePath = null;
	private boolean isError = false;
	private String errorMsg = null;
	
	public String getErrorMsg(){
		return this.errorMsg;
	}
	
	public void setErrorMsg(String value){
		this.errorMsg = value;
	}
	
	public boolean isError(){
		return this.isError;
	}
	
	public void setIsError(boolean value){
		this.isError = value;
	}
	
	public String getReportFilePath(){
		return this.reportFilePath;
	}
	
	public ReportFiles getReportFile(){
		return this.reportFile;
	}
	
	public void setReportFile(ReportFiles value){
		this.reportFile = value;
	}
	
	public void setReportFilePath(String value){
		this.reportFilePath = value;
	}
}
