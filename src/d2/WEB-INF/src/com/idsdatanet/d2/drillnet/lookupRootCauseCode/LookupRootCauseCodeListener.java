package com.idsdatanet.d2.drillnet.lookupRootCauseCode;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.LookupRootCauseCode;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LookupRootCauseCodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor{

	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		String selectedOperationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
		
		Object object = node.getData();
		if (object instanceof LookupRootCauseCode) {
			LookupRootCauseCode data = (LookupRootCauseCode) object;
			data.setOperationCode(selectedOperationCode);
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof LookupRootCauseCode) {
			LookupRootCauseCode lookupRootCauseCode = (LookupRootCauseCode) object;
			lookupRootCauseCode.setSysPaddedCode(WellNameUtil.getPaddedStr(lookupRootCauseCode.getShortCode()));
			lookupRootCauseCode.setSysPaddedEquipmentCode(WellNameUtil.getPaddedStr(lookupRootCauseCode.getJobTypeShortCode()));
			lookupRootCauseCode.setSysPaddedSubEquipmentCode(WellNameUtil.getPaddedStr(lookupRootCauseCode.getRcTypeId()));
		}
		
	}

	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		
		if (commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN) {
			String selectedOperationCode = (String) commandBean.getRoot().getDynaAttr().get("operationCodeFilter");
			String selectedInternalCode = (String) commandBean.getRoot().getDynaAttr().get("internalCodeFilter");
			if (StringUtils.isNotBlank(selectedOperationCode)){
				customCondition+=(customCondition.length()>0?" AND ":"")+" operationCode =:operationCode ";
				query.addParam("operationCode", selectedOperationCode);
			}
			else
			{
				customCondition = " 1 = 1";
			}
			if (StringUtils.isNotBlank(selectedInternalCode)){
				customCondition+=(customCondition.length()>0?" AND ":"")+" internalCode =:internalCode ";
				query.addParam("internalCode", selectedInternalCode);
			}
		}
		
		if (userSelection.getCustomProperty("reportOperationCodesFilter")!=null) {
			List<String> reportOperationCodesFilter = (List<String>) userSelection.getCustomProperty("reportOperationCodesFilter");
			if (reportOperationCodesFilter.size()>0) {
				customCondition+=(customCondition.length()>0?" AND ":"")+" (operationCode in (:reportOperationCodesFilter) or operationCode='' or operationCode is null) ";
				query.addParam("reportOperationCodesFilter", reportOperationCodesFilter);
			}
		}
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
