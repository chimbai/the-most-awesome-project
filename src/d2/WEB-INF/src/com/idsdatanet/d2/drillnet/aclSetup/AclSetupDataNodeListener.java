package com.idsdatanet.d2.drillnet.aclSetup;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;

import com.idsdatanet.d2.core.model.AclGroup;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class AclSetupDataNodeListener extends EmptyDataNodeListener {
	
	public void afterDataNodeDelete(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		if (node.getData() instanceof AclGroup) {
			commandBean.getRoot().resetPersistedDynamicAttribute("groupUid");
			commandBean.getRoot().resetPersistedDynamicAttribute("aclGroupUid");
			
			// also delete all users in this acl group
			AclGroup group = (AclGroup) node.getData();
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("update User set isDeleted = true where aclGroupUid = :aclGroupUid", new String[] {"aclGroupUid"}, new Object[] {group.getAclGroupUid()});
		}
	}

	@Override
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, 
			DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		super.beforeDataNodeSaveOrUpdate(commandBean, node, session, status, request);
		
		// If all viewable items have been disabled the process must be stopped otherwise
		// the user can not see any menu
		if (node.getData() != null && node.getData() instanceof AclGroup){
			AclGroup group = (AclGroup) node.getData();
			if (!BooleanUtils.isTrue(group.getCanViewWells()) && !BooleanUtils.isTrue(group.getCanViewTour())){
				commandBean.getSystemMessage().addError("All views have been disabled, this cannot be saved as users in this group will not have a menu. Please set the Can Views column accordingly.");
				status.setContinueProcess(false, true);
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		if (commandBean.getFlexClientControl() != null) {
			commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		}
	}

}
