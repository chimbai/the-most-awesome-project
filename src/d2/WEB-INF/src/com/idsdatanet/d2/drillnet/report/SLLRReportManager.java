package com.idsdatanet.d2.drillnet.report;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.view.FreemarkerUtils;

public class SLLRReportManager {
	public static final int RESPONSE_STATUS_SUCCESS = 1;
	public static final int RESPONSE_STATUS_ERROR = 2;
	protected SLLRReportModule jasperJsonModule = null;
	private List<SLLRReportJobParams> esReportJobs;
	
	public void setSllrReportModule(SLLRReportModule jasperJsonModule) {
		this.jasperJsonModule = jasperJsonModule;
	}
	
	public void triggerSLLReport(UserSession session, HttpServletRequest request, HttpServletResponse response) {
		if (this.esReportJobs == null) this.esReportJobs = new ArrayList<SLLRReportJobParams>();
		SLLRReportJobParams jobData = null;
		int status = RESPONSE_STATUS_SUCCESS;
		String responseMessage = "";
		
		try {
			jobData = this.jasperJsonModule.submitReportJob(session,request);
			this.esReportJobs.add(jobData);
		} catch (Exception e) {
			status = RESPONSE_STATUS_ERROR;
			responseMessage = "Error Generating Report: " + e.getMessage();
			e.printStackTrace();
		} finally {
			try {
				getAllReports(response,session,status,responseMessage);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void getAllReports(HttpServletResponse response, UserSession session, int status, String responseMessage) throws Exception {
		JSONObject jObject = new JSONObject();
		JSONArray sllrReportFile = getSLLRReportFileList();
		JSONObject sllrReportJob = getSLLRReportJobList();
		jObject.put("status", status);
		jObject.put("responseMessage", responseMessage);
		jObject.put("baseUrl", session.getClientBaseUrl());
		jObject.put("SLLRReportFile", sllrReportFile);
		jObject.put("SLLRReportJob", sllrReportJob);
		output2Response(jObject, response);
	}

	private JSONArray getSLLRReportFileList() {
		try {
			checkSLLRReportFileList();
			
			String queryString = "SELECT b.reportFilesUid, b.isPrivate, b.displayName, b.reportUserUid, b.reportFile " +
								 "FROM ReportFiles b " +
								 "WHERE (b.isDeleted=false OR b.isDeleted is null) " +
								 "AND b.reportType = :reportType ORDER BY b.dateGenerated DESC";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "reportType", jasperJsonModule.getReportType());
			if (lstResult.size() > 0) {
				JSONArray jArray = new JSONArray();
				
				for (Object objResult: lstResult) {
					Object[] obj = (Object[]) objResult;
					
					String reportFilesUid = "";
					String isPrivate = "";
					String displayName = "";
					String reportUserUid = "";
					String reportFile = "";
									
					if (obj[0] != null && StringUtils.isNotBlank(obj[0].toString())) reportFilesUid = obj[0].toString();
					if (obj[1] != null && StringUtils.isNotBlank(obj[1].toString())) isPrivate = obj[1].toString();
					if (obj[2] != null && StringUtils.isNotBlank(obj[2].toString())) displayName = obj[2].toString();
					if (obj[3] != null && StringUtils.isNotBlank(obj[3].toString())) reportUserUid = obj[3].toString();
					if (obj[4] != null && StringUtils.isNotBlank(obj[4].toString())) reportFile = obj[4].toString();
					
					File f = new File(ReportUtils.getFullOutputFilePath(reportFile));
					if(f.exists()) {
						Date timeReportFileCreated = new Date(f.lastModified());
						
						JSONObject jObjd = new JSONObject();
						jObjd.put("fileUid", reportFilesUid);
						jObjd.put("fileName", displayName);
						jObjd.put("fileExtension", getFileExtension(reportFile));
						jObjd.put("fileIsPrivate", isPrivate);
						jObjd.put("newlyGeneratedFlag", getNewlyGeneratedFlag(timeReportFileCreated));
						jObjd.put("timeSinceCreated", getTimeSinceReportFileCreated(timeReportFileCreated));
						jObjd.put("generatedBy", getUserName(reportUserUid));
						jArray.add(jObjd);
					}					
				}
				return jArray;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private JSONObject getSLLRReportJobList() {
		try {
			JSONObject jObjd2 = new JSONObject();
			JSONArray jArray = new JSONArray();
			int runningJobs = 0;
			
			if (this.esReportJobs != null) {
				for (Iterator<SLLRReportJobParams> itr = this.esReportJobs.iterator(); itr.hasNext();) {
					SLLRReportJobParams jobData = itr.next();
					ReportJobResult jr = jobData.getReportJobParams().getReportJobResult().get(0);
					String userUid = jobData.getJobContext().getUserContext().getUserSelection().getUserUid();

					int jobStatus = jobData.getJobContext().getJobStatus();
					if (jobStatus!=3) {
						runningJobs += 1;
						JSONObject jObjd = new JSONObject();
						jObjd.put("jodId", jobData.getJobContext().getJobId());
						jObjd.put("jobStatus", jobStatus);
						jObjd.put("fileName", FilenameUtils.getBaseName(jobData.getReportJobParams().getLocalOutputFile()));
						jObjd.put("fileIsPrivate", getIsPrivate(jr));
						jObjd.put("generatedBy", getUserName(userUid));
						jArray.add(jObjd);
					} else {
						itr.remove();
					}
				}
			}
			
			jObjd2.put("isAllJobEnded", (runningJobs==0 ? true : false));
			jObjd2.put("reportJob", jArray);
			return jObjd2;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void updateSLLRReportFile(HttpServletResponse response, String reportFilesUid, Boolean isPrivate) throws Exception {
		Boolean success = updateSLLRReportVisibility(reportFilesUid, isPrivate);
		String responseMessage = (success ? "successful" : "Error: Fail to update report file!");
		sendResponseOnStatus(response, success, responseMessage, reportFilesUid);
	}
	
	public void deleteSLLRReportFile(HttpServletResponse response, String reportFilesUid) throws Exception {
		Boolean success = delSLLRReportFile(reportFilesUid);
		String responseMessage = (success ? "successful" : "Error: Fail to delete report file!");
		sendResponseOnStatus(response, success, responseMessage, reportFilesUid);
	}

	public boolean updateSLLRReportVisibility(String reportFilesUid, Boolean isPrivate) {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		try {					
			String strSql1 = "UPDATE ReportFiles SET isPrivate = :isPrivate WHERE reportFilesUid =:reportFilesUid";
			String[] paramsFields1 = {"reportFilesUid", "isPrivate"};
			Object[] paramsValues1 = {reportFilesUid, isPrivate};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Boolean delSLLRReportFile(String reportFilesUid) {
		try {
			if(reportFilesUid != null) {	
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);		
					
				String strSql1 = "FROM ReportFiles WHERE reportFilesUid =:reportFilesUid";
				List<Object>lst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, "reportFilesUid", reportFilesUid, qp);
				
				if(lst.size() > 0) {
					ReportFiles reportFile = (ReportFiles) lst.get(0);
					this.delete(reportFile.getReportFilesUid(), reportFile.getReportFile());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Boolean checkSLLRReportFileList() {
		try {
			String queryString = "SELECT reportFilesUid, reportFile FROM ReportFiles WHERE (isDeleted=false OR isDeleted is null) " +
					 			"AND reportType = :reportType ORDER BY dateGenerated DESC";
			List lst = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "reportType", jasperJsonModule.getReportType());
			
			int displayNum = jasperJsonModule.getNumberOfReportToDisplay();
			if(lst.size() > displayNum) {
				for(int i=displayNum; i<lst.size(); i++) {
					Object[] obj = (Object[]) lst.get(i);
					this.delete(obj[0].toString(), obj[1].toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void delete(String reportFilesUid, String reportFilePath) throws Exception {
		if (StringUtils.isNotBlank(reportFilePath)) {
			File reportFile = new File(ReportUtils.getFullOutputFilePath(reportFilePath));
			if(reportFile.exists()) reportFile.delete();
		}
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam("UPDATE ReportFiles SET isDeleted = true WHERE reportFilesUid = :reportFilesUid", new String[] {"reportFilesUid"}, new Object[] {reportFilesUid});
	}
	
	public void sendResponseOnStatus(HttpServletResponse response, Boolean success, String responseMessage, String reportFilesUid) throws Exception {
		JSONObject jObject = new JSONObject();
		jObject.put("success", success);
		jObject.put("responseMessage", responseMessage);
		jObject.put("reference", reportFilesUid);
		output2Response(jObject, response);
	}
	
	public void output2Response(JSONObject jsonObj, HttpServletResponse response) throws IOException {
		response.setCharacterEncoding("utf-8");
		Writer writer = response.getWriter();
		jsonObj.write(writer);
		writer.close();
	}
	
	public String getTimeSinceReportFileCreated(Date endTime) {
		if(endTime == null) return null;
		return FreemarkerUtils.formatDurationSince(endTime);
	}
	
	public boolean getNewlyGeneratedFlag(Date timeReportFileCreated){
		if(timeReportFileCreated == null) return false;
		return FreemarkerUtils.isDocumentNewlyGenerated(timeReportFileCreated);
	}
	
	public Boolean getIsPrivate(ReportJobResult jr) {
		if(jr.getReportFile() == null) return false;
		return jr.getReportFile().getIsPrivate();
	}
	
	public String getUserName(String userUid) throws Exception {
		if (StringUtils.isNotBlank(userUid)) {
			User user = (User) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(User.class, userUid);
			if (user == null) return "";
			if (user.getFname()!=null) {
				return user.getFname();
			}
			return user.getUserName();
		}
		return "";
	}
	
	public String getFileExtension(String file) {
		if (file == null) return null;
		int i = file.lastIndexOf(".");
		if (i != -1) {
			return file.substring(i + 1);
		} else {
			return null;
		}
	}
}
