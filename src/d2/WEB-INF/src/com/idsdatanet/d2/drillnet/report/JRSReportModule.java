package com.idsdatanet.d2.drillnet.report;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.jasper.JRSReportConfig;
import com.idsdatanet.d2.core.report.jasper.JRSReportJob;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class JRSReportModule extends DDRReportModule {
	
	private JRSReportConfig jrsReportConfiguration = null;

	public JRSReportConfig getJrsReportConfiguration() {
		return jrsReportConfiguration;
	}
	
	public void setJrsReportConfiguration(JRSReportConfig jrsReportConfiguration) {
 		this.jrsReportConfiguration = jrsReportConfiguration;
 	}

	@Override
	protected D2Job createReportJob(String xsl,
			String output, ReportDataGenerator[] generators, String reportType,
			UserContext userContext, Map runtimeContext,
			ReportJobParams reportJobData) throws Exception {
		
		return new JRSReportJob(xsl, output, new ByteArrayOutputStream(), generators, reportType, userContext, this.jrsReportConfiguration);
	}

	@Override
	public String getOutputFileExtension() throws Exception {	
		return this.jrsReportConfiguration.getJrExecutionRequest().getOutputFormat();
	}
	
	synchronized protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		
		ReportJobParams params = new ReportJobParams();
		if(params.isUserSessionReportJob() && this.isUserSessionReportJobRunning()) throw new Exception("Current job is still running");
		
		List<String> list = null;
		if (this.isShowAllDays()) {
			list = this.getSelectedDaysForShowAllDays(commandBean);
		} else {
			list = this.getSelectedDays(commandBean);
		}
		
		if (list != null && list.size() > 0) {
			List<UserSelectionSnapshot> selections = new ArrayList<UserSelectionSnapshot>();
			for(String dailyUid: list){
				selections.add(UserSelectionSnapshot.getInstanceForCustomDaily(session, dailyUid.toString()));
			}
			return this.submitReportJob(selections, params);
		} else {
			if (this.isSelectedDayReportDailyAllowed(session, session.getCurrentDailyUid())) {
				return super.submitReportJob(UserSelectionSnapshot.getInstanceForCustomDaily(session, session.getCurrentDailyUid()), this.getParametersForReportSubmission(session, request, commandBean));		
			} else {
				commandBean.getSystemMessage().addError("No data fit the selected criteria therefore unable to generate report.");
				return null;
			}
		}
	}
	
	private Boolean isSelectedDayReportDailyAllowed(UserSession session, String dailyUid) throws Exception {
		if (dailyUid==null) return false;
		
		List<ReportDaily> list = this.getAllowedReportDaily(session);
		if (list==null) return false;
		if (list.size()==0) return false;
		for(ReportDaily rec:list){
			if (rec.getDailyUid().equals(dailyUid)) return true;
		}
		return false;
	}
	
	private List<ReportDaily> getAllowedReportDaily(UserSession session) throws Exception {
		Operation thisOperation = session.getCurrentOperation();
		String sortOrder = this.getReportDateChooserSortOrder();

		if (thisOperation == null) return null;
		
		String dailyType = this.getDailyType(new UserSelectionSnapshot(session));
		if(dailyType == null){
			dailyType = this.getDailyType();
			//for tight hole mgt and tight hole geo mgt report
			if ("MGTTH".equals(super.getReportType())) {
				dailyType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(thisOperation.getOperationUid());
			}
		}
		return CommonUtil.getConfiguredInstance().getDailyForOperation(thisOperation.getOperationUid(), dailyType, sortOrder);
	}
	
	protected String getJasperReportUnitUri(UserContext userContext, ReportJobParams reportJobParams) throws Exception{
		
		String jrxmlSource = this.jrsReportConfiguration.getJrExecutionRequest().getReportUnitUri();
		if(this.jasper_report && StringUtils.isNotBlank(jrxmlSource)){
			if(StringUtils.isBlank(jrxmlSource)) throw new Exception("Jrxml report path must not be null");
			return jrxmlSource;
		} else {
			return null;
		}
	}
	
}
