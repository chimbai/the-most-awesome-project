package com.idsdatanet.d2.drillnet.multiWellStt;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.InetAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.SysLogSendToTown;
import com.idsdatanet.d2.core.stt.STTActionCallback;
import com.idsdatanet.d2.core.stt.STTComConfig;
import com.idsdatanet.d2.core.stt.STTDataDirection;
import com.idsdatanet.d2.core.stt.STTEngine;
import com.idsdatanet.d2.core.stt.STTJobConfig;
import com.idsdatanet.d2.core.stt.STTStatus;
import com.idsdatanet.d2.core.stt.STTTransactionType;
import com.idsdatanet.d2.core.stt.jaxb.SttStatus;
import com.idsdatanet.d2.core.util.xml.JAXBContextManager;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.SystemExceptionHandler;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;
/**
 *
 * @author lling
 *
 */
public class MultiWellSTTActionHandler implements ActionHandler {
	
	private STTComConfig comConfig;
	private List<STTJobConfig> jobConfigs;
	
	public MultiWellSTTActionHandler(STTComConfig comConfig, List<STTJobConfig> jobConfigs) {
		this.comConfig = comConfig;
		this.jobConfigs = jobConfigs;
	}
	
	private String nullToEmptyString(Object value) {
		if (value==null) return "";
		return value.toString();
	}
	
	public ActionHandlerResponse process(BaseCommandBean commandBean,
			UserSelectionSnapshot userSelection, AclManager aclManager,
			HttpServletRequest request, CommandBeanTreeNode node, String key)
			throws Exception {
		
		STTStatus status = new STTStatus(); 
		SysLogSendToTown sysLog = new SysLogSendToTown();
		
		String stringResponseBody = null;
		String sysMsg = "";
		try {
			final long start = System.currentTimeMillis();
			
			// load and marshal
			File f = STTEngine.getConfiguredInstance().marshal(jobConfigs, userSelection, status, STTEngine.MARSHALLING_MODE_STT, false);
			if (f == null) {
				throw new Exception("Failed to marshal data");
			}
			sysLog.setFilename(f.getName());
			
			// extra request parameters, mainly for logging at the other end
			Map<String, String> requestParameters = new HashMap<String, String>();
			requestParameters.put("groupUid", StringUtils.defaultString(userSelection.getGroupUid()));
			requestParameters.put("wellUid", StringUtils.defaultString(userSelection.getWellUid()));
			requestParameters.put("wellboreUid", StringUtils.defaultString(userSelection.getWellboreUid()));
			requestParameters.put("operationUid", StringUtils.defaultString(userSelection.getOperationUid()));
			requestParameters.put("rigInformationUid", StringUtils.defaultString(userSelection.getRigInformationUid()));
			requestParameters.put("dailyUid", StringUtils.defaultString(userSelection.getDailyUid()));
			for (STTJobConfig jobConfig : jobConfigs) {
				requestParameters.put("jobConfigBeanNames", StringUtils.defaultString(jobConfig.getBeanName()));
			}
			requestParameters.put("destinationUrl", StringUtils.defaultString(comConfig.getDestinationUrl()));
			requestParameters.put("scheduled", "false");
			requestParameters.put("userName", UserSession.getInstance(request).getUserName());
			requestParameters.put("hostName", InetAddress.getLocalHost().getHostName());
			
			// send data
			stringResponseBody = STTEngine.getConfiguredInstance().send(comConfig, "/webservice/sendtotownservice.html", f, requestParameters, status);
			
			// parse response
			JAXBContext jc = JAXBContextManager.getContext("com.idsdatanet.d2.core.stt.jaxb");
			Unmarshaller m = jc.createUnmarshaller();
			SttStatus sttStatus = (SttStatus) m.unmarshal(new ByteArrayInputStream(stringResponseBody.getBytes()));
			
			if (sttStatus.getErrors() != null && sttStatus.getErrors().getError().size() > 0) {
				sysLog.setStatus(STTStatus.FAIL);
				for (String error : sttStatus.getErrors().getError()) {
					//commandBean.getSystemMessage().addError("Send to town failed:\n" + error);
					sysMsg = error;
				}
				
			} else {
				sysLog.setStatus(STTStatus.OK);
				//commandBean.getSystemMessage().addInfo("Send to town completed");
				
				for (STTJobConfig jobConfig : jobConfigs) {
					if (jobConfig.getActionCallbacks() != null) {
						for (STTActionCallback actionCallback : jobConfig.getActionCallbacks()) {
							if (actionCallback != null) {
								actionCallback.afterSendToTownCompleted(userSelection);
							}
						}
					}
				}	
				sysMsg = "Send to town completed";
			}
			
			status.log("Elapsed time: " + (System.currentTimeMillis() - start) + "ms");
			
		} catch (javax.xml.bind.UnmarshalException e) {
			status.log("Failed to parse response from town:\n" + stringResponseBody);
			sysLog.setStatus(STTStatus.FAIL);
			//commandBean.getSystemMessage().addError("Send to town failed:\nFailed to parse response from town");
			sysMsg = "Failed to parse response from town";
		} catch (Exception e) {
			status.log("Caught exception:\n" + SystemExceptionHandler.printAsString(e));
			sysLog.setStatus(STTStatus.FAIL);
			//commandBean.getSystemMessage().addError("Send to town failed:\n" + e.getMessage());
			sysMsg = e.getMessage();
		} finally {
			if ("OK".equals(sysLog.getStatus())) setStatus(commandBean, userSelection.getOperationUid(), "1", sysMsg);
			else {
				setStatus(commandBean, userSelection.getOperationUid(), "0", sysMsg);
			}
			
			// logging
			sysLog.setGroupUid(userSelection.getGroupUid());
			sysLog.setWellUid(userSelection.getWellUid());
			sysLog.setWellboreUid(userSelection.getWellboreUid());
			sysLog.setOperationUid(userSelection.getOperationUid());
			sysLog.setRigInformationUid(userSelection.getRigInformationUid());
			sysLog.setDailyUid(userSelection.getDailyUid());
			sysLog.setJobconfigs(STTJobConfig.jobConfigsToString(jobConfigs));
			sysLog.setTransactionDatetime(new Date());
			sysLog.setTransactionType(STTTransactionType.SEND_TO_TOWN);
			sysLog.setDataDirection(STTDataDirection.OUTGOING);
			sysLog.setIsScheduled(false);
			sysLog.setRemoteAddr(comConfig.getDestinationUrl());
			sysLog.setTransactionLog(status.getTransactionLog().toString());
			sysLog.setUserName(UserSession.getInstance(request).getUserName());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(sysLog);
			
			status.dispose();
		}
		return new ActionHandlerResponse(true, false);
	}

	public ActionHandlerResponse process(BaseCommandBean commandBean,
			UserSession userSession, AclManager aclManager,
			HttpServletRequest request, CommandBeanTreeNode node, String key)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void setStatus(BaseCommandBean commandBean, String operationUid, String sttStatus, String sysMsg) throws Exception{
		String status = (String) nullToEmptyString(commandBean.getRoot().getDynaAttr().get("status"));
		String ops = (String) nullToEmptyString(commandBean.getRoot().getDynaAttr().get("ops"));
		if (status != null){
			status += (StringUtils.isNotBlank(status)?",":"") + sttStatus ;
		}
		if (ops != null){
			ops += (StringUtils.isNotBlank(ops)?",":"") + operationUid;
		}
		commandBean.getRoot().getDynaAttr().put("status", status);
		commandBean.getRoot().getDynaAttr().put("ops", ops);
		commandBean.getRoot().getDynaAttr().put("sysMsg", sysMsg);	
	}
	
}
