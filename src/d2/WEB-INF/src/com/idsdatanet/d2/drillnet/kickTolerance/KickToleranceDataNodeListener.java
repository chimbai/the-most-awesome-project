package com.idsdatanet.d2.drillnet.kickTolerance;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.KickTolerance;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class KickToleranceDataNodeListener extends EmptyDataNodeListener{
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();		
		if (object instanceof KickTolerance) {
			//GET REPORT DAILY FIELDS
			KickToleranceUtils.getRelatedInfo(node, userSelection, request);
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {	
		
		Object object = node.getData();		
		if (object instanceof KickTolerance) {
			//GET REPORT DAILY FIELDS			
			KickToleranceUtils.getRelatedInfo(node, userSelection, request);			
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		_afterDataNodeProcesses(commandBean, node, session, request);
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		_afterDataNodeProcesses(commandBean, node, session, request);
	}
	
	private void _afterDataNodeProcesses(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request) throws Exception {
		//TO PERFORM SCREEN REFRESH 
		if(commandBean.getFlexClientControl() != null){
			commandBean.getFlexClientControl().setReloadParentHtmlPage();
		}
	}
	
	
}
