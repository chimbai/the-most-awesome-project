package com.idsdatanet.d2.drillnet.well;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;

public class ActionManager implements com.idsdatanet.d2.core.web.mvc.ActionManager{
	private DeleteHandler deleteHandler = new DeleteHandler();
	
	private class DeleteHandler implements com.idsdatanet.d2.core.web.mvc.ActionHandler{
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			//ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWell(((Well) node.getData()).getWellUid());
			ApplicationUtils.getConfiguredInstance().setDeleteFlagOnWell((Well) node.getData());
			return new ActionHandlerResponse(true, false, null, BaseCommandBean.OPERATION_PERFORMED_DELETE);
		}
	}
	
	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if(node.getData() instanceof com.idsdatanet.d2.core.model.Well){
			if(com.idsdatanet.d2.core.web.mvc.Action.DELETE.equals(action)){
				return this.deleteHandler;
			}
		}
		return null;
	}
}
