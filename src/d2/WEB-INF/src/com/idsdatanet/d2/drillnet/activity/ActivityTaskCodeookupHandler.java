package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityTaskCodeookupHandler implements LookupHandler{

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {

		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		
		String operationalUnit = "";
		String operationalCode = "";
		String operationCode = "";
		String conditions = null;
		
		if(StringUtils.isNotBlank(operation.getOperationalCode())) operationalCode = "%" + operation.getOperationalCode() + "%";
		if(StringUtils.isNotBlank(operation.getOperationalUnit())) operationalUnit = operation.getOperationalUnit();
		if(StringUtils.isNotBlank(operation.getOperationCode())) operationCode = operation.getOperationCode();
		
		conditions = "ltc.operationalUnit = :operationalUnit AND (ltc.operationalCode like :operationalCode OR ltc.operationalCode = '' OR ltc.operationalCode is null)";
			
		if(StringUtils.isBlank(operationalCode) && StringUtils.isBlank(operationalUnit)) {
			conditions = "(ltc.operationalUnit = :operationalUnit OR ltc.operationalUnit is null) AND (ltc.operationalCode like :operationalCode OR ltc.operationalCode is null)";
		} 
		else if(operationalCode == null || StringUtils.isBlank(operationalCode)) {
			conditions = "ltc.operationalUnit = :operationalUnit AND (ltc.operationalCode like :operationalCode OR ltc.operationalCode is null)";
		}	
		
		List<Object[]> listTaskCodeResults = new ArrayList<Object[]>();
		
		String[] paramsFields = {"operationCode", "operationalUnit", "operationalCode"};		
 		Object[] paramsValues = {operationCode, operationalUnit, operationalCode};
		
		String strSql = "SELECT ltc.shortCode, ltc.name, ltc.lookupTaskCodeUid FROM LookupTaskCode ltc WHERE (ltc.isDeleted = false or ltc.isDeleted is null) AND " +
						conditions + " AND ltc.operationCode = :operationCode ORDER BY name";
		listTaskCodeResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (listTaskCodeResults.size() > 0) {
			for (Object[] listTaskCodeResult : listTaskCodeResults) {
				if (listTaskCodeResult[0] != null && listTaskCodeResult[1] != null) {
					
					String key = listTaskCodeResult[0].toString();
					String value = listTaskCodeResult[1].toString() + " (" + listTaskCodeResult[0].toString() + ")";
					String lookupTaskCodeUid = listTaskCodeResult[2].toString();
					
					LookupItem item = new LookupItem(key, value);
					item.setTableUid(lookupTaskCodeUid);
					result.put(key, item);
				}
			}
		}
		
		return result;
	}
}
