package com.idsdatanet.d2.drillnet.formation;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.beanutils.PropertyUtils;
import com.idsdatanet.d2.core.model.BasinFormation;
import com.idsdatanet.d2.core.model.TargetZone;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class TargetZoneCommandBeanListener extends EmptyCommandBeanListener  {
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(targetCommandBeanTreeNode != null){
			if(TargetZone.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())){
				if("zoneName".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					TargetZone thisTargetZone = (TargetZone) targetCommandBeanTreeNode.getData();	
					String zoneName = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "zoneName");	
					UserSession getSession = UserSession.getInstance(request);
					String basin, basinFormationUid;
							
					basin = getSession.getCurrentWell().getBasin();
					if (basin != null)
					{
						//to retrieve the age and description from basin_formation and auto populate to target_zone based on formation selected
						String strSql = "select basinFormationUid from BasinFormation where (isDeleted = false or isDeleted is null) and basinUid = :basinUid and name = :name";
						String[] paramsFields = {"basinUid", "name"};
						Object[] paramsValues = {getSession.getCurrentWell().getBasin(), zoneName};
						List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);	
						if (!lstResult.isEmpty())
						{
							Object thisResult = (Object) lstResult.get(0);
							if(thisResult != null) 
								{
									basinFormationUid = thisResult.toString();
									BasinFormation formation = (BasinFormation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(BasinFormation.class, basinFormationUid);
									
									if (formation != null && (formation.getIsDeleted() == null || !formation.getIsDeleted())) {
										if (formation.getAgeName() != null) {
											thisTargetZone.setFormationAge(formation.getAgeName());
										}		
										if (formation.getDescription() != null) {
											thisTargetZone.setEubFormationCode(formation.getDescription());
										}
									}
								}
						}
					}
				}
			}
		}
	}
}
