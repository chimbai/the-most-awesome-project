package com.idsdatanet.d2.drillnet.fileBridgeFilesManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.sql.rowset.CachedRowSet;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;

public class FileBridgeFilesArchive {
	
	private CachedRowSet recordSet = null;
	private int totalRecordCount = 0;
	
	private LinkedHashMap<String, LookupItem> lookupWellOps = new LinkedHashMap<String, LookupItem>();
	private LinkedHashMap<String, LookupItem> lookupParser = new LinkedHashMap<String, LookupItem>();
	//final String hqlQueryAll = "SELECT * FROM v_file_bridge_files_manager INNER JOIN file_bridge_files ON file_bridge_files.file_bridge_files_uid=v_file_bridge_files_manager.file_bridge_files_uid";
	final String hqlCols = "	file_bridge_files.well_uid, " + 
			"	file_bridge_files.wellbore_uid, " + 
			"	file_bridge_files.operation_uid, " + 
			"	file_bridge_files.file_bridge_files_uid, " + 
			"	file_bridge_files.daily_uid, " + 
			"	file_bridge_files.parser_key, " + 
			"	file_bridge_type_lookup.name, " + 
			"	common_lookup.lookup_label, " + 
			"	file_bridge_files.remark, " + 
			"	file_bridge_files.created_datetime, " + 
			"	file_manager_files.uploaded_datetime, " + 
			"	file_manager_files.uploaded_user_uid, " + 
			"	file_manager_files.file_name, " + 
			"	well_name, wellbore_name, operation_name, " + 
			"	well.well_name_padded, wellbore.wellbore_name_padded, operation.operation_name_padded, " + 
			"	report_number, report_datetime, day_number, day_date ";
	
	private class LookupItemComparator implements  Comparator<LookupItem>{
		public int compare(LookupItem o1, LookupItem o2){
			try{
				
				String s1 = (String) o1.getValue();
				String s2 = (String) o2.getValue();
				
				if (s1 == null || s2 == null) return 0;
				return s1.compareTo(s2);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	private String getHqlQuery(String cols) {
		String hqlQuery = "SELECT " + cols +
			"FROM file_bridge_files " + 
			"INNER JOIN file_manager_files ON external_key_1=CONCAT('FileBridgeFiles:',file_bridge_files_uid) " + 
			"LEFT JOIN well ON file_bridge_files.well_uid = well.well_uid " + 
			"LEFT JOIN wellbore ON file_bridge_files.wellbore_uid = wellbore.wellbore_uid " + 
			"LEFT JOIN operation ON file_bridge_files.operation_uid = operation.operation_uid " + 
			"LEFT JOIN report_daily ON file_bridge_files.daily_uid = report_daily.daily_uid " + 
			"LEFT JOIN daily ON report_daily.daily_uid = daily.daily_uid " + 
			"LEFT JOIN file_bridge_type_lookup ON file_bridge_type_lookup.parser_key = file_bridge_files.parser_key " + 
			"LEFT JOIN common_lookup ON file_bridge_files.`status` = common_lookup.short_code AND common_lookup.lookup_type_selection='FileBridge.status' " +
			"WHERE (file_bridge_files.is_deleted IS NULL OR file_bridge_files.is_deleted=FALSE) ";
		
		return hqlQuery;
	}
	public CachedRowSet getRecordSet() throws Exception {
		if(this.recordSet == null) throw new Exception("Record set is empty. Please load first.");
		return this.recordSet;
	}
	
	public int getTotalRecordCount() throws Exception {
		return this.totalRecordCount;
	}
	
	public LinkedHashMap<String, LookupItem> getLookupWellOps() {
		return this.lookupWellOps;
	}
	
	public LinkedHashMap<String, LookupItem> getLookupParser() {
		return this.lookupParser;
	}
	
	public void load(CommandBean commandBean, UserSession userSession, UserSelectionSnapshot userSelection, Pagination pagination) throws Exception{
		List<String> hqlCondition = new ArrayList<String>();
		List<String> hqlOrder = new ArrayList<String>();
		QueryProperties qp = new QueryProperties();
		String hql = this.getHqlQuery(this.hqlCols);
		
		String selectedWellOps = (String) commandBean.getRoot().getDynaAttr().get("lookupWellOps");
		String selectedParserKey = (String) commandBean.getRoot().getDynaAttr().get("lookupParserKey");
		String statusFilter = (String) commandBean.getRoot().getDynaAttr().get("statusFilter");
		String lookupSort = (String) commandBean.getRoot().getDynaAttr().get("lookupSort");
		Operations all_accessible_operations = userSession.getCachedAllAccessibleOperations();
		
		if(StringUtils.isNotBlank(selectedWellOps)){
			hqlCondition.add("file_bridge_files.operation_uid = '" + selectedWellOps + "'");
			hqlOrder.add("well.well_name_padded ASC, well.well_name ASC, " + 
					"wellbore.wellbore_name_padded ASC, wellbore.wellbore_name ASC, " + 
					"operation.operation_name_padded ASC, operation.operation_name ASC");
		} else {
			String operationUids = "";
			int count = 0;
			for(Entry<_Operation, _Operation> entry : all_accessible_operations.entrySet()) {
				String key = entry.getKey().getOperationUid();
				operationUids += key;
				if(count != all_accessible_operations.entrySet().size() - 1) {
					operationUids += "', '";
				}
				count++;
			}
			
			hqlCondition.add("(file_bridge_files.operation_uid IS NULL OR file_bridge_files.operation_uid IN ('" + operationUids + "'))") ;
		}
		
		if(StringUtils.isNotBlank(selectedParserKey)){
			hqlCondition.add("file_bridge_files.parser_key = '" + selectedParserKey + "'");
		}
		
		if(!(StringUtils.isEmpty(statusFilter) || "All".equalsIgnoreCase(statusFilter))){
			hqlCondition.add("file_bridge_files.status = '" + statusFilter + "'");
		}
		
		if(hqlCondition.size() > 0) {
			hql += " AND ";
			for(int i=0; i < hqlCondition.size(); i++) {
				hql += hqlCondition.get(i);
				if(i != hqlCondition.size() - 1) {
					hql += " AND ";
				}
			}
		}
		
		if(StringUtils.isEmpty(lookupSort)){
			hqlOrder.add("file_manager_files.uploaded_datetime DESC");
		} else if("sortByReportDatetime".equals(lookupSort)) {
			hqlOrder.add("report_daily.report_datetime DESC");
		}
		
		if(hqlOrder.size() > 0) {
			hql += " ORDER BY ";
			for(int i=0; i < hqlOrder.size(); i++) {
				hql += hqlOrder.get(i);
				if(i != hqlOrder.size() - 1) {
					hql += ", ";
				}
			}
		}
		
		if (pagination!=null && pagination.isEnabled()) {
			qp.setRowsToFetch(pagination.getCurrentPage() * pagination.getRowsPerPage(), pagination.getRowsPerPage());
		}
		
		this.recordSet = FileBridgeFilesManagerUtils.getConfiguredInstance().query(hql, qp);
		
		//set record count
		this.setTotalRecordCount(hql);
		
		//set well/ops dropdown
		this.setLookupWellOps(all_accessible_operations, userSelection);
	}
	
	private void setTotalRecordCount(String hql) throws Exception {
		//final long startTime = System.currentTimeMillis();
		CachedRowSet totalResultSet = FileBridgeFilesManagerUtils.getConfiguredInstance().query(hql.replaceAll("SELECT (.*) FROM", "SELECT COUNT(*) FROM"), null);
		this.totalRecordCount = 0;
		while (totalResultSet.next()) {
			this.totalRecordCount =  Integer.parseInt((String.valueOf(totalResultSet.getObject(1))));
		}
		totalResultSet.close();
		//final long endTime = System.currentTimeMillis();
		//System.out.println("setTotalRecordCount: " + (endTime - startTime));
	}
	
	private void setLookupWellOps(Operations all_accessible_operations, UserSelectionSnapshot userSelection) throws Exception {
		//final long startTime = System.currentTimeMillis();
		String operationUid = null;
		String completeOperationName = null;
		LookupItem item = null;
		this.lookupWellOps = new LinkedHashMap<String, LookupItem>();
		this.lookupWellOps.put("", new LookupItem("", "All"));
		
		final CachedRowSet resultSet = FileBridgeFilesManagerUtils.getConfiguredInstance().query(this.getHqlQuery(this.hqlCols), null);
		while(resultSet.next()) {
			operationUid = resultSet.getString("operation_uid");
			if(StringUtils.isNotBlank(operationUid) && all_accessible_operations.containsKey(operationUid)) {
				completeOperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), operationUid, null, true);
				item = new LookupItem(operationUid, completeOperationName);
				this.lookupWellOps.put(operationUid, item);
			}
		}
		//final long endTime = System.currentTimeMillis();
		//System.out.println("setLookupWellOps: " + (endTime - startTime));
		this.setLookupParserKey(resultSet);
		
	}
	
	private void setLookupParserKey(CachedRowSet resultSet) throws Exception {
		String parserKey = null;
		String parserName = null;
		List<LookupItem> list = new ArrayList<LookupItem>();
		this.lookupParser = new LinkedHashMap<String, LookupItem>();
		
		resultSet.beforeFirst();
		while(resultSet.next()) {
			parserKey = resultSet.getString("parser_key");
			parserName = resultSet.getString("name");
			if(StringUtils.isNotBlank(parserName) && StringUtils.isNotBlank(parserKey)) {
				list.add(new LookupItem(parserKey, parserName));
			}
		}
		
		Collections.sort(list, new LookupItemComparator());
		this.lookupParser.put("", new LookupItem("", "All"));
		for(LookupItem i : list) {
			this.lookupParser.put(i.getKey(), i);
		}
	}
}
