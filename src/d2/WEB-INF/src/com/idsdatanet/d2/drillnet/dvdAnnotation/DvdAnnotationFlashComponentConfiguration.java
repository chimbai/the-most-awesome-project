package com.idsdatanet.d2.drillnet.dvdAnnotation;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DvdAnnotationFlashComponentConfiguration extends AbstractFlashComponentConfiguration{
	public String getFlashvars(UserSession userSession, HttpServletRequest request, String sessionId, String targetSubModule) throws Exception{
		String baseUrl = "../../";
		return "baseUrl=" + urlEncode(baseUrl);
	}
	
	public String getFlashComponentId(String targetSubModule){
		return "dvdAnnotation";
	}

	@Override
	public String getAdditionalHtmlHeaderContent(String targetSubModule, UserSession userSession, HttpServletRequest request){
		return "<script language=\"JavaScript\" type=\"text/javascript\">" +
		"function reloadParentHtmlPage(clearQueryString){parent.d2ScreenManager.reloadParentHtmlPage(clearQueryString);}" +
		"function submitSummary(dailyUid){parent.d2ScreenManager.submitSummary(dailyUid);}" +
		"function setBlockUI(block){" +
		"  if(block){"+
		"    parent.d2ScreenManager.toggleMainBlockUI();" +
		"  }else{" +
		"    parent.d2ScreenManager.toggleMainUnblockUI();" +
		"  }" +
		"}" +
		"function gotoUrl(url,msg){parent.d2ScreenManager.gotoToUrl(url,msg);}" +
		"function npdAjaxSubmit(id, service, versionKind) {" +
		"  parent.d2ScreenManager.npdAjaxSubmit(id,service,versionKind);" +
		"}" +
		"function getFlexApp() {" +
		"	if(navigator.appName.indexOf(\"Microsoft\") != -1){" +
		"		return window[\"GenericTemplateRenderer\"];" +
		"	}else{" +
		"		return document[\"GenericTemplateRenderer\"];" +
		"	}" +
		"}" +
		"function callbackEvent(eventId,eventData){" +
		"	getFlexApp().callbackEvent(eventId,eventData);" +
		"}" +
		"</script>";
	}
}
