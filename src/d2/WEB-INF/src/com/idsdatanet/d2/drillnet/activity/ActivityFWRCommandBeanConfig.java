package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.List;

import com.idsdatanet.d2.core.report.validation.CommandBeanReportValidator;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.AjaxAutoPopulateListener;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanConfiguration;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanReportDataListener;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataSummaryListener;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxActionHandler;

public class ActivityFWRCommandBeanConfig extends CommandBeanConfiguration {
	public void init(BaseCommandBean commandBean) throws Exception{
	}
	
	public DataNodeListener getDataNodeListener() throws Exception{
		return null;
	}
	
	public CommandBeanListener getCommandBeanListener() throws Exception{
		return null;
	}
	
	public DataLoaderInterceptor getDataLoaderInterceptor() throws Exception{
		return null;
	}
	
	public ActionManager getActionManager() throws Exception{
		return null;
	}

	public DataNodeAllowedAction getDataNodeAllowedAction() throws Exception{
		return null;
	}
	
	public DataNodeLoadHandler getDataNodeLoadHandler() throws Exception {return null;}
	
	public SimpleAjaxActionHandler getSimpleAjaxActionHandler() throws Exception {return null;}
	
	public AjaxAutoPopulateListener getAjaxAutoPopulateListener() throws Exception {return null;}
	
	public CommandBeanReportDataListener getCommandBeanReportDataListener() throws Exception {return null;}
	
	public DataSummaryListener getDataSummaryListener() throws Exception {return null;}
	
	public List<CommandBeanReportValidator> getCommandBeanReportValidators() throws Exception {
		List<CommandBeanReportValidator> list = new ArrayList<CommandBeanReportValidator>();
		list.add(new ActivityFWRReportValidation());
		return list;
	}
}
