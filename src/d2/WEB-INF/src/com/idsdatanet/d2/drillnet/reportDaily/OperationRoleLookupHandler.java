package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class OperationRoleLookupHandler implements LookupHandler {	
private List<String> designation;
	
	public void setDesignation(List<String> designation)
	{
		if(designation != null){
			this.designation = new ArrayList<String>();
			for(String value: designation){
				this.designation.add(value.trim().toUpperCase());
			}
		}
	}
	
	public List<String> getDesignation() {
		return this.designation;
	}
		
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		
		String strInCondition = "";
		
		if(this.getDesignation() != null)
		{
			for(String value:this.designation)
			{
				strInCondition = strInCondition + "'" + value.toString() + "',";
			}
			strInCondition = StringUtils.substring(strInCondition, 0, -1);
			strInCondition = " and b.designation IN (" + StringUtils.upperCase(strInCondition) + ")";					
		}
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		String sql = "select b.pobMasterUid, b.pobName from OperationRole a, PobMaster b where (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and a.pobMasterUid=b.pobMasterUid and a.operationUid=:operationUid" + strInCondition + " order by b.pobName";
			
		List<Object[]> operationRoleResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid",userSelection.getOperationUid());
		if (operationRoleResults.size() > 0) {
			for (Object[] operationRoleResult : operationRoleResults) {
				if (operationRoleResult[0] != null && operationRoleResult[1] != null) {
					result.put(operationRoleResult[0].toString(), new LookupItem(operationRoleResult[0].toString(), operationRoleResult[1].toString()));
				}
			}
		}
		return result;
		
	}
}

