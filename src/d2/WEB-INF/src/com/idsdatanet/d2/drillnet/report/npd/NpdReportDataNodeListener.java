package com.idsdatanet.d2.drillnet.report.npd;

import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class NpdReportDataNodeListener extends EmptyDataNodeListener {
	/**
	 * Sets each unmapped Activity to initially be in edit mode.
	 * 
	 * @param commandBean
	 * @param meta
	 * @param node
	 * @param userSelection
	 * @param request
	 * @return
	 * @throws Exception
	 * @author Jackey
	 */
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		
		if (object instanceof Activity) {
			node.getAtts().setAction(Action.SAVE);
			node.getAtts().setEditMode(true);
			node.setDirty(true);
		}
	}
}
