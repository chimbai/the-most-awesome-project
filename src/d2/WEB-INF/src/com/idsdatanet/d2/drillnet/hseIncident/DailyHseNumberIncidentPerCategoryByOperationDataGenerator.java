package com.idsdatanet.d2.drillnet.hseIncident;

import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class DailyHseNumberIncidentPerCategoryByOperationDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// To be overriden
	}
	
	// Ticket: 48366 
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		String operationUid = userContext.getUserSelection().getOperationUid();
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) {
			return;
		}
		Date todayDate = daily.getDayDate();
		
		String strSql = "SELECT h.incidentCategory, sum(h.numberOfIncidents) AS sumNumber FROM HseIncident h, Daily d " +
				"WHERE (h.isDeleted = false OR h.isDeleted IS NULL) " +
				"AND (d.isDeleted = false OR d.isDeleted IS NULL) " +
				"AND h.operationUid = :operationUid " +
				"AND d.dailyUid = h.dailyUid " +
				"AND d.dayDate <= :todayDate " + 
				"GROUP BY h.incidentCategory";
		
		String[] paramsFields = {"todayDate", "operationUid"};
		Object[] paramsValues = {todayDate, operationUid};
		
		List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (!lstResult.isEmpty()) {
			for (Object objResult: lstResult) {
				Object[] obj = (Object[]) objResult;
				String incidentCategory = (obj[0] != null) ? obj[0].toString() : "";
				String sumNumberOfIncidents = (obj[1] != null) ? obj[1].toString() : "";
				ReportDataNode thisReportNode = reportDataNode.addChild("numberIncidentsPerCategory");
				thisReportNode.addProperty("incidentCategory", incidentCategory);
				thisReportNode.addProperty("sumNumberOfIncidents", sumNumberOfIncidents);
			}
		}
	}
}
