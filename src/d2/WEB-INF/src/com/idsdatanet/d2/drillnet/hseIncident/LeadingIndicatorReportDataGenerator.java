package com.idsdatanet.d2.drillnet.hseIncident;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.HsePlan;
import com.idsdatanet.d2.core.model.HsePlanEvents;
import com.idsdatanet.d2.core.model.HseRelatedOperation;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


public class LeadingIndicatorReportDataGenerator implements ReportDataGenerator{
	public static final String LEADING_INDICATORS = "Leading Indicators";
	public static final String LEADING_INDICATORS_FOR_WELL_SERVICES = "Leading Indicators For Well Services";
	public static final String WELL_SERVICES = "Well Services";
	public static final String POB = "POB";
	private String sortOrder;
	
	public void setSortOrder(String value){
		this.sortOrder = value;
	}
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		
		String thisRigInformationUid = "";
		if(userContext.getUserSelection().getRigInformationUid() != null) thisRigInformationUid = userContext.getUserSelection().getRigInformationUid().toString();
				
		String operationUidList = "";
		String operationUidList2 = "";
		String thisHsePlanUid = "";
		String sortOrder = "";
		
		//BY DEFAULT, USE THIS HSE PLAN
		String thisPlanName = LEADING_INDICATORS;
		String thisSystemStatus = "leading";
		
		//DISCONTINUE IF NO RIG FOR CURRENT OPERATION
		if(StringUtils.isBlank(thisRigInformationUid)) return;
		
		//GET ALL operationUid WITH THE SAME rigInformationUid
		String strSql = "SELECT DISTINCT operationUid FROM ReportDaily WHERE rigInformationUid = :thisRigInformationUid AND (isDeleted = false or isDeleted is null)";
		String[] paramsFields = {"thisRigInformationUid"};
		Object[] paramsValues = {thisRigInformationUid};
				
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (lstResult.size() > 0){
			
			for(Object objResult: lstResult){
				Object thisOperationUid = (Object) objResult;
				if(thisOperationUid != null) operationUidList = operationUidList + "'" + thisOperationUid.toString() + "',";
			}
			if(StringUtils.isNotBlank(operationUidList)) operationUidList = StringUtils.substring(operationUidList, 0, -1);
		}
		//END OF GET ALL operationUid WITH THE SAME rigInformationUid
		
		//GET CURRENT operationUid AND RELATED operationUid 
		strSql = "FROM HseRelatedOperation WHERE operationUid = :thisOperationUid AND (isDeleted = false OR isDeleted IS NULL)";
		String[] paramsFields1 = {"thisOperationUid"};
		Object[] paramsValues1 = {userContext.getUserSelection().getOperationUid().toString()};
		List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1);
		
		operationUidList2 = "'" + userContext.getUserSelection().getOperationUid().toString() + "',";
		if (lstResult1.size() > 0){
						
			for(Object objResult: lstResult1){
				HseRelatedOperation thisHseRelatedOperation = (HseRelatedOperation) objResult;
				if(thisHseRelatedOperation.getRelatedOperationUid() != null) operationUidList2 = operationUidList2 + "'" + thisHseRelatedOperation.getRelatedOperationUid().toString() + "',";
			}
		}
		
		if(StringUtils.isNotBlank(operationUidList2)) 
		{
			operationUidList2 = StringUtils.substring(operationUidList2, 0, -1);
		}
		//END OF GET CURRENT operationUid AND RELATED operationUid 
		
		//DISCONTINUE WHEN NO OPERATION FOR THIS RIG
		if(StringUtils.isBlank(operationUidList)) return;
				
		String thisOperationUid = "";
		if(userContext.getUserSelection().getOperationUid() != null) thisOperationUid = userContext.getUserSelection().getOperationUid().toString();
		
		//DISCONTINUE IF NO OPERATION IS SELECTED	
		if(StringUtils.isBlank(thisOperationUid)) return;
		
		//DISCONTINUE IF NO DAY IS SELECTED
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
					
		//CHECK IF CURRENT RIG IS WELL SERVICES
		strSql = "FROM RigInformation WHERE rigInformationUid = :rigInformationUid AND (isDeleted = false or isDeleted is null)";
		List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigInformationUid", thisRigInformationUid);	
		if (lstResult2.size() > 0){
			RigInformation thisRigInformation = (RigInformation) lstResult2.get(0);
				
			if(thisRigInformation.getRigName() != null)
			{
				if(StringUtils.trim(thisRigInformation.getRigName().toString()).equalsIgnoreCase(WELL_SERVICES) )
				{
					thisPlanName = LEADING_INDICATORS_FOR_WELL_SERVICES;
				}
			}
		}
				
		//LOAD LEADING INDICATOR FROM HsePlan TABLE
		Date thisEventDate = daily.getDayDate();
		strSql = "FROM HsePlan WHERE rigInformationUid = :thisRigInformationUid AND planName = :thisPlanName AND (isDeleted = false or isDeleted is null) AND " +
			"((dateStart <=:dayDate and dateEnd >=:dayDate) or (dateStart is null and dateEnd is null) or (dateStart <=:dayDate and dateEnd is null) or " +
			"(dateStart is null and dateEnd >=:dayDate)) ORDER BY dateStart DESC";
		String[] paramsFields3 = {"thisRigInformationUid", "thisPlanName", "dayDate"};
		Object[] paramsValues3 = {thisRigInformationUid, thisPlanName, thisEventDate};
					
		//GET hsePlanUid FROM HsePlan TABLE
		List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields3, paramsValues3);
		if (lstResult3.size() > 0){
			HsePlan thisHsePlan = (HsePlan) lstResult3.get(0);
			
			if(thisHsePlan != null)
			{
				thisHsePlanUid = thisHsePlan.getHsePlanUid().toString();
			}
			
			//GET INDICATORS FROM HsePlanEvents TABLE			
			if(StringUtils.isNotBlank(thisHsePlanUid))
			{
				if(this.sortOrder != null){
					sortOrder = "ORDER BY " + this.sortOrder;
				}else{
					sortOrder = "ORDER BY shortName";
				}
				
				strSql = "FROM HsePlanEvents WHERE hsePlanUid = :thisHsePlanUid AND (isDeleted = false or isDeleted is null) "+sortOrder;
				List result4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisHsePlanUid", thisHsePlanUid);	
		
				if (result4.size() > 0){
					String thisIncidentCategory = "";
					String thisHsePlanEventsValue = "";
					String thisShortName = "";
					
					ReportDataNode reportDataNode2 = reportDataNode.addChild("LeadingIndicator");
					reportDataNode2.addProperty("operationUid", thisOperationUid);
					
					// START TO LOOP THRU EACH INDICATOR
					for(Object objResult2: result4){
						HsePlanEvents thisHsePlanEvents = (HsePlanEvents) objResult2;
						String thisDailyEventNumber = "";
										
						if(thisHsePlanEvents != null)
						{
							if (thisHsePlanEvents.getIncidentCategory() != null) thisIncidentCategory = thisHsePlanEvents.getIncidentCategory().toString();
							thisHsePlanEventsValue = "";
							thisShortName = "";
							
							if(thisHsePlanEvents.getHsePlanEventsValue() != null) thisHsePlanEventsValue = thisHsePlanEvents.getHsePlanEventsValue().toString();
							if(thisHsePlanEvents.getShortName() != null) thisShortName = thisHsePlanEvents.getShortName().toString();
							
							//DISCONTINUE IF NO INDICATOR IS FOUND
							if(StringUtils.isBlank(thisIncidentCategory)) return;
							
							//CREATE CHILD NODE FOR EACH INDICATOR
							ReportDataNode thisReportNode = reportDataNode2.addChild("Indicator");
							thisReportNode.addProperty("incidentCategory", thisIncidentCategory);
														
							//IF CURRENT INDICATOR IS POB FOR WELL SERVICES
							if(StringUtils.trim(thisHsePlanEventsValue).equalsIgnoreCase(POB))
							{
								thisReportNode.addProperty("hsePlanEventsValue", this.calculatePOB(userContext));
							}
							else
							{
								thisReportNode.addProperty("hsePlanEventsValue", thisHsePlanEventsValue);
							}
									
							if(StringUtils.isNotBlank(thisShortName))
							{
								thisReportNode.addProperty("shortName", thisShortName);
							}
							else
							{
								thisReportNode.addProperty("shortName", thisIncidentCategory);
							}
											
							Integer intYear = Integer.parseInt(StringUtils.substring(thisEventDate.toString(), 0, 4));
							Integer intMonth = Integer.parseInt(StringUtils.substring(thisEventDate.toString(), 5, 7));
							Integer intDay = Integer.parseInt(StringUtils.substring(thisEventDate.toString(), 8, 10));
							
							Calendar thisCalendar = Calendar.getInstance();
							thisCalendar.setTime(thisEventDate);
							thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
							thisCalendar.set(Calendar.MINUTE, 59);
							thisCalendar.set(Calendar.SECOND , 59);
							thisCalendar.set(Calendar.MILLISECOND , 59);
							
							//GET DAILY INDICATOR NUMBER
							strSql = "SELECT SUM(hi.numberOfIncidents) FROM HseIncident hi,Daily d WHERE hi.operationUid IN (" + operationUidList + ") AND hi.incidentCategory = :thisIncidentCategory AND hi.rigInformationUid = :thisRigInformationUid AND (hi.hseEventdatetime >= :thisEventDate and hi.hseEventdatetime <= :thisEventEndDate) " +
									"AND hi.systemStatus = :thisSystemStatus " +
									"AND hi.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) AND (hi.isDeleted = false or hi.isDeleted is null) AND (hi.supportVesselInformationUid is null or hi.supportVesselInformationUid = '')";
							String[] paramsFields5 = {"thisIncidentCategory", "thisRigInformationUid", "thisEventDate", "thisEventEndDate", "thisSystemStatus"};
							Object[] paramsValues5 = {thisIncidentCategory, thisRigInformationUid, thisEventDate, thisCalendar.getTime(), thisSystemStatus};
							
							List lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields5, paramsValues5);
							if (lstResult5.size() > 0){
								Object eventNumber = (Object) lstResult5.get(0);
																
								if(eventNumber != null)
								{
									thisReportNode.addProperty("dailyEventNumber", eventNumber.toString());
									thisDailyEventNumber = eventNumber.toString();
									
								}
								else
								{
									thisReportNode.addProperty("dailyEventNumber", "0");
									thisDailyEventNumber = "0";
								}
							}
							//END OF DAILY INDICATOR NUMBER
							
							//GET WELL TO DATE INDICATOR NUMBER
							//THIS IS FOR CURRENT OPERATION AND ALL RELATED OPERATIONS
							strSql = "SELECT SUM(hi.numberOfIncidents) FROM HseIncident hi,Daily d WHERE hi.operationUid IN (" + operationUidList2 + ") AND hi.incidentCategory = :thisIncidentCategory AND hi.hseEventdatetime <= :thisEventDate AND (hi.isDeleted = false or hi.isDeleted is null) " +
									"AND hi.systemStatus = :thisSystemStatus " +
									"AND hi.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
							String[] paramsFields6 = {"thisIncidentCategory", "thisEventDate", "thisSystemStatus"};
							Object[] paramsValues6 = {thisIncidentCategory, thisCalendar.getTime(), thisSystemStatus};
							
							List lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields6, paramsValues6);
							if (lstResult6.size() > 0){
								Object eventNumber = (Object) lstResult6.get(0);
																
								if(eventNumber != null)
								{
									thisReportNode.addProperty("wellToDateEventNumber", eventNumber.toString());
								}
								else
								{
									thisReportNode.addProperty("wellToDateEventNumber", "0");
								}
							}
							//END OF WELL TO DATE INDICATOR NUMBER
							
							//GET MONTHLY INDICATOR NUMBER
							strSql = "SELECT SUM(hi.numberOfIncidents) FROM HseIncident hi,Daily d WHERE hi.operationUid IN (" + operationUidList + ") AND hi.incidentCategory = :thisIncidentCategory AND hi.rigInformationUid = :thisRigInformationUid AND (YEAR(hi.hseEventdatetime) = :Year AND MONTH(hi.hseEventdatetime) = :Month AND DAY(hi.hseEventdatetime) <= :Day) AND (hi.isDeleted = false or hi.isDeleted is null) " +
									"AND hi.systemStatus = :thisSystemStatus " +
									"AND hi.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
							String[] paramsFields7 = {"thisIncidentCategory", "thisRigInformationUid", "Year", "Month", "Day", "thisSystemStatus"};
							Object[] paramsValues7 = {thisIncidentCategory, thisRigInformationUid, intYear, intMonth, intDay, thisSystemStatus};
							
							List lstResult7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields7, paramsValues7);
							if (lstResult7.size() > 0){
								Object eventNumber = (Object) lstResult7.get(0);
																
								if(eventNumber != null)
								{
									thisReportNode.addProperty("monthlyEventNumber", eventNumber.toString());
								}
								else
								{
									thisReportNode.addProperty("monthlyEventNumber", "0");
								}
							}
							//END OF MONTHLY INDICATOR NUMBER
							
							//GET YEARLY INDICATOR NUMBER
							strSql = "SELECT SUM(hi.numberOfIncidents) FROM HseIncident hi,Daily d WHERE hi.operationUid IN (" + operationUidList + ") AND hi.incidentCategory = :thisIncidentCategory AND hi.rigInformationUid = :thisRigInformationUid AND YEAR(hi.hseEventdatetime) = :Year AND hi.hseEventdatetime <= :thisEventDate AND (hi.isDeleted = false or hi.isDeleted is null) " +
									"AND hi.systemStatus = :thisSystemStatus " +
									"AND hi.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
							String[] paramsFields8 = {"thisIncidentCategory", "thisRigInformationUid", "Year", "thisEventDate", "thisSystemStatus"};
							Object[] paramsValues8 = {thisIncidentCategory, thisRigInformationUid, intYear, thisCalendar.getTime() , thisSystemStatus};
							
							List lstResult8 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields8, paramsValues8);
							if (lstResult8.size() > 0){
								Object eventNumber = (Object) lstResult8.get(0);
																
								if(eventNumber != null)
								{
									thisReportNode.addProperty("yearlyEventNumber", eventNumber.toString());
								}
								else
								{
									thisReportNode.addProperty("yearlyEventNumber", "0");
								}
							}
							//END OF YEARLY INDICATOR NUMBER
							
							//GET INDICATOR COMMENT
							strSql = "SELECT DISTINCT hi.hseShortdescription FROM HseIncident hi,Daily d WHERE hi.operationUid = :thisOperationUid AND hi.incidentCategory = :thisIncidentCategory AND (YEAR(hi.hseEventdatetime) = :Year AND MONTH(hi.hseEventdatetime) = :Month AND DAY(hi.hseEventdatetime) = :Day) AND (hi.isDeleted = false or hi.isDeleted is null) " +
									"AND hi.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null)";
							String[] paramsFields9 = {"thisOperationUid", "thisIncidentCategory", "Year", "Month", "Day"};
							Object[] paramsValues9 = {thisOperationUid, thisIncidentCategory, intYear, intMonth, intDay};
							
							List lstResult9 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields9, paramsValues9);
							if (lstResult9.size() > 0){
								String strComment = "";
								String strComment2 = "";
								strComment = thisIncidentCategory + " " + thisDailyEventNumber + " - ";
								
								for(Object objShortDesc: lstResult9){
									if(objShortDesc != null) 
									{
										if(StringUtils.isNotBlank(objShortDesc.toString()))
										{
											strComment2 = strComment2 + objShortDesc.toString() + "; ";
										}
									}
								}
								
								strComment2 = StringUtils.substring(strComment2, 0, -2);
																							
								if(StringUtils.isNotBlank(strComment2))
								{
									thisReportNode.addProperty("comment", strComment + strComment2);
								}
								else
								{
									thisReportNode.addProperty("comment", "");
								}
							}
							//END OF INDICATOR COMMENT
						}
					}
				}
			}
		}
	}
	
	private String calculatePOB(UserContext userContext) throws Exception{
		String thisOperationUid = userContext.getUserSelection().getOperationUid().toString();
		String thisDailyUid = userContext.getUserSelection().getDailyUid().toString();
		String paxNumber = "0";
		
		String strSql = "SELECT SUM(pax) FROM PersonnelOnSite WHERE operationUid = :operationUid AND dailyUid = :dailyUid AND (isDeleted = false or isDeleted is null)";
		String[] paramsFields = {"operationUid", "dailyUid"};
		Object[] paramsValues = {thisOperationUid, thisDailyUid};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (lstResult.size() > 0){
			Object totalPax = (Object) lstResult.get(0);
						
			if(totalPax != null)
			{
				paxNumber = totalPax.toString();
			}			
		}
				
		return paxNumber;
	}
}
