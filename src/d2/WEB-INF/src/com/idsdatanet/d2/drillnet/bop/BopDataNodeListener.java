package com.idsdatanet.d2.drillnet.bop;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Bop;
import com.idsdatanet.d2.core.model.BopLog;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class BopDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener {
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		//Auto-fill Bop.bopInterval when a new record is created
		Object object = node.getData();		
		if (object instanceof Bop){
			Bop bop = (Bop) object;			
			bop.setBopInterval(Double.parseDouble(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_BOP_TEST_DUE_EVERY_X_DAYS)));
			
		}
		if (object instanceof BopLog){
			BopLog thisBopLog = (BopLog) object;
			thisBopLog.setOperationUid(userSelection.getOperationUid());
		}
	}
	
	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof BopLog) {
			BopLog log = (BopLog) node.getData();
			if (log!=null && log.getCasingSectionUid()!=null) {
				BopUtil.populateCasingSectionData(commandBean, node, log.getCasingSectionUid());
			}
		} else if (node.getData() instanceof Bop) {
			Map<String, CommandBeanTreeNode> children = node.getChild("BopLog");
			if (children!=null) {
				int counter = 0;
				for (Map.Entry<String, CommandBeanTreeNode> entry : children.entrySet()) {
					CommandBeanTreeNode childNode = entry.getValue();
					
					if (childNode.getData() instanceof BopLog) {
						childNode.getDynaAttr().put("overdue", false);
						if (counter==children.size()-1) {
							BopLog log = (BopLog) childNode.getData();
							if (log.getNextTestDatetime()!=null) {
								Calendar nextDueTest = Calendar.getInstance();
								nextDueTest.setTime(log.getNextTestDatetime());
								
								Calendar todayDate = Calendar.getInstance();
								todayDate.setTime(new Date());
								todayDate.set(Calendar.HOUR_OF_DAY, 23);
								todayDate.set(Calendar.MINUTE, 59);
								todayDate.set(Calendar.SECOND, 59);
								
								if (nextDueTest.getTime().before(todayDate.getTime())) {
									childNode.getDynaAttr().put("overdue", true);
								}
							}
						}
					}
					counter ++;
				}
			}
			
			if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "singleActiveBopInstalled"))) {
				String rigInformationUid = userSelection.getRigInformationUid();
				Boolean enabled = true;
				if (StringUtils.isNotBlank(rigInformationUid)) {
					String strSql = "FROM Bop WHERE (isDeleted = false or isDeleted is null) and rigInformationUid =:rigInformationUid";
					List <Bop> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "rigInformationUid", rigInformationUid);
					if (lstResult.size() > 0 && lstResult!=null){
						
						for(Bop objValue:lstResult)
						{
							if(objValue.getRemoveDate()==null)
							{
								enabled = false;
							}
						}
					}
				}
				if (enabled == false){
					commandBean.getSystemMessage().addInfo("Kindly enter Remove Date for the previous BOP in order to add new BOP record.", true);
				}
			}
			
		}
		//get dynamic attribute well name
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String wellUid="";
		wellUid=userSelection.getWellUid();
		
		String strSql = "SELECT wellName from Well WHERE (isDeleted = false or isDeleted is null) AND wellUid = :thiswellUid";
		String[] paramsFields = {"thiswellUid"};
		Object[] paramsValues = {wellUid};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			Object wellname = (Object) lstResult.get(0);
			
			if (wellname != null && StringUtils.isNotBlank(wellname.toString()))
			{
				node.getDynaAttr().put("wellName",wellname.toString());
			}
		}
				node.getDynaAttr().put("wellUid",wellUid);

	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
	
		Object object = node.getData();		
		if (object instanceof Bop){
			
			Bop bop = (Bop) object;			
				
				//Validation Code for Installed Date > Remove Date
				if(bop.getInstallDate()!= null && bop.getRemoveDate()!= null){
					if(bop.getInstallDate().getTime() > bop.getRemoveDate().getTime()){
						status.setContinueProcess(false, true);
						status.setFieldError(node, "removeDate", "Remove Date cannot be earlier than Install Date.");
						return;
					}
				}
							
			//Auto-fill Bop.bopInterval if empty before save			
			if (bop.getBopInterval() == null) {
				bop.setBopInterval(Double.parseDouble(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_BOP_TEST_DUE_EVERY_X_DAYS)));	
			}
	        if (object instanceof BopLog){                                    
	            BopLog bopLog = (BopLog) object;
				if(bopLog.getTestType()!= null)                       
		            if(!bopLog.getTestType().equals("pressure")){                               
		                bopLog.setRamsTestPressure(null);                          
		                bopLog.setDiverterTestPressure(null);                           
		                bopLog.setChokeLineTestPressure(null);                          
		                bopLog.setKellyHoseTestPressure(null);                          
		                bopLog.setStandpipeTestPressure(null);                            
		                bopLog.setAnnularTestPressure(null);                              
		                bopLog.setChokeTestPressure(null);                           
		                bopLog.setKillLineTestPressure(null);                                                                     
		            }                                                          
	        }
		}
	}
	
	public void afterDataNodeProcessed(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, String action, int operationPerformed, boolean errorOccurred) throws Exception{
		//Calculate BopLog.nextTestDatetime, after all nodes have been saved, and before afterDataNodeLoad() 
		Object object = node.getData();
		if ("0".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "manualEntryForBOPTestLog"))){
			if (object instanceof Bop){
				
				Bop bop = (Bop) object;
				String thisBopUid = bop.getBopUid().toString();
				
				//Get BopLog record by Bop.bopUid
				List bopLogList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM BopLog WHERE bopUid = :bopUid", "bopUid", thisBopUid);
						
				DecimalFormat formatter = new DecimalFormat("#0");
				
				for(Object thisBopLog : bopLogList)
				{
					BopLog bopLog = (BopLog) thisBopLog;
					
					//Only calculate when BopLog.testingDatetime is not empty
					if(bopLog.getTestingDatetime()!=null)
					{
						try {
							//Get Bop.bopInterval from current node
							String thisBopInterval = CommonUtils.roundUpFormat(formatter, PropertyUtils.getProperty(node.getData(),"bopInterval"));				
							bopLog.setNextTestDatetime(new Date(bopLog.getTestingDatetime().getTime() + Long.parseLong(thisBopInterval) * 86400000));
												
						} catch (Exception e) {
						}
					}
					
					//Re-save BopLog record before afterDataNodeLoad() 
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bopLog);
				}
			}
		}
	}

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if (targetCommandBeanTreeNode.getData() instanceof BopLog) {
			BopLog log = (BopLog) targetCommandBeanTreeNode.getData();
			String targetField = targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField();

			if("wellboreUid".equals(targetField)){
				BopUtil.populateCasingSectionData(commandBean, targetCommandBeanTreeNode, "");
			} else if ("casingSectionUid".equals(targetField)) {
				BopUtil.populateCasingSectionData(commandBean, targetCommandBeanTreeNode, log.getCasingSectionUid());
			}
		}
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
