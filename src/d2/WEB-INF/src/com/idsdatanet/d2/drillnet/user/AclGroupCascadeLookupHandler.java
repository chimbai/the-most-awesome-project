package com.idsdatanet.d2.drillnet.user;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.AclGroup;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class AclGroupCascadeLookupHandler implements CascadeLookupHandler {
	
	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		List<AclGroup> aclGroups = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from AclGroup where (isDeleted = false or isDeleted is null) and groupUid = :groupUid order by name", "groupUid", key);
		if (aclGroups != null) {
			List<String> currentAclGroups = null;
			if (request != null) {
				UserSession session = UserSession.getInstance(request);
				if (session != null) {
					currentAclGroups = session.getCombinedAclGroups().getAclGroupUids();
				}
			}
			result.put("ALL", new LookupItem("ALL", "All Groups"));
			for (AclGroup aclGroup : aclGroups) {
				if (currentAclGroups != null) {
					if ("Super Administrator Group".equals(aclGroup.getName()) && !currentAclGroups.contains(aclGroup.getAclGroupUid())) {
						continue;
					}
				}
				result.put(aclGroup.getAclGroupUid(), new LookupItem(aclGroup.getAclGroupUid(), aclGroup.getName()));
			}
		}
		return result;
	}
	
	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		List<String> groupUids = ApplicationUtils.getConfiguredInstance().getDaoManager().find("select groupUid from Group where (isDeleted = false or isDeleted is null)");
		if (groupUids != null) {
			for (String groupUid : groupUids) {
				CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(groupUid);
				cascadeLookupSet.setLookup(new LinkedHashMap(this.getCascadeLookup(commandBean, null, userSelection, request, groupUid, null)));
				result.add(cascadeLookupSet);
			}
		}
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);		
		return result_in_array;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		return null;
	}

}
