package com.idsdatanet.d2.drillnet.operation;

import java.util.List;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.stt.STTDataGeneratorConfig;
import com.idsdatanet.d2.core.stt.active.ActiveSTTObjectListener;
import com.idsdatanet.d2.core.stt.active.ActiveSTTRequestParam;
import com.idsdatanet.d2.core.stt.active.DaoCommand;
import com.idsdatanet.d2.core.uom.hibernate.tables.UomTemplate;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class OperationActiveSTTListener implements ActiveSTTObjectListener {

	@Override
	public void beforePerformDaoCommand(DaoCommand daoCommand, ActiveSTTRequestParam params) throws Exception {
		if (params != null && params.isPublishSubscribe()) {
			Object data = daoCommand.getParams()[0];
			if (data instanceof Operation) {
				Operation operation = (Operation)data;
				Operation dbOperation = (Operation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operation.getOperationUid());
				// existing
				if (dbOperation != null) {
					operation.setUomTemplateUid(dbOperation.getUomTemplateUid());
				// new
				} else {
					List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from UomTemplate where (isDeleted = false or isDeleted is null) and groupUid=:groupUid and defaultTemplate=true", "groupUid", operation.getGroupUid());
					if (result.size() > 0) {
						UomTemplate uomTemplate = (UomTemplate)result.get(0);
						operation.setUomTemplateUid(uomTemplate.getUomTemplateUid());
					}
				}
			}
		}
	}

	@Override
	public void afterPerformDaoCommand(DaoCommand daoCommand, ActiveSTTRequestParam params) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforePerformRFT(Object object) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterPerformRFT(Object object) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterCollectionPerformDaoCommand(STTDataGeneratorConfig dataGeneratorConfig,
			List<DaoCommand> daoCommands, ActiveSTTRequestParam params) throws Exception {
		// TODO Auto-generated method stub

	}

}
