package com.idsdatanet.d2.drillnet.rigUtilization;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigContract;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


public class RigUtilizationUtils {
	/**
	 * Method for days on contract Calculation
	 * @param Contract Start date
	 * @param Contract End date
	 * @throws Exception Standard Error Throwing Exception
	 * @return return days on contract value
	 */
	public static double calculateDaysOnContract(Date ContractStart, Date ContractEnd) throws Exception{
		
		Calendar thisCalender = Calendar.getInstance();
		thisCalender.setTime(ContractEnd);
		thisCalender.set(Calendar.HOUR_OF_DAY, 23);
		thisCalender.set(Calendar.MINUTE, 59);
		thisCalender.set(Calendar.SECOND , 59);
		thisCalender.set(Calendar.MILLISECOND , 0);
		
		double usecOnContract = thisCalender.getTimeInMillis() - ContractStart.getTime();
		double daysOnContractInBase = usecOnContract / 1000; // important, daysOnContractInBase must be of type double
		if (daysOnContractInBase < 0) {
			return 0.0;
		}
		return daysOnContractInBase;
	}

	/**
	 * Method for days since last LTI Calculation
	 * @param groupUid
	 * @param rigInformationUid
	 * @param dailyUid
	 * @throws Exception Standard Error Throwing Exception
	 * @return return days since last lti value
	 */
	public static Double calculateDaysSinceLastLTI(String groupUid, String rigInformationUid, String dailyUid) throws Exception{
        Double timeLapsed = 0.0;
        Double daysSinceLastLti = null;
		if ("1".equalsIgnoreCase(GroupWidePreference.getValue(groupUid, "calculateHseDayLapsedIncludeDayOfIncident"))){
			timeLapsed = 86399.0; // count the time lapse as at 23:59 of the day
		}
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		if (daily==null) return null;
		Date currentDate = daily.getDayDate();
		
		//get last incident date
		HseIncident lastLti = CommonUtil.getConfiguredInstance().lastLTI(rigInformationUid, dailyUid);
		if (lastLti==null || lastLti.getHseEventdatetime()==null) return null;
		Calendar ltiCalender = Calendar.getInstance();
		ltiCalender.setTime(lastLti.getHseEventdatetime());
		ltiCalender.set(Calendar.HOUR_OF_DAY, 0);
		ltiCalender.set(Calendar.MINUTE, 0);
		ltiCalender.set(Calendar.SECOND , 0);
		ltiCalender.set(Calendar.MILLISECOND , 0);
		Date lastLtiDate = ltiCalender.getTime();

		/*
		 * 1. Report Date & LTI Date in same contract
		 * 2. Report Date & LTI Date after the last contract 
		 * 3. Report Date in current contract but LTI not in any contract
		 * 4. Report Date in current contract and LTI in previous contract - no extra days in between the contracts
		 * 5. Report Date in current contract and LTI in previous contract - with extra days in between the contracts
		 */
		
		Date fromDate = null;
		Date toDate = null;
		String queryString = "SELECT rc FROM RigInformation r, RigContract rc " +
				"WHERE (r.isDeleted=false or r.isDeleted is null) " +
				"AND (rc.isDeleted=false or rc.isDeleted is null) " +
				"AND r.rigInformationUid=rc.rigInformationUid " +
				"AND r.rigInformationUid=:rigInformationUid " +
				"AND rc.contractStartDatetime<=:reportDate " +
				"AND rc.contractEndDatetime>=:reportDate " +
				"AND rc.contractStartDatetime is not null " +
				"AND rc.contractEndDatetime is not null " +
				"AND r.groupUid=:groupUid " +
				"ORDER BY rc.contractEndDatetime DESC";
		List<RigContract> rigContractList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"groupUid", "rigInformationUid","reportDate"}, new Object[] {groupUid, rigInformationUid, currentDate});
		if (rigContractList.isEmpty()) {
			//day not in calculation for rig utilisation days.
			queryString = "SELECT rc FROM RigInformation r, RigContract rc " +
					"WHERE (r.isDeleted=false or r.isDeleted is null) " +
					"AND (rc.isDeleted=false or rc.isDeleted is null) " +
					"AND r.rigInformationUid=rc.rigInformationUid " +
					"AND r.rigInformationUid=:rigInformationUid " +
					"AND rc.contractEndDatetime<:reportDate " +
					"AND rc.contractStartDatetime is not null " +
					"AND rc.contractEndDatetime is not null " +
					"AND r.groupUid=:groupUid " +
					"ORDER BY rc.contractEndDatetime DESC";
			rigContractList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"groupUid", "rigInformationUid","reportDate"}, new Object[] {groupUid, rigInformationUid, currentDate});
			if (rigContractList.isEmpty()) { 
				//if there's NO rig utilization before current date - CALCULATE the days since LTI
				fromDate = lastLtiDate;
				toDate = currentDate;
			} else {
				// if there's rig utilization before current date
				RigContract currentRigContract = rigContractList.get(0);
				if (lastLtiDate.compareTo(currentRigContract.getContractEndDatetime())>=0) { 
					//if the LTI Date after the previous contract - CALCULATE the days since LTI
					fromDate = lastLtiDate;
					toDate = currentDate;
				} else {	
					// if there's no LTI after the previous contract, with extra days - show BLANK 
				}
			}
		} else {
			RigContract currentRigContract = rigContractList.get(0);
			if (lastLtiDate.compareTo(currentRigContract.getContractStartDatetime())>=0) { 
				//LTI within the same contract
				fromDate = lastLtiDate;
				toDate = currentDate;
			} else { 
				//LTI before this contract start date
				//find all rig contract before the current contract
				queryString = "SELECT rc FROM RigInformation r, RigContract rc " +
						"WHERE (r.isDeleted=false or r.isDeleted is null) " +
						"AND (rc.isDeleted=false or rc.isDeleted is null) " +
						"AND r.rigInformationUid=rc.rigInformationUid " +
						"AND r.rigInformationUid=:rigInformationUid " +
						"AND rc.contractEndDatetime<:reportDate " +
						"AND rc.contractStartDatetime is not null " +
						"AND rc.contractEndDatetime is not null " +
						"AND r.groupUid=:groupUid " +
						"ORDER BY rc.contractEndDatetime DESC";
				rigContractList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"groupUid", "rigInformationUid","reportDate"}, new Object[] {groupUid, rigInformationUid, currentRigContract.getContractStartDatetime()});
				if (rigContractList.isEmpty()) {
					//if there's no other contract 
					fromDate = currentRigContract.getContractStartDatetime();
					toDate = currentDate;
				} else {
					//for loop
					RigContract nextRigContract = currentRigContract;
					for (RigContract rec : rigContractList) {
						
						Calendar currentContractEndTime = Calendar.getInstance();
						currentContractEndTime.setTime(rec.getContractEndDatetime());
						currentContractEndTime.add(Calendar.DATE, 1);
						Calendar nextContractStartTime = Calendar.getInstance();
						nextContractStartTime.setTime(nextRigContract.getContractStartDatetime());
						nextContractStartTime.add(Calendar.DATE, -1);
						
						Double daysIntervalBetweenContract = calculateReportDailyNumber(rigInformationUid, currentContractEndTime.getTime(), nextContractStartTime.getTime());
						if (daysIntervalBetweenContract>0) {
							//if there's extra day before the contract
							if (lastLtiDate.compareTo(rec.getContractEndDatetime())>0) {
								//if has HSE in the extra days - calculate the days since next contract start
								fromDate = nextRigContract.getContractStartDatetime();
								toDate = currentDate;
							} //else show BLANK from this onward
							break;
						} else {
							if (lastLtiDate.compareTo(rec.getContractStartDatetime())>=0) {
								//LTI in previous contract with no extra days between the contracts
								fromDate = lastLtiDate;
								toDate = currentDate;
								break;
							}
						}

						nextRigContract = rec;
					}
				}
			}
		}
		
		
		if (fromDate!=null && toDate!=null) {
			daysSinceLastLti = RigUtilizationUtils.calculateReportDailyNumber(rigInformationUid, fromDate, toDate);
		}
		if (daysSinceLastLti!=null) {
			daysSinceLastLti = daysSinceLastLti * 86400.0;
			daysSinceLastLti += timeLapsed;
		}
		return daysSinceLastLti;
	}
	
	/**
	 * Method for number of report daily calculation
	 * @param rigInformationUid
	 * @param dateFrom
	 * @param dateTo
	 * @throws Exception Standard Error Throwing Exception
	 * @return return number of report daily 
	 */
	public static Double calculateReportDailyNumber(String rigInformationUid, Date dateFrom, Date dateTo) throws Exception {
		String queryString = "SELECT d.dayDate FROM ReportDaily rd, Daily d " +
				"WHERE (rd.isDeleted=false or rd.isDeleted is null) " +
				"AND (d.isDeleted=false or d.isDeleted is null) " +
				"AND rd.dailyUid=d.dailyUid " +
				"AND d.dayDate>=:dateFrom " +
				"AND d.dayDate<=:dateTo " +
				"AND rd.rigInformationUid=:rigInformationUid " +
				"GROUP BY d.dayDate ";
		List dateList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"dateFrom", "dateTo", "rigInformationUid"}, new Object[]{dateFrom, dateTo, rigInformationUid});
		return dateList.size() - 1.0; //minus current day
	}
	
	/**
	 * Method to update all report daily Days Since Last LTI , which start from last 2 LTI till next 2 LTI of current day
	 * @param groupUid
	 * @param rigInformationUid
	 * @param dailyUid
	 * @throws Exception Standard Error Throwing Exception
	 */
	public static void updateAllReportDailyDaysSinceLastLti(String groupUid, String rigInformationUid, String dailyUid) throws Exception {
		Date dateFrom = null;
		Date dateTo = null;
		HseIncident lastHseIncident = CommonUtil.getConfiguredInstance().lastLTI(rigInformationUid, dailyUid);
		if (lastHseIncident!=null) {
			Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getCachedDaily(lastHseIncident.getDailyUid());

			if (daily!=null) {
				String queryString = "FROM ReportDaily WHERE (isDeleted=false or isDeleted is null) " +
						"AND rigInformationUid=:rigInformationUid " +
						"AND reportDatetime<:todayDate " +
						"ORDER BY reportDatetime DESC";
				List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"rigInformationUid","todayDate"}, new Object[]{rigInformationUid, daily.getDayDate()});
				if (reportDailyList.size()>0) {
					ReportDaily previousDaily = reportDailyList.get(0);
					if (previousDaily!=null) {
						lastHseIncident = CommonUtil.getConfiguredInstance().lastLTI(rigInformationUid, previousDaily.getDailyUid());
						if (lastHseIncident!=null) {
							dateFrom = lastHseIncident.getHseEventdatetime();
						}
					}
				}
			}
		}
		HseIncident nextHseIncident = CommonUtil.getConfiguredInstance().nextLTI(rigInformationUid, dailyUid);
		if (nextHseIncident!=null) {
			dateTo = nextHseIncident.getHseEventdatetime();
			/*Daily nextDaily = ApplicationUtils.getConfiguredInstance().getTomorrow(nextHseIncident.getOperationUid(), nextHseIncident.getDailyUid());
			if (nextDaily!=null) {
				nextHseIncident = CommonUtil.getConfiguredInstance().lastLTI(rigInformationUid, nextDaily.getDailyUid());
				if (nextHseIncident!=null) {
					dateTo = nextHseIncident.getHseEventdatetime();
				}
			}*/
		}
		RigUtilizationUtils.updateAllReportDailyDaysSinceLastLti(groupUid, rigInformationUid, dateFrom, dateTo);
	}
	
	/**
	 * Method to update all report daily Days Since Last LTI , which between 2 dates
	 * @param groupUid
	 * @param rigInformationUid
	 * @param dateFrom
	 * @param dateTo
	 * @throws Exception Standard Error Throwing Exception
	 */
	public static void updateAllReportDailyDaysSinceLastLti(String groupUid, String rigInformationUid, Date dateFrom, Date dateTo) throws Exception {
		if (dateFrom==null) dateFrom = RigUtilizationUtils.getFirstRigDate(rigInformationUid);
		if (dateTo==null) dateTo = RigUtilizationUtils.getLastRigDate(rigInformationUid);
		if (dateFrom==null) dateFrom = new Date();
		if (dateTo==null) dateTo = new Date();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateFrom);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND , 0);
		calendar.set(Calendar.MILLISECOND , 0);
		dateFrom = calendar.getTime();
		
		calendar.setTime(dateTo);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND , 0);
		calendar.set(Calendar.MILLISECOND , 0);
		dateTo = calendar.getTime();
		
		Double duration = null;
		Date nextChangeDate = null;
		Date previousReportDate = null;
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		String queryString = "FROM ReportDaily " +
				"WHERE (isDeleted=false or isDeleted is null) " +
				"AND rigInformationUid=:rigInformationUid " +
				"AND reportDatetime>=:dateFrom " +
				"AND reportDatetime<=:dateTo " +
				"ORDER BY reportDatetime";
		List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"rigInformationUid", "dateFrom", "dateTo"}, new Object[]{rigInformationUid, dateFrom, dateTo}, qp);
		for (ReportDaily reportDaily : reportDailyList) {
			
			if (nextChangeDate==null || DateUtils.isSameDay(nextChangeDate,reportDaily.getReportDatetime()) || nextChangeDate.before(reportDaily.getReportDatetime())) {
				duration = RigUtilizationUtils.calculateDaysSinceLastLTI(groupUid, rigInformationUid, reportDaily.getDailyUid());
				nextChangeDate = RigUtilizationUtils.getNextDateChangedForDaysSinceLta(rigInformationUid, reportDaily.getDailyUid());
			} else {
				if (duration!=null) {
					if (previousReportDate==null || !DateUtils.isSameDay(previousReportDate, reportDaily.getReportDatetime())) {
						duration += 86400.0;
					}
				}
			}
			previousReportDate = reportDaily.getReportDatetime();
			if (reportDaily.getDaysSinceLta()==null && duration==null) continue;
			if (duration!=null && duration.equals(reportDaily.getDaysSinceLta())) continue;
			String hql = "UPDATE ReportDaily set daysSinceLta=:duration WHERE reportDailyUid=:reportDailyUid";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(hql, new String[]{"duration","reportDailyUid"}, new Object[]{duration, reportDaily.getReportDailyUid()}, qp);
		}
	}
	
	/**
	 * Method to get next change date (LTI / Rig Contrat Start/End)
	 * @param rigInformationUid
	 * @param dailyUid
	 * @throws Exception Standard Error Throwing Exception
	 * @return date
	 */
	public static Date getNextDateChangedForDaysSinceLta(String rigInformationUid, String dailyUid) throws Exception {
		if (dailyUid==null) return null;
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
		if (daily==null) return null;
		Date currentDate = daily.getDayDate();
		
		Date nearestDateChange = null;
		String queryString = "FROM RigContract " +
				"WHERE (isDeleted=false or isDeleted is null) " +
				"AND rigInformationUid=:rigInformationUid " +
				"AND contractEndDatetime>=:date " +
				"ORDER BY contractEndDatetime ";
		List<RigContract> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"rigInformationUid","date"}, new Object[]{rigInformationUid,currentDate});
		if (list.size()>0) {
			RigContract rigContract = list.get(0);
			if (rigContract.getContractStartDatetime()!=null && currentDate.before(rigContract.getContractStartDatetime())) {
				nearestDateChange = rigContract.getContractStartDatetime();
			} else if (rigContract.getContractEndDatetime()!=null && currentDate.before(rigContract.getContractEndDatetime())) {
				nearestDateChange = rigContract.getContractEndDatetime();
				nearestDateChange = DateUtils.addDays(nearestDateChange, 1);
			}
		}
		
		HseIncident nextLti = CommonUtil.getConfiguredInstance().nextLTI(rigInformationUid, dailyUid);
		if (nextLti!=null && nextLti.getHseEventdatetime()!=null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(nextLti.getHseEventdatetime());
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND , 0);
			calendar.set(Calendar.MILLISECOND , 0);
			Date ltiDate = calendar.getTime();
			if (nearestDateChange==null || nearestDateChange.after(ltiDate)) {
				nearestDateChange = nextLti.getHseEventdatetime();
			}
		}
		
		return nearestDateChange;
	}
	
	/**
	 * Method to get first date of the rig operater
	 * @param rigInformationUid
	 * @throws Exception Standard Error Throwing Exception
	 * @return date
	 */
	public static Date getFirstRigDate(String rigInformationUid) throws Exception {
		String queryString = "SELECT MIN(reportDatetime) FROM ReportDaily WHERE (isDeleted=false or isDeleted is null) AND rigInformationUid=:rigInformationUid";
		List<Date> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "rigInformationUid", rigInformationUid);
		if (list.size()>0) return list.get(0);
		return null;
	}
	
	/**
	 * Method to get last date of the rig operater
	 * @param rigInformationUid
	 * @throws Exception Standard Error Throwing Exception
	 * @return date
	 */
	public static Date getLastRigDate(String rigInformationUid) throws Exception {
		String queryString = "SELECT MAX(reportDatetime) FROM ReportDaily WHERE (isDeleted=false or isDeleted is null) AND rigInformationUid=:rigInformationUid";
		List<Date> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "rigInformationUid", rigInformationUid);
		if (list.size()>0) return list.get(0);
		return null;
	}
	
	/**
	 * Method to update all report daily Days Since Last LTI , which between 2 dates
	 * @param groupUid
	 * @param operationUid
	 * @throws Exception Standard Error Throwing Exception
	 */
	public static void updateAllReportDailyDaysSinceLastLtiViaStt(String groupUid, String operationUid) throws Exception {
		String queryString = "select rigInformationUid, min(reportDatetime) FROM ReportDaily " +
				"WHERE (isDeleted=false or isDeleted is null) " +
				"AND operationUid=:operationUid " +
				"GROUP BY rigInformationUid ";
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid);
		for (Object[] rec : list) {
			String rigInformationUid = (String) rec[0];
			Date date = (Date) rec[1];
			if (rigInformationUid==null) continue;
			RigUtilizationUtils.updateAllReportDailyDaysSinceLastLti(groupUid, rigInformationUid, date, null);
		}
	}
}