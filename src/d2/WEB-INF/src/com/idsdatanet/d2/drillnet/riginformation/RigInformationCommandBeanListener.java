package com.idsdatanet.d2.drillnet.riginformation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.RigCompressors;
import com.idsdatanet.d2.core.model.RigContainers;
import com.idsdatanet.d2.core.model.RigGenerators;
import com.idsdatanet.d2.core.model.RigHydrTongs;
import com.idsdatanet.d2.core.model.RigMobileWinch;
import com.idsdatanet.d2.core.model.RigPowerUnitHydrTongs;
import com.idsdatanet.d2.core.model.RigPowerUnitRotaryTable;
import com.idsdatanet.d2.core.model.RigPreventorControlUnit;
import com.idsdatanet.d2.core.model.RigPump;
import com.idsdatanet.d2.core.model.RigRotaryTable;
import com.idsdatanet.d2.core.model.RigSubstructure;
import com.idsdatanet.d2.core.model.RigSwivel;
import com.idsdatanet.d2.core.model.RigTanks;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class RigInformationCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {

    @Override
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigMobileWinch.class))) {
			if("winchNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				winchEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigPump.class))) {
			if("lookupRigEquipmentListUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				pumpEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigPreventorControlUnit.class))) {
			if("pcuNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				preventorEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigPowerUnitHydrTongs.class))) {
			if("puTongsNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				powerHydroEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigHydrTongs.class))) {
			if("tongsNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				hydroTongsEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigPowerUnitRotaryTable.class))) {
			if("puRtNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				powerRotaryEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigRotaryTable.class))) {
			if("rtNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				rotaryTableEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigGenerators.class))) {
			if("lookupRigEquipmentListUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				rigGenEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigContainers.class))) {
			if("lookupRigEquipmentListUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				rigConEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigTanks.class))) {
			if("tankNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				tankEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigSubstructure.class))) {
			if("substructureNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				subStrucEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigCompressors.class))) {
			if("lookupRigEquipmentListUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				compressorEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
        if (targetCommandBeanTreeNode != null && (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigSwivel.class))) {
			if("swivelNumber".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				swivelEquipment(request, commandBean, targetCommandBeanTreeNode);
			}
		}
	}

    public void winchEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	
    	if(node.getData() instanceof RigMobileWinch){
    		RigMobileWinch thisRigMobileWinch = (RigMobileWinch) node.getData();
    		String equipmentCode = thisRigMobileWinch.getWinchNumber();
    		
    		if (StringUtils.isNotBlank(equipmentCode)) {
    			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select makeModel,type,serialNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
    			if (lstResult.size() > 0) {
    				Object[] a = (Object[]) lstResult.get(0);
    				if (a[0] != null) {
        				thisRigMobileWinch.setMakeModel(a[0].toString());
    				} else {
    					thisRigMobileWinch.setMakeModel(null);
    				}
    				if (a[1] != null) {
    					thisRigMobileWinch.setType(a[1].toString());
    				} else {
    					thisRigMobileWinch.setType(null);
    				}
    				if (a[2] != null) {
    					thisRigMobileWinch.setSerialNumber(a[2].toString());
    				} else {
    					thisRigMobileWinch.setSerialNumber(null);
    				}
    			}
    		}
		}
	}
    
    public void pumpEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	
    	if(node.getData() instanceof RigPump){
    		RigPump thisRigPump = (RigPump) node.getData();
    		String equipmentCode = thisRigPump.getLookupRigEquipmentListUid();
    		
    		if (StringUtils.isNotBlank(equipmentCode)) {
    			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select manufacturerUid,makeModel,type,serialNumber,name from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
    			if (lstResult.size() > 0) {
    				Object[] a = (Object[]) lstResult.get(0);
    				if (a[0] != null) {
    					thisRigPump.setManufacturerUid(a[0].toString());
    				} else {
    					thisRigPump.setManufacturerUid(null);
    				}
    				if (a[1] != null) {
    					thisRigPump.setUnitModel(a[1].toString());
    				} else {
    					thisRigPump.setUnitModel(null);
    				}
    				if (a[2] != null) {
    					thisRigPump.setPumpType(a[2].toString());
    				} else {
    					thisRigPump.setPumpType(null);
    				}
    				if (a[3] != null) {
    					thisRigPump.setUnitSerialNumber(a[3].toString());
    				} else {
    					thisRigPump.setUnitSerialNumber(null);
    				}
    				if (a[4] != null) {
    					thisRigPump.setUnitName(a[4].toString());
    				} else {
    					thisRigPump.setUnitName(null);
    				}
    			}
    		}
		}
	}
    
    public void preventorEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	
    	if(node.getData() instanceof RigPreventorControlUnit){
    		RigPreventorControlUnit thisPreventor = (RigPreventorControlUnit) node.getData();
    		String equipmentCode = thisPreventor.getPcuNumber();
    		
    		if (StringUtils.isNotBlank(equipmentCode)) {
    			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select makeModel,type,serialNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
    			if (lstResult.size() > 0) {
    				Object[] a = (Object[]) lstResult.get(0);
    				if (a[0] != null) {
    					thisPreventor.setMakeModel(a[0].toString());
    				} else {
    					thisPreventor.setMakeModel(null);
    				}
    				if (a[1] != null) {
    					thisPreventor.setType(a[1].toString());
    				} else {
    					thisPreventor.setType(null);
    				}
    				if (a[2] != null) {
    					thisPreventor.setSerialNumber(a[2].toString());
    				} else {
    					thisPreventor.setSerialNumber(null);
    				}
    			}
    		}
		}
	}
    
    public void powerHydroEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	
    	if(node.getData() instanceof RigPowerUnitHydrTongs){
    		RigPowerUnitHydrTongs thisPowerHydro = (RigPowerUnitHydrTongs) node.getData();
    		String equipmentCode = thisPowerHydro.getPuTongsNumber();
    		
    		if (StringUtils.isNotBlank(equipmentCode)) {
    			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select makeModel,type,serialNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
    			if (lstResult.size() > 0) {
    				Object[] a = (Object[]) lstResult.get(0);
    				if (a[0] != null) {
    					thisPowerHydro.setMakeModel(a[0].toString());
    				} else {
    					thisPowerHydro.setMakeModel(null);
    				}
    				if (a[1] != null) {
    					thisPowerHydro.setType(a[1].toString());
    				} else {
    					thisPowerHydro.setType(null);
    				}
    				if (a[2] != null) {
    					thisPowerHydro.setSerialNumber(a[2].toString());
    				} else {
    					thisPowerHydro.setSerialNumber(null);
    				}
    			}
    		}
		}
	}
    
    public void hydroTongsEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	
    	if(node.getData() instanceof RigHydrTongs){
    		RigHydrTongs thisHydroTongs = (RigHydrTongs) node.getData();
    		String equipmentCode = thisHydroTongs.getTongsNumber();
    		
    		if (StringUtils.isNotBlank(equipmentCode)) {
    			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select makeModel,type,serialNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
    			if (lstResult.size() > 0) {
    				Object[] a = (Object[]) lstResult.get(0);
    				if (a[0] != null) {
    					thisHydroTongs.setMakeModel(a[0].toString());
    				} else {
    					thisHydroTongs.setMakeModel(null);
    				}
    				if (a[1] != null) {
    					thisHydroTongs.setType(a[1].toString());
    				} else {
    					thisHydroTongs.setType(null);
    				}
    				if (a[2] != null) {
    					thisHydroTongs.setSerialNumber(a[2].toString());
    				} else {
    					thisHydroTongs.setSerialNumber(null);
    				}
    			}
    		}
		}
	}
    
    public void powerRotaryEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	
    	if(node.getData() instanceof RigPowerUnitRotaryTable){
    		RigPowerUnitRotaryTable thisPowerRotary = (RigPowerUnitRotaryTable) node.getData();
    		String equipmentCode = thisPowerRotary.getPuRtNumber();
    		
    		if (StringUtils.isNotBlank(equipmentCode)) {
    			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select makeModel,type,serialNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
    			if (lstResult.size() > 0) {
    				Object[] a = (Object[]) lstResult.get(0);
    				if (a[0] != null) {
    					thisPowerRotary.setMakeModel(a[0].toString());
    				} else {
    					thisPowerRotary.setMakeModel(null);
    				}
    				if (a[1] != null) {
    					thisPowerRotary.setType(a[1].toString());
    				} else {
    					thisPowerRotary.setType(null);
    				}
    				if (a[2] != null) {
    					thisPowerRotary.setSerialNumber(a[2].toString());
    				} else {
    					thisPowerRotary.setSerialNumber(null);
    				}
    			}
    		}
		}
	}
    
    public void rotaryTableEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	
    	if(node.getData() instanceof RigRotaryTable){
    		RigRotaryTable thisRotaryTable = (RigRotaryTable) node.getData();
    		String equipmentCode = thisRotaryTable.getRtNumber();
    		
    		if (StringUtils.isNotBlank(equipmentCode)) {
    			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select makeModel,type,serialNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
    			if (lstResult.size() > 0) {
    				Object[] a = (Object[]) lstResult.get(0);
    				if (a[0] != null) {
    					thisRotaryTable.setMakeModel(a[0].toString());
    				} else {
    					thisRotaryTable.setMakeModel(null);
    				}
    				if (a[1] != null) {
    					thisRotaryTable.setType(a[1].toString());
    				} else {
    					thisRotaryTable.setType(null);
    				}
    				if (a[2] != null) {
    					thisRotaryTable.setSerialNumber(a[2].toString());
    				} else {
    					thisRotaryTable.setSerialNumber(null);
    				}
    			}
    		}
		}
	}
    
    public void rigGenEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	
    	if(node.getData() instanceof RigGenerators){
    		RigGenerators thisRigGen = (RigGenerators) node.getData();
    		String equipmentCode = thisRigGen.getLookupRigEquipmentListUid();
    		
    		if (StringUtils.isNotBlank(equipmentCode)) {
    			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select makeModel,type,serialNumber,componentNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
    			if (lstResult.size() > 0) {
    				Object[] a = (Object[]) lstResult.get(0);
    				if (a[0] != null) {
    					thisRigGen.setMakeModel(a[0].toString());
    				} else {
    					thisRigGen.setMakeModel(null);
    				}
    				if (a[1] != null) {
    					thisRigGen.setType(a[1].toString());
    				} else {
    					thisRigGen.setType(null);
    				}
    				if (a[2] != null) {
    					thisRigGen.setSerialNumber(a[2].toString());
    				} else {
    					thisRigGen.setSerialNumber(null);
    				}
    				if (a[3] != null) {
    					thisRigGen.setEngineNumber(a[3].toString());
    				} else {
    					thisRigGen.setEngineNumber(null);
    				}
    			}
    		}
		}
	}
    
    public void rigConEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	
    	if(node.getData() instanceof RigContainers){
    		RigContainers thisRigCon = (RigContainers) node.getData();
    		String equipmentCode = thisRigCon.getLookupRigEquipmentListUid();
    		
    		if (StringUtils.isNotBlank(equipmentCode)) {
				List <String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select serialNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
				if (lstResult.size() > 0) {
					String serialNumber = lstResult.get(0);
    				if (serialNumber != null){
    					thisRigCon.setSerialNumber(serialNumber);
    				} else {
    					thisRigCon.setSerialNumber(null);
    				}
				}
			}
		}
	}

    public void tankEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	
    	if(node.getData() instanceof RigTanks){
    		RigTanks thisRigTanks = (RigTanks) node.getData();
    		String equipmentCode = thisRigTanks.getTankNumber();
    		
    		if (StringUtils.isNotBlank(equipmentCode)) {
    			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select maxVolumeCapacity,serialNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
    			if (lstResult.size() > 0) {
    				Object[] a = (Object[]) lstResult.get(0);
    				if (a[0] != null) {
    					thisRigTanks.setMaxVolumeCapacity((Double) a[0]);
    				} else {
    					thisRigTanks.setMaxVolumeCapacity(null);
    				}
    				if (a[1] != null) {
    					thisRigTanks.setSerialNumber(a[1].toString());
    				} else {
    					thisRigTanks.setSerialNumber(null);
    				}
    			}
    		}
		}
	}
    
    public void subStrucEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
    	if(node.getData() instanceof RigSubstructure){
    		RigSubstructure thisRigSub = (RigSubstructure) node.getData();
    		String equipmentCode = thisRigSub.getSubstructureNumber();
    		
    		if (StringUtils.isNotBlank(equipmentCode)) {
    			List <String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select serialNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
    			if (lstResult.size() > 0) {
    				String serialNumber = lstResult.get(0);
    				if (serialNumber != null){
    					thisRigSub.setSerialNumber(serialNumber);
    				} else {
    					thisRigSub.setSerialNumber(null);
    				}
    			}
    		}
		}
	}

	public void compressorEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
		
		if(node.getData() instanceof RigCompressors){
			RigCompressors thisRigCompressor = (RigCompressors) node.getData();
			String equipmentCode = thisRigCompressor.getLookupRigEquipmentListUid();
			
			if (StringUtils.isNotBlank(equipmentCode)) {
				List <String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select serialNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
				if (lstResult.size() > 0) {
					String serialNumber = lstResult.get(0);
    				if (serialNumber != null){
    					thisRigCompressor.setSerialNumber(serialNumber);
    				} else {
    					thisRigCompressor.setSerialNumber(null);
    				}
				}
			}
		}
	}

	public void swivelEquipment(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
		
		if(node.getData() instanceof RigSwivel){
			RigSwivel thisRigSwivel = (RigSwivel) node.getData();
			String equipmentCode = thisRigSwivel.getSwivelNumber();
			
			if (StringUtils.isNotBlank(equipmentCode)) {
				List <String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select serialNumber from LookupRigEquipmentList where (isDeleted = false or isDeleted is null) and lookupRigEquipmentListUid = :equipmentCode", "equipmentCode", equipmentCode);
				if (lstResult.size() > 0) {
					String serialNumber = lstResult.get(0);
    				if (serialNumber != null){
    					thisRigSwivel.setSerialNumber(serialNumber);
    				} else {
    					thisRigSwivel.setSerialNumber(null);
    				}
				}
			}
		}
	}
}
