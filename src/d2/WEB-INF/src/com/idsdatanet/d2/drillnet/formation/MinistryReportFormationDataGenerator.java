package com.idsdatanet.d2.drillnet.formation;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class MinistryReportFormationDataGenerator implements ReportDataGenerator {
	private Boolean wellboreBased = false;
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String thisReportType = null;
		Integer sequence = null;
		Double midNightDepth = null;
		Double lastPrognosedTopMdMsl = 0.0;
		Double nextPrognosedTopMdMsl = 0.0;
		Double lastPrognosedTopTvdMsl = 0.0;
		Double nextPrognosedTopTvdMsl = 0.0;
		Double lastSampleTopMdMsl = 0.0;
		Double nextSampleTopMdMsl = 0.0;
		Double lastSampleTopTvdMsl = 0.0;
		
		//For wellbore-based
		String thiswellboreUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getWellboreUid().toString())) thiswellboreUid = userContext.getUserSelection().getWellboreUid().toString();
		
		//For well-based
		String thisWellUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getWellUid().toString())) thisWellUid = userContext.getUserSelection().getWellUid().toString();		
		
		String thisOperationUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getOperationUid().toString())) thisOperationUid = userContext.getUserSelection().getOperationUid().toString();
		
		String thisDailyUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getDailyUid().toString())) thisDailyUid = userContext.getUserSelection().getDailyUid().toString();
		
		if (thisReportType==null) thisReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(thisOperationUid);
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "depthMdMsl");

		//Get Midnight depth
		String strSql = "FROM ReportDaily WHERE (isDeleted is null or isDeleted = false) and operationUid =:thisOperationUid and reportType =:thisReportType and dailyUid =:thisDailyUid";
		String[] paramsFields = {"thisOperationUid", "thisReportType", "thisDailyUid"};
		Object[] paramsValues = {thisOperationUid, thisReportType, thisDailyUid};
		
		List<ReportDaily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if (lstResult.size() > 0) {
			ReportDaily thisReportDaily = lstResult.get(0);
			
			if(thisReportDaily != null){		
				if (thisReportDaily.getDepthMdMsl() != null) {
					thisConverter.setBaseValue(thisReportDaily.getDepthMdMsl());
					midNightDepth = thisConverter.getConvertedValue();
				}		
			}
		}
		ReportDataNode thisReportNode = reportDataNode.addChild("Formation");
		
		// Getting Formation Data For Ministry Report.	
		List lResult = null;	
		if(this.wellboreBased){
			String strSql2 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) AND wellboreUid = :thiswellboreUid and sampleTopMdMsl <= :midNightDepth ORDER BY sampleTopMdMsl DESC";
			String[] paramsFields2 = {"thiswellboreUid", "midNightDepth"};
			Object[] paramsValues2 = {thiswellboreUid, midNightDepth};
			lResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
		} else{
			String strSql2 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) AND wellUid = :thiswellUid and sampleTopMdMsl <= :midNightDepth ORDER BY sampleTopMdMsl DESC";
			String[] paramsFields2 = {"thiswellUid", "midNightDepth"};
			Object[] paramsValues2 = {thisWellUid, midNightDepth};
			lResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
	    }

		if (lResult.size() > 0 )
		{	
			Formation tmpFormation = (Formation) lResult.get(0);
			
			thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Formation.class, "PrognosedTopMdMsl");
			thisConverter.setReferenceMappingField(Formation.class, "PrognosedTopMdMsl");
			if (tmpFormation.getPrognosedTopMdMsl()!=null){
				thisConverter.setBaseValueFromUserValue(tmpFormation.getPrognosedTopMdMsl(), false);
				lastPrognosedTopMdMsl = (double)Math.round(thisConverter.getConvertedValue() * 10)/10.0;
				thisReportNode.addProperty("lastPrognosedTopMdMsl", lastPrognosedTopMdMsl.toString());
				
				if (thisConverter.isUOMMappingAvailable()){			
					thisReportNode.addProperty("lastPrognosedTopMdMslUom", thisConverter.getUomSymbol());
				}
			}

			if (tmpFormation.getSampleTopMdMsl()!=null){
				thisConverter.setBaseValueFromUserValue(tmpFormation.getSampleTopMdMsl(), false);
				lastSampleTopMdMsl = (double)Math.round(thisConverter.getConvertedValue() * 10)/10.0;
				thisReportNode.addProperty("lastSampleTopMdMsl", lastSampleTopMdMsl.toString());
				
				if (thisConverter.isUOMMappingAvailable()){			
					thisReportNode.addProperty("lastSampleTopMdMslUom", thisConverter.getUomSymbol());
				}
			}

			//Get LastPrognosedTopTvdMsl
			if (tmpFormation.getPrognosedTopTvdMsl()!=null){
				thisConverter.setBaseValueFromUserValue(tmpFormation.getPrognosedTopTvdMsl(), false);
				lastPrognosedTopTvdMsl = (double)Math.round(thisConverter.getConvertedValue() * 10)/10.0;
				thisReportNode.addProperty("lastPrognosedTopTvdMsl", lastPrognosedTopTvdMsl.toString());
				
				if (thisConverter.isUOMMappingAvailable()){			
					thisReportNode.addProperty("lastPrognosedTopTvdMslUom", thisConverter.getUomSymbol());
				}
			}

			//Get LastSampleTopTvdMsl
			if (tmpFormation.getSampleTopTvdMsl()!=null){
				thisConverter.setBaseValueFromUserValue(tmpFormation.getSampleTopTvdMsl(), false);
				lastSampleTopTvdMsl = (double)Math.round(thisConverter.getConvertedValue() * 10)/10.0;
				thisReportNode.addProperty("lastSampleTopTvdMsl", lastSampleTopTvdMsl.toString());
				
				if (thisConverter.isUOMMappingAvailable()){			
					thisReportNode.addProperty("lastSampleTopTvdMslUom", thisConverter.getUomSymbol());
				}
			}
			
			thisReportNode.addProperty("lastFormationName", tmpFormation.getFormationName());
			thisReportNode.addProperty("lastFormationUid", tmpFormation.getFormationUid());
			
			if (tmpFormation.getSequence()!=null) sequence = tmpFormation.getSequence();
		
		
			if (sequence!=null){
				List lResult2 = null;
				if(this.wellboreBased){
				String strSql3 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) AND wellboreUid = :thiswellboreUid and sequence > :sequence ORDER BY sequence";
				String[] paramsFields3 = {"thiswellboreUid", "sequence"};
				Object[] paramsValues3 = {thiswellboreUid, sequence};
				lResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3);
				}
				else{
				String strSql3 = "FROM Formation WHERE (isDeleted = false or isDeleted is null) AND wellUid = :thiswellUid and sequence > :sequence ORDER BY sequence";
				String[] paramsFields3 = {"thiswellUid", "sequence"};
				Object[] paramsValues3 = {thisWellUid, sequence};
				lResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3);
				}
				if (lResult2.size() > 0 )
				{	
					Formation tmpFormation2 = (Formation) lResult2.get(0);
					thisConverter.setReferenceMappingField(Formation.class, "PrognosedTopMdMsl");
					if (tmpFormation2.getPrognosedTopMdMsl()!=null){
						thisConverter.setBaseValueFromUserValue(tmpFormation2.getPrognosedTopMdMsl(), false);

						nextSampleTopMdMsl = (double)Math.round(thisConverter.getConvertedValue() * 10)/10.0;
						thisReportNode.addProperty("nextSampleTopMdMsl", nextSampleTopMdMsl.toString());
						
						if (thisConverter.isUOMMappingAvailable()){
							thisReportNode.addProperty("nextSampleTopMdMslUom", thisConverter.getUomSymbol());
						}

						//Get NextPrognosedTopMdMsl
						nextPrognosedTopMdMsl = (double)Math.round(thisConverter.getConvertedValue() * 10)/10.0;
						thisReportNode.addProperty("nextPrognosedTopMdMsl", nextPrognosedTopMdMsl.toString());
						
						if (thisConverter.isUOMMappingAvailable()){
							thisReportNode.addProperty("nextPrognosedTopMdMslUom", thisConverter.getUomSymbol());
						}
					}

					thisConverter.setReferenceMappingField(Formation.class, "PrognosedTopTvdMsl");
					if (tmpFormation2.getPrognosedTopTvdMsl()!=null){
						thisConverter.setBaseValueFromUserValue(tmpFormation2.getPrognosedTopTvdMsl(), false);

						//Get NextPrognosedTopTvdMsl
						nextPrognosedTopTvdMsl = (double)Math.round(thisConverter.getConvertedValue() * 10)/10.0;
						thisReportNode.addProperty("nextPrognosedTopTvdMsl", nextPrognosedTopTvdMsl.toString());
						
						if (thisConverter.isUOMMappingAvailable()){
							thisReportNode.addProperty("nextPrognosedTopTvdMslUom", thisConverter.getUomSymbol());
						}
					}

					thisReportNode.addProperty("nextFormationName", tmpFormation2.getFormationName());
					thisReportNode.addProperty("nextFormationUid", tmpFormation2.getFormationUid());
				}
			}
		}	
	}
	public void setWellboreBased(boolean wellboreBased) {
		this.wellboreBased = wellboreBased;
	}

	public  boolean isWellboreBased() {
		return wellboreBased;
	}
}
