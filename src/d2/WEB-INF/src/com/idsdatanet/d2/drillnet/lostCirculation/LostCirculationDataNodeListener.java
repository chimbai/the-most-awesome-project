package com.idsdatanet.d2.drillnet.lostCirculation;

import javax.servlet.http.HttpServletRequest;
import com.idsdatanet.d2.core.model.*;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LostCirculationDataNodeListener extends EmptyDataNodeListener {

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof LostCirculation) {
			LostCirculation lostCirculation  = (LostCirculation) object;
			if (lostCirculation != null && lostCirculation.getEndDateTime()!= null && lostCirculation.getStartDateTime() != null){
				if(lostCirculation.getEndDateTime().before(lostCirculation.getStartDateTime())){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "startDateTime", "Start Date cannot be greater than End Date.");
					return;
				}
			}
		}
		
	}
	
}
