
package com.idsdatanet.d2.drillnet.supportVesselParam;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.EnvironmentDischarges;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.model.SupportVesselInformation;
import com.idsdatanet.d2.core.model.SupportVesselParam;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.rigstock.RigStockUtil;

import edu.emory.mathcs.backport.java.util.Arrays;

public class SupportVesselParamDataNodeListener extends EmptyDataNodeListener {
	
	private boolean vesselArrivalDatetime = false;
	
	public void setVesselArrivalDatetime(boolean value){
		this.vesselArrivalDatetime = value;
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RigStock.class, "amt_start");
		if (object instanceof RigStock) {
			RigStock rigstock = (RigStock)object;
			this.setIsBatchDrillFlag(node, userSelection);

			// get the current data
			String stockcode = rigstock.getStockCode();
			Double amtstart = rigstock.getAmtStart();
			Double amtused = rigstock.getAmtUsed();
			Double amtadjust = rigstock.getAmtDiscrepancy();
			Double trftorig = rigstock.getTrfToRig();
			Double trftobeach = rigstock.getTrfToBeach();
			String supportVesselInformationUid = rigstock.getSupportVesselInformationUid(); 
		
			//check if value is null set to 0
			if (amtstart == null) amtstart = 0.0;
			if (amtused == null) amtused = 0.0;
			if (amtadjust == null) amtadjust = 0.0;
			if (trftorig == null) trftorig = 0.0;
			if (trftobeach == null) trftobeach = 0.0;
			
			// set 'amttoday' as the balance for today
			Double amttoday = amtstart - amtused + amtadjust - trftorig - trftobeach;
			
			// get the current date from the session
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null) return;
			String operationUid = userSelection.getOperationUid();
			
			// if 'stockcode' is available
			if (StringUtils.isNotBlank(stockcode)) {
				
				//populate stockBrand to dynamicAttribute @stockName
				node.getDynaAttr().put("stockName", RigStockUtil.getStockName(stockcode));
				
				//check if today is the first day of operation or not to determine want to show the starting amount
				node.getDynaAttr().put("isFirstDay", CommonUtil.getConfiguredInstance().getOperationSupportVesselStockcodeFirstDay(operationUid, supportVesselInformationUid, stockcode, userSelection.getDailyUid()));
				
				// default 'previousBalance' to 0.0
				Double previousBalance = 0.0;
				
				// default 'currentBalance' as today's balance
				Double currentBalance = amttoday;
				
				thisConverter.setReferenceMappingField(RigStock.class, "amtInitial");

				if (rigstock.getAmtInitial()==null)
				{
					previousBalance = CommonUtil.getConfiguredInstance().getOperationSupportVesselStockcodePreviousBalance(operationUid, supportVesselInformationUid, stockcode, daily);
					thisConverter.setBaseValue(previousBalance);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@previousBalance", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("previousBalance", thisConverter.getConvertedValue());
				}else{
					thisConverter.setBaseValueFromUserValue(rigstock.getAmtInitial());
					previousBalance = thisConverter.getBasevalue();
				}
				// set the currentBalance = previousBalance + today's balance, and assign into the dynamic variable
				currentBalance = previousBalance + amttoday;
				thisConverter.setBaseValue(currentBalance);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@currentBalance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("currentBalance", thisConverter.getConvertedValue());	
				
			}
			// if no 'stockcode' available
			else {
				// assign 0 to both balances if no 'stockcode' is found
				thisConverter.setBaseValue(0);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@previousBalance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("previousBalance", thisConverter.getConvertedValue());
				
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@currentBalance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("currentBalance", thisConverter.getConvertedValue());
			}
		
		}
		
		if (! node.getInfo().isTemplateNode() && object instanceof SupportVesselParam)
		{	
			String supportVesselInfoUid = (String) PropertyUtils.getProperty(object, "supportVesselInformationUid");
			String[] paramsFields = {"supportVesselInfoUid"};
			Object[] paramsValues = {supportVesselInfoUid}; 
			
			String strSql = "select operator from SupportVesselInformation where (isDeleted = false or isDeleted is null) and supportVesselInformationUid = :supportVesselInfoUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (lstResult.size()>0)
			{
				Object a = (Object) lstResult.get(0);
				String operator = (String) a;
				node.getDynaAttr().put("companyid", operator);
			}
		
		}
		
		if (object instanceof HseIncident) {
			HseIncident hseIncident = (HseIncident) object;
			Date eventDateTime = hseIncident.getHseEventdatetime();
			//if (eventDateTime != null && StringUtils.isNotBlank(userSelection.getDailyUid())) {
			if (eventDateTime != null && StringUtils.isNotBlank(hseIncident.getDailyUid())) {
				//Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(hseIncident.getDailyUid());
				if (daily != null) {
					Date dayDate = daily.getDayDate();
					if (dayDate != null) {	
						double daysLapsed = CommonUtil.getConfiguredInstance().calculateDaysSinceHseIncident(dayDate, eventDateTime, userSelection.getGroupUid());
						
						//assign unit to the value base on daily days_since_lta unit
						//CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "days_since_lta");
						thisConverter.setReferenceMappingField(ReportDaily.class, "days_since_lta");
						thisConverter.setBaseValue(daysLapsed);	
						if (thisConverter.isUOMMappingAvailable()) {
							node.setCustomUOM("@daysLapsed", thisConverter.getUOMMapping());   
						}
						node.getDynaAttr().put("daysLapsed", thisConverter.getConvertedValue());
					}
				}
			}
			this.includeNextEventDate(node);
		}
	
	}
	
	public void includeNextEventDate(CommandBeanTreeNode node) throws Exception
	{
		HseIncident hseIncident = (HseIncident) node.getData();
		if(hseIncident.getHseEventdatetime()!=null && !StringUtils.isBlank(hseIncident.getFrequency()))
		{
			String frequency = hseIncident.getFrequency();
			if (frequency.length()>1)
			{
				String[] tokenList={"d","m","y"};
				String token=frequency.substring(0,1);
				String tokenVal=frequency.substring(1,frequency.length());
				if (Arrays.asList(tokenList).contains(token) && NumberUtils.isNumber(tokenVal) && Integer.parseInt(tokenVal)>0)
				{
					Date nextEventDate=dateAdd(hseIncident.getHseEventdatetime(),token,Integer.parseInt(tokenVal));
					node.getDynaAttr().put("nextHseEventdatetime",nextEventDate);
				}
			}
		}
	}
	private void setIsBatchDrillFlag(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception
	{
		Operation op = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		if (op.getIsBatchDrill()!=null && op.getIsBatchDrill())
		{
			node.getDynaAttr().put("isBatch", "1");
		}
	}
	public Date dateAdd(Date date1,String token,int tokenVal){
		if(token.equalsIgnoreCase("d")){
			return DateUtils.addDays(date1, tokenVal);
		}else if(token.equalsIgnoreCase("m")){
			return DateUtils.addMonths(date1, tokenVal);
		}else if(token.equalsIgnoreCase("y")){	
			return DateUtils.addYears(date1, tokenVal);
		}
		return date1;
	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(session);
		
		Object object = node.getData();
		if (object instanceof SupportVesselParam) {
			SupportVesselParam supportVesselParam = (SupportVesselParam) object;
			
			if(supportVesselParam.getArrdt()!= null && supportVesselParam.getDepdt()!= null){
				if(supportVesselParam.getArrdt().getTime() > supportVesselParam.getDepdt().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "arrdt", "Arrival Date time cannot be greater than Departure Date time");
					return;
				}
			}
			
			if(vesselArrivalDatetime){
				if(supportVesselParam.getVesselArrivalDatetime() != null && supportVesselParam.getVesselDepartureDatetime() != null){
					if(supportVesselParam.getVesselArrivalDatetime().getTime() > supportVesselParam.getVesselDepartureDatetime().getTime()){
						status.setContinueProcess(false, true);
						status.setFieldError(node, "vesselArrivalDatetime", "Arrival Date time cannot be greater than Departure Date time");
						return;
					}
				}
			}
			
			String vesselName = (String) node.getDynaAttr().get("vesselName");
			
			if (StringUtils.isNotBlank(vesselName)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from SupportVesselInformation where (isDeleted = false or isDeleted is null) and vesselName = :vesselName", "vesselName", vesselName);
				if (list.size() > 0) {
					status.setDynamicAttributeError(node, "vesselName", "The vessel '" + vesselName + "' already exists in database");
					status.setContinueProcess(false, true);
				}
				
				if (status.isProceed()) {
					SupportVesselInformation supportVesselInformation = CommonUtil.getConfiguredInstance().createSupportVesselInformation(vesselName);
					commandBean.getSystemMessage().addInfo("A new support vessel information has been created with minimal information from the Support Vessel screen. Please make sure that the Support Vessel Information module is reviewed and relevant details are added in.");
					supportVesselParam.setSupportVesselInformationUid(supportVesselInformation.getSupportVesselInformationUid());
				}
			}	
			
		}
		else if (object instanceof RigStock) {
			Object parentObject = node.getParent().getData();
			SupportVesselParam vessel = null;
			if (parentObject instanceof SupportVesselParam)
				vessel = (SupportVesselParam)parentObject;
			
			RigStock rigstock = (RigStock)object;
			String stockcode = rigstock.getStockCode();
			Double amtstart = rigstock.getAmtStart();
			Double amtused = rigstock.getAmtUsed();
			Double amtdiscrepancy = rigstock.getAmtDiscrepancy();
			Double trftorig = rigstock.getTrfToRig();			
			Double trftobeach = rigstock.getTrfToBeach();
			rigstock.setType("rigbulkstock");
			
			if (StringUtils.isNotBlank(stockcode)) {
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select storedUnit from LookupRigStock where (isDeleted = false or isDeleted is null) and lookupRigStockUid = :stockCode", "stockCode", stockcode);
				if (lstResult.size() > 0) rigstock.setStoredUnit(lstResult.get(0).toString());
			} else {
				status.setContinueProcess(false, true);
				status.setFieldError(node, "stockBrand", "Stock Name is required.");
				return;
			}
			
			String rigStockUid = rigstock.getRigStockUid();
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(session.getCurrentDailyUid());
			String supportVesselInfoUid = "";			
			if ( node.getParent().getData() instanceof SupportVesselParam ){
				SupportVesselParam supportVesselParam = (SupportVesselParam) node.getParent().getData();
				supportVesselInfoUid = supportVesselParam.getSupportVesselInformationUid();
			}
			
			List lstResult = null;			
						
			if ( rigStockUid != null ){	
				String[] paramsFields = {"stockCode", "dailyUid", "stockType", "supportVesselParamUid", "rigStockUid"};
				Object[] paramsValues = {stockcode, daily.getDailyUid(), rigstock.getType(), supportVesselInfoUid, rigStockUid}; 
				String strSql = "from RigStock where (isDeleted = false or isDeleted is null) and stockCode = :stockCode and dailyUid = :dailyUid and type = :stockType and supportVesselInformationUid = :supportVesselParamUid and rigStockUid != :rigStockUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);				
			}else{
				String[] paramsFields = {"stockCode", "dailyUid", "stockType", "supportVesselParamUid"};
				Object[] paramsValues = {stockcode, daily.getDailyUid(), rigstock.getType(), supportVesselInfoUid}; 
				String strSql = "from RigStock where (isDeleted = false or isDeleted is null) and stockCode = :stockCode and dailyUid = :dailyUid and type = :stockType and supportVesselInformationUid = :supportVesselParamUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			}				

			if ( lstResult != null && lstResult.size() > 0 ){
				status.setContinueProcess(false, true);
				status.setFieldError(node, "stockCode", "Duplicate Stock Item.");					
				return;
			}			
						
			if (amtstart == null) {
				rigstock.setAmtStart(0.0);
			}
			if (amtused == null) {
				rigstock.setAmtUsed(0.0);
			}
			if (amtdiscrepancy == null) {
				rigstock.setAmtDiscrepancy(0.0);
			}
			if (trftorig == null) {
				rigstock.setTrfToRig(0.0);
			}
			if (trftobeach == null) {
				rigstock.setTrfToBeach(0.0);
			}
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RigStock.class, "amt_start");

			//SET CHECKING TO DISALLOW DATA SAVING FOR STOCK WITH NEGATIVE BALANCE
			Double startingAmount = 0.0;
			Double currentBalance = 0.0;
			Double amountToday = rigstock.getAmtStart() - rigstock.getAmtUsed() + rigstock.getAmtDiscrepancy();
			
			//CHECK IF THIS RECORD IS THE FIRST RECORD FOR THIS OPERATION
			String isFirstDay = CommonUtil.getConfiguredInstance().getOperationSupportVesselStockcodeFirstDay(session.getCurrentOperationUid(), vessel.getSupportVesselInformationUid(), stockcode, userSelection.getDailyUid());
			
			Boolean isBatchDrill = session.getCurrentOperation().getIsBatchDrill();
			if (isBatchDrill==null)
				isBatchDrill=false;
			
			if (isBatchDrill)
			{
				if (rigstock.getAmtInitial()!=null)
					startingAmount = rigstock.getAmtInitial();
				else
					startingAmount = CommonUtil.getConfiguredInstance().getOperationSupportVesselStockcodePreviousBalance(session.getCurrentOperationUid(), vessel.getSupportVesselInformationUid(), stockcode, daily);
			}else{
				//IF YES, GET amtInitial
				if(isFirstDay.equals("1"))
				{
					if(rigstock.getAmtInitial() != null)
					{
						startingAmount = rigstock.getAmtInitial();
					}
				}
				//GET FROM PREVIOUS DAY
				else
				{
					startingAmount = CommonUtil.getConfiguredInstance().getOperationSupportVesselStockcodePreviousBalance(session.getCurrentOperationUid(), vessel.getSupportVesselInformationUid(), stockcode, daily);
				}
			}
			currentBalance = startingAmount + amountToday;
			
			//IF CURRENT BALANCE IS NEGATIVE
			if(currentBalance < 0){
				
				thisConverter.setBaseValue(currentBalance);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@currentBalance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("currentBalance", thisConverter.getConvertedValue());			
				status.setFieldError(node, "stockCode", "Current Balance is negative.");
				status.setFieldError(node, "amtDiscrepancy", "Please adjust.");
				
				//BREAK SAVE PROCESS
				status.setContinueProcess(false, true);
				return;
			}
			else
			{
				thisConverter.setBaseValue(currentBalance);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@currentBalance", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("currentBalance", thisConverter.getConvertedValue());
			}
			
			
			rigstock.setRigInformationUid(null);
		}
		
		if (object instanceof HseIncident) {
			HseIncident hseIncident = (HseIncident) object;
			
			//hseIncident.setRigInformationUid(session.getCurrentRigInformationUid());
			
			if (hseIncident.getNumberOfIncidents() == null) hseIncident.setNumberOfIncidents(1);
		}
	}
	
	@Override
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Double sumAmount = null;
		Object obj = node.getData();
		if (obj instanceof RigStock) {
			String dailyUid = session.getCurrentDailyUid();
			String sql = "SELECT SUM(amtUsed) FROM RigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND supportVesselInformationUid IS NOT NULL " 
					+ "AND dailyUid = :dailyUid " 
					+ "AND stockCode IN (SELECT lookupRigStockUid FROM LookupRigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND stockBrand = 'DIESEL')";
					String[] paramsFields = {"dailyUid"};
					Object[] paramsValues = {dailyUid};
			List res = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,paramsFields,paramsValues);
			if(res.size()>0){
				if(res.get(0) != null) {
					sumAmount = Double.parseDouble(res.get(0).toString());
					String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='Vessel Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
					String[] paramsFields1 = {"dailyUid"};
					String[] paramsValues1 = {dailyUid};
					List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
					if (resED.size() > 0) {
						for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
							thisEnvironmentDischarges.setActualAmount(sumAmount);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
						}
					}	
				}else {
					String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='Vessel Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
					String[] paramsFields1 = {"dailyUid"};
					String[] paramsValues1 = {dailyUid};
					List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
					if (resED.size() > 0) {
						for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
							thisEnvironmentDischarges.setActualAmount(sumAmount);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
						}
					}
				}
			}else {
				String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='Vessel Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
				String[] paramsFields1 = {"dailyUid"};
				String[] paramsValues1 = {dailyUid};
				List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
				if (resED.size() > 0) {
					for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
						thisEnvironmentDischarges.setActualAmount(sumAmount);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
					}
				}
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		Double sumAmount = null;
		Object obj = node.getData();
		if (obj instanceof RigStock) {
			String dailyUid = session.getCurrentDailyUid();
			String sql = "SELECT SUM(amtUsed) FROM RigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND supportVesselInformationUid IS NOT NULL " 
					+ "AND dailyUid = :dailyUid " 
					+ "AND stockCode IN (SELECT lookupRigStockUid FROM LookupRigStock WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND stockBrand = 'DIESEL')";
					String[] paramsFields = {"dailyUid"};
					Object[] paramsValues = {dailyUid};
			List res = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql,paramsFields,paramsValues);
			if(res.size()>0){
				if(res.get(0) != null) {
					sumAmount = Double.parseDouble(res.get(0).toString());
					String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='Vessel Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
					String[] paramsFields1 = {"dailyUid"};
					String[] paramsValues1 = {dailyUid};
					List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
					if (resED.size() > 0) {
						for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
							thisEnvironmentDischarges.setActualAmount(sumAmount);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
						}
					}	
				}else {
					String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='Vessel Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
					String[] paramsFields1 = {"dailyUid"};
					String[] paramsValues1 = {dailyUid};
					List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
					if (resED.size() > 0) {
						for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
							thisEnvironmentDischarges.setActualAmount(sumAmount);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
						}
					}
				}
			}else {
				String strSql = "FROM EnvironmentDischarges WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND lookupEnvironmentDischargesUid IN (SELECT lookupEnvironmentDischargesUid FROM LookupEnvironmentDischarges WHERE name='Vessel Diesel' AND (isDeleted IS NULL OR isDeleted = FALSE)) AND dailyUid =:dailyUid AND category='emissions'";
				String[] paramsFields1 = {"dailyUid"};
				String[] paramsValues1 = {dailyUid};
				List<EnvironmentDischarges> resED = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields1,paramsValues1);
				if (resED.size() > 0) {
					for(EnvironmentDischarges thisEnvironmentDischarges : resED) {
						thisEnvironmentDischarges.setActualAmount(sumAmount);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisEnvironmentDischarges);
					}
				}
			}
		}
		ApplicationUtils.getConfiguredInstance().refreshCachedData();
	}

	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{	//on paste as new record data with initial amount will be set to null
		
		Object object = targetNode.getData();
		
		if (object instanceof RigStock) {
			this.setIsBatchDrillFlag(targetNode, new UserSelectionSnapshot(userSession));

			Object parentObject = targetNode.getParent().getData();
			SupportVesselParam vessel = null;
			if (parentObject instanceof SupportVesselParam)
				vessel = (SupportVesselParam)parentObject;
			
			
			RigStock rigstock  = (RigStock) targetNode.getData();
			
			Double previousBalance = 0.0;
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, RigStock.class, "amt_start");
			
			Boolean isBatchDrill = userSession.getCurrentOperation().getIsBatchDrill();
			if (isBatchDrill==null)
				isBatchDrill=false;
			
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSession.getCurrentDailyUid());
			// get the current date from the session
			if (!isBatchDrill)
			{
				
				rigstock.setAmtInitial(null);	

				previousBalance = CommonUtil.getConfiguredInstance().getOperationSupportVesselStockcodePreviousBalance(userSession.getCurrentOperationUid(), vessel.getSupportVesselInformationUid(), rigstock.getStockCode(), daily);
				thisConverter.setBaseValue(previousBalance);
				if (thisConverter.isUOMMappingAvailable()){
					targetNode.setCustomUOM("@previousBalance", thisConverter.getUOMMapping());
				}
				targetNode.getDynaAttr().put("previousBalance", thisConverter.getConvertedValue());	
			}else{
				if (targetNode.getDynaAttr().get("currentBalance")!=null)
				{
					rigstock.setAmtInitial((Double) targetNode.getDynaAttr().get("currentBalance"));
					targetNode.getDynaAttr().remove("previousBalance");
				}
			}
			
					
			targetNode.getDynaAttr().put("currentBalance", null);
			targetNode.getDynaAttr().put("cumRecvd", null);
			targetNode.getDynaAttr().put("cumUsed", null);
			targetNode.getDynaAttr().put("cumCost", null);
			
			
			targetNode.getDynaAttr().put("isFirstDay", CommonUtil.getConfiguredInstance().getOperationSupportVesselStockcodeFirstDay(rigstock.getOperationUid(), rigstock.getSupportVesselInformationUid(), rigstock.getStockCode(), daily.getDailyUid()));
			
		}
		
		if (object instanceof SupportVesselParam) {
			targetNode.getDynaAttr().put("addNewAllowChangeVesselName", "1");
		}
		
	}
	
	public void onDataNodeCarryForwardFromYesterday(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception{
		//on carry record data with initial amount will be set to null
		
		Object object = node.getData();
		
		if (object instanceof RigStock) {
			Boolean isBatchDrill = session.getCurrentOperation().getIsBatchDrill();
			if (isBatchDrill==null)
				isBatchDrill=false;
			
			RigStock rigstock = (RigStock)object;
			if (isBatchDrill)
			{
				if (node.getDynaAttr().get("currentBalance")!=null)
				{
					rigstock.setAmtInitial((Double) node.getDynaAttr().get("currentBalance"));
					node.getDynaAttr().remove("previousBalance");
				}
			}else{
				rigstock.setAmtInitial(null);	
			}
			
			rigstock.setAmtStart(null);
			rigstock.setAmtUsed(null);
			rigstock.setAmtDiscrepancy(null);
			rigstock.setTrfToRig(null);
			rigstock.setTrfToBeach(null);
		}
		
		if (object instanceof HseIncident) {
			HseIncident hseIncident = (HseIncident) object;
	
			hseIncident.setNumberOfIncidents(1);
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof SupportVesselParam) {
			node.getDynaAttr().put("addNewAllowChangeVesselName", "1");
			if(vesselArrivalDatetime){
				SupportVesselParam vessel = (SupportVesselParam) obj;
				vessel.setTransporttype("boat");
			}	
		}
		
		if (obj instanceof HseIncident) {
			
			HseIncident hseIncident = (HseIncident) obj;
			if (userSelection.getDailyUid() != null){
				Daily today = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());			
				Date todayDate = today.getDayDate();
				Date defaultDateTime = DateUtils.addHours(todayDate, 0);				
					
				hseIncident.setHseEventdatetime(defaultDateTime);				
			}
		}
	}
}
