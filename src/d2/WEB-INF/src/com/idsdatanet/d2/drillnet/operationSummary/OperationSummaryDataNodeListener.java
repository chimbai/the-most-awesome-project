package com.idsdatanet.d2.drillnet.operationSummary;


import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.OperationSummary;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class OperationSummaryDataNodeListener extends EmptyDataNodeListener {


    public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
        Object object = node.getData();
        if (object instanceof OperationSummary) {
        	 OperationSummary operationSummary = (OperationSummary) object;
        	 String monthYear = operationSummary.getMonthYear();
        	 String format = "MM-yyyy";
        	 SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        	 Date utilDate = dateFormat.parse(monthYear);
        	 operationSummary.setReportDateTime(utilDate);
        }
    }
    
}