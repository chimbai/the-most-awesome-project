package com.idsdatanet.d2.drillnet.hserelatedoperationsetup;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class OperationListLookupHandler implements LookupHandler {
	

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String strSql = "SELECT operationUid, operationName FROM Operation WHERE (isDeleted='' OR isDeleted IS NULL) AND operationUid <> :thisOperationUid ORDER BY operationName";
		String[] paramsFields = {"thisOperationUid"};
		Object[] paramsValues = {userSelection.getOperationUid()};
		
		List<Object[]> objs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (objs.size() > 0) {
			for (Object[] obj : objs) {
				String operationUid = (String) obj[0];
				String operationName = (String) obj[1];
					
				LookupItem lookupItem = new LookupItem(operationUid, operationName);
				result.put(operationUid, lookupItem);
			}
		}
		return result;
	}

}

