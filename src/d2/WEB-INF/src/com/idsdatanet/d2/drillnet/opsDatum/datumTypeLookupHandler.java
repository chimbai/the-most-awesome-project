package com.idsdatanet.d2.drillnet.opsDatum;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class datumTypeLookupHandler implements LookupHandler {
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String hql = "select shortCode, lookupLabel from CommonLookup where lookupTypeSelection ='datum_type' and (isDeleted = false or isDeleted is null) order by sequence, lookupLabel";
		List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);

		if(list == null){
			return result;
		}
		for (Object[] item : list) {
			String shortCode = (String) item[0];
			Object lookupLabel = item[1];
			LookupItem lookup = new LookupItem(shortCode, lookupLabel);
			result.put(shortCode, lookup);
		}

		if(!userSelection.getOperationType().equals("SVC")&&!userSelection.getOperationType().equals("INTV")&&!userSelection.getOperationType().equals("WKO")){
			result.remove("TH");
		}
		return result;
	}
}
