package com.idsdatanet.d2.drillnet.activity.npt;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.drillnet.report.ReportOption;

public class BHIQueryUtils {
	public static final String CONSTANT_FIELD = "@field";	
	public static final String CONSTANT_WELL = "@wells";
	public static final String CONSTANT_OPERATION_TYPE = "@operationType";
	public static final String CONSTANT_REPORT_GENERATION_CATEGORY = "@reportGenerationCategory";
	public static final String CONSTANT_COMPANY = "@serviceCompany";
	public static final String CONSTANT_NPT_CATEGORY = "@nptCategory";
	public static final String CONSTANT_OPERATION_AREA = "@operationArea";
	public static final String CONSTANT_QUERIES = "@query";
	public static final String CONSTANT_SECTIONS = "@sections";
	public static final String CONSTANT_TOOL_SIZE = "@toolSize";
	public static final String CONSTANT_TOOL_CATEGORY = "@toolCategory";
	public static final String CONSTANT_COUNTRY = "@country";	

	
	public static List<Object> getReportOptionValue(UserContext userContext, String name)
	{
		return getReportOptionValue(userContext,name,false);
	}
	
	public static List<Object> getReportOptionValue(UserContext userContext, String name,Boolean isDouble)
	{
		Object ro = userContext.getUserSelection().getCustomProperty(name);
		List<Object> result = new ArrayList<Object>();
		if (ro !=null)
		{
			
			for (Object obj: ((ReportOption)ro).getValues())
			{
				if (isDouble)
					obj = Double.parseDouble(obj.toString());
				result.add(obj);
			}
		}
		return result;
	}
	public static List convertArrayToList(Object[] array)
	{
		List result = new ArrayList();
		for (Object obj : array)
		{
			result.add(obj);
		}
		return result;
	}
	
	public static String getQueryFilter(Boolean useOperationCode)
	{
		return " and ("+(useOperationCode?"w.country in(:country)":"o.operationArea in (:operationArea)")+" )";
	}
	public static String getQuerySQL(Boolean useOperationCode)
	{
		return (useOperationCode?"w.country":"o.operationArea");
	}
	public static String getQueryParamName(Boolean useCountry)
	{
		return useCountry?"country":"operationArea";
	}
	public static Object getQueryParamValue(UserContext userContext,Boolean useCountry)
	{
		return (useCountry?BHIQueryUtils.getReportOptionValue(userContext,BHIQueryUtils.CONSTANT_COUNTRY):BHIQueryUtils.getReportOptionValue(userContext,BHIQueryUtils.CONSTANT_OPERATION_AREA));
	}
	
	public static void setCountryOrArea(Object obj, Boolean useCountry, Object value) throws Exception
	{
		BeanUtils.setProperty(obj, useCountry?"country":"operationArea", value);
	}
	
	public static String concat(List<String> list, String separator, Boolean includeSpace)
	{
		String result = null;
		for (String row : list)
		{
			if (result ==null)
				result = row;
			else
				result +=separator+(includeSpace?" ":"")+row;
		}
		return result;
	}
}
