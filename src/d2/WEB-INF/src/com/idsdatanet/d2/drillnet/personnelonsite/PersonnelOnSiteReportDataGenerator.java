package com.idsdatanet.d2.drillnet.personnelonsite;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.model.Daily;

public class PersonnelOnSiteReportDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		
		String thisOperationUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getOperationUid().toString())) thisOperationUid = userContext.getUserSelection().getOperationUid().toString();
		
		Map<String,Map> pobMap = new HashMap<String,Map>();
		
		String strSql = "SELECT p.crewName, p.dailyUid, p.crewPosition, p.coname, p.crewCompany FROM PersonnelOnSite p, Daily d WHERE p.dailyUid = d.dailyUid " +
					    "AND (d.isDeleted IS NULL or d.isDeleted = false) AND (p.isDeleted IS NULL or p.isDeleted = false) " +
					    "AND p.operationUid = :thisOperationUid AND d.dayDate <= :userDate ORDER BY p.crewName,d.dayDate";
		String[] paramsFields = {"userDate", "thisOperationUid"};
		Object[] paramsValues = {todayDate, thisOperationUid};
		
		List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		for(Object[] a: rs){			
			String crewName = "";
			String dailyUid = "";
			String crewPosition = "";
			String coname = "";
			String crewCompany = "";
			if(a[0] != null) crewName = a[0].toString();
			if(a[1] != null) dailyUid = a[1].toString();
			if(a[2] != null) crewPosition = a[2].toString();
			if(a[3] != null) coname = a[3].toString();
			if(a[4] != null) crewCompany = a[4].toString();
			
			Map<String, Object> dailyMap = new HashMap<String, Object>();
			if (pobMap.containsKey(crewName)) {
				dailyMap = pobMap.get(crewName);
			} else {
				dailyMap = new HashMap<String, Object>();				
			}
			dailyMap.put(dailyUid, dailyUid);
			dailyMap.put("crewPosition", crewPosition);
			dailyMap.put("coname", coname);
			dailyMap.put("crewCompany", crewCompany);
			pobMap.put(crewName, dailyMap);
		}
		
		String strSql2 = "FROM Daily WHERE operationUid = :thisOperationUid AND (isDeleted is null or isDeleted = false) ORDER BY dayDate";
		String[] paramsFields2 = {"thisOperationUid"};
		Object[] paramsValues2 = {thisOperationUid};
		
		List<Daily> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
		
		//ArrayList result = new ArrayList();
		if(lstResult.size() > 0) {	
			for (Map.Entry<String, Map> item:pobMap.entrySet() ) {
				Map<String , String> itemMap = item.getValue();
				//Boolean hasPreviousDay = false;
				String crewCompany_raw = "";
				String coname_raw = "";
				if(StringUtils.isNotBlank(this.getCompanyName(itemMap.get("crewCompany")))) crewCompany_raw = this.getCompanyName(itemMap.get("crewCompany"));
				if(StringUtils.isNotBlank(this.getCompanyName(itemMap.get("coname")))) coname_raw = this.getCompanyName(itemMap.get("coname"));
				Map<String, Object> d = null;
				for (Daily thisDaily : lstResult) {
					if(itemMap.containsKey(thisDaily.getDailyUid())) {
						//hasPreviousDay = true;
						if (d!=null) {
							d.put("endDateDailyUid", thisDaily.getDailyUid());
							d.put("endDate", thisDaily.getDayDate());
							d.put("endDate_raw", thisDaily.getDayDate().getTime());
							d.put("total", (Integer)d.get("total")+1);
						} else if (d==null){
							d = new HashMap<String , Object>();
							d.put("name", item.getKey());
							d.put("startDateDailyUid", thisDaily.getDailyUid());
							d.put("startDate", thisDaily.getDayDate());
							d.put("startDate_raw", thisDaily.getDayDate().getTime());
							d.put("endDateDailyUid", thisDaily.getDailyUid());
							d.put("endDate", thisDaily.getDayDate());
							d.put("endDate_raw", thisDaily.getDayDate().getTime());
							d.put("crewPosition", itemMap.get("crewPosition"));
							d.put("crewCompany", itemMap.get("crewCompany"));
							d.put("crewCompany_raw", crewCompany_raw);
							d.put("coname", itemMap.get("coname"));
							d.put("coname_raw", coname_raw);
							d.put("total", 1);
						}
					} else {
						//hasPreviousDay = false;
						if (d!=null) this.addNode(reportDataNode, d);
						d = null;
					}
				}
				if (d!=null) this.addNode(reportDataNode, d);
			}
		}		
	}
	
	private void addNode(ReportDataNode node, Map map) {
		ReportDataNode newNode = node.addChild("POBSummary");
		newNode.addProperty("name", map.get("name").toString());
		newNode.addProperty("startDateDailyUid", map.get("startDateDailyUid").toString());
		newNode.addProperty("startDate", map.get("startDate").toString());
		newNode.addProperty("startDate_raw", map.get("startDate_raw").toString());
		newNode.addProperty("endDateDailyUid", map.get("endDateDailyUid").toString());
		newNode.addProperty("endDate", map.get("endDate").toString());
		newNode.addProperty("endDate_raw", map.get("endDate_raw").toString());
		newNode.addProperty("crewPosition", map.get("crewPosition").toString());
		newNode.addProperty("crewCompany", map.get("crewCompany").toString());
		newNode.addProperty("crewCompany_raw", map.get("crewCompany_raw").toString());
		newNode.addProperty("coname", map.get("coname").toString());
		newNode.addProperty("coname_raw", map.get("coname_raw").toString());
		newNode.addProperty("total", map.get("total").toString());
	}
	
	private String getCompanyName(String companyUid) throws Exception{
		String[] paramsFields = {"companyUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = companyUid;
		
		String strSql = "SELECT companyName FROM LookupCompany WHERE (isDeleted = false or isDeleted is null) and lookupCompanyUid =:companyUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (!lstResult.isEmpty())
		{
			Object thisResult = (Object) lstResult.get(0);
			
			if (thisResult != null) {
				return thisResult.toString();
			}else {
				return null;
			}
		}else {
			return null;
		}

	}
}
