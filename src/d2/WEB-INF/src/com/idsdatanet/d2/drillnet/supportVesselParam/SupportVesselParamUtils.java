package com.idsdatanet.d2.drillnet.supportVesselParam;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.model.SupportVesselInformation;
import com.idsdatanet.d2.core.model.SupportVesselParam;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class SupportVesselParamUtils {

	public static void stockCodeChanged(HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if (targetCommandBeanTreeNode != null) {
			if (targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(RigStock.class)) {
				UserSession userSession = UserSession.getInstance(request);
				CustomFieldUom thisConverter = new CustomFieldUom(targetCommandBeanTreeNode.getCommandBean(), RigStock.class, "amt_start");
				
				SupportVesselParam thisSupportv = (SupportVesselParam) targetCommandBeanTreeNode.getParent().getData();
				String SupportVesselInformationUid = thisSupportv.getSupportVesselInformationUid();
				
				//show the correct unit as well
				RigStock thisRigStock = (RigStock) targetCommandBeanTreeNode.getData();					
				String stockcode = thisRigStock.getStockCode();
				
				if (StringUtils.isNotBlank(stockcode)) {
					List<Object> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select storedUnit from LookupRigStock where (isDeleted = false or isDeleted is null) and lookupRigStockUid = :stockCode", "stockCode", stockcode);
					if (lstResult.size() > 0) thisRigStock.setStoredUnit(lstResult.get(0).toString());
				}
				
				//check if today is the first day of operation or not to determine want to show the starting amount
				targetCommandBeanTreeNode.getDynaAttr().put("isFirstDay", CommonUtil.getConfiguredInstance().getOperationSupportVesselStockcodeFirstDay(userSession.getCurrentOperationUid(), SupportVesselInformationUid, thisRigStock.getStockCode(), userSession.getCurrentDailyUid()));
				SupportVesselParamUtils.setIsBatchDrillFlag(targetCommandBeanTreeNode, new UserSelectionSnapshot(UserSession.getInstance(request)));

				// default 'previousBalance' to 0.0
				Double previousBalance = 0.0;
								
				// get the current date from the session
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSession.getCurrentDailyUid());
				
				previousBalance = CommonUtil.getConfiguredInstance().getOperationSupportVesselStockcodePreviousBalance(userSession.getCurrentOperationUid(),SupportVesselInformationUid, stockcode, daily);
				thisConverter.setBaseValue(previousBalance);
				if (thisConverter.isUOMMappingAvailable()) {
					targetCommandBeanTreeNode.setCustomUOM("@previousBalance", thisConverter.getUOMMapping());
				}
				targetCommandBeanTreeNode.getDynaAttr().put("previousBalance", thisConverter.getConvertedValue());
			}
		}
	}

	public static void setIsBatchDrillFlag(CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception {
		Operation op = ApplicationUtils.getConfiguredInstance().getCachedOperation(userSelection.getOperationUid());
		if (op.getIsBatchDrill()!=null && op.getIsBatchDrill()) {
			node.getDynaAttr().put("isBatch", "1");
		}
	}
	
	public static String resolveSupportVesselInformationUid(String vesselName) throws Exception {
		return SupportVesselParamUtils.resolveSupportVesselInformationUid(vesselName, true);
	}

	public static String resolveSupportVesselInformationUid(String vesselName, boolean missingLookupAllowed) throws Exception {
		String rv = vesselName;
		if (StringUtils.isNotEmpty(vesselName)) {
			String strSql = "FROM SupportVesselInformation WHERE (isDeleted IS NULL OR isDeleted = 0) AND vesselName LIKE :vesselName";
			String[] paramsFields = { "vesselName" };
			Object[] paramsValues = { vesselName };
			List<SupportVesselInformation> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty()) {
				rv = lstResult.get(0).getSupportVesselInformationUid();
			}
		}
		return rv;
	}
	
}
