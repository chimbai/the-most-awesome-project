package com.idsdatanet.d2.drillnet.rushmore;

import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Basin;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.Platform;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.RushmoreTimeInterrupt;
import com.idsdatanet.d2.core.model.RushmoreWells;
import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

import edu.emory.mathcs.backport.java.util.Arrays;

public class RushmoreUtils {
	
	
	public static void defaultPopulateProperties(RushmoreWells thisRushmoreWell, String operationUid, CustomFieldUom thisConverter, CommandBeanTreeNode node, Boolean setDatumConversion, OpsDatum datum) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(setDatumConversion);
		thisRushmoreWell.setOwnerDrilled("1");
		thisRushmoreWell.setLocatorWell("N");
		thisRushmoreWell.setMultiLateral("N");
		thisRushmoreWell.setIsUnderBalancedDrillWell("N");
		thisRushmoreWell.setCuttingsDisposal("D");
		
		List<Operation> opList =  ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From Operation Where operationUid=:operationUid",new String[]{"operationUid"},new Object[]{operationUid},qp);
		if (opList !=null && opList.size()>0)
		{
			Operation thisOperation = opList.get(0);
			String basin, field, offshore = "";
			Date tdDateTdTime = null;

			List<Well> wellList =  ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("From Well Where wellUid=:wellUid",new String[]{"wellUid"},new Object[]{thisOperation.getWellUid()},qp);
			
			if (wellList !=null && wellList.size()>0)
			{
				Well thisWell = wellList.get(0);
				thisRushmoreWell.setCountry(thisWell.getCountry());						//get Country value
				thisRushmoreWell.setFullWellName(thisWell.getWellName());				//get Full Well Name value
				thisRushmoreWell.setInhouseWellName(thisWell.getWellName());			//get 'In house' or common well name value
				thisRushmoreWell.setBlockNum((String)thisWell.getBlock());	//get Block Number value
				thisRushmoreWell.setLatDegs(toString(thisWell.getLatDeg()));			//get Lat Deg value
				thisRushmoreWell.setLatMins(toString(thisWell.getLatMinute()));		    //get Lat Min value
				thisRushmoreWell.setLatSecs(toString(thisWell.getLatSecond()));		    //get Lat Sec value
				thisRushmoreWell.setLatNs(thisWell.getLatNs());							//get Lat S/N value
				thisRushmoreWell.setLongDegs(toString(thisWell.getLongDeg()));		    //get Long Deg value
				thisRushmoreWell.setLongMins(toString(thisWell.getLongMinute()));	    //get Long Min value
				thisRushmoreWell.setLongSecs(toString(thisWell.getLongSecond()));	    //get Long Sec value
				thisRushmoreWell.setLongEw(thisWell.getLongEw());
				thisRushmoreWell.setUwi(thisWell.getUwi());  //get Unique Well ID value
				thisRushmoreWell.setUniqueWellId(thisWell.getUniqueWellId());
				
				basin = thisWell.getBasin();
				field = thisWell.getField();
				String basinSql = "FROM Basin WHERE (isDeleted = false or isDeleted is null) AND basinUid = :thisBasinUid";			
				List basinResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(basinSql, "thisBasinUid", basin);					
				if (basinResult.size() > 0){
					Basin thisBasin = (Basin) basinResult.get(0);
					basin = thisBasin.getName();
				}
				
				if (field != "" && basin==""){
					thisRushmoreWell.setBasinName(field);
				} else if (field=="" && basin != ""){
					thisRushmoreWell.setBasinName(basin);
				} else if (field != "" && basin != ""){
					//basin = field + " or " + basin;
					thisRushmoreWell.setBasinName(field + " or " + basin);
				} else if (field=="" && basin==""){
					thisRushmoreWell.setBasinName("N/A");								//get Basin/Field value
				}				
				
				offshore = thisWell.getOnOffShore();
				if (offshore.equals("OFF")){
					
					Double waterDepth = thisWell.getWaterDepth();
					if(waterDepth!=null)
					{
						if (thisConverter!=null)
						{
							thisConverter.setReferenceMappingField(RushmoreWells.class, "waterDepthMdMsl");
							thisConverter.setBaseValue(waterDepth);
							waterDepth = thisConverter.getConvertedValue();
						}
						thisRushmoreWell.setWaterDepthMdMsl(waterDepth);		//get Water Depth value if the well is offshore well
					}
					
				}
				
				String PlatformUid = thisWell.getPlatformUid();
				String strSqlPlat = "FROM Platform WHERE (isDeleted = false or isDeleted is null) AND platformUid = :PlatformUid";			
				List ResultPlat = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSqlPlat, "PlatformUid", PlatformUid);
				if (ResultPlat.size() > 0){
					Platform thisPlatform = (Platform) ResultPlat.get(0);
					thisRushmoreWell.setPlatform(thisPlatform.getPlatformName());		//get Platform value
				}		
			}
			String strSql2 = "FROM Wellbore WHERE (isDeleted = false or isDeleted is null) AND wellboreUid = :thisWellboreUid";			
			List Result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "thisWellboreUid", thisOperation.getWellboreUid(),qp);	
			Double SpudDepth = 0.0;
			Double opSpud = 0.0;
			Double zPlannedTdMd = 0.0;
			Double zTargetTdMD = 0.0;
			
			if (datum != null)
			{
			    Double offsetMsl = 0.00;
                if(datum.getOffsetMsl() != null) {
                    offsetMsl = datum.getOffsetMsl();
                }
                Double reportingDatumOffset = 0.00;
                if(datum.getReportingDatumOffset() != null) {
                    reportingDatumOffset = datum.getReportingDatumOffset();
                }
                Double offset = offsetMsl + reportingDatumOffset;
				SpudDepth -= offset;
				opSpud -= offset;
				zPlannedTdMd -= offset;
				zTargetTdMD -= offset;
			}
			
			if (Result.size() > 0){
				Wellbore thisWellbore = (Wellbore) Result.get(0);
				thisRushmoreWell.setIsHighTemperature(thisWellbore.getIshightemperature());	//get Is High Temperature value
				thisRushmoreWell.setIsHighPressure(thisWellbore.getIshighpressure ());		//get Is High Pressure 
				if (thisWellbore.getKickoffMdMsl() != null){
					SpudDepth = thisWellbore.getKickoffMdMsl();		
				}
				
				zPlannedTdMd = thisWellbore.getFinalTdMdMsl();
				zTargetTdMD = thisWellbore.getPlannedTdMdMsl();
			}
			
			String typeIntent = thisOperation.getTypeIntent();		//Get Intention	value from Operation
			if ("DEV".equals(typeIntent)){
				thisRushmoreWell.setWellType("D");
			} else if ("APR".equals(typeIntent)){
				thisRushmoreWell.setWellType("A");
			} else if ("EXP".equals(typeIntent)){
				thisRushmoreWell.setWellType("E");
			} else if ("DEL".equals(typeIntent)){
				thisRushmoreWell.setWellType("A");
			} else if ("INJ".equals(typeIntent)){
				thisRushmoreWell.setWellType("D");
			}
			
			if (thisOperation.getSpudMdMsl() !=null){
				opSpud = thisOperation.getSpudMdMsl();
			}
			
			if (opSpud != null) {					
				if (SpudDepth != null && SpudDepth > opSpud){
					if (thisConverter!=null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "spudDepthMdMsl");
						thisConverter.setBaseValue(SpudDepth);
						SpudDepth = thisConverter.getConvertedValue();
					}
					thisRushmoreWell.setSpudDepthMdMsl(SpudDepth);
				} else {
					if (thisConverter!=null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "spudDepthMdMsl");
						thisConverter.setBaseValue(opSpud);
						opSpud = thisConverter.getConvertedValue();
					}
					thisRushmoreWell.setSpudDepthMdMsl(opSpud);
					// get Spud Depth value
				}
			}
			
			thisRushmoreWell.setSpudDate(thisOperation.getSpudDate());				//get Spud Date	value
			tdDateTdTime = thisOperation.getTdDateTdTime();
			
			String rigInformationUid = thisOperation.getRigInformationUid();			
			String strSql4 = "FROM RigInformation WHERE (isDeleted = false or isDeleted is null) AND rigInformationUid = :thisRigInformationUid";			
			List Result4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, "thisRigInformationUid", rigInformationUid,qp);			
			String rigType = "";
			if (Result4.size() > 0){				
				RigInformation thisRigInformation = (RigInformation) Result4.get(0);
				thisRushmoreWell.setRigName(thisRigInformation.getRigName());
				
				if (thisRigInformation.getRigSubType() !=null){
					rigType = thisRigInformation.getRigSubType();
				}
				
				if (rigType.equals("jackup")){
					thisRushmoreWell.setRigType("JK");
				} else if (rigType.equals("drillship")){
					thisRushmoreWell.setRigType("DS");
				} else if (rigType.equals("platform")){
					thisRushmoreWell.setRigType("PL");
				} else if (rigType.equals("submersible")){
					thisRushmoreWell.setRigType("SU");
				} else if (rigType.equals("semisubmersible")){
					thisRushmoreWell.setRigType("SS");					
				} else {
					thisRushmoreWell.setRigType("");			// get Rig Type value
				}	
				
				String RigMangerUid = thisRigInformation.getRigManager();
				String strSql5 = "FROM LookupCompany WHERE (isDeleted = false or isDeleted is null) AND lookupCompanyUid = :thisRigMangerUid";				
				List Result5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, "thisRigMangerUid", RigMangerUid,qp);				
				if (Result5.size() > 0){
					LookupCompany thisLookupCompany = (LookupCompany) Result5.get(0);
					thisRushmoreWell.setDrillingContractor(thisLookupCompany.getCompanyName());	//get Drilling Co. value
				}
			}
			
			String strSql10 = "FROM Bitrun WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid ORDER BY bitDiameter DESC";
			List Result10 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql10, "thisOperationUid", operationUid, qp);
			
			if (Result10.size() > 0){
				Bitrun thisBitrun = (Bitrun) Result10.get(0);
				Double bitDiameter = thisBitrun.getBitDiameter();
				
				if (bitDiameter != null) {
					if (thisConverter != null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "finalDrillBitOd1");
						thisConverter.setBaseValue(bitDiameter);
						bitDiameter = thisConverter.getConvertedValue();
					}
					
					thisRushmoreWell.setFinalDrillBitOd1(bitDiameter);	//get Final Drill Bit Hole Size value
				}
			}
			
			Double amountSpentPriorToSpud = 0.0;
			if (thisOperation.getAmountSpentPriorToSpud() !=null){
				amountSpentPriorToSpud = thisOperation.getAmountSpentPriorToSpud();
			}
			
			Double totalWellCost = 0.0;
			Date rigReleaseDate = thisOperation.getRigOffHireDate();
			String[] paramsFields5 = {"thisOperationUid","thisRigReleaseDate"};
			Object[] paramsValues5 = {operationUid, rigReleaseDate};
			String strSql19 = "SELECT sum(daycost) as totalDayCost FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid AND reportDatetime <= :thisRigReleaseDate";
			List Result19 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql19, paramsFields5, paramsValues5,qp);
			Double totalDayCost = 0.00;
			if (Result19.size() > 0){
				Object thisReportDaily = Result19.get(0);
				if (thisReportDaily != null) {						
					totalDayCost = Double.parseDouble(thisReportDaily.toString());
					totalWellCost = (amountSpentPriorToSpud + totalDayCost)/1000000;										
					if (thisConverter!=null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "costWellTotal");
						thisConverter.setBaseValue(totalWellCost);
						totalWellCost = thisConverter.getConvertedValue();
					}
					thisRushmoreWell.setCostWellTotal(totalWellCost);	// get Total Well Cost						
				}			
			}
			
			Date onLocDate = null;
			
			if (thisOperation.getOnlocDateOnlocTime() != null)
			{	
				onLocDate = thisOperation.getOnlocDateOnlocTime();
			}

			String strSql28 = "";
			List Result28;
			
			if (onLocDate !=null){
				String[] paramsFields9 = {"thisOperationUid","thisOnLocDate"};
				Object[] paramsValues9 = {operationUid,onLocDate};
				strSql28 = "SELECT SUM(a.activityDuration) FROM Activity a, Daily d " +
						"WHERE (a.isDeleted = false or a.isDeleted is null) " +
						"AND (d.isDeleted = false or d.isDeleted is null) " +
						"AND (a.dailyUid = d.dailyUid) " +
						"AND (a.operationUid = :thisOperationUid) " +
						"AND (d.dayDate >= DATE(:thisOnLocDate)) " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null) " +
						"AND TIME_TO_SEC(a.endDatetime) >= TIME_TO_SEC(:thisOnLocDate)";
				Result28 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql28, paramsFields9, paramsValues9, qp);
			}
			else {
				strSql28 = "SELECT SUM(a.activityDuration) FROM Activity a, Daily d " +
						"WHERE (a.isDeleted = false or a.isDeleted is null) " +
						"AND (d.isDeleted = false or d.isDeleted is null) " +
						"AND (a.dailyUid = d.dailyUid) " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null) " +
						"AND (a.operationUid = :thisOperationUid)";
				Result28 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql28, "thisOperationUid", operationUid ,qp);
			}
			
			if (Result28.size() > 0)
			{
				Object thisActivity = Result28.get(0);
				if (thisActivity != null) {
					Double totalDurationToEndDryHolePeriod = Double.parseDouble(thisActivity.toString());
					if (thisConverter != null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "totalDaysToEndDryHolePeriod");
						thisConverter.setBaseValue(totalDurationToEndDryHolePeriod);
						totalDurationToEndDryHolePeriod = thisConverter.getConvertedValue();
					}
					thisRushmoreWell.setTotalDaysToEndDryHolePeriod(totalDurationToEndDryHolePeriod);	// get Total days to end of dry hole period value
				}
			}			
			
			//get current SurveyStation object to auto fill data
			String strSql6 = "FROM SurveyStation WHERE (isDeleted = false or isDeleted is null) AND wellUid = :thisWellUid ORDER BY inclinationAngle DESC";			
			List Result6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, "thisWellUid", thisOperation.getWellUid(), qp);	
			if (Result6.size() > 0){
				SurveyStation thisSurveyStation = (SurveyStation) Result6.get(0);
				
				if (thisSurveyStation.getInclinationAngle() != null) {
					Double maxAngleDegrees = thisSurveyStation.getInclinationAngle();
					if (thisConverter != null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "maxAngleDegrees");
						thisConverter.setBaseValue(thisSurveyStation.getInclinationAngle());
						maxAngleDegrees = thisConverter.getConvertedValue();
					}
					thisRushmoreWell.setMaxAngleDegrees(maxAngleDegrees);		//get Max Angle in Degrees value
				}
			}
			
			
			String strSql7 = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid ORDER BY reportDatetime DESC";			
			List Result7 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql7, "thisOperationUid", operationUid, qp);			
			if (Result7.size() > 0){
				ReportDaily thisReportDaily = (ReportDaily) Result7.get(0);
				
				if (thisReportDaily.getDepthMdMsl() != null) {
					Double holeMeasuredTdMsl = thisReportDaily.getDepthMdMsl();
					if (thisConverter !=null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "holeMeasuredTdMsl");
						thisConverter.setBaseValue(holeMeasuredTdMsl);
						holeMeasuredTdMsl = thisConverter.getConvertedValue();
					}
					thisRushmoreWell.setHoleMeasuredTdMsl(holeMeasuredTdMsl);	//get MTD value
				}
				
				if (thisReportDaily.getDepthTvdMsl() != null) {
					Double holeTvdMsl = thisReportDaily.getDepthTvdMsl();
					if (thisConverter != null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "holeTvdMsl");
						thisConverter.setBaseValue(holeTvdMsl);
						holeTvdMsl = thisConverter.getConvertedValue();
					}
					thisRushmoreWell.setHoleTvdMsl(holeTvdMsl);				//get TVD value
				}
			}
			
			String strSql8 = "SELECT MAX(mudWeight) as maxMudWeight FROM MudProperties WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid";
			List Result8 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql8, "thisOperationUid", operationUid, qp);
			Double mudWeight = 0.00;
			if (!Result8.isEmpty())			
			{
				Object thisMudProperties = Result8.get(0);
				if (thisMudProperties != null) {
					mudWeight = Double.parseDouble(thisMudProperties.toString());
					if (thisConverter != null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "mudWeightMax");
						thisConverter.setBaseValue(mudWeight);
						mudWeight = thisConverter.getConvertedValue();
					}
					thisRushmoreWell.setMudWeightMax(mudWeight);			// get Max Mud Weight
				}
			}
			
			String strSql9 = "FROM MudProperties WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid ORDER BY depthMdMsl DESC";			
			List Result9 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql9, "thisOperationUid", operationUid, qp);		
			if (Result9.size() > 0){
				MudProperties thisMudProperties2 = (MudProperties) Result9.get(0);
				if (thisMudProperties2.getMudWeight() != null) {
					Double mudWeight2 = thisMudProperties2.getMudWeight();
					if (thisConverter != null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "mudWeightTd");
						thisConverter.setBaseValue(thisMudProperties2.getMudWeight());
						mudWeight2 = thisConverter.getConvertedValue();
					}
					thisRushmoreWell.setMudWeightTd(mudWeight2);				//get Mud weight at TD value
				}
			}
			
			String[] paramsFields18 = {"thisOperationUid","thisTdDateTdTime"};
			Object[] paramsValues18 = {operationUid, tdDateTdTime};

			String strSql18 = "Select casingOd FROM CasingSection WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid AND installStartDate <= :thisTdDateTdTime ORDER BY casingOd DESC";			
			List Result18 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql18, paramsFields18, paramsValues18, qp);			
			//Integer record = 1;
			int max = 11;
			
			NumberFormat formatter = new DecimalFormat("#.###");

			if (Result18.size()<max)
				max = Result18.size();
			for (int a=0; a<max;a++)
			{
				Object thisCasingSection = Result18.get(a);
				if (thisCasingSection != null) {
					Double casingOd = Double.parseDouble(thisCasingSection.toString());
					
					Double casingInInch = casingOd / 0.0254;
					if (thisConverter != null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "newCasing"+(a+1));
						thisConverter.setBaseValue(casingOd);
						casingOd = thisConverter.getConvertedValue();
					}
					PropertyUtils.setProperty(thisRushmoreWell, "newCasing"+(a+1), casingOd);
					PropertyUtils.setProperty(thisRushmoreWell, "casing"+(a+1), formatter.format(casingInInch));
				}
			}
			
			List thisPhaseCode = new ArrayList<String>();
			thisPhaseCode.add("E1");
			thisPhaseCode.add("E2");
			thisPhaseCode.add("E3");
			String[] paramsFields6 = {"thisOperationUid", "thisPhaseCode"};
			Object[] paramsValues6 = {operationUid,thisPhaseCode};
			String strSql21 = "FROM ReportDaily d, Activity a WHERE (d.isDeleted = false or d.isDeleted is null) AND (a.isDeleted = false or a.isDeleted is null) AND a.operationUid = :thisOperationUid AND d.dailyUid = a.dailyUid AND (a.phaseCode in (:thisPhaseCode)) ORDER BY d.reportDatetime DESC";			
			List<Object[]> Result21 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql21, paramsFields6, paramsValues6);			
			
			if (Result21.size() > 0){
				Object[] thisResult = (Object[]) Result21.get(0);
				ReportDaily thisReportDaily = (ReportDaily) thisResult[0];
				thisRushmoreWell.setDateEndDryHole(thisReportDaily.getReportDatetime());			//get End Dry Hole Date	
			}
			
			thisPhaseCode = new ArrayList<String>();
			thisPhaseCode.add("ABN");
			thisPhaseCode.add("S");
			
			String[] paramsFields7 = {"thisOperationUid", "thisPhaseCode"};
			Object[] paramsValues7 = {operationUid,thisPhaseCode};
			String strSql22 = "SELECT SUM(a.activityDuration) as totalPA_S FROM Activity a, Daily d " +
					"WHERE (a.isDeleted = false or a.isDeleted is null) " +
					"AND (d.isDeleted = false or d.isDeleted is null) " +
					"AND (a.dailyUid=d.dailyUid) " +
					"AND a.operationUid = :thisOperationUid " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.phaseCode in (:thisPhaseCode))";
			List Result22 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql22, paramsFields7, paramsValues7,qp);
			
			if (Result22.size() > 0)
			{
				Object thisActivity = Result22.get(0);
				if (thisActivity != null) {
					Double duration = Double.parseDouble(thisActivity.toString());
					if (thisConverter !=null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "paSuOrCompletionDays");
						thisConverter.setBaseValue(duration);
						duration = thisConverter.getConvertedValue();
					}
					thisRushmoreWell.setPaSuOrCompletionDays(duration);			// get PA, SU or Completion Days
				}
			}
			
			String strSql23 = "SELECT DISTINCT(shortCode) FROM LookupTaskCode WHERE (isDeleted = false or isDeleted is null) AND (name like '%CORING%')";
			List Result23 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql23);

			Double coringDays = 0.0;
			if (Result23 != null && Result23.size()>0)
			{
				String[] paramsFields8 = {"thisOperationUid", "taskCode"};
				Object[] paramsValues8 = {operationUid,Result23};
				String strSql24 = "SELECT SUM(a.activityDuration) FROM Activity a, Daily d " +
						"WHERE (a.isDeleted = false or a.isDeleted is null) " +
						"AND (d.isDeleted = false or d.isDeleted is null) " +
						"AND a.dailyUid=d.dailyUid " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null) " + 
						"AND a.operationUid = :thisOperationUid " +
						"AND (a.taskCode in (:taskCode))";
				List Result24 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql24, paramsFields8, paramsValues8,qp);
				
				if (Result24.size() > 0)
				{
					Object thisActivity = Result24.get(0);
					if (thisActivity != null) {
						coringDays += Double.parseDouble(thisActivity.toString());	
					}
				
				}				
				
				
				if (thisConverter !=null)
				{
					thisConverter.setReferenceMappingField(RushmoreWells.class, "daysCoring");
					thisConverter.setBaseValue(coringDays);
					coringDays = thisConverter.getConvertedValue();
				}
				thisRushmoreWell.setDaysCoring(coringDays);			// get Coring Days
			}
			
			
			String strSql33 = "SELECT DISTINCT(shortCode) FROM LookupTaskCode WHERE (isDeleted = false or isDeleted is null) AND (name like '%REAM%')";
			List Result33 = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql33);

			Double reamingDays = 0.0;
			if (Result33 != null && Result33.size()>0)
			{
				String[] paramsFields8 = {"thisOperationUid", "taskCode"};
				Object[] paramsValues8 = {operationUid,Result33};
				String strSql34 = "SELECT SUM(a.activityDuration) FROM Activity a, Daily d " +
				"WHERE (a.isDeleted = false or a.isDeleted is null) " +
				"AND (d.isDeleted = false or d.isDeleted is null) " +
				"AND a.dailyUid=d.dailyUid " +
				"AND (a.isSimop=false or a.isSimop is null) " +
				"AND (a.isOffline=false or a.isOffline is null) " + 
				"AND a.operationUid = :thisOperationUid " +
				"AND (a.taskCode in (:taskCode))";
				List Result34 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql34, paramsFields8, paramsValues8, qp);
				if (Result34.size() > 0)
				{
					Object thisActivity = Result34.get(0);
					if (thisActivity != null) {
						reamingDays += Double.parseDouble(thisActivity.toString());	
					}
					
				}				
					
				
				if (thisConverter !=null)
				{
					thisConverter.setReferenceMappingField(RushmoreWells.class, "reamingDays");
					thisConverter.setBaseValue(reamingDays);
					reamingDays = thisConverter.getConvertedValue();
				}
				thisRushmoreWell.setReamingDays(reamingDays);			// get Under reaming days
			}
			
			
			if (zTargetTdMD != null || zPlannedTdMd != null) {
				Double depth = 0.0;

				if (offshore.equals("OFF")){
					depth = zPlannedTdMd;
				} else {
					depth = zTargetTdMD;
				}
				
				String sqlDrillingTaskCodes = "SELECT DISTINCT(shortCode) FROM LookupTaskCode WHERE (isDeleted = false or isDeleted is null) AND (name like '%LOG%') AND (operationCode = :operationCode)";
				List drillingTaskCodes = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlDrillingTaskCodes, "operationCode", "DRLLG");
	
				Double LoggingDays = 0.0;
				Double NotLoggingDays = 0.0;
					
				if (drillingTaskCodes !=null && drillingTaskCodes.size()>0)
				{
					String sqlLoggingDaysTd = "SELECT SUM(a.activityDuration) FROM Activity a, Daily d " +
							"WHERE (a.isDeleted = false or a.isDeleted is null) " +
							"AND (d.isDeleted = false or d.isDeleted is null) " +
							"AND a.dailyUid = d.dailyUid " +
							"AND a.operationUid = :thisOperationUid " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null) " +
							"AND (a.taskCode in (:taskCode)) AND a.depthMdMsl >= :depth";
					List resultLoggingDays = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlLoggingDaysTd, new String[]{"thisOperationUid", "taskCode", "depth"}, new Object[]{operationUid,drillingTaskCodes, depth},qp);
					
					if (resultLoggingDays.size() > 0)
					{
						Object thisActivity = resultLoggingDays.get(0);
						if (thisActivity != null) {
							LoggingDays = Double.parseDouble(thisActivity.toString());							
						}
					}	
					
	
					String sqlLoggingDaysNotTd = "SELECT SUM(a.activityDuration) FROM Activity a, Daily d " +
							"WHERE (a.isDeleted = false or a.isDeleted is null) " +
							"AND (d.isDeleted = false or d.isDeleted is null) " +
							"AND d.dailyUid=a.dailyUid " +
							"AND a.operationUid = :thisOperationUid " +
							"AND (a.taskCode in (:taskCode)) " +
							"AND (a.isSimop=false or a.isSimop is null) " +
							"AND (a.isOffline=false or a.isOffline is null) " +
							"AND a.depthMdMsl < :depth";
					List Result27 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlLoggingDaysNotTd, new String[]{"thisOperationUid", "taskCode", "depth"}, new Object[]{operationUid,drillingTaskCodes, depth}, qp);
					
					if (Result27.size() > 0)
					{
						Object thisActivity2 = Result27.get(0);
						if (thisActivity2 != null) {
							NotLoggingDays += Double.parseDouble(thisActivity2.toString());			
						}
					}					
					
					if (thisConverter !=null)
					{
						thisConverter.setReferenceMappingField(RushmoreWells.class, "loggingDaysTd");
						thisConverter.setBaseValue(LoggingDays);
						LoggingDays = thisConverter.getConvertedValue();
						
						thisConverter.setReferenceMappingField(RushmoreWells.class, "loggingDaysNotTd");
						thisConverter.setBaseValue(NotLoggingDays);
						NotLoggingDays = thisConverter.getConvertedValue();
					}
					thisRushmoreWell.setLoggingDaysTd(LoggingDays);			// get Logging Days at TD
					thisRushmoreWell.setLoggingDaysNotTd(NotLoggingDays);			// get Logging Days Not at TD
				}

			}
			
			Double timeNptRigContractor = calculateDurationBy(operationUid,new String[]{"BO", "TD", "MP", "DW", "BH", "RO"}, new String[]{"TP","TU"},qp);
			// calculate IT by root cause
			
			if (timeNptRigContractor != null) {
				if (thisConverter != null){
					thisConverter.setReferenceMappingField(RushmoreWells.class, "timeNptRigContractor");
					thisConverter.setBaseValue(timeNptRigContractor);
					timeNptRigContractor = thisConverter.getConvertedValue();
				}
				thisRushmoreWell.setTimeNptRigContractor(timeNptRigContractor);			// get Interrupt time due to downhole problems
			}
			
			Double timeNptServiceCompany = calculateDurationBy(operationUid,new String[]{"CT", "CR", "DE", "BT", "LW", "LE", "MM", "MW", "PR", "RV", "SV", "SX", "WL", "SO"}, new String[]{"TP","TU"},qp);
		
			if (timeNptServiceCompany != null) {
				if (thisConverter != null){
					thisConverter.setReferenceMappingField(RushmoreWells.class, "timeNptServiceCompany");
					thisConverter.setBaseValue(timeNptServiceCompany);
					timeNptServiceCompany = thisConverter.getConvertedValue();
				}
				thisRushmoreWell.setTimeNptServiceCompany(timeNptServiceCompany);			// get Interrupt time due to service company
			}
			
			Double timeNptOperatorProblems = calculateDurationBy(operationUid,new String[]{"LH", "TU", "OO"}, new String[]{"TP","TU"},qp);

			if (timeNptOperatorProblems != null) {
				if (thisConverter != null){
					thisConverter.setReferenceMappingField(RushmoreWells.class, "timeNptOperatorProblems");
					thisConverter.setBaseValue(timeNptOperatorProblems);
					timeNptOperatorProblems = thisConverter.getConvertedValue();
				}
				thisRushmoreWell.setTimeNptOperatorProblems(timeNptOperatorProblems);			// get Interrupt time due to operator problems
			}
		
			Double timeNptExternalProblems = calculateDurationBy(operationUid,new String[]{"AI", "PD", "LG", "LD", "CP", "FM", "SI", "WO"}, new String[]{"TP","TU"},qp);
		
			if (timeNptExternalProblems != null) {
				if (thisConverter != null){
					thisConverter.setReferenceMappingField(RushmoreWells.class, "timeNptExternalProblems");
					thisConverter.setBaseValue(timeNptExternalProblems);
					timeNptExternalProblems = thisConverter.getConvertedValue();
				}					
				thisRushmoreWell.setTimeNptExternalProblems(timeNptExternalProblems);			// get Interrupt time due to external problems
			}
			
			Double timeNptDownHoleProblems = calculateDurationBy(operationUid,new String[]{"LC", "FP", "WC", "HC", "SP", "FS", "ST", "CS", "CM", "DD", "LP", "DO"}, new String[]{"TP","TU"},qp);
			
			if (timeNptDownHoleProblems != null) {
				if (thisConverter != null){
					thisConverter.setReferenceMappingField(RushmoreWells.class, "timeNptDownHoleProblems");
					thisConverter.setBaseValue(timeNptDownHoleProblems);
					timeNptDownHoleProblems = thisConverter.getConvertedValue();
				}	
				thisRushmoreWell.setTimeNptDownHoleProblems(timeNptDownHoleProblems);			// get Interrupt time due to downhole problems
			}
			
			Double timeWowDemooring = calculateDurationBy(operationUid,new String[]{"SC"}, new String[]{"TP","TU"},qp);

			if (timeWowDemooring != null) {
				if (thisConverter != null){
					thisConverter.setReferenceMappingField(RushmoreWells.class, "timeWowDemooring");
					thisConverter.setBaseValue(timeWowDemooring);
					timeWowDemooring = thisConverter.getConvertedValue();
				}	
				thisRushmoreWell.setTimeWowDemooring(timeWowDemooring);			// get Interrupt time due to wait on weather also includes sea conditions
			}
	
		}
		
	}
	public static Double calculateDurationBy(String operationUid, String[] arrayRushmoreCode, String[] arrayInternalClassCode,QueryProperties qp) throws Exception
	{
		List<String> rushmoreCode = Arrays.asList(arrayRushmoreCode);
		List<String> internalClassCode = Arrays.asList(arrayInternalClassCode);
		String sql = "SELECT SUM(a.activityDuration) as duration FROM Activity a, Daily d " +
		"WHERE (a.isDeleted = false or a.isDeleted is null) " +
		"AND (d.isDeleted = false or d.isDeleted is null) " +
		"AND a.dailyUid = d.dailyUid " +
		"AND a.operationUid = :thisOperationUid " +
		"AND (a.isSimop=false or a.isSimop is null) " +
		"AND (a.isOffline=false or a.isOffline is null) " +
		"AND (a.internalClassCode IN (:internalClassCode)) " +
		"AND (a.rootCauseCode IN (select shortCode from LookupRootCauseCode " +
		"	where (isDeleted=false or isDeleted is null) " +
		"	and rushmoreCode in (:rushmoreCode)))";
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[]{"thisOperationUid","rushmoreCode","internalClassCode"},new Object[]{operationUid, rushmoreCode,internalClassCode}, qp);
		
		if (result.size() > 0)
		{
			Object thisActivity = result.get(0);
			if (thisActivity != null) {
				return Double.parseDouble(thisActivity.toString());
			}
		}
		return null;
	}
	
	
	public static void populateInterruptCodes(CommandBeanTreeNode node,RushmoreWells rushmoreWells, CustomFieldUom thisConverter, String operationUid, QueryProperties qp) throws Exception
	{
		//Root cause has mapped to rushmore
		String strSql88="SELECT SUM(a.activityDuration) as totalHours, rc.rushmoreCode FROM Activity a, Daily d, LookupRootCauseCode rc, Operation o " +
					" WHERE (a.isDeleted = false or a.isDeleted is null)   " +
					" AND (d.isDeleted = false or d.isDeleted is null)   " +
					" AND (rc.isDeleted = false or rc.isDeleted is null)   " +
					" AND (o.isDeleted = false or o.isDeleted is null)   " +
					" AND o.operationUid = d.operationUid   " +
					" AND d.dailyUid = a.dailyUid   " +
					" AND rc.shortCode = a.rootCauseCode " +
					" and (rc.operationCode = '' or rc.operationCode is null or rc.operationCode = o.operationCode) " +
					" AND d.operationUid = :thisOperationUid " +
					" AND (a.isSimop=false or a.isSimop is null)   " +
					" AND (a.isOffline=false or a.isOffline is null)   " +
					" and (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
					" AND a.rootCauseCode !='' and a.rootCauseCode is not null  " +
					" AND rc.rushmoreCode !='' and rc.rushmoreCode is not null"+
					" GROUP BY rc.rushmoreCode ORDER BY rc.rushmoreCode";
	
		List Result88 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql88, "thisOperationUid", operationUid, qp);
		populateInterruptCode(Result88,node,thisConverter,rushmoreWells,qp);
		
		strSql88="SELECT SUM(a.activityDuration) as totalHours, a.rootCauseCode FROM Activity a, Daily d, Operation o " +
		" WHERE (a.isDeleted = false or a.isDeleted is null)   " +
		" AND (d.isDeleted = false or d.isDeleted is null)   " +
		" AND (o.isDeleted = false or o.isDeleted is null)   " +
		" AND o.operationUid = d.operationUid   " +
		" AND d.dailyUid = a.dailyUid   " +
		" AND d.operationUid = :thisOperationUid " +
		" AND (a.isSimop=false or a.isSimop is null)   " +
		" AND (a.isOffline=false or a.isOffline is null)   " +
		" and (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is null) " +
		" AND a.rootCauseCode != '' and a.rootCauseCode is not null  " +
		" AND a.internalClassCode in ('TP', 'TU') " +
		" AND a.rootCauseCode not in (" +
		"		select rc.shortCode from LookupRootCauseCode rc " +
		"		WHERE (rc.isDeleted=false or rc.isDeleted is null) " +
		"		AND (rc.operationCode = '' or rc.operationCode is null or rc.operationCode = o.operationCode) " +
		" 		AND rc.rushmoreCode !='' " +
		"		AND rc.rushmoreCode is not null "+
		"	) " +
		"GROUP BY a.rootCauseCode ORDER BY a.rootCauseCode";

		Result88 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql88, "thisOperationUid", operationUid, qp);
		populateInterruptCode(Result88,node,thisConverter,rushmoreWells,qp);
		
	}
	
	
	public static void beforeSaveOrUpdate(RushmoreWells thisRushmoreWell, Boolean _setHiddenFieldsToNull,String onOffShore) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
	{
		if (_setHiddenFieldsToNull) { // set hidden fields on screen to null - For rushmore DPR 2011 onwards
			
			//Locator Well = No, Disable Locator Unused Length field
			if (!"Y".equals(thisRushmoreWell.getLocatorWell())) thisRushmoreWell.setLocatorUnusedLength(null);

			//IF Multi-Lateral = Yes, Enable "Multi-lateral junction type" and "No. of laterals"
			if (!"Y".equals(thisRushmoreWell.getMultiLateral())) {
				thisRushmoreWell.setMultiLateralJunctionType(null);
				thisRushmoreWell.setNumLaterals(null);
			}

			//Pre-existing casing strings, Only enable this section IF Well Types = "G" or "S".
			if (!"G".equals(thisRushmoreWell.getHoleType()) && !"S".equals(thisRushmoreWell.getHoleType())) {
				for(int a=0;a<11;a++)
				{
					PropertyUtils.setProperty(thisRushmoreWell, "preExistCasing"+(a+1), null);
				}
			}

			//New conductor casing, Only enable it if Hole Type = "N".
			if (!"N".equals(thisRushmoreWell.getHoleType())) {
				for(int a=0;a<11;a++)
				{
					PropertyUtils.setProperty(thisRushmoreWell, "casing"+(a+1), null);
				}
				thisRushmoreWell.setConductorInstalled(null);
			}
			
			//Land Well, Enable section when operation is onshore else make it non editable.
			if (!"ON".equals(onOffShore)) {
				thisRushmoreWell.setRigMoveTime(null);
				thisRushmoreWell.setRigMovedWithinField(null);
			}

			//IF Hole Type = S, Set these two fields to mandatory. 
			if (!"S".equals(thisRushmoreWell.getHoleType())) {
				thisRushmoreWell.setDaysSlotRecoveryPrespud(null);
				thisRushmoreWell.setSlotRecoveryOffline(null);
			}
			
		}
	}
	private static void populateInterruptCode(List Result88, CommandBeanTreeNode node, CustomFieldUom thisConverter,RushmoreWells rushmoreWells,QueryProperties qp) throws Exception{
		
		if (Result88 != null)
		{
		
			
			for (Object rec:Result88) {
				RushmoreTimeInterrupt thisRushmoreTimeInterrupt = new RushmoreTimeInterrupt();
				Object[] objDuration = (Object[]) rec;
				
				Double totalDuration = (Double)objDuration[0];
				String rushmoreCode = (String)objDuration[1];//"N/A";
				if (StringUtils.isBlank(rushmoreCode)) rushmoreCode = "N/A";
				
				if (totalDuration != null && totalDuration>0.0){
					Double interruptTime = Double.parseDouble(objDuration[0].toString());
					if (thisConverter!=null)
					{
						thisConverter.setReferenceMappingField(RushmoreTimeInterrupt.class, "interruptTime");
						thisConverter.setBaseValue(interruptTime);
						interruptTime = thisConverter.getConvertedValue();
					}
					thisRushmoreTimeInterrupt.setInterruptTime(interruptTime);		// Duration
					thisRushmoreTimeInterrupt.setInterruptCode(rushmoreCode);									// Interrupt Codes
				}
				if (node != null && node.getInfo().isTemplateNode()) {
					node.addCustomNewChildNodeForInput(thisRushmoreTimeInterrupt);
				}
				if (node == null && rushmoreWells !=null)
				{
					thisRushmoreTimeInterrupt.setOperationUid(rushmoreWells.getOperationUid());
					thisRushmoreTimeInterrupt.setWellboreUid(rushmoreWells.getWellboreUid());
					thisRushmoreTimeInterrupt.setWellUid(rushmoreWells.getWellUid());
					thisRushmoreTimeInterrupt.setRushmoreWellsUid(rushmoreWells.getRushmoreWellsUid());
					thisRushmoreTimeInterrupt.setGroupUid(rushmoreWells.getGroupUid());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisRushmoreTimeInterrupt,qp);
				}
				
			}
		}
	}
	
	public static void populateDefaultMultiSelect(CommandBeanTreeNode node, String operationUid) throws Exception
	{
		String strSql11 = "SELECT DISTINCT(mudType) as drillingFluidType FROM MudProperties WHERE (isDeleted = false or isDeleted is null) AND operationUid = :thisOperationUid";
		List Result11 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql11, "thisOperationUid", operationUid);
		String mudtype = "";
		if (!Result11.isEmpty())
		{
			List<String> drillingFluidTypes = new ArrayList<String>();
			for (Object obj : Result11) {
				if (obj != null) {
					mudtype = (String) obj;
					if (mudtype.equals("h2o")){
						drillingFluidTypes.add("W");
					} else if (mudtype.equals("oil")){
						drillingFluidTypes.add("O");
					} else if (mudtype.equals("sbm")){
						drillingFluidTypes.add("S");
					} else if (mudtype.equals("ester")){
						drillingFluidTypes.add("E");
					} else if (mudtype.equals("foam")){
						drillingFluidTypes.add("F");
					} else if (mudtype.equals("air")){
						drillingFluidTypes.add("A");
					} else if (mudtype.equals("oth")){
						// oth
					}
				}
			}
			node.getMultiSelect().getValues().put("drillingFluidType", drillingFluidTypes);
		}
	}
	
	private static String toString(Double value) {
		if (value != null) {
			return value.toString();
		}
		return null;
	}
}
