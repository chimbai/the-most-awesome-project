package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;


public class DvdPlanDetailReportGenerator implements ReportDataGenerator{
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		QueryProperties qp = new QueryProperties();
		String currentOperationUid = userContext.getUserSelection().getOperationUid();
		
		
		String strSql = "SELECT operationPlanTaskUid, detailCode, sequence, detailName, tlDuration, p10Duration, p50Duration, p90Duration, operationPlanDetailUid, operationPlanPhaseUid " +
				"FROM OperationPlanDetail " +
				"WHERE isDeleted IS NULL " +
				"AND operationUid = :currentOperationUid ";
		String[] paramsFields = {"currentOperationUid"};
		Object[] paramsValues = {currentOperationUid};
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues,qp);
		
		if (lstResult.size() > 0){
			for(Object objResult: lstResult){
				Object[] objDuration = (Object[]) objResult;
				ReportDataNode thisReportNode = reportDataNode.addChild("planDetails");
				String operationPlanTaskUid = "";
				String detailCode = "";
				String sequence = "";
				String detailName = "";
				String tlDuration = "";
				String p10Duration = "";
				String p50Duration = "";
				String p90Duration = "";
				String actualDepth = "";
				String actualDuration = "";
				String operationPlanDetailUid = "";
				String operationPlanPhaseUid = "";
				String phaseName = "";
				
				if (objDuration[8] != null && StringUtils.isNotBlank(objDuration[8].toString())){
					operationPlanTaskUid = objDuration[0].toString();
					operationPlanDetailUid = objDuration[8].toString();
					
					String strSql2 = "SELECT MAX(depthMdMsl) as depth, SUM(activityDuration)/3600.00 as duration " +
							"FROM Activity " +
							"WHERE isDeleted IS NULL " +
							"AND planDetailReference = :operationPlanDetailUid ";
					String[] paramsFields2 = {"operationPlanDetailUid"};
					Object[] paramsValues2 = {operationPlanDetailUid};
					List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2,qp);
					
					for(Object objResults: lstResult2){
						Object[] objDur = (Object[]) objResults;
						
						if (objDur[0] != null && StringUtils.isNotBlank(objDur[0].toString())){
							actualDepth = objDur[0].toString();
						}
						else {
							actualDepth = "0.00";
						}
						
						if (objDur[1] != null && StringUtils.isNotBlank(objDur[1].toString())){ 
							actualDuration = objDur[1].toString();
						}
						else{
							actualDuration = "0.00";
						}
						
						if (lstResult2.size() > 0){
							thisReportNode.addProperty("operationPlanTaskUid", operationPlanTaskUid.toString());
							thisReportNode.addProperty("actualDepth", actualDepth.toString());
							thisReportNode.addProperty("actualDuration", actualDuration.toString());
							thisReportNode.addProperty("operationPlanDetailUid", operationPlanDetailUid.toString());
						}
					}
				}
				else {
					thisReportNode.addProperty("operationPlanTaskUid", "");
					thisReportNode.addProperty("actualDepth", "0.00");
					thisReportNode.addProperty("actualDuration", "0.00");
				}
				
				if (objDuration[1] != null && StringUtils.isNotBlank(objDuration[1].toString())){ detailCode = objDuration[1].toString();}
				if (objDuration[2] != null && StringUtils.isNotBlank(objDuration[2].toString())){ sequence = objDuration[2].toString();}
				if (objDuration[3] != null && StringUtils.isNotBlank(objDuration[3].toString())){ detailName = objDuration[3].toString();}
				if (objDuration[4] != null && StringUtils.isNotBlank(objDuration[4].toString())){ tlDuration = objDuration[4].toString();}
				if (objDuration[5] != null && StringUtils.isNotBlank(objDuration[5].toString())){
					p10Duration = objDuration[5].toString();
				}
				else {
					p10Duration = "0.00";
				}
				if (objDuration[6] != null && StringUtils.isNotBlank(objDuration[6].toString())){
					p50Duration = objDuration[6].toString();
				}
				else {
					p50Duration = "0.00";
				}
				if (objDuration[7] != null && StringUtils.isNotBlank(objDuration[7].toString())){
					p90Duration = objDuration[7].toString();
				}
				else {
					p90Duration = "0.00";
				}
				
				if (objDuration[9] != null && StringUtils.isNotBlank(objDuration[9].toString())){
					operationPlanPhaseUid = objDuration[9].toString();
					
					String strSql3 = "SELECT operationPlanPhaseUid, phaseName " +
							"FROM OperationPlanPhase " +
							"WHERE isDeleted IS NULL " +
							"AND operationPlanPhaseUid = :operationPlanPhaseUid ";
					String[] paramsFields3 = {"operationPlanPhaseUid"};
					Object[] paramsValues3 = {operationPlanPhaseUid};
					List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3,qp);
					
					for(Object obj: lstResult3){
						Object[] objLength = (Object[]) obj;
						
						if (objLength[1] != null && StringUtils.isNotBlank(objLength[1].toString())){
							phaseName = objLength[1].toString();
						}
						
						thisReportNode.addProperty("phaseName", phaseName.toString());	
						thisReportNode.addProperty("operationPlanPhaseUid", operationPlanPhaseUid.toString());
					}
				}
				
				thisReportNode.addProperty("detailCode", detailCode.toString());
				thisReportNode.addProperty("sequence", sequence.toString());
				thisReportNode.addProperty("detailName", detailName.toString());
				thisReportNode.addProperty("tlDuration", tlDuration.toString());
				thisReportNode.addProperty("p10Duration", p10Duration.toString());
				thisReportNode.addProperty("p50Duration", p50Duration.toString());
				thisReportNode.addProperty("p90Duration", p90Duration.toString());
			}
					
		}
	}

}