package com.idsdatanet.d2.drillnet.cementJob;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupProvider;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CementJobCasingDescriptionOdLookupHandler implements LookupHandler {
	private URI xmlLookup = null;
	private URI xmlDescriptionLookup = null;
	private LookupProvider lookupProvider = null;
	private Boolean isWellbore = false;
	
	public void setXmlDescriptionLookup(URI xmlDescriptionLookup) {
		this.xmlDescriptionLookup = xmlDescriptionLookup;
	}
	
	public URI getXmlDescriptionLookup() {
		return this.xmlDescriptionLookup;
	}
	
	public void setXmlLookup(URI xmlLookup) {
		this.xmlLookup = xmlLookup;
	}
	
	public URI getXmlLookup() {
		return this.xmlLookup;
	}

	public LookupProvider getLookupProvider() {
		return lookupProvider;
	}

	public void setLookupProvider(LookupProvider lookupProvider) {
		this.lookupProvider = lookupProvider;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		Map<String, LookupItem> casingLookup = this.getLookupProvider().getLookup(this.getXmlLookup(), userSelection);
		Map<String, LookupItem> casingDescriptionLookup = this.getLookupProvider().getLookup(this.getXmlDescriptionLookup(), userSelection);
		
		String condition  = isWellbore?"wellboreUid = :wellboreUid":"operationUid= :operationUid";
		List<CasingSection> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and "+condition+" and (tallyType is null)   order by casingOd", isWellbore?"wellboreUid":"operationUid", isWellbore?userSelection.getWellboreUid():userSelection.getOperationUid());
		if((list != null && list.size() > 0) && (casingLookup != null)) {
			for(CasingSection casingSection : list) {
				String casingSectionUid 	 = casingSection.getCasingSectionUid();
				String strCasingOd = null;
				if (casingSection.getCasingOd()!=null)
				{
					CustomFieldUom thisConverter = new CustomFieldUom((Locale) null, CasingSection.class, "casingOd");
					strCasingOd = thisConverter.formatOutputPrecision(casingSection.getCasingOd());
				}
				
				String captionCasingOd = "";
				String captionDescription = "";
				
				
				
				if(StringUtils.isNotBlank(strCasingOd) && casingLookup.containsKey(strCasingOd)) {
					LookupItem lookupCasingOdItem = casingLookup.get(strCasingOd);
					captionCasingOd = lookupCasingOdItem.getValue().toString();
				}
				
				if(StringUtils.isNotBlank(casingSection.getSectionName()) && casingDescriptionLookup.containsKey(casingSection.getSectionName()))
				{
					LookupItem lookupDescItem = casingDescriptionLookup.get(casingSection.getSectionName());
					captionDescription = lookupDescItem.getValue().toString();
				}
				
				if (StringUtils.isNotBlank(captionDescription) || StringUtils.isNotBlank(captionCasingOd))
				{
					String lookupValue = captionDescription + " - " +captionCasingOd;
					result.put(casingSectionUid, new LookupItem(casingSectionUid,lookupValue,strCasingOd));
				}
				
					
			}
		}
		
		return result;
	}

	public void setIsWellbore(Boolean isWellbore) {
		this.isWellbore = isWellbore;
	}

	public Boolean getIsWellbore() {
		return isWellbore;
	}
}