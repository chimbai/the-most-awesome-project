package com.idsdatanet.d2.drillnet.fileBridgeFilesManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.FileBridgeFiles;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.infra.filemanager.html.FileManagerCommandBeanListener;
import com.idsdatanet.depot.d2.filebridge.util.FileBridgeUtils;

public class FileBridgeFilesManagerCommandBeanListener extends FileManagerCommandBeanListener{
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		if (commandBean.getRoot().getDynaAttr().get("statusFilter") == null) commandBean.getRoot().getDynaAttr().put("statusFilter", "All");
		super.afterDataLoaded(commandBean, root, userSelection, request);
	}
	
	@Override
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
		String fileBridgeFilesUid = (String) request.getParameter("fileBridgeFilesUid");
		if (StringUtils.isNotBlank(fileBridgeFilesUid)) {
			if (invocationKey.equals("statusRemarkCheck")) {
				FileBridgeFiles fbf = (FileBridgeFiles) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(FileBridgeFiles.class, fileBridgeFilesUid);
				String strResult = "<response>"; 
				strResult += "<parserKey>" + FileBridgeUtils.getParserKeyLabel(fbf.getParserKey()) + "</parserKey>";
				strResult += "<status>" + FileBridgeUtils.getStatusLabel(fbf.getStatus()) + "</status>";
				strResult += "<remark>" + fbf.getRemark() + "</remark>";
				strResult += "</response>"; 
				response.getWriter().write(strResult);
			}
		}
	}
}
