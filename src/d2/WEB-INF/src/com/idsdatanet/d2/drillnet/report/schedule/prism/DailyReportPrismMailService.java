package com.idsdatanet.d2.drillnet.report.schedule.prism;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.report.DDRReportModule;
import com.idsdatanet.d2.drillnet.report.ReportJobParams;
import com.idsdatanet.d2.drillnet.report.ReportJobResult;
import com.idsdatanet.d2.drillnet.report.schedule.EmailConfiguration;
import com.idsdatanet.d2.drillnet.report.schedule.corportatedrillingportal.BaseDataTransferService;

public class DailyReportPrismMailService extends BaseDataTransferService{

	public static final int SUCCESS=1;
	public static final int ERROR=-1;
	private static final String RIG_PATTERN="{dailyrigname}";
	
	private String reportDateFormat="dd MMM yyyy";

	private DDRReportModule ddrReportModule;
	private boolean isRunning=false;
	
	public void setDdrReportModule(DDRReportModule ddrReportModule) {
		this.ddrReportModule = ddrReportModule;
	}

	public DDRReportModule getDdrReportModule() {
		return ddrReportModule;
	}

	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void setReportDateFormat(String reportDateFormat) {
		this.reportDateFormat = reportDateFormat;
	}

	public String getReportDateFormat() {
		return reportDateFormat;
	}
	
	public synchronized void run() throws Exception
	{
		if (this.getEnabled())
		{
			this.setRunning(true);
			List<Daily> list = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(new Date());
			for(Daily day:list)
			{
				Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
				if (StringUtils.isNotBlank(operation.getOperationName())) {
					this.runExtractionAndSendEmail(day,null);
				}
			}
			this.setRunning(false);
		}
	}

	public synchronized Integer runExtractionAndSendEmail(Daily day, String[] emailList) throws Exception
	{
		try{
			ReportJobParams reportData=ddrReportModule.generateDDRByDailyUid(day.getDailyUid(), this.getGenerateReportUserName(),null);
			reportData.waitOnThisObject();
			ReportJobResult jobResult=reportData.getLastReportJobResult();
			if (!jobResult.isError())
			{
				this.sendDailyEmail(day,jobResult,emailList);
				return SUCCESS;
			}
			else
			{
				this.sendErrorEmail(day);
				return ERROR;
			}
		}catch(Exception ex)
		{
			this.sendErrorEmail(day);
			this.getLogger().error(ex);
			return ERROR;
		}
	}
	
	private String[] getEmailList(EmailConfiguration emailConfig) throws Exception
	{
		if (emailConfig.getIncludeSupportEmail()!=null && emailConfig.getIncludeSupportEmail())
		{
			return (emailConfig.getSpecificEmailDistributionKey()!=null?this.getEmailListWithSupport(emailConfig.getSpecificEmailDistributionKey()):this.getEmailListWithSupport());
		}
		return emailConfig.getSpecificEmailDistributionKey()!=null?this.getEmailList(emailConfig.getSpecificEmailDistributionKey()):this.getEmailList();
	}
	
	private void sendErrorEmail(Daily day) throws Exception
	{
		String[] errorReceipient={this.getSupportEmail()};
		
		Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(day.getOperationUid());
		ReportDaily thisReportDaily = this.reportDailyList(day.getOperationUid(), reportType, day.getDailyUid());
			
		EmailConfiguration emailConfig=new EmailConfiguration(this.getErrorEmailConfiguration());
		emailConfig.clearContentMapping();
		emailConfig.addContentMapping("operationName",this.getCompleteOperationName(operation));
		emailConfig.addContentMapping("reportDatetime", this.formatDate(thisReportDaily.getReportDatetime(), reportDateFormat));
		emailConfig.setEmailList(errorReceipient);
		if (emailConfig!=null && emailConfig.getSubject()!=null){
			emailConfig.setSubject(emailConfig.getSubject());
		}else{
			emailConfig.setSubject("IDS Report");
		}
		this.sendEmail(emailConfig);
	}
	
	private void sendDailyEmail(Daily day, ReportJobResult reportJobResult,String[] emailList) throws Exception
	{
		Operation operation=ApplicationUtils.getConfiguredInstance().getCachedOperation(day.getOperationUid());		
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(day.getOperationUid());
		ReportDaily thisReportDaily = this.reportDailyList(day.getOperationUid(), reportType, day.getDailyUid());
		String reportFileName=reportJobResult.getReportFile().getDisplayName()+"."+this.getFileExtension(reportJobResult.getReportFilePath());
		RigInformation rig=(RigInformation)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class, thisReportDaily.getRigInformationUid());
		
		Map<String, File> attachment = new HashMap<String,File>();
		attachment.put(reportFileName, new File(reportJobResult.getReportFilePath()));
		
		//attach prism report if generated 
		String[] prismReport = checkPrismReportGenerated(day.getOperationUid(), day.getDailyUid());
		if (prismReport != null){
			attachment.put(prismReport[0], new File(prismReport[1]));
		}
		
		EmailConfiguration emailConfig=new EmailConfiguration(this.getValidEmailConfiguration());
		
		emailConfig.clearContentMapping();
		emailConfig.addContentMapping("operationName",this.getCompleteOperationName(operation));
		emailConfig.addContentMapping("reportDatetime", this.formatDate(thisReportDaily.getReportDatetime(), reportDateFormat));
		
		emailConfig.setEmailList(emailList==null?this.getEmailList(emailConfig):emailList);
		
		if (emailConfig!=null && emailConfig.getSubject()!=null){
			emailConfig.setSubject(emailConfig.getSubject().replace(RIG_PATTERN, rig.getRigName()));
		}else{
			emailConfig.setSubject("IDS Report");
		}		
		emailConfig.setAttachment(attachment);
		this.sendEmail(emailConfig);

	}
	
	private String[] checkPrismReportGenerated (String operationUid, String dailyUid) throws Exception{
		String[] selectReportFile = new String[2];
		String thisReportType = "PRISM";
		String[] paramsFields = {"thisOperation", "reportFilesReportType", "dailyUid"};
		String[] paramsValues = {operationUid, thisReportType, dailyUid};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rf.reportType=:reportFilesReportType and rf.dailyUid=:dailyUid ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues);
					
		if(items.size() > 0)
		{
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();

				ReportFiles thisReportFile = (ReportFiles) obj_array[0];
				File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());				
			  
				if(report_file.exists()){
					selectReportFile[0] = thisReportFile.getDisplayName() + "." + this.getFileExtension(thisReportFile.getReportFile()) ;
					selectReportFile[1] = ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile().toString();
					return selectReportFile;
				}
			}
		}
		return null;
		
	}
	private ReportDaily reportDailyList (String operationUid, String reportType, String dailyUid) throws Exception {
		
		String strSql ="FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid =:operationUid AND dailyUid =:dailyUid AND (reportType = :reportType)"; 
		String[] paramsFields = {"operationUid", "dailyUid", "reportType"};
		String[] paramsValues = {operationUid, dailyUid, reportType};						
		List<ReportDaily> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		
		if (rs.size() > 0) {
			return (ReportDaily) rs.get(0);
		} else {
			return null;
		}
	}	
	
}