package com.idsdatanet.d2.drillnet.transportDailyParam;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.SupportVesselInformation;
import com.idsdatanet.d2.core.model.TransportDailyParam;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.transportDailyParam.TransportDailyParamUtils;

public class TransportDailyParamDataNodeListener extends EmptyDataNodeListener {

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof TransportDailyParam) {
			TransportDailyParam thisTransportDailyParam = (TransportDailyParam) object;
			
			Date arrival = (Date) PropertyUtils.getProperty(object, "arrivalTime");
			Date departure = (Date) PropertyUtils.getProperty(object, "departureTime");
			
			if(arrival != null && departure != null){
				if(arrival.getTime() > departure.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "arrivalTime", "Arrival date time cannot be greater than Departure date time.");
					return;
				}
				Long duration = Math.abs((arrival.getTime() - departure.getTime()) / 1000);
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, TransportDailyParam.class, "tripDuration");
				thisConverter.setBaseValue(duration);
				
				thisTransportDailyParam.setTripDuration(thisConverter.getConvertedValue());
			}
			
			String vesselName = (String) node.getDynaAttr().get("vesselName");
			String companyuid = (String) node.getDynaAttr().get("companyuid");
			
			if (StringUtils.isNotBlank(vesselName)) {
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from SupportVesselInformation where (isDeleted = false or isDeleted is null) and vesselName = :vesselName", "vesselName", vesselName);
				if (list.size() > 0) {
					status.setDynamicAttributeError(node, "vesselName", "The Vessel Name '" + vesselName + "' already exists in database");
					status.setContinueProcess(false, true);
				}
			}
			if (status.isProceed()) {
				if (StringUtils.isNotBlank(vesselName)) {
					SupportVesselInformation thisSupportVesselInformation = TransportDailyParamUtils.createSupportVesselInformation(vesselName,companyuid);
					thisTransportDailyParam.setSupportVesselInformationUid(thisSupportVesselInformation.getSupportVesselInformationUid());
					thisTransportDailyParam.setLookupCompanyUid(companyuid);
					commandBean.getSystemMessage().addInfo("A new support vessel information has been created with minimal information from the Support Vessel screen. Please make sure that the Support Vessel Information module is reviewed and relevant details are added in.");
				}
			}
			
		}		
	}
}
