package com.idsdatanet.d2.drillnet.report;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.multiselect.MultiSelect;

public class ActivityCodesReportModule extends DefaultReportModule {
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		
		UserSelectionSnapshot userSelection = UserSelectionSnapshot.getInstanceForCustomDaily(session, session.getCurrentDailyUid());
		MultiSelect multiselect = commandBean.getRoot().getMultiSelect();
		if (multiselect!=null) {
			Map<String, List<String>> values = multiselect.getValues();
			if (values!=null && values.containsKey("@reportOperationCodesFilter")) {
				userSelection.setCustomProperty("reportOperationCodesFilter", values.get("@reportOperationCodesFilter"));		
			}
		}
		
		return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));
	}
}
