package com.idsdatanet.d2.drillnet.well;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.Datum;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class DefaultDatumLookupHandler implements LookupHandler {
	
	public final static String MSL = "MSL";
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		result.put(MSL, new LookupItem(MSL, MSL));
		List<Datum> datums = DatumManager.getDatums(commandBean.getUserLocale(), userSelection.getOperationUid(), userSelection.getGroupUid());
		if (datums != null) {
			for (Datum datum : datums) {
				result.put(datum.getOpsDatumUid(), new LookupItem(datum.getOpsDatumUid(), datum.getDisplayName() + slantRigLabel(datum.getOpsDatumUid(),commandBean)));
			}
		}
		return result;
	}
	
	public String slantRigLabel(String opsDatumUid, CommandBean commandBean) throws Exception
	{
		String sql = "select entryType, slantAngle, surfaceSlantLength from OpsDatum where (isDeleted = false or isDeleted is null) and opsDatumUid=:opsDatumUid";
		String label = "";
		List<Object []> opsDatumResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "opsDatumUid",  opsDatumUid);
		if (opsDatumResults.size() > 0) {
			for (Object[] opsDatumResult : opsDatumResults) {
				
				if (opsDatumResult[0] != null && "1".equals(opsDatumResult[0].toString()))
					{
						if (opsDatumResult[1] != null && opsDatumResult[2] != null ) {		
							
							CustomFieldUom thisConverter = new CustomFieldUom(commandBean, OpsDatum.class, "slantAngle");
							thisConverter.setBaseValue(Double.parseDouble(opsDatumResult[1].toString()));
							label = thisConverter.getFormattedValue(thisConverter.getConvertedValue());
							
							thisConverter.setReferenceMappingField(OpsDatum.class, "surfaceSlantLength");
							thisConverter.setBaseValue(Double.parseDouble(opsDatumResult[2].toString()));
							label = label + " " + thisConverter.getFormattedValue(thisConverter.getConvertedValue());
							
							return " Angle/Length: " + label;
						}
					}		
			}
		}
		return "";
	}

}
