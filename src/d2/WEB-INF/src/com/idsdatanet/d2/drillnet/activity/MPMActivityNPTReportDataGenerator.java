package com.idsdatanet.d2.drillnet.activity;

import java.util.List;
import java.util.Map;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class MPMActivityNPTReportDataGenerator implements ReportDataGenerator {
    
    private String uriLookupCompany = "db://LookupCompany?key=lookupCompanyUid&value=companyName&cache=false&order=companyName";
    private Map<String,LookupItem> companyList = null;
    
    @Override
    public void disposeOnDataGeneratorExit() {
        // TODO Auto-generated method stub
    }

    @Override
    public void generateData(UserContext userContext,
            ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
        // TODO Auto-generated method stub
        
        this.setCompanyList(LookupManager.getConfiguredInstance().getLookup(this.uriLookupCompany, userContext.getUserSelection(), null));
        String strSql = "FROM Activity WHERE (isDeleted = false OR isDeleted IS NULL) AND operationUid = :thisOperationUid AND dailyUid = :thisDailyUid AND (internalClassCode = 'TP' OR internalClassCode = 'TU')  AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL) AND (dayPlus IS NULL OR dayPlus=0) ORDER BY startDatetime ASC";
        String[] paramsFields = {"thisOperationUid","thisDailyUid"};
        Object[] paramsValues = {userContext.getUserSelection().getOperationUid(),userContext.getUserSelection().getDailyUid()};

        List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
        
        if (lstResult != null && lstResult.size() > 0) {
            String strSql2 = "FROM ReportDaily WHERE (isDeleted = false OR isDeleted IS NULL) AND operationUid = :thisOperationUid AND dailyUid = :thisDailyUid";
            String[] paramsFields2 = {"thisOperationUid","thisDailyUid"};
            Object[] paramsValues2 = {userContext.getUserSelection().getOperationUid(),userContext.getUserSelection().getDailyUid()};
        
            List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);

            Double dayCost = 0.0;
            Double totalActivityDuration = 0.0;
            
            if (lstResult2 != null) {
                ReportDaily rd = (ReportDaily) lstResult2.get(0);
                dayCost = rd.getDaycost();
                totalActivityDuration = CommonUtil.getConfiguredInstance().calculateActivityDurationByDaily(rd.getDailyUid()) / 3600;
            }
            
            CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "activity_duration");
            String uomDuration = thisConverter.getUomSymbol();
            
            if (lstResult.size() > 1) {
                Boolean isContinuous = false;
                Double cumDuration = 0.0;
                String initialStartDateTime = null;
                String lastEndDateTime = null;
                for (int i = 0; i < lstResult.size(); i++) {
                    Activity result = (Activity) lstResult.get(i);
                    if (initialStartDateTime == null) {
                        initialStartDateTime = result.getStartDatetime().toString();
                    }
                    String nptCategory = result.getRegulatorySubcode();
                    String nptType = result.getRegulatoryCode();
                    String vendor = (result.getLookupCompanyUid() == null) ? "" : result.getLookupCompanyUid();
                    if (i + 1 < lstResult.size()) {
                        Activity nextResult = (Activity) lstResult.get(i + 1);
                        String nptCategory2 = nextResult.getRegulatorySubcode();
                        String nptType2 = nextResult.getRegulatoryCode();
                        String vendor2 = (nextResult.getLookupCompanyUid() == null) ? "" : nextResult.getLookupCompanyUid();
                        Double duration2 = nextResult.getActivityDuration();
                        if (nptCategory != null && nptType != null && vendor != null) {
                            if (nptCategory.equals(nptCategory2) && nptType.equals(nptType2) && vendor.equals(vendor2) && result.getEndDatetime().equals(nextResult.getStartDatetime())) {
                                if (!isContinuous) {
                                    initialStartDateTime = result.getStartDatetime().toString();
                                    cumDuration = result.getActivityDuration() + duration2;
                                } else {
                                    cumDuration += duration2;
                                }
                                lastEndDateTime = nextResult.getEndDatetime().toString();
                                isContinuous = true;
                            } else {
                                if (isContinuous) {
                                    // add cumulative data into xml
                                    ReportDataNode thisReportNode = reportDataNode.addChild("ActivityNPT");
                                    thisReportNode.addProperty("startDateTime", initialStartDateTime);
                                    thisReportNode.addProperty("endDateTime", lastEndDateTime);
                                    thisReportNode.addProperty("activityDuration", cumDuration.toString());
                                    thisReportNode.addProperty("uomDuration", uomDuration);
                                    thisReportNode.addProperty("regulatorySubcode", nptCategory);
                                    thisReportNode.addProperty("regulatoryCode", nptType);
                                    thisReportNode.addProperty("lookupCompanyUid", vendor);
                                    LookupItem item = this.companyList.get(vendor);
                                    if (item != null) {
                                        thisReportNode.addProperty("lookupCompanyUid_lookupLabel",item.getValue().toString());
                                    }
                                    thisReportNode.addProperty("activityDescription", "");
                                    Double netCost = 0.0;
                                    if (dayCost != null && cumDuration != null && totalActivityDuration > 0.0) {
                                        netCost = (dayCost * cumDuration) / totalActivityDuration;
                                    }
                                    thisReportNode.addProperty("netCost", netCost.toString());
                                    isContinuous = false;
                                } else {
                                    ReportDataNode thisReportNode = reportDataNode.addChild("ActivityNPT");
                                    thisReportNode.addProperty("startDateTime", result.getStartDatetime().toString());
                                    thisReportNode.addProperty("endDateTime", result.getEndDatetime().toString());
                                    thisReportNode.addProperty("activityDuration", result.getActivityDuration().toString());
                                    thisReportNode.addProperty("uomDuration", uomDuration);
                                    thisReportNode.addProperty("regulatorySubcode", nptCategory);
                                    thisReportNode.addProperty("regulatoryCode", nptType);
                                    thisReportNode.addProperty("lookupCompanyUid", vendor);
                                    LookupItem item = this.companyList.get(vendor);
                                    if (item != null) {
                                        thisReportNode.addProperty("lookupCompanyUid_lookupLabel",item.getValue().toString());
                                    }
                                    thisReportNode.addProperty("activityDescription", result.getActivityDescription());
                                    Double netCost = 0.0;
                                    if (dayCost != null && result.getActivityDuration() != null && totalActivityDuration > 0.0) {
                                    	netCost = (dayCost * result.getActivityDuration()) / totalActivityDuration;
                                    }
                                    thisReportNode.addProperty("netCost", netCost.toString());
                                    isContinuous = false;
                                }
                            }
                        }
                    } else {
                        if (isContinuous) {
                            //add cumulative data into xml
                            ReportDataNode thisReportNode = reportDataNode.addChild("ActivityNPT");
                            thisReportNode.addProperty("startDateTime", initialStartDateTime);
                            thisReportNode.addProperty("endDateTime", lastEndDateTime);
                            thisReportNode.addProperty("activityDuration", cumDuration.toString());
                            thisReportNode.addProperty("uomDuration", uomDuration);
                            thisReportNode.addProperty("regulatorySubcode", nptCategory);
                            thisReportNode.addProperty("regulatoryCode", nptType);
                            thisReportNode.addProperty("lookupCompanyUid", vendor);
                            LookupItem item = this.companyList.get(vendor);
                            if (item != null) {
                                thisReportNode.addProperty("lookupCompanyUid_lookupLabel",item.getValue().toString());
                            }
                            thisReportNode.addProperty("activityDescription", "");
                             
                            Double netCost = 0.0;
                            if (dayCost != null && cumDuration != null && totalActivityDuration > 0.0) {
                                netCost = (dayCost * cumDuration) / totalActivityDuration;
                            }
                            thisReportNode.addProperty("netCost", netCost.toString());
                            isContinuous = false;
                        } else {
                            ReportDataNode thisReportNode = reportDataNode.addChild("ActivityNPT");
                            thisReportNode.addProperty("startDateTime", result.getStartDatetime().toString());
                            thisReportNode.addProperty("endDateTime", result.getEndDatetime().toString());
                            thisReportNode.addProperty("activityDuration", result.getActivityDuration().toString());
                            thisReportNode.addProperty("uomDuration", uomDuration);
                            thisReportNode.addProperty("regulatorySubcode", nptCategory);
                            thisReportNode.addProperty("regulatoryCode", nptType);
                            thisReportNode.addProperty("lookupCompanyUid", vendor);
                            LookupItem item = this.companyList.get(vendor);
                            if (item != null) {
                                thisReportNode.addProperty("lookupCompanyUid_lookupLabel",item.getValue().toString());
                            }
                            thisReportNode.addProperty("activityDescription", result.getActivityDescription());
                            Double netCost = 0.0;
                            if (dayCost != null && result.getActivityDuration() != null && totalActivityDuration > 0.0) {
                                netCost = (dayCost * result.getActivityDuration()) / totalActivityDuration;
                            }
                            thisReportNode.addProperty("netCost", netCost.toString());
                        }
                    }
                }
            } else {
                Activity result = (Activity) lstResult.get(0);
                String nptCategory = result.getRegulatorySubcode();
                String nptType = result.getRegulatoryCode();
                String vendor = result.getLookupCompanyUid();
                ReportDataNode thisReportNode = reportDataNode.addChild("ActivityNPT");
                thisReportNode.addProperty("startDateTime", result.getStartDatetime().toString());
                thisReportNode.addProperty("endDateTime", result.getEndDatetime().toString());
                thisReportNode.addProperty("activityDuration", result.getActivityDuration().toString());
                thisReportNode.addProperty("uomDuration", uomDuration);
                thisReportNode.addProperty("regulatorySubcode", nptCategory);
                thisReportNode.addProperty("regulatoryCode", nptType);
                thisReportNode.addProperty("lookupCompanyUid", vendor);
                LookupItem item = this.companyList.get(vendor);
                if (item != null) {
                    thisReportNode.addProperty("lookupCompanyUid_lookupLabel",item.getValue().toString());
                }
                thisReportNode.addProperty("activityDescription", result.getActivityDescription());
                Double netCost = 0.0;
                if (dayCost != null && result.getActivityDuration() != null && totalActivityDuration > 0.0) {
                    netCost = (dayCost * result.getActivityDuration()) / totalActivityDuration;
                }
                thisReportNode.addProperty("netCost", netCost.toString());
            }
        }
    }
    
    public void setCompanyList(Map<String,LookupItem> companyList) {
        this.companyList = companyList;
    }

    public Map<String,LookupItem> getCompanyList() {
        return companyList;
    }

}
