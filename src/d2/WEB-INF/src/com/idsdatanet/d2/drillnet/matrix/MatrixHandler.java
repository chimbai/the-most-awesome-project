package com.idsdatanet.d2.drillnet.matrix;

import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
/**
 * An interface for developer to write their own logic to generate matrix data
 * @author RYAU
 *
 */
public interface MatrixHandler {
	public void process(Matrix matrix, UserSelectionSnapshot userSelection) throws Exception;
}
