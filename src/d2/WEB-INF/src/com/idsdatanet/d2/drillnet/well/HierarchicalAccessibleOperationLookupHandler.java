package com.idsdatanet.d2.drillnet.well;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupProviderForLazyLoading;
import com.idsdatanet.d2.core.lookup.LazyLoadingRequest;
import com.idsdatanet.d2.core.lookup.LazyLoadingResult;
import com.idsdatanet.d2.core.lookup.LookupResolverForLazyLoading;
import com.idsdatanet.d2.core.lookup.LookupResolverForLookupMap;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.D2UserSelection;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;

/**
 * 
 * @author zjong
 * 
 */
public class HierarchicalAccessibleOperationLookupHandler implements LookupHandler, LookupProviderForLazyLoading {
	private static class WellboreInfo{
		final String wellboreUid;
		final String wellboreName;
		final String wellName;
		final String parentWellboreUid;
		
		WellboreInfo(String wellboreUid, String wellboreName, String wellName, String parentWellboreUid){
			this.wellboreUid = wellboreUid;
			this.wellboreName = wellboreName;
			this.wellName = wellName;
			this.parentWellboreUid = parentWellboreUid;
		}
	}
	
	private class OperationInfo{
		private final String operationUid;
		private final String operationName;
		private final String wellboreUid;
		
		OperationInfo(String operationUid, String operationName, String wellboreUid){
			this.operationUid = operationUid;
			this.operationName = operationName;
			this.wellboreUid = wellboreUid;
		}

		OperationInfo(_Operation operation){
			this.operationUid = operation.getOperationUid();
			this.operationName = operation.getOperationName();
			this.wellboreUid = operation.getWellboreUid();
		}
		
		String getOperationUid() {
			return this.operationUid;
		}
		
		String getOperationName() {
			return this.operationName;
		}
		
		String getWellboreUid() {
			return this.wellboreUid;
		}
	}
	
	private static class WellAndWellboreData{
		final Map<String,WellboreInfo> map = new HashMap<String,WellboreInfo>();
		
		static WellAndWellboreData loadByGroupUid(String groupUid) throws Exception {
			WellAndWellboreData instance = new WellAndWellboreData();
			instance.load(ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select WB.wellboreUid, WB.wellboreName, W.wellName, WB.parentWellboreUid from Wellbore WB, Well W where (WB.isDeleted = false or WB.isDeleted is null) and (W.isDeleted = false or W.isDeleted is null) and W.wellUid = WB.wellUid and WB.groupUid = :groupUid", "groupUid", groupUid));
			return instance;
		}
		
		@SuppressWarnings("rawtypes")
		static WellAndWellboreData loadByOperationUidList(List operationUidList) throws Exception {
			WellAndWellboreData instance = new WellAndWellboreData();
			instance.load(ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select WB.wellboreUid, WB.wellboreName, W.wellName, WB.parentWellboreUid from Wellbore WB, Well W where (WB.isDeleted = false or WB.isDeleted is null) and (W.isDeleted = false or W.isDeleted is null) and W.wellUid = WB.wellUid and W.wellUid in (select wellUid from Operation where operationUid in (:keys))", "keys", operationUidList));
			return instance;
		}
		
		WellboreInfo get(String wellboreUid) {
			return this.map.get(wellboreUid);
		}
		
		@SuppressWarnings("rawtypes")
		private void load(List list) {
			for(Object item: list) {
				Object[] data = (Object[]) item;
				WellboreInfo wellboreInfo = new WellboreInfo((String) data[0], (String) data[1], (String) data[2], (String) data[3]);
				this.map.put(wellboreInfo.wellboreUid, wellboreInfo);
			}
		}
	}
	
	private class LookupItemHolder implements Comparable<LookupItemHolder>{
		final String compareName;
		final LookupItem lookupItem;
		
		LookupItemHolder(String compareName, LookupItem lookupItem){
			this.compareName = compareName;
			this.lookupItem = lookupItem;
		}

		@Override
		public int compareTo(LookupItemHolder o) {
			return this.compareName.compareTo(o.compareName);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private class OperationsIterator implements Iterator<OperationInfo>{
		private final List data;
		private int index = 0;
		
		OperationsIterator(String groupUid) throws Exception{
			this.data = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select operationUid, operationName, wellboreUid from Operation where (isDeleted = false or isDeleted is null) and groupUid = :groupUid", "groupUid", groupUid);
		}
		
		@Override
		public boolean hasNext() {
			return this.index < data.size();
		}

		@Override
		public OperationInfo next() {
			Object[] rec = (Object[]) this.data.get(this.index++);
			return new OperationInfo((String) rec[0], (String) rec[1], (String) rec[2]);
		}
	}
	
	private class AccessibleOperationsIterator implements Iterator<OperationInfo>{
		private final Iterator<_Operation> operations;
		
		AccessibleOperationsIterator(Operations operations) {
			this.operations = operations.values().iterator();
		}
		
		@Override
		public boolean hasNext() {
			return this.operations.hasNext();
		}

		@Override
		public OperationInfo next() {
			return new OperationInfo(this.operations.next());
		}
	}
	
	private class SelectedAccessibleOperationsIterator implements Iterator<OperationInfo>{
		private final List<Object> keys;
		private final Operations operations;
		private int index = 0;
		
		SelectedAccessibleOperationsIterator(List<Object> keys, Operations operations){
			this.keys = keys;
			this.operations = operations;
		}
		
		@Override
		public boolean hasNext() {
			return this.index < this.keys.size();
		}

		@Override
		public OperationInfo next() {
			String key = (String) this.keys.get(this.index++);
			_Operation value = this.operations.get(key);
			if(value == null) {
				return null;
			}else {
				return new OperationInfo(value);
			}
		}
	}
	
	private int nameMaxLength = 30;
	private int lazyLoadingPageSize = 500;
	
	public void setLazyLoadingPageSize(int value) {
		this.lazyLoadingPageSize = value;
	}
	
	public void setNameMaxLength(int nameMaxLength) {
		this.nameMaxLength = nameMaxLength;
	}

	@Override
	public LazyLoadingResult getLookupForLazyLoading(URI uri, HttpServletRequest request) throws Exception {
		LazyLoadingResult result = new LazyLoadingResult();
		UserSession userSession = UserSession.getInstance(request);
		loadLookup(this.getOperations(request, userSession.getCurrentGroupUid()), WellAndWellboreData.loadByGroupUid(userSession.getCurrentGroupUid()), new LazyLoadingRequest(request), result, true);
		return result;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		if (request == null) { // AbstractSTTEngine may call this method with a null request, in that case, we just return an empty map
			return new HashMap<String,LookupItem>();
		}else {
			return loadLookup(this.getOperations(request, userSelection.getGroupUid()), WellAndWellboreData.loadByGroupUid(userSelection.getGroupUid()), null, null, true);
		}
	}
	
	private Map<String, LookupItem> loadLookup(Iterator<OperationInfo> operations, WellAndWellboreData wellAndWellboreData, LazyLoadingRequest lazyLoadingRequest, LazyLoadingResult lazyLoadingResult, boolean sort) throws Exception {
		List<LookupItemHolder> lookupItems = new ArrayList<LookupItemHolder>();
		String search = lazyLoadingRequest != null && StringUtils.isNotBlank(lazyLoadingRequest.getSearch()) ? lazyLoadingRequest.getSearch() : null;
		while(operations.hasNext()) {
			OperationInfo operation = operations.next();
			if(operation != null) {
				WellboreInfo wellboreInfo = wellAndWellboreData.get(operation.getWellboreUid());
				if(wellboreInfo != null) {
					String wellName = wellboreInfo.wellName;
					String hierarchicalWellbore = getCascadeParentWellboreName(operation.getWellboreUid(), "", MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES, wellAndWellboreData, 0);
					hierarchicalWellbore = hierarchicalWellbore.substring(0, hierarchicalWellbore.lastIndexOf(MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES));
					String operationName = operation.getOperationName();
					String label = operationName;
					if(! StringUtils.equals(operationName.trim(), hierarchicalWellbore.trim())) {
						String wellboreOperationName = hierarchicalWellbore + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + operationName;
						if(! StringUtils.equals(wellName.trim(), hierarchicalWellbore.trim())) {
							label = wellName + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + wellboreOperationName;
						} else {
							label = wellboreOperationName;
						}
					} else {
						if(! StringUtils.equals(wellName.trim(), hierarchicalWellbore.trim())) {
							label = wellName + MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES + operationName;
						}
					}
					String text = joinString(label, MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES);
					if(search == null || text.indexOf(search) != -1) {
						lookupItems.add(new LookupItemHolder(sort ? WellNameUtil.getPaddedStr(text) : null, new LookupItem(operation.getOperationUid(), text)));
					}
				}
			}
		}
		
		if(sort) {
			Collections.sort(lookupItems);
		}
		
		int start = lazyLoadingRequest != null && this.lazyLoadingPageSize > 0 ? lazyLoadingRequest.getPageNo() * this.lazyLoadingPageSize : 0;
		int end = lazyLoadingRequest != null && this.lazyLoadingPageSize > 0 ? Math.min(start + this.lazyLoadingPageSize, lookupItems.size()) : lookupItems.size();
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		for (int i=start; i < end; ++i) {
			LookupItem lookupItem = lookupItems.get(i).lookupItem;
			result.put(lookupItem.getKey(), lookupItem);
		}
		
		if(lazyLoadingResult != null) {
			lazyLoadingResult.setHasMore(lookupItems.size() > end);
			lazyLoadingResult.setLookup(result);
		}
		
		return result;
	}

	protected boolean onlyReturnAccessibleOperations() {
		return true;
	}

	private Iterator<OperationInfo> getOperations(HttpServletRequest request, String groupUid) throws Exception{
		if(this.onlyReturnAccessibleOperations()) {
			return new AccessibleOperationsIterator(UserSession.getInstance(request).getCachedAllAccessibleOperations());
		}else {
			return new OperationsIterator(groupUid);
		}
	}
	
	private static String null2EmptyString(String value){
		if(value == null) return "";
		return value;
	}

	private static String getCascadeParentWellboreName(String wellboreUid, String name, String delim, WellAndWellboreData wellAndWellboreData, int depth) {
		if (depth > 5) {
			return "..." + delim + null2EmptyString(name);
		}
		WellboreInfo wb = wellAndWellboreData.get(wellboreUid);
		if(wb == null) return name;
		return getCascadeParentWellboreName(wb.parentWellboreUid, null2EmptyString(wb.wellboreName) + delim + null2EmptyString(name), delim, wellAndWellboreData, depth + 1);
	}
	
	/**
	 * Merge the same occurance of well, wellbore and operation name in a given input
	 * @param input
	 * @param delim
	 * @return
	 */
	private String joinString(String input, String delim) {
		if(input.indexOf(delim) != -1) {
			String[] values = input.split(delim);
			if(values.length > 1) {
				String pivot = values[0];
				StringBuilder out = new StringBuilder(getShortName(pivot));
				for(int i=1; i < values.length; i++) {
					if(! StringUtils.equals(pivot.trim(), values[i].trim())) {
						out.append(delim + getShortName(values[i]));
						pivot = values[i];
					}
				}
				return out.toString();
			} else {
				return input;
			}
		} else {
			return input;
		}
	}
	
	private String getShortName(String name) {
		if (name.length() > nameMaxLength) {
			int midLength = nameMaxLength / 2;
			return name.substring(0, midLength - 3) + "..." + name.substring(name.length() - midLength);
		}
		return name;
	}

	@Override
	public LookupResolverForLazyLoading getLookupResolverForLazyLoading(List<Object> keys, D2UserSelection userSelection, HttpServletRequest request, String lookupHandlerUri) throws Exception {
		UserSession userSession = UserSession.getInstance(request);
		return new LookupResolverForLookupMap(loadLookup(new SelectedAccessibleOperationsIterator(keys, userSession.getCachedAllAccessibleOperations()), WellAndWellboreData.loadByOperationUidList(keys), null, null, false));
	}
}
