package com.idsdatanet.d2.drillnet.report.schedule.corportatedrillingportal;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.web.context.ServletContextAware;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.report.schedule.EmailConfiguration;

public abstract class BaseDataTransferService extends DefaultBeanSupport  implements ServletContextAware, InitializingBean{
	private String generateReportUserName;
	
	private String supportEmail;
	private Boolean enabled = false;
	private Map<String,EmailConfiguration> emailConfigurations;
	public Map<String, EmailConfiguration> getEmailConfigurations() {
		return emailConfigurations;
	}
	public void setEmailConfigurations(
			Map<String, EmailConfiguration> emailConfigurations) {
		this.emailConfigurations = emailConfigurations;
	}
	private MailEngine mailEngine=null;
	private String emailDistributionListKey = null;
	private String webRootPath;
	private boolean opTeamBased=false;
	private Log logger = LogFactory.getLog(this.getClass());
	private String xmlPath;
	private String xmlPathAbsolutePath;
	public String getXmlPathAbsolutePath() {
		return xmlPathAbsolutePath;
	}



	public void setXmlPathAbsolutePath(String xmlPathAbsolutePath) {
		this.xmlPathAbsolutePath = xmlPathAbsolutePath;
	}



	public Log getLogger()
	{
		return this.logger;
	}
	
	protected EmailConfiguration getValidEmailConfiguration(){
		return this.getEmailConfigurationByType(false, true);
	}
	
	protected EmailConfiguration getErrorEmailConfiguration(){
		return this.getEmailConfigurationByType(true, false);
	}
	
	private EmailConfiguration getEmailConfigurationByType(boolean isError, boolean isValid)
	{
		for (Iterator iter = this.getEmailConfigurations().entrySet().iterator(); iter.hasNext();) 
		{
			Map.Entry entry = (Map.Entry) iter.next();
			EmailConfiguration emailConfiguration=(EmailConfiguration) entry.getValue();
			if (emailConfiguration.getErrorEmail().equals(isError) && emailConfiguration.getValidEmail().equals(isValid))
			{
				return emailConfiguration;
			}
		}
		return null;
	}
	
	protected UserSelectionSnapshot getUserSelection() throws Exception
	{
		return UserSelectionSnapshot.getInstanceForCustomLogin(this.generateReportUserName);
	}
	
	protected String[] getEmailListWithSupport() throws Exception
	{
		return this.getEmailListWithSupport(null);
	}
	
	protected String[] getEmailListWithSupport(String distributionKey) throws Exception
	{
		String[] emailList=this.getEmailList(distributionKey);
		String[] newList=new String[emailList.length+1];
		for(int a=0;a<emailList.length;a++)
		{
			newList[a]=emailList[a];
		}	
		newList[newList.length-1]=this.getSupportEmail();
		return newList;
	}
	
	protected String[] getEmailList() throws Exception
	{
		return this.getEmailList(null);
	}
	protected String[] getEmailList(String distributionKey) throws Exception
	{
		return ApplicationUtils.getConfiguredInstance().getCachedEmailList(this.getUserSelection().getGroupUid(), distributionKey!=null?distributionKey:this.getEmailDistributionListKey());

	}
	
	public void sendEmail(EmailConfiguration emailConfig) throws Exception
	{
		if(emailConfig == null) return;
		if(emailConfig.getEmailList()==null || (emailConfig.getEmailList()!=null && emailConfig.getEmailList().length==0)){
			throw new Exception("Unable to send mail because email list is empty");
		}
		if(StringUtils.isBlank(emailConfig.getTemplateFile())){
			throw new Exception("Unable to send mail because email template not assigned");
		}
		if(this.mailEngine == null){
			throw new Exception("Unable to send mail because MailEngine is null");
		}
		
		if(emailConfig.getAttachment()!=null && ! isFilesExist(emailConfig.getAttachment())){
			throw new Exception("Unable to send mail because physical report file not found");
		}
		this.mailEngine.sendMail(emailConfig.getEmailList(), this.getSupportEmail(), ApplicationConfig.getConfiguredInstance().getSupportEmail(),
				emailConfig.getSubject(), this.mailEngine.generateContentFromTemplate(emailConfig.getTemplateFile(), emailConfig.getContentMapping()), emailConfig.getAttachment()==null?null:this.getInputStreamSourceFromFile(emailConfig.getAttachment()));
	}
	
	private Map<String, InputStreamSource> getInputStreamSourceFromFile(
			Map<String, File> attachment) {
		// TODO Auto-generated method stub
		Map<String, InputStreamSource> map=new HashMap<String, InputStreamSource>();
		for (Iterator iter = attachment.entrySet().iterator(); iter.hasNext();) 
		{
			Map.Entry entry = (Map.Entry) iter.next();
			File entryFile=(File)entry.getValue();
			map.put((String) entry.getKey(), new FileSystemResource(entryFile));
		}
		return map;
	}



	private boolean isFilesExist(Map<String,File> attachment)
	{
		for (Iterator iter = attachment.entrySet().iterator(); iter.hasNext();) 
		{
			Map.Entry entry = (Map.Entry) iter.next();
			File entryFile=(File)entry.getValue();
			if (!entryFile.exists())
				return false;
		}
		return true;
	}
	
	public void setSupportEmail(String supportEmail) {
		this.supportEmail = supportEmail;
	}
	public String getSupportEmail() {
		return supportEmail;
	}
	public void setEmailDistributionListKey(String emailDistributionListKey) {
		this.emailDistributionListKey = emailDistributionListKey;
	}
	public String getEmailDistributionListKey() {
		return emailDistributionListKey;
	}

	public void setServletContext(ServletContext servletContext) {
		this.setWebRootPath(servletContext.getRealPath("/"));
		
	}
	
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		if (this.supportEmail==null)
			this.supportEmail=ApplicationConfig.getConfiguredInstance().getSupportEmail();
		if (StringUtils.isEmpty(this.supportEmail) && this.getEnabled())
			throw new Exception("[D2] ["+this.getClass().getSimpleName()+"] Error Support Email Not Configured");
		if(this.xmlPathAbsolutePath != null && this.xmlPath == null){
			this.xmlPath = this.xmlPathAbsolutePath;
		}else{
			this.xmlPath = joinPath(this.webRootPath, this.xmlPath);
		}
		File path=new File(this.xmlPath);
		path.mkdirs();
	}

	protected String getCompleteOperationName(Operation operation) throws Exception
	{
		return CommonUtil.getConfiguredInstance().getCompleteOperationName(operation.getGroupUid(), operation.getOperationUid());
	}
	
	protected Map<String, Object> convertListToMap(List<Operation> oplist) throws Exception
	{
		return this.convertListToMap(oplist,null,null);
	}

	protected Map<String, Object> convertListToMap(List<Operation> oplist, Map<String,Daily> dailyList, String dateFormat) throws Exception
	{
		
		Map<String, Object> map=new HashMap<String,Object>();
		for (Operation op:oplist)
		{
			String completeOperationName=this.getCompleteOperationName(op);
			if (dailyList!=null)
			{
				Daily selectedDay = dailyList.get(op.getOperationUid());
				if (selectedDay !=null)
					completeOperationName +=" - "+this.formatDate(selectedDay.getDayDate(), dateFormat);
			}
			map.put(completeOperationName,completeOperationName);
		}
		return map;
	}

	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}



	public MailEngine getMailEngine() {
		return mailEngine;
	}

	public void setOpTeamBased(boolean opTeamBased) {
		this.opTeamBased = opTeamBased;
	}

	public boolean isOpTeamBased() {
		return opTeamBased;
	}
	public String getGenerateReportUserName() {
		return generateReportUserName;
	}



	public void setGenerateReportUserName(String generateReportUserName) {
		this.generateReportUserName = generateReportUserName;
	}


	public void setWebRootPath(String webRootPath) {
		this.webRootPath = webRootPath;
	}



	public String getWebRootPath() {
		return webRootPath;
	}



	public void setXmlPath(String xmlPath) {
		this.xmlPath = xmlPath;
	}



	public String getXmlPath() {
		return xmlPath;
	}
	
	private String joinPath(String path1, String path2){
		if(StringUtils.isBlank(path2)){
			return path1;
		}
		
		if("/".equals(path2) || "\\".equals(path2)){
			return path1;
		}
		
		if(path1.endsWith("/") || path1.endsWith("\\")){
			path1 = path1.substring(0, path1.length() - 1);
		}
		if(path2.startsWith("/") || path2.startsWith("\\")){
			path2 = path2.substring(1);
		}
		return path1 + getDirSeparator() + path2;
	}
	private static String getDirSeparator(){
		return System.getProperty("file.separator");
	}
	protected String replaceDateTimePattern(Date date,String text, String pattern,String dateFormat)
	{
		if (text.contains(pattern))
			return text.replace(pattern, formatDate(date,dateFormat));
		return text;
	}
	protected String formatDate(Date date, String format)
	{
		DateFormat df=new SimpleDateFormat(format);
		return df.format(date);
	}
	protected String getFileExtension(String file){
		int i = file.lastIndexOf(".");
		if(i != -1){
			return file.substring(i + 1);
		}else{
			return null;
		}
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public Boolean getEnabled() {
		return enabled;
	}
}
