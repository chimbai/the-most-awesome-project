package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.core.model.DepotWellOperationMapping;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.core.DepotConstants;
import com.idsdatanet.depot.core.mapping.DepotMappingModule;
import com.idsdatanet.depot.core.mapping.MappingAggregator;
import com.idsdatanet.depot.core.query.QueryResult;
import com.idsdatanet.depot.core.util.DepotUtils;
import com.idsdatanet.depot.edm.EDMManager;
import com.idsdatanet.depot.edm.util.EdmScreenUtils;

import net.sf.json.JSONObject;

public class DvdPlanMasterCommandBeanListener extends EmptyDataNodeListener implements CommandBeanListener {
	
	public final static String EDM_DVDPLAN_IMPORT = "edm_dvdplan_import";
	
	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode targetCommandBeanTreeNode, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		}

	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
		if (commandBean.getRoot().getList() != null) {
			int countRec = 0;
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("OperationPlanMaster").values()) {
				if(!node.getAtts().getAction().equals(Action.DELETE) && !node.getAtts().getAction().equals(Action.CANCEL)) {
					countRec++;
				}
			}
			if (countRec > 1) {
				commandBean.getRoot().setDirty(true);
				commandBean.getSystemMessage().addError("Only up to 1 record is allowed for DVD Plan" ,true, true);
				commandBean.setAbortFormProcessing(true);
			}
		}
	}
	
	@Override
	public void init(CommandBean commandBean) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
			
				EdmScreenUtils.checkForEdmSyncJob(commandBean);
				
				if (invocationKey.equals(EDM_DVDPLAN_IMPORT) && commandBean.getSystemMessage().getTotalMessages() == 0) {
					
					JSONObject responseOutput = new JSONObject();
					boolean success = true;
					
						// 1. get EDM DWOM Mapping
						// 2. use depotMappingModule to query EDM object.
					
						UserSession session = UserSession.getInstance(request);

						String sql ="FROM DepotWellOperationMapping WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND wellUid=:wellUid AND wellboreUid=:wellboreUid AND operationUid=:operationUid AND edmPolicyUid IS NOT NULL AND edmProjectUid IS NOT NULL AND edmSiteUid IS NOT NULL";
						
						List<?> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"wellUid", "wellboreUid", "operationUid"}, new Object[] {session.getCurrentWellUid(), session.getCurrentWellboreUid(), session.getCurrentOperationUid()});
						if(result != null && result.size() > 0){
						
						try {
							DepotWellOperationMapping dwom = (DepotWellOperationMapping)result.get(0); 
							String edmWellUid = dwom.getDepotWellUid();
							String edmWellboreUid = dwom.getDepotWellboreUid();
							String edmEventId = dwom.getDepotOperationUid();

							MappingAggregator mappingAggregator = DepotMappingModule.getConfiguredInstance().getMappingAggregator(DepotConstants.EDM, "import_scenario_well_plan");
							String dataObjectId = mappingAggregator.getDataObjectId();
							if (mappingAggregator!=null) {
								QueryResult queryResult = EDMManager.getConfiguredInstance().retrieveDataObjectAndSave(dataObjectId, edmWellUid, edmWellboreUid, edmEventId);
								if (!queryResult.isSuccessful()) {
									success = false;
									responseOutput.element("error", queryResult.getAllErrorMessages());
								}
							}
						}catch (Exception e) {
							success = false;
							responseOutput.element("error", DepotUtils.getStackTrace(e));
						}
						finally {
							if(success) {
							responseOutput.element("success", success);
							response.getWriter().write(responseOutput.toString());
							commandBean.getSystemMessage().addInfo("EDM data is import completed.", true);
							}else {
								commandBean.getSystemMessage().addError("Failed to import dvd plan data.", true);
							}
						}
				}else {
					commandBean.getSystemMessage().addError("This Well/Ops has not been mapped to EDM. Please map the Well/Ops in Wellbore Operation Mapping screen.", true);
				}
			}
	}
}
		