package com.idsdatanet.d2.drillnet.wellExternalCatalog;

import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.model.Group;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.WellExternalCatalog;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.depot.core.query.QueryResult;
import com.idsdatanet.depot.core.util.DepotHttpClient;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
	
public class OMVHipWellExternalCatalogManager extends DefaultBeanSupport implements InitializingBean  {
	public static final String EXTERNAL_WELL_CATALOG = "EXT_WELL_CATALOG";
	public static final String DEPOT_VERSION = "dlcwellheader";
	private static final String tokenUrl = "https://login.microsoftonline.com/a8f2ac6f-681f-4361-b51f-c85d86014a17/oauth2/v2.0/token";
	private static final String scope = "https://api.omv.com/partner/prodtechnology/.default";
	private static final String subscriptionKey = "Ocp-Apim-Subscription-Key";
	private static final String subscriptionValue = "36f91d27d0744e6980795af6e6f07c73";
	private boolean isRunning = false;
	private long startTime, endTime;
	private JSONObject wellsResponse, wellboresResponse;
	private JSONArray arrWells, arrWellbores;
	
	public static OMVHipWellExternalCatalogManager configuredInstance = null;
	
	public static OMVHipWellExternalCatalogManager getConfiguredInstance() throws Exception{
		if (configuredInstance == null) configuredInstance = getSingletonInstance(OMVHipWellExternalCatalogManager.class);
		return configuredInstance;

	}
	
	public void run() throws Exception {
		startTime = System.currentTimeMillis();
		ImportExportServerProperties serverProperties = this.getAgentServerProperties();
		String miscConfig = null;
		if (serverProperties != null) {
			if(!this.isRunning) {
				try {
					this.isRunning = true;
					
					String hql = "SELECT miscConfig FROM SchedulerJobDetail WHERE (isDeleted is null or isDeleted=0) AND serverPropertiesUid=:serverPropertiesUid";
					List<?> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "serverPropertiesUid", serverProperties.getServerPropertiesUid());
					if (result != null && result.size() > 0) {
						miscConfig = result.toString().replace("[", "").replace("]", "");
					}
					
					// getWells will query wells from OMV HIP getWells API and continue with getBorehole API
					// Once process is done, saveOrUpdateWellWellbore will be called to check API response against WellExternalCatalog records in D2 - Create new or update if exist
					getWells(serverProperties, miscConfig);
					endTime = System.currentTimeMillis();
					System.out.println("Save/updated records in " + (endTime - startTime) + " milliseconds");
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					this.isRunning = false;
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void saveOrUpdateWellWellbore(ImportExportServerProperties serverProperties) throws Exception {
		String groupUid = this.getGroupUid();
		String latNs, longEw;
		double latitude, latDeg, latMinute, latSecond, longitude, longDeg, longMinute, longSecond;
		String hql = "FROM WellExternalCatalog WHERE (isDeleted is null or isDeleted=0) AND importExportServerPropertiesUid =: importExportServerPropertiesUid";
		List<WellExternalCatalog> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, "importExportServerPropertiesUid", serverProperties.getServerPropertiesUid());
		
		for (int i = 0; i < arrWells.size(); i++) {
			for (int j = 0; j < arrWellbores.size(); j++) {
				if (arrWellbores.getJSONObject(j).getString("UWI").equals(arrWells.getJSONObject(i).getString("UWI"))) { 
					try {
						WellExternalCatalog wec = null;
						if (result.size() > 0 ) {
							for (WellExternalCatalog wecFromDb : result) {
								if (wecFromDb.getUwi().equals(arrWells.getJSONObject(i).getString("UWI"))) {
									wec = wecFromDb;
									break;
								}
							}
							
							if (wec == null) {
								wec = new WellExternalCatalog();
							}
						} else {
							wec = new WellExternalCatalog();
						}
			        	wec.setGroupUid(groupUid);
			        	wec.setWellName(arrWells.getJSONObject(i).getString("WELL_NAME"));
			        	wec.setWellNamePadded(WellNameUtil.getPaddedStr(arrWells.getJSONObject(i).getString("WELL_NAME")));
			        	if (arrWells.getJSONObject(i).containsKey("GENERAL_FIELD_NAME")) {
			        		wec.setField(arrWells.getJSONObject(i).getString("GENERAL_FIELD_NAME"));
			        	}
			        	
			        	if (arrWells.getJSONObject(i).containsKey("SURFACE_LATITUDE_WGS84")) {
				        	latitude = Double.parseDouble(arrWells.getJSONObject(i).getString("SURFACE_LATITUDE_WGS84"));
							latNs = latitude > 0 ? "N" : "S";
							if(latitude < 0) { latitude = latitude * -1; }
							latDeg = (int) latitude;
							latMinute = (int) ((latitude - latDeg) * 60);
							latSecond = (latitude - latDeg - (latMinute / 60.00)) * 3600;
							wec.setLatNs(latNs);
							wec.setLatDeg(latDeg);
							wec.setLatMinute(latMinute);
							wec.setLatSecond(latSecond);
			        	}
						
			        	if (arrWells.getJSONObject(i).containsKey("SURFACE_LONGITUDE_WGS84")) {
							longitude = Double.parseDouble(arrWells.getJSONObject(i).getString("SURFACE_LONGITUDE_WGS84"));
							longEw = longitude > 0 ? "E" : "W";
							if(longitude < 0) { longitude = longitude * -1; }
							longDeg = (int) longitude;
							longMinute = (int) ((longitude - longDeg) * 60);
							longSecond = (longitude - longDeg - (longMinute / 60.00)) * 3600;
							wec.setLongEw(longEw);
							wec.setLongDeg(longDeg);
							wec.setLongMinute(longMinute);
							wec.setLongSecond(longSecond);
			        	}
			        	
			        	if (arrWells.getJSONObject(i).containsKey("ON_OFF_WELL")) {
			        		wec.setOnOffShore((arrWells.getJSONObject(i).getString("ON_OFF_WELL") == "N") ? "ON" : "OFF");
			        	}
			        	
			        	if (arrWells.getJSONObject(i).containsKey("COUNTRY_CODE")) {
			        		wec.setCountry(arrWells.getJSONObject(i).getString("COUNTRY_CODE"));
			        	}
			        	wec.setUwi(arrWells.getJSONObject(i).getString("UWI"));
			        	wec.setWellboreName(arrWellbores.getJSONObject(j).getString("BOREHOLE_NAME"));
			        	wec.setWellboreNamePadded(WellNameUtil.getPaddedStr(arrWellbores.getJSONObject(j).getString("BOREHOLE_NAME")));
			        	wec.setUwbi(arrWellbores.getJSONObject(j).getString("UWI"));
			        	wec.setImportExportServerPropertiesUid(serverProperties.getServerPropertiesUid());
			        	
			        	String sourceData = arrWells.getJSONObject(i).toString(1) + "\n" + arrWellbores.getJSONObject(j).toString(1);
			        	wec.setSourceData(sourceData);
			        	
			        	ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(wec);
					} catch (Exception e) {
						e.printStackTrace();
						throw new Exception("Object '" + arrWells.getJSONObject(i).getString("UWI") + "' has missing data");
					}
				}
			}
        }
    }
	
	public void getWells(ImportExportServerProperties serverProperties, String miscConfig) throws Exception {
		String url = serverProperties.getEndPoint();
		boolean continueQuery = true;
		int offset = 0;
		arrWells = new JSONArray();

		if (url.endsWith("dlc/")) {
			url = url + "wells";
		} else if (url.endsWith("dlc")) {
			url = url + "/wells";
		}
		
		if (miscConfig != null && miscConfig != "") {
			url = url + "?" + miscConfig;
		}

		while (continueQuery = true) {
			startTime = System.currentTimeMillis();
			String queryUrl = url;
			if (offset != 0) {
				queryUrl = url + "&offset=" + Integer.toString(offset);
			}
			DepotHttpClient httpClient = new DepotHttpClient(queryUrl);
			httpClient.addHeader(subscriptionKey, subscriptionValue);
			httpClient.addHeader("Authorization", this.getWellsLogin(serverProperties));
			QueryResult queryResult = new QueryResult();
			httpClient.invokeGet(queryResult);
			if (queryResult.isSuccessful()) {
				wellsResponse = JSONObject.fromObject(queryResult.getOutputString());
				JSONArray queriedWells =  wellsResponse.getJSONArray("records");
				
				arrWells.addAll(queriedWells);
				offset += 2000;
				endTime = System.currentTimeMillis();
				System.out.println("Received " + queriedWells.size() + " objects from getWells in " + (endTime - startTime) + " milliseconds");
				
				if (queriedWells.size() == 0) {
		        	continueQuery = false;
		        	endTime = System.currentTimeMillis();
		    		System.out.println("Finished receiving " + arrWells.size() + " objects from getWells in " + (endTime - startTime) + " milliseconds");
		        	getWellbores(serverProperties, miscConfig);
		        	break;
		        }
			} else {
				continueQuery = false;
				throw new Exception("Failed to query wells from OMV HIP");
			}
		}
	}
	
	public void getWellbores(ImportExportServerProperties serverProperties, String miscConfig) throws Exception {
		String url = serverProperties.getEndPoint();
		boolean continueQuery = true;
		int offset = 0;
		arrWellbores = new JSONArray();
		
		if (url.endsWith("dlc/")) {
			url = url + "borehole";
		} else if (url.endsWith("dlc")) {
			url = url + "/borehole";
		}
		
		if (miscConfig != null && miscConfig != "") {
			url = url + "?" + miscConfig;
		}
		
		while (continueQuery = true) {
			String queryUrl = url;
			if (offset != 0) {
				queryUrl = url + "&offset=" + Integer.toString(offset);
			}
			DepotHttpClient httpClient = new DepotHttpClient(queryUrl);
			httpClient.addHeader(subscriptionKey, subscriptionValue);
			httpClient.addHeader("Authorization", this.getBoreholeLogin(serverProperties));
			QueryResult queryResult = new QueryResult();
			httpClient.invokeGet(queryResult);
			if (queryResult.isSuccessful()) {
				wellboresResponse = JSONObject.fromObject(queryResult.getOutputString());
				JSONArray queriedWellbores =  wellboresResponse.getJSONArray("records"); 
				arrWellbores.addAll(queriedWellbores);
				offset += 2000;
				endTime = System.currentTimeMillis();
				System.out.println("Received " + queriedWellbores.size() + " objects from getBorehole in " + (endTime - startTime) + " milliseconds");
				
				if (queriedWellbores.size() == 0) { 
		        	continueQuery = false;
		        	endTime = System.currentTimeMillis();
		        	System.out.println("Finished receiving " + arrWellbores.size() + " objects from getBorehole in " + (endTime - startTime) + " milliseconds");
		        	saveOrUpdateWellWellbore(serverProperties);
		        	break;
		        }
			} else {
				continueQuery = false;
				throw new Exception("Failed to query wellbores from OMV HIP");
			}
		}
	}
	
	public String getWellsLogin(ImportExportServerProperties serverProperties) throws Exception {
		startTime = System.currentTimeMillis();
		DepotHttpClient httpClient = new DepotHttpClient(tokenUrl);
		httpClient.addParam("grant_type", "client_credentials");
		httpClient.addParam("client_id", serverProperties.getUsername());
		httpClient.addParam("client_secret", serverProperties.getPassword());
		httpClient.addParam("response_type", "id_token+code");
		httpClient.addParam("scope", scope);
		QueryResult queryResult = new QueryResult();
		httpClient.invokePost(queryResult);
		endTime = System.currentTimeMillis();
		System.out.println("getWells login time: " + (endTime - startTime) + " milliseconds");
		if (queryResult.isSuccessful()) {
			String result = queryResult.getOutputString();
			JSONObject jsonObject = JSONObject.fromObject(result);
			return "Bearer " + jsonObject.getString("access_token");
		} else {
			throw new Exception("Failed to login to OMV HIP endpoint.");
		}
	}
	
	public String getBoreholeLogin(ImportExportServerProperties serverProperties) throws Exception {
		startTime = System.currentTimeMillis();
		DepotHttpClient httpClient = new DepotHttpClient(tokenUrl);
		httpClient.addParam("grant_type", "client_credentials");
		httpClient.addParam("client_id", serverProperties.getUsername());
		httpClient.addParam("client_secret", serverProperties.getPassword());
		httpClient.addParam("response_type", "id_token+code");
		httpClient.addParam("scope", scope);
		QueryResult queryResult = new QueryResult();
		httpClient.invokePost(queryResult);
		endTime = System.currentTimeMillis();
		System.out.println("getBorehole login time: " + (endTime - startTime) + " milliseconds");
		if (queryResult.isSuccessful()) {
			String result = queryResult.getOutputString();
			JSONObject jsonObject = JSONObject.fromObject(result);
			return "Bearer " + jsonObject.getString("access_token");
		} else {
			throw new Exception("Failed to login to OMV HIP endpoint.");
		}
	}
	
	public ImportExportServerProperties getAgentServerProperties() throws Exception {
		String hql = "FROM ImportExportServerProperties WHERE (isDeleted is null or isDeleted=0) AND dataTransferType=:transferType AND depotVersion=:depotVersion";
		String[] paramFields = {"transferType", "depotVersion"};
		String[] paramValues = {EXTERNAL_WELL_CATALOG, DEPOT_VERSION};
		List<?> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, paramFields, paramValues);
		if (result != null && result.size() > 0) {
			return (ImportExportServerProperties)result.get(0);
		}
		return null;
	}
	
	private String getGroupUid() throws Exception {
		String hql = "from Group where (isDeleted is null or isDeleted=0) and parentGroupUid is null";
		List<?> result = ApplicationUtils.getConfiguredInstance().getDaoManager().find(hql);
		if (result != null && result.size() > 0) {
			Group group = (Group)result.get(0);
			return group.getGroupName();
		}
		return "UNKNOWN";
	}
	

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}

	
}
