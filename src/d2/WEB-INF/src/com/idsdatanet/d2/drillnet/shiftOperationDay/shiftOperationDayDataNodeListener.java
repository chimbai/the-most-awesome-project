package com.idsdatanet.d2.drillnet.shiftOperationDay;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;

import com.idsdatanet.d2.common.lookup.cleanup.LookupCleanupUtil;
import com.idsdatanet.d2.core.dao.hibernate.TableMapping;
import com.idsdatanet.d2.core.model.BhaComponent;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.SurveyReference;
import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.util.ClassUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc.multiselect.MultiSelect;

public class shiftOperationDayDataNodeListener extends EmptyDataNodeListener{
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if (obj instanceof Operation) {
			String currentOperationUid = session.getCurrentOperationUid();
			if (currentOperationUid == null) return;
			String targetOperationUid = (String) node.getDynaAttr().get("targetOperationUid");
			if (targetOperationUid == null) return;
			if ("".equals(targetOperationUid)) return;
			if ((targetOperationUid).equals(currentOperationUid)) return;
			
			Operation currentOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, currentOperationUid);
			Operation targetOperation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, targetOperationUid);
			if (currentOperation==null || targetOperation==null) return;
			
			String currentWellUid = currentOperation.getWellUid();
			String currentWellboreUid = currentOperation.getWellboreUid();
			String targetWellUid = targetOperation.getWellUid();
			String targetWellboreUid = targetOperation.getWellboreUid();
			
			MultiSelect multiSelect = node.getMultiSelect();
			String joinCondition = "";
			if (multiSelect!=null) {
				List<String> dailyUids = multiSelect.getValues().get("@dailyUids");
				if (dailyUids!=null) {
					for (String dailyUid : dailyUids) {
						if (!"".equals(joinCondition)) joinCondition += "','";
						joinCondition += dailyUid;
					}
				}
			}
			if (!"".equals(joinCondition)) {
				joinCondition = " dailyUid in ('" + joinCondition + "')";
				
				LookupCleanupUtil util = new LookupCleanupUtil();
				util.addTask("ShiftDay_operationUid", currentOperationUid, targetOperationUid, joinCondition);
				if (!currentWellboreUid.equals(targetWellboreUid)) util.addTask("ShiftDay_wellboreUid", currentWellboreUid, targetWellboreUid, joinCondition);
				if (!currentWellUid.equals(targetWellUid)) util.addTask("ShiftDay_wellUid", currentWellUid, targetWellUid, joinCondition);
				
				//update operationUid, wellboreUid, wellUid to all related tables
				//do looping to add general fields
				List<Class> allClasses = TableMapping.getTableModel();
				for (Class thisClass : allClasses) {
					String className = thisClass.getSimpleName();
					if (ClassUtils.isPropertyExists(thisClass, "dailyUid")){
						if (className.equals(BharunDailySummary.class.getSimpleName())) {
							this.processBharunForBharunDailySummary(currentOperation, targetOperation, joinCondition);
						} else if (className.equals(DrillingParameters.class.getSimpleName())) {
							this.processBharunForDrillingParameters(currentOperation, targetOperation, joinCondition);
						} else if (className.equals(SurveyStation.class.getSimpleName())) {
							this.processSurveyStation(currentOperation, targetOperation, joinCondition);
						}
						
						if (ClassUtils.isPropertyExists(thisClass, "wellUid") && !currentWellUid.equals(targetWellUid)){
							this.updateValue(className, "wellUid", currentWellUid, targetWellUid, joinCondition);
						}
						if (ClassUtils.isPropertyExists(thisClass, "wellboreUid") && !currentWellboreUid.equals(targetWellboreUid)){
							this.updateValue(className, "wellboreUid", currentWellboreUid, targetWellboreUid, joinCondition);
						}
						if (ClassUtils.isPropertyExists(thisClass, "operationUid")){
							this.updateValue(className, "operationUid", currentOperationUid, targetOperationUid, joinCondition);
						}
					}
				}
				
				this.checkBharun(currentOperation.getOperationUid());
			}
		}
	}
	
	private void processSurveyStation(Operation currentOperation, Operation targetOperation, String condition) throws Exception {
		if (!"".equals(condition)) {
			String strSql = "FROM SurveyStation WHERE (isDeleted=false or isDeleted is null) " +
					" AND " + condition + "";
			List<SurveyStation> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			for (SurveyStation rec : list) {
				String surveyReferenceUid = rec.getSurveyReferenceUid();
				String newSurveyReferenceUid = this.copySurveyReferenceToTargetOperation(currentOperation, targetOperation, surveyReferenceUid);
				rec.setSurveyReferenceUid(newSurveyReferenceUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
				
				strSql = "FROM SurveyStation WHERE (isDeleted=false or isDeleted is null) and surveyReferenceUid=:surveyReferenceUid";
				List list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "surveyReferenceUid", surveyReferenceUid);
				if (list2.size()<=0) {
					SurveyReference survey = (SurveyReference) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SurveyReference.class, surveyReferenceUid);
					survey.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(survey);
				}
			}
		}
	}
	
	private String copySurveyReferenceToTargetOperation(Operation currentOperation, Operation targetOperation, String surveyReferenceUid) throws Exception {
		if (surveyReferenceUid!=null) {
			SurveyReference survey = (SurveyReference) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SurveyReference.class, surveyReferenceUid);
			if (survey!=null) {
				String strSql = "FROM SurveyReference WHERE (isDeleted=false or isDeleted is null) " +
						" AND operationUid=:operationUid and isPlanned=:isPlanned";
				List<SurveyReference> surveyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, 
						new String[] {"operationUid", "isPlanned"}, 
						new Object[] {targetOperation.getOperationUid(), survey.getIsPlanned()});
				if (surveyList.size()>0) {
					survey = surveyList.get(0);
					return survey.getSurveyReferenceUid();
				} else {
					//create new
					SurveyReference newSurvey = new SurveyReference();
					//BeanUtils.copyProperties(newSurvey, survey);
					newSurvey = (SurveyReference) BeanUtils.cloneBean(survey);
					newSurvey.setSurveyReferenceUid(null);
					newSurvey.setWellUid(targetOperation.getWellUid());
					newSurvey.setWellboreUid(targetOperation.getWellboreUid());
					newSurvey.setOperationUid(targetOperation.getOperationUid());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newSurvey);
					return newSurvey.getSurveyReferenceUid();

				}
			}
		}
		return surveyReferenceUid;
	}
	
	private void processBharunForBharunDailySummary(Operation currentOperation, Operation targetOperation, String condition) throws Exception {
		if (!"".equals(condition)) {
			String strSql = "FROM BharunDailySummary WHERE (isDeleted=false or isDeleted is null) " +
					" AND " + condition + "";
			List<BharunDailySummary> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			for (BharunDailySummary rec : list) {
				String bharunUid = rec.getBharunUid();
				String newBharunUid = this.copyBharunToTargetOperation(currentOperation, targetOperation, bharunUid);
				rec.setBharunUid(newBharunUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
			}
		}
	}
	
	private void processBharunForDrillingParameters(Operation currentOperation, Operation targetOperation, String condition) throws Exception {
		if (!"".equals(condition)) {
			String strSql = "FROM DrillingParameters WHERE (isDeleted=false or isDeleted is null) " +
					" AND " + condition + "";
			List<DrillingParameters> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			for (DrillingParameters rec : list) {
				String bharunUid = rec.getBharunUid();
				String newBharunUid = this.copyBharunToTargetOperation(currentOperation, targetOperation, bharunUid);
				rec.setBharunUid(newBharunUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
			}
		}
	}
	
	private void checkBharun(String operationUid) throws Exception {
		String strSql = "FROM Bharun where (isDeleted=false or isDeleted is null) and operationUid=:operationUid";
		List<Bharun> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
		for (Bharun rec : list) {
			this.checkAndRemoveOldBharun(rec.getBharunUid());
		}
	}
	
	private void checkAndRemoveOldBharun(String bharunUid) throws Exception {
		Boolean allowRemove = true;
		//check bharun is still using in current well bharundailysummary, drilling parameters
		String strSql = "FROM BharunDailySummary WHERE (isDeleted=false or isDeleted is null) " +
				" AND bharunUid=:bharunUid";
		List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bharunUid", bharunUid);
		if (list.size()>0) return;
		
		strSql = "FROM DrillingParameters WHERE (isDeleted=false or isDeleted is null) " +
				" AND bharunUid=:bharunUid";
		list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bharunUid", bharunUid);
		if (list.size()>0) return;
		
		//if no longer use, remove it
		if (allowRemove) {
			//delete Bharun
			Bharun bharun = (Bharun) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Bharun.class, bharunUid);
			bharun.setIsDeleted(true);
			if (bharun!=null) ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharun);
			
			//delete BhaComponent
			strSql = "FROM BhaComponent WHERE (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid";
			List<BhaComponent> listBhaComponent = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bharunUid", bharunUid);
			for (BhaComponent rec : listBhaComponent) {
				rec.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
			}

			//delete Bitrun
			strSql = "FROM Bitrun WHERE (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid";
			List<Bitrun> listBitrun= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bharunUid", bharunUid);
			for (Bitrun rec : listBitrun) {
				String bitrunUid = rec.getBitrunUid();
				rec.setIsDeleted(true);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
				
				//delete bitNozzle
				strSql = "FROM BitNozzle WHERE (isDeleted=false or isDeleted is null) and bitrunUid=:bitrunUid";
				List<BitNozzle> listBitNozzle= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bitrunUid", bitrunUid);
				for (BitNozzle rec2 : listBitNozzle) {
					rec2.setIsDeleted(true);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec2);
				}
			}

		}
	}
	
	private String copyBharunToTargetOperation(Operation currentOperation, Operation targetOperation, String bharunUid) throws Exception {
		Bharun bharun = (Bharun) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Bharun.class, bharunUid);
		if (bharun==null) return bharunUid;
		String bharunNumber = bharun.getBhaRunNumber();
		String newBharunUid = bharunUid;
		
		String strSql = "FROM Bharun where (isDeleted=false or isDeleted is null) and operationUid=:operationUid and bhaRunNumber=:bhaRunNumber";
		String[] paramNames = {"operationUid", "bhaRunNumber"};
		Object[] values = {targetOperation.getOperationUid(), bharunNumber};
		List<Bharun> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, values);
		if (list.size()>0) {
			Bharun newBharun = list.get(0);
			newBharunUid = newBharun.getBharunUid();
		} else {
			//duplicate Bharun
			Bharun newBhaRun = new Bharun();
			//BeanUtils.copyProperties(newBhaRun, bharun);
			newBhaRun = (Bharun) BeanUtils.cloneBean(bharun);
			newBhaRun.setBharunUid(null);
			newBhaRun.setWellUid(targetOperation.getWellUid());
			newBhaRun.setWellboreUid(targetOperation.getWellboreUid());
			newBhaRun.setOperationUid(targetOperation.getOperationUid());
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newBhaRun);
			newBharunUid = newBhaRun.getBharunUid();
			
			//duplicate BhaComponent
			strSql = "FROM BhaComponent WHERE (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid";
			List<BhaComponent> listBhaComponent = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bharunUid", bharunUid);
			for (BhaComponent rec : listBhaComponent) {
				BhaComponent bhaComponent = new BhaComponent();
				//BeanUtils.copyProperties(bhaComponent, rec);
				bhaComponent = (BhaComponent) BeanUtils.cloneBean(rec);
				bhaComponent.setBhaComponentUid(null);
				bhaComponent.setWellUid(targetOperation.getWellUid());
				bhaComponent.setBharunUid(newBharunUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bhaComponent);
			}
			
			//duplicate Bitrun
			strSql = "FROM Bitrun WHERE (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid";
			List<Bitrun> listBitrun= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bharunUid", bharunUid);
			for (Bitrun rec : listBitrun) {
				Bitrun bitrun = new Bitrun();
				//BeanUtils.copyProperties(bitrun, rec);
				bitrun = (Bitrun) BeanUtils.cloneBean(rec);
				bitrun.setBitrunUid(null);
				bitrun.setWellUid(targetOperation.getWellUid());
				bitrun.setWellboreUid(targetOperation.getWellboreUid());
				bitrun.setOperationUid(targetOperation.getOperationUid());
				bitrun.setBharunUid(newBharunUid);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bitrun);
				String bitrunUid = bitrun.getBitrunUid();
				
				//duplicate bitNozzle
				strSql = "FROM BitNozzle WHERE (isDeleted=false or isDeleted is null) and bitrunUid=:bitrunUid";
				List<BitNozzle> listBitNozzle= ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bitrunUid", rec.getBitrunUid());
				for (BitNozzle rec2 : listBitNozzle) {
					BitNozzle bitNozzle = new BitNozzle();
					//BeanUtils.copyProperties(bitNozzle, rec2);
					bitNozzle = (BitNozzle) BeanUtils.cloneBean(rec2);
					bitNozzle.setBitrunUid(bitrunUid);
					bitNozzle.setBitNozzleUid(null);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bitNozzle);
				}				
			}
		}
		
		return newBharunUid;
	}
		
	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		ApplicationUtils.getConfiguredInstance().refreshCachedData();
		if (commandBean.getFlexClientAdaptor()!=null) commandBean.getFlexClientAdaptor().setReloadParentHtmlPage();
	}

	private void updateValue(String className, String fieldName, String oldValue, String newValue, String extraCond) {
		try {
			String sql = "update " + className + " set " + fieldName + "=:newValue where " + fieldName + "=:oldValue " + ("".equals(extraCond)?"":" AND " + extraCond) ;
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sql, new String[] {"newValue","oldValue"}, new String[] {newValue, oldValue});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
