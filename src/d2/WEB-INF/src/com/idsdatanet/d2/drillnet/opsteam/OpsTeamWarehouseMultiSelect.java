package com.idsdatanet.d2.drillnet.opsteam;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.multiselect.StandardTreeNodeMultiSelect;

public class OpsTeamWarehouseMultiSelect extends StandardTreeNodeMultiSelect {
	
	public List<String> loadMultiSelectKeys(CommandBeanTreeNode node) throws Exception {
		String parent_id = BeanUtils.getProperty(node.getData(), this.parentIdField);
		return this.daoManager.findByNamedParam("select " + this.childLookupField + " from " + this.childTableClass.getName() + " where (isDeleted = false or isDeleted is null) and " + this.childIdField + " = :parent_id ", new String[] {"parent_id"}, new Object[] {parent_id});
	}
	
	public void saveMultiSelectKeys(List<String> keys, CommandBeanTreeNode node, HttpServletRequest request) throws Exception {
		List<String> keys_in_db = this.loadMultiSelectKeys(node);
		String parent_id = BeanUtils.getProperty(node.getData(), this.parentIdField);
		
		for(String key: keys){
			if(StringUtils.isNotBlank(key) && (! keys_in_db.contains(key))){
				Object new_key = this.childTableClass.newInstance();
				BeanUtils.setProperty(new_key, this.childIdField, parent_id);
				BeanUtils.setProperty(new_key, this.childLookupField, key);	
				
				this.daoManager.saveObject(new_key);
			}
		}
		
		String[] params_name = {"lastEditDatetime", "child_id", "lookup_id"};
		Object[] params_value = {new Date(), parent_id, ""};
		
		for(String key: keys_in_db){
			if(! keys.contains(key)){
				params_value[2] = key; 
				this.daoManager.executeByNamedParam("update " + this.childTableClass.getName() + " set isDeleted = true, lastEditDatetime = :lastEditDatetime where " + this.childIdField + " = :child_id and " + this.childLookupField + " = :lookup_id", params_name, params_value);
			}
		}
	}

}
