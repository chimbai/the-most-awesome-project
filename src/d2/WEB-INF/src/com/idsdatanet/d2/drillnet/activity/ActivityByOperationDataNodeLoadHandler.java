package com.idsdatanet.d2.drillnet.activity;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ActivityByOperationDataNodeLoadHandler implements DataNodeLoadHandler {

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		if ("Activity".equals(meta.getTableClass().getSimpleName())) {
			return true;
		}
		return false;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		if ("Activity".equals(meta.getTableClass().getSimpleName())) {
			List<Object> result = new ArrayList<Object>();
			QueryProperties qp = new QueryProperties();
			
			List<String> param_names = new ArrayList<String>();
			List<Object> param_values = new ArrayList<Object>();
			param_names.add("operationUid");
			param_values.add(userSelection.getOperationUid());
			
			String strFilterConditions = "";
			String rootCauseCode = (String) commandBean.getRoot().getDynaAttr().get("rootCauseCode");
			String downtimeEventNumber = (String) commandBean.getRoot().getDynaAttr().get("downtimeEventNumber");
			String equipmentCode = (String) commandBean.getRoot().getDynaAttr().get("equipmentCode");
			String subEquipmentCode = (String) commandBean.getRoot().getDynaAttr().get("subEquipmentCode");

			if (StringUtils.isNotBlank(rootCauseCode)) {
				strFilterConditions += " AND a.rootCauseCode=:rootCauseCode ";
				param_names.add("rootCauseCode");
				param_values.add(rootCauseCode);
			} else {
				equipmentCode = null;
				commandBean.getRoot().getDynaAttr().put("equipmentCode", equipmentCode);
			}
			
			if (StringUtils.isNotBlank(downtimeEventNumber)) {
				strFilterConditions += " AND a.downtimeEventNumber=:downtimeEventNumber ";
				param_names.add("downtimeEventNumber");
				param_values.add(downtimeEventNumber);
			}

			if (StringUtils.isNotBlank(equipmentCode)) {
				strFilterConditions += " AND a.equipmentCode=:equipmentCode ";
				param_names.add("equipmentCode");
				param_values.add(equipmentCode);
			} else {
				subEquipmentCode = null;
				commandBean.getRoot().getDynaAttr().put("subEquipmentCode", subEquipmentCode);
			}

			if (StringUtils.isNotBlank(subEquipmentCode)) {
				strFilterConditions += " AND a.subEquipmentCode=:subEquipmentCode ";
				param_names.add("subEquipmentCode");
				param_values.add(subEquipmentCode);
			}
			
			if (pagination!=null && pagination.isEnabled()) {
				qp.setRowsToFetch(pagination.getCurrentPage() * pagination.getRowsPerPage(), pagination.getRowsPerPage());
			}
			String queryString = "FROM Activity a, Daily d " +
					"WHERE (a.isDeleted=false or a.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND ((a.dayPlus IS NULL) OR a.dayPlus = '0') " +
					"AND (a.carriedForwardActivityUid='' or a.carriedForwardActivityUid is NULL) " +
					"AND (a.isOffline=false or a.isOffline is null) " +
					"AND (a.isSimop=false or a.isSimop is null) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND d.operationUid=:operationUid " +
					strFilterConditions +
					"ORDER BY a.startDatetime";
			List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, param_names, param_values, qp);
			for (Object[] rec : lstResult) {
				result.add(rec[0]);
			}
			
			Long total_rows = new Long(-1);
			List<Object> lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, param_names, param_values);
			if (lstResult2.size()>0) {
				total_rows = Long.valueOf(lstResult2.size());
			}
			if (pagination!=null && pagination.isEnabled()) {
				commandBean.getInfo().getPagination().setTotalRows(total_rows.intValue());
				commandBean.getInfo().getPagination().setRowsPerPage(pagination.getRowsPerPage());
				commandBean.getInfo().getPagination().setCurrentPage(pagination.getCurrentPage());
				commandBean.getInfo().getPagination().setEnabled(pagination.isEnabled());
			}
			return result;
		}
		return null;
	}

}
