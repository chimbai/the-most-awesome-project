package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ReportDailySummaryDataNodeLoadHandler implements DataNodeLoadHandler{

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		// TODO Auto-generated method stub
		return true;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		if (meta.getTableClass().equals(ReportDailySummary.class)) {
		
			List<Object> output_maps = new ArrayList<Object>();
			//create new record for Report Daily Summary to show the QC status without need to add new record on screen		
			ReportDailySummary rptDailySummary = new ReportDailySummary();		
			output_maps.add(rptDailySummary);

			return output_maps;
			
		}
		
		return null;
	}

}
