package com.idsdatanet.d2.drillnet.activity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.LogCurveDefinition;
import com.idsdatanet.d2.core.model.SchedulerTemplate;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.witsml.WitsmlManager;
import com.idsdatanet.depot.witsml.dataobject.log.LogCurveDataSource;

public class WitsmlLogSchedulerTemplateLookupHandler implements LookupHandler, ApplicationContextAware {
	private ApplicationContext applicationContext;
	
	public void setApplicationContext(ApplicationContext appContext) {
		this.applicationContext = appContext;
	}
	
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		Map<String, LookupItem> collections = new HashMap<String, LookupItem>();
		UserSession session = UserSession.getInstance(request);
		String hql = "select st.schedulerTemplateUid, dwom.depotWellOperationMappingUid from DepotWellOperationMapping dwom, SchedulerTemplate st where dwom.schedulerTemplateUid=st.schedulerTemplateUid and st.dataObjects like '" + WitsmlManager.WITSML_LOG_PREFIX + "%' and dwom.wellUid=:wellUid and dwom.wellboreUid=:wellboreUid and dwom.operationUid=:operationUid and (dwom.isDeleted=false or dwom.isDeleted is null)";
		List<Object[]> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"wellUid", "wellboreUid", "operationUid"}, new Object[] {session.getCurrentWellUid(), session.getCurrentWellboreUid(), session.getCurrentOperationUid()});
		if (results != null && results.size() > 0) {
			for (Object[] result : results) {
				String schedulerTemplateUid = (String)result[0];
				String dwomUid = (String)result[1];
				SchedulerTemplate schedulerTemplate = (SchedulerTemplate)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SchedulerTemplate.class, schedulerTemplateUid);
				ImportExportServerProperties serverProperties = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, schedulerTemplate.getImportExportServerPropertiesUid());
				
				String dataObjects = schedulerTemplate.getDataObjects();
				String[] dataObjectList = dataObjects.split("[,]");
				for (String dataObject : dataObjectList) {
					if (dataObject.startsWith(WitsmlManager.WITSML_LOG_PREFIX)) {
						String[] args = dataObject.split("[.]");
						String logCurveDefUid = args[1];
						LogCurveDefinition logCurveDefinition = (LogCurveDefinition)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LogCurveDefinition.class, logCurveDefUid);
						LogCurveDataSource dataSource = (LogCurveDataSource)this.getApplicationContext().getBean(logCurveDefinition.getLogSoapQueryId());
						String tableName = dataSource.getTablename();
						if (tableName.equals("Activity")) {
							String key = schedulerTemplateUid + "." + dwomUid + "." + logCurveDefUid;
							collections.put(key, new LookupItem(key, schedulerTemplate.getSchedulerName() + " [" + serverProperties.getServiceName() + "]"));
						}
					}
				}
			}
		}
		return collections;
	}
}
