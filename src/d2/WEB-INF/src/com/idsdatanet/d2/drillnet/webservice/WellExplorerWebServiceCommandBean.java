package com.idsdatanet.d2.drillnet.webservice;

/* @TODO
 * Comment Code
 * Get Current Operation working
 */


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;

import com.idsdatanet.d2.infra.uom.OperationUomAndDatumSelector;
import com.idsdatanet.d2.infra.uom.OperationUomContext;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.cache.Wellbores;
import com.idsdatanet.d2.core.cache.Wells;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.util.xml.SimpleAttributes;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.menu.OperationDescriptor;
import com.idsdatanet.d2.core.web.mvc.AbstractGenericWebServiceCommandBean;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Well;
import com.idsdatanet.d2.drillnet.operation.OperationUtils;
import com.idsdatanet.d2.drillnet.report.ReportUtils;

public class WellExplorerWebServiceCommandBean extends AbstractGenericWebServiceCommandBean{
	// Final strings to determine which method is being called
	public static final String PARAM_INVOCATION_METHOD = "method";
	public static final String METHOD_GET_WELL_LIST = "get_well_list";
	public static final String METHOD_GET_WELLBORE_LIST = "get_wellbore_list";
	public static final String METHOD_GET_OPERATION_LIST = "get_operation_list";
	public static final String METHOD_GET_REPORT_DAILY_LIST = "get_report_daily_list";
	public static final String METHOD_GET_SELECTED_GROUP = "get_selected_group";
	public static final String MAIN_ACTION_SELECTED_WELL = "get_selected_well";
	
	// Global Variables
	private Map<String, LookupItem> resultOne,resultTwo;
	private SimpleDateFormat sdf = new SimpleDateFormat();
	private UserSession session;
	private UserSelectionSnapshot snapshot;
	private LookupCache lookupCache;
	private String temp,uri = new String();
	private QueryProperties qp = new QueryProperties();
	//private String operationScreen = "welloperation.html";
	
	//private CustomFieldUom mdConverter,tvdConverter,thisConverter;
	
	public void process(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		// Local Variables
		String method = request.getParameter(PARAM_INVOCATION_METHOD);
		session = UserSession.getInstance(request);
		snapshot = new UserSelectionSnapshot(session);
	    lookupCache = new LookupCache();
	    sdf.applyPattern("dd-MMM-yyyy");
	    //qp.setUomDatumUid(value);
	    
		if(METHOD_GET_WELL_LIST.equals(method)){
			this.getWellList(request, response);
		}
		else if (MAIN_ACTION_SELECTED_WELL.equals(method)){
			this.getSelectedWell(request, response);
		}
		else if(METHOD_GET_WELLBORE_LIST.equals(method)){
			this.getWellBoreList(request, response);
		}
		else if(METHOD_GET_OPERATION_LIST.equals(method)){
			this.getOperationList(request, response);
		}
		else if(METHOD_GET_REPORT_DAILY_LIST.equals(method)){
			this.getReportDailyList(request, response);
		}else if (METHOD_GET_SELECTED_GROUP.equals(method)){
			this.getSelectedGroup(request, response);
		}
	}
	
	private void getWellList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//List<Well> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().find("from Well where (isDeleted = false or isDeleted is null) order by wellName");
		Wells accessibleWells = session.getCachedAllAccessibleWells();
		
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		
		SimpleAttributes atts = new SimpleAttributes();
		atts.addAttribute("operationScreen", OperationUtils.getWWOHyperlinkRedirection(session.getCurrentGroupUid()));
		writer.startElement("configuration",atts);
		writer.endElement();

		//sort the well name so well 11 appear after well 5
						 
		//getWellData(writer,rs,request);
		getWellData(writer,accessibleWells,request);
		writer.close();
	}
	
	private void getSelectedWell(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String wellUid = request.getParameter("wellUid");
		List <Well>rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Well where (isDeleted = false or isDeleted is null) and wellUid=:wellUid","wellUid",wellUid);
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		
		getSelectedWellData(writer,rs,request);
		writer.close();
	}
	
	private void getWellBoreList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String wellUid = request.getParameter("wellUid");
		List<Wellbore> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where (isDeleted = false or isDeleted is null) and wellUid=:wellUid","wellUid",wellUid);
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		
		//sort the well name so wellbore 11 appear after wellbore 5
		Collections.sort(rs, new WellboreNameComparator());
		
		getWellboreData(writer,rs,request);
		writer.close();
	}
	
	private void getOperationList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String wellboreUid = request.getParameter("wellboreUid");
		List<Operation> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and wellboreUid=:wellboreUid", "wellboreUid", wellboreUid);
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		
		//sort the well name so Operation 11 appear after Operation 5
		//Collections.sort(rs, new OperationNameComparator());
		
		//use this sorting to sort Operation in chronological order
		Collection<OperationDescriptor> operationDescrList = MenuManager.getConfiguredInstance().getOperationDescriptionList(rs);
		
		getOperationData(writer, operationDescrList);
		writer.close();
	}

	private void getReportDailyList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String operationUid = request.getParameter("operationUid");
		List<Operation> op = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and operationUid=:operationUid AND sys_operation_subclass='well'", "operationUid", operationUid);
	//	if(op.size()==1){
	//		qp.setUomDatumUid(op.get(0).getDefaultDatumUid());
	//	}

		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		
		String[] params = new String[]{"operationUid","reportType"};
		Object[] values = new Object[]{operationUid,reportType};

		List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT a, (SELECT b.reportType FROM ReportDaily b WHERE a.dailyUid = b.dailyUid AND a.reportType <> b.reportType AND (b.isDeleted = false OR b.isDeleted is null)) AS otherReport, (SELECT c.reportNumber FROM ReportDaily c WHERE a.dailyUid = c.dailyUid AND a.reportType <> c.reportType AND (c.isDeleted = false OR c.isDeleted is NULL)) AS otherReportNumber FROM ReportDaily a WHERE a.reportType=:reportType AND (a.isDeleted = false or a.isDeleted is null) and a.operationUid=:operationUid ORDER BY a.reportDatetime", params, values);		
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		getReportDailyData(writer,rs,request);
		writer.close();
	}
	
	private Locale getLocale(HttpServletRequest request) throws Exception {
		return UserSession.getInstance(request).getUserLocale();
	}
	
	private String getCompanyName(String companyUid) throws Exception {
		if (companyUid==null) return "";
		LookupCompany company = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, companyUid);
		if (company==null) return companyUid;
		return company.getCompanyName();
	}
	
	//private SimpleXmlWriter getWellData(SimpleXmlWriter writer, List<Well> rs, HttpServletRequest request) throws Exception{
	private SimpleXmlWriter getWellData(SimpleXmlWriter writer, Wells accessibleWells, HttpServletRequest request) throws Exception{
			
		//uri = "xml://well.on_off_shore?key=code&amp;value=label";
		//resultOne = LookupManager.getConfiguredInstance().getLookup(uri, snapshot, lookupCache);
		//uri = "xml://wellbore.type?key=code&amp;value=label";
		//resultTwo = LookupManager.getConfiguredInstance().getLookup(uri, snapshot, lookupCache);
		
		temp = "";
		Operation op = new Operation();
		if(snapshot.getOperationUid() != null) {
			op = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, snapshot.getOperationUid());
		}
		
		//CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Well.class,"waterDepth");

		for (_Well rec:accessibleWells.values()){		
			writer.startElement("w");
			writer.addElement("w", rec.getWellUid());
			writer.addElement("g", rec.getGroupUid());
			writer.addElement("n", rec.getWellName());
			writer.addElement("s", WellNameUtil.getPaddedStr(rec.getWellName()));
			/*writer.addElement("platformUid",rec.getPlatformUid());
			
			writer.addElement("country", rec.getCountry());
			if(rec.getWaterDepth() != null){
				writer.addElement("waterDepth", thisConverter.getFormattedValue(rec.getWaterDepth()));
			}
			if(rec.getOnOffShore() != null){
				temp = rec.getOnOffShore();
				if(resultOne.get(rec.getOnOffShore()) != null)
					temp = resultOne.get(rec.getOnOffShore()).getValue().toString();
			}
			writer.addElement("onOffShore", temp);
			*/
			
			//String opCo = rec.getOpCo();
			//if(rec.getOpCo() != null)writer.addElement("opCompany", this.getCompanyName(opCo));
			
			writer.endElement();
			
			if(op != null && rec.getWellUid().equals(op.getWellUid())){
				writer.startElement("CurrentOperation");
				writer.addElement("currentOperationUid", op.getOperationUid());
				writer.addElement("wellToUse", op.getWellUid());
				writer.addElement("wellboreToUse", op.getWellboreUid());
				writer.endElement();
				List<Well> wl = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Well where (isDeleted = false or isDeleted is null) and wellUid=:wellUid","wellUid",op.getWellUid());
				getSelectedWellData(writer,wl,request);
				List<Wellbore> wbl = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where (isDeleted = false or isDeleted is null) and wellUid=:wellUid","wellUid",op.getWellUid());
				getWellboreData(writer,wbl,request);
				List<Operation> opl = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and wellboreUid=:wellboreUid  AND sys_operation_subclass='well'", "wellboreUid", op.getWellboreUid());
				Collection<OperationDescriptor> operationDescrList = MenuManager.getConfiguredInstance().getOperationDescriptionList(opl);
				getOperationData(writer,operationDescrList);
				List<Object[]> rdl = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT a, (SELECT b.reportType FROM ReportDaily b WHERE a.dailyUid = b.dailyUid AND a.reportType <> b.reportType AND (b.isDeleted = false OR b.isDeleted is null)) AS otherReport, (SELECT c.reportNumber FROM ReportDaily c WHERE a.dailyUid = c.dailyUid AND a.reportType <> c.reportType AND (c.isDeleted = false OR c.isDeleted is null)) AS otherReportNumber FROM ReportDaily a WHERE a.reportType<>'DGR' AND (a.isDeleted = false or a.isDeleted is null) and a.operationUid=:operationUid ORDER BY a.reportDatetime", "operationUid", op.getOperationUid(),qp);
				getReportDailyData(writer,rdl,request);
			}
		}
		
		
		/*Map<String, _Well> accessibleWells = session.getCachedAllAccessibleWells();
		for(Well rec: rs){
			if (accessibleWells.containsKey(rec.getWellUid())) {
				writer.startElement("Well");
				writer.addElement("wellUid", rec.getWellUid());
				writer.addElement("wellName", rec.getWellName());
				writer.addElement("groupUid", rec.getGroupUid());
				//writer.addElement("platformUid",rec.getPlatformUid());
				
				//writer.addElement("country", rec.getCountry());
				//if(rec.getWaterDepth() != null){
				//	writer.addElement("waterDepth", thisConverter.getFormattedValue(rec.getWaterDepth()));
				//}
				//if(rec.getOnOffShore() != null){
				//	temp = rec.getOnOffShore();
				//	if(resultOne.get(rec.getOnOffShore()) != null)
				//		temp = resultOne.get(rec.getOnOffShore()).getValue().toString();
				//}
				//writer.addElement("onOffShore", temp);
				
				//String opCo = rec.getOpCo();
				//if(rec.getOpCo() != null)writer.addElement("opCompany", this.getCompanyName(opCo));
				
				writer.endElement();
			}
			
			if(op != null && rec.getWellUid().equals(op.getWellUid())){
				writer.startElement("CurrentOperation");
				writer.addElement("currentOperationUid", op.getOperationUid());
				writer.addElement("wellToUse", op.getWellUid());
				writer.addElement("wellboreToUse", op.getWellboreUid());
				writer.endElement();
				List<Wellbore> wbl = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Wellbore where (isDeleted = false or isDeleted is null) and wellUid=:wellUid","wellUid",op.getWellUid());
				getWellboreData(writer,wbl,request);
				List<Operation> opl = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and wellboreUid=:wellboreUid  AND sys_operation_subclass='well'", "wellboreUid", op.getWellboreUid());
				Collection<OperationDescriptor> operationDescrList = MenuManager.getConfiguredInstance().getOperationDescriptionList(opl);
				getOperationData(writer,operationDescrList);
				List<Object[]> rdl = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT a, (SELECT b.reportType FROM ReportDaily b WHERE a.dailyUid = b.dailyUid AND a.reportType <> b.reportType AND (b.isDeleted = false OR b.isDeleted is null)) AS otherReport, (SELECT c.reportNumber FROM ReportDaily c WHERE a.dailyUid = c.dailyUid AND a.reportType <> c.reportType AND (c.isDeleted = false OR c.isDeleted is null)) AS otherReportNumber FROM ReportDaily a WHERE a.reportType<>'DGR' AND (a.isDeleted = false or a.isDeleted is null) and a.operationUid=:operationUid ORDER BY a.reportDatetime", "operationUid", op.getOperationUid(),qp);
				getReportDailyData(writer,rdl,request);
			}
		}*/
		return writer;
	}
	
	
	private SimpleXmlWriter getSelectedWellData(SimpleXmlWriter writer, List<Well> rs, HttpServletRequest request) throws Exception{
		uri = "xml://well.on_off_shore?key=code&amp;value=label";
		resultOne = LookupManager.getConfiguredInstance().getLookup(uri, snapshot, lookupCache);
		uri = "xml://wellbore.type?key=code&amp;value=label";
		resultTwo = LookupManager.getConfiguredInstance().getLookup(uri, snapshot, lookupCache);
		
		temp = "";
		
		CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), Well.class,"waterDepth");

		if (rs != null && rs.size() > 0) {
			Well rec = rs.get(0);
			writer.startElement("sWell");
			writer.addElement("uid", rec.getWellUid());
			writer.addElement("platformUid",rec.getPlatformUid());
			
			String SQL = null;
			QueryProperties queryProperties = new QueryProperties();
			queryProperties.setUomConversionEnabled(false);
			queryProperties.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			SQL = "Select lookupLabel from CommonLookup where shortCode=:country";
			List CL = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(SQL,"country",rec.getCountry(),queryProperties);
			String countryDesc = null;
			if (CL.size()>0){
				//Object[] b = (Object[]) RD.get(0);
				countryDesc = CL.get(0).toString();
			}
			
			writer.addElement("country", countryDesc);
			if(rec.getWaterDepth() != null){
				writer.addElement("waterDepth", thisConverter.getFormattedValue(rec.getWaterDepth()));
			}
			if(rec.getOnOffShore() != null){
				temp = rec.getOnOffShore();
				if(resultOne.get(rec.getOnOffShore()) != null)
					temp = resultOne.get(rec.getOnOffShore()).getValue().toString();
			}
			writer.addElement("onOffShore", temp);
			writer.endElement();
		}

		return writer;
	}
	
	private SimpleXmlWriter getWellboreData(SimpleXmlWriter writer, List<Wellbore> rs, HttpServletRequest request) throws Exception{
		uri = "xml://wellbore.type?key=code&amp;value=label";
		resultOne = LookupManager.getConfiguredInstance().getLookup(uri, snapshot, lookupCache);
		uri = "xml://wellbore.final_purpose?key=code&amp;value=label";
		resultTwo = LookupManager.getConfiguredInstance().getLookup(uri, snapshot, lookupCache);
		
		temp = "";
		
		CustomFieldUom mdConverter = new CustomFieldUom(this.getLocale(request), Wellbore.class, "plannedTdMdMsl");
		CustomFieldUom tvdConverter = new CustomFieldUom(this.getLocale(request), Wellbore.class, "plannedTdTvdMsl");
		
		Wellbores accessibleWellbores = session.getCachedAllAccessibleWellbores();
		
		for(Wellbore rec: rs){
			
			if(accessibleWellbores.containsKey(rec.getWellboreUid())){
				writer.startElement("Wellbore");
				writer.addElement("wellboreUid", rec.getWellboreUid());
				writer.addElement("wellboreName", rec.getWellboreName());
				Wellbore pwb = new Wellbore();
				if(rec.getParentWellboreUid() != null) pwb = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, rec.getParentWellboreUid());
				if(pwb != null)writer.addElement("parentWellbore", pwb.getWellboreName());
				if(rec.getWellboreType() != null){
					temp = rec.getWellboreType();
					if(resultOne.get(rec.getWellboreType()) != null)
						temp = resultOne.get(rec.getWellboreType()).getValue().toString();
				}
				writer.addElement("wellboreType", temp);
				temp = "";
				if(rec.getWellboreFinalPurpose() != null){
					temp = rec.getWellboreFinalPurpose();
					if(resultTwo.get(rec.getWellboreFinalPurpose()) != null)
						temp = resultTwo.get(rec.getWellboreFinalPurpose()).getValue().toString();
				}
				writer.addElement("wellboreFinalPurpose", temp);
				if(rec.getPlannedTdMdMsl() != null){
					
					writer.addElement("plannedMd", mdConverter.getFormattedValue(rec.getPlannedTdMdMsl()));
				}
				if(rec.getPlannedTdTvdMsl() != null){
					
					writer.addElement("plannedTvd", tvdConverter.getFormattedValue(rec.getPlannedTdTvdMsl()));
				}
				writer.endElement();
			}
		}
		return writer;
	}
	
	private SimpleXmlWriter getOperationData(SimpleXmlWriter writer, Collection<OperationDescriptor> rs) throws Exception{
		uri = "xml://operationtype?key=code&amp;value=label";
		resultOne = LookupManager.getConfiguredInstance().getLookup(uri, snapshot, lookupCache);
		
		temp = "";
		String temp2 = "";
		
		Operations accessibleOperations = session.getCachedAllAccessibleOperations();
		
		if (rs != null && rs.size() > 0) {
			for (OperationDescriptor od : rs) {
				Operation rec = od.getOperation();
				if(accessibleOperations.containsKey(rec.getOperationUid())){
					writer.startElement("Operation");
					writer.addElement("operationUid",rec.getOperationUid());
					writer.addElement("operationName", rec.getOperationName());
					
					String opCo = rec.getOpCo();
					if(rec.getOpCo() != null)writer.addElement("opCompany", this.getCompanyName(opCo));
	
					//forming the start and end date
					if (od.getStartDate() != null && od.getLastDate() != null) temp2 = " (" + od.getStartDate() + "-" + od.getLastDate() + ")";
					else if (od.getStartDate() != null && od.getLastDate() == null) temp2 = " (" + od.getStartDate() + ")";
					else temp2="";
					
					writer.addElement("operationNameWithDate", rec.getOperationName() + temp2);
					if(rec.getOperationCode() != null){
						temp = rec.getOperationCode();
						if(resultOne.get(rec.getOperationCode()) != null)
							temp = resultOne.get(rec.getOperationCode()).getValue().toString();
					}
					writer.addElement("operationType", temp);
	
					if(od.getStartDate() != null)writer.addElement("startDate", sdf.format(od.getStartDate()));
					if(rec.getRigInformationUid() != null && StringUtils.isNotBlank(rec.getRigInformationUid())){
						List<RigInformation> rig = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from RigInformation where (isDeleted = false or isDeleted is null) and (rigInformationUid=:rigUid or rigName=:rigUid)","rigUid",rec.getRigInformationUid());
						if(rig.size()==1){
							for(RigInformation ri: rig){
								writer.addElement("rig", ri.getRigName());
							}
						}
					}
					writer.addElement("afe", rec.getAfeNumber());
					writer.addElement("operationSummary", rec.getOperationSummary());
					writer.endElement();
				}
			}
		}
		return writer;
	}
	
	private SimpleXmlWriter getReportDailyData(SimpleXmlWriter writer, List<Object[]> rs, HttpServletRequest request) throws Exception{
		uri = "xml://report.reportTypes?key=code&amp;value=label";
		resultOne = LookupManager.getConfiguredInstance().getLookup(uri, snapshot, lookupCache);
		uri = "xml://reportDaily.qcflag?key=code&amp;value=label";
		resultTwo = LookupManager.getConfiguredInstance().getLookup(uri, snapshot, lookupCache);
		
		temp = "";
		OperationUomContext operationUomContext = new OperationUomContext(true, true);
		for(Object obj[]: rs){
			ReportDaily rec = (ReportDaily) obj[0];
			String otherReport = (String) obj[1];
			String otherReportNumber = (String) obj[2];
			String SQL = null;
			List<Operation> op = null;
			//String currencySymbol = "";
			if(rec.getOperationUid() != null){
				op = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where (isDeleted = false or isDeleted is null) and operationUid=:operationUid","operationUid",rec.getOperationUid(),qp);
			}
			QueryProperties queryProperties = new QueryProperties();
			queryProperties.setUomConversionEnabled(false);
			queryProperties.setDynamicUomAndDatumSelector(OperationUomAndDatumSelector.getDynamicDatumByOperationUid());
			SQL = "Select operationUid, depthMdMsl, depthTvdMsl from ReportDaily where reportDailyUid=:reportDailyUid";
			List RD = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(SQL,"reportDailyUid",rec.getReportDailyUid(),queryProperties);
			writer.startElement("ReportDaily");
			Double depthMdMsl = null;
			Double depthTvdMsl = null;
			if (RD.size()>0){
				Object[] b = (Object[]) RD.get(0);
				if (b[1]!=null){
					depthMdMsl= Double.parseDouble(b[1].toString());
				}
				if (b[2]!=null){
					depthTvdMsl= Double.parseDouble(b[2].toString());
				}
			}
			if(op.size()==1){
				operationUomContext.setOperationUid(rec.getOperationUid());
				CustomFieldUom mdConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "depthMdMsl",operationUomContext);
				CustomFieldUom tvdConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "depthTvdMsl",operationUomContext);
				CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale(request), ReportDaily.class, "daycost",operationUomContext);
				
				Double dayCost = CommonUtil.getConfiguredInstance().calculateDailyCostFromCostNet(rec.getDailyUid());
				if (dayCost==null) dayCost = rec.getDaycost(); 
				if(dayCost != null){
					thisConverter.setBaseValue(dayCost);
					writer.addElement("dailyCost", thisConverter.getFormattedValue(thisConverter.getConvertedValue(), false));
				}
				if(rec.getDepthMdMsl() != null){
					mdConverter.setBaseValue(depthMdMsl);
					 writer.addElement("depthMdMsl", mdConverter.getFormattedValue());
				}
				if(rec.getDepthTvdMsl() != null){
					tvdConverter.setBaseValue(depthTvdMsl);
					 writer.addElement("depthTvdMsl", tvdConverter.getFormattedValue());
				}
			}
			
			writer.addElement("dailyUid",rec.getDailyUid());
			writer.addElement("reportDailyUid",rec.getReportDailyUid());
			if(rec.getReportType() != null){
				temp = rec.getReportType();
				if(resultOne.get(rec.getReportType()) != null)
					temp = resultOne.get(rec.getReportType()).getValue().toString();
			}
			if(StringUtils.isNotBlank(otherReport) && resultOne.get(otherReport) != null){
				otherReport = resultOne.get(otherReport).getValue().toString();
			}
			writer.addElement("ddrReport", temp);
			writer.addElement("dgrReport", otherReport);
			writer.addElement("ddrReportNum", rec.getReportNumber());
			writer.addElement("dgrReportNum", otherReportNumber);
			
			temp = "";
			
			if(rec.getReportDatetime() != null)writer.addElement("reportDate", sdf.format(rec.getReportDatetime()));
			if(rec.getRigInformationUid() != null && StringUtils.isNotBlank(rec.getRigInformationUid())){
				List<RigInformation> rig = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from RigInformation where (isDeleted = false or isDeleted is null) and (rigInformationUid=:rigUid or rigName=:rigUid)","rigUid",rec.getRigInformationUid());
				if(rig.size()==1){
					for(RigInformation ri: rig){
						writer.addElement("rig", ri.getRigName());
					}
				}
			}
			if(rec.getQcFlag() != null){
				temp = rec.getQcFlag();
				if(resultTwo.get(rec.getQcFlag()) != null)
					temp = resultTwo.get(rec.getQcFlag()).getValue().toString();
			}
			writer.addElement("qcFlag", temp);
			
//			 Get the report files UID for the DDR and DGR for this daily, not on SWF Yet
			File output_file = null;
			if(StringUtils.isNotBlank(rec.getDailyUid())){
				String strSql = "FROM ReportFiles WHERE dailyUid = :dailyUid AND reportType = :reportType AND (isDeleted = false OR isDeleted is NULL)";
				String[] params = new String[]{"dailyUid","reportType"};
				Object[] values = new Object[]{rec.getDailyUid(),"DDR"};
				List<ReportFiles> ddrs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, params, values);
				if(ddrs != null && ddrs.size() > 0){
					ReportFiles ddr = ddrs.get(0);
					String reportFilesUid = null;
					try{
						output_file = new File(ReportUtils.getFullOutputFilePath(ddr.getReportFile()));
					}catch(Exception e){
						e.printStackTrace();
					}
					if (output_file.exists() && output_file != null) reportFilesUid = ddr.getReportFilesUid();
					writer.addElement("ddrReportFilesUid", reportFilesUid);
					writer.addElement("ddrReportFilesName", ddr.getReportFile());
					writer.addElement("ddrReportDisplayName", ddr.getDisplayName());
				}
				values = new Object[]{rec.getDailyUid(),"DGR"};
				List<ReportFiles> dgrs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, params, values);
				if(dgrs != null && dgrs.size() > 0){
					ReportFiles dgr = dgrs.get(0);
					String reportFilesUid = null;
					
					try{
						output_file = new File(ReportUtils.getFullOutputFilePath(dgr.getReportFile()));
					}catch(Exception e){
						e.printStackTrace();
					}
					if (output_file.exists() && output_file != null) reportFilesUid = dgr.getReportFilesUid();
					writer.addElement("dgrReportFilesUid", reportFilesUid);
					writer.addElement("dgrReportFilesName", dgr.getReportFile());
					writer.addElement("dgrReportDisplayName", dgr.getDisplayName());
				}
			}
			
			writer.endElement();
		}
		return writer;
	}
	
	
	//comparator to order the well name so well 11 appear after well 5
	/*private class WellNameComparator implements Comparator<_Well>{		
		public int compare(_Well w1, _Well w2) {			
			String v1 = w1.getWellName();
			String v2 = w2.getWellName();			
			if (v1 == null || v2 == null || StringUtils.equals(v1, v2)) {
				return 0;
			}
			return WellNameUtil.getPaddedStr(v1).compareTo(WellNameUtil.getPaddedStr(v2));
		}
	}*/
	
	private class WellboreNameComparator implements Comparator<Wellbore>{		
		public int compare(Wellbore w1, Wellbore w2) {			
			String v1 = w1.getWellboreName();
			String v2 = w2.getWellboreName();			
			if (v1 == null || v2 == null || StringUtils.equals(v1, v2)) {
				return 0;
			}
			return WellNameUtil.getPaddedStr(v1).compareTo(WellNameUtil.getPaddedStr(v2));
		}
	}
	/*
	public String getOperationScreen() {
		return operationScreen;
	}

	public void setOperationScreen(String operationScreen) {
		this.operationScreen = operationScreen;
	}
	*/
	private void getSelectedGroup(HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserSession userSession = UserSession.getInstance(request);
		SimpleXmlWriter writer = new SimpleXmlWriter(response);
		writer.startElement("root");
		writer.startElement("Group");
		writer.addElement("currentWellUid", userSession.getCurrentWellUid());
		writer.addElement("currentWellboreUid", userSession.getCurrentWellboreUid());
		writer.addElement("currentOperationUid", userSession.getCurrentOperationUid());
		writer.addElement("currentDailyUid",userSession.getCurrentDailyUid());
		writer.endAllElements();
		writer.close();		
	}	
	
	/*
	private class OperationNameComparator implements Comparator<Operation>{		
		public int compare(Operation w1, Operation w2) {			
			String v1 = w1.getOperationName();
			String v2 = w2.getOperationName();			
			if (v1 == null || v2 == null || StringUtils.equals(v1, v2)) {
				return 0;
			}
			return WellNameUtil.getPaddedStr(v1).compareTo(WellNameUtil.getPaddedStr(v2));
		}
	}
	*/
}
