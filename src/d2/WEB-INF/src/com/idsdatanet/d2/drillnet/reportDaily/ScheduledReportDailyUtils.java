package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.List;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class ScheduledReportDailyUtils {

	synchronized public void run() throws Exception
	{
		try{
			this.calculateDryHole();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void calculateDryHole() throws Exception {
		String sql = null;
		sql = "UPDATE ReportDaily rd SET rd.dryHoleDuration=null, rd.dryHoleNptDuration=null, rd.costPerSecond=null, rd.dryHoleCost=null "
				+ "WHERE rd.operationUid in (SELECT operationUid FROM Operation o WHERE (o.isDeleted = false or o.isDeleted is null) AND (o.spudDate is null or o.dryHoleEndDateTime is null)) "
				+ "AND (rd.isDeleted = false or rd.isDeleted is null) ";
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sql, new String [] {}, new Object[] {});
		
		List<Operation> operations = ApplicationUtils.getConfiguredInstance().getDaoManager().find("From Operation Where (isDeleted = false or isDeleted is null) and spudDate is not null and dryHoleEndDateTime is not null ");
		
		for (Operation op:operations)
		{
			List<ReportDaily> days = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from ReportDaily where (isDeleted = false or isDeleted is null) and reportType <> 'DGR' and operationUid=:operationUid", new String[] { "operationUid" }, new Object[] { op.getOperationUid() });
			if (days.size()>0)
			{
				for( ReportDaily day:days){
					ReportDailyUtils.setReportDailyDryHole(day, op.getSpudDate(), op.getDryHoleEndDateTime());
				}
			}
		}
	}
}
