package com.idsdatanet.d2.drillnet.wellexplorer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellExplorerCommandBeanListener extends DefaultBeanSupport implements CommandBeanListener  {
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		commandBean.getReferenceData().put("_sys_well_explorer", WellExplorerUtils.getConfiguredInstance().getWellExplorerTreeMenu(UserSession.getInstance(request)));
	}

	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
	}
	
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
	}

	public void init(CommandBean commandBean){
	}
	
}
