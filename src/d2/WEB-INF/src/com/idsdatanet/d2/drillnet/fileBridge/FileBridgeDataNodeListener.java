package com.idsdatanet.d2.drillnet.fileBridge;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.birt.report.model.api.util.StringUtil;
import org.springframework.util.StringUtils;

import com.idsdatanet.d2.core.model.FileBridgeFiles;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.depot.d2.filebridge.log.FileBridgeLogUtil;
import com.idsdatanet.depot.d2.filebridge.util.FileBridgeUtils;

public class FileBridgeDataNodeListener extends EmptyDataNodeListener {

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof FileBridgeFiles) {
			FileBridgeFiles fbf = (FileBridgeFiles) object;
			
			if (fbf.getParserKey().contains("_PDF")) {
				if (!(fbf.getFileName().toLowerCase().endsWith(".pdf"))){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "fileName", "The File must in .pdf format");
					return;
				}
			}else if(fbf.getParserKey().contains("_XLSX")) {
				if (fbf.getParserKey().equals("IWC_DRILLPLAN_TIME_PLAN_XLSX") && (fbf.getFileName().toLowerCase().endsWith(".csv"))){
					
				} else if(!(fbf.getFileName().toLowerCase().endsWith(".xls")) && !(fbf.getFileName().toLowerCase().endsWith(".xlsx"))){
					status.setContinueProcess(false, true);
					String message = "The File must in a valid Excel format. (.xls/.xlsx)";
					if(fbf.getParserKey().equals("IWC_DRILLPLAN_TIME_PLAN_XLSX")) message += " or CSV format (.csv)";
					status.setFieldError(node, "fileName", message);
					return;
				}
			}else if(fbf.getParserKey().contains("_XML")) {
				if (!(fbf.getFileName().toLowerCase().endsWith(".xml"))){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "fileName", "The File must in .xml format");
					return;
				}
			}else if(fbf.getParserKey().contains("_HTML")) {
				if (!(fbf.getFileName().toLowerCase().endsWith(".html"))){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "fileName", "The File must in .html format");
					return;
				}
			}else if(!(fbf.getFileName().toLowerCase().endsWith(".pdf") || fbf.getFileName().toLowerCase().endsWith(".xls") || fbf.getFileName().toLowerCase().endsWith(".xlsx") || fbf.getFileName().toLowerCase().endsWith(".csv")) && StringUtil.isEmpty(fbf.getParserKey())) {
				status.setContinueProcess(false, true);
				status.setFieldError(node, "parserKey", "Please select the appropriate report type.");
				return;
			}
			
			if (StringUtils.isEmpty(fbf.getCreatedDatetime())) fbf.setCreatedDatetime(new Date());
			fbf.setStatus(FileBridgeUtils.UPLOADED);
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		FileBridgeFiles fbf = (FileBridgeFiles) node.getData();
		node.setValue("@status", FileBridgeUtils.getStatusLabel(fbf.getStatus()));
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		FileBridgeFiles fbf = (FileBridgeFiles) node.getData();
		FileBridgeLogUtil.deleteLog(fbf.getFileBridgeFilesUid());
	}
}
