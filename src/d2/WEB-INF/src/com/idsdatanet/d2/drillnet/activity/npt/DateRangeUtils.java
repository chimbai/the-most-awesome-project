package com.idsdatanet.d2.drillnet.activity.npt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateRangeUtils {
	
	public static final String WEEKLY="0";
	public static final String MONTHLY="1";
	public static final String QUARTERLY="2";
	public static final String HALFYEARLY="3";
	public static final String YEARLY="4";
	
	public static List<Date> getDateList(Date start, Date end, Boolean isEnd, String dateType) throws Exception
	{
		List<Date> result = new ArrayList<Date>();
		Date startQ = getDateByType(start,isEnd,dateType,false);
		Date endQ = getDateByType(end,isEnd,dateType,false);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startQ);
		Date counter = startQ;
		while(counter.compareTo(endQ)<=0)
		{
			result.add(counter);
			counter = getDateByType(counter,isEnd,dateType,true);
		}
		return result;
	}
	
	public static String formatDateLabel(Date date,String dateType) throws Exception
	{
		return formatDateLabel(date,dateType,false);
	}
	
	public static String formatDateLabel(Date date,String dateType, Boolean isReportOutput) throws Exception
	{
		SimpleDateFormat df = new SimpleDateFormat();
		df.applyPattern("yyyy");
		if (dateType.equalsIgnoreCase(DateRangeUtils.WEEKLY))
		{
			if (isReportOutput)
			{
				df.applyLocalizedPattern("dd/MM/yyyy");
				return df.format(DateRangeUtils.getDateByType(date, true, dateType, false));
			}else
				return "W"+DateRangeUtils.getWeek(date)+" "+df.format(date);
		}
		if (dateType.equalsIgnoreCase(DateRangeUtils.MONTHLY))
		{
			df.applyPattern("MMM yyyy");
			return df.format(date);
		}
		if (dateType.equalsIgnoreCase(DateRangeUtils.QUARTERLY))
		{
			return "Q"+DateRangeUtils.getQuarter(date)+" "+df.format(date);
		}
		if (dateType.equalsIgnoreCase(DateRangeUtils.HALFYEARLY))
		{
			return (DateRangeUtils.getHalfYear(date)==1?"First Half":"Second Half")+" "+df.format(date);
		}
		if (dateType.equalsIgnoreCase(DateRangeUtils.YEARLY))
		{
			return df.format(date);
		}
		return null;
	}
	public static Date getDateByType(Date date, Boolean isEnd, String dateType, Boolean isNext) throws Exception
	{
		if (dateType.equalsIgnoreCase(WEEKLY))
			return isNext?getNextWeekDate(date,isEnd):getWeekDate(date,isEnd);
		if (dateType.equalsIgnoreCase(MONTHLY))
			return isNext?getNextMonthDate(date,isEnd):getMonthDate(date,isEnd);
		if (dateType.equalsIgnoreCase(QUARTERLY))
			return isNext?getNextQuarterDate(date,isEnd):getQuarterDate(date,isEnd);
			if (dateType.equalsIgnoreCase(HALFYEARLY))
				return isNext?getNextHalfYearDate(date,isEnd):getHalfYearDate(date,isEnd);
		if (dateType.equalsIgnoreCase(YEARLY))
			return isNext?getNextYearDate(date,isEnd):getYearDate(date,isEnd);
		return null;
	}
	
	public static Date getNextQuarterDate(Date date,Boolean isEnd) throws Exception
	{
		Date thisQuarterDate = getQuarterDate(date,isEnd);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int daysOfMonth = 31;
		int quarter = getQuarter(date);
		int year = calendar.get(Calendar.YEAR);
		if (quarter>=4)
		{
			quarter=1;
			year++;
		}else
		{
			quarter++;
		}
		int month = isEnd?(quarter*3):((quarter*3)-2);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date quarterDate = sdf.parse("1/"+month+"/"+year);
		if (isEnd)
			quarterDate = sdf.parse(daysInMonth(quarterDate)+"/"+month+"/"+year);
		return quarterDate;
	}
	
	public static int getQuarter(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int month = calendar.get(Calendar.MONTH);
		int quarter = (month+1)/3;
		if ((month+1)%3!=0)
			quarter++;
		return quarter;
	}
	
	public static int getWeek(Date date){
		return getDateInt(date,Calendar.WEEK_OF_YEAR);
	}
	
	public static int getMonth(Date date)
	{
		return getDateInt(date,Calendar.MONTH);
	}
	
	public static int getYear(Date date)
	{
		return getDateInt(date,Calendar.YEAR);
	}
	
	public static int getDateInt(Date date, int calendarType)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.MONDAY);
		calendar.setTime(date);
		int Int = calendar.get(calendarType);
		return Int;
	}
	
	public static Date getNextWeekDate(Date date, Boolean isEnd) throws Exception
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.MONDAY);
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 7);
		return calendar.getTime();
	}
	
	public static Date getWeekDate(Date date, Boolean isEnd) throws ParseException
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.MONDAY);
		calendar.setTime(date);
		if(calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
			calendar.add(Calendar.DATE, -calendar.get(Calendar.DAY_OF_WEEK)+2);
		}
		Date startDate = calendar.getTime();
		if (isEnd)
			calendar.add(Calendar.DATE,6);
		Date endTime = calendar.getTime();
		return calendar.getTime();
	}
	
	public static Date getQuarterDate(Date date, Boolean isEnd) throws ParseException
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int daysOfMonth = 31;
		int quarter = getQuarter(date);
		int month = isEnd?(quarter*3):((quarter*3)-2);
		int year = calendar.get(Calendar.YEAR);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date quarterDate = sdf.parse("1/"+month+"/"+year);
		if (isEnd)
			quarterDate = sdf.parse(daysInMonth(quarterDate)+"/"+month+"/"+year);
		return quarterDate;
	}
	
	public static Date getMonthDate(Date date, Boolean isEnd) throws ParseException
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int day = isEnd?daysInMonth(date):1;
		int month = getMonth(date);
		int year = getYear(date);
		calendar.set(year, month, day);
		return calendar.getTime();
	}

	public static Date getNextMonthDate(Date date, Boolean isEnd) throws ParseException
	{
		Date thisMonthDate = getMonthDate(date,isEnd);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(thisMonthDate);
		calendar.add(Calendar.MONTH, 1);
		return calendar.getTime();
	}
	
	public static Date getHalfYearDate(Date date, Boolean isEnd) throws Exception
	{
		int month = getMonth(date)+1;
		if (month<7)
			month = isEnd?6:1;
		else
			month =  isEnd?12:7;
		int year = getYear(date);
		int day=isEnd?daysInMonth(constructDate(1,month,year)):1;
		return constructDate(day,month,year);
	}
	public static int getHalfYear(Date date)
	{
		int month = getMonth(date)+1;
		if (month<7)
			return 1;
		else
			return 2;
	}
	public static Date getNextHalfYearDate(Date date,Boolean isEnd) throws Exception
	{
		Date thisHalf = getHalfYearDate(date,isEnd);
		int year = getYear(date);
		int month = getMonth(thisHalf)+1;
		if (month<7)
			month=isEnd?12:7;
		else{
			month=isEnd?6:1;
			year++;
		}
		int day=isEnd?daysInMonth(constructDate(1,month,year)):1;
		return constructDate(day,month,year);
	}
	
	public static Date getYearDate(Date date, Boolean isEnd) throws ParseException
	{
		int day = isEnd?31:1;
		int month = isEnd?12:1;
		int year = getYear(date);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date ret = sdf.parse(day+"/"+month+"/"+year);
		return ret;
	}

	public static Date getNextYearDate(Date date, Boolean isEnd) throws ParseException
	{
		Date thisYearDate = getYearDate(date,isEnd);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(thisYearDate);
		calendar.add(Calendar.YEAR, 1);
		return calendar.getTime();
	}
	
	public static Date constructDate(int day, int month, int year) throws Exception
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date ret = sdf.parse(day+"/"+month+"/"+year);
		return ret;
	}
	
	public static int daysInMonth(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
}
