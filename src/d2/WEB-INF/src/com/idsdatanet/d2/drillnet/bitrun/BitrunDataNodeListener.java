package com.idsdatanet.d2.drillnet.bitrun;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.BitNozzle;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.bharun.BharunUtils;

public class BitrunDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	
	@Override
	public void afterDataNodeDelete(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		if (node.getData() instanceof BitNozzle) {
			this.updateBharunJetVelocity(node);
		}
	}

	@Override
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		if (node.getData() instanceof BitNozzle) {
			this.updateBharunJetVelocity(node);
		}
	}
	
	private void updateBharunJetVelocity(CommandBeanTreeNode node) throws Exception {
		String bitrunUid = null;
		if (node.getData() instanceof Bitrun) {
			Bitrun bitrun = (Bitrun) node.getData();
			bitrunUid = bitrun.getBitrunUid();
		} else if (node.getData() instanceof BitNozzle) {
			BitNozzle bitNozzle = (BitNozzle) node.getData();
			bitrunUid = bitNozzle.getBitrunUid();
		}
		if (bitrunUid!=null) {
			Bitrun bitrun = (Bitrun) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Bitrun.class, bitrunUid);
			if (bitrun!=null) {
				String bharunUid = bitrun.getBharunUid();
				if (bharunUid!=null) {
					CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
					thisConverter.setReferenceMappingField(BharunDailySummary.class, "jetvelocity");
					String strSql = "FROM BharunDailySummary where (isDeleted=false or isDeleted is null) and bharunUid=:bharunUid";
					List<BharunDailySummary> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "bharunUid", bharunUid);
					for (BharunDailySummary bharunDailySummary : list) {
						Double jetvelocity = CommonUtil.getConfiguredInstance().calculateJetVelocity(bharunUid, bharunDailySummary.getDailyUid());
						if (jetvelocity!=null) {
							thisConverter.setBaseValue(jetvelocity);
							bharunDailySummary.setJetvelocity(jetvelocity);
						} else {
							bharunDailySummary.setJetvelocity(null);
						}
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(bharunDailySummary);
					}
				}
			}
		}
	}

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj=node.getData();
		if (obj instanceof Bitrun) {
			this.includeIADCBitDull(commandBean, node);
		}
	}
	private void includeIADCBitDull(CommandBean commandBean, CommandBeanTreeNode node) throws Exception
	{
		Object obj=node.getData();
		if (obj instanceof Bitrun)
		{
			Bitrun bit=(Bitrun) obj;
			
			String cond_final_inner=null;
			if (bit.getCondFinalInner()!=null)
				cond_final_inner=bit.getCondFinalInner();
			
			String cond_final_outer=null;
			if (bit.getCondFinalOuter()!=null)
				cond_final_outer=bit.getCondFinalOuter();
			
			String cond_final_dull=null;
			if (bit.getCondFinalDull()!=null)
				cond_final_dull=bit.getCondFinalDull();
			
			String cond_final_location=null;
			if (bit.getCondFinalLocation()!=null)
				cond_final_location=bit.getCondFinalLocation();
			
			String cond_final_bearing=null;
			if (bit.getCondFinalBearing()!=null)
				cond_final_bearing=bit.getCondFinalBearing();
			
			String cond_final_gauge=null;
			if (bit.getCondFinalGauge()!=null)
				cond_final_gauge=bit.getCondFinalGauge();
				
			String cond_final_other=null;
			if (bit.getCondFinalOther()!=null)
				cond_final_other=bit.getCondFinalOther().replaceAll("\t", "/");
			
			String cond_final_reason=null;
			if (bit.getCondFinalReason()!=null)
				cond_final_reason=bit.getCondFinalReason();
			
			String iadcBitDull=cond_final_inner+"-"+cond_final_outer+"-"+cond_final_dull+"-"+cond_final_location+"-"+cond_final_bearing+"-"+cond_final_gauge+"-"+cond_final_other+"-"+cond_final_reason;
			node.getDynaAttr().put("iadcBitDull", iadcBitDull);
		}
	}
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}