package com.idsdatanet.d2.drillnet.wellRecovery;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WellRecoveryDataNodeListener extends EmptyDataNodeListener {
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if(object instanceof Operation) {
			Operation thisOperation = (Operation)object;
			String thisWellUid = thisOperation.getWellUid();
			String thisWellboreUid = thisOperation.getWellboreUid();
			
			String strSql = "FROM Well WHERE wellUid = :thisWellUid";
			String[] paramsFields = {"thisWellUid"};
			Object[] paramsValues = {thisWellUid};
			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			if (lstResult.size() > 0){
				Well tmpWell = (Well) lstResult.get(0);
				node.getDynaAttr().put("wellName", tmpWell.getWellName());
				if (tmpWell.getIsDeleted() != null) {
					if (tmpWell.getIsDeleted() == true) {
						node.getDynaAttr().put("wellHighlight", "1");
					} else {
						node.getDynaAttr().put("wellHighlight", "0");
					}
				}
			}
			
			String strSql2 = "FROM Wellbore WHERE wellboreUid = :thisWellboreUid";
			String[] paramsFields2 = {"thisWellboreUid"};
			Object[] paramsValues2 = {thisWellboreUid};
			
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
			
			if (lstResult2.size() > 0){
				Wellbore tmpWellbore = (Wellbore) lstResult2.get(0);
				node.getDynaAttr().put("wellboreName", tmpWellbore.getWellboreName());
				if (tmpWellbore.getIsDeleted() != null) {
					if (tmpWellbore.getIsDeleted() == true) {
						node.getDynaAttr().put("wellboreHighlight", "1");
					} else {
						node.getDynaAttr().put("wellboreHighlight", "0");
					}
				}
			}
			
			if (thisOperation.getIsDeleted() != null) {
				if (thisOperation.getIsDeleted() == true) {
					node.getDynaAttr().put("opHighlight", "1");
				} else {
					node.getDynaAttr().put("opHighlight", "0");
				}
			}
		}
	}
}
