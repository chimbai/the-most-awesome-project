package com.idsdatanet.d2.drillnet.activity;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class PerformanceSummaryReportDataGenerator implements ReportDataGenerator {
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
				
		String thisOperationUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getOperationUid().toString())) thisOperationUid = userContext.getUserSelection().getOperationUid().toString();
		
		//DISCONTINUE IF NO OPERATION IS SELECTED	
		if(StringUtils.isBlank(thisOperationUid)) return;
		
		String thisDailyUid = "";
		if(StringUtils.isNotBlank(userContext.getUserSelection().getDailyUid().toString())) thisDailyUid = userContext.getUserSelection().getDailyUid().toString();
		
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		
		Date thisDayDate = daily.getDayDate();
		
		String thisWellUid = userContext.getUserSelection().getWellUid();
		String strOperationList = "";
		String strBit = "";
				
		ReportDataNode thisReportNode = reportDataNode.addChild("PerformanceSummary");
		thisReportNode.addProperty("operationUid", thisOperationUid);
		thisReportNode.addProperty("dailyUid", thisDailyUid);
		thisReportNode.addProperty("wellUid", thisWellUid);
		
		String sql = "SELECT COUNT(*) FROM Operation WHERE wellUid = :wellUid AND (isDeleted = false or isDeleted is null)";
		List result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "wellUid", thisWellUid);
		int count = Integer.parseInt(result.get(0).toString());
		thisReportNode.addProperty("multipleOperations", count > 1 ? "true" : "false");
		
		//GET CUMULATIVE ACTIVITY TOTAL DURATION TO-DATE
		String strSql = "SELECT SUM(activityDuration) FROM Activity WHERE dailyUid IN (SELECT dailyUid FROM Daily WHERE operationUid = :thisOperationUid AND dayDate <= :thisDayDate AND " +
			"(isDeleted = false or isDeleted is null)) AND (isDeleted = false or isDeleted is null) AND (dayPlus <> 1 AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL))";
		String[] paramsFields = {"thisOperationUid", "thisDayDate"};
		Object[] paramsValues = {thisOperationUid, thisDayDate};
					
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (lstResult.size() > 0){
			Object objCumDuration = (Object) lstResult.get(0);
			
			if(objCumDuration != null)
			{
				Double cumulativeTotalDuration = Double.parseDouble(objCumDuration.toString()) / 3600;
				thisReportNode.addProperty("cumulativeTotalDuration", cumulativeTotalDuration.toString());
			}
			else
			{
				thisReportNode.addProperty("cumulativeTotalDuration", "0");
			}
		}
		
		//GET DAILY ACTIVITY TOTAL DURATION
		String strSql2 = "SELECT SUM(activityDuration) FROM Activity WHERE dailyUid = :thisDailyUid AND (isDeleted = false or isDeleted is null) AND " +
			"(dayPlus <> 1 AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL))";
		
		List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "thisDailyUid", thisDailyUid);
		if (lstResult2.size() > 0){
			Object objdailyDuration = (Object) lstResult2.get(0);
			
			if(objdailyDuration != null)
			{
				Double dailyTotalDuration = Double.parseDouble(objdailyDuration.toString()) / 3600;
				thisReportNode.addProperty("dailyTotalDuration", dailyTotalDuration.toString());
			}
			else
			{
				thisReportNode.addProperty("dailyTotalDuration", "0");
			}
		}
				
		//GET DAILY DURATION FOR EACH CLASS
		String strSql3 = "SELECT SUM(activityDuration), classCode, internalClassCode FROM Activity WHERE dailyUid = :thisDailyUid AND (isDeleted = false or isDeleted is null) AND " +
			"(dayPlus <> 1 AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL)) GROUP BY classCode, internalClassCode";
						
		List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, "thisDailyUid", thisDailyUid);
		if (lstResult3.size() > 0){
			for(Object objResult: lstResult3){
				if(objResult != null)
				{
					Object[] objActivity = (Object[]) objResult;
					if (objActivity[0] != null && objActivity[1] != null) {
						String strActivityDuration = objActivity[0].toString();
						String strClassCode = objActivity[1].toString();
						String strInternalClassCode = null;
						if(objActivity[2] != null) strInternalClassCode = objActivity[2].toString();
						
						ReportDataNode thisReportNode2 = thisReportNode.addChild("DailyDurationByClass");
						thisReportNode2.addProperty("operationUid", thisOperationUid);
						thisReportNode2.addProperty("classCode", strClassCode);
						thisReportNode2.addProperty("internalClassCode", strInternalClassCode);
						Double duration = Double.parseDouble(strActivityDuration) / 3600;
						thisReportNode2.addProperty("duration", duration.toString());
					}
				}
			}
		}
		
		//GET CUMULATIVE DURATION TO-DATE FOR EACH CLASS
		String strSql4 = "SELECT SUM(activityDuration), classCode, internalClassCode FROM Activity WHERE dailyUid IN (SELECT dailyUid FROM Daily WHERE operationUid = :thisOperationUid AND " +
			"dayDate <= :thisDayDate AND (isDeleted = false or isDeleted is null)) AND (isDeleted = false or isDeleted is null) AND (dayPlus <> 1 AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL)) " +
			"GROUP BY classCode, internalClassCode";
		String[] paramsFields4 = {"thisOperationUid", "thisDayDate"};
		Object[] paramsValues4 = {thisOperationUid, thisDayDate};
					
		List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4);
		if (lstResult4.size() > 0){
			for(Object objResult: lstResult4){
				if(objResult != null)
				{
					Object[] objActivity = (Object[]) objResult;
					if (objActivity[0] != null && objActivity[1] != null) {
						String strActivityDuration = objActivity[0].toString();
						String strClassCode = objActivity[1].toString();
						String strInternalClassCode = null;
						if(objActivity[2] != null) strInternalClassCode = objActivity[2].toString();
						
						ReportDataNode thisReportNode2 = thisReportNode.addChild("CumulativeDurationByClass");
						thisReportNode2.addProperty("operationUid", thisOperationUid);
						thisReportNode2.addProperty("classCode", strClassCode);
						thisReportNode2.addProperty("internalClassCode", strInternalClassCode);
						Double duration = Double.parseDouble(strActivityDuration) / 3600;
						thisReportNode2.addProperty("duration", duration.toString());
					}
				}
			}
		}
		
		//Use the strWellboreList to retrieve all of the operations using an IN clause
		List <Operation> lstOperation = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Operation WHERE (isDeleted = false or isDeleted is null) and wellUid=:wellUid", "wellUid", thisWellUid);
		if(lstOperation!=null && lstOperation.size() > 0){
			strBit = "";
			for(Operation objOperation: lstOperation){
				strOperationList = strOperationList + strBit + "'" + objOperation.getOperationUid() + "'";
				strBit = ",";
			}
		}
		
		if(StringUtils.isNotBlank(strOperationList)) {
			//GET WELLBASED CUMULATIVE ACTIVITY TOTAL DURATION TO-DATE
			String strSql5 = "SELECT SUM(activityDuration) FROM Activity WHERE dailyUid IN (SELECT dailyUid FROM Daily WHERE operationUid IN (" + strOperationList + 
				") AND dayDate <= :thisDayDate AND (isDeleted = false or isDeleted is null)) AND (isDeleted = false or isDeleted is null) AND ((dayPlus IS NULL OR dayPlus=0) AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL))";
			String[] paramsFields5 = {"thisDayDate"};
			Object[] paramsValues5 = {thisDayDate};
						
			List lstResult5 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql5, paramsFields5, paramsValues5);
			if (lstResult5.size() > 0){
				Object objCumDuration = (Object) lstResult5.get(0);
				
				if(objCumDuration != null)
				{
					Double wellbasedCumulativeTotalDuration = Double.parseDouble(objCumDuration.toString()) / 3600;
					thisReportNode.addProperty("wellbasedCumulativeTotalDuration", wellbasedCumulativeTotalDuration.toString());
				}
				else
				{
					thisReportNode.addProperty("wellbasedCumulativeTotalDuration", "0");
				}
			}
			
			//GET WELLBASED CUMULATIVE DURATION TO-DATE FOR EACH CLASS
			String strSql6 = "SELECT SUM(activityDuration), classCode, internalClassCode FROM Activity WHERE dailyUid IN (SELECT dailyUid FROM Daily WHERE operationUid IN (" + strOperationList + 
							 ") AND dayDate <= :thisDayDate AND (isDeleted = false or isDeleted is null)) AND (isDeleted = false or isDeleted is null) AND ((dayPlus IS NULL OR dayPlus=0) AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL)) " +
							 "GROUP BY classCode, internalClassCode";
			String[] paramsFields6 = {"thisDayDate"};
			Object[] paramsValues6 = {thisDayDate};
						
			List lstResult6 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql6, paramsFields6, paramsValues6);
			if (lstResult6.size() > 0){
				for(Object objResult: lstResult6){
					if(objResult != null)
					{
						Object[] objActivity = (Object[]) objResult;
						if (objActivity[0] != null && objActivity[1] != null) {
							String strActivityDuration = objActivity[0].toString();
							String strClassCode = objActivity[1].toString();
							String strInternalClassCode = null;
							if(objActivity[2] != null) strInternalClassCode = objActivity[2].toString();
							
							ReportDataNode thisReportNode2 = thisReportNode.addChild("WellbasedCumulativeDurationByClass");
							thisReportNode2.addProperty("operationUid", thisOperationUid);
							thisReportNode2.addProperty("classCode", strClassCode);
							thisReportNode2.addProperty("internalClassCode", strInternalClassCode);
							Double duration = Double.parseDouble(strActivityDuration) / 3600;
							thisReportNode2.addProperty("duration", duration.toString());
						}
					}
				}
			}
		}
	}	
}
