package com.idsdatanet.d2.drillnet.lookupTaskCode;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class lookupTaskCodeCommandBeanListener extends EmptyCommandBeanListener {

	@Override
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		String orderByJob = (String) commandBean.getRoot().getDynaAttr().get("filterJobCode");
		if (StringUtils.isBlank(orderByJob)) {
			orderByJob = "Show All";
			commandBean.getRoot().getDynaAttr().put("filterJobCode", orderByJob);
		} 
		
		String orderByPhase = (String) commandBean.getRoot().getDynaAttr().get("filterPhaseCode");
		if (StringUtils.isBlank(orderByPhase)) {
			orderByPhase = "Show All";
			commandBean.getRoot().getDynaAttr().put("filterPhaseCode", orderByPhase);
		} 
	}
	
}
