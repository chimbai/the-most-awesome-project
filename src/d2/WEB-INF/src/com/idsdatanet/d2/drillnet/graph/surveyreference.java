package com.idsdatanet.d2.drillnet.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Polygon;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.graph.BaseGraph;
import com.idsdatanet.d2.core.model.SurveyReference;
import com.idsdatanet.d2.core.model.SurveyStation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class surveyreference extends BaseGraph {
	
	private String surveyReferenceUid = null;
	private NumberFormat formatter = new DecimalFormat("#.###");
	private String graphTitle=null;
	private String graphType=null;
	private String groupUid = null;
	private String graphScaleX = null;
	private String graphScaleY = null;
	private String graphAutoBound = null;
	private Double xAxisMin = 0.0;
	private Double xAxisMax = 0.0;
	private Double yAxisMin = 0.0;
	private Double yAxisMax = 0.0;
	
	public void setGraphAutoBound(String value){
		graphAutoBound = value;
	}
	public String getGraphAutoBound(){
		return graphAutoBound;
	}
	
	public void setGraphScaleX(String value){
		graphScaleX = value;
	}
	public String getGraphScaleX(){
		return graphScaleX;
	}
	
	public void setGraphScaleY(String value){
		graphScaleY = value;
	}
	public String getGraphScaleY(){
		return graphScaleY;
	}
	
	public void setSurveyReferenceUid(String value) {
		surveyReferenceUid = value;
	}
	
	public String getSurveyReferenceUid() {
		return surveyReferenceUid;
	}
	
	public void setGraphType(String value) {
		graphType = value;
	}
	
	public String getGraphType() {
		return graphType;
	}
	
	public void setGroupUid(String value) {
		groupUid = value;
	}
	
	public String getGroupUid() {
		return groupUid;
	}
	
	public String getGraphTitle() {
		return graphTitle;
	}

	public void setGraphTitle(String graphTitle) {
		this.graphTitle = graphTitle;
	}
	
	public Object paint() throws Exception {
		Locale locale = null;
		
		if (this.getCurrentHttpRequest()!=null) {
			HttpServletRequest request = this.getCurrentHttpRequest();
			UserSession session = UserSession.getInstance(request);
			groupUid = session.getCurrentGroupUid();
			locale = session.getUserLocale();
			this.formatter = (DecimalFormat) DecimalFormat.getInstance(locale);
			this.surveyReferenceUid = CommonUtils.null2EmptyString(request.getParameter("uid"));
			if ("".equals(this.surveyReferenceUid)) {
				Object uid = this.getCurrentUserContext().getUserSelection().getCustomProperty("surveyReferenceUid");
				if (uid!=null) this.surveyReferenceUid = (String) uid;
			}
		} else {
			groupUid = this.getCurrentUserContext().getUserSelection().getGroupUid();
			locale = this.getCurrentUserContext().getUserSelection().getLocale();
			Object uid = (String) this.getCurrentUserContext().getUserSelection().getCustomProperty("surveyReferenceUid");
			if (uid!=null) this.surveyReferenceUid = (String) uid;
			
		}
		
		String operationName = "";
		CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale());
		String yAxis = "";
		String xAxis = "";
		
		if (graphType!=null) 
			this.setFileName("SURVEY_"+ graphType + "_" + surveyReferenceUid+".jpg");
		else
			this.setFileName("SURVEY_" + surveyReferenceUid+".jpg");
		
		if ("TvdVsect".equals(graphType)){
			thisConverter.setReferenceMappingField(SurveyStation.class, "depthTvdMsl");
			xAxis = "TVD (" + thisConverter.getUomSymbol() + ")";
			thisConverter.setReferenceMappingField(SurveyStation.class, "verticalSectionExtrusion");
			yAxis = "Vert. Sect. (" + thisConverter.getUomSymbol() + ")";
		}
		else if ("NsEw".equals(graphType)){
			thisConverter.setReferenceMappingField(SurveyStation.class, "nsOffset");
			xAxis = "Northing (+N/-S) (" + thisConverter.getUomSymbol() + ")";
			thisConverter.setReferenceMappingField(SurveyStation.class, "ewOffset");
			yAxis = "Easting (+E/-W) (" + thisConverter.getUomSymbol() + ")";
		}	

		SurveyReference surveyReference = (SurveyReference) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(SurveyReference.class, this.surveyReferenceUid);
		if (surveyReference!=null) {
			operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, surveyReference.getOperationUid());
			
			return this.emptyChart(this.collectDataset(), operationName, xAxis, yAxis,graphType);
		} 
		return this.emptyChart(null, "", xAxis, yAxis,graphType);
	}

	private JFreeChart emptyChart(XYSeriesCollection dataset, String header, String xLabel, String yLabel, String graphType) throws Exception {
		if (dataset==null) {
			dataset = new XYSeriesCollection();
		}
		
		NumberAxis xAxis = new NumberAxis(xLabel);
		xAxis.setAutoRangeIncludesZero(false);
		if ("TvdVsect".equals(graphType)) xAxis.setInverted(true);
		NumberAxis yAxis = new NumberAxis(yLabel);
		yAxis.setAutoRangeIncludesZero(false);
		XYItemRenderer renderer = new StandardXYItemRenderer();
		renderer.setSeriesStroke(0, stroke);		
		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
		plot.setOrientation(PlotOrientation.HORIZONTAL);
		XYLineAndShapeRenderer shapeRenderer = new XYLineAndShapeRenderer(true, true);
		plot.setRenderer(shapeRenderer);
		shapeRenderer.setSeriesShape(0, new Polygon(new int[] {0, 4, -4}, new int[] {-4, 4, 4}, 3));
		shapeRenderer.setBaseShapesVisible(true);
		shapeRenderer.setBaseShapesFilled(true);
		JFreeChart chart = new JFreeChart(graphTitle, JFreeChart.DEFAULT_TITLE_FONT, plot, false);
		chart.addSubtitle(new TextTitle(header));
		chart.setBackgroundPaint(Color.lightGray);
		chart.getXYPlot().getRenderer().setSeriesPaint(0, Color.BLUE);
        
        ((NumberAxis) chart.getXYPlot().getRangeAxis()).setNumberFormatOverride(formatter);
        ((NumberAxis) chart.getXYPlot().getDomainAxis()).setNumberFormatOverride(formatter);
        //optional to fix the scale
        if (graphScaleY!=null){
        	((NumberAxis) chart.getXYPlot().getDomainAxis()).setTickUnit(new NumberTickUnit(Double.parseDouble(graphScaleY)));
        	//((NumberAxis) chart.getXYPlot().getDomainAxis()).setUpperBound(yAxisMax + Double.parseDouble(graphScaleY));
            //((NumberAxis) chart.getXYPlot().getDomainAxis()).setLowerBound(yAxisMin - Double.parseDouble(graphScaleY));             
        }
        if (graphScaleX!=null){
        	((NumberAxis) chart.getXYPlot().getRangeAxis()).setTickUnit(new NumberTickUnit(Double.parseDouble(graphScaleX))); 
        	((NumberAxis) chart.getXYPlot().getRangeAxis()).setUpperBound(xAxisMax + Double.parseDouble(graphScaleX)); 	 
            ((NumberAxis) chart.getXYPlot().getRangeAxis()).setLowerBound(xAxisMin - Double.parseDouble(graphScaleX));
        }
        
        if (graphAutoBound!=null && "true".equals(graphAutoBound)){
        	Double yAxisBound = 0.0;
        	Double xAxisBound =0.0;
        	
        	if (Math.abs(xAxisMin) > Math.abs(xAxisMax))  xAxisBound = Math.abs(xAxisMin);
        	else {
        		if (Math.abs(xAxisMax)== 0){
        			xAxisBound = 10.0;
            	}
        		else 
        			xAxisBound = Math.abs(xAxisMax);
        	}
        		
        	if (Math.abs(yAxisMin) > Math.abs(yAxisMax))  yAxisBound = Math.abs(yAxisMin);
        	else {
        		if (Math.abs(yAxisMax)== 0){
        			yAxisBound = 10.0;
            	}
        		else
        			yAxisBound = Math.abs(yAxisMax);
        	}
        		
        	
        	 if (graphScaleY!=null){
        		 if (yAxisBound < Double.parseDouble(graphScaleY)) yAxisBound = Double.parseDouble(graphScaleY);
        		 else {
        			 long a = Math.round(yAxisBound/Double.parseDouble(graphScaleY));;
        			 yAxisBound =  ((double)a + 1.0)* Double.parseDouble(graphScaleY);
        		 }
        			 
        	 }
        	
        	 if (graphScaleX!=null){
        		 if (xAxisBound < Double.parseDouble(graphScaleX)) xAxisBound = Double.parseDouble(graphScaleX);
        		 else {
        			 long a = Math.round(xAxisBound/Double.parseDouble(graphScaleX));;
        			 xAxisBound =  ((double)a + 1.0)* Double.parseDouble(graphScaleX);
        		 }
             }
        	 
        	 ((NumberAxis) chart.getXYPlot().getDomainAxis()).setUpperBound(yAxisBound);
             ((NumberAxis) chart.getXYPlot().getDomainAxis()).setLowerBound(-1*yAxisBound);
             ((NumberAxis) chart.getXYPlot().getRangeAxis()).setUpperBound(xAxisBound);
             ((NumberAxis) chart.getXYPlot().getRangeAxis()).setLowerBound(-1*xAxisBound);
        }
       
        return chart;
	}
	
	// dashed line
	private BasicStroke stroke = new java.awt.BasicStroke(
		1f,
        BasicStroke.CAP_SQUARE,
        BasicStroke.JOIN_MITER,
        10.0f,
        new float[] {2f, 3f, 2f, 3f},
        0f
	);
	
	private XYSeriesCollection collectDataset() throws Exception {
		XYSeriesCollection dataset = new XYSeriesCollection();
		String queryString = "FROM SurveyStation WHERE (isDeleted=false or isDeleted is null) and surveyReferenceUid=:surveyReferenceUid order by depthMdMsl";
		List<SurveyStation> details = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "surveyReferenceUid", this.surveyReferenceUid);
		String title="Survey Plot";
		
		XYSeries series=new XYSeries(title, false); //set false to disable autoSort 

		for (SurveyStation rec : details) {
			if ("TvdVsect".equals(graphType)){
				if (rec.getDepthTvdMsl()!=null && rec.getVerticalSectionExtrusion()!=null) {
					series.add(rec.getDepthTvdMsl(), rec.getVerticalSectionExtrusion());
					if (yAxisMax < nullToZero((Double)rec.getDepthTvdMsl())){
						yAxisMax = nullToZero((Double)rec.getDepthTvdMsl());
					}
					if (yAxisMin > nullToZero((Double)rec.getDepthTvdMsl())){
						yAxisMin = nullToZero((Double)rec.getDepthTvdMsl());
					}
					if (xAxisMax < nullToZero((Double)rec.getVerticalSectionExtrusion())){
						xAxisMax = nullToZero((Double)rec.getVerticalSectionExtrusion());
					}
					if (xAxisMin > nullToZero((Double)rec.getVerticalSectionExtrusion())){
						xAxisMin = nullToZero((Double)rec.getVerticalSectionExtrusion());
					}	
				}
			}
			else if ("NsEw".equals(graphType)){
				if (rec.getEwOffset()!=null && rec.getNsOffset()!=null) {
					series.add(rec.getNsOffset(), rec.getEwOffset());
					if (yAxisMax < nullToZero((Double)rec.getNsOffset())){
						yAxisMax = nullToZero((Double)rec.getNsOffset());
					}
					if (yAxisMin > nullToZero((Double)rec.getNsOffset())){
						yAxisMin = nullToZero((Double)rec.getNsOffset());
					}
					if (xAxisMax < nullToZero((Double)rec.getEwOffset())){
						xAxisMax = nullToZero((Double)rec.getEwOffset());
					}
					if (xAxisMin > nullToZero((Double)rec.getEwOffset())){
						xAxisMin = nullToZero((Double)rec.getEwOffset());
					}	
				}
			}
		}
		dataset.addSeries(series);
		
		this.graphTitle=null;
		return dataset;
	}
	
	private Double nullToZero(Double value) {
		if (value==null) return 0.0;
		return value;
	}
}
