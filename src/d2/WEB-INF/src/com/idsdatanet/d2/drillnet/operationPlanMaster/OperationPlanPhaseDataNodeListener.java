package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.FileManagerFiles;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.OperationPlanMaster;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.filemanager.FileManagerUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.ChildClassNodesProcessStatus;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.report.DefaultReportModule;
import com.idsdatanet.d2.drillnet.report.ReportJobStatus;

import edu.emory.mathcs.backport.java.util.Collections;

public class OperationPlanPhaseDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener {
	
	private DynamicLookaheadDVDPlan dvdPlanHandler = new DefaultDynamicLookaheadDVDPlanImpl();
	private DefaultReportModule dvdPlanReportModule = null;
	
	public void setDvdPlanReportModule(DefaultReportModule dvdPlanReportModule) {
		this.dvdPlanReportModule = dvdPlanReportModule;
	}

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
		UserSession session = UserSession.getInstance(request);
		this.setActiveOperationPlanMaster(session.getCurrentOperationUid());
		//Calculate planned duration from operation plan phase
		if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoSumP50DurationToOperation")))
		{
			CommonUtil.getConfiguredInstance().calcPlannedDurationFromOperationPlanPhase(session.getCurrentOperationUid());
		}
		
		if(commandBean instanceof BaseCommandBean) {
			BaseCommandBean bean = (BaseCommandBean) commandBean;
			DataNodeAllowedAction allowAction = bean.getDataNodeAllowedAction();
			if(allowAction instanceof DvdPlanLocker) {
				((DvdPlanLocker) allowAction).lockAllDvdPlans(bean.getRoot());
				bean.getFlexClientControl().setForceLoadReferenceData(true);
				
			}
		}
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		calculateCumDayCurveOnLoad(userSelection);
	}
	
	private void calculateCumDayCurveOnLoad(UserSelectionSnapshot userSelection) throws Exception{
		String planMasterUid = "";
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"operationUid","wellUid","wellboreUid"};
		Object[] paramsValues = {userSelection.getOperationUid(),userSelection.getWellUid(),userSelection.getWellboreUid()};
				
		String strSql = "SELECT operationPlanMasterUid FROM OperationPlanMaster WHERE wellUid =:wellUid AND wellboreUid =:wellboreUid AND operationUid =:operationUid AND (isDeleted = FALSE OR isDeleted IS NULL)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if (lstResult.size() > 0){
			planMasterUid = (String) lstResult.get(0);
		}
		
		calculateCumDayCurve(planMasterUid);
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
	}

	public void init(CommandBean commandBean) throws Exception {
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
		if(StringUtils.isNotBlank(invocationKey) && invocationKey.equalsIgnoreCase("check_report_status")) {
			if(this.dvdPlanReportModule != null) {
				ReportJobStatus status = this.dvdPlanReportModule.getReportJobStatus();
				if(status.getStatus() == ReportJobStatus.STATUS_JOB_DONE){
					ReportFiles reportFiles = this.dvdPlanReportModule.reportGeneratedFiles;
					if(reportFiles != null) {
						//set the report file name to report types display name pattern
						reportFiles.setReportFile(reportFiles.getDisplayName());
					}	
				}
				this.dvdPlanReportModule.onCustomFilterInvoked(request, response, commandBean, "check_report_status");
			}
		}
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		
		String action = request.getParameter("action");
		if(StringUtils.isNotBlank(action) && action.equalsIgnoreCase("calculatePlanStartAndEndDateTime")) {
			Map<String, CommandBeanTreeNode> planPhaseNodes = targetCommandBeanTreeNode.getChild(OperationPlanPhase.class.getSimpleName());
			if(planPhaseNodes == null || planPhaseNodes.isEmpty()) {
				return;
			}
			List<CommandBeanTreeNode> list = new ArrayList<CommandBeanTreeNode>(planPhaseNodes.values());
			Collections.sort(list, new DvdPlanSequenceComparator());
			OperationPlanPhase firstNode = (OperationPlanPhase) list.get(0).getData();
			if(firstNode.getPlannedStartDatetime() == null) {
				commandBean.getSystemMessage().addInfo("First Planned Start Date/Time is missing. Please enter to proceed.");
				return;
			}
			
			calculateDVDPlannedStartAndEndDateTime(commandBean, targetCommandBeanTreeNode);
		}else if(StringUtils.isNotBlank(action) && action.equalsIgnoreCase("generateDvdPlanReport")) {
			if(this.dvdPlanReportModule != null) {
				this.dvdPlanReportModule.onSubmitForServerSideProcess(commandBean, request, targetCommandBeanTreeNode);
			}
		}
	}

	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
	
		if (object instanceof OperationPlanTask) {
			OperationPlanTask thisPlanTask = (OperationPlanTask) object;
			String planPhaseUid = thisPlanTask.getOperationPlanPhaseUid();
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DVD_PLAN_PHASE_DURATION))){
				saveDuration(planPhaseUid, node);
			}
		} else if (object instanceof OperationPlanPhase) {
			OperationPlanPhase thisPlanPhase = (OperationPlanPhase) object;
			String planMasterUid = thisPlanPhase.getOperationPlanMasterUid();
			calculateCumDayCurve(planMasterUid);
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{

		Object object = node.getData();
		
		if (object instanceof OperationPlanTask) {
			OperationPlanTask thisPlanTask = (OperationPlanTask) object;
			String planPhaseUid = thisPlanTask.getOperationPlanPhaseUid();
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DVD_PLAN_PHASE_DURATION))){
				saveDuration(planPhaseUid, node);
			}
		} else if (object instanceof OperationPlanPhase) {
			OperationPlanPhase thisPlanPhase = (OperationPlanPhase) object;
			String planMasterUid = thisPlanPhase.getOperationPlanMasterUid();
			calculateCumDayCurve(planMasterUid);
		}
		
	}
	
	private void calculateCumDayCurve(String planMasterUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"thisPlanMasterUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = planMasterUid;
				
		String strSql = "SELECT sum(p50Duration) as totalP50Duration, sum(duration) as totalDuration from OperationPlanPhase WHERE operationPlanMasterUid =:thisPlanMasterUid AND (isDeleted = false or isDeleted is null)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double cumCurveDay =0.0;
		
		if (lstResult.size() > 0){
			Object[] thisResult = (Object[]) lstResult.get(0);
			
			if (thisResult[0] != null && thisResult[1] != null){
				cumCurveDay = Double.parseDouble(thisResult[0].toString()) - Double.parseDouble(thisResult[1].toString());
			}
		}
		
		String strSql1 = "UPDATE OperationPlanMaster SET cumDayCurve =:cumCurveDay WHERE operationPlanMasterUid =:thisPlanMasterUid";
		String[] paramsFields1 = {"cumCurveDay", "thisPlanMasterUid"};
		Object[] paramsValues1 = {cumCurveDay, planMasterUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);

	}
	
	private void saveDuration(String planPhaseUid, CommandBeanTreeNode node) throws Exception  {
		Object object = node.getData();
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		if (object instanceof OperationPlanTask) {

			String[] paramsFields = {"thisPlanPhaseUid"};
			Object[] paramsValues = new Object[1]; paramsValues[0] = planPhaseUid;
					
			//String strSql = "SELECT sum(tlDuration) as totalTlDuration, sum(p10Duration) as totalP10Duration, sum(p50Duration) as totalP50Duration, sum(p90Duration) as totalP90Duration, sum(pmeanDuration) as totalPmeanDuration from OperationPlanTask WHERE operationPlanPhaseUid =:thisPlanPhaseUid AND (isDeleted = false or isDeleted is null)";
			String strSql = "SELECT sum(tlDuration) as totalTlDuration, sum(p10Duration) as totalP10Duration, sum(p50Duration) as totalP50Duration, sum(p90Duration) as totalP90Duration from OperationPlanTask WHERE operationPlanPhaseUid =:thisPlanPhaseUid AND (isDeleted = false or isDeleted is null)";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
			Double totalTlDuration = 0.0;
			Double totalP10Duration = 0.0;
			Double totalP50Duration = 0.0;
			Double totalP90Duration = 0.0;
//			Double totalPmeanDuration = 0.0;
			
			if (lstResult.size() > 0){
				Object[] thisResult = (Object[]) lstResult.get(0);
				
				if (thisResult[0] != null){
					totalTlDuration = Double.parseDouble(thisResult[0].toString());
				}
				if (thisResult[1] != null) {
					totalP10Duration = Double.parseDouble(thisResult[1].toString());
				}
				if (thisResult[2] != null) {
					totalP50Duration = Double.parseDouble(thisResult[2].toString());
				}				
				if (thisResult[3] != null) {
					totalP90Duration = Double.parseDouble(thisResult[3].toString());
				}
//				if (thisResult[4] != null) {
//					totalPmeanDuration = Double.parseDouble(thisResult[4].toString());
//				}
			}
			
//			String strSql1 = "UPDATE OperationPlanPhase SET tlDuration =:totalTlDuration, p10Duration =:totalP10Duration, p50Duration =:totalP50Duration, p90Duration =:totalP90Duration, pmeanDuration =:totalPmeanDuration WHERE operationPlanPhaseUid =:thisPhaseUid";
//			String[] paramsFields1 = {"totalTlDuration", "totalP10Duration", "totalP50Duration" ,"totalP90Duration", "totalPmeanDuration", "thisPhaseUid"};
//			Object[] paramsValues1 = {totalTlDuration, totalP10Duration, totalP50Duration ,totalP90Duration, totalPmeanDuration, planPhaseUid};
			String strSql1 = "UPDATE OperationPlanPhase SET tlDuration =:totalTlDuration, p10Duration =:totalP10Duration, p50Duration =:totalP50Duration, p90Duration =:totalP90Duration WHERE operationPlanPhaseUid =:thisPhaseUid";
			String[] paramsFields1 = {"totalTlDuration", "totalP10Duration", "totalP50Duration" ,"totalP90Duration", "thisPhaseUid"};
			Object[] paramsValues1 = {totalTlDuration, totalP10Duration, totalP50Duration ,totalP90Duration, planPhaseUid};
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);

			return;
		}
	}
	
	private void setActiveOperationPlanMaster(String operationUid) throws Exception {
		if (operationUid==null) return;
		String planMasterUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
		String queryString = "FROM OperationPlanMaster WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid";
		List<OperationPlanMaster> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid);
		for (OperationPlanMaster rec : rs) {
			if (planMasterUid.equals(rec.getOperationPlanMasterUid())) {
				rec.setDvdPlanStatus(true);
			} else {
				rec.setDvdPlanStatus(false);
			}
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
	
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN || commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT){
			if (object instanceof OperationPlanPhase) {
				OperationPlanPhase thisPlanPhase = (OperationPlanPhase) object;
				
				thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");
				Double planVsActual = 0.0;
				
				if (thisPlanPhase.getP50Duration()!=null && thisPlanPhase.getDuration()!=null) {
					planVsActual = thisPlanPhase.getP50Duration() - thisPlanPhase.getDuration();
				}	
				
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@planVsActual", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("planVsActual", planVsActual);
				
				//calculate cumulative 
				
				Double tld = thisPlanPhase.getTlDuration();
				Double p10d = thisPlanPhase.getP10Duration();
				Double p50d = thisPlanPhase.getP50Duration();
				Double p90d = thisPlanPhase.getP90Duration();
				Double planCost = thisPlanPhase.getPhaseBudget();
				Double pmeand = thisPlanPhase.getPmeanDuration();
				
				String planMasterUid = thisPlanPhase.getOperationPlanMasterUid();
				
				double cumTl = 0.0;
				double cumP10 = 0.0;
				double cumP50 = 0.0;
				double cumP90 = 0.0;
				double cumPlanCost = 0.0;
				double cumPmean = 0.0;
				
				String operationPlanMasterUid = (String) commandBean.getRoot().getDynaAttr().get("planMasterUid");
			
				if (planMasterUid.equals(operationPlanMasterUid)) {
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumTlDuration")!= null) cumTl = (Double) commandBean.getRoot().getDynaAttr().get("cumTlDuration");
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumP10Duration")!= null) cumP10 = (Double) commandBean.getRoot().getDynaAttr().get("cumP10Duration");
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumP50Duration")!= null) cumP50 = (Double) commandBean.getRoot().getDynaAttr().get("cumP50Duration");
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumP90Duration")!= null) cumP90 = (Double) commandBean.getRoot().getDynaAttr().get("cumP90Duration");
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumPlanCost")!= null) cumPlanCost = (Double) commandBean.getRoot().getDynaAttr().get("cumPlanCost");
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumPmeanDuration")!= null) cumPmean = (Double) commandBean.getRoot().getDynaAttr().get("cumPmeanDuration");
				}else {
					cumTl = 0.0;
					cumP10 = 0.0;
					cumP50 = 0.0;
					cumP90 = 0.0;
					cumPlanCost = 0.0;
					cumPmean = 0.0;
				}
				
				if (tld != null) cumTl += tld;
					
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "tl_duration");
					//thisConverter.setBaseValueFromUserValue(cumTl);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumTlDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumTlDuration", cumTl);
					commandBean.getRoot().getDynaAttr().put("cumTlDuration", cumTl);
			
				if (p10d != null) cumP10 += p10d;
				
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p10_duration");
					//thisConverter.setBaseValueFromUserValue(cumP10);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP10Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP10Duration", cumP10);
					commandBean.getRoot().getDynaAttr().put("cumP10Duration", cumP10);
			
				if (p50d != null) cumP50 += p50d;
				
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");
					//thisConverter.setBaseValueFromUserValue(cumP50);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP50Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP50Duration", cumP50);
					commandBean.getRoot().getDynaAttr().put("cumP50Duration", cumP50);
				
				if (p90d != null) cumP90 += p90d;
				
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p90_duration");
					//thisConverter.setBaseValueFromUserValue(cumP90);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP90Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP90Duration", cumP90);
					commandBean.getRoot().getDynaAttr().put("cumP90Duration", cumP90);
				
				if (planCost != null) cumPlanCost += planCost;
				
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "phase_budget");
					//thisConverter.setBaseValueFromUserValue(cumPlanCost);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumPlanCost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumPlanCost", cumPlanCost);
					commandBean.getRoot().getDynaAttr().put("cumPlanCost", cumPlanCost);
					
				if (pmeand != null) cumPmean += pmeand;
				
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "pmean_duration");
					//thisConverter.setBaseValueFromUserValue(cumPmean);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumPmeanDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumPmeanDuration", cumPmean);
					commandBean.getRoot().getDynaAttr().put("cumPmeanDuration", cumPmean);
					
				if (tld != null || p10d != null || p50d != null || p90d != null || pmeand != null || planCost != null) {
					commandBean.getRoot().getDynaAttr().put("planMasterUid", planMasterUid);
				}
				
				String selectedOperation = userSelection.getOperationUid();
				
				if (thisPlanPhase.getOperationPlanPhaseUid() != null) {
					
					//TO OBTAIN ACTUAL DURATION FROM ACTIVITY FOR SELECTED PLAN REFERENCE
					double actualDuration = 0.00;
					double activityDuration = 0.00;
					double plannedDuration = 0.00;
					double actualDepth = 0.00;
					double p_50_Duration = 0.00;
					double cumActualDuration = 0.00;
					//String dayplusminus = "0.0";
					
					DecimalFormat formatter = new DecimalFormat("#0.0");
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");

					actualDuration = this.dvdPlanHandler.calculateActualDuration(thisPlanPhase, selectedOperation);

					if (thisPlanPhase.getP50Duration() != null) p_50_Duration = thisPlanPhase.getP50Duration();

					thisConverter.setBaseValue(actualDuration);

					thisConverter.getUOMMapping().getUnitOutputPrecision();
					activityDuration = Math.round(thisConverter.getConvertedValue() *  Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision())) / Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision());
					plannedDuration = Math.round(p_50_Duration *  Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision())) / Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision());
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@actualDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("actualDuration", thisConverter.getConvertedValue());
					commandBean.getRoot().getDynaAttr().put("actualDuration", actualDuration);

					//Calculate Cumulative Actual Duration
					if (commandBean.getRoot().getDynaAttr().containsKey("cumActualDuration")) {
						cumActualDuration = (Double) commandBean.getRoot().getDynaAttr().get("cumActualDuration");
					}
					else {
						cumActualDuration = 0.00;
					}

					cumActualDuration += activityDuration;
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumActualDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumActualDuration", cumActualDuration);
					commandBean.getRoot().getDynaAttr().put("cumActualDuration", cumActualDuration);
						//}
					//}
					
					//TO CALCULATE DAY +/- FROM ACTUAL/PLANNED DURATION
					if (activityDuration > 0) {
						if (thisConverter.isUOMMappingAvailable()){
							node.setCustomUOM("@dayplusminus", thisConverter.getUOMMapping());
						}
						node.getDynaAttr().put("dayplusminus",this.dvdPlanHandler.calculateDayPlusMinus(activityDuration, plannedDuration));
					}
					
					//TO OBTAIN ACTUAL DEPTH FROM ACTIVITY FOR SELECTED PLAN REFERENCE
					thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");

					actualDepth = this.dvdPlanHandler.calculateActualDepth(thisPlanPhase, selectedOperation);

					thisConverter.setBaseValue(actualDepth);

					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@actualDepth", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("actualDepth", thisConverter.getConvertedValue());
					commandBean.getRoot().getDynaAttr().put("actualDepth", actualDepth);
					
					//TO OBTAIL ACTUAL COST FROM REPORT DAILY AND ACTIVITY, BASED ON SELECTED PLAN REFERENCE
					Double actualCost = this.dvdPlanHandler.calculateActualCost(thisPlanPhase, selectedOperation, request);
					thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
					thisConverter.setBaseValue(actualCost);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@actualCost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("actualCost", thisConverter.getConvertedValue());
					commandBean.getRoot().getDynaAttr().put("actualCost", actualCost);
				}				
			}
		}
		
		if (object instanceof LookupPhaseCode){
			LookupPhaseCode phasecode = (LookupPhaseCode) object;

			String selectedOperation = (String) commandBean.getRoot().getDynaAttr().get("operationUid");
			String phaseshortcode = phasecode.getShortCode();
			
			if (StringUtils.isNotBlank(selectedOperation)) {
				String strSql = "SELECT sum(a.activityDuration) as totalActualTime from Activity a, Daily d " +
						"WHERE a.operationUid =:operationUid " +
						"AND a.phaseCode =:phaseshortcode " +
						"AND (a.isDeleted = false or a.isDeleted is null) " +
						"AND (d.isDeleted = false or d.isDeleted is null) " +
						"AND (a.dayPlus is null or a.dayPlus = 0) " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null)";
				String[] paramNames = {"operationUid", "phaseshortcode"};
				Object[] paramValues = {selectedOperation, phaseshortcode};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
				
				double quickestActualTime = 0.00;
				thisConverter.setReferenceMappingField(OperationPlanPhase.class, "tl_duration");
				
				if (lstResult.size() > 0) {
					if (lstResult.get(0) != null){					
						quickestActualTime = Double.parseDouble(lstResult.get(0).toString());
					}
				}
				//thisConverter.setBaseValueFromUserValue(quickestActualTime);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@quickestActualTime", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("quickestActualTime", quickestActualTime);
				
				String strSql2 = "SELECT sum(a.activityDuration) as totalActualTime from Activity a, Daily d " +
						"WHERE a.operationUid =:operationUid " +
						"AND a.phaseCode =:phaseshortcode " +
						"AND d.dailyUid = a.dailyUid " +
						"AND a.classCode ='P' " +
						"AND (a.isDeleted = false or a.isDeleted is null) " +
						"AND (d.isDeleted = false or d.isDeleted is null) " +
						"AND (a.dayPlus is null or a.dayPlus = 0) " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null)";
				String[] paramNames2 = {"operationUid", "phaseshortcode"};
				Object[] paramValues2 = {selectedOperation, phaseshortcode};
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramNames2, paramValues2);
				
				double bestTime = 0.00;
				thisConverter.setReferenceMappingField(OperationPlanPhase.class, "tl_duration");
				
				if (lstResult2.size() > 0) {
					if (lstResult2.get(0) != null){					
						bestTime = Double.parseDouble(lstResult2.get(0).toString());
					}
				}
				//thisConverter.setBaseValueFromUserValue(bestTime);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@bestTime", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("bestTime", bestTime);
				
			}
		}
		
		if(object instanceof OperationPlanMaster) {
			OperationPlanMaster rec = (OperationPlanMaster) object;
			String query = "from FileManagerFiles where (isDeleted = false or isDeleted is null) and (popupAttachment = false or popupAttachment is null) and (attachmentMode = :attachmentMode or attachmentMode is null) and attachToModule = :module and attachToOperationUid = :operationUid";
			List<FileManagerFiles> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, new String[]{"attachmentMode", "module", "operationUid"}, new Object[] {FileManagerUtils.ATTACHMENT_MODE_DEFAULT, commandBean.getControllerBeanName(), rec.getOperationUid()});
			
			Boolean hasAttachmentFiles = false;
			for(FileManagerFiles dbFile: result){
				File attachedFile = FileManagerUtils.getAttachedFile(dbFile);
				hasAttachmentFiles = attachedFile.exists();
				if(hasAttachmentFiles) {
					break;
				}
			}
			node.getDynaAttr().put("hasAttachmentFiles", hasAttachmentFiles);
			
			if(commandBean instanceof BaseCommandBean) {
				BaseCommandBean bean = (BaseCommandBean) commandBean;
				DataNodeAllowedAction allowAction = bean.getDataNodeAllowedAction();
				if(allowAction != null && allowAction instanceof DvdPlanLocker) {
					if(rec.getIsLocked() == null || !rec.getIsLocked()) {
						commandBean.getSystemMessage().addInfo("This DVD Plan screen is currently not locked.");
					}else if(rec.getIsLocked() != null && rec.getIsLocked()) {
						commandBean.getSystemMessage().addInfo("This DVD Plan screen is locked. Only PIC with Lock/Unlock access can unlock record.");
					}
				}
			}
		}
	}

	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof OperationPlanMaster) {
			OperationPlanMaster planMaster = (OperationPlanMaster) node.getData();
			planMaster.setOperationUid(userSelection.getOperationUid());
		}
	}
	
	private class DvdPlanSequenceComparator implements Comparator<CommandBeanTreeNode> {
		  
	    @Override
	    public int compare(CommandBeanTreeNode rec1, CommandBeanTreeNode rec2) {
	    	try {
	    		Integer seq1 = ((OperationPlanPhase) rec1.getData()).getSequence();
	    		Integer seq2 = ((OperationPlanPhase) rec2.getData()).getSequence();
	    		
	    		if(seq1 == null || seq2 == null) {
		    		return 0;
		    	}
		    	
		    	return seq1.compareTo(seq2);

	    	}catch(Exception e) {
	    		return 0;
	    	}
	    }
	}
	
	private void calculateDVDPlannedStartAndEndDateTime(CommandBean commandBean, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		Map<String, CommandBeanTreeNode> dvdPlanPhases = targetCommandBeanTreeNode.getChild(OperationPlanPhase.class.getSimpleName());
		if(dvdPlanPhases == null || dvdPlanPhases.isEmpty()) {
			return;
		}
		
		List<CommandBeanTreeNode> list = new ArrayList<CommandBeanTreeNode>(dvdPlanPhases.values());
		Collections.sort(list, new DvdPlanSequenceComparator());
		Date nextPlannedStartDatetime = null;
		
		for(CommandBeanTreeNode rec : list) {
			OperationPlanPhase data = (OperationPlanPhase) rec.getData();
			
			if(nextPlannedStartDatetime != null) {
				data.setPlannedStartDatetime(nextPlannedStartDatetime);
			}
			
			CustomFieldUom uomConverter = new CustomFieldUom(OperationPlanPhase.class, "p50Duration");
			uomConverter.setBaseValueFromUserValue((data.getP50Duration() == null ? 0.00 : data.getP50Duration()));
			long calculatedPlannedEndDatetime = data.getPlannedStartDatetime().getTime() + (new Double(uomConverter.getBasevalue()).longValue() * 1000);
			data.setPlannedEndDatetime(new Date(calculatedPlannedEndDatetime));
			nextPlannedStartDatetime = data.getPlannedEndDatetime();
		}
	}
}
