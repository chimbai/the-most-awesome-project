package com.idsdatanet.d2.drillnet.graph;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class CVDGraphDataGenerator implements ReportDataGenerator {

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		QueryProperties qp = new QueryProperties();
	    qp.setUomConversionEnabled(false);
	    
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
		Double minCost = 0.0;
		Double maxCost = 0.0;
		Double minDepth = 0.0;
		Double maxDepth = 0.0;
		Double kickOffMdMsl = 0.0;
		CustomFieldUom costConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "daycost");
		CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), Activity.class, "depthMdMsl");
		String currentWellboreUid = userContext.getUserSelection().getWellboreUid();
		String currentOperationPlanMasterUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(userContext.getUserSelection().getOperationUid());
		
		if(StringUtils.isNotBlank(currentWellboreUid) && currentWellboreUid != null){
			Wellbore wellbore = ApplicationUtils.getConfiguredInstance().getCachedWellbore(currentWellboreUid);
			
			if(wellbore.getKickoffMdMsl() != null){
				thisConverter.setBaseValue(wellbore.getKickoffMdMsl());
				kickOffMdMsl = thisConverter.getConvertedValue();
			}
		}
		
		ReportDataNode thisReportNode = reportDataNode.addChild("operationPlanPhase");
		thisReportNode.addProperty("p10Cost","0.0");
		thisReportNode.addProperty("cumP10Cost","0.0");
		thisReportNode.addProperty("p90Cost","0.0");
		thisReportNode.addProperty("cumP90Cost","0.0");
		thisReportNode.addProperty("phaseBudget","0.0");
		thisReportNode.addProperty("cumPhaseBudget","0.0");
		thisReportNode.addProperty("phaseCost","0.0");
		thisReportNode.addProperty("cumPhaseCost","0.0");
		if(costConverter.isUOMMappingAvailable() ){
			thisReportNode.addProperty("costUomSymbol", costConverter.getUomSymbol());
			thisReportNode.addProperty("costUomLabel", costConverter.getUomLabel());
		}
		thisReportNode.addProperty("depthMdMsl",kickOffMdMsl.toString());
		if(thisConverter.isUOMMappingAvailable() ){
			thisReportNode.addProperty("depthUomSymbol", thisConverter.getUomSymbol());
			thisReportNode.addProperty("depthUomLabel", thisConverter.getUomLabel());
		}
		
		// Collect Planned Cost vs Depth Data
		String strSql = "SELECT p10Cost, p90Cost, phaseBudget, phaseCost, depthMdMsl FROM OperationPlanPhase " +
						"WHERE (isDeleted = FALSE OR isDeleted IS NULL) " +
						"AND operationPlanMasterUid = :operationPlanMasterUid " +
						"ORDER BY sequence, phaseSequence";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationPlanMasterUid", currentOperationPlanMasterUid, qp);
		
		if(lstResult != null && lstResult.size() > 0){
			
			Double cumP10Cost = 0.0;
			Double cumP90Cost = 0.0;
			Double cumPhaseBudget = 0.0;
			Double cumPhaseCost = 0.0;
			
			for(Object objResult: lstResult){
				//due to result is in array, cast the result to array first and read 1 by 1
				Object[] plannedObject = (Object[]) objResult;
				
				Double p10Cost = 0.0;
				Double p90Cost = 0.0;
				Double phaseBudget = 0.0;
				Double phaseCost = 0.0;
				Double depthMdMsl = 0.0;
				
				if(plannedObject[0]!=null && StringUtils.isNotBlank(plannedObject[0].toString())) p10Cost = Double.parseDouble(plannedObject[0].toString());
				if(plannedObject[1]!=null && StringUtils.isNotBlank(plannedObject[1].toString())) p90Cost = Double.parseDouble(plannedObject[1].toString());
				if(plannedObject[2]!=null && StringUtils.isNotBlank(plannedObject[2].toString())) phaseBudget = Double.parseDouble(plannedObject[2].toString());
				if(plannedObject[3]!=null && StringUtils.isNotBlank(plannedObject[3].toString())) phaseCost = Double.parseDouble(plannedObject[3].toString());
				if(plannedObject[4]!=null && StringUtils.isNotBlank(plannedObject[4].toString())) depthMdMsl = Double.parseDouble(plannedObject[4].toString());
				
				if(p10Cost != null){
					costConverter.setBaseValue(p10Cost);
					p10Cost = costConverter.getConvertedValue();
					cumP10Cost += p10Cost;
					if(cumP10Cost > maxCost) maxCost = cumP10Cost;
				}
				
				if(p90Cost != null){
					costConverter.setBaseValue(p90Cost);
					p90Cost = costConverter.getConvertedValue();
					cumP90Cost += p90Cost;
					if(cumP90Cost > maxCost) maxCost = cumP90Cost;
				}
				
				if(phaseBudget != null){
					costConverter.setBaseValue(phaseBudget);
					phaseBudget = costConverter.getConvertedValue();
					cumPhaseBudget += phaseBudget;
					if(cumPhaseBudget > maxCost) maxCost = cumPhaseBudget;
				}
				
				if(phaseCost != null){
					costConverter.setBaseValue(phaseCost);
					phaseCost = costConverter.getConvertedValue();
					cumPhaseCost += phaseCost;
					if(cumPhaseCost > maxCost) maxCost = cumPhaseCost;
				}
				
				if(depthMdMsl != null){
					thisConverter.setBaseValue(depthMdMsl);
					depthMdMsl = thisConverter.getConvertedValue();
					if(depthMdMsl > maxDepth) maxDepth = depthMdMsl;
				}
				
				thisReportNode = reportDataNode.addChild("operationPlanPhase");
				thisReportNode.addProperty("p10Cost",p10Cost.toString());
				thisReportNode.addProperty("cumP10Cost",cumP10Cost.toString());
				thisReportNode.addProperty("p90Cost",p90Cost.toString());
				thisReportNode.addProperty("cumP90Cost",cumP90Cost.toString());
				thisReportNode.addProperty("phaseBudget",phaseBudget.toString());
				thisReportNode.addProperty("cumPhaseBudget",cumPhaseBudget.toString());
				thisReportNode.addProperty("phaseCost",phaseCost.toString());
				thisReportNode.addProperty("cumPhaseCost",cumPhaseCost.toString());
				if(costConverter.isUOMMappingAvailable() ){
					thisReportNode.addProperty("costUomSymbol", costConverter.getUomSymbol());
					thisReportNode.addProperty("costUomLabel", costConverter.getUomLabel());
				}
				thisReportNode.addProperty("depthMdMsl",depthMdMsl.toString());
				if(thisConverter.isUOMMappingAvailable() ){
					thisReportNode.addProperty("depthUomSymbol", thisConverter.getUomSymbol());
					thisReportNode.addProperty("depthUomLabel", thisConverter.getUomLabel());
				}
			} 
		}

		// Collect Actual Cost vs Depth Data
		String dailyReportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(userContext.getUserSelection().getOperationUid());
		strSql = "FROM ReportDaily where (isDeleted = FALSE OR isDeleted IS NULL) " +
				 "AND operationUid = :operationUid " +
				 "AND reportDatetime <= :userDate " +
				 "AND reportType = :reportType " +
				 "ORDER BY reportDatetime";
		String[] paramsFields = {"operationUid","userDate","reportType"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid(),todayDate,dailyReportType};
		List dailyLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		if(dailyLists != null && dailyLists.size() > 0){
			
			thisReportNode = reportDataNode.addChild("report");
			thisReportNode.addProperty("dayCost", "0.0");
			if(costConverter.isUOMMappingAvailable() ){
				thisReportNode.addProperty("costUomSymbol", costConverter.getUomSymbol());
				thisReportNode.addProperty("costUomLabel", costConverter.getUomLabel());
			}
			thisReportNode.addProperty("depthMdMsl",kickOffMdMsl.toString());
			if(thisConverter.isUOMMappingAvailable() ){
				thisReportNode.addProperty("depthUomSymbol", thisConverter.getUomSymbol());
				thisReportNode.addProperty("depthUomLabel", thisConverter.getUomLabel());
			}
			
			for(Iterator i=dailyLists.iterator(); i.hasNext();){
				ReportDaily reportDaily = (ReportDaily)i.next();
				
				strSql = "FROM Activity " +
						 "WHERE (isDeleted = FALSE OR isDeleted IS NULL) " +
						 "AND dailyUid = :dailyUid " +
						 "AND operationUid = :operationUid " +
						 "AND ((dayPlus IS NULL OR dayPlus=0) AND (carriedForwardActivityUid IS NULL OR carriedForwardActivityUid='') AND (isSimop <> 1 OR isSimop IS NULL) AND (isOffline <> 1 OR isOffline IS NULL)) " +
						 "ORDER BY endDatetime"; 
				String[] paramsFields2 = {"dailyUid", "operationUid"};
				Object[] paramsValues2 = {reportDaily.getDailyUid(), userContext.getUserSelection().getOperationUid()};
				
				List activityLists = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
				
				Double depthMdMsl = 0.0;
				
				if (activityLists != null && activityLists.size() > 0){					
					for(Iterator it=activityLists.iterator(); it.hasNext();){
						Activity activity = (Activity)it.next();
						
						if(activity.getDepthMdMsl() != null){
							thisConverter.setBaseValue(activity.getDepthMdMsl());
							depthMdMsl = thisConverter.getConvertedValue();
							if(depthMdMsl > maxDepth) maxDepth = depthMdMsl;
						}
					}
					
					Double dayCost = CommonUtil.getConfiguredInstance().calculateCummulativeCost(reportDaily.getDailyUid(), reportDaily.getReportType());
					
					if(dayCost != null && depthMdMsl != null){
						costConverter.setBaseValue(dayCost);
						dayCost = costConverter.getConvertedValue();
						if(dayCost > maxCost) maxCost = dayCost;
						
						thisReportNode = reportDataNode.addChild("report");
						thisReportNode.addProperty("dayCost", dayCost.toString());
						if(costConverter.isUOMMappingAvailable() ){
							thisReportNode.addProperty("costUomSymbol", costConverter.getUomSymbol());
							thisReportNode.addProperty("costUomLabel", costConverter.getUomLabel());
						}
						thisReportNode.addProperty("depthMdMsl",depthMdMsl.toString());
						if(thisConverter.isUOMMappingAvailable() ){
							thisReportNode.addProperty("depthUomSymbol", thisConverter.getUomSymbol());
							thisReportNode.addProperty("depthUomLabel", thisConverter.getUomLabel());
						}
					}
				}
			}
		}		
		
		maxDepth = maxDepth + (maxDepth * 0.02);
		maxCost = maxCost + (maxCost * 0.01);
		
		thisReportNode = reportDataNode.addChild("scaleValue");
		thisReportNode.addProperty("minDepth", minDepth.toString());
		thisReportNode.addProperty("maxDepth", maxDepth.toString());
		thisReportNode.addProperty("minCost", minCost.toString());
		thisReportNode.addProperty("maxCost", maxCost.toString());
	}
}
