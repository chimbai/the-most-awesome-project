package com.idsdatanet.d2.drillnet.reportDaily;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class FwrCompanyRepresentativeReportDataGenerator implements ReportDataGenerator {
	private Map<String, String> comList = null;
	private Map<String, String> fieldLists = null;
	
	public void setFieldLists(Map<String, String> fieldLists){
		this.fieldLists = fieldLists;
	}	
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
		
	}

	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation validation) throws Exception {
		this.comList = this.getCompanyList();
		for (Entry<String, String> a : this.fieldLists.entrySet()){
			this.generateCompanyRepGrouping(userContext, reportDataNode, a.getKey(), a.getValue());
		}
	}

	private void generateCompanyRepGrouping(UserContext userContext, ReportDataNode reportDataNode, String com, String type) throws Exception{
	//	SimpleDateFormat sdf = new SimpleDateFormat(ApplicationConfig.getConfiguredInstance().getApplicationDateFormat());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		String strSql = "SELECT reportDatetime, " + com + " FROM ReportDaily where (isDeleted = false OR isDeleted is null) " +
				"AND reportType!='DGR' " +
				"AND operationUid=:operationUid ORDER BY reportDatetime";
				
		String[] paramsFields = {"operationUid"};
		Object[] paramsValues = {userContext.getUserSelection().getOperationUid()};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		String recData = "";
		Date StartD = null;
		Date CurD = null;
		if (lstResult.size()>0) {
			for(int i = 0; i < lstResult.size(); i++ ){
				Object[] rd = (Object[]) lstResult.get(i);
				Date day = (Date) rd[0];
				String company = (String) rd[1];
				
				String checkedData = company;
				if ("com".equals(type)) {
					if (comList.containsKey(company)) {
						checkedData = comList.get(company);
					}else {
						checkedData = "";
					}
				}
				
				if (i==0) {
					recData = checkedData;
					StartD = day;
					CurD = day;
				}else {
					if (StringUtils.isEmpty(checkedData)) checkedData = "";
					if (recData != null && recData.equals(checkedData)) {
						CurD = day;
					}else {
						ReportDataNode thisReportNode = reportDataNode.addChild("CompanyRep");
						thisReportNode.addProperty("field", com);
						thisReportNode.addProperty("comR", recData);
						thisReportNode.addProperty("from", sdf.format(StartD));
						thisReportNode.addProperty("to", sdf.format(CurD));
						recData = checkedData;
						StartD = day;
						CurD = day;
					}
				}
			}
			ReportDataNode thisReportNode = reportDataNode.addChild("CompanyRep");
			thisReportNode.addProperty("field", com);
			thisReportNode.addProperty("comR", recData);
			thisReportNode.addProperty("from", sdf.format(StartD));
			thisReportNode.addProperty("to", sdf.format(CurD));
		}
	}
	
	public Map<String, String> getCompanyList() throws Exception{
		Map<String, String> comList = new HashMap();
		
		String sql = "FROM LookupCompany WHERE (isDeleted = false OR isDeleted IS NULL) ";
        List<LookupCompany> lookupCompanyList = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
         
        if (lookupCompanyList.size() >0) {
        	for (LookupCompany lc : lookupCompanyList) {
        		comList.put(lc.getLookupCompanyUid(), lc.getCompanyName());
        	}
        }

		return comList;
	}
	
}
