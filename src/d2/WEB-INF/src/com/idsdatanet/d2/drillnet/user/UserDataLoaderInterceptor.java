package com.idsdatanet.d2.drillnet.user;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DynamicAttributeDataFilter;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class UserDataLoaderInterceptor extends DynamicAttributeDataFilter {

	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	@Override
	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if (conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		
		String customCondition = "";
		String filterCondition = "";
		String filterRegionCondition = "";
		String showBlockedUser = (String) commandBean.getRoot().getDynaAttr().get("showBlockedUser");
		if (showBlockedUser==null) showBlockedUser = "";
		if (!"show".equals(showBlockedUser)) customCondition = " and (isblocked=false or isblocked is null) ";
		
		String aclGroupUid= (String) commandBean.getRoot().getDynaAttr().get("aclGroupUid");
		if (aclGroupUid==null) aclGroupUid = "";

		if ("ALL".equals(aclGroupUid)) filterCondition = "";		
		else {
			filterCondition = "and userUid in (select u.userUid from UserAclGroup u where (u.isDeleted = false or u.isDeleted is null) and u.aclGroupUid = :aclGroupUid)";
			query.addParam("aclGroupUid", aclGroupUid);		
		}
		
		String userRegion = (String) commandBean.getRoot().getDynaAttr().get("userRegion");
		if (userRegion == null) userRegion = "";
		if ("".equals(userRegion)) filterRegionCondition = "";
		else {
			filterRegionCondition = " and userRegion = :userRegion";
			query.addParam("userRegion", userRegion);
		}
		
		customCondition = customCondition + filterCondition + filterRegionCondition;
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}
	
	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		if ("User".equals(meta.getTableClass().getSimpleName())) {
			String orderBy = (String) commandBean.getRoot().getDynaAttr().get("userSortOrder");
			if (StringUtils.isBlank(orderBy)) {
				orderBy = "fname";
				commandBean.getRoot().getDynaAttr().put("userSortOrder", orderBy);
			} 
			return orderBy; 
		}
		return null;
	}
	
}
