package com.idsdatanet.d2.drillnet.personnelonsite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.PersonnelOnSite;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class PersonnelOnSiteSummaryDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		return true;
	}
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if (meta.getTableClass().equals(PersonnelOnSiteSummary.class)) {

			//UserSession userSession = UserSession.getInstance(request);
			//String strSql = "select c.lookupCompanyUid from PersonnelOnSite pob, LookupCompany c where pob.crewCompany = c.lookupCompanyUid and (pob.isDeleted = false or pob.isDeleted is null) " +
			//		"and (c.isDeleted = false or c.isDeleted is null) and pob.dailyUid =:dailyUid group by c.lookupCompanyUid";
			
			List<Object> output_maps = new ArrayList<Object>();
			String strSql = "Select crewCompany, sum(pax), sum(totalWorkingHours), dailyUid, operationUid, wellboreUid, wellUid, location from PersonnelOnSite where " +
					"(isDeleted = false or isDeleted is null) and dailyUid =:dailyUid group by crewCompany, dailyUid, operationUid, wellboreUid, wellUid, location";
			List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "dailyUid", userSelection.getDailyUid());
			
			if (!items.isEmpty()) {
				for(Iterator i = items.iterator(); i.hasNext(); ){
					Object[] obj_array = (Object[]) i.next();
					
					PersonnelOnSiteSummary pobSummary = new PersonnelOnSiteSummary();
					
					pobSummary.setCrewCompany(this.nullToEmptyString(obj_array[0]));

					if (obj_array[1] != null)  {
						pobSummary.setPax(Integer.parseInt(obj_array[1].toString()));
					}
					
					if (obj_array[2] != null){
						pobSummary.setTotalWorkingHours(Double.parseDouble(obj_array[2].toString()));
					}
					
					pobSummary.setDailyUid(obj_array[3].toString());
					pobSummary.setOperationUid(obj_array[4].toString());
					pobSummary.setWellboreUid(obj_array[5].toString());
					pobSummary.setWellUid(obj_array[6].toString());
					
					if (obj_array[7] != null){
						pobSummary.setLocation(obj_array[7].toString());
					}
					
					output_maps.add(pobSummary);
				
				}
				return output_maps;
			}
			
		}
		
		return null;
	}
	
	private String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}
	
	public void afterDataNodeLoad(CommandBean commandBean,TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();

		if(object instanceof PersonnelOnSiteSummary) {
			PersonnelOnSiteSummary pobSummary = (PersonnelOnSiteSummary) object;

			if (pobSummary.getTotalWorkingHours() != null){
				double totalhours = pobSummary.getTotalWorkingHours();
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, PersonnelOnSite.class, "total_working_hours");
				thisConverter.setBaseValue(totalhours);
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@totalWorkingHours", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalWorkingHours", thisConverter.getConvertedValue());
			}
			
		}
	}
}


