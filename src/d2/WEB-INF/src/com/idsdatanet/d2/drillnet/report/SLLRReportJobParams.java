package com.idsdatanet.d2.drillnet.report;

import com.idsdatanet.d2.core.job.JobContext;

/**
 * Wrapper class for JobContext and ReportJobParams
 */

public class SLLRReportJobParams {
	private JobContext jobContext = null;
	private ReportJobParams reportJobParams = null;
	
	public JobContext getJobContext() {
		return jobContext;
	}
	
	public void setJobContext(JobContext jobContext) {
		this.jobContext = jobContext;
	}
	
	public ReportJobParams getReportJobParams() {
		return reportJobParams;
	}
	
	public void setReportJobParams(ReportJobParams reportJobParams) {
		this.reportJobParams = reportJobParams;
	}
}
