package com.idsdatanet.d2.drillnet.personnelonsite;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.CommonLookup;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.MenuItem;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.RigInformation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.drillnet.commonLookup.CommonLookupUtil;


public class SupplierKpiCrewDataGenerator implements ReportDataGenerator {
	private List<String> serviceCode;
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void setServiceCode(List<String> serviceCode)
	{
		if(serviceCode != null){
			this.serviceCode = new ArrayList<String>();
			for(String value: serviceCode){
				this.serviceCode.add(value.trim().toUpperCase());
			}
		}
	}
	
	public List<String> getServiceCode() {
		return this.serviceCode;
	}
	
	public void generateData(UserContext userContext, ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
	// TODO Auto-generated method stub
					
			String operationUid = userContext.getUserSelection().getOperationUid();
			Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class,operationUid);
			/*List dailyUidList = CommonUtil.getConfiguredInstance().getDailyForOperationProcurement(operationUid,"DDR",operation.getSpudDate(),operation.getRigOffHireDate());
			
			if(dailyUidList==null)
			{
				throw new Exception("No day in operation");
			}*/
					
			String companyUid = "";
			String companyUid2 = "";
			String companyServiceProvided = "";
			String companyServiceProvided2 = "";
			String dailyUid = "";
			String dailyUid2 = "";
			String rigUid = "";
			double sumHour = 24;
			double belowExpectedDayPercent = 0;
			double asExpectedDayPercent = 0;
			double aboveExpectedDayPercent = 0;
			double belowExpectedNightPercent = 0;
			double asExpectedNightPercent = 0;
			double aboveExpectedNightPercent = 0;
			String countRecord;
			List<Object> resultTotal = new ArrayList();
			List<Object> supplierTotalRecordsPerDay = new ArrayList();
					
			
			Date startDate=null;
			Date endDate=null;
			if(operation.getSpudDate()!=null && operation.getRigOffHireDate()!=null)
			{
				startDate = operation.getSpudDate();
				endDate = operation.getRigOffHireDate();
			}
			if(operation.getSpudDate()!=null && operation.getRigOffHireDate()==null)
			{
				startDate = operation.getSpudDate();
				endDate = CommonUtil.getConfiguredInstance().getFirstOrLastDateFromOperation(operationUid, "DDR", "DESC");
			}
			if(operation.getSpudDate()==null && operation.getRigOffHireDate()!=null)
			{
				startDate = CommonUtil.getConfiguredInstance().getFirstOrLastDateFromOperation(operationUid, "DDR", "ASC");
				endDate = operation.getRigOffHireDate();
				
			}
			if(operation.getSpudDate()==null && operation.getRigOffHireDate()==null)
			{
				startDate = CommonUtil.getConfiguredInstance().getFirstOrLastDateFromOperation(operationUid, "DDR", "ASC");
				endDate = CommonUtil.getConfiguredInstance().getFirstOrLastDateFromOperation(operationUid, "DDR", "DESC");
			}
			
			//GEt spud date in date only
			Calendar thisStartDateTimeObj = Calendar.getInstance();
			Calendar thisStartDateObj = Calendar.getInstance();
			
			thisStartDateTimeObj.setTime(startDate);
			
			thisStartDateObj.set(Calendar.YEAR, thisStartDateTimeObj.get(Calendar.YEAR));
			thisStartDateObj.set(Calendar.MONTH, thisStartDateTimeObj.get(Calendar.MONTH));
			thisStartDateObj.set(Calendar.DAY_OF_MONTH, thisStartDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisStartDateObj.set(Calendar.HOUR_OF_DAY, 0);
			thisStartDateObj.set(Calendar.MINUTE, 0);
			thisStartDateObj.set(Calendar.SECOND, 0);
			thisStartDateObj.set(Calendar.MILLISECOND, 0);
			
			startDate = thisStartDateObj.getTime();
			
			Calendar thisEndDateTimeObj = Calendar.getInstance();
			Calendar thisEndDateObj = Calendar.getInstance();
			
			thisEndDateTimeObj.setTime(endDate);
			
			thisEndDateObj.set(Calendar.YEAR, thisEndDateTimeObj.get(Calendar.YEAR));
			thisEndDateObj.set(Calendar.MONTH, thisEndDateTimeObj.get(Calendar.MONTH));
			thisEndDateObj.set(Calendar.DAY_OF_MONTH, thisEndDateTimeObj.get(Calendar.DAY_OF_MONTH));
			thisEndDateObj.set(Calendar.HOUR_OF_DAY, 0);
			thisEndDateObj.set(Calendar.MINUTE, 0);
			thisEndDateObj.set(Calendar.SECOND, 0);
			thisEndDateObj.set(Calendar.MILLISECOND, 0);
			
			endDate = thisEndDateObj.getTime();
			
			if(this.getServiceCode() != null)
			{
				//GET SERVICECODE
				String strInCondition = "";
				
				for(String value:this.serviceCode)
				{
					strInCondition = strInCondition + "'" + value.toString() + "',";
				}
				
				strInCondition = StringUtils.substring(strInCondition, 0, -1);
				strInCondition = StringUtils.upperCase(strInCondition);
				
				String sqlTotalHour = "SELECT p.crewCompany,lc.companyName, p.dailyUid, count(*), p.companyServiceProvided FROM LookupCompany lc, PersonnelOnSite p, ReportDaily rd "
					+ "WHERE p.companyServiceProvided IN (" + strInCondition + ") "
					+ "AND p.pax!=0 "
					+ "AND p.crewCompany = lc.lookupCompanyUid "
					+ "AND rd.dailyUid = p.dailyUid "
					+ "AND rd.reportType = 'DDR' "
					+ "AND p.operationUid = :operationUid "
					+ "AND (p.isDeleted IS NULL OR p.isDeleted=FALSE) "
					+ "AND (lc.isDeleted IS NULL OR lc.isDeleted=FALSE) "
					+ "AND (rd.isDeleted IS NULL OR rd.isDeleted=FALSE) AND rd.reportDatetime >= :startDate "
					+ "AND rd.reportDatetime <=:endDate "
					+ "GROUP BY p.crewCompany,p.companyServiceProvided, p.dailyUid "
					+ "ORDER BY lc.companyName, p.crewCompany,p.companyServiceProvided";
					
					String[] paramNames = {"operationUid","startDate","endDate"};
					Object[] paramValues = {operationUid,startDate,endDate};
							
					List<Object> totalHour = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlTotalHour,paramNames,paramValues);
						if (totalHour.size()>0){
											
							for (int i = 0;i < totalHour.size();i++)
								{	
									Object[] obj = (Object[]) totalHour.get(i);
									companyUid = (obj[0]!=null)?obj[0].toString():"";
									dailyUid = ((obj[2]!=null)?obj[2].toString():"");
									countRecord = ((obj[3]!=null)?obj[3].toString():"");
									companyServiceProvided = (obj[4]!=null)?obj[4].toString():"";
									
									if(i+1 < totalHour.size())
										{
											Object[] obj2 = (Object[]) totalHour.get(i+1);
											companyUid2 = (obj2[0]!=null)?obj2[0].toString():"";
											dailyUid2 = ((obj2[2]!=null)?obj2[2].toString():"");
											companyServiceProvided2 = (obj2[4]!=null)?obj2[4].toString():"";
											
											if (StringUtils.equals(companyUid, companyUid2) && !StringUtils.equals(dailyUid,dailyUid2) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
											{
												sumHour+= 24;
											}
											else 
											{
												Object[] objTemp = new Object[3];
												objTemp[0] = companyUid;
												objTemp[1] = sumHour;
												objTemp[2] = companyServiceProvided;
												resultTotal.add(objTemp);
												sumHour=24;
											}
										}
									else
										{
											Object[] objTemp = new Object[3];
											objTemp[0] = companyUid;
													
											if(StringUtils.equals(companyUid, companyUid2) && !StringUtils.equals(dailyUid, dailyUid2) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
											{	
												sumHour+= 24;
												objTemp[1] = sumHour;
											}
											else if(StringUtils.equals(companyUid, companyUid2) && StringUtils.equals(dailyUid, dailyUid2) && StringUtils.equals(companyServiceProvided, companyServiceProvided2))
											{
												objTemp[1] = sumHour;
											}
											else
											{
												sumHour = 24;
												objTemp[1] = sumHour;
											}
											objTemp[2] = companyServiceProvided;
											resultTotal.add(objTemp);
										}
									Object[] records = new Object[4];
									records[0] = companyUid;
									records[1] = dailyUid;
									records[2] = countRecord;
									records[3] = companyServiceProvided;
									supplierTotalRecordsPerDay.add(records);
								}
						}
							
						
					String strSql = "SELECT p.crewCompany, r.rigInformationUid, p.pax, p.crewRating1, p.crewRating2, lc.companyName, p.dailyUid, p.companyServiceProvided " +
							"FROM PersonnelOnSite p, LookupCompany lc, ReportDaily r " +
							"WHERE p.crewCompany = lc.lookupCompanyUid AND p.crewCompany IS NOT NULL " +
							"AND p.companyServiceProvided " +
							"IN (" + strInCondition + ") " +
							"AND (p.isDeleted IS NULL OR p.isDeleted = FALSE) " +
							"AND (lc.isDeleted IS NULL OR lc.isDeleted = FALSE) " +
							"AND (r.isDeleted IS NULL OR r.isDeleted = FALSE) " +
							"AND r.dailyUid = p.dailyUid " +
							"AND r.reportType = 'DDR' " +
							"AND r.reportDatetime >= :startDate AND r.reportDatetime <=:endDate " +
							"AND p.operationUid = :operationUid " +
							"ORDER BY lc.companyName, p.crewCompany, p.companyServiceProvided, p.dailyUid, p.crewRating1, p.crewRating2";
										
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
							
					if (lstResult.size()>0){
						
						List listCompany = new ArrayList();
						List <double[]> listCounter = new ArrayList<double[]>();
						String company1, company2, daily1, daily2, dayRatings, nightRatings, serviceProvided1, serviceProvided2;
						double[] workingHours = {0,0,0,0,0,0}; 
						double[] totalHours = {0,0,0,0,0,0};
								
						for (int i= 0;i<lstResult.size(); i++)
						{
							int belowExp = 0;
							int asExp = 0;
							int aboveExp = 0;
									
							setHoursToNull(workingHours, totalHours);
							
								for(int j=i;j<lstResult.size();j++)
								{
									int daily = 0;
									if(i==j || (i==j && j==lstResult.size()-1))
										daily = j;
									else if(j!=0 || i!=j)
										daily = j-1;
									
									Object objResult = lstResult.get(i);
									Object objResult2 = lstResult.get(j);
									Object objResultDay = lstResult.get(daily);
									Object[] obj = (Object[]) objResult;
									Object[] obj2 = (Object[]) objResult2;
									Object[] objDay = (Object[]) objResultDay;
									
									company1 = (obj[0]!=null)?obj[0].toString():"";
									company2 = (obj2[0]!=null)?obj2[0].toString():"";
									serviceProvided1 = (obj[7]!=null)?obj[7].toString():"";
									serviceProvided2 = (obj2[7]!=null)?obj2[7].toString():"";
									daily1 = (objDay[6]!=null)?objDay[6].toString():"";
									daily2 = (obj2[6]!=null)?obj2[6].toString():"";
									dayRatings = (obj2[3]!=null)?obj2[3].toString():"";
									nightRatings = (obj2[4]!=null)?obj2[4].toString():"";
									
									if(StringUtils.equals(company1, company2) && StringUtils.equals(serviceProvided1, serviceProvided2)){
										
										if(StringUtils.equals(daily1, daily2))
										{
											if(dayRatings!=null){
												countDayRatings(workingHours, belowExp, asExp, aboveExp, dayRatings);
											}
											if(nightRatings!=null){
												countNightRatings(workingHours, belowExp, asExp, aboveExp, nightRatings);
											}
										}
										else if (!(StringUtils.equals(daily1, daily2)))
										{
											calculateWorkingHours(workingHours, totalHours, supplierTotalRecordsPerDay, company1, daily1,serviceProvided1);
											
											for(int hr=0;hr<workingHours.length;hr++){
												workingHours[hr] = 0;
											}
											if(dayRatings!=null){
												countDayRatings(workingHours, belowExp, asExp, aboveExp, dayRatings);
											}
											if(nightRatings!=null){
												countNightRatings(workingHours, belowExp, asExp, aboveExp, nightRatings);
											}
										}
									}	
											
									if(!(StringUtils.equals(company1, company2)) || !(StringUtils.equals(serviceProvided1, serviceProvided2))){
										listCompany.add(lstResult.get(i));
										calculateWorkingHours(workingHours, totalHours, supplierTotalRecordsPerDay, company1, daily1,serviceProvided1);
										double[] temp = new double[6];
										System.arraycopy(totalHours, 0, temp, 0, temp.length);
										listCounter.add(temp);
										i=--j;
										j=lstResult.size();
									}
											
									if(i==lstResult.size()-1 || j==lstResult.size()-1){
										listCompany.add(lstResult.get(i));
										calculateWorkingHours(workingHours, totalHours, supplierTotalRecordsPerDay, company1, daily2,serviceProvided1);
										double[] temp = new double[6];
										System.arraycopy(totalHours, 0, temp, 0, temp.length);
										listCounter.add(temp);
										i=lstResult.size()-1;
									}
								}		
						}
						
						Well well = ApplicationUtils.getConfiguredInstance().getCachedWell(userContext.getUserSelection().getWellUid());
						rigUid = operation.getRigInformationUid();
					
						Map<String,LookupItem> serviceLookup= LookupManager.getConfiguredInstance().getLookup("xml://company.service?key=code&amp;value=label", userContext.getUserSelection(), null);
						
						
						for (int company = 0; company < listCompany.size(); company++ ){
							
							Object objResult1 = listCompany.get(company);
							double[] objResult2 = listCounter.get(company);
						    Object[] obj1 = (Object[]) objResult1;
							double[] obj2 = (double[]) objResult2;
							RigInformation rig = (RigInformation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(RigInformation.class,rigUid);
							LookupCompany companyObj = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, obj1[0].toString());
							
							String dunsNumber = companyObj.getDunsNumber();
							String rigName = rig.getRigName();
							String wellName = well.getWellName();
							double totalHourCompany = 0;
							double totalHourDay = 0;
							double totalHourNight = 0;
							double belowExpectedDay = obj2[0];
							double asExpectedDay = obj2[1];
							double aboveExpectedDay = obj2[2];
							double belowExpectedNight = obj2[3];
							double asExpectedNight = obj2[4];
							double aboveExpectedNight = obj2[5];
							LookupItem lookupItem = null;	
							
							for(Object o2: resultTotal)
							{
								Object[] ob2 = (Object[]) o2;
								companyUid2 = (ob2[0]!= null)?ob2[0].toString():"";
								companyServiceProvided2 = (ob2[2]!= null)?ob2[2].toString():"";
								if(StringUtils.equals(obj1[0].toString(),companyUid2) && StringUtils.equals(obj1[7].toString(),companyServiceProvided2))
								{
									totalHourCompany = Double.parseDouble(ob2[1].toString());
									totalHourDay = totalHourCompany/2;
									totalHourNight = totalHourCompany/2;
									break;
								}
							}
									
							if(totalHourCompany != 0)
							{
								belowExpectedDayPercent = belowExpectedDay*100/totalHourDay;
								asExpectedDayPercent = asExpectedDay*100/totalHourDay;
								aboveExpectedDayPercent = aboveExpectedDay*100/totalHourDay;
								belowExpectedNightPercent = belowExpectedNight*100/totalHourNight;
								asExpectedNightPercent = asExpectedNight*100/totalHourNight;
								aboveExpectedNightPercent = aboveExpectedNight*100/totalHourNight;
							}
							else
							{
								belowExpectedDayPercent = 0;
								asExpectedDayPercent = 0;
								aboveExpectedDayPercent = 0;
								belowExpectedNightPercent = 0;
								asExpectedNightPercent = 0;
								aboveExpectedNightPercent = 0;
							}
						    		
							String companySvcProvided = "";
							if(obj1[7]!=null)
							{
								lookupItem = serviceLookup.get(obj1[7].toString());
								if (lookupItem != null) {
									Object val = lookupItem.getValue();
									companySvcProvided = val != null ? val.toString() : "";
								}
							}
							
								ReportDataNode thisReportNode = reportDataNode.addChild("SupplierList");
								thisReportNode.addProperty("crewCompany", obj1[5].toString());
								thisReportNode.addProperty("serviceProvided", companySvcProvided);
								thisReportNode.addProperty("wellName", wellName);
								thisReportNode.addProperty("rigName", rigName);
								if(!StringUtils.isBlank(dunsNumber))
									thisReportNode.addProperty("dunsNumber", dunsNumber);
								else
									thisReportNode.addProperty("dunsNumber", "");
								thisReportNode.addProperty("country", well.getCountry());
								
								String lookupLabel = getLookupLabel(well.getCountry(), "country");
								thisReportNode.addProperty("countryLabel", lookupLabel);
								
								thisReportNode.addProperty("totalHourCompany", Double.toString(totalHourCompany));
								thisReportNode.addProperty("totalHourDay", Double.toString(totalHourDay));
								thisReportNode.addProperty("totalHourNight", Double.toString(totalHourNight));
								thisReportNode.addProperty("belowExpectedDay", String.valueOf(belowExpectedDay));
								thisReportNode.addProperty("asExpectedDay", String.valueOf(asExpectedDay));
								thisReportNode.addProperty("aboveExpectedDay", String.valueOf(aboveExpectedDay));
								thisReportNode.addProperty("belowExpectedNight", String.valueOf(belowExpectedNight));
								thisReportNode.addProperty("asExpectedNight", String.valueOf(asExpectedNight));
								thisReportNode.addProperty("aboveExpectedNight", String.valueOf(aboveExpectedNight));
								thisReportNode.addProperty("belowExpectedDayPercent", Double.toString(belowExpectedDayPercent));
								thisReportNode.addProperty("asExpectedDayPercent", Double.toString(asExpectedDayPercent));
								thisReportNode.addProperty("aboveExpectedDayPercent", Double.toString(aboveExpectedDayPercent));
								thisReportNode.addProperty("belowExpectedNightPercent", Double.toString(belowExpectedNightPercent));
								thisReportNode.addProperty("asExpectedNightPercent", Double.toString(asExpectedNightPercent));
								thisReportNode.addProperty("aboveExpectedNightPercent", Double.toString(aboveExpectedNightPercent));
								
					}
						
				}
			}
			
			
			ReportDataNode thisReportNode = reportDataNode.addChild("DateRange");
			thisReportNode.addProperty("startDate", ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(startDate));
		    thisReportNode.addProperty("endDate", ApplicationConfig.getConfiguredInstance().getReportDateFormater().format(endDate));
	}
	
	public void setHoursToNull(double[] workingHours, double[] totalHours){
		
		for(int hours = 0; hours<workingHours.length; hours++){
			workingHours[hours] = 0;
			totalHours[hours] = 0;
		}
	}
	
	public void countDayRatings(double[] workingHours, int belowExp, int asExp, int aboveExp, String dayRatings){
		
			if("below expectation".equals(dayRatings)){
				belowExp = belowExp + 1;
				workingHours[0] = workingHours[0] + 1;
			}
			if("as expected".equals(dayRatings)){
				asExp = asExp + 1;
				workingHours[1] = workingHours[1] + 1;
			}
			if("above expectation".equals(dayRatings)){
				aboveExp = aboveExp + 1;
				workingHours[2] = workingHours[2] + 1;
			}
	}

	public void countNightRatings(double workingHours[], int belowExp, int asExp, int aboveExp, String nightRatings){
			
			if("below expectation".equals(nightRatings)){
				belowExp = belowExp + 1;
				workingHours[3] = workingHours[3] + 1;
			}
			if("as expected".equals(nightRatings)){
				asExp = asExp + 1;
				workingHours[4] = workingHours[4] + 1;
			}
			if("above expectation".equals(nightRatings)){
				aboveExp = aboveExp + 1;
				workingHours[5] = workingHours[5] + 1;
			}
			
	}
	
	public void calculateWorkingHours(double[] workingHours, double[] totalHours, List<Object> supplierTotalRecordsPerDay, String company, String daily, String serviceProvided1){
		
		for (int hr=0;hr<workingHours.length;hr++){
			if (workingHours[hr]!=0){
				for(int match=0; match<supplierTotalRecordsPerDay.size(); match++)
				{
					Object objRecord = supplierTotalRecordsPerDay.get(match);
					Object[] rec = (Object[]) objRecord;
					String companyUid = (rec[0]!=null)?rec[0].toString():"";
					String dailyUid = ((rec[1]!=null)?rec[1].toString():"");
					String countRecord = ((rec[2]!=null)?rec[2].toString():"");
					String serviceProvided2 = ((rec[3]!=null)?rec[3].toString():"");
					if (StringUtils.equals(company, companyUid) && StringUtils.equals(daily, dailyUid) && StringUtils.equals(serviceProvided1, serviceProvided2))
					{
						workingHours[hr] = 12*workingHours[hr]/Integer.parseInt(countRecord);
						totalHours[hr] = totalHours[hr] + workingHours[hr];
					}
				}
			}
		}
	}
	
	public String getLookupLabel(String shortCode, String lookupTypeSelection) throws Exception{
		String sql = "from CommonLookup where (isDeleted = false or isDeleted is null) and lookupTypeSelection=:lookupTypeSelection and shortCode=:shortCode";
		String[] paramsFields = {"lookupTypeSelection","shortCode"};
		Object[] paramsValues = {lookupTypeSelection,shortCode};
		List<CommonLookup> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues);
		if(results.size() > 0){
			return ((CommonLookup)results.get(0)).getLookupLabel();
		}
		
		return shortCode;
	}
	
}

		
