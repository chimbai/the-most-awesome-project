package com.idsdatanet.d2.drillnet.transportDailyParam;

import com.idsdatanet.d2.core.model.SupportVesselInformation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class TransportDailyParamUtils {

	public static SupportVesselInformation createSupportVesselInformation(String vesselName, String companyid) throws Exception {
		SupportVesselInformation supportVesselInformation = new SupportVesselInformation();
		supportVesselInformation.setVesselName(vesselName);
		supportVesselInformation.setOperator(companyid);
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(supportVesselInformation);
		
		return supportVesselInformation;
	}

}
