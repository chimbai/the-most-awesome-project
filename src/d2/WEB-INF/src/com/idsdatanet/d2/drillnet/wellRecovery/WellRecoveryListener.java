package com.idsdatanet.d2.drillnet.wellRecovery;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.InspectionResult;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WellRecoveryListener implements DataNodeLoadHandler {

	@Override
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		if (meta.getTableClass().equals(Operation.class)) {
			List<Object> output_maps = new ArrayList<Object>();
			String strSql = "FROM Operation o, Wellbore wb, Well w" +
					" WHERE (o.isDeleted='1' OR wb.isDeleted='1' OR w.isDeleted='1')" +
					" AND o.wellboreUid = wb.wellboreUid" +
					" AND o.wellUid = w.wellUid" +
					" order by o.lastEditDatetime DESC";
			
			List<Object[]> items = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
			
			for (Object[] item : items) {
				Operation data = (Operation) item[0];
				output_maps.add(data);
			}
			return output_maps;
		}
		return null;
	}

	@Override
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		// TODO Auto-generated method stub
		if ("Operation".equals(meta.getTableClass().getSimpleName())) {
			return true;
		}
		return false;
	}
}
