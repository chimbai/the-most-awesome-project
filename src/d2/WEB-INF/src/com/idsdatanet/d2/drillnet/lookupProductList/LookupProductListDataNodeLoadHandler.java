package com.idsdatanet.d2.drillnet.lookupProductList;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.LookupProductList;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;

public class LookupProductListDataNodeLoadHandler implements DataNodeLoadHandler {
	
	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception {
		
		String strVendorUid = (String) commandBean.getRoot().getDynaAttr().get("vendorCompany");
		String strRegion = (String) commandBean.getRoot().getDynaAttr().get("wellRegion");										
		Boolean vendor=false;
		Boolean region = false;
			String customCondition = " WHERE (lpl.isDeleted = false or lpl.isDeleted is null) and ";
			if (meta.getTableClass().equals(LookupProductList.class)) {
				if (StringUtils.isNotBlank(strVendorUid)) {
					customCondition += "lpl.mudVendorCompanyUid = :companyUid";
					vendor=true;	
				}else {
					customCondition += "(lpl.mudVendorCompanyUid is null or lpl.mudVendorCompanyUid ='')";
				}
				
				if (StringUtils.isNotBlank(strRegion)) {
					customCondition += " and lpl.region = :region";
					region = true;
				}else {
					customCondition += " and (lpl.region is null or lpl.region = '')";
				}
				
				customCondition += " AND lpl.stockUid=lrs.lookupRigStockUid ORDER BY lrs.stockBrand";
				
				String strSql = "FROM LookupProductList lpl,LookupRigStock lrs "+customCondition;
				
				List<Object[]> items;
				
				if(vendor && region)
				{
					String[]paramNames =  {"companyUid","region"};
					Object[] paramValues = {strVendorUid,strRegion};
					items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
				}else if(vendor)
				{
					String[]paramNames =  {"companyUid"};
					Object[] paramValues = {strVendorUid};
					items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
					
				}else if(region)
				{
					String[]paramNames =  {"region"};
					Object[] paramValues = {strRegion};
					items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
					
				}else{					
					items = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strSql);
				}
									
			List<Object> output_maps = new ArrayList<Object>();
			
			for(Object[]  objResult: items){				
				LookupProductList productListObj = (LookupProductList) objResult[0];
				output_maps.add(productListObj);
			}
						
			return output_maps;
		}
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}
	
	
	
}
