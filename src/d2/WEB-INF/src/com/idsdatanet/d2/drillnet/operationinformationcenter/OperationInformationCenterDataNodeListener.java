package com.idsdatanet.d2.drillnet.operationinformationcenter;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.cache.RigInformations;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.graph.BaseGraph;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.query.representation.DatabaseQuery;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.ModuleConstants;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._RigInformation;
import com.idsdatanet.d2.core.web.session.ParameterizedQueryFilter;
import com.idsdatanet.d2.drillnet.operation.OperationUtils;

public class OperationInformationCenterDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler{
	private BaseGraph baseGraph = null;
	private String graphType = null;
	private String graphSize = null;
	private Boolean includeLegend = true;
	private int rowsToFetch = 20;

	public void setIncludeLegend(Boolean includeLegend) {
		this.includeLegend = includeLegend;
	}

	public Boolean getIncludeLegend() {
		return includeLegend;
	}
	
	public void setGraphType(String graphType) {
		this.graphType = graphType;
	}

	public void setGraphSize(String graphSize) {
		this.graphSize = graphSize;
	}
	
	public void setBaseGraph(BaseGraph baseGraph) {
		this.baseGraph = baseGraph;
	}
	
	public void setRowsToFetch(int rowsToFetch) {
		this.rowsToFetch = rowsToFetch;
	}

	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		Object object = node.getData();

		// a trick to only load used Operation object (initially load as String, only load the real object when needed)
		if((object instanceof String) && meta.getTableClass() == Operation.class){
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Operation WHERE operationUid=:thisOperationUid", "thisOperationUid", object.toString());
			object = lstResult.get(0);
			node.setData(object);
		}
		
		if(object instanceof Operation) {
			
			Operation thisOperation = (Operation) object;
			CommonUtil.getConfiguredInstance().setBBNameToDynamicAttribute(thisOperation.getOperationUid(), "bbName", node);
			//set the unit to false so always get raw value
			QueryProperties qp = new QueryProperties();
						
			String dailyUid = ApplicationUtils.getConfiguredInstance().getLastDailyUidInOperation(thisOperation.getOperationUid());
			Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyUid);
			
			Daily yesterday = ApplicationUtils.getConfiguredInstance().getYesterday(thisOperation.getOperationUid(), dailyUid);
			
			if(StringUtils.isNotBlank(dailyUid))
			{
				//get report daily object to fill out information from it
				ReportDaily thisReportDaily = ApplicationUtils.getConfiguredInstance().getReportDailyByOperationType(dailyUid, thisOperation.getOperationCode());
				
				if(thisReportDaily != null){
					String strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and reportDailyUid = :thisReportDailyUid";
					String[] paramsFields = {"thisReportDailyUid"};
					String[] paramsValues = {thisReportDaily.getReportDailyUid()};				
				
					List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					if (!lstResult.isEmpty())
					{
						ReportDaily selectedReportDaily =  (ReportDaily) lstResult.get(0);
						if (StringUtils.isNotBlank(selectedReportDaily.getRigInformationUid())) node.getDynaAttr().put("todayRig", selectedReportDaily.getRigInformationUid());
						else node.getDynaAttr().put("todayRig", thisOperation.getRigInformationUid());
						
						node.getDynaAttr().put("24HoursSummary", selectedReportDaily.getReportPeriodSummary());
						node.getDynaAttr().put("lastEntryOn", selectedDaily.getDayDate());
						
						if (selectedReportDaily.getQcFlag()==null) selectedReportDaily.setQcFlag("0");
						if(selectedReportDaily.getQcFlag().equals("1"))
						{
							node.getDynaAttr().put("qcFlag", "Rig Side QC Done");
						}
						else if(selectedReportDaily.getQcFlag().equals("2"))
						{
							node.getDynaAttr().put("qcFlag", "Town Side QC Done");
						} else {
							node.getDynaAttr().put("qcFlag", "QC Not Done");
						}
						
						node.getDynaAttr().put("sentby", selectedReportDaily.getSentby());
						node.getDynaAttr().put("sentto", selectedReportDaily.getSentto());
						node.getDynaAttr().put("reportPlanSummary", selectedReportDaily.getReportPlanSummary());
						
						node.getDynaAttr().put("dailyEndSummary", selectedReportDaily.getDailyEndSummary());
						node.getDynaAttr().put("predictedLoggingDateTime", selectedReportDaily.getPredictedLoggingDateTime());
						node.getDynaAttr().put("geolOps", selectedReportDaily.getGeolOps());
						node.getDynaAttr().put("last24hrsincident", selectedReportDaily.getLast24hrsincident());
						
						//LOAD REPORT_DAILY DATA
						Map<String, Object> reportDailyFieldValues = PropertyUtils.describe(selectedReportDaily);
						for (Map.Entry entry : reportDailyFieldValues.entrySet()) {
							node.getDynaAttr().put("ddr." + entry.getKey(), entry.getValue());
							if("daycost".equals(entry.getKey()) || "daycompletioncost".equals(entry.getKey()))
							{
								if(entry.getValue() == null) node.getDynaAttr().put("ddr." + entry.getKey(), 0);
							}
						}
						showCustomUOM(node, userSelection.getGroupUid());
						
						//CALCULATE CUMTOTALCOST TO DATE
						String[] paramsFields2 = {"todayDate", "operationUid", "reportType"};
						Object[] paramsValues2 = new Object[3]; 
						paramsValues2[0] = selectedReportDaily.getReportDatetime(); 
						paramsValues2[1] = selectedReportDaily.getOperationUid();
						paramsValues2[2] = selectedReportDaily.getReportType();
						
						//GET Operation.amountSpentPriorToSpud, TO BE ADDED TO CUMTOTALCOST
						Double amountSpendPriorToSpud = 0.00;
						Double cumCost = 0.00;
						Double cumcostadjust = 0.00;
						Double cumcompletioncost = 0.00;
						Double cumtangiblecost = 0.00;
						Double cumtotalcost = 0.00;
						
						Operation currentOperation = ApplicationUtils.getConfiguredInstance().getCachedOperation(selectedReportDaily.getOperationUid());
						if (currentOperation.getAmountSpentPriorToSpud() != null) amountSpendPriorToSpud = currentOperation.getAmountSpentPriorToSpud();
						
						String strSql2 = "select sum(daycost),sum(costadjust),sum(daycompletioncost),sum(daytangiblecost) from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and reportDatetime <= :todayDate and operationUid = :operationUid";
						List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
						if (!lstResult2.isEmpty()){	
							Object[] obj = (Object[]) lstResult2.get(0);
							
							if (obj[0] != null) cumCost = Double.parseDouble(obj[0].toString());
							if (obj[1] != null) cumcostadjust = Double.parseDouble(obj[1].toString());
							if (obj[2] != null) cumcompletioncost = Double.parseDouble(obj[2].toString());
							if (obj[3] != null) cumtangiblecost = Double.parseDouble(obj[3].toString());
						}
						
						if ((cumCost!=null) && (cumcompletioncost !=null) && (cumtangiblecost !=null)) {
							cumtotalcost = cumCost + cumcostadjust + cumcompletioncost + cumtangiblecost + amountSpendPriorToSpud;
						}
						
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, ReportDaily.class, "daycost");
						thisConverter.setBaseValue(cumtotalcost);
						if (thisConverter.isUOMMappingAvailable()){
							node.setCustomUOM("@ddr.cumtotalcost", thisConverter.getUOMMapping());
						}
						node.getDynaAttr().put("ddr.cumtotalcost", thisConverter.getConvertedValue());
						//END OF CALCULATE CUMTOTALCOST TO DATE
														
					}
					else
					{
						node.getDynaAttr().put("todayRig", thisOperation.getRigInformationUid());
					}
				
					//fill up yesterday detail in OIC
					if (yesterday != null){
						strSql = "FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) and dailyUid = :yesterdayDailyUid AND reportType = :thisReportType";
						String[] paramsFields1 = {"yesterdayDailyUid", "thisReportType"};
						String[] paramsValues1 = {yesterday.getDailyUid(), thisReportDaily.getReportType()};
						
						List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1);
						if (!lstResult1.isEmpty())
						{
							ReportDaily lastReportDaily =  (ReportDaily) lstResult1.get(0);
							node.getDynaAttr().put("prev24HoursSummary", lastReportDaily.getReportPeriodSummary());
						}
					}
				}
				
				//set the query property to use this operation uomtemplate and datum
				qp.setUomDatumUid(thisOperation.getDefaultDatumUid());
				qp.setUomTemplateUid(thisOperation.getUomTemplateUid());
				
				//get activity info - last depth and last activity
				String strSql = "FROM Activity WHERE (isDeleted = false or isDeleted is null) and dailyUid = :selectedDailyUid ORDER BY endDatetime DESC";
				String[] paramsFields2 = {"selectedDailyUid"};
				String[] paramsValues2 = {dailyUid};
				List lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields2, paramsValues2, qp);
				
				//assign unit to the value base on UOM 
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Activity.class, "depthMdMsl", 0, thisOperation.getUomTemplateUid());
				
				SimpleDateFormat timeOnly = new SimpleDateFormat("HH:mm");
				
				if (!lstResult1.isEmpty())
				{
					Activity lastActivity =  (Activity) lstResult1.get(0);
					String endDateTime = null;
					if (("23:59").equalsIgnoreCase((String) timeOnly.format(lastActivity.getEndDatetime()))) {
						endDateTime = "24:00";
					}
					else {
						endDateTime = timeOnly.format(lastActivity.getEndDatetime());
					}
					node.getDynaAttr().put("lastActivityDate", selectedDaily.getDayDate()); 
					node.getDynaAttr().put("lastActivityTime", timeOnly.format(lastActivity.getStartDatetime()) + " - " + endDateTime);
					node.getDynaAttr().put("lastActivity", lastActivity.getActivityDescription());
					node.getDynaAttr().put("lastActivityClass", lastActivity.getClassCode());
					node.getDynaAttr().put("lastActivityDaily", lastActivity.getDailyUid());
					if (lastActivity.getDepthMdMsl() != null)
					{
						if (thisConverter.isUOMMappingAvailable()) {
							node.setCustomUOM("@lastDepth", thisConverter.getUOMMapping());
						}
						node.getDynaAttr().put("lastDepth", lastActivity.getDepthMdMsl());
					}
				}
				else{
					//if last day no activity, let it point to the last daily
					node.getDynaAttr().put("lastActivityDaily", dailyUid);
				}
				
				//get days on operation 
				thisConverter.setReferenceMappingField(Operation.class, "days_spent_prior_to_spud");
				thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysOnOperation(thisOperation.getOperationUid(), dailyUid));
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@daysOnWell", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("daysOnWell", thisConverter.getConvertedValue());
				
				
				thisConverter.setReferenceMappingField(ReportDaily.class, "daysOnWell");
                //thisConverter.setBaseValue(CommonUtil.getConfiguredInstance().daysOnOperation(thisOperation.getOperationUid(), dailyUid));
                if (thisConverter.isUOMMappingAvailable()) {
                    node.setCustomUOM("@reportDailyDaysOnWell", thisConverter.getUOMMapping());
                }
				strSql = "SELECT daysOnWell FROM ReportDaily WHERE (isDeleted = false or isDeleted is null) AND operationUid=:operationUid ORDER BY reportDatetime DESC";
                String[] pf = {"operationUid"};
                String[] pv = {thisOperation.getOperationUid()};
                QueryProperties qp2 = new QueryProperties();
                qp2.setFetchFirstRowOnly();
                List<Double> rv = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, pf, pv, qp2);
                if (!rv.isEmpty()) {
                    node.getDynaAttr().put("reportDailyDaysOnWell", rv.get(0));
                } else {
                    node.getDynaAttr().put("reportDailyDaysOnWell", null);
                }
				
				//get last HSE event
				strSql = "SELECT hse FROM HseIncident hse, Daily d WHERE (hse.isDeleted = false or hse.isDeleted is null) AND hse.dailyUid = d.dailyUid AND hse.operationUid = :thisOperationUid  AND (hse.supportVesselInformationUid is null or hse.supportVesselInformationUid = '') ORDER BY hse.hseEventdatetime DESC, d.dayDate DESC";
				String[] paramsFields1 = {"thisOperationUid"};
				String[] paramsValues1 = {thisOperation.getOperationUid()};
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1, qp);
				
				if (!lstResult2.isEmpty())
				{
					HseIncident lastIncident =  (HseIncident) lstResult2.get(0);
					node.getDynaAttr().put("lastHseDatetime", lastIncident.getHseEventdatetime());
					node.getDynaAttr().put("lastHse", lastIncident.getDescription());
					node.getDynaAttr().put("lastHseDaily", lastIncident.getDailyUid());
				}
				else{
					//if last day no activity, let it point to the last daily
					node.getDynaAttr().put("lastHseDaily", dailyUid);
					node.getDynaAttr().put("lastHse", "None");
				}
				
				node.getDynaAttr().put("latestReports", "None");
			}
			
			// only load the report list when parent node (Operation) is visible (in a page) in order to improve performance
			this.loadDDRReportList(node, thisOperation, userSelection, commandBean);
			
			// only load other report list for Geology
			this.loadOtherReportList(node, thisOperation, userSelection);
			
			//Disable load of the report list for DGR if GWP enableDGRReportTypeCheckInOIC set to ON
			if(!"1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_ENABLE_DGR_REPORT_TYPE_CHECK_IN_OIC)))
				this.loadDGRReportList(node, thisOperation, userSelection);
			
			//get country from well 
			if (thisOperation.getWellUid()!=null) {
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, thisOperation.getWellUid());
				String countryname = null;
				if (well != null) {
					//send short code and get country name in return
					countryname = CommonUtil.getConfiguredInstance().getCountryFullName(well.getCountry());
					node.getDynaAttr().put("country", countryname);
					node.getDynaAttr().put("contactNumber", this.nullToEmptyString(well.getContactNumber()));
				}
			}
			this.loadLessonTicketList(node, thisOperation, userSelection);
			this.loadCasingCementingReportList(node, thisOperation, userSelection);
		
			//load file manager file list 
			Integer maxNumberFilesToShow = this.getMaxUploadedFileToShow(userSelection);
			String fileMangerList = CommonUtil.getConfiguredInstance().loadFileManagerFileListForOIC(thisOperation.getOperationUid(), maxNumberFilesToShow);
			if (StringUtils.isNotBlank(fileMangerList)) {
				node.getDynaAttr().put("fileManagerList", fileMangerList);
			}
			
			this.loadGraphProperties(node, request);
			
			String completeOperationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), thisOperation.getOperationUid());

			if (StringUtils.isNotBlank(completeOperationName)){
				node.getDynaAttr().put("completeOperationName", completeOperationName);
			}
						
			//redirect operation page based on gwp
			node.getDynaAttr().put("operationPage" , OperationUtils.getWWOHyperlinkRedirection(userSelection.getGroupUid()));	
		}
	}
	
	private void loadGraphProperties(CommandBeanTreeNode node, HttpServletRequest request) throws Exception {
		if (this.graphType != null && this.graphSize != null) {
			this.baseGraph.preProcessRequest(this.graphSize, request, null, null);
			String graphParameter = "type=" + this.graphType + "&size=" + this.graphSize + "&fromOIC=true&showLegend="+this.includeLegend.toString();
			
			node.getDynaAttr().put("graphHeight", this.baseGraph.getHeight());
			node.getDynaAttr().put("graphWidth", this.baseGraph.getWidth());
			node.getDynaAttr().put("graphParameter", graphParameter);
		}
	}
	
	private int getMaxReportToShow(UserSelectionSnapshot userSelection) throws Exception {
		String maxReportNumber = GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_MAX_REPORT_TO_SHOW_IN_OIC);
		int intMaxReportNumber = 6;
		if(maxReportNumber != null){
			intMaxReportNumber = Integer.parseInt(maxReportNumber) - 1;
		}
		return intMaxReportNumber;
	}
	
	private void loadDGRReportList(CommandBeanTreeNode parentNode, Operation operation, UserSelectionSnapshot userSelection) throws Exception {
		int intMaxReportNumber = this.getMaxReportToShow(userSelection);
		
		Integer intCounter = 0;
		String[] paramsFields = {"thisOperation"};
		String[] paramsValues = {operation.getOperationUid()};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation AND (rd.reportType='DGR') AND (rf.reportType='DGR') ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues);
				
		for(Iterator i = items.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			DGRReportList dgr = new DGRReportList();
			ReportFiles thisReportFile = (ReportFiles) obj_array[0];
			dgr.setData(thisReportFile, operation.getOperationName());
			File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());
			
			if(report_file.exists()){
				parentNode.addChild(DGRReportList.class.getSimpleName(), dgr);
				intCounter++;
				if (intCounter > intMaxReportNumber) break;
			}
		}
	}
	
	private List<Object> sortByDateGenerated(List<Object> _items){
		//refer OP-6606
		//filter to grab the latest version of the report (singular) based on date generated for that selected day
		HashMap<Long,Object> _items_hashmap = new HashMap<Long,Object>();
		for(Object item:_items) {
			Object[] obj_array = (Object[]) item;
			ReportFiles reportFile = (ReportFiles)obj_array[0];
			
			//check if reportDayDate key exists in hash map
			boolean doesKeyExist = _items_hashmap.containsKey(reportFile.getReportDayDate().getTime());
			if(!doesKeyExist){
				_items_hashmap.put(reportFile.getReportDayDate().getTime(), item);
			}else {
				Object[] temp_obj_array = (Object[]) _items_hashmap.get(reportFile.getReportDayDate().getTime());
				ReportFiles temp_reportFile = (ReportFiles)temp_obj_array[0];
				boolean isStoredLessThanCurrent = temp_reportFile.getDateGenerated().getTime()<reportFile.getDateGenerated().getTime();
				if(isStoredLessThanCurrent) {
					_items_hashmap.put(reportFile.getReportDayDate().getTime(), item);
				}
			}
		}
		
		//OP-6877
		//filter to grab latest generated reports
		//Create map with dateGenerated as key
		HashMap<Long,Object> _dateGenerated_hashmap = new HashMap<Long,Object>();
		
		for(Map.Entry<Long,Object> entry:_items_hashmap.entrySet()) {
			Object[] obj_array = (Object[])_items_hashmap.get(entry.getKey());
			ReportFiles reportFile = (ReportFiles)obj_array[0];
			
			Long dateGenerated = reportFile.getDateGenerated().getTime();
			_dateGenerated_hashmap.put(dateGenerated, obj_array);
		}
		//Sort by dateGenerated key in descending
		TreeMap<Long,Object> ordered = new TreeMap<Long,Object>(Collections.reverseOrder());
		for(Map.Entry<Long, Object> entry:_dateGenerated_hashmap.entrySet()) {
			ordered.put(entry.getKey(), entry.getValue());
		}
		
		//populate items ArrayList with these latest reports
		List<Object> items = new ArrayList<Object>();
		for(Long key:ordered.keySet()) {
			items.add(ordered.get(key));
		}
		return items;
	}
	private void loadDDRReportList(CommandBeanTreeNode parentNode, Operation operation, UserSelectionSnapshot userSelection, CommandBean commandBean) throws Exception {
		int intMaxReportNumber = this.getMaxReportToShow(userSelection);
		
		Integer intCounter = 0;
		String thisReportType = "DDR";
		String[] paramsFields = {"thisOperation", "reportDailyReportType", "reportFilesReportType"};
		String[] paramsValues = {operation.getOperationUid(), thisReportType, thisReportType};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rd.reportType=:reportDailyReportType and rf.reportType=:reportFilesReportType ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues);
		items = sortByDateGenerated(items);
		
		if(items.size() < 1)
		{
			Map<String, String> reportTypePriorityWhenDrllgNotExist = CommonUtils.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist();
			String operationCode = operation.getOperationCode();
			String strReportTypes = reportTypePriorityWhenDrllgNotExist.get(operationCode);
			if (strReportTypes != null) {
				String[] reportTypes = strReportTypes.split("[,]");
				for (String reportType : reportTypes) {
					String[] paramsValues2 = {operation.getOperationUid(), reportType, reportType};
					thisReportType = reportType;
					items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rd.reportType=:reportDailyReportType and rf.reportType=:reportFilesReportType ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues2);
					items = sortByDateGenerated(items);
					if(items.size() > 0)
					{
						break;
					}
				}
			}
		}
				
		for(Iterator i = items.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			DDRReportList ddr = new DDRReportList();
			ReportFiles thisReportFile = (ReportFiles) obj_array[0];
			ddr.setData(thisReportFile, operation.getOperationName());
						
			File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());
			
			if(report_file.exists()){
				parentNode.addChild(DDRReportList.class.getSimpleName(),ddr);
				
				intCounter++;
				if (intCounter > intMaxReportNumber) break;
			}
		}
	}
	
	public Boolean checkRigReleaseDateTime(String operationUid) throws Exception{
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S"); 
		String[] paramsFields = {"operationUid"};
		String[] paramsValues = {operationUid};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select rigOffHireDate, operationUid FROM Operation WHERE operationUid = :operationUid and (isDeleted = false or isDeleted is null)", paramsFields, paramsValues);
					
		if(items.size() > 0)
		{
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();
				
				if (!isNullEmptyBlank(nullToEmptyString(obj_array[0]))){					
					 Date todayDateTime = new Date();				 
					 Date rigOffHireDateTime = (Date)formatter.parse(obj_array[0].toString());					
					 long diff = todayDateTime.getTime() - rigOffHireDateTime.getTime();

					 if (diff > 0){
						 if ((diff/(1000 * 60 * 60 * 24)) >= 3){
							 return true;
						 }
					 }
				}			
			}
		}
		return false;
	}
	
	public Boolean checkByEngineer(String dailyUid) throws Exception{	
		String[] paramsFields = {"dailyUid"};
		String[] paramsValues = {dailyUid};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select checkedby, dailyUid FROM CementJob WHERE dailyUid = :dailyUid and (isDeleted = false or isDeleted is null) ORDER BY jobNumber", paramsFields, paramsValues);
					
		if(items.size() > 0)
		{
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();
				if (isNullEmptyBlank(nullToEmptyString(obj_array[0]))){	
					return true;
				}
			}
		}
		return false;
	}
	
	public Boolean isNullEmptyBlank(String stringvalue){
		if ( stringvalue == null ) return true; 					//check null
		else if ( stringvalue.length() == 0 ) return true; 			//check empty
		else if ( stringvalue.trim().length () == 0 ) return true; 	//check blank or other whitespace
		return false;
	}
	
	private void loadCasingCementingReportList(CommandBeanTreeNode node, Operation operation, UserSelectionSnapshot userSelection) throws Exception {
		int intMaxReportNumber = this.getMaxReportToShow(userSelection);
		String casingCementingInfo = "";
		Integer intCounter = 0;
		String thisReportType = "DCCR";
		String[] paramsFields = {"thisOperation", "reportFilesReportType"};
		String[] paramsValues = {operation.getOperationUid(), thisReportType};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rf.reportType=:reportFilesReportType ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues);
					
		if(items.size() > 0)
		{
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();

				ReportFiles thisReportFile = (ReportFiles) obj_array[0];
				File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());	
				
				DateFormat formatterDate = new SimpleDateFormat("dd MMM yyyy");
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S"); 
				Date date; 
				String asterisk = "";
			    
				if(report_file.exists()){
					date = (Date)formatter.parse(thisReportFile.getReportDayDate().toString()); 
					
					if (checkRigReleaseDateTime(thisReportFile.getReportOperationUid()) && checkByEngineer(thisReportFile.getDailyUid())){
						asterisk = "(*)";
					}
	
					if (StringUtils.isBlank(casingCementingInfo)){
						casingCementingInfo = "reportfileuid="  + URLEncoder.encode(thisReportFile.getReportFilesUid(),"utf-8") + "&reportname=" + URLEncoder.encode(formatterDate.format(date).toString(),"utf-8") + "&filename=" + URLEncoder.encode(thisReportFile.getReportFile().toString(),"utf-8") + "&asterisk=" + URLEncoder.encode(asterisk,"utf-8");
					}
					else
						casingCementingInfo += ",reportfileuid="  + URLEncoder.encode(thisReportFile.getReportFilesUid(),"utf-8") + "&reportname=" + URLEncoder.encode(formatterDate.format(date).toString(),"utf-8") + "&filename=" + URLEncoder.encode(thisReportFile.getReportFile().toString(),"utf-8") + "&asterisk=" + URLEncoder.encode(asterisk,"utf-8");
									
					intCounter++;
					if (intCounter > intMaxReportNumber) break;
				}
			}
		}
		node.getDynaAttr().put("casingCementingList", casingCementingInfo);
	}
	
	
	private void loadLessonTicketList(CommandBeanTreeNode node, Operation operation, UserSelectionSnapshot userSelection) throws Exception {
	
		String lessonTicketInfo = "";
		String[] paramsFields = {"thisOperation"};
		String[] paramsValues = {operation.getOperationUid()};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("Select d.dayNumber, d.dailyUid, d.dayDate, l.lessonTicketUid, l.lessonTitle" +
				" FROM Daily d, LessonTicket l WHERE (d.isDeleted = false or d.isDeleted is null) and " +
				"(l.isDeleted = false or l.isDeleted is null) AND (l.lessonReportType = '' OR l.lessonReportType IS NULL) and d.operationUid=:thisOperation AND " +
				"l.dailyUid=d.dailyUid order by d.dayDate DESC", paramsFields, paramsValues);
		
		SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
		
		for(Iterator i = items.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			
			String dayNum= URLEncoder.encode(this.nullToEmptyString(obj_array[0]), "utf-8");
			String dailyUid = URLEncoder.encode(obj_array[1].toString(), "utf-8");		
			String dDate = df.format(obj_array[2]);			
			String dayDate = URLEncoder.encode(dDate, "utf-8");
			String lessonTicketUid = URLEncoder.encode(obj_array[3].toString(), "utf-8");
			String lessonTicketTitle = URLEncoder.encode(this.nullToEmptyString(obj_array[4]), "utf-8");
			
			if (StringUtils.isBlank(lessonTicketInfo)) {
				lessonTicketInfo = "daynum=" + dayNum + "&dailyuid=" + dailyUid + "&daydate=" + dayDate  + "&lessonticketuid=" + lessonTicketUid + "&lessontitle=" + lessonTicketTitle;
			}else {
				lessonTicketInfo = lessonTicketInfo + ",daynum=" + dayNum + "&dailyuid=" + dailyUid + "&daydate=" + dayDate  + "&lessonticketuid=" + lessonTicketUid + "&lessontitle=" + lessonTicketTitle;
			}

		}
		node.getDynaAttr().put("lessonTicketList", lessonTicketInfo);

	}
	
	private Integer getMaxUploadedFileToShow(UserSelectionSnapshot userSelection) throws Exception {
		String maxFilesNumber = GroupWidePreference.getValue(userSelection.getGroupUid(), "maxUploadedFilesToShowInOIC");
		Integer intMaxFilesNumber = 9;
		if(maxFilesNumber != null){
			intMaxFilesNumber = Integer.parseInt(maxFilesNumber) - 1;
		}
		return intMaxFilesNumber;
	}

	public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
		if (meta.getTableClass().equals(Operation.class)) {
			// retrieve all the operations that are not being assigned to a team that the current user not belongs to
			/*List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT o.operationUid, " +
					"MAX(d.dayDate) FROM Operation o, Daily d WHERE (o.isDeleted = '0' OR o.isDeleted IS NULL) AND " +
					"(d.isDeleted = '0' OR d.isDeleted IS NULL) AND o.operationUid = d.operationUid AND " +
					"(((o.tightHole = '0' OR o.tightHole IS NULL) AND (o.operationUid NOT IN " +
					"(SELECT a.operationUid FROM OpsTeamOperation a WHERE (a.isDeleted = '0' OR a.isDeleted IS NULL)) " +
					"OR o.operationUid IN (SELECT b.operationUid FROM OpsTeamOperation b, OpsTeamUser c " +
					"WHERE b.opsTeamUid = c.opsTeamUid AND (b.isDeleted = '0' OR b.isDeleted is null) AND " +
					"(c.isDeleted = '0' OR c.isDeleted IS NULL) AND c.userUid = :userUid))) OR (o.tightHole = '1' AND o.operationUid IN" +
					" (SELECT d.operationUid FROM TightHoleUser d WHERE (d.isDeleted = '0' OR d.isDeleted IS NULL) AND d.userUid = :userUid)))" +
					" GROUP BY o.operationUid", "userUid", UserSession.getInstance(request).getUserUid());
			List<Object> output_maps = new ArrayList<Object>();
			
			Collections.sort(items, new OperationComparator());
			
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();
				output_maps.add(obj_array[0]);
			}
			return output_maps;*/
			
			/*
			List<Object> output_maps = new ArrayList<Object>();
			List<Object> output_maps2 = new ArrayList<Object>();
			List result = new ArrayList();
			*/
			UserSession userSession = UserSession.getInstance(request);
			RigInformations rigInformations = userSession.getCachedAllAccessibleRigInformations();
			List<String> rigInformationUids = new ArrayList<String>();
			if (rigInformations!=null) {
				if (rigInformations.size()>0) {
					for (_RigInformation rigInformation : rigInformations.values()) {
						rigInformationUids.add(rigInformation.getRigInformationUid());
					}
				} else {
					rigInformationUids.add("");
				}
			}
			
			String operationNameFilter  = (String) commandBean.getRoot().getDynaAttr().get("operationNameFilter");
			String OICFilter  = (String) commandBean.getRoot().getDynaAttr().get("OICFilter");
			String operationTypeFilter  = (String) commandBean.getRoot().getDynaAttr().get("operationTypeFilter");
			String countryFilter  = (String) commandBean.getRoot().getDynaAttr().get("countryFilter");
			String dateFilter  = (String) commandBean.getRoot().getDynaAttr().get("dateFilter");
			
			List<String> wellUids = new ArrayList<String>();
			List<String> operationUids = new ArrayList<String>();
			
			Calendar dateFrom = Calendar.getInstance();
			Date currentDate = dateFrom.getTime();
			
			if (StringUtils.isNotBlank(OICFilter)){
				String[] oicFilter = OICFilter.split("::");
				
				if(oicFilter.length==4){
					if (oicFilter[0]!=null)	{
						operationNameFilter = oicFilter[0];
						commandBean.getRoot().getDynaAttr().put("operationNameFilter", operationNameFilter);
					}
					if (oicFilter[1]!=null){
						operationTypeFilter = oicFilter[1];
						commandBean.getRoot().getDynaAttr().put("operationTypeFilter", operationTypeFilter);
					}
					if (oicFilter[2]!=null)	{
						countryFilter = oicFilter[2];
						commandBean.getRoot().getDynaAttr().put("countryFilter", countryFilter);
						
						String strSql = "SELECT w FROM Operation op, Well w WHERE (op.isDeleted = false or op.isDeleted is null) AND (w.isDeleted = false or w.isDeleted is null) AND op.wellUid = w.wellUid AND w.country = :thisCountry ";
						String[] paramsFields = {"thisCountry"};
						String[] paramsValues = {countryFilter};
						List<Well> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
						
						if (!lstResult.isEmpty()){
							for (Well wells : lstResult) {
								wellUids.add(wells.getWellUid());
							}
						}else{
							wellUids.add("");
						}
					}
					if (oicFilter[3]!=null){
						dateFilter = oicFilter[3];
						commandBean.getRoot().getDynaAttr().put("dateFilter", dateFilter);
						
						
						if(StringUtils.isNotBlank(dateFilter)) {
							dateFrom.add(Calendar.MONTH, -(Integer.parseInt(dateFilter)));
							Date dateRange = dateFrom.getTime();
							
							String strSql = "SELECT op FROM Operation op WHERE (op.isDeleted = false or op.isDeleted is null) AND op.sysOperationLastDatetime >= :dateRange AND op.sysOperationLastDatetime <= :currentDate";
							String[] paramsFields = {"dateRange","currentDate"};
							Date[] paramsValues = {dateRange,currentDate};
							List<Operation> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,paramsFields, paramsValues);
							
							if (!lstResult.isEmpty()){
								for (Operation operations : lstResult) {
									operationUids.add(operations.getOperationUid());
								}
							}else{
								operationUids.add("");
							}
							
						}
					}
				}
			}
			
			String rigInformationUid = (String) commandBean.getRoot().getDynaAttr().get("rigInformationUid");
			if (operationNameFilter==null) operationNameFilter = "";
			
			String allOperationsNotAssignedToOpsTeam = "o.operationUid NOT IN (SELECT a.operationUid FROM OpsTeamOperation a, OpsTeam e WHERE " + 
			"e.opsTeamUid = a.opsTeamUid AND (a.isDeleted = false OR a.isDeleted IS NULL) AND (e.isPublic = false OR e.isPublic IS NULL)) OR ";
			if (BooleanUtils.isTrue(userSession.getCurrentUser().getAccessToOpsTeamOperationsOnly())) {
				allOperationsNotAssignedToOpsTeam = "";
			}
			
			String queryString = "SELECT o.operationUid FROM Operation o";
			
			// query filter
			DatabaseQuery queryFilter = userSession.getSystemSelectionFilter().getQueryFilter(ModuleConstants.WELL_MANAGEMENT,"Operation", "o");
			if (StringUtils.isNotBlank(queryFilter.getParentTablesString())) {
				queryString += ", " + queryFilter.getParentTablesString();
			}
			
			String conditionClause = queryFilter.getConditionClause(userSession);
			if (queryFilter instanceof ParameterizedQueryFilter){
				queryString += " WHERE ";
			}
			
			if (StringUtils.isNotBlank(conditionClause)) {
				queryString += conditionClause + " AND ";
			}
			
			queryString += " o.operationName like :operationNamePrefix AND (o.isDeleted = '0' OR o.isDeleted IS NULL) AND (o.isCampaignOverviewOperation = '0' OR o.isCampaignOverviewOperation IS NULL) AND o.sysOperationSubclass='well' AND " +
					(StringUtils.isNotBlank(dateFilter)?" o.operationUid IN (:operationUids) AND ":"") +
					(StringUtils.isNotBlank(operationTypeFilter)?" o.operationCode='" + operationTypeFilter + "' AND ":"") +
					(StringUtils.isNotBlank(countryFilter)?" o.wellUid IN (:wellUids) AND ":"") +
					(StringUtils.isNotBlank(rigInformationUid)?" o.rigInformationUid='"+rigInformationUid+"' AND ":"") +
					(rigInformations!=null?" (o.rigInformationUid in (:rigInformationUids) " +
						"or o.operationUid in (" +
						"select rd.operationUid from ReportDaily rd " +
						"where (rd.isDeleted = false or rd.isDeleted is null) " +
						"and rd.rigInformationUid in (:rigInformationUids)" +
						")) and ":"") + 
				"(o.groupUid in (:groupUid) ) and " +
				"(o.tightHole = '0' OR o.tightHole IS NULL OR (o.tightHole = '1' AND o.operationUid IN (SELECT d.operationUid FROM TightHoleUser d WHERE (d.isDeleted = '0' OR d.isDeleted IS NULL) AND d.userUid = :userUid))) AND (" + 
				allOperationsNotAssignedToOpsTeam + "o.operationUid IN (SELECT b.operationUid FROM OpsTeamOperation b, OpsTeamUser c " + 
				"WHERE b.opsTeamUid = c.opsTeamUid AND (b.isDeleted = '0' OR b.isDeleted is null) AND " + 
				"(c.isDeleted = '0' OR c.isDeleted IS NULL) AND c.userUid = :userUid)) " + 
				"GROUP BY o.operationUid, o.sysOperationLastDatetime ORDER BY o.sysOperationLastDatetime DESC";
			
			String[] paramNames;
			Object[] paramValues;
			if (queryFilter instanceof ParameterizedQueryFilter){
				ParameterizedQueryFilter pQueryFilter = (ParameterizedQueryFilter)queryFilter;
				if (rigInformations!=null) {
					paramNames = (String[]) ArrayUtils.addAll(pQueryFilter.getParameterNames(), new String[] { "rigInformationUids", "operationNamePrefix", "userUid" });
					paramValues = ArrayUtils.addAll(pQueryFilter.getParameterValues(), new Object[] { rigInformationUids, operationNameFilter + "%", userSession.getUserUid() });
				} else {
					paramNames = (String[]) ArrayUtils.addAll(pQueryFilter.getParameterNames(), new String[] { "operationNamePrefix", "userUid" });
					paramValues = ArrayUtils.addAll(pQueryFilter.getParameterValues(), new Object[] { operationNameFilter + "%", userSession.getUserUid() });
				}
			} else {
				if (rigInformations!=null) {
					paramNames = new String[] { "rigInformationUids", "groupUid", "operationNamePrefix", "userUid" };
					paramValues = new Object[] { rigInformationUids, userSelection.getAccessibleGroupUids(), operationNameFilter + "%", userSession.getUserUid()};
				} else {
					if(StringUtils.isNotBlank(dateFilter) && StringUtils.isNotBlank(countryFilter)){
						paramNames = new String[] { "operationUids","wellUids", "operationNamePrefix", "groupUid", "userUid"};
						paramValues = new Object[] { operationUids, wellUids, operationNameFilter + "%", userSelection.getAccessibleGroupUids(), userSession.getUserUid()};
					}else if (StringUtils.isNotBlank(dateFilter)) {
						paramNames = new String[] { "operationUids", "operationNamePrefix", "groupUid", "userUid"};
						paramValues = new Object[] { operationUids, operationNameFilter + "%", userSelection.getAccessibleGroupUids(), userSession.getUserUid()};
					}else if (StringUtils.isNotBlank(countryFilter)) {
						paramNames = new String[] { "wellUids", "operationNamePrefix", "groupUid", "userUid"};
						paramValues = new Object[] {  wellUids, operationNameFilter + "%", userSelection.getAccessibleGroupUids(), userSession.getUserUid()};
					}
					else{
						paramNames = new String[] { "operationNamePrefix", "groupUid", "userUid"};
						paramValues = new Object[] { operationNameFilter + "%", userSelection.getAccessibleGroupUids(), userSession.getUserUid()};
					}
					
				}
			}
			QueryProperties qp = new QueryProperties();
			qp.setRowsToFetch(0, this.rowsToFetch);
			List items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, paramNames, paramValues, qp);
			if (!items.isEmpty()){
				return items;
			}
			
			/*
			OperationQuery oq = new OperationQuery(userSession.getSystemSelectionFilter(), "o.operationName like :operationName", new String[] { "operationName" }, new Object[] { operationNameFilter + "%" });
			oq.applyTightHoleFilter(userSession.getUserUid());
			oq.applyOpsTeamFilter(userSession.getUserUid(), BooleanUtils.isTrue(userSession.getCurrentUser().getAccessToOpsTeamOperationsOnly()));
			Map<String, _Operation> operations = oq.getOperations();
			
			if (operations != null && operations.size() > 0) {
				// instead of querying operationUid and MAX(dayDate) pairs in a loop, load all of them in a single query and put in a map
				Map<String, Date> map = new HashMap<String, Date>();
				List items2 = ApplicationUtils.getConfiguredInstance().getDaoManager().find("select operationUid, MAX(dayDate) " + 
						"from Daily where (isDeleted = '0' OR isDeleted IS NULL) GROUP BY operationUid");
				if (items2 != null && !items2.isEmpty()) {
					for (Iterator i2 = items2.iterator(); i2.hasNext(); ) {
						Object[] obj_array = (Object[]) i2.next();
						map.put((String) obj_array[0], (Date) obj_array[1]);
					}
				}
				
				for (String operationUid : operations.keySet()) {
					if (map.containsKey(operationUid)) {
						result.add(new Object[] { operationUid, map.get(operationUid) });
					}else {
						output_maps2.add(operationUid);
					}	
				}
				
				Collections.sort(result, new OperationComparator());
				
				for(Iterator i3 = result.iterator(); i3.hasNext(); ){
					Object[] obj_array = (Object[]) i3.next();
					output_maps.add(obj_array[0]);
				}
				
				output_maps.addAll(output_maps2);
				
				return output_maps;
				
			}
			*/
		}
		
		return null;
	}
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
		return true;
	}
	
	@SuppressWarnings("unused")
	private class OperationComparator implements Comparator<Object[]>{
		public int compare(Object[] o1, Object[] o2){
			try{
				Object in1 = o1[1];
				Object in2 = o2[1];
				
				Date f1 = (Date) in1;
				Date f2 = (Date) in2;
				
				if (f1 == null || f2 == null) return 0;
				return f2.compareTo(f1);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	private String nullToEmptyString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return "";
	}
	
	private static void showCustomUOM(CommandBeanTreeNode node, String groupUid) throws Exception {
		Object object = node.getData();
		if (object instanceof Operation) {
			CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
			// set units for standard fields in Well.class
			for (Field field : ReportDaily.class.getDeclaredFields()) {	
				thisConverter.setReferenceMappingField(ReportDaily.class, field.getName());
				if(thisConverter.getUOMMapping() != null){
					node.setCustomUOM("@ddr." + field.getName(), thisConverter.getUOMMapping());
				}
			}
			thisConverter.dispose();
		}
	}
	
	private void loadCoreReportList(CommandBeanTreeNode node, Operation operation, UserSelectionSnapshot userSelection) throws Exception {
		int intMaxReportNumber = this.getMaxReportToShow(userSelection);
		String coreReportList = "";
		Integer intCounter = 0;
		String thisReportType = "CORE";
		String[] paramsFields = {"thisOperation", "reportFilesReportType"};
		String[] paramsValues = {operation.getOperationUid(), thisReportType};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd WHERE rf.dailyUid = rd.dailyUid and (rf.isDeleted = false or rf.isDeleted is null) and (rd.isDeleted = false or rd.isDeleted is null) and rf.reportOperationUid=:thisOperation and rf.reportType=:reportFilesReportType ORDER BY rf.reportDayDate DESC", paramsFields, paramsValues);
					
		if(items.size() > 0)
		{
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();

				ReportFiles thisReportFile = (ReportFiles) obj_array[0];
				File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());	
				
				DateFormat formatterDate = new SimpleDateFormat("dd MMM yyyy");
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S"); 
				Date date; 
				String asterisk = "";
			    
				if(report_file.exists()){
					date = (Date)formatter.parse(thisReportFile.getReportDayDate().toString()); 
					
					if (checkRigReleaseDateTime(thisReportFile.getReportOperationUid()) && checkByEngineer(thisReportFile.getDailyUid())){
						asterisk = "(*)";
					}
	
					if (StringUtils.isBlank(coreReportList)){
						coreReportList = "reportfileuid="  + URLEncoder.encode(thisReportFile.getReportFilesUid(),"utf-8") + "&reportname=" + URLEncoder.encode(formatterDate.format(date).toString(),"utf-8") + "&filename=" + URLEncoder.encode(thisReportFile.getReportFile().toString(),"utf-8") + "&asterisk=" + URLEncoder.encode(asterisk,"utf-8");
					}
					else
						coreReportList += ",reportfileuid="  + URLEncoder.encode(thisReportFile.getReportFilesUid(),"utf-8") + "&reportname=" + URLEncoder.encode(formatterDate.format(date).toString(),"utf-8") + "&filename=" + URLEncoder.encode(thisReportFile.getReportFile().toString(),"utf-8") + "&asterisk=" + URLEncoder.encode(asterisk,"utf-8");
									
					intCounter++;
					if (intCounter > intMaxReportNumber) break;
				}
			}
		}
		node.getDynaAttr().put("coreReportList", coreReportList);
	}
	
	/**
	 * only load other report list for Geology
	 * @param node
	 * @param operation
	 * @param userSelection
	 * @throws Exception
	 */
	private void loadOtherReportList(CommandBeanTreeNode node, Operation operation, UserSelectionSnapshot userSelection) throws Exception {
		int intMaxReportNumber = this.getMaxReportToShow(userSelection);
		String otherReportList = "";
		Integer intCounter = 0;
		String thisReportTypes = "'CORE','COREEXCEL','WRLSUITERUN','FWR','BSG','GFMTR','LITHOLOG','LWDR','GSR','RDG','SWCR','FPR','FSMPR','LLRGEO'";
		String[] paramsFields = {"thisOperation"};
		String[] paramsValues = {operation.getOperationUid()};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf, ReportDaily rd " +
				"WHERE rf.dailyUid = rd.dailyUid " +
				"and (rf.isDeleted = false or rf.isDeleted is null) " +
				"and (rd.isDeleted = false or rd.isDeleted is null) " +
				"and rf.reportOperationUid=:thisOperation " +
				"and rf.reportType in (" + thisReportTypes + ") " +
				"ORDER BY rf.reportDayDate DESC," +
				"rf.reportType ASC", 
			paramsFields, paramsValues);
					
		if(items.size() > 0)
		{
			for(Iterator i = items.iterator(); i.hasNext(); ){
				Object[] obj_array = (Object[]) i.next();

				ReportFiles thisReportFile = (ReportFiles) obj_array[0];
				File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + thisReportFile.getReportFile());	
				
				DateFormat formatterDate = new SimpleDateFormat("dd MMM yyyy");
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S"); 
				Date date; 
				String asterisk = "";
			    
				if(report_file.exists()){
					date = (Date)formatter.parse(thisReportFile.getReportDayDate().toString()); 
					
					if (checkRigReleaseDateTime(thisReportFile.getReportOperationUid()) && checkByEngineer(thisReportFile.getDailyUid())){
						asterisk = "(*)";
					}
	
					if (StringUtils.isBlank(otherReportList)){
						otherReportList = "reportfileuid="  + URLEncoder.encode(thisReportFile.getReportFilesUid(),"utf-8") + "&reportname=" + URLEncoder.encode(thisReportFile.getReportType().toString(),"utf-8") + "&reportDate=" + URLEncoder.encode(formatterDate.format(date).toString(),"utf-8") + "&filename=" + URLEncoder.encode(thisReportFile.getReportFile().toString(),"utf-8") + "&asterisk=" + URLEncoder.encode(asterisk,"utf-8");
					}
					else
						otherReportList += ",reportfileuid="  + URLEncoder.encode(thisReportFile.getReportFilesUid(),"utf-8") + "&reportname=" + URLEncoder.encode(thisReportFile.getReportType().toString(),"utf-8") + "&reportDate=" + URLEncoder.encode(formatterDate.format(date).toString(),"utf-8") + "&filename=" + URLEncoder.encode(thisReportFile.getReportFile().toString(),"utf-8") + "&asterisk=" + URLEncoder.encode(asterisk,"utf-8");
									
					intCounter++;
					if (intCounter > intMaxReportNumber) break;
				}
			}
		}
		node.getDynaAttr().put("otherReportList", otherReportList);
	}
}
