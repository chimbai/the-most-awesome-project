package com.idsdatanet.d2.drillnet.reportDaily;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class PreviousReportDailyReportDataGenerator implements ReportDataGenerator {
	
	private String defaultReportType;
	
	public void setReportType(String value){
		this.defaultReportType = value;
	}

	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub	
	}
	
	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType, ReportValidation reportValidation) throws Exception {
		// TODO Auto-generated method stub
		
		//set the unit to false so always get raw value
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//get the current date from the user selection
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userContext.getUserSelection().getDailyUid());
		if (daily == null) return;
		Date todayDate = daily.getDayDate();
	
		if(this.defaultReportType != null){
			reportType = this.defaultReportType;
		}
		
		String strSql = "SELECT d.dayDate, rd.reportNumber, rd.depthMdMsl, rd.depthTvdMsl, rd.depth0600MdMsl, rd.depth0600TvdMsl, rd.reportType, rd.operationUid, rd.dailyUid, rd.lastCsgshoeMdMsl, rd.reportPeriodSummary, rd.dailyEndSummary, rd.subseaTvd FROM ReportDaily rd, Daily d WHERE rd.dailyUid = d.dailyUid AND (d.isDeleted = false or d.isDeleted is null) AND (rd.isDeleted = false or rd.isDeleted is null) AND rd.operationUid = :thisOperationUid AND d.dayDate < :userDate AND rd.reportType = :thisReportType ORDER BY rd.reportDatetime DESC";
		String[] paramsFields = {"userDate", "thisOperationUid","thisReportType"};
		Object[] paramsValues = {todayDate, userContext.getUserSelection().getOperationUid(),reportType};
		
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		if (lstResult.size() > 0){
			Object[] previousReport = (Object[]) lstResult.get(0);

			String reportDatetime = "";
			String reportNumber = "";
			Double depth0600MdMsl = 0.0;
			Double depth0600TvdMsl = 0.0;
			Double depthMdMsl = 0.0;
			Double depthTvdMsl = 0.0;
			String currentReportType = "";
			String operationUid = "";
			String previousDailyUid = "";
			Double lastCsgshoeMdMsl = 0.0;
			String reportPeriodSummary = "";
			String dailyEndSummary = "";
			Double subseaTvd = 0.0;
			
			if (previousReport[0] != null && StringUtils.isNotBlank(previousReport[0].toString())) reportDatetime = previousReport[0].toString();
			if (previousReport[1] != null && StringUtils.isNotBlank(previousReport[1].toString())) reportNumber = previousReport[1].toString();
			if (previousReport[2] != null && StringUtils.isNotBlank(previousReport[2].toString())){
				depthMdMsl = Double.parseDouble(previousReport[2].toString());
				
				CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "depth_md_msl");
				thisConverter.setBaseValue(depthMdMsl);
				depthMdMsl = thisConverter.getConvertedValue();
			}
			if (previousReport[3] != null && StringUtils.isNotBlank(previousReport[3].toString())){
				depthTvdMsl = Double.parseDouble(previousReport[3].toString());
				
				CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "depth_tvd_msl");
				thisConverter.setBaseValue(depthTvdMsl);
				depthTvdMsl = thisConverter.getConvertedValue();
			}
			if (previousReport[4] != null && StringUtils.isNotBlank(previousReport[4].toString())){
				depth0600MdMsl = Double.parseDouble(previousReport[4].toString());
				
				CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "depth_0600_md_msl");
				thisConverter.setBaseValue(depth0600MdMsl);
				depth0600MdMsl = thisConverter.getConvertedValue();
			}
			if (previousReport[5] != null && StringUtils.isNotBlank(previousReport[5].toString())){
				depth0600TvdMsl = Double.parseDouble(previousReport[5].toString());
				
				CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "depth_0600_tvd_msl");
				thisConverter.setBaseValue(depth0600TvdMsl);
				depth0600TvdMsl = thisConverter.getConvertedValue();
			}
			if (previousReport[6] != null && StringUtils.isNotBlank(previousReport[6].toString())) currentReportType = previousReport[6].toString();
			if (previousReport[7] != null && StringUtils.isNotBlank(previousReport[7].toString())) operationUid = previousReport[7].toString();
			if (previousReport[8] != null && StringUtils.isNotBlank(previousReport[8].toString())) previousDailyUid = previousReport[8].toString();
			
			if (previousReport[9] != null && StringUtils.isNotBlank(previousReport[9].toString())){
				lastCsgshoeMdMsl = Double.parseDouble(previousReport[9].toString());
				
				CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "last_csgshoe_md_msl");
				thisConverter.setBaseValue(lastCsgshoeMdMsl);
				lastCsgshoeMdMsl = thisConverter.getConvertedValue();
			}
			
			if (previousReport[10] != null && StringUtils.isNotBlank(previousReport[10].toString())) reportPeriodSummary = previousReport[10].toString();
			
			if (previousReport[11] != null && StringUtils.isNotBlank(previousReport[11].toString())) dailyEndSummary = previousReport[11].toString();
			
			if (previousReport[12] != null && StringUtils.isNotBlank(previousReport[12].toString())){
				subseaTvd = Double.parseDouble(previousReport[12].toString());
				
				CustomFieldUom thisConverter = new CustomFieldUom(userContext.getUserSelection().getLocale(), ReportDaily.class, "subsea_tvd");
				thisConverter.setBaseValue(subseaTvd);
				subseaTvd = thisConverter.getConvertedValue();
			}
			
			ReportDataNode thisReportNode = reportDataNode.addChild("PreviousReport");
			thisReportNode.addProperty("previousReportDatetime", reportDatetime);
			thisReportNode.addProperty("previousReportNumber", reportNumber);
			thisReportNode.addProperty("geologyDepth0600MdMsl", depth0600MdMsl.toString());
			thisReportNode.addProperty("geologyDepth0600TvdMsl", depth0600TvdMsl.toString());
			thisReportNode.addProperty("drillerDepthMdMsl", depthMdMsl.toString());
			thisReportNode.addProperty("drillerDepthTvdMsl", depthTvdMsl.toString());
			thisReportNode.addProperty("reportType", currentReportType);
			thisReportNode.addProperty("operationUid", operationUid);
			thisReportNode.addProperty("previousDailyUid", previousDailyUid);
			thisReportNode.addProperty("lastCsgshoeMdMsl", lastCsgshoeMdMsl.toString());
			thisReportNode.addProperty("reportPeriodSummary", reportPeriodSummary.toString());
			thisReportNode.addProperty("dailyEndSummary", dailyEndSummary.toString());
			thisReportNode.addProperty("drillerSubseaTvd", subseaTvd.toString());
		}		
	}
}
