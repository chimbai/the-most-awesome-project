package com.idsdatanet.d2.drillnet.rigpump;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.RigPump;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class RigPumpDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	//private static final String CUSTOM_FROM_MARKER = "{_custom_from_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		/*if(StringUtils.isNotBlank(fromClause) && fromClause.indexOf(CUSTOM_FROM_MARKER) < 0) return null;
		
		String currentClass	= meta.getTableClass().getSimpleName();
		String customFrom = "";
		
		if(currentClass.equals("RigPump")) {
			customFrom = "RigPump rigPump, RigPumpParam rigPumpParam";			
			return fromClause.replace(CUSTOM_FROM_MARKER, customFrom);
		}*/
		return null;
	}
	

	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, 
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;

		String currentClass		= meta.getTableClass().getSimpleName();
		String customCondition 	= "";
		
		if(currentClass.equals("RigPump")) {
			Date currentDate = null;
			String rigInformationUid = null;
			
			if (userSelection.getRigInformationUid()!= null)
			{
				rigInformationUid = userSelection.getRigInformationUid();
			}
			
			if (userSelection.getDailyUid()!=null)
			{
				Daily currentDay =  ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				if (currentDay !=null) currentDate = currentDay.getDayDate();
			}	
			else
			{
				rigInformationUid = null;
			}
			
			//customCondition = "rigPump.groupUid = session.groupUid and (rigPump.isDeleted = false or rigPump.isDeleted is null) and rigPump.rigInformationUid = session.rigInformationUid and rigPump.rigPumpUid = rigPumpParam.rigPumpUid and rigPumpParam.dailyUid = session.dailyUid";
			customCondition = "(isDeleted = false or isDeleted is null) and rigInformationUid =:rigInformationUid and ((installDate <=:dayDate and removeDate >=:dayDate) or (installDate is null and removeDate is null) or (installDate <=:dayDate and removeDate is null) or (installDate is null and removeDate >=:dayDate)) ";
			query.addParam("dayDate", currentDate);
			query.addParam("rigInformationUid", rigInformationUid);									
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
			
		}
		return null;
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		node.getDynaAttr().put("operationCode", userSelection.getOperationType());
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		node.getDynaAttr().put("operationCode", userSelection.getOperationType());
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		super.beforeDataNodeSaveOrUpdate(commandBean, node, session, status, request);
		
		Object object = node.getData();
		
		//Validation Code for Installed Date > Remove Date
		if (object.equals("RigPump")) {
			RigPump thisRigPump = (RigPump) object;
			
			if(thisRigPump.getInstallDate()!= null && thisRigPump.getRemoveDate()!= null){
				if(thisRigPump.getInstallDate().getTime() > thisRigPump.getRemoveDate().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "removeDate", "Remove Date cannot be earlier than Install Date.");
					return;
				}
			}
		}
	}
	
	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
}
