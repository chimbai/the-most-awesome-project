package com.idsdatanet.d2.drillnet.report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserRequestContext;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CombinedAllReportModule extends DefaultReportModule {
	//private static final String FIELD_SELECTED_DAYS = "reportSelectedDays";
	private SimpleDateFormat formatter = null;
	
	protected ReportOutputFile onLoadReportOutputFile(ReportOutputFile reportOutputFile){
		if(reportOutputFile.isAssociatedDailyAvailable()){
			try {
				// exclude this report from the list if the related reportDaily is outside the access scope
				ReportDaily reportDaily = getRelatedReportDaily(reportOutputFile.getDailyUid(), this.getDailyType());
				UserSession userSession = UserRequestContext.getThreadLocalInstance().getUserSessionOptional();
				if (userSession != null && !userSession.withinAccessScope(reportDaily)) {
					return null;
				}
			} catch (Exception e) {
				// if we can't figure out the access scope, bail
			}
			return reportOutputFile;
		}else{
			return null;
		}
	}

	protected List<ReportFiles> loadSourceReportFilesFromDB(UserSelectionSnapshot userSelection, String reportType) throws Exception {
		List<Object> param_values = new ArrayList<Object>();
		List<String> param_names = new ArrayList<String>();
		String query = null;
		
		
		param_names.add("operationUid"); param_values.add(userSelection.getOperationUid());
		query = "from ReportFiles where reportOperationUid = :operationUid and (isDeleted = false or isDeleted is null) order by reportType, dateGenerated desc";
		

		return ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, param_names, param_values);
	}
	
	protected String getReportDisplayName(ReportFiles reportFile, Operation operation, UserSelectionSnapshot userSelection) throws Exception{
		if(StringUtils.isNotBlank(reportFile.getDisplayName())) return reportFile.getDisplayName();
		if(operation == null){
			//change file name to auto follow report type
			return this.getReportType() + " " + this.formatter.format(reportFile.getReportDayDate());
		}else{
			return super.getReportDisplayName(reportFile, operation, userSelection);
		}
	}
	
	
}
