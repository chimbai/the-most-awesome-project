package com.idsdatanet.d2.drillnet.operationPlanMaster;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.FileManagerFiles;
import com.idsdatanet.d2.core.model.LookupPhaseCode;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationPlanDetail;
import com.idsdatanet.d2.core.model.OperationPlanMaster;
import com.idsdatanet.d2.core.model.OperationPlanPhase;
import com.idsdatanet.d2.core.model.OperationPlanTask;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.web.filemanager.FileManagerUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.report.DefaultReportModule;
import com.idsdatanet.d2.drillnet.report.ReportJobStatus;

import edu.emory.mathcs.backport.java.util.Collections;

public class OperationPlanDetailDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener {
	
	private DynamicLookaheadDVDPlan dvdPlanHandler = new DefaultDynamicLookaheadDVDPlanImpl();
	private DefaultReportModule dvdPlanReportModule = null;
	
	public void setDvdPlanReportModule(DefaultReportModule dvdPlanReportModule) {
		this.dvdPlanReportModule = dvdPlanReportModule;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof OperationPlanPhase) {
			OperationPlanPhase planPhase = (OperationPlanPhase) object;
			if(StringUtils.isEmpty(planPhase.getOperationPlanMasterUid())) {
				savePlanMaster(planPhase, node);
			}
		}
		if (object instanceof OperationPlanDetail) {
			if(node.getParent() != null) {
				if (node.getParent().getData() != null) {
					OperationPlanTask planTask = (OperationPlanTask) node.getParent().getData();
					OperationPlanDetail planDetail = (OperationPlanDetail) object;
					planDetail.setOperationPlanPhaseUid(planTask.getOperationPlanPhaseUid());
				}
			}	
			
			if(PropertyUtils.getProperty(object, "p50Duration") != null && PropertyUtils.getProperty(object, "plannedStartDatetime") != null) {
				OperationPlanDetail planDetail = (OperationPlanDetail) object;
				if(planDetail.getP50Duration() == null || planDetail.getPlannedStartDatetime() == null) {
					commandBean.getSystemMessage().addError("Planned Start Date/Time and/or duration is missing. Please enter to proceed.");
					status.addError("Planned Start Date/Time and/or duration is missing. Please enter to proceed.");
					status.setContinueProcess(false);
				} else {
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
					thisConverter.setReferenceMappingField(OperationPlanDetail.class, "p50_duration");
					thisConverter.setBaseValueFromUserValue((planDetail.getP50Duration() == null ? 0.00 : planDetail.getP50Duration()));
					long calculatedPlannedEndDatetime = planDetail.getPlannedStartDatetime().getTime() + (new Double(thisConverter.getBasevalue()).longValue() * 1000);
					planDetail.setPlannedEndDatetime(new Date(calculatedPlannedEndDatetime));
				}
			}
		}
	}

	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection,	HttpServletRequest request) throws Exception {

	}
	
	public void onDataNodePasteAsNew(CommandBean commandBean, CommandBeanTreeNode sourceNode, CommandBeanTreeNode targetNode, UserSession userSession) throws Exception{
		Object object = sourceNode.getData();
		targetNode.getDynaAttr().put("isFirstNode", (isFirstInCollection(targetNode) ? "1" : "0"));
		//isVeryFirstNode is not possible copy and paste as new.
		targetNode.getDynaAttr().put("isVeryFirstNode", "0");
		targetNode.getDynaAttr().put("isBatchDrilling", sourceNode.getDynaAttr().get("isBatchDrilling"));
		int sequence = 0;
		List<CommandBeanTreeNode> list = null;
		Map<String, CommandBeanTreeNode> details = targetNode.getParent().getChild(targetNode.getData().getClass().getSimpleName());
		list = new ArrayList<CommandBeanTreeNode>(details.values());
		for(CommandBeanTreeNode detail : list) {
			if(detail.getAtts().getEditMode()) {
				PropertyUtils.setProperty(detail.getData(), "plannedStartDatetime", null);
				if (object instanceof OperationPlanPhase) {
					PropertyUtils.setProperty(detail.getData(), "operationPlanMasterUid", null);
				}
				PropertyUtils.setProperty(detail.getData(), "plannedEndDatetime", null);
				PropertyUtils.setProperty(detail.getData(), "p50Duration", null);
				PropertyUtils.setProperty(detail.getData(), "tlDuration", null);
				PropertyUtils.setProperty(detail.getData(), "sequence", ++sequence);
			} else if((int) PropertyUtils.getProperty(detail.getData(), "sequence") > sequence) {
				sequence = (int) PropertyUtils.getProperty(detail.getData(), "sequence");
			}			
		}
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		UserSession session = UserSession.getInstance(request);
		this.setActiveOperationPlanMaster(session.getCurrentOperationUid());
		//Calculate planned duration from operation plan phase
		if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoSumP50DurationToOperation")))
		{
			CommonUtil.getConfiguredInstance().calcPlannedDurationFromOperationPlanPhase(session.getCurrentOperationUid());
		}
		
		Operation operation = session.getCurrentOperation();
		this.calculatePlanPhaseDetailStartAndEndDateTime(commandBean, commandBean.getRoot(), operation, true, session);
		
		if(commandBean instanceof BaseCommandBean) {
			BaseCommandBean bean = (BaseCommandBean) commandBean;
			DataNodeAllowedAction allowAction = bean.getDataNodeAllowedAction();
			if(allowAction instanceof DvdPlanLocker) {
				((DvdPlanLocker) allowAction).lockAllDvdPlans(bean.getRoot());
				bean.getFlexClientControl().setForceLoadReferenceData(true);
				
			}
		}
	}

	public void beforeDataLoad(CommandBean commandBean,	CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {

	}

	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {	}

	public void init(CommandBean commandBean) throws Exception {
	}

	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {
		if(StringUtils.isNotBlank(invocationKey)) {
			if(invocationKey.equalsIgnoreCase("check_report_status")  && this.dvdPlanReportModule != null) {
				ReportJobStatus status = this.dvdPlanReportModule.getReportJobStatus();
				if(status.getStatus() == ReportJobStatus.STATUS_JOB_DONE){
					ReportFiles reportFiles = this.dvdPlanReportModule.reportGeneratedFiles;
					if(reportFiles != null) {
						//set the report file name to report types display name pattern
						reportFiles.setReportFile(reportFiles.getDisplayName());
					}	
				}
				this.dvdPlanReportModule.onCustomFilterInvoked(request, response, commandBean, "check_report_status");
			} else if(invocationKey.equalsIgnoreCase("getIsBatchDrilling")) {
				String operationUid = UserSession.getInstance(request).getCurrentOperationUid();
				if(StringUtils.isNotBlank(operationUid)) {
					Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, operationUid);
					SimpleXmlWriter writer = new SimpleXmlWriter(response);
					writer.startElement("root");
					writer.addElement("isBatchDrilling", ((operation.getIsBatchDrill() != null && operation.getIsBatchDrill()) ? "1" : "0"));
					writer.endElement();
					writer.close();
				}
			}
		}
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		String action = request.getParameter("action");
		if(StringUtils.isNotBlank(action) && action.equalsIgnoreCase("calculatePlanStartAndEndDateTime")) {
			Map<String, CommandBeanTreeNode> planPhaseNodes = targetCommandBeanTreeNode.getChild(OperationPlanPhase.class.getSimpleName());
			if(planPhaseNodes == null || planPhaseNodes.isEmpty()) {
				return;
			}
			List<CommandBeanTreeNode> list = new ArrayList<CommandBeanTreeNode>(planPhaseNodes.values());
			Collections.sort(list, new DvdPlanSequenceComparator());
			OperationPlanPhase firstNode = (OperationPlanPhase) list.get(0).getData();
			if(firstNode.getPlannedStartDatetime() == null) {
				commandBean.getSystemMessage().addInfo("First Planned Start Date/Time is missing. Please enter to proceed.");
				return;
			}
			
			calculateDVDPlannedStartAndEndDateTime(commandBean, targetCommandBeanTreeNode);
		}else if(StringUtils.isNotBlank(action) && action.equalsIgnoreCase("calculatePlanPhaseDetailStartAndEndDateTime")) {
			UserSession session = UserSession.getInstance(request);
			Operation operation = session.getCurrentOperation();
			calculatePlanPhaseDetailStartAndEndDateTime(commandBean, targetCommandBeanTreeNode, operation, false, session);
		}else if(StringUtils.isNotBlank(action) && action.equalsIgnoreCase("generateDvdPlanReport")) {
			if(this.dvdPlanReportModule != null) {
				this.dvdPlanReportModule.onSubmitForServerSideProcess(commandBean, request, targetCommandBeanTreeNode);
			}
		} else {
			Object data = targetCommandBeanTreeNode.getData();
			if(data instanceof OperationPlanDetail) {
				OperationPlanDetail planDetail = (OperationPlanDetail) data;
				if(planDetail.getP50Duration() != null && planDetail.getPlannedStartDatetime() != null) {
					//assume planDetail.getP50Duration() always in hours and double typed
					Double minutes =  (planDetail.getP50Duration() *  60);
					planDetail.setPlannedEndDatetime(DateUtils.addMinutes(planDetail.getPlannedStartDatetime(), minutes.intValue()));
				}
			}
		}
	}

	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
	
		if (object instanceof OperationPlanTask) {
			OperationPlanTask thisPlanTask = (OperationPlanTask) object;
			String planPhaseUid = thisPlanTask.getOperationPlanPhaseUid();
			String planTaskUid = thisPlanTask.getOperationPlanTaskUid();
			updateTaskActivity(thisPlanTask);
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DVD_PLAN_PHASE_DURATION))){
				saveDuration(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_PHASE_BUDGET))){
				savePhaseBudget(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_TASK_BUDGET))){
				saveTaskBudget(planTaskUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_TASK_DURATION))){
				saveTaskDuration(planTaskUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_PLAN_TASK_DEPTH))){
				populateTaskPlannedDepth(planTaskUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_PLAN_PHASE_DEPTH))){
				populatePlannedDepth(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_TASK_ACTUAL_COST))){
				saveTaskActualCost(planTaskUid, node); 
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_PHASE_ACTUAL_COST))){
				savePhaseActualCost(planPhaseUid, node); 
			}		
		} else if (object instanceof OperationPlanPhase) {
			OperationPlanPhase thisPlanPhase = (OperationPlanPhase) object;
			String planMasterUid = thisPlanPhase.getOperationPlanMasterUid();
			String planPhaseUid = thisPlanPhase.getOperationPlanPhaseUid();
			updatePhaseActivity(thisPlanPhase);
			calculateCumDayCurve(planMasterUid);
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_PHASE_BUDGET))){
				savePhaseBudget(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_PLAN_PHASE_DEPTH))){
				populatePlannedDepth(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DVD_PLAN_PHASE_DURATION))){
				saveDuration(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_PHASE_ACTUAL_COST))){
				savePhaseActualCost(planPhaseUid, node); 
			}
			
		} else if (object instanceof OperationPlanDetail) {
			OperationPlanDetail thisPlanDetail = (OperationPlanDetail) object;
			String planTaskUid = thisPlanDetail.getOperationPlanTaskUid();
			updateDetailActivity(thisPlanDetail);
			Object obj = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanTask.class, planTaskUid);
			OperationPlanTask opt;
			String planPhaseUid = null;
			if(obj != null) {
				opt = (OperationPlanTask) obj;
				planPhaseUid = opt.getOperationPlanPhaseUid();
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_TASK_BUDGET))){
				saveTaskBudget(planTaskUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_TASK_DURATION))){
				saveTaskDuration(planTaskUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_PLAN_TASK_DEPTH))){
				populateTaskPlannedDepth(planTaskUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_PHASE_BUDGET))){
				savePhaseBudget(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DVD_PLAN_PHASE_DURATION))){
				saveDuration(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_PLAN_PHASE_DEPTH))){
				populatePlannedDepth(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_TASK_ACTUAL_COST))){
				saveTaskActualCost(planTaskUid, node); 
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_PHASE_ACTUAL_COST))){
				savePhaseActualCost(planPhaseUid, node); 
			}
		}
	}
	
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{

		Object object = node.getData();
		
		if (object instanceof OperationPlanTask) {
			OperationPlanTask thisPlanTask = (OperationPlanTask) object;
			String planPhaseUid = thisPlanTask.getOperationPlanPhaseUid();
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DVD_PLAN_PHASE_DURATION))){
				saveDuration(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_PHASE_BUDGET))){
				savePhaseBudget(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_PLAN_PHASE_DEPTH))){
				populatePlannedDepth(planPhaseUid, node);
			}
		} else if (object instanceof OperationPlanPhase) {
			OperationPlanPhase thisPlanPhase = (OperationPlanPhase) object;
			String planMasterUid = thisPlanPhase.getOperationPlanMasterUid();
			calculateCumDayCurve(planMasterUid);
		} else if (object instanceof OperationPlanDetail) {
			OperationPlanDetail thisPlanDetail = (OperationPlanDetail) object;
			String planTaskUid = thisPlanDetail.getOperationPlanTaskUid();
			Object obj = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OperationPlanTask.class, planTaskUid);
			OperationPlanTask opt;
			String planPhaseUid = null;
			if(obj != null) {
				opt = (OperationPlanTask) obj;
				planPhaseUid = opt.getOperationPlanPhaseUid();
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_TASK_BUDGET))){
				saveTaskBudget(planTaskUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_TASK_DURATION))){
				saveTaskDuration(planTaskUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_PLAN_TASK_DEPTH))){
				populateTaskPlannedDepth(planTaskUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_PHASE_BUDGET))){
				savePhaseBudget(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DVD_PLAN_PHASE_DURATION))){
				saveDuration(planPhaseUid, node);
			}
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_PLAN_PHASE_DEPTH))){
				populatePlannedDepth(planPhaseUid, node);
			}
		}
		
	}
	
	private void calculateCumDayCurve(String planMasterUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"thisPlanMasterUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = planMasterUid;
				
		String strSql = "SELECT sum(p50Duration) as totalP50Duration, sum(duration) as totalDuration from OperationPlanPhase WHERE operationPlanMasterUid =:thisPlanMasterUid AND (isDeleted = false or isDeleted is null)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double cumCurveDay =0.0;
		
		if (lstResult.size() > 0){
			Object[] thisResult = (Object[]) lstResult.get(0);
			
			if (thisResult[0] != null && thisResult[1] != null){
				cumCurveDay = Double.parseDouble(thisResult[0].toString()) - Double.parseDouble(thisResult[1].toString());
			}
		}
		
		String strSql1 = "UPDATE OperationPlanMaster SET cumDayCurve =:cumCurveDay WHERE operationPlanMasterUid =:thisPlanMasterUid";
		String[] paramsFields1 = {"cumCurveDay", "thisPlanMasterUid"};
		Object[] paramsValues1 = {cumCurveDay, planMasterUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);

	}
	
	private void savePlanMaster(OperationPlanPhase thisPlanPhase, CommandBeanTreeNode node) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);

		String[] paramsFields = {"operationUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = thisPlanPhase.getOperationUid();					
		String strSql = "SELECT operationPlanMasterUid from OperationPlanMaster WHERE operationUid =:operationUid AND (isDeleted = false or isDeleted is null) ORDER BY lastEditDatetime DESC";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		String planMasterUid = null;
			
		if (lstResult.size() > 0){
			Object thisResult = (Object) lstResult.get(0);
				
			if (thisResult != null){
				planMasterUid = thisResult.toString();
			}
		}
			
		thisPlanPhase.setOperationPlanMasterUid(planMasterUid);
		return;
	}
	
	private void saveDuration(String planPhaseUid, CommandBeanTreeNode node) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"thisPlanPhaseUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = planPhaseUid;
					
		String strSql = "SELECT sum(tlDuration) as totalTlDuration, sum(p10Duration) as totalP10Duration, sum(p50Duration) as totalP50Duration, sum(p90Duration) as totalP90Duration from OperationPlanTask WHERE operationPlanPhaseUid =:thisPlanPhaseUid AND (isDeleted = false or isDeleted is null)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double totalTlDuration = 0.0;
		Double totalP10Duration = 0.0;
		Double totalP50Duration = 0.0;
		Double totalP90Duration = 0.0;
			
		if (lstResult.size() > 0){
			Object[] thisResult = (Object[]) lstResult.get(0);
				
			if (thisResult[0] != null){
				totalTlDuration = Double.parseDouble(thisResult[0].toString());
			}
			if (thisResult[1] != null) {
				totalP10Duration = Double.parseDouble(thisResult[1].toString());
			}
			if (thisResult[2] != null) {
				totalP50Duration = Double.parseDouble(thisResult[2].toString());
			}				
			if (thisResult[3] != null) {
				totalP90Duration = Double.parseDouble(thisResult[3].toString());
			}
		}
			
		String strSql1 = "UPDATE OperationPlanPhase SET tlDuration =:totalTlDuration, p10Duration =:totalP10Duration, p50Duration =:totalP50Duration, p90Duration =:totalP90Duration WHERE operationPlanPhaseUid =:thisPhaseUid";
		String[] paramsFields1 = {"totalTlDuration", "totalP10Duration", "totalP50Duration" ,"totalP90Duration", "thisPhaseUid"};
		Object[] paramsValues1 = {totalTlDuration, totalP10Duration, totalP50Duration ,totalP90Duration, planPhaseUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);

		return;
	}
	
	private void saveTaskDuration(String planTaskUid, CommandBeanTreeNode node) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"thisPlanTaskUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = planTaskUid;
					
		String strSql = "SELECT sum(tlDuration) as totalTlDuration, sum(p10Duration) as totalP10Duration, sum(p50Duration) as totalP50Duration, sum(p90Duration) as totalP90Duration from OperationPlanDetail WHERE operationPlanTaskUid =:thisPlanTaskUid AND (isDeleted = false or isDeleted is null)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double totalTlDuration = 0.0;
		Double totalP10Duration = 0.0;
		Double totalP50Duration = 0.0;
		Double totalP90Duration = 0.0;
			
		if (lstResult.size() > 0){
			Object[] thisResult = (Object[]) lstResult.get(0);
				
			if (thisResult[0] != null){
				totalTlDuration = Double.parseDouble(thisResult[0].toString());
			}
			if (thisResult[1] != null) {
				totalP10Duration = Double.parseDouble(thisResult[1].toString());
			}
			if (thisResult[2] != null) {
				totalP50Duration = Double.parseDouble(thisResult[2].toString());
			}				
			if (thisResult[3] != null) {
				totalP90Duration = Double.parseDouble(thisResult[3].toString());
			}
		}
			
		String strSql1 = "UPDATE OperationPlanTask SET tlDuration =:totalTlDuration, p10Duration =:totalP10Duration, p50Duration =:totalP50Duration, p90Duration =:totalP90Duration WHERE operationPlanTaskUid =:thisTaskUid";
		String[] paramsFields1 = {"totalTlDuration", "totalP10Duration", "totalP50Duration" ,"totalP90Duration", "thisTaskUid"};
		Object[] paramsValues1 = {totalTlDuration, totalP10Duration, totalP50Duration ,totalP90Duration, planTaskUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);
		
		return;
	}
	
	private void savePhaseBudget(String planPhaseUid, CommandBeanTreeNode node) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);

		String[] paramsFields = {"thisPlanPhaseUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = planPhaseUid;
					
		String strSql = "SELECT sum(taskBudget) from OperationPlanTask WHERE operationPlanPhaseUid =:thisPlanPhaseUid AND (isDeleted = false or isDeleted is null)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double totalTaskBudget = 0.0;
			
		if (lstResult.size() > 0){
			Object thisResult = (Object) lstResult.get(0);
				
			if (thisResult != null){
				totalTaskBudget = Double.parseDouble(thisResult.toString());
			}
		}
			
		String strSql1 = "UPDATE OperationPlanPhase SET phaseBudget =:totalTaskBudget WHERE operationPlanPhaseUid =:thisPhaseUid";
		String[] paramsFields1 = {"totalTaskBudget", "thisPhaseUid"};
		Object[] paramsValues1 = {totalTaskBudget, planPhaseUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);

		return;
	}
	
	private void saveTaskBudget(String planTaskUid, CommandBeanTreeNode node) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);

		String[] paramsFields = {"thisPlanTaskUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = planTaskUid;
					
		String strSql = "SELECT sum(detailBudget) from OperationPlanDetail WHERE operationPlanTaskUid =:thisPlanTaskUid AND (isDeleted = false or isDeleted is null)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double totalDetailBudget = 0.0;
			
		if (lstResult.size() > 0){
			Object thisResult = (Object) lstResult.get(0);
				
			if (thisResult != null){
				totalDetailBudget = Double.parseDouble(thisResult.toString());
			}
		}
			
		String strSql1 = "UPDATE OperationPlanTask SET taskBudget =:totalDetailBudget WHERE operationPlanTaskUid =:planTaskUid";
		String[] paramsFields1 = {"totalDetailBudget", "planTaskUid"};
		Object[] paramsValues1 = {totalDetailBudget, planTaskUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);

		return;
	}
	
	private void populatePlannedDepth(String planPhaseUid, CommandBeanTreeNode node) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);

		String[] paramsFields = {"thisPlanPhaseUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = planPhaseUid;
					
		String strSql = "SELECT depthMdMsl from OperationPlanTask WHERE operationPlanPhaseUid =:thisPlanPhaseUid AND (isDeleted = false or isDeleted is null) ORDER BY sequence DESC";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double lastDepthMdMsl = 0.0;
			
		if (lstResult.size() > 0){
			Object thisResult = (Object) lstResult.get(0);
				
			if (thisResult != null){
				lastDepthMdMsl = Double.parseDouble(thisResult.toString());
			}
		}
			
		String strSql1 = "UPDATE OperationPlanPhase SET depthMdMsl =:lastDepthMdMsl WHERE operationPlanPhaseUid =:thisPhaseUid";
		String[] paramsFields1 = {"lastDepthMdMsl", "thisPhaseUid"};
		Object[] paramsValues1 = {lastDepthMdMsl, planPhaseUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);

		return;
	}
	
	private void populateTaskPlannedDepth(String planTaskUid, CommandBeanTreeNode node) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);

		String[] paramsFields = {"thisPlanTaskUid"};
		Object[] paramsValues = new Object[1]; paramsValues[0] = planTaskUid;
					
		String strSql = "SELECT depthMdMsl from OperationPlanDetail WHERE operationPlanTaskUid =:thisPlanTaskUid AND (isDeleted = false or isDeleted is null) ORDER BY sequence DESC";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double lastDepthMdMsl = 0.0;
			
		if (lstResult.size() > 0){
			Object thisResult = (Object) lstResult.get(0);
				
			if (thisResult != null){
				lastDepthMdMsl = Double.parseDouble(thisResult.toString());
			}
		}
			
		String strSql1 = "UPDATE OperationPlanTask SET depthMdMsl =:lastDepthMdMsl WHERE operationPlanTaskUid =:thisTaskUid";
		String[] paramsFields1 = {"lastDepthMdMsl", "thisTaskUid"};
		Object[] paramsValues1 = {lastDepthMdMsl, planTaskUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);

		return;
	}
	
	private void updatePhaseActivity(OperationPlanPhase planPhase) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String planPhaseUid = planPhase.getOperationPlanPhaseUid();
			
		String strSql = "UPDATE Activity SET phaseCode =:phaseCode, holeSize =:holeSize WHERE planReference =:thisPhaseUid";
		String[] paramsFields = {"phaseCode", "holeSize", "thisPhaseUid"};
		Object[] paramsValues = {planPhase.getPhaseCode(), planPhase.getHoleSize(), planPhaseUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues, qp);

		return;
	}
	
	private void updateTaskActivity(OperationPlanTask planTask) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String planTaskUid = planTask.getOperationPlanTaskUid();
			
		String strSql = "UPDATE Activity SET taskCode =:taskCode WHERE planTaskReference =:thisTaskUid";
		String[] paramsFields = {"taskCode", "thisTaskUid"};
		Object[] paramsValues = {planTask.getTaskCode(), planTaskUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues, qp);

		return;
	}
	
	private void updateDetailActivity(OperationPlanDetail planDetail) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String planDetailUid = planDetail.getOperationPlanDetailUid();
			
		String strSql = "UPDATE Activity SET userCode =:userCode WHERE planDetailReference =:thisDetailUid";
		String[] paramsFields = {"userCode", "thisDetailUid"};
		Object[] paramsValues = {planDetail.getDetailCode(), planDetailUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues, qp);

		return;
	}
	
	private void setActiveOperationPlanMaster(String operationUid) throws Exception {
		if (operationUid==null) return;
		String planMasterUid = CommonUtil.getConfiguredInstance().getActiveDvdPlanUid(operationUid);
		String queryString = "FROM OperationPlanMaster WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid";
		List<OperationPlanMaster> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", operationUid);
		for (OperationPlanMaster rec : rs) {
			if (planMasterUid.equals(rec.getOperationPlanMasterUid())) {
				rec.setDvdPlanStatus(true);
			} else {
				rec.setDvdPlanStatus(false);
			}
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rec);
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		Boolean isBatchDrilling = false;
		//String operationUid = (String) PropertyUtils.getProperty(object, "operationUid");
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Operation.class, userSelection.getOperationUid());
		if(operation.getIsBatchDrill() != null && operation.getIsBatchDrill()) isBatchDrilling = true;
		commandBean.getRoot().getDynaAttr().put("isBatchDrilling", (isBatchDrilling ? "1" : "0"));
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN || commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT){
			if (object instanceof OperationPlanPhase) {
				OperationPlanPhase thisPlanPhase = (OperationPlanPhase) object;
				
				thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");
				Double planVsActual = 0.0;
				
				if (thisPlanPhase.getP50Duration()!=null && thisPlanPhase.getDuration()!=null) {
					planVsActual = thisPlanPhase.getP50Duration() - thisPlanPhase.getDuration();
				}	
				
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@planVsActual", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("planVsActual", planVsActual);
				
				//calculate cumulative 
				
				Double tld = thisPlanPhase.getTlDuration();
				Double p10d = thisPlanPhase.getP10Duration();
				Double p50d = thisPlanPhase.getP50Duration();
				Double p90d = thisPlanPhase.getP90Duration();
				Double planCost = thisPlanPhase.getPhaseBudget();
				Double pmeand = thisPlanPhase.getPmeanDuration();
				
				String planMasterUid = thisPlanPhase.getOperationPlanMasterUid();
					
				double cumTl = 0.0;
				double cumP10 = 0.0;
				double cumP50 = 0.0;
				double cumP90 = 0.0;
				double cumPlanCost = 0.0;
				double cumPmean = 0.0;
				
				String operationPlanMasterUid = (String) commandBean.getRoot().getDynaAttr().get("planMasterUid");
			
				if (planMasterUid.equals(operationPlanMasterUid)) {
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumTlDuration")!= null) cumTl = (Double) commandBean.getRoot().getDynaAttr().get("cumTlDuration");
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumP10Duration")!= null) cumP10 = (Double) commandBean.getRoot().getDynaAttr().get("cumP10Duration");
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumP50Duration")!= null) cumP50 = (Double) commandBean.getRoot().getDynaAttr().get("cumP50Duration");
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumP90Duration")!= null) cumP90 = (Double) commandBean.getRoot().getDynaAttr().get("cumP90Duration");
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumPlanCost")!= null) cumPlanCost = (Double) commandBean.getRoot().getDynaAttr().get("cumPlanCost");
					if ((Double) commandBean.getRoot().getDynaAttr().get("cumPmeanDuration")!= null) cumPmean = (Double) commandBean.getRoot().getDynaAttr().get("cumPmeanDuration");
				}else {
					cumTl = 0.0;
					cumP10 = 0.0;
					cumP50 = 0.0;
					cumP90 = 0.0;
					cumPlanCost = 0.0;
					cumPmean = 0.0;
				}
				
				if (tld != null) cumTl += tld;
					
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "tl_duration");
					//thisConverter.setBaseValueFromUserValue(cumTl);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumTlDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumTlDuration", cumTl);
					commandBean.getRoot().getDynaAttr().put("cumTlDuration", cumTl);
			
				if (p10d != null) cumP10 += p10d;
				
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p10_duration");
					//thisConverter.setBaseValueFromUserValue(cumP10);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP10Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP10Duration", cumP10);
					commandBean.getRoot().getDynaAttr().put("cumP10Duration", cumP10);
			
				if (p50d != null) cumP50 += p50d;
				
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");
					//thisConverter.setBaseValueFromUserValue(cumP50);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP50Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP50Duration", cumP50);
					commandBean.getRoot().getDynaAttr().put("cumP50Duration", cumP50);
				
				if (p90d != null) cumP90 += p90d;
				
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p90_duration");
					//thisConverter.setBaseValueFromUserValue(cumP90);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP90Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP90Duration", cumP90);
					commandBean.getRoot().getDynaAttr().put("cumP90Duration", cumP90);
				
				if (planCost != null) cumPlanCost += planCost;
				
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "phase_budget");
					//thisConverter.setBaseValueFromUserValue(cumPlanCost);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumPlanCost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumPlanCost", cumPlanCost);
					commandBean.getRoot().getDynaAttr().put("cumPlanCost", cumPlanCost);
					
				if (pmeand != null) cumPmean += pmeand;
				
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "pmean_duration");
					//thisConverter.setBaseValueFromUserValue(cumPmean);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumPmeanDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumPmeanDuration", cumPmean);
					commandBean.getRoot().getDynaAttr().put("cumPmeanDuration", cumPmean);
					
				if (tld != null || p10d != null || p50d != null || p90d != null || pmeand != null || planCost != null) {
					commandBean.getRoot().getDynaAttr().put("planMasterUid", planMasterUid);
				}
				
				String selectedOperation = userSelection.getOperationUid();
				this.updatePlanPhaseActualDatetime(node, userSelection.getOperationUid());
				
				if (thisPlanPhase.getOperationPlanPhaseUid() != null) {
					
					//TO OBTAIN ACTUAL DURATION FROM ACTIVITY FOR SELECTED PLAN REFERENCE
					double actualDuration = 0.00;
					double activityDuration = 0.00;
					double plannedDuration = 0.00;
					double actualDepth = 0.00;
					double p_50_Duration = 0.00;
					double cumActualDuration = 0.00;
					double nptDuration = 0.00;
					double activityNptDuration = 0.00;
					double cumNptDuration = 0.00;
					//String dayplusminus = "0.0";
					
					DecimalFormat formatter = new DecimalFormat("#0.0");
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");

					actualDuration = this.dvdPlanHandler.calculateActualDuration(thisPlanPhase, selectedOperation);

					if (thisPlanPhase.getP50Duration() != null) p_50_Duration = thisPlanPhase.getP50Duration();

					thisConverter.setBaseValue(actualDuration);

					thisConverter.getUOMMapping().getUnitOutputPrecision();
					activityDuration = Math.round(thisConverter.getConvertedValue() *  Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision())) / Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision());
					plannedDuration = Math.round(p_50_Duration *  Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision())) / Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision());
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@actualDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("actualDuration", thisConverter.getConvertedValue());
					commandBean.getRoot().getDynaAttr().put("actualDuration", actualDuration);

					//Calculate Cumulative Actual Duration
					if (commandBean.getRoot().getDynaAttr().containsKey("cumActualDuration")) {
						cumActualDuration = (Double) commandBean.getRoot().getDynaAttr().get("cumActualDuration");
					}
					else {
						cumActualDuration = 0.00;
					}

					cumActualDuration += activityDuration;
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumActualDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumActualDuration", cumActualDuration);
					commandBean.getRoot().getDynaAttr().put("cumActualDuration", cumActualDuration);
					
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");
					nptDuration = this.dvdPlanHandler.calculateNptDuration(thisPlanPhase.getOperationPlanPhaseUid(), selectedOperation);
					thisConverter.setBaseValue(nptDuration);
					
					thisConverter.getUOMMapping().getUnitOutputPrecision();
					activityNptDuration = Math.round(thisConverter.getConvertedValue() *  Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision())) / Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision());
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@nptDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("nptDuration", thisConverter.getConvertedValue());
					commandBean.getRoot().getDynaAttr().put("nptDuration", nptDuration);
					
					if (commandBean.getRoot().getDynaAttr().containsKey("cumNptDuration")) {
						cumNptDuration = (Double) commandBean.getRoot().getDynaAttr().get("cumNptDuration");
					}
					else {
						cumNptDuration = 0.00;
					}
					
					cumNptDuration += activityNptDuration;
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumNptDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumNptDuration", cumNptDuration);
					commandBean.getRoot().getDynaAttr().put("cumNptDuration", cumNptDuration);
						//}
					//}
					
					//TO CALCULATE DAY +/- FROM ACTUAL/PLANNED DURATION
					if (activityDuration > 0) {
						if (thisConverter.isUOMMappingAvailable()){
							node.setCustomUOM("@dayplusminus", thisConverter.getUOMMapping());
						}
						node.getDynaAttr().put("dayplusminus",this.dvdPlanHandler.calculateDayPlusMinus(activityDuration, plannedDuration));
					}
					
					//TO OBTAIN ACTUAL DEPTH FROM ACTIVITY FOR SELECTED PLAN REFERENCE
					thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");

					actualDepth = this.dvdPlanHandler.calculateActualDepth(thisPlanPhase, selectedOperation);

					thisConverter.setBaseValue(actualDepth);

					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@actualDepth", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("actualDepth", thisConverter.getConvertedValue());
					commandBean.getRoot().getDynaAttr().put("actualDepth", actualDepth);
					
					//TO OBTAIL ACTUAL COST FROM REPORT DAILY AND ACTIVITY, BASED ON SELECTED PLAN REFERENCE
					Double actualCost = this.dvdPlanHandler.calculateActualCost(thisPlanPhase, selectedOperation, request);
					thisConverter.setReferenceMappingField(ReportDaily.class, "daycost");
					thisConverter.setBaseValue(actualCost);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@actualCost", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("actualCost", thisConverter.getConvertedValue());
					commandBean.getRoot().getDynaAttr().put("actualCost", actualCost);
				}				
			}
			if (object instanceof OperationPlanTask) {
				OperationPlanTask thisPlanTask = (OperationPlanTask) object;
				
				Double tld = thisPlanTask.getTlDuration();
				Double p10d = thisPlanTask.getP10Duration();
				Double p50d = thisPlanTask.getP50Duration();
				Double p90d = thisPlanTask.getP90Duration();
					
				double cumTl = 0.0;
				double cumP10 = 0.0;
				double cumP50 = 0.0;
				double cumP90 = 0.0;
				
				if (commandBean.getRoot().getDynaAttr().containsKey("planPhaseUid")) {
					String rootPlanPhaseUid = (String) commandBean.getRoot().getDynaAttr().get("planPhaseUid");
					if(thisPlanTask.getOperationPlanPhaseUid().equals(rootPlanPhaseUid)) {
						if (commandBean.getRoot().getDynaAttr().containsKey("cumTlDurationTask")) {
							cumTl = (Double) commandBean.getRoot().getDynaAttr().get("cumTlDurationTask");
						}
						if (commandBean.getRoot().getDynaAttr().containsKey("cumP10DurationTask")) {
							cumP10 = (Double) commandBean.getRoot().getDynaAttr().get("cumP10DurationTask");
						}
						if (commandBean.getRoot().getDynaAttr().containsKey("cumP50DurationTask")) {
							cumP50 = (Double) commandBean.getRoot().getDynaAttr().get("cumP50DurationTask");
						}
						if (commandBean.getRoot().getDynaAttr().containsKey("cumP90DurationTask")) {
							cumP90 = (Double) commandBean.getRoot().getDynaAttr().get("cumP90DurationTask");
						}
					} else {
						commandBean.getRoot().getDynaAttr().put("cumTlDurationTask", cumTl);
						commandBean.getRoot().getDynaAttr().put("cumP10DurationTask", cumP10);
						commandBean.getRoot().getDynaAttr().put("cumP50DurationTask", cumP50);
						commandBean.getRoot().getDynaAttr().put("cumP90DurationTask", cumP90);
					}
				}
				
				if (tld != null) cumTl += tld;
					
					thisConverter.setReferenceMappingField(OperationPlanTask.class, "tl_duration");
					//thisConverter.setBaseValueFromUserValue(cumTl);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumTlDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumTlDuration", cumTl);
					commandBean.getRoot().getDynaAttr().put("cumTlDurationTask", cumTl);
			
				if (p10d != null) cumP10 += p10d;
				
					thisConverter.setReferenceMappingField(OperationPlanTask.class, "p10_duration");
					//thisConverter.setBaseValueFromUserValue(cumP10);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP10Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP10Duration", cumP10);
					commandBean.getRoot().getDynaAttr().put("cumP10DurationTask", cumP10);
			
				if (p50d != null) cumP50 += p50d;
				
					thisConverter.setReferenceMappingField(OperationPlanTask.class, "p50_duration");
					//thisConverter.setBaseValueFromUserValue(cumP50);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP50Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP50Duration", cumP50);
					commandBean.getRoot().getDynaAttr().put("cumP50DurationTask", cumP50);
				
				if (p90d != null) cumP90 += p90d;
				
					thisConverter.setReferenceMappingField(OperationPlanTask.class, "p90_duration");
					//thisConverter.setBaseValueFromUserValue(cumP90);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP90Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP90Duration", cumP90);
					commandBean.getRoot().getDynaAttr().put("cumP90DurationTask", cumP90);
				
				
				String selectedOperation = userSelection.getOperationUid();
				this.updatePlanTaskActualDatetime(node, userSelection.getOperationUid());
				
				if (thisPlanTask.getOperationPlanTaskUid() != null) {
					
					//TO OBTAIN ACTUAL DURATION FROM ACTIVITY FOR SELECTED PLAN REFERENCE
					double actualDuration = 0.00;
					double activityDuration = 0.00;
					double actualDepth = 0.00;
					double cumActualDuration = 0.00;
					double nptDuration = 0.00;
					double activityNptDuration = 0.00;
					double cumNptDuration = 0.00;
					
					if (commandBean.getRoot().getDynaAttr().containsKey("planPhaseUid")) {
						String rootPlanPhaseUid = (String) commandBean.getRoot().getDynaAttr().get("planPhaseUid");
						if(thisPlanTask.getOperationPlanTaskUid().equals(rootPlanPhaseUid)) {
							if (commandBean.getRoot().getDynaAttr().containsKey("cumActualDurationTask")) {
								cumActualDuration = (Double) commandBean.getRoot().getDynaAttr().get("cumActualDurationTask");
							}
							if (commandBean.getRoot().getDynaAttr().containsKey("cumNptDurationTask")) {
								cumNptDuration = (Double) commandBean.getRoot().getDynaAttr().get("cumNptDurationTask");
							}
						} else {
							commandBean.getRoot().getDynaAttr().put("cumActualDurationTask", cumActualDuration);
							commandBean.getRoot().getDynaAttr().put("cumNptDurationTask", cumNptDuration);
						}
					}
					
					thisConverter.setReferenceMappingField(OperationPlanTask.class, "p50_duration");

					actualDuration = this.dvdPlanHandler.calculateActualDurationTask(thisPlanTask, selectedOperation);
		
					thisConverter.setBaseValue(actualDuration);
		
					thisConverter.getUOMMapping().getUnitOutputPrecision();
					activityDuration = Math.round(thisConverter.getConvertedValue() *  Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision())) / Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision());
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@actualDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("actualDuration", thisConverter.getConvertedValue());
					commandBean.getRoot().getDynaAttr().put("actualDurationTask", actualDuration);
		
					//Calculate Cumulative Actual Duration
					if (commandBean.getRoot().getDynaAttr().containsKey("cumActualDurationTask")) {
						cumActualDuration = (Double) commandBean.getRoot().getDynaAttr().get("cumActualDurationTask");
					}
		
					cumActualDuration += activityDuration;
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumActualDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumActualDuration", cumActualDuration);
					commandBean.getRoot().getDynaAttr().put("cumActualDurationTask", cumActualDuration);
					
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");
					nptDuration = this.dvdPlanHandler.calculateNptDurationTask(thisPlanTask.getOperationPlanTaskUid(), selectedOperation);
					thisConverter.setBaseValue(nptDuration);
					
					thisConverter.getUOMMapping().getUnitOutputPrecision();
					activityNptDuration = Math.round(thisConverter.getConvertedValue() *  Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision())) / Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision());
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@nptDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("nptDuration", thisConverter.getConvertedValue());
					commandBean.getRoot().getDynaAttr().put("nptDurationTask", nptDuration);
					
					if (commandBean.getRoot().getDynaAttr().containsKey("cumNptDurationTask")) {
						cumNptDuration = (Double) commandBean.getRoot().getDynaAttr().get("cumNptDurationTask");
					}
					
					cumNptDuration += activityNptDuration;
					thisConverter.setReferenceMappingField(OperationPlanPhase.class, "p50_duration");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumNptDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumNptDuration", cumNptDuration);
					commandBean.getRoot().getDynaAttr().put("cumNptDurationTask", cumNptDuration);
			
					
					//TO OBTAIN ACTUAL DEPTH FROM ACTIVITY FOR SELECTED PLAN REFERENCE
					thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");

					actualDepth = this.dvdPlanHandler.calculateActualDepthTask(thisPlanTask, selectedOperation);

					thisConverter.setBaseValue(actualDepth);

					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@actualDepth", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("actualDepth", thisConverter.getConvertedValue());
					commandBean.getRoot().getDynaAttr().put("actualDepth", actualDepth);
				}
				commandBean.getRoot().getDynaAttr().put("planPhaseUid",  thisPlanTask.getOperationPlanPhaseUid());
			}
			if (object instanceof OperationPlanDetail) {
				OperationPlanDetail thisPlanDetail = (OperationPlanDetail) object;
				node.getDynaAttr().put("isBatchDrilling", (isBatchDrilling ? "1" : "0"));
				//firstNode
				node.getDynaAttr().put("isFirstNode", (isFirstInCollection(node) ? "1" : "0"));
				//veryFirstNode
				node.getDynaAttr().put("isVeryFirstNode", (isVeryFirstInCollection(node) ? "1" : "0"));

				Double tld = thisPlanDetail.getTlDuration();
				Double p10d = thisPlanDetail.getP10Duration();
				Double p50d = thisPlanDetail.getP50Duration();
				Double p90d = thisPlanDetail.getP90Duration();
					
				double cumTl = 0.0;
				double cumP10 = 0.0;
				double cumP50 = 0.0;
				double cumP90 = 0.0;
				
				if (commandBean.getRoot().getDynaAttr().containsKey("planTaskUid")) {
					String rootPlanTaskUid = (String) commandBean.getRoot().getDynaAttr().get("planTaskUid");
					if(thisPlanDetail.getOperationPlanTaskUid().equals(rootPlanTaskUid)) {
						if (commandBean.getRoot().getDynaAttr().containsKey("cumTlDurationDetail")) {
							cumTl = (Double) commandBean.getRoot().getDynaAttr().get("cumTlDurationDetail");
						}
						if (commandBean.getRoot().getDynaAttr().containsKey("cumP10DurationDetail")) {
							cumP10 = (Double) commandBean.getRoot().getDynaAttr().get("cumP10DurationDetail");
						}
						if (commandBean.getRoot().getDynaAttr().containsKey("cumP50DurationDetail")) {
							cumP50 = (Double) commandBean.getRoot().getDynaAttr().get("cumP50DurationDetail");
						}
						if (commandBean.getRoot().getDynaAttr().containsKey("cumP90DurationDetail")) {
							cumP90 = (Double) commandBean.getRoot().getDynaAttr().get("cumP90DurationDetail");
						}
					} else {
						commandBean.getRoot().getDynaAttr().put("cumTlDurationDetail", cumTl);
						commandBean.getRoot().getDynaAttr().put("cumP10DurationDetail", cumP10);
						commandBean.getRoot().getDynaAttr().put("cumP50DurationDetail", cumP50);
						commandBean.getRoot().getDynaAttr().put("cumP90DurationDetail", cumP90);
					}
				}

				
				if (tld != null) cumTl += tld;
					
					thisConverter.setReferenceMappingField(OperationPlanDetail.class, "tl_duration");
					//thisConverter.setBaseValueFromUserValue(cumTl);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumTlDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumTlDuration", cumTl);
					commandBean.getRoot().getDynaAttr().put("cumTlDurationDetail", cumTl);
			
				if (p10d != null) cumP10 += p10d;
				
					thisConverter.setReferenceMappingField(OperationPlanDetail.class, "p10_duration");
					//thisConverter.setBaseValueFromUserValue(cumP10);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP10Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP10Duration", cumP10);
					commandBean.getRoot().getDynaAttr().put("cumP10DurationDetail", cumP10);
			
				if (p50d != null) cumP50 += p50d;
				
					thisConverter.setReferenceMappingField(OperationPlanDetail.class, "p50_duration");
					//thisConverter.setBaseValueFromUserValue(cumP50);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP50Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP50Duration", cumP50);
					commandBean.getRoot().getDynaAttr().put("cumP50DurationDetail", cumP50);
				
				if (p90d != null) cumP90 += p90d;
				
					thisConverter.setReferenceMappingField(OperationPlanDetail.class, "p90_duration");
					//thisConverter.setBaseValueFromUserValue(cumP90);
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumP90Duration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumP90Duration", cumP90);
					commandBean.getRoot().getDynaAttr().put("cumP90DurationDetail", cumP90);
				
				
				String selectedOperation = userSelection.getOperationUid();
				this.updatePlanDetailActualDatetime(node, userSelection.getOperationUid());
				if (thisPlanDetail.getOperationPlanDetailUid() != null) {
					
					//TO OBTAIN ACTUAL DURATION FROM ACTIVITY FOR SELECTED PLAN REFERENCE
					double actualDuration = 0.00;
					double activityDuration = 0.00;
					double cumActualDuration = 0.00;
					double nptDuration = 0.00;
					double activityNptDuration = 0.00;
					double cumNptDuration = 0.00;
					double actualDepth = 0.00;
					
					if (commandBean.getRoot().getDynaAttr().containsKey("planTaskUid")) {
						String rootPlanTaskUid = (String) commandBean.getRoot().getDynaAttr().get("planTaskUid");
						if(thisPlanDetail.getOperationPlanTaskUid().equals(rootPlanTaskUid)) {
							if (commandBean.getRoot().getDynaAttr().containsKey("cumActualDurationDetail")) {
								cumActualDuration = (Double) commandBean.getRoot().getDynaAttr().get("cumActualDurationDetail");
							}
							if (commandBean.getRoot().getDynaAttr().containsKey("cumNptDurationDetail")) {
								cumNptDuration = (Double) commandBean.getRoot().getDynaAttr().get("cumNptDurationDetail");
							}
						} else {
							commandBean.getRoot().getDynaAttr().put("cumActualDurationDetail", cumActualDuration);
							commandBean.getRoot().getDynaAttr().put("cumNptDurationDetail", cumNptDuration);
						}
					}
					
					thisConverter.setReferenceMappingField(OperationPlanDetail.class, "p50_duration");

					actualDuration = this.dvdPlanHandler.calculateActualDurationDetail(thisPlanDetail, selectedOperation);
		
					thisConverter.setBaseValue(actualDuration);
		
					thisConverter.getUOMMapping().getUnitOutputPrecision();
					activityDuration = Math.round(thisConverter.getConvertedValue() *  Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision())) / Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision());
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@actualDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("actualDuration", thisConverter.getConvertedValue());
		
					cumActualDuration += activityDuration;
					thisConverter.setReferenceMappingField(OperationPlanDetail.class, "p50_duration");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumActualDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumActualDuration", cumActualDuration);
					commandBean.getRoot().getDynaAttr().put("cumActualDurationDetail", cumActualDuration);
					
					thisConverter.setReferenceMappingField(OperationPlanDetail.class, "p50_duration");
					nptDuration = this.dvdPlanHandler.calculateNptDurationDetail(thisPlanDetail.getOperationPlanDetailUid(), selectedOperation);
					thisConverter.setBaseValue(nptDuration);
					
					thisConverter.getUOMMapping().getUnitOutputPrecision();
					activityNptDuration = Math.round(thisConverter.getConvertedValue() *  Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision())) / Math.pow(10, thisConverter.getUOMMapping().getUnitOutputPrecision());
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@nptDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("nptDuration", thisConverter.getConvertedValue());
					
					cumNptDuration += activityNptDuration;
					thisConverter.setReferenceMappingField(OperationPlanDetail.class, "p50_duration");
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@cumNptDuration", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("cumNptDuration", cumNptDuration);
					commandBean.getRoot().getDynaAttr().put("cumNptDurationDetail", cumNptDuration);
					
					//TO OBTAIN ACTUAL DEPTH FROM ACTIVITY FOR SELECTED PLAN REFERENCE
					thisConverter.setReferenceMappingField(Activity.class, "depth_md_msl");

					actualDepth = this.dvdPlanHandler.calculateActualDepthDetail(thisPlanDetail, selectedOperation);

					thisConverter.setBaseValue(actualDepth);

					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@actualDepth", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("actualDepth", thisConverter.getConvertedValue());
					commandBean.getRoot().getDynaAttr().put("actualDepth", actualDepth);
				}
				commandBean.getRoot().getDynaAttr().put("planTaskUid",  thisPlanDetail.getOperationPlanTaskUid());
			}
		}
		
		if (object instanceof LookupPhaseCode){
			LookupPhaseCode phasecode = (LookupPhaseCode) object;

			String selectedOperation = (String) commandBean.getRoot().getDynaAttr().get("operationUid");
			String phaseshortcode = phasecode.getShortCode();
			
			if (StringUtils.isNotBlank(selectedOperation)) {
				String strSql = "SELECT sum(a.activityDuration) as totalActualTime from Activity a, Daily d " +
						"WHERE a.operationUid =:operationUid " +
						"AND a.phaseCode =:phaseshortcode " +
						"AND (a.isDeleted = false or a.isDeleted is null) " +
						"AND (d.isDeleted = false or d.isDeleted is null) " +
						"AND (a.dayPlus is null or a.dayPlus = 0) " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null)";
				String[] paramNames = {"operationUid", "phaseshortcode"};
				Object[] paramValues = {selectedOperation, phaseshortcode};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
				
				double quickestActualTime = 0.00;
				thisConverter.setReferenceMappingField(OperationPlanPhase.class, "tl_duration");
				
				if (lstResult.size() > 0) {
					if (lstResult.get(0) != null){					
						quickestActualTime = Double.parseDouble(lstResult.get(0).toString());
					}
				}
				//thisConverter.setBaseValueFromUserValue(quickestActualTime);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@quickestActualTime", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("quickestActualTime", quickestActualTime);
				
				String strSql2 = "SELECT sum(a.activityDuration) as totalActualTime from Activity a, Daily d " +
						"WHERE a.operationUid =:operationUid " +
						"AND a.phaseCode =:phaseshortcode " +
						"AND d.dailyUid = a.dailyUid " +
						"AND a.classCode ='P' " +
						"AND (a.isDeleted = false or a.isDeleted is null) " +
						"AND (d.isDeleted = false or d.isDeleted is null) " +
						"AND (a.dayPlus is null or a.dayPlus = 0) " +
						"AND (a.isSimop=false or a.isSimop is null) " +
						"AND (a.isOffline=false or a.isOffline is null)";
				String[] paramNames2 = {"operationUid", "phaseshortcode"};
				Object[] paramValues2 = {selectedOperation, phaseshortcode};
				List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramNames2, paramValues2);
				
				double bestTime = 0.00;
				thisConverter.setReferenceMappingField(OperationPlanPhase.class, "tl_duration");
				
				if (lstResult2.size() > 0) {
					if (lstResult2.get(0) != null){					
						bestTime = Double.parseDouble(lstResult2.get(0).toString());
					}
				}
				//thisConverter.setBaseValueFromUserValue(bestTime);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@bestTime", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("bestTime", bestTime);
				
			}
		}
		
		if(object instanceof OperationPlanMaster) {
			OperationPlanMaster rec = (OperationPlanMaster) object;
			String query = "from FileManagerFiles where (isDeleted = false or isDeleted is null) and (popupAttachment = false or popupAttachment is null) and (attachmentMode = :attachmentMode or attachmentMode is null) and attachToModule = :module and attachToOperationUid = :operationUid";
			List<FileManagerFiles> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, new String[]{"attachmentMode", "module", "operationUid"}, new Object[] {FileManagerUtils.ATTACHMENT_MODE_DEFAULT, commandBean.getControllerBeanName(), rec.getOperationUid()});
			
			Boolean hasAttachmentFiles = false;
			for(FileManagerFiles dbFile: result){
				File attachedFile = FileManagerUtils.getAttachedFile(dbFile);
				hasAttachmentFiles = attachedFile.exists();
				if(hasAttachmentFiles) {
					break;
				}
			}
			node.getDynaAttr().put("hasAttachmentFiles", hasAttachmentFiles);
			
			if(commandBean instanceof BaseCommandBean) {
				BaseCommandBean bean = (BaseCommandBean) commandBean;
				DataNodeAllowedAction allowAction = bean.getDataNodeAllowedAction();
				if(allowAction != null && allowAction instanceof DvdPlanLocker) {
					if(rec.getIsLocked() == null || !rec.getIsLocked()) {
						commandBean.getSystemMessage().addInfo("This DVD Plan screen is currently not locked.");
					}else if(rec.getIsLocked() != null && rec.getIsLocked()) {
						commandBean.getSystemMessage().addInfo("This DVD Plan screen is locked. Only PIC with Lock/Unlock access can unlock record.");
					}
				}
			}
		}
	}

	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof OperationPlanMaster) {
			OperationPlanMaster planMaster = (OperationPlanMaster) node.getData();
			planMaster.setOperationUid(userSelection.getOperationUid());
		}
	}
	
	private class DvdPlanSequenceComparator implements Comparator<CommandBeanTreeNode> {
		  
	    @Override
	    public int compare(CommandBeanTreeNode rec1, CommandBeanTreeNode rec2) {
	    	try {
	    		Integer seq1 = (Integer) PropertyUtils.getProperty(rec1.getData(), "sequence");
	    		Integer seq2 = (Integer) PropertyUtils.getProperty(rec2.getData(), "sequence");
	    		
	    		if(seq1 == null || seq2 == null) {
		    		return 0;
		    	}
		    	
		    	return seq1.compareTo(seq2);

	    	}catch(Exception e) {
	    		return 0;
	    	}
	    }
	}
	
	private void calculateDVDPlannedStartAndEndDateTime(CommandBean commandBean, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		Map<String, CommandBeanTreeNode> dvdPlanPhases = targetCommandBeanTreeNode.getChild(OperationPlanPhase.class.getSimpleName());
		if(dvdPlanPhases == null || dvdPlanPhases.isEmpty()) {
			return;
		}
		
		List<CommandBeanTreeNode> list = new ArrayList<CommandBeanTreeNode>(dvdPlanPhases.values());
		Collections.sort(list, new DvdPlanSequenceComparator());
		Date nextPlannedStartDatetime = null;
		
		for(CommandBeanTreeNode rec : list) {
			OperationPlanPhase data = (OperationPlanPhase) rec.getData();
			
			if(nextPlannedStartDatetime != null) {
				data.setPlannedStartDatetime(nextPlannedStartDatetime);
			}
			
			CustomFieldUom uomConverter = new CustomFieldUom(OperationPlanPhase.class, "p50Duration");
			uomConverter.setBaseValueFromUserValue((data.getP50Duration() == null ? 0.00 : data.getP50Duration()));
			long calculatedPlannedEndDatetime = data.getPlannedStartDatetime().getTime() + (new Double(uomConverter.getBasevalue()).longValue() * 1000);
			data.setPlannedEndDatetime(new Date(calculatedPlannedEndDatetime));
			nextPlannedStartDatetime = data.getPlannedEndDatetime();
		}
	}
	
	/**
	 * Originally made to recalculate on screen with onSubmitForServerSideProcess.
	 * This function is used to recalculate and set planned Start/End datetime based on the first detail planned start datetime 
	 * and duration for each detail. Change in the first detail planned start datetime and duration of each detail has a cascading 
	 * effect on details, phase and task planned start/end datetime after the node being modified.
	 * 
	 * @param commandBean
	 * @param targetCommandBeanTreeNode
	 * @param operation
	 * @param isActionSave
	 * @throws Exception
	 */
	private void calculatePlanPhaseDetailStartAndEndDateTime(CommandBean commandBean, CommandBeanTreeNode targetCommandBeanTreeNode, Operation operation, Boolean isActionSave, UserSession session) throws Exception {
		
		CustomFieldUom thisUomConverter = new CustomFieldUom(commandBean);	
		Object plannedStartDatetime = null;
		List<CommandBeanTreeNode> listPhaseNodes = null;

		double cumPhaseP50Duration = 0.0;
		double cumPhaseP10Duration = 0.0;
		double cumPhaseP90Duration = 0.0;
		double cumPhaseTlDuration = 0.0;
		
		listPhaseNodes = new ArrayList<CommandBeanTreeNode>(targetCommandBeanTreeNode.getChild(OperationPlanPhase.class.getSimpleName()).values());
		Collections.sort(listPhaseNodes, new DvdPlanSequenceComparator());
		String exeSql = null;
		for(CommandBeanTreeNode nodePhase : listPhaseNodes) {
			double cumTaskP50Duration = 0.0;
			double cumTaskP10Duration = 0.0;
			double cumTaskP90Duration = 0.0;
			double cumTaskTlDuration = 0.0;
			
			if(PropertyUtils.getProperty(nodePhase.getData(), "operationPlanPhaseUid") != null) {
				plannedStartDatetime = this.processPlannedTaskNodes(new ArrayList<CommandBeanTreeNode>(nodePhase.getChild("OperationPlanTask").values()), thisUomConverter, operation, plannedStartDatetime, session);
				
				String planPhaseUid = (String) PropertyUtils.getProperty(nodePhase.getData(), "operationPlanPhaseUid");
				exeSql = "UPDATE OperationPlanPhase SET "
						+ "plannedStartDatetime = (SELECT MIN(plannedStartDatetime) FROM OperationPlanTask WHERE operationPlanPhaseUid = :operationPlanPhaseUid AND (isDeleted IS NULL OR isDeleted=FALSE)), "
						+ "plannedEndDatetime = (SELECT MAX(plannedEndDatetime) FROM OperationPlanTask WHERE operationPlanPhaseUid = :operationPlanPhaseUid AND (isDeleted IS NULL OR isDeleted=FALSE)) ";
				
				if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_DVD_PLAN_PHASE_DURATION))) {
					exeSql += ", p50Duration = (SELECT COALESCE(SUM(p50Duration),0.0) FROM OperationPlanTask WHERE operationPlanPhaseUid = :operationPlanPhaseUid AND (isDeleted IS NULL OR isDeleted=FALSE)) ";
					exeSql += ", tlDuration = (SELECT COALESCE(SUM(tlDuration),0.0) FROM OperationPlanTask WHERE operationPlanPhaseUid = :operationPlanPhaseUid AND (isDeleted IS NULL OR isDeleted=FALSE)) ";
				}
				
				if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_PLAN_PHASE_DEPTH))) {
					exeSql += ", depthMdMsl = (SELECT COALESCE(MAX(depthMdMsl),0.0) FROM OperationPlanTask WHERE operationPlanPhaseUid = :operationPlanPhaseUid AND (isDeleted IS NULL OR isDeleted=FALSE)) ";
				}
				
				exeSql += "WHERE operationPlanPhaseUid = :operationPlanPhaseUid AND (isDeleted IS NULL OR isDeleted=FALSE)";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(exeSql, new String[] {"operationPlanPhaseUid"}, new Object[] {planPhaseUid});
			}
			this.updatePlanPhaseActualDatetime(nodePhase, operation.getOperationUid());
			
			//TL duration
			cumPhaseTlDuration += cumTaskTlDuration;
			nodePhase.getDynaAttr().put("cumTlDuration", cumPhaseTlDuration);
			PropertyUtils.setProperty(nodePhase.getData(), "tlDuration", cumTaskTlDuration);
			
			//p50Duration
			cumPhaseP50Duration += cumTaskP50Duration;
			nodePhase.getDynaAttr().put("cumP50Duration", cumPhaseP50Duration);
			PropertyUtils.setProperty(nodePhase.getData(), "p50Duration", cumTaskP50Duration);
			
			//p10Duration
			cumPhaseP10Duration += cumTaskP10Duration;
			nodePhase.getDynaAttr().put("cumP10Duration", cumPhaseP10Duration);
			PropertyUtils.setProperty(nodePhase.getData(), "p50Duration", cumTaskP10Duration);
			
			//p90Duration
			cumPhaseP90Duration += cumTaskP90Duration;
			nodePhase.getDynaAttr().put("cumP90Duration", cumPhaseP90Duration);
			PropertyUtils.setProperty(nodePhase.getData(), "p50Duration", cumTaskP90Duration);
			
		}
	}
	
	private Object processPlannedTaskNodes(List<CommandBeanTreeNode> listTaskNodes, CustomFieldUom thisUomConverter, Operation operation, Object prevPlannedStartDatetime, UserSession session) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, Exception {
		double cumDetailTlDuration = 0.0;
		double cumTaskP50Duration = 0.0;
		double cumTaskP10Duration = 0.0;
		double cumTaskP90Duration = 0.0;
		double cumTaskTlDuration = 0.0;
		
		Object plannedStartDatetime = prevPlannedStartDatetime;
		String exeSql = null;
		Collections.sort(listTaskNodes, new DvdPlanSequenceComparator());
		for(CommandBeanTreeNode nodeTask : listTaskNodes) {
			double cumDetailP50Duration = 0.0;
			double cumDetailP10Duration = 0.0;
			double cumDetailP90Duration = 0.0;
			
			if(PropertyUtils.getProperty(nodeTask.getData(), "operationPlanTaskUid") != null) {
				plannedStartDatetime = this.processPlannedDetailNodes(new ArrayList<CommandBeanTreeNode>(nodeTask.getChild("OperationPlanDetail").values()), thisUomConverter, operation, plannedStartDatetime);

				String planTaskUid = (String) PropertyUtils.getProperty(nodeTask.getData(), "operationPlanTaskUid");
				if(nodeTask.getChild("OperationPlanDetail").size() == 0 || this.hasAllChildrenMarkedAsDeleted(nodeTask.getChild("OperationPlanDetail"))) {
					exeSql = "UPDATE OperationPlanTask SET plannedStartDatetime = null, plannedEndDatetime = null WHERE operationPlanTaskUid = :operationPlanTaskUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(exeSql, new String[] {"operationPlanTaskUid"}, new Object[] {planTaskUid});
				} else {
					exeSql = "UPDATE OperationPlanTask SET "
							+ "plannedStartDatetime = (SELECT MIN(plannedStartDatetime) FROM OperationPlanDetail WHERE operationPlanTaskUid = :operationPlanTaskUid AND (isDeleted IS NULL OR isDeleted=FALSE)), "
							+ "plannedEndDatetime = (SELECT MAX(plannedEndDatetime) FROM OperationPlanDetail WHERE operationPlanTaskUid = :operationPlanTaskUid AND (isDeleted IS NULL OR isDeleted=FALSE)) ";
					
					if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_PLAN_TASK_DURATION))) {
						exeSql += ", p50Duration = (SELECT COALESCE(SUM(p50Duration),0.0) FROM OperationPlanDetail WHERE operationPlanTaskUid = :operationPlanTaskUid AND (isDeleted IS NULL OR isDeleted=FALSE)) ";
						exeSql += ", tlDuration = (SELECT COALESCE(SUM(tlDuration),0.0) FROM OperationPlanDetail WHERE operationPlanTaskUid = :operationPlanTaskUid AND (isDeleted IS NULL OR isDeleted=FALSE)) ";
					}
					
					if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_POPULATE_PLAN_TASK_DEPTH))) {
						exeSql += ", depthMdMsl = (SELECT COALESCE(MAX(depthMdMsl),0.0) FROM OperationPlanDetail WHERE operationPlanTaskUid = :operationPlanTaskUid AND (isDeleted IS NULL OR isDeleted=FALSE)) ";
					}
							
					exeSql += "WHERE operationPlanTaskUid = :operationPlanTaskUid AND (isDeleted IS NULL OR isDeleted=FALSE)";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(exeSql, new String[] {"operationPlanTaskUid"}, new Object[] {planTaskUid});
				}
			}
			this.updatePlanTaskActualDatetime(nodeTask, operation.getOperationUid());
			
			//TL duration
			cumTaskTlDuration += cumDetailTlDuration;
			nodeTask.getDynaAttr().put("cumTlDuration", cumTaskTlDuration);
			PropertyUtils.setProperty(nodeTask.getData(), "tlDuration", cumDetailTlDuration);
			
			//p50Duration
			cumTaskP50Duration += cumDetailP50Duration;
			nodeTask.getDynaAttr().put("cumP50Duration", cumTaskP50Duration);
			PropertyUtils.setProperty(nodeTask.getData(), "p50Duration", cumDetailP50Duration);
			
			//p10Duration
			cumTaskP10Duration += cumDetailP10Duration;
			nodeTask.getDynaAttr().put("cumP10Duration", cumTaskP10Duration);
			PropertyUtils.setProperty(nodeTask.getData(), "p10Duration", cumDetailP10Duration);
			
			//p90Duration
			cumTaskP50Duration += cumDetailP90Duration;
			nodeTask.getDynaAttr().put("cumP90Duration", cumTaskP90Duration);
			PropertyUtils.setProperty(nodeTask.getData(), "p90Duration", cumDetailP90Duration);
		}
		return plannedStartDatetime; //return for next task's detail planned start datetime 
	}
	
	/**
	 * for each PlanDetail, 
	 * 		if(isBatchDrilling) update planned start/end datetime under current task
	 * 		else update planned start/end datetime under current phase
	 * 
	 * @param listDetailNodes
	 * @throws Exception 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	private Object processPlannedDetailNodes(List<CommandBeanTreeNode> listDetailNodes, CustomFieldUom thisUomConverter, Operation operation, Object prevPlannedStartDatetime) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, Exception {
		double cumDetailTlDuration = 0.0;
		long calculatedPlannedEndDatetime = 0L;
		Double tlDuration = 0.0;
		Double p50Duration = 0.0;
		Object plannedStartDatetime = prevPlannedStartDatetime;
		String exeSql = null;
		Collections.sort(listDetailNodes, new DvdPlanSequenceComparator());
		for(CommandBeanTreeNode nodeDetail : listDetailNodes) {
			if(PropertyUtils.getProperty(nodeDetail.getData(), "operationPlanDetailUid") != null
					&& (PropertyUtils.getProperty(nodeDetail.getData(), "isDeleted") == null || !((Boolean)(PropertyUtils.getProperty(nodeDetail.getData(), "isDeleted"))))) {

				//TL duration
				tlDuration = PropertyUtils.getProperty(nodeDetail.getData(), "tlDuration") != null ? (Double) PropertyUtils.getProperty(nodeDetail.getData(), "tlDuration"): 0.0;
				cumDetailTlDuration += (tlDuration != null ? tlDuration : 0.0);
				nodeDetail.getDynaAttr().put("cumTlDuration", cumDetailTlDuration);
													
				if(plannedStartDatetime == null) { //if is the first detail
					plannedStartDatetime = PropertyUtils.getProperty(nodeDetail.getData(), "plannedEndDatetime"); //for next planned detail record
				} else {
					//update cum duration
					p50Duration = (Double) PropertyUtils.getProperty(nodeDetail.getData(), "p50Duration");
					thisUomConverter.setReferenceMappingField(OperationPlanDetail.class, "p50_duration");
					thisUomConverter.setBaseValueFromUserValue((p50Duration == null ? 0.00 : p50Duration));
					calculatedPlannedEndDatetime = ((Date) plannedStartDatetime).getTime() + (new Double(thisUomConverter.getBasevalue()).longValue() * 1000);
					
					exeSql = "UPDATE OperationPlanDetail SET plannedStartDatetime = :plannedStartDatetime, plannedEndDatetime = :plannedEndDatetime WHERE operationPlanDetailUid = :operationPlanDetailUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(exeSql, new String[] {"plannedStartDatetime", "plannedEndDatetime", "operationPlanDetailUid"}, new Object[] {plannedStartDatetime, new Date(calculatedPlannedEndDatetime), (String) PropertyUtils.getProperty(nodeDetail.getData(), "operationPlanDetailUid")});
					plannedStartDatetime = new Date(calculatedPlannedEndDatetime); //new start date time for next record
				}
				
				this.updatePlanDetailActualDatetime(nodeDetail, operation.getOperationUid());
			}
		}
		return (operation.getIsBatchDrill() != null && operation.getIsBatchDrill()) ? null : plannedStartDatetime;
	}
	
	private Boolean hasAllChildrenMarkedAsDeleted(Map<String, CommandBeanTreeNode> targetNodes) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, Exception {
		Boolean isAllDeleted = true;
		List<CommandBeanTreeNode> list = new ArrayList<CommandBeanTreeNode>(targetNodes.values());
		for(CommandBeanTreeNode node : list) {
			if(PropertyUtils.getProperty(node.getData(), "isDeleted") == null || !((Boolean)(PropertyUtils.getProperty(node.getData(), "isDeleted")))) {
				isAllDeleted = false;
			}
		}
		return isAllDeleted;
	}
	
	/**
	 * returns true if the node is the first child of the parent on top of the chain
	 * @param node
	 * @return
	 * @throws Exception
	 */
	private Boolean isVeryFirstInCollection (CommandBeanTreeNode node) throws Exception {
		return isFirstInCollection(node) && isFirst(node.getParent());
	}
	
	private Boolean isFirst(CommandBeanTreeNode node) throws Exception {	
		if(node.getData().getClass().getSimpleName().equals("OperationPlanPhase")) 
			return isFirstInCollection(node);
		return isFirstInCollection(node) && isFirst(node.getParent());
	}
	
	/**
	 * returns true if the node is the 1st child of the parent
	 * @param node
	 * @return
	 * @throws Exception
	 */
	private Boolean isFirstInCollection (CommandBeanTreeNode node) throws Exception {
		Map<String, CommandBeanTreeNode> children = node.getParent().getChild(node.getData().getClass().getSimpleName());
		Map.Entry<String, CommandBeanTreeNode> entry = children.entrySet().iterator().next();
		return entry.getValue().equals(node);
	}
	
	private void updatePlanDetailActualDatetime (CommandBeanTreeNode node, String operationUid) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, Exception {
		String query = "SELECT MIN(startDatetime), MAX(endDatetime) FROM Activity WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND dayPlus=0 "
				+ "AND dailyUid IN (SELECT dailyUid FROM Daily WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND operationUid=:operationUid) AND planDetailReference=:operationPlanDetailUid";
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, new String[] {"operationUid","operationPlanDetailUid"}, new Object[] {operationUid, PropertyUtils.getProperty(node.getData(), "operationPlanDetailUid")});
		if(result.size() > 0 && (result.get(0)[0] != null && result.get(0)[1] != null)) {
			node.getDynaAttr().put("detailActualStartDatetime",  (Date) result.get(0)[0]);
			
			//convert 23:59:59 to 00:00 next day date
			Date endDatetime = (Date)result.get(0)[1];
			if((new SimpleDateFormat("HH:mm:ss")).format(endDatetime).equals("23:59:59")) {
				endDatetime = new Date(endDatetime.getTime() + 1000);
			}
			
			node.getDynaAttr().put("detailActualEndDatetime", endDatetime);
		}
	}
	
	private void updatePlanTaskActualDatetime(CommandBeanTreeNode node, String operationUid) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, Exception {
		String query = "SELECT MIN(startDatetime), MAX(endDatetime) FROM Activity WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND dayPlus=0 "
				+ "AND dailyUid IN (SELECT dailyUid FROM Daily WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND operationUid=:operationUid) AND planTaskReference=:operationPlanTaskUid";
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, new String[] {"operationUid","operationPlanTaskUid"}, new Object[] {operationUid, PropertyUtils.getProperty(node.getData(), "operationPlanTaskUid")});
		if(result.size() > 0 && (result.get(0)[0] != null && result.get(0)[1] != null)) {
			node.getDynaAttr().put("taskActualStartDatetime", result.get(0)[0]);
			
			//convert 23:59:59 to 00:00 next day date
			Date endDatetime = (Date)result.get(0)[1];
			if((new SimpleDateFormat("HH:mm:ss")).format(endDatetime).equals("23:59:59")) {
				endDatetime = new Date(endDatetime.getTime() + 1000);
			}
			
			node.getDynaAttr().put("taskActualEndDatetime", endDatetime);
		}
	}

	private void updatePlanPhaseActualDatetime(CommandBeanTreeNode node, String operationUid) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, Exception {
		String query = "SELECT MIN(startDatetime), MAX(endDatetime) FROM Activity WHERE (isDeleted IS NULL OR isDeleted = FALSE) AND dayPlus=0  "
				+ "AND dailyUid IN (SELECT dailyUid FROM Daily WHERE (isDeleted IS NULL OR isDeleted = FALSE) and operationUid=:operationUid) "
				+ "AND planReference=:operationPlanPhaseUid";
		List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, new String[] {"operationUid","operationPlanPhaseUid"}, new Object[] {operationUid, PropertyUtils.getProperty(node.getData(), "operationPlanPhaseUid")});
		if(result.size() > 0 && (result.get(0)[0] != null && result.get(0)[1] != null)) {
			node.getDynaAttr().put("phaseActualStartDatetime", result.get(0)[0]);
			
			//convert 23:59:59 to 00:00 next day date
			Date endDatetime = (Date)result.get(0)[1];
			if((new SimpleDateFormat("HH:mm:ss")).format(endDatetime).equals("23:59:59")) {
				endDatetime = new Date(endDatetime.getTime() + 1000);
			}
			
			node.getDynaAttr().put("phaseActualEndDatetime", endDatetime);
		}
	}
	
	private void savePhaseActualCost(String planPhaseUid, CommandBeanTreeNode node) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"thisPlanPhaseUid"};
		Object[] paramsValues = new Object[1]; 
		paramsValues[0] = planPhaseUid;
					
		String strSql = "SELECT sum(taskCost) as totalActualCost, operationPlanPhaseUid from OperationPlanTask WHERE operationPlanPhaseUid =:thisPlanPhaseUid AND (isDeleted = false or isDeleted is null)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double totalActualCost = 0.0;
			
		if (lstResult.size() > 0){
			Object[] thisResult = (Object[]) lstResult.get(0);
				
			if (thisResult[0] != null){
				totalActualCost = Double.parseDouble(thisResult[0].toString()); 
			}
		}
			
		String strSql1 = "UPDATE OperationPlanPhase SET phaseCost =:phaseCost WHERE operationPlanPhaseUid =:thisPhaseUid";
		String[] paramsFields1 = {"phaseCost", "thisPhaseUid"};
		Object[] paramsValues1 = {totalActualCost, planPhaseUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);

		return;
	}
	
	private void saveTaskActualCost(String operationPlanTaskUid, CommandBeanTreeNode node) throws Exception  {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"operationPlanTaskUid"};
		Object[] paramsValues = new Object[1]; 
		paramsValues[0] = operationPlanTaskUid;
					
		String strSql = "SELECT sum(detailCost) as totalActualCost, operationPlanTaskUid from OperationPlanDetail WHERE operationPlanTaskUid =:operationPlanTaskUid AND (isDeleted = false or isDeleted is null)";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
		
		Double totalActualCost = 0.0;
			
		if (lstResult.size() > 0){
			Object[] thisResult = (Object[]) lstResult.get(0);
				
			if (thisResult[0] != null){
				totalActualCost = Double.parseDouble(thisResult[0].toString()); 
			}
		}
			
		String strSql1 = "UPDATE OperationPlanTask SET taskCost =:taskCost WHERE operationPlanTaskUid =:operationPlanTaskUid";
		String[] paramsFields1 = {"taskCost", "operationPlanTaskUid"};
		Object[] paramsValues1 = {totalActualCost, operationPlanTaskUid};
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields1, paramsValues1, qp);

		return;
	}
}
