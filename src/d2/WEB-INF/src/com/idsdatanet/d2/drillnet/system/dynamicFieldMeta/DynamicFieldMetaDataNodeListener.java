package com.idsdatanet.d2.drillnet.system.dynamicFieldMeta;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.DynamicFieldMeta;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DynamicFieldMetaDataNodeListener extends EmptyDataNodeListener {
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof DynamicFieldMeta) {	
			DynamicFieldMeta thisMeta = (DynamicFieldMeta) object;
			thisMeta.setFieldUid(thisMeta.getTableName() + "." + thisMeta.getFieldName());
		}
	}
}
