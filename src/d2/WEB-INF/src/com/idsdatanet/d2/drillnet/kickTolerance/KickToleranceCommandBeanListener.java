package com.idsdatanet.d2.drillnet.kickTolerance;

import java.lang.reflect.Field;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.KickTolerance;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class KickToleranceCommandBeanListener extends EmptyCommandBeanListener  {

	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		CustomFieldUom converter = new CustomFieldUom(commandBean);
		
		// set units for standard fields in CasingSection.class		
		for (Field field : ReportDaily.class.getDeclaredFields()) {
			converter.setReferenceMappingField(ReportDaily.class, field.getName());
			if (converter.isUOMMappingAvailable()){
				commandBean.setCustomUOM(KickTolerance.class, "@ddr_" + field.getName(), converter.getUOMMapping());
			}
		}
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{

		//ALLOW TO ENTER ONE RECORD PER DAY PER OPERATION AND HIDE BUTTON
		if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "allowToEnterOneKickTolerence")))
		{
			String[] paramsFields = {"thisOperationUid","thisDailyUid"};
			Object[] paramsValues = {userSelection.getOperationUid(),userSelection.getDailyUid()};
			
			String strSql = "FROM KickTolerance WHERE operationUid = :thisOperationUid AND dailyUid = :thisDailyUid AND (isDeleted IS NULL OR isDeleted = '')";
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);		
			if(list.size() == 0 || list == null)
			{
				commandBean.setSupportedAction("add", true);
				commandBean.setSupportedAction("deleteSelected", true);
				commandBean.setSupportedAction("copySelected", true);
				commandBean.setSupportedAction("selectAll", true);
			}
			else
			{			
				commandBean.setSupportedAction("add", false);
				commandBean.setSupportedAction("deleteSelected", false);
				commandBean.setSupportedAction("copySelected", false);
				commandBean.setSupportedAction("selectAll", false);
			}
		}
	}
}
