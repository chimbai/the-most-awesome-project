package com.idsdatanet.d2.drillnet.activity;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.NextDayActivity;
import com.idsdatanet.d2.core.web.mvc.ActionHandler;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.AclManager;

/**
 * A custom Copy Selected action handler that copies the selected node(s) to two
 * clipboards, to enable copy and paste between Activity and NextDayActivity
 * 
 * @author zjong
 * 
 */
public class ActivityActionManager implements com.idsdatanet.d2.core.web.mvc.ActionManager {
	
	private ActivityCopySelectedHandler activityCopySelectedHandler = new ActivityCopySelectedHandler();
	private NextDayActivityCopySelectedHandler nextDayActivityCopySelectedHandler = new NextDayActivityCopySelectedHandler();
	
	private class ActivityCopySelectedHandler implements com.idsdatanet.d2.core.web.mvc.ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			doCopySelected(commandBean, request, node, key);
			commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(true);
			return new ActionHandlerResponse(true, false, null, BaseCommandBean.OPERATION_PERFORMED_UNKNOWN);
		}
		
		private void doCopySelected(BaseCommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			CommandBeanTreeNodeImpl activityClipboard = new CommandBeanTreeNodeImpl(commandBean);
			CommandBeanTreeNodeImpl nextDayActivityClipboard = new CommandBeanTreeNodeImpl(commandBean);
			
			for (Iterator j = node.getList().get("Activity").entrySet().iterator(); j.hasNext();) {
				Map.Entry entry = (Map.Entry) j.next();
				if (((CommandBeanTreeNodeImpl) entry.getValue()).getAtts().getSelected()) {
					CommandBeanTreeNodeImpl activityTargetChild = activityClipboard.addChild(getTreeModelDataDefinitionMeta(activityClipboard.getChildDataDefinition(), "Activity"), (String) entry.getKey(), ((CommandBeanTreeNodeImpl) entry.getValue()).getData());
					activityTargetChild.copyDynamicFieldFrom((CommandBeanTreeNodeImpl) entry.getValue());
					activityTargetChild.copyDynamicAttrFrom((CommandBeanTreeNodeImpl) entry.getValue());
					
					NextDayActivity nextDayActivity = new NextDayActivity();
					PropertyUtils.copyProperties(nextDayActivity, ((CommandBeanTreeNodeImpl) entry.getValue()).getData());
					if(getTreeModelDataDefinitionMeta(nextDayActivityClipboard.getChildDataDefinition(), "NextDayActivity") != null)
					{
						CommandBeanTreeNodeImpl nextDayActivityTargetChild = nextDayActivityClipboard.addChild(getTreeModelDataDefinitionMeta(nextDayActivityClipboard.getChildDataDefinition(), "NextDayActivity"), (String) entry.getKey(), nextDayActivity);
						nextDayActivityTargetChild.copyDynamicFieldFrom((CommandBeanTreeNodeImpl) entry.getValue());
						nextDayActivityTargetChild.copyDynamicAttrFrom((CommandBeanTreeNodeImpl) entry.getValue());
					}
				}
			}
			
			UserSession.getInstance(request).setClipboard(commandBean.getControllerBeanName(), "Activity", activityClipboard);
			UserSession.getInstance(request).setClipboard(commandBean.getControllerBeanName(), "NextDayActivity", nextDayActivityClipboard);
		}
		
		private TreeModelDataDefinitionMeta getTreeModelDataDefinitionMeta(Iterator childDataDefinition, String className) {
			for (Iterator i = childDataDefinition; i.hasNext(); ) {
				TreeModelDataDefinitionMeta current = (TreeModelDataDefinitionMeta) i.next();
				if (current.getTableClass().getSimpleName().equals(className)) {
					return current;
				}
				TreeModelDataDefinitionMeta child = getTreeModelDataDefinitionMeta(current.getChildDataDefinition().iterator(), className);
				if (child != null) {
					return child;
				}
			}
			return null;
		}
	}
	
	private class NextDayActivityCopySelectedHandler implements com.idsdatanet.d2.core.web.mvc.ActionHandler {
		public ActionHandlerResponse process(BaseCommandBean commandBean, UserSession userSession, AclManager aclManager, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			doCopySelected(commandBean, request, node, key);
			commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(true);
			return new ActionHandlerResponse(true, false, null, BaseCommandBean.OPERATION_PERFORMED_UNKNOWN);
		}
		
		private void doCopySelected(BaseCommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode node, String key) throws Exception {
			CommandBeanTreeNodeImpl activityClipboard = new CommandBeanTreeNodeImpl(commandBean);
			CommandBeanTreeNodeImpl nextDayActivityClipboard = new CommandBeanTreeNodeImpl(commandBean);
			
			for (Iterator j = node.getList().get("NextDayActivity").entrySet().iterator(); j.hasNext();) {
				Map.Entry entry = (Map.Entry) j.next();
				if (((CommandBeanTreeNodeImpl) entry.getValue()).getAtts().getSelected()) {
					CommandBeanTreeNodeImpl nextDayActivityTargetChild = nextDayActivityClipboard.addChild(getTreeModelDataDefinitionMeta(nextDayActivityClipboard.getChildDataDefinition(), "NextDayActivity"), (String) entry.getKey(), ((CommandBeanTreeNodeImpl) entry.getValue()).getData());
					nextDayActivityTargetChild.copyDynamicFieldFrom((CommandBeanTreeNodeImpl) entry.getValue());
					nextDayActivityTargetChild.copyDynamicAttrFrom((CommandBeanTreeNodeImpl) entry.getValue());
					
					Activity activity = new Activity();
					PropertyUtils.copyProperties(activity, ((CommandBeanTreeNodeImpl) entry.getValue()).getData());
					CommandBeanTreeNodeImpl activityTargetChild = activityClipboard.addChild(getTreeModelDataDefinitionMeta(activityClipboard.getChildDataDefinition(), "Activity"), (String) entry.getKey(), activity);
					activityTargetChild.copyDynamicFieldFrom((CommandBeanTreeNodeImpl) entry.getValue());
					activityTargetChild.copyDynamicAttrFrom((CommandBeanTreeNodeImpl) entry.getValue());
				}
			}
			
			UserSession.getInstance(request).setClipboard(commandBean.getControllerBeanName(), "Activity", activityClipboard);
			UserSession.getInstance(request).setClipboard(commandBean.getControllerBeanName(), "NextDayActivity", nextDayActivityClipboard);
		}
		
		private TreeModelDataDefinitionMeta getTreeModelDataDefinitionMeta(Iterator childDataDefinition, String className) {
			for (Iterator i = childDataDefinition; i.hasNext(); ) {
				TreeModelDataDefinitionMeta current = (TreeModelDataDefinitionMeta) i.next();
				if (current.getTableClass().getSimpleName().equals(className)) {
					return current;
				}
				TreeModelDataDefinitionMeta child = getTreeModelDataDefinitionMeta(current.getChildDataDefinition().iterator(), className);
				if (child != null) {
					return child;
				}
			}
			return null;
		}
	}
	
	public ActionHandler getActionHandler(String action, CommandBeanTreeNode node) throws Exception {
		if (com.idsdatanet.d2.core.web.mvc.Action.COPY_SELECTED.equals(action)) {
			if ("Activity".equals(node.getAtts().getCopySelectedClass())) {
				return this.activityCopySelectedHandler;
			} else if ("NextDayActivity".equals(node.getAtts().getCopySelectedClass())) {
				return this.nextDayActivityCopySelectedHandler;
			}
		}
		return null;
	}

}
