package com.idsdatanet.d2.drillnet.cementJob;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupProvider;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * Custom lookuphandler for cementjob holeSize. Populated all casing's holeSize within operation.
 * @author fu
 *
 */
public class CementJobHoleSizeLookupHandler implements LookupHandler {
	private URI xmlLookup = null;
	private LookupProvider lookupProvider = null;

	public void setXmlLookup(URI xmlLookup) {
		this.xmlLookup = xmlLookup;
	}
	
	public URI getXmlLookup() {
		return this.xmlLookup;
	}

	public LookupProvider getLookupProvider() {
		return lookupProvider;
	}

	public void setLookupProvider(LookupProvider lookupProvider) {
		this.lookupProvider = lookupProvider;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		Map<String, LookupItem> holeSizeLookup = this.getLookupProvider().getLookup(this.getXmlLookup(), userSelection);
		
		List<CasingSection> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from CasingSection where (isDeleted = false or isDeleted is null) and operationUid = :operationUid and openHoleId > 0 order by openHoleId", "operationUid", userSelection.getOperationUid());
		if((list != null && list.size() > 0) && (holeSizeLookup != null)) {
			for(CasingSection casingSection : list) {
				CustomFieldUom thisConverter = new CustomFieldUom((Locale) null, CasingSection.class, "openHoleId");
				String strHoleSize 			 = thisConverter.formatOutputPrecision(casingSection.getOpenHoleId());
				
				if(StringUtils.isNotBlank(strHoleSize) && holeSizeLookup.containsKey(strHoleSize)) {
					LookupItem lookupItem = holeSizeLookup.get(strHoleSize);
					if(lookupItem != null)
					{
						result.put(lookupItem.getKey(), new LookupItem(lookupItem.getKey(), lookupItem.getValue()));
					}
				}
			}
		}
		
		return result;
	}
}