package com.idsdatanet.d2.drillnet.rushmore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class RushmoreTimeDepthDataNodeListener extends EmptyDataNodeListener implements DataNodeLoadHandler {

	Map<String, ReportDaily> _reportDailyList = new HashMap<String, ReportDaily>();
	
	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		if (node.getData() instanceof Activity) {
			Activity activity = (Activity) node.getData();
			if (activity.getDailyUid()!=null) {
				if (_reportDailyList.containsKey(activity.getDailyUid())) {
					ReportDaily reportDaily = _reportDailyList.get(activity.getDailyUid());
					node.getDynaAttr().put("reportNumber", reportDaily.getReportNumber());
				} else {
					String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(userSelection.getOperationUid());
					String queryString = "FROM ReportDaily WHERE (isDeleted=false or isDeleted is null) " +
							"AND reportType=:reportType " +
							"AND dailyUid=:dailyUid ";
					List<ReportDaily> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"reportType","dailyUid"}, new Object[]{reportType, activity.getDailyUid()});
					if (list.size()>0) {
						ReportDaily reportDaily = list.get(0);
						_reportDailyList.put(activity.getDailyUid(), reportDaily);
						node.getDynaAttr().put("reportNumber", reportDaily.getReportNumber());
					}
				}
			}
		}
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		
		List<Object> result = new ArrayList();
		
		if (meta.getTableClass().equals(Activity.class)) {
			//set unit and operation name 
			commandBean.getRoot().getDynaAttr().put("wellName", CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), userSelection.getOperationUid(), null, true));
			CustomFieldUom thisConverter = new CustomFieldUom(userSelection.getLocale(), Activity.class, "depthMdMsl");
			if (thisConverter.isUOMMappingAvailable()) {
				commandBean.getRoot().getDynaAttr().put("unit", thisConverter.getUomLabel());
			}
			
			if (userSelection!=null) {
				String queryString = "SELECT a.dailyUid, d.dayDate, a.depthMdMsl, a.holeSize " +
						"FROM Daily d, Activity a " +
						"WHERE (a.isDeleted=false or a.isDeleted is null) " +
						"AND (d.isDeleted=false or d.isDeleted is null) " +
						"AND a.dailyUid=d.dailyUid " +
						"and d.operationUid=:operationUid " +
						"GROUP BY a.dailyUid, d.dayDate, a.depthMdMsl, a.holeSize " +
						"ORDER BY d.dayDate, a.depthMdMsl, a.holeSize DESC ";
				List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[]{"operationUid"}, new Object[]{userSelection.getOperationUid()});
				for (Object[] rec : list) {
					Activity activity = new Activity();
					activity.setActivityUid((String) rec[0] + (Double) rec[2] + (Double) rec[3]); //set dummy PK
					activity.setDailyUid((String) rec[0]);
					activity.setDepthMdMsl((Double) rec[2]);
					activity.setHoleSize((Double) rec[3]);
					activity.setOperationUid(userSelection.getOperationUid());
					result.add(activity);
				}
			}
		}
		return result;
	}

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}


}
