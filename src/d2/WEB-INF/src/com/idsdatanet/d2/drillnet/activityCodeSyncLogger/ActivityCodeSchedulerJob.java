package com.idsdatanet.d2.drillnet.activityCodeSyncLogger;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;

import com.idsdatanet.d2.core.model.DepotSyncJob;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.depot.core.DepotConstants;
import com.idsdatanet.depot.core.query.QueryResult;
import com.idsdatanet.depot.core.util.DepotHttpClient;

public class ActivityCodeSchedulerJob {
	
	private MailEngine mailEngine = null;
	
	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}
	
	/**
	 * Method triggered by scheduler activitySyncJob
	 * @author mstan
	 *
	 */
	synchronized public void run() throws Exception
	{
		
		String sql = "FROM DepotSyncJob WHERE (isDeleted = false or isDeleted is null) AND syncStatus = 'INCOMPLETE'";
		List<DepotSyncJob> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		if(!list.isEmpty())
		{
			for(DepotSyncJob dpt : list)
			{
				Date date = new Date();
				String serverUid = dpt.getImportExportServerPropertiesUid();
				String xmlFilePath = dpt.getXmlFilepath();
				String serverPath = null;
				String xmlContent = null;
				Integer retryCount = dpt.getSyncRetryCount();
				String result = "";
				
				//process xml and send to destination define in serverUid
				String sql2 = "SELECT endPoint FROM ImportExportServerProperties WHERE (isDeleted = false or isDeleted is null) " + 
				"AND serverPropertiesUid = :thisServerUid";
				
				List endPoint = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql2, "thisServerUid",serverUid);
				
				if(!endPoint.isEmpty())
				{
					serverPath = endPoint.get(0).toString() + "/webservice/depotwebservice.html";
				
					xmlContent = getXmlContent(xmlFilePath);
					
					
					if(xmlContent != null && StringUtils.isNotBlank(xmlContent))
						result = sendXML(xmlContent,xmlFilePath,serverPath);
					
					//send of xml as string
					if(result != null && StringUtils.isNotBlank(result))
					{
						if(result.toLowerCase().contains("successful"))
							dpt.setSyncStatus("SUCCESSFUL");
						else
						{
							retryCount = retryCount + 1;
							dpt.setSyncRetryCount(retryCount);
						}
					}
					else
					{
						retryCount = retryCount + 1;
						dpt.setSyncRetryCount(retryCount);
					}
				}
				else
				{
					dpt.setSyncStatus("FAILED,NOT FOUND");
					dpt.setSyncFailNotifyDatetime(date);
				}
				
				
				
				//if retry = 3, send xml in email
				if(retryCount >= 3)
				{
					dpt.setSyncStatus("FAILED");
					sendNotificationEmail(xmlFilePath, xmlContent, result, serverPath);
					dpt.setSyncFailNotifyDatetime(date);
				}
				
				dpt.setLastSyncDatetime(date);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(dpt);
				
				String sql3 = "FROM DepotSyncJob WHERE (isDeleted = false or isDeleted is null) AND syncStatus = 'INCOMPLETE' AND xmlFilepath = :thisFilePath";
				List<DepotSyncJob> list2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql3, "thisFilePath", xmlFilePath);
				if(list2.isEmpty())
				{
					//delete the xml file
					String fullPath = ApplicationConfig.getConfiguredInstance().getServerRootPath() + "WEB-INF/depot/xml/" + xmlFilePath;
					File f = new File(fullPath);
					f.delete();
				}
			}
		}
	}
	
	/**
	 * To send XML string as post method through depot
	 * @param String xmlContent (the xml string)
	 * @param String xmlPath (the name of the physical XML file)
	 * @param String serverPath (the destination point otherwise known as end point)
	 * @return return String result (result of the sending).
	 */
	private String sendXML(String xmlContent, String xmlPath, String serverPath) throws Exception{
		String objectId = null;
		String result = null;
		
		if(xmlPath != null && StringUtils.isNotBlank(xmlPath))
		{
			if(xmlPath.toLowerCase().contains("lookupclasscode"))
				objectId = "lookupClassCode";
			else if(xmlPath.toLowerCase().contains("lookupphasecode"))
				objectId = "lookupPhaseCode";
			else if(xmlPath.toLowerCase().contains("lookuptaskcode"))
				objectId = "lookupTaskCode";
			else if(xmlPath.toLowerCase().contains("lookuprootcausecode"))
				objectId = "lookupRootCauseCode";
		}
		
		if (StringUtils.isNotBlank(xmlContent)){	
			
			DepotHttpClient httpClient = new DepotHttpClient(serverPath);
			httpClient.setBasicAuthentication("idsdepot", "enokawaras");
			
			QueryResult queryResult = new QueryResult();
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("type", DepotConstants.D2);
			params.put("version", "1.0");
			params.put("data_object", objectId);
			params.put("query_template", xmlContent);
			httpClient.invokePostWithParams(params, queryResult);
			
			if(queryResult.getOutputString() != null && StringUtils.isNotBlank(queryResult.getOutputString()))
				return queryResult.getOutputString();
			else
				return "Destination unreachable, please check on server endpoint";
		}
		
		return result;
	}

	/**
	 * To obtain XML string from physical XML
	 * @param String xmlFilePath (the path where the physical XML file is stored)
	 * @return return String fullString (the XML string).
	 */
	private String getXmlContent(String xmlFilePath) throws Exception {
		String fullPath = ApplicationConfig.getConfiguredInstance().getServerRootPath() + "WEB-INF/depot/xml/" + xmlFilePath;
		String fullString = null;
		
		FileReader file = new FileReader(fullPath);
		BufferedReader br = new BufferedReader(file);
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	       while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	       fullString = sb.toString();
	        
	    } finally {
	        br.close();
	    }
		
		return fullString;
	}

	/**
	 * To send sending fail notification to support email with XML file attached
	 * @param String xmlFilePath (the path where the physical XML file is stored)
	 * @param String xmlContent (the XML string)
	 * @param String result (the result which will be the error message return from the http)
	 * @param String serverPath (the destination point otherwise known as end point)
	 */
	public void sendNotificationEmail(String xmlFilePath, String xmlContent, String result, String serverPath) throws Exception
	{
		
		Map<String,byte[]> exportOutput = new LinkedHashMap<String,byte[]>();
		Map<String,InputStreamSource> zipFile = new HashMap<String,InputStreamSource>();
		Map<String, Object> params = new HashMap<String, Object>();
		
		String supportAddr = ApplicationConfig.getConfiguredInstance().getSupportEmail();
		String subject = "DEPOT synchronisation failed for " + serverPath;
		
		String[] recipientArray = new String[1];
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		byte[] xmlByte = xmlContent.getBytes();
		exportOutput.put(xmlFilePath, xmlByte);
		
		if (exportOutput.size() > 0) zipFile.put(xmlFilePath, writeZipFile(exportOutput));
		
		params.put("siteName", serverPath);
		params.put("dateTime", dateFormat.format(date).toString());
		params.put("errorMessage", result);
		recipientArray[0] = supportAddr;
		
		Map<String,InputStreamSource> attachment = new HashMap<String,InputStreamSource>();;
		if (zipFile != null) {
			for (Map.Entry<String,InputStreamSource> entry : zipFile.entrySet()) {
				attachment.put(entry.getKey()+".zip", entry.getValue());
			}
		}
		
		try{
			mailEngine.sendMail(recipientArray, supportAddr, supportAddr, subject, mailEngine.generateContentFromTemplate("core/activityCodeDepotSyncFailEmail.ftl", params), attachment);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	private InputStreamSource writeZipFile(Map<String, byte[]> files) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		writeZipFile(files, out);
		InputStreamSource iss = new ByteArrayResource(out.toByteArray());
		return iss;
	}
	
	private void writeZipFile(Map<String, byte[]> files, OutputStream output) throws Exception {
		ZipOutputStream zipOut = new ZipOutputStream(output);
		for (Map.Entry<String, byte[]> entry : files.entrySet()) {
			zipOut.putNextEntry(new ZipEntry(entry.getKey()));
			ByteArrayInputStream in = new ByteArrayInputStream(entry.getValue());

			byte[] buffer = new byte[1024];
			int len;
			while ((len = in.read(buffer)) != -1) {
				zipOut.write(buffer, 0, len);
			}
			in.close();
		}
		zipOut.flush();
		zipOut.close();
	}

}