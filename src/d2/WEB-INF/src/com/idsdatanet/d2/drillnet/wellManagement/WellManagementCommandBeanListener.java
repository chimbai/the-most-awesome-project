package com.idsdatanet.d2.drillnet.wellManagement;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.event.D2ApplicationEvent;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellManagementCommandBeanListener extends EmptyCommandBeanListener {

	@Override
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {
		D2ApplicationEvent.publishTableChangedEvent(Operation.class);
		D2ApplicationEvent.publishTableChangedEvent(Wellbore.class);
		D2ApplicationEvent.publishTableChangedEvent(Well.class);
		ApplicationUtils.getConfiguredInstance().refreshCachedData();
		UserSession session = UserSession.getInstance(request);
		session.setCurrentOperationUid(null);
		session.setCurrentDailyUid(null);
		if (commandBean.getFlexClientControl() != null) {
			commandBean.getFlexClientControl().setReloadParentHtmlPage(true);
		}
	}
	
}
