package com.idsdatanet.d2.report.dot;

import java.util.*;

import com.idsdatanet.d2.core.report.RecursiveDataHandler;

public class DotTemplateReportConfiguration {
	private List<String> beanForReportXml = null;
	//private String outputType=null;
	private String birtReportOutputType = null;
	private Boolean useBirtReportEngine = false;
	private List<RecursiveDataHandler> recursiveDataHandler = null;
	
	public void setRecursiveDataHandler(List<RecursiveDataHandler> value){
		this.recursiveDataHandler = value;
	}
	
	public List<RecursiveDataHandler> getRecursiveDataHandler(){
		return this.recursiveDataHandler;
	}
	
	public void setBeanForReportXml(List<String> value){
		this.beanForReportXml = value;
	}
	
	public List<String> getBeanForReportXml(){
		return this.beanForReportXml;
	}
	
	public String getBirtReportOutputType(){
		return this.birtReportOutputType;
	}
	
	public void setBirtReportOutputType(String value){
		this.birtReportOutputType = value;
	}
	
	public Boolean getUseBirtReportEngine(){
		return this.useBirtReportEngine;
	}
	
	public void setUseBirtReportEngine(Boolean value){
		this.useBirtReportEngine = value;
	}
}
