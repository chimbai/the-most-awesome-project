package com.idsdatanet.d2.report.dot.annualNPT;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.report.dot.DotTemplateReportListener;

public class AnnualNPTDotTemplateReportListener implements DotTemplateReportListener {
	private String last_requested_year = null;
	
	public UserSelectionSnapshot getUserSelectionSnapshotForReportGeneration(HttpServletRequest request, String template, UserSelectionSnapshot defaultUserSelectionSnapshot) throws Exception{
		defaultUserSelectionSnapshot.setCustomProperty(AnnualNPTReportCommandBeanConfig.REPORT_YEAR, this.last_requested_year);
		defaultUserSelectionSnapshot.setCustomProperty(AnnualNPTReportCommandBeanConfig.ACCESSIBLE_OPERATIONS, this.getAccessibleOperations(request));
		
		this.last_requested_year = null;
		return defaultUserSelectionSnapshot;
	}
	
	public void onWebStartRequest(HttpServletRequest request, String template) throws Exception{
		this.last_requested_year = request.getParameter("year");
	}
	
	public String getAccessibleOperations(HttpServletRequest request) throws Exception{
		String accessibleOps = "";
		Integer count = 0;
		
		//get all the user allowed to access operation
		Operations accessibleOperations = UserSession.getInstance(request).getCachedAllAccessibleOperations();

		if (accessibleOperations!=null){
			Iterator i = accessibleOperations.entrySet().iterator();

		    while(i.hasNext()){
		      Map.Entry me = (Map.Entry)i.next();
		      String operationUid = accessibleOperations.get(me.getKey()).getOperationUid();
		      
		      if (operationUid != null && count > 0) accessibleOps += ",";
		      accessibleOps += "'" + operationUid + "'";
		      count++;	

		    }
		
		}
		
		return accessibleOps;
	}
}
