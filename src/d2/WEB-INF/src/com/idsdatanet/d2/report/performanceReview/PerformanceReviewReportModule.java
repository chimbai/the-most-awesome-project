package com.idsdatanet.d2.report.performanceReview;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.model.ReportTypes;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.report.DefaultReportModule;
import com.idsdatanet.d2.drillnet.report.ReportAutoEmail;
import com.idsdatanet.d2.drillnet.report.ReportJobParams;
import com.idsdatanet.d2.drillnet.report.ReportUtils;

public class PerformanceReviewReportModule extends DefaultReportModule{
	
	public static String CUSTOM_PROPERTY_SELECTED_DATE = "CP_SELECTED_DATE";
	public static String CUSTOM_PROPERTY_SUPERVISOR = "CP_SUPERVISOR";
	private Boolean distinctReport = false;
	
	public void setDistinctReport(Boolean value) {
		this.distinctReport = value;
	}
	
	public Boolean getDistinctReport() {
		return this.distinctReport;
	}
	
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		//commandBean.getSystemMessage().clear();
		UserSelectionSnapshot userSelection=new UserSelectionSnapshot(session);
		
		Date selectedDate = null;
		Object obj=commandBean.getRoot().getDynaAttr().get("selectedDate");
		if (obj != null) {
			selectedDate = CommonDateParser.parse(obj.toString());
		}
		if (selectedDate==null) selectedDate = new Date();
		Calendar cDate = Calendar.getInstance();
		cDate.setTime(selectedDate);
		cDate.set(Calendar.HOUR_OF_DAY, 0);
		cDate.set(Calendar.MINUTE, 0);
		cDate.set(Calendar.SECOND, 0);
		cDate.set(Calendar.MILLISECOND, 0);
		cDate.set(Calendar.DAY_OF_MONTH, 1);
		selectedDate = cDate.getTime();
		cDate.add(Calendar.MONTH, -3);
		Date startingDate = cDate.getTime();
		userSelection.setCustomProperty(CUSTOM_PROPERTY_SELECTED_DATE, selectedDate);
		
		obj = commandBean.getRoot().getDynaAttr().get("distinct_report");
		if (obj!=null) {
			this.distinctReport = ("1".equals(obj.toString()));
		}
		userSelection.setCustomProperty("distinct_report", this.distinctReport);
		
		if (this.distinctReport) {
			String queryString = "SELECT rd.supervisor " +
					"FROM ReportDaily rd, Daily d, Well w, Wellbore wb, Operation o " +
					"WHERE (rd.isDeleted=false or rd.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (w.isDeleted=false or w.isDeleted is null) " +
					"AND (wb.isDeleted=false or wb.isDeleted is null) " +
					"AND (o.isDeleted=false or o.isDeleted is null) " +
					"AND w.wellUid=wb.wellUid " +
					"AND wb.wellboreUid=o.wellboreUid " +
					"AND o.operationUid=d.operationUid " +
					"AND d.dailyUid=rd.dailyUid " +
					"AND (rd.supervisor is not null and rd.supervisor!='') " +
					"AND d.dayDate<:endDate " +
					"AND d.dayDate>=:startDate " +
					"GROUP BY rd.supervisor " +
					"ORDER BY rd.supervisor";
			
			List<UserSelectionSnapshot> selections = new ArrayList<UserSelectionSnapshot>();
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"endDate", "startDate"}, new Object[]{selectedDate, startingDate});
			for (Object rec: list) {
				String supervisor = rec.toString();
				
				if (StringUtils.isNotBlank(supervisor)) {
					UserSelectionSnapshot selection = new UserSelectionSnapshot(session);
					selection.setCustomProperty(CUSTOM_PROPERTY_SELECTED_DATE, selectedDate);
					selection.setCustomProperty(CUSTOM_PROPERTY_SUPERVISOR, supervisor);
					selections.add(selection);
				}	
			}
			if (selections.size()>0) {
				return super.submitReportJob(selections, this.getParametersForReportSubmission(session, request, commandBean));
			} else {
				throw new Exception("No data fit the selected criteria therefore unable to generate report.");
			}

		} else {
			return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));
		}
	}
	
	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception{
		SimpleDateFormat df = new SimpleDateFormat("MMM yyyy");
		String supervisor  = (String) userContext.getUserSelection().getCustomProperty(CUSTOM_PROPERTY_SUPERVISOR);
		Date selectedDate = (Date) userContext.getUserSelection().getCustomProperty(CUSTOM_PROPERTY_SELECTED_DATE);
		
		if (StringUtils.isNotBlank(supervisor)) {
			return supervisor + " " + df.format(selectedDate) + (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
		} else {
			return "Perfomance Review Report "+ df.format(selectedDate) + (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
		}
	}	
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		return this.getReportType(userContext.getUserSelection()) + 
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize));
	}

	protected String getReportDisplayNameOnCreation(ReportFiles reportFile, ReportTypes reportType, Operation operation, Daily daily, ReportDaily reportDaily, UserSelectionSnapshot userSelection, ReportJobParams reportJobParams) throws Exception {
		String file_name_pattern = null;
		if(reportType != null){
			file_name_pattern = reportType.getReportFilenamePattern();
		}
		
		if(StringUtils.isBlank(file_name_pattern)){
			return null;
		}

		SimpleDateFormat df = new SimpleDateFormat("MMM yyyy");
		Date selectedDate = (Date) userSelection.getCustomProperty(CUSTOM_PROPERTY_SELECTED_DATE);
		if(this.isCombinedReport(userSelection)){
			file_name_pattern += " - " + df.format(selectedDate);
		}else{
			String supervisor  = (String) userSelection.getCustomProperty(CUSTOM_PROPERTY_SUPERVISOR);
			file_name_pattern = supervisor + " - " + df.format(selectedDate);
		}
		
		return this.formatDisplayName(file_name_pattern, reportFile, operation, daily, reportDaily, userSelection, reportJobParams);
	}
	
	private boolean isCombinedReport(UserSelectionSnapshot userSelection){
		String supervisor  = (String) userSelection.getCustomProperty(CUSTOM_PROPERTY_SUPERVISOR);
		return (StringUtils.isBlank(supervisor));
	}

	public void generateReportOnCurrentSystemDatetime(String loginUser, ReportAutoEmail reportAutoEmail) throws Exception {
		PerformanceReviewReportScheduleParams params = new PerformanceReviewReportScheduleParams();
		params.setDistinctReport(false);
		params.setReportUserUid(loginUser);
		this.generateReportOnCurrentSystemDatetime(params, reportAutoEmail);
	}
	
	public void generateReportOnCurrentSystemDatetime(PerformanceReviewReportScheduleParams scheduleParam, ReportAutoEmail reportAutoEmail) throws Exception {

		UserSelectionSnapshot userSelection = UserSelectionSnapshot.getInstanceForCustomLogin(scheduleParam.getReportUserUid()); 
		this.distinctReport = scheduleParam.isDistinctReport();
		
		Date selectedDate = new Date();
		Calendar cDate = Calendar.getInstance();
		cDate.setTime(selectedDate);
		cDate.set(Calendar.HOUR_OF_DAY, 0);
		cDate.set(Calendar.MINUTE, 0);
		cDate.set(Calendar.SECOND, 0);
		cDate.set(Calendar.MILLISECOND, 0);
		cDate.set(Calendar.DAY_OF_MONTH, 1);
		selectedDate = cDate.getTime();
		cDate.add(Calendar.MONTH, -3);
		Date startingDate = cDate.getTime();
		userSelection.setCustomProperty(CUSTOM_PROPERTY_SELECTED_DATE, selectedDate);
		userSelection.setCustomProperty("distinct_report", this.distinctReport);
		
		ReportJobParams params = new ReportJobParams();
		params.setUserSessionReportJob(false);
		params.setReportAutoEmail(reportAutoEmail);
		params.setPaperSize(super.defaultPaperSize);	
		
		if (this.distinctReport) {
			String queryString = "SELECT rd.supervisor " +
					"FROM ReportDaily rd, Daily d, Well w, Wellbore wb, Operation o " +
					"WHERE (rd.isDeleted=false or rd.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (w.isDeleted=false or w.isDeleted is null) " +
					"AND (wb.isDeleted=false or wb.isDeleted is null) " +
					"AND (o.isDeleted=false or o.isDeleted is null) " +
					"AND w.wellUid=wb.wellUid " +
					"AND wb.wellboreUid=o.wellboreUid " +
					"AND o.operationUid=d.operationUid " +
					"AND d.dailyUid=rd.dailyUid " +
					"AND (rd.supervisor is not null and rd.supervisor!='') " +
					"AND d.dayDate<:endDate " +
					"AND d.dayDate>=:startDate " +
					"GROUP BY rd.supervisor " +
					"ORDER BY rd.supervisor";
			
			List<UserSelectionSnapshot> selections = new ArrayList<UserSelectionSnapshot>();
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"endDate", "startDate"}, new Object[]{selectedDate, startingDate});
			for (Object rec: list) {
				String supervisor = rec.toString();
				
				if (StringUtils.isNotBlank(supervisor)) {
					UserSelectionSnapshot selection = UserSelectionSnapshot.getInstanceForCustomLogin(scheduleParam.getReportUserUid());;
					selection.setCustomProperty(CUSTOM_PROPERTY_SELECTED_DATE, selectedDate);
					selection.setCustomProperty(CUSTOM_PROPERTY_SUPERVISOR, supervisor);
					selections.add(selection);
				}	
			}
			if (selections.size()>0) {
				super.submitReportJob(selections, params);
			} else {
				//throw new Exception("No data fit the selected criteria therefore unable to generate report.");
			}

		} else {
			super.submitReportJob(userSelection, params);
		}

	}
}
