package com.idsdatanet.d2.report.dvd;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OperationPlanMaster;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ParentDVDOperationUserSelectionSnapshot extends UserSelectionSnapshot{
	private boolean parentExists = false;
	
	public ParentDVDOperationUserSelectionSnapshot(UserSelectionSnapshot userSelection) throws Exception{
		List<OperationPlanMaster> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from OperationPlanMaster where operationUid = :operationUid and (isDeleted = false or isDeleted is null)", "operationUid", userSelection.getOperationUid());
		if(rs.size() > 0){
			OperationPlanMaster opm = rs.get(0);
			if(StringUtils.isNotBlank(opm.getDvdParentOperationUid())){
				List<Operation> o_rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from Operation where operationUid = :operationUid and (isDeleted is null or isDeleted = false)","operationUid",opm.getDvdParentOperationUid());
				if(o_rs.size() > 0){
					this.setParentOperation(userSelection, o_rs.get(0));
				}
			}
		}
	}
	
	private void setParentOperation(UserSelectionSnapshot userSelection, Operation operation) throws Exception{
		this.copyFrom(userSelection, true); // to deep copy, because copied value may be modified
		
		this.getPropertySetter().setDailyUid(null);
		this.getPropertySetter().setWellUid(operation.getWellUid());
		this.getPropertySetter().setWellboreUid(operation.getWellboreUid());
		this.getPropertySetter().setOperationUid(operation.getOperationUid());
		this.getPropertySetter().setOperationType(operation.getOperationCode());
		this.getPropertySetter().setCampaignUid(operation.getOperationCampaignUid());

		super.updateRigInformationUid();
		
		this.parentExists = true;
	}
	
	public boolean isDVDParentExists(){
		return this.parentExists;
	}
}
