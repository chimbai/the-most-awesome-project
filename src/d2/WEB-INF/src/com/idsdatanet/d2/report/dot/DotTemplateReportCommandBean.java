package com.idsdatanet.d2.report.dot;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;

import com.idsdatanet.d2.drawing.BhaDrillingParamGraph;
import com.idsdatanet.d2.core.report.BeanDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.SystemReportUtils;
import com.idsdatanet.d2.core.report.XslJob;
import com.idsdatanet.d2.core.report.birt.BirtReportEngine;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.helper.FileDownloadWriter;
import com.idsdatanet.d2.core.web.mvc.AbstractGenericWebServiceCommandBean;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.BaseFormController;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class DotTemplateReportCommandBean extends AbstractGenericWebServiceCommandBean {
	public static final String PARAM_TEMPLATE_FILE = "template";
	public static final String PARAM_LAUNCH_WEBSTART = "webstart";
	public static final String PARAM_FORMAT="format";
	
	private BeanDataGenerator generator = null;
	private String template_dir = null;
	private Map<String, DotTemplateReportConfiguration> template_config = null;
	private DotTemplateReportListener reportListener = null;
	private String controllerBeanUrl = null;
	private Locale reportLocale = null;
	public static final String OUTPUT_TYPE_PDF = "pdf";
	public static final String OUTPUT_TYPE_WORD = "doc";
	public static final String OUTPUT_TYPE_XLS="xls";
	public static final String OUTPUT_TYPE_HTML="html";
	
	public void init(BaseFormController controller){
		super.init(controller);
		this.controllerBeanUrl = controller.getBeanName().toLowerCase();
		if(this.controllerBeanUrl.endsWith("controller")){
			this.controllerBeanUrl = this.controllerBeanUrl.substring(0, this.controllerBeanUrl.length() - "controller".length());
		}
	}
	
	public void setReportLocale(String code) throws Exception {
		this.reportLocale = CommonUtils.getLocale(code);
		if(this.reportLocale == null) throw new Exception("Invalid locale: " + code);
	}
	
	private Locale getEffectiveReportLocale() throws Exception {
		if(this.reportLocale != null) return this.reportLocale;
		return ApplicationConfig.getConfiguredInstance().getDefaultReportLocaleObject();
	}
	
	public void setTemplateDir(String value){
		this.template_dir = value;
	}
	
	public void setDataGenerator(BeanDataGenerator value){
		this.generator = value;
	}
	
	public void setReportListener(DotTemplateReportListener listener){
		this.reportListener = listener;
	}
	
	public void setDotTemplateReportConfiguration(Map<String, DotTemplateReportConfiguration> value){
		this.template_config = value;
	}
	
	private static void writeOutput(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024 * 200];
	    int bytesRead = 0;
	    while ((bytesRead = in.read(buffer)) >= 0) {
	        out.write(buffer, 0, bytesRead);
	    }
	}

	private UserSelectionSnapshot getUserSelectionSnapshotForReportGeneration(HttpServletRequest request, String templateName) throws Exception{
		UserSelectionSnapshot userSelection = new UserSelectionSnapshot(UserSession.getInstance(request));
		SystemReportUtils.setDefaultReportLocale(userSelection, this.getEffectiveReportLocale());
		if(reportListener != null){
			userSelection = reportListener.getUserSelectionSnapshotForReportGeneration(request, templateName, userSelection);
		}
		return userSelection;
	}
	
	public void process(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		String template_name = request.getParameter(PARAM_TEMPLATE_FILE);
		if(StringUtils.isBlank(template_name)){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Template not specified");
			return;
		}

		DotTemplateReportConfiguration config = null;
		
		if(this.template_config != null){
			config = this.template_config.get(template_name);
		}

		if (config==null){
			if(request.getParameter(PARAM_LAUNCH_WEBSTART) != null){
				if(reportListener != null) reportListener.onWebStartRequest(request, template_name);
				this.writeWebstartLaunchJnlp(request, response, template_name);
				return;
			}
		}else{
			if (!config.getUseBirtReportEngine())
			{
				if(request.getParameter(PARAM_LAUNCH_WEBSTART) != null){
					if(reportListener != null) reportListener.onWebStartRequest(request, template_name);
					this.writeWebstartLaunchJnlp(request, response, template_name);
					return;
				}
			}
		}
		File template_file = new File(new File(new File(ApplicationConfig.getConfiguredInstance().getServerRootPath()), this.template_dir), template_name);
		if(! template_file.exists()){
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "Template file not found");
			return;
		}
		
		// use this to configure xml output for different "template"
		if(this.generator != null){
			if(config != null){
				this.generator.setUseSelectedModuleGeneratorBeansOnly(config.getBeanForReportXml());
				this.generator.setRecursiveDataHandler(config.getRecursiveDataHandler());
			}else{
				this.generator.setUseSelectedModuleGeneratorBeansOnly(null);
				this.generator.setRecursiveDataHandler(null);
			}
		}
		
		ByteArrayOutputStream xml_out = new ByteArrayOutputStream();
		XslJob xslJob = new XslJob(null, xml_out, new ReportDataGenerator[] {this.generator}, "XML");
		xslJob.setSaveGeneratedXml(false);
		xslJob.setOutputValidationMessagesInXml(true);
		xslJob.run(this.getUserSelectionSnapshotForReportGeneration(request, template_name));
		
		if (config== null){
			runMacroVersion(xml_out,template_file,template_name,request,response);
		}else{
			if (config.getUseBirtReportEngine()){
				String outputType=request.getParameter(PARAM_FORMAT);
				runBirtVersion(xml_out,template_file,template_name,(outputType!=null?outputType:config.getBirtReportOutputType()),request,response);	
			}else{
				runMacroVersion(xml_out,template_file,template_name,request,response);
			}
		}
	}	
	private void runMacroVersion(ByteArrayOutputStream xml_out,File template_file,String template_name,HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		ByteArrayOutputStream zipped_bytes = new ByteArrayOutputStream();
		ZipOutputStream zipped_out = new ZipOutputStream(zipped_bytes);
		
		zipped_out.putNextEntry(new ZipEntry("data.xml"));
		zipped_out.write(xml_out.toByteArray());
		zipped_out.closeEntry();
		
		xml_out.close();
		xml_out = null;
		
		FileInputStream file_in = new FileInputStream(template_file);
		zipped_out.putNextEntry(new ZipEntry(template_file.getName()));
		writeOutput(file_in, zipped_out);

		zipped_out.closeEntry();
		file_in.close();
		
		zipped_out.finish();
		zipped_out.close();
		
		byte[] data = zipped_bytes.toByteArray();
		
		FileDownloadWriter.writeFileOutputFromStream(request, response, new ByteArrayInputStream(data), true, extractFileNameWithNoExt(template_file) + ".zip", "application/zip", data.length);
	}

	private void runBirtVersion(ByteArrayOutputStream xml_out,File template_file,String template_name,String outputType,HttpServletRequest request, HttpServletResponse response) throws Exception{
		ByteArrayOutputStream report_files = new ByteArrayOutputStream();
		
		
		Map runtimeContext = new HashMap();
		
		BhaDrillingParamGraph b= new BhaDrillingParamGraph();
		
		runtimeContext.put("BhaDrillingParamGraph", b);
		
		
		BirtReportEngine engine=BirtReportEngine.getConfiguredInstance();
		engine.run(template_file.getAbsolutePath(), report_files, new ByteArrayInputStream(xml_out.toByteArray()), outputType, this.getUserSelectionSnapshotForReportGeneration(request, template_name),runtimeContext);
		
		byte[] data = report_files.toByteArray();
		
		FileDownloadWriter.writeFileOutputFromStream(request, response, new ByteArrayInputStream(data), true, extractFileNameWithNoExt(template_file) + "."+outputType, "application/"+outputType, data.length);

	}
	private void writeWebstartLaunchJnlp(HttpServletRequest request, HttpServletResponse response, String template) throws Exception {

		UserSession userSession = UserSession.getInstance(request);
		String baseUrl = userSession.getClientAbsoluteBaseUrl();
		
		response.setContentType("application/x-java-jnlp-file");
		
		Writer writer = response.getWriter();
		writer.append(
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>" + 
				"<jnlp codebase=\"" + baseUrl + "public/dotreport/webstart/" + "\">" +
			    "<information>" + 
			       "<title>IDSDATANET REPORT</title>" +
			       "<vendor>IDSDatanet</vendor>" +
			       "<description>test</description>" + 
			    "</information>" + 	
			    "<security><all-permissions/></security>" +
				"<resources>" +
					"<j2se version=\"1.5+\"/>" + 
					"<jar href=\"idsdata.jar\"/>" +
				"</resources>" + 
				"<application-desc main-class=\"idsdata.Main\">" +
					"<argument>" + request.getSession().getId() + "</argument>" +
					"<argument>" + template + "</argument>" + 
					"<argument>" + baseUrl + this.controllerBeanUrl + ".html" +
					";jsessionid=" + request.getSession().getId() +
					"</argument>" +
				"</application-desc>" +
				"</jnlp>");
		writer.close();
	}
	
	private static String extractFileNameWithNoExt(File f){
		String name = f.getName();
		int i = name.lastIndexOf(".");
		if(i > 0){
			return name.substring(0, i);
		}else{
			return name;
		}
	}
	
}
