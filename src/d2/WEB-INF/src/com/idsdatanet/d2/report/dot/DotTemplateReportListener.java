package com.idsdatanet.d2.report.dot;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public interface DotTemplateReportListener {
	public UserSelectionSnapshot getUserSelectionSnapshotForReportGeneration(HttpServletRequest request, String template, UserSelectionSnapshot defaultUserSelectionSnapshot) throws Exception;
	
	public void onWebStartRequest(HttpServletRequest request, String template) throws Exception;
}
