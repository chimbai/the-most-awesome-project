package com.idsdatanet.d2.report.performanceReview;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.HseIncident;
import com.idsdatanet.d2.core.model.LessonTicket;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class PerformanceReviewReportDataNodeLoadHandler implements DataNodeLoadHandler {

	public static String CUSTOM_PROPERTY_SELECTED_DATE = "CP_SELECTED_DATE";
	private Map<String, ReportDaily> _reportDailyMap = new HashMap<String, ReportDaily>();
	
	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}
	
	private ReportDaily getReportDaily(String operationUid, String dailyUid) throws Exception {
		if (this._reportDailyMap.containsKey(dailyUid)) return this._reportDailyMap.get(dailyUid);
		
		String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
		String query = "from ReportDaily where (isDeleted = false or isDeleted is null) and reportType=:reportType and dailyUid=:dailyUid";
		String[] paramNames = {"reportType", "dailyUid"};
		Object[] paramValues = {reportType, dailyUid };
		List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(query, paramNames, paramValues);
		if (reportDailyList.size()>0) {
			return reportDailyList.get(0);
		}
		
		return null;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		
		List<Object> output_maps = new ArrayList<Object>();
		
		Date endDate = (Date) userSelection.getCustomProperty(CUSTOM_PROPERTY_SELECTED_DATE);
		if (endDate==null) endDate = new Date();
		
		Calendar cDate = Calendar.getInstance();
		cDate.setTime(endDate);
		cDate.set(Calendar.HOUR_OF_DAY, 0);
		cDate.set(Calendar.MINUTE, 0);
		cDate.set(Calendar.SECOND, 0);
		cDate.set(Calendar.MILLISECOND, 0);
		cDate.set(Calendar.DAY_OF_MONTH, 1);
		endDate = cDate.getTime();
		cDate.add(Calendar.MONTH, -3);
		Date startDate = cDate.getTime();
	
		
		if(meta.getTableClass().equals(LessonTicket.class)) {
			String queryString = "SELECT l FROM LessonTicket l, Daily d " +
					"WHERE (l.isDeleted=false or l.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND l.dailyUid=d.dailyUid " +
					"AND d.dayDate>=:startDate and d.dayDate<:endDate " +
					"AND d.dailyUid in (select dailyUid from " +
					"	ReportDaily where (isDeleted=false or isDeleted is null) " +
					"	and supervisor!='' and supervisor is not null" +
					"	and reportDatetime>=:startDate and reportDatetime<:endDate)" +
					"ORDER BY d.dayDate ";
			
			List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"startDate", "endDate"}, new Object[]{startDate, endDate});
			for (Object rec: list) {
				LessonTicket lessonTicket = (LessonTicket) rec;
				
				if (lessonTicket!=null){
					ReportDaily reportDaily = this.getReportDaily(lessonTicket.getOperationUid(), lessonTicket.getDailyUid());
					if (reportDaily!=null) {
						lessonTicket.setResponsibleparty(reportDaily.getCompEngineer());
						lessonTicket.setReportedbyName(reportDaily.getSupervisor());
					}
				}
				
				output_maps.add(lessonTicket);
			}
		} else if(meta.getTableClass().equals(HseIncident.class)) {
			String queryString = "SELECT h FROM HseIncident h, Daily d " +
					"WHERE (h.isDeleted=false or h.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND h.dailyUid=d.dailyUid " +
					"AND date(h.hseEventdatetime) = date(d.dayDate) " +
					"AND d.dayDate>=:startDate and d.dayDate<:endDate " +
					"AND d.dailyUid in (select dailyUid from " +
					"	ReportDaily where (isDeleted=false or isDeleted is null) " +
					"	and supervisor!='' and supervisor is not null" +
					"	and reportDatetime>=:startDate and reportDatetime<:endDate)" +
					"ORDER BY d.dayDate ";
			
			List<Object> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"startDate", "endDate"}, new Object[]{startDate, endDate});
			for (Object rec: list) {
				HseIncident hseIncident = (HseIncident) rec;
				if (hseIncident!=null){
					ReportDaily reportDaily = this.getReportDaily(hseIncident.getOperationUid(), hseIncident.getDailyUid());
					if (reportDaily!=null) {
						hseIncident.setReportedBy(reportDaily.getSupervisor());
					}
				}
				output_maps.add(hseIncident);
			}
		}
		return output_maps;
	}

}
