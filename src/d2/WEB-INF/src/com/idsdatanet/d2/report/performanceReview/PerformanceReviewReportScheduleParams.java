package com.idsdatanet.d2.report.performanceReview;

public class PerformanceReviewReportScheduleParams {
	private boolean distinctReport = false;
	private String reportUserUid = null;
	
	public void setDistinctReport(boolean value){
		this.distinctReport = value;
	}
	
	public void setReportUserUid(String value){
		this.reportUserUid = value;
	}
	
	public boolean isDistinctReport(){
		return this.distinctReport;
	}
	
	public String getReportUserUid(){
		return this.reportUserUid;
	}

}
