package com.idsdatanet.d2.report.dvd;

import java.util.ArrayList;
import java.util.List;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.RecursiveDataHandler;
import com.idsdatanet.d2.core.report.RecursiveDataParam;

public class ParentDVDOperationHandler implements RecursiveDataHandler {
	public RecursiveDataParam getNextData(List<UserContext> userContexts) throws Exception {
		List<UserContext> parent_list = new ArrayList<UserContext>();
		
		for(UserContext userContext : userContexts){
			ParentDVDOperationUserSelectionSnapshot parent_snapshot = new ParentDVDOperationUserSelectionSnapshot(userContext.getUserSelection());
			if(parent_snapshot.isDVDParentExists()){
				parent_list.add(UserContext.copy(userContext, parent_snapshot));
			}
		}
	
		if(parent_list.size() == 0) return null;
		
		RecursiveDataParam param = new RecursiveDataParam();
		param.setContinue(true);
		param.setUserContext(parent_list);
		param.setXmlElementName("ParentWell");
		
		return param;
	}
}
