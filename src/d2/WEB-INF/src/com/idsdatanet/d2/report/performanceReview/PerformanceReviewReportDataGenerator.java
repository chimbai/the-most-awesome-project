package com.idsdatanet.d2.report.performanceReview;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataNode;
import com.idsdatanet.d2.core.report.validation.ReportValidation;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;

public class PerformanceReviewReportDataGenerator implements ReportDataGenerator{

	public static String CUSTOM_PROPERTY_SELECTED_DATE = "CP_SELECTED_DATE";
	public static String CUSTOM_PROPERTY_SUPERVISOR = "CP_SUPERVISOR";
	
	public void disposeOnDataGeneratorExit() {
		// TODO Auto-generated method stub
	}

	public void generateData(UserContext userContext,
			ReportDataNode reportDataNode, String reportType,
			ReportValidation validation) throws Exception {

		Date selectedDate = new Date(); 
		if (userContext!=null && userContext.getUserSelection()!=null && userContext.getUserSelection().getCustomProperties()!=null) { 
			selectedDate = (Date) userContext.getUserSelection().getCustomProperty(CUSTOM_PROPERTY_SELECTED_DATE);
		}
		if (selectedDate==null) selectedDate = new Date();
		
		String thisSupervisor = (String) userContext.getUserSelection().getCustomProperty(CUSTOM_PROPERTY_SUPERVISOR);
		
		Calendar cDate = Calendar.getInstance();
		cDate.setTime(selectedDate);
		cDate.set(Calendar.HOUR_OF_DAY, 0);
		cDate.set(Calendar.MINUTE, 0);
		cDate.set(Calendar.SECOND, 0);
		cDate.set(Calendar.MILLISECOND, 0);
		cDate.set(Calendar.DAY_OF_MONTH, 1);
		selectedDate = cDate.getTime();
		cDate.add(Calendar.MONTH, -3);
		Date startDate = cDate.getTime();
		
		if (StringUtils.isNotBlank(thisSupervisor)) {
			this.addChildElement(reportDataNode, thisSupervisor, startDate, selectedDate);
		} else {
			String queryString = "SELECT rd.supervisor " +
					"FROM ReportDaily rd, Daily d, Well w, Wellbore wb, Operation o " +
					"WHERE (rd.isDeleted=false or rd.isDeleted is null) " +
					"AND (d.isDeleted=false or d.isDeleted is null) " +
					"AND (w.isDeleted=false or w.isDeleted is null) " +
					"AND (wb.isDeleted=false or wb.isDeleted is null) " +
					"AND (o.isDeleted=false or o.isDeleted is null) " +
					"AND w.wellUid=wb.wellUid " +
					"AND wb.wellboreUid=o.wellboreUid " +
					"AND o.operationUid=d.operationUid " +
					"AND d.dailyUid=rd.dailyUid " +
					"AND (rd.supervisor is not null and rd.supervisor!='') " +
					"AND d.dayDate<:endDate " +
					"AND d.dayDate>=:startDate " +
					"GROUP BY rd.supervisor " +
					"ORDER BY rd.supervisor";
			
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"endDate", "startDate"}, new Object[]{selectedDate, startDate});
			for (Object rec: list) {
				String supervisor = rec.toString();
				
				if (StringUtils.isNotBlank(supervisor)) {
					this.addChildElement(reportDataNode, supervisor, startDate, selectedDate);
					//this.addChildElement(reportDataNode, supervisor, null, null);
				}	
			}
		}
	}
	
	private void addChildElement(ReportDataNode reportDataNode, String thisSupervisor, Date startDate, Date endDate) throws Exception {
		String queryString = "SELECT o.groupUid, o.operationUid " +
			"FROM ReportDaily rd, Daily d, Well w, Wellbore wb, Operation o " +
			"WHERE (rd.isDeleted=false or rd.isDeleted is null) " +
			"AND (d.isDeleted=false or d.isDeleted is null) " +
			"AND (w.isDeleted=false or w.isDeleted is null) " +
			"AND (wb.isDeleted=false or wb.isDeleted is null) " +
			"AND (o.isDeleted=false or o.isDeleted is null) " +
			"AND w.wellUid=wb.wellUid " +
			"AND wb.wellboreUid=o.wellboreUid " +
			"AND o.operationUid=d.operationUid " +
			"AND d.dailyUid=rd.dailyUid " +
			"AND rd.supervisor=:supervisor " +
			(endDate!=null && startDate!=null?"AND d.dayDate<:endDate AND d.dayDate>=:startDate ":"") +
			"GROUP BY rd.supervisor, o.groupUid, o.operationUid " +
			"ORDER BY rd.supervisor, w.wellName, wb.wellboreName, o.operationName, d.dayDate";
		
		List<Object[]> wells;
		if (startDate!=null && endDate!=null) {
			wells = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"supervisor","endDate","startDate"}, new Object[]{thisSupervisor, endDate, startDate});
		} else {
			wells = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"supervisor"}, new Object[]{thisSupervisor});
		}
		for (Object[] well: wells) {
			String groupUid = (String) well[0];
			String operationUid = (String) well[1];
			
			if (StringUtils.isNotBlank(thisSupervisor) && StringUtils.isNotBlank(groupUid) && StringUtils.isNotBlank(operationUid)) {
				ReportDataNode childNode = reportDataNode.addChild("PerformanceReview");
				childNode.addProperty("supervisor", StringUtils.trim(thisSupervisor));
				childNode.addProperty("groupUid", groupUid);
				childNode.addProperty("operationUid", operationUid);
				childNode.addProperty("operationName", CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, operationUid));
			}							
		}					


	}
	
}
