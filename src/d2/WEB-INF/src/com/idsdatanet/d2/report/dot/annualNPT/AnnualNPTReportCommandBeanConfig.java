package com.idsdatanet.d2.report.dot.annualNPT;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Activity;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.report.validation.CommandBeanReportValidator;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.AjaxAutoPopulateListener;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanConfiguration;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanReportDataListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeAllowedAction;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.DataSummaryListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.SimpleAjaxActionHandler;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class AnnualNPTReportCommandBeanConfig extends CommandBeanConfiguration {
	public static final String REPORT_YEAR = "annual_npt_report_year";
	public static final String ACCESSIBLE_OPERATIONS = "accessible_operations_by_ops_team";
	
	private class DataLoader implements DataNodeLoadHandler {
		public List<Object> loadChildNodes(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, Pagination pagination, HttpServletRequest request) throws Exception{
			int year = Integer.parseInt((String) userSelection.getCustomProperty(REPORT_YEAR));
			Calendar calendar = Calendar.getInstance();
			calendar.set(year, 0, 1, 0, 0, 0);
			Date d1 = calendar.getTime();
			calendar.set(year + 1, 0, 1, 0, 0, 0);
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			Date d2 = calendar.getTime();

			String ops = (String) userSelection.getCustomProperty(ACCESSIBLE_OPERATIONS);
			String opsFilter = "";
			if (StringUtils.isNotBlank(ops)){
				opsFilter = " AND O.operationUid IN ("  + ops + ") ";
			}		
			
			if(Operation.class.equals(meta.getTableClass())){
				return removeDuplicatedEntry(ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select O from Daily D, Operation O where D.operationUid = O.operationUid " + opsFilter + " AND (D.dayDate >= :start_date and D.dayDate <= :end_date) and (D.isDeleted is null or D.isDeleted = false) and (O.isDeleted is null or O.isDeleted = false)", new String[] {"start_date","end_date"}, new Object[] {d1, d2}), userSelection);
			}else if(Activity.class.equals(meta.getTableClass())){
				return ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select A " +
						"from Activity A, Daily D, Operation O " +
						"where D.operationUid = O.operationUid " +
						"AND A.dailyUid = D.dailyUid " +
						"and (D.dayDate >= :start_date and D.dayDate <= :end_date) " +
						"and (A.isOffline is null or A.isOffline = false) " +
						"and (A.isSimop is null or A.isSimop = false) " +
						"and (A.carriedForwardActivityUid is null or A.carriedForwardActivityUid = '') " +
						"and (A.isDeleted is null or A.isDeleted = false) " +
						"and (D.isDeleted is null or D.isDeleted = false) " +
						"and (O.isDeleted is null or O.isDeleted = false)" + 
						opsFilter, new String[] {"start_date","end_date"}, new Object[] {d1, d2});
			}else if(ReportDaily.class.equals(meta.getTableClass())){
				return ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("select RD from ReportDaily RD, Daily D, Operation O where D.operationUid = O.operationUid " + opsFilter + " AND RD.dailyUid = D.dailyUid and (D.dayDate >= :start_date and D.dayDate <= :end_date) and (RD.isDeleted is null or RD.isDeleted = false) and (D.isDeleted is null or D.isDeleted = false) and (O.isDeleted is null or O.isDeleted = false)", new String[] {"start_date","end_date"}, new Object[] {d1, d2});
			}
			return null;
		}
		
		private List<Object> removeDuplicatedEntry(List<Operation> rs, UserSelectionSnapshot userSelection) throws Exception {
			Map<String,Operation> map = new HashMap<String,Operation>();
			for(Operation o: rs){
				if (!map.containsKey(o.getOperationUid())) {
					o.setOperationName(CommonUtil.getConfiguredInstance().getCompleteOperationName(userSelection.getGroupUid(), o.getOperationUid()));
					map.put(o.getOperationUid(), o);
				}
			}
			
			return new ArrayList(map.values());
		}
		
		public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta){
			if(Operation.class.equals(meta.getTableClass())) return true;
			if(ReportDaily.class.equals(meta.getTableClass())) return true;
			if(Activity.class.equals(meta.getTableClass())) return true;
			return false;
		}
	}
	
	public void init(BaseCommandBean commandBean) throws Exception{
		List<String> fields = new ArrayList<String>();
		fields.add("Activity.operationUid");
		fields.add("Activity.activityDuration");
		fields.add("Activity.classCode");
		fields.add("Activity.rootCauseCode");
		fields.add("Activity.startDatetime");
		fields.add("Activity.endDatetime");
		fields.add("Activity.lookupCompanyUid");
		
		fields.add("Operation.operationUid");
		fields.add("Operation.operationName");
		fields.add("Operation.spudDate");
		fields.add("Operation.afe");
		fields.add("Operation.amountSpentPriorToSpud");
		
		fields.add("ReportDaily.operationUid");
		fields.add("ReportDaily.daycost");
		fields.add("ReportDaily.daytangiblecost");
		fields.add("ReportDaily.reportType");
		
		commandBean.setReportXmlOutputFields(fields);
	}
	
	public DataNodeListener getDataNodeListener() throws Exception{
		return null;
	}
	
	public CommandBeanListener getCommandBeanListener() throws Exception{
		return null;
	}
	
	public DataLoaderInterceptor getDataLoaderInterceptor() throws Exception{
		return null;
	}
	
	public ActionManager getActionManager() throws Exception{
		return null;
	}

	public DataNodeAllowedAction getDataNodeAllowedAction() throws Exception{
		return null;
	}
	
	public DataNodeLoadHandler getDataNodeLoadHandler() throws Exception {
		return new DataLoader();
	}
	
	public SimpleAjaxActionHandler getSimpleAjaxActionHandler() throws Exception {return null;}
	
	public AjaxAutoPopulateListener getAjaxAutoPopulateListener() throws Exception {return null;}
	
	public CommandBeanReportDataListener getCommandBeanReportDataListener() throws Exception {return null;}
	
	public DataSummaryListener getDataSummaryListener() throws Exception {return null;}
	
	public List<CommandBeanReportValidator> getCommandBeanReportValidators() throws Exception {
		return null;
	}
}
