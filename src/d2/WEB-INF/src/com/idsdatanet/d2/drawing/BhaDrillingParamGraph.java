package com.idsdatanet.d2.drawing;

/* Moses's Comments
   This is a independent class that will help in generating a BHA Drilling Parameters Graph 
   The output are based on CNRL given template.

*/
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.*;

import java.io.File;

import java.io.OutputStream;
import java.util.ArrayList;


import javax.imageio.ImageIO;

import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;

public class BhaDrillingParamGraph {
	
	// D2 Field Related Declaration
	private String bharunUidField="bharunUid";
	private String unitDepth="m";
	private String unitRop="m/hr";
	private String unitRpm="rpm";
	private String unitWob="daN";
	private String titleRop="Interval Rop ("+unitRop+")";
	private String titleRpm="Bit RPM ("+unitRpm+")";
	private String titleWob="Weight on Bit ("+unitWob+")";
	private String titleDepth="Depth in "+unitDepth;
	private String bharunUid=null;
	private ArrayList paramValues=null;
	
	
	// Graphic  Declaration
	private Graphics g;
	private BufferedImage bi=null;
	
	// Diagram  Drawing Properties Declaration
	private int width=1200;
	private int height=1600;
	private int graphPadding=15;
	private int axisLabelReserved=70;
	private int offsetReserved=this.graphPadding+this.axisLabelReserved;
	
	// Graph Scaling Properties
	private int scaleXRop=0;
	private int scaleXRpm=0;
	private int scaleXWob=0;
	
	// Graph Highest X axis values
	private double endRop=0;
	private double endRpm=0;
	private double endWob=0;
	
	// Graph Y axis Range and Scale
	private double startDepth=0;
	private double endDepth=0;
	private int scaleDepth=0;
	
	// Color Declaration
	private Color colorOutline=Color.BLACK;
	private Color colorGraphOutline=Color.BLACK;
	private Color colorGraphInline=Color.GRAY;
	private Color colorGraphValues=Color.BLACK;
	private Color colorLabels=Color.BLACK;
	private Color colorBackground=Color.WHITE;
	
	// Font Declaration
	private Font fontScale=new Font("Arial",0,20);
	private Font fontTitle=new Font("Arial",0,25);
	
	// Empty Parameter Constructor
	public BhaDrillingParamGraph(){
		
	}
	
	// Constructor to be Used in D2
	public BhaDrillingParamGraph(String bharunUid )
	{
		this.bharunUid=bharunUid;
		
		this.paramValues=new ArrayList();
		
	}
	
	public void setBharunUid(String bharunUid) {
		this.bharunUid = bharunUid;
		if (paramValues==null){
		this.paramValues=new ArrayList();
		}
	}
	
	// Actual Generation of the Drawing 
	public String generateModel() throws Exception{
		initiateGraphics();
		drawBackgroundColor();
		drawOutline();
		if (paramValues!=null){
			if (paramValues.size()>1)
			{
				drawGraphLayout();
				plotValues();
			}
		}
		String Files=outputImage();
		paramValues=null;
		clearSetting();
		return Files;
		
	}
	
	// Output Diagram to Jpeg
	private String outputImage() throws Exception{
		OutputStream output= null;
		String diagram_files=ApplicationConfig.getConfiguredInstance().getServerRootPath()+"WEB-INF/Diagram/Drill_Param_"+this.bharunUid+".jpg";
		File f=new File(diagram_files);
		if(!f.exists())
		{
			f.createNewFile();
		}
		ImageIO.write(bi, "jpg", f);
		return "WEB-INF/Diagram/Drill_Param_"+this.bharunUid+".jpg";
	}
	
	// Initiate Graphic Instance for drawing.
	private void initiateGraphics(){
		bi=new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
		g=bi.getGraphics();
	}
	
	// Draw Diagram Backgound
	private void drawBackgroundColor(){
		g.setColor(this.colorBackground);
		g.fillRect(0, 0, width, height);
	}
	
	// Draw Outline of the whole diagram
	private void drawOutline(){
			g.setColor(this.colorOutline);
			g.drawRect(0, 0, width-2, height-2);
			g.drawLine(width/3, 1, width/3, height-2);
			g.drawLine(width/3*2, 1, width/3*2, height-2);
	}
	
	// Define the Drawing Scale for Y Axis Depth
	private void defineDepthDrawingValues(){
		double startDepth=((BhaDrillingParam)paramValues.get(0)).getDepthEnd();
		double endDepth=((BhaDrillingParam)paramValues.get(paramValues.size()-1)).getDepthEnd();
		double progressDepth=endDepth-startDepth;
		if (progressDepth<=500)
		{
			this.startDepth=roundNearest((int)startDepth,10)-50;
			this.endDepth=roundNearest((int)endDepth,10)+50;
			scaleDepth=10;
			
		}else{
			this.startDepth=roundNearest((int)startDepth,100)-100;
			this.endDepth=roundNearest((int)endDepth,100);
			scaleDepth=100;
		}
	}
	
	// Define the Drilling Parameters Values based on user Input
	private void defineXDrawingValues(){
		for(int a=0;a<paramValues.size();a++)
		{
			if (((BhaDrillingParam)paramValues.get(a)).getRop()>endRop)
				endRop=((BhaDrillingParam)paramValues.get(a)).getRop();
			if (((BhaDrillingParam)paramValues.get(a)).getRpm()>endRpm)
				endRpm=((BhaDrillingParam)paramValues.get(a)).getRpm();
			if (((BhaDrillingParam)paramValues.get(a)).getWob()>endWob)
				endWob=((BhaDrillingParam)paramValues.get(a)).getWob();
		}
		endRop=roundNearest(endRop,1);
		endRpm=roundNearest(endRpm,10);
		endWob=roundNearest(endWob,1);
		scaleXRop=calScale((int)endRop,1);
		scaleXRpm=calScale((int)endRpm,10);
		scaleXWob=calScale((int)endWob,1);
	}
	
	// Draw the Graph Skeleton 
	private void drawGraphLayout(){
		int startX=0;
		for(int a=0;a<3;a++)
		{
			g.setColor(this.colorGraphOutline);
			int offsetReserved=this.graphPadding+this.axisLabelReserved;
			g.drawRect(startX+offsetReserved, offsetReserved,300 , height-(offsetReserved*2));
			startX+=400;
		}
		defineDepthDrawingValues();
		defineXDrawingValues();
		drawGraphInline();
	}
	
	// Draw the Graph Inlines with scales
	private void drawGraphInline(){
		
		int graphHeight=height-((this.graphPadding+this.axisLabelReserved)*2);
		int graphWidth=300;
		int scaleDrawNeeded=(int)(this.endDepth-this.startDepth)/this.scaleDepth;
		int pixelPerScale=graphHeight/scaleDrawNeeded;
		int startX=0;
		int offsetReserved=this.graphPadding+this.axisLabelReserved;
		g.setFont(this.fontTitle);
		int depthTitleY=(this.height/2)-((wordHeight(this.titleDepth)+3)*this.titleDepth.length()/2);
		for(int a=0;a<this.titleDepth.length();a++){
			g.drawString(this.titleDepth.substring(a, a+1), 15-(wordWidth(this.titleDepth.substring(a, a+1))/2), depthTitleY);
			depthTitleY+=wordHeight(this.titleDepth)+3;
		}
		
		for(int a=0;a<3;a++)
		{
			int fontWidth=0;
			int fontHeight=0;
			int textStartX=0;
			int textStartY=0;
			String textToDraw="";
			switch (a)
			{
			case 0:	
				fontWidth=wordWidth(this.titleRop);
				fontHeight=wordHeight(this.titleRop);
				textToDraw=this.titleRop;
				break;
			case 1:
				fontWidth=wordWidth(this.titleRpm);
				fontHeight=wordHeight(this.titleRpm);
				textToDraw=this.titleRpm;
				break;
			case 2:
				fontWidth=wordWidth(this.titleWob);
				fontHeight=wordHeight(this.titleWob);
				textToDraw=this.titleWob;
				break;
			}
			textStartX=(a*400)+((400)/2)-(fontWidth/2);
			textStartY=35;
			g.setColor(this.colorLabels);
			g.setFont(this.fontTitle);
			g.drawString(textToDraw, textStartX, textStartY);
			
			
			int depthCounter=(int)this.startDepth;
			g.setFont(this.fontScale);
			g.setColor(this.colorLabels);
			g.drawString(String.valueOf(depthCounter), startX+offsetReserved-5-wordWidth(String.valueOf(depthCounter)), offsetReserved+(wordHeight(String.valueOf(depthCounter))/2));
			depthCounter+=this.scaleDepth;
			for(int row=1;row<=scaleDrawNeeded-1;row++)
			{
				g.setColor(this.colorLabels);
				g.drawString(String.valueOf(depthCounter), startX+offsetReserved-5-wordWidth(String.valueOf(depthCounter)), offsetReserved+(row*pixelPerScale)+(wordHeight(String.valueOf(depthCounter))/2));
				depthCounter+=this.scaleDepth;
				g.setColor(this.colorGraphInline);
				drawDashedLine(g,startX+offsetReserved-5,offsetReserved+(row*pixelPerScale), startX+offsetReserved+300, offsetReserved+(row*pixelPerScale),10,4);
			}
			g.setColor(this.colorLabels);
			g.drawString(String.valueOf(depthCounter), startX+offsetReserved-5-wordWidth(String.valueOf(depthCounter)), offsetReserved+(scaleDrawNeeded*pixelPerScale)+(wordHeight(String.valueOf(depthCounter))/2));
			
			startX+=400;
		}
		g.setFont(this.fontScale);
		int xScaleValue=scaleXRop;
		// draw rop x axis inline
		int ropScaleDrawNeeded=(int)this.endRop/this.scaleXRop;
		int ropPixelPerScale=(graphWidth-2)/ropScaleDrawNeeded;
		g.setColor(this.colorLabels);
		g.drawString("0",offsetReserved-5, 80);
		for(int col=1;col<ropScaleDrawNeeded;col++)
		{
			g.setColor(this.colorLabels);
			g.drawString(String.valueOf(xScaleValue), offsetReserved+(ropPixelPerScale*col)+1-(wordWidth(String.valueOf(xScaleValue))/2), 80);
			g.setColor(this.colorGraphInline);
			drawDashedLine(g,offsetReserved+(ropPixelPerScale*col)+1,offsetReserved-5, offsetReserved+(ropPixelPerScale*col)+1, this.height-offsetReserved,10,4);
			xScaleValue+=scaleXRop;
		}
		g.setColor(this.colorLabels);
		g.drawString(String.valueOf(xScaleValue), offsetReserved+(ropPixelPerScale*ropScaleDrawNeeded)+1-(wordWidth(String.valueOf(xScaleValue))/2), 80);
		
		
		xScaleValue=scaleXRpm;
		// draw rpm x axis inline
		int rpmScaleDrawNeeded=(int)this.endRpm/this.scaleXRpm;
		int rpmPixelPerScale=(graphWidth-2)/rpmScaleDrawNeeded;
		g.setColor(this.colorLabels);
		g.drawString("0",400+offsetReserved-5, 80);
		for(int col=1;col<rpmScaleDrawNeeded;col++)
		{
			g.setColor(this.colorLabels);
			g.drawString(String.valueOf(xScaleValue), 400+offsetReserved+(rpmPixelPerScale*col)+1-(wordWidth(String.valueOf(xScaleValue))/2), 80);
			g.setColor(this.colorGraphInline);
			drawDashedLine(g,(this.width/3)+offsetReserved+(rpmPixelPerScale*col)+1,offsetReserved-5, (this.width/3)+offsetReserved+(rpmPixelPerScale*col)+1, this.height-offsetReserved,10,4);
			xScaleValue+=scaleXRpm;
		}
		g.setColor(this.colorLabels);
		g.drawString(String.valueOf(xScaleValue), 400+offsetReserved+(rpmPixelPerScale*rpmScaleDrawNeeded)+1-(wordWidth(String.valueOf(xScaleValue))/2), 80);
		
		
		xScaleValue=scaleXWob;
		// draw wob x axis inline
		int wobScaleDrawNeeded=(int)this.endWob/this.scaleXWob;
		int wobPixelPerScale=(graphWidth-2)/wobScaleDrawNeeded;
		g.setColor(this.colorLabels);
		g.drawString("0",800+offsetReserved-5, 80);
		for(int col=1;col<wobScaleDrawNeeded;col++)
		{
			g.setColor(this.colorLabels);
			g.drawString(String.valueOf(xScaleValue),800+ offsetReserved+(wobPixelPerScale*col)+1-(wordWidth(String.valueOf(xScaleValue))/2), 80);
			g.setColor(this.colorGraphInline);
			drawDashedLine(g,(this.width/3*2)+offsetReserved+(wobPixelPerScale*col)+1,offsetReserved-5, (this.width/3*2)+offsetReserved+(wobPixelPerScale*col)+1, this.height-offsetReserved,10,4);
			xScaleValue+=scaleXWob;
		}
		g.setColor(this.colorLabels);
		g.drawString(String.valueOf(xScaleValue), 800+offsetReserved+(wobPixelPerScale*wobScaleDrawNeeded)+1-(wordWidth(String.valueOf(xScaleValue))/2), 80);
		
	}
	
	// Plot Values
	private void plotValues(){
		plotRop();
		plotRpm();
		plotWob();
	}
	
	// Plot ROP Values onto the Graph
	private void plotRop(){
		int graphHeight=height-((this.graphPadding+this.axisLabelReserved)*2);
		int graphWidth=300;
		double pixelPerUnit=graphHeight/(this.endDepth-this.startDepth);
		double pixelPerRop=graphWidth/this.endRop;
		for(int a=0;a<paramValues.size()-1;a++)
		{
			BhaDrillingParam param=(BhaDrillingParam)paramValues.get(a);
			BhaDrillingParam paramNext=(BhaDrillingParam)paramValues.get(a+1);
			double ystart=(param.getDepthEnd()-this.startDepth)*pixelPerUnit;
			double yend=(paramNext.getDepthEnd()-this.startDepth)*pixelPerUnit;
			
			double xstart=param.getRop()*pixelPerRop;
			double xstartNext=paramNext.getRop()*pixelPerRop;
			g.setColor(this.colorGraphValues);
			if (param.getRop()<paramNext.getRop())
			{
				g.fillRect(this.offsetReserved+(int)xstart-4,this.offsetReserved+(int)ystart-4,(int)xstartNext-(int)xstart+8,8);
				g.fillRect(this.offsetReserved+(int)xstartNext-4, this.offsetReserved+(int)ystart, 8, (int)(yend-ystart));
			}else{
				
				g.fillRect(this.offsetReserved+(int)xstart-4, this.offsetReserved+(int)ystart, 8, (int)(yend-ystart));
				if (param.getRop()!=paramNext.getRop())
				{
					if (param.getRop()>paramNext.getRop())
					{
						g.fillRect(this.offsetReserved+(int)xstartNext-4,this.offsetReserved+(int)yend-4,((int)xstart-(int)xstartNext+8),8);
					}else{
						g.fillRect(this.offsetReserved+(int)xstart-4,this.offsetReserved+(int)yend-4,(int)xstartNext-(int)xstart+8,8);
					}
				}else{
					g.fillRect(this.offsetReserved+(int)xstart-4, this.offsetReserved+(int)yend, 8, 2);
				}
			}
		}
	}
	private void clearSetting(){
		scaleXRop=0;
		scaleXRpm=0;
		scaleXWob=0;
		
		// Graph Highest X axis values
		endRop=0;
		endRpm=0;
		endWob=0;
		
		// Graph Y axis Range and Scale
		startDepth=0;
		endDepth=0;
		scaleDepth=0;
	}
	
	// Plot RPM Values onto the Graph
	private void plotRpm(){
		int graphHeight=height-((this.graphPadding+this.axisLabelReserved)*2);
		int graphWidth=300;
		double pixelPerUnit=graphHeight/(this.endDepth-this.startDepth);
		double pixelPerRpm=graphWidth/this.endRpm;
		for(int a=0;a<paramValues.size()-1;a++)
		{
			BhaDrillingParam param=(BhaDrillingParam)paramValues.get(a);
			BhaDrillingParam paramNext=(BhaDrillingParam)paramValues.get(a+1);
			double ystart=(param.getDepthEnd()-this.startDepth)*pixelPerUnit;
			double yend=(paramNext.getDepthEnd()-this.startDepth)*pixelPerUnit;
			double xstart=param.getRpm()*pixelPerRpm;
			double xstartNext=paramNext.getRpm()*pixelPerRpm;
			g.setColor(this.colorGraphValues);
			if (param.getRpm()<paramNext.getRpm())
			{
				g.fillRect(400+this.offsetReserved+(int)xstart-4,this.offsetReserved+(int)ystart-4,(int)xstartNext-(int)xstart+8,8);
				g.fillRect(400+this.offsetReserved+(int)xstartNext-4, this.offsetReserved+(int)ystart, 8, (int)(yend-ystart));
			}else{
				g.fillRect(400+this.offsetReserved+(int)xstart-4, this.offsetReserved+(int)ystart, 8, (int)(yend-ystart));
				if (param.getRpm()!=paramNext.getRpm())
				{
					if (param.getRpm()>paramNext.getRpm())
					{
						g.fillRect(400+this.offsetReserved+(int)xstartNext-4,this.offsetReserved+(int)yend-4,((int)xstart-(int)xstartNext+8),8);
					}else{
						g.fillRect(400+this.offsetReserved+(int)xstart-4,this.offsetReserved+(int)yend-4,(int)xstartNext-(int)xstart+8,8);
					}
				}else{
				g.fillRect(400+this.offsetReserved+(int)xstart-4, this.offsetReserved+(int)yend-4, 8, 2);
				}
			}
		}
	}
	
	// Plot WOB Values onto the Graph
	private void plotWob(){
		int graphHeight=height-((this.graphPadding+this.axisLabelReserved)*2);
		int graphWidth=300;
		double pixelPerUnit=graphHeight/(this.endDepth-this.startDepth);
		double pixelPerWob=graphWidth/this.endWob;
		for(int a=0;a<paramValues.size()-1;a++)
		{
			BhaDrillingParam param=(BhaDrillingParam)paramValues.get(a);
			BhaDrillingParam paramNext=(BhaDrillingParam)paramValues.get(a+1);
			double ystart=(param.getDepthEnd()-this.startDepth)*pixelPerUnit;
			double yend=(paramNext.getDepthEnd()-this.startDepth)*pixelPerUnit;
			double xstart=param.getWob()*pixelPerWob;
			double xstartNext=paramNext.getWob()*pixelPerWob;
			g.setColor(this.colorGraphValues);
			if (param.getWob()<paramNext.getWob())
			{
				g.fillRect(800+this.offsetReserved+(int)xstart-4,this.offsetReserved+(int)ystart-4,(int)xstartNext-(int)xstart+8,8);
				g.fillRect(800+this.offsetReserved+(int)xstartNext-4, this.offsetReserved+(int)ystart, 8, (int)(yend-ystart));
			}else{
				g.fillRect(800+this.offsetReserved+(int)xstart-4, this.offsetReserved+(int)ystart, 8, (int)(yend-ystart));
				if (param.getWob()!=paramNext.getWob())
				{
					if (param.getWob()>paramNext.getWob())
					{
						g.fillRect(800+this.offsetReserved+(int)xstartNext-4,this.offsetReserved+(int)yend-4,((int)xstart-(int)xstartNext+8),8);
					}else{
						g.fillRect(800+this.offsetReserved+(int)xstart-4,this.offsetReserved+(int)yend-4,(int)xstartNext-(int)xstart+8,8);
					}
				}else{
					g.fillRect(800+this.offsetReserved+(int)xstart-4, this.offsetReserved+(int)yend-4, 8, 2);
				}
			}
		}
	}
	
	// Public method allowing external source to add drilling parameter for diagram generation
	// this is the main source for the diagram generation
	public void addParam(double depthStart, double depthEnd, double rop, double rpm, double wob)
	{
		if (rop>0 && rpm>0 && wob>0)
			paramValues.add(new BhaDrillingParam(depthStart, depthEnd, rop, rpm, wob));
	}
	
	// Method to round out figures
	private double roundNearest(double values, int nearest)
	{
		if ((values*10)%(nearest*10)>0)
		return ((values*10)+((nearest*10)-(values*10)%(nearest*10)))/10;
		else
			return values;
	}
	
	// Calculate appropriate scaling for graph axis
	private int calScale(int end, int multiplier){
		for(int a=4;a>1;a--)
		{
			if (end%(a*multiplier)<=(a*multiplier/10))
				return a*multiplier;
		}
		return 2*multiplier;
	}
	
	// Method for Dashed Line drawing
	private void drawDashedLine(Graphics g, int x1, int y1, int x2, int y2, double dashlength, double spacelength)
    {
            if ((x1 == x2) && (y1 == y2))
            {
                    g.drawLine(x1, y1, x2, y2);
                    return;
            }
            double linelength = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            double yincrement = (y2 - y1) / (linelength / (dashlength + spacelength));
            double xincdashspace = (x2 - x1) / (linelength / (dashlength + spacelength));
            double yincdashspace = (y2 - y1) / (linelength / (dashlength + spacelength));
            double xincdash = (x2 - x1) / (linelength / (dashlength));
            double yincdash = (y2 - y1) / (linelength / (dashlength));
            int counter = 0;
            for (double i = 0; i < linelength - dashlength; i += dashlength + spacelength)
            {
                    g.drawLine((int) (x1 + xincdashspace * counter), (int) (y1 + yincdashspace * counter), (int) (x1 + xincdashspace * counter + xincdash), (int) (y1 + yincdashspace * counter + yincdash));
                    counter++;
            }
            if ((dashlength + spacelength) * counter <= linelength)
            {
                    g.drawLine((int) (x1 + xincdashspace * counter), (int) (y1 + yincdashspace * counter), x2, y2);
           
            }
    }
	
	// Method for calculation word width
	private int wordWidth( String word){
		int Width=0;
		for(int a=0;a<word.length();a++){
			Width+=g.getFontMetrics().charWidth(word.charAt(a));
		}
		return Width;
	}
	
	// Method for calculation word height
	private int wordHeight(String word){
		
		return g.getFontMetrics().getHeight();
	}
}


class BhaDrillingParam{
	private double depthStart;
	private double depthEnd;
	private double rop;
	private double rpm;
	private double wob;
	
	public BhaDrillingParam(double depthStart, double depthEnd, double rop, double rpm, double wob) {
		this.depthStart = depthStart;
		this.depthEnd = depthEnd;
		this.rop = rop;
		this.rpm = rpm;
		this.wob = wob;
	}
	public double getDepthStart() {
		return depthStart;
	}
	public void setDepthStart(double depthStart) {
		this.depthStart = depthStart;
	}
	public double getDepthEnd() {
		return depthEnd;
	}
	public void setDepthEnd(double depthEnd) {
		this.depthEnd = depthEnd;
	}
	public double getRop() {
		return rop;
	}
	public void setRop(double rop) {
		this.rop = rop;
	}
	public double getRpm() {
		return rpm;
	}
	public void setRpm(double rpm) {
		this.rpm = rpm;
	}
	public double getWob() {
		return wob;
	}
	public void setWob(double wob) {
		this.wob = wob;
	}
	
}