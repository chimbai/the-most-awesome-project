package com.idsdatanet.d2.calculation.drillnet;
/**
 * All Formula/Calculation related to LeakOffTest
 * @author Moses
 *
 */
public class LeakOffTestCalculation {
	/**
	 * To calculate Estimated Mud Weight 
	 * Formula When
	 * i.  Depth & Pressure is not Blank : ( (Pressure(psi) / 0.052 / Depth(ft) ) + Mud Density(ppg) ) * 119.8264
	 * ii. Either Blank : Mud Density(ppg) * 119.8264
	 * @param pressure value in ft
	 * @param mudDensity value in ppg
	 * @param depth value in ft
	 * @return return estimated mud weight in based value (kg/m3).
	 */
	public static Double calculateEMW(Double pressure, Double mudDensity, Double depth)
	{
		Double emw = null;
		if (depth!=0 && pressure !=0)
			emw = ((pressure/0.052/depth) +  mudDensity) * 119.8264;
		else
			emw = mudDensity * 119.8264;
		return emw;
		
	}
}
