package com.idsdatanet.d2.calculation.drillnet;
/**
 * All Formula/Calculation related to Bharun
 * @author Fu
 *
 */
public class BharunCalculation {
	/**
	 * To calculate Pressure Drop across bit calculated (Pb)
	 * Formula When
	 * i.  flowAvg, mudWeight, nozzleTotal is not Blank :  (156.5 * flowAvg * flowAvg * mudWeight) / (nozzleTotal * nozzleTotal);
	 * @param flowAvg value in galUS/min
	 * @param mudWeight value in ppg
	 * @param nozzleTotal value in 32ndOfAnInch
	 * @return return Pb in value (psi).
	 */
	public static Double calculatePb(Double flowAvg, Double mudWeight, Double nozzleTotal)
	{
		Double pb = 0.00;
		if (nozzleTotal != 0 && flowAvg != 0 && mudWeight != 0){
			pb = (156.5 * flowAvg * flowAvg * mudWeight) / (nozzleTotal * nozzleTotal);
		}
		return pb;
		
	}
	
	/**
	 * To calculate %HHP at bit
	 * Formula When
	 * i.  pbCalc, flowAvg, sppAvg is not Blank : (((flowAvg * pbCalc) / 1714) / ((flowAvg * sppAvg) / 1714)) * 0.01;
	 * @param pbCalc value in psi
	 * @param flowAvg value in galUS/min
	 * @param spp value in psi
	 * @return return percentHHPb in based value (Number).
	 */
	public static Double calculatePercentHHPb(Double pbCalc, Double flowAvg, Double sppAvg)
	{
		Double percentHHPb = 0.00;
		if (flowAvg != 0 && pbCalc != 0 && sppAvg != 0){
			percentHHPb = ((((flowAvg * pbCalc) / 1714) / ((flowAvg * sppAvg) / 1714)) * 100 ) * 0.01;
		}		
		
		return percentHHPb;		
	}
	
	/**
	 * To calculate Impact Force
	 * Formula When
	 * i.  flowAvg, mudWeight, jetVelocity is not Blank : flowAvg * mudWeight * (jetVelocity * 3.281) / 1930 
	 * @param flowAvg value in galUS/min
	 * @param mudWeight value in ppg
	 * @param jetVelocity in m/s
	 * @return return impactForce in based value (Newton).
	 */
	public static Double calculateImpactForce(Double flowAvg, Double mudWeight, Double jetVelocity)
	{
		Double impactForce = 0.00;
		if (flowAvg != 0 && mudWeight != 0 && jetVelocity != 0){
			impactForce = (flowAvg * mudWeight * (jetVelocity * 3.281) / 1930) * 4.448222;
		}		
		
		return impactForce;		
	}
	/**
	 * To calculate Surface Pressure
	 * Formula: (estimatedMudWeight - mudWeight) * 0.052 * shoeTopTvdMsl 
	 * @param estimatedMudWeight value in pound per square inch (ppg)
	 * @param mudWeight value in pound per square inch (ppg)
	 * @param shoeTopTvdMsl in feet (ft)
	 * @return return surface pressure pound per squre inch (psi).
	 */
	public static Double calculateSurfacePressure(Double estimatedMudWeight, Double mudWeight, Double shoeTopTvdMsl){
		Double surfacePressure = 0.00;
		
		surfacePressure = (estimatedMudWeight - mudWeight) * 0.052 * shoeTopTvdMsl;
		
		return surfacePressure;
	}
	
	/**
	 * To calculate New Impact Force
	 * Formula When
	 * i.  flowAvg, mudWeight, jetVelocity is not Blank : (flowAvg * mudWeight * jetVelocity) / 1930 
	 * @param flowAvg value in galUS/min
	 * @param mudWeight value in ppg
	 * @param jetVelocity in ft/s
	 * @return return impactForce in based value (Newton).
	 */
	public static Double calculateNewImpactForce(Double mudWeight, Double jetVelocity, Double flowAvg)
	{
		Double impactForce = 0.00;
		if (flowAvg != 0 && mudWeight != 0 && jetVelocity != 0){
			impactForce = (flowAvg * mudWeight * jetVelocity) / 1930;
		}		
		
		return impactForce;		
	}
}
