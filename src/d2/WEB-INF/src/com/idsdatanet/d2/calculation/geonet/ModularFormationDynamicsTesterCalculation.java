package com.idsdatanet.d2.calculation.geonet;

/**
 * All Formula/Calculation related to modularFormationDynamicsTester
 * @author Tonney
 *
 */
public class ModularFormationDynamicsTesterCalculation {

	/**
	 * To calculate Formation Pressure Estimated Mud Weight (EMW)
	 * Pressure(psi) / 0.05192 / Depth(ft)
	 * @param pressure value in (psi)
	 * @param depth value in (ft)
	 * @return return Formation Pressure EMW in based value (kg/m3).
	 */
	public static Double calculateFormPressEMW(Double pressure, Double depth) {
		if (depth != null && pressure != null)
			return (pressure/0.05192/depth) * 119.8264;
		else
			return 0.00;
	}
}