package com.idsdatanet.d2.witsml.witsmllog;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WitsmlLogLookupHandler implements LookupHandler {

	private List<String> lookupList = null;
		
	public List<String> getLookupList() {
		return lookupList;
	}

	public void setLookupList(List<String> lookupList) {
		this.lookupList = lookupList;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		if (this.lookupList != null && this.lookupList.size() > 0) {
			for (String capability : this.lookupList) {
				result.put(capability, new LookupItem(capability, capability));
			}
		}
		
		return result;
	}
}