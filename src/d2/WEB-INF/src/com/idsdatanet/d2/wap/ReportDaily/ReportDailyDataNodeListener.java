package com.idsdatanet.d2.wap.ReportDaily;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.DataNodeLoadHandler;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;

public class ReportDailyDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener, DataNodeListener, DataNodeLoadHandler {

	public boolean canHandleLoadingOf(TreeModelDataDefinitionMeta meta) {
		return true;
	}

	public List<Object> loadChildNodes(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, Pagination pagination,
			HttpServletRequest request) throws Exception {
		if (meta.getTableClass().equals(ReportDaily.class)) {
			List<Object> output_maps = new ArrayList<Object>();
			String dailyUid = (String) commandBean.getRoot().getDynaAttr().get("dailyUid");
			String operationUid = (String) commandBean.getRoot().getDynaAttr().get("operationUid");
			if (dailyUid!=null && operationUid!=null) {
				UserSession session = UserSession.getInstance(request);
				_Operation operation = session.getCachedAllAccessibleOperations().get(operationUid);
				if (operation!=null) { 
					ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(session, operation, dailyUid, true);
					if (reportDaily!=null) {
						output_maps.add(reportDaily);
					}
				}
			}
			
			return output_maps;
		}
		return null;
	}

	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
	}

	public void afterDataLoaded(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
		
		Map<String, LookupItem> dailyList = commandBean.getLookupMap(root, "", "@dailyUid");
		if (dailyList!=null) {
			String dailyUid = (String) commandBean.getRoot().getDynaAttr().get("dailyUid");
			if (dailyUid!=null) {
				if (!dailyList.containsKey(dailyUid)) {
					root.getDynaAttr().put("dailyUid", null);
				}
			}
		}
		

	}

	public void afterProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
	}

	public void beforeDataLoad(CommandBean commandBean,
			CommandBeanTreeNode root, UserSelectionSnapshot userSelection,
			HttpServletRequest request) throws Exception {
	}

	public void beforeProcessFormSubmission(CommandBean commandBean,
			HttpServletRequest request) throws Exception {
	}

	public void init(CommandBean commandBean) throws Exception {
	}

	public void onCustomFilterInvoked(HttpServletRequest request,
			HttpServletResponse response, BaseCommandBean commandBean,
			String invocationKey) throws Exception {
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean,
			HttpServletRequest request,
			CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
	}

}
