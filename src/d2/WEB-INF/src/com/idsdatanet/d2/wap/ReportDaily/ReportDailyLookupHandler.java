package com.idsdatanet.d2.wap.ReportDaily;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;

public class ReportDailyLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {

		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		String selectedOperationUid = (String) commandBean.getRoot().getDynaAttr().get("operationUid");
		
		String strSql = "FROM Daily WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid order by dayDate DESC";
		List<Daily> dailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", selectedOperationUid);
		for (Daily rec : dailyList) {
			UserSession session = UserSession.getInstance(request);
			_Operation operation = session.getCachedAllAccessibleOperations().get(selectedOperationUid);
			ReportDaily reportDaily = CommonUtil.getConfiguredInstance().getReportDailyWithPriorityAwareByDailyUid(session, operation, rec.getDailyUid(), true);
			if (reportDaily==null) continue;
			String dayNumber = reportDaily.getReportNumber(); 
			if (dayNumber==null) dayNumber = "-";
			SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy");
			String dayNumberFormat = "#" + dayNumber + " (" +  dateFormatter.format(rec.getDayDate()) + ")";
			result.put(rec.getDailyUid(), new LookupItem(rec.getDailyUid(), dayNumberFormat));
		}
		
		return result;
	}

}
