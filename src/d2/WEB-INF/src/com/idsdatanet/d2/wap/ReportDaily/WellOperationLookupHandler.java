package com.idsdatanet.d2.wap.ReportDaily;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.mvc._Operation;

public class WellOperationLookupHandler implements LookupHandler {

	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
		
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		List<_Operation> lookupList = new ArrayList();
		
		UserSession session = UserSession.getInstance(request);
		session.getSystemSelectionFilter().setQueryFilterUid(null);
		
		Operations accessibleOperations = session.getCachedAllAccessibleOperations();
		for (_Operation operation : accessibleOperations.values()) {
			if (operation.getIsCampaignOverviewOperation()!=null) {
				if (operation.getIsCampaignOverviewOperation()) continue;
			}
			lookupList.add(operation);
		}
		
		//Sorting by operation last date
		Collections.sort(lookupList, new WellOperationLastDateComparator());
		for(Iterator i = lookupList.iterator(); i.hasNext(); ){
			_Operation operation = (_Operation) i.next();
			String operationUid = operation.getOperationUid();
			String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(session.getCurrentGroupUid(), operationUid);
			if (!"".equals(operationName)) {
				LookupItem item = new LookupItem(operationUid, operationName);
				if (!result.containsKey(operationUid)) {
					result.put(operationUid, item);
				}
			}
		}

		return result;
		
	}
	
	private class WellOperationLastDateComparator implements Comparator<_Operation>{
		public int compare(_Operation o1, _Operation o2){
			try{
				Date f1 = o1.getSysOperationLastDatetime();
				Date f2 = o2.getSysOperationLastDatetime();
				
				if (f1 == null) return 1;
				if (f2 == null) return 1;
				return f2.compareTo(f1);
				
			}catch(Exception e){
				e.printStackTrace();
				return 0;
			}
		}

	}
}
