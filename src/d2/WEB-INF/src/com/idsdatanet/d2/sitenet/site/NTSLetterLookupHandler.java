package com.idsdatanet.d2.sitenet.site;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * Lookup handler for generating all possibilities lettering systems in NTS
 * 
 * @author William
 */
public class NTSLetterLookupHandler implements LookupHandler {
	
	// Character to generate to
	private Character endingLetter;
	// Character to start generating from
	private Character startingLetter;

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache cache) throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		int code = (int) startingLetter.charValue();
		
		int startingCode = (int) startingLetter.charValue();
		int endingCode = (int) endingLetter.charValue();
		while (startingCode != (endingCode + 1)){
			
			startingLetter = (char) startingCode;
			LookupItem item= new LookupItem(String.valueOf(startingLetter),String.valueOf(startingLetter));
			result.put(item.getKey(), item);

			startingCode++;
		}
		startingLetter = new Character((char) code);
		
		return result;
	}

	/************************* GETTERS / SETTERS *************************/
	
	public Character getEndingLetter() {
		return endingLetter;
	}

	public void setEndingLetter(Character endingLetter) {
		this.endingLetter = endingLetter;
	}

	public Character getStartingLetter() {
		return startingLetter;
	}

	public void setStartingLetter(Character startingLetter) {
		this.startingLetter = startingLetter;
	}
}
