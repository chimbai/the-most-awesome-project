package com.idsdatanet.d2.sitenet.site;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * Handler for generating a location minute for longitude and latitude
 * 
 * @author William
 */
public class LocationMinuteHandler implements LookupHandler {

	// The value to increment the lookup by
	private Integer incrementValue;
	
	/**
	 * Get a value from 0 to 100 incremented by the defined value
	 */
	public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {

		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		for (int i=0;i < 100;i = i + incrementValue) {
			LookupItem item = new LookupItem(String.valueOf(i),String.valueOf(i));
			result.put(item.getKey(), item);
		}
		return result;
		
	}
	
	/************************* GETTERS / SETTERS *************************/
	
	public Integer getIncrementValue() {
		return incrementValue;
	}

	public void setIncrementValue(Integer incrementValue) {
		this.incrementValue = incrementValue;
	}

}
