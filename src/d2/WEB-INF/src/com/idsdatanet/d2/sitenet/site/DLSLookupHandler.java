package com.idsdatanet.d2.sitenet.site;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * Lookup handler for generating all possibilities for the DLS
 * 
 * @author William
 */
public class DLSLookupHandler implements LookupHandler {
	
	// Total number to generate to values (values generated to totalNumber - 1)
	private int totalNumber;
	// Include padding for numbers
	private Boolean includeLeadingZeros = true;
	// Starting number
	private Integer startingNumber = 1;

	/**
	 * Gets a lookup in the range startingNumber to (totalNumber - 1)
	 */
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache cache) throws Exception {
		Map<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		for (int i=startingNumber;i < totalNumber;i++) {
			LookupItem item;
			if (!includeLeadingZeros){
				item = new LookupItem(String.valueOf(i),String.valueOf(i));
			} else {
				if (i < 10) {
					if (totalNumber > 100){
						item = new LookupItem("00" + i,"00" + i);
					} else {
						item = new LookupItem("0" + i,"0" + i);
					}
				}else if (i < 100){
					if (totalNumber > 100){
						item = new LookupItem("0" + i,"0" + i);
					} else {
						item = new LookupItem(String.valueOf(i),String.valueOf(i));
					}
				} else {
					item = new LookupItem(String.valueOf(i),String.valueOf(i));
				}
			}
			result.put(item.getKey(), item);
		}
		return result;
	}
	
	/************************* GETTERS / SETTERS *************************/
	
	public int getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(int totalNumber) {
		this.totalNumber = totalNumber;
	}

	public Boolean getIncludeLeadingZeros() {
		return includeLeadingZeros;
	}

	public void setIncludeLeadingZeros(Boolean includeLeadingZeros) {
		this.includeLeadingZeros = includeLeadingZeros;
	}

	public Integer getStartingNumber() {
		return startingNumber;
	}

	public void setStartingNumber(Integer startingNumber) {
		this.startingNumber = startingNumber;
	}
	

}
