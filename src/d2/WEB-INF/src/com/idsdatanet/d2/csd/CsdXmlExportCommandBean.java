package com.idsdatanet.d2.csd;

//import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindException;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.report.BeanDataGenerator;
import com.idsdatanet.d2.core.report.ReportDataGenerator;
import com.idsdatanet.d2.core.report.XslJob;
import com.idsdatanet.d2.core.util.xml.SimpleXmlWriter;
import com.idsdatanet.d2.core.util.xml.parser.SimpleXMLElement;
import com.idsdatanet.d2.core.web.mvc.AbstractGenericWebServiceCommandBean;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.report.ReportUtils;
import com.idsdatanet.d2.infra.system.security.DefaultPasswordEncoder;


public class CsdXmlExportCommandBean extends AbstractGenericWebServiceCommandBean {
	
	private BeanDataGenerator generator = null;
	
	public void setDataGenerator(BeanDataGenerator value){
		this.generator = value;
	}	

	private static boolean isUserPasswordMatch(List<User> users, String presentedPassword) {
		if(users.size() > 0) {
			return DefaultPasswordEncoder.isPasswordMatch(presentedPassword, users.get(0).getPassword());
		}else {
			return false;
		}
	}
	
	public void process(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		
		//String requestType = "";
		String requestWellid = "";
		Wellbore currentWell;
		
		try {
			FileItem fileItem = null;
			
			if (request instanceof MultipartHttpServletRequest) {
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				if (!multipartRequest.getFileMap().isEmpty()) {
					fileItem = ((CommonsMultipartFile) multipartRequest.getFileMap().values().iterator().next()).getFileItem();
				}
			} else {
				FileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List items = upload.parseRequest(request);
				
				for (Iterator i = items.iterator(); i.hasNext();) {
					FileItem item = (FileItem) i.next();
					if (!item.isFormField()) {
						fileItem = item;
						break;
					}
				}
			}
			
			if (fileItem == null) {
				throw new Exception("fileItem is null");
			}
			
			List<User> users = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from User where (isDeleted = false or isDeleted is null) and userName = :userName", new String[] {"userName"}, new String[] {request.getParameter("j_username")});
			if (! isUserPasswordMatch(users, request.getParameter("j_password"))) {
				
				//Invalid login
				
				/*SimpleXmlWriter writer = new SimpleXmlWriter(response);
				writer.startElement("response");
				writer.addElement("response", "version", "1.0");
				writer.addElement("success", "false");
				writer.addElement("error-message", "Invalid Username/Password");
				writer.addElement("data", "");
				writer.endElement();
				writer.close();*/
				String loginError = null;
				loginError = "<response version=\"1.0\">\n"
							+ "<success>false</success>\n"
							+ "<error-message>Invalid Username/Password!</error-message>\n"
							+ "<data>\n"
							+ "</data>\n"
							+ "</response>\n";
				//response.getWriter().write(loginError);
				//return;
				throw new UsernameNotFoundException(loginError);
			}
			
			//process the XML attached in the request (to get the request type and well id)
			
			SimpleXMLElement xml = SimpleXMLElement.loadXMLString(fileItem.getString());
			for(Iterator i = xml.getChild().iterator(); i.hasNext(); ){
				SimpleXMLElement csdRequest = (SimpleXMLElement) i.next();
				if (csdRequest.getTagName() == "method") {
					//requestType = csdRequest.getText();
				}
				for (Iterator rowIterator = csdRequest.getChild().iterator(); rowIterator.hasNext();) {
					SimpleXMLElement params = (SimpleXMLElement) rowIterator.next();
					if (params.getTagName() == "ids-well-id") {
						requestWellid = params.getText();
					}
				}
			}
			
			//check the well id requested by CSD
			
			String strSql = "FROM Wellbore WHERE (isDeleted = false or isDeleted is null) AND wellbore_uid =:requestedWell";
			String[] paramField = {"requestedWell"};
			String[] paramValue = {requestWellid};
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramField, paramValue);
			
			if (lstResult.size() > 0) {
				
				// well matched! will process the xml and return back here
			
				String xsl = ReportUtils.getFullXslFilePath("csd_xml_export.xsl");
				ByteArrayOutputStream xml_out = new ByteArrayOutputStream();
				XslJob xslJob = new XslJob(xsl, xml_out, new ReportDataGenerator[] {this.generator}, "XML");
				xslJob.setSaveGeneratedXml(true);
				xslJob.run(UserSelectionSnapshot.getInstanceForCustomWellbore(request.getParameter("j_username"), requestWellid));
				xml_out.close();
				response.getWriter().write(xml_out.toString());
				xml_out = null;
				return;			
			} else {	
				
				//requested well not found! trying to list out the available well(s) for selection
				
				String strListAllWells = "FROM Wellbore WHERE (isDeleted = false or isDeleted is null)";
				List lstAllWells = ApplicationUtils.getConfiguredInstance().getDaoManager().find(strListAllWells);
				if (lstAllWells.size() > 0) {
					
					//well(s) available. Compile a XML and return to CSD.
					
					SimpleXmlWriter writer = new SimpleXmlWriter(response);
					Map<String, Object> responseAttr = new HashMap<String, Object>();
					responseAttr.put("version", "1.0");
					writer.startElement("response", responseAttr);
					writer.addElement("success", "true");
					writer.addElement("error-message", "");
					writer.startElement("data");
					writer.startElement("well-list");
					for (Object objWellbore: lstAllWells) {
						currentWell = (Wellbore) objWellbore;
						Map<String, Object> wellAttr = new HashMap<String, Object>();
						wellAttr.put("name", currentWell.getWellboreName());
						wellAttr.put("ids-id", currentWell.getWellboreUid());
						wellAttr.put("field", "");
						writer.addElement("well", "", wellAttr);
					}
					writer.endAllElements();
					writer.close();
					return;
				} else {
					
					//when no well available in this database... ...
					
					String noWellFound = "<response version=\"1.0\">\n"
										+ "<success>false</success>\n"
										+ "<error-message>No well found from this database.</error-message>\n"
										+ "<data></data>\n"
										+ "</response>\n";
					response.getWriter().write(noWellFound);
					return;
				}
			}
			
			//response.getWriter().write("");

		} catch (Exception e) {
			e.printStackTrace();
			response.getWriter().write(e.getMessage());
		}		
	}
}
