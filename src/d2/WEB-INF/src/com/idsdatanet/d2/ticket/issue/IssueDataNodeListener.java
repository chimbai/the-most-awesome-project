package com.idsdatanet.d2.ticket.issue;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Ticket;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class IssueDataNodeListener extends EmptyDataNodeListener{
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof Ticket) {
			Ticket thisTicket = (Ticket) object;
			Date currentDate =  new Date();
			
			SimpleDateFormat thisDateFormater =  new SimpleDateFormat("yyMMddHHmmss");
			
			thisTicket.setTicketNumber(thisDateFormater.format(currentDate));
		}
	}

}
