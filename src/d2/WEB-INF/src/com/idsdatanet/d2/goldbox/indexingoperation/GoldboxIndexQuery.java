package com.idsdatanet.d2.goldbox.indexingoperation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.GoldboxIndex;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration.OffsetWellType;
import com.idsdatanet.d2.goldbox.model.GoldboxIndexWrapperModel;
import com.idsdatanet.d2.goldbox.model.GoldboxIndexWrapperModel.StatusAggregation;
import com.idsdatanet.d2.goldbox.query.DataQuery;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

public class GoldboxIndexQuery implements DataQuery {

	@Override
	public void beforeProcessQueryResult(JSONObject parameter, Map<String, Object> referencedData) throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean filterData(Object data, Map<String, Object> referencedData) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canHandle(String type) {
		return type.equalsIgnoreCase(OffsetWellType.D2_WELL.getKey());
	}

	@Override
	public JSONObject toJSONObject(Object bean, Class targetClass) throws Exception {
		if(bean instanceof GoldboxIndexWrapperModel && targetClass.equals(GoldboxIndexWrapperModel.class)) {
			JsonConfig jsonConfig = new JsonConfig();
			jsonConfig.setRootClass(GoldboxIndexWrapperModel.class);
			jsonConfig.setExcludes(OffsetWellDashboardConfiguration.getDefaultExcludeFields());
			jsonConfig.registerJsonValueProcessor(StatusAggregation.class, new JsonValueProcessor() {

				@Override
				public Object processArrayValue(Object arg0, JsonConfig arg1) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public Object processObjectValue(String fieldName, Object value, JsonConfig arg2) {
					if(fieldName.equalsIgnoreCase("statusAggregation")) {
						return JSONObject.fromObject(value);
					}
					return null;
				}
				
			});
			JSONObject json = JSONObject.fromObject(bean, jsonConfig);
			return json;
		}
		return null;
	}

	@Override
	public Object toJavaObject(JSONObject json, Class targetClass) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> getQueryResult(JSONObject parameters, Map<String, Object> referencedData) throws Exception {
		String sql = "select a.goldboxIndexUid, a.status, count(a.beanName) from GoldboxAuditTransaction a "
				+ "where (a.isDeleted is null or a.isDeleted = 0) "
				+ "group by a.goldboxIndexUid, a.status "
				+ "order by a.goldboxIndexUid, a.status ";
		
		List<Object[]> aggResult = ApplicationUtils.getConfiguredInstance().getDaoManager().find(sql);
		referencedData.put("goldboxIndexAuditStatus", aggResult);
		
		List<GoldboxIndexWrapperModel> queryResult = null;
		if(parameters != null) {
			String goldboxOffsetWellFilterUid = parameters.optString("goldboxOffsetWellFilterUid");
			queryResult = new ArrayList<GoldboxIndexWrapperModel>();
			List<String> paramName = new ArrayList<String>();
			List<Object> paramValue = new ArrayList<Object>();
			
			if(StringUtils.isNotBlank(goldboxOffsetWellFilterUid) && !goldboxOffsetWellFilterUid.equalsIgnoreCase(OffsetWellDashboardConfiguration.ALL_WELLS_FILTER)) {	
				String hql = "from GoldboxIndex gb, GoldboxOffsetWellSelection gows "
						+ "where (gb.isDeleted is null or gb.isDeleted = 0) "
						+ "and (gows.isDeleted is null or gows.isDeleted = 0) "
						+ "and gb.wellUid = gows.goldboxOffsetWellKey "
						+ "and gows.goldboxOffsetWellType = :offsetWellType "
						+ "and gows.goldboxOffsetWellFilterUid = :goldboxOffsetWellFilterUid ";
				
				paramName.add("goldboxOffsetWellFilterUid");
				paramValue.add(goldboxOffsetWellFilterUid);
				paramName.add("offsetWellType");
				paramValue.add(OffsetWellType.D2_WELL.getKey());
				
				if(parameters.optJSONArray("statusFilter") != null && parameters.optJSONArray("statusFilter").size() > 0) {
					hql += "and gb.status in (:status) ";
					paramName.add("status");
					paramValue.add(JSONArray.toCollection(parameters.optJSONArray("statusFilter"), String.class));
				}

				List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, paramName, paramValue);
				if(result != null && result.size() > 0) {
					for(Object [] rec : result) {
						queryResult.add(this.toGoldboxIndexWrapperModel((GoldboxIndex) rec[0], (List<Object[]>) referencedData.get("goldboxIndexAuditStatus")));
					}
				}
				
			}else if(StringUtils.isNotBlank(goldboxOffsetWellFilterUid) && goldboxOffsetWellFilterUid.equalsIgnoreCase(OffsetWellDashboardConfiguration.ALL_WELLS_FILTER)) {
				String hql = "from GoldboxIndex gb where (gb.isDeleted is null or gb.isDeleted = 0) ";
				if(parameters.optJSONArray("statusFilter") != null && parameters.optJSONArray("statusFilter").size() > 0) {
					hql += "and gb.status in (:status) ";
					paramName.add("status");
					paramValue.add(JSONArray.toCollection(parameters.optJSONArray("statusFilter"), String.class));
				}
				
				List<GoldboxIndex> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, paramName, paramValue);
				if(result != null && result.size() > 0) {
					for(GoldboxIndex rec: result) {
						queryResult.add(this.toGoldboxIndexWrapperModel(rec, (List<Object[]>) referencedData.get("goldboxIndexAuditStatus")));
					}
				}
			}
		}
		
		return queryResult;
	}
	
	private GoldboxIndexWrapperModel toGoldboxIndexWrapperModel(GoldboxIndex gb, List<Object[]> goldboxIndexStatus) throws Exception {
		GoldboxIndexWrapperModel gbwm = new GoldboxIndexWrapperModel();
		PropertyUtils.copyProperties(gbwm, gb);
		gbwm.setGoldboxIndexType(OffsetWellType.D2_WELL.getKey());
		gbwm.setCompleteOperationName(CommonUtil.getConfiguredInstance().getCompleteOperationName(gbwm.getGroupUid(), gbwm.getOperationUid(), null, true));

		//Default to 100
		Integer total = 100;
		//If no audit transaction per bean is created yet, get status from goldbox_index
		String[] labels = new String[] {gbwm.getStatus()};
		//Default 100 to show full pie 
		Integer[] data = new Integer[] {100};
		
		//Set according to audit transaction status if there's any
		if(goldboxIndexStatus != null && goldboxIndexStatus.size() > 0) {
			total = 0;
			
			List<String> auditStatus = new ArrayList<String>();
			List<Integer> auditData = new ArrayList<Integer>();
			
			for(int i=0; i<goldboxIndexStatus.size(); i++) {
				String goldboxIndexUid = goldboxIndexStatus.get(i)[0].toString();
				String status = goldboxIndexStatus.get(i)[1].toString();
				Integer count = Integer.valueOf(goldboxIndexStatus.get(i)[2].toString());
				if(goldboxIndexUid.equalsIgnoreCase(gb.getGoldboxIndexUid())) {
					auditStatus.add(status);
					auditData.add(count);
					total += count;
				}
			}
			labels = auditStatus.toArray(new String[0]);
			data = auditData.toArray(new Integer[0]);
		}

		StatusAggregation sa = gbwm.new StatusAggregation();
		sa.setData(data);
		sa.setLabels(labels);
		sa.setTotal(total);
		gbwm.setStatusAggregation(sa);
		
		return gbwm;
	}
}
