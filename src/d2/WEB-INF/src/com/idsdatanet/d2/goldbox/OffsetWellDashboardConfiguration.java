package com.idsdatanet.d2.goldbox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.depot.goldbox.GoldboxManager;


public class OffsetWellDashboardConfiguration extends DefaultBeanSupport {

	private static List<OffsetWellParameterDefinition> offsetWellParameterConfiguration = null;
	private static String[] defaultExcludeFields = {"isDeleted", "sysDeleted", "groupUid", "lastEditUserUid", "lastEditDatetime", "isSubmit", "recordOwnerUid"};
	public static final String ALL_WELLS_FILTER = "ALL_WELLS_FILTER";
	
	
	public enum OffsetWellType {
		D2_WELL("d2", "Imported Wells"),
		NON_IMPORTED_WELL("nonImported", "Non Imported Wells");
		
		private final String key;
		private final String value;
		
		private OffsetWellType(String key, String value) {
			this.key = key;
			this.value = value;
		}

		public String getKey() {
			return key;
		}

		public String getValue() {
			return value;
		}
	}
	
	public enum GoldboxIndexStatus {
		PENDING(GoldboxManager.GB_PENDING, "Pending"),
		ERROR(GoldboxManager.GB_ERROR, "Error"),
		DONE(GoldboxManager.GB_DONE, "Done"),
		PUBLISHED(GoldboxManager.GB_PUBLISHED, "Published");
		
		private final String key;
		private final String value;
		
		private GoldboxIndexStatus(String key, String value) {
			this.key = key;
			this.value = value;
		}

		public String getKey() {
			return key;
		}

		public String getValue() {
			return value;
		}
	}

	public static String[] getDefaultExcludeFields() {
		return defaultExcludeFields;
	}

	public List<OffsetWellParameterDefinition> getOffsetWellParameterConfiguration() {
		return OffsetWellDashboardConfiguration.offsetWellParameterConfiguration;
	}

	public void setOffsetWellParameterConfiguration(List<OffsetWellParameterDefinition> offsetWellParameterConfiguration) {
		OffsetWellDashboardConfiguration.offsetWellParameterConfiguration = offsetWellParameterConfiguration;
	}
	
	public List<String> getFilterKeys() throws Exception {
		List<String> filterKeys = new ArrayList<String>();
		if(OffsetWellDashboardConfiguration.offsetWellParameterConfiguration != null) {
			for(OffsetWellParameterDefinition d : OffsetWellDashboardConfiguration.offsetWellParameterConfiguration) {
				filterKeys.add(d.getKey());
			}
		}
		return filterKeys;
	}
	
	public Map<String, LookupItem> getLookup(Object definition, UserSelectionSnapshot userSelection) throws Exception{
		
		if(userSelection == null) {
			userSelection = new UserSelectionSnapshot();
		}
		
		Map<String, LookupItem> lookup = new HashMap<String, LookupItem>();
		//TODO support lookuphandler
		if(definition instanceof String) {
			String uri = (String) definition;
			if(StringUtils.isNotBlank(uri)){
				lookup = LookupManager.getConfiguredInstance().getLookup((String)definition, userSelection, null);
			}
		}
		return lookup; 
	}

}
