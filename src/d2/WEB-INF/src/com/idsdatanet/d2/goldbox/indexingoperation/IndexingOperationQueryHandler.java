package com.idsdatanet.d2.goldbox.indexingoperation;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration.OffsetWellType;
import com.idsdatanet.d2.goldbox.model.GoldboxIndexWrapperModel;
import com.idsdatanet.d2.goldbox.query.DataQuery;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class IndexingOperationQueryHandler {

	Map<String, DataQuery> indexingOperationQueries = new HashMap<String, DataQuery>();
	Pagination pagination = null;
	
	
	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
		if(this.pagination != null) {
			this.pagination.normalizeCurrentPageNo();
		}
	}

	public IndexingOperationQueryHandler() {
		this.indexingOperationQueries.put(OffsetWellType.D2_WELL.getKey(), new GoldboxIndexQuery());
		this.indexingOperationQueries.put(OffsetWellType.NON_IMPORTED_WELL.getKey(), new WellExternalCatalogIndexingQuery());
	}
	
	public List<?> getIndexingOperations(JSONObject parameters) throws Exception {
		JSONArray result = new JSONArray();
		String filterByType = null;
		if(parameters.optJSONArray("typeFilter") != null && parameters.optJSONArray("typeFilter").size() > 0) {
			filterByType = parameters.optJSONArray("typeFilter").get(0).toString();
		}

		for(Map.Entry<String, DataQuery> query : this.indexingOperationQueries.entrySet()) {
			DataQuery q = query.getValue();
			HashMap<String, Object> referencedData = new HashMap<String, Object>();
			
			if(StringUtils.isNotBlank(filterByType) && !query.getKey().equalsIgnoreCase(filterByType)) {
				continue;
			}
			
			if(q.canHandle(query.getKey())) {
				List<?> data = q.getQueryResult(parameters, referencedData);
				q.beforeProcessQueryResult(parameters, referencedData);
				if(data != null && data.size() > 0) {
					for(Object rec : data) {
						if(!q.filterData(rec, referencedData)) {
							JSONObject json = q.toJSONObject(rec, GoldboxIndexWrapperModel.class);
							if(json != null) {
								result.add(json);
							}
						}
					}
					Collections.sort(result, new SorByCompleteOperationName());
				}
			}
		}
		
		if(this.pagination != null && this.pagination.isEnabled()) {
			if(StringUtils.isNoneEmpty(parameters.optString("page"))) {
				this.pagination.setCurrentPage(Integer.valueOf(parameters.optString("page"))-1);
			}else {
				this.pagination.setCurrentPage(0);
			}
			
			if(StringUtils.isNoneEmpty(parameters.optString("pageSize"))) {
				this.pagination.setRowsPerPage(Integer.valueOf(parameters.optString("pageSize")));
			}else {
				this.pagination.setRowsPerPage(20);
			}
			
			this.pagination.setTotalRows(result.size());
			this.pagination.normalizeCurrentPageNo();
			
			int startRow = this.pagination.getCurrentPage() * this.pagination.getRowsPerPage();
			if(startRow > result.size()) {
				return result;
			}else if (startRow <= result.size() && (startRow + this.pagination.getRowsPerPage() > result.size())) {
				return result.subList(startRow, result.size());
			}else {
				return result.subList(startRow, (startRow + this.pagination.getRowsPerPage()));
			}
		}else {
			return result;
		}
	}

	
	private class SorByCompleteOperationName implements Comparator<JSONObject> {

		@Override
		public int compare(JSONObject arg0, JSONObject arg1) {
			
			String completeOperationName1 = arg0.optString("completeOperationName");
			String completeOperationName2 = arg1.optString("completeOperationName");
			
			if(StringUtils.isNotBlank(completeOperationName1) && StringUtils.isNotBlank(completeOperationName2)) {
				return completeOperationName1.compareToIgnoreCase(completeOperationName2);
			}

			return 0;
		}
		
	}
}
