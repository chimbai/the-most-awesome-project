package com.idsdatanet.d2.goldbox.offsetwell;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.idsdatanet.d2.core.model.GoldboxIndex;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.WellExternalCatalog;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.goldbox.GoldBoxDashboardUtils;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration.GoldboxIndexStatus;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration.OffsetWellType;
import com.idsdatanet.d2.goldbox.model.OffsetWell;
import com.idsdatanet.d2.goldbox.query.DataQuery;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class WellQuery implements DataQuery {

	@Override
	public boolean canHandle(String type) {
		return type.equals(OffsetWellType.D2_WELL.getKey());
	}
	
	@Override
	public List<?> getQueryResult(JSONObject parameters, Map<String, Object> referencedData) throws Exception {
		List<String> paramName = new ArrayList<String>();
		List<Object> paramValue = new ArrayList<Object>();
		String sql = "From Well where (isDeleted is null or isDeleted = 0) ";
		
		if(parameters != null) {
			JSONArray param = parameters.optJSONArray("country");
			if(param != null && param.size() > 0) {
				sql += "and country in (:country)";
				paramName.add("country");
				paramValue.add(param.toCollection(param, String.class));
			}
			
			param = parameters.optJSONArray("field");
			if(param != null && param.size() > 0) {
				sql += "and field in (:field)";
				paramName.add("field");
				paramValue.add(param.toCollection(param, String.class));
			}
			
			param = parameters.optJSONArray("onOffShore");
			if(param != null && param.size() > 0) {
				sql += "and onOffShore in (:onOffShore)";
				paramName.add("onOffShore");
				paramValue.add(param.toCollection(param, String.class));
			}
			
			String goldboxOffsetWellFilterUid = parameters.optString("goldboxOffsetWellFilterUid");
			if(StringUtils.isNotBlank(goldboxOffsetWellFilterUid) && !goldboxOffsetWellFilterUid.equalsIgnoreCase(OffsetWellDashboardConfiguration.ALL_WELLS_FILTER)) {
				sql += "and wellUid in (select goldboxOffsetWellKey from GoldboxOffsetWellSelection where (isDeleted is null or isDeleted = 0) and goldboxOffsetWellFilterUid = :goldboxOffsetWellFilterUid and goldboxOffsetWellType = :offsetWellType) ";
				paramName.add("goldboxOffsetWellFilterUid");
				paramName.add("offsetWellType");
				paramValue.add(goldboxOffsetWellFilterUid);
				paramValue.add(OffsetWellType.D2_WELL.getKey());
			}
		}
		
		List<Well> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramName, paramValue);
		return result;
	}

	@Override
	public void beforeProcessQueryResult(JSONObject parameter, Map<String, Object> referencedData) throws Exception {
		JSONArray anchorWell = parameter.optJSONArray("anchorWellReferenceKey");
		if(anchorWell != null && anchorWell.size() > 0) {
			referencedData.put("anchorWellInstance", GoldBoxDashboardUtils.getAnchorWellInstance(anchorWell.get(0).toString()));
		}
		referencedData.put("distanceFromAnchorWellInKM", parameter.optJSONArray("distanceFromAnchorWellInKM"));
	}

	@Override
	public boolean filterData(Object data, Map<String, Object> referencedData) throws Exception {
		Object anchorWellInstance = referencedData.get("anchorWellInstance");
		if(anchorWellInstance != null) {
			if(anchorWellInstance instanceof WellExternalCatalog) {
				WellExternalCatalog anchorWell = (WellExternalCatalog) anchorWellInstance;
				Well rec = (Well) data;
				if(anchorWell != null) {
					JSONArray distanceFromAnchorWell = (JSONArray) referencedData.get("distanceFromAnchorWellInKM");
					if(distanceFromAnchorWell != null && distanceFromAnchorWell.size() > 0) {
						Double toKM = Double.valueOf(distanceFromAnchorWell.opt(0).toString());
						Double fromLatitude = GoldBoxDashboardUtils.getLatitudeInDegree(anchorWell.getLatNs(), anchorWell.getLatDeg(), anchorWell.getLatMinute(), anchorWell.getLatSecond());
						Double fromLongitude = GoldBoxDashboardUtils.getLongitudeInDegree(anchorWell.getLongEw(), anchorWell.getLongDeg(), anchorWell.getLongMinute(), anchorWell.getLongSecond());
						Double toLatitude = GoldBoxDashboardUtils.getLatitudeInDegree(rec.getLatNs(), rec.getLatDeg(), rec.getLatMinute(), rec.getLatSecond());
						Double toLongitude = GoldBoxDashboardUtils.getLongitudeInDegree(rec.getLongEw(), rec.getLongDeg(), rec.getLongMinute(), rec.getLongSecond());
						Double distance = GoldBoxDashboardUtils.calculateDistanceInKM(fromLatitude, toLatitude, fromLongitude, toLongitude);
						return !(distance <= toKM);
					}
				}
			}else if(anchorWellInstance instanceof Well) {
				Well anchorWell = (Well) anchorWellInstance;
				Well rec = (Well) data;
				if(anchorWell != null) {
					JSONArray distanceFromAnchorWell = (JSONArray) referencedData.get("distanceFromAnchorWellInKM");
					if(distanceFromAnchorWell != null && distanceFromAnchorWell.size() > 0) {
						Double toKM = Double.valueOf(distanceFromAnchorWell.opt(0).toString());
						Double fromLatitude = GoldBoxDashboardUtils.getLatitudeInDegree(anchorWell.getLatNs(), anchorWell.getLatDeg(), anchorWell.getLatMinute(), anchorWell.getLatSecond());
						Double fromLongitude = GoldBoxDashboardUtils.getLongitudeInDegree(anchorWell.getLongEw(), anchorWell.getLongDeg(), anchorWell.getLongMinute(), anchorWell.getLongSecond());
						Double toLatitude = GoldBoxDashboardUtils.getLatitudeInDegree(rec.getLatNs(), rec.getLatDeg(), rec.getLatMinute(), rec.getLatSecond());
						Double toLongitude = GoldBoxDashboardUtils.getLongitudeInDegree(rec.getLongEw(), rec.getLongDeg(), rec.getLongMinute(), rec.getLongSecond());
						Double distance = GoldBoxDashboardUtils.calculateDistanceInKM(fromLatitude, toLatitude, fromLongitude, toLongitude);
						return !(distance <= toKM);
					}
				}
			}
		}
		return false;
	}
	
	@Override
	public JSONObject toJSONObject(Object bean, Class targetClass) throws Exception {
		JSONObject rec = null;
		try {
			if(bean instanceof Well && targetClass.equals(OffsetWell.class)) {
				rec = new JSONObject();
				Well data = (Well) bean;
				rec.element("offsetWellKey", data.getWellUid());
				rec.element("offsetWellName", data.getWellName());
				rec.element("offsetWellType", OffsetWellType.D2_WELL.getKey());
				rec.element("onOffShore", data.getOnOffShore());
				rec.element("country", data.getCountry());
				rec.element("field", data.getField());
				rec.element("latNs", data.getLatNs());
				rec.element("latDeg", data.getLatDeg());
				rec.element("latMinute", data.getLatMinute());
				rec.element("latSecond", data.getLatSecond());
				rec.element("longEw", data.getLongEw());
				rec.element("longDeg", data.getLongDeg());
				rec.element("longSecond", data.getLongSecond());
				rec.element("longMinute", data.getLongMinute());
				rec.element("latitude", GoldBoxDashboardUtils.getLatitudeInDegree(data.getLatNs(), data.getLatDeg(),  data.getLatMinute(), data.getLatSecond()));
				rec.element("longitude", GoldBoxDashboardUtils.getLongitudeInDegree(data.getLongEw(), data.getLongDeg(), data.getLongMinute(), data.getLongSecond()));
				
				String sql = "from GoldboxIndex gb, Operation o where (gb.isDeleted is null or gb.isDeleted = 0) "
						+ "and (o.isDeleted is null or o.isDeleted = 0) "
						+ "and o.operationUid = gb.operationUid "
						+ "and o.wellUid = :wellUid "
						+ "order by o.sysOperationLastDatetime desc";
				
				List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "wellUid", data.getWellUid());
				if(result != null && result.size() > 0) {
					GoldboxIndex g = (GoldboxIndex) result.get(0)[0];
					rec.element("status", g.getStatus());
				}else {
					rec.element("status", GoldboxIndexStatus.PENDING.getKey());
				}
				
				
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return rec;
	}

	@Override
	public Object toJavaObject(JSONObject json, Class targetClass) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
