package com.idsdatanet.d2.goldbox;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.GoldboxIndex;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.WellExternalCatalog;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.web.menu.MenuManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration.GoldboxIndexStatus;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration.OffsetWellType;
import com.idsdatanet.depot.goldbox.GoldboxManager;

import edu.emory.mathcs.backport.java.util.Collections;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class GoldBoxDashboardUtils {
	
	public static Double getLatitudeInDegree(Object latNs, Object latDeg, Object latMinute, Object latSecond) throws Exception {
		Double latitude = 0.0;
		if(latDeg != null) {
			if(latDeg instanceof Double) {
				latitude += (Double) latDeg;
			}else if(latDeg instanceof String) {
				latitude += Double.valueOf(latDeg.toString());
			}
		}

		if(latMinute != null) {
			if(latMinute instanceof Double) {
				latitude += (Double) latMinute / 60;
			}else if(latMinute instanceof String) {
				latitude += Double.valueOf(latMinute.toString()) / 60;
			}
		}
		if(latSecond != null) {
			if(latSecond instanceof Double) {
				latitude += (Double) latSecond / 3600;
			}else if(latSecond instanceof String) {
				latitude += Double.valueOf(latSecond.toString()) / 3600;
			}
		}
		
		if(latNs != null) {
			if(latNs.toString().equalsIgnoreCase("S")) {
				latitude *= -1;
			}
		}
		return latitude;
	}
	
	public static Double getLongitudeInDegree(Object longEw, Object longDeg, Object longMinute, Object longSecond) throws Exception {
		Double longitude = 0.0;
		if(longDeg != null) {
			if(longDeg instanceof Double) {
				longitude += (Double) longDeg;
			}else if(longDeg instanceof String) {
				longitude += Double.valueOf(longDeg.toString());
			}
		}
		if(longMinute != null) {
			if(longMinute instanceof Double) {
				longitude += (Double) longMinute / 60;
			}else if(longMinute instanceof String) {
				longitude += Double.valueOf(longMinute.toString()) / 3600;
			}
		}
		if(longSecond != null) {
			if(longSecond instanceof Double) {
				longitude += (Double) longSecond / 3600;
			}else if(longSecond instanceof String) {
				longitude += Double.valueOf(longSecond.toString()) / 3600;
			}
		}
		
		if(longEw != null) {
			if(longEw.toString().equalsIgnoreCase("W")) {
				longitude *= -1;
			}
		}
		return longitude;
	}

	public static JSONObject writeLookupOutput (JSONObject writeTo, Map<String, LookupItem> lookup, String fieldName) throws Exception {
		JSONArray items = new JSONArray();
		for(LookupItem item : lookup.values()){
			JSONObject obj = new JSONObject();
			obj.put("id", item.getKey());
			obj.put("text", item.getValue());
			items.add(obj);
		}
		writeTo.put(fieldName, items);
		return writeTo;
	} 
	
	public static JSONObject writeObjectOutput(JSONObject writeTo, Object object, String[] selectiveFields) throws Exception {
		if(selectiveFields != null && selectiveFields.length > 0) {
			for(String fieldName: selectiveFields) {
				writeTo.put(fieldName, PropertyUtils.getProperty(object, fieldName));
			}
		}
		
		return writeTo;
	}
	
	public static String printAsString(Exception e) throws UnsupportedEncodingException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
	    PrintWriter writer = new PrintWriter(out);
	    e.printStackTrace(writer);
	    writer.close();
	    return out.toString("utf-8");
    }
	
	/**
	 * Map is using leaflet library, leaflet distance calculate formula:
	 * Mean Earth Radius, as recommended for use by
	 * the International Union of Geodesy and Geophysics,
	 * see https://rosettacode.org/wiki/Haversine_formula
	 * 
	 * https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
	 * 
	 * @param fromLatitude
	 * @param toLatitude
	 * @param fromLongitude
	 * @param toLongitude
	 * @return
	 * @throws Exception
	 */
	public static Double calculateDistanceInKM(Double fromLatitude, Double toLatitude, Double fromLongitude, Double toLongitude) throws Exception {
		double avgOfEarthRadiusInKM_R = 6371;
		
		double latDistance = Math.toRadians(toLatitude - fromLatitude);
	    double lngDistance = Math.toRadians(toLongitude - fromLongitude);
	    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
	  	      + Math.cos(Math.toRadians(toLatitude)) * Math.cos(Math.toRadians(fromLatitude))
	  	      * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    return new Double(avgOfEarthRadiusInKM_R * c);
	}
	
	public static Object getAnchorWellInstance(String anchorWellReferenceKey) throws Exception {
		if(StringUtils.isNotBlank(anchorWellReferenceKey)) {
			String[] key = anchorWellReferenceKey.split(":");
			
			if(key[0].equalsIgnoreCase(WellExternalCatalog.class.getSimpleName())) {
				String sql = "from WellExternalCatalog where (isDeleted is null or isDeleted = 0) and wellExternalCatalogUid = :wellExternalCatalogUid";
				List<WellExternalCatalog> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"wellExternalCatalogUid"}, new Object[] {key[1]});
				if(result != null && result.size() > 0) {
					return result.get(0);
				}
			}else if(key[0].equalsIgnoreCase(Well.class.getSimpleName())) {
				String sql = "from Well where (isDeleted is null or isDeleted = 0) and wellUid = :wellUid";
				List<Well> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"wellUid"}, new Object[] {key[1]});
				if(result != null && result.size() > 0) {
					return result.get(0);
				}
			}
		}
		return null;
	}
	
	public static void createGoldboxIndexes (String goldboxOffsetWellFilterUid) throws Exception {
		if(StringUtils.isNotBlank(goldboxOffsetWellFilterUid)) {
			String hql = "from Well w, Wellbore wb, Operation o, GoldboxOffsetWellSelection gows "
					+ "where (w.isDeleted is null or w.isDeleted = 0) "
					+ "and (wb.isDeleted is null or wb.isDeleted = 0) "
					+ "and (o.isDeleted is null or o.isDeleted = 0) "
					+ "and (gows.isDeleted is null or gows.isDeleted = 0) "
					+ "and w.wellUid = o.wellUid "
					+ "and wb.wellboreUid = o.wellboreUid "
					+ "and w.wellUid = gows.goldboxOffsetWellKey "
					+ "and gows.goldboxOffsetWellType = :offsetWellType "
					+ "and gows.goldboxOffsetWellFilterUid = :goldboxOffsetWellFilterUid "
					+ "and o.operationUid not in (select gb.operationUid from GoldboxIndex gb where (gb.isDeleted is null or gb.isDeleted = 0))";
			
			//only create goldbox index for D2/imported wells
			List<Object[]> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(hql, new String[] {"goldboxOffsetWellFilterUid", "offsetWellType"}, new Object[] {goldboxOffsetWellFilterUid, OffsetWellType.D2_WELL.getKey()});
			if(result != null && result.size() > 0) {
				for(Object[] rec : result) {
					Well well = (Well) rec[0];
					Wellbore wellbore = (Wellbore) rec[1];
					Operation operation = (Operation) rec[2];
					
					GoldboxIndex gb = new GoldboxIndex();
					gb.setWellUid(well.getWellUid());
					gb.setWellName(well.getWellName());
					gb.setWellboreUid(wellbore.getWellboreUid());
					gb.setWellboreName(wellbore.getWellboreName());
					gb.setOperationUid(operation.getOperationUid());
					gb.setOperationName(operation.getOperationName());
					gb.setStatus(GoldboxIndexStatus.PENDING.getKey());
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(gb);
					GoldboxManager.getConfiguredInstance().indexWellData(gb);
				}
			}
		}
	}
	
	public static String getCompleteExternalWellName(String wellName, String wellboreName, Boolean useOperationNameDelimiter, String customDelimiter) throws Exception {
		
		String delimiter = MenuManager.OPERATION_NAME_DELIMITER_WITH_SPACES;
		if(StringUtils.isNotBlank(customDelimiter)) delimiter = customDelimiter;
		
		String finalName = "";
		
		finalName = wellboreName.trim();
		if (! wellboreName.equals(wellName)) finalName = wellName.trim() + (useOperationNameDelimiter?delimiter:" ") + finalName;	
		
		return finalName;
	}
	
	public static String getGoldboxOffsetWellFilterName(String filterName) throws Exception {
		String validatedFilterName = filterName.trim();
		if(StringUtils.isNotBlank(filterName)) {
			String sql = "select filterName from GoldboxOffsetWellFilter where (isDeleted is null or isDeleted = 0) and filterName = :filterName order by filterName";
			List<String> filterNames = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "filterName", validatedFilterName);
			
			if(filterNames != null && filterNames.size() > 0) {
				sql = "select filterName from GoldboxOffsetWellFilter where (isDeleted is null or isDeleted = 0) and filterName like concat(:filterName, '%') and filterName != :filterName2 order by filterName";
				filterNames = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"filterName", "filterName2"}, new Object[] {validatedFilterName, validatedFilterName});
				//sql = "select filterName from GoldboxOffsetWellFilter where (isDeleted is null or isDeleted = 0) and filterName like concat(:filterName, '%') order by filterName";
				//filterNames = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, filterName, validatedFilterName);
				
				if(filterNames != null && filterNames.size() > 0) {
					String pattern = "Copy$";
					Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
					boolean hasCopy = false;
					for(String name : filterNames) {
						Matcher m = p.matcher(name.trim().toLowerCase());
						if(m.find()) {
							hasCopy = true;
							break;
						}
					}
					if(!hasCopy) {
						validatedFilterName += " - Copy";
					}else {
						pattern = "Copy\\([0-9]+\\)$";
						List<Integer> copies = new ArrayList<Integer>();
						p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
						int copyCount = 1;
						
						for(String name : filterNames) {
							Matcher m = p.matcher(name.trim().toLowerCase());
							if(m.find()) {
								try {
									copies.add(Integer.valueOf(m.group(0).substring(m.group(0).indexOf("(") + 1, m.group(0).indexOf(")"))));
								}catch(IndexOutOfBoundsException e) {
									continue;
								}
							}
						}
						
						if(copies.size() > 0) {
							Collections.sort(copies);
							copyCount = copies.get(copies.size()-1) + 1;
							for(int i=0; i<copies.size(); i++) {
								if(i+1 != copies.get(i).intValue()) {
									copyCount = i + 1;
									break;
								}
							}
						}
						
						validatedFilterName += " - Copy(" + copyCount + ")";
					}
				}else {
					validatedFilterName += " - Copy";
				}
			} 
		}
		return validatedFilterName;
	}
}
