package com.idsdatanet.d2.goldbox.model;

public class OffsetWell {
	
	private String offsetWellKey;
	private String offsetWellName;
	private String offsetWellType;
	private String onOffShore;
	private String country;
	private String field;
	private Double latNs;
	private Double latDeg;
	private Double latMinute;
	private Double latSecond;
	private Double longEw;
	private Double longDeg;
	private Double longSecond;
	private Double longMinute;
	private String status;
	
	
	public OffsetWell() {
	}


	public String getOffsetWellKey() {
		return offsetWellKey;
	}


	public void setOffsetWellKey(String offsetWellKey) {
		this.offsetWellKey = offsetWellKey;
	}


	public String getOffsetWellName() {
		return offsetWellName;
	}


	public void setOffsetWellName(String offsetWellName) {
		this.offsetWellName = offsetWellName;
	}


	public String getOffsetWellType() {
		return offsetWellType;
	}


	public void setOffsetWellType(String offsetWellType) {
		this.offsetWellType = offsetWellType;
	}


	public String getOnOffShore() {
		return onOffShore;
	}


	public void setOnOffShore(String onOffShore) {
		this.onOffShore = onOffShore;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getField() {
		return field;
	}


	public void setField(String field) {
		this.field = field;
	}


	public Double getLatNs() {
		return latNs;
	}


	public void setLatNs(Double latNs) {
		this.latNs = latNs;
	}


	public Double getLatDeg() {
		return latDeg;
	}


	public void setLatDeg(Double latDeg) {
		this.latDeg = latDeg;
	}


	public Double getLatMinute() {
		return latMinute;
	}


	public void setLatMinute(Double latMinute) {
		this.latMinute = latMinute;
	}


	public Double getLatSecond() {
		return latSecond;
	}


	public void setLatSecond(Double latSecond) {
		this.latSecond = latSecond;
	}


	public Double getLongEw() {
		return longEw;
	}


	public void setLongEw(Double longEw) {
		this.longEw = longEw;
	}


	public Double getLongDeg() {
		return longDeg;
	}


	public void setLongDeg(Double longDeg) {
		this.longDeg = longDeg;
	}


	public Double getLongSecond() {
		return longSecond;
	}


	public void setLongSecond(Double longSecond) {
		this.longSecond = longSecond;
	}


	public Double getLongMinute() {
		return longMinute;
	}


	public void setLongMinute(Double longMinute) {
		this.longMinute = longMinute;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
