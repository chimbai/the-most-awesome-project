package com.idsdatanet.d2.goldbox.bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindException;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.GoldboxOffsetWellFilter;
import com.idsdatanet.d2.core.model.GoldboxOffsetWellFilterParams;
import com.idsdatanet.d2.core.model.GoldboxOffsetWellSelection;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.WellExternalCatalog;
import com.idsdatanet.d2.core.web.mvc.AbstractGenericWebServiceCommandBean;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.goldbox.GoldBoxDashboardUtils;
import com.idsdatanet.d2.goldbox.JsonResponse;
import com.idsdatanet.d2.goldbox.JsonResponse.JsonResponseBuilder;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration;
import com.idsdatanet.d2.goldbox.OffsetWellParameterDefinition;
import com.idsdatanet.d2.goldbox.offsetwell.OffsetWellQueryHandler;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

public class OffsetWellDashboardWebServiceCommandBean extends AbstractGenericWebServiceCommandBean {
	
	public static final String METHOD = "method";
	public static final String JSONREQUEST = "jsonRequest";
	public static final String GET_OTHER_FILTER_PARAMETERS = "getOtherFilterParameters";
	public static final String SEARCH_OFFSETWELLS = "searchOffsetWells";
	public static final String GET_OFFSET_WELL_FILTER_QUERIES = "getOffsetWellFilterQueries";
	public static final String GET_SELECTED_OFFSET_WELL_FILTER = "getSelectedOffsetWellFilter";
	public static final String SAVE_NEW_OFFSET_WELL_QUERY = "saveNewOffsetWellQuery";
	public static final String UPDATE_OFFSET_WELL_QUERY = "updateOffsetWellQuery";
	public static final String GET_ANCHOR_WELLS = "getAnchorWells";
	public static final String GET_FILTER_PARAMETER_DEFINITIONS = "getFilterParameterDefinitions";
	public static final String GET_IMPORTED_ANCHOR_WELLS = "getImportedAnchorWells";
	private OffsetWellQueryHandler offsetWellQueryHandler = new OffsetWellQueryHandler();
	private OffsetWellDashboardConfiguration offsetWellDashboardConfig = new OffsetWellDashboardConfiguration();
	
	@Override
	public void process(HttpServletRequest request, HttpServletResponse response, BindException bindError)
			throws Exception {
		String method = request.getParameter(METHOD);
		JSONObject jsonRequest = JSONObject.fromObject(request.getParameter(JSONREQUEST));
		
		if(method == null){
			method = jsonRequest.get(METHOD).toString();
		}
		
		if(GET_OTHER_FILTER_PARAMETERS.equals(method)) {
			this.getOtherFilterParameters(request, response, bindError);
		}else if(SEARCH_OFFSETWELLS.equals(method)) {
			this.searchOffsetWells(jsonRequest, response, bindError);
		}else if(GET_OFFSET_WELL_FILTER_QUERIES.equals(method)) {
			this.getOffsetWellFilterQueries(request, response, bindError);
		}else if(GET_SELECTED_OFFSET_WELL_FILTER.equals(method)) {
			this.getSelectedOffsetWellFilter(request, response, bindError);
		}else if(SAVE_NEW_OFFSET_WELL_QUERY.equals(method)) {
			this.saveNewOffsetWellQuery(jsonRequest, response, bindError);
		}else if(GET_ANCHOR_WELLS.equals(method)) {
			this.getAnchorWells(request, response, bindError);
		}else if(UPDATE_OFFSET_WELL_QUERY.equals(method)) {
			this.updateOffsetWellQuery(jsonRequest, response, bindError);
		}else if(GET_FILTER_PARAMETER_DEFINITIONS.equals(method)) {
			this.getFilterParameterDefinitions(request, response, bindError);
		}else if(GET_IMPORTED_ANCHOR_WELLS.equals(method)) {
			this.getImportedAnchorWells(request, response, bindError);
		}
	}
	
	private void getFilterParameterDefinitions(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		JsonResponseBuilder json = new JsonResponse.JsonResponseBuilder(response);
		try {
			List<OffsetWellParameterDefinition> parameters = offsetWellDashboardConfig.getOffsetWellParameterConfiguration();
			JsonConfig jsonConfig = new JsonConfig();
			jsonConfig.setRootClass(OffsetWellParameterDefinition.class);
			json.result(JSONArray.fromObject(parameters, jsonConfig));
		} catch(Exception e) {
			e.printStackTrace();
			json.error(GoldBoxDashboardUtils.printAsString(e));
		} finally {
			json.build();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getImportedAnchorWells(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		String searchTerm = request.getParameter("term");
		String sql = "from Well where (isDeleted is null or isDeleted = 0)";
		List<String> paramName = new ArrayList<String>();
		List<Object> paramValue = new ArrayList<Object>();
		
		// minimum to set with size = 10. Front end (select2 lookup) has issue with small page size where it doesn't show the scroll bar, 
		// which will not allow to trigger the scrolling for pagination to work
		// https://github.com/select2/select2/issues/3088
		int pageSize = 10;
		int requestPageNo = 1;
		if(StringUtils.isNotBlank(request.getParameter("page"))) {
			try {
				requestPageNo = Integer.parseInt(request.getParameter("page"));
			} catch(Exception e) {
				requestPageNo = 1;
			}
		}
		int startRow = pageSize * (requestPageNo - 1); //page starts from 0
		if(StringUtils.isNotBlank(searchTerm)) {
			searchTerm = searchTerm.trim();
			sql += " and wellName like concat('%', :searchTerm, '%')";
			paramName.add("searchTerm");
			paramValue.add(searchTerm);
		}
		sql += " order by wellName";
		
		QueryProperties qp = new QueryProperties();
		qp.setRowsToFetch(startRow, pageSize);
		List<Well> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramName, paramValue, qp);
		List<Object> totalResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramName, paramValue);
		
		JSONArray out = new JSONArray();
		int total = totalResult.size();
		JsonResponseBuilder json = new JsonResponse.JsonResponseBuilder(response);
		try {
			if(result != null && result.size() > 0) {	
				JsonConfig jsonConfig = new JsonConfig();
				List<String> excludeFields = new ArrayList<String>(Arrays.asList(offsetWellDashboardConfig.getDefaultExcludeFields()));
				jsonConfig.setExcludes(excludeFields.toArray(new String[0]));
				for(Well rec : result) {
					JSONObject data = JSONObject.fromObject(rec, jsonConfig);
					data.put("id", rec.getWellUid());
					data.put("text", rec.getWellName());
					data.put("latitude", GoldBoxDashboardUtils.getLatitudeInDegree(rec.getLatNs(), rec.getLatDeg(), rec.getLatMinute(), rec.getLatSecond()));
					data.put("longitude", GoldBoxDashboardUtils.getLongitudeInDegree(rec.getLongEw(), rec.getLongDeg(), rec.getLongMinute(), rec.getLongSecond()));
					out.add(data);
				}
			}

			json.result(out).pagination(new JSONObject().accumulate("more", (startRow < total && total > pageSize)));

		} catch(Exception e) {
			e.printStackTrace();
			json.error(GoldBoxDashboardUtils.printAsString(e));
		}finally {
			json.build();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getAnchorWells(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		String searchTerm = request.getParameter("term");
		String sql = "from WellExternalCatalog where (isDeleted is null or isDeleted = 0) and (status is null or status != 'IMPORTED') ";
		List<String> paramName = new ArrayList<String>();
		List<Object> paramValue = new ArrayList<Object>();
		
		// minimum to set with size = 10. Front end (select2 lookup) has issue with small page size where it doesn't show the scroll bar, 
		// which will not allow to trigger the scrolling for pagination to work
		// https://github.com/select2/select2/issues/3088
		int pageSize = 10;
		int requestPageNo = 1;
		if(StringUtils.isNotBlank(request.getParameter("page"))) {
			try {
				requestPageNo = Integer.parseInt(request.getParameter("page"));
			} catch(Exception e) {
				requestPageNo = 1;
			}
		}
		int startRow = pageSize * (requestPageNo - 1); //page starts from 0
		if(StringUtils.isNotBlank(searchTerm)) {
			searchTerm = searchTerm.trim();
			sql += " and wellName like concat('%', :searchTerm, '%') or wellboreName like concat('%', :searchTerm, '%')";
			paramName.add("searchTerm");
			paramValue.add(searchTerm);
		}
		sql += " order by wellName";
		
		QueryProperties qp = new QueryProperties();
		qp.setRowsToFetch(startRow, pageSize);
		List<WellExternalCatalog> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramName, paramValue, qp);
		List<Object> totalResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramName, paramValue);
		
		JSONArray out = new JSONArray();
		int total = totalResult.size();
		JsonResponseBuilder json = new JsonResponse.JsonResponseBuilder(response);
		try {
			if(result != null && result.size() > 0) {	
				JsonConfig jsonConfig = new JsonConfig();
				List<String> excludeFields = new ArrayList<String>(Arrays.asList(offsetWellDashboardConfig.getDefaultExcludeFields()));
				excludeFields.add("sourceData");
				jsonConfig.setExcludes(excludeFields.toArray(new String[0]));
				for(WellExternalCatalog rec : result) {
					JSONObject data = JSONObject.fromObject(rec, jsonConfig);
					data.put("id", rec.getWellExternalCatalogUid());
					data.put("text", GoldBoxDashboardUtils.getCompleteExternalWellName(rec.getWellName(), rec.getWellboreName(), true, null));
					data.put("latitude", GoldBoxDashboardUtils.getLatitudeInDegree(rec.getLatNs(), rec.getLatDeg(), rec.getLatMinute(), rec.getLatSecond()));
					data.put("longitude", GoldBoxDashboardUtils.getLongitudeInDegree(rec.getLongEw(), rec.getLongDeg(), rec.getLongMinute(), rec.getLongSecond()));
					out.add(data);
				}
			}

			json.result(out).pagination(new JSONObject().accumulate("more", (startRow < total && total > pageSize)));

		} catch(Exception e) {
			e.printStackTrace();
			json.error(GoldBoxDashboardUtils.printAsString(e));
		}finally {
			json.build();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void updateOffsetWellQuery(JSONObject jsonRequest, HttpServletResponse response, BindException bindError) throws Exception {
		JsonResponseBuilder json = new JsonResponse.JsonResponseBuilder(response);
		try {
			JSONObject filter = JSONObject.fromObject(jsonRequest.opt("filter"));
			if(filter != null) {
				String goldboxOffsetWellFilterUid = filter.optString("goldboxOffsetWellFilterUid");
				if(StringUtils.isNotBlank(goldboxOffsetWellFilterUid)) {
					GoldboxOffsetWellFilter gowf = (GoldboxOffsetWellFilter) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(GoldboxOffsetWellFilter.class, goldboxOffsetWellFilterUid);
					if(!filter.optString("name").equalsIgnoreCase(gowf.getFilterName())) {
						gowf.setFilterName(GoldBoxDashboardUtils.getGoldboxOffsetWellFilterName(filter.optString("name")));
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(gowf);
					}
					
					//delete the old selection
					String sql = "from GoldboxOffsetWellFilterParams where (isDeleted is null or isDeleted = 0) and goldboxOffsetWellFilterUid = :goldboxOffsetWellFilterUid";
					List<GoldboxOffsetWellFilterParams> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "goldboxOffsetWellFilterUid", goldboxOffsetWellFilterUid);
					if(result != null && result.size() > 0) {
						for(GoldboxOffsetWellFilterParams oldParam : result) {
							oldParam.setIsDeleted(true);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(oldParam);
						}
					}

					//delete old well selection
					sql = "from GoldboxOffsetWellSelection where (isDeleted is null or isDeleted = 0) and goldboxOffsetWellFilterUid = :goldboxOffsetWellFilterUid";
					List<GoldboxOffsetWellSelection> selectedWells = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "goldboxOffsetWellFilterUid", goldboxOffsetWellFilterUid);
					if(result != null && result.size() > 0) {
						for(GoldboxOffsetWellSelection oldWells : selectedWells) {
							oldWells.setIsDeleted(true);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(oldWells);
						}
					}
					
					this.saveOffsetWellQuery(goldboxOffsetWellFilterUid, JSONObject.fromObject(jsonRequest.opt("parameters")), jsonRequest.optJSONArray("offsetWells"), json);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			json.error(GoldBoxDashboardUtils.printAsString(e));
		} finally{
			json.build();
		}
	}
	
	private void saveNewOffsetWellQuery(JSONObject jsonRequest, HttpServletResponse response, BindException bindError) throws Exception {
		JsonResponseBuilder json = new JsonResponse.JsonResponseBuilder(response);
		try {
			JSONObject filter = JSONObject.fromObject(jsonRequest.opt("filter"));
			JSONObject out = new JSONObject();
			if(filter != null) {
				GoldboxOffsetWellFilter newFilter = new GoldboxOffsetWellFilter();
				newFilter.setFilterName(GoldBoxDashboardUtils.getGoldboxOffsetWellFilterName(filter.optString("name")));
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newFilter);
				
				this.saveOffsetWellQuery(newFilter.getGoldboxOffsetWellFilterUid()
						, JSONObject.fromObject(jsonRequest.opt("parameters"))
						, jsonRequest.optJSONArray("offsetWells"), json);
				
				out.put("goldboxOffsetWellFilterUid",  newFilter.getGoldboxOffsetWellFilterUid());
				json.result(out);
			}
		} catch(Exception e) {
			e.printStackTrace();
			json.error(GoldBoxDashboardUtils.printAsString(e));
		}finally {
			json.build();
		}
	}
	
	private void saveOffsetWellQuery(String goldboxOffsetWellFilterUid, JSONObject parameters, JSONArray offsetWells, JsonResponseBuilder json) throws Exception {
		try {
			//save the new selection
			if(parameters != null) {
				List<String> filterKeys = this.offsetWellDashboardConfig.getFilterKeys();
				for(String key : filterKeys) {
					JSONArray values = parameters.optJSONArray(key);
					if(values != null) {
						for(Object value : values) {
							GoldboxOffsetWellFilterParams param = new GoldboxOffsetWellFilterParams();
							param.setFilterKey(key);
							param.setFilterValue(value.toString());
							param.setGoldboxOffsetWellFilterUid(goldboxOffsetWellFilterUid);
							ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(param);
						}
					}
				}
			}

			if(offsetWells != null) {
				for(Object offsetWell : offsetWells) {
					GoldboxOffsetWellSelection well = new GoldboxOffsetWellSelection();
					well.setGoldboxOffsetWellFilterUid(goldboxOffsetWellFilterUid);
					well.setGoldboxOffsetWellKey(((JSONObject) offsetWell).getString("offsetWellKey"));
					well.setGoldboxOffsetWellType(((JSONObject) offsetWell).getString("offsetWellType"));
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(well);
				}
			}
			
			//handle create new for now, no update yet
			GoldBoxDashboardUtils.createGoldboxIndexes(goldboxOffsetWellFilterUid);
		} catch(Exception e) {
			e.printStackTrace();
			json.error(GoldBoxDashboardUtils.printAsString(e));
		} 
	}
	
	private void searchOffsetWells (JSONObject jsonRequest, HttpServletResponse response, BindException bindError) throws Exception{
		JsonResponseBuilder json = new JsonResponse.JsonResponseBuilder(response);
		try {
			JSONArray result = this.offsetWellQueryHandler.getOffsetWells(jsonRequest.getJSONObject("parameters"));
			json.result(result);
		} catch (Exception e) {
			e.printStackTrace();
			json.error(GoldBoxDashboardUtils.printAsString(e));
		}finally {
			json.build();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void getOffsetWellFilterQueries (HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception{
		JsonResponseBuilder json = new JsonResponse.JsonResponseBuilder(response);
		try {
			UserSession session = UserSession.getInstance(request);
			JSONArray jsonArray = new JSONArray();
			
			String sql = "from GoldboxOffsetWellFilter where (isDeleted = false or isDeleted is null) and groupUid = :thisGroupUid order by filterName";
			String[] paramFields = { "thisGroupUid"};
			Object[] paramValues = {session.getCurrentGroupUid()};
			List<GoldboxOffsetWellFilter> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramFields, paramValues);
			
			if(results.size() > 0) {
				for (GoldboxOffsetWellFilter filter: results) {
					JSONObject rec = GoldBoxDashboardUtils.writeObjectOutput(new JSONObject(), filter, new String[] {"goldboxOffsetWellFilterUid", "filterName"});
					jsonArray.element(rec);
				}
			}
			
			JSONObject all = new JSONObject();
			all.put("goldboxOffsetWellFilterUid", OffsetWellDashboardConfiguration.ALL_WELLS_FILTER);
			all.put("filterName", "All Wells");
			jsonArray.add(0, all);
			json.result(jsonArray);
			
		} catch (Exception e) {
			e.printStackTrace();
			json.error(GoldBoxDashboardUtils.printAsString(e));
		} finally {
			json.build();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getSelectedOffsetWellFilter (HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception{
		JsonResponseBuilder json = new JsonResponse.JsonResponseBuilder(response);
		try {
			UserSession session = UserSession.getInstance(request);
			JSONObject jsonRequest = JSONObject.fromObject(request.getParameter(JSONREQUEST));
			JSONObject out = new JSONObject();
			String goldboxOffsetWellFilterUid = jsonRequest.optString("goldboxOffsetWellFilterUid");
			
			if(StringUtils.isNotBlank(goldboxOffsetWellFilterUid)) {
				List<String> filterKeys = this.offsetWellDashboardConfig.getFilterKeys();
				out.put("parameters", new JSONObject());
				for(String filterKey : filterKeys) {
					String sql = "select filterValue from GoldboxOffsetWellFilterParams where (isDeleted = false or isDeleted is null) and groupUid = :thisGroupUid "
							+ "and goldboxOffsetWellFilterUid = :goldboxOffsetWellFilterUid and filterKey = :filterKey";
					String[] paramFields = { "thisGroupUid", "goldboxOffsetWellFilterUid", "filterKey"};
					Object[] paramValues = {session.getCurrentGroupUid(), goldboxOffsetWellFilterUid, filterKey};
					List<String> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramFields, paramValues);
					
					if(result != null && result.size() > 0) {
						JSONArray values = new JSONArray();
						for(String filterValue: result) {
							if(filterKey.equalsIgnoreCase("anchorWellReferenceKey")) {
								JSONObject jsonAnchorWell = new JSONObject();
								Object anchorWell = GoldBoxDashboardUtils.getAnchorWellInstance(filterValue);
								JsonConfig jsonConfig = new JsonConfig();
								List<String> excludeFields = new ArrayList<String>(Arrays.asList(offsetWellDashboardConfig.getDefaultExcludeFields()));
								
								//If anchor well exists (it may be deleted), then collect full object
								if(anchorWell != null) {	
									if(anchorWell instanceof WellExternalCatalog) {
										WellExternalCatalog wec = (WellExternalCatalog) anchorWell;
										excludeFields.add("sourceData");
										jsonConfig.setExcludes(excludeFields.toArray(new String[0]));
										jsonAnchorWell = JSONObject.fromObject(anchorWell, jsonConfig);
										jsonAnchorWell.put("className", WellExternalCatalog.class.getSimpleName());
										jsonAnchorWell.put("id", wec.getWellExternalCatalogUid());
										jsonAnchorWell.put("text", GoldBoxDashboardUtils.getCompleteExternalWellName(wec.getWellName(), wec.getWellboreName(), true, null));
										jsonAnchorWell.put("latitude", GoldBoxDashboardUtils.getLatitudeInDegree(wec.getLatNs(), wec.getLatDeg(), wec.getLatMinute(), wec.getLatSecond()));
										jsonAnchorWell.put("longitude", GoldBoxDashboardUtils.getLongitudeInDegree(wec.getLongEw(), wec.getLongDeg(), wec.getLongMinute(), wec.getLongSecond()));
									}else if(anchorWell instanceof Well) {
										Well w = (Well) anchorWell;
										jsonConfig.setExcludes(excludeFields.toArray(new String[0]));
										jsonAnchorWell = JSONObject.fromObject(anchorWell, jsonConfig);
										jsonAnchorWell.put("className", Well.class.getSimpleName());
										jsonAnchorWell.put("id", w.getWellUid());
										jsonAnchorWell.put("text", w.getWellName());
										jsonAnchorWell.put("latitude", GoldBoxDashboardUtils.getLatitudeInDegree(w.getLatNs(), w.getLatDeg(), w.getLatMinute(), w.getLatSecond()));
										jsonAnchorWell.put("longitude", GoldBoxDashboardUtils.getLongitudeInDegree(w.getLongEw(), w.getLongDeg(), w.getLongMinute(), w.getLongSecond()));
									}
								}else {
									jsonAnchorWell.put("className", filterValue.split(":")[0]);
									jsonAnchorWell.put("id", filterValue.split(":")[1]);
								}
								values.add(jsonAnchorWell);
							}else {
								values.add(filterValue);
							}
						}
						
						out.getJSONObject("parameters").put(filterKey, values);
					}
				}
				
				//TODO OffsetWellSelection to get list of offset well interface, consist of different type of wells and join as single list
				JSONArray selectedOffsetWells = this.offsetWellQueryHandler.getOffsetWellByFilterUid(goldboxOffsetWellFilterUid);
				out.put("offsetWells", selectedOffsetWells);
				
			}
			json.result(out);
		} catch (Exception e) {
			e.printStackTrace();
			json.error(GoldBoxDashboardUtils.printAsString(e));
		} finally {
			json.build();
		}
	}
	
	private void getOtherFilterParameters (HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception{
		JsonResponseBuilder json = new JsonResponse.JsonResponseBuilder(response);
		OffsetWellDashboardConfiguration config = new OffsetWellDashboardConfiguration();
		List<OffsetWellParameterDefinition> parameterConfig = config.getOffsetWellParameterConfiguration();
		try {
			if(parameterConfig != null) {
				JSONArray output = new JSONArray();
				for(OffsetWellParameterDefinition f : parameterConfig) {
					if(!f.isHidden()) {
						JSONObject o = new JSONObject();
						o.put("key", f.getKey());
						o.put("label", f.getLabel());
						o.put("type", f.getType());
						
						Object data = f.getData();
						GoldBoxDashboardUtils.writeLookupOutput(o, config.getLookup(data, null), "data");
						output.add(o);
					}
				}
				
				json.result(output);
			}
		} catch(Exception e) {
			e.printStackTrace();
			json.error(GoldBoxDashboardUtils.printAsString(e));
		} finally {
			json.build();
		}
	}
	
	
}
