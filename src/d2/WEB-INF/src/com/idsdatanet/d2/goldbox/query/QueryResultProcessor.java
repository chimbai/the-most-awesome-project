package com.idsdatanet.d2.goldbox.query;

import java.util.Map;

import net.sf.json.JSONObject;

public interface QueryResultProcessor extends BeanProcessor {

	public void beforeProcessQueryResult(JSONObject parameter, Map<String, Object> referencedData) throws Exception;
	boolean filterData(Object data, Map<String, Object> referencedData) throws Exception;
	public boolean canHandle(String type);
	
}
