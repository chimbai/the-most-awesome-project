package com.idsdatanet.d2.goldbox.indexingoperation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.WellExternalCatalog;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.goldbox.GoldBoxDashboardUtils;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration.OffsetWellType;
import com.idsdatanet.d2.goldbox.model.GoldboxIndexWrapperModel;
import com.idsdatanet.d2.goldbox.model.GoldboxIndexWrapperModel.StatusAggregation;
import com.idsdatanet.d2.goldbox.query.DataQuery;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

public class WellExternalCatalogIndexingQuery implements DataQuery {

	@Override
	public void beforeProcessQueryResult(JSONObject parameter, Map<String, Object> referencedData) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean filterData(Object data, Map<String, Object> referencedData) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canHandle(String type) {
		// TODO Auto-generated method stub
		return type.equalsIgnoreCase(OffsetWellType.NON_IMPORTED_WELL.getKey());
	}

	@Override
	public JSONObject toJSONObject(Object bean, Class targetClass) throws Exception {
		if(bean instanceof WellExternalCatalog && targetClass.equals(GoldboxIndexWrapperModel.class)) {
			GoldboxIndexWrapperModel rec = new GoldboxIndexWrapperModel();
			WellExternalCatalog data = (WellExternalCatalog) bean;
			rec.setGoldboxIndexUid(data.getWellExternalCatalogUid());
			rec.setWellUid(data.getWellUid());
			rec.setWellName(data.getWellName());
			rec.setWellboreUid(data.getWellboreUid());
			rec.setWellboreName(data.getWellboreName());
			rec.setStatus(data.getStatus());
			rec.setOperationName(data.getWellName()); 
			rec.setCompleteOperationName(GoldBoxDashboardUtils.getCompleteExternalWellName(data.getWellName(), data.getWellboreName(), true, null)); //no operation yet, set to wellname
// Skip for now
//			rec.setOperationUid("unimported");
//			rec.setMessage("default message");
			StatusAggregation statusAgg = rec.new StatusAggregation();
			statusAgg.setLabels(new String[] {OffsetWellType.NON_IMPORTED_WELL.getValue()}); //TODO clarify which status to use, since it has different status as in goldbox_index table
			statusAgg.setData(new Integer[] {100}); //need to set 100 to show full pie
			statusAgg.setTotal(100);
			rec.setStatusAggregation(statusAgg);
			rec.setGoldboxIndexType(OffsetWellType.NON_IMPORTED_WELL.getKey());
			
			JsonConfig jsonConfig = new JsonConfig();
			jsonConfig.setRootClass(GoldboxIndexWrapperModel.class);
			JSONObject jsonRec = JSONObject.fromObject(rec, jsonConfig);
			return jsonRec;
			
		}
		return null;
	}

	@Override
	public Object toJavaObject(JSONObject json, Class targetClass) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> getQueryResult(JSONObject parameters, Map<String, Object> referencedData) throws Exception {
		List<String> paramName = new ArrayList<String>();
		List<Object> paramValue = new ArrayList<Object>();
		String sql = "From WellExternalCatalog where (isDeleted is null or isDeleted = 0) and (status is null or status != 'IMPORTED') ";
		
		if(parameters != null) {
			String goldboxOffsetWellFilterUid = parameters.optString("goldboxOffsetWellFilterUid");
			
			if(StringUtils.isNotBlank(goldboxOffsetWellFilterUid) && goldboxOffsetWellFilterUid.equalsIgnoreCase(OffsetWellDashboardConfiguration.ALL_WELLS_FILTER)) {
				return null;
			}
			
			if(StringUtils.isNotBlank(goldboxOffsetWellFilterUid)) {
				sql += "and wellExternalCatalogUid in (select goldboxOffsetWellKey from GoldboxOffsetWellSelection where (isDeleted is null or isDeleted = 0) and goldboxOffsetWellFilterUid = :goldboxOffsetWellFilterUid and goldboxOffsetWellType = :offsetWellType) ";
				paramName.add("goldboxOffsetWellFilterUid");
				paramValue.add(goldboxOffsetWellFilterUid);
				paramName.add("offsetWellType");
				paramValue.add(OffsetWellType.NON_IMPORTED_WELL.getKey());
				
				if(parameters.optJSONArray("statusFilter") != null && parameters.optJSONArray("statusFilter").size() > 0) {
					sql += "and status in (:status) ";
					paramName.add("status");
					paramValue.add(JSONArray.toCollection(parameters.optJSONArray("statusFilter"), String.class));
				}
			}
		}
		
		List<WellExternalCatalog> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramName, paramValue);
		return result;
	}

}
