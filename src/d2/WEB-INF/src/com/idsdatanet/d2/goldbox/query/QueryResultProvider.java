package com.idsdatanet.d2.goldbox.query;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

public interface QueryResultProvider {
	public List<?> getQueryResult(JSONObject parameters, Map<String, Object> referencedData) throws Exception;
}
