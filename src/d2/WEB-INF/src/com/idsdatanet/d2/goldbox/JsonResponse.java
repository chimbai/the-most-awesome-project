package com.idsdatanet.d2.goldbox;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSON;
import net.sf.json.JSONObject;

public class JsonResponse {
	
	private final JSON result;
	private final String error;
	private final JSON pagination;
	private final HttpServletResponse response;
	
	private JsonResponse(JsonResponseBuilder builder) {
		this.result = builder.result;
		this.error = builder.error;
		this.pagination = builder.pagination;
		this.response = builder.response;
		try {
			this.writeResponse();
		}catch(IOException e) {
			
		}
	}
	
	private void writeResponse() throws IOException {
		this.response.setContentType("application/json");
		this.response.setCharacterEncoding("utf-8");
		Writer writer = response.getWriter();
		
		JSONObject output = new JSONObject();
		output.element("result", this.result);
		if(this.error != null && !this.error.isEmpty()) {
			output.element("error", new JSONObject().accumulate("message", this.error));
			//this.response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			this.response.sendError(HttpServletResponse.SC_BAD_REQUEST, this.error);
		}
		if(this.pagination != null && !this.pagination.isEmpty()) {
			output.element("pagination", this.pagination);
		}
		output.write(writer);
		writer.close();
	}
	
	
	
	public static class JsonResponseBuilder {
		private JSON result;
		private String error;
		private JSON pagination;
		private HttpServletResponse response;
		
		public JsonResponseBuilder(HttpServletResponse response) {
			this.response = response;
		}
		
		public JsonResponseBuilder error(String error) {
			this.error = error;
			return this;
		}
		
		public JsonResponseBuilder pagination(JSON pagination) {
			this.pagination = pagination;
			return this;
		}
		
		public JsonResponseBuilder result(JSON result) {
			this.result = result;
			return this;
		}
		
		public JsonResponse build() {
			return new JsonResponse(this);
		}
	}
}