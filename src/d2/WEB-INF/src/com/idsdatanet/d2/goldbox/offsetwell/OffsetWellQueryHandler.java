package com.idsdatanet.d2.goldbox.offsetwell;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration.OffsetWellType;
import com.idsdatanet.d2.goldbox.model.OffsetWell;
import com.idsdatanet.d2.goldbox.query.DataQuery;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class OffsetWellQueryHandler {
	
	Map<String, DataQuery> offsetWellQueries = new HashMap<String, DataQuery>();
	
	public OffsetWellQueryHandler() {
		this.offsetWellQueries.put(OffsetWellType.D2_WELL.getKey(), new WellQuery());
		this.offsetWellQueries.put(OffsetWellType.NON_IMPORTED_WELL.getKey(), new WellExternalCatalogQuery());
	}

	public JSONArray getOffsetWells(JSONObject parameters) throws Exception{
		JSONArray result = new JSONArray();
		for(Map.Entry<String, DataQuery> query : this.offsetWellQueries.entrySet()) {
			DataQuery q = query.getValue();
			HashMap<String, Object> referencedData = new HashMap<String, Object>();
			if(q.canHandle(query.getKey())) {
				List<?> data = q.getQueryResult(parameters, referencedData);
				q.beforeProcessQueryResult(parameters, referencedData);
				if(data != null && data.size() > 0) {
					for(Object rec : data) {
						if(!q.filterData(rec, referencedData)) {
							JSONObject json = q.toJSONObject(rec, OffsetWell.class);
							if(json != null) {
								result.add(json);
							}
						}
					}
					Collections.sort(result, new SorByWellName());
				}
			}
		}
		return result;
	}
	
	public JSONArray getOffsetWellByFilterUid(String goldboxOffsetWellFilterUid) throws Exception {
		JSONArray result = new JSONArray();
		JSONObject parameter = new JSONObject();
		parameter.put("goldboxOffsetWellFilterUid", goldboxOffsetWellFilterUid);
		for(Map.Entry<String, DataQuery> query : this.offsetWellQueries.entrySet()) {
			DataQuery q = query.getValue();
			HashMap<String, Object> referencedData = new HashMap<String, Object>();
			List<?> data = q.getQueryResult(parameter, referencedData);
			if(data != null && data.size() > 0) {
				for(Object rec : data) {
					JSONObject json = q.toJSONObject(rec, OffsetWell.class);
					if(json != null) {
						result.add(json);
					}
				}
				Collections.sort(result, new SorByWellName());
			}
		}
		return result;
	}
	
	private class SorByWellName implements Comparator<JSONObject> {

		@Override
		public int compare(JSONObject arg0, JSONObject arg1) {
			
			String wellName1 = arg0.optString("offsetWellName");
			String wellName2 = arg1.optString("offsetWellName");
			
			if(StringUtils.isNotBlank(wellName1) && StringUtils.isNotBlank(wellName2)) {
				return wellName1.compareToIgnoreCase(wellName2);
			}

			return 0;
		}
		
	}
}
