package com.idsdatanet.d2.goldbox.model;

import com.idsdatanet.d2.core.model.GoldboxIndex;

public class GoldboxIndexWrapperModel extends GoldboxIndex {
	
	public class StatusAggregation  {
		private String[] labels;
		private Integer[] data;
		private Integer total;
		
		public String[] getLabels() {
			return labels;
		}
		public void setLabels(String[] labels) {
			this.labels = labels;
		}
		public Integer[] getData() {
			return data;
		}
		public void setData(Integer[] data) {
			this.data = data;
		}
		public Integer getTotal() {
			return total;
		}
		public void setTotal(Integer total) {
			this.total = total;
		}
		
		
	}
	
	private String goldboxIndexType;
	private String completeOperationName;
	private StatusAggregation statusAggregation;
	
	
	
	public String getGoldboxIndexType() {
		return goldboxIndexType;
	}
	public void setGoldboxIndexType(String goldboxIndexType) {
		this.goldboxIndexType = goldboxIndexType;
	}
	public String getCompleteOperationName() {
		return completeOperationName;
	}
	public void setCompleteOperationName(String completeOperationName) {
		this.completeOperationName = completeOperationName;
	}
	public StatusAggregation getStatusAggregation() {
		return statusAggregation;
	}
	public void setStatusAggregation(StatusAggregation statusAggregation) {
		this.statusAggregation = statusAggregation;
	}


	
	
}
