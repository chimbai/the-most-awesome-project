package com.idsdatanet.d2.goldbox.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.WellExternalCatalog;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.Pagination;
import com.idsdatanet.d2.goldbox.GoldBoxDashboardUtils;
import com.idsdatanet.d2.goldbox.JsonResponse;
import com.idsdatanet.d2.goldbox.JsonResponse.JsonResponseBuilder;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration.GoldboxIndexStatus;
import com.idsdatanet.d2.goldbox.OffsetWellDashboardConfiguration.OffsetWellType;
import com.idsdatanet.d2.goldbox.indexingoperation.IndexingOperationQueryHandler;
import com.idsdatanet.d2.goldbox.model.GoldboxIndexWrapperModel;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

public class IndexingOperationQatStatusCommandbean extends BaseCommandBean{
	public static final String JSONREQUEST = "jsonRequest";
	private IndexingOperationQueryHandler indexingOperationQueryHandler = new IndexingOperationQueryHandler();
	private Map<String, LookupItem> offsetWellType = null;
	private Map<String, LookupItem> goldboxIndexStatus = null;
	private Map<String, Object> currentUserSelection = null;
	private Map<String, LookupItem> pageSizeList = null;
	private List data;

	@Override
	public void load(HttpServletRequest request) throws Exception {
		JSONObject jsonRequest = JSONObject.fromObject(request.getParameter(JSONREQUEST));
		if(jsonRequest != null) {
			this.currentUserSelection.put("offsetWellfilterParameters", jsonRequest);
			data = this.indexingOperationQueryHandler.getIndexingOperations(jsonRequest);
		}
	}
	
	public String getJsonData() throws Exception {
		JSONObject json = new JSONObject();
		json.put(GoldboxIndexWrapperModel.class.getSimpleName(), JSONSerializer.toJSON(this.data));
		GoldBoxDashboardUtils.writeLookupOutput(json, this.offsetWellType, "offsetWellType");
		GoldBoxDashboardUtils.writeLookupOutput(json, this.goldboxIndexStatus, "goldboxIndexStatus");
		GoldBoxDashboardUtils.writeLookupOutput(json, this.pageSizeList, "pageSizeList");
		if(this.getPagination().isEnabled()) {
			json.put("pagination", JSONSerializer.toJSON(this.indexingOperationQueryHandler.getPagination()));
		}
		return json.toString();
	}
	
	public String getOffsetWellFilterParameters() throws Exception {
		if( this.currentUserSelection.get("offsetWellfilterParameters") != null) {
			return this.currentUserSelection.get("offsetWellfilterParameters").toString();
		}
		return "";
	}

	public Map<String, LookupItem> getPageSizeList() {
		return pageSizeList;
	}

	public void setPageSizeList(Map<String, LookupItem> pageSizeList) {
		this.pageSizeList = pageSizeList;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.currentUserSelection = new HashMap<String, Object>();
		
		this.offsetWellType = new HashMap<String, LookupItem>();
		this.offsetWellType.put(OffsetWellType.D2_WELL.getKey(), new LookupItem(OffsetWellType.D2_WELL.getKey(), OffsetWellType.D2_WELL.getValue()));
		this.offsetWellType.put(OffsetWellType.NON_IMPORTED_WELL.getKey(), new LookupItem(OffsetWellType.NON_IMPORTED_WELL.getKey(), OffsetWellType.NON_IMPORTED_WELL.getValue()));
		
		this.goldboxIndexStatus = new HashMap<String, LookupItem>();
		this.goldboxIndexStatus.put(GoldboxIndexStatus.DONE.getKey(), new LookupItem(GoldboxIndexStatus.DONE.getKey(), GoldboxIndexStatus.DONE.getValue()));
		this.goldboxIndexStatus.put(GoldboxIndexStatus.ERROR.getKey(), new LookupItem(GoldboxIndexStatus.ERROR.getKey(), GoldboxIndexStatus.ERROR.getValue()));
		this.goldboxIndexStatus.put(GoldboxIndexStatus.PUBLISHED.getKey(), new LookupItem(GoldboxIndexStatus.PUBLISHED.getKey(), GoldboxIndexStatus.PUBLISHED.getValue()));
		this.goldboxIndexStatus.put(GoldboxIndexStatus.PENDING.getKey(), new LookupItem(GoldboxIndexStatus.PENDING.getKey(), GoldboxIndexStatus.PENDING.getValue()));
		
		this.indexingOperationQueryHandler.setPagination(this.getPagination());
	}


	@Override
	public boolean filterRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject jsonRequest = JSONObject.fromObject(request.getParameter(JSONREQUEST));
		String custom_filter_invocation_key = jsonRequest.optString("_invokeCustomFilter"); 
		if(StringUtils.isNotBlank(custom_filter_invocation_key) && custom_filter_invocation_key.equalsIgnoreCase("filterGoldboxIndexOperation")){
			JsonResponseBuilder json = new JsonResponse.JsonResponseBuilder(response);
			try {
				List result = this.indexingOperationQueryHandler.getIndexingOperations(jsonRequest);
				Map out = new HashMap<String, Object>();
				out.put(GoldboxIndexWrapperModel.class.getSimpleName(), result);
				if(this.getPagination().isEnabled()) {
					out.put("pagination", this.indexingOperationQueryHandler.getPagination());
				}
				json.result(JSONSerializer.toJSON(out));
			} catch(Exception e) {
				e.printStackTrace();
				json.error(GoldBoxDashboardUtils.printAsString(e));
			} finally {
				json.build();
			}
			return true;
		}
	
		return super.filterRequest(request, response);
	}
}
