package com.idsdatanet.d2.goldbox.query;

import net.sf.json.JSONObject;

public interface BeanProcessor {
	public JSONObject toJSONObject(Object bean, Class targetClass) throws Exception;
	public Object toJavaObject(JSONObject json, Class targetClass) throws Exception;
}
