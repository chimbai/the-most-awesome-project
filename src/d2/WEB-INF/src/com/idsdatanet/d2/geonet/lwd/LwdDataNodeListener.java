package com.idsdatanet.d2.geonet.lwd;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.Lwd;
import com.idsdatanet.d2.core.model.LwdSuite;
import com.idsdatanet.d2.core.model.LwdTool;
import com.idsdatanet.d2.core.model.LwdToolLookup;
import com.idsdatanet.d2.core.model.LwdWitness;
import com.idsdatanet.d2.core.model.WirelineSuite;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.geonet.wirelineRun.WirelineRunDataNodeListener;

public class LwdDataNodeListener extends EmptyDataNodeListener {

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof Lwd){
			
			Lwd thisLwd = (Lwd) object ;
			
			String bitrunNumber = null;
			String bitDiameter = null;
			Double tfa = 0.0;
			String cutterType = null;
			String bitType = null;
			Double flowMin = 0.0;
			Double flowAvg = 0.0;
			Double flowMax = 0.0;
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Bitrun.class, "tfa");
			DecimalFormat formatter = new DecimalFormat("#0.000000");
			
			String strSql = "FROM Bitrun WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :thisBharunUid";			
			List Result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisBharunUid", thisLwd.getBharunUid());			
			if (Result.size() > 0){
				Bitrun thisBitrun = (Bitrun) Result.get(0);
				if (StringUtils.isNotBlank(thisBitrun.getBitrunNumber())){
					bitrunNumber = thisBitrun.getBitrunNumber();
				}
				
				if (thisBitrun.getBitDiameter() != null) {
					bitDiameter = CommonUtils.roundUpFormat(formatter, thisBitrun.getBitDiameter());
				}
				
				if (thisBitrun.getTfa() != null) {
					//thisConverter.setBaseValueFromUserValue(thisBitrun.getTfa());
					//tfa = thisConverter.getConvertedValue();
					tfa = thisBitrun.getTfa();
				}
				
				if (StringUtils.isNotBlank(thisBitrun.getCutterType())){
					cutterType = thisBitrun.getCutterType();
				}
				if (StringUtils.isNotBlank(thisBitrun.getBitType())){
					bitType = thisBitrun.getBitType();
				}
			}
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@tfa", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("tfa", tfa);
			
			thisConverter.setReferenceMappingField(DrillingParameters.class, "flow_avg");
			
			CommandBeanTreeNode parentNode = node.getParent();
			LwdSuite thisLwdSuite = (LwdSuite) parentNode.getData();
			
			String[] paramsFields = {"thisBharunUid", "thisDailyUid"};
			Object[] paramsValues = new Object[2]; paramsValues[0] = thisLwd.getBharunUid(); paramsValues[1] = thisLwdSuite.getOutDailyUid();
			
			String strSql1 = "FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :thisBharunUid AND dailyUid = :thisDailyUid";			
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields, paramsValues);			
			if (lstResult.size() > 0){
				DrillingParameters thisDP = (DrillingParameters) lstResult.get(0);
				
				if (thisDP.getFlowMin() != null) {
					//thisConverter.setBaseValueFromUserValue(thisDP.getFlowMin());
					//flowMin = thisConverter.getConvertedValue();
					flowMin = thisDP.getFlowMin();
				}
				
				if (thisDP.getFlowAvg()!= null) {
					//thisConverter.setBaseValueFromUserValue(thisDP.getFlowAvg());
					//flowAvg = thisConverter.getConvertedValue();
					flowAvg = thisDP.getFlowAvg();
				}
				
				if (thisDP.getFlowMax()!= null) {
					//thisConverter.setBaseValueFromUserValue(thisDP.getFlowMax());
					//flowMax = thisConverter.getConvertedValue();
					flowMax = thisDP.getFlowMax();
				}
			}
			
			node.getDynaAttr().put("bitrunNumber", bitrunNumber);
			node.getDynaAttr().put("bitDiameter", bitDiameter);
			
			node.getDynaAttr().put("cutterType", cutterType);
			node.getDynaAttr().put("bitType", bitType);
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@flowMin", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("flowMin", flowMin);
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@flowAvg", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("flowAvg", flowAvg);
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@flowMax", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("flowMax", flowMax);
			
			//Load All Mud Data
			LwdUtils.getMudPropertiesInfo(node, thisLwd.getMudPropertiesUid(), userSelection.getGroupUid());
		}
		if (object instanceof LwdWitness){
			
			/*
			Date start = (Date) PropertyUtils.getProperty(object, "startTime");
			Date end = (Date) PropertyUtils.getProperty(object, "endTime");
			
			if(start != null && end != null){
								
				//Long duration = (end.getTime() - start.getTime()) / 1000;
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
				
				//GET UOM FROM Activity.activityDuration
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, LwdWitness.class, "duration");
				thisConverter.setBaseValue(thisDuration);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@duration", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("duration", thisConverter.getConvertedValue());
			}
			*/
			
			//LwdWitness object
			LwdWitness thisLwdWitness = (LwdWitness) object;
			//variable declaration
			String thisOperationUid = thisLwdWitness.getOperationUid();
			Double tvdbrt = 0.0;
			Double mdbrt = 0.0;
			String strSql = "";
			List lstResult = null;
			String surveyReferenceUid = "";
			if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CALC_TVDBRT))) {
				//auto calculate TVD based on MD
				
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);
				qp.setDatumConversionEnabled(true);
				
				//get the MD from screen
				if (thisLwdWitness.getMdMsl() != null ) mdbrt = Double.parseDouble(thisLwdWitness.getMdMsl().toString());
				//get the base value of the MD 
				CustomFieldUom thisConverterField = new CustomFieldUom(commandBean, LwdWitness.class, "mdMsl");
				thisConverterField.setBaseValueFromUserValue(mdbrt);
				thisConverterField.addDatumOffset();
				mdbrt = thisConverterField.getBasevalue();

				//get the survey reference id from the well operation
				strSql = "SELECT surveyReferenceUid FROM SurveyReference WHERE (isDeleted = '' or isDeleted is null) and operationUid = :thisOperationUid";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisOperationUid", thisOperationUid);	
				//when survey record not empty
				if (!lstResult.isEmpty())
				{
					Object thisResult_uid = (Object) lstResult.get(0);
					if (thisResult_uid != null) surveyReferenceUid = thisResult_uid.toString();
					//query statement
					String[] paramsFields = {"thisSurveyReferenceUid", "thisMdbrtMdMsl"};
					Object[] paramsValues = {surveyReferenceUid, mdbrt};
					strSql = "SELECT depthMdMsl, depthTvdMsl, inclinationAngle, azimuthAngle FROM SurveyStation WHERE (isDeleted = '' or isDeleted is null) and surveyReferenceUid = :thisSurveyReferenceUid AND depthMdMsl < :thisMdbrtMdMsl ORDER BY depthMdMsl desc";
					lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);	
					//variable
					Double md1 = 0.0;
					Double tvd1 = 0.0;
					Double inclination1 = 0.0;
					Double corrazimuth1 = 0.0;
					Double inclination0 = 0.0;
					Double corrazimuth0 = 0.0;
					//get the query result from the survey 
					if (!lstResult.isEmpty())
					{		
						Object[] thisResult_md = (Object[]) lstResult.get(0);
						if(thisResult_md != null)
						{	
							if (thisResult_md[0] != null) md1 = Double.parseDouble(thisResult_md[0].toString());
							if (thisResult_md[1] != null) tvd1 = Double.parseDouble(thisResult_md[1].toString());
							if (thisResult_md[2] != null) inclination1 = Double.parseDouble(thisResult_md[2].toString());
							if (thisResult_md[3] != null) corrazimuth1 = Double.parseDouble(thisResult_md[3].toString());
						}

						if (lstResult.size() > 1)
						{
							Object[] thisResult_tvd = (Object[]) lstResult.get(1);
							if(thisResult_tvd != null)
							{
								if (thisResult_tvd[2] != null) inclination0 = Double.parseDouble(thisResult_tvd[2].toString());
								if (thisResult_tvd[3] != null) corrazimuth0 = Double.parseDouble(thisResult_tvd[3].toString());
							}
						}
						

					}
					//variable
					String strSql1 = "";
					List lstResult1 = null;
					Double md2 = 0.0;
					Double inclination2 = 0.0;
					
					String[] paramsFields1 = {"thisSurveyReferenceUid", "thisMdbrtMdMsl"};
					Object[] paramsValues1 = {surveyReferenceUid, mdbrt};
					strSql1 = "SELECT depthMdMsl, depthTvdMsl, inclinationAngle, azimuthAngle FROM SurveyStation WHERE (isDeleted = '' or isDeleted is null) and surveyReferenceUid = :thisSurveyReferenceUid AND depthMdMsl > :thisMdbrtMdMsl ORDER BY depthMdMsl";
					lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1, qp);	
					
					//get the query result 
					if (!lstResult1.isEmpty())
					{	
						Object[] thisResult_md = (Object[]) lstResult1.get(0);
						if(thisResult_md != null)
						{
							if (thisResult_md[0] != null) md2 = Double.parseDouble(thisResult_md[0].toString());
							if (thisResult_md[2] != null)inclination2 = Double.parseDouble(thisResult_md[2].toString());
						}
					}
					//tvdrt calculation
					Double b = 0.0, s = 0.0, DL = 0.0, RF = 0.0, pi = Math.PI;
					
					b = (inclination2 - inclination1)/(md2- md1)*(mdbrt - md1)+ inclination1;
					s = pi / 180;
					DL = Math.acos((Math.cos((inclination1-inclination0)*s))- Math.sin(inclination0*s)*Math.sin(inclination1*s)*(1-Math.cos((corrazimuth1*s)-(corrazimuth0*s))))*180/pi;
					RF = (180/pi)*(2/DL)*Math.tan(DL/2*s);
					tvdbrt =tvd1+(mdbrt-md1)/2*(Math.cos(inclination1*s)+Math.cos(b*s))*RF;
					if (tvdbrt.isNaN()) tvdbrt =0.0;
					
					//set the calculation value to on screen value 
					thisConverterField.setReferenceMappingField(LwdWitness.class, "tvdMsl");
					thisConverterField.setBaseValue(tvdbrt); 
					tvdbrt = thisConverterField.getConvertedValue();
					thisLwdWitness.setTvdMsl(tvdbrt);
					
				}
				
			
			}
		}
		
		if (object instanceof LwdTool) {
			LwdTool lwdTool = (LwdTool)object;
			this.loadEquipmentFailureRecordList(node, lwdTool, userSelection);		
		}

		if(object instanceof LwdSuite) {
			LwdSuite lwdsuite = (LwdSuite) object;
			Daily thisDailyIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(lwdsuite.getInDailyUid());
			Daily thisDailyOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(lwdsuite.getOutDailyUid());
			String bharunUid = lwdsuite.getBharunUid();
			
	    	Long epochIn = null;
	    	Long epochOut = null;
	    	
	    	if(thisDailyIn != null) {
	    		epochIn = Long.valueOf(thisDailyIn.getDayDate().getTime());
	    		node.getDynaAttr().put("dateInEpoch" , epochIn.toString());
	    	}
	    	
	    	if(thisDailyOut != null) {
	    		epochOut = Long.valueOf(thisDailyOut.getDayDate().getTime());
	    		node.getDynaAttr().put("dateOutEpoch" , epochOut.toString());
	    	}
	    	
	    	node.getDynaAttr().put("lwdRunPage" , "lwdrun.html");
	    	
	    	if(bharunUid != null) {
	    		String strSql2 = "SELECT a.dailyUid FROM BharunDailySummary a, Daily b WHERE (a.isDeleted = FALSE OR a.isDeleted IS NULL) AND (b.isDeleted = FALSE OR b.isDeleted IS NULL) AND a.dailyUid = b.dailyUid AND a.bharunUid= :thisBharunUid ORDER BY b.dayDate DESC";
				List<String> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "thisBharunUid", bharunUid, QueryProperties.create().setFetchFirstRowOnly());
				
				if (lstResult.size() > 0) {
					node.getDynaAttr().put("dailyUid", lstResult.get(0).toString());
				}
	    	}
		}
		
	}
	
	private void loadEquipmentFailureRecordList(CommandBeanTreeNode node,
			LwdTool lwdTool, UserSelectionSnapshot userSelection) throws Exception {
		
		Date lwdToolFailureDatetime = lwdTool.getFailureDatetime();
		
		if (lwdToolFailureDatetime==null){
			node.getDynaAttr().put("isEquipmentFailure", "no");
		}
	}

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof LwdTool) {
			LwdTool lwdTool = (LwdTool)object;		
			
			if(lwdTool.getToolDesc() != null)
			{
				LwdToolLookup thisLwdToolLookup = (LwdToolLookup) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LwdToolLookup.class, lwdTool.getToolDesc());
				if(thisLwdToolLookup != null) {
					lwdTool.setToolType(thisLwdToolLookup.getToolCode());
				}
			}			
		}
		if (object instanceof LwdSuite) {
			LwdSuite lwdsuite = (LwdSuite)object;
			
			Daily thisDailyIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(lwdsuite.getInDailyUid());
			Daily thisDailyOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(lwdsuite.getOutDailyUid());
			
			if (thisDailyIn != null && thisDailyOut != null){
				
				if(thisDailyOut.getDayDate().before(thisDailyIn.getDayDate())){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "outDailyUid", "Date Out should be greater than Date In.");
					return;
				}
				
				if(lwdsuite.getDateInTimeIn() != null && lwdsuite.getDateOutTimeOut() != null){
					if(lwdsuite.getDateInTimeIn().getTime() > lwdsuite.getDateOutTimeOut().getTime()){
						if(thisDailyOut.getDayDate().after(thisDailyIn.getDayDate())){
							return;
						} else {
							status.setContinueProcess(false, true);
							status.setFieldError(node, "dateOutTimeOut", "Time Out should be greater than Time In.");
							return;
						}
					}
				}
				
			}
			
			if(lwdsuite.getDepthInMdMsl()!=null && lwdsuite.getDepthOutMdMsl()!=null)
			{
				if(lwdsuite.getDepthInMdMsl() > lwdsuite.getDepthOutMdMsl())
				{
					status.setContinueProcess(false, true);
					status.setFieldError(node, "depthOutMdMsl", "Log End Depth should be greater than Log Start Depth.");
					return;
				}
			}
		}
		if (object instanceof LwdWitness){
			LwdWitness wit = (LwdWitness) object;
			
			
			String thisWitnessDailyUid = wit.getDailyUid();
			if (thisWitnessDailyUid!=null)
			{
				Object objDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, thisWitnessDailyUid);
				if (objDaily!=null)
				{
					Daily daily=(Daily) objDaily;
					if (wit.getStartTime()!=null)
					{
						WirelineRunDataNodeListener.setTimeField(wit, daily.getDayDate(), "startTime");
					}
					if (wit.getEndTime()!=null)
					{
						WirelineRunDataNodeListener.setTimeField(wit, daily.getDayDate(), "endTime");
					}
				}
			}
			Date start = wit.getStartTime();
			Date end = wit.getEndTime();
			
			if(start != null && end != null){
				if(start.getTime() > end.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "startTime", "Start time can not be greater than end time.");
					//node.getDynaAttr().put("duration", "");
					return;
				}
				
				//Long duration = (end.getTime() - start.getTime()) / 1000;
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
				
				//GET UOM FROM Activity.activityDuration
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, LwdWitness.class, "duration");
				thisConverter.setBaseValue(thisDuration);
				PropertyUtils.setProperty(object, "duration", (thisConverter.getConvertedValue()));
				
			}
			else {
				PropertyUtils.setProperty(object, "duration", null);
			}
		}
	}

	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();

		String isEquipmentFailure = (String)node.getDynaAttr().get("isEquipmentFailure");
		
		if (!"no".equalsIgnoreCase(isEquipmentFailure)) {
			if (object instanceof LwdSuite) {
				//update run number and company
				String strInCondition = "";
				LwdSuite lwdSuite = (LwdSuite) object;
				String strSql = "From LwdTool where (isDeleted is null or isDeleted = '') and lwdSuiteUid=:lwdSuiteUid";	
				List<LwdTool>lstLwdTool = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "lwdSuiteUid", lwdSuite.getLwdSuiteUid());
				
				if (lstLwdTool != null && lstLwdTool.size() > 0) {
					
					for (LwdTool rs: lstLwdTool){
						strInCondition = strInCondition + "'" + rs.getLwdToolUid() + "',"; 
					}
				}
				
				strInCondition = StringUtils.substring(strInCondition, 0, -1);
				
				if (StringUtils.isNotBlank(strInCondition)) {
					String strSqlUpdate = "UPDATE EquipmentFailure SET runNumber=:runNumber, company=:companyUid WHERE toolUid IN (" + strInCondition +  ") and toolType='lwd_tool'";	
					String[] paramNames = {"runNumber","companyUid"};
					String[] paramValues = {lwdSuite.getSuiteNum(), lwdSuite.getCompanyUid()};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
				}
			}
			
			if (object instanceof LwdTool){
				LwdTool lwdtool = (LwdTool) object;
				
				String toolDesc = lwdtool.getToolDesc();
				Date failureDateTime = lwdtool.getFailureDatetime();
				String toolUid = lwdtool.getLwdToolUid();

				String strSqlUpdate = "UPDATE EquipmentFailure SET failedItem =:failItem, dateFailStart=:failureDateTime WHERE toolUid =:toolUid and toolType='lwd_tool'";	
				String[] paramNames = {"failItem","failureDateTime", "toolUid"};
				Object[] paramValues = {toolDesc, failureDateTime, toolUid};
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);

				//recalculate cost
				Double dailyCost = null;
				if (session != null) dailyCost = CommonUtil.getConfiguredInstance().getDailyDayCost(failureDateTime, session);
				CommonUtil.getConfiguredInstance().calculateCostIncurred(failureDateTime, dailyCost, "lwd_tool");
				
			}
		}
	}
}
