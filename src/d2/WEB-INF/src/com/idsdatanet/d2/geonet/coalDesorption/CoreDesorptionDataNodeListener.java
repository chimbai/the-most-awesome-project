package com.idsdatanet.d2.geonet.coalDesorption;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.CoalDesorption;
import com.idsdatanet.d2.core.model.CoalDesorptionDetails;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CoreDesorptionDataNodeListener extends EmptyDataNodeListener {

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		
		Object obj = node.getData();
		if(obj instanceof CoalDesorptionDetails){
			CoalDesorptionDetails thisCoalDesorptionDetails = (CoalDesorptionDetails) obj;
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			Double lostGas = 0.00;
			if(thisCoalDesorptionDetails.getLostGas()!=null){
				lostGas = thisCoalDesorptionDetails.getLostGas();
				thisConverter.setReferenceMappingField(CoalDesorptionDetails.class, "lostGas");
				thisConverter.setBaseValueFromUserValue(lostGas);
				lostGas = thisConverter.getBasevalue();
			}
			
			Double desorbed = 0.00;
			if(thisCoalDesorptionDetails.getDesorbed()!=null){
				thisConverter.setReferenceMappingField(CoalDesorptionDetails.class, "desorbed");
				desorbed = thisCoalDesorptionDetails.getDesorbed();
				thisConverter.setBaseValueFromUserValue(desorbed);
				desorbed = thisConverter.getBasevalue();
			}
			
			Double totalGas = null;
			if(lostGas!=null && desorbed!=null){
				totalGas = lostGas + desorbed;
				thisConverter.setReferenceMappingField(CoalDesorptionDetails.class, "totalGas");
				thisConverter.setBaseValue(totalGas);
				thisCoalDesorptionDetails.setTotalGas(thisConverter.getConvertedValue());
			}
		}
	}

	public void afterDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			HttpServletRequest request, int operationPerformed)
			throws Exception {
		
		Object obj = node.getData();
		if(obj instanceof CoalDesorption){
			CoalDesorption thisCoalDesorption = (CoalDesorption) obj;
			String desorptionType = "";
			if(thisCoalDesorption.getDesorptionType()!=null)
				desorptionType = thisCoalDesorption.getDesorptionType();
			
			if(!desorptionType.equalsIgnoreCase("desorption") || StringUtils.isBlank(desorptionType)){
				String coalDesorptionUid = "";
				if(thisCoalDesorption.getCoalDesorptionUid()!=null){
					coalDesorptionUid = thisCoalDesorption.getCoalDesorptionUid();
					
					String sqlCoalDesorptionDetails = "SELECT coalDesorptionDetailsUid FROM CoalDesorptionDetails " +
							"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
							"AND coalDesorptionUid=:coalDesorptionUid";
					List listCoalDesorptionDetails = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlCoalDesorptionDetails, new String[] {"coalDesorptionUid"}, new Object[] {coalDesorptionUid});
					
					String coalDesorptionDetailsUids = "";
					if(listCoalDesorptionDetails.size()>0){
						for(Object coalDesorptionDetailsUid : listCoalDesorptionDetails){
							if(coalDesorptionDetailsUid!=null)
								coalDesorptionDetailsUids += (StringUtils.isNotBlank(coalDesorptionDetailsUids)?",":"") + "'" + coalDesorptionDetailsUid + "'";
						}
						
						String sqlUpdateCoalDesorptionDetails = "UPDATE CoalDesorptionDetails SET isDeleted=1 WHERE coalDesorptionUid=:coalDesorptionUid";
						String[] paramNames = {"coalDesorptionUid"};
						Object[] paramValues = {coalDesorptionUid};
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sqlUpdateCoalDesorptionDetails, paramNames, paramValues);
						
						if(StringUtils.isNotBlank(coalDesorptionDetailsUids) || StringUtils.isNotEmpty(coalDesorptionDetailsUids)){
							String sqlUpdateRawDesorbedGasTest = "UPDATE RawDesorbedGasTest SET isDeleted=1 WHERE coalDesorptionDetailsUid IN (:coalDesorptionDetailsUids)";
							String[] paramNames2 = {"coalDesorptionDetailsUids"};
							Object[] paramValues2 = {coalDesorptionDetailsUids};
							ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sqlUpdateRawDesorbedGasTest, paramNames2, paramValues2);
						}
					}
				}
			}
		}
	}
}
