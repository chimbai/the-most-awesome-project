package com.idsdatanet.d2.geonet.sidewallCore;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.SidewallCore;
import com.idsdatanet.d2.core.model.WirelineRun;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class SidewallCoreDataNodeListener extends EmptyDataNodeListener {
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof SidewallCore) {
			SidewallCore rec = (SidewallCore) object;
			String wirelineRunUid = "";
			if(rec.getWirelineUid()!=null)
			{
				wirelineRunUid = rec.getWirelineUid();
			}
			String lookupCompanyUid = "";
			if (wirelineRunUid!=null) {
				String strSql = "FROM WirelineRun WHERE (isDeleted=false or isDeleted is null) AND wirelineRunUid=:wirelineRunUid";
				List<WirelineRun> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wirelineRunUid", wirelineRunUid);
				if (rs.size()>0) {
					for (WirelineRun run : rs) {
						if (run.getSvcCo()!=null) {
							lookupCompanyUid = run.getSvcCo(); 
							
							if(lookupCompanyUid!=null)
							{
								String strSql1 = "Select companyName FROM LookupCompany WHERE (isDeleted=false or isDeleted is null) AND lookupCompanyUid=:lookupCompanyUid";
								List rs1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, "lookupCompanyUid", lookupCompanyUid);
								if(rs1.size() > 0){
									if(rs1.get(0).toString()!=null)
									{
										node.getDynaAttr().put("wirelineCompany", rs1.get(0).toString());
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof SidewallCore) {
			
			SidewallCore thisSidewallCore = (SidewallCore) object;
			//Check if End Depth Greater than start depth
			if (thisSidewallCore.getStartDepthMdMsl()!=null && thisSidewallCore.getEndDepthMdMsl()!=null) {
				if (thisSidewallCore.getStartDepthMdMsl() > thisSidewallCore.getEndDepthMdMsl()) {
					status.setFieldError(node, "endDepthMdMsl", "End Depth (MD) must greater than Start Depth (MD).");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (thisSidewallCore.getStartDepthTvdMsl()!=null && thisSidewallCore.getEndDepthTvdMsl()!=null) {
				if (thisSidewallCore.getStartDepthTvdMsl() > thisSidewallCore.getEndDepthTvdMsl()) {
					status.setFieldError(node, "endDepthTvdMsl", "End Depth (TVD) must greater than Start Depth (TVD).");
					status.setContinueProcess(false, true);
					return;
				}
			}
		}
	}
}