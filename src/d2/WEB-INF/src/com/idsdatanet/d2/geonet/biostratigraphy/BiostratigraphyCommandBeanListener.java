package com.idsdatanet.d2.geonet.biostratigraphy;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class BiostratigraphyCommandBeanListener extends EmptyCommandBeanListener  {
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN || commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT){
			commandBean.getRoot().getDynaAttr().put("datumLabel", commandBean.getInfo().getCurrentDatumDisplayName());
		}
		
		if (StringUtils.isNotBlank(userSelection.getWellUid())) {
			Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
			if (StringUtils.isBlank(well.getBasin())) {
				commandBean.getSystemMessage().addWarning("No Basin is selected in Well Data");
			}
		}
	}
}
