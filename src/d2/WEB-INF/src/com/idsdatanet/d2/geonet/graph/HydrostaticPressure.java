package com.idsdatanet.d2.geonet.graph;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.graph.BaseGraph;
import com.idsdatanet.d2.core.model.ModularFormationDynamicsTester;
import com.idsdatanet.d2.core.model.ModularFormationDynamicsTesterDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class HydrostaticPressure extends BaseGraph {
	private Map<String, XYDataset> xyDatasetMap   	  = new HashMap<String, XYDataset>();
	private String currentMdtUid					  = null;
	private QueryProperties queryProperties 		  = new QueryProperties();
	private NumberFormat formatter 					  = new DecimalFormat("#.###");
	
	@Override
	public JFreeChart paint() throws Exception {
		this.queryProperties.setUomConversionEnabled(false);
		String groupUid = null;
		Locale locale = null;
		
		if (this.getCurrentHttpRequest()!=null) {
			HttpServletRequest request = this.getCurrentHttpRequest();
			UserSession session = UserSession.getInstance(request);
			groupUid = session.getCurrentGroupUid();
			locale = session.getUserLocale();
			this.formatter = (DecimalFormat) DecimalFormat.getInstance(locale);
			this.currentMdtUid = CommonUtils.null2EmptyString(request.getParameter("uid"));
			if ("".equals(this.currentMdtUid)) {
				Object uid = this.getCurrentUserContext().getUserSelection().getCustomProperty("mdtUid");
				if (uid!=null) this.currentMdtUid = (String) uid;
			}
		} else {
			groupUid = this.getCurrentUserContext().getUserSelection().getGroupUid();
			locale = this.getCurrentUserContext().getUserSelection().getLocale();
			Object uid = (String) this.getCurrentUserContext().getUserSelection().getCustomProperty("mdtUid");
			if (uid!=null) this.currentMdtUid = (String) uid;
		}
		this.setFileName("hydrostatic_pressure_"+this.currentMdtUid+".jpg");
		
		CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale());
		thisConverter.setReferenceMappingField(ModularFormationDynamicsTesterDetail.class, "depthSsTvd");
		String depth = "Depth (" + thisConverter.getUomSymbol() + ")";
		
		thisConverter.setReferenceMappingField(ModularFormationDynamicsTesterDetail.class, "finalPmudPressure");
		String pressure = "Hydrostatic Pressure (" + thisConverter.getUomSymbol() + ")";

		ModularFormationDynamicsTester mdt = (ModularFormationDynamicsTester) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ModularFormationDynamicsTester.class, this.currentMdtUid);
		if (mdt!=null) {
			setTitle(CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, mdt.getOperationUid()));
		}

		XYDataset dataset = createHydrostaticPressureDataset();
		JFreeChart chart = ChartFactory.createScatterPlot(
				getTitle(),      			// chart title
				pressure, 					// x axis label
	            depth,                    	// y axis label
	            dataset,                  	// data
	            PlotOrientation.VERTICAL,
	            false,                     	// include legend
	            false,                     	// tooltips
	            false                     	// urls
			);
		
		// main settings
        XYPlot plot = chart.getXYPlot();
        plot.setOrientation(PlotOrientation.VERTICAL);
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.lightGray);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.getRangeAxis().setFixedDimension(15.0);
        XYItemRenderer renderer = plot.getRenderer();
        renderer.setSeriesPaint(0, Color.blue);
		
        chart.setBackgroundPaint(Color.lightGray);
        chart.addSubtitle(new TextTitle("Hydrostatic Pressure"));
        chart.getXYPlot().getRangeAxis(0).setInverted(true);
        ((NumberAxis) chart.getXYPlot().getRangeAxis()).setNumberFormatOverride(formatter);
        ((NumberAxis) chart.getXYPlot().getDomainAxis()).setNumberFormatOverride(formatter);
	        			
	    return chart;
	}
	
	private XYDataset createHydrostaticPressureDataset() throws Exception {
		XYSeries hydrostaticPressureSeries = new XYSeries("Hydrostatic Pressure");
		String strSql = "FROM ModularFormationDynamicsTesterDetail WHERE (isDeleted=false or isDeleted is null) and modularFormationDynamicsTesterUid=:modularFormationDynamicsTesterUid";
		List<ModularFormationDynamicsTesterDetail> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "modularFormationDynamicsTesterUid", this.currentMdtUid);
		
		for (ModularFormationDynamicsTesterDetail rec:rs) {
			if (rec.getFinalPmudPressure()== null) continue;
			if (rec.getDepthSsTvd()== null) continue;
			hydrostaticPressureSeries.add(rec.getFinalPmudPressure(), rec.getDepthSsTvd());
		}
		
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(hydrostaticPressureSeries);
		return dataset;
	}

	public void dispose() {
		this.xyDatasetMap.clear();
		this.currentMdtUid = null;
	}
}
