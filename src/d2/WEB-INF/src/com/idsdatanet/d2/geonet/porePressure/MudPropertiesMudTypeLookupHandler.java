package com.idsdatanet.d2.geonet.porePressure;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class MudPropertiesMudTypeLookupHandler implements LookupHandler{
	private URI xmlLookup = null;
	
	public void setXmlLookup(URI xmlLookup) {
		this.xmlLookup = xmlLookup;
	}
	
	public URI getXmlLookup() {
		return this.xmlLookup;
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		Map<String, LookupItem> mudtypeLookup = LookupManager.getConfiguredInstance().getLookup(this.getXmlLookup().toString(), userSelection, null);
		
		List<MudProperties> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from MudProperties where (isDeleted = false or isDeleted is null) and dailyUid = :dailyUid", "dailyUid", userSelection.getDailyUid());
		if((list != null && list.size() > 0) && (mudtypeLookup != null)) {
			for(MudProperties mudProperties : list) {
	
				String strMudType = mudProperties.getMudType();
				String strMudPropertiesUid	= mudProperties.getMudPropertiesUid();
				
				if(StringUtils.isNotBlank(strMudType) && mudtypeLookup.containsKey(strMudType)) {
					LookupItem lookupItem = mudtypeLookup.get(strMudType);
					if(lookupItem != null)
					{
						result.put(strMudPropertiesUid, new LookupItem(strMudPropertiesUid, lookupItem.getValue(), lookupItem.getKey()));
					}
				}
			}
		}
		return result;
	}

}
