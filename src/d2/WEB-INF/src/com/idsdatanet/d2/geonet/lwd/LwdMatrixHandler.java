package com.idsdatanet.d2.geonet.lwd;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.idsdatanet.d2.core.model.LwdSuite;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.matrix.DefaultMatrixHandler;
import com.idsdatanet.d2.drillnet.matrix.Matrix;
/**
 * 
 * @author RYAU
 *
 */
public class LwdMatrixHandler extends DefaultMatrixHandler {
	
	public void process(Matrix matrix, UserSelectionSnapshot userSelection) throws Exception {
		boolean f = false;
		try {
			LwdSuite.class.getDeclaredField("isIncomplete");
			f = true;
		} catch (NoSuchFieldException e) {
			f = false;
		}
		String sql = "";
		if(f) {
			sql = "select lw.dailyUid, ls.isIncomplete from LwdSuite ls,  Lwd l, LwdWitness lw where (ls.isDeleted = false or ls.isDeleted is null) and (lw.isDeleted = false or lw.isDeleted is null) and (l.isDeleted = false or l.isDeleted is null) and ls.lwdSuiteUid = l.lwdSuiteUid"
					+ " and l.lwdWitnessUid = lw.lwdWitnessUid and ls.operationUid = :operationUid";
			List<Object[]> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", userSelection.getOperationUid());
			
			if(list != null && list.size() > 0) {
				Set<String> linkedHashSet = new LinkedHashSet<String>();
				for(Object[] daily : list) {
					String dailyUid = null;
					if(daily[0] != null) {
						dailyUid = (String) daily[0];
					}
					linkedHashSet.add(dailyUid);
				}
				for(String id: linkedHashSet) {
					String dailyUid = null;
					String isIncomplete = null;
					for(Object[] daily : list) {
						if(daily[0] != null) {
							dailyUid = (String) daily[0];
							if(id.equals(dailyUid)){
								if(daily[1] != null) {
									if(isIncomplete != "true") isIncomplete = String.valueOf(daily[1]);
								}
							}
						}
					}
					matrix.addData(id,isIncomplete);
				}
			}
		}else{
			sql = "select distinct wl.dailyUid from LwdSuite ls,  Lwd l, LwdWitness lw where (ls.isDeleted = false or ls.isDeleted is null) and (lw.isDeleted = false or lw.isDeleted is null) and (l.isDeleted = false or l.isDeleted is null) and ls.lwdSuiteUid = l.lwdSuiteUid"
					+ " and l.lwdWitnessUid = lw.lwdWitnessUid and ls.operationUid = :operationUid";
			List<String> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "operationUid", userSelection.getOperationUid());
			
			if(list.size() > 0) {
				for(String dailyUid : list) {
					matrix.addData(dailyUid,"N/A");
				}
			}
		}

	}
}
