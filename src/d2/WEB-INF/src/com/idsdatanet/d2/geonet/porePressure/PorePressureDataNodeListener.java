package com.idsdatanet.d2.geonet.porePressure;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.PorePressure;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class PorePressureDataNodeListener extends EmptyDataNodeListener{

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof PorePressure){
			
			PorePressure thisPorePressure = (PorePressure) object;
			
			if (thisPorePressure.getDepthTopTvdMsl()!=null && thisPorePressure.getDepthBottomTvdMsl()!=null) {
				if (thisPorePressure.getDepthTopTvdMsl() > thisPorePressure.getDepthBottomTvdMsl()) {
					status.setFieldError(node, "depthBottomTvdMsl", "Depth To must greater than Depth From.");
					status.setContinueProcess(false, true);
					return;
				}
			}
		}
	}
	

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof PorePressure) {
			PorePressure thisPorePressure = (PorePressure) obj;
			String operationUid = thisPorePressure.getOperationUid();
			String dailyid = thisPorePressure.getDailyUid();
			Daily selectedDaily = ApplicationUtils.getConfiguredInstance().getCachedDaily(dailyid);
			
			if (selectedDaily.getDayDate()!= null && operationUid != null) {
				Double cumMudLossVolume = 0.00;
				
				String[] paramsFields = {"userDate", "thisOperationUid"};
				Object[] paramsValues = {selectedDaily.getDayDate(), operationUid};
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, PorePressure.class, "mudLossVolume");
				
				//query statement for getting cumulative mud loss volume
				String strSql = "SELECT SUM(p.mudLossVolume) FROM Daily d, PorePressure p WHERE (d.isDeleted = false OR d.isDeleted is null) AND (p.isDeleted = false OR p.isDeleted is null) AND p.dailyUid = d.dailyUid AND d.dayDate <= :userDate AND d.operationUid = :thisOperationUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				
				//return query result
				Object ResultMudLossVolume = (Object) lstResult.get(0);
				if (ResultMudLossVolume != null) cumMudLossVolume = Double.parseDouble(ResultMudLossVolume.toString()) ;
				
				//set the base value cumulative mud loss volume to based on mud loss volume fields on screen, 
				//and assign to field @cumMudLossVolume
				thisConverter.setBaseValue(cumMudLossVolume);
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@cumMudLossVolume", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("cumMudLossVolume", thisConverter.getConvertedValue());
			}
		}
	}
}
