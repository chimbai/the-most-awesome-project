package com.idsdatanet.d2.geonet.gasreadings;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.*;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import java.util.Date;

public class GasReadingsDataNodeListener extends EmptyDataNodeListener {
	
	private boolean showPumpOffGasType = false;
	private boolean updateTopMdMsl = false;
	
	public void setShowPumpOffGasType(boolean value){
		this.showPumpOffGasType = value;
	}
	
	public void setUpdateTopMdMsl(boolean value){
		this.updateTopMdMsl = value;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
				
		if (object instanceof GasReadings) {
			GasReadings gasreadings = (GasReadings) object;
			Double gwrFrom = 0.00;
			Double lhrFrom = 0.00;
			Double ocqFrom = 0.00;
			
			Double c1MethaneFrom = 0.00;
			Double c2EthaneFrom = 0.00;
			Double c3PropaneFrom = 0.00;
			Double ic4IsoButaneFrom = 0.00;
			Double nc4NorButaneFrom = 0.00;
			Double ic5SoPethaneFrom = 0.00;

			if(gasreadings.getC1MethaneFrom()!= null) c1MethaneFrom = gasreadings.getC1MethaneFrom();
			if(gasreadings.getC2EthaneFrom() != null) c2EthaneFrom = gasreadings.getC2EthaneFrom();
			if(gasreadings.getC3PropaneFrom() != null) c3PropaneFrom = gasreadings.getC3PropaneFrom();	
			if(gasreadings.getIc4IsoButaneFrom() != null) ic4IsoButaneFrom = gasreadings.getIc4IsoButaneFrom();
			if(gasreadings.getNc4NorButaneFrom() != null) nc4NorButaneFrom = gasreadings.getNc4NorButaneFrom();
			if(gasreadings.getIc5SoPethaneFrom() != null) ic5SoPethaneFrom = gasreadings.getIc5SoPethaneFrom();			

			Double c1MethaneAvg = 0.00;
			Double c1MethaneTo = 0.00;
			Double c2EthaneAvg = 0.00;
			Double c2EthaneTo = 0.00;
			Double c3PropaneAvg = 0.00;
			Double c3PropaneTo = 0.00;
			Double nc4NorButaneAvg = 0.00;
			Double nc4NorButaneTo = 0.00;
			Double ic4IsoButaneAvg = 0.00;
			Double ic4IsoButaneTo = 0.00;
			Double ic5SoPethaneTo = 0.00;
			
			if (gasreadings.getTopMdMsl()!=null && gasreadings.getBottomMdMsl()!=null) {
				if (gasreadings.getTopMdMsl() > gasreadings.getBottomMdMsl()) {
					status.setFieldError(node, "bottomMdMsl", "Depth To must greater than Depth From");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (gasreadings.getC1MethaneAvg()!=null) {
				c1MethaneAvg = gasreadings.getC1MethaneAvg();
				if (gasreadings.getC1MethaneFrom()!=null) {
					if (c1MethaneFrom > c1MethaneAvg) {
						status.setFieldError(node, "c1MethaneAvg", "C1 Average must be greater than C1 Minimum");	
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			if (gasreadings.getC1MethaneTo()!=null) {
				c1MethaneTo = gasreadings.getC1MethaneTo();
				if (gasreadings.getC1MethaneAvg()!=null) {
					if (c1MethaneAvg > c1MethaneTo) {
						status.setFieldError(node, "c1MethaneTo", "C1 Maximum must be greater than C1 Average");	
						status.setContinueProcess(false, true);
						return;
					}
				} else if (gasreadings.getC1MethaneFrom()!=null) {
					if (c1MethaneFrom > c1MethaneTo) {
						status.setFieldError(node, "c1MethaneTo", "C1 Maximum must be greater than C1 Minimum");	
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			
			if (gasreadings.getC2EthaneAvg()!=null) {
				c2EthaneAvg = gasreadings.getC2EthaneAvg();
				if (gasreadings.getC2EthaneFrom()!=null) {
					if (c2EthaneFrom > c2EthaneAvg) {
						status.setFieldError(node, "c2EthaneAvg", "C2 Average must be greater than C2 Minimum");	
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			if (gasreadings.getC2EthaneTo()!=null) {
				c2EthaneTo = gasreadings.getC2EthaneTo();
				if (gasreadings.getC2EthaneAvg()!=null) {
					if (c2EthaneAvg > c2EthaneTo) {
						status.setFieldError(node, "c2EthaneTo", "C2 Maximum must be greater than C2 Average");	
						status.setContinueProcess(false, true);
						return;
					}
				} else if (gasreadings.getC2EthaneFrom()!=null) {
					if (c2EthaneFrom > c2EthaneTo) {
						status.setFieldError(node, "c2EthaneTo", "C2 Maximum must be greater than C2 Minimum");	
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			if (gasreadings.getC3PropaneAvg()!=null) {
				c3PropaneAvg = gasreadings.getC3PropaneAvg();
				if (gasreadings.getC3PropaneFrom()!=null) {
					if (c3PropaneFrom > c3PropaneAvg) {
						status.setFieldError(node, "c3PropaneAvg", "C3 Average must be greater than C3 Minimum");	
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			if (gasreadings.getC3PropaneTo()!=null) {
				c3PropaneTo = gasreadings.getC3PropaneTo();
				if (gasreadings.getC3PropaneAvg()!=null) {
					if (c3PropaneAvg > c3PropaneTo) {
						status.setFieldError(node, "c3PropaneTo", "C3 Maximum must be greater than C3 Average");	
						status.setContinueProcess(false, true);
						return;
					}
				} else if (gasreadings.getC3PropaneFrom()!=null) {
					if (c3PropaneFrom > c3PropaneTo) {
						status.setFieldError(node, "c3PropaneTo", "C3 Maximum must be greater than C3 Minimum");	
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			if (gasreadings.getNc4NorButaneAvg()!=null) {
				nc4NorButaneAvg = gasreadings.getNc4NorButaneAvg();
				if (gasreadings.getNc4NorButaneFrom()!=null) {
					if (nc4NorButaneFrom > nc4NorButaneAvg) {
						status.setFieldError(node, "nc4NorButaneAvg", "NC4 Average must be greater than NC4 Minimum");	
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			if (gasreadings.getNc4NorButaneTo()!=null) {
				nc4NorButaneTo = gasreadings.getNc4NorButaneTo();
				if (gasreadings.getNc4NorButaneAvg()!=null) {
					if (nc4NorButaneAvg > nc4NorButaneTo) {
						status.setFieldError(node, "nc4NorButaneTo", "NC4 Maximum must be greater than NC4 Average");	
						status.setContinueProcess(false, true);
						return;
					}
				} else if (gasreadings.getNc4NorButaneFrom()!=null) {
					if (nc4NorButaneFrom > nc4NorButaneTo) {
						status.setFieldError(node, "nc4NorButaneTo", "NC4 Maximum must be greater than NC4 Minimum");	
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			if (gasreadings.getIc4IsoButaneAvg()!=null) {
				ic4IsoButaneAvg = gasreadings.getIc4IsoButaneAvg();
				if (gasreadings.getIc4IsoButaneFrom()!=null) {
					if (ic4IsoButaneFrom > ic4IsoButaneAvg) {
						status.setFieldError(node, "ic4IsoButaneAvg", "IC4 Average must be greater than IC4 Minimum");	
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			if (gasreadings.getIc4IsoButaneTo()!=null) {
				ic4IsoButaneTo = gasreadings.getIc4IsoButaneTo();
				if (gasreadings.getIc4IsoButaneAvg()!=null) {
					if (ic4IsoButaneAvg > ic4IsoButaneTo) {
						status.setFieldError(node, "ic4IsoButaneTo", "IC4 Maximum must be greater than IC4 Average");	
						status.setContinueProcess(false, true);
						return;
					}
				} else if (gasreadings.getIc4IsoButaneFrom()!=null) {
					if (ic4IsoButaneFrom > ic4IsoButaneTo) {
						status.setFieldError(node, "ic4IsoButaneTo", "IC4 Maximum must be greater than IC4 Minimum");	
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			if (gasreadings.getIc5SoPethaneTo()!=null) {
				ic5SoPethaneTo = gasreadings.getIc5SoPethaneTo();
				if (gasreadings.getIc5SoPethaneFrom()!=null) {
					if (ic5SoPethaneFrom > ic5SoPethaneTo) {
						status.setFieldError(node, "ic5SoPethaneTo", "IC5 Maximum must be greater than IC5 Minimum");	
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			Double c4_c5_from = 0.00;
			c4_c5_from = ic4IsoButaneFrom + nc4NorButaneFrom + ic5SoPethaneFrom;
			
			if(c4_c5_from > 0)
			{
				gwrFrom = ((c2EthaneFrom + c3PropaneFrom + c4_c5_from) / (c1MethaneFrom + c2EthaneFrom + c3PropaneFrom + c4_c5_from)) * 100;
				lhrFrom = (c1MethaneFrom + c2EthaneFrom) / (c3PropaneFrom + c4_c5_from);				
				
				if(c3PropaneFrom > 0)
				{
					ocqFrom = c4_c5_from / c3PropaneFrom;
				}
				else
				{
					ocqFrom = 0.00;
				}
			}
			
			gasreadings.setGwrFrom(gwrFrom);
			gasreadings.setLhrFrom(lhrFrom);
			gasreadings.setOcqFrom(ocqFrom);
			
			String[] datePattern = {"yyyy-MM-dd"};
			Date defaultDate = DateUtils.parseDate("1970-01-01", datePattern);
			Date defaultDateTime = DateUtils.addHours(defaultDate, 0);
			
			//If no time input by client default to 00:00
			if(gasreadings.getReadingTime()==null){		
				gasreadings.setReadingTime(defaultDateTime);
			}
			
			if (gasreadings.getTotalfrom()!=null && gasreadings.getAvgGasConcentration()!=null) {
				if (gasreadings.getTotalfrom() > gasreadings.getAvgGasConcentration() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_GAS_CONCENTRATION))) {
					status.setFieldError(node, "avgGasConcentration", "Flow Avg must be greater than Flow Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (gasreadings.getTotalfrom()!=null && gasreadings.getTotalto()!=null) {
				if (gasreadings.getTotalfrom() > gasreadings.getTotalto()) {
					status.setFieldError(node, "totalto", "Flow Max must be greater than Flow Min.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			if (gasreadings.getAvgGasConcentration()!=null && gasreadings.getTotalto()!=null) {
				if (gasreadings.getAvgGasConcentration() > gasreadings.getTotalto() && !"1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_GAS_CONCENTRATION))) {
					status.setFieldError(node, "totalto", "Flow Max must be greater than Flow Avg.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_GAS_CONCENTRATION))){
				//auto calculate the average
				if (gasreadings.getTotalto() != null && gasreadings.getTotalfrom() != null) gasreadings.setAvgGasConcentration((gasreadings.getTotalto() + gasreadings.getTotalfrom())/2);
				else 
				{
					if (gasreadings.getTotalto() != null)gasreadings.setAvgGasConcentration(gasreadings.getTotalto());
					else if (gasreadings.getTotalfrom() != null)gasreadings.setAvgGasConcentration(gasreadings.getTotalfrom());
					else gasreadings.setAvgGasConcentration(null);
				}
			}
			
			if (showPumpOffGasType){
				if ("Peak".equals(gasreadings.getGastype())) {
					if (StringUtils.isBlank(gasreadings.getPumpOffGasType())) {
						status.setFieldError(node, "pumpOffGasType", "This field is required");
						status.setContinueProcess(false, true);
						return;
					}
				}
			}
			
			if(updateTopMdMsl) {
				if(gasreadings.getLaggedDepthMdMsl() != null) {
					gasreadings.setTopMdMsl(gasreadings.getLaggedDepthMdMsl());
				}
			}
			
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();
		
		if (object instanceof GasReadings) {
			GasReadings gasreadings = (GasReadings) object;
			
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_AVERAGE_GAS_READING))){
				//auto calculate the average
				if (gasreadings.getC1MethaneFrom() != null && gasreadings.getC1MethaneTo() == null) {
					gasreadings.setC1MethaneAvg(gasreadings.getC1MethaneFrom());
				}else if(gasreadings.getC1MethaneFrom() == null && gasreadings.getC1MethaneTo() != null) {
					gasreadings.setC1MethaneAvg(gasreadings.getC1MethaneTo());
				}else if(gasreadings.getC1MethaneFrom() != null && gasreadings.getC1MethaneTo() != null) {
					gasreadings.setC1MethaneAvg((gasreadings.getC1MethaneFrom() + gasreadings.getC1MethaneTo())/2);
				}
				
				if(gasreadings.getC2EthaneFrom() != null && gasreadings.getC2EthaneTo() == null) {
					gasreadings.setC2EthaneAvg(gasreadings.getC2EthaneFrom());
				}else if(gasreadings.getC2EthaneFrom() == null && gasreadings.getC2EthaneTo() != null) {
					gasreadings.setC2EthaneAvg(gasreadings.getC2EthaneTo());
				}else if(gasreadings.getC2EthaneFrom() != null && gasreadings.getC2EthaneTo() != null) {
					gasreadings.setC2EthaneAvg((gasreadings.getC2EthaneFrom() + gasreadings.getC2EthaneTo())/2);
				}
				
				if(gasreadings.getC3PropaneFrom() != null && gasreadings.getC3PropaneTo() == null) {
					gasreadings.setC3PropaneAvg(gasreadings.getC3PropaneFrom());
				}else if(gasreadings.getC3PropaneFrom() == null && gasreadings.getC3PropaneTo() != null) {
					gasreadings.setC3PropaneAvg(gasreadings.getC3PropaneTo());
				}else if(gasreadings.getC3PropaneFrom() != null && gasreadings.getC3PropaneTo() != null) {
					gasreadings.setC3PropaneAvg((gasreadings.getC3PropaneFrom() + gasreadings.getC3PropaneTo())/2);
				}
				
				if(gasreadings.getIc4IsoButaneFrom() != null && gasreadings.getIc4IsoButaneTo()== null) {
					gasreadings.setIc4IsoButaneAvg(gasreadings.getIc4IsoButaneFrom());
				}else if(gasreadings.getIc4IsoButaneFrom() == null && gasreadings.getIc4IsoButaneTo() != null) {
					gasreadings.setIc4IsoButaneAvg(gasreadings.getIc4IsoButaneTo());
				}else if(gasreadings.getIc4IsoButaneFrom() != null && gasreadings.getIc4IsoButaneTo() != null) {
					gasreadings.setIc4IsoButaneAvg((gasreadings.getIc4IsoButaneFrom() + gasreadings.getIc4IsoButaneTo())/2);
				}
				
				if(gasreadings.getNc4NorButaneFrom() != null && gasreadings.getNc4NorButaneTo() == null) {
					gasreadings.setNc4NorButaneAvg(gasreadings.getNc4NorButaneFrom());
				}else if(gasreadings.getNc4NorButaneFrom() == null && gasreadings.getNc4NorButaneTo() != null) {
					gasreadings.setNc4NorButaneAvg(gasreadings.getNc4NorButaneTo());
				}else if(gasreadings.getNc4NorButaneFrom() != null && gasreadings.getNc4NorButaneTo() != null){
					gasreadings.setNc4NorButaneAvg((gasreadings.getNc4NorButaneFrom() + gasreadings.getNc4NorButaneTo())/2);
				}
				
				if(gasreadings.getIc5SoPethaneFrom() != null && gasreadings.getIc5SoPethaneTo() == null) {
					gasreadings.setIc5SoPethaneAvg(gasreadings.getIc5SoPethaneFrom());
				}else if(gasreadings.getIc5SoPethaneFrom() == null && gasreadings.getIc5SoPethaneTo() != null) {
					gasreadings.setIc5SoPethaneAvg(gasreadings.getIc5SoPethaneTo());
				}else if(gasreadings.getIc5SoPethaneFrom() != null && gasreadings.getIc5SoPethaneTo() != null) {
					gasreadings.setIc5SoPethaneAvg((gasreadings.getIc5SoPethaneFrom() + gasreadings.getIc5SoPethaneTo())/2);
				}
				
				if(gasreadings.getNc5PentaneFrom() != null && gasreadings.getNc5PentaneTo() == null) {
					gasreadings.setNc5PentaneAvg(gasreadings.getNc5PentaneFrom());
				}else if (gasreadings.getNc5PentaneFrom() == null && gasreadings.getNc5PentaneTo() != null) {
					gasreadings.setNc5PentaneAvg(gasreadings.getNc5PentaneTo());
				}else if (gasreadings.getNc5PentaneFrom() != null && gasreadings.getNc5PentaneTo() != null) {
					gasreadings.setNc5PentaneAvg((gasreadings.getNc5PentaneFrom() + gasreadings.getNc5PentaneTo())/2);
				}
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(gasreadings);
			}
		}
	}
}