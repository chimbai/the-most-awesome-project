package com.idsdatanet.d2.geonet.wirelineRun;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

/**
 * An implementation of CascadeLookupHandler that filters service companies by
 * the given service code. This handler can only be used as the "controlling"
 * field, but not as "dependent" field.
 * 
 * @author zjong
 * 
 */
public class ServiceCompanyLookupHandler implements CascadeLookupHandler {
	
	private String serviceCode;
		
	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String sql = "select a.lookupCompanyUid, a.companyName from LookupCompany a, LookupCompanyService b where (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and a.lookupCompanyUid = b.lookupCompanyUid and b.code = :serviceCode order by a.companyName";
		List<Object[]> listCompanyResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { "serviceCode" }, new String[] { serviceCode });
		if (listCompanyResults.size() > 0) {
			for (Object[] listCompanyResult : listCompanyResults) {
				if (listCompanyResult[0] != null && listCompanyResult[1] != null) {
					result.put(listCompanyResult[0].toString(), new LookupItem(listCompanyResult[0].toString(), listCompanyResult[1].toString()));
				}
			}
		}
		return result;
	}
	
	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		return null;
	}
	
	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception{
		return null;
	}

}
