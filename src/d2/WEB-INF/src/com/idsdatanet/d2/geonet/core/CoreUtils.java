package com.idsdatanet.d2.geonet.core;

import java.util.List;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Core;
import com.idsdatanet.d2.core.model.CoreDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CoreUtils{	
	/**
	 * common method to calculate total recovered value
	 * @param node
	 * @param session
	 * @param thisConverter
	 */
	public static void calculateTotalRecovered(CommandBeanTreeNode node, UserSession session, CustomFieldUom thisConverter) throws Exception {
		//the length recovered field in core section will auto calculate when the below gwp is turn on
		if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_CORE_TOTAL_RECOVERED))) {
			Object obj = node.getData();
			Double totalRecovered = 0.0;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			String operationUid="";
			String dailyUid="";
			if(session.getCurrentOperationUid()!=null)
			{
				operationUid = 	session.getCurrentOperationUid();
			}
			if(session.getCurrentDailyUid()!=null)
			{
				dailyUid = session.getCurrentDailyUid();
			}
			if(obj instanceof Core)
			{
				
				Core thisCore = (Core) obj;
				String coreUid="";
				if(thisCore.getCoreUid()!=null)
				{
					coreUid = thisCore.getCoreUid();
				}
				String[] paramsFields = {"operationUid", "coreUid", "dailyUid"};
				Object[] paramsValues = {operationUid, coreUid, dailyUid};
				//this is to total up the recovery value in the core detail section and saved it into the length recovered field in core section
				String strSql1 = "SELECT SUM(recovery) " +
				"FROM CoreDetail " +
				"WHERE (isDeleted = false or isDeleted is null) " +
				"and dailyUid = :dailyUid AND operationUid = :operationUid AND coreUid = :coreUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields, paramsValues,qp);
				
				if(lstResult.size() > 0)
				{
					if(lstResult.get(0)!=null)
					{
						totalRecovered = Double.parseDouble(lstResult.get(0).toString());
						if (thisConverter.isUOMMappingAvailable()){
							thisConverter.setBaseValue(totalRecovered);
							totalRecovered = thisConverter.getConvertedValue();
						}
						thisCore.setLengthRecovered(totalRecovered);
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisCore);
					}
					
					
				}
				
			}
			if(obj instanceof CoreDetail)
			{
				CoreDetail thisCoreDetail = (CoreDetail) obj;
				String coreUid="";
				
				if(thisCoreDetail.getCoreUid()!=null)
				{
					coreUid = thisCoreDetail.getCoreUid();
				}
				String[] paramsFields = {"operationUid", "coreUid", "dailyUid"};
				Object[] paramsValues = {operationUid, coreUid, dailyUid};
				//this is to total up the recovery value in the core detail section and saved it into the length recovered field in core section
				String strSql1 = "SELECT SUM(recovery) " +
				"FROM CoreDetail " +
				"WHERE (isDeleted = false or isDeleted is null) " +
				"and dailyUid = :dailyUid AND operationUid = :operationUid AND coreUid = :coreUid";
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields, paramsValues,qp);
				
				if(lstResult.size() > 0)
				{
					if(lstResult.get(0)!=null)
					{
						totalRecovered = Double.parseDouble(lstResult.get(0).toString());
						if (thisConverter.isUOMMappingAvailable()){
							thisConverter.setBaseValue(totalRecovered);
							totalRecovered = thisConverter.getConvertedValue();
						}
						
						String strSql = "From Core where (isDeleted = false or isDeleted is null) " +
						"and coreUid=:coreUid";
						
						List <Core> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
								new String[] {"coreUid"},
								new Object[] {coreUid});
						
						if(lstResult4!=null && lstResult4.size() > 0)
						{
							for(Core coreRecord:lstResult4)
							{
								coreRecord.setLengthRecovered(totalRecovered);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(coreRecord);
							}
							
						}
						
					}
					else
					{
						//this is to set length recovered field in core section to null after all the core detail records is being deleted
						String strSql = "From Core where (isDeleted = false or isDeleted is null) " +
						"and coreUid=:coreUid";
						
						List <Core> lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql,
								new String[] {"coreUid"},
								new Object[] {coreUid});
						
						if(lstResult4!=null && lstResult4.size() > 0)
						{
							for(Core coreRecord:lstResult4)
							{
								coreRecord.setLengthRecovered(null);
								ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(coreRecord);
							}
							
						}
					}
				}
				
			}
		}
	}
	/**
	 * method to calculate cut and lost 
	 * @param node
	 * @param session
	 * @param thisConverter
	 */
	public static void calculateCutLost(CommandBeanTreeNode node, UserSession session, CustomFieldUom thisConverter) throws Exception {
		//the cut and lost field in core details section will auto calculate when the autoCalculateCutLost gwp is turn on
		if ("1".equalsIgnoreCase(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_AUTO_CALCULATE_CUT_LOST))) {
			Object obj = node.getData();
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(true);
			if(obj instanceof CoreDetail)
			{
				CoreDetail thisCoreDetail = (CoreDetail) obj;
				Double startmd=0.0;
				Double endmd =0.0;
				Double recovery =0.0;
				Double cutVal=0.0;
				Double lostVal=0.0;
				
				thisConverter.setReferenceMappingField(CoreDetail.class, "startdepthMdMsl"); 
				if(thisCoreDetail.getStartdepthMdMsl() != null)
				{
					thisConverter.setBaseValueFromUserValue(thisCoreDetail.getStartdepthMdMsl());
					thisConverter.addDatumOffsetToUserReferencePoint();
					startmd = thisConverter.getBasevalue();
				}
				
				thisConverter.setReferenceMappingField(CoreDetail.class, "enddepthMdMsl");
				if(thisCoreDetail.getEnddepthMdMsl() != null)
				{
					thisConverter.setBaseValueFromUserValue(thisCoreDetail.getEnddepthMdMsl());
					thisConverter.addDatumOffsetToUserReferencePoint();
					endmd = thisConverter.getBasevalue();
				}
				
				
				thisConverter.setReferenceMappingField(CoreDetail.class, "recovery");
				if(thisCoreDetail.getRecovery() != null)
				{
					thisConverter.setBaseValueFromUserValue(thisCoreDetail.getRecovery());
					recovery = thisConverter.getBasevalue();
				}
									
				//calculate cut
				cutVal = endmd - startmd;
				
				//calculate Lost 
				lostVal=recovery - cutVal;
				
				thisCoreDetail.setCut(cutVal);
				thisCoreDetail.setLostCores(lostVal);
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisCoreDetail, qp);
				
			}
		}
	}
	
	/**
	 * method to calculate amount cut from core details 
	 * @param operationUId
	 * @param coreUid
	 */
	public static Double calculateAmountcut(String operationUid,String coreUid) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		Double sumCut = 0.0;
		
			String[] paramsFields = {"operationUid", "coreUid"};
			Object[] paramsValues = {operationUid, coreUid};
			//this is to total up the cut value in the core detail section
			String strSql1 = "SELECT SUM(cut) " +
			"FROM CoreDetail " +
			"WHERE (isDeleted = false or isDeleted is null) " +
			"and operationUid = :operationUid AND coreUid = :coreUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields, paramsValues,qp);
			
			if(lstResult.size() > 0)
			{
				if(lstResult.get(0)!=null)
				{
					sumCut = Double.parseDouble(lstResult.get(0).toString());
				}
			}
			return sumCut;
	}
}