package com.idsdatanet.d2.geonet.wellSummary;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.context.ServletContextAware;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

import edu.emory.mathcs.backport.java.util.Arrays;

public class WellSummaryImagePathLookupHandler implements LookupHandler, ServletContextAware, InitializingBean {
	private ServletContext servetContext;
	private String imagePath;
	private List<String> imageExtension;
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache cache) throws Exception {
		String imageDirectory = this.getServletContext().getRealPath("/") + this.getImagePath();
		File directory = new File(imageDirectory);
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		if(directory.isDirectory()) {
			// retrieve all content of the current directory
			File[] fileList = directory.listFiles();
			if(fileList != null) {
				List<File> imageList = Arrays.asList(fileList);
				Collections.sort(imageList, new FileComparator());
				
				for(File image : imageList) {
					if (image.isFile()) {
						if(this.isMatchExtension(image)){
							LookupItem item = new LookupItem(image.getName(), this.removeFileExtension(image));
							result.put(item.getKey(), item);
						}
					}
				}
			}
		}
		
		return result;
	}

	private boolean isMatchExtension(File file){
		String name = file.getName().toLowerCase();
		for(String ext: this.imageExtension){
			if(name.endsWith("." + ext)) return true;
		}
		return false;
	}
	
	public String removeFileExtension(File file) {
		String name = file.getName();
		int i = name.lastIndexOf(".");
		if(i != -1){
			return name.substring(0, i);
		}else{
			return name;
		}
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	public String getImagePath() {
		return this.imagePath;
	}
	
	public void setImageExtension(List<String> list) {
		if(list != null){
			this.imageExtension = new ArrayList<String>();
			for(String item: list){
				this.imageExtension.add(item.toLowerCase());
			}
		}
	}
	
	public List<String> getImageExtension() {
		return this.imageExtension;
	}
	
	public void setServletContext(ServletContext servletContext) {
		this.servetContext = servletContext;
	}
	
	public ServletContext getServletContext() {
		return this.servetContext;
	}

	public void afterPropertiesSet() throws Exception {
		if(this.imagePath == null) throw new Exception(this.getClass() + " - imagePath property must be set.");
		if(this.imageExtension == null) throw new Exception(this.getClass() + " - imageExtension property must be set.");		
	}

	private class FileComparator implements Comparator<File>{
		public int compare(File o1, File o2){
			return o1.getName().compareTo(o2.getName());
		}
	}
}
