package com.idsdatanet.d2.geonet.core;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Core;
import com.idsdatanet.d2.core.model.CoreDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CoreDataNodeListener extends EmptyDataNodeListener {
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		
		Object object = node.getData();
		
		if (object instanceof CoreDetail) {
			CoreDetail thisCoreDetail = (CoreDetail) object;
			
			Double thickness = 0.0;
			Double startmd = 0.0;
			Double endmd = 0.0;
						
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CoreDetail.class, "startdepthMdMsl"); 
			if(thisCoreDetail.getStartdepthMdMsl() != null)
			{
				thisConverter.setBaseValueFromUserValue(thisCoreDetail.getStartdepthMdMsl());
				thisConverter.addDatumOffsetToUserReferencePoint();
				startmd = thisConverter.getBasevalue();
			}
			
			thisConverter.setReferenceMappingField(CoreDetail.class, "enddepthMdMsl");
			if(thisCoreDetail.getEnddepthMdMsl() != null)
			{
				thisConverter.setBaseValueFromUserValue(thisCoreDetail.getEnddepthMdMsl());
				thisConverter.addDatumOffsetToUserReferencePoint();
				endmd = thisConverter.getBasevalue();
			}
									
			if((endmd - startmd) > 0)
			{
				thickness = endmd - startmd;
			}
						
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@thickness", thisConverter.getUOMMapping(false));
			}
			thisConverter.setBaseValue(thickness);
			node.getDynaAttr().put("thickness", thisConverter.getConvertedValue());
		}
		
		if (object instanceof Core) {
			Core thisCore = (Core) object;
			Double amountCut = 0.0;
			Double startmd = 0.0;
			Double endmd = 0.0;
			Double lengthrecovered = 0.0;
			Double percentrecovered = 0.0;
			
			//To Calculate amountCut
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Core.class, "topMdMsl"); 
			if(thisCore.getTopMdMsl() != null)
			{
				thisConverter.setBaseValueFromUserValue(thisCore.getTopMdMsl());
				thisConverter.addDatumOffsetToUserReferencePoint();
				startmd = thisConverter.getBasevalue();
			}
			
			thisConverter.setReferenceMappingField(Core.class, "bottomMdMsl");
			if(thisCore.getBottomMdMsl() != null)
			{
				thisConverter.setBaseValueFromUserValue(thisCore.getBottomMdMsl());
				thisConverter.addDatumOffsetToUserReferencePoint();
				endmd = thisConverter.getBasevalue();
			}
			
			// gwp to auto calculate amount cut base on core details.
			if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "autoCalculateAmountCutFromCoreDetails"))) 
			{
				amountCut = CoreUtils.calculateAmountcut(thisCore.getOperationUid(),thisCore.getCoreUid());
			}else
			{
				if((endmd - startmd) > 0)
				{
					amountCut = endmd - startmd;
				}
			}
						
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@amountCut", thisConverter.getUOMMapping(false));
			}
			thisConverter.setBaseValue(amountCut);
			node.getDynaAttr().put("amountCut", thisConverter.getConvertedValue());
			
			//Calculate percentRecovered formula : lengthRecovered divide by amountCut (20090902-1408-lting)
			if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "calcPercentRecovered"))) {
				if(amountCut != 0 && thisCore.getLengthRecovered() != null)
				{
					CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, Core.class, "lengthRecovered");
					thisConverter2.setBaseValueFromUserValue(thisCore.getLengthRecovered());
					lengthrecovered = thisConverter2.getBasevalue();
					percentrecovered = (lengthrecovered / amountCut) * 100;
					
				}
				thisCore.setPercentRecovered(percentrecovered);
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisCore, qp);
			}
		}
	}
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Core.class, "lengthRecovered");
		//this is to calculate the total recovered value by total up all the recovered value in core detail section 
		CoreUtils.calculateTotalRecovered(node, session, thisConverter);
		//this is to calculate the cut and lost for each of the core details records
		CoreUtils.calculateCutLost(node, session, thisConverter);	
		
	}
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Core.class, "lengthRecovered");
		//this is to calculate the total recovered value by total up all the recovered value in core detail section after the core detail is deleted.
		CoreUtils.calculateTotalRecovered(node, session, thisConverter);
	}
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
	
		Object object = node.getData();
		if (object instanceof Core) {
			
			Core thisCore = (Core) object;
			//Check if End Depth Greater than start depth
			if (thisCore.getTopMdMsl()!=null && thisCore.getBottomMdMsl()!=null) {
				if (thisCore.getTopMdMsl() > thisCore.getBottomMdMsl()) {
					status.setFieldError(node, "bottomMdMsl", "End Depth (MD) must greater than Start Depth (MD).");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (thisCore.getTopTvdMsl()!=null && thisCore.getBottomTvdMsl()!=null) {
				if (thisCore.getTopTvdMsl() > thisCore.getBottomTvdMsl()) {
					status.setFieldError(node, "bottomTvdMsl", "End Depth (TVD) must greater than Start Depth (TVD).");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (thisCore.getDepthFromSubseaTvd()!=null && thisCore.getDepthToSubseaTvd()!=null) {
				if (thisCore.getDepthFromSubseaTvd() > thisCore.getDepthToSubseaTvd()) {
					status.setFieldError(node, "depthToSubseaTvd", "End Depth must greater than Start Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
            if (thisCore.getRecoveryTopMdMsl()!=null && thisCore.getRecoveryBottomMdMsl()!=null) {
                if (thisCore.getRecoveryTopMdMsl() > thisCore.getRecoveryBottomMdMsl()) {
                     status.setFieldError(node, "recoveryBottomMdMsl", "End Depth must greater than Start Depth.");                 
                     status.setContinueProcess(false, true);                    
                     return;                                       
                }                                                    
            }   
		}
		
		if (object instanceof CoreDetail) {
			
			CoreDetail thisCoreDetail = (CoreDetail) object;
			
			if (thisCoreDetail.getStartdepthMdMsl()!=null && thisCoreDetail.getEnddepthMdMsl()!=null) {
				if (thisCoreDetail.getStartdepthMdMsl() > thisCoreDetail.getEnddepthMdMsl()) {
					status.setFieldError(node, "enddepthMdMsl", "End Depth (MD) must greater than Start Depth (MD).");
					status.setContinueProcess(false, true);
					return;
				}
			}
		}
	}
}
