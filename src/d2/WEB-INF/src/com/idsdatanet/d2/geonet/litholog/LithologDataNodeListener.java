package com.idsdatanet.d2.geonet.litholog;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.*;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LithologDataNodeListener extends EmptyDataNodeListener {

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof Litholog) {
			Litholog litholog = (Litholog)object;
			//TO CALCULATE THICKNESS FOR EACH SAMPLE DESCRIPTION			
			Double depthMdFrom = litholog.getDepthFromMdMsl();
			Double depthMdTo = litholog.getDepthToMdMsl();
			Double thickness = 0.0;
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Litholog.class, "depthFromMdMsl");
			if (depthMdFrom == null) depthMdFrom =0.0;
			if (depthMdTo == null) depthMdTo =0.0;
						
			if (depthMdTo > depthMdFrom) {
				thickness = depthMdTo - depthMdFrom;
			}
			else
			{
				thickness = 0.0;
			}
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@thickness", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("thickness", thickness);
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
				
		if (object instanceof Litholog) {
			Litholog litholog = (Litholog)object;
			Double grainTotalPct = 0.00;
			Double grainSizeTotalPct = 0.00;
			
			//CALCULATE TOTAL FOR GRAINS PERCENTAGE
			if(litholog.getGrainCalcerousClayPct() != null)
			{
				grainTotalPct += litholog.getGrainCalcerousClayPct();
			}
			
			if(litholog.getGrainCalcerousSiltPct() != null)
			{
				grainTotalPct += litholog.getGrainCalcerousSiltPct();
			}
			
			if(litholog.getGrainCalcerousSandPct() != null)
			{
				grainTotalPct += litholog.getGrainCalcerousSandPct();
			}
			
			if(litholog.getGrainSilicicClayPct() != null)
			{
				grainTotalPct += litholog.getGrainSilicicClayPct();
			}
			
			if(litholog.getGrainSilicicSiltPct() != null)
			{
				grainTotalPct += litholog.getGrainSilicicSiltPct();
			}
			
			if(litholog.getGrainSilicicSandPct() != null)
			{
				grainTotalPct += litholog.getGrainSilicicSandPct();
			}
						
			litholog.setGrainTotalPct(grainTotalPct);
			
			//CALCULATE TOTAL FOR GRAINS SIZE PERCENTAGE
			if(litholog.getGrainSizeVfinePct() != null)
			{
				grainSizeTotalPct += litholog.getGrainSizeVfinePct();
			}
			
			if(litholog.getGrainSizeFinePct() != null)
			{
				grainSizeTotalPct += litholog.getGrainSizeFinePct();
			}
			
			if(litholog.getGrainSizeMediumPct() != null)
			{
				grainSizeTotalPct += litholog.getGrainSizeMediumPct();
			}
			
			if(litholog.getGrainSizeCoarsePct() != null)
			{
				grainSizeTotalPct += litholog.getGrainSizeCoarsePct();
			}
			
			if(litholog.getGrainSizeVcoarsePct() != null)
			{
				grainSizeTotalPct += litholog.getGrainSizeVcoarsePct();
			}
			
			if(litholog.getGrainSizeGranularPct() != null)
			{
				grainSizeTotalPct += litholog.getGrainSizeGranularPct();
			}
								
			litholog.setGrainSizeTotalPct(grainSizeTotalPct);
		}
	}
}
