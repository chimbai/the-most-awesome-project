package com.idsdatanet.d2.geonet.lwd;

import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.DrillingParameters;
import com.idsdatanet.d2.core.model.Lwd;
import com.idsdatanet.d2.core.model.LwdSuite;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LwdCommandBeanListener extends EmptyCommandBeanListener {
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(Lwd.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())){
			if("bharunUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				//To get Bharun record when onChange runNumber
				bharunChanged(commandBean, request, targetCommandBeanTreeNode);
			}else if("mudPropertiesUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				this.mudPropertyChanged(targetCommandBeanTreeNode, request);
			}
		}
	}
				
	private void bharunChanged(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode node) throws Exception {
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Bitrun.class, "tfa");
		DecimalFormat formatter = new DecimalFormat("#0.000000");

		String bharunUid = (String) PropertyUtils.getProperty(node.getData(), "bharunUid");				
		
		CommandBeanTreeNode parentNode = node.getParent();				
		LwdSuite thisLwdSuite = (LwdSuite) parentNode.getData();
		
		String bitrunNumber = null;
		String bitDiameter = null;
		Double tfa = 0.0;
		String cutterType = null;
		Double flowMin = 0.0;
		Double flowAvg = 0.0;
		Double flowMax = 0.0;
		
		String strSql = "FROM Bitrun WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :thisBharunUid";			
		List Result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisBharunUid", bharunUid);			
		if (Result.size() > 0){
			Bitrun thisBitrun = (Bitrun) Result.get(0);
			if (StringUtils.isNotBlank(thisBitrun.getBitrunNumber())){
				bitrunNumber = thisBitrun.getBitrunNumber();
			}
			
			if (thisBitrun.getBitDiameter() != null) {
				bitDiameter = CommonUtils.roundUpFormat(formatter, thisBitrun.getBitDiameter());
			}
			
			if (thisBitrun.getTfa() != null) {
				//thisConverter.setBaseValueFromUserValue(thisBitrun.getTfa());
				//tfa = thisConverter.getConvertedValue();
				tfa = thisBitrun.getTfa();
				
			}
			
			if (StringUtils.isNotBlank(thisBitrun.getCutterType())){
				cutterType = thisBitrun.getCutterType();
			}
		}
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@tfa", thisConverter.getUOMMapping());
		}
		node.getDynaAttr().put("tfa", tfa);
		
		thisConverter.setReferenceMappingField(DrillingParameters.class, "flow_avg");
		
		String[] paramsFields = {"thisBharunUid", "thisDailyUid"};
		Object[] paramsValues = new Object[2]; paramsValues[0] = bharunUid; paramsValues[1] = thisLwdSuite.getOutDailyUid();
		
		String strSql1 = "FROM DrillingParameters WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :thisBharunUid AND dailyUid = :thisDailyUid";			
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields, paramsValues);			
		if (lstResult.size() > 0){
			DrillingParameters thisDP = (DrillingParameters) lstResult.get(0);
			if (thisDP.getFlowMin() != null) {
				//thisConverter.setBaseValueFromUserValue(thisDP.getFlowMin());
				//flowMin = thisConverter.getConvertedValue();
				flowMin = thisDP.getFlowMin();
			}
			
			if (thisDP.getFlowAvg()!= null) {
				//thisConverter.setBaseValueFromUserValue(thisDP.getFlowAvg());
				//flowAvg = thisConverter.getConvertedValue();
				flowAvg = thisDP.getFlowAvg();
			}
			
			if (thisDP.getFlowMax()!= null) {
				//thisConverter.setBaseValueFromUserValue(thisDP.getFlowMax());
				//flowMax = thisConverter.getConvertedValue();
				flowMax = thisDP.getFlowMax();
				
			}
		}
			
		node.getDynaAttr().put("bitrunNumber", bitrunNumber);
		node.getDynaAttr().put("bitDiameter", bitDiameter);
		
		node.getDynaAttr().put("cutterType", cutterType);
		
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@flowMin", thisConverter.getUOMMapping());
		}
		node.getDynaAttr().put("flowMin", flowMin);
		
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@flowAvg", thisConverter.getUOMMapping());
		}
		node.getDynaAttr().put("flowAvg", flowAvg);
		
		if (thisConverter.isUOMMappingAvailable()){
			node.setCustomUOM("@flowMax", thisConverter.getUOMMapping());
		}
		node.getDynaAttr().put("flowMax", flowMax);
	}

	//To Load Mud Properties Detail
	private void mudPropertyChanged(CommandBeanTreeNode node, HttpServletRequest request) throws Exception {
		UserSession session = UserSession.getInstance(request);
		String mudPropertiesUid = (String) PropertyUtils.getProperty(node.getData(), "mudPropertiesUid");				
		LwdUtils.getMudPropertiesInfo(node, mudPropertiesUid, session.getCurrentGroupUid());
	}
}
