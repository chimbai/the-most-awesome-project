package com.idsdatanet.d2.geonet.palynology;
//import class
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Palynology;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import org.apache.commons.lang.StringUtils;

public class PalynologyDataNodeListener extends EmptyDataNodeListener {
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof Palynology) {
			Palynology thisPalynology = (Palynology) object;
						
			Double thickness = 0.0;
			Double nextDepth = 0.0;
			List lstResult = null;
			
			//CALCULATE THICKNESS
			Double tvdbrt = (Double) thisPalynology.getTvdbrtTvdMsl();
			if (tvdbrt==null) {
				if("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), GroupWidePreference.GWP_CALC_TVDRT))) {
					//calculate tvdbrt
					if (thisPalynology.getMdbrtMdMsl()!=null) {
						tvdbrt = this.calculateTvdBrt(commandBean, thisPalynology);
						thisPalynology.setTvdbrtTvdMsl(tvdbrt);
						//save to db
						String strSql2 = "UPDATE Palynology SET tvdbrtTvdMsl = :tvdbrtTvdMsl WHERE palynologyUid = :thisPalynologyUid";
						ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, new String[] {"tvdbrtTvdMsl", "thisPalynologyUid"}, new Object[] {tvdbrt, thisPalynology.getPalynologyUid()});
					}
				} else {
					tvdbrt = 0.0;
				}
			}
			
			String[] paramsFields = {"thisOperationUid", "tvdbrtTvdMsl", "thisPalynologyUid"};
			Object[] paramsValues = {thisPalynology.getOperationUid(), tvdbrt, thisPalynology.getPalynologyUid()};
			String strSql = "SELECT tvdbrtTvdMsl FROM Palynology WHERE (isDeleted = '' or isDeleted is null) and operationUid = :thisOperationUid AND tvdbrtTvdMsl > :tvdbrtTvdMsl AND palynologyUid <> :thisPalynologyUid ORDER BY mdbrtMdMsl";
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);	
			if (!lstResult.isEmpty()) {
				Object thisResult = (Object) lstResult.get(0);
				if(thisResult != null) nextDepth = Double.parseDouble(thisResult.toString());
				thickness = nextDepth - tvdbrt;
			}
			thisPalynology.setThickness(thickness);
			
			//UPDATE THICKNESS IN DATABASE
			String[] paramsFields2 = {"thickness", "thisPalynologyUid"};
			Object[] paramsValues2 = {thickness, thisPalynology.getPalynologyUid()};
			String strSql2 = "UPDATE Palynology SET thickness = :thickness WHERE palynologyUid = :thisPalynologyUid";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields2, paramsValues2);
			
			//CALCULATE TVDSS FOR DYNAMIC FIELD
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Palynology.class, "tvdbrtTvdMsl");
			QueryProperties qp = new QueryProperties();
			//qp.setDatumConversionEnabled(false);
			qp.setUomConversionEnabled(false);
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Palynology WHERE palynologyUid=:palynologyUid", "palynologyUid", thisPalynology.getPalynologyUid(), qp);
			if (lstResult.size()>0) {
				Palynology rec = (Palynology) lstResult.get(0);
				if (rec.getTvdbrtTvdMsl()!=null) {
					thisConverter.setBaseValue(rec.getTvdbrtTvdMsl());
					thisConverter.removeDatumOffset();
					// - depth to subsea (include offset to msl)
					node.getDynaAttr().put("tvdssTvdMsl", thisConverter.getConvertedValue());
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@tvdssTvdMsl", thisConverter.getUOMMapping());
					}
					
					// - depth to datum reference point (without offset to msl)
					thisConverter = new CustomFieldUom(commandBean, Palynology.class, "tvdbrtTvdMsl");
					thisConverter.setBaseValue(rec.getTvdbrtTvdMsl());
					thisConverter.removeDatumOffsetToUserReferencePoint();
					node.getDynaAttr().put("tvdssTvdMslDatumReferencePoint", thisConverter.getConvertedValue());
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@tvdssTvdMslDatumReferencePoint", thisConverter.getUOMMapping());
					}
				}
			}			
		}
	}

	@Override
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		
		Object object = node.getData();
		
		if (object instanceof Palynology) {
			Palynology thisPalynology = (Palynology) object;
			if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), GroupWidePreference.GWP_CALC_TVDRT)))
			{
				if (thisPalynology.getMdbrtMdMsl()!=null) {
					Double tvdbrt = this.calculateTvdBrt(commandBean, thisPalynology);
					thisPalynology.setTvdbrtTvdMsl(tvdbrt);
				} else {
					thisPalynology.setTvdbrtTvdMsl(null);
				}
			}
		}
	}
	
	private Double calculateTvdBrt(CommandBean commandBean, Palynology thisPalynology) throws Exception {
		//Double tvdbrt = 0.0;
		Double mdbrt = thisPalynology.getMdbrtMdMsl();
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(false);
		
		//get the base value of the MD 
		CustomFieldUom thisConverterField = new CustomFieldUom(commandBean, Palynology.class, "mdbrtMdMsl");
		thisConverterField.setBaseValueFromUserValue(mdbrt);
		mdbrt = thisConverterField.getBasevalue();

		String strSql = "SELECT surveyReferenceUid FROM SurveyReference WHERE (isDeleted = '' or isDeleted is null) and (isPlanned=false or isPlanned is null) and wellboreUid = :wellboreUid";
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellboreUid", thisPalynology.getWellboreUid());	
		if (lstResult.size()>0)
		{
			String surveyReferenceUid = (String) lstResult.get(0);
			if (StringUtils.isNotBlank(surveyReferenceUid)) {
				
				//query statement
				String[] paramsFields = {"thisSurveyReferenceUid", "thisMdbrtMdMsl"};
				Object[] paramsValues = {surveyReferenceUid, mdbrt};
				strSql = "SELECT depthMdMsl, depthTvdMsl, inclinationAngle, azimuthAngle FROM SurveyStation WHERE (isDeleted = '' or isDeleted is null) and surveyReferenceUid = :thisSurveyReferenceUid AND depthMdMsl < :thisMdbrtMdMsl ORDER BY depthMdMsl desc";
				lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);	
				//variable
				Double md1 = 0.0;
				Double tvd1 = 0.0;
				Double inclination1 = 0.0;
				Double corrazimuth1 = 0.0;
				Double inclination0 = 0.0;
				Double corrazimuth0 = 0.0;
				//get the query result from the survey 
				if (!lstResult.isEmpty())
				{		
					Object[] thisResult_md = (Object[]) lstResult.get(0);
					if(thisResult_md != null)
					{	
						if (thisResult_md[0] != null) md1 = Double.parseDouble(thisResult_md[0].toString());
						if (thisResult_md[1] != null) tvd1 = Double.parseDouble(thisResult_md[1].toString());
						if (thisResult_md[2] != null) inclination1 = Double.parseDouble(thisResult_md[2].toString());
						if (thisResult_md[3] != null) corrazimuth1 = Double.parseDouble(thisResult_md[3].toString());
					}
					if (lstResult.size() > 1)
					{
						Object[] thisResult_tvd = (Object[]) lstResult.get(1);
						if(thisResult_tvd != null)
						{
							if (thisResult_tvd[2] != null) inclination0 = Double.parseDouble(thisResult_tvd[2].toString());
							if (thisResult_tvd[3] != null) corrazimuth0 = Double.parseDouble(thisResult_tvd[3].toString());
						}
					}

				}

				String strSql1 = "";
				List lstResult1 = null;
				Double md2 = 0.0;
				Double inclination2 = 0.0;
				
				String[] paramsFields1 = {"thisSurveyReferenceUid", "thisMdbrtMdMsl"};
				Object[] paramsValues1 = {surveyReferenceUid, mdbrt};
				strSql1 = "SELECT depthMdMsl, depthTvdMsl, inclinationAngle, azimuthAngle FROM SurveyStation WHERE (isDeleted = '' or isDeleted is null) and surveyReferenceUid = :thisSurveyReferenceUid AND depthMdMsl > :thisMdbrtMdMsl ORDER BY depthMdMsl";
				lstResult1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, paramsFields1, paramsValues1, qp);	
				
				//get the query result 
				if (!lstResult1.isEmpty())
				{	
					Object[] thisResult_md = (Object[]) lstResult1.get(0);
					if(thisResult_md != null)
					{
						if (thisResult_md[0] != null) md2 = Double.parseDouble(thisResult_md[0].toString());
						if (thisResult_md[2] != null)inclination2 = Double.parseDouble(thisResult_md[2].toString());
					}
				}
				//tvdrt calculation
				Double b = 0.0, s = 0.0, DL = 0.0, RF = 0.0, pi = Math.PI;
				
				b = (inclination2 - inclination1)/(md2- md1)*(mdbrt - md1)+ inclination1;
				s = pi / 180;
				DL = Math.acos((Math.cos((inclination1-inclination0)*s))- Math.sin(inclination0*s)*Math.sin(inclination1*s)*(1-Math.cos((corrazimuth1*s)-(corrazimuth0*s))))*180/pi;
				RF = (180/pi)*(2/DL)*Math.tan(DL/2*s);
				Double tvdbrt =tvd1+(mdbrt-md1)/2*(Math.cos(inclination1*s)+Math.cos(b*s))*RF;
				
				thisConverterField.setReferenceMappingField(Palynology.class, "tvdbrtTvdMsl");
				if (tvdbrt.isNaN()) {
					thisConverterField.setBaseValue(mdbrt);
				} else {
					//set the calculation value to on screen value 
					thisConverterField.setBaseValue(tvdbrt);
				}
				thisConverterField.addDatumOffset();
				tvdbrt = thisConverterField.getConvertedValue();
				return tvdbrt; 
			}
		}
		return null;
	}
}
