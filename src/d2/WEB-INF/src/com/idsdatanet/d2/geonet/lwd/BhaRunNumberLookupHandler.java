package com.idsdatanet.d2.geonet.lwd;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.util.WellNameUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.bharun.BharunUtils;

public class BhaRunNumberLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String operationUid = userSelection.getOperationUid();
		String sqlStr = "SELECT bharunUid, bhaRunNumber, dailyidIn, dailyidOut FROM Bharun WHERE " +
				"(isDeleted = FALSE OR isDeleted IS NULL) AND operationUid = :operationUid ORDER BY bhaRunNumber";
		List<Object[]> listBharunResults = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlStr, "operationUid", operationUid);

		if (listBharunResults.size() > 0) {
			Collections.sort(listBharunResults, new BharunNumberComparator());
			for (Object[] listBharunResult : listBharunResults) {
				if (listBharunResult[0] != null && listBharunResult[1] != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
					String bhaRunLabel = "#" + listBharunResult[1].toString();
					String dailyidIn = BharunUtils.nullSafeToString(listBharunResult[2]);
					String dailyidOut = BharunUtils.nullSafeToString(listBharunResult[3]);
					if (StringUtils.isNotBlank(dailyidIn) && StringUtils.isNotBlank(dailyidOut)) {
						Daily bharunDateIn =  ApplicationUtils.getConfiguredInstance().getCachedDaily(listBharunResult[2].toString());
						Daily bharunDateOut =  ApplicationUtils.getConfiguredInstance().getCachedDaily(listBharunResult[3].toString());
						if (bharunDateIn !=null && bharunDateOut !=null) {
							String dateIn = sdf.format(bharunDateIn.getDayDate());
							String dateOut = sdf.format(bharunDateOut.getDayDate());
							bhaRunLabel += " (" + dateIn + " - " + dateOut + ")";
						}
					} else if (StringUtils.isNotBlank(dailyidIn) && StringUtils.isBlank(dailyidOut)) {
						Daily bharunDateIn =  ApplicationUtils.getConfiguredInstance().getCachedDaily(listBharunResult[2].toString());
						if (bharunDateIn !=null) {
							String dateIn = sdf.format(bharunDateIn.getDayDate());
							bhaRunLabel += " (" + dateIn + " - N/A)";
						}
					} else if (StringUtils.isBlank(dailyidIn) && StringUtils.isNotBlank(dailyidOut)) {
						Daily bharunDateOut =  ApplicationUtils.getConfiguredInstance().getCachedDaily(listBharunResult[3].toString());
						if (bharunDateOut !=null) {
							String dateOut = sdf.format(bharunDateOut.getDayDate());
							bhaRunLabel += " (N/A - " + dateOut + ")";
						}
					}
					result.put(listBharunResult[0].toString(), new LookupItem(listBharunResult[0].toString(), bhaRunLabel));
				}
			}
		}

		return result;
	}
	
	private class BharunNumberComparator implements Comparator<Object[]> {
		public int compare(Object[] o1, Object[] o2) {
			try {
				Object in1 = o1[1];
				Object in2 = o2[1];
				
				String f1 = WellNameUtil.getPaddedStr(in1.toString());
				String f2 = WellNameUtil.getPaddedStr(in2.toString());
				
				if (f1 == null || f2 == null) return 0;
				return f1.compareTo(f2);
				
			} catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
	}

}
