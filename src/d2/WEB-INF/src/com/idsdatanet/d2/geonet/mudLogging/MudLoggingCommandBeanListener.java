package com.idsdatanet.d2.geonet.mudLogging;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class MudLoggingCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {

	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
		if (commandBean.getRoot().getList() != null) {
			int countRec = 0;
			for (CommandBeanTreeNode node: commandBean.getRoot().getList().get("GeologistTeam").values()) {
				if(!node.getAtts().getAction().equals(Action.DELETE) && !node.getAtts().getAction().equals(Action.CANCEL)) {
					countRec++;
				}
			}
			if (countRec > 1) {
				commandBean.getRoot().setDirty(true);
				commandBean.getSystemMessage().addError("Only up to 1 record is allowed for Mud Logging" ,true, true);
				commandBean.setAbortFormProcessing(true);
			}
		}
	}

}