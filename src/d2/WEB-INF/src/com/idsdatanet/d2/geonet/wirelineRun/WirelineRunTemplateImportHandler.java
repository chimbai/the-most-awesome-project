package com.idsdatanet.d2.geonet.wirelineRun;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.core.lookup.CascadeLookupMeta;
import com.idsdatanet.d2.core.lookup.DbLookupDefinition;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.templateimport.DefaultTemplateImportHandler;
import com.idsdatanet.d2.core.web.mvc.templateimport.EmptySlot;

public class WirelineRunTemplateImportHandler extends DefaultTemplateImportHandler{
	private Map<String, Object> _lookup = null;
	private Map<String, Map> cachedLookup = new HashMap<String, Map>();
	
	public void setLookup(Object lookup) {
		if (lookup instanceof Map) {
			this._lookup = (Map<String,Object>)lookup;
		}
	}
	
	public Object getLookup() {
		return this._lookup;
	}

	@Override
	protected void setupAdditionalBindingValues(int line,
			List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode) {
		UserSelectionSnapshot userSelection = null;
		List<Daily> dailyList = null;
		
		if (this._lookup==null) return;
		if (bindingValue != null) {
			if (bindingValue.get(0) instanceof EmptySlot) {
			} else {
				try {
					
					for (Map.Entry<String, Object> entry : this._lookup.entrySet()) {
						String fieldName = entry.getKey();
						Object lookup = entry.getValue();
						String[] key = fieldName.split("[.]");
						if (lookup==null) continue;
						fieldName = fieldName.replaceAll(key[0], "data");
						
						Map<String, LookupItem> lookup_map = null;

						if (this.cachedLookup.containsKey(fieldName)) {
							lookup_map = this.cachedLookup.get(fieldName);
						} else {
							if(lookup instanceof DbLookupDefinition)
							{
								lookup = (String) ((DbLookupDefinition) lookup).getObject();			
							}
							lookup_map = LookupManager.getConfiguredInstance().getLookup(lookup.toString(), new UserSelectionSnapshot(commandBean.getCurrentUserSession()), null);
							this.cachedLookup.put(fieldName, lookup_map);
						}
						
						if (lookup_map!=null) {
							for (int i=0; i<bindingProperties.size(); i++) {
								if (fieldName.equals(bindingProperties.get(i))) {
									Object fieldValue = bindingValue.get(i);
									if (fieldValue==null) break;
									if (!lookup_map.containsKey(fieldValue.toString())) {
										String value = fieldValue.toString().toUpperCase();
										for (Map.Entry<String, LookupItem> lookupEntry : lookup_map.entrySet()) {
											LookupItem item = lookupEntry.getValue();
											if (item.getValue()==null) continue;
											String lookupValue = item.getValue().toString().toUpperCase();
											if (value.equals(lookupValue)) {
												bindingValue.set(i, item.getKey());
											}
										}
									}
									break;	
								}
							}
							
						}
					}
					//this for getting the date value from csv import template and import into screen drop down list 
					if (bindingProperties.contains(this.getPropertyNameFromField(targetMeta,"dailyUid")))
					{
						userSelection = new UserSelectionSnapshot(commandBean.getCurrentUserSession());
						dailyList = ApplicationUtils.getConfiguredInstance().getDaysInOperation(userSelection.getOperationUid());
						String fieldName = this.getPropertyNameFromField(targetMeta, "dailyUid");
						Date wirelineActivityDate = null;
						if(bindingProperties.indexOf(fieldName) < 0) return;
						Object fieldValue = bindingValue.get(bindingProperties.indexOf(fieldName));
						if (fieldValue!=null) wirelineActivityDate = CommonDateParser.parse(fieldValue.toString());
						
						if (dailyList!=null || dailyList.size() > 0) {
							if(wirelineActivityDate!=null) {
								for(Daily d:dailyList){
									if(DateUtils.isSameDay(d.getDayDate(), wirelineActivityDate)) {
										bindingValue.set(bindingProperties.indexOf(fieldName), d.getDailyUid());
									}
								}
							}
						}
					}
					
					if (bindingProperties.contains(this.getPropertyNameFromField(targetMeta,"wirelineRunDailyUid")))
					{
						userSelection = new UserSelectionSnapshot(commandBean.getCurrentUserSession());
						dailyList = ApplicationUtils.getConfiguredInstance().getDaysInOperation(userSelection.getOperationUid());
						String fieldName = this.getPropertyNameFromField(targetMeta, "wirelineRunDailyUid");
						Date wirelineRunDate = null;
						if(bindingProperties.indexOf(fieldName) < 0) return;
						Object fieldValue = bindingValue.get(bindingProperties.indexOf(fieldName));
						if (fieldValue!=null) wirelineRunDate = CommonDateParser.parse(fieldValue.toString());
						
						if (dailyList!=null || dailyList.size() > 0) {
							if(wirelineRunDate!=null) {
								for(Daily d:dailyList){
									if(DateUtils.isSameDay(d.getDayDate(), wirelineRunDate)) {
										bindingValue.set(bindingProperties.indexOf(fieldName), d.getDailyUid());
									}
								}
							}
						}
					}
					
					if (bindingProperties.contains(this.getPropertyNameFromField(targetMeta,"wirelineOutDailyUid")))
					{
						userSelection = new UserSelectionSnapshot(commandBean.getCurrentUserSession());
						dailyList = ApplicationUtils.getConfiguredInstance().getDaysInOperation(userSelection.getOperationUid());
						String fieldName = this.getPropertyNameFromField(targetMeta, "wirelineOutDailyUid");
						Date wirelineOutDate = null;
						if(bindingProperties.indexOf(fieldName) < 0) return;
						Object fieldValue = bindingValue.get(bindingProperties.indexOf(fieldName));
						if (fieldValue!=null) wirelineOutDate = CommonDateParser.parse(fieldValue.toString());
						
						if (dailyList!=null || dailyList.size() > 0) {
							if(wirelineOutDate!=null) {
								for(Daily d:dailyList){
									if(DateUtils.isSameDay(d.getDayDate(), wirelineOutDate)) {
										bindingValue.set(bindingProperties.indexOf(fieldName), d.getDailyUid());
									}
								}
							}
						}
					}
					//this is for csv import of the data into the cascade field 
					this.processServiceCompanyCascadeLookup(bindingProperties, bindingValue, targetMeta, commandBean, parentNode);
					this.processToolCodeCascadeLookup(bindingProperties, bindingValue, targetMeta, commandBean, parentNode);
					this.processMudPropertiesCascadeLookup(bindingProperties, bindingValue, targetMeta, commandBean, parentNode);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		
	}
	
	private void processServiceCompanyCascadeLookup(List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode) throws Exception
	{
		String propertyName = this.getPropertyNameFromField(targetMeta, "svcCo");
		int index = bindingProperties.indexOf(propertyName);
		String value = null;
		if(index > -1)
		{
			value = bindingValue.get(index).toString();
		}
		if (StringUtils.isNotBlank(value))
		{
			CascadeLookupMeta meta = commandBean.getCascadeLookupMeta("WirelineRun", "svcCo");
			if (meta!=null)
			{
				Map<String,LookupItem> catLookups = commandBean.getCascadeLookupMap(parentNode, "WirelineRun", "svcCo");
				for (Map.Entry<String, LookupItem> entry : catLookups.entrySet())
				{
					if (entry.getValue().getValue().toString().equalsIgnoreCase(value))
					{
						bindingValue.set(index, entry.getKey());
					}
				}
			}
			
		}
	}
	
	private void processToolCodeCascadeLookup(List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode) throws Exception
	{
		String propertyName = this.getPropertyNameFromField(targetMeta, "toolCode");
		int index = bindingProperties.indexOf(propertyName);
		String value = null;
		if(index > -1)
		{
			value = bindingValue.get(index).toString();
		}
		if (StringUtils.isNotBlank(value))
		{
			CascadeLookupMeta meta = commandBean.getCascadeLookupMeta("WirelineTool", "toolCode");
			if (meta!=null)
			{
				Map<String,LookupItem> catLookups = commandBean.getCascadeLookupMap(parentNode, "WirelineTool", "toolCode");
				for (Map.Entry<String, LookupItem> entry : catLookups.entrySet())
				{
					if (entry.getValue().getValue().toString().equalsIgnoreCase(value))
					{
						bindingValue.set(index, entry.getKey());
					}
				}
				
			}
			
		}
	}
	
	private void processMudPropertiesCascadeLookup(List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode) throws Exception
	{
		String propertyName = this.getPropertyNameFromField(targetMeta, "mudPropertiesUid");
		int index = bindingProperties.indexOf(propertyName);
		String value = bindingValue.get(index).toString();
		
		if (StringUtils.isNotBlank(value))
		{
			CascadeLookupMeta meta = commandBean.getCascadeLookupMeta("WirelineRun", "mudPropertiesUid");
			if (meta!=null)
			{
				Map<String,LookupItem> catLookups = commandBean.getCascadeLookupMap(parentNode, "WirelineRun", "mudPropertiesUid");
				for (Map.Entry<String, LookupItem> entry : catLookups.entrySet())
				{
						if (entry.getValue().getValue().toString().equalsIgnoreCase(value))
						{
							bindingValue.set(index, entry.getKey());
							
						}
				}
			}
			
			
		}
	}
	
}
