package com.idsdatanet.d2.geonet.graph;

import java.awt.Color;
import java.awt.Font;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.function.Function2D;
import org.jfree.data.function.LineFunction2D;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.statistics.Regression;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.TextAnchor;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.graph.BaseGraph;
import com.idsdatanet.d2.core.model.ModularFormationDynamicsTester;
import com.idsdatanet.d2.core.model.ModularFormationDynamicsTesterDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class FormationPressure extends BaseGraph {
	private Map<String, Object> xyLineMap   	  = new HashMap<String, Object>();
	private String currentMdtUid					  = null;
	private QueryProperties queryProperties 		  = new QueryProperties();
	private Color[] colors = {Color.blue, 
			Color.cyan,
			Color.darkGray, 
			Color.gray, 
			Color.green, 
			Color.lightGray, 
			Color.magenta, 
			Color.orange, 
			Color.pink, 
			Color.red, 
			Color.yellow
		};
	private NumberFormat formatter = new DecimalFormat("#.###");

	
	@Override
	public JFreeChart paint() throws Exception {
		this.queryProperties.setUomConversionEnabled(false);
		
		String groupUid = null;
		Locale locale = null;
		
		if (this.getCurrentHttpRequest()!=null) {
			HttpServletRequest request = this.getCurrentHttpRequest();
			UserSession session = UserSession.getInstance(request);
			groupUid = session.getCurrentGroupUid();
			locale = session.getUserLocale();
			this.formatter = (DecimalFormat) DecimalFormat.getInstance(locale);
			this.currentMdtUid = CommonUtils.null2EmptyString(request.getParameter("uid"));
			if ("".equals(this.currentMdtUid)) {
				Object uid = this.getCurrentUserContext().getUserSelection().getCustomProperty("mdtUid");
				if (uid!=null) this.currentMdtUid = (String) uid;
			}
		} else {
			groupUid = this.getCurrentUserContext().getUserSelection().getGroupUid();
			locale = this.getCurrentUserContext().getUserSelection().getLocale();
			Object uid = (String) this.getCurrentUserContext().getUserSelection().getCustomProperty("mdtUid");
			if (uid!=null) this.currentMdtUid = (String) uid;
		}
		this.setFileName("formation_pressure_"+this.currentMdtUid+".jpg");
		
		CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale());
		thisConverter.setReferenceMappingField(ModularFormationDynamicsTesterDetail.class, "depthSsTvd");
		String depth = "Depth (" + thisConverter.getUomSymbol() + ")";
		thisConverter.setReferenceMappingField(ModularFormationDynamicsTesterDetail.class, "formationPressureQg");
		String pressure = "Formation Pressure (" + thisConverter.getUomSymbol() + ")";
		
		ModularFormationDynamicsTester mdt = (ModularFormationDynamicsTester) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ModularFormationDynamicsTester.class, this.currentMdtUid);
		if (mdt!=null) {
			setTitle(CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, mdt.getOperationUid()));
		}

		//scatter
		XYDataset dataset = createFormationPressureDataset();
		NumberAxis xAxis = new NumberAxis(pressure);
		xAxis.setAutoRangeIncludesZero(false);
		NumberAxis yAxis = new NumberAxis(depth);
		yAxis.setAutoRangeIncludesZero(false);
		XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer(false, true);
		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer1);
		
		for (Map.Entry<String, Object> map: this.xyLineMap.entrySet()) {
			Map<String, Object> a = (Map<String, Object>) map.getValue();
			String lineGroup = (String) a.get("name");
			Double xStart = (Double) a.get("xStart");
			Double xEnd = (Double) a.get("xEnd");
			XYDataset data = (XYDataset) a.get("series");

			if (data.getSeriesCount()>1) {
				double[] coefficients = Regression.getOLSRegression(data, 0);
				Function2D curve = new LineFunction2D(coefficients[0], coefficients[1]);
				XYDataset regressionData = DatasetUtilities.sampleFunction2D(curve, xStart, xEnd, 100, "Regression " + lineGroup);
				
				int lineIdx = plot.getDatasetCount();
				plot.setDataset(lineIdx, regressionData);
				XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer(true, false);
				plot.setRenderer(lineIdx, renderer2);
				renderer2.setSeriesPaint(0, Color.BLACK);
				
				//XYToolTipGenerator tt2 = renderer2.getToolTipGenerator(0, 0);
				
			}
		}
		renderer1.setBaseItemLabelsVisible(true);
		renderer1.setBaseItemLabelGenerator(new LabelGenerator());
		renderer1.setBaseItemLabelFont(new Font("SansSerif", Font.PLAIN, 9));
		ItemLabelPosition  post = new ItemLabelPosition(ItemLabelAnchor.OUTSIDE2, TextAnchor.BASELINE_LEFT);
		renderer1.setBasePositiveItemLabelPosition(post);
		renderer1.setItemLabelAnchorOffset(2);
		
		for (int c=0;c < this.colors.length; c++) {
			renderer1.setSeriesPaint(c, this.colors[c]);
		}
		
		/*if (this.showAnnotation) {
        	if (this.annotationLists!=null) {
        		
        		for (Object x : this.annotationLists) {
                	Double offY = (this.maxDepth - this.minDepth) * 0.02;
                	Double offX = (this.maxPressure - this.minPressure) * 0.002;
        			Object[] rec = (Object[]) x; 
        			String gradient = (String) rec[2].toString();
        			
        			Double rowHeight = 0.035;
        			offY = offY + ((gradient.length() - 1) * rowHeight * (this.maxDepth - this.minDepth));
        			if ((Double) rec[1] < offY + (rowHeight * (this.maxDepth - this.minDepth))) offY = (Double) rec[1] - (rowHeight * (this.maxDepth - this.minDepth));
    
    				//if (offX > (this.maxPressure * 0.002) {
    					
    				//}else {
    					XYTextAnnotation annotation = new XYTextAnnotation(gradient, ((Double) rec[0]) + offX , ((Double) rec[1]));
        			annotation.setTextAnchor(TextAnchor.BASELINE_LEFT);
        			annotation.setOutlineVisible(false);
        			//annotation.setBackgroundPaint(Color.LIGHT_GRAY);
        			annotation.setFont(new Font("SansSerif", Font.PLAIN, 9));
        			plot.addAnnotation(annotation);
        			offY = offY - (rowHeight * (this.maxDepth - this.minDepth)); 
        			
        		}
        	}
        }*/

		
		//line
		XYDataset meanLineDataSet = createFormationPressureMeanLineDataset();
		XYItemRenderer meanLineRenderer = new StandardXYItemRenderer();
		plot.mapDatasetToRangeAxis(1, 0);
		plot.setRenderer(1, meanLineRenderer);
		plot.setDataset(1, meanLineDataSet);
		  
		
		for (int c=0;c < this.colors.length; c++) {
			meanLineRenderer.setSeriesPaint(c, this.colors[c]);
		}
		
        plot.setOrientation(PlotOrientation.VERTICAL);
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.lightGray);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.getRangeAxis().setFixedDimension(15.0);
        
		JFreeChart chart = new JFreeChart(getTitle(), JFreeChart.DEFAULT_TITLE_FONT, plot, false);
        chart.setBackgroundPaint(Color.lightGray);
        chart.addSubtitle(new TextTitle("Formation Pressure"));
        chart.getXYPlot().getRangeAxis(0).setInverted(true);
        ((NumberAxis) chart.getXYPlot().getRangeAxis()).setNumberFormatOverride(formatter);
        ((NumberAxis) chart.getXYPlot().getDomainAxis()).setNumberFormatOverride(formatter);
        			
        return chart;
	}
	
	private XYDataset createFormationPressureDataset() throws Exception {
		XYSeriesCollection dataset = new XYSeriesCollection();
		String strSql = "SELECT linegroup FROM ModularFormationDynamicsTesterDetail WHERE (isDeleted=false or isDeleted is null) and modularFormationDynamicsTesterUid=:modularFormationDynamicsTesterUid group by linegroup order by linegroup DESC";
		List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "modularFormationDynamicsTesterUid", this.currentMdtUid);
		
		for (Object a:rs) {
			if (a == null) continue;
			String lineGroup = a.toString();
			
				XYSeries scatterSeries = new XYSeries(lineGroup);
				
				String[] paramNames = {"modularFormationDynamicsTesterUid","linegroup"};
				Object[] paramValues = {this.currentMdtUid, lineGroup};
				strSql = "FROM ModularFormationDynamicsTesterDetail WHERE (isDeleted=false or isDeleted is null) and modularFormationDynamicsTesterUid=:modularFormationDynamicsTesterUid and linegroup=:linegroup";
				List<ModularFormationDynamicsTesterDetail> rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
				
				double start = 0.0;
				double end = 0.0;
				
				for (ModularFormationDynamicsTesterDetail rec:rs2) {
					if (rec.getFormationPressureQg()== null) continue;
					if (rec.getFormationPressureQg()== 0.0) continue;
					if (rec.getDepthSsTvd()== null) continue;
					scatterSeries.add(rec.getFormationPressureQg(), rec.getDepthSsTvd());
					if (start==0.0) start = rec.getFormationPressureQg();
					if (start > rec.getFormationPressureQg()) start = rec.getFormationPressureQg();
					if (end < rec.getFormationPressureQg()) end = rec.getFormationPressureQg(); 
					
				}
				
				dataset.addSeries(scatterSeries);
				
				if (start>0 && end>0 && !"".equals(lineGroup)) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("name", lineGroup);
					map.put("xStart", start);
					map.put("xEnd", end);
					map.put("series", new XYSeriesCollection(scatterSeries));
					this.xyLineMap.put(lineGroup, map);
				}
			
		}
		
		return dataset;
	}
	
	
	private XYDataset createFormationPressureMeanLineDataset() throws Exception {
		XYSeriesCollection dataset = new XYSeriesCollection();
		String strSql = "SELECT linegroup FROM ModularFormationDynamicsTesterDetail WHERE (isDeleted=false or isDeleted is null) and modularFormationDynamicsTesterUid=:modularFormationDynamicsTesterUid group by linegroup order by linegroup DESC";
		List rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "modularFormationDynamicsTesterUid", this.currentMdtUid);
		for (Object a:rs) {
			if (a == null) continue;
			
			String lineGroup = a.toString();
			
			if (StringUtils.isNotBlank(lineGroup)) {
				XYSeries meanLineSeries = new XYSeries(lineGroup);
				
				String[] paramNames = {"modularFormationDynamicsTesterUid","linegroup"};
				Object[] paramValues = {this.currentMdtUid, lineGroup};
				strSql = "FROM ModularFormationDynamicsTesterDetail WHERE (isDeleted=false or isDeleted is null) and modularFormationDynamicsTesterUid=:modularFormationDynamicsTesterUid and linegroup=:linegroup order by depthSsTvd ASC";
				List<ModularFormationDynamicsTesterDetail> rs2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramNames, paramValues);
				
				double lastDepth = 0.0;
				double lastPress = 0.0;
				double firstDepth = 0.0;
				double firstPress = 0.0;
				
				int counter = 0;
				double cumval = 0.0;
				
				for (ModularFormationDynamicsTesterDetail rec:rs2) {
					if (rec.getFormationPressureQg()== null) continue;
					if (rec.getFormationPressureQg()== 0.0) continue;
					if (rec.getDepthSsTvd()== null) continue;
					
					if (firstDepth == 0.0) firstDepth = rec.getDepthSsTvd();
					if (firstPress == 0.0) firstPress = rec.getFormationPressureQg();
					
					if (lastDepth > 0 && lastPress > 0) {
						cumval += (rec.getDepthSsTvd() - lastDepth) / (rec.getFormationPressureQg() - lastPress);
						counter ++;
					}
					lastDepth = rec.getDepthSsTvd();
					lastPress = rec.getFormationPressureQg();
					
				}
				
				double middepth = (lastDepth + firstDepth) / 2;
				double midpress = (lastPress + firstPress) / 2;
				
				double average = cumval / counter;
	
				meanLineSeries.add((midpress - ((middepth - firstDepth) / average)), firstDepth);
				meanLineSeries.add((((lastDepth - middepth) / average) + midpress), lastDepth);
				dataset.addSeries(meanLineSeries);
			}
			
			
			
		}
		
		return dataset;
	}
	
	private class LabelGenerator extends StandardCategoryItemLabelGenerator implements XYItemLabelGenerator{
		
		public String generateLabel(XYDataset arg0, int arg1, int arg2) {
			// TODO Auto-generated method stub
			if (arg0 !=null) {
				Double gradient =  arg0.getXValue(arg1, arg2) / arg0.getYValue(arg1, arg2);  
				return formatter.format(gradient);
			}
			
			return null;
		}
	}

	public void dispose() {
		this.xyLineMap.clear();
		this.currentMdtUid = null;
	}
}
