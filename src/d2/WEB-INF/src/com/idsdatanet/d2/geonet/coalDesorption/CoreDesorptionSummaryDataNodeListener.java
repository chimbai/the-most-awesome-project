package com.idsdatanet.d2.geonet.coalDesorption;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.CoalDesorption;
import com.idsdatanet.d2.core.model.CoalDesorptionDetails;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CoreDesorptionSummaryDataNodeListener extends EmptyDataNodeListener {

	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean,
			CommandBeanTreeNode node, UserSession session,
			DataNodeProcessStatus status, HttpServletRequest request)
			throws Exception {
		
		Object obj = node.getData();
		if(obj instanceof CoalDesorption){
			CoalDesorption thisCoalDesorption = (CoalDesorption) obj;
			String coalDesorptionDetailsUid = null;
			String sql = "FROM CoalDesorptionDetails WHERE (isDeleted IS NULL OR isDeleted=FALSE) AND coalDesorptionUid=:coalDesorptionUid";
			List<CoalDesorptionDetails> lsResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] {"coalDesorptionUid"}, new Object[] {thisCoalDesorption.getCoalDesorptionUid()});
			if(lsResult.size()>0){
				CoalDesorptionDetails thisCoalDesorptionDetails = (CoalDesorptionDetails) lsResult.get(0);
				if(thisCoalDesorptionDetails.getCoalDesorptionDetailsUid()!=null)
					coalDesorptionDetailsUid = thisCoalDesorptionDetails.getCoalDesorptionDetailsUid();
				
				String rawGc = null;
				String dafGc = null;
				
				if(node.getDynaAttr().get("rawGc")!=null){
					rawGc = node.getDynaAttr().get("rawGc").toString();
					String sqlUpdate = "UPDATE CoalDesorptionDetails SET rawGc=:rawGc WHERE coalDesorptionDetailsUid=:coalDesorptionDetailsUid";
					String[] paramNames = {"rawGc", "coalDesorptionDetailsUid"};
					Object[] paramValues = {(StringUtils.isNotBlank(rawGc)?Double.parseDouble(rawGc):null), coalDesorptionDetailsUid};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sqlUpdate, paramNames, paramValues);
				}
					
				if(node.getDynaAttr().get("dafGc")!=null){
					dafGc = node.getDynaAttr().get("dafGc").toString();
					String sqlUpdate = "UPDATE CoalDesorptionDetails SET dafGc=:dafGc WHERE coalDesorptionDetailsUid=:coalDesorptionDetailsUid";
					String[] paramNames = {"dafGc", "coalDesorptionDetailsUid"};
					Object[] paramValues = {(StringUtils.isNotBlank(dafGc)?Double.parseDouble(dafGc):null), coalDesorptionDetailsUid};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(sqlUpdate, paramNames, paramValues);
				}
			}else{
				commandBean.getSystemMessage().addWarning("Sample No.: "+ thisCoalDesorption.getSampleNo() + " - Please create Desorption Details before add the values for Raw GC or DAF GC");
			}
		}
	}

	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		Object obj = node.getData();
		if(obj instanceof CoalDesorption){
			CoalDesorption thisCoalDesorption = (CoalDesorption) obj;
			CustomFieldUom thisConverter=null;
			Double rawGc = null;
			Double dafGc = null;
			String sqlCoalDesorptionDetails = "FROM CoalDesorptionDetails " +
					"WHERE (isDeleted IS NULL OR isDeleted=FALSE) " +
					"AND coalDesorptionUid=:coalDesorptionUid";
			String[] paramNames = {"coalDesorptionUid"};
			Object[] paramValues = {thisCoalDesorption.getCoalDesorptionUid()};
			List<CoalDesorptionDetails> lsResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sqlCoalDesorptionDetails, paramNames, paramValues);
			if(lsResult.size()>0){
				CoalDesorptionDetails thisCoalDesorptionDetails = (CoalDesorptionDetails) lsResult.get(0);
				if(thisCoalDesorptionDetails.getRawGc()!=null)
					rawGc = thisCoalDesorptionDetails.getRawGc();
				if(thisCoalDesorptionDetails.getDafGc()!=null)
					dafGc = thisCoalDesorptionDetails.getDafGc();
			}
			
			thisConverter = new CustomFieldUom(commandBean,CoalDesorptionDetails.class, "rawGc");
			node.getDynaAttr().put("rawGc", rawGc);
			if (thisConverter.isUOMMappingAvailable()) { 
				node.setCustomUOM("@rawGc", thisConverter.getUOMMapping());
			}
			
			thisConverter = new CustomFieldUom(commandBean,CoalDesorptionDetails.class, "dafGc");
			node.getDynaAttr().put("dafGc", dafGc);
			if (thisConverter.isUOMMappingAvailable()) { 
				node.setCustomUOM("@dafGc", thisConverter.getUOMMapping());
			}
		}
		
	}

	
	
}
