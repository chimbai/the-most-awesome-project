package com.idsdatanet.d2.geonet.wirelineRun;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.WirelineInterruptions;
import com.idsdatanet.d2.core.model.WirelineLog;
import com.idsdatanet.d2.core.model.WirelinePreLogging;
import com.idsdatanet.d2.core.model.WirelineRun;
import com.idsdatanet.d2.core.model.WirelineSuite;
import com.idsdatanet.d2.core.model.WirelineTool;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WirelineRunDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}
	
	public String generateHQLConditionClause(String conditionClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode) throws Exception {
		
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		String currentClass	= meta.getTableClass().getSimpleName();
		
		//ADDED CHECKING TO FILTER WIRELINE_SUITE BASED ON rigUpDateTime
		if(currentClass.equals("WirelineSuite")) {
			String customCondition = "";
			String thisDailyUid = userSelection.getDailyUid();
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisDailyUid);
			if(daily!=null){
				Date thisCurrentDate = null;		
				thisCurrentDate = daily.getDayDate();
				customCondition = "operationUid = session.operationUid AND date(rigUpDateTime) <= :thisCurrentDate AND (date(rigDownDateTime) >= :thisCurrentDate OR rigDownDateTime is null)";
				query.addParam("thisCurrentDate", thisCurrentDate);
			}else{
				customCondition = "operationUid = session.operationUid";
			}
				
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
		}
		
		if(currentClass.equals("WirelineRun")) {
			String customCondition = "";
			String thisDailyUid = userSelection.getDailyUid();
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(thisDailyUid);
			
			Date thisCurrentDate = null;		
			thisCurrentDate = daily.getDayDate();
			customCondition = "date(rigupStartdateRigupStarttime) <= :thisCurrentDate AND (date(rigdownEnddateRigdownEndtime) >= :thisCurrentDate OR rigdownEnddateRigdownEndtime is null)";
			query.addParam("thisCurrentDate", thisCurrentDate);
			return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);			
		}		
		return null;
	}
		
	public String generateHQLFromClause(String fromClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception {
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause, CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode parentNode) throws Exception{
		return null;
	}
	
	private class ReversedDateComparator implements Comparator<java.util.Date>{
		public int compare(Date d1, Date d2){
			if(d1 != null && d2 != null){
				if(d1.getTime() > d2.getTime()) return -1;
				if(d1.getTime() < d2.getTime()) return 1;
				return 0;
			}else{
				if(d1 == null && d2 == null) return 0;
				if(d1 != null) return -1;
				return 1;
			}
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof WirelineSuite) {			
			Date rigUpDateTime = (Date) PropertyUtils.getProperty(object, "rigUpDateTime");
			Date rigDownDateTime = (Date) PropertyUtils.getProperty(object, "rigDownDateTime");
			if(rigUpDateTime != null && rigDownDateTime != null){
				//ADDED CHECKING IF RIG DOWN DATE/TIME EARLIER THAN RIG UP DATE/TIME
				if(rigUpDateTime.getTime() > rigDownDateTime.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "rigDownDateTime", "Rig Down date/time cannot be earlier than Rig Up date/time.");
					return;
				}
			}
			
			WirelineSuite wirelineSuite = (WirelineSuite)object;
			Daily dailyIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(wirelineSuite.getInDailyUid());
			Daily dailyOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(wirelineSuite.getOutDailyUid());
			
			if (dailyIn != null && dailyOut != null){
				if(dailyOut.getDayDate().before(dailyIn.getDayDate())){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "outDailyUid", "Date Out should be greater than Date In.");
					return;
				}
				if(wirelineSuite.getDateInTimeIn() != null && wirelineSuite.getDateOutTimeOut() != null){
					if(wirelineSuite.getDateInTimeIn().getTime() > wirelineSuite.getDateOutTimeOut().getTime()){
						if(dailyOut.getDayDate().after(dailyIn.getDayDate())){
							return;
						} else {
							status.setContinueProcess(false, true);
							status.setFieldError(node, "dateOutTimeOut", "Time Out should be greater than Time In.");
							return;
						}
					}
				}
			}
		}
		
		if (object instanceof WirelineRun) {
			
			WirelineRun thisWirelineRun = (WirelineRun) object;
			Double totalLengthLogged = 0.0;
			Double logTop = 0.0;
			Double logBottom = 0.0;			
			Date rigupStartdateRigupStarttime = (Date) PropertyUtils.getProperty(object, "rigupStartdateRigupStarttime");
			Date rigdownEnddateRigdownEndtime = (Date) PropertyUtils.getProperty(object, "rigdownEnddateRigdownEndtime");
			Date dateCircStartTimeCircStart = (Date) PropertyUtils.getProperty(object, "dateCircStartTimeCircStart");
			Date dateCircStoppedTimeCircStopped = (Date) PropertyUtils.getProperty(object, "dateCircStoppedTimeCircStopped");
			Date dateToolLeftbtmTimeToolLeftbtm = (Date) PropertyUtils.getProperty(object, "dateToolLeftbtmTimeToolLeftbtm");
			
			if(rigupStartdateRigupStarttime != null && rigdownEnddateRigdownEndtime != null){
				//ADDED CHECKING IF RIG DOWN DATE/TIME EARLIER THAN RIG UP DATE/TIME ON TRIP LEVEL
				if(rigupStartdateRigupStarttime.getTime() > rigdownEnddateRigdownEndtime.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "rigdownEnddateRigdownEndtime", "Rig Down date/time cannot be earlier than Rig Up date/time.");
					return;
				}
				
				//CALCULATE TOTAL LOGGING TIME DURATION ON TRIP LEVEL
				//Long duration = (rigdownEnddateRigdownEndtime.getTime() - rigupStartdateRigupStarttime.getTime()) / 1000;
				
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(rigupStartdateRigupStarttime, rigdownEnddateRigdownEndtime);
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelineRun.class, "totalLoggingTime");
				thisConverter.setBaseValue(thisDuration);				
				PropertyUtils.setProperty(object, "totalLoggingTime", (thisConverter.getConvertedValue()));
			}
			
			if(dateCircStartTimeCircStart != null) {
				List<Daily> listOfDaily = ApplicationUtils.getConfiguredInstance().getDaysInOperation(thisWirelineRun.getOperationUid());
				if (listOfDaily != null && !listOfDaily.isEmpty()) {
					Daily day = (Daily) listOfDaily.get(0);
					//Added checking if date/time circ start earlier than day 1
					if (day != null && day.getDayDate() != null && dateCircStartTimeCircStart.getTime() < day.getDayDate().getTime()) {
						status.setContinueProcess(false, true);
						status.setFieldError(node, "dateCircStartTimeCircStart", "Date and Time Circ Started cannot be earlier than the first day of operation.");
						return;
					}
				}
			}

			if (dateCircStoppedTimeCircStopped != null) {
				if(dateCircStartTimeCircStart != null){
					//ADDED CHECKING IF DATE/TIME CIRC STOPPED EARLIER THAN DATE/TIME CIRC START
					if(dateCircStoppedTimeCircStopped.getTime() < dateCircStartTimeCircStart.getTime()){
						status.setContinueProcess(false, true);
						status.setFieldError(node, "dateCircStoppedTimeCircStopped", "Date and Time Circ Stopped cannot be earlier than Date and Time Circ Started.");
						return;
					}
				}
				if(dateToolLeftbtmTimeToolLeftbtm != null) {
					//ADDED CHECKING IF Date and Time Tool left Max Depth Earlier THAN DATE/TIME CIRC STOPPED
					if(dateCircStoppedTimeCircStopped.getTime() > dateToolLeftbtmTimeToolLeftbtm.getTime()){
						status.setContinueProcess(false, true);
						status.setFieldError(node, "dateToolLeftbtmTimeToolLeftbtm", "Date and Time Circ Stopped cannot be later than Date and Time Tool Left Max Depth.");
						return;
					}
				}
			}
			
			//Validate Date Run and Date Out
			String runDailyUid = thisWirelineRun.getWirelineRunDailyUid();
			String outDailyUid = thisWirelineRun.getWirelineOutDailyUid();
			if((StringUtils.isNotBlank(runDailyUid)) && (StringUtils.isNotBlank(outDailyUid))) {
				Daily dailyIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(runDailyUid);
				Daily dailyOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(outDailyUid);
				if(dailyOut==null){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "wirelineOutDailyUid", "Please input valid date.");
					return;
				}else if(dailyIn==null){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "wirelineRunDailyUid", "Please input valid date.");
					return;
				}else if(dailyOut.getDayDate().before(dailyIn.getDayDate())){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "wirelineRunDailyUid", "Date Run cannot be greater than Date Out.");
					return;
				}
			}else if(StringUtils.isBlank(runDailyUid) && StringUtils.isBlank(outDailyUid)){
				status.setContinueProcess(true, false);
				return;
			}else if((StringUtils.isBlank(runDailyUid)) && (StringUtils.isNotBlank(outDailyUid))){
				status.setContinueProcess(false, true);
				status.setFieldError(node, "wirelineRunDailyUid", "This field is required.");
				return;
			}else if(StringUtils.isNotBlank(runDailyUid) && StringUtils.isBlank(outDailyUid)){
				Daily dailyIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(runDailyUid);
				if(dailyIn==null){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "wirelineRunDailyUid", "Please input valid date.");
					return;
				}
			}
			
			//CALCULATE TOTAL LOGGED DEPTH ON TRIP LEVEL
			if(thisWirelineRun.getLogTop() != null){				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelineRun.class, "logTop");
				thisConverter.setBaseValueFromUserValue(thisWirelineRun.getLogTop());
				logTop = thisConverter.getBasevalue();
			}
			
			if(thisWirelineRun.getLogBottom() != null){				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelineRun.class, "logBottom");
				thisConverter.setBaseValueFromUserValue(thisWirelineRun.getLogBottom());
				logBottom = thisConverter.getBasevalue();
			}
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelineRun.class, "totalLengthLogged");
			
			totalLengthLogged = logBottom - logTop;
			thisConverter.setBaseValue(totalLengthLogged);
			thisWirelineRun.setTotalLengthLogged(thisConverter.getConvertedValue());
			
			//AUTO POPULATION FOR SERVICE COMPANY
			WirelineSuite thisWirelineSuite = (WirelineSuite) node.getParent().getData();

			String serviceCompany = thisWirelineSuite.getServiceCompany();
			String svcCo = thisWirelineRun.getSvcCo();

			if (svcCo != null && StringUtils.isNotBlank(svcCo)){
				thisWirelineRun.setSvcCo(svcCo);
			}else thisWirelineRun.setSvcCo(serviceCompany);
		}
		
		if (object instanceof WirelineInterruptions) {			
							
			Date interruptStartDateTime = (Date) PropertyUtils.getProperty(object, "interruptStartDateTime");
			Date interruptEndDateTime = (Date) PropertyUtils.getProperty(object, "interruptEndDateTime");
			Long totalTime = null;
			if(interruptStartDateTime != null && interruptEndDateTime != null){
				//ADDED CHECKING IF INTERRUPTIONS END DATE/TIME EARLIER THAN INTERRUPTIONS START DATE/TIME ON INTERRUPTIONS LEVEL
				if(interruptStartDateTime.getTime() > interruptEndDateTime.getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "interruptEndDateTime", "End date/time cannot be earlier than Start date/time.");
					return;
				}
				
				//CALCULATE TOTAL INTERRUPTIONS TIME ON INTERRUPTION LEVEL
				//totalTime = (interruptEndDateTime.getTime() - interruptStartDateTime.getTime()) / 1000;
				
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(interruptStartDateTime, interruptEndDateTime);
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelineInterruptions.class, "totalTime");
				thisConverter.setBaseValue(thisDuration);	
				
				PropertyUtils.setProperty(object, "totalTime", (thisConverter.getConvertedValue()));
			}else{
				//IF START/END TIME IS NULL, SET TOTAL TIME TO NULL
				PropertyUtils.setProperty(object, "totalTime", totalTime);
			}
		}
		
		if (object instanceof WirelineLog) {
			
			List<Date> endDatetimeList = new ArrayList<Date>();
			
			CommandBeanTreeNode parentNode = node.getParent();
			for (Iterator i = parentNode.getList().values().iterator(); i.hasNext();) {
				Map items = (Map) i.next();
				
				for (Iterator j = items.values().iterator(); j.hasNext();) {
					CommandBeanTreeNodeImpl childNode = (CommandBeanTreeNodeImpl) j.next();
					if (!childNode.isTemplateNode() && childNode.getData() != null && childNode.getData().getClass().equals(object.getClass())) {
						Boolean isDeleted = (Boolean) PropertyUtils.getProperty(childNode.getData(), "isDeleted"); // shouldn't need this, temporary fix for Flex client
						if (isDeleted == null || !isDeleted.booleanValue()) {
							endDatetimeList.add((Date) PropertyUtils.getProperty(childNode.getData(), "endTime"));
						}
					}
				}
			}
			Collections.sort(endDatetimeList, new ReversedDateComparator());
			commandBean.getRoot().getDynaAttr().put("endTime", endDatetimeList);
			
			Date start = (Date) PropertyUtils.getProperty(object, "startTime");
			Date end = (Date) PropertyUtils.getProperty(object, "endTime");
			
			// if user didn't enter start time, find the first smaller than the current end time.
			if(start == null) {
				boolean found = false;
				for(Date endTime : endDatetimeList) {
					Calendar currentEndDatetime = Calendar.getInstance();
					currentEndDatetime.setTime(end);
					
					Calendar cachedEndDatetime = Calendar.getInstance();
					cachedEndDatetime.setTime(endTime);
					
					if(cachedEndDatetime.before(currentEndDatetime)) {
						PropertyUtils.setProperty(object, "startTime", cachedEndDatetime.getTime());
						found = true;
						break;
					}
				}
				// if no matches, set to 00:00
				if(!found) {
					Calendar zero = Calendar.getInstance();
					zero.clear();
					zero.set(Calendar.HOUR_OF_DAY, 0);
					zero.set(Calendar.MINUTE, 0);
					zero.set(Calendar.SECOND, 0);
					zero.set(Calendar.MILLISECOND, 0);
					PropertyUtils.setProperty(object, "startTime", zero.getTime());
				}
				start = (Date) PropertyUtils.getProperty(object, "startTime");
			}
			
			WirelineLog thisWirelineLog = (WirelineLog) object;
			
			String thisLogDailyUid = thisWirelineLog.getDailyUid();
			if (thisLogDailyUid!=null)
			{
				Object objDaily = ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, thisLogDailyUid);
				if (objDaily!=null)
				{
					Daily daily=(Daily) objDaily;
					if (thisWirelineLog.getStartTime()!=null)
					{
						this.setTimeField(thisWirelineLog, daily.getDayDate(), "startTime");
					}
					if (thisWirelineLog.getEndTime()!=null)
					{
						this.setTimeField(thisWirelineLog, daily.getDayDate(), "endTime");
					}
				}
			}				
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelineLog.class, "duration");
			if (thisWirelineLog.getEndTime() != null &&  thisWirelineLog.getStartTime() != null) {
				double thisDuration = 0.0;
				
				if(thisWirelineLog.getStartTime().getTime() > thisWirelineLog.getEndTime().getTime()){
					status.setContinueProcess(false, true);
					status.setFieldError(node, "startTime", "Start time can not be greater than end time.");
					return;
				}
				
				//thisDuration = thisWirelineLog.getEndTime().getTime() - thisWirelineLog.getStartTime().getTime();
				//call to method to calculate duration base on 2 dates;
				thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(thisWirelineLog.getStartTime(), thisWirelineLog.getEndTime());
				
				thisConverter.setBaseValue(thisDuration);
				thisWirelineLog.setDuration(thisConverter.getConvertedValue());
			}
			else
			{
				thisWirelineLog.setDuration(null);
			}
			
			
		}
		
	}
	public static void setTimeField(Object object, Date thisDate, String fieldName) throws Exception{
		Calendar dayDate = Calendar.getInstance();
		dayDate.setTime(thisDate);
				
		if (PropertyUtils.getProperty(object, fieldName) != null)
		{
			Calendar TimeFieldDatetime = Calendar.getInstance();
			TimeFieldDatetime.setTime((Date) PropertyUtils.getProperty(object, fieldName));
			TimeFieldDatetime.set(Calendar.YEAR, dayDate.get(Calendar.YEAR));
			TimeFieldDatetime.set(Calendar.MONTH, dayDate.get(Calendar.MONTH));
			TimeFieldDatetime.set(Calendar.DAY_OF_MONTH, dayDate.get(Calendar.DAY_OF_MONTH));
			PropertyUtils.setProperty(object, fieldName, TimeFieldDatetime.getTime());
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof WirelineSuite) {
			WirelineSuite wirelinesuite = (WirelineSuite) object;
			
			String wirelineSuiteUid = wirelinesuite.getWirelineSuiteUid();
			String operationUid = wirelinesuite.getOperationUid();
			Double totalLengthLogged = 0.00;
			Double totalLoggingTime = 0.00;
			Double totalLostTime = 0.00;
			Double totalInterruptionsTime = 0.00;
			Double depthDifference = 0.0;
			Double loggingEfficiency = 0.00;
			
			//CALCULATE DIFFERENCE FOR DRILLER'S DEPTH AND LOGGER'S DEPTH
			Double drillerDepth = wirelinesuite.getDrillerDepthMdMsl();
			Double loggerDepth = wirelinesuite.getLoggerDepthMdMsl();
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelineSuite.class, "DrillerDepthMdMsl");
		
			if (drillerDepth == null) drillerDepth =0.0;
			if (loggerDepth == null) loggerDepth =0.0;
			
			depthDifference = (loggerDepth - drillerDepth);
			
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@depthDifference", thisConverter.getUOMMapping());
			}
			node.getDynaAttr().put("depthDifference", depthDifference);
			
			//TO SUM LOGGEDDEPTH AND TIME FROM WIRELINE RUN
			String[] paramsFields = {"operationUid", "wirelineSuiteUid"};
			Object[] paramsValues = new Object[2];
			paramsValues[0] = operationUid; 
			paramsValues[1] = wirelineSuiteUid;
			
			String strSql = "SELECT SUM(totalLengthLogged) as totalLengthLogged, SUM(totalLoggingTime) as totalLoggingTime, SUM(totalLostTime) as totalLostTime FROM WirelineRun WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND wirelineSuiteUid = :wirelineSuiteUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			
			if(lstResult.size() > 0){
				Object[] thisResult = (Object[]) lstResult.get(0);
								
				thisConverter.setReferenceMappingField(WirelineRun.class, "total_length_logged");
				Double dblConvertedValue = 0.00;			
				if (thisResult[0] != null) {				
					totalLengthLogged = Double.parseDouble(thisResult[0].toString());
					thisConverter.setBaseValue(totalLengthLogged);
					dblConvertedValue = thisConverter.getConvertedValue();
				}
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@totalLengthLogged", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalLengthLogged", dblConvertedValue);
				
				thisConverter.setReferenceMappingField(WirelineRun.class, "total_logging_time");
				dblConvertedValue = 0.00;
				if (thisResult[1] != null) {				
					totalLoggingTime = Double.parseDouble(thisResult[1].toString());
					thisConverter.setBaseValue(totalLoggingTime);
					dblConvertedValue = thisConverter.getConvertedValue();
				}
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@totalLoggingTime", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalLoggingTime", dblConvertedValue);
				
				thisConverter.setReferenceMappingField(WirelineRun.class, "total_lost_time");
				dblConvertedValue = 0.00;
				if (thisResult[2] != null) {				
					totalLostTime = Double.parseDouble(thisResult[2].toString());
					thisConverter.setBaseValue(totalLostTime);
					dblConvertedValue = thisConverter.getConvertedValue();
				}
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@totalLostTime", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalLostTime", dblConvertedValue);
			}
			
			//CALCULATE TOTALTIME FROM WIRELINE_INTERRUPTIONS			
			String[] paramsFields2 = {"operationUid", "wirelineSuiteUid"};
			Object[] paramsValues2 = new Object[2];
			paramsValues2[0] = operationUid; 
			paramsValues2[1] = wirelineSuiteUid;
			
			String strSql2 = "SELECT SUM(totalTime) as totalTime FROM WirelineInterruptions WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND wirelineSuiteUid = :wirelineSuiteUid";
			List lstResult2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
			
			if(lstResult2.size() > 0){
				Object thisResult2 = (Object) lstResult2.get(0);
				
				//CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelineInterruptions.class, "totalTime");
				thisConverter.setReferenceMappingField(WirelineInterruptions.class, "total_time");
				Double dblConvertedValue = 0.00;			
				if (thisResult2 != null) {				
					totalInterruptionsTime = Double.parseDouble(thisResult2.toString());
					thisConverter.setBaseValue(totalInterruptionsTime);
					dblConvertedValue = thisConverter.getConvertedValue();
				}
				if (thisConverter.isUOMMappingAvailable()){
					node.setCustomUOM("@totalTime", thisConverter.getUOMMapping());
				}
				node.getDynaAttr().put("totalTime", dblConvertedValue);
			}
			
			//CALCULATE LOGGING EFFICIENCY			
			DecimalFormat formatter = new DecimalFormat("#0.00");
			
			if(totalLoggingTime - totalInterruptionsTime <= 0){
				loggingEfficiency = 0.00;
			}else{
				//CALCULATE LOGGING EFFICIENCY AS PERCENTAGE VALUE
				loggingEfficiency = (1 - (totalLostTime/(totalLoggingTime - totalInterruptionsTime)))*100;
			}
			node.getDynaAttr().put("loggingEfficiency", CommonUtils.roundUpFormat(formatter, loggingEfficiency));
			
			//get Max deviation from survey - GEONET 2
			String maxSurveyAngleDepth = CommonUtil.getConfiguredInstance().getMaxDeviationDepth(userSelection);
			node.getDynaAttr().put("maxDeviation", maxSurveyAngleDepth);
		}
		
		//THIS IS TO GET MUD PROPERTIES DATA ON WIRELINE RUN SECTION
		if (object instanceof WirelineRun) {
			WirelineRun wirelinerun = (WirelineRun) object;	
			WirelineRunUtils.getMudPropertiesInfo(node, wirelinerun.getMudPropertiesUid(), userSelection.getGroupUid());
		}
		
		if (object instanceof WirelineTool) {
			WirelineTool wirelineTool = (WirelineTool) object;	
			Date wirelineFailureDatetime = wirelineTool.getFailureDatetime();
			if (wirelineFailureDatetime==null){
				node.getDynaAttr().put("isEquipmentFailure", "0");
			}else{
				node.getDynaAttr().put("isEquipmentFailure", "1");
			}
		}
		if(object instanceof WirelineSuite) {
			node.getDynaAttr().put("wirelineSuitePage" , "wirelinerun.html");
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof WirelineSuite) {
			WirelineSuite thisWirelineSuite = (WirelineSuite) object;
			
			//GET REPORT DAYDATE ON ADD NEW 
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null) return;
			thisWirelineSuite.setRigUpDateTime(daily.getDayDate());
		}
	}

	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object object = node.getData();		
		
		// update wireline_pre_logging table to unset the link with deleted wireline run data
		if (object instanceof WirelineRun) {
			WirelineRun wirelineRun = (WirelineRun) object;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			String strSql = "From WirelinePreLogging where (isDeleted is null or isDeleted = '') and wirelineRunUid=:WirelineRunUid";	
			List<WirelinePreLogging>lstWirelinePreLogging = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "WirelineRunUid", wirelineRun.getWirelineRunUid());
			if (lstWirelinePreLogging != null && lstWirelinePreLogging.size() > 0) {
				
				for (WirelinePreLogging rs: lstWirelinePreLogging){
					rs.setWirelineRunUid(null);
					rs.setWirelineRunNumber(null);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rs, qp);
				}
			}
		}
		
		if (object instanceof WirelineSuite) {
			WirelineSuite wirelinesuite = (WirelineSuite) object;
			QueryProperties qp = new QueryProperties();
			qp.setUomConversionEnabled(false);
			
			String strSql = "From WirelinePreLogging where (isDeleted is null or isDeleted = '') and wirelineSuiteUid=:wirelineSuiteUid";	
			List<WirelinePreLogging>lstWirelinePreLogging = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wirelineSuiteUid", wirelinesuite.getWirelineSuiteUid());
			if (lstWirelinePreLogging != null && lstWirelinePreLogging.size() > 0) {
				
				for (WirelinePreLogging rs: lstWirelinePreLogging){
					rs.setWirelineRunUid(null);
					rs.setWirelineRunNumber(null);
					rs.setWirelineSuiteNumber(null);
					rs.setWirelineSuiteUid(null);
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(rs, qp);
				}
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object object = node.getData();

		String isEquipmentFailure = (String)node.getDynaAttr().get("isEquipmentFailure");
		
		if (!"0".equalsIgnoreCase(isEquipmentFailure)) {
			if (object instanceof WirelineRun) {
				//update run number and company
				String strInCondition = "";
				WirelineRun wirelineRun = (WirelineRun) object;
				String strSql = "From WirelineTool where (isDeleted is null or isDeleted = '') and wirelineRunUid=:WirelineRunUid";	
				List<WirelineTool>lstWirelineTool = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "WirelineRunUid", wirelineRun.getWirelineRunUid());
				
				if (lstWirelineTool != null && lstWirelineTool.size() > 0) {
					
					for (WirelineTool rs: lstWirelineTool){
						strInCondition = strInCondition + "'" + rs.getWirelineToolUid() + "',"; 
					}
				}
				
				strInCondition = StringUtils.substring(strInCondition, 0, -1);
				
				if (StringUtils.isNotBlank(strInCondition)) {
					String strSqlUpdate = "UPDATE EquipmentFailure SET runNumber=:runNumber, company=:companyUid WHERE toolUid IN (" + strInCondition +  ") and toolType='wireline_tool'";	
					String[] paramNames = {"runNumber","companyUid"};
					String[] paramValues = {wirelineRun.getRunNumber(), wirelineRun.getSvcCo()};
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);
				}
			}
			
			if (object instanceof WirelineTool){
				WirelineTool wirelineTool = (WirelineTool) object;
				
				String toolDesc = wirelineTool.getToolCode();
				Date failureDateTime = wirelineTool.getFailureDatetime();
				String toolUid = wirelineTool.getWirelineToolUid();

				String strSqlUpdate = "UPDATE EquipmentFailure SET failedItem =:failItem, dateFailStart=:failureDateTime WHERE toolUid =:toolUid and toolType='wireline_tool'";	
				String[] paramNames = {"failItem","failureDateTime", "toolUid"};
				Object[] paramValues = {toolDesc, failureDateTime, toolUid};
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSqlUpdate, paramNames, paramValues);

				//recalculate cost
				Double dailyCost = null;
				if (session != null) dailyCost = CommonUtil.getConfiguredInstance().getDailyDayCost(failureDateTime, session);
				CommonUtil.getConfiguredInstance().calculateCostIncurred(failureDateTime, dailyCost, "wireline_tool");
				
			}
		}
	}

}
