package com.idsdatanet.d2.geonet.sidewallCore;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WirelineRunLookupHandler implements LookupHandler {
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception {
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		String operationUid = userSelection.getOperationUid();
		
		String strSql = "SELECT r.wirelineRunUid, s.suiteNumber, r.runNumber FROM WirelineRun r, WirelineSuite s " +
				" WHERE r.wirelineSuiteUid=s.wirelineSuiteUid " +
				" AND (r.isDeleted=false or r.isDeleted is null) " +
				" AND (s.isDeleted=false or s.isDeleted is null) " +
				" AND r.operationUid=:operationUid " +
				" ORDER BY s.suiteNumber, r.runNumber";
		List<Object[]> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
		
		for (Object[] rec : rs) {
			String wirelineUid = rec[0].toString();
			String suiteNumber = rec[1].toString();
			String runNumber = rec[2].toString();
			
			result.put(wirelineUid, new LookupItem(wirelineUid, "Suite#"+suiteNumber+" Run#"+runNumber));
		}
			
		return result;
	}
}
