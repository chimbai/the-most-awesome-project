package com.idsdatanet.d2.geonet.geolshows;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Geolshows;
import com.idsdatanet.d2.core.model.LithologyShows;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class GeolShowsDataNodeListener extends EmptyDataNodeListener implements DataLoaderInterceptor {

	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		// TODO Auto-generated method stub
		
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		String customCondition = "";
		
		if (request != null) {
			String lithologyShowsUid = request.getParameter("lithologyShowsUid");
			if (lithologyShowsUid==null) lithologyShowsUid = "";
			
			if (!"".equals(lithologyShowsUid)) {
				String currentClass	= meta.getTableClass().getSimpleName();
				if(currentClass.equals("Geolshows")) {
					customCondition = "operationUid = session.operationUid AND lithologyShowsUid=:lithologyShowsUid";
					query.addParam("lithologyShowsUid", lithologyShowsUid);
				}
			}
		}
		
		if ("".equals(customCondition)) {
			customCondition = "operationUid = session.operationUid";
		}
		
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		if (request!=null) {
			String newRecordInputMode = request.getParameter("newRecordInputMode");
			String lithologyShowsUid = request.getParameter("lithologyShowsUid");
			if (lithologyShowsUid==null) lithologyShowsUid = "";
			if ("1".equals(newRecordInputMode) && !"".equals(lithologyShowsUid)) {
				
				commandBean.getFlexClientControl().setReloadAfterPageCancel(true);
				
				if (node.getData() instanceof Geolshows) {
					Geolshows geolshows = (Geolshows) node.getData();
					
					String queryString = "FROM LithologyShows WHERE (isDeleted=false or isDeleted is null) and lithologyShowsUid=:lithologyShowsUid and operationUid=:operationUid";
					List<LithologyShows> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"lithologyShowsUid","operationUid"}, new Object[] {lithologyShowsUid, UserSession.getInstance(request).getCurrentOperationUid()}, qp);
					if (list.size()>0) {
						LithologyShows rec = list.get(0);
						
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Geolshows.class, "fromDepthMdMsl");
						thisConverter.setBaseValue(rec.getTopMdMsl());
						geolshows.setFromDepthMdMsl(thisConverter.getConvertedValue());
						
						thisConverter.setReferenceMappingField(Geolshows.class, "toDepthMdMsl");
						thisConverter.setBaseValue(rec.getBottomMdMsl());
						geolshows.setToDepthMdMsl(thisConverter.getConvertedValue());
						
						geolshows.setLithology(rec.getLithology());
						geolshows.setLithologyShowsUid(lithologyShowsUid);
						
						
						//node.getDynaAttr().put("lithologyShowsUid", lithologyShowsUid);
						node.getDynaAttr().put("hasLithologyShows", "1");
					}
				}
			}
		}		
		
	}

	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		if (node.getData() instanceof Geolshows) {
			Geolshows geolshows = (Geolshows) node.getData();
			if (geolshows.getLithologyShowsUid()!=null) {
				String queryString = "FROM LithologyShows where (isDeleted=false or isDeleted is null) AND lithologyShowsUid=:lithologyShowsUid and operationUid=:operationUid";
				List<LithologyShows> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"lithologyShowsUid","operationUid"}, new Object[] {geolshows.getLithologyShowsUid(), userSelection.getOperationUid()});
				if (list.size()>0) {
					node.getDynaAttr().put("hasLithologyShows", "1");
				}
			}
		}		
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof Geolshows){
			
			Geolshows thisGeolshows = (Geolshows) object;
			
			//Calculate thickness
			GeolShowsUtils.calculateThickness(commandBean, node);
			
			//Calculate rating and rating score
			GeolShowsUtils.calculateRatingScore(commandBean, node, session);
			
			if (thisGeolshows.getFromDepthMdMsl()!=null && thisGeolshows.getToDepthMdMsl()!=null) {
				if (thisGeolshows.getFromDepthMdMsl() > thisGeolshows.getToDepthMdMsl()) {
					status.setFieldError(node, "toDepthMdMsl", "Depth To must greater than Depth From.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (thisGeolshows.getShowPercentageFrom()!=null && thisGeolshows.getShowPercentage()!=null) {
				if (thisGeolshows.getShowPercentageFrom() > thisGeolshows.getShowPercentage()) {
					status.setFieldError(node, "showPercentage", "Range To must greater than Range From.");
					status.setContinueProcess(false, true);
					return;
				}
			}
		}
	}
}
