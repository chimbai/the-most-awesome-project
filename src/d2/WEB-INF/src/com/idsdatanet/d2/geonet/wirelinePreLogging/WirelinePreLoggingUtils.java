package com.idsdatanet.d2.geonet.wirelinePreLogging;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.model.LookupCompany;
import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.WirelinePreLogging;
import com.idsdatanet.d2.core.model.WirelinePreLoggingZone;
import com.idsdatanet.d2.core.model.WirelineRun;
import com.idsdatanet.d2.core.model.WirelineSuite;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

/**
 * Method to get Daily, Casing, Survey, WellOperatiom, Formation Data
 * Method to create new Wireline Suite, Wireline Run upon create new Wireline Pre Logging
 * @author Jackson
 *
 */
public class WirelinePreLoggingUtils {
	
	private static final String TVDSS_REFERENCE_FIELD = "sampleTopTvdMsl";
	
	/**
	 * Method to get Daily, Survey and Casing info when on select Daily in wireline Pre Logging
	 * @param request
	 * @param session
	 * @param wirelinePreLogging object from onSumbitForServerSideProccess Job and afterTemplateNodeCreated 
	 * @throws Exception
	 * @return No return
	 */
	public static void getDailySurveyCasingInfo(CommandBean commandBean, HttpServletRequest request, UserSession session, WirelinePreLogging wirelinePreLogging) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String dailyUid = wirelinePreLogging.getDailyUid();
		String operationUid = session.getCurrentOperationUid();
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelinePreLogging.class, "drillerTdMdMsl");
		if(StringUtils.isNotBlank(dailyUid)){
			
			{
				String reportType = CommonUtil.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist(operationUid);
				String sql = "FROM ReportDaily WHERE (isDeleted is null or isDeleted=false) and reportType=:reportType and dailyUid=:dailyUid and operationUid=:operationUid";
				String[] paramsFields = {"operationUid", "dailyUid","reportType"};
				Object[] paramsValues = {operationUid, dailyUid, reportType};
				List<ReportDaily> reportDailyList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
				
				Double lastLinerMdMsl = null;
				Double lastCsgshoeMdMsl = null;
				
				if (reportDailyList.size() > 0) {
					
					ReportDaily reportDaily = (ReportDaily) reportDailyList.get(0);
					
					if(reportDaily.getDepthMdMsl() != null) {
						thisConverter.setBaseValue(reportDaily.getDepthMdMsl());
						wirelinePreLogging.setDrillerTdMdMsl(thisConverter.getConvertedValue());
					}
					
					if (reportDaily.getLastLinerMdMsl() != null) {						
						lastLinerMdMsl = reportDaily.getLastLinerMdMsl();
					}
					if(reportDaily.getLastCsgshoeMdMsl()!=null) {						
						lastCsgshoeMdMsl = reportDaily.getLastCsgshoeMdMsl();
					}
					
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "casingShoeMdMsl");
					if(lastLinerMdMsl != null && lastCsgshoeMdMsl != null) {
						if(lastLinerMdMsl < lastCsgshoeMdMsl) {
							
							thisConverter.setBaseValue(lastCsgshoeMdMsl);
							wirelinePreLogging.setCasingShoeMdMsl(thisConverter.getConvertedValue());
						} else if(lastLinerMdMsl > lastCsgshoeMdMsl) {
							
							thisConverter.setBaseValue(lastLinerMdMsl);
							wirelinePreLogging.setCasingShoeMdMsl(thisConverter.getConvertedValue());
						}
					} else {
						if(lastLinerMdMsl != null) {
							thisConverter.setBaseValue(lastLinerMdMsl);
							wirelinePreLogging.setCasingShoeMdMsl(thisConverter.getConvertedValue());
						} else if (lastCsgshoeMdMsl != null) {
							thisConverter.setBaseValue(lastCsgshoeMdMsl);
							wirelinePreLogging.setCasingShoeMdMsl(thisConverter.getConvertedValue());
						} else {
							wirelinePreLogging.setCasingShoeMdMsl(null);
						}
					}
					
					if(reportDaily.getLastCsgshoeMdMsl()==null && reportDaily.getLastLinerMdMsl()==null) {
						wirelinePreLogging.setCasingShoeMdMsl(null);
					}
					
					if(reportDaily.getLastHolesize() != null) {
						thisConverter.setReferenceMappingField(WirelinePreLogging.class, "bitSize");
						thisConverter.setBaseValue(reportDaily.getLastHolesize());
						wirelinePreLogging.setBitSize(thisConverter.getConvertedValue());
					}
				}
			}
			
			//CASING
			Object objectCasing = CommonUtil.getConfiguredInstance().lastCasingSection(session.getCurrentGroupUid(), session.getCurrentWellboreUid(), dailyUid);
			
			if(objectCasing != null ){				
				
				CasingSection casingSection = (CasingSection) objectCasing;		
				
				if(casingSection.getCasingOd() != null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "casingOd");
					thisConverter.setBaseValue(casingSection.getCasingOd());
					wirelinePreLogging.setCasingOd(Double.parseDouble(thisConverter.formatOutputPrecision()));
				}
				
				if(casingSection.getCasingId() != null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "casingId");
					thisConverter.setBaseValue(casingSection.getCasingId());
					wirelinePreLogging.setCasingId(Double.parseDouble(thisConverter.formatOutputPrecision()));
				}
				//wirelinePreLogging.setCasingWeight(casingSection.getWeightActual());
			}
			
			//SURVEY
			{
				String[] paramsFields = {"operationUid","dailyUid"};
				Object[] paramsValues = {operationUid,dailyUid};
				String strSql = "SELECT max(b.inclinationAngle) as maxincangle, max(b.depthMdMsl) as maxdepthmdmsl FROM SurveyReference a, SurveyStation b WHERE (a.isDeleted = false or a.isDeleted is null) AND a.surveyReferenceUid=b.surveyReferenceUid AND a.operationUid = :operationUid AND b.dailyUid=:dailyUid";
				List listSurveyStation = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp); //puts the result query into list object.
				Object[] thisResult = (Object[]) listSurveyStation.get(0);
				
				if(listSurveyStation != null ){	
					Double maxInclinationAngle = null;
					Double maxdepthmdmsl = null;
					
					if (thisResult[0]!= null) {						
						maxInclinationAngle = Double.parseDouble(thisResult[0].toString());
						
						thisConverter.setReferenceMappingField(WirelinePreLogging.class, "maxDeviationAngle");
						thisConverter.setBaseValue(maxInclinationAngle);
						wirelinePreLogging.setMaxDeviationAngle(thisConverter.getConvertedValue());
					}
					
					if (thisResult[1] != null){
						maxdepthmdmsl = Double.parseDouble(thisResult[1].toString());
						
						thisConverter.setReferenceMappingField(WirelinePreLogging.class, "MaxDeviationMdMsl");
						thisConverter.setBaseValue(maxdepthmdmsl);
						wirelinePreLogging.setMaxDeviationMdMsl(thisConverter.getConvertedValue());
					}
				}
			}
		}
	}
	
	/**
	 * Method to get Well Operation info as dynamic attribute
	 * @param commandBean
	 * @param node
	 * @param userSelection
	 * @param wirelinePreLogging object from afterDataNodeLoad and afterTemplateNodeCreated
	 * @param operationUid current selected operationUid
	 * @throws Exception
	 * @return No return
	 */
	public static void getWellOperationDetail(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, WirelinePreLogging wirelinePreLogging, String operationUid) throws Exception {
		
		Operation operation = (Operation) ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
		
		String onOffShore = "";
		String defaultdatumuid = null;
		
		if(operation != null){
			
			String lookupCompanyUid ="";
			String companyName ="";
			if(operation.getOpCo()!= null)
			{
				lookupCompanyUid = operation.getOpCo();
				companyName = operation.getOpCo();
				
				LookupCompany lookupCompany = (LookupCompany) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(LookupCompany.class, lookupCompanyUid);
				if(lookupCompany != null) {
					companyName = lookupCompany.getCompanyName();
				}
				
				node.getDynaAttr().put("opCo", companyName);
			}
			
			if(operation.getRigInformationUid() != null) node.getDynaAttr().put("rigInformationUid", operation.getRigInformationUid());
			if(operation.getOperationName() != null) node.getDynaAttr().put("operationName", operation.getOperationName());
			
			defaultdatumuid = operation.getDefaultDatumUid();
			String[] paramsFields1 = {"defaultdatumuid"};
			Object[] paramsValues1 = new Object[1]; paramsValues1[0] = defaultdatumuid;
			String strSql = "FROM OpsDatum WHERE (isDeleted = false or isDeleted is null) AND opsDatumUid =:defaultdatumuid";
			List<OpsDatum> listOpsDatum = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1); //puts the result query into list object.
			
			if(listOpsDatum.size() > 0 ){				
				
				OpsDatum opsDatumrec = listOpsDatum.get(0);
				if(opsDatumrec.getDatumReferencePoint() != null ){
					
					node.getDynaAttr().put("datumReferencePoint", opsDatumrec.getDatumReferencePoint());
				}
				if(opsDatumrec.getReportingDatumOffset() != null ) {
					node.getDynaAttr().put("reportingDatumOffset", opsDatumrec.getReportingDatumOffset());
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, OpsDatum.class, "reportingDatumOffset");					
					if (thisConverter.isUOMMappingAvailable()) {
						node.setCustomUOM("@reportingDatumOffset", thisConverter.getUOMMapping(false));
					}
				}
			}
		}
		
		String wellUid = userSelection.getWellUid();
		String countryname = null;
		Well well = (Well) ApplicationUtils.getConfiguredInstance().getCachedWell(wellUid);
		if (well != null) {
			if(well.getCountry() != null )
				//send short code and get country name in return
				countryname = CommonUtil.getConfiguredInstance().getCountryFullName(well.getCountry());
				node.getDynaAttr().put("country", countryname);
			if(well.getField() != null ) node.getDynaAttr().put("field", well.getField());
			if(well.getLatNs() != null ) {
				node.getDynaAttr().put("latDeg", well.getLatDeg());
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Well.class, "latDeg");
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@latDeg", thisConverter.getUOMMapping(false));
				}
			}
			if(well.getLatNs() != null ) {
				node.getDynaAttr().put("latMinute", well.getLatMinute());
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Well.class, "latMinute");
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@latMinute", thisConverter.getUOMMapping(false));
				}
			}
			if(well.getLatNs() != null ) {
				node.getDynaAttr().put("latSecond", well.getLatSecond());
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Well.class, "latSecond");
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@latSecond", thisConverter.getUOMMapping(false));
				}
			}
			if(well.getLongEw() != null ) {
				node.getDynaAttr().put("longDeg", well.getLongDeg());
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Well.class, "longDeg");
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@longDeg", thisConverter.getUOMMapping(false));
				}
			}
			if(well.getLongEw() != null ) {
				node.getDynaAttr().put("longMinute", well.getLongMinute());
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Well.class, "longMinute");
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@longMinute", thisConverter.getUOMMapping(false));
				}
			}
			if(well.getLongEw() != null ) {
				node.getDynaAttr().put("longSecond", well.getLongSecond());
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Well.class, "longSecond");
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@longSecond", thisConverter.getUOMMapping(false));
				}
			}
			if(well.getGridEw() != null ) {
				node.getDynaAttr().put("gridEw", well.getGridEw());
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Well.class, "gridEw");
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@gridEw", thisConverter.getUOMMapping(false));
				}
			}
			if(well.getGridNs() != null ) {
				node.getDynaAttr().put("gridNs", well.getGridNs());
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Well.class, "gridNs");
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@gridNs", thisConverter.getUOMMapping(false));
				}
			}
			if(well.getOnOffShore() != null ) {
				
				onOffShore = well.getOnOffShore();
			}
			if(onOffShore.equalsIgnoreCase("ON"))
			{
				if(well.getGlHeightMsl() != null ) node.getDynaAttr().put("offsetMsl", well.getGlHeightMsl());
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Well.class, "glHeightMsl");
				if (thisConverter.isUOMMappingAvailable()) {
					node.setCustomUOM("@offsetMsl", thisConverter.getUOMMapping(false));
				}
			}
			
			if(well.getRtHeightDatum() != null) node.getDynaAttr().put("rtHeightDatum", well.getRtHeightDatum());
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Well.class, "rtHeightDatum");
			if (thisConverter.isUOMMappingAvailable()) {
				node.setCustomUOM("@rtHeightDatum", thisConverter.getUOMMapping(false));
			}
			
			
		}
	}
	
	/**
	 * Method to get Formation depthMd, depthTvd, tvdss, thickness and comment when on selected formation. Display as dynamic attribute
	 * @param commandBean
	 * @param node
	 * @param groupUid current session groupUid
	 * @param wirelinePreLoggingZone object from afterDataNodeLoad and onSubmitForServerSideProccess Job
	 * @throws Exception
	 * @return No return
	 */
	public static void getWirelinePreLoggingZoneFormation(CommandBean commandBean, CommandBeanTreeNode node, String groupUid, WirelinePreLoggingZone wirelinePreLoggingZone) throws Exception {
		
		String formationUid = wirelinePreLoggingZone.getFormationName();			
		
		int invert = ("negative".equals(GroupWidePreference.getValue(groupUid, "structureformationgeol")) ? -1 : 1);
		if(StringUtils.isNotBlank(formationUid)){
			
			Formation formation = (Formation) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Formation.class, formationUid);
			
			if(formation != null){				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, "sampleTopMdMsl");
				
				if(formation.getSampleTopMdMsl() != null) {
					thisConverter.setBaseValueFromUserValue(formation.getSampleTopMdMsl(), false);
					node.getDynaAttr().put("sampleTopMdMsl", thisConverter.getFormattedValue());
				}
				if(formation.getSampleTopTvdMsl() != null) {
					thisConverter.setReferenceMappingField(Formation.class, "sampleTopTvdMsl");
					thisConverter.setBaseValueFromUserValue(formation.getSampleTopTvdMsl(), false);
					node.getDynaAttr().put("sampleTopTvdMsl", thisConverter.getFormattedValue());					
				}
				if(formation.getThickness() != null) {
					thisConverter.setReferenceMappingField(Formation.class, "thickness");
					thisConverter.setBaseValueFromUserValue(formation.getThickness(), false);
					node.getDynaAttr().put("thickness", thisConverter.getFormattedValue());
				}
				if(formation.getFormationDescription() != null) {
					node.getDynaAttr().put("formationDescription", formation.getFormationDescription());
				}
				
				if(formation.getSampleTopTvdMsl()!=null) {
					thisConverter.setReferenceMappingField(Formation.class, TVDSS_REFERENCE_FIELD);
					thisConverter.setBaseValueFromUserValue(formation.getSampleTopTvdMsl() * invert, false);
					thisConverter.removeDatumOffset();
					node.getDynaAttr().put("tvdMslSubsea", thisConverter.getFormattedValue());
				}
			}				
		} else {
			
			node.getDynaAttr().put("sampleTopMdMsl", null);
			node.getDynaAttr().put("sampleTopTvdMsl", null);
			node.getDynaAttr().put("thickness", null);
			node.getDynaAttr().put("formationDescription", null);
			node.getDynaAttr().put("tvdMslSubsea", null);
		}
	}
	
	/**
	 * method to get Mud Properties information on selected Mud Check
	 * @param thisWirelinePreLogging targetCommandBeanTreeNode from onSubmitForServerSideProccess to set current selected mud value to target node
	 * @param mudPropertiesUid from onSubmitForServerSideProccess when on selected mud check 
	 * @throws Exception
	 */
	public static void getMudPropertiesDetails(CommandBean commandBean, WirelinePreLogging thisWirelinePreLogging, String mudPropertiesUid) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String sql = "FROM MudProperties WHERE (isDeleted is null or isDeleted=false) and mudPropertiesUid=:mudPropertiesUid";
		String[] paramsFields = {"mudPropertiesUid"};
		Object[] paramsValues = {mudPropertiesUid};
		List<MudProperties> mudList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
		
		if(mudList.size() > 0)
		{
			MudProperties mudProperties = (MudProperties) mudList.get(0);
			
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelinePreLogging.class, "mudWeight");
			
			thisWirelinePreLogging.setMudType(mudProperties.getMudMedium());
			
			if(mudProperties.getMudWeight() != null) {
				
				thisConverter.setBaseValue(mudProperties.getMudWeight());
				thisWirelinePreLogging.setMudWeight(thisConverter.getConvertedValue());
			}
			
			if(mudProperties.getMudPh() != null) {
				thisConverter.setReferenceMappingField(WirelinePreLogging.class, "mudPh");
				thisConverter.setBaseValue(mudProperties.getMudPh());
				thisWirelinePreLogging.setMudPh(thisConverter.getConvertedValue());
			}
			if(mudProperties.getMudPv() != null) {
				thisConverter.setReferenceMappingField(WirelinePreLogging.class, "mudPv");
				thisConverter.setBaseValue(mudProperties.getMudPv());
				thisWirelinePreLogging.setMudPv(thisConverter.getConvertedValue());
			}
			if(mudProperties.getMudYp() != null) {
				thisConverter.setReferenceMappingField(WirelinePreLogging.class, "mudYpPressure");
				thisConverter.setBaseValue(mudProperties.getMudYp());
				thisWirelinePreLogging.setMudYpPressure(thisConverter.getConvertedValue());
			}
			if(mudProperties.getFluidLossApiVolume() != null) {
				thisConverter.setReferenceMappingField(WirelinePreLogging.class, "mudApiFluidLoss");
				thisConverter.setBaseValue(mudProperties.getFluidLossApiVolume());
				thisWirelinePreLogging.setMudApiFluidLoss(thisConverter.getConvertedValue());
			}
			if(mudProperties.getMudChlorideIonDensity() != null) {
				thisConverter.setReferenceMappingField(WirelinePreLogging.class, "mudChlorideIonDensity");
				thisConverter.setBaseValue(mudProperties.getMudChlorideIonDensity());
				thisWirelinePreLogging.setMudChlorideIonDensity(thisConverter.getConvertedValue());
			}
			if(mudProperties.getBarite() != null) {
				thisConverter.setReferenceMappingField(WirelinePreLogging.class, "barite");
				thisConverter.setBaseValue(mudProperties.getBarite());
				thisWirelinePreLogging.setBarite(thisConverter.getConvertedValue());
			}
			if(mudProperties.getMudKcl() != null) {
				thisConverter.setReferenceMappingField(WirelinePreLogging.class, "mudKcl");
				thisConverter.setBaseValue(mudProperties.getMudKcl());
				thisWirelinePreLogging.setMudKcl(thisConverter.getConvertedValue());
			}
			if(mudProperties.getGlycol() != null) {
				thisConverter.setReferenceMappingField(WirelinePreLogging.class, "glycol");
				thisConverter.setBaseValue(mudProperties.getGlycol());
				thisWirelinePreLogging.setGlycol(thisConverter.getConvertedValue());
			}
			if(mudProperties.getMudFv() != null) {
				thisConverter.setReferenceMappingField(WirelinePreLogging.class, "mudFv");
				thisConverter.setBaseValue(mudProperties.getMudFv());
				thisWirelinePreLogging.setMudFv(thisConverter.getConvertedValue());
			}
		}
	}
	
	/**
	 * Method to save existing Wireline Suite info when on edit wireline pre logging section
	 * @param operationUid current selected operation
	 * @param drillerTdMdMsl double raw value 
	 * @param casingShoeMdMsl double raw value
	 * @param wirelineSuiteUid 
	 * @param maxDeviationAngle double raw value
	 * @param maxDeviationMdMsl double raw value
	 * @throws Exception
	 */
	public static void saveExistingWirelineSuite(String operationUid, Double drillerTdMdMsl, Double casingShoeMdMsl, 
			String wirelineSuiteUid, Double maxDeviationAngle, Double maxDeviationMdMsl,String suiteNumber) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
			
		String sql = "FROM WirelineSuite WHERE (isDeleted is null or isDeleted=false) and wirelineSuiteUid=:wirelineSuiteUid";
		String[] paramsFields = {"wirelineSuiteUid"};
		Object[] paramsValues = {wirelineSuiteUid};
		List<WirelineSuite> wirelineSuiteList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
		
		if(wirelineSuiteList.size()>0)
		{
			WirelineSuite wirelineSuite = (WirelineSuite) wirelineSuiteList.get(0);
			wirelineSuite.setDepthMdMsl(drillerTdMdMsl);
			wirelineSuite.setShoeDepthMdMsl(casingShoeMdMsl);
			wirelineSuite.setMaxDeviation(maxDeviationAngle);
			wirelineSuite.setMaxDeviationDepth(maxDeviationMdMsl);
			wirelineSuite.setSuiteNumber(suiteNumber);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(wirelineSuite, qp);
		}
	}
	
	/**
	 * Method to save existing wireline run info when on edit wireline pre logging section
	 * @param operationUid
	 * @param rmValue double raw value 
	 * @param rmTemp double raw value 
	 * @param rmfValue double raw value 
	 * @param rmfTemp double raw value 
	 * @param rmcResistivityValue double raw value 
	 * @param rmcTemp double raw value 
	 * @param mudSource text value
	 * @param holeSize double raw value 
	 * @param expectedHydrostatic double raw value 
	 * @param dateTdReachedTimeTdReached date value
	 * @param dateCircStoppedTimeCircStopped date value
	 * @param dailyUid
	 * @param mudPropertiesUid
	 * @param wirelineRunUid
	 * @throws Exception
	 */
	public static void saveExistingWirelineRun(String operationUid, Double rmValue, Double rmTemp, Double rmfValue, Double rmfTemp, 
			Double rmcResistivityValue, Double rmcTemp, String mudSource, Double holeSize, Double expectedHydrostatic, 
			Date dateTdReachedTimeTdReached, Date dateCircStoppedTimeCircStopped, String dailyUid, String mudPropertiesUid, 
			String wirelineRunUid,String runNumber) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String sql = "FROM WirelineRun WHERE (isDeleted is null or isDeleted=false) and wirelineRunUid=:wirelineRunUid";
		String[] paramsFields = {"wirelineRunUid"};
		Object[] paramsValues = {wirelineRunUid};
		List<WirelineRun> wireLineList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, paramsFields, paramsValues, qp);
		
		if(wireLineList.size() > 0)
		{
			WirelineRun wirelineRunRec = (WirelineRun) wireLineList.get(0);
			wirelineRunRec.setRmValue(rmValue);
			wirelineRunRec.setRunNumber(runNumber);
			wirelineRunRec.setRmTemp(rmTemp);
			wirelineRunRec.setRmfValue(rmfValue);
			wirelineRunRec.setRmfTemp(rmfTemp);
			wirelineRunRec.setRmcValue(rmcResistivityValue);
			wirelineRunRec.setRmcTemp(rmcTemp);
			wirelineRunRec.setMudSource(mudSource);
			wirelineRunRec.setHoleSize(holeSize);
			wirelineRunRec.setExpectedHydrostatic(expectedHydrostatic);
			wirelineRunRec.setDateTdReachedTimeTdReached(dateTdReachedTimeTdReached);
			wirelineRunRec.setDateCircStoppedTimeCircStopped(dateCircStoppedTimeCircStopped);
			wirelineRunRec.setMudPropertiesUid(mudPropertiesUid);
			wirelineRunRec.setWirelineRunDailyUid(dailyUid);
			wirelineRunRec.setOperationUid(operationUid);
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(wirelineRunRec,qp);
		}		
		
	}
	
	/**
	 * Method to create new wireline suite and run wireline suite and run not exist on selected operation
	 * @param groupUid
	 * @param operationUid
	 * @param wirelinePreLoggingUid
	 * @param suitenumber double value
	 * @param runNumber double value 
	 * @param drillerTdMdMsl double raw value 
	 * @param casingShoeMdMsl double raw value 
	 * @param rmValue double raw value 
	 * @param rmTemp double raw value 
	 * @param rmfValue double raw value 
	 * @param rmfTemp double raw value 
	 * @param rmcResistivityValue double raw value 
	 * @param rmcTemp double raw value 
	 * @param mudSource text value
	 * @param holeSize double raw value 
	 * @param expectedHydrostatic double raw value 
	 * @param dateTdReachedTimeTdReached date value
	 * @param dateCircStoppedTimeCircStopped date value
	 * @param dailyUid
	 * @param mudPropertiesUid
	 * @param maxDeviationAngle double raw value 
	 * @param maxDeviationMdMsl double raw value 
	 * @throws Exception
	 */
	public static void createNewWirelineSuiteRun(String groupUid, String operationUid, String wirelinePreLoggingUid, String suitenumber, String runNumber, 
			Double drillerTdMdMsl, Double casingShoeMdMsl, Double rmValue, Double rmTemp, Double rmfValue, Double rmfTemp, Double rmcResistivityValue, 
			Double rmcTemp, String mudSource, Double holeSize, Double expectedHydrostatic, 
			Date dateTdReachedTimeTdReached, Date dateCircStoppedTimeCircStopped, String dailyUid, 
			String mudPropertiesUid, Double maxDeviationAngle, Double maxDeviationMdMsl) throws Exception{
		// TODO Auto-generated method stub
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//Create new Wireline Suite
		WirelineSuite newrecWirelineSuite = new WirelineSuite();
		if (StringUtils.isNotBlank(suitenumber)) {
			
			newrecWirelineSuite.setSuiteNumber(suitenumber);
			newrecWirelineSuite.setGroupUid(groupUid);
			newrecWirelineSuite.setDepthMdMsl(drillerTdMdMsl);	
			newrecWirelineSuite.setShoeDepthMdMsl(casingShoeMdMsl);
			newrecWirelineSuite.setMaxDeviation(maxDeviationAngle);
			newrecWirelineSuite.setMaxDeviationDepth(maxDeviationMdMsl);
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newrecWirelineSuite, qp);
			
			//Create new Wireline Run
			if(StringUtils.isNotBlank(runNumber)) {
				
				WirelineRun newrecWirelineRun = new WirelineRun();	
				
				newrecWirelineRun.setRunNumber(runNumber);			
				newrecWirelineRun.setWirelineSuiteUid(newrecWirelineSuite.getWirelineSuiteUid());		
				newrecWirelineRun.setGroupUid(groupUid);		
				newrecWirelineRun.setWirelineRunDailyUid(dailyUid);
				newrecWirelineRun.setRmValue(rmValue);
				newrecWirelineRun.setRmTemp(rmTemp);		
				newrecWirelineRun.setRmfValue(rmfValue);	
				newrecWirelineRun.setRmfTemp(rmfTemp);	
				newrecWirelineRun.setRmcValue(rmcResistivityValue);	
				newrecWirelineRun.setRmcTemp(rmcTemp);	
				newrecWirelineRun.setMudSource(mudSource);
				newrecWirelineRun.setHoleSize(holeSize);
				newrecWirelineRun.setExpectedHydrostatic(expectedHydrostatic);
				newrecWirelineRun.setDateTdReachedTimeTdReached(dateTdReachedTimeTdReached);
				newrecWirelineRun.setDateCircStoppedTimeCircStopped(dateCircStoppedTimeCircStopped);
				
				newrecWirelineRun.setMudPropertiesUid(mudPropertiesUid);
					
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newrecWirelineRun, qp);
				
				String[] paramsFields = {"wirelineSuiteUid", "wirelineRunUid", "operationUid", "wirelinePreLoggingUid"};
				Object[] paramsValues = {newrecWirelineSuite.getWirelineSuiteUid(), newrecWirelineRun.getWirelineRunUid(), operationUid, wirelinePreLoggingUid};
				String strSql = "UPDATE WirelinePreLogging SET wirelineSuiteUid =:wirelineSuiteUid, wirelineRunUid=:wirelineRunUid WHERE operationUid =:operationUid AND wirelinePreLoggingUid = :wirelinePreLoggingUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
				
				String[] paramsFields2 = {"wirelineRunUid", "operationUid", "wirelinePreLoggingUid"};
				Object[] paramsValues2 = {newrecWirelineRun.getWirelineRunUid(), operationUid, wirelinePreLoggingUid};
				String strSql2 = "UPDATE WirelinePreLoggingCirculation SET wirelineRunUid=:wirelineRunUid WHERE operationUid =:operationUid AND wirelinePreLoggingUid=:wirelinePreLoggingUid";
				ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
			}
		}
	}
	
	/**
	 * Method to create new wireline run if exist wireline suite but no wireline run when on edit wireline pre logging
	 * @param operationUid
	 * @param rmValue double raw value 
	 * @param rmTemp double raw value 
	 * @param rmfValue double raw value 
	 * @param rmfTemp double raw value 
	 * @param rmcResistivityValue double raw value 
	 * @param rmcTemp double raw value 
	 * @param mudSource text value
	 * @param holeSize double raw value 
	 * @param expectedHydrostatic double raw value 
	 * @param dateTdReachedTimeTdReached date value
	 * @param dateCircStoppedTimeCircStopped date value
	 * @param dailyUid
	 * @param mudPropertiesUid
	 * @param wirelineSuiteUid
	 * @param runNumber double value 
	 * @param groupUid
	 * @param wirelinePreLoggingUid
	 * @param drillerTdMdMsl double raw value 
	 * @param casingShoeMdMsl double raw value 
	 * @param maxDeviationAngle double raw value 
	 * @param maxDeviationMdMsl double raw value 
	 * @throws Exception
	 */
	public static void createNewWirelineRun(String operationUid, Double rmValue, Double rmTemp, Double rmfValue, Double rmfTemp, 
			Double rmcResistivityValue, Double rmcTemp, String mudSource, Double holeSize, Double expectedHydrostatic, 
			Date dateTdReachedTimeTdReached, Date dateCircStoppedTimeCircStopped, String dailyUid, String mudPropertiesUid, 
			String wirelineSuiteUid, String runNumber, String groupUid, String wirelinePreLoggingUid, Double drillerTdMdMsl, Double casingShoeMdMsl, Double maxDeviationAngle, Double maxDeviationMdMsl) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		//Create new Wireline Suite
		if (StringUtils.isNotBlank(runNumber) && StringUtils.isNotBlank(wirelineSuiteUid)) {
			
			WirelineSuite wirelineSuite = (WirelineSuite) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(WirelineSuite.class, wirelineSuiteUid);
			wirelineSuite.setDepthMdMsl(drillerTdMdMsl);	
			wirelineSuite.setShoeDepthMdMsl(casingShoeMdMsl);
			wirelineSuite.setMaxDeviation(maxDeviationAngle);
			wirelineSuite.setMaxDeviationDepth(maxDeviationMdMsl);
			
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(wirelineSuite, qp);
			
			//Create new Wireline Run
				
			WirelineRun newrecWirelineRun = new WirelineRun();	
			
			newrecWirelineRun.setRunNumber(runNumber);			
			newrecWirelineRun.setWirelineSuiteUid(wirelineSuiteUid);		
			newrecWirelineRun.setGroupUid(groupUid);		
			newrecWirelineRun.setWirelineRunDailyUid(dailyUid);
			newrecWirelineRun.setRmValue(rmValue);
			newrecWirelineRun.setRmTemp(rmTemp);		
			newrecWirelineRun.setRmfValue(rmfValue);	
			newrecWirelineRun.setRmfTemp(rmfTemp);	
			newrecWirelineRun.setRmcValue(rmcResistivityValue);	
			newrecWirelineRun.setRmcTemp(rmcTemp);	
			newrecWirelineRun.setMudSource(mudSource);
			newrecWirelineRun.setHoleSize(holeSize);
			newrecWirelineRun.setExpectedHydrostatic(expectedHydrostatic);
			newrecWirelineRun.setDateTdReachedTimeTdReached(dateTdReachedTimeTdReached);
			newrecWirelineRun.setDateCircStoppedTimeCircStopped(dateCircStoppedTimeCircStopped);
			
			newrecWirelineRun.setMudPropertiesUid(mudPropertiesUid);
				
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newrecWirelineRun, qp);
			
			String[] paramsFields = {"wirelineSuiteUid", "wirelineRunUid", "operationUid", "wirelinePreLoggingUid"};
			Object[] paramsValues = {wirelineSuiteUid, newrecWirelineRun.getWirelineRunUid(), operationUid, wirelinePreLoggingUid};
			String strSql = "UPDATE WirelinePreLogging SET wirelineSuiteUid =:wirelineSuiteUid, wirelineRunUid=:wirelineRunUid WHERE operationUid =:operationUid AND wirelinePreLoggingUid = :wirelinePreLoggingUid";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues);
			
			String[] paramsFields2 = {"wirelineRunUid", "operationUid", "wirelinePreLoggingUid"};
			Object[] paramsValues2 = {newrecWirelineRun.getWirelineRunUid(), operationUid, wirelinePreLoggingUid};
			String strSql2 = "UPDATE WirelinePreLoggingCirculation SET wirelineRunUid=:wirelineRunUid WHERE operationUid =:operationUid AND wirelinePreLoggingUid=:wirelinePreLoggingUid";
			ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql2, paramsFields2, paramsValues2, qp);
			
		}
		
	}
}
