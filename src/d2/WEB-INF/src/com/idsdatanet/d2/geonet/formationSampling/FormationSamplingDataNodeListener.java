package com.idsdatanet.d2.geonet.formationSampling;

import javax.servlet.http.HttpServletRequest;


import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.FormationFluid;
import com.idsdatanet.d2.core.model.FormationFluidSample;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class FormationSamplingDataNodeListener extends EmptyDataNodeListener {
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof FormationFluid) {
			FormationFluid formationFluid = (FormationFluid) object;
			if (StringUtils.isNotBlank(userSelection.getDailyUid())){
				Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
				if(daily!=null) formationFluid.setSamplingDateTime(daily.getDayDate());
			}
		}
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof FormationFluidSample) {
			FormationFluidSample formationFluidSample = (FormationFluidSample) object;
			if (formationFluidSample.getChamberStartTime() !=null && formationFluidSample.getChamberEndTime() !=null){
				Long chamberDuration = CommonUtil.getConfiguredInstance().calculateDuration(formationFluidSample.getChamberStartTime(), formationFluidSample.getChamberEndTime());
				
				if (chamberDuration !=null){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, FormationFluidSample.class, "chamberDuration");
					thisConverter.setBaseValue(chamberDuration);				
					formationFluidSample.setChamberDuration(thisConverter.getConvertedValue());
				}
				
			}
			
			if (formationFluidSample.getPumpStartDateTime()!=null && formationFluidSample.getPumpEndDateTime() !=null) {
				Long pumpDuration = CommonUtil.getConfiguredInstance().calculateDuration(formationFluidSample.getPumpStartDateTime(), formationFluidSample.getPumpEndDateTime());
				
				if (pumpDuration !=null){
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, FormationFluidSample.class, "pumpDuration");
					thisConverter.setBaseValue(pumpDuration);				
					formationFluidSample.setPumpDuration(thisConverter.getConvertedValue());
				}
			}
			
			Double avgFlowPressure = CommonUtil.getConfiguredInstance().getAvgValue(formationFluidSample.getFlowPressureMax(), formationFluidSample.getFlowPressureMin());			
			if (avgFlowPressure !=null) formationFluidSample.setFlowPressureAvg(avgFlowPressure);
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		
		if (object instanceof FormationFluid) {
			
			String maxSurveyAngleDepth = CommonUtil.getConfiguredInstance().getMaxDeviationDepth(userSelection);
			node.getDynaAttr().put("maxDeviation", maxSurveyAngleDepth);
			
		}
	}

}
