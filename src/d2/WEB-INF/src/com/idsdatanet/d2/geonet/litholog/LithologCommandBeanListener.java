package com.idsdatanet.d2.geonet.litholog;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Litholog;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LithologCommandBeanListener extends EmptyCommandBeanListener  {
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		
		for (CommandBeanTreeNode node1: commandBean.getRoot().getList().get("Litholog").values()) {
			Litholog thisLitholog = (Litholog) node1.getData();
			String thisLithologUid = thisLitholog.getLithologUid();
			Double totallength = 0.00;		
			
			if (StringUtils.isNotBlank(thisLithologUid)) {
								
				String[] paramsFields = {"thisOperationUid","thisDailyUid"};
				Object[] paramsValues = {userSelection.getOperationUid(),userSelection.getDailyUid()};				
				
				String strSql = "SELECT depthFromMdMsl, depthToMdMsl FROM Litholog WHERE operationUid = :thisOperationUid AND dailyUid = :thisDailyUid AND (isDeleted = false OR isDeleted IS NULL)";
				List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Litholog.class, "depthFromMdMsl");
				
				if (lstResult.size() > 0) 
				{
					for (Object[] thisResult: lstResult){
						Double depthMdFrom = 0.00;
						Double depthMdTo = 0.00;
											
						if (thisResult[0] != null){
							depthMdFrom = Double.parseDouble(thisResult[0].toString());
						}
						if (thisResult[1] != null){
							depthMdTo = Double.parseDouble(thisResult[1].toString());
						}
						
						if (depthMdTo > depthMdFrom) {
							totallength = totallength + (depthMdTo - depthMdFrom);
						}
						
					}
				}
		
				if (thisConverter.isUOMMappingAvailable()){
					commandBean.getRoot().setCustomUOM("@totallength", thisConverter.getUOMMapping());
				}
				commandBean.getRoot().getDynaAttr().put("totallength", totallength);
				
			}			
		}
		
		//Set UOM unit type
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Litholog.class, "depthFromMdMsl");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.getRoot().setCustomUOM("@fromDepth", thisConverter.getUOMMapping());
			commandBean.getRoot().setCustomUOM("@toDepth", thisConverter.getUOMMapping());
		}
		
	}
}
