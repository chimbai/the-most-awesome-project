package com.idsdatanet.d2.geonet.core;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Core;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class CoreChipDataNodeListener extends EmptyDataNodeListener {
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if(object instanceof Core)
		{
			Core thisCore = (Core) object;
			thisCore.setCoreType("Chips");
			
			//Check if End Depth Greater than start depth
			if (thisCore.getTopMdMsl()!=null && thisCore.getBottomMdMsl()!=null) {
				if (thisCore.getTopMdMsl() > thisCore.getBottomMdMsl()) {
					status.setFieldError(node, "bottomMdMsl", "End Depth (MD) must greater than Start Depth (MD).");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			//Check if End Depth Greater than start depth
			if (thisCore.getDepthFromSubseaTvd()!=null && thisCore.getDepthToSubseaTvd()!=null) {
				if (thisCore.getDepthFromSubseaTvd() > thisCore.getDepthToSubseaTvd()) {
					status.setFieldError(node, "depthToSubseaTvd", "End Depth (TVDSS) must greater than Start Depth (TVDSS).");
					status.setContinueProcess(false, true);
					return;
				}
			}
		}
	}
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		
		Object object = node.getData();
		
		if (object instanceof Core) {
			Core thisCore = (Core) object;
			Double amountCut = 0.0;
			Double startmd = 0.0;
			Double endmd = 0.0;
			Double lengthrecovered = 0.0;
			Double percentrecovered = 0.0;
			
			//To Calculate amountCut
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Core.class, "topMdMsl"); 
			if(thisCore.getTopMdMsl() != null)
			{
				thisConverter.setBaseValueFromUserValue(thisCore.getTopMdMsl());
				thisConverter.addDatumOffsetToUserReferencePoint();
				startmd = thisConverter.getBasevalue();
			}
			
			thisConverter.setReferenceMappingField(Core.class, "bottomMdMsl");
			if(thisCore.getBottomMdMsl() != null)
			{
				thisConverter.setBaseValueFromUserValue(thisCore.getBottomMdMsl());
				thisConverter.addDatumOffsetToUserReferencePoint();
				endmd = thisConverter.getBasevalue();
			}
									
			if((endmd - startmd) > 0)
			{
				amountCut = endmd - startmd;
			}
						
			if (thisConverter.isUOMMappingAvailable()){
				node.setCustomUOM("@amountCut", thisConverter.getUOMMapping(false));
			}
			thisConverter.setBaseValue(amountCut);
			node.getDynaAttr().put("amountCut", thisConverter.getConvertedValue());
			
			//Calculate percentRecovered formula : lengthRecovered divide by amountCut (20090902-1408-lting)
			if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "calcPercentRecovered"))) {
				if(amountCut != 0 && thisCore.getLengthRecovered() != null)
				{
					CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, Core.class, "lengthRecovered");
					thisConverter2.setBaseValueFromUserValue(thisCore.getLengthRecovered());
					lengthrecovered = thisConverter2.getBasevalue();
					percentrecovered = (lengthrecovered / amountCut) * 100;
					
				}
				thisCore.setPercentRecovered(percentrecovered);
				
				ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisCore, qp);
			}
		}
	}
}
