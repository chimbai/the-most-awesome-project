package com.idsdatanet.d2.geonet.lwdPreLogging;

import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LwdPreLogging;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
	
public class LwdPreLoggingCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener {
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(LwdPreLogging.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())){
			if("bharunUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				
				//To get Bharun record when onChange runNumber
				LwdPreLogging thislwdPreLogging = (LwdPreLogging) targetCommandBeanTreeNode.getData();
												
				String bharunUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "bharunUid");				
				String bitDiameter = null;
				
				Bharun bharun = (Bharun) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Bharun.class, bharunUid);
								
				if (bharun != null && (bharun.getIsDeleted() == null || !bharun.getIsDeleted())) {
					
					if (bharun.getDepthInMdMsl() != null) {
						thislwdPreLogging.setDepthInMdMsl(bharun.getDepthInMdMsl());
					}					
					
					Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(bharun.getDailyidIn());
					if (StringUtils.isNotBlank(bharun.getDailyidIn())) {
						thislwdPreLogging.setInHoleDatetime(daily.getDayDate());
					}
					
				}
				
				DecimalFormat formatter = new DecimalFormat("#0.000000");
				
				String strSql1 = "FROM Bitrun WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :thisBharunUid";			
				List Result1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql1, "thisBharunUid", bharunUid);			
				if (Result1.size() > 0){
					Bitrun thisBitrun = (Bitrun) Result1.get(0);
					//node.getDynaAttr().put("bitDiameter", CustomFieldUom.format(thisBitrun.getBitDiameter(), Bitrun.class, "bitDiameter"));
					if (thisBitrun.getBitDiameter() != null) {
						bitDiameter = CommonUtils.roundUpFormat(formatter, thisBitrun.getBitDiameter());
					}
					//node.getDynaAttr().put("bitDiameter", formatter.format(thisBitrun.getBitDiameter()));				
				}
				targetCommandBeanTreeNode.getDynaAttr().put("bitDiameter", bitDiameter);
				
			}
		}
	}
}
