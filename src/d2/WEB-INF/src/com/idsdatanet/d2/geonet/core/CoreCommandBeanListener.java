package com.idsdatanet.d2.geonet.core;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Core;
import com.idsdatanet.d2.core.model.CoreDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class CoreCommandBeanListener extends EmptyCommandBeanListener  {
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(true);
		
		for (CommandBeanTreeNode node1: commandBean.getRoot().getList().get("Core").values()) {
			Core thisCore = (Core) node1.getData();
			String thisCoreUid = thisCore.getCoreUid();
			Double totallength = 0.00;		
			//Double lengthrecovered = 0.00;
			//Double percentrecovered = 0.00;
			
			if (StringUtils.isNotBlank(thisCoreUid)) {
								
				String[] paramsFields = {"coreUid"};
				Object[] paramsValues = new Object[1]; 
				paramsValues[0] = thisCoreUid;
				
				String strSql = "SELECT startdepthMdMsl, enddepthMdMsl FROM CoreDetail WHERE coreUid = :coreUid AND (isDeleted = false OR isDeleted IS NULL) AND (exclude <> '1' OR exclude IS NULL)";
				List<Object[]> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, CoreDetail.class, "startdepthMdMsl");
				//Calculate core detail total thickness				
				if (lstResult.size() > 0) 
				{
					for (Object[] thisResult: lstResult){
						Double startdepth = 0.00;
						Double enddepth = 0.00;
												
						if (thisResult[0] != null){
							startdepth = Double.parseDouble(thisResult[0].toString());
						}
						if (thisResult[1] != null){
							enddepth = Double.parseDouble(thisResult[1].toString());
						}
						
						if((enddepth - startdepth) > 0)
						{
							totallength = totallength + (enddepth - startdepth);
						}
						
					}
				}
				
				if (thisConverter.isUOMMappingAvailable()){
					node1.setCustomUOM("@totallength", thisConverter.getUOMMapping(false));
				}
				node1.getDynaAttr().put("totallength", totallength);
				/* Remark this. Calculation is done at DataNodeListener
				if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "calcPercentRecovered"))) {
					if(totallength != 0 && thisCore.getLengthRecovered() != null)
					{
						CustomFieldUom thisConverter2 = new CustomFieldUom(commandBean, Core.class, "lengthRecovered");
						thisConverter2.setBaseValueFromUserValue(thisCore.getLengthRecovered());
						lengthrecovered = thisConverter2.getBasevalue();
						percentrecovered = (lengthrecovered / totallength) * 100;
						
					}
					thisCore.setPercentRecovered(percentrecovered);
					//Fire save statement to save percentRecovered
					ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(thisCore,qp);
				}*/
			}			
		}			
	}
}
