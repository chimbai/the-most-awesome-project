package com.idsdatanet.d2.geonet.graph;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.graph.BaseGraph;
import com.idsdatanet.d2.core.model.GasReadings;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class GasGraph extends BaseGraph {
	
	private NumberFormat formatter = new DecimalFormat("#.###");
	private QueryProperties queryProperties = new QueryProperties();
	private Boolean _autoRangeIncludesZero = false;
	private Boolean _showLegend = true;
	private Boolean _showTitle = true;
	private Boolean _showAxis = true;
	private Boolean _graphByWell = false;
	private Color[] colors = {Color.blue, 
			Color.cyan,
			Color.darkGray, 
			Color.gray, 
			Color.green, 
			Color.lightGray, 
			Color.magenta, 
			Color.orange, 
			Color.pink, 
			Color.red, 
			Color.yellow
		};
	
	public void setAutoRangeIncludesZero(Boolean autoRangeIncludesZero) {
		this._autoRangeIncludesZero = autoRangeIncludesZero;
	}
	public Boolean getAutoRangeIncludesZero() {
		return this._autoRangeIncludesZero;
	}
	
	public void setShowLegend(Boolean showLegend) {
		this._showLegend = showLegend;
	}
	public Boolean getShowLegend() {
		return this._showLegend;
	}
	
	public void setShowTitle(Boolean showTitle) {
		this._showTitle = showTitle;
	}
	public Boolean getShowTitle() {
		return this._showTitle;
	}
	public void setShowAxis(Boolean showAxis) {
		this._showAxis = showAxis;
	}
	public Boolean getShowAxis() {
		return this._showAxis;
	}
	public void setGraphByWell(Boolean graphByWell) {
		this._graphByWell = graphByWell;
	}
	public Boolean getGraphByWell() {
		return this._graphByWell;
	}
	
	public JFreeChart paint() throws Exception {
		Locale locale = null;
		this.queryProperties.setUomConversionEnabled(false);
		
		String groupUid = null;
		if (this.getCurrentHttpRequest() != null) {
			UserSession session = UserSession.getInstance(this.getCurrentHttpRequest());
			locale = session.getUserLocale();
			groupUid = session.getCurrentGroupUid();
		} else {
			locale = this.getCurrentUserContext().getUserSelection().getLocale();
			groupUid = this.getCurrentUserContext().getUserSelection().getGroupUid();
		}
		this.formatter = (DecimalFormat) DecimalFormat.getInstance(locale);
		
		if (this.getCurrentUserContext() != null) {
			String operationUid = this.getCurrentUserContext().getUserSelection().getOperationUid();
			
			this.setTitle(CommonUtil.getConfiguredInstance().getCompleteOperationName(groupUid, operationUid));
			this.setFileName(operationUid + "_" + "gasGraph.jpg");
		}

		CustomFieldUom thisConverter = new CustomFieldUom(this.getLocale());
		thisConverter.setReferenceMappingField(GasReadings.class, "topMdMsl");
		String depthLabel = "Depth (" + thisConverter.getUomSymbol() + ")";
		thisConverter.setReferenceMappingField(GasReadings.class, "c1MethaneFrom");
		String gasLabel = "(" + thisConverter.getUomSymbol() + ")";

		XYDataset dataset = createGasDataset();
		NumberAxis xAxis = new NumberAxis(depthLabel);
		xAxis.setAutoRangeIncludesZero(this._autoRangeIncludesZero);
		xAxis.setInverted(true);
		xAxis.setVisible(this._showAxis);
		NumberAxis yAxis = new NumberAxis(gasLabel);
		yAxis.setAutoRangeIncludesZero(true);
		yAxis.setVisible(this._showAxis);
		XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer(true, false);
		for (int c=0;c < this.colors.length; c++) {
			renderer1.setSeriesPaint(c, this.colors[c]);
		}
		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer1);
		
        plot.setOrientation(PlotOrientation.HORIZONTAL);
        plot.setBackgroundPaint(Color.white);
        if (this._showAxis) {
        	plot.setDomainGridlinePaint(Color.lightGray);
        	plot.setRangeGridlinePaint(Color.lightGray);
        	plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        	plot.getRangeAxis().setFixedDimension(15.0);
        } else {
        	plot.setDomainGridlinePaint(Color.WHITE);
        	plot.setRangeGridlinePaint(Color.WHITE);
        	plot.setAxisOffset(new RectangleInsets(0.0,0.0,0.0,0.0));
        	plot.getRangeAxis().setFixedDimension(0.0);
        }
        
		JFreeChart chart = new JFreeChart(getTitle(), JFreeChart.DEFAULT_TITLE_FONT, plot, true);
        chart.setBackgroundPaint(Color.lightGray);
        if (!this._showTitle) {
        	chart.setTitle("");
        } else {
        	chart.addSubtitle(new TextTitle("Gas Readings"));
        }
        if (!this._showLegend) chart.removeLegend();
        if (!this._showAxis) chart.setPadding(new RectangleInsets(0.0,0.0,0.0,0.0));
        ((NumberAxis) chart.getXYPlot().getRangeAxis()).setNumberFormatOverride(formatter);
        ((NumberAxis) chart.getXYPlot().getDomainAxis()).setNumberFormatOverride(formatter);
        			
        return chart;
	}
	
	private XYDataset createGasDataset() throws Exception {
		XYSeriesCollection dataset = new XYSeriesCollection();
		
		XYSeries c1Series = new XYSeries("C1");
		XYSeries c2Series = new XYSeries("C2");
		XYSeries c3Series = new XYSeries("C3");
		XYSeries ic4Series = new XYSeries("iC4");
		XYSeries nc4Series = new XYSeries("nC4");
		XYSeries ic5Series = new XYSeries("iC5");
		XYSeries nc5Series = new XYSeries("nC5");
		
		if (this.getCurrentUserContext() != null) {
			List<GasReadings> rs = null;
			
			
			if (this._graphByWell) {
				String wellUid = this.getCurrentUserContext().getUserSelection().getWellUid();
				String strSql = "FROM GasReadings WHERE (isDeleted=false or isDeleted is null) and wellUid=:wellUid order by topMdMsl";
				rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellUid", wellUid);
			} else {
				String operationUid = this.getCurrentUserContext().getUserSelection().getOperationUid();
				String strSql = "FROM GasReadings WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid order by topMdMsl";
				rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operationUid);
			}
			if (rs!=null) {
				if (this._autoRangeIncludesZero) {
					c1Series.add(0.0, 0.0);
					c2Series.add(0.0, 0.0);
					c3Series.add(0.0, 0.0);
					ic4Series.add(0.0, 0.0);
					nc4Series.add(0.0, 0.0);
					ic5Series.add(0.0, 0.0);
					nc5Series.add(0.0, 0.0);
				}
				
				for (GasReadings rec:rs) {
					if (rec.getTopMdMsl()!=null) {
						Double depth = rec.getTopMdMsl();
						if (rec.getC1MethaneFrom()!=null) c1Series.add(depth, rec.getC1MethaneFrom());
						if (rec.getC2EthaneFrom()!=null) c2Series.add(depth, rec.getC2EthaneFrom());
						if (rec.getC3PropaneFrom()!=null) c3Series.add(depth, rec.getC3PropaneFrom());
						if (rec.getIc4IsoButaneFrom()!=null) ic4Series.add(depth, rec.getIc4IsoButaneFrom());
						if (rec.getNc4NorButaneFrom()!=null) nc4Series.add(depth, rec.getNc4NorButaneFrom());
						if (rec.getIc5SoPethaneFrom()!=null) ic5Series.add(depth, rec.getIc5SoPethaneFrom());
						if (rec.getNc5PentaneFrom()!=null) nc5Series.add(depth, rec.getNc5PentaneFrom());
					}
				}
			}			
			
			if (c1Series.getItemCount()>1) dataset.addSeries(c1Series);
			if (c2Series.getItemCount()>1) dataset.addSeries(c2Series);
			if (c3Series.getItemCount()>1) dataset.addSeries(c3Series);
			if (ic4Series.getItemCount()>1) dataset.addSeries(ic4Series);
			if (nc4Series.getItemCount()>1) dataset.addSeries(nc4Series);
			if (ic5Series.getItemCount()>1) dataset.addSeries(ic5Series);
			if (nc5Series.getItemCount()>1) dataset.addSeries(nc5Series);
		}
		return dataset;
	}
}
