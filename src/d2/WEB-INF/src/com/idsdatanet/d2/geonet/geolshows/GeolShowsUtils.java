package com.idsdatanet.d2.geonet.geolshows;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.model.Geolshows;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;

/**
 * All common utility related to Geology Shows.
 * @author Jackson
 *
 */
public class GeolShowsUtils {

	/**
	 * Calculate thickness using from depth and to depth
	 * @param commandBean
	 * @param node
	 * @throws Exception
	 */
	public static void calculateThickness(CommandBean commandBean, CommandBeanTreeNode node) throws Exception{
		
		Geolshows geoRecord = (Geolshows) node.getData();
		
		Double fromDepth = 0.0;
		Double toDepth = 0.0;
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
					
		if (geoRecord.getToDepthMdMsl()!=null && geoRecord.getFromDepthMdMsl()!=null){
			thisConverter.setReferenceMappingField(Geolshows.class, "toDepthMdMsl");
			thisConverter.setBaseValueFromUserValue(geoRecord.getToDepthMdMsl());
			toDepth = thisConverter.getBasevalue();
			
			thisConverter.setReferenceMappingField(Geolshows.class, "fromDepthMdMsl");
			thisConverter.setBaseValueFromUserValue(geoRecord.getFromDepthMdMsl());
			fromDepth = thisConverter.getBasevalue();
			
			thisConverter.setReferenceMappingField(Geolshows.class, "thickness");
			Double thickness = toDepth - fromDepth;
			thisConverter.setBaseValue(thickness);
			geoRecord.setThickness(thisConverter.getConvertedValue());
		} else {
			geoRecord.setThickness(null);
		}
		
	}
	
	/**
	 * Calculate rating and rating score.
	 * @param commandBean
	 * @param node
	 * @param session
	 * @throws Exception
	 */
	public static void calculateRatingScore(CommandBean commandBean, CommandBeanTreeNode node, UserSession session) throws Exception{
		
			
		Geolshows geoRecord = (Geolshows) node.getData();
		
		Double mvOverBalanceRate = (geoRecord.getMudWeightOverbalanceRate()==null)?0.0:geoRecord.getMudWeightOverbalanceRate();
		Double ropIntoShowIncreaseRate = (geoRecord.getRopIntoShowIncreaseRate() == null)?0.0:geoRecord.getRopIntoShowIncreaseRate();
		Double tgOverBgIncreaseRate = (geoRecord.getTgOverBgIncreaseRate() ==null)?0.0:geoRecord.getTgOverBgIncreaseRate();
		Double c1OverBgIncreaseRate =(geoRecord.getC1OverBgIncreaseRate() == null)?0.0:geoRecord.getC1OverBgIncreaseRate();
		Double c2OverBgIncreaseRate = (geoRecord.getC2OverBgIncreaseRate() == null)?0.0:geoRecord.getC2OverBgIncreaseRate();
		Double c3OverBgIncreaseRate = (geoRecord.getC3OverBgIncreaseRate() == null)?0.0:geoRecord.getC3OverBgIncreaseRate();
		Double c4OverBgIncreaseRate = (geoRecord.getC3OverBgIncreaseRate() == null)?0.0:geoRecord.getC3OverBgIncreaseRate();
		Double visualPorosityRate = (geoRecord.getVisualPorosityRate() == null)?0.0:geoRecord.getVisualPorosityRate();
		Double fluoInShowLithologRate = (geoRecord.getFluoInShowLithologyRate() == null)?0.0:geoRecord.getFluoInShowLithologyRate();
		Double fluorColourRate = (geoRecord.getFluorColourRate() ==null)?0.0:geoRecord.getFluorColourRate();
		Double fluorDistributionRate = (geoRecord.getFluorDistributionRate() == null)?0.0:geoRecord.getFluorDistributionRate();
		Double fluorIntensityRate = (geoRecord.getFluorIntensityRate() == null)?0.0:geoRecord.getFluorIntensityRate();
		Double dominantCutTypeRate = (geoRecord.getDominantCutTypeRate() == null)?0.0:geoRecord.getDominantCutTypeRate();
		Double hcResidueRate = (geoRecord.getHcResidueRate() == null)?0.0:geoRecord.getHcResidueRate();
		
		
		Double ratingScore = 0.0;
		
		ratingScore = (mvOverBalanceRate * 3) + (ropIntoShowIncreaseRate * 3) + tgOverBgIncreaseRate + c1OverBgIncreaseRate + 
					c2OverBgIncreaseRate + c3OverBgIncreaseRate + c4OverBgIncreaseRate + (visualPorosityRate * 3) + 
					(fluoInShowLithologRate * 2) + fluorColourRate + fluorDistributionRate + fluorIntensityRate + 
					(dominantCutTypeRate * 2) + hcResidueRate;
		
		String rating = null;
		if("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "autoPopulateRatingBasedOnRatingScore"))) {
			if (ratingScore < 65) {
				rating = "trace";
			}else if (ratingScore >= 65 && ratingScore < 95) {
				rating = "poor";
			}else if (ratingScore >=95 && ratingScore < 125) {
				rating = "fair";
			}else if(ratingScore >=125) {
				rating = "good";
			}
			
			geoRecord.setRating(rating);
		}
		
		geoRecord.setRatingScore(ratingScore);
	}
	
	
}
