package com.idsdatanet.d2.geonet.wirelinePreLogging.ReportModule;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.WirelinePreLogging;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.report.DefaultReportModule;
import com.idsdatanet.d2.drillnet.report.ReportJobParams;
import com.idsdatanet.d2.drillnet.report.ReportUtils;

public class WirelinePreLoggingReportModule extends DefaultReportModule {

	private class WirelinePreLoogingLookupHandler implements LookupHandler {
		private Map<String, LookupItem> lookup = null;		
		
		WirelinePreLoogingLookupHandler(){
		}		
				
		public Map<String, LookupItem> getLookup(CommandBean commandBean,
			CommandBeanTreeNode node, UserSelectionSnapshot userSelection,
			HttpServletRequest request, LookupCache lookupCache)
			throws Exception {
			
			this.lookup=new LinkedHashMap<String, LookupItem>();
			LookupItem item = new LookupItem("ALL", "ALL");
			
			try {
				String queryString = "FROM WirelinePreLogging WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid";
				List<WirelinePreLogging> wirelinePreLogging;
				
				wirelinePreLogging = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "operationUid", userSelection.getOperationUid());
				
				if (wirelinePreLogging.size()>0)
					this.lookup.put("ALL", item);
					for (WirelinePreLogging thisWirelinePreLogging :wirelinePreLogging)
					{
						String suiteNumber = thisWirelinePreLogging.getWirelineSuiteNumber();
						String runNumber = thisWirelinePreLogging.getWirelineRunNumber();
						if(StringUtils.isNotBlank(runNumber)){
							this.lookup.put(thisWirelinePreLogging.getWirelinePreLoggingUid(), new LookupItem(thisWirelinePreLogging.getWirelinePreLoggingUid(),"Suite " + suiteNumber + " - Run " + runNumber));
						}else{
							this.lookup.put(thisWirelinePreLogging.getWirelinePreLoggingUid(), new LookupItem(thisWirelinePreLogging.getWirelinePreLoggingUid(), "Suite " + suiteNumber + " (No Run)"));
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}					
			
			return this.lookup;
		}
	}
	
	//override the report generating function, if got multiple day selected construct the List of UserSelectionSnapShot and generate report	
	protected ReportJobParams submitReportJob(UserSession session, HttpServletRequest request, CommandBean commandBean) throws Exception {
		commandBean.getSystemMessage().clear();
		String wirelinePreLoggingUid = "";
		String selectionOption = (String) commandBean.getRoot().getDynaAttr().get("suiteNumber");
		
		UserSelectionSnapshot userSelection = UserSelectionSnapshot.getInstanceForCustomDaily(session, session.getCurrentDailyUid());
		userSelection.setCustomProperty("suiteNumber", selectionOption);
		
		if (StringUtils.isNotBlank(selectionOption)) {
			
			wirelinePreLoggingUid = selectionOption;
			WirelinePreLogging thisWirelinePreLogging = (WirelinePreLogging) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(WirelinePreLogging.class, wirelinePreLoggingUid);
			
			if (thisWirelinePreLogging != null) {
				String suiteNumber = "";
				String runNumber = "";
				
				if(StringUtils.isNotBlank(thisWirelinePreLogging.getWirelineSuiteNumber())) {
					suiteNumber = thisWirelinePreLogging.getWirelineSuiteNumber();
					userSelection.setCustomProperty("suiteNo", "Suite #" + suiteNumber);
				}
				if(StringUtils.isNotBlank(thisWirelinePreLogging.getWirelineRunNumber())) {
					runNumber = thisWirelinePreLogging.getWirelineRunNumber();
					userSelection.setCustomProperty("runNo", "Run #" + runNumber);
				} else {
					userSelection.setCustomProperty("runNo", "(No Run)");
				}
			}
			
			if ("ALL".equals(commandBean.getRoot().getDynaAttr().get("suiteNumber"))) userSelection.setCustomProperty("suiteNo", "All Suite");
		}else{
			userSelection.setCustomProperty("suiteNo", "All Suite");
		}
		
		return super.submitReportJob(userSelection, this.getParametersForReportSubmission(session, request, commandBean));
		
	}
	
	protected String getOutputFileName(UserContext userContext, String paperSize) throws Exception{
		Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(userContext.getUserSelection().getOperationUid());
		
		String suiteNumber = (String) userContext.getUserSelection().getCustomProperties().get("suiteNumber");
		String wirelinePreLogging = "ALL";
		if(StringUtils.isNotBlank(suiteNumber)){
			wirelinePreLogging = suiteNumber;
		}
		if(super.getOperationRequired() && operation == null) throw new Exception("Operation must be selected");
		
		
		if(operation == null) throw new Exception("Unable to generate output file name");
		
		return (operation == null ? "" : operation.getOperationName()) + " " + (StringUtils.isNotBlank(wirelinePreLogging) ? "" : wirelinePreLogging) + (StringUtils.isBlank(paperSize) ? "" : " (" + paperSize + ")") + "." + this.getOutputFileExtension();
	}	
	
	protected String getOutputFileInLocalPath(UserContext userContext, String paperSize) throws Exception{
		String operation_uid = userContext.getUserSelection().getOperationUid();
		if(super.getOperationRequired() && StringUtils.isBlank(operation_uid)){
			throw new Exception("Operation must be selected");
		}	
		
		String suiteNumber = (String) userContext.getUserSelection().getCustomProperties().get("suiteNumber");		
		
		String wirelinePreLogging = "ALL";
		if(StringUtils.isNotBlank(suiteNumber)){
			wirelinePreLogging = suiteNumber;
		}		
		return this.getReportType(userContext.getUserSelection()) + 
			(StringUtils.isBlank(operation_uid) ? "" : "/" + operation_uid) + 
			(StringUtils.isBlank(wirelinePreLogging) ? "" : "/" + wirelinePreLogging) +
			"/" + ReportUtils.replaceInvalidCharactersInFileName(this.getOutputFileName(userContext, paperSize));
		
	}
	
	public void init(CommandBean commandBean){
		super.init(commandBean);
		
		if(commandBean instanceof BaseCommandBean){
			Map lookup = new HashMap();
			lookup.put("@suiteNumber", new WirelinePreLoogingLookupHandler());
			
			((BaseCommandBean) commandBean).setLookup(lookup);
							
		}
	}
}
