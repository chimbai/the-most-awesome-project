package com.idsdatanet.d2.geonet.drillStemTest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.DrillStemTest;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.Datum;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class DrillStemTestDataNodeListener extends EmptyDataNodeListener {

	@Override
	public void afterTemplateNodeCreated(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		this.setKbToGL(commandBean, node, userSelection);
	}

	@Override
	public void afterNewEmptyInputNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		this.setKbToGL(commandBean, node, userSelection);
	}

	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		this.setKbToGL(commandBean, node, userSelection);
	}
	
	private void setKbToGL(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection) throws Exception {
		
		if (node.getData() instanceof DrillStemTest) {
			DrillStemTest drillStemTest = (DrillStemTest) node.getData();
			
			node.getDynaAttr().put("currentDatumType", "RT"); // set default value of @datumType at server side, as currently there is no way to set default value for field embedded in Flex client's CreateNewComboBoxField
			Datum currentDatum = commandBean.getInfo().getCurrentDatum();
			if(currentDatum != null){
				node.getDynaAttr().put("currentDatumType", currentDatum.getDatumCode());
			}
			
			String onOffshore = GroupWidePreference.getValue(userSelection.getGroupUid(), "defaultOnOffShore");
			if (userSelection.getWellUid()!=null) {
				Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, userSelection.getWellUid());
				if (well != null) {
					if (well.getOnOffShore()!=null) {
						onOffshore = well.getOnOffShore();
					}
 				}
			}
			node.getDynaAttr().put("well.onOffShore", onOffshore);
			
			Double kbToGl = null;
			if (userSelection.getOperationUid()!=null) {
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);
				String strSql = "FROM Operation WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid";
				List<Operation> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, new String[] {"operationUid"}, new Object[] {userSelection.getOperationUid()}, qp);
				if (list.size()>0) {
					Operation operation = list.get(0);
					if (operation.getRkbToMlGl()!=null) {
						CustomFieldUom thisConverter = new CustomFieldUom(commandBean, DrillStemTest.class, "kbToGl");
						thisConverter.setBaseValue(operation.getRkbToMlGl());
						kbToGl = thisConverter.getConvertedValue();
					}
				}
				drillStemTest.setKbToGl(kbToGl);
			}
			
		}
		
	}

}
