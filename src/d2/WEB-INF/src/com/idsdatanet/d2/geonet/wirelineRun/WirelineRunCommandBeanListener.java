package com.idsdatanet.d2.geonet.wirelineRun;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import com.idsdatanet.d2.core.model.WirelineRun;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WirelineRunCommandBeanListener extends EmptyCommandBeanListener {
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(WirelineRun.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())){
			if("mudPropertiesUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				UserSession session = UserSession.getInstance(request);
				String mudPropertiesUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "mudPropertiesUid");				
				WirelineRunUtils.getMudPropertiesInfo(targetCommandBeanTreeNode,mudPropertiesUid, session.getCurrentGroupUid());
			}
		}
	}
}
