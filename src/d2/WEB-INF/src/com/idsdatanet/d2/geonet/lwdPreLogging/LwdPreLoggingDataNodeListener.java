package com.idsdatanet.d2.geonet.lwdPreLogging;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Bitrun;
import com.idsdatanet.d2.core.model.LwdPreLogging;
import com.idsdatanet.d2.core.model.LwdPreLoggingDetail;
import com.idsdatanet.d2.core.model.LwdSuite;
import com.idsdatanet.d2.core.model.LwdTool;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;


public class LwdPreLoggingDataNodeListener extends EmptyDataNodeListener {

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		_afterNewEmptyInputNodeLoad(commandBean, meta, node, userSelection, request);
	}
	
	private void _afterNewEmptyInputNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
				
		Object object = node.getData();			
		if (object instanceof LwdPreLogging) {
			
			//get current well object to auto fill data
			String wellUid = userSelection.getWellUid();
			String operationUid=userSelection.getOperationUid();
			String strSql = "FROM Well WHERE (isDeleted = false or isDeleted is null) AND wellUid = :thisWellUid";			
			List Result1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisWellUid", wellUid);			
			if (Result1.size() > 0){
				Well thisWell = (Well) Result1.get(0);
				
				node.getDynaAttr().put("latDeg", thisWell.getLatDeg());
				node.getDynaAttr().put("latMinute", thisWell.getLatMinute());
				node.getDynaAttr().put("latSecond", thisWell.getLatSecond());
				node.getDynaAttr().put("longDeg", thisWell.getLongDeg());
				node.getDynaAttr().put("longMinute", thisWell.getLongMinute());
				node.getDynaAttr().put("longSecond", thisWell.getLongSecond());
				node.getDynaAttr().put("gridNs", thisWell.getGridNs());				
				node.getDynaAttr().put("gridEw", thisWell.getGridEw());
				
				if (thisWell.getOnOffShore().equals("OFF")){
					if (thisWell.getWaterDepth() != null) {
						node.getDynaAttr().put("waterDepth", CustomFieldUom.format(commandBean, thisWell.getWaterDepth(), Well.class, "waterDepth"));					
					}
				}
			}
			
			//get current operation object to auto fill data					
			String strSql2 = "FROM Operation WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid";			
			List Result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "operationUid", operationUid);			
			if (Result2.size() > 0){
				Operation thisOperation = (Operation) Result2.get(0);
				node.getDynaAttr().put("operName", thisOperation.getOperationName());		
				node.getDynaAttr().put("opCo", thisOperation.getOpCo());
			}
			
			//get Datum Label
			/*String uomDatumUid = userSelection.getUomDatumUid();
			if (uomDatumUid != null) {
				OpsDatum opsDatum = (OpsDatum) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(OpsDatum.class, uomDatumUid);
				if (opsDatum != null) {
					CustomFieldUom thisConverter = new CustomFieldUom(commandBean, OpsDatum.class, "offsetMsl");
					thisConverter.setBaseValue(opsDatum.getOffsetMsl());
					if (thisConverter.isUOMMappingAvailable()){
						node.setCustomUOM("@rtmsl", thisConverter.getUOMMapping());
					}
					node.getDynaAttr().put("datumLabel", commandBean.getInfo().getCurrentDatumDisplayName());
				}
			}*/
			if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN || commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT){
				node.getDynaAttr().put("datumLabel", commandBean.getInfo().getCurrentDatumDisplayName());
			}
		}		
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		
		if (object instanceof LwdPreLogging) {
			
			//get current well object to auto fill data
			String wellUid = userSelection.getWellUid();
			String operationUid=userSelection.getOperationUid();
			String strSql = "FROM Well WHERE (isDeleted = false or isDeleted is null) AND wellUid = :thisWellUid";			
			List Result1 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "thisWellUid", wellUid);			
			if (Result1.size() > 0){
				Well thisWell = (Well) Result1.get(0);
				
				node.getDynaAttr().put("latDeg", thisWell.getLatDeg());
				node.getDynaAttr().put("latMinute", thisWell.getLatMinute());
				node.getDynaAttr().put("latSecond", thisWell.getLatSecond());
				node.getDynaAttr().put("longDeg", thisWell.getLongDeg());
				node.getDynaAttr().put("longMinute", thisWell.getLongMinute());
				node.getDynaAttr().put("longSecond", thisWell.getLongSecond());
				node.getDynaAttr().put("gridNs", thisWell.getGridNs());				
				node.getDynaAttr().put("gridEw", thisWell.getGridEw());
				
				if (thisWell.getOnOffShore().equals("OFF")){
					if (thisWell.getWaterDepth() != null) {
						node.getDynaAttr().put("waterDepth", CustomFieldUom.format(commandBean, thisWell.getWaterDepth(), Well.class, "waterDepth"));					
					}															
				}
			}
			
			//get current operation object to auto fill data					
			String strSql2 = "FROM Operation WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid";			
			List Result2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, "operationUid", operationUid);			
			if (Result2.size() > 0){
				Operation thisOperation = (Operation) Result2.get(0);				
				node.getDynaAttr().put("operName", thisOperation.getOperationName());			
				node.getDynaAttr().put("opCo", thisOperation.getOpCo());
			}
			
			if(commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_SCREEN || commandBean.getOperatingMode() == BaseCommandBean.OPERATING_MODE_REPORT){
				node.getDynaAttr().put("datumLabel", commandBean.getInfo().getCurrentDatumDisplayName());
			}
		}
		
		//to display current selected bharun record
		
		if (! node.getInfo().isTemplateNode() && object instanceof LwdPreLogging)
		{	
			//LwdPreLogging thislwdPreLogging = (LwdPreLogging) object;
			String bharunUid = (String) PropertyUtils.getProperty(object, "bharunUid");
			String[] paramsFields = {"bharunUid"};
			Object[] paramsValues = {bharunUid}; 
			
			DecimalFormat formatter = new DecimalFormat("#0.000000");
			
			String strSql4 = "FROM Bitrun WHERE (isDeleted = false or isDeleted is null) AND bharunUid = :bharunUid";			
			List Result4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields, paramsValues);			
			if (Result4.size() > 0){
				Bitrun thisBitrun = (Bitrun) Result4.get(0);				
				//node.getDynaAttr().put("bitDiameter", CustomFieldUom.format(thisBitrun.getBitDiameter(), Bitrun.class, "bitDiameter"));
				node.getDynaAttr().put("bitDiameter", CommonUtils.roundUpFormat(formatter, thisBitrun.getBitDiameter()));
			}
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		
		Object obj = node.getData();
		
		if(obj instanceof LwdPreLogging) {
			
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "allowOverwriteLwdFromPreLogging"))) {
				LwdPreLogging thisLwdPreLogging = (LwdPreLogging) obj;
				
				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);
				
				String operationUid = thisLwdPreLogging.getOperationUid();
				String lwdSuiteNumber = thisLwdPreLogging.getRunNumber();
				//String lwdSuiteUid = "";
				
				Date inHoleDatetime = thisLwdPreLogging.getInHoleDatetime();
				String inHoleDailyUid = "";
				if (inHoleDatetime != null) {
					String strSql = "SELECT rd.reportDatetime,rd.dailyUid FROM Daily d, ReportDaily rd WHERE rd.dailyUid=d.dailyUid AND (d.isDeleted is null or d.isDeleted=false) AND (rd.isDeleted is null or rd.isDeleted=false) AND rd.reportDatetime=:inHoleDatetime AND rd.operationUid=:operationUid";
					String[] paramsFields = {"operationUid", "inHoleDatetime"};
					Object[] paramsValues = {operationUid,inHoleDatetime};
					List listReportDatetime = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
					
					if (listReportDatetime != null && listReportDatetime.size() > 0) {
						
						Object[] b = (Object[]) listReportDatetime.get(0);
						if (b[1] != null) inHoleDailyUid = b[1].toString();
					}
				}
				
				//Get DepthIn
				Double depthIn = thisLwdPreLogging.getDepthInMdMsl();
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, LwdPreLogging.class, "depthIn");
				if(thisLwdPreLogging.getDepthInMdMsl()!=null)
				{
					thisConverter.setBaseValueFromUserValue(depthIn);
					depthIn = thisConverter.getBasevalue();
				}
				
				//Get Witness
				String witness = thisLwdPreLogging.getGeologists();
				
				//Get Engineer
				String engineer01 = thisLwdPreLogging.getEngineer01();
				String engineer02 = thisLwdPreLogging.getEngineer02();
				String engineer03 = thisLwdPreLogging.getEngineer03();
				
				//Get Company
				String companyUid = thisLwdPreLogging.getCompanyUid();
				
				String[] paramsFields2 = {"operationUid", "lwdSuiteNumber"};
				Object[] paramsValues2 = {operationUid,lwdSuiteNumber};
				
				/*List listLwdSuite = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT lwdSuiteUid FROM LwdSuite " +
						"WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND suiteNum = :lwdSuiteNumber", paramsFields2, paramsValues2);
				//If LwdSuite record exist
				if(listLwdSuite.size() > 0) {
					if(listLwdSuite.get(0)!=null)
					{
						lwdSuiteUid = listLwdSuite.get(0).toString();
						saveExistingLwdSuite(operationUid, lwdSuiteUid, inHoleDailyUid, depthIn, witness, engineer01, engineer02, engineer03, companyUid);					
					}
				}
				//Create new LwdSuite record
				else {
					
					if (thisLwdPreLogging.getRunNumber()!=null && !StringUtils.isEmpty(thisLwdPreLogging.getRunNumber()))
					{
						saveNewLwdSuiteLwdTool(thisLwdPreLogging.getRunNumber(), session.getCurrentGroupUid(), inHoleDailyUid, thisLwdPreLogging.getCompanyUid(), thisLwdPreLogging.getDepthInMdMsl(), thisLwdPreLogging.getGeologists(), thisLwdPreLogging.getEngineer01(), thisLwdPreLogging.getEngineer02(), thisLwdPreLogging.getEngineer03(), null, null, null, null, null, null);
					}
				}*/
				
				List<LwdSuite> listLwdSuite = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM LwdSuite " +
						"WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND suiteNum = :lwdSuiteNumber", paramsFields2, paramsValues2);
				//If LwdSuite record exist
				if (listLwdSuite.size() > 0) {
					if (listLwdSuite.get(0) != null) {
						LwdSuite lwdsuite = listLwdSuite.get(0);
						saveExistingLwdSuite(lwdsuite, qp, inHoleDailyUid, depthIn, witness, engineer01, engineer02, engineer03, companyUid);
					}
				}
				//Create new LwdSuite record
				else {
					if (thisLwdPreLogging.getRunNumber()!=null && !StringUtils.isEmpty(thisLwdPreLogging.getRunNumber())) {
						saveNewLwdSuiteLwdTool(thisLwdPreLogging.getRunNumber(), session.getCurrentGroupUid(), inHoleDailyUid, thisLwdPreLogging.getCompanyUid(), thisLwdPreLogging.getDepthInMdMsl(), thisLwdPreLogging.getGeologists(), thisLwdPreLogging.getEngineer01(), thisLwdPreLogging.getEngineer02(), thisLwdPreLogging.getEngineer03(), null, null, null, null, null, null);
					}
				}
			}
		}
		
		if(obj instanceof LwdPreLoggingDetail)
		{
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "allowOverwriteLwdFromPreLogging"))) {
				LwdPreLoggingDetail thisLwdPreLoggingDetail = (LwdPreLoggingDetail) obj;
				
				String operationUid = "";
				String preLoggingUid = "";
				String suiteUid = "";
				String lwdToolCode = "";
				
				//Get base value for distanceToBit
				Double distance = thisLwdPreLoggingDetail.getDistanceToBit();
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, LwdPreLoggingDetail.class, "distanceToBit");
				if(thisLwdPreLoggingDetail.getDistanceToBit()!=null)
				{
					thisConverter.setBaseValueFromUserValue(distance);
					distance = thisConverter.getBasevalue();
				}
				
				String preLoggingRunNumber = "";
				
				String seq = "";
				if (thisLwdPreLoggingDetail.getSequence() != null) seq = thisLwdPreLoggingDetail.getSequence().toString();
					
				String toolName = thisLwdPreLoggingDetail.getDescr();			
				Double toolSize = thisLwdPreLoggingDetail.getToolSize();
					
				QueryProperties qp = new QueryProperties();
				
				if(thisLwdPreLoggingDetail.getOperationUid()!=null) operationUid = thisLwdPreLoggingDetail.getOperationUid();
				if(thisLwdPreLoggingDetail.getLwdPreLoggingUid()!=null) preLoggingUid = thisLwdPreLoggingDetail.getLwdPreLoggingUid();
				
				String strSql = "SELECT toolCode from LwdToolLookup WHERE (isDeleted = false or isDeleted is null) AND lwdToolLookupUid = :toolName";
				String[] paramsFields = {"toolName"};
				Object[] paramsValues = {toolName};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues, qp);
				
				if (lstResult.size() > 0)
				{
					if (lstResult.get(0) != null) lwdToolCode = lstResult.get(0).toString();
				}
				
				String strSql151 = "FROM LwdPreLogging WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND lwdPreLoggingUid = :preLoggingUid";
				String[] paramsFields151 = {"operationUid", "preLoggingUid"};
				Object[] paramsValues151 = {operationUid, preLoggingUid};
				List lstResult151 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql151, paramsFields151, paramsValues151, qp);
				
				if (lstResult151.size() > 0)
				{
					LwdPreLogging thisLwdPreLogging = (LwdPreLogging) lstResult151.get(0);
					
					Date inHoleDatetime = thisLwdPreLogging.getInHoleDatetime();
					String inHoleDailyUid = "";
					if (inHoleDatetime != null) {
						String strSql2 = "SELECT rd.reportDatetime,rd.dailyUid FROM Daily d, ReportDaily rd WHERE rd.dailyUid=d.dailyUid AND (d.isDeleted is null or d.isDeleted=false) AND (rd.isDeleted is null or rd.isDeleted=false) AND rd.reportDatetime=:inHoleDatetime AND rd.operationUid=:operationUid";
						String[] paramsFields2 = {"operationUid", "inHoleDatetime"};
						Object[] paramsValues2 = {operationUid,inHoleDatetime};
						List listReportDatetime = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql2, paramsFields2, paramsValues2);
						
						if (listReportDatetime != null && listReportDatetime.size() > 0) {
							
							Object[] b = (Object[]) listReportDatetime.get(0);
							if (b[1] != null) inHoleDailyUid = b[1].toString();
						}
					}
					
					if(thisLwdPreLogging.getRunNumber() != null) {
						
						preLoggingRunNumber = thisLwdPreLogging.getRunNumber();
						String strSql3 = "SELECT lwdSuiteUid from LwdSuite WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND suiteNum = :preLoggingRunNumber";
						String[] paramsFields3 = {"operationUid", "preLoggingRunNumber"};
						Object[] paramsValues3 = {operationUid, preLoggingRunNumber};
						List lstResult3 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql3, paramsFields3, paramsValues3, qp);
						
						if (lstResult3.size() > 0)
						{
							suiteUid = lstResult3.get(0).toString();
							
							String strSql4 = "SELECT toolNumber from LwdTool WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND lwdSuiteUid = :suiteUid AND toolNumber=:seq";
							String[] paramsFields4 = {"operationUid", "suiteUid", "seq"};
							Object[] paramsValues4 = {operationUid, suiteUid, seq};
							List lstResult4 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql4, paramsFields4, paramsValues4, qp);
							
							//If Lwd Tools Records exist
							if (lstResult4.size() > 0)
							{	
								seq = lstResult4.get(0).toString();
								saveExistingLwdTool(operationUid, suiteUid, seq, toolName, toolSize, distance, lwdToolCode);
							}
							//Create new LwdTool record
							else 
							{							
								if (thisLwdPreLoggingDetail.getSequence()!=null && !StringUtils.isEmpty(thisLwdPreLoggingDetail.getSequence().toString()))
								{
									saveNewLwdSuiteLwdTool(null, null, null,  null, null, null, null, null, null, thisLwdPreLoggingDetail.getSequence().toString(),
											thisLwdPreLoggingDetail.getDescr(), lwdToolCode, thisLwdPreLoggingDetail.getDistanceToBit(), thisLwdPreLoggingDetail.getToolSize(), suiteUid);	
								}							
							}
						} 
						//Create new LwdSuite record
						else 
						{						
							if (preLoggingRunNumber!=null && !StringUtils.isEmpty(preLoggingRunNumber))
							{
								saveNewLwdSuiteLwdTool(preLoggingRunNumber, session.getCurrentGroupUid(), inHoleDailyUid, thisLwdPreLogging.getCompanyUid(), 
										thisLwdPreLogging.getDepthInMdMsl(), thisLwdPreLogging.getGeologists(), thisLwdPreLogging.getEngineer01(), 
										thisLwdPreLogging.getEngineer02(), thisLwdPreLogging.getEngineer03(), thisLwdPreLoggingDetail.getSequence().toString(),
										thisLwdPreLoggingDetail.getDescr(), lwdToolCode, thisLwdPreLoggingDetail.getDistanceToBit(), thisLwdPreLoggingDetail.getToolSize(), null);							
							}
						}
					}
				}
			}
		}	
	}
	
	/**
	 * update existing Lwd Suite
	 * Update 2 July 2014 - Scrapped due to the method updating the wrong value to depthInMdMsl (datum field, and any other datum field if in use) in town when active stt happens between nbb and town.
	 * 
	 * @param operationUid
	 * @param lwdSuiteUid
	 * @param inHoleDailyUid
	 * @param depthIn
	 * @param witness
	 * @param engineer01
	 * @param engineer02
	 * @param engineer03
	 * @param companyUid
	 * @throws Exception
	 */
	/*private void saveExistingLwdSuite(String operationUid, String lwdSuiteUid, String inHoleDailyUid, Double depthIn, 
			String witness, String engineer01, String engineer02, String engineer03, String companyUid) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"operationUid","lwdSuiteUid","inHoleDailyUid","depthIn","witness","engineer01","engineer02","engineer03","companyUid"};
		Object[] paramsValues = {operationUid,lwdSuiteUid, inHoleDailyUid,depthIn,witness,engineer01,engineer02,engineer03,companyUid};
		String strSql = "UPDATE LwdSuite SET inDailyUid=:inHoleDailyUid, depthInMdMsl=:depthIn, witness01=:witness, " +
				"lwdEngineer01=:engineer01, lwdEngineer02=:engineer02, lwdEngineer03=:engineer03, companyUid=:companyUid " +
				"WHERE operationUid =:operationUid AND lwdSuiteUid = :lwdSuiteUid";
		
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues, qp);
		
	}*/
	
	/**
	 * update existing Lwd Suite
	 * 
	 * @param lwdsuite
	 * @param qp
	 * @param inHoleDailyUid
	 * @param depthIn
	 * @param witness
	 * @param engineer01
	 * @param engineer02
	 * @param engineer03
	 * @param companyUid
	 * @throws Exception
	 */
	private void saveExistingLwdSuite(LwdSuite lwdsuite, QueryProperties qp, String inHoleDailyUid, Double depthIn, 
			String witness, String engineer01, String engineer02, String engineer03, String companyUid) throws Exception {
		lwdsuite.setInDailyUid(inHoleDailyUid);
		lwdsuite.setDepthInMdMsl(depthIn);
		lwdsuite.setWitness01(witness);
		lwdsuite.setLwdEngineer01(engineer01);
		lwdsuite.setLwdEngineer02(engineer02);
		lwdsuite.setLwdEngineer03(engineer03);
		lwdsuite.setCompanyUid(companyUid);
		
		ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(lwdsuite,qp);
	}
	
	/**
	 * update existing Lwd Tool
	 * @param operationUid
	 * @param suiteUid
	 * @param seq
	 * @param toolName
	 * @param toolSize
	 * @param distance
	 * @param lwdToolCode
	 * @throws Exception
	 */
	private void saveExistingLwdTool(String operationUid, String suiteUid, String seq, String toolName, Double toolSize, Double distance, String lwdToolCode) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		String[] paramsFields = {"operationUid", "suiteUid", "seq", "toolName", "toolSize", "distance", "lwdToolCode"};
		Object[] paramsValues = {operationUid, suiteUid, seq, toolName, toolSize, distance, lwdToolCode};
		String strSql = "UPDATE LwdTool SET toolDesc=:toolName, distanceFromBit=:distance, toolSize=:toolSize, toolType=:lwdToolCode WHERE operationUid =:operationUid AND lwdSuiteUid=:suiteUid AND toolNumber=:seq";
		ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql, paramsFields, paramsValues, qp);
	}
	
	/**
	 * Create new lwd suite and lwd tool
	 * @param runNumber
	 * @param groupUid
	 * @param inDailyUid
	 * @param companyUid
	 * @param depthMdMsl
	 * @param geologist
	 * @param engineer01
	 * @param engineer02
	 * @param engineer03
	 * @param sequence
	 * @param descr
	 * @param lwdToolCode
	 * @param distanceToBit
	 * @param toolSize
	 * @param suiteUid
	 * @throws Exception
	 */
	private void saveNewLwdSuiteLwdTool (String runNumber, String groupUid, String inDailyUid, String companyUid, Double depthMdMsl, String geologist, 
			String engineer01, String engineer02, String engineer03, String sequence, String descr, String lwdToolCode, Double distanceToBit, Double toolSize, String suiteUid) throws Exception {
		
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		if (runNumber != null) {
			LwdSuite newrecLwdSuite = new LwdSuite();
			newrecLwdSuite.setSuiteNum(runNumber);				
			newrecLwdSuite.setGroupUid(groupUid);
			newrecLwdSuite.setInDailyUid(inDailyUid);
			newrecLwdSuite.setCompanyUid(companyUid);
			newrecLwdSuite.setDepthInMdMsl(depthMdMsl);
			newrecLwdSuite.setWitness01(geologist);
			newrecLwdSuite.setLwdEngineer01(engineer01);
			newrecLwdSuite.setLwdEngineer02(engineer02);
	
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newrecLwdSuite, qp);
			
			LwdTool newrecLwdTool = new LwdTool();
			if (sequence!=null && !StringUtils.isEmpty(sequence))
			{
				newrecLwdTool.setLwdSuiteUid(newrecLwdSuite.getLwdSuiteUid());
				newrecLwdTool.setToolNumber(sequence);
				newrecLwdTool.setToolDesc(descr);
				newrecLwdTool.setToolType(lwdToolCode);
				newrecLwdTool.setDistanceFromBit(distanceToBit);
				newrecLwdTool.setToolSize(toolSize);
			}
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newrecLwdTool, qp);
		} else {
			
			LwdTool newrecLwdTool = new LwdTool();
			if (sequence!=null && !StringUtils.isEmpty(sequence))
			{
				newrecLwdTool.setLwdSuiteUid(suiteUid);
				newrecLwdTool.setToolNumber(sequence);
				newrecLwdTool.setToolDesc(descr);
				newrecLwdTool.setToolType(lwdToolCode);
				newrecLwdTool.setDistanceFromBit(distanceToBit);
				newrecLwdTool.setToolSize(toolSize);
			}
			ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(newrecLwdTool, qp);
		}
	}
}
