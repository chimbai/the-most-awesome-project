package com.idsdatanet.d2.geonet.wellSummary;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.CasingSection;
import com.idsdatanet.d2.core.model.CementStage;
import com.idsdatanet.d2.core.model.Core;
import com.idsdatanet.d2.core.model.DrillStemTest;
import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.OpsDatum;
import com.idsdatanet.d2.core.model.ProductionString;
import com.idsdatanet.d2.core.model.ProductionStringDetail;
import com.idsdatanet.d2.core.model.Well;
import com.idsdatanet.d2.core.model.Wellbore;
import com.idsdatanet.d2.core.model.WirelineRun;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.uom.mapping.DatumManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.drillnet.well.Constant;

public class WellSummaryDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener, DataNodeListener {
	
	private Double imageHeight = 1000.0;
	private Double wellDepth = 0.0;
	private String bgColor[] = {"#51a8ff","#0f87ff", "#0061c1", "#00458a","#002f5e"};
	private Integer casingCounter = 0;
	private Boolean _byWellDepth = false;
	
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
	}
	
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		if (request!=null) {
			//TODO: temporary pass the data through request
			String byWellDepth = request.getParameter("byWellDepth");
			if (byWellDepth!=null) {
				this._byWellDepth = (byWellDepth.equals("true"));
			}
		}
		
		Double formationStartDepth = 0.0;
		this.getWellDepth(userSelection.getWellUid(), userSelection.getWellboreUid(), userSelection.getOperationUid());
		commandBean.getRoot().getDynaAttr().put("imageHeight", this.imageHeight.toString());
		commandBean.getRoot().getDynaAttr().put("wellDepth", this.wellDepth.toString());
		
		//To set the data for datum reference point and reporting datum offset
		String defaultDatumUid = userSelection.getUomDatumUid();
		if (defaultDatumUid !=null) {
			OpsDatum datum = DatumManager.getOpsDatum(defaultDatumUid);
			if (datum !=null) {
				if(datum.getDatumReferencePoint() != null || StringUtils.isNotBlank(datum.getDatumReferencePoint())) {
					commandBean.getRoot().getDynaAttr().put("datumReferencePoint", datum.getDatumReferencePoint());
				}
				
				CustomFieldUom thisConverter=new CustomFieldUom(commandBean,OpsDatum.class,"reportingDatumOffset");
				thisConverter.setReferenceMappingField(OpsDatum.class, "reportingDatumOffset");
				thisConverter.setBaseValue(datum.getReportingDatumOffset());
				String datumLabel = thisConverter.getFormattedValue();
				if(datum.getReportingDatumOffset() != null) {
					commandBean.getRoot().getDynaAttr().put("datumLabel", datumLabel);
				}
			}
		}

		String operationUid = userSelection.getOperationUid();
		String wellboreUid = userSelection.getWellboreUid();
		String paramsFields = "operationUid";
		Object paramsValues = operationUid;
		
		if (this._byWellDepth) {
			paramsFields = "wellUid";
			paramsValues = userSelection.getWellUid();
		}
		
		String strSql;
		if (_byWellDepth) {
			strSql = "SELECT count(*) as rec_count from CasingSection WHERE (isDeleted = false or isDeleted is null) and wellUid=:wellUid";
		} else {
			strSql = "SELECT count(*) as rec_count from CasingSection WHERE (isDeleted = false or isDeleted is null) and wellboreUid=:wellboreUid";
			paramsFields = "wellboreUid";
			paramsValues = wellboreUid;
		}
		List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		Integer casingCount = 0;
		if (!lstResult.isEmpty())
		{
			Object thisResult = (Object) lstResult.get(0);
			
			if (thisResult != null) casingCount = Integer.parseInt(thisResult.toString());
			commandBean.getRoot().getDynaAttr().put("casingCount", casingCount.toString());
		}
		
		if (this._byWellDepth) {
			paramsFields = "wellUid";
			paramsValues = userSelection.getWellUid();
		}else{
			paramsFields = "operationUid";
			paramsValues = operationUid;
		}
		if (_byWellDepth) {
			strSql = "SELECT MIN(sampleTopMdMsl) as firstDepth from Formation WHERE (isDeleted = false or isDeleted is null) and wellUid=:wellUid";
		} else {
			strSql = "SELECT MIN(sampleTopMdMsl) as firstDepth from Formation WHERE (isDeleted = false or isDeleted is null) and operationUid=:operationUid";
		}
		lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		if (!lstResult.isEmpty())
		{
			Object thisResult = (Object) lstResult.get(0);
			
			if (thisResult != null) formationStartDepth = Double.parseDouble(thisResult.toString());
			formationStartDepth = formationStartDepth / this.wellDepth * this.imageHeight;
			commandBean.getRoot().getDynaAttr().put("formationFirstDepth", formationStartDepth.toString());
		}
		this.wellDepth = 0.0;
		this.casingCounter = 0;
	}
	
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	}
	
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception{
	}
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception{
	}

	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		/*Object object = node.getData();
		if (object instanceof CasingSection)
		{
			this.casingCounter = 0;
		}
		this.wellDepth=0.0;
		*/
	}

	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		Double depthFrom = 0.0;
		Double depthTo = 0.0;
		
		this.getWellDepth(userSelection.getWellUid(), userSelection.getWellboreUid(), userSelection.getOperationUid());
		
		if (object instanceof Operation) {
			Operation operation = (Operation)object;
			setDynaAttr(node, "wellName", operation.getOperationName());
			
			if (operation.getOperationCode().equals(Constant.DRLLG)) {
				Wellbore wellbore = (Wellbore) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Wellbore.class, operation.getWellboreUid());
				setDynaAttr(node, "parentWellboreUid", wellbore.getParentWellboreUid());
			} else {
				setDynaAttr(node, "parentWellboreUid", operation.getWellboreUid());
			}
			
			Well well = (Well) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Well.class, operation.getWellUid());
			setDynaAttr(node, "onOffShore", well.getOnOffShore());
			setDynaAttr(node, "opCo", well.getOpCo());
			setDynaAttr(node, "platformUid", well.getPlatformUid());
			setDynaAttr(node, "slotNumber", well.getSlotNumber());
			setDynaAttr(node, "comment", well.getComment());
			setDynaAttr(node, "waterDepthMsl", well.getWaterDepth());
			setDynaAttr(node, "purposeType", well.getPurposeType());
			setDynaAttr(node, "region", well.getRegion());
			setDynaAttr(node, "country", well.getCountry());
			setDynaAttr(node, "state", well.getState());
			setDynaAttr(node, "basin", well.getBasin());
			setDynaAttr(node, "block", well.getBlock());
			setDynaAttr(node, "field", well.getField());
			setDynaAttr(node, "govPermitNumber", well.getGovPermitNumber());
			setDynaAttr(node, "seismicLine", well.getSeismicLine());
			setDynaAttr(node, "licenseNumber", well.getLicenseNumber());
			setDynaAttr(node, "spheroidType", well.getSpheroidType());
			setDynaAttr(node, "longEw", well.getLongEw());
			setDynaAttr(node, "longDeg", well.getLongDeg());
			setDynaAttr(node, "longMinute", well.getLongMinute());
			setDynaAttr(node, "longSecond", well.getLongSecond());
			setDynaAttr(node, "latNs", well.getLatNs());
			setDynaAttr(node, "latDeg", well.getLatDeg());
			setDynaAttr(node, "latMinute", well.getLatMinute());
			setDynaAttr(node, "latSecond", well.getLatSecond());
			setDynaAttr(node, "gridDatum", well.getGridDatum());
			setDynaAttr(node, "gridNs", well.getGridNs());
			setDynaAttr(node, "gridEw", well.getGridEw());
			setDynaAttr(node, "tzGmtOffset", well.getTzGmtOffset());
			setDynaAttr(node, "locationDetail", well.getLocationDetail());
			setDynaAttr(node, "locParish", well.getLocParish());
			setDynaAttr(node, "locUtmZone", well.getLocUtmZone());
			
			String strSql = "SELECT a.phaseCode FROM Activity a, Daily d " +
					"WHERE (d.isDeleted=false or d.isDeleted is null) " +
					"AND (a.isDeleted=false or a.isDeleted is null) " +
					"AND a.dailyUid=d.dailyUid " +
					"AND a.operationUid=:operationUid " +
					"ORDER BY d.dayDate desc, a.startDatetime desc";
			List<Object> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "operationUid", operation.getOperationUid());
			if (rs!=null) {
				if (rs.size()>0) {
					setDynaAttr(node, "currentPhase", (String) rs.get(0));
				}
			}
			
			setDynaAttr(node, "currentDatum", commandBean.getInfo().getCurrentDatumDisplayName());
			
			strSql = "FROM ProductionString " +
				"WHERE (isDeleted=false or isDeleted is null) " +
				"AND wellboreUid=:wellboreUid " +
				"AND uninstallDateTime is null";
			List<ProductionString> ps = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wellboreUid", operation.getWellboreUid());
			
			if(ps!=null & ps.size() == 1)
			{
				String SqlStr = "SELECT max(tallyLength) as tallyLength from ProductionStringDetail WHERE (isDeleted = false or isDeleted is null) and " +
						"productionStringUid= :productionStringUid ";
				ProductionString psdata = (ProductionString) ps.get(0);
				String[] paramsFields = {"productionStringUid"};
				Object[] paramsValues = {psdata.getProductionStringUid()};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(SqlStr, paramsFields, paramsValues);
				Double tallyLength = 0.0; 
				if (!lstResult.isEmpty())
				{
					Object thisResult = (Object) lstResult.get(0);
					if (thisResult != null) tallyLength = Double.parseDouble(thisResult.toString());
					Double sectionHeight = (tallyLength / this.wellDepth * this.imageHeight);
					if (sectionHeight > this.imageHeight) setDynaAttr(node, "ps_tallyHeight", (this.imageHeight - 1));
					else setDynaAttr(node, "ps_tallyHeight", sectionHeight.toString());
					CustomFieldUom thisConvertor = new CustomFieldUom(userSelection.getLocale(), ProductionStringDetail.class, "tallyLength");
					thisConvertor.setBaseValueFromUserValue(tallyLength);
					setDynaAttr(node, "ps_tallyLenght", thisConvertor.getFormattedValue());
					setDynaAttr(node, "hasPs", "1");
					setDynaAttr(node, "ps_desc", psdata.getProductionType() + " - " + psdata.getDescription());
				}else {
					setDynaAttr(node, "hasPs", "0");
				}
			}else{
				setDynaAttr(node, "hasPs", "0");
			}

		}
		else if (object instanceof Formation) {
			Formation thisObject = (Formation) object;
			Double sampleTopMdMsl = thisObject.getSampleTopMdMsl();
			if (sampleTopMdMsl==null) {
				node.getDynaAttr().put("height", "0");
				return;
			}
			
			String[] paramsFields = {"wellUid","sampleTopMdMsl"};
			Object[] paramsValues = {userSelection.getWellUid(), sampleTopMdMsl};
			String strSql = "SELECT min(sampleTopMdMsl) as wellDepth from Formation WHERE (isDeleted = false or isDeleted is null) and wellUid= :wellUid and sampleTopMdMsl> :sampleTopMdMsl";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			Double nextDepth = 0.0; 
			if (!lstResult.isEmpty())
			{
				Object thisResult = (Object) lstResult.get(0);
				if (thisResult != null) nextDepth = Double.parseDouble(thisResult.toString());
			}
			
			Double sectionHeight = ((nextDepth - sampleTopMdMsl) / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height", sectionHeight.toString());
			
			//strSql = "select fieldValue from DynamicField where (isDeleted = false or isDeleted is null) and fieldKey = :formationUid and fieldUid = 'formation.formation_image'";
			//lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "formationUid", thisObject.getFormationUid());
			//if (!lstResult.isEmpty())
			//{
			//	Object thisResult = (Object) lstResult.get(0);
			//	if (thisResult != null) //nextDepth = Double.parseDouble(thisResult.toString());
			//		node.getDynaAttr().put("formationImage", thisResult.toString());
			//}
		}
		else if (object instanceof Core) {
			Core thisObject = (Core) object;
			depthFrom = thisObject.getTopMdMsl();
			depthTo = thisObject.getBottomMdMsl();
			if (depthFrom==null) depthFrom = 0.0;
			if (depthTo==null) depthTo = 0.0;
			
			Double sectionHeight = (depthFrom / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height_before", sectionHeight.toString());
			
			sectionHeight = ((depthTo - depthFrom) / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height", sectionHeight.toString());
		}
		else if (object instanceof CementStage) {
			CementStage thisObject = (CementStage) object;
			depthFrom = thisObject.getCementTopDepthMdMsl();
			depthTo = thisObject.getCmtbtmMd();
			if (depthFrom==null) depthFrom = 0.0;
			if (depthTo==null) depthTo = thisObject.getCementBottomDepthMdMsl();
			if (depthTo==null) depthTo = 0.0;
			
			Double sectionHeight = (depthFrom / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height_before", sectionHeight.toString());
			
			sectionHeight = ((depthTo - depthFrom) / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height", sectionHeight.toString());
		}
		else if (object instanceof DrillStemTest) {
			DrillStemTest thisObject = (DrillStemTest) object;
			depthFrom = thisObject.getTestIntervalTopMdMsl();
			depthTo = thisObject.getTestIntervalBtmMdMsl();
			
			if (depthFrom==null) depthFrom = 0.0;
			if (depthTo==null) depthTo = 0.0;
			
			Double sectionHeight = (depthFrom / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height_before", sectionHeight.toString());
			
			sectionHeight = ((depthTo - depthFrom) / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height", sectionHeight.toString());
		}
		else if (object instanceof WirelineRun) {
			WirelineRun thisObject = (WirelineRun) object;
			String suite_number = "";
			
			 depthFrom = thisObject.getLogTop();
			 if (depthFrom==null) depthFrom = 0.0;
			
			//String strSql = "select fieldValueDouble as log_top from DynamicField where (isDeleted = false or isDeleted is null) and fieldKey = :wirelineRunUid and fieldUid = 'wireline_run.log_top'";
			//String paramsFields = "wirelineRunUid";
			//Object paramsValues = thisObject.getWirelineRunUid();
			//List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			//if (!lstResult.isEmpty()) {
			//	Object thisResult = (Object) lstResult.get(0);
			//	if (thisResult != null) depthFrom = Double.parseDouble(thisResult.toString());
			//}
			
			 
			 depthTo = thisObject.getLogBottom();
			 if (depthTo==null) depthTo = 0.0;
			 
			//strSql = "select fieldValueDouble as log_top from DynamicField where (isDeleted = false or isDeleted is null) and fieldKey = :wirelineRunUid and fieldUid = 'wireline_run.log_bottom'";
			//lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			//if (!lstResult.isEmpty()) {
			//	Object thisResult = (Object) lstResult.get(0);
			//	if (thisResult != null) depthTo = Double.parseDouble(thisResult.toString());
			//}
			
			String paramsFields = "wirelineSuiteUid";
			Object paramsValues = thisObject.getWirelineSuiteUid();
			String strSql = "select suiteNumber from WirelineSuite where (isDeleted = false or isDeleted is null) and wirelineSuiteUid = :wirelineSuiteUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty()) {
				Object thisResult = (Object) lstResult.get(0);
				if (thisResult != null) suite_number = thisResult.toString();
			}
			
			Double sectionHeight = (depthFrom / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height_before", sectionHeight.toString());
			
			sectionHeight = ((depthTo - depthFrom) / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height", sectionHeight.toString());
			
			node.getDynaAttr().put("suite_number", suite_number);
			
		}
		else if (object instanceof Bharun) {
			Bharun thisObject = (Bharun) object;
			depthFrom = thisObject.getDepthInMdMsl();
			depthTo = thisObject.getDepthOutMdMsl();
			
			if (depthFrom==null) depthFrom = 0.0;
			if (depthTo==null) depthTo = 0.0;
			
			Double sectionHeight = (depthFrom / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height_before", sectionHeight.toString());
			
			sectionHeight = ((depthTo - depthFrom) / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height", sectionHeight.toString());
			
			
			String dailyUid = "";
			String strSql = "select bds.dailyUid from BharunDailySummary bds, Daily d where (bds.isDeleted = false or bds.isDeleted is null) and (d.isDeleted = false or d.isDeleted is null) and bds.bharunUid= :bharunUid and d.dailyUid=bds.dailyUid order by d.dayDate";
			String paramsFields = "bharunUid";
			Object paramsValues = thisObject.getBharunUid();
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty()) {
				Object thisResult = (Object) lstResult.get(0);
				if (thisResult != null) dailyUid = thisResult.toString();
			}
			node.getDynaAttr().put("dailyUid", dailyUid);
		}
		else if (object instanceof CasingSection) {
			CasingSection thisObject= (CasingSection) object;
			depthFrom = thisObject.getLinerTopDepthMdMsl();
			if (depthFrom==null) depthFrom = 0.0;
			if (thisObject.getHangerElevationMdMsl()!=null) {
				depthFrom = thisObject.getHangerElevationMdMsl();
			}
			
			depthTo = thisObject.getShoeTopMdMsl(); 
			if (depthTo==null) depthTo = 0.0;
			
			Double sectionHeight = ((depthFrom) / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height_before", sectionHeight.toString());
			
			sectionHeight = ((depthTo - depthFrom) / this.wellDepth * this.imageHeight);
			node.getDynaAttr().put("height", sectionHeight.toString());
			node.getDynaAttr().put("bgColor", this.bgColor[this.casingCounter]);
			
			this.casingCounter ++;
			if (this.casingCounter>4) this.casingCounter = 0;
		}
	}

	private void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		}
	}
	
	private void getWellDepth(String wellUid, String wellboreUid, String operationUid) throws Exception
	{
		Double currentWellDepth = 0.0;
		if (this.wellDepth==0.0)
		{
			String[] paramsFields = {"wellUid"};
			Object[] paramsValues = {wellUid};
			String strSql = "SELECT max(sampleTopMdMsl) as wellDepth from Formation WHERE (isDeleted = false or isDeleted is null) and wellUid= :wellUid";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty())
			{
				Object thisResult = (Object) lstResult.get(0);
				if (thisResult != null) this.wellDepth = Double.parseDouble(thisResult.toString());
			}

			if (!this._byWellDepth) {
				paramsFields[0] = "wellboreUid";
				paramsValues[0] = wellboreUid;
				strSql = "SELECT max(shoeTopMdMsl) as wellDepth from CasingSection WHERE (isDeleted = false or isDeleted is null) and wellboreUid= :wellboreUid";
			} else {
				strSql = "SELECT max(shoeTopMdMsl) as wellDepth from CasingSection WHERE (isDeleted = false or isDeleted is null) and wellUid= :wellUid";
			}
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty())
			{
				Object thisResult = (Object) lstResult.get(0);
				if (thisResult != null) {
					currentWellDepth = Double.parseDouble(thisResult.toString());
					if (currentWellDepth>this.wellDepth) this.wellDepth = currentWellDepth;
				}
			}
			
			if (this._byWellDepth) {
				strSql = "SELECT max(bottomMdMsl) as wellDepth from Core WHERE (isDeleted = false or isDeleted is null) and wellUid= :wellUid";
			} else {
				paramsFields[0] = "operationUid";
				paramsValues[0] = operationUid;
				strSql = "SELECT max(bottomMdMsl) as wellDepth from Core WHERE (isDeleted = false or isDeleted is null) and operationUid= :operationUid";
			}
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty())
			{
				Object thisResult = (Object) lstResult.get(0);
				if (thisResult != null) {
					currentWellDepth = Double.parseDouble(thisResult.toString());
					if (currentWellDepth>this.wellDepth) this.wellDepth = currentWellDepth;
				}
			}
			
			if (this._byWellDepth) {
				strSql = "SELECT max(testIntervalBtmMdMsl) as wellDepth from DrillStemTest WHERE (isDeleted = false or isDeleted is null) and wellUid= :wellUid";
			} else {
				strSql = "SELECT max(testIntervalBtmMdMsl) as wellDepth from DrillStemTest WHERE (isDeleted = false or isDeleted is null) and operationUid= :operationUid";
			}
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty())
			{
				Object thisResult = (Object) lstResult.get(0);
				if (thisResult != null) {
					currentWellDepth = Double.parseDouble(thisResult.toString());
					if (currentWellDepth>this.wellDepth) this.wellDepth = currentWellDepth;
				}
			}
			
			if (this._byWellDepth) {
				strSql = "SELECT max(depthOutMdMsl) as wellDepth from Bharun WHERE (isDeleted = false or isDeleted is null) and wellUid= :wellUid";
			} else {
				strSql = "SELECT max(depthOutMdMsl) as wellDepth from Bharun WHERE (isDeleted = false or isDeleted is null) and operationUid= :operationUid";
			}
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty())
			{
				Object thisResult = (Object) lstResult.get(0);
				if (thisResult != null) {
					currentWellDepth = Double.parseDouble(thisResult.toString());
					if (currentWellDepth>this.wellDepth) this.wellDepth = currentWellDepth;
				}
			}
			
			if (this._byWellDepth) {
				strSql = "SELECT max(a.cmtbtmMd) as wellDepth from CementStage a, CementJob b WHERE (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and a.cementJobUid=b.cementJobUid and b.wellUid= :wellUid";
			} else {
				strSql = "SELECT max(a.cmtbtmMd) as wellDepth from CementStage a, CementJob b WHERE (a.isDeleted = false or a.isDeleted is null) and (b.isDeleted = false or b.isDeleted is null) and a.cementJobUid=b.cementJobUid and b.operationUid= :operationUid";
			}
			lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
			if (!lstResult.isEmpty())
			{
				Object thisResult = (Object) lstResult.get(0);
				if (thisResult != null) {
					currentWellDepth = Double.parseDouble(thisResult.toString());
					if (currentWellDepth>this.wellDepth) this.wellDepth = currentWellDepth;
				}
			}
		}
		if (this.wellDepth==0.0) this.wellDepth = 1.0;
	}

	public void init(CommandBean commandBean){
	}
	
}
