package com.idsdatanet.d2.geonet.formationEvaluationAssessment;

import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.CascadeLookupHandler;
import com.idsdatanet.d2.core.lookup.CascadeLookupSet;
import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.LookupCompanyService;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ServiceCascadeLookupHandler implements CascadeLookupHandler {
	private URI lookup = null;
	
	public void setLookup(String value) throws Exception {
		this.lookup = new URI(value);
	}
	
	public Map<String, LookupItem> getLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, LookupCache lookupCache) throws Exception{
		return LookupManager.getConfiguredInstance().getLookup(this.lookup.toString(), userSelection, null);
	}

	public CascadeLookupSet[] getCompleteCascadeLookupSet(CommandBean commandBean, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		List<LookupCompanyService> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupCompanyService where (isDeleted = false or isDeleted is null) and groupUid = :groupUid order by lookupCompanyUid", "groupUid", userSelection.getGroupUid());
		String previous_id = null;
		List selected = new ArrayList();
		List<CascadeLookupSet> result = new ArrayList<CascadeLookupSet>();
		Map<String, LookupItem> source_lookup = LookupManager.getConfiguredInstance().getLookup(this.lookup.toString(), userSelection, null);
		
		for(LookupCompanyService lcs: rs){
			if(previous_id != null && (! previous_id.equals(lcs.getLookupCompanyUid()))){
				CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(previous_id);
				cascadeLookupSet.setLookup(this.filter(source_lookup, selected));
				result.add(cascadeLookupSet);
				selected.clear();
			}
			
			selected.add(lcs.getCode());
			previous_id = lcs.getLookupCompanyUid();
		}

		if(previous_id != null){
			CascadeLookupSet cascadeLookupSet = new CascadeLookupSet(previous_id);
			cascadeLookupSet.setLookup(this.filter(source_lookup, selected));
			result.add(cascadeLookupSet);
		}
		
		CascadeLookupSet[] result_in_array = new CascadeLookupSet[result.size()];
		result.toArray(result_in_array);
		return result_in_array;
	}

	public LinkedHashMap<String, LookupItem> filter(Map<String, LookupItem> source, List<String> selected){
		LinkedHashMap<String, LookupItem> result = new LinkedHashMap<String, LookupItem>();
		
		// try to retain the original order -ssjong
		if(selected.size() > 0){
			for(String source_key: source.keySet()){
				if(selected.contains(source_key)) result.put(source_key, source.get(source_key));
			}
		}

		return result;
	}
	
	public Map<String, LookupItem> getCascadeLookup(CommandBean commandBean, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request, String key, LookupCache lookupCache) throws Exception{
		String unique_lookup_id = this.lookup.toString() + "[service_cascade_lookup][" + key + "]";
		
		if(lookupCache.containsLookup(unique_lookup_id)) return lookupCache.getFromCache(unique_lookup_id);
		
		List<LookupCompanyService> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from LookupCompanyService where (isDeleted = false or isDeleted is null) and lookupCompanyUid = :id and groupUid = :groupUid", new String[] {"id", "groupUid"}, new String[] {key, userSelection.getGroupUid()});
		List<String> selected = new ArrayList<String>();

		for(LookupCompanyService lcs: rs){
			selected.add(lcs.getCode());
		}
		
		Map<String, LookupItem> result = this.filter(LookupManager.getConfiguredInstance().getLookup(this.lookup.toString(), userSelection, null), selected);
		lookupCache.saveToCache(unique_lookup_id, result);
		return result;
	}
}
