package com.idsdatanet.d2.geonet.wirelinePreLogging;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.model.WirelinePreLogging;
import com.idsdatanet.d2.core.model.WirelinePreLoggingZone;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WirelinePreLoggingCommandBeanListener extends EmptyCommandBeanListener {
	
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		
		UserSession session = UserSession.getInstance(request);
		if (targetCommandBeanTreeNode.getDataDefinition() != null) {
			Object object = targetCommandBeanTreeNode.getData();
			if (WirelinePreLogging.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())) {				
				if(object instanceof WirelinePreLogging) {
					if("mudPropertiesUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())) {
						
						WirelinePreLogging thisWirelinePreLogging = (WirelinePreLogging) targetCommandBeanTreeNode.getData();
						
						String mudPropertiesUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "mudPropertiesUid");
						
						//MUD
						if(StringUtils.isNotBlank(mudPropertiesUid)) {
							WirelinePreLoggingUtils.getMudPropertiesDetails(commandBean, thisWirelinePreLogging, mudPropertiesUid);
						} else {
							thisWirelinePreLogging.setMudType(null);
							thisWirelinePreLogging.setMudWeight(null);
							thisWirelinePreLogging.setMudPh(null);
							thisWirelinePreLogging.setMudPv(null);
							thisWirelinePreLogging.setMudYpPressure(null);
							thisWirelinePreLogging.setMudApiFluidLoss(null);
							thisWirelinePreLogging.setMudChlorideIonDensity(null);
							thisWirelinePreLogging.setBarite(null);
							thisWirelinePreLogging.setMudKcl(null);
							thisWirelinePreLogging.setGlycol(null);
							thisWirelinePreLogging.setMudFv(null);
						}	
					}
					
					if("dailyUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())) {
					
						WirelinePreLogging wirelinePreLogging = (WirelinePreLogging) object;
						
						String dailyUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "dailyUid");
						//Daily Survey Casing
						if(StringUtils.isNotBlank(dailyUid))
							WirelinePreLoggingUtils.getDailySurveyCasingInfo(commandBean, request, session, wirelinePreLogging);
					}
				}
			}
			
			if (WirelinePreLoggingZone.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())) {
				if(object instanceof WirelinePreLoggingZone) {
					if("formationName".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())) {
						
						WirelinePreLoggingZone wirelinePreLoggingZone = (WirelinePreLoggingZone) object;
						WirelinePreLoggingUtils.getWirelinePreLoggingZoneFormation(commandBean, targetCommandBeanTreeNode, session.getCurrentGroupUid(), wirelinePreLoggingZone);
					}
				}
			}
		}
	}
	
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean, Formation.class, "sampleTopMdMsl");;
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(Formation.class, "@sampleTopMdMsl", thisConverter.getUOMMapping(false));
		}
		
		thisConverter.setReferenceMappingField(Formation.class, "sampleTopTvdMsl");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(Formation.class, "@sampleTopTvdMsl", thisConverter.getUOMMapping(false));
		}
		
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(Formation.class, "@tvdMslSubsea", thisConverter.getUOMMapping(false));
		}
		
		thisConverter.setReferenceMappingField(Formation.class, "thickness");
		if (thisConverter.isUOMMappingAvailable()) {
			commandBean.setCustomUOM(Formation.class, "@thickness", thisConverter.getUOMMapping(false));
		}
	}
}