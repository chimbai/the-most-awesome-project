package com.idsdatanet.d2.geonet.lwdFormationPressure;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.BhaComponent;
import com.idsdatanet.d2.core.model.Bharun;
import com.idsdatanet.d2.core.model.BharunDailySummary;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.LwdFormationPressure;
import com.idsdatanet.d2.core.model.LwdFormationPressureDetail;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.bharun.BharunUtils;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public class LwdFormationPressureDataNodeListener extends EmptyDataNodeListener
{
  public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request)
    throws Exception
  {
    Object object = node.getData();
    if ((object instanceof LwdFormationPressure))
    {
      LwdFormationPressure LwdFormationPressure = (LwdFormationPressure)object;
      
      Daily thisDailyIn = ApplicationUtils.getConfiguredInstance().getCachedDaily(LwdFormationPressure.getDailyUidIn());
      Daily thisDailyOut = ApplicationUtils.getConfiguredInstance().getCachedDaily(LwdFormationPressure.getDailyUidOut());
      if ((thisDailyIn != null) && (thisDailyOut != null)){
    	  if((thisDailyOut.getDayDate().before(thisDailyIn.getDayDate()))){
            status.setContinueProcess(false, true);
            status.setFieldError(node, "dailyUidOut", "Date Out should be greater than Date In.");
            return;
          }else if(thisDailyOut.getDayDate().equals(thisDailyIn.getDayDate())){
        	  Date thisTimeIn = LwdFormationPressure.getTimeIn();
        	  Date thisTimeOut = LwdFormationPressure.getTimeOut();
        	  if((thisTimeIn != null) && (thisTimeOut != null) && (thisTimeOut.getTime() < thisTimeIn.getTime())){
        		  status.setContinueProcess(false, true);
        		  status.setFieldError(node, "timeOut", "Time Out should be greater than Time In.");
        		  return;
        	  }
          }
      }
    }
    if ((object instanceof LwdFormationPressureDetail))
    {
      LwdFormationPressureDetail LwdFormationPressureDetail = (LwdFormationPressureDetail)object;
      
      Date start = LwdFormationPressureDetail.getStartTime();
      Date end = LwdFormationPressureDetail.getEndTime();
      if ((start != null) && (end != null) && 
        (start.getTime() > end.getTime()))
      {
        status.setContinueProcess(false, true);
        status.setFieldError(node, "startTime", "Start time can not be greater than end time.");
        return;
      }
    }
  }
  
  public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request)
    throws Exception
  {}
  
  public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode)
    throws Exception
  {}
  
  public void init(CommandBean commandBean)
    throws Exception
  {}
  
  public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request)
    throws Exception
  {}
  
  public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request)
    throws Exception
  {}
  
  public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request)
    throws Exception
  {}
  
  public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request)
    throws Exception
  {}
}
