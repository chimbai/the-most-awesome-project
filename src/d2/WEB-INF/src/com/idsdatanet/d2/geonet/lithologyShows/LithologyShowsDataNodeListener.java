package com.idsdatanet.d2.geonet.lithologyShows;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.model.LithologyShows;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class LithologyShowsDataNodeListener extends EmptyDataNodeListener{

	@Override
	public void afterDataNodeLoad(CommandBean commandBean,
			TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node,
			UserSelectionSnapshot userSelection, HttpServletRequest request)
			throws Exception {
		
		if (node.getData() instanceof LithologyShows) {
			String lithologyShowsUid = ((LithologyShows) node.getData()).getLithologyShowsUid();
			if (lithologyShowsUid!=null) {
				String queryString = "FROM Geolshows WHERE (isDeleted=false or isDeleted is null) and operationUid=:operationUid and lithologyShowsUid=:lithologyShowsUid";
				List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, new String[] {"operationUid", "lithologyShowsUid"}, new Object[]{userSelection.getOperationUid(), lithologyShowsUid});
				if (list.size()>0) {
					node.getDynaAttr().put("hasGeolShows", "1");
				}
			}
		}
		
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		if (object instanceof LithologyShows) {
			
			LithologyShows thisLithologyShows = (LithologyShows) object;
			//Check if End Depth Greater than start depth
			if (thisLithologyShows.getTopMdMsl()!=null && thisLithologyShows.getBottomMdMsl()!=null) {
				if (thisLithologyShows.getTopMdMsl() > thisLithologyShows.getBottomMdMsl()) {
					status.setFieldError(node, "bottomMdMsl", "End Depth must greater than Start Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
			
			if (thisLithologyShows.getDepthFromSubseaTvd()!=null && thisLithologyShows.getDepthToSubseaTvd()!=null) {
				if (thisLithologyShows.getDepthFromSubseaTvd() > thisLithologyShows.getDepthToSubseaTvd()) {
					status.setFieldError(node, "depthToSubseaTvd", "End Depth must greater than Start Depth.");
					status.setContinueProcess(false, true);
					return;
				}
			}
		}
	}

}
