package com.idsdatanet.d2.geonet.wirelineRun;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.model.MudProperties;
import com.idsdatanet.d2.core.model.WirelineRun;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class WirelineRunUtils {
	
	public static void getMudPropertiesInfo (CommandBeanTreeNode node, String mudPropertiesUid, String groupUid) throws Exception {
		Object object = node.getData();
		if (object instanceof WirelineRun) {
			String[] paramsFields = {"mudPropertiesUid"};
	 		Object[] paramsValues = new Object[] {mudPropertiesUid};
			
			String strSql = "FROM MudProperties where mudPropertiesUid = :mudPropertiesUid and (isDeleted = false or isDeleted is null)";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields,paramsValues);

			MudProperties mudProperties = null;
			boolean clear_all = false;
			
			if (!lstResult.isEmpty()) {
				mudProperties = (MudProperties) lstResult.get(0);
			}else{
				mudProperties = new MudProperties();
				clear_all = true;
			}

			Map<String, Object> mudFieldValues = PropertyUtils.describe(mudProperties);
			for (Map.Entry entry : mudFieldValues.entrySet()) {
				setDynaAttr(node, "mudProperties." + entry.getKey(), (clear_all ? "" : entry.getValue()));
			}
				
			showCustomUOM(node, groupUid);
		}
	}
		
	private static void setDynaAttr(CommandBeanTreeNode node, String dynaAttrName, Object value) throws Exception {
		if (value != null) {
			node.getDynaAttr().put(dynaAttrName, value);
		} else {
			node.getDynaAttr().put(dynaAttrName, "");
		}
	}
	
	private static void showCustomUOM(CommandBeanTreeNode node, String groupUid) throws Exception {
		Object object = node.getData();
		if (object instanceof WirelineRun) {
			CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
		
			for (Field field : MudProperties.class.getDeclaredFields()) {	
				thisConverter.setReferenceMappingField(MudProperties.class, field.getName());
				if(thisConverter.getUOMMapping() != null){
					node.setCustomUOM("@mudProperties." + field.getName(), thisConverter.getUOMMapping());
				}
			}
			thisConverter.dispose();
		}
	}
}
