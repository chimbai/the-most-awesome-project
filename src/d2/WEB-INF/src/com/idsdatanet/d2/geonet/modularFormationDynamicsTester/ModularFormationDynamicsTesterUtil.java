package com.idsdatanet.d2.geonet.modularFormationDynamicsTester;

import com.idsdatanet.d2.core.model.ModularFormationDynamicsTesterDetail;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.calculation.geonet.ModularFormationDynamicsTesterCalculation;

/**
 * All common utility related to ModularFormationDynamicsTester.
 * @author Tonney
 *
 */
public class ModularFormationDynamicsTesterUtil {

	/**
	 * Common method for Formation Pressure EMW with business logic
	 * @param commandBean
	 * @param object
	 * @throws Exception Standard Error Throwing Exception
	 * @return No Return
	 */
	public static void calculateFormPressEMW(CommandBean commandBean, Object object) throws Exception {

		if (object instanceof ModularFormationDynamicsTesterDetail) {
			ModularFormationDynamicsTesterDetail mfdtd = (ModularFormationDynamicsTesterDetail)object;

			Double formPress = 0.00;
			Double tvdMsl = 0.00;
			formPress = mfdtd.getFormationPressureQg();
			tvdMsl = mfdtd.getDepthTvdMsl();

			CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
			if (formPress != null) {
				thisConverter.setReferenceMappingField(ModularFormationDynamicsTesterDetail.class, "formationPressureQg");
				thisConverter.setBaseValueFromUserValue(formPress);
				thisConverter.changeUOMUnit("PoundsPerSquareInchAbsolute", 10);
				formPress = thisConverter.getConvertedValue();
			}
			if (tvdMsl != null) {
				thisConverter.setReferenceMappingField(ModularFormationDynamicsTesterDetail.class, "depthTvdMsl");
				thisConverter.setBaseValueFromUserValue(tvdMsl, false);
				thisConverter.changeUOMUnit("Feet", 10);
				tvdMsl = thisConverter.getConvertedValue();
			}
			
			//Pressure(psi) / 0.05192 / Depth(ft)
			Double emw = 0.00;
			if (formPress != null && tvdMsl != null) {
				emw = ModularFormationDynamicsTesterCalculation.calculateFormPressEMW(formPress, tvdMsl);
				thisConverter.setReferenceMappingField(ModularFormationDynamicsTesterDetail.class, "emw");
				thisConverter.setBaseValue(emw);
				mfdtd.setEmw(thisConverter.getConvertedValue());
			}
			else
				mfdtd.setEmw(null); //Update to null if no value entered 
		}
	}
}