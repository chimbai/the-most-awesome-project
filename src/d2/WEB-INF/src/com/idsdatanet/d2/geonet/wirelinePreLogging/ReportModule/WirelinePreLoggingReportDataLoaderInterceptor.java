package com.idsdatanet.d2.geonet.wirelinePreLogging.ReportModule;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.model.WirelinePreLogging;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class WirelinePreLoggingReportDataLoaderInterceptor implements DataLoaderInterceptor{

	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		String preLoggingUid = (String) userSelection.getCustomProperty("suiteNumber");		
		conditionClause = "";	
		
		if(WirelinePreLogging.class.equals(meta.getTableClass())) {
			if(StringUtils.isNotBlank(preLoggingUid)){
				
				if("ALL".equalsIgnoreCase(preLoggingUid)) {
					conditionClause = "(isDeleted = false or isDeleted is null) and operationUid = session.operationUid";
				} else {
					conditionClause = "(isDeleted = false or isDeleted is null) and operationUid = session.operationUid and wirelinePreLoggingUid = '" + preLoggingUid + "'";
				}
			}else{
				conditionClause = "(isDeleted = false or isDeleted is null) and operationUid = session.operationUid";
			}
		}
		
		return conditionClause;
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
