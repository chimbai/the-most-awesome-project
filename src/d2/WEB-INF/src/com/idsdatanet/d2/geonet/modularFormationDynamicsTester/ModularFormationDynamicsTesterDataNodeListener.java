package com.idsdatanet.d2.geonet.modularFormationDynamicsTester;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.graph.GraphManager;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.*;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.*;

public class ModularFormationDynamicsTesterDataNodeListener extends EmptyDataNodeListener implements CommandBeanListener {
	private GraphManager graphManager = null;
	
	public GraphManager getGraphManager() {
		return graphManager;
	}

	public void setGraphManager(GraphManager graphManager) {
		this.graphManager = graphManager;
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof ModularFormationDynamicsTesterDetail)
			ModularFormationDynamicsTesterUtil.calculateFormPressEMW(commandBean, object);
		
		if (object instanceof FormationFluidSample) {
			FormationFluidSample formationFluidSample = (FormationFluidSample) object;
			Double flowPressureMin = formationFluidSample.getFlowPressureMin();
			Double flowPressureMax = formationFluidSample.getFlowPressureMax();
			if (flowPressureMin!=null && flowPressureMax!=null)
				formationFluidSample.setFlowPressureAvg((flowPressureMin + flowPressureMax) / 2);
			
			//Pump Time Duration
			CustomFieldUom thisConverter = new CustomFieldUom(commandBean, FormationFluidSample.class, "pumpDuration");
			Date start = formationFluidSample.getPumpStartTime();
			Date end = formationFluidSample.getPumpEndTime();
			if(start != null && end != null){
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
				
				//Long duration = end.getTime() - start.getTime();
				thisConverter.setBaseValue(thisDuration);
				formationFluidSample.setPumpDuration(thisConverter.getConvertedValue());
			}
			
			//Chamber Time Duration
			thisConverter = new CustomFieldUom(commandBean, FormationFluidSample.class, "chamberDuration");
			start = formationFluidSample.getChamberStartTime();
			end = formationFluidSample.getChamberEndTime();
			if(start != null && end != null){
				//call to method to calculate duration base on 2 dates;
				Long thisDuration = CommonUtil.getConfiguredInstance().calculateDuration(start, end);
				//Long duration = end.getTime() - start.getTime();
				thisConverter.setBaseValue(thisDuration);
				formationFluidSample.setChamberDuration(thisConverter.getConvertedValue());
			}
		}
	}
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		if (object instanceof ModularFormationDynamicsTester) {
			ModularFormationDynamicsTester modularFormationDynamicsTester = (ModularFormationDynamicsTester)object;
			
			UserContext userContext = UserContext.getUserContext(userSelection);
			
			Map<String, Object> customProperties = new HashMap<String, Object>();
			customProperties.put("mdtUid", modularFormationDynamicsTester.getModularFormationDynamicsTesterUid());
			
			this.graphManager.process(userContext, request, null, "formation_pressure", "medium", customProperties);
			this.graphManager.process(userContext, request, null, "hydrostatic_pressure", "medium", customProperties);
		} 
	}

	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		Object object = targetCommandBeanTreeNode.getData();
		if (object instanceof ModularFormationDynamicsTester) {
			ModularFormationDynamicsTester rec = (ModularFormationDynamicsTester) object;
			String wirelineRunUid = rec.getWirelineRunUid();
			String toolString = "";
			if (wirelineRunUid!=null) {
				String strSql = "FROM WirelineTool WHERE (isDeleted=false or isDeleted is null) AND wirelineRunUid=:wirelineRunUid ORDER BY sequence";
				List<WirelineTool> rs = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wirelineRunUid", wirelineRunUid);
				if (rs.size()>0) {
					for (WirelineTool tool : rs) {
						if (tool.getToolCode()!=null) {
							if (!"".equals(toolString)) toolString +="-";
							toolString += tool.getToolCode();
						}
					}
				}
			}
			rec.setToolstring(toolString);
		}
	}
	
	public void onCustomFilterInvoked(HttpServletRequest request, HttpServletResponse response, BaseCommandBean commandBean, String invocationKey) throws Exception {}
	public void init(CommandBean commandBean) throws Exception {}
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {}
	public void beforeDataLoad(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {}
	public void beforeProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {}
	public void afterProcessFormSubmission(CommandBean commandBean, HttpServletRequest request) throws Exception {}
}
