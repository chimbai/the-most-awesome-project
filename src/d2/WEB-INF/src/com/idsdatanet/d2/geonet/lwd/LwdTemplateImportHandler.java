package com.idsdatanet.d2.geonet.lwd;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.core.lookup.DbLookupDefinition;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.lookup.LookupManager;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.CommonDateParser;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.templateimport.DefaultTemplateImportHandler;
import com.idsdatanet.d2.core.web.mvc.templateimport.EmptySlot;

public class LwdTemplateImportHandler extends DefaultTemplateImportHandler{
	private Map<String, Object> _lookup = null;
	private Map<String, Map> cachedLookup = new HashMap<String, Map>();
	
	public void setLookup(Object lookup) {
		if (lookup instanceof Map) {
			this._lookup = (Map<String,Object>)lookup;
		}
	}
	
	public Object getLookup() {
		return this._lookup;
	}

	@Override
	protected void setupAdditionalBindingValues(int line,
			List<String> bindingProperties, List bindingValue, TreeModelDataDefinitionMeta targetMeta, BaseCommandBean commandBean, CommandBeanTreeNodeImpl parentNode) {
		UserSelectionSnapshot userSelection = null;
		List<Daily> dailyList = null;
		
		if (this._lookup==null) return;
		if (bindingValue != null) {
			if (bindingValue.get(0) instanceof EmptySlot) {
			} else {
				try {
					//this is to get the lookup setting in the lookup property in template Import Handler property
					for (Map.Entry<String, Object> entry : this._lookup.entrySet()) {
						String fieldName = entry.getKey();
						Object lookup = entry.getValue();
						String[] key = fieldName.split("[.]");
						if (lookup==null) continue;
						fieldName = fieldName.replaceAll(key[0], "data");
						
						Map<String, LookupItem> lookup_map = null;

						if (this.cachedLookup.containsKey(fieldName)) {
							lookup_map = this.cachedLookup.get(fieldName);
						} else {
							if(lookup instanceof DbLookupDefinition)
							{
								lookup = (String) ((DbLookupDefinition) lookup).getObject();			
							}
							lookup_map = LookupManager.getConfiguredInstance().getLookup(lookup.toString(), new UserSelectionSnapshot(commandBean.getCurrentUserSession()), null);
							this.cachedLookup.put(fieldName, lookup_map);
						}
						//this is for checking on whether the imported records value is exist in the lookup list or not 
						if (lookup_map!=null) {
							for (int i=0; i<bindingProperties.size(); i++) {
								if (fieldName.equals(bindingProperties.get(i))) {
									Object fieldValue = bindingValue.get(i);
									if (fieldValue==null) break;
									if (!lookup_map.containsKey(fieldValue.toString())) {
										String value = fieldValue.toString().toUpperCase();
										for (Map.Entry<String, LookupItem> lookupEntry : lookup_map.entrySet()) {
											LookupItem item = lookupEntry.getValue();
											if (item.getValue()==null) continue;
											String lookupValue = item.getValue().toString().toUpperCase();
											if (value.equals(lookupValue)) {
												bindingValue.set(i, item.getKey());
											}
										}
									}
									break;	
								}
							}
							
						}
					}
					//this for getting the date value from csv import template and import into screen drop down list 
					if (bindingProperties.contains(this.getPropertyNameFromField(targetMeta,"dailyUid")))
					{
						userSelection = new UserSelectionSnapshot(commandBean.getCurrentUserSession());
						dailyList = ApplicationUtils.getConfiguredInstance().getDaysInOperation(userSelection.getOperationUid());
						String fieldName = this.getPropertyNameFromField(targetMeta, "dailyUid");
						Date lwdActivityDate = null;
						if(bindingProperties.indexOf(fieldName) < 0) return;
						Object fieldValue = bindingValue.get(bindingProperties.indexOf(fieldName));
						if (fieldValue!=null) lwdActivityDate = CommonDateParser.parse(fieldValue.toString());
						
						if (dailyList!=null || dailyList.size() > 0) {
							if(lwdActivityDate!=null) {
								for(Daily d:dailyList){
									if(DateUtils.isSameDay(d.getDayDate(), lwdActivityDate)) {
										bindingValue.set(bindingProperties.indexOf(fieldName), d.getDailyUid());
									}
								}
							}
						}
					}
					
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		
	}
	
}
