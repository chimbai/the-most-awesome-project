package com.idsdatanet.d2.geonet.wirelinePreLogging;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.groupwidepreference.GroupWidePreference;
import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.WirelinePreLogging;
import com.idsdatanet.d2.core.model.WirelinePreLoggingCirculation;
import com.idsdatanet.d2.core.model.WirelinePreLoggingZone;
import com.idsdatanet.d2.core.model.WirelineSuite;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WirelinePreLoggingDataNodeListener extends EmptyDataNodeListener  {
	
	public void afterDataNodeLoad(CommandBean commandBean,TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object object = node.getData();
		String strSql = "";
		
		if(object instanceof WirelinePreLogging) {
			WirelinePreLogging wirelinePreLogging = (WirelinePreLogging) object;
			String operationUid = wirelinePreLogging.getOperationUid();
			
			WirelinePreLoggingUtils.getWellOperationDetail(commandBean, node, userSelection, wirelinePreLogging,  operationUid);
			
			if ("1".equals(GroupWidePreference.getValue(userSelection.getGroupUid(), "useWitnessFromWirelinesuite"))) {		
				
				String wirelineSuiteUid = wirelinePreLogging.getWirelineSuiteUid();
				
				String[] paramsFields1 = {"operationUid","wirelineSuiteUid"};
				Object[] paramsValues1 = {operationUid,wirelineSuiteUid};
				strSql = "FROM WirelineSuite WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND wirelineSuiteUid=:wirelineSuiteUid";
				List<WirelineSuite> listWirelineSuite = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields1, paramsValues1); //puts the result query into list object.
				
				if(listWirelineSuite.size() > 0 ){				
					
					WirelineSuite wirelineSuite = listWirelineSuite.get(0);
					wirelinePreLogging.setWitness(wirelineSuite.getWirelineWitness());
					String witness=wirelineSuite.getWirelineWitness();
					String strSql1=null;
					
					String[] paramsFields2 = {"operationUid","wirelineSuiteUid","witness"};
					Object[] paramsValues2 = {operationUid,wirelineSuiteUid,witness};
					strSql1 = "Update WirelinePreLogging SET witness=:witness WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND wirelineSuiteUid=:wirelineSuiteUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql1, paramsFields2, paramsValues2); //puts the result query into list object.
				}
			}
		}
		
		
		if(object instanceof WirelinePreLoggingZone) {
			WirelinePreLoggingZone wirelinePreLoggingZone = (WirelinePreLoggingZone) object;
			WirelinePreLoggingUtils.getWirelinePreLoggingZoneFormation(commandBean, node, userSelection.getGroupUid(), wirelinePreLoggingZone);
		}
	}
	
	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();	
		
		if(obj instanceof WirelinePreLogging){
			if ("1".equals(GroupWidePreference.getValue(session.getCurrentGroupUid(), "allowOverwriteWirelineRun"))) {
				
				WirelinePreLogging thisWirelinePreLogging = (WirelinePreLogging) obj;

				QueryProperties qp = new QueryProperties();
				qp.setUomConversionEnabled(false);

				String operationUid = thisWirelinePreLogging.getOperationUid();
				Double drillerTdMdMsl = thisWirelinePreLogging.getDrillerTdMdMsl();
				Double casingShoeMdMsl = thisWirelinePreLogging.getCasingShoeMdMsl();
				Double maxDeviationMdMsl = thisWirelinePreLogging.getMaxDeviationMdMsl();
				Double maxDeviationAngle = thisWirelinePreLogging.getMaxDeviationAngle();
				
				String runNumber = thisWirelinePreLogging.getWirelineRunNumber();
				String suitenumber = thisWirelinePreLogging.getWirelineSuiteNumber();
				
				Double rmValue = thisWirelinePreLogging.getRm();
				Double rmTemp = thisWirelinePreLogging.getRmTemperature();
				Double rmfValue = thisWirelinePreLogging.getRmf();
				Double rmfTemp = thisWirelinePreLogging.getRmfTemperature();
				Double rmcResistivityValue = thisWirelinePreLogging.getRmcResistivity();
				Double rmcTemp = thisWirelinePreLogging.getRmcTemperature();
				
				String mudSource = thisWirelinePreLogging.getMudSource();
				Double holeSize = thisWirelinePreLogging.getBitSize();
				Double expectedHydrostatic = thisWirelinePreLogging.getExpectedHydrostaticPressure();
				Date dateTdReachedTimeTdReached = thisWirelinePreLogging.getBitReachedTdDateTime();
				Date dateCircStoppedTimeCircStopped = thisWirelinePreLogging.getCirculationTdEndDateTime();
				
				String dailyUid = CommonUtils.null2EmptyString(thisWirelinePreLogging.getDailyUid());
				String mudPropertiesUid = CommonUtils.null2EmptyString(thisWirelinePreLogging.getMudPropertiesUid());
				String wirelinePreLoggingUid = thisWirelinePreLogging.getWirelinePreLoggingUid().toString();
				String wirelineSuiteUid = thisWirelinePreLogging.getWirelineSuiteUid();
				String wirelineRunUid = thisWirelinePreLogging.getWirelineRunUid();
				
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, WirelinePreLogging.class, "drillerTdMdMsl");
				
				if(drillerTdMdMsl!=null) {
					thisConverter.setBaseValueFromUserValue(drillerTdMdMsl, false);
					drillerTdMdMsl = thisConverter.getBasevalue();	
				}
				if(casingShoeMdMsl!=null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "casingShoeMdMsl");
					thisConverter.setBaseValueFromUserValue(casingShoeMdMsl, false);
					casingShoeMdMsl = thisConverter.getBasevalue();	
				}
				if(maxDeviationMdMsl!=null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "maxDeviationMdMsl");
					thisConverter.setBaseValueFromUserValue(maxDeviationMdMsl, false);
					maxDeviationMdMsl = thisConverter.getBasevalue();	
				}
				if(maxDeviationAngle!=null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "maxDeviationAngle");
					thisConverter.setBaseValueFromUserValue(maxDeviationAngle);
					maxDeviationAngle = thisConverter.getBasevalue();	
				}
				if(rmValue!=null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "rm");
					thisConverter.setBaseValueFromUserValue(rmValue);
					rmValue = thisConverter.getBasevalue();	
				}
				if(rmTemp!=null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "rmTemperature");
					thisConverter.setBaseValueFromUserValue(rmTemp);
					rmTemp = thisConverter.getBasevalue();	
				}
				if(rmfValue!=null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "rmf");
					thisConverter.setBaseValueFromUserValue(rmfValue);
					rmfValue = thisConverter.getBasevalue();
				}
				if(rmfTemp!=null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "rmfTemperature");
					thisConverter.setBaseValueFromUserValue(rmfTemp);
					rmfTemp = thisConverter.getBasevalue();	
				}
				if(rmcResistivityValue!=null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "rmcResistivity");
					thisConverter.setBaseValueFromUserValue(rmcResistivityValue);
					rmcResistivityValue = thisConverter.getBasevalue();
				}
				if(rmcTemp!=null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "rmcTemperature");
					thisConverter.setBaseValueFromUserValue(rmcTemp);
					rmcTemp = thisConverter.getBasevalue();	
				}
				if(holeSize!=null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "bitSize");
					thisConverter.setBaseValueFromUserValue(holeSize);
					holeSize = thisConverter.getBasevalue();
				}
				if(expectedHydrostatic!=null) {
					thisConverter.setReferenceMappingField(WirelinePreLogging.class, "expectedHydrostaticPressure");
					thisConverter.setBaseValueFromUserValue(expectedHydrostatic);
					expectedHydrostatic = thisConverter.getBasevalue();	
				}
				
				String[] paramsFields = {"operationUid", "wirelineSuiteUid"};
				Object[] paramsValues = {operationUid, wirelineSuiteUid};
				
				List listWirelineSuite = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT wirelineSuiteUid FROM WirelineSuite " +
						"WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND wirelineSuiteUid = :wirelineSuiteUid", paramsFields, paramsValues);
				//If WirelineSuite record exist
				if(listWirelineSuite.size() > 0) {
					if(listWirelineSuite.get(0)!=null)
					{
						WirelinePreLoggingUtils.saveExistingWirelineSuite(operationUid, drillerTdMdMsl, casingShoeMdMsl, wirelineSuiteUid, maxDeviationAngle, maxDeviationMdMsl,suitenumber);
						
						String[] paramsFields2 = {"operationUid", "wirelineSuiteUid", "wirelineRunUid"};
						Object[] paramsValues2 = {operationUid, wirelineSuiteUid, wirelineRunUid};
						List listWirelineRun = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("SELECT wirelineRunUid FROM WirelineRun " +
								"WHERE (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND wirelineSuiteUid=:wirelineSuiteUid AND wirelineRunUid = :wirelineRunUid", paramsFields2, paramsValues2);
						//If WirelineSuite record exist
						if(listWirelineRun.size() > 0) {
							if(listWirelineRun.get(0)!=null) {								
							WirelinePreLoggingUtils.saveExistingWirelineRun(operationUid, rmValue, rmTemp, rmfValue, rmfTemp, rmcResistivityValue, rmcTemp, mudSource, holeSize, expectedHydrostatic,
										dateTdReachedTimeTdReached, dateCircStoppedTimeCircStopped, dailyUid, mudPropertiesUid, wirelineRunUid,runNumber);
							}
						} else {
							WirelinePreLoggingUtils.createNewWirelineRun(operationUid, rmValue, rmTemp, rmfValue, rmfTemp, rmcResistivityValue, rmcTemp, mudSource, holeSize, expectedHydrostatic,
									dateTdReachedTimeTdReached, dateCircStoppedTimeCircStopped, dailyUid, mudPropertiesUid, wirelineSuiteUid, runNumber, session.getCurrentGroupUid(), 
									wirelinePreLoggingUid, drillerTdMdMsl, casingShoeMdMsl, maxDeviationAngle, maxDeviationMdMsl);
						}
					}
				}
				//Create new LwdSuite record
				else {
					if (StringUtils.isNotBlank(suitenumber)) {
						WirelinePreLoggingUtils.createNewWirelineSuiteRun(session.getCurrentGroupUid(), operationUid, wirelinePreLoggingUid, suitenumber, runNumber, drillerTdMdMsl, casingShoeMdMsl, 
								rmValue, rmTemp, rmfValue, rmfTemp, rmcResistivityValue, rmcTemp, mudSource, holeSize, expectedHydrostatic,
								dateTdReachedTimeTdReached, dateCircStoppedTimeCircStopped, dailyUid, mudPropertiesUid, maxDeviationAngle, maxDeviationMdMsl);
					}
				}
			}
		}
		if(obj instanceof WirelinePreLoggingCirculation) {
			
			WirelinePreLoggingCirculation preLoggingCirculation = (WirelinePreLoggingCirculation) obj;
			
			if(preLoggingCirculation.getWirelinePreLoggingUid()!=null)
			{
				String wirelinePreLoggingUid = preLoggingCirculation.getWirelinePreLoggingUid();
				String wirelinePreLoggingCirculationUid = preLoggingCirculation.getWirelinePreLoggingCirculationUid();
				String strSql = "SELECT wirelineRunUid from WirelinePreLogging WHERE (wirelineRunUid !=null or wirelineRunUid != '') AND (isDeleted = false or isDeleted is null) AND operationUid = :operationUid AND wirelinePreLoggingUid = :wirelinePreLoggingUid";
				String[] paramsFields = {"operationUid", "wirelinePreLoggingUid"};
				Object[] paramsValues = {session.getCurrentOperationUid(), wirelinePreLoggingUid};
				List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
				if (lstResult.size() > 0)
				{
					String wirelineRunUid = lstResult.get(0).toString();
					
					String[] paramsFields7 = {"wirelineRunUid", "operationUid", "wirelinePreLoggingUid", "wirelinePreLoggingCirculationUid"};
					Object[] paramsValues7 = {wirelineRunUid, session.getCurrentOperationUid(), wirelinePreLoggingUid, wirelinePreLoggingCirculationUid};
					String strSql7 = "UPDATE WirelinePreLoggingCirculation SET wirelineRunUid=:wirelineRunUid WHERE operationUid =:operationUid AND wirelinePreLoggingUid=:wirelinePreLoggingUid AND wirelinePreLoggingCirculationUid=:wirelinePreLoggingCirculationUid";
					ApplicationUtils.getConfiguredInstance().getDaoManager().executeByNamedParam(strSql7, paramsFields7, paramsValues7);
				}
			}
		}
	}
	
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		
		Object object = node.getData();
		
		UserSession session = UserSession.getInstance(request);
		if (object instanceof WirelinePreLogging) {
			WirelinePreLogging wirelinePreLogging = (WirelinePreLogging) object;
			Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSelection.getDailyUid());
			if(daily == null) return;
			String operationUid=userSelection.getOperationUid();
						
			WirelinePreLoggingUtils.getWellOperationDetail(commandBean, node, userSelection, wirelinePreLogging,  operationUid);
			
			wirelinePreLogging.setDailyUid(daily.getDailyUid());
			
			if(wirelinePreLogging.getDailyUid() != null && !StringUtils.isEmpty(wirelinePreLogging.getDailyUid())){
				
				WirelinePreLoggingUtils.getDailySurveyCasingInfo(commandBean, request, session, wirelinePreLogging);
			}			
		}
	}
}
