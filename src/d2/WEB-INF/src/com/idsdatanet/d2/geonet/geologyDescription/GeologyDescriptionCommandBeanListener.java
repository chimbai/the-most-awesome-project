package com.idsdatanet.d2.geonet.geologyDescription;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.Formation;
import com.idsdatanet.d2.core.model.GeologyDescription;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class GeologyDescriptionCommandBeanListener extends com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener{
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception{
		if(targetCommandBeanTreeNode != null){
			if(targetCommandBeanTreeNode.getDataDefinition().getTableClass().equals(GeologyDescription.class)) {
				if("formationName".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
					formationChanged(request, commandBean, targetCommandBeanTreeNode);
				}
			}
		}
	}
	
	private void formationChanged(HttpServletRequest request, CommandBean commandBean, CommandBeanTreeNode node) throws Exception {
		if(Action.DELETE.equals(node.getAtts().getAction())) return; //why do we need to test for "delete" ? ssjong - 04/may/2009
		
		CustomFieldUom thisConverter = new CustomFieldUom(node.getCommandBean());
		
		GeologyDescription thisDescription = (GeologyDescription) node.getData();
		
		UserSession userSession = UserSession.getInstance(request);
		
		//show the correct unit as well
		String formationUid = thisDescription.getFormationName();
		if (StringUtils.isNotBlank(formationUid)) {
			
				Formation formation = this.getFormation(formationUid, true);
				if (formation !=null && formation.getSampleTopMdMsl()!=null)
				{
					thisConverter.setReferenceMappingField(GeologyDescription.class, "topDepthMdMsl");
					
					thisConverter.setBaseValue(formation.getSampleTopMdMsl());
					thisDescription.setTopDepthMdMsl(thisConverter.getConvertedValue());
				}
				formation = this.getFormation(formationUid, false);

				if (formation!=null && formation.getSampleTopTvdMsl()!=null)
				{
					
					thisConverter.setReferenceMappingField(GeologyDescription.class, "topSubseaTvd");
					thisConverter.setBaseValue(formation.getSampleTopTvdMsl());
					thisDescription.setTopSubseaTvd(thisConverter.getConvertedValue());
				}
			
		}
		
	
		
		
	}

	public static Formation getFormation(String formationUid,Boolean datumConversionEnabled) throws Exception
	{
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		qp.setDatumConversionEnabled(datumConversionEnabled);
		List<Object> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Formation where (isDeleted is null or isDeleted = false) and formationUid=:formationUid", "formationUid", formationUid,qp);
		if (result.size()>0)
			return (Formation)result.get(0);
		return null;
	}
	
}
