package com.idsdatanet.d2.geonet.formationSampling;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

import com.idsdatanet.d2.core.dao.QueryProperties;
import com.idsdatanet.d2.core.model.FormationFluid;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;


public class FormationSamplingCommandBeanListener extends EmptyCommandBeanListener {
	public void onSubmitForServerSideProcess(CommandBean commandBean, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode) throws Exception {
		if(FormationFluid.class.equals(targetCommandBeanTreeNode.getDataDefinition().getTableClass())){
			if("wirelineUid".equals(targetCommandBeanTreeNode.getInfo().getSubmitForServerSideProcessTargetField())){
				
				String wirelineUid = (String) PropertyUtils.getProperty(targetCommandBeanTreeNode.getData(), "wirelineUid");
				this.getWirelineInfo(targetCommandBeanTreeNode, wirelineUid, commandBean);
				
			}
		}
	}
	
	private void getWirelineInfo (CommandBeanTreeNode node, String wirelineUid, CommandBean commandBean) throws Exception {
		QueryProperties qp = new QueryProperties();
		qp.setUomConversionEnabled(false);
		
		Object object = node.getData();
		if (object instanceof FormationFluid) {
			
			FormationFluid formationFluid = (FormationFluid) object;
			String strSql = "Select maxDeviation FROM WirelineRun where wirelineRunUid = :wirelineRunUid and " +
					"(isDeleted = false or isDeleted is null)";
			List lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, "wirelineRunUid" ,wirelineUid, qp);
		
			if (lstResult.size() > 0){
				
				Object a  = lstResult.get(0);
				CustomFieldUom thisConverter = new CustomFieldUom(commandBean, FormationFluid.class, "maxDeviationAngle");
				if (a !=null){
					thisConverter.setBaseValue(Double.parseDouble(a.toString()));
					formationFluid.setMaxDeviationAngle(thisConverter.getConvertedValue());
				}

			}

		}
	}
}
