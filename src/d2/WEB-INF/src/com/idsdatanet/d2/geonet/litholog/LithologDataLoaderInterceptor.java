package com.idsdatanet.d2.geonet.litholog;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.InitializingBean;

import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataDefinitionHQLQuery;
import com.idsdatanet.d2.core.web.mvc.DataLoaderInterceptor;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class LithologDataLoaderInterceptor implements DataLoaderInterceptor, InitializingBean{
	
	public void afterPropertiesSet() throws Exception {
    }
	
	private static final String CUSTOM_CONDITION_MARKER = "{_custom_condition_}";
	
	public DataDefinitionHQLQuery generateHQL(CommandBean commandBean,
			UserSelectionSnapshot userSelection, HttpServletRequest request,
			TreeModelDataDefinitionMeta meta, DataDefinitionHQLQuery query,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLConditionClause(String conditionClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			DataDefinitionHQLQuery query, CommandBeanTreeNode parentNode)
			throws Exception {
		
		if(conditionClause.indexOf(CUSTOM_CONDITION_MARKER) < 0) return null;
		String customCondition = "";
		String errorMsg = "";
		String filter = "";
		String fromDepthValue = "";
		String toDepthValue = "";
		String mainLithologyValue = "";
		
		if (parentNode.getDynaAttr().get("filterButton") != null){
			filter = (String) parentNode.getDynaAttr().get("filterButton");
			String[] Array = filter.split(",");
			
			if (Array.length>0){
				fromDepthValue = Array[0];
			}
			if (Array.length>1){
				toDepthValue = Array[1];
			}
			if (Array.length>2){
				mainLithologyValue = Array[2];
			}
		}
		
		parentNode.getDynaAttr().put("fromDepth", fromDepthValue);
		parentNode.getDynaAttr().put("toDepth", toDepthValue);
		parentNode.getDynaAttr().put("mainLithology", mainLithologyValue);
		
		if (fromDepthValue!=null && !"".equals(fromDepthValue.toString()) ){				
			if(NumberUtils.isNumber(fromDepthValue.toString())){
				Double fromDepth = Double.parseDouble(fromDepthValue.toString());
					customCondition += " AND (depthFromMdMsl>=:fromDepth)";
					query.addParam("fromDepth", fromDepth);						
			}
			else
			{
				errorMsg = "\"From Depth\""; 						
			}
		}
			
		if (toDepthValue!=null && !"".equals(toDepthValue.toString()) ){				
			if(NumberUtils.isNumber(toDepthValue.toString())){
				Double toDepth = Double.parseDouble(toDepthValue.toString());
				customCondition += " AND (depthFromMdMsl<=:toDepth)";
				query.addParam("toDepth", toDepth);					
			}
			else
			{
				if(StringUtils.isNotBlank(errorMsg))
					errorMsg = errorMsg + " and "+ "\"To Depth\""; 
				else
					errorMsg = "\"To Depth\""; 
			}
		}
		
		//dynaValue = parentNode.getDynaAttr().get("mainLithology");
		if (mainLithologyValue!=null) {
			if (!"".equals(mainLithologyValue.toString())) {
				customCondition += " AND mainLithology=:mainLithology";
				query.addParam("mainLithology", mainLithologyValue.toString());
			}
		}
		
		if(StringUtils.isNotBlank(errorMsg))
		{
			errorMsg = "Invalid " + errorMsg + " value.";
			commandBean.getSystemMessage().addError(errorMsg);
		}
				
		return conditionClause.replace(CUSTOM_CONDITION_MARKER, customCondition);
	}

	public String generateHQLFromClause(String fromClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public String generateHQLOrderByClause(String orderByClause,
			CommandBean commandBean, UserSelectionSnapshot userSelection,
			HttpServletRequest request, TreeModelDataDefinitionMeta meta,
			CommandBeanTreeNode parentNode) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
