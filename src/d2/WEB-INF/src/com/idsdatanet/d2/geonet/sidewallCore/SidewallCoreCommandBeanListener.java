package com.idsdatanet.d2.geonet.sidewallCore;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class SidewallCoreCommandBeanListener extends EmptyCommandBeanListener  {
	public void afterDataLoaded(CommandBean commandBean, CommandBeanTreeNode root, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception{
		if (StringUtils.isNotBlank(userSelection.getOperationUid())) {
			String thisOperationUid = userSelection.getOperationUid();
			List list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM Formation WHERE (isDeleted = false or isDeleted is null) and operationUid = :thisOperationUid", "thisOperationUid", thisOperationUid);
			if (list == null || list.size() == 0) 
			{
				commandBean.getSystemMessage().addError("No formation data is found for this well");
			}
		}
	}
}
