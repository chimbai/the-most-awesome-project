package com.idsdatanet.d2.common.wellfilter;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.infra.flash.AbstractFlashComponentConfiguration;
import com.idsdatanet.d2.core.web.mvc.UserSession;

public class WellFilterFlashComponentConfiguration extends AbstractFlashComponentConfiguration{
	public String getFlashvars(UserSession userSession, HttpServletRequest request, String sessionId, String targetSubModule) throws Exception{
		String baseUrl = "../../";
		
		return "d2Url=" + urlEncode(baseUrl) +
				"&currentView=" + urlEncode(userSession.getCurrentView());
	}
	
	public String getFlashComponentId(String targetSubModule){
		return "well_filter_builder";
	}


}
