package com.idsdatanet.d2.common.scheduler.jobs;

import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobResponse;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.publishsubscribe.PublishSubscribeManager;

public class PubSubJob implements D2Job {
	
	@Override
	public void run(UserContext userContext, JobResponse jobResponse) throws Exception {
		PublishSubscribeManager.getConfiguredInstance().startPublisherWorkerJobIfNecessary();
	}

	@Override
	public void dispose() {}
}
