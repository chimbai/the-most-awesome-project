<html>
    <head><style>
      body {
        margin: 0px;
        padding: 0px;
      }
    </style>
    <script type='text/javascript' src="${visualizejsUtils.getJasperServerBaseUrl()}/client/visualize.js?baseUrl=${visualizejsUtils.getJasperServerBaseUrl()?url}"></script>
    	<script src="http://code.jquery.com/jquery-2.1.0.js"></script>
		
    	<script type="text/javascript" src="script/d3/schematic/jquery-latest.js"></script>
  		<script type="text/javascript" src="script/d3/schematic/jquery-ui-latest.js"></script>
 		<script type="text/javascript" src="script/d3/schematic/jquery.layout-latest.js"></script>
 		<script type="text/javascript" src="script/generic/d3.js"></script>
 		<script type="text/javascript" src="script/d3/schematic/accordion.js"></script>
 		<script type="text/javascript" src="script/d3/schematic/underscore-min.js"></script>
 		 
    </head>
    <body>
    	<script >
    		authenticate = ${visualizejsUtils.getAuthentication()};
    		
    		$(document).ready(function () {
		        $('body').layout({
		            applyDefaultStyles: true,
		
		            west__resizable: true,
		            west__minSize: 420,
		            west__closable: false,
		        });
		    });
    	</script>
    	<div id='mainContainer' class="ui-layout-center">
		</div>
		<div id='filterContainer' class="ui-layout-west">
		</div>
		<script type="text/javascript" src="script/jasper/JasperViewerEvent.js"></script>
    	<script type="text/javascript" src="script/jasper/ReportViewer.js"></script>
		<script src="script/jasper/RigWellFilter.js"></script>
		<script type="text/javascript" src="script/jasper/ReportToolbar.js"></script>
		<script src="script/jasper/report_9.js"></script>
    	
		
    </body>
</html>