package com.idsdatanet.d2.common.scheduler.jobs;

import org.apache.commons.lang.StringUtils;
import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobResponse;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.depot.core.DepotConstants;
import com.idsdatanet.depot.core.mapping.DepotMappingModule;
import com.idsdatanet.depot.d2.filebridge.FileBridgeWebSocketClientPoint;
import com.idsdatanet.depot.d2.filebridge.WebSocketClientEndpoint;
import com.idsdatanet.depot.d2.filebridge.util.FileBridgeUtils;

public class FileBridgeWebSocketJob implements D2Job {
	WebSocketClientEndpoint fileBridgeWebSocketClientPoint = null;
	
	public FileBridgeWebSocketJob() {}
	public FileBridgeWebSocketJob (String serverPropertiesUid, String config) throws Exception {
		if(StringUtils.isBlank(serverPropertiesUid)) throw new Exception("Server Properties NOT Set!!");
		if(StringUtils.isBlank(config)) throw new Exception("Socket configuration NOT Set!!");
		else {
			if(FileBridgeWebSocketClientPoint.getConfiguredInstance().getActiveWsConnection() == null) {
				FileBridgeWebSocketClientPoint.getConfiguredInstance().setActiveWsConnection(new WebSocketClientEndpoint());
			}
			
			ImportExportServerProperties server = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, serverPropertiesUid);
			FileBridgeWebSocketClientPoint.getConfiguredInstance().getActiveWsConnection().updateConn(server, config);
			fileBridgeWebSocketClientPoint = FileBridgeWebSocketClientPoint.getConfiguredInstance().getActiveWsConnection();
			
		}
		
	}
	
	@Override
	public void run(UserContext userContext, JobResponse jobResponse) throws Exception {
		//fail if rest service is enabled
		String errorMsg = FileBridgeUtils.checkEnabledFileBridgeJob();
		if(StringUtils.isNotBlank(errorMsg)) {
			throw new Exception(errorMsg);
		}

		/**
		 * A simple workaround to error "[javax.xml.bind.JAXBException: "com.idsdatanet.depot.core.mapping.beanmappings.jaxb" doesn't contain ObjectFactory.class or jaxb.index]" 
		 * when a reply from server is sent via com.idsdatanet.depot.d2.thirdparty.FileBridgeCentralClientEndpoint.onMessage(String, boolean). This error will happen when the server
		 * has restarted and the scheduler kicks in 1st, before any bean and relationship mappings are loaded.
		 * 
		 * What happens is com.idsdatanet.depot.d2.thirdparty.FileBridgeCentralClientEndpoint.onMessage(String, boolean) creates a new thread that may not contain the correct path for 
		 * "com.idsdatanet.depot.core.mapping.beanmappings.jaxb". Unless a related screen to load bean and relationship mapping (like depotcontrolpanel screen) is loaded first, the error 
		 * will be thrown.  
		 * 
		 * The following line will load depot bean mappings and relationship 1st, so that OnMessage can use them without having to go through creating a new instance of 
		 * "com.idsdatanet.depot.core.mapping.beanmappings.jaxb" (at com.idsdatanet.depot.core.mapping.DepotMappingModule.getDefaultStoreVersionByType(String))
		 * 
		 * **/
	
		DepotMappingModule.getConfiguredInstance().getDefaultStoreVersionByType(DepotConstants.D2);
		
		fileBridgeWebSocketClientPoint.run(userContext, jobResponse);
	
	}

	@Override
	public void dispose() {}
	
	public void disconnect() {
		fileBridgeWebSocketClientPoint.disconnect();
	}

}