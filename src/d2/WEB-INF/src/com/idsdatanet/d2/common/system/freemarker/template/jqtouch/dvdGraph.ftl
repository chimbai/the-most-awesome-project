<div id="dvdgraph-${getDynamicFieldValue(form.root, "@operationUid")}" class="metal">
    <div class="toolbar">
        <a class="back" href="#">Back</a>
        <h1>${getDynamicFieldValue(form.root, "@operationName")}</h1>
    </div>
	<div id="chart-container-${getDynamicFieldValue(form.root, "@operationUid")}">
	</div>
</div>