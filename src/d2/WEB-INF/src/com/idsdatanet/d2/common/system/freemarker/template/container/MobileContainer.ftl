<!DOCTYPE HTML>
<#assign v_resources_path = "${contextPath}/vresources/cache/${systemStartupRandomValue}/htmllib"/>
<html>
	<head>
		<title>IDS DataNet 2.5 Mobile</title>
		<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
		<link rel="icon" type="image/x-icon" href="images/favicon.ico">
		<link rel="manifest" href="images/favicon/site.webmanifest">
		<link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#2d89ef">
		<meta name="msapplication-config" content="images/favicon/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="${v_resources_path}/script/bootstrap413/css/bootstrap.min.css">
		<style type="text/css">
			body, html { 
				font-family: helvetica,arial,sans-serif; 
				font-size: 90%; 
				width: 100%; 
				height: 100%; 
				margin: 0;
				overflow: hidden; 
			}
		    img, a {
		    	-webkit-touch-callout: none;
				-webkit-user-select: none;
			}
			.main-content-div{
				padding: 0;
				border: 0;
				margin: 0;
				position: absolute;
				top: 0;
				left: 0;
				right: 0;
				bottom: 0;
			}
			.main-content-iframe{
				padding: 0;
				border: 0;
				margin: 0;
				width: 100%;
				height: 100%;			
			}
		}
        </style>
        <script type="text/javascript" src="${v_resources_path}/jquery/jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="${v_resources_path}/script/bootstrap413/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="${v_resources_path}/script/d3-MobileContainerManager.js"></script>
        <script>
        	$(document).ready(function() {
       			var topNavigationBar = document.getElementById("topNavigationBar");
        		var mainCotentDiv = document.getElementById("mainContentDiv");
        		mainContentDiv.style.top = topNavigationBar.offsetHeight + "px";
        		
        		window.containerManager.contextPath = "${contextPath}";
				window.containerManager.startSessionValidityPing(${javascriptKeepAliveIntervalSeconds});
        	});
        </script>
	</head>
	<body oncontextmenu="return false;">
		<nav class="navbar navbar-expand-sm fixed-top navbar-dark bg-dark device-fixed-height" id="topNavigationBar" style="padding: 5px">
			<div><img id="company_logo" style="max-height:37px;" src="images/mobile_company_logo.png"/></div>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsMobile" aria-controls="navbarsMobile" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarsMobile">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="mobileoperationinformationcenter.html">Operation Daily Overview</a>
					</li>
				</ul>
				<ul class="navbar-nav navbar-right ml-auto">
		            <li class="nav-item">
		                <a class="nav-link" href="logout.jsp">Sign out</a>
		            </li>
		        </ul>
			</div>
		</nav>
		<div class="main-content-div" id="mainContentDiv">
			<iframe class="main-content-iframe" src="${contentTabs.getUrl('data')}"></iframe>
		</div>
	</body>
</html>