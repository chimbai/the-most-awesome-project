<!DOCTYPE HTML>
<html>
	<body style="overflow-x: hidden; overflow-y: hidden;background-color:#ffffff;">
	<#assign v_resources_path = "${form.getVirtualResourcePath()}/htmllib"/>
	<#include "/core/defaultJavascriptAndStyleTemplate.ftl" />

	<link rel="stylesheet" type="text/css" href="${v_resources_path}/assets/vendors/font-awesome/css/fontawesome-all.min.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/goldbox/css/OffsetWellFilter.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/select2/select2.min.css"/>
	
	<script type="text/javascript" src="${v_resources_path}/jquery/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="${v_resources_path}/select2/select2.min.js"></script>
	<script type="text/javascript" src="${v_resources_path}/jquery/jquery-ui-1.13.0.min.js"></script>
	
	<!-- chart -->
	<script type="text/javascript" src="${v_resources_path}/goldbox/js/libs/chart.min.js"></script>
	
	<!-- screen logic -->
	<script type="text/javascript" src="${v_resources_path}/goldbox/js/FilterField.js"></script>
	<script type="text/javascript" src="${v_resources_path}/goldbox/js/MessageManager.js"></script>
	<script type="text/javascript" src="${v_resources_path}/goldbox/js/OperationQatStatus.js"></script>
	
	
	<style type="text/css">
		.body {
			box-sizing: border-box;
		}
	
		.view-chart-container{
			display: flex;
			flex-direction: column;
			overflow: hidden;
			background-color: white;
			position: absolute;
			top: 0px;
			left: 0px;
			width: 100%;
			height: 100%;
			border: 1px solid;
			border-color: #888;
		}
		
		.close-chart-details-i::after {
			content: '\f00d';
		    font-family: 'FontAwesome';
		    text-align: center;
		    cursor: pointer;
		    margin: 0px 0px 0px 0px;
		    display: inline-block;
		    font-weight: 100;
		    font-size: 15px;
		    height: 18px;
		    width: 18px;
		    line-height: 18px;
		    right: 2px;
		    top: 2px;
		    position: absolute;
		    color: #888;
		   
		}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
		
		.view-chart-footer {
			padding: 2px;
			justify-content: center;
			display: flex;
			min-height: 34px;
			padding: 0px;
			margin: 0px;
			box-sizing: border-box;
			align-items: center;
			flex-wrap: wrap;
			
		}
		
		.view-chart-header {
			position: relative;
			height: 18px;
		}
		
		.view-chart-info{
			flex: 2 1 auto;
			white-space: normal;
			overflow-x: hidden;
			overflow-y: auto;
		}
		
		.chart-container {
			display: flex;
			flex-direction: row;
			flex-wrap: wrap;
			padding: 0px;
			margin:0px;
			box-sizing: border-box;
			align-items: flex-start;
			width: 100%;
   			height: 100%;
   			overflow-x: hidden;
   			overflow-y: auto;
   			border-top: 1px solid darkgray;
   			border-bottom: 1px solid darkgray;
   			align-content: flex-start;
		}
		
		.chart-wrapper-container {
			flex: 0 0 auto;
			position: relative;
			margin: 5px 0px 0px 5px;
			white-space: nowrap;
			width: 13%;
		}
		
		.chart-info-wrapper {
			width: 80%;
			margin: auto;
			line-height: 20px;
		}
		
		.main-page-panel {
			overflow: hidden;
			width: 100%;
			height: 100%;
			display: flex;
			flex-direction: column;
		}
		
		.main-page-header {
			min-height: 40px;
			padding-bottom: 5px;
			flex-wrap: nowrap;
		}
		
		.main-page-content {
			flex: 1;
			padding-bottom: 5px;
		}
		
		.main-page-footer {
			height: 30px;
		}
		
		.prev-i::after {
			content: '\f104';
		    font-family: 'FontAwesome';
		    text-align: center;
		    cursor: pointer;
		    margin: 0px 2px 0px 0px;
		    display: inline-block;
		    font-weight: 100;
		    font-size: 15px;
		    height: 30px;
		    width: 30px;
		    line-height: 28px;
		    background-color: rgb(128, 128, 128);
			color: rgb(237, 239, 244);
		}
		
		.next-i::after {
			content: '\f105';
		    font-family: 'FontAwesome';
		    text-align: center;
		    cursor: pointer;
		    margin: 0px 2px 0px 0px;
		    display: inline-block;
		    font-weight: 100;
		    font-size: 15px;
		    height: 30px;
		    width: 30px;
		    line-height: 28px;
		    background-color: rgb(128, 128, 128);
			color: rgb(237, 239, 244);
		}
		
		.pagination-control {
			display: flex;
			align-items: center;
			flex-direction: row;
			justify-content: center;
			margin-left: -35px
		}
		
		.page-no {
			width: 30px;
			height: 30px;
			text-align: center;
			cursor: pointer;
			line-height: 28px;
		}
		
		.page-left-i::before{
			content: '\f104';
		    font-family: 'FontAwesome';
		    text-align: center;
		    cursor: pointer;
		    margin: 0px 2px 0px 0px;
		    display: inline-block;
		    font-weight: 700;
		    font-size: 15px;
		    height: 30px;
		    width: 30px;
		    line-height: 28px;
		}
		
		.page-right-i::before{
			content: '\f105';
		    font-family: 'FontAwesome';
		    text-align: center;
		    cursor: pointer;
		    margin: 0px 2px 0px 0px;
		    display: inline-block;
		    font-weight: 700;
		    font-size: 15px;
		    height: 30px;
		    width: 30px;
		    line-height: 28px;
		}
		
		.page-current {
			color: #08c;
			font-size: 15px;
			font-weight: 700;
		}
		
		.page-no:hover {
			background-color: rgb(128, 128, 128);
			color: rgb(237, 239, 244);
		}
		
		.page-disabled,
		.page-ellipsis {
			pointer-events: none;
			color: lightgrey;
		}
		
		.pagesize-filter {
			display: flex;
		    justify-content: right;
		    flex: 1;
		    padding-right: 10px !important;
		}
		
		.view-chart-container.hide-item {
			display: none;
		}
		
		.chart-container .no-chart-data-wrapper{
			display: flex;
			width: 100%;
			height: 100%;
			justify-content: center;
			align-items: center;
			font-size: 25px;
		}

	</style>
	

	
	
	<script type="text/javascript">
		$(document).ready(function(){
			var operationStatus = new D3.OperationQatStatus({
				renderTo: "main-page-panel",
				data: ${form.getJsonData()},
				currentOffsetWellfilterParameters: ${form.getOffsetWellFilterParameters()}
			});
		});
	</script>
	
	<div id="main-page-panel" class='w100 h100 main-page-panel'></div>
	
	</body>
</html>