<html>
    <head>
    	<style>
      		body {
        		margin: 0px;
        		padding: 0px;
      		}
    	</style>

		<script type='text/javascript' src="${visualizejsUtils.getJasperServerBaseUrl()}/client/visualize.js?baseUrl=${visualizejsUtils.getJasperServerBaseUrl()?url}"></script>
    	<script src="http://code.jquery.com/jquery-2.1.0.js"></script>
    	<script src="http://underscorejs.org/underscore-min.js"></script>
		<script type="text/javascript" src="script/d3/schematic/jquery-latest.js"></script>
		<script type="text/javascript" src="script/d3/schematic/jquery-ui-latest.js"></script>
		<script type="text/javascript" src="script/d3/schematic/jquery.layout-latest.js"></script>
		
		
		
    	<link rel="stylesheet" type="text/css" href="script/d3/schematic/jquery.datepick.css"/>		
		<script type="text/javascript" src="script/d3/schematic/jquery.datepick.pack.js"></script>
		<script type="text/javascript" src="script/d3/schematic/jquery.datepick-en-GB.js"></script>
		
    </head>
    <body>
    	<script >
    		authenticate = ${visualizejsUtils.getAuthentication()};
    	</script>
    	
    	<script src="script/jasper/sample_report.js"></script>
    	<div class="ui-layout-center">
    		Export to: 
            <select id="outputType">
                <option value="pdf">PDF</option>
                <option value="xlsx">XLSX</option>
                <option value="xls">XLS</option>
                <option value="xml">XML</option>                
                <option value="docx">DOCX</option>
                <!--
                <option value="rtf">RTF</option>
                <option value="csv">CSV</option>
                <option value="odt">ODT</option>
                <option value="ods">ODS</option> -->
            </select>
    		<button id="button" disabled>Export</button>
    		<button id="previousPage">Drill Up</button>
		    <div id="container"></div>
		</div>
		<div class="ui-layout-west">
			Start Date: </P>
		    <input id="startDateInput" type="text" style="width:90%"></input> </P>
		    
		    End Date: </P>
		    <input id="endDateInput" type="text" style="width:90%"></input> </P>
		    
		    <button id="btnRun">Run</button>
		</div>
    </body>
</html> 