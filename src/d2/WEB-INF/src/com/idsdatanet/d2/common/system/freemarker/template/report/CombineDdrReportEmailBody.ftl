<html>
<body>	
<#if wellNameList?exists>
<p>
		<table border="1">
			<tr>
				<td>Well Name</td>
			</tr>
			<#list wellNameList? keys as wellUid>
				
				<tr>
					<td>${wellNameList[wellUid]["wellName"]}</td>
				</tr>
				
			</#list>
		</table>
	</p>
</#if>
-------------------------------------------		
<p>This is an <b>automated message.</b></p>
<p>Please do not reply to the address above.</p> 
<p>Thank you.</p>
<p><b>IDS Datanet Service</b></p>
</body>
</html>