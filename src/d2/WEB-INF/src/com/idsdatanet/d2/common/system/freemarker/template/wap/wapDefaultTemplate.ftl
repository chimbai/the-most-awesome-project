<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<#include "/templateHelper.ftl"/>
<#include "/core/d2Helper.ftl"/>

<head>
	<title>${getFormTitle("IDS Datanet")}</title>
</head>

<body>
	<#if form.info.getCustomViewProperty("accessDenied")?default("false") == "true">
		<h2>Access denied</h2>
		<p>We apologize, but you do not have permission to access this page.</p>
	<#else>
		<#if form.info.isFormValid()>
			<form name="form" id="form" method=post action="${springMacroRequestContext.getRequestUri()}">
				<#if form??>
					<#if form.info.getCustomView("data")??>
						<#include form.info.getCustomView("data")/>
					</#if>
				</#if>
			</form>
		</#if>
	</#if>
	
	<br/>
	<a href="logout.jsp">Logout</a>
	<br/>&copy;&nbsp;Independent Data Services (Global) Pte. Ltd.
	
</body>
</html>
