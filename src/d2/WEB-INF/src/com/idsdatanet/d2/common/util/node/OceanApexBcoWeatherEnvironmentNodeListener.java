package com.idsdatanet.d2.common.util.node;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.WeatherEnvironment;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class OceanApexBcoWeatherEnvironmentNodeListener extends GenericNodeListener {
    
    @Override
    public void afterNodeCreate(Object object, BaseCommandBean commandBean, EmptyCommandBeanListener commandBeanListener, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        UserSelectionSnapshot userSelection = commandBean.getCurrentUserSelectionSnapshot();
        if (object != null && object instanceof WeatherEnvironment) {
            WeatherEnvironment w = (WeatherEnvironment) object;

            // Setting ReportTime (Default to 24:00)
            String dailyUid = userSelection.getDailyUid();
            if (dailyUid != null && StringUtils.isNotBlank(dailyUid)) {
                Daily daily = (Daily) ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(Daily.class, dailyUid);
                if (daily != null) {
                    Date date = daily.getDayDate();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    cal.set(Calendar.HOUR_OF_DAY, 23);
                    cal.set(Calendar.MINUTE, 59);
                    cal.set(Calendar.SECOND, 59);
                    date = cal.getTime();
                    w.setReportDatetime(date);
                }
            }
        }
    }
    
}