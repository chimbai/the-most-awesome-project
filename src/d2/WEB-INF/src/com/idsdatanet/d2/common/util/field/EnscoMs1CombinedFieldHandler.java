package com.idsdatanet.d2.common.util.field;

import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.common.util.RegexUtil;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class EnscoMs1CombinedFieldHandler extends GenericFieldHandler {
    
    private String targetFields = null;
    private String targetTypes = null;
    private String targetUnits = null;
    private String delimiter = ":";

    @Override
    public Object translate(Object value, Object object, BaseCommandBean commandBean, ExcelMapping mapping, List<Object> objList, Workbook book, Cell cell) throws Exception {
        Object v = null;
        if (value != null && value instanceof String && StringUtils.isNotBlank(value.toString())) {
            if (targetFields == null || targetTypes == null || targetUnits == null) {
                commandBean.getSystemMessage().addError("Cell: [" + mapping.getColumnIndex() + "," + mapping.getRowIndex() + "] has invalid input.");
                return null;
            } else {
                String[] fields = this.targetFields.split(delimiter);
                String[] types = this.targetTypes.split(delimiter);
                String[] units = this.targetUnits.split(delimiter);
                if (fields.length == types.length && types.length == units.length) {
                    int numberOfFields = fields.length;
                    String[] values = value.toString().split(delimiter);
                    for (int i = 0; i < numberOfFields; i++) {
                        try {
                            Object rv = values[i];
                            if (StringUtils.isBlank(rv.toString())) {
                                continue;
                            }
                            rv = RegexUtil.parseType(rv, types[i]); // Cell Type Handling
                            if (ExcelMapping.UOM_TYPE_LIST.contains(types[i])) {
                                rv = ExcelImportTemplate.processUomConversion(object, fields[i], units[i], commandBean, Double.parseDouble(rv.toString()));
                            }
                            if (mapping.getIds().equals(fields[i])) {
                                v = rv;
                            } else if (rv != null && PropertyUtils.isWriteable(object, fields[i])) {
                                PropertyUtils.setProperty(object, fields[i], rv);
                            }
                        } catch (ArrayIndexOutOfBoundsException er) {
                            break;
                        } catch (NumberFormatException er) {
                            commandBean.getSystemMessage().addError("Cell: [" + mapping.getColumnIndex() + "," + mapping.getRowIndex() + "] contains non-numbers. Provided: " + value);
                            return null;
                        }
                    }
                } else {
                    commandBean.getSystemMessage().addError("Cell: [" + mapping.getColumnIndex() + "," + mapping.getRowIndex() + "] has invalid setup definitions.");
                    return null;
                }
            }
        }
        return v;
    }

    public String getTargetFields() {
        return targetFields;
    }

    public void setTargetFields(String targetFields) {
        this.targetFields = targetFields;
    }

    public String getTargetTypes() {
        return targetTypes;
    }

    public void setTargetTypes(String targetTypes) {
        this.targetTypes = targetTypes;
    }

    public String getTargetUnits() {
        return targetUnits;
    }

    public void setTargetUnits(String targetUnits) {
        this.targetUnits = targetUnits;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }
}