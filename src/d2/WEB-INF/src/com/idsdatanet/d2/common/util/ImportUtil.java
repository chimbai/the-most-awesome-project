package com.idsdatanet.d2.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;

public class ImportUtil extends DefaultBeanSupport {
	
    public static final String TEMP_UPLOAD_FILE_LOCATION = "WEB-INF/temp";
	
	public static File getFileFromHttpServletRequest(HttpServletRequest request, String fileKey) throws Exception {
        if (request == null || StringUtils.isBlank(fileKey)) return null;
        if (request instanceof MultipartHttpServletRequest) {
            return ImportUtil.getFileFromMultipartHttpServletRequest((MultipartHttpServletRequest) request, fileKey);
        }
        return null;
    }
	
	public static File getFileFromMultipartHttpServletRequest(MultipartHttpServletRequest multipartHttpServletRequest, String fileKey) throws Exception {
        if (multipartHttpServletRequest == null || StringUtils.isBlank(fileKey)) return null;
        return ImportUtil.convertMultipartToFile(multipartHttpServletRequest.getFile(fileKey));
    }
	
	public static File convertMultipartToFile(MultipartFile multipart) throws Exception {
        if(multipart == null) return null;
	    File convFile = new File(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "_" + multipart.getOriginalFilename());
	    if (convFile.exists()) { convFile.delete(); }
        multipart.transferTo(convFile);
        return convFile;
    }
	
	public static Object getNewObject(String className) throws Exception {
	    if (StringUtils.isNotBlank(className)) {
            Class<?> clazz = Class.forName("com.idsdatanet.d2.core.model." + className);
            Object o = clazz.newInstance();
            return o;
	    }
	    return null;
	}
	
	public static String saveFile(File file) throws Exception {
	    String rootPath = ApplicationConfig.getConfiguredInstance().getServerRootPath();
	    String filePath = ImportUtil.TEMP_UPLOAD_FILE_LOCATION + "/" + file.getName();
	    File fileDir = new File(new File(rootPath), filePath);
        fileDir.getParentFile().mkdirs();
	    FileInputStream fileInputStream = new FileInputStream(file);
	    Files.copy(fileInputStream, Paths.get(rootPath + filePath), StandardCopyOption.REPLACE_EXISTING);
	    fileInputStream.close();
	    file.delete(); // Temp file
	    return rootPath + filePath;
	}
	
	/**TODO: Maybe: Should we add a clean-up function of empty folders and "orphaned files"?
	 * Not done yet because of potential conflicts if simultaneous imports happen at the same time (deleting a just uploaded but not processed file?)
	 * In theory possible, practically not.
	*/

}
