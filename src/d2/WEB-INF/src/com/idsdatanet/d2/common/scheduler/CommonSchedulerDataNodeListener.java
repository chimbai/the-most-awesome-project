package com.idsdatanet.d2.common.scheduler;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.idsdatanet.d2.core.model.SchedulerJobDetail;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.DataNodeProcessStatus;
import com.idsdatanet.d2.core.web.mvc.EmptyDataNodeListener;
import com.idsdatanet.d2.core.web.mvc.TreeModelDataDefinitionMeta;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.infra.scheduler.ConfigurableScheduler;
import com.idsdatanet.d2.infra.scheduler.SchedulerServiceType;
import com.idsdatanet.depot.d2.filebridge.FileBridgeWebSocketClientPoint;

public class CommonSchedulerDataNodeListener extends EmptyDataNodeListener{
	
	public static final String FREQUENCY_MONTH = "m";
	public static final String FREQUENCY_WEEK = "w";
	
	public void afterDataNodeLoad(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if (obj instanceof SchedulerJobDetail)
		{
			SchedulerJobDetail detail = (SchedulerJobDetail) obj;
			SchedulerServiceType serviceType = (SchedulerServiceType) node.getParent().getData();
			node.getDynaAttr().put("oldIsActive",isTrue(detail.getIsActive())?"1":"0");
			node.getDynaAttr().put("type", serviceType.getType());
		}
		if (obj instanceof SchedulerServiceType)
		{
			setIsNBBDeployedModule(node);
		}
	}

	public void afterDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception {
		Object obj = node.getData();
		if (obj instanceof SchedulerJobDetail)
		{
			SchedulerJobDetail jobDetail = (SchedulerJobDetail) obj;
			SchedulerServiceType serviceType = (SchedulerServiceType) node.getParent().getData();
			if (BaseCommandBean.OPERATION_PERFORMED_SAVE_NEW != operationPerformed)
			{
				ConfigurableScheduler.getConfiguredInstance().removeTrigger(serviceType, jobDetail);
			}
			if (isTrue(jobDetail.getIsActive()))
				ConfigurableScheduler.getConfiguredInstance().addTrigger(serviceType, jobDetail);
		}
	}

	Boolean isTrue(Boolean bol)
	{
		return bol==null?true:bol;
	}
	
	private void setCronExpression(SchedulerJobDetail jobDetail, List<String> dayOfWeek)
	{
		String cronExpression = "0 m h d * w ";
		
		
		if (jobDetail.getServiceFrequency().startsWith("every"))
		{
			if (jobDetail.getServiceFrequency().equalsIgnoreCase("every_min")){
				cronExpression = cronExpression.replaceAll("m","0/"+jobDetail.getEveryValue());
				cronExpression = cronExpression.replaceAll("h","*");
			}else if (jobDetail.getServiceFrequency().equalsIgnoreCase("every_hour"))
			{
				cronExpression = cronExpression.replaceAll("h","0/"+jobDetail.getEveryValue());
				cronExpression = cronExpression.replaceAll("m","0");
			}
		}else{
			Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
			calendar.setTime(jobDetail.getServerTime());   
			cronExpression = cronExpression.replaceAll("m",String.valueOf(calendar.get(Calendar.MINUTE)));
			cronExpression = cronExpression.replaceAll("h",String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)));
		}
		if (jobDetail.getServiceFrequency().equalsIgnoreCase(FREQUENCY_MONTH))
			if (StringUtils.isNotEmpty(jobDetail.getDayOfMonth())){
				cronExpression = cronExpression.replaceAll("d",jobDetail.getDayOfMonth());
			}else cronExpression = cronExpression.replaceAll("d","1");
		else if (jobDetail.getServiceFrequency().equalsIgnoreCase(FREQUENCY_WEEK))
			cronExpression = cronExpression.replaceAll("d","?");
		else
			cronExpression = cronExpression.replaceAll("d","*");
		
		if (jobDetail.getServiceFrequency().equalsIgnoreCase(FREQUENCY_WEEK))
		{
			if (dayOfWeek.size()==7)
			{
				cronExpression = cronExpression.replaceAll("w","?");
			}else{
				String days = "";
				for (String day : dayOfWeek)
				{
					days+=day+",";
				}
				cronExpression = cronExpression.replaceAll("w", days.substring(0,days.length()-1));
			}
		}else{
			cronExpression = cronExpression.replaceAll("w","?");
		}
		
		jobDetail.setCronExpression(cronExpression);
	}
	
	public void beforeDataNodeSaveOrUpdate(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, DataNodeProcessStatus status, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		if (obj instanceof SchedulerJobDetail)
		{
			SchedulerJobDetail jobDetail = (SchedulerJobDetail) obj;
			
			if (!jobDetail.getServiceFrequency().startsWith("every"))
			{
				Boolean isValid = true;
				if (jobDetail.getTriggerTime() == null)
				{
					status.setFieldError(node, "triggerTime", "User Time need to be set");
					isValid = false;
				}
				if (jobDetail.getUserTimeZone() == null)
				{
					status.setFieldError(node, "userTimeZone", "GMT Offset need to be set");
					isValid = false;
				}
				if (!isValid)
				{
					status.setContinueProcess(false, true);
					return;
				}
				
			}
			
			if (jobDetail.getServiceFrequency().startsWith("every") && jobDetail.getEveryValue() == null)
			{
				
				status.setContinueProcess(false, true);
				status.setFieldError(node, "everyValue", "For Frequency that is set to Every, a value need to be set to it");
				return;
			}else{
				if (!jobDetail.getServiceFrequency().startsWith("every"))
				{
					Date gmtTime = jobDetail.getTriggerTime();
					int min =  (int) (jobDetail.getUserTimeZone()*60);
					
					gmtTime = DateUtils.addMinutes(gmtTime, -(min));
					jobDetail.setServerTime(gmtTime);
					jobDetail.setEveryValue(null);
				}else{
					jobDetail.setTriggerTime(null);
					jobDetail.setUserTimeZone(null);
					jobDetail.setServerTime(null);
					jobDetail.setDayOfMonth(null);
					jobDetail.setDayOfWeek(null);
				}
				if (jobDetail.getServiceFrequency().equalsIgnoreCase(FREQUENCY_WEEK))
				{
					List list = node.getMultiSelect().getValues().get("dayOfWeek");
					if (list == null || (list!=null && list.size()<1))
					{
						status.setContinueProcess(false, true);
						status.setFieldError(node, "dayOfWeek", "For Frequency that is set to Week, at least a day need to be selected");
						return;
					}
				}
				this.setCronExpression(jobDetail, node.getMultiSelect().getValues().get("dayOfWeek"));
			}
			
			if(StringUtils.isNotEmpty(jobDetail.getServiceKey()) && jobDetail.getServiceKey().equalsIgnoreCase("fileBridgeJob") && FileBridgeWebSocketClientPoint.getConfiguredInstance().getActiveWsConnection() != null)
				FileBridgeWebSocketClientPoint.getConfiguredInstance().getActiveWsConnection().disconnect();
		}
	}
	public void afterDataNodeDelete(CommandBean commandBean, CommandBeanTreeNode node, UserSession session, HttpServletRequest request, int operationPerformed) throws Exception{
		Object obj = node.getData();
		if (obj instanceof SchedulerJobDetail)
		{
			SchedulerJobDetail jobDetail = (SchedulerJobDetail) obj;
			SchedulerServiceType serviceType = (SchedulerServiceType) node.getParent().getData();
			ConfigurableScheduler.getConfiguredInstance().removeTrigger(serviceType, jobDetail);
		
		}
		
		if(FileBridgeWebSocketClientPoint.getConfiguredInstance().getActiveWsConnection() != null)
			FileBridgeWebSocketClientPoint.getConfiguredInstance().getActiveWsConnection().disconnect();
	}
	public void afterTemplateNodeCreated(CommandBean commandBean, TreeModelDataDefinitionMeta meta, CommandBeanTreeNode node, UserSelectionSnapshot userSelection, HttpServletRequest request) throws Exception {
		Object obj = node.getData();
		
		if (obj instanceof SchedulerServiceType)
		{
			setIsNBBDeployedModule(node);
		}
	}
	
	private void setIsNBBDeployedModule(CommandBeanTreeNode node) throws Exception
	{
		String nbbRigUid = ApplicationConfig.getConfiguredInstance().getNbbDeployedRig();
		if (StringUtils.isNotBlank(nbbRigUid)) {
			node.getDynaAttr().put("isNBBDeployed", "1");
		}
	}
}
