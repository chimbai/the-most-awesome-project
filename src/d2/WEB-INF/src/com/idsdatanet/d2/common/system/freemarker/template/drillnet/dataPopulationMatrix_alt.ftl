<!DOCTYPE HTML>
<html>
<body style="overflow-x: hidden; overflow-y: hidden;background-color:#ffffff;">
	<#assign v_resources_path = "${form.getVirtualResourcePath()}/htmllib"/>
	<#include "/core/defaultJavascriptAndStyleTemplate.ftl" />
	
	<#assign customCss = form.getCss()/>
	
	<#if customCss??>
		<#list customCss as css>
			<#if css.url??>
 				<link rel="stylesheet" type="text/css" href="${css.url}"/>
 			<#elseif css.content??>
 				<style>
 					${css.content}
 				</style>
 			</#if>
		</#list>
 	</#if>
	
	<#assign scripts = form.getScripts()/>
	
	<#if scripts??>
		<#list scripts as script>
			<#if script.url??>
 				<script type="text/javascript" src="${script.url}"></script>
 			<#elseif script.content??>
 				<script type="text/javascript">
 					${script.content}
 				</script>
 			</#if>
		</#list>
 	</#if>
<!--	<script type="text/javascript">
		$(document).ready(function(){
			var dpm = new window.D3.DataPopulationMatrix(document.getElementById('moduleContain'));
			dpm.initData(${form.getJSONData()});
		    dpm.initCurrentDailyUid(${form.getJSONDailyUid()});
			dpm.initDays(${form.getJSONDays()});
			dpm.render();
			window.D3.DPM = dpm;
		});
	</script>-->
<script>
	$(document).ready(function(){
		var dpm = new window.D3.DataPopulationMatrix(document.getElementById('moduleContain'), document.getElementById('sTable'), document.getElementById('cTable'));
		dpm.initData(${form.getJSONData()});
	    dpm.initCurrentDailyUid(${form.getJSONDailyUid()});
		dpm.initDays(${form.getJSONDays()});
		dpm.render();
		window.D3.DPM = dpm;
	});
    $(function() {
    	var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    	var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    	$('.relativeContainer').height(function () {
			return h - 50;
		});
    	$('.rightContainer').height(function() {
    		return h - 50;
    	});
    	$('.rightContainer').width(function() {
    		return w - 200;
    	});
        $.fn.cTable = function(o) {     
       		$('.SBWrapper').css({'height': $('.relativeContainer').height() - $('.topSBWrapper').height() - 70});
       		//padding-left and right 10px    
       		$('.SBWrapper').css({'width': $('.rightContainer').width() - 20});  
            $('.SBWrapper').scroll(function () {
                var rc = $(this).closest('.relativeContainer');
                var lfW = rc.find('.leftSBWrapper');
                var tpW = rc.find('.topSBWrapper');
                
                lfW.css('top', ($(this).scrollTop()*-1));
                tpW.css('left', ($(this).scrollLeft()*-1));        
            });
            
            $(window).resize(function () {
            	                                
				w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		    	h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
				$('.rightContainer').height(function() {
		    		return h - 50;
		    	});
		    	$('.rightContainer').width(function() {
		    		return w - 200;
		    	});
		    	$('.relativeContainer').height(function () {
					return h - 50;
				});
				$('.SBWrapper').css({'height': $('.relativeContainer').height() - $('.topSBWrapper').height() - 70});   
				$('.SBWrapper').css({'width': $('.rightContainer').width() - 20});
            });
        }
               
        $('#cTable').cTable({
            width: window.innerWidth,
            height: window.innerHeight,
            fCols: 1,
            fRows: 1
        });
    });
</script>
<table class="BaseNodesGroupContainerHeader" cellspacing="0" cellpadding="0">
<tbody>
<tr><td><div class="BaseNodesGroupContainerHeaderText">${form.info.getLabel("label.DPM.Title")}</div></td></tr>
</tbody>
</table>
<div id="headercontainer" align="center" style='background-color:#ffffff;font-size: 10pt;padding: 10px 0px 0px 0px;'>
	${form.info.getLabel("label.DPM.Info")}
</div>
<div class="cTContainer">
<div class="relativeContainer" style="width:100%;height:100%" >
<div class="fixedTB">
<table width="95%" cellspacing="1" cellpadding="0" border="1" align="center" style="width: 200px;">
<tbody>
<tr>
<td>${form.info.getLabel("label.DPM.Modules")}</td>
</tr>
<tr>
<td  class="qc">${form.info.getLabel("label.DPM.QcStatus")}</td>
</tr>
</tbody>
</table>
</div>
<div class="leftContainer" style="top: 61px; width: 200px;">
<div class="leftSBWrapper" style="top: 0px;">
<table id="moduleContain" width="90%" cellspacing="1" cellpadding="0" border="1" align="center" style="width: 200px;">

</table>
</div>
</div>
<div class="rightContainer" style="left: 200px; width: 94%; max-width: 95%;align:left;">
<div class="topSBWrapper" style="left: 0px;">
<table id="sTable" cellspacing="1" cellpadding="0" border="1" align="left" style="width:1300px;">
</table>
</div>
<div class="SBWrapper" >
<table id="cTable" cellspacing="1" cellpadding="0" border="1" align="left" style="width:1300px;">
</table>
</div>
</div>
</div>
</div>
	
</body>
</html> 
    
    
    