package com.idsdatanet.d2.common.util.field;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;

import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class GenericFieldHandler implements FieldHandlerInterface {

    @Override
    public Object translate(Object value, Object object, BaseCommandBean commandBean, ExcelMapping mapping, List<Object> objList, Workbook book, Cell cell) throws Exception {
        return object;
    }
}