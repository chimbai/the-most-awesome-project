<!DOCTYPE HTML>
<html>
<head>
</head>
<body>
	<#assign v_resources_path = "${contextPath}/vresources/jartimestamp/8888888888/htmllib"/>
<link rel="stylesheet" type="text/css" href="${v_resources_path}/css/d3-renderer.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/css/jquery.datepick.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/css/smoothness.datepick.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/css/tipsy.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/css/jquery-ui-1.8.21.custom.css"/>
    <meta charset="UTF-8">
    <title>ExtJsPage</title>
    <script type="text/javascript" src="${v_resources_path}/script/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="${v_resources_path}/script/jquery-ui-1.8.19.custom.min.js"></script>
	<script type="text/javascript" src="${v_resources_path}/script/jquery.datepick.pack.js"></script>
	<script type="text/javascript" src="${v_resources_path}/script/jquery.datepick-en-GB.js"></script>
	<script type="text/javascript" src="${v_resources_path}/script/jquery.tipsy.js"></script>	
    <script type="text/javascript" src="${contextPath}/script/generic/d3.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/Common.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/Collection.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/UIManager.js"></script>
    
    <script type="text/javascript" src="${contextPath}/script/generic/LookupItem.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/LookupReference.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/TreeNode.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/PopupManager.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/UIComponentManager.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/TreeNodeProxy.js"></script>
    <script type="text/javascript" src="${contextPath}/script/default/DefaultProxy.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/FieldValidator.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/FieldFactory.js"></script>

    <script type="text/javascript" src="${contextPath}/script/generic/Field.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/AbstractFieldComponent.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/ui/Popup.js"></script>
	<script type="text/javascript" src="${contextPath}/script/generic/ui/FieldComponent.js"></script>
	
	
	<script type="text/javascript" src="${contextPath}/script/generic/ui/RecordActions.js"></script>
	<script type="text/javascript" src="${contextPath}/script/generic/ui/RecordActionsHeader.js"></script>
	<script type="text/javascript" src="${contextPath}/script/generic/ui/RootButtons.js"></script>
	<script type="text/javascript" src="${contextPath}/script/generic/ui/BaseNodesGroupContainer.js"></script>
	
	
	<script type="text/javascript" src="${contextPath}/script/generic/ui/LayoutParserForHtmlTable.js"></script>
	<script type="text/javascript" src="${contextPath}/script/generic/ui/SimpleGrid.js"></script>    
	<script type="text/javascript" src="${contextPath}/script/generic/ui/DataGrid.js"></script>
	<script type="text/javascript" src="${contextPath}/script/generic/ui/NodeViewRepeater.js"></script> 
 	<script type="text/javascript" src="${contextPath}/script/generic/TreeNodeViewTemplate.js"></script>
 	<script type="text/javascript" src="${contextPath}/script/generic/TemplateModel.js"></script>
 	<script type="text/javascript" src="${contextPath}/script/generic/DataViewerEvent.js"></script>
 	<script type="text/javascript" src="${contextPath}/script/generic/DataViewer.js"></script>
 	
 	
	<script type="text/javascript">
		window.contextPath = "${contextPath}";
		var data = ${data};
		var template = ${screenTemplate};
    	D3.UIComponentManager.setUIPackage("D3.ui");
    	var viewer = new D3.DataViewer({});
    	viewer.init(template,data);
    </script>
   

</body>
</html>