<!DOCTYPE HTML>
<html>
<head>
	<#assign v_resources_path = "${form.getVirtualResourcePath(1)}/htmllib"/>
	<#assign v_resource_i18nlabel = "${form.getI18nLabelSystemResource()}"/>
	<#include "/core/defaultJavascriptAndStyleTemplate.ftl" />
	
	<#assign customCss = form.getCss()/>
	
	<#if customCss??>
		<#list customCss as css>
			<#if css.url??>
 				<link rel="stylesheet" type="text/css" href="${css.url}"/>
 			<#elseif css.content??>
 				<style>
 					${css.content}
 				</style>
 			</#if>
		</#list>
 	</#if>
	
	<#assign scripts = form.getScripts()/>
	
	<#if scripts??>
		<#list scripts as script>
			<#if script.url??>
 				<script type="text/javascript" src="${script.url}"></script>
 			<#elseif script.content??>
 				<script type="text/javascript">
 					${script.content}
 				</script>
 			</#if>
		</#list>
 	</#if>
	
	<style>
		.topContainer {
			background-color: #ECECEC;
		}
	</style>
	
	<script type="text/javascript">
		$(document).ready(function(){
			mylayout = new D2Layout(document.body, {
				north: {
					id: "myNodeViewer", 
					resizable: true, 
					size: "40%", 
					onResizeEnds: function(){myviewer.refreshLayout();}
				},
				center: {id: "graph"}
			});
			mylayout.refreshLayout();
			myviewer = new D3.NodesViewer(${form.getOutputAdaptorResult()}, document.getElementById("myNodeViewer"));
			document.getElementById("dvdGraph").src = "graph.html?type=dvdAnnotation&size=medium";
			$(window).resize(function(){
				mylayout.refreshLayout(); 
				myviewer.refreshLayout();
			});
		});
	</script>
</head>
<body>	
	<div id="myNodeViewer" class="nodeViewer" style="overflow:hidden">
	</div>
	<div id="graph" class="graph">
		<center><img width="640px" id="dvdGraph"/></center>
	</div>
</body>
</html> 