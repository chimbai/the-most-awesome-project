package com.idsdatanet.d2.common.util.node;

import java.util.List;
import java.util.Map;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.MarineSupport;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class OceanApexBcoMarineSupportNodeListener extends GenericNodeListener {
    
    @Override
    public Object beforeNodeCreate(Object object, BaseCommandBean commandBean, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        if (object != null && object instanceof MarineSupport) {
            MarineSupport w = (MarineSupport) object;
            w.setSupportDatumType("anchor");
        }
        return object;
    }
    
}