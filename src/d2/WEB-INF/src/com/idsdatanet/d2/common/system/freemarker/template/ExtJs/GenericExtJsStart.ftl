<!DOCTYPE HTML>
<html>
<head>
	<#assign v_resources_path = "${contextPath}/vresources/jartimestamp/8888888888/htmllib"/>

    <meta charset="UTF-8">
    <title>ExtJsPage</title>
    <script type="text/javascript" src="${v_resources_path}/script/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="${v_resources_path}/script/jquery-ui-1.8.19.custom.min.js"></script>
	<script type="text/javascript" src="${v_resources_path}/script/jquery.datepick.pack.js"></script>
	<script type="text/javascript" src="${v_resources_path}/script/jquery.datepick-en-GB.js"></script>
	<script type="text/javascript" src="${v_resources_path}/script/jquery.tipsy.js"></script>	
    <script type="text/javascript" src="${v_resources_path}/script/d3-all.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/TreeNode.js"></script>
	<script type="text/javascript" src="${contextPath}/script/generic/FieldValidator.js"></script>
	<script type="text/javascript" src="${contextPath}/script/generic/FieldComponent.js"></script>
	<script type="text/javascript" src="${contextPath}/script/generic/FieldFactory.js"></script>
	<script type="text/javascript" src="${contextPath}/script/generic/GenericField.js"></script>

	
    <link rel="stylesheet" href="resources/css/default/app.css">
    
    
    
    <!-- <x-compile> -->
    <!-- <x-bootstrap> -->
    <script src="ext/ext-dev.js"></script>

    <script type="text/javascript" src="${contextPath}/script/generic/TreeNodeProxy.js"></script>
    <script type="text/javascript" src="${contextPath}/script/generic/ExtJsFieldComponent.js"></script>
	<script type="text/javascript" src="${contextPath}/script/generic/ExtJsFieldFactory.js"></script>
	<script type="text/javascript" src="${contextPath}/script/extJS/ExtJS.js"></script>
	<script type="text/javascript" src="${contextPath}/script/extJS/Proxy.js"></script>
	
	
	<script type="text/javascript">
    	var proxy = new D3.ExtJS.Proxy();
    	proxy.init(${data});
    </script>
    <script src="bootstrap.js"></script>
    <!-- </x-bootstrap> -->
    <script src="app/app.js"></script>
    <!-- </x-compile> -->
    
    <script type="text/javascript">
                
        Ext.onReady(function(){
			//var a = ExtJsPage.view.Viewport;
            alert("Congratulations!  You have Ext configured correctly!");
			window.D3.test = JSON.stringify(window.proxy.getTreeNodeData('rec'));
			window.D3.a="111";
			
        }); //end onReady
        
        
    </script>
</head>
<body></body>
</html>