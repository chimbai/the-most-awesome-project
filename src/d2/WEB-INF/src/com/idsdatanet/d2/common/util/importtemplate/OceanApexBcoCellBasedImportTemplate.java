package com.idsdatanet.d2.common.util.importtemplate;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.common.util.ImportUtil;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class OceanApexBcoCellBasedImportTemplate extends ExcelImportTemplate {

    @Override
    public List<Object> parseTemplate(File excelFile, BaseCommandBean commandBean) throws Exception {
        List<Object> objectList = null;
        List<ExcelMapping> cellMapList = this.excelMappingList;
        InputStream inputStream = new FileInputStream(excelFile);
        this.book = WorkbookFactory.create(inputStream);
        this.evaluator = book.getCreationHelper().createFormulaEvaluator();

        try {
            Sheet sheet = book.getSheetAt(0);
            if (sheet != null) {
                objectList = new ArrayList<>();
                Object object = ImportUtil.getNewObject(this.className);
                if (this.listener != null) { // BeforeNodeCreate
                    object = this.listener.beforeNodeCreate(object, commandBean, this, objectList, null);
                }
                for (ExcelMapping cellMap : cellMapList) { // Populating Single Object
                    object = this.parseCell(object, sheet, this.className, cellMap, commandBean, objectList);
                }
                if (object != null && this.listener != null) { // AfterNodeCreate
                    object = this.listener.afterObjectCreate(object, commandBean, this, objectList, null);
                }
                if (object != null) {
                    objectList.add(object);
                } else {
                    // break; error msg? or maybe the error messages from the actual error is enough
                }
            }
            if (this.listener != null) {
                objectList = this.listener.afterAllObjectCreate(objectList, commandBean, this, null);
            }
        } finally {
            book.close();
            inputStream.close();
        }
        return objectList;
    }

    public Object parseCell(Object object, Sheet sheet, String className, ExcelMapping cellMapping, BaseCommandBean commandBean, List<Object> objectList) throws Exception {
        int rowIndex = cellMapping.getRowIndex();
        int columnIndex = cellMapping.getColumnIndex();

        if (rowIndex == ExcelMapping.UNDEFINED && columnIndex == ExcelMapping.UNDEFINED) {
            commandBean.getSystemMessage().addError("Import Template has not been setup properly for field: " + className + "." + cellMapping.getName());
            return null;
        } else {
            Row row = sheet.getRow(rowIndex);
            Cell c = row.getCell(columnIndex);
            String ids = cellMapping.getIds();
            String type = cellMapping.getType();
            return this.processCell(c, ids, type, object, cellMapping, commandBean, objectList);
        }
    }

}
