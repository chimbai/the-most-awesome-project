package com.idsdatanet.d2.common.scheduler.jobs;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.InetAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.SysLogSendToTown;
import com.idsdatanet.d2.core.stt.AbstractSTTEngine;
import com.idsdatanet.d2.core.stt.STTComConfig;
import com.idsdatanet.d2.core.stt.STTDataDirection;
import com.idsdatanet.d2.core.stt.STTEngine;
import com.idsdatanet.d2.core.stt.STTJobConfig;
import com.idsdatanet.d2.core.stt.STTStatus;
import com.idsdatanet.d2.core.stt.STTTransactionType;
import com.idsdatanet.d2.core.stt.jaxb.SttStatus;
import com.idsdatanet.d2.core.util.xml.JAXBContextManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.SystemExceptionHandler;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class STTJob {
	
	private Log log = LogFactory.getLog(this.getClass());
	
	private STTComConfig comConfig;
	private List<STTJobConfig> jobConfigs;
	
	public void setComConfig(STTComConfig comConfig) {
		this.comConfig = comConfig;
	}

	public void setJobConfigs(List<STTJobConfig> jobConfigs) {
		this.jobConfigs = jobConfigs;
	}

	public void doSendToTown() throws Exception {
		if (STTEngine.getConfiguredInstance().isEnabled()) {
			UserSelectionSnapshot userSelection = new UserSelectionSnapshot();
			try {
				userSelection.populdateDataAccordingToUserLogin(AbstractSTTEngine.SYSTEM_USER);
			} catch (Exception e) {
				log.error("Cannot find the user 'idsadmin' that is required for scheduled send to town job");
			}
			
			if (userSelection.getUserUid() != null) {
				List<Daily> days = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(new Date(), true, false);
				for (Daily day : days) {
					userSelection.populateDataAccordingToDailyUid(day.getDailyUid());
					
					STTStatus status = new STTStatus(); 
					SysLogSendToTown sysLog = new SysLogSendToTown();
					
					String stringResponseBody = null;
					
					try {
						final long start = System.currentTimeMillis();
						
						// load and marshal
						File f = STTEngine.getConfiguredInstance().marshal(jobConfigs, userSelection, status, STTEngine.MARSHALLING_MODE_STT, false);
						if (f == null) {
							throw new Exception("Failed to marshal data");
						}
						sysLog.setFilename(f.getName());
						
						// extra request parameters, mainly for logging at the other end
						Map<String, String> requestParameters = new HashMap<String, String>();
						requestParameters.put("groupUid", nullToEmptyString(userSelection.getGroupUid()));
						requestParameters.put("wellUid", nullToEmptyString(userSelection.getWellUid()));
						requestParameters.put("wellboreUid", nullToEmptyString(userSelection.getWellboreUid()));
						requestParameters.put("operationUid", nullToEmptyString(userSelection.getOperationUid()));
						requestParameters.put("rigInformationUid", nullToEmptyString(userSelection.getRigInformationUid()));
						requestParameters.put("dailyUid", nullToEmptyString(userSelection.getDailyUid()));
						for (STTJobConfig jobConfig : jobConfigs) {
							requestParameters.put("jobConfigBeanNames", nullToEmptyString(jobConfig.getBeanName()));
						}
						requestParameters.put("destinationUrl", nullToEmptyString(comConfig.getDestinationUrl()));
						requestParameters.put("scheduled", "true");
						requestParameters.put("userName", AbstractSTTEngine.SYSTEM_USER);
						requestParameters.put("hostName", InetAddress.getLocalHost().getHostName());

						// send data
						stringResponseBody = STTEngine.getConfiguredInstance().send(comConfig, "/webservice/sendtotownservice.html", f, requestParameters, status);
						
						// parse response
						JAXBContext jc = JAXBContextManager.getContext("com.idsdatanet.d2.core.stt.jaxb");
						Unmarshaller m = jc.createUnmarshaller();
						SttStatus sttStatus = (SttStatus) m.unmarshal(new ByteArrayInputStream(stringResponseBody.getBytes()));
						
						if (sttStatus.getErrors() != null && sttStatus.getErrors().getError().size() > 0) {
							sysLog.setStatus(STTStatus.FAIL);
						} else {
							sysLog.setStatus(STTStatus.OK);
						}
						
						status.log("Elapsed time: " + (System.currentTimeMillis() - start) + "ms");
						
					} catch (javax.xml.bind.UnmarshalException e) {
						status.log("Failed to parse response from town:\n" + stringResponseBody);
						sysLog.setStatus(STTStatus.FAIL);
					} catch (Exception e) {
						status.log("Caught exception:\n" + SystemExceptionHandler.printAsString(e));
						sysLog.setStatus(STTStatus.FAIL);
					} finally {
						// logging
						sysLog.setGroupUid(userSelection.getGroupUid());
						sysLog.setWellUid(userSelection.getWellUid());
						sysLog.setWellboreUid(userSelection.getWellboreUid());
						sysLog.setOperationUid(userSelection.getOperationUid());
						sysLog.setRigInformationUid(userSelection.getRigInformationUid());
						sysLog.setDailyUid(userSelection.getDailyUid());
						sysLog.setJobconfigs(STTJobConfig.jobConfigsToString(jobConfigs));
						sysLog.setTransactionDatetime(new Date());
						sysLog.setTransactionType(STTTransactionType.SEND_TO_TOWN);
						sysLog.setDataDirection(STTDataDirection.OUTGOING);
						sysLog.setIsScheduled(true);
						sysLog.setRemoteAddr(comConfig.getDestinationUrl());
						sysLog.setUserName(AbstractSTTEngine.SYSTEM_USER);
						sysLog.setTransactionLog(status.getTransactionLog().toString());
						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(sysLog);
						
						status.dispose();
					}
				}
			}
		} // if (this.enabled)
	}
	
	private String nullToEmptyString(String str) {
		if (str == null) {
			return "";
		}
		return str;
	}
	
}
