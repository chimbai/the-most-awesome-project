package com.idsdatanet.d2.common.util;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupCache;
import com.idsdatanet.d2.core.lookup.LookupHandler;
import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class ExcelImportTemplateLookupHandler implements LookupHandler {

    private String className = null;
    private ExcelImportTemplateManager excelImportTemplateManager = null;

    @Override
    public Map<String, LookupItem> getLookup(CommandBean paramCommandBean, CommandBeanTreeNode paramCommandBeanTreeNode, UserSelectionSnapshot paramUserSelectionSnapshot, HttpServletRequest paramHttpServletRequest, LookupCache paramLookupCache) throws Exception {
        return this.excelImportTemplateManager.getTemplateLookupList(className);
    }
    
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public ExcelImportTemplateManager getExcelImportTemplateManager() {
        return excelImportTemplateManager;
    }

    public void setExcelImportTemplateManager(ExcelImportTemplateManager excelImportTemplateManager) {
        this.excelImportTemplateManager = excelImportTemplateManager;
    }

}