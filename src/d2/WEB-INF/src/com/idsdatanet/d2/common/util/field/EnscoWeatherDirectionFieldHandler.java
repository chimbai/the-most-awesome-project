package com.idsdatanet.d2.common.util.field;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;

import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class EnscoWeatherDirectionFieldHandler extends GenericFieldHandler {
    
    @Override
    public Object translate(Object value, Object object, BaseCommandBean commandBean, ExcelMapping mapping, List<Object> objList, Workbook book, Cell cell) throws Exception {
        if (value != null && value instanceof String) {
            // Check whether numeric value or not
            if (NumberUtils.isNumber((String) value)) {
                return Double.parseDouble(value.toString());
            } else {
                // Tentative Logic between letter inputs. Atm just set to 0
                return 0.0;
            }
        }
        return value;
    }
}