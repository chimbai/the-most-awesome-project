<html>
<body>
Dear Sir/Madam, 

<p><i>**This is an automated message. Please do not reply to the address above.**</i></p>

<p>A new Lesson Learned matching your registered interest has been posted.</p>
<#if lessonLearned?exists>
	<p>
		<table>
			<#list lessonLearned?keys as lessonLearnedUid>
				<tr>
					<td>
						Please click on the link for further details on the new Lesson Learned ticket: <a href="${lessonLearned[lessonLearnedUid]["hyperLink"]}">&lt;${lessonLearned[lessonLearnedUid]["lessonTicketNumber"]}&gt;</a>
					</td>
				</tr>
			</#list>
		</table>
	</p>
</#if>

<p>Thank you.</p>
<p><b>IDS Datanet Service</b></p>
</body>
</html>