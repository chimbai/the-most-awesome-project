package com.idsdatanet.d2.common.util.dynamicAttribute;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.CommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;

public class DynamicAttributeProcessor {
	
	public static void saveDynamicAttributes(CommandBean commandBean, CommandBeanTreeNode node, Object object, Map<String, String> dynamicAttributeMap) throws Exception {
		if (object == null || dynamicAttributeMap == null || dynamicAttributeMap.isEmpty()) { return; }
		
		Map<String, Object> loadedObjects = new LinkedHashMap<>();
		Map<String, Object> modifiedObjects = new LinkedHashMap<>();
		
		for (Map.Entry<String, String> entry : dynamicAttributeMap.entrySet()) {
			String dynamicAttribute = entry.getKey();
			String foreignKey = entry.getValue();
			Object value = DynamicAttributeProcessor.getDynamicAttributeNoBlankString(node, dynamicAttribute);
			DynamicAttributeProcessor.saveDynamicAttribute(commandBean, dynamicAttribute, value, object, foreignKey, loadedObjects, modifiedObjects);
		}
		for (Map.Entry<String, Object> entry : modifiedObjects.entrySet()) {
		    ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(entry.getValue());
		}
	}

	private static void saveDynamicAttribute(CommandBean commandBean, String dynaAttr, Object value, Object obj, String foreignKey, Map<String, Object> loadedObjects, Map<String, Object> modifiedObjects) throws Exception {
		String[] components = dynaAttr.split("[.]");
		if (components.length != 2) { return; }
		String tableName = components[0];
		String fieldName = components[1];
		Object currentObject = null;
		Object foreignKeyValue = PropertyUtils.getProperty(obj, foreignKey);
		
		if (loadedObjects.containsKey(tableName)) { // Check Map for already processed objects
			currentObject = loadedObjects.get(tableName);
		} else { // Check for Existing Record
			String sql = "FROM " + tableName + " WHERE " + foreignKey + "=:" + foreignKey + " AND (isDeleted IS NULL OR isDeleted = false)";
			List<Object> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { foreignKey }, new Object[] { foreignKeyValue });
			if (result.size() > 0) {
				currentObject = result.get(0);
			} else { // New Object
				Object newObject = DynamicAttributeUtils.getNewObject(tableName);
				DynamicAttributeUtils.populateContextUids(obj, newObject);
				DynamicAttributeUtils.copyField(obj, newObject, foreignKey);
				currentObject = newObject;
			}
			if (!loadedObjects.containsKey(tableName)) { // So will not do additional queries for other fields of the same table
				loadedObjects.put(tableName, currentObject);
			}
		}
		
		Object currentValue = PropertyUtils.getProperty(currentObject, fieldName); // Populate Field From Screen
		String type = PropertyUtils.getPropertyType(currentObject, fieldName).getSimpleName();
		Object convertedValue = DynamicAttributeUtils.convertObject(commandBean, value, type); // Convert Type
		
		if (convertedValue == null && currentValue == null) {
			return;
		} else if (currentValue != null && currentValue.equals(convertedValue)) {
			return;
		}
		
		DynamicAttributeUtils.populateField(currentObject, fieldName, convertedValue);
		if (!modifiedObjects.containsKey(currentObject)) {
			modifiedObjects.put(tableName, currentObject);
		}
	}
	
	private static Object getDynamicAttributeNoBlankString(CommandBeanTreeNode node, String string) throws Exception {
		Object value = node.getDynaAttr().get(string);
		if (value != null && StringUtils.equals(value.toString(), "")) {
			value = null;
		}
		return value;
	}

	public static void loadDynamicAttributes(CommandBean commandBean, CommandBeanTreeNode node, Object object, Map<String, String> dynamicAttributeMap) throws Exception {
		if (object == null || dynamicAttributeMap == null || dynamicAttributeMap.isEmpty()) { return; }
		
		Map<String, Object> loadedObjects = new LinkedHashMap<>();
		
		for (Map.Entry<String, String> entry : dynamicAttributeMap.entrySet()) {
			String dynamicAttribute = entry.getKey();
			String foreignKey = entry.getValue();
			DynamicAttributeProcessor.loadDynamicAttribute(commandBean, dynamicAttribute, node, object, foreignKey, loadedObjects);
		}
	}

	private static void loadDynamicAttribute(CommandBean commandBean, String string, CommandBeanTreeNode node, Object object, String foreignKey, Map<String, Object> loadedObjects) throws Exception {
		String[] components = string.split("[.]");
		if (components.length != 2) { return; }
		String tableName = components[0];
		String fieldName = components[1];
		Object currentObject = null;
		Object foreignKeyValue = PropertyUtils.getProperty(object, foreignKey);
		
		// Set UOM Symbol (need to test with dynamic string field to see if need to add condition)
		CustomFieldUom thisConverter = new CustomFieldUom(commandBean);
		thisConverter.setReferenceMappingField(DynamicAttributeUtils.getClass(tableName), DynamicAttributeUtils.convertCamelCaseToUnderscore(fieldName));
		if (thisConverter.isUOMMappingAvailable()) {
			node.setCustomUOM("@" + string, thisConverter.getUOMMapping());
		}
		
		// Load existing data
		if (loadedObjects.containsKey(tableName)) {
			currentObject = loadedObjects.get(tableName);
		} else {
			String sql = "FROM " + tableName + " WHERE " + foreignKey + "=:" + foreignKey + " AND (isDeleted IS NULL OR isDeleted = false)";
			List<Object> result = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { foreignKey }, new Object[] { foreignKeyValue });
			if (result.size() > 0) {
				currentObject = result.get(0);
			}
		}
		if (!loadedObjects.containsKey(tableName)) {
			loadedObjects.put(tableName, currentObject);
		}

		if (currentObject != null) {
			Object rawValue = PropertyUtils.getProperty(currentObject, fieldName);
			if (rawValue == null) { return; }
			node.getDynaAttr().put(string, rawValue);
		}
	}
}
