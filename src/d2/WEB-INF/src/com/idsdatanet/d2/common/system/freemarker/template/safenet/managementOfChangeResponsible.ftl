<html>
<body>
<p>Dear Sir/Madam,</p>

<p>This message was sent to notify that you are responsible for a Management of Change or there are changes to the Management of Change under your responsibility:</p>
			
<p>MOC #: MOC-${mocDisp.mocDispNumber}</p>

<p>Click <a href="${mocDisp.hyperLink}">here</a> for further action.</p>

<p>Thanks,</p>
<p><b>IDS Support Group</b></p>
</body>
</html>