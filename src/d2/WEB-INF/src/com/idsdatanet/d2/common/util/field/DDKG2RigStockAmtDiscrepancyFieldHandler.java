package com.idsdatanet.d2.common.util.field;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.RigStock;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.rigstock.RigStockUtil;

public class DDKG2RigStockAmtDiscrepancyFieldHandler extends GenericFieldHandler{
    @Override
    public Object translate(Object value, Object object, BaseCommandBean commandBean, ExcelMapping mapping, List<Object> objList, Workbook book, Cell cell) throws Exception {
    	if(object instanceof RigStock) {
    		UserSession userSession = commandBean.getCurrentUserSession();
    		RigStock rigStock = (RigStock) object;
    		double previousBalance = this.getPreviousBalance(rigStock, userSession, commandBean);
    		Double rob = (Double) value;
    		return rob + rigStock.getAmtUsed() - rigStock.getAmtStart() - previousBalance;
    	}else {
    		return value;
    	}
    }
    
    private double getPreviousBalance(RigStock rigStock, UserSession userSession, BaseCommandBean commandBean) throws Exception {
		Daily daily = ApplicationUtils.getConfiguredInstance().getCachedDaily(userSession.getCurrentDailyUid());
		Double previousBalance = CommonUtil.getConfiguredInstance().getOperationStockcodePreviousBalance(userSession.getCurrentOperationUid(), userSession.getCurrentRigInformationUid(), rigStock.getStockCode(), daily, RigStockUtil.getRigStockType(RigStock.class));
		CustomFieldUom uomConverter = new CustomFieldUom(commandBean, RigStock.class, "amt_start");
		uomConverter.setBaseValue(previousBalance);
		return uomConverter.getConvertedValue();
    }
}
