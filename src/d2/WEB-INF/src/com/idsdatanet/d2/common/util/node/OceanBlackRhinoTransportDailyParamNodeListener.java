package com.idsdatanet.d2.common.util.node;

import java.util.List;

import java.util.Map;


import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.TransportDailyParam;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class OceanBlackRhinoTransportDailyParamNodeListener extends GenericNodeListener {
    
    @Override
    public Object afterObjectCreate(Object object, BaseCommandBean commandBean, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        if (object != null && object instanceof TransportDailyParam) {
        	TransportDailyParam td = (TransportDailyParam) object;
        	td.setTransportType("helicopter"); // Default value as specified in specification
            return td;
        }
        return object;
    }
}