<html>
    <head>
    	<#assign v_resources_path = "${contextPath}/vresources/jartimestamp/${systemStartupTime}/htmllib/jasper"/>
    	<style>
      		body {
        		margin: 0px;
        		padding: 0px;
      		}
    	</style>
    	 
		 <script type='text/javascript' src="${visualizejsUtils.getJasperServerBaseUrl()}/client/visualize.js?baseUrl=${visualizejsUtils.getJasperServerBaseUrl()?url('utf-8')}"></script>
		 <script type="text/javascript" src="${v_resources_path}/js/libs/jquery-1.11.2.js"></script>
		 <script type="text/javascript" src="${v_resources_path}/js/libs/jquery-ui.js"></script>
		 <script type="text/javascript" src="${v_resources_path}/js/libs/jquery.layout-latest.js"></script>
		 <script type="text/javascript" src="${v_resources_path}/js/libs/d3.js"></script>
		 <script type="text/javascript" src="${v_resources_path}/js/libs/accordion.js"></script>
	
		<link rel="stylesheet" href="${v_resources_path}/js/libs/dist/themes/default/style.min.css" />
		<link rel="stylesheet" href="${v_resources_path}/resources/jquery-ui.min.css" />
		<link rel="stylesheet" href="${v_resources_path}/resources/d3-renderer.css" />
		<link rel="stylesheet" href="${v_resources_path}/resources/dashboard-viewer.css" />
		<link rel="stylesheet" href="${v_resources_path}/resources/pre-filter.css" />
		<link rel="stylesheet" href="${v_resources_path}/resources/wwo-filter.css" />
		<link rel="stylesheet" href="${v_resources_path}/resources/report-toolbar.css" />
		<link rel="stylesheet" href="${v_resources_path}/resources/throbber.css" />
		<link rel="stylesheet" href="${v_resources_path}/resources/customMessageDialog.css" />
		
		<script type="text/javascript" src="${v_resources_path}/js/libs/dist/jstree.min.js"></script>
		<script type="text/javascript" src="${v_resources_path}/js/libs/underscore-min.js"></script>
		
    </head>
    <body>
    	<script >
    		authenticate = ${visualizejsUtils.getAuthentication()};
    		
    		$(document).ready(function () {
				var layoutConfig ={
		            applyDefaultStyles: true,
		            fxSpeed: "slow",
	
		            west__resizable: true,
		            west__minSize: 320,
		            west__closable: true,
		        
		            south__closable: false,
		            south__size: 20,
		            south__resizable: false,
		            south__spacing_open: 0,
		            south__spacing_closed: 0
		        };
		        
		        if (window.D3.filter_onResize){
		        	layoutConfig.west__onresize_end = window.D3.filter_onResize;
		        }
		        
		        if (window.D3.ReportViewer_onResize){
		        	layoutConfig.west__onresize_end = window.D3.ReportViewer_onResize;
		        	layoutConfig.west__onclose_end = window.D3.ReportViewer_onResize;
		        } 
		        
		       window.D3.layout = $('body').layout(layoutConfig);	
		    });
    	</script>
    	<div id="mainContainer" class="ui-layout-center">
    	</div>
		<div id="filterContainer" class="ui-layout-west">
		</div>
		<div id="idsFooterContainer" class="ui-layout-south">
		</div>
		
		<script type="text/javascript" src="${v_resources_path}/js/CustomMessageDialog.js"></script>
		<script type="text/javascript" src="${v_resources_path}/js/JrsReportController.js"></script>
		<script type="text/javascript" src="${v_resources_path}/js/JasperViewerEvent.js"></script>
		<script type="text/javascript" src="${v_resources_path}/js/ReportViewer.js"></script>
		<script type="text/javascript" src="${v_resources_path}/js/DashboardViewer.js"></script>
    	<script type="text/javascript" src="${v_resources_path}/js/PreFilter.js"></script>
    	<script type="text/javascript" src="${v_resources_path}/js/WwoFilter.js"></script>
		<script type="text/javascript" src="${v_resources_path}/js/ReportToolbar.js"></script>
    	<script src="${v_resources_path}/js/report_11.js"></script>
    </body>
</html> 