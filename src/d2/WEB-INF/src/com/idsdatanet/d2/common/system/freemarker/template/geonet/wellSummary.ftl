<#assign imgpath = "images/formation" />
<#assign imageHeight = getDynamicAttribute(form.root, "imageHeight") /> 
<#assign casingCount = getDynamicAttribute(form.root, "casingCount") />
<#assign wellDepth = getDynamicAttribute(form.root, "wellDepth") />
<#assign formationFirstDepth = getDynamicAttribute(form.root, "formationFirstDepth") />
<#assign formationDepthUOM = getFieldUomSymbol("Formation", "sampleTopMdMsl") />
<#assign casingCount = casingCount?number - 1 />
<#assign datumReferencePoint = getDynamicAttribute(form.root, "datumReferencePoint") />
<#assign datumUOM = getFieldUomSymbol("OpsDatum", "reportingDatumOffset") />
<#assign datumLabel = getDynamicAttribute(form.root, "datumLabel") />

<script language="javascript">
<!-- 
	var imgpath = "images/formation";
	function showhide(typename)
	{
		for (i=1;i<50;i++)
		{
			var x = document.getElementById(typename + "text_" + i);
			if (x)
			{
				x.style.display = (x.style.display =="none"?"":"none");
			}
			else
			{
				break;
			}
		}
	}
	
	function change_formation_image(control, classname) {
 		$("." + classname).each(function() {
  			//$(this).attr("style", "url(" + imgpath + "/" + control.val() + ")");
  			 $(this).css({background: "url(" + imgpath + "/" + control.val() + ")"});
  		});
		d2ScreenManager.toggleEditModeBlockUI("edit");
	}

-->
</script>

<#if form.root.list.Operation?exists>
	<#list form.root.list.Operation?values as item>
		<#assign operationUid = getFieldValue(item, "operationUid") />
		<#assign zRtwellhead = getFieldValue(item, "zRtwellhead") />
		<#if operationUid!="">
		
			<div align="center">
				<table cellspacing="0" cellpadding="0">
					<tr valign="top">
						<td style="border:none;background-color:#ffffff; padding:5px;">
							<table style="border:none;background-color:#ffffff;" cellspacing="0" cellpadding="0">
								<tr style="text-align:center" valign="top">
									<td><b>Formation</b></td>
									<td><b>Casing</b></td>
									<td><b>&nbsp;</b></td>
									<#if item.list.Bharun?exists && item.list.Bharun?size &gt; 0><td style="padding-left:10px;padding-right:10px;"><a href="javascript:showhide('bha');" title="Click to show/hide BHA Run#"><b>BHA Run</b></a></td></#if>
									<#if item.list.CementJob?exists && item.list.CementJob?size &gt; 0><td style="padding-left:10px;padding-right:10px;"><a href="javascript:showhide('cement');" title="Click to show/hide Cement"><b>Cement Top</b></a></td></#if>
									<#if item.list.WirelineRun?exists && item.list.WirelineRun?size &gt; 0><td style="padding-left:10px;padding-right:10px;"><a href="javascript:showhide('wireline');" title="Click to show/hide Logging Run#"><b>Wireline Logging<br/>(Suite/Run)</b></a></td></#if>
									<#if item.list.Core?exists && item.list.Core?size &gt; 0><td style="padding-left:10px;padding-right:10px;"><a href="javascript:showhide('corerun');" title="Click to show/hide Core No.#"><b>Coring</b></a></td></#if>
									<#if item.list.DrillStemTest?exists && item.list.DrillStemTest?size &gt; 0><td style="padding-left:10px;padding-right:10px;"><a href="javascript:showhide('dst');" title="Click to show/hide DST No."><b>DST</b></a></td></#if>
									<#if item.list.GasReadings?exists && item.list.GasReadings?size &gt; 0><td style="padding-left:10px;padding-right:10px;"><b>Gas</b></td></#if>
									<td>&nbsp;</td>
								</tr>
								
								<tr style="text-align:center" valign="bottom">
									<td style="padding:0px;" colspan="3">
										<table width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="padding:0px;" align="right">
													<#assign width = (casingCount?number * 20) + 70 />
													<div style="border: 2px solid rgb(0, 0, 0); margin: 0pt 40px 0pt 0px; padding: 0pt; overflow: hidden; height: 5px; width: ${width}px;">&nbsp;</div>
												</td>
											</tr>
										</table>
									</td>
									<td>&nbsp;</td>
									<#if item.list.Bharun?exists && item.list.Bharun?size &gt; 0><td>&nbsp;</td></#if>
									<#if item.list.CementJob?exists && item.list.CementJob?size &gt; 0><td>&nbsp;</td></#if>
									<#if item.list.WirelineRun?exists && item.list.WirelineRun?size &gt; 0><td>&nbsp;</td></#if>
									<#if item.list.Core?exists && item.list.Core?size &gt; 0><td>&nbsp;</td></#if>
									<#if item.list.DrillStemTest?exists && item.list.DrillStemTest?size &gt; 0><td>&nbsp;</td></#if>
									<#if item.list.GasReadings?exists && item.list.GasReadings?size &gt; 0><td>&nbsp;</td></#if>
								</tr>
								<tr valign="top">
									<td align="center" style="border:none;padding:0px;" onclick="self.location.href='formation.html'">
										<table style="padding:0px;" cellspacing="0" cellpadding="0">
											<#if item.list.Formation?exists>
												<#if formationFirstDepth!="">
													<tr valign="top">
														<#if datumReferencePoint=="WH">
															<#if datumLabel!=""> 
																<td style="cursor:pointer; padding:0px; padding-top:1px; height:${datumLabel}px; text-align:right;" nowrap> 
																	 ${datumLabel}
																</td> 
															<#else> 
																<td style="cursor:pointer; padding:0px; padding-top:1px; height:${zRtwellhead}px; text-align:right;" nowrap> 
																	<span style="overflow: hidden; scrolls: no; padding:0px;padding-top:0px; height:${zRtwellhead}px;text-align:right; border-top:solid 1px #000000;"> 
																	0.0 ${datumUOM} 
																	</span> 
																</td> 
															</#if>
														<#else>
															<#if zRtwellhead!=""> 
																<td style="cursor:pointer; padding:0px; padding-top:1px; height:${zRtwellhead}px; text-align:right;" nowrap> 
																	<@showField node=item field="zRtwellhead" editable=false /> 
																</td> 
															<#else> 
																<td style="cursor:pointer; padding:0px; padding-top:1px; height:${zRtwellhead}px; text-align:right;" nowrap> 
																	<span style="overflow: hidden; scrolls: no; padding:0px;padding-top:0px; height:${zRtwellhead}px;text-align:right; border-top:solid 1px #000000;"> 
																	0.0 ${getFieldUomSymbol("Operation", "zRtwellhead")} 
																	</span> 
																</td> 
															</#if>
														</#if>
														<td style="cursor:pointer; padding:0px;height:${formationFirstDepth}px;text-align:center;border-top:solid 1px #000000;border-right:solid 1px #000000;" nowrap>
															<div style="overflow: hidden; padding:0px;padding-top:0px; height:${formationFirstDepth}px;text-align:center;">&nbsp;</div>
														</td>
														<td style="cursor:pointer; padding:0px;height:${formationFirstDepth}px;text-align:center;border-top:dotted 1px #000000;width:50px;" nowrap>
															<div style="overflow: hidden; padding:0px;padding-top:0px; height:${formationFirstDepth}px;text-align:center;">&nbsp;</div>
														</td>
													</tr>
												</#if>
												
												<#assign row = 0 />
												<#list item.list.Formation?values as detail_item>
													<#assign formationUid = getFieldValue(detail_item, "formationUid")?replace(".", "") />
													<#assign height = getDynamicAttribute(detail_item, "height") />
													<#assign sampleTopMdMsl = getFieldValue(detail_item, "sampleTopMdMsl") />
													<#assign formationName = getFieldValue(detail_item, "formationName") />
													<#assign formationImage = getFieldValue(detail_item, "formationImage") />
													<#if height!="">
														<tr valign="top">
															<td style="cursor:pointer; padding:0px;height:${height}px;text-align:right;" nowrap title="${formationName} @ ${sampleTopMdMsl} ${formationDepthUOM}">
																<#if detail_item_has_next>
																	<#if height?number &gt;= 10>
																		<#if (row % 2) == 0>
																			<div style="overflow: visible; scrolls: no; padding:0px;padding-top:0px; height:${height}px;text-align:left; border-top:solid 1px #000000;">
																				<@showField node=detail_item field="sampleTopMdMsl" editable=false />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																			</div>
																		<#else>
																			<div style="overflow: visible; scrolls: no; width:50%; margin-left:50%; padding:0px;padding-top:0px; height:${height}px;text-align:left; border-top:solid 1px #000000;">
																				<@showField node=detail_item field="sampleTopMdMsl" editable=false />
																			</div>
																		</#if>
																		<#assign row = row + 1>
																	<#else>
																		<div style="overflow: visible; scrolls: no; width:50%; margin-left:50%; padding:0px;padding-top:0px; height:${height}px;text-align:right; ">
																			&nbsp;
																		</div>
																	</#if>
																<#else>
																	<#if (row % 2) == 0>
																		<div style="overflow: visible; scrolls: no; padding:0px;padding-top:0px; height:${height}px;text-align:left; border-top:solid 1px #000000;">
																			<@showField node=detail_item field="sampleTopMdMsl" editable=false />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		</div>
																	<#else>
																		<div style="overflow: visible; scrolls: no; width:50%; margin-left:50%; padding:0px;padding-top:0px; height:${height}px;text-align:left; border-top:solid 1px #000000;">
																			<@showField node=detail_item field="sampleTopMdMsl" editable=false />
																		</div>
																	</#if>
																</#if>
															</td>
															<td style="cursor:pointer; padding:0px;height:${height}px;text-align:center;border-top:solid 1px #000000;border-right:solid 1px #000000;" nowrap title="${formationName} @ ${sampleTopMdMsl} ${formationDepthUOM}">
																<div style="overflow: hidden; padding:0px;padding-top:0px; height:${height}px;text-align:center;">&nbsp;</div>
															</td>
															<td style="cursor:pointer; padding:0px;height:${height}px;text-align:center;background:url(${imgpath}/${formationImage});border-top:dotted 1px #000000;width:50px;" nowrap id="${formationUid}" class="${formationUid}">
																<div style="overflow: hidden; padding:0px;padding-top:0px; height:${height}px;text-align:center;" title="${formationName} @ ${sampleTopMdMsl} ${formationDepthUOM}">&nbsp;</div>
															</td>
														</tr>
													</#if>
												</#list>
											</#if>
										</table>
									</td>
									<#assign hasPSData = getDynamicAttribute(item, "@hasPs") />
									<td align="center" style="background-color:#aaaaaa;padding:0px;">
										<table style="padding:0px;" cellspacing="0" cellpadding="0">
											<tr valign="top">
												<#if item.list.CasingSection?exists>
													<#list item.list.CasingSection?values as detail_item>
														<#assign height = getDynamicAttribute(detail_item, "height") />
														<#assign height_before = getDynamicAttribute(detail_item, "height_before") />
														<#assign casingOd = getLookupLabel(detail_item, "casingOd") />
														<#assign casingSummary = getFieldValue(detail_item, "casingSummary") />
														<#assign bgColor = getDynamicAttribute(detail_item, "bgColor") />
														<#assign shoeTopMdMsl = getFieldValue(detail_item, "shoeTopMdMsl") />
														<#assign shoeTopMdMslUom = getFieldUomSymbol(detail_item, "shoeTopMdMsl") />
														<#if height!="">
															<td style="cursor:pointer; padding:0px;text-align:center;" onclick="self.location.href='casingsection.html'">
																<table style="padding:0px;" cellspacing="0" cellpadding="0">
																	<#if height_before?number &gt; 0>
																	<tr>
																		<td style="font-size:1pt; padding:0px;height:${height_before}px;width:7px;">&nbsp;</td>
																	</tr>
																	</#if>
																	<tr valign="bottom">
																		<td style="font-size:1pt; padding:0px;height:${height}px;border-right:solid 1px #000000; border-top:solid 1px #000000;width:7px; background-color:${bgColor}; text-align:right;"
																			title="${casingOd?replace("\"","&quot;")} @ <#if shoeTopMdMsl!="">shoe depth ${shoeTopMdMsl} ${shoeTopMdMslUom}</#if>
																			Casing - ${casingSummary?replace("\"","&quot;")}"><img src="images/hangleft.gif"></td>
																	</tr>
																</table>
															</td>
														</#if>
													</#list>
													
													<#if hasPSData!="0">
														<#assign psTallyHeight = getDynamicAttribute(item, "@ps_tallyHeight") />
														<#assign psTallyLenght = getDynamicAttribute(item, "@ps_tallyLenght") />
														<#assign tallyLenghtUOM = getFieldUomSymbol("ProductionStringDetail", "tallyLength") />
														<#assign psDesc = getDynamicAttribute(item, "@ps_desc") />
														<#assign psTallyLenght = getDynamicAttribute(item, "@ps_tallyLenght") />
														<#assign imageHeightx = imageHeight?number - psTallyHeight?number />
													</#if>
													<td style="padding:0px;text-align:center;">
														<table style="padding:0px;" cellspacing="0" cellpadding="0">
															<#if hasPSData!="0">
																<tr  valign="bottom">
																	<td style="cursor:pointer;font-size:1pt; padding:0px;height:${psTallyHeight}px;width:7px;background-color:#00AE23" nowrap  
																		onclick="self.location.href='productionstring.html'"
																		title="Production String @ ${psTallyLenght} ${tallyLenghtUOM} (${psDesc?replace("\"","&quot;")})"
																		>&nbsp;</td>
																	<td style="height:${psTallyHeight}px;background-color:#ffffff;width:16px;">&nbsp;</td>
																	<td style="cursor:pointer;font-size:1pt; padding:0px;height:${psTallyHeight}px;width:7px;background-color:#00AE23" nowrap
																		onclick="self.location.href='productionstring.html'"
																		title="Production String @ ${psTallyLenght} ${tallyLenghtUOM} (${psDesc?replace("\"","&quot;")})"
																		>&nbsp;</td>
																</tr>
																<tr>
																	<td style="font-size:1pt; padding:0px;height:${imageHeightx}px;width:7px;background-color:#ffffff">&nbsp;</td>
																	<td style="height:${imageHeightx}px;background-color:#ffffff;width:16px;">&nbsp;</td>
																	<td style="font-size:1pt; padding:0px;height:${imageHeightx}px;width:7px;background-color:#ffffff" >&nbsp;</td>
																</tr>
															<#else>
																<tr>
																	<td style="height:${imageHeight}px;background-color:#ffffff;width:30px;">&nbsp;</td>
																</tr>
															
															</#if>
														</table>
													</td>
													
													<#assign actualKeys = [] />
													<#list item.list.CasingSection?keys as key>
														<#assign actualKeys = actualKeys + [key] />
													</#list>
													<#assign reversedKeys = actualKeys?reverse />
													
													<#list reversedKeys as reversedKey>
														<#assign detail_item = item.list.CasingSection[reversedKey] />
														<#assign height = getDynamicAttribute(detail_item, "height") />
														<#assign height_before = getDynamicAttribute(detail_item, "height_before") />
														<#assign casingOd = getLookupLabel(detail_item, "casingOd") />
														<#assign casingSummary = getFieldValue(detail_item, "casingSummary") />
														<#assign bgColor = getDynamicAttribute(detail_item, "bgColor") />
														<#assign shoeTopMdMsl = getFieldValue(detail_item, "shoeTopMdMsl") />
														<#assign shoeTopMdMslUom = getFieldUomSymbol(detail_item, "shoeTopMdMsl") />
														<#if height!="">
															<td style="cursor:pointer; padding:0px;text-align:center;" onclick="self.location.href='casingsection.html'">
																<table style="padding:0px;" cellspacing="0" cellpadding="0">
																	<#if height_before?number &gt; 0>
																	<tr>
																		<td style="font-size:1pt; padding:0px;height:${height_before}px;width:7px;">&nbsp;</td>
																	</tr>
																	</#if>
																	<tr valign="bottom">
																		<td style="font-size:1pt; padding:0px;height:${height}px;border-left:solid 1px #000000; border-top:solid 1px #000000;width:7px; background-color:${bgColor};"
																			title="${casingOd?replace("\"","&quot;")} @ <#if shoeTopMdMsl!="">shoe depth ${shoeTopMdMsl} ${shoeTopMdMslUom}</#if>
																			Casing - ${casingSummary?replace("\"","&quot;")}"><img src="images/hangright.gif"></td>
																	</tr>
																</table>
															</td>
														</#if>
													</#list>
												</#if>	
											</tr>
										</table>
									</td>
									
									<td align="center" style="border:none;padding:0px;" onclick="self.location.href='formation.html'">
										<table style="padding:0px;" cellspacing="0" cellpadding="0">
										<#if item.list.Formation?exists>
										    <#if formationFirstDepth!="">
										        <tr valign="top">
										            <#if datumReferencePoint=="WH">
										                <#if datumLabel!="">
										                    <td style="cursor:pointer; padding:0px; padding-top:1px; height:${datumLabel}px; text-align:right;" nowrap>
										                        ${datumLabel}
										                    </td>
										                    <#else>
										                        <td style="cursor:pointer; padding:0px; padding-top:1px; height:${zRtwellhead}px; text-align:right;" nowrap>
										                            <span style="overflow: hidden; scrolls: no; padding:0px;padding-top:0px; height:${zRtwellhead}px;text-align:right; border-top:solid 1px #000000;"> 
																											0.0 ${datumUOM} 
																											</span>
										                        </td>
										                </#if>
										                <#else>
										                    <#if zRtwellhead!="">
										                        <td style="cursor:pointer; padding:0px; padding-top:1px; height:${zRtwellhead}px; text-align:right;" nowrap>
										                            <@showField node=item field="zRtwellhead" editable=false />
										                        </td>
										                        <#else>
										                            <td style="cursor:pointer; padding:0px; padding-top:1px; height:${zRtwellhead}px; text-align:right;" nowrap>
										                                <span style="overflow: hidden; scrolls: no; padding:0px;padding-top:0px; height:${zRtwellhead}px;text-align:right; border-top:solid 1px #000000;"> 
																											0.0 ${getFieldUomSymbol("Operation", "zRtwellhead")} 
																											</span>
										                            </td>
										                    </#if>
										            </#if>
										            <td style="cursor:pointer; padding:0px;height:${formationFirstDepth}px;text-align:center;border-top:solid 1px #000000;" nowrap>
														<div style="overflow: hidden; padding:0px;padding-top:0px; height:${formationFirstDepth}px;text-align:center;"></div>
													</td>
										        </tr>
										    </#if>
											
											<#list item.list.Formation?values as detail_item>
												<#assign formationUid = getFieldValue(detail_item, "formationUid")?replace(".", "") />
												<#assign height = getDynamicAttribute(detail_item, "height") />
												<#assign sampleTopMdMsl = getFieldValue(detail_item, "sampleTopMdMsl") />
												<#assign formationName = getFieldValue(detail_item, "formationName") />
												<#assign formationImage = getFieldValue(detail_item, "formationImage") />
												<#if height!="">
													<tr valign="top">
														<td style="cursor:pointer; padding:0px;height:${height}px;text-align:center;background:url(${imgpath}/${formationImage});border-top:dotted 1px #000000;width:50px;" nowrap id="${formationUid}" class="${formationUid}">
															<div style="overflow: hidden; padding:0px;padding-top:0px; height:${height}px;text-align:center;" title="${formationName} @ ${sampleTopMdMsl} ${formationDepthUOM}">&nbsp;</div>
														</td>
													</tr>
												</#if>
											</#list>
										</#if>	
										</table>
									</td>
									<#if item.list.Bharun?exists && item.list.Bharun?size &gt; 0>
										<td align="center" style="border-left:solid 1px #000000;background-color:#dddddd;padding:0px;">
											<table style="padding:0px;border:none;" cellspacing="0" cellpadding="0">
												<tr valign="top">
													<td style="padding:0px;width:5;">&nbsp;</td>
													<#assign counter = 0 />
													<#list item.list.Bharun?values as detail_item>
														<#assign height = getDynamicAttribute(detail_item, "height") />
														<#assign height_before = getDynamicAttribute(detail_item, "height_before") />
														<#assign bhaRunNumber = getFieldValue(detail_item, "bhaRunNumber") />
														<#assign bhaType = getFieldValue(detail_item, "bhaType") />
														<#assign dailyUid = getDynamicAttribute(detail_item, "dailyUid") />
														<#if height!="" >
															<#assign counter = counter?number + 1 />
															<td style="padding:0px;text-align:center;">
																<table style="padding:0px;" cellspacing="0" cellpadding="0">
																	<#if height_before?number &gt; 1 >
																	<tr>
																		<td style="padding:0px;height:${height_before}px;border-bottom:solid 1px #000000;width:4px;cursor:none;" colspan="2">&nbsp;</td>
																	</tr>
																	</#if>
																	<#if height?number &gt; 15 >
																		<tr title="BHA #${bhaRunNumber}, BHA Type:${bhaType}" onclick="self.location.href='bharun.html?gotoday=${dailyUid}'">
																			<td style="padding:0px;height:${height}px;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:2px;" title="BHA #${bhaRunNumber}, BHA Type: ${bhaType}">
																				<div style="overflow: hidden; padding:0px;height:${height}px;cursor:pointer;">&nbsp;</div>
																			</td>
																			<td style="padding:0px;height:${height}px;border-bottom:solid 1px #000000;text-align:center;width:2px;" title="BHA #${bhaRunNumber}, BHA Type: ${bhaType}">
																				<div style="overflow: hidden; padding:0px;height:${height}px;cursor:pointer;">&nbsp;</div>
																			</td>
																			<td style="padding:0px;text-align:center;width:2px;" title="BHA #${bhaRunNumber}, BHA Type: ${bhaType}" colspan="2" nowrap>
																				<div id="bhatext_${counter}" style="display:none;">${bhaRunNumber}</div>
																			</td>
																		</tr>
																	<#else>
																		
																		<#if height?number &gt; 1 >
																			<tr title="BHA #${bhaRunNumber}, BHA Type:${bhaType}" onclick="self.location.href='bharun.html?gotoday=${dailyUid}'">
																				<td style="padding:0px;height:${height}px;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:2px;" title="BHA #${bhaRunNumber}, BHA Type:${bhaType}">
																					<div style="overflow: hidden; padding:0px;height:${height}px;cursor:pointer;">&nbsp;</div>
																				</td>
																				<td style="padding:0px;height:${height}px;border-bottom:solid 1px #000000;text-align:center;width:2px;" title="BHA #${bhaRunNumber}, BHA Type:${bhaType}">
																					<div style="overflow: hidden; padding:0px;height:${height}px;cursor:pointer;">&nbsp;</div>
																				</td>
																				<td style="font-size:1pt;padding:0px;text-align:center;width:2px;" title="BHA #${bhaRunNumber}, BHA Type:${bhaType}" colspan="2" nowrap>
																					&nbsp;
																				</td>
																			</tr>
																			<tr title="BHA #${bhaRunNumber}, BHA Type:${bhaType}" onclick="self.location.href='bharun.html?gotoday=${dailyUid}'">
																				<td style="padding:0px;text-align:center;width:2px;" title="BHA #${bhaRunNumber}, BHA Type:${bhaType}">
																					<div style="overflow: hidden; padding:0px;height:${height}px;cursor:pointer;">&nbsp;</div>
																				</td>
																				<td style="padding:0px;text-align:center;width:2px;" title="BHA #${bhaRunNumber}, BHA Type:${bhaType}">
																					<div style="overflow: hidden; padding:0px;height:${height}px;cursor:pointer;">&nbsp;</div>
																				</td>
																				<td style="padding:0px;text-align:center;width:2px;height:${height}px;" title="BHA #${bhaRunNumber}, BHA Type:${bhaType}" colspan="2" nowrap>
																					<div id="bhatext_${counter}" style="display:none;height:${height}px;overflow:visible;">${bhaRunNumber}</div>
																				</td>
																			</tr>
																		<#else>
																			<tr title="BHA #${bhaRunNumber}, BHA Type:${bhaType}" onclick="self.location.href='bharun.html?gotoday=${dailyUid}'">
																				<td style="padding:0px;text-align:center;width:2px;" title="BHA #${bhaRunNumber}, BHA Type:${bhaType}">
																					<div style="overflow: hidden; padding:0px;height:${height}px;cursor:pointer;">&nbsp;</div>
																				</td>
																				<td style="padding:0px;text-align:center;width:2px;" title="BHA #${bhaRunNumber}, BHA Type:${bhaType}">
																					<div style="overflow: hidden; padding:0px;height:${height}px;cursor:pointer;">&nbsp;</div>
																				</td>
																				<td style="padding:0px;text-align:center;width:2px;height:${height}px;" title="BHA #${bhaRunNumber}, BHA Type:${bhaType}" colspan="2" nowrap>
																					<div id="bhatext_${counter}" style="display:none;height:${height}px;overflow:visible;">${bhaRunNumber}</div>
																				</td>
																			</tr>
																		</#if>
																	</#if>
																</table>
															</td>
														</#if>
													</#list>
													<td style="padding:0px;width:5;">&nbsp;</td>
												</tr>
											</table>
										</td>
									</#if>
									
									<#if item.list.CementJob?exists && item.list.CementJob?size &gt; 0>
										<td align="center" style="border-left:dotted 1px #000000;background-color:#dddddd;padding:0px;">
											<#assign counter = 0 />
											<table style="padding:0px;" cellspacing="0" cellpadding="0">
												<tr valign="top">
													<td style="width:5;">&nbsp;</td>
													<#list item.list.CementJob?values as item2>
														<#list item2.list.CementStage?values as detail_item>
															<#assign height = getDynamicAttribute(detail_item, "height") />
															<#assign height_before = getDynamicAttribute(detail_item, "height_before") />
															<#assign jobNumber = getFieldValue(item2, "jobNumber") />
															<#assign stageNumber = getFieldValue(detail_item, "stageNumber") />
															<#assign dailyUid = getFieldValue(item2, "dailyUid") />
															<#if height!="" >
																<#assign counter = counter?number + 1 />
																<td style="padding:0px;text-align:center;">
																	<table style="padding:0px;" cellspacing="0" cellpadding="0">
																		<tr>
																			<td style="font-size:1pt; padding:0px;height:${height_before}px;" colspan="2">&nbsp;</td>
																		</tr>
																		<tr title="Job #${jobNumber} Stage #${stageNumber}" onclick="self.location.href='cementjob.html?gotoday=${dailyUid}'">
																			<td style="font-size:1pt; padding:0px;height:${height}px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:3px;cursor:pointer;">&nbsp;</td>
																			<td style="font-size:1pt; padding:0px;height:${height}px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;text-align:center;width:3px;cursor:pointer;">&nbsp;</td>
																			<td style="padding:0px;text-align:center;height:${height}px;width:2px;" title="Job #${jobNumber} Stage #${stageNumber}" colspan="2" nowrap>
																				<div id="cementtext_${counter}" style="display:none;overflow:visible;height:${height}px;">${jobNumber}/${stageNumber}</div>
																			</td>
																		</tr>
																	</table>
																</td>
															</#if>
														</#list>
													</#list>
													<td style="width:5;">&nbsp;</td>
												</tr>
											</table>
										</td>
									</#if>
									
									<#if item.list.WirelineRun?exists && item.list.WirelineRun?size &gt; 0>
										<td align="center" style="border-left:dotted 1px #000000;background-color:#dddddd;padding:0px;">
											<#assign counter = 0 />
											<table style="padding:0px;" cellspacing="0" cellpadding="0">
												<tr valign="top">
													<td style="width:5;">&nbsp;</td>
													<#list item.list.WirelineRun?values as detail_item>
														<#assign height = getDynamicAttribute(detail_item, "height") />
														<#assign height_before = getDynamicAttribute(detail_item, "height_before") />
														<#assign runNumber = getFieldValue(detail_item, "runNumber") />
														<#assign suiteNumber = getDynamicAttribute(detail_item, "suite_number") />
														<#assign wirelineLogStringDescr = getFieldValue(detail_item, "wirelineLogStringDescr") />
														<#assign wirelineSuiteUid = getFieldValue(detail_item, "wirelineSuiteUid") />
														<#assign wirelineRunUid = getFieldValue(detail_item, "wirelineRunUid") />
														<#if height!="" >
															<#assign counter = counter?number + 1 />
															<td style="padding:0px;text-align:center;">
																<table style="padding:0px;" cellspacing="0" cellpadding="0">
																	<tr>
																		<td style="font-size:1pt; padding:0px;height:${height_before}px;" colspan="2">&nbsp;</td>
																	</tr>
																	<tr title="Suite #${suiteNumber} Run #${runNumber}, Tool: ${wirelineLogStringDescr}" onclick="self.location.href='wirelinerun.html'">
																		<td style="font-size:1pt; padding:0px;height:${height}px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:3px;cursor:pointer;">&nbsp;</td>
																		<td style="font-size:1pt; padding:0px;height:${height}px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;text-align:center;width:3px;cursor:pointer;">&nbsp;</td>
																		<td style="padding:0px;text-align:center;height:${height}px;width:2px;" title="Suite #${suiteNumber} Run #${runNumber}, Tool: ${wirelineLogStringDescr}" colspan="2" nowrap>
																			<div id="wirelinetext_${counter}" style="display:none;overflow:visible;height:${height}px;">${suiteNumber}/${runNumber}</div>
																		</td>
																	</tr>
																</table>
															</td>
														</#if>
													</#list>
													<td style="width:5;">&nbsp;</td>
												</tr>
											</table>
										</td>
									</#if>
									
									<#if item.list.Core?exists && item.list.Core?size &gt; 0> 
										<td align="center" style="border-left:dotted 1px #000000;background-color:#dddddd;padding:0px;">
											<#assign counter = 0 />
											<table style="padding:0px;" cellspacing="0" cellpadding="0">
												<tr valign="top">
													<td style="width:5;">&nbsp;</td>
													<#list item.list.Core?values as detail_item>
														<#assign height = getDynamicAttribute(detail_item, "height") />
														<#assign height_before = getDynamicAttribute(detail_item, "height_before") />
														<#assign coreNumber = getFieldValue(detail_item, "coreNumber") />
														<#assign dailyUid = getFieldValue(detail_item, "dailyUid") />
														<#if height!="">
															<#assign counter = counter?number + 1 />
															<td style="padding:0px;text-align:center;">
																<table style="padding:0px;" cellspacing="0" cellpadding="0">
																	<tr>
																		<td style="font-size:1pt; padding:0px;height:${height_before}px;" colspan="2">&nbsp;</td>
																	</tr>
																	<tr title="Run #${coreNumber}" onclick="self.location.href='core.html?gotoday=${dailyUid}'">
																		<td style="font-size:1pt; padding:0px;height:${height}px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:3px;cursor:pointer;">&nbsp;</td>
																		<td style="font-size:1pt; padding:0px;height:${height}px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;text-align:center;width:3px;cursor:pointer;">&nbsp;</td>
																		<td style="padding:0px;text-align:center;height:${height}px;width:2px;" title="Run #${coreNumber}" colspan="2">
																			<div id="coreruntext_${counter}" style="display:none;overflow:visible;height:${height}px;">${coreNumber}</div>
																		</td>
																	</tr>
																</table>
															</td>
														</#if>
													</#list>
													<td style="width:5;">&nbsp;</td>
												</tr>
											</table>
										</td>
									</#if>
									<#if item.list.DrillStemTest?exists && item.list.DrillStemTest?size &gt; 0>
										<td align="center" style="border-left:dotted 1px #000000;background-color:#dddddd;padding:0px;">
											<#assign counter = 0 />
											<table style="padding:0px;" cellspacing="0" cellpadding="0">
												<tr valign="top">
													<td style="width:5;">&nbsp;</td>
													<#list item.list.DrillStemTest?values as detail_item>
														<#assign height = getDynamicAttribute(detail_item, "height") />
														<#assign height_before = getDynamicAttribute(detail_item, "height_before") />
														<#assign testNum = getFieldValue(detail_item, "testNum") />
														<#assign dailyUid = getFieldValue(detail_item, "dailyUid") />
														<#if height!="" >
															<#assign counter = counter?number + 1 />
															<td style="padding:0px;text-align:center;">
																<table style="padding:0px;" cellspacing="0" cellpadding="0">
																	<tr>
																		<td style="font-size:1pt; padding:0px;height:${height_before}px;" colspan="2">&nbsp;</td>
																	</tr>
																	<tr title="Run #${testNum}" onclick="self.location.href='drillstemtest.html?gotoday=${dailyUid}'">
																		<td style="font-size:1pt; padding:0px;height:${height}px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;border-right:solid 2px #000000;text-align:center;width:3px;cursor:pointer;">&nbsp;</td>
																		<td style="font-size:1pt; padding:0px;height:${height}px;border-top:solid 1px #000000;border-bottom:solid 1px #000000;text-align:center;width:3px;cursor:pointer;">&nbsp;</td>
																		<td style="padding:0px;text-align:center;height:${height}px;width:2px;" title="Run #${testNum}" colspan="2">
																			<div id="dsttext_${counter}" style="overflow:visible;  display:none; height:${height}px;">${testNum}</div>
																		</td>
																	</tr>
																</table>
															</td>
														</#if>
													</#list>
													<td style="width:5;">&nbsp;</td>
												</tr>
											</table>
										</td>
									</#if>
									<#if item.list.GasReadings?exists && item.list.GasReadings?size &gt; 0>
										<td align="center" style="border-left:dotted 1px #000000;background-color:#dddddd;padding:0px;">
											<img src="graph.html?type=gas_graph_well_summary&size=medium&wellDepth=${wellDepth}" style="height:{$gas_depth}px;cursor:pointer;" border="0"  onclick="top.location.href='gasreadings.html'">
										</td>
									</#if>
								</tr>	
								<tr><td>&nbsp;</td></tr>
							</table>
						</td>
						<td style="border:none;background-color:#ffffff; padding:5px;"> 
							<table cellspacing="0">
								<tr><td><b>Well Details</b><br>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
							</table>
							
							<table style="border:solid 1px #000000; width:100%; text-align:left;" cellspacing="0">
								<tr>
									<td><b>Well Name</b></td>
									<td><@showField node=item dynamic="@wellName" editable=false/></td>
								</tr>
								<tr>
									<td><b>Operator</b></td>
									<td><@showField node=item field="opCo" editable=false/></td>
								</tr>
								<tr>
									<td><b>Location</b></td>
									<td><@showField node=item dynamic="@locParish" editable=false/>
										<@showField node=item dynamic="@state" editable=false/> 
										<@showField node=item dynamic="@country" editable=false/></td>
								</tr>
								<tr>
									<td><b>Permit</b></td>
									<td><@showField node=item dynamic="@govPermitNumber" editable=false/></td>
								</tr>
								<tr>
									<td><b>Spud Date</b></td>
									<td><@showField node=item field="spudDate" format="dd MMM yyyy HH:mm" editable=false/></td>
								</tr>
								<tr>
									<td><b>Status</b></td>
									<td><@showField node=item dynamic="@currentPhase" editable=false/></td>
								</tr>
								<tr>
									<td><b>Rig Name</b></td>
									<td><@showField node=item field="rigInformationUid" editable=false/></td>
								</tr>
								<tr>
									<td><b>Rig RT</b></td>
									<td><@showField node=item dynamic="@currentDatum" editable=false/></td>
								</tr>
								<tr>
									<td><b>Latitude</b></td>
									<td><@showField node=item options="size=5" dynamic="@latDeg" editable=false />&deg;
										<@showField node=item options="size=5" dynamic="@latMinute" editable=false />'
										<@showField node=item options="size=5" dynamic="@latSecond" editable=false />&quot; 
										<@showField node=item dynamic="@latNs" editable=false /></td>
								</tr>
								<tr>
									<td><b>Longitude</b></td>
									<td><@showField node=item options="size=5" dynamic="@longDeg" editable=false />&deg;  
										<@showField node=item options="size=5" dynamic="@longMinute" editable=false />' 
										<@showField node=item options="size=5" dynamic="@longSecond" editable=false />&quot; 
										<@showField node=item dynamic="@longEw" editable=false /></td>
								</tr>
								
								<tr>
									<td><b>Easting</b></td>
									<td><@showField node=item options="size=20" dynamic="@gridEw" editable=false/></td>
								</tr>
								<tr>
									<td><b>Northing</b></td>
									<td><@showField node=item options="size=20" dynamic="@gridNs" editable=false/></td>
								</tr>
								<tr>
									<td><b>UTM Zone</b></td>
									<td><@showField node=item options="size=20" dynamic="@locUtmZone" editable=false/></td>
								</tr>
								
								<tr>
									<td><b>Spheroid</b></td>
									<td><@showField node=item options="size=15" dynamic="@spheroidType" editable=false/></td>
								</tr>
								
								<tr>
									<td><b>Cart. Datum</b></td>
									<td><@showField node=item options="size=20" dynamic="@gridDatum" editable=false/></td>
								</tr>
								<tr>
									<td><b>AFE Code</b></td>
									<td><@showField node=item field="afeNumber" editable=false/></td>
								</tr>
							</table>
							
							<br/>
							<table style="border:solid 1px #000000;width:100%; text-align:left;" cellspacing="0">
								<tr>
									<td colspan="4"><B><U>Formation Legend</U></B></td>
								</tr>
								<#list item.list.Formation?values as detail_item>
									<#assign formationImage = getFieldValue(detail_item, "formationImage") />
									<#assign formationUid = getFieldValue(detail_item, "formationUid")?replace(".", "") />
									<#assign formationName = getFieldValue(detail_item, "formationName") />
									<#assign height = getDynamicAttribute(detail_item, "height") />
									<#assign sampleTopMdMsl = getFieldValue(detail_item, "sampleTopMdMsl") />
									<#if height!="">
										<tr valign="top">
											<td><@showRecordActions node=detail_item showEdit=false showDelete=false showCancel=false value="save"/></td>
											<#if form.info.isPermittedAction('edit')>
											<td><@showField node=detail_item field="formationImage" editmode=true onchange="change_formation_image($(this),'${formationUid}');"/></td>
											</#if>
											<td style="background:url(${imgpath}/${formationImage});width:20px;" id="${formationUid}" class="${formationUid}">
												&nbsp;
											</td>
											<td nowrap>${formationName} (${sampleTopMdMsl} ${formationDepthUOM})</td>
										</tr>
									</#if>
								</#list>
							</table>
							
							<br/>
							<table style="border:solid 1px #000000; text-align:left;" cellspacing="0">
								<tr>
									<td colspan="2"><B><U>Casing Legend</U></B></td>
								</tr>
								<#list item.list.CasingSection?values as detail_item>
									<#assign bgColor = getDynamicAttribute(detail_item, "bgColor") />
									<#assign height = getDynamicAttribute(detail_item, "height") />
									<#assign casingOd = getFieldFinalValue(detail_item, "casingOd") />
									<#assign casingSummary = getFieldValue(detail_item, "casingSummary") />
									<#assign shoeTopMdMsl = getFieldValue(detail_item, "shoeTopMdMsl") />
									<#assign shoeTopMdMslUom = getFieldUomSymbol(detail_item, "shoeTopMdMsl") />
									<#if height!="">
										<tr valign="top">
											<td style="background-color:${bgColor};width:30px;border:solid 1px #000000;">&nbsp;</td>							
											<td>
												<b><@showField node=detail_item field="casingOd" editmode=false/></b>
												<#if shoeTopMdMsl!="">
												@ Shoe Depth : ${shoeTopMdMsl} ${shoeTopMdMslUom}
												</#if>
											</td>
										</tr>
									</#if>
								</#list>
								
							</table>
							<br/>
							<#if hasPSData!="0">
								<table style="border:solid 1px #000000; text-align:left;" cellspacing="0">
									<tr>
										<td colspan="2"><B><U>Production String Legend</U></B></td>
									</tr>
									<tr valign="top">
										<td style="background-color:#00AE23;width:30px;border:solid 1px #000000;">&nbsp;</td>							
										<td>
											<b>${psDesc}</b> 
											@ Depth: ${psTallyLenght}
										</td>
									</tr>
								</table>
							</#if>
						</td>
					</tr>
				</table>
			</div>
		</#if>
	</#list>
</#if>
