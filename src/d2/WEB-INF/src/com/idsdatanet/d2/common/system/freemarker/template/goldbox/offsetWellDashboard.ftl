<!DOCTYPE HTML>
<html>
	<body style="overflow-x: hidden; overflow-y: hidden;background-color:#ffffff;">
	<#assign v_resources_path = "${form.getVirtualResourcePath()}/htmllib"/>
	<#include "/core/defaultJavascriptAndStyleTemplate.ftl" />
	<#include "/templateHelper.ftl"/>

	<link rel="stylesheet" type="text/css" href="${v_resources_path}/assets/vendors/font-awesome/css/fontawesome-all.min.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/goldbox/css/OffsetWellFilter.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/select2/select2.min.css"/>
	
	<script type="text/javascript" src="${v_resources_path}/jquery/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="${v_resources_path}/select2/select2.min.js"></script>
	<script type="text/javascript" src="${v_resources_path}/jquery/jquery-ui-1.13.0.min.js"></script>

	<!-- map -->
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/anova/css/libs/leaflet.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/anova/css/libs/leaflet-messagebox.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/anova/css/libs/MarkerCluster.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/anova/css/libs/MarkerCluster.Default.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/anova/css/libs/pruneClusterLeafletStyleSheet.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/anova/css/mapMenuUI-style.css"/> 
	<script type="text/javascript" src="${v_resources_path}/anova/js/libs/leaflet-min.js"></script>
	<script type="text/javascript" src="${v_resources_path}/anova/js/libs/leaflet.markercluster.js"></script>
	<script type="text/javascript" src="${v_resources_path}/anova/js/libs/d3.js"></script>
	<script type="text/javascript" src="${v_resources_path}/anova/js/libs/lasso.js"></script>
	<script type="text/javascript" src="${v_resources_path}/anova/js/libs/leaflet-ajax.js"></script>
	<script type="text/javascript" src="${v_resources_path}/anova/js/libs/leaflet-messagebox.js"></script>
	<script type="text/javascript" src="${v_resources_path}/anova/js/libs/leaflet-providers.js"></script>
	<script type="text/javascript" src="${v_resources_path}/anova/js/libs/PruneCluster.js"></script>
	<script type="text/javascript" src="${v_resources_path}/anova/js/libs/wNumb.js"></script>	
	
	<!-- Date range and slider -->
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/anova/css/libs/nouislider.min.css"/>
	<link rel="stylesheet" type="text/css" href="${v_resources_path}/datepicker/css/daterangepicker.min.css">
	<script type="text/javascript" src="${v_resources_path}/anova/js/libs/wNumb.js"></script>
	<script type="text/javascript" src="${v_resources_path}/datepicker/moment.js"></script> 
	<script type="text/javascript" src="${v_resources_path}/datepicker/knockout-3.4.2.js"></script>
	<script type="text/javascript" src="${v_resources_path}/datepicker/js/daterangepicker.min.js"></script>
	<script type="text/javascript" src="${v_resources_path}/anova/js/libs/nouislider.min.js"></script>
	
	<!-- screen logic -->
	<script type="text/javascript" src="${v_resources_path}/goldbox/js/mapMenuUI.js"></script>
	<script type="text/javascript" src="${v_resources_path}/goldbox/js/FilterField.js"></script>
	<script type="text/javascript" src="${v_resources_path}/goldbox/js/MessageManager.js"></script>
	<script type="text/javascript" src="${v_resources_path}/goldbox/js/OffsetWellFilter.js"></script>
	

	<!-- start page -->
	<script type="text/javascript">
		$(document).ready(function() {			
			var filter = new D3.OffsetWellFilter({
				renderTo: "filterbox",
				width: "100%",
				height: "100%"
			});
		});
	</script>
	
		<div id="filterbox"></div>
		<div id="maincontentbox" class='w100 h100' style='padding-left: 35px; overflow:hidden'>
			<iframe id="innerFrame" style="padding: 0px; border: 0px none; margin: 0px; height: 100%; width: 100%;" frameborder="0"></iframe>
		</div>
	</body>
</html>