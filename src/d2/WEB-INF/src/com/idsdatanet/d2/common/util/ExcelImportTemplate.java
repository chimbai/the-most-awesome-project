package com.idsdatanet.d2.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.idsdatanet.d2.common.util.node.GenericNodeListener;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class ExcelImportTemplate {
    protected String className = null;
    protected String name = null;
    protected Integer headerRow = 0;
    protected Integer startRow = 0;
    protected Integer endRow = 0;
    protected List<Integer> skipRows = null;
    protected GenericNodeListener listener = null;
    protected List<ExcelMapping> excelMappingList = null;
    protected Boolean breakOnEmpty = false;
    
    protected String parent = null;
    protected List<String> children = null;

    protected Workbook book;
    protected FormulaEvaluator evaluator;

    public ExcelImportTemplate() { 
        this.headerRow = 0;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHeaderRow() {
        return headerRow;
    }

    public void setHeaderRow(Integer headerRow) {
        this.headerRow = headerRow;
    }

    public List<ExcelMapping> getExcelMappingList() {
        return excelMappingList;
    }

    public void setExcelMappingList(List<ExcelMapping> excelMappingList) {
        this.excelMappingList = excelMappingList;
    }
    
    public GenericNodeListener getListener() {
        return listener;
    }

    public void setListener(GenericNodeListener listener) {
        this.listener = listener;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public List<String> getChildren() {
        return children;
    }

    public void setChildren(List<String> children) {
        this.children = children;
    }

    public Integer getStartRow() {
        return startRow;
    }

    public void setStartRow(Integer startRow) {
        this.startRow = startRow;
    }

    public Integer getEndRow() {
        return endRow;
    }

    public void setEndRow(Integer endRow) {
        this.endRow = endRow;
    }

    public List<Integer> getSkipRows() {
        return skipRows;
    }

    public void setSkipRows(List<Integer> skipRows) {
        this.skipRows = skipRows;
    }

    public Object getNewObject(String className) throws Exception {
	    if (StringUtils.isNotBlank(className)) {
            Class<?> clazz = Class.forName("com.idsdatanet.d2.core.model." + className);
            Object o = clazz.newInstance();
            return o;
	    }
	    return null;
	}

    public List<Object> parseTemplate(File excelFile, BaseCommandBean commandBean) throws Exception {
        List<Object> objectList = null;
        InputStream inputStream = new FileInputStream(excelFile);
        this.book = WorkbookFactory.create(inputStream);
        this.evaluator = book.getCreationHelper().createFormulaEvaluator();
        Sheet sheet = book.getSheetAt(0);

        if (sheet != null) {
            objectList = new ArrayList<>();
            Map<Integer, ExcelMapping> columnMap = null;
            int lastRowNum = sheet.getLastRowNum();
            Row row = null;
            for (int i = this.headerRow; i <= lastRowNum; i++) {
                row = sheet.getRow(i);
                if (row == null) {
                    continue;
                }
                if (i == this.headerRow) {
                    columnMap = this.parseHeader(row, commandBean);
                } else {
                    if (columnMap == null || columnMap.size() == 0) {
                        break;
                    } // or throw error
                    Object obj = this.parseRow(row, this.className, columnMap, commandBean, objectList);
                    if (obj != null) {
                        objectList.add(obj);
                    } else {
                        if (this.breakOnEmpty) {
                            break;
                        } else {
                            continue;
                        }
                    }
                }
            }
            if (this.listener != null) {
                objectList = this.listener.afterAllObjectCreate(objectList, commandBean, this, columnMap);
            }
        }
        book.close();
        inputStream.close();
        return objectList;
    }

    public Map<Integer, ExcelMapping> parseHeader(Row row, BaseCommandBean commandBean) throws Exception {
        DataFormatter formatter = new DataFormatter();
        LinkedHashMap<Integer, ExcelMapping> map = new LinkedHashMap<>();
        int j = 0;
        while (true) {
            Cell cell = row.getCell(j);
            if (cell == null) {
                break;
            }
            String value = formatter.formatCellValue(cell);
            if (StringUtils.isBlank(value)) {
                break;
            }
            for (ExcelMapping excelMapping : this.excelMappingList) {
                if (StringUtils.equalsIgnoreCase(excelMapping.getName(), value)) {
                    excelMapping.setColumn(j);
                    map.put(j, excelMapping);
                    break;
                }
            }
            j++;
        }
        return map;
    }

    public Object parseRow(Row row, String className, Map<Integer, ExcelMapping> columnMap, BaseCommandBean commandBean, List<Object> objList) throws Exception {
        Object object = ImportUtil.getNewObject(className);

        if (this.listener != null) {
            object = this.listener.beforeNodeCreate(object, commandBean, this, objList, columnMap);
        }
        Iterator<Entry<Integer, ExcelMapping>> it = columnMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, ExcelMapping> pair = (Map.Entry<Integer, ExcelMapping>) it.next();
            Integer columnNo = pair.getKey();
            ExcelMapping mapping = pair.getValue();
            String ids = mapping.getIds();
            Cell c = row.getCell(columnNo);
            if (c == null) {
                if (mapping.isMandatory()) {
                    return null;
                } else {
                    continue;
                }
            } else {
                object = this.processCell(c, ids, mapping.getType(), object, mapping, commandBean, objList);
                if (object == null) {
                    return null;
                }
            }
        }
        if (object != null && this.listener != null) {
            object = this.listener.afterObjectCreate(object, commandBean, this, objList, columnMap);
        }
        return object;
    }

    public Object processCell(Cell c, String ids, String type, Object object, ExcelMapping mapping, BaseCommandBean commandBean, List<Object> objectList) throws Exception {
        if (object == null || c == null) {
            return null;
        }
        String cellValue = this.getCellValue(c);
        if(StringUtils.isBlank(cellValue)) {
            if (mapping.isMandatory()) {
                return null;
            } else {
                return object;
            }
        }
        return this.processField(c, object, cellValue, ids, type, mapping, commandBean, objectList);
    }

    protected String getCellValue(Cell c) {
        DataFormatter formatter = new DataFormatter();
        String cellValue = null;
        if (c.getCellType() == Cell.CELL_TYPE_FORMULA) {
            cellValue = formatter.formatCellValue(c, evaluator);
        } else if (StringUtils.isNotBlank(formatter.formatCellValue(c))) {
            cellValue = formatter.formatCellValue(c);
        }
        return cellValue;
    }

    protected Object parseCellValue(String cellValue, String type) {
    	return RegexUtil.parseType(cellValue, type);
    }
    
    protected Object processField(Cell c, Object object, String cellValue, String ids, String type, ExcelMapping mapping, BaseCommandBean commandBean, List<Object> objectList) throws Exception {
        // Handling Mandatory fields that are not used in IDS
        if (StringUtils.isNotBlank(ids) && PropertyUtils.isWriteable(object, ids)) {
            Object value = this.parseCellValue(cellValue, type);
            String valueStr = value != null ? value.toString() : null;

            // Dealing with UOM
            if (ExcelMapping.UOM_TYPE_LIST.contains(type)) {
                String unitSymbol = this.getUnitSymbol(valueStr, mapping);
                Double fieldValue = this.getFieldValue(valueStr, c);
                value = ExcelImportTemplate.processUomConversion(object, mapping.getIds(), unitSymbol, commandBean, fieldValue);
            }

            if (value == null) {
                return null;
            }

            if (mapping.getFieldHandler() != null) { // Field Specific Handler
                value = mapping.getFieldHandler().translate(value, object, commandBean, mapping, objectList, this.book, c);
            }
            PropertyUtils.setProperty(object, ids, value);
        } else {
            if (StringUtils.isNotBlank(cellValue) && mapping.isMandatory()) {
                return object;
            } else {
                return null;
            }
        }
        return object;
    }

    protected String getUnitSymbol(String valueStr, ExcelMapping mapping) {
        String unitSymbol = RegexUtil.parseUnitSymbol(valueStr);
        if (StringUtils.isBlank(unitSymbol) && mapping.getDefaultUnit() != null) {
            unitSymbol = mapping.getDefaultUnit();
        }
        return unitSymbol;
    }

    protected Double getFieldValue(String valueStr, Cell c) throws Exception {
        Double fieldValue = RegexUtil.parseDouble(valueStr);
        if (c.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            fieldValue = Double.parseDouble(c.toString());
        }
        return fieldValue;
    }

    public static Object processUomConversion(Object object, String idsFieldName, String unitSymbol, BaseCommandBean commandBean, Double fieldValue) throws Exception {
        if (fieldValue == null) {
            return null;
        }
        CustomFieldUom incomingFormat = new CustomFieldUom((Locale) null, object.getClass(), idsFieldName);
        try {
            incomingFormat.changeUOMUnitWithSymbol(unitSymbol);
        } catch (Exception e) {
            commandBean.getSystemMessage().addError("Cell: " + idsFieldName + " does not have unit: " + unitSymbol);
        }
        incomingFormat.setBaseValueFromUserValue(fieldValue);
        CustomFieldUom thisFormat = new CustomFieldUom(commandBean);
        thisFormat.setReferenceMappingField(object.getClass(), idsFieldName, commandBean.getCurrentUserSelectionSnapshot().getUomTemplateUid());
        thisFormat.setBaseValue(incomingFormat.getBasevalue());
        return thisFormat.getConvertedValue();
    }
    
    public Boolean getbreakOnEmpty() {
        return breakOnEmpty;
    }

    public void setbreakOnEmpty(Boolean value) {
        this.breakOnEmpty = value;
    }

}