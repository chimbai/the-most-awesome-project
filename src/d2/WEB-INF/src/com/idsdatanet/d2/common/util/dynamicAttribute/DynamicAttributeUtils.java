package com.idsdatanet.d2.common.util.dynamicAttribute;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.web.mvc.CommandBean;



public class DynamicAttributeUtils {
	
	public static Class<?> getClass(String className) throws Exception {
	    if (StringUtils.isNotBlank(className)) {
            Class<?> clazz = Class.forName("com.idsdatanet.d2.core.model." + className);
            return clazz;
	    }
	    return null;
	}
	
	public static Object getNewObject(String className) throws Exception {
	    if (StringUtils.isNotBlank(className)) {
            Class<?> clazz = Class.forName("com.idsdatanet.d2.core.model." + className);
            Object o = clazz.newInstance();
            return o;
	    }
	    return null;
	}
	
	public static Object convertObject(CommandBean commandBean, Object value, String type) throws Exception {
	    if (value != null && StringUtils.isNotBlank(type)) {
            if (StringUtils.equals("String", type)) {
            	return value;
            } else if (StringUtils.equals("Double", type)) {
            	String doubleStr = value.toString();
    			DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(commandBean.getUserLocale());
                String thousandSeparator = Character.toString(formatter.getDecimalFormatSymbols().getGroupingSeparator());
                String decimalSeparator = Character.toString(formatter.getDecimalFormatSymbols().getDecimalSeparator());
                doubleStr = doubleStr.replace(thousandSeparator, "");
                doubleStr = doubleStr.replace(decimalSeparator, ".");
            	return Double.parseDouble(doubleStr);
            } else if (StringUtils.equals("Integer", type)) {
            	return Integer.parseInt(value.toString());
            }
	    }
	    return null;
	}
	
	public static void populateContextUids(Object source, Object target) throws Exception {
		List<String> fieldNames = Arrays.asList("wellUid","wellboreUid","operationUid","dailyUid");
		Map<String, Object> sourceClassProperties = PropertyUtils.describe(source);
		Map<String, Object> targetClassProperties = PropertyUtils.describe(target);
		
		for (String fieldName : fieldNames) {
			if (sourceClassProperties.containsKey(fieldName) && PropertyUtils.isReadable(source, fieldName)) {
				if (targetClassProperties.containsKey(fieldName) && PropertyUtils.isWriteable(target, fieldName)) {
					PropertyUtils.setProperty(target, fieldName, PropertyUtils.getProperty(source, fieldName));
				}
			}
		}	
	}
	
	public static void copyField(Object source, Object target, String fieldName) throws Exception {
		Map<String, Object> sourceClassProperties = PropertyUtils.describe(source);
		Map<String, Object> targetClassProperties = PropertyUtils.describe(target);
		if (sourceClassProperties.containsKey(fieldName) && PropertyUtils.isReadable(source, fieldName)) {
			if (targetClassProperties.containsKey(fieldName) && PropertyUtils.isWriteable(target, fieldName)) {
				PropertyUtils.setProperty(target, fieldName, PropertyUtils.getProperty(source, fieldName));
			}
		}
	}
	
	public static void populateField(Object target, String fieldName, Object value) throws Exception {
		Map<String, Object> targetClassProperties = PropertyUtils.describe(target);
		if (targetClassProperties.containsKey(fieldName) && PropertyUtils.isWriteable(target, fieldName)) {
			PropertyUtils.setProperty(target, fieldName, value);
		}
	}
	
	public static String convertCamelCaseToUnderscore(String camelCase) {
		if (StringUtils.isBlank(camelCase)) return camelCase;
		StringBuffer sb = new StringBuffer();
		for (int i=0; i<camelCase.length(); i++) {
			char c = camelCase.charAt(i);
			if (Character.isUpperCase(c)) {
				if (i == 0) {
					sb.append(Character.toLowerCase(c));
				} else {
					sb.append("_"  + Character.toLowerCase(c));
				}
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}
	
}
