<html>
    <head><style>
      
      body {
        margin: 0px;
        padding: 0px;
      }
      .ui-slider-horizontal {
		    height: 0.4em !important;
		}
		.ui-slider .ui-slider-handle {
	   
	    height: 0.8em !important;
	    width: 0.8em !important;
	}
	.schematicLabel{
		font-size:10pt !important;
	}
    </style></head>
<body>
	<#assign v_resources_path = "${form.getVirtualResourcePath()}/htmllib"/>
	
	<#include "/core/defaultJavascriptAndStyleTemplate.ftl" />

	<script src="${v_resources_path}/schematics/d3-Schematic.js"></script>
	<script src="${v_resources_path}/schematics/d3-Engine.js"></script>
	<script src="${v_resources_path}/schematics/d3-WellSchematic.js"></script>

	<script type="text/javascript">
		
	
		$(document).ready(function(){
			
			window.D3.Layout = $('body').layout(
			{ 
				north:{
					size:44,
					spacing_open: 2,
		            spacing_closed: 0,
		            togglerLength_open: 0,
		            togglerLength_clsed: 0,
				},
				west:{
					resizeble:true,
					onresize_end:window.D3.onResize,
					closable:true,
					minSize:420
				}
			}
			);
			$("#sliderDay").slider();
			$("#sliderTime").slider();
			window.WellSchematic = new D3.WellSchematic(${form.getOutputAdaptorResult()},window.D3.Layout.state.west);
			
		});
		
		window.D3.onResize = function(){
			window.WellSchematic.onResize();
		}
	</script>
	
	<div class="ui-layout-center">
<iframe id="dataTab" frameborder="0"  style="padding:0; border:0; margin:0; height:100%; width:100%" src=""></iframe>
	
	</div>

	<div class="ui-layout-north">
		<table width="100%" cellSpacing="1" cellPadding="1">
			<tr>
				<td style="width:80px;text-align:right"><span id="lblStartDate" class="schematicLabel"></span></td>
				<td ><div id="sliderDay"></div></td>
				<td style="width:80px"><span id="lblEndDate" class="schematicLabel"></span></td>
				<td style="width:80px"><span class="schematicLabel">Date:</span></td>
				<td style="width:120px"><span id="lblSelectedDate" class="schematicLabel"></span></td>
			</tr>
			<tr>
				<td style="width:80px;text-align:right"><span class="schematicLabel">00:00</span></td>
				<td><div id="sliderTime"></div></td>
				<td style="width:80px"><span class="schematicLabel">24:00</span></td>
				<td style="width:80px"><span class="schematicLabel">Time:</span></td>
				<td style="width:120px"><span id="lblSelectedTime" class="schematicLabel"></td>
			</tr>
		</table>
	</div>
	<div class="ui-layout-west" id="schematicPanel">
		<div id="header" class="schematicHeader">
		</div>
		<div id="main" class="schematicBody">
		</div>
		<div id="footer" class="schematicFooter">
			<image src="images/schematics/setting.png" class="schematicButton"/>
			<image id="btnZoomOut" src="images/schematics/zoomout.png" class="schematicButton"/>
			<image id="btnZoom" src="images/schematics/zoomin.png" class="schematicButton"/>
			<image id="btnDataEntry" src="images/schematics/pointer.png" class="schematicButton"/>
			<image id="btnAnnotation" src="images/schematics/annotation.png" class="schematicButton"/>
			<image id="btnAnnotationSwitch" src="images/schematics/on.png" class="schematicButton"/>
			<image id="btnDownloadImage" src="images/schematics/export.png" class="schematicButton"/>
			<span id="lblDepth" class="schematicLabel"></span>
		</div>
	</div>

</body>
</html> 