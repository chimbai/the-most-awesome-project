package com.idsdatanet.d2.common.util.field;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.SupportVesselInformation;
import com.idsdatanet.d2.core.model.SupportVesselParam;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class OceanApexBlackRhinoVesselNameFieldHandler extends GenericFieldHandler {
	
    @Override
    public Object translate(Object value, Object object, BaseCommandBean commandBean, ExcelMapping mapping, List<Object> objList, Workbook book, Cell cell) throws Exception {
        String rv = null;
        if (value != null && value instanceof String && object != null && object instanceof SupportVesselParam) {
            rv = value.toString();
            
            // Reverse Lookup
            String sql = "FROM SupportVesselInformation WHERE (isDeleted = false OR isDeleted IS NULL) AND vesselName LIKE :rv ";
            List<SupportVesselInformation> SupportVesselInformationList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "rv", value.toString());
            if (!SupportVesselInformationList.isEmpty()) {
                for (SupportVesselInformation supportVesselInformation : SupportVesselInformationList) {
	                rv = supportVesselInformation.getSupportVesselInformationUid();
	                return rv;
                }
            }
            if (SupportVesselInformationList.isEmpty()) {
            	CommonUtil.getConfiguredInstance().createSupportVesselInformation(rv);
				String sql2 = "FROM SupportVesselInformation WHERE (isDeleted = false OR isDeleted IS NULL) AND vesselName LIKE :rv ";
				List<SupportVesselInformation> SupportVesselInformationList2 = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql2, "rv", value.toString());
				if (!SupportVesselInformationList2.isEmpty()) {
				    for (SupportVesselInformation supportVesselInformation2 : SupportVesselInformationList2) {
				       rv = supportVesselInformation2.getSupportVesselInformationUid();
				       return rv;
				    }
				}
            }
        }
        return rv;
    }
}