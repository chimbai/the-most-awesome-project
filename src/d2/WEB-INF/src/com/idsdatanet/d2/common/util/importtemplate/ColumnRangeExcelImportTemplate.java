package com.idsdatanet.d2.common.util.importtemplate;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class ColumnRangeExcelImportTemplate extends ExcelImportTemplate {
	private int columnStart = -1;
	private int columnEnd = -1;
	private String autoIncrementField = null;
	
	private class AutoIncrement{
		int count = 0;
		final String field;
		
		AutoIncrement(String field){
			this.field = field;
		}
		
		Object apply(Object data) throws IllegalAccessException, InvocationTargetException {
			if(this.field != null) {
				BeanUtils.setProperty(data, this.field, ++this.count);
			}
			return data;
		}
	}
	
	public void setAutoIncrementField(String value) {
		this.autoIncrementField = value;
	}
	
	public String getAutoIncrementField() {
		return this.autoIncrementField;
	}
	
	public void setColumnStart(int value) {
		this.columnStart = value;
	}
	
	public int getColumnStart() {
		return this.columnStart;
	}
	
	public void setColumnEnd(int value) {
		this.columnEnd = value;
	}
	
	public int getColumnEnd() {
		return this.columnEnd;
	}
	
	@Override
    public List<Object> parseTemplate(File excelFile, BaseCommandBean commandBean) throws Exception {
        InputStream inputStream = new FileInputStream(excelFile);
        try {
        	return this.parseWorkbook(inputStream, commandBean);
        }finally {
       		inputStream.close();
        }
    }
	
	private List<Object> parseWorkbook(InputStream inputStream, BaseCommandBean commandBean) throws Exception {
        this.book = WorkbookFactory.create(inputStream);
        try {
	        this.evaluator = book.getCreationHelper().createFormulaEvaluator();
	        Sheet sheet = book.getSheetAt(0);
	        if (sheet != null) {
	            List<Object> objectList = new ArrayList<>();
	            AutoIncrement autoIncrement = new AutoIncrement(this.autoIncrementField);
	            this.parseSheet(sheet, commandBean, objectList, autoIncrement);
	            return objectList;
	        }else {
	        	return null;
	        }
        }finally {
        	book.close();
        }
	}
	
	private void parseSheet(Sheet sheet, BaseCommandBean commandBean, List<Object> objectList, AutoIncrement autoIncrement) throws Exception{
		if(this.columnStart != -1 && this.columnEnd != -1) {
            for (int i = this.columnStart; i <= this.columnEnd; i++) {
                Object obj = this.parseColumn(sheet, i, commandBean, objectList);
                if (obj != null) {
                    objectList.add(autoIncrement.apply(obj));
                }
            }
		}
	}
	
    public Object parseColumn(Sheet sheet, int col, BaseCommandBean commandBean, List<Object> objList) throws Exception {
        Object object = this.getNewObject(this.className);
        for(ExcelMapping mapping: this.getExcelMappingList()) {
        	Row row = sheet.getRow(mapping.getRowIndex());
        	Cell c = row.getCell(col);
            if (c == null) {
                if (mapping.isMandatory()) {
                    return null;
                } else {
                    continue;
                }
            } else {
            	String ids = mapping.getIds();
                object = this.processCell(c, ids, mapping.getType(), object, mapping, commandBean, objList);
                if (object == null) {
                    return null;
                }
            }
        }
        return object;
    }
    
    @Override
    protected Object parseCellValue(String cellValue, String type) {
    	if("string".equalsIgnoreCase(type)) {
    		return cellValue;
    	}else if("double".equalsIgnoreCase(type)) {
    		return Double.parseDouble(cellValue);
    	}else if("integer".equalsIgnoreCase(type)) {
    		return Integer.parseInt(cellValue);
    	}else {
    		return super.parseCellValue(cellValue, type);
    	}
    }
}
