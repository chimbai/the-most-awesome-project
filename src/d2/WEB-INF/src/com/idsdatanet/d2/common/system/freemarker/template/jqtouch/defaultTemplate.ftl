<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<#include "/templateHelper.ftl"/>
<#include "/core/d2Helper.ftl"/>

<head>
	<title>IDS Datanet</title>
	<style type="text/css" media="screen">@import "styles/jqtouch/jqtouch.css";</style>
	<style type="text/css" media="screen">@import "styles/jqtouch/themes/apple/theme.css";</style>
	<style type="text/css" media="screen">
        #jqt ul.plastic li small {
		    text-transform: none;
		}
		#jqt #ids {
		    background: #17181a;
		}
		#jqt ul.ids {
		    background: #17181a;
		    color: #aaa;
		    font: bold 18px "Helvetica Neue", Helvetica;
		    margin: 0;
		    padding: 0;
		    border-width: 0 0 1px 0;
		}
		#jqt ul.ids li {
		    border-width: 1px 0;
		    border-style: solid;
		    border-top-color: #222;
		    border-bottom-color: #000;
		    color: #666;
		    list-style-type: none;
		    overflow: hidden;
		    padding: 10px 10px 10px 10px;
		}
		#jqt ul.ids li a.active.loading {
		    background-image: url(styles/jqtouch/themes/jqt/img/loading.gif);
		    background-position: 95% center;
		    background-repeat: no-repeat;
		}
		#jqt ul.ids li a em {
		    display: block;
		    font-size: 14px;
		    font-style: normal;
		    color: #444;
		    width: 50%;
		    line-height: 14px;
		}
		#jqt ul.ids li small {
		    color: #888;
		    font-size: 13px;
		    text-transform: none;
		    float: right;
		    position: relative;
		    margin-top: 10px;
		    font-weight: bold;
		    width: 30%;
		}
		#jqt ul.ids li:nth-child(odd) {
		    background-color: #1c1c1f;
		}
		#jqt ul.ids li.arrow {
		    background-image: url(styles/jqtouch/themes/jqt/img/chevron.png);
		    background-position: right center;
		    background-repeat: no-repeat;
		}
		#jqt ul.ids li.arrow a.active {
		    background-image: url(styles/jqtouch/themes/jqt/img/chevron.png);
		    background-position: right center;
		    background-repeat: no-repeat;
		}
		#jqt ul.ids li.forward {
		    background-image: url(styles/jqtouch/themes/jqt/img/chevron_circle.png);
		    background-position: right center;
		    background-repeat: no-repeat;
		}
		#jqt ul.ids li.forward a.active {
		    background-image: url(styles/jqtouch/themes/jqt/img/chevron_circle.png);
		    background-position: right center;
		    background-repeat: no-repeat;
		}
		
		#jqt ul.ids1 li a em {
		    display: block;
		    font-size: 14px;
		    font-style: normal;
		    color: #888;
		    width: 50%;
		    line-height: 14px;
		}
		#jqt ul.ids1 li small {
		    color: #888;
		    font-size: 13px;
		    text-transform: none;
		    float: right;
		    position: relative;
		    margin-top: 10px;
		    font-weight: bold;
		    width: 30%;
		}
    </style>
	<script src="public/jqtouch/jquery.1.3.2.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="public/highcharts/highcharts.js" type="text/javascript" charset="utf-8"></script>
	<script src="public/highcharts/excanvas-compressed.js" type="text/javascript" charset="utf-8"></script>
	<script src="public/jqtouch/jqtouch.js" type="application/x-javascript" charset="utf-8"></script>
	<script type="text/javascript" charset="utf-8">
		var jQT = new $.jQTouch({
		    icon: 'jqtouch.png',
		    addGlossToIcon: false,
		    startupScreen: 'jqt_startup.png',
		    statusBar: 'black',
		    preloadImages: [
		        'styles/jqtouch/themes/jqt/img/back_button.png',
		        'styles/jqtouch/themes/jqt/img/back_button_clicked.png',
		        'styles/jqtouch/themes/jqt/img/button_clicked.png',
		        'styles/jqtouch/themes/jqt/img/grayButton.png',
		        'styles/jqtouch/themes/jqt/img/whiteButton.png',
		        'styles/jqtouch/themes/jqt/img/loading.gif'
		        ]
		});
	</script>
</head>
<body>
	<#if form??>
		<#if form.info.getCustomView("data")??>
			<div id="jqt">
				<#include form.info.getCustomView("data")/>
			</div>
		</#if>
	</#if>
</body>
</html>