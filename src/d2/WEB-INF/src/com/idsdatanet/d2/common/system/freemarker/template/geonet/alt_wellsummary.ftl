<!DOCTYPE HTML>
<html>
<head>
	<#assign v_resources_path = "${form.getVirtualResourcePath()}/htmllib"/>
	<#include "/core/defaultJavascriptAndStyleTemplate.ftl" />
	<#assign customCss = form.getCss()/>
	
	<#if customCss??>
		<#list customCss as css>
			<#if css.url??>
 				<link rel="stylesheet" type="text/css" href="${css.url}"/>
 			<#elseif css.content??>
 				<style>
 					${css.content}
 				</style>
 			</#if>
		</#list>
 	</#if>
	
	<#assign scripts = form.getScripts()/>
	
	<#if scripts??>
		<#list scripts as script>
			<#if script.url??>
 				<script type="text/javascript" src="${script.url}"></script>
 			<#elseif script.content??>
 				<script type="text/javascript">
 					${script.content}
 				</script>
 			</#if>
		</#list>
 	</#if>
	<script type="text/javascript">
		$(document).ready(function(){
			var ws = new window.D3.WellSummary(document.getElementById('container'),document.getElementById('topContainer'));
			ws.init(${form.getOutputAdaptorResult()});
			ws.render();
			window.D3.WS = ws;
		});
	</script>
</head>
<body style="overflow:auto">
	<div id="topContainer" style="display:none"></div>
	<div id="container" align="center" style="background-color:#ffffff; min-height:100%"/>
</body>
</html> 