<html>
<body>
Dear Sir/Madam, 

<p>This is an automated message.</p>

<p>Match found as follows:</p>
<#if lessonLearned?exists>
	<p>
		<table>
			<#list lessonLearned?keys as lessonLearnedUid>
				<tr>
					<td>
						<a href="${lessonLearned[lessonLearnedUid]["hyperLink"]}">Lesson Ticket# ${lessonLearned[lessonLearnedUid]["lessonTicketNumber"]}</a>
					</td>
				</tr>
			</#list>
		</table>
	</p>
</#if>

<p>Please click on the ticket link to get further details on the lesson ticket.</p>

<p>Please do not reply to the address above.</p> 
<p>Thank you.</p>
<p><b>IDS Datanet Service</b></p>
</body>
</html>