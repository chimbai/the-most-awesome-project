package com.idsdatanet.d2.common.util.node;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.PersonnelOnSite;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class OceanApexPobNodeListener extends GenericNodeListener {
    
    @Override
    public Object beforeNodeCreate(Object object, BaseCommandBean commandBean, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        return object;
    }
    
    @Override
    public Object afterObjectCreate(Object object, BaseCommandBean commandBean, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        if (object != null && object instanceof PersonnelOnSite) {
            PersonnelOnSite pob = (PersonnelOnSite) object;
            pob.setPax(1); // Default value as specified in specification
            return pob;
        }
        return object;
    }
    
    @Override
    public List<Object> afterAllObjectCreate(List<Object> objList, BaseCommandBean commandBean, ExcelImportTemplate template, Map<Integer, ExcelMapping> columnMap) throws Exception {
        if (objList == null || objList.isEmpty()) {
            return objList;
        }
        
        for (Object object : objList) {
        	PersonnelOnSite pob = (PersonnelOnSite) object;
         	// ticekt number #16628 - Resolve Nationality
             String nationality = pob.getNationality();
             if (StringUtils.isNotBlank(nationality)) {
                 if (StringUtils.equals("Nat.", nationality)) {
                     pob.setNationality("local");
                 } else {
                     pob.setNationality("expat");
                 }
             }
        	 
        }

        
        return objList;
    }
    
}