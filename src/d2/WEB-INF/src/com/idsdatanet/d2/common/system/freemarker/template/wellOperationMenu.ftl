<#include "../../templateHelper.ftl"/>
<#include "../../core/d2Helper.ftl"/>
<style type="text/css">
	#welloperationsearch .sbox input {
		width:150px;
	}
	
	#welloperationsearch .sbox input {
		-moz-background-clip:border;
		-moz-background-inline-policy:continuous;
		-moz-background-origin:padding;
		background:white url(images/magglass.png) no-repeat scroll 4px 5px;
		border:1px solid #828790;
		color:#555555;
		float:right;
		font-size:10pt;
		padding:2px 2px 2px 18px;
	}
</style>
<script type="text/javascript">
<!--
	$(document).ready(function(){
		$('#operationDropDown').dblclick(function() {
			wellOperationMenuManager.submitWellOperationSelection();
		});
	});

	wellOperationMenuManager = {
		isBlock: false,
		isFetched: false,
		isSearchBoxCleared: false,
		timeOutId: 0,
		
		<#if getGWP(GWPReference.GWP_OPERATION_SELECTOR) == "1">
			showWellOperationMenu: function() {
				$("#OperationSelectorMenuPopup").click();
			},
		<#else>
			<#if getGWP(GWPReference.GWP_WELL_OPERATION_MENU_POPUP_LAZY_LOADING) != "1">
				showWellOperationMenu: function() {
					$("#wellOperationMenuPopup").click();
				},
			<#else>
				showWellOperationMenu: function() {
					$("#wellOperationMenuPopup").click();
					
					var selectedWellUid = '${_sys_menu.selected_wellUid!""}';
					
					if (!this.isFetched) {
						// check if campaign is available
						var campaignDropDown = $("#campaignDropDown");
						var rigDropDown = $("#rigDropDown");
						
						if (selectedWellUid != '') {
							this.onClickDropdown(null, 'wellDropDown', 'wellByWellUid', selectedWellUid);
						// campaign dropdown is visible
						} else if (campaignDropDown.val()) {
							// retrieve well list by campaignUid
							this.onClickDropdown(campaignDropDown, 'wellDropDown', 'well', null);
						} else if (rigDropDown.val()) {
							// retrieve well list by rigInformationUid
							this.onClickDropdown(rigDropDown, 'wellDropDown', 'wellByRigUid', null);
						}else {
							this.onClickDropdown('--all_well--', 'wellDropDown', 'well', null);
						}
						this.isFetched = true;
					}
				},
			</#if>
		</#if>
		
		onClickDropdown: function(currentObj, target, requestType, searchValue) {
			var uuid = new UUID();
			var campaignDropDownValue = $("#campaignDropDown").val();
			var operationCodeDropDownValue = $("#operationCodeDropDown").val();
			var rigDropDownValue = $("#rigDropDown").val();
			var campaignUid = "";
			var operationCode = "";
			var rigInformationUid = "";
			var id = "";
			
			// remove unwanted child
			wellOperationMenuManager.removeUnwantedData(requestType);
			
			if (searchValue != null) {
				id = searchValue;
			} else {
				id = (currentObj != '--all_well--')? currentObj.val() : currentObj;
			}
			
			if (operationCodeDropDownValue) {
				operationCode = operationCodeDropDownValue;
			}
			
			if (campaignDropDownValue) {
				campaignUid = campaignDropDownValue;
			}
			
			if (rigDropDownValue) {
				rigInformationUid = rigDropDownValue;
			}
			
			if (id != null && id != "") {
				this.showLoading();
				
				$.post("wellOperationMenuAjax.html?" + uuid, {"requestType":requestType, "id":id, "campaignUid":campaignUid, "operationCode":operationCode, "rigInformationUid":rigInformationUid}, function(dataFromServer) {
					var message = dataFromServer.message;
					var messageContainer = document.getElementById("wellOperationMenuMessageContainer");
					wellOperationMenuManager.removeChildNodes(messageContainer);
					messageContainer.innerHTML = message;
					
					// has data, populate to dropdown accordingly
					//if (dataFromServer.hasData == "true") {	
						var selectedIndex = dataFromServer.selectedIndex;
						var dropDownContainer = target + "Container";
						var container = document.getElementById(dropDownContainer);
						wellOperationMenuManager.populateOptions(requestType, dataFromServer.data, dataFromServer.selectedKey, container);				
						
						var targetObj = document.getElementById(target);
						var targetObjForJQuery = $("#" + target);
						
						// selectedIndex doesn't update according to "selected" attribute - manually updating it for now
						if (selectedIndex != null) {
							if (requestType == "campaign" || requestType == "rig") selectedIndex = selectedIndex + 1;
							targetObj.selectedIndex = selectedIndex;							 
						} else if (dataFromServer.size == 1) {
							targetObj.selectedIndex = 0;
						} else {
							targetObj.selectedIndex = -1;
						}
						
						if (selectedIndex != -1 && (dataFromServer.size == "1" || selectedIndex != null)) {
							// if there is only one well/wellbore, straight away query for wellbore/operation
							
							if (requestType == "campaign"){
								wellOperationMenuManager.onClickDropdown(targetObjForJQuery, 'wellDropDown', "well", null);
							}else if (requestType == "rig"){
								wellOperationMenuManager.onClickDropdown(targetObjForJQuery, 'wellDropDown', "wellByRigUid", null);
							}else if (requestType == "well" || requestType == "wellByWellName" || requestType == "wellByWellUid" || requestType == "wellByRigUid") {
								wellOperationMenuManager.onClickDropdown(targetObjForJQuery, 'wellboreDropDown', "wellbore", null);
							} else if (requestType == "wellbore") {
								wellOperationMenuManager.onClickDropdown(targetObjForJQuery, 'operationDropDown', "operation", null);
							} else {
								wellOperationMenuManager.hideLoading();
							}
						} else {
							wellOperationMenuManager.hideLoading();
						}
					//} else {
						//wellOperationMenuManager.hideLoading();
					//}
				},"json");
			}
		},
		
		populateOptions: function(requestType, data, selectedKey, targetObj) {
			var selectString = "";
			
			if (requestType == "campaign" ) {
				selectString = "<select id='campaignDropDown' style='width:100%;' onchange=\"wellOperationMenuManager.onClickDropdown($(this), 'wellDropDown', 'well', null);\">";
			}else if (requestType == "rig" ) {
				selectString = "<select id='rigDropDown' style='width:100%;' onchange=\"wellOperationMenuManager.onClickDropdown($(this), 'wellDropDown', 'wellByRigUid', null);\">";
			}else if (requestType == "well" || requestType == "wellByWellName" || requestType == "wellByWellUid" || requestType == "wellByRigUid") {
				selectString = "<select id='wellDropDown' style='width:100%;' size='9' onclick=\"wellOperationMenuManager.onClickDropdown($(this), 'wellboreDropDown', 'wellbore', null);\">";
			} else if (requestType == "wellbore") {
				selectString = "<select id=\"wellboreDropDown\" style=\"width:100%;\" size=\"4\" onclick=\"wellOperationMenuManager.onClickDropdown($(this), 'operationDropDown', 'operation', null);\">";
			} else if (requestType == "operation") {
				selectString = "<select id=\"operationDropDown\" size=\"6\" style=\"width:100%;\">";
			}			
			
			var optionsHTML = [selectString];
			
			if (requestType == "campaign" || requestType == "rig") {
				optionsHTML.push("<option value='--all_well--'>...</option>");
			}
			
			for (var i=0; i<data.dataList.length; i++) {
				var selected = "";
				var currentData = data.dataList[i];
				
				if (selectedKey == currentData.key) {
					selected = " selected='selected'";
				}
				optionsHTML.push("<option value='" + currentData.key + "'" + selected + ">" + currentData.value + "</option>");
			}
			optionsHTML.push("</select>");
			targetObj.innerHTML = optionsHTML.join("");
			
			$('#operationDropDown').dblclick(function() {
				wellOperationMenuManager.submitWellOperationSelection();
			});
		},
		
		removeUnwantedData: function(requestType) {
			if (requestType == "campaign") {
				var campaignDropDown = document.getElementById("campaignDropDownContainer");
				this.removeChildNodes(campaignDropDown);
				
				var wellDropDown = document.getElementById("wellDropDownContainer");
				this.removeChildNodes(wellDropDown);
				
				var wellboreDropDown = document.getElementById("wellboreDropDownContainer");
				this.removeChildNodes(wellboreDropDown);
			}else if (requestType == "rig") {
				var rigDropDown = document.getElementById("rigDropDownContainer");
				this.removeChildNodes(rigDropDown);
				
				var wellDropDown = document.getElementById("wellDropDownContainer");
				this.removeChildNodes(wellDropDown);
				
				var wellboreDropDown = document.getElementById("wellboreDropDownContainer");
				this.removeChildNodes(wellboreDropDown);
			}else if (requestType == "well" || requestType == "wellByWellName" || requestType == "wellByWellUid" || requestType == "wellByRigUid") {
				var wellDropDown = document.getElementById("wellDropDownContainer");
				this.removeChildNodes(wellDropDown);
				
				var wellboreDropDown = document.getElementById("wellboreDropDownContainer");
				this.removeChildNodes(wellboreDropDown);
			} else if (requestType == "wellbore") {
				var wellboreDropDown = document.getElementById("wellboreDropDownContainer");
				this.removeChildNodes(wellboreDropDown);
			}
			var operationDropDown = document.getElementById("operationDropDownContainer");
			this.removeChildNodes(operationDropDown);
		},
		
		removeChildNodes: function(element) {
			var length = element.childNodes.length;
			if (length > 0) {
				while(element.hasChildNodes()) {
					element.removeChild(element.firstChild);
				}
			}
		},
		
		submitWellOperationSelection: function() {
			var wellUid = $("#wellDropDown").val();
			var wellboreUid = $("#wellboreDropDown").val();
			var operationUid = $("#operationDropDown").val();
			var campaignUid = $("#campaignDropDown").val();
			var separator = "--separator--";
			
			if ((wellUid != null && wellUid != "") && (wellboreUid != null && wellboreUid != "") && (operationUid != null && operationUid != "")) {
				this.showLoading();
				$("#wellOperationUidHolder").val(wellUid + separator + wellboreUid + separator + operationUid);
				document.menuForm.submit();
			} else if ((wellUid == null || wellUid == "") && (wellboreUid == null || wellboreUid == "") && (operationUid == null || operationUid == "") && (campaignUid != null && campaignUid != "")) {
				this.showLoading();
				$("#wellOperationUidHolder").val(campaignUid);
				document.menuForm.submit();
			}
		},
		
		selectCurrentCampaignOperation: function() {
			var campaignUid = $("#campaignDropDown").val();
			if (campaignUid != null && campaignUid != "--all_well--") {
				$("#wellOperationUidHolder").val(campaignUid);
				document.menuForm.submit();
			}
		},
		
		close: function() {
			tb_remove();
		},
		
		showLoading: function() {
			if (!this.isBlock) {
				$("#TB_ajaxContent").block("<div style='border:3px solid #CCCCCC;'>Loading...</div>");
			}
			this.isBlock = true;
		},
		
		hideLoading: function() {
			$("#TB_ajaxContent").unblock();
			this.isBlock = false;
		},
		
		clearSearchBox: function() {
			if (!this.isSearchBoxCleared) {
				var searchBox = $("#welloperation_srch_fld");
				searchBox.val('');
				searchBox.css("color", "#000000");
				
				this.isSearchBoxCleared = true;
			}
		},
		
		searchBoxOnKeyUp: function(e) {
			clearTimeout(this.timeOutId);
			
			if (e.keyCode == 13) {
				wellOperationMenuManager.toggleWellNameSearch();
			} else {
				this.timeOutId = setTimeout("wellOperationMenuManager.toggleWellNameSearch()", 1000);
			}
		},
		
		toggleWellNameSearch: function() {
			var value = $("#welloperation_srch_fld").val();
			
			var messageContainer = document.getElementById("wellOperationMenuMessageContainer");
			messageContainer.innerHTML = "";
			
			if (value != "") {
				this.onClickDropdown(null, 'wellDropDown', 'wellByWellName', value);
			} else {
				this.onClickDropdown('--all_well--', 'wellDropDown', 'well', null);
			}
		}
	};
-->
</script>

<#assign menuInput = " hide" />
<#assign menuLabel = "" />

<#if form.info??>
	<#if ! form.info.isFormValid() >
		<#assign menuInput = "" />
		<#assign menuLabel = " hide" />
	</#if>
</#if>

<#if getGWP(GWPReference.GWP_HIDE_WELL_OPS_BAR) != "1">
	<table id="wellOperationMenu" class="sub-menu-left-table">
		<tr>
			<#assign WELL_OPERATION_MAX_LENGTH = 50 />
			<#if getGWP(GWPReference.GWP_SHOW_WELL_FILTER_MENU) != "0">
				<td><a href="javascript:d2SM.toggleFilter();" title="Filter"><#if !isFiltered()><img src="images/filter.gif" border="0" /><#else><img src="images/filter-red.gif" border="0" /></#if></a></td>
			</#if>
			
			<#if getGWP(GWPReference.GWP_SHOW_OP_FILTER_NAME) = "1">
				<td class="center menuLabel" nowrap>Current Filter:&nbsp;</td>
				<td><label id="opFilterLabel"  onclick="wellOperationMenuManager.showWellOperationMenu()" class="barMenuValue tooltip" title=  "Current applied filter, click to change filter" >${userSession.getCurrentOpFilter()}</label>&nbsp;&nbsp;</td>
			</#if>
			
			<td class="center menuLabel" nowrap>Well/Ops:&nbsp;</td>
			<td>
				<#assign selectedOperationLabel = "Select Operation" />
				<#assign shortLabel = selectedOperationLabel />
				
					<#assign wellOperationMenuHeight = "460"/>
					<input type="hidden" id="wellOperationUidHolder" name="_sys_menu_Operation" value="${_sys_menu.selected_wellOperationMenuKey}" />
					<#if _sys_menu.selected_operationName?has_content && _sys_menu.selected_operationName != "">
						<#assign selectedOperationLabel = _sys_menu.selected_operationName />
					</#if>
					<#if _sys_menu.selected_operationShortName?has_content && _sys_menu.selected_operationShortName != "">
						<#assign shortLabel = _sys_menu.selected_operationShortName />
					</#if>
					<div id="wellOperationModalContent" class="hide">
						<table width="100%" cellpadding="2" cellspacing="2">
							<tr>
								<td colspan="2">
									<table width="100%">
										<tr>
											<#if getGWP(GWPReference.GWP_SHOW_WELL_OP_TYPE_FILTER_MENU) != "0">	
												<#assign showOpCodeFilter = "1"/>
												<#assign containerWidth = "40%"/>
											<#else>
												<#assign showOpCodeFilter = "0"/>
												<#assign containerWidth = "100%"/>
											</#if>	
											<td align="left">
												<span id="welloperationsearch">
													<span class="sbox">
														<input id="welloperation_srch_fld" type="text" onclick="wellOperationMenuManager.clearSearchBox();" onkeyup="wellOperationMenuManager.searchBoxOnKeyUp(event)" value="Search wells..."/>
													</span>
												</span>
											</td>
											<td width="${containerWidth}"><div id="wellOperationMenuMessageContainer" width="100%" style="color:red;"/></td>	
											
											<#if showOpCodeFilter == "1">																	
											<td align="right">
												<#if _sys_menu.campaigns?? && _sys_menu.campaigns?has_content>
													<#assign TargetVal = "campaignDropDown"/>
													<#assign requestTypeVal = "campaign"/>
													<#assign requestVal = ''/>
												<#elseif getGWP(GWPReference.GWP_RIG_FILTER_ON_WELL_OPERATION_MENU) != "0" && _sys_menu.rigs?? && _sys_menu.rigs?has_content>
													<#assign TargetVal = "rigDropDown"/>
													<#assign requestTypeVal = "rig"/>
													<#assign requestVal = ''/>
												<#else>
													<#assign TargetVal = "wellDropDown"/>
													<#assign requestTypeVal = "well"/>
													<#assign requestVal = "'--all_well--'"/>
												</#if>
												<#if _sys_menu.operationCodes?? && _sys_menu.operationCodes?has_content>
												    <#if requestVal =="">
													<select id="operationCodeDropDown" onchange="wellOperationMenuManager.onClickDropdown($(this), '${TargetVal}', '${requestTypeVal}', null);">
													<#else>
													<select id="operationCodeDropDown" onchange="wellOperationMenuManager.onClickDropdown($(this), '${TargetVal}', '${requestTypeVal}', ${requestVal});">
													</#if>
															<option value="--all_well--">...</option>
													<#list _sys_menu.operationCodes as operationCodes>
														<option value="${operationCodes.getOperationCode()}" >${operationCodes.getOperationCodeFullName()}</option>
													</#list>
												</select>												
												</#if>										
											</td>
											</#if>
										</tr>
									</table>
								</td>
							</tr>						
							<#if _sys_menu.campaigns?? && _sys_menu.campaigns?has_content>
								<#assign wellOperationMenuHeight = "520"/>
								<tr>
									<td width="100%">
										<span style="color:#1A5F88; fontSize: 16pt;"><b>Campaign</b></span>
										<div id="campaignDropDownContainer" width="100%">
										<select id="campaignDropDown" style="width:100%;" onchange="wellOperationMenuManager.onClickDropdown($(this), 'wellDropDown', 'well', null);">
											<option value="--all_well--">All Wells</option>
										<#list _sys_menu.campaigns?values as campaign>
											<option value="${campaign.getCampaignUid()}" <#if campaign.getCampaignUid() == _sys_menu.selected_campaignUid!"">selected</#if>>${campaign.getCampaignName()}</option>
										</#list>
										</select>
										</div>
									</td>
									<td style="vertical-align:bottom;">
										<input type="button" id="btnSelectCurrentCampaign" value="Select" onclick="wellOperationMenuManager.selectCurrentCampaignOperation()"/>
									</td>
								</tr>
							</#if>
							<#if getGWP(GWPReference.GWP_RIG_FILTER_ON_WELL_OPERATION_MENU) != "0">
								<#assign wellOperationMenuHeight = "520"/>
								<tr>
									<td colspan="2">
										<div width="100%">
											<table width="100%">
												<tr>
													<td><span style="color:#1A5F88; fontSize: 16pt;"><b>Rig</b></span></td>
												</tr>
											</table>
										</div>
										<div id="rigDropDownContainer" width="100%">
										
										<#if _sys_menu.rigs?? && _sys_menu.rigs?has_content>
											<#assign selectedRigInformationUid = ""/>
											<#if _sys_menu.selected_operation?? && _sys_menu.selected_operation?has_content>
												<#assign selectedRigInformationUid = _sys_menu.selected_operation.getRigInformationUid()/>
											</#if>								
											<select id="rigDropDown" style="width:100%;"  onclick="wellOperationMenuManager.onClickDropdown($(this), 'wellDropDown', 'wellByRigUid', null);">
											<option value="--all_well--">All Rigs</option>
											<#list _sys_menu.rigs as rig>
												<#assign label = rig.getRigName() />
												<option value="${rig.getRigInformationUid()}" <#if rig.getRigInformationUid() == (selectedRigInformationUid!"")>selected</#if>>${label}</option>
											</#list>
											</select>
										</#if>
										</div>
									</td>
								</tr>
							</#if>
							<tr>
								<td colspan="2">
									<div width="100%">
										<table width="100%">
											<tr>
												<td><span style="color:#1A5F88; fontSize: 16pt;"><b>Well</b></span></td>
											</tr>
										</table>
									</div>
									<div id="wellDropDownContainer" width="100%">
									<#if getGWP(GWPReference.GWP_WELL_OPERATION_MENU_POPUP_LAZY_LOADING) != "1" && _sys_menu.wells?? && _sys_menu.wells?has_content>
										<select id="wellDropDown" style="width:100%;" size="9" onclick="wellOperationMenuManager.onClickDropdown($(this), 'wellboreDropDown', 'wellbore', null);">
										<#list _sys_menu.wells as well>
											<#assign label = well.getWellName() />
											<option value="${well.getWellUid()}" <#if well.getWellUid() == _sys_menu.selected_wellUid!"">selected</#if>>${label}</option>
										</#list>
										</select>
									</#if>
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<span style="color:#1A5F88; fontSize: 16pt;"><b>Wellbore</b></span><br/>
									<div id="wellboreDropDownContainer" width="100%">
									<#if getGWP(GWPReference.GWP_WELL_OPERATION_MENU_POPUP_LAZY_LOADING) != "1" && _sys_menu.wellbores?? && _sys_menu.wellbores?has_content>
										<select id="wellboreDropDown" style="width:100%;" size="4" onclick="wellOperationMenuManager.onClickDropdown($(this), 'operationDropDown', 'operation', null);">
										<#list _sys_menu.wellbores as wellbore>
											<#assign label = wellbore.getWellboreName() />
											<option value="${wellbore.getWellboreUid()}" <#if wellbore.getWellboreUid() == _sys_menu.selected_wellboreUid!"">selected</#if>>${label}</option>
										</#list>
										</select>
									</#if>
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<span style="color:#1A5F88; fontSize: 16pt;"><b>Operation</b></span><br/>
									<div id="operationDropDownContainer" width="100%">
									<#if getGWP(GWPReference.GWP_WELL_OPERATION_MENU_POPUP_LAZY_LOADING) != "1" && _sys_menu.operations?? && _sys_menu.operations?has_content>
										<select id="operationDropDown" size="6" style="width:100%;">
										<#list _sys_menu.operations as operationDescriptor>
											<#assign operation = operationDescriptor.getOperation() />
											
											<#assign startDateAndLastDateLabel = "" />
											<#if _sys_menu.operationNameWithStartAndEndDateEnabled!"" == "true">
												<#if operationDescriptor.getFormattedStartDate()?has_content && operationDescriptor.getFormattedLastDate()?has_content>
													<#assign startDateAndLastDateLabel = " (" + operationDescriptor.getFormattedStartDate() + " - " + operationDescriptor.getFormattedLastDate() + ")" />
												<#elseif operationDescriptor.getFormattedStartDate()?has_content>
													<#assign startDateAndLastDateLabel = " (" + operationDescriptor.getFormattedStartDate() + ")" />
												<#elseif operationDescriptor.getFormattedLastDate()?has_content>
													<#assign startDateAndLastDateLabel = " (" + operationDescriptor.getFormattedLastDate() + ")" />
												</#if>
											</#if>
											<#assign label = operation.getOperationName() + startDateAndLastDateLabel/>
											<option value="${operation.getOperationUid()}" <#if operation.getOperationUid() == _sys_menu.selected_operationUid!"">selected</#if>>${label}</option>
										</#list>
										</select>
									</#if>
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="center">
									<input type="button" value="Confirm" onclick="wellOperationMenuManager.submitWellOperationSelection()" />&nbsp;or&nbsp;<a class="blue-link" href="javascript:void(0);" onclick="wellOperationMenuManager.close();">Cancel</a>
								</td>
							</tr>
						</table>
					</div>
					<div id="OperationSelectorModalContent" class="hide">
						<#if getGWP(GWPReference.GWP_OPERATION_SELECTOR) == "1">
							<iframe style="padding-top:0px;padding-bottom:0px;padding-right:0px;height:514px;width:659px" src="flash/visnet/flash/virtualpath/d2_visnet_operation_selector.start" scrolling=NO>
								<p> Flash failed to load</p>
							</iframe>
						</#if>
					</div>
					<label id="operationLabel" style="color:orange;" class="pointer tooltip" title="${selectedOperationLabel?html} <br> Click to change Well/Operation" onclick="wellOperationMenuManager.showWellOperationMenu()">${shortLabel}</label>&nbsp;&nbsp;
					<a id="wellOperationMenuPopup" class="thickbox hide" title="Well Operation Menu" href="&#TB_inline?inlineId=wellOperationModalContent&height=${wellOperationMenuHeight}&width=550&modal=true"></a>
					<#if getGWP(GWPReference.GWP_OPERATION_SELECTOR) == "1">
						<a id="OperationSelectorMenuPopup" class="thickbox hide" title="Operation Selector" href="&#TB_inline?inlineId=OperationSelectorModalContent&height=523&width=634&modal=true"></a>
					</#if>
				
			</td>
			<td class="center menuLabel">&nbsp;Day:&nbsp;</td>
			<#if _sys_menu.prev_day?? && _sys_menu.prev_day?has_content>
				<td class="pointer tooltip" onclick="d2ScreenManager.submitSummary('${_sys_menu.prev_day}')" title="go to the PREVIOUS day"><img src="images/prev.gif" /></td>
			</#if>
			<td nowrap class="align-right">
				<select id="_sys_menu_daily" class="barMenuDropdown${menuInput}" name="_sys_menu_Daily" onchange="javascript:onMenuWellDaySelect($(this));" selectedValue="${_sys_menu.selected_dailyUid?default('')}">
					<option value="">Select Day</option> <#-- this value must be blank, ssjong -->
					<#if _sys_menu.days?exists>
						<#list _sys_menu.days as menu_day>
							<option value="${menu_day.dailyUid}" <#if "${menu_day.dailyUid}" = "${_sys_menu.selected_dailyUid?default('')}">selected<#assign _menu_dayLabel = menu_day.label /></#if>>${menu_day.label}</option>
						</#list>
					</#if>
					<#if _sys_menu.selected_operationUid?has_content && _sys_menu.selected_operationUid != "" && _sys_menu.allowCreateNewDay =="1">
						<option value="createNewDate" class="create-new-date">[Add New Day]</option>
					</#if>
				</select>
				<label id="dayLabel" class="pointer barMenuValue${menuLabel}" onclick="d2ScreenManager.showDropDown()">${_menu_dayLabel?default('Select Day')}</label>&nbsp;&nbsp;
			</td>
			<#if _sys_menu.next_day?? && _sys_menu.next_day?has_content>
				<td class="pointer tooltip" onclick="d2ScreenManager.submitSummary('${_sys_menu.next_day}')" title="go to the NEXT day"><img src="images/next.gif" /></td>
			</#if>
			<td class="center menuLabel">&nbsp;Datum:&nbsp;</td>
			<td>
				<#if _sys_menu.selected_uomDatumUid?? && _sys_menu.selected_uomDatumUid?has_content>
					<select id="datumValue" name="_sys_menu_UOMDatum" class="barMenuDropdown hide" onchange="javascript:onMenuDatumChange($(this), '${_sys_menu.selected_uomDatumUid}');">
				<#else>
					<select id="datumValue" name="_sys_menu_UOMDatum" class="barMenuDropdown hide" onchange="javascript:onMenuDatumChange($(this), '');">
				</#if>	
					<option value="">MSL</option>
					<#if _sys_menu.uom_datums?exists>
						<#list _sys_menu.uom_datums as uom_datum>
							<option value="${uom_datum.opsDatumUid}" <#if uom_datum.opsDatumUid = _sys_menu.selected_uomDatumUid?default("")>selected <#assign _menu_uomDatumLabel = uom_datum.displayName/></#if>>${uom_datum.displayName}</option>
						</#list>
					</#if>
				</select>
				<label id="datumLabel" style="<#if ! userSession.isDefaultDatumSelected()>color:#CCCCCC</#if>" class="pointer barMenuValue" onclick="d2ScreenManager.showDropDown()">${_menu_uomDatumLabel?default("MSL")}</label>
				
				<#if _sys_menu.no_of_uom &gt; 1>
					<span class="center menuLabel">&nbsp;UOM:&nbsp;</span>
					<select id="uomTemplateValue" name="_sys_menu_UOMTemplate" class="barMenuDropdown hide" onchange="javascript:onMenuUOMChange();">
						<option value="">Select Template</option>
						<#if _sys_menu.uom_templates?exists>
							<#list _sys_menu.uom_templates as uom_template>
								<option value="${uom_template.uomTemplateUid}" <#if uom_template.uomTemplateUid = _sys_menu.selected_uomTemplateUid?default('')>selected="1" <#assign _menu_uomTemplateLabel = uom_template.name/></#if>>${uom_template.name}</option>
							</#list>
						</#if>
					</select>
					<label id="uomTemplateLabel" class="pointer barMenuValue" onclick="d2ScreenManager.showDropDown()">${_menu_uomTemplateLabel?default("[none]")}</label>
				</#if>
				
			</td>
		</tr>
	</table>
</#if>