<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="overflow:hidden; height:100%; width:100%">
<#include "/templateHelper.ftl"/>
<#include "/core/d2Helper.ftl"/>

<head>
	<#if ClientBrowserUtil.isIE()>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	</#if>
	<#if form.redirectUrl?has_content>
		<meta http-equiv="refresh" content="0;url=${form.redirectUrl}">
	</#if>
	<script type="text/javascript">
		_tab_layout_logic_version = 1;
	</script>
	<#include "/defaultJavascriptAndStyleTemplate.ftl" />
	<#if form.redirectUrl?has_content>
		<script type="text/javascript">
		<!--
			$(document).ready(function(){
				d2ScreenManager.showSimpleModalMsg("Redirection", "${form.redirectMessage!"Redirecting, please wait"}...");
				checkConnectivity();
			});
		-->
		</script>
	</#if>
	<style type="text/css">
		.ui-tabs-container {
			margin-top: 0px !important;
		}
	</style>
	<#if form.info?exists>
		<#assign currentTabMode = form.info.getCurrentTabMode()!"" />
		<#if form.info.isDirty() && disableMenuWhenFormIsDirty() && currentTabMode == "data" && !form.info.isFlexClientInUse()>
			<#if form.info.isFormValid()>
			<script type="text/javascript">
			<!--
				$(document).ready(function(){
					d2ScreenManager.toggleMainBlockUI();
					
					
				});
			-->
			</script>
			</#if>
		<#elseif currentTabMode == "ie">
			<script type="text/javascript">
			<!--
				$(document).ready(function(){
					d2ScreenManager.toggleImportExportBlockUI();
				});
			-->
			</script>
		<#elseif currentTabMode == "report">
			<script type="text/javascript">
			<!--
				$(document).ready(function() {
					var report_tab = $("#reportMainTab");
					if(report_tab != undefined) {
						var url = report_tab.attr("dynamicTabUrl");
						if(url){
							var report_tab_iframe = $("#reportsTab").find("iframe");
							report_tab_iframe.attr("src",url);
						}
					}
				});
			-->
			</script>
		<#elseif currentTabMode == "filemanager">
			<script type="text/javascript">
			<!--
				$(document).ready(function() {
					var fm_tab = $("#fileManagerTabHeader");
					if(fm_tab != undefined) {
						var url = fm_tab.attr("dynamicTabUrl");
						if(url){
							var fm_tab_iframe = $("#fileManagerTab").find("iframe");
							fm_tab_iframe.attr("src",url);
						}
					}
				});
			-->
			</script>
		<#else>
			<script type="text/javascript">
			<!--
				$(document).ready(function() {
 					var currentTabMode = "${currentTabMode}";
					var tabHeader = $("#${currentTabMode}TabHeader");
 					if(tabHeader != undefined) {
  						var url = tabHeader.attr("dynamicTabUrl");
  						if(url){
   							var iframe = $("#${currentTabMode}Tab").find("iframe");
   							iframe.attr("src",url);
  						}
 					}
				});
			-->
			</script>
		</#if>
	</#if>
	
	<#assign custom_view_flash_url = form.info.getCustomView("flash")?default("")/>

	<@generateCascadeLookupScript />
	
	<#if getGWP(GWPReference.GWP_TOWN_SERVER_REACHABILITY_INDICATOR) == "1" && _sys_menu.sendToTownDestinationUrl?default("") != "">
		<script type="text/javascript">
		<!--
			$(document).ready(function() {
				checkReachability();
			});
			
			function checkReachability() {
				$.get("webservice/checkreachabilityservice.html", {}, function(data, textStatus) {
					var img = $("#reachability-indicator");
					if (textStatus == "success" && data.status == "success") {
						if (data.pendingSendToTownRequestsCount != "") {
							img.attr("src", "images/server-active-queue.gif");
						} else {
							img.attr("src", "images/server-active.gif");
						}
						img.attr("title", "Town server is online");
					} else {
						if (data.pendingSendToTownRequestsCount != "") {
							img.attr("src", "images/server-down-queue.gif");
						} else {
							img.attr("src", "images/server-down.gif");
						}
						img.attr("title", "Town server is unreachable: " + data.message);
					}
					$("#pending-send-to-town-requests-count").text(data.pendingSendToTownRequestsCount);
				}, "json");
				setTimeout("checkReachability()", 30000);
			}
		-->
		</script>
	</#if>
	
	<script type="text/javascript">
		<!--
			$(document).ready(function(){
				setTabsHeight();
				d2ScreenManager.setTabsContentHeightFunction = setTabsHeight;
				d2SystemCheck();
			});
			
			function setTabsHeight(){
				var tabsContentDiv = $("#tabsContentDiv");
				if(tabsContentDiv.length > 0){
					var contentTop = $("#tabsContentTop");
					var newHeight = ($(window).height() - contentTop.position().top);
					tabsContentDiv.css("height", newHeight + "px"); 
					tabsContentDiv.css("display", "block");
					<#if custom_view_flash_url == "">
						var dataTabScrollable = tabsContentDiv.find("#dataTabScrollable");
						if(dataTabScrollable.length > 0){
							var bottomDivHeight = 0;
							var bottomDiv = tabsContentDiv.find("#dataTabBottomDiv");
							if(bottomDiv.length > 0 && bottomDiv.get(0).childNodes && bottomDiv.get(0).childNodes.length > 0){
								bottomDivHeight = bottomDiv.outerHeight();
							}
							dataTabScrollable.css("height", (newHeight - dataTabScrollable.position().top - bottomDivHeight) + "px");
						}
					<#else>
						var bottomDiv = tabsContentDiv.find("#dataTabBottomDiv");
						if(bottomDiv.length > 0 && bottomDiv.get(0).childNodes && bottomDiv.get(0).childNodes.length > 0){
							var mainDataFrame = tabsContentDiv.find("#mainDataFrame");
							mainDataFrame.css("height", (newHeight - bottomDiv.outerHeight()) + "px");
						}
					</#if>
				}
			}
		
			function d2SystemCheck(endTime) {
				var dtNow = new Date();
				if(endTime == undefined){
					endTime = dtNow.getTime() + (${userSession.getMaxSessionTimeoutHours()} * 60 * 60 * 1000);
					setTimeout("d2SystemCheck(" + endTime + ")",1000);
				}else{
					var keepalive = (dtNow.getTime() < endTime ? "&alive=1" : "");
					$.post("${userSession.getApplicationContextPath()}/session_validity_check?v=2&checkAutoUpdate=1" + keepalive, null, function(response){
						if(response["autoUpdate_standby"] == "1"){
							window.location = "${userSession.getApplicationContextPath()}/logout.jsp";
						}else if(response["autoUpdate_showCountDown"] == "1"){
							var countdown = d2ScreenManager.autoUpdateCountdown;
							if(countdown == null){
								countdown = {};
								d2ScreenManager.autoUpdateCountdown = countdown;
								countdown.popup = $("#autoUpdateCountdown");
								countdown.title = $("#autoUpdateCountdownTitle", countdown.popup);
								countdown.message = $("#autoUpdateCountdownMessage", countdown.popup); 
							}
							countdown.title.text("Warning: System shutting down soon for maintenance!");
							if(response["autoUpdate_timeLeave"] == ""){
								countdown.message.text("System will be shutting down for maintenance now, please logout from the application.");
							}else{
								countdown.message.text("System will be shutting down for maintenance in " + response["autoUpdate_timeLeave"] + ", please save your work and logout from the application.");
							}
							$.blockUI({message:countdown.popup, css:{border:"2px solid red"}});
						}
					},"json");
					setTimeout("d2SystemCheck(" + endTime + ")",60000);
				}
			}
			
			function hideAutoUpdateCountdown()
			{
				$.unblockUI();
			}
			
			function launchFileManagerFrame(url, myWindow){
				window.popupFileManagerWindow = myWindow;
				createUploadFrame();
				var frame = window.parent.document.getElementById("iframeContainer");
				frame.setAttribute("src", url.url);
				frame.style.width = "760px";
				frame.style.height = "540px";
				$("#FileManagerPopup").click();
			}
			
			function createUploadFrame(){
				var fileManagerUploadContainer = window.parent.document.getElementById("fileManagerUploadContainer");
				if (!fileManagerUploadContainer){
					fileManagerUploadContainer = window.parent.document.createElement("div");
					fileManagerUploadContainer.id="fileManagerUploadContainer";
					fileManagerUploadContainer.className="hide";
					fileManagerUploadContainer.style.height="600px";
					fileManagerUploadContainer.style.width="800px";
					window.parent.document.body.appendChild(fileManagerUploadContainer);
					
					var iframeContainer = window.parent.document.createElement("iframe");
					iframeContainer.id="iframeContainer";
					
					input = document.createElement("input");
					input.type = "button";
					input.value="Close";
					input.onclick = function() {
				       tb_remove();
				       modal=false;
				    };
					
					table = document.createElement("table");
					table.id="test";
					table.style.marginLeft="30px";
					table.align="center";
					tr1 = document.createElement("tr");
					td1 = document.createElement("td");
					td1.align = "center";
					td1.appendChild(input);
					tr1.appendChild(td1);
					table.appendChild(tr1);
					
					tr2 = document.createElement("tr");
					td2 = document.createElement("td");
					td2.align = "center";
					td2.appendChild(iframeContainer);
					tr2.appendChild(td2);
					table.appendChild(tr2);
					
					fileManagerUploadContainer.appendChild(table);
				}	
			}
		-->
	</script>
</head>

<body style="overflow:hidden; border:0; margin:0; height:100%; width:100%; padding:0" onResize="setTabsHeight()">
	<div id="templateImportPopup" style="display:none">
		<table border="0" width="100%" height="100%" style="background-color:#CCCCFF">
			<tr><td align="center">
				<table border="0" cellpadding="3">
					<tr><td align="left">Paste your data here:</td></tr>
					<tr><td><textarea cols="100" rows="10" templateImport="source"></textarea></td></tr>
					<tr><td align="left"><a href="javascript: d2ScreenManager.templateImportDownload();" templateImport="templateLink">You may download the template file here</a></td></tr>
					<tr><td align="center"><div class="buttons" style="padding-top:8pt"><input type="button" value="OK" style="width:50pt" onclick="d2ScreenManager.templateImportSubmit()"/><input type="button" value="Cancel" style="width:50pt" onclick="tb_remove();"/></div></td></tr>
				</table>
			</td></tr>
		</table>
	</div>
	
	<div>
		<a id="FileManagerPopup" class="thickbox hide" title="Files" href="&#TB_inline?inlineId=fileManagerUploadContainer&height=600&width=800&modal=true&keepThis=true"></a>
	</div>
	
	<@systemMessage />

	<#if ! __d3ShowOldHtmlDataTab??>
	<div id="topMenuWrapperBlocker" style="display:none; position:absolute; left:0; height:30px; width:100%; background-color:#FFFFFF; opacity:0.8; filter:alpha(opacity=80); cursor:wait; z-index:2000"></div> 
	<div id="topMenuWrapper" border="1" style="height:30px">
		<ul id="top-right-menu" class="top-right-menu">
			<li><a class="white-link" href="javascript:d2ScreenManager.toggleLogout();">${form.info.getLabel("label.link.logout")}</a></li>
			<#--		
			<li class="pipe">|</li>
			<li><a class="white-link" href="dvdgraph.html">DvD Graph</a></li>
			-->
			<li class="pipe">|</li>
			<li><a class="white-link" href="javascript:d2ScreenManager.submitRefresh();">${form.info.getLabel("label.link.refresh")}</a></li>
			<#--
			<li><div><a href="http://www.idsdata.net" target="_blank" style="text-decoration:none" ><img src="images/datanet_logo_small.png" border="0" /></a></div></li>
			-->
			<li class="pipe">|</li>
			<li><a class="white-link" href="http://sg.idsdatanet.com/helpfiles/" target="_blank" >${form.info.getLabel("label.link.help")}</a></li>
			<#if getGWP(GWPReference.GWP_TOWN_SERVER_REACHABILITY_INDICATOR) == "1" && _sys_menu.sendToTownDestinationUrl?default("") != "">
				<li style="padding: 1px 7px 1px 1px;"><img id="reachability-indicator" src="images/server-down.gif" border="0"/></li>
				<li><span id="pending-send-to-town-requests-count" title="Pending Send To Town Requests" style="color: #ccc"></span></li>
			</#if>
			<#if _sys_menu.remainingDaysAllowedToRunWithoutDongle &gt; 0>
				<li style="background-color: white;color: red;" class="tooltip" title="This application will be blocked after ${_sys_menu.remainingDaysAllowedToRunWithoutDongle} day(s).">WARNING: IDS2Go not found.</li>
			</#if>
		</ul>
	</div>
	</#if>
	<#--
	<div id="navigationHistoryTrend">
		<#if _sys_menu.user_navigation_history??>
			<ul>
			<#assign user_navigation_separator = "" />
			<#list _sys_menu.user_navigation_history.getHistoryList() as history>
				<#if history?has_content>
					<#assign historyValue = history?split("__value_separator__") />
					<li onmouseover="d2SM.toggleUserNavigationMouseOver();" onmouseout="d2SM.toggleUserNavigationMouseOut();">${user_navigation_separator}<a href="${historyValue[1]}">${historyValue[0]}</a></li>
					<#assign user_navigation_separator = "|&nbsp;" />
				</#if>
			</#list>
			<li><img id="clearUserNavigationHistory" src="images/trash.gif" class="pointer tooltip hide" title="Clear All" onmouseover="d2SM.toggleUserNavigationMouseOver();" onmouseout="d2SM.toggleUserNavigationMouseOut();" onclick="d2SM.clearUserNavigationHistory();"></li>
			</ul>
		</#if>
	</div>
	-->

	<#if ! __d3ShowOldHtmlDataTab??>
	<#if form.info.getCustomViewProperty("accessDenied")?default("false") == "true">
		<div id="accessDenied">
			<h2>Access denied</h2>
			<p>We apologize, but you do not have permission to access this page.</p>
		</div>
	<#else>
		<#if getGWP(GWPReference.GWP_SHOW_WELL_FILTER_MENU) != "0">
			<div id="filterMenu" isShow="false">
				<div id="filterContent">
					<form id="filterForm" name="filterForm" method="post" action="${springMacroRequestContext.getRequestUri()}${.globals.urlParam}">
						<div id="filterOptions"><a href="javascript:d2SM.toggleFilter();" class="filter-close"><img src="images/cross.gif" border="0" /></a></div>
						<table id="filterMenuTable">
							<tr>
							<#if getGWP(GWPReference.GWP_WELL_QUERY_FILTER_ENABLED) == "1">
								<td>Filter:&nbsp;</td>
								<td>
									<#include "/queryFilters.ftl" />
								</td>
							<#else>
								<td>Field:&nbsp;</td>
								<td>
									<@showField inputName="_sys_menu_filter_field" node=form.root editmode=true dynamic="@sysFieldMenu" defaultValue=_sys_menu.selected_filter_field!"" />
								</td>
								<td>Rig:&nbsp;</td>
								<td>
									<@showField inputName="_sys_menu_filter_rig" node=form.root editmode=true dynamic="@sysRigMenu" defaultValue=_sys_menu.selected_filter_rig!"" />
								</td>
								<td>Operation:&nbsp;</td>
								<td>
									<@showField inputName="_sys_menu_filter_ops_type" node=form.root editmode=true dynamic="@sysOpsTypeMenu" defaultValue=_sys_menu.selected_filter_opsType!"" />
								</td>
								<td>State:&nbsp;</td>
								<td>
									<@showField inputName="_sys_menu_filter_state" node=form.root editmode=true dynamic="@sysStateMenu" defaultValue=_sys_menu.selected_filter_state!"" />
								</td>
								<td>Work Group:&nbsp;</td>
								<td>
									<@showField inputName="_sys_menu_filter_workinggroup" node=form.root editmode=true dynamic="@sysWorkinggroupMenu" defaultValue=_sys_menu.selected_filter_workinggroup!"" />
								</td>
							<!-- // asked to be removed by dlee -->
							<#--
								<td>Well:&nbsp;</td>
								<td>
									<@showField inputName="_sys_menu_filter_well" node=form.root editmode=true dynamic="@sysWellMenu" defaultValue=_sys_menu.selected_filter_wellUid!"" />
								</td>
								<td>Wellbore:&nbsp;</td>
								<td>
									<@showField inputName="_sys_menu_filter_wellbore" node=form.root editmode=true dynamic="@sysWellboreMenu" defaultValue=_sys_menu.selected_filter_wellboreUid!"" />
								</td>
							-->
							</#if>
								<td class="buttons">
									<input type="submit" value="Run Filter" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</#if>
		
		<div id="topContainerWrapper">
			<div class="sub-menu">
				<table>
					<tr>
						<td class="left">
							<#-- kwong (24 Sep 08): replace following line due to the url param is missing when clicking the link in summary table -->
							<#-- <form id="menuForm" name="menuForm" method="post" action="${springMacroRequestContext.getRequestUri()}${.globals.urlParam}"> -->
							<form id="menuForm" name="menuForm" method="post">
								<input type="hidden" id="currentViewHolder" name="_sys_menu_view_holder" value="${_sys_menu.selected_view}" />
								<#if _sys_menu.selected_view == ModuleConstants.WELL_MANAGEMENT>
									<#include "/wellOperationMenu.ftl" />
								<#elseif _sys_menu.selected_view == ModuleConstants.TOUR_MANAGEMENT>
									<#include "/tourOperationMenu.ftl" />
								<#elseif _sys_menu.selected_view == 'Site Management'>
									<#include "/siteOperationMenu.ftl" />
								</#if>
							</form>
						</td>
						<#--
						<td nowrap="nowrap">
							<div style="margin-right: 10px;">
								<#include "../search/advancedSearch.ftl"/>
								<a class="pointer thickbox white-link tooltip" title="Click for Advanced Search" href="&#TB_inline?inlineId=advancedSearchModalContent&height=550&width=775&modal=true">Search</a>
							</div>
						</td>
						<td class="right-menu vertical-top">
							<div id="searchWrapper">
								<form action="search.html${.globals.urlParam}" method="post">
								<div id="applesearch">
									<span class="sbox">
										<input type='hidden' name='__mainformsubmission' value='1' />
										<#if form.query??>
											<#assign query = springMacroRequestContext.getBindStatus("form.query").value!"" />
											<input type='hidden' name='prevUrl' value='${form.prevUrl!""}' />
											<input type='hidden' name='prevTitle' value='${form.prevTitle!""}' />
										<#else>
											<input type='hidden' name='prevUrl' value='${springMacroRequestContext.getRequestUri()}${.globals.urlParam}' />
											<input type='hidden' name='prevTitle' value='${getFormTitle("IDS Datanet")}' />
										</#if>
										<input id="srch_fld" type="search" placeholder="Search..." onkeyup="d2ScreenManager.toggleAppleSearch();" autosave="bsn_srch" results="5" name="query" value="${(query!"")?html}"/>
									</span>
								</div>
								</form>
							</div>
						</td>
						-->
					</tr>
				</table>
			</div>
		</div>

		<div style="width:100%">
			<@mainTabs />
		</div>
		
		<#assign currentTabMode = form.info.getCurrentTabMode()!"" />
		<#assign contentWrapperClass = form.info.getCustomViewProperty("DefaulTemplate.contentWrapper")!"" />
		<div id="tabsContentTop" style="font-size:0; height:1px; width:1px"></div>
		<div id="tabsContentDiv" style="display:none; position:relative; height:500px; width:100%; padding:0; border:0; margin:0">		
			<#if custom_view_flash_url != "">
				<div id="flashTab" style="height:100%; width:100%; border:0; padding:0; margin:0;" class="ui-tabs-container <#if currentTabMode != "" && currentTabMode != "data">ui-tabs-hide</#if>">
					<iframe id="mainDataFrame" frameborder="0" style="padding:0; border:0; margin:0; height:100%; width:100%" src="${custom_view_flash_url}"></iframe>
					<div id="dataTabBottomDiv" style="width:100%; border:0; padding:0; margin:0">
						<@dataSummary />
					</div>
				</div>
			<#else>
				<div id="dataTab" style="width:100%; border:0; padding:0; margin:0" class="ui-tabs-container data <#if currentTabMode != "" && currentTabMode != "data">ui-tabs-hide</#if>">
					<@writeHtmlDataTabContent/>
				</div>
			</#if>
			
			<@mainTabsContent true/>
		</div>
	</#if>
	<#else>
		<#if form.info.getCustomViewProperty("accessDenied")?default("false") == "true">
			<div id="accessDenied">
				<h2>Access denied</h2>
				<p>We apologize, but you do not have permission to access this page.</p>
			</div>
		<#else>
			<@writeHtmlDataTabContent/>
		</#if>
	</#if>
	
	<@sysRefreshForm/>
	<@sysSortForm />
	
	<#if !form.info.isFlexClientInUse()>
		<@writePaginationForm/>	
	</#if>
	
	<#--
	<div class="footer">
		<div><a href="http://www.idsdatanet.com" title="IDS DataNet">&copy; 2008 Independent Data Services (Global) Pte. Ltd.</a></div>
		<#if userSession??>
			Current User: ${userSession.getCurrentUserName()}
		</#if>
		&nbsp;
		[<a href="mailto:support@idsdatanet.com?subject=IDS Support Required (${userSession.getClientBaseUrl()!userSession.getCurrentGroupUid()})" title="E-mail IDS Support">E-mail IDS Support</a>]
	</div>
	-->
	
	<!-- NPD Report (jman) -->
	<a id="npdPreview" class="thickbox hide" href="" npdAction="preview" targetUrl="${springMacroRequestContext.getRequestUri()}?_invokeCustomFilter=__ie.npd.export.customFilter&height=600&width=800&dailyUid=${_sys_menu.selected_dailyUid?default('')}&objType=drillreport&modal=true"></a>
	<a id="npdSendFile" class="thickbox hide" href="" npdAction="send" targetUrl="${springMacroRequestContext.getRequestUri()}?_invokeCustomFilter=__ie.npd.export.customFilter&height=600&width=800&dailyUid=${_sys_menu.selected_dailyUid?default('')}&objType=drillreport&modal=true"></a>
	<form id="npdDownloadForm" name="npdForm" action="${springMacroRequestContext.getRequestUri()}" method="post">
		<input type="hidden" name="action" value="download" />
		<input type="hidden" name="_invokeCustomFilter" value="__ie.npd.export.customFilter" />
		<input type="hidden" name="__mainformsubmission" value="1" />
		<input id="npdService" type="hidden" name="service" value="" />
		<input id="npdVersionKind" type="hidden" name="versionKind" value="" />
	</form>
	<!-- End NPD Report -->
	
	<#if ! __d3ShowOldHtmlDataTab??>
		<#assign _menu_bar_offset_top = 4/>
		<#include "/drillnet/createNewDate.ftl" />
		<#include "/milonic.ftl"/>
	</#if>
	<div id="autoUpdateCountdown" style="display:none; padding-bottom: 12px">
		<div id="autoUpdateCountdownTitle" style="background-color: #FCCDCD; font-weight:bold; padding:6px; text-align:left">
		</div>
		<div id="autoUpdateCountdownMessage"style="padding: 15px; text-align:left">
		</div>
		<button onclick="hideAutoUpdateCountdown()">Close</button>
	</div>
</body>
</html>

<@setCurrentPageStateInJavascript/>

<#macro writeHtmlDataTabContent>
	<#if form.info.isFormValid()>
		<form name="form" id="form" method=post onsubmit="return false;" action="${springMacroRequestContext.getRequestUri()}">
			<#if form.info.getCustomViewProperty("disableRootButtonTable")?default("") != "true">
				<@showRootButtonsTable/>
			</#if>
			<div id="dataTabScrollable" style="overflow:scroll; padding:0; margin:0; border:0; height:500px; width:100%">
				<a name="toppage" href=""></a>
				<#if form??>
					<#if form.info.getCustomView("data")??>
						<#include form.info.getCustomView("data")/>
					</#if>
				</#if>
				<a name="bottompage" href=""></a>
			</div>
			<#--
			<#assign upDownArrowDisabled = form.info.getCustomViewProperty("upDownArrow.disabled")!"" />
			<#if upDownArrowDisabled != "true">
				<div>
					<table class="root-action-bottom">
						<tr>
							<td class="tdRight" colspan="2">
								<div align="right"><a href="#bottompage"><img src="images/arrow-down.gif" border="0" /></a></div>
							</td>
						</tr>
					</table>
				</div>
			</#if>
			-->
			<div id="dataTabBottomDiv" style="width:100%; border:0; padding:0; margin:0">
				<#if !form.info.isFlexClientInUse()>
					<@pagination layoutLogic="DefaultTemplate_V2"/>
					<@dataSummary />
				</#if>
			</div>
		</form>
	</#if>
</#macro>