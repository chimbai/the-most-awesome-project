<html>
<body>
Dear Sir/Madam, 
<p></p>
<p>
	<b>Country:</b> ${country}<br>
	<b>Rig:</b> ${rigName}
</p>
<p><u><b>Operation: ${operationName} (Day#${reportNumber} - ${reportDatetime})</b></u></p>
<p>
	<b>Summary of Period 0000 to 2400:</b> ${reportPeriodSummary}<br>
	<b>Status at 0600:</b> ${reportCurrentStatus}<br>
	<b>Planned Operations Summary:</b> ${reportPlanSummary}<br>
	<#--
	Ticket: 20130207-1937-kllau. If client requests to display the following fields in the scheduled custom mail, please add the following accordingly ===
	- operation name : ${operationName}
	- day # : ${reportNumber}
	- rig name : ${rigName}
	- afe amount : ${afe}
	- spud date : ${spudDate}
	- dsv : ${snrdrillsupervisor}
	- cost to date : ${cumCost}
	- days from spud : ${daysFromSpud}
	-->

</p>
<#if hseDetails?exists>
	<p>
		<table border="1">
			<tr>
				<th>Events</th>
				<th>Num. Events</th>
				<th>Date of Last</th>
				<th>Days Since</th>
				<th>Comments</th>
			</tr>
			
			<#list hseDetails?keys as hseUid>
				<tr>
					<td>${hseDetails[hseUid]["incidentCategory"]}</td>
					<td>${hseDetails[hseUid]["numberOfIncidents"]}</td>
					<td>${hseDetails[hseUid]["hseEventDatetime"]}</td>
					<td>${hseDetails[hseUid]["daysLapsed"]}</td>
					<td>${hseDetails[hseUid]["hseShortDescription"]}</td>
				</tr>
			</#list>
		</table>
	</p>
</#if>
-----------------------------------------
<p>This is an automated message.</p>
<p>Please do not reply to the address above.</p> 
<p>Thank you.</p>
<p><b>IDS Datanet Service</b></p>
</body>
</html>