package com.idsdatanet.d2.common.util.importtemplate;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.common.util.ImportUtil;
import com.idsdatanet.d2.common.util.RegexUtil;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class OceanApexBcoColumnBasedImportTemplate extends ExcelImportTemplate {

    private Integer columnRow = null;
    private String columnRange = null;

    @Override
    public List<Object> parseTemplate(File excelFile, BaseCommandBean commandBean) throws Exception {
        List<Object> objectList = null;
        List<ExcelMapping> mappingList = this.excelMappingList;
        InputStream inputStream = new FileInputStream(excelFile);
        this.book = WorkbookFactory.create(inputStream);
        this.evaluator = book.getCreationHelper().createFormulaEvaluator();

        try {
            Sheet sheet = book.getSheetAt(0);
            if (sheet != null) {
                objectList = new ArrayList<>();
                String[] range = columnRange.split("-");
                for (int i = Integer.parseInt(range[0]); i <= Integer.parseInt(range[1]); i++) {
                    Object object = ImportUtil.getNewObject(this.className);
                    if (this.listener != null) { // BeforeNodeCreate
                        object = this.listener.beforeNodeCreate(object, commandBean, this, objectList, null);
                    }
                    object = this.parseColumn(i, object, sheet, this.className, mappingList, this.columnRow, this.columnRange, commandBean, objectList);
                    if (object != null) {
                        objectList.add(object);
                    }
                }
            }
            if (this.listener != null) {
                objectList = this.listener.afterAllObjectCreate(objectList, commandBean, this, null);
            }
        } finally {
            book.close();
            inputStream.close();
        }
        return objectList;
    }

    public Object parseColumn(int columnIndex, Object object, Sheet sheet, String className, List<ExcelMapping> mappingList, Integer columnRow, String columnRange, BaseCommandBean commandBean, List<Object> objectList) throws Exception {
        for (ExcelMapping map : mappingList) {
            int rowIndex = map.getRowIndex();
            if (rowIndex == ExcelMapping.UNDEFINED || columnRow == null || StringUtils.isBlank(columnRange)) {
                commandBean.getSystemMessage().addError("Import Template has not been setup properly for field: " + className + "." + map.getName());
                return null;
            }
            Row row = sheet.getRow(rowIndex);
            Cell c = row.getCell(columnIndex);
            String ids = map.getIds();
            String type = map.getType();
            object = this.processCell(c, ids, type, object, map, commandBean, objectList);
            if (object == null) {
                return null;
            }
            if (this.listener != null) { // AfterNodeCreate
                object = this.listener.afterObjectCreate(object, commandBean, this, objectList, null);
            }
        }
        return object;
    }

    @Override
    protected String getUnitSymbol(String valueStr, ExcelMapping mapping) {
        // Get uom string
        Sheet sheet = this.book.getSheetAt(0);
        Row row = sheet.getRow(mapping.getRowIndex());
        Cell cell = row.getCell(mapping.getUnitColumn());
        String cellStr = this.getCellValue(cell);
        String unitSymbol = RegexUtil.parseUnitSymbol(cellStr);
        if (StringUtils.isBlank(unitSymbol) && mapping.getDefaultUnit() != null) {
            unitSymbol = mapping.getDefaultUnit();
        }
        return unitSymbol;
    }

    public Integer getColumnRow() {
        return columnRow;
    }

    public void setColumnRow(Integer columnRow) {
        this.columnRow = columnRow;
    }

    public String getColumnRange() {
        return columnRange;
    }

    public void setColumnRange(String columnRange) {
        this.columnRange = columnRange;
    }

}
