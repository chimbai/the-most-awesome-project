<html>
<body>
Dear Sir/Madam,
<p>Please find attached report with regards to Daily Observation Report for ${operationName}, ${reportTime}.</p>
--------------------------------------<br>
This is an automated message.
<p>Please do not reply to the address above.</p>
<p>Thank you.</p>
<p><b>IDS Datanet Service</b></p>
</body>
</html>