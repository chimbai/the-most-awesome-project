<html>
<body>
Dear Sir/Madam, 

<#if lessonTicket.lessonReportType?? && lessonTicket.lessonReportType == "plan">
<p>This message was sent to notify that you are responsible for a Planned Lesson Learned or there are changes to the Planned Lesson Learned under your responsibility:</p>
<#elseif lessonTicket.lessonReportType?? && lessonTicket.lessonReportType == "non_op">
<p>This message was sent to notify that you are responsible for a Non-Operational Lesson Learned or there are changes to the Non-Op Lesson Learned under your responsibility:</p>
<#else>
<p>This message was sent to notify that you are responsible for a Lesson Learned or there are changes to the Lesson Learned under your responsibility:</p>
</#if>

			
		<p>Lesson #: ${lessonTicket.lessonTicketNumber}</p>

		<p>Click <a href="${lessonTicket.hyperLink}">here</a> for further action.</p>

<p>Thank you.</p>
<p><b>IDS Datanet Service</b></p>
</body>
</html>