package com.idsdatanet.d2.common.util;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.idsdatanet.d2.core.lookup.LookupItem;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.Action;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNodeImpl;
import com.idsdatanet.d2.core.web.mvc.DataNodeListener;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;

import net.sf.json.JSONObject;

public class ExcelImportTemplateManager extends DefaultBeanSupport {
	private static final String TEMP_UPLOAD_FILE_LOCATION = "WEB-INF/temp";
	
	private static ExcelImportTemplateManager configuredInstance = null;
	public static ExcelImportTemplateManager getConfiguredInstance() {
		if (configuredInstance == null) {
			try {
				configuredInstance = getSingletonInstance(ExcelImportTemplateManager.class);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return configuredInstance;
	}
    
    public static final String IMPORT_TEMPLATE_EXCEL = "importTemplateExcel";
    public static final String KEY_TEMPLATE = "template";
    public static final String KEY_FILE = "file";
    
    private LinkedHashMap<String, LinkedHashMap<String, ExcelImportTemplate>> excelImportTemplateList = null;
    
    public LinkedHashMap<String, LinkedHashMap<String, ExcelImportTemplate>> getExcelImportTemplateList() {
        return excelImportTemplateList;
    }

    public void setExcelImportTemplateList(LinkedHashMap<String, LinkedHashMap<String, ExcelImportTemplate>> excelImportTemplateList) {
        this.excelImportTemplateList = excelImportTemplateList;
    }

    public ExcelImportTemplate getExcelImportTemplate(String className, String template) {
        LinkedHashMap<String, ExcelImportTemplate> e = this.excelImportTemplateList.get(className);
        if (e != null && e.size() > 0) {
            return e.get(template);
        }
        return null;
    }
    
    public Map<String, LookupItem> getTemplateLookupList(String className) {
        LinkedHashMap<String, LookupItem> result = new LinkedHashMap<>();
        LinkedHashMap<String, ExcelImportTemplate> excelImportTemplateListForClass = this.excelImportTemplateList.get(className);
        for (Map.Entry<String, ExcelImportTemplate> template : excelImportTemplateListForClass.entrySet()) {
            LookupItem lookupItem = new LookupItem(template.getKey() ,template.getValue().getName());
            result.put(lookupItem.getKey(), lookupItem);
        }
        return result;
    }
    
    private File getUploadFolder() throws Exception {
    	return new File(new File(ApplicationConfig.getConfiguredInstance().getServerRootPath()), TEMP_UPLOAD_FILE_LOCATION);
    }
    
    private File saveUploadedFile(HttpServletRequest request, String fileKey) throws Exception {
        if (request instanceof MultipartHttpServletRequest) {
        	MultipartFile multipart = ((MultipartHttpServletRequest) request).getFile(fileKey);
        	File dir = this.getUploadFolder();
        	if(!dir.exists()) {
        		dir.mkdirs();
        	}
        	File temp = File.createTempFile("xlsImport", null, dir);
        	multipart.transferTo(temp);
        	return temp;
        }else {
        	return null;
        }
    }
	
    public void uploadFile(HttpServletRequest request, HttpServletResponse response, String fileKey) throws Exception {
        File file = this.saveUploadedFile(request, fileKey);
        if (file != null) {
            JSONObject result = new JSONObject();
            result.element("status", "ok");
            result.element("tempFile", file.getName());
            this.writeOutput(result, response);
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
    
    public void importExcelFile(BaseCommandBean commandBean, EmptyCommandBeanListener commandBeanListener, HttpServletRequest request, String templateName, String filePath, String className) throws Exception {
        this.importExcelFile(commandBean, commandBeanListener, request, templateName, filePath, new String[] { className }, true);
    }

    public void importExcelFile(BaseCommandBean commandBean, EmptyCommandBeanListener commandBeanListener, HttpServletRequest request, String templateName, String filePath, String className, boolean deleteFile) throws Exception {
        this.importExcelFile(commandBean, commandBeanListener, request, templateName, filePath, new String[] { className }, deleteFile);
    }

    public void importExcelFile(BaseCommandBean commandBean, EmptyCommandBeanListener commandBeanListener, HttpServletRequest request, String templateName, String filePath, String[] className) throws Exception {
        this.importExcelFile(commandBean, commandBeanListener, request, templateName, filePath, className, true);
    }

    public void importExcelFile(BaseCommandBean commandBean, EmptyCommandBeanListener commandBeanListener, HttpServletRequest request, String templateName, String filePath, String[] className, boolean deleteFile) throws Exception {
        if (StringUtils.isBlank(templateName)) {
            commandBean.getSystemMessage().addError("Invalid/Empty Import Template Selected");
        } else if (StringUtils.isBlank(filePath)) {
            commandBean.getSystemMessage().addError("Invalid/Empty File Path");
        } else {
            this.processExcelFile(commandBean, commandBeanListener, request, templateName, filePath, className, true, deleteFile);
        }
    }

    public void processExcelFile(BaseCommandBean commandBean, EmptyCommandBeanListener commandBeanListener, HttpServletRequest request, String templateName, String filePath, String[] className, boolean useInternalListeners, boolean deleteFile) throws Exception {
        commandBean.getFlexClientAdaptor().setSelectiveResponseForCurrentSubmitAction(false);
        File excelFile = new File(this.getUploadFolder(), filePath);
        HashMap<String, CommandBeanTreeNodeImpl> parentNodes = new HashMap<>();
        for (int i = 0; i < className.length; i++) {
            ExcelImportTemplate template = this.getExcelImportTemplate(className[i], templateName);
            if (template == null) {
                continue;
            }
            try {
                List<Object> objList = this.parseTemplate(excelFile, template, commandBean);
                if (!objList.isEmpty()) {
                	String validParam = objList.get(0).toString();
                    if (!validParam.startsWith("com")) {
                    	objList.remove(0);
                    }
                }
                
                if (objList == null || objList.isEmpty()) {
                    commandBean.getSystemMessage().addError("Invalid/Empty Import Template");
                }
                CommandBeanTreeNodeImpl targetNode = (CommandBeanTreeNodeImpl) commandBean.getRoot();
                if (template.getParent() != null) {
                    CommandBeanTreeNodeImpl pNode = parentNodes.get(template.getParent());
                    if (pNode != null) {
                        targetNode = pNode;
                    }
                }

                // Create Nodes
                for (Object obj : objList) {
                    CommandBeanTreeNodeImpl node = (CommandBeanTreeNodeImpl) targetNode.addCustomNewChildNodeForInput(obj);
                    node.getAtts().setAction(Action.SAVE);
                    node.getAtts().setEditMode(true);
                    node.setNewlyCreated(true);
                    node.setDirty(true);
                    if (useInternalListeners) {
                        for (DataNodeListener listener : commandBean.getInternalSequencedDataNodeListeners()) {
                            listener.afterTemplateNodeCreated(commandBean, targetNode.getChildDataDefinition(obj.getClass()), node, commandBean.getCurrentUserSelectionSnapshot(), request);
                        }
                    }
                    // if listener exist do listeners
                    if (template.getListener() != null) {
                        template.getListener().afterNodeCreate(node.getData(), commandBean, commandBeanListener, request, node, template, objList, null);
                    }
                    if (template.getParent() == null) {
                        parentNodes.put(template.getClassName(), node);
                    }
                }

            } catch (Exception e) {
            	LogFactory.getLog(this.getClass()).error("Error with template import", e);
                commandBean.getSystemMessage().addError("Invalid File/Empty Import Template");
            }
        }
        // listener here? of the root parent(s)
        if (excelFile.exists() && deleteFile) {
            excelFile.delete();
        }
    }

    public List<Object> parseTemplate(File excelFile, ExcelImportTemplate template, BaseCommandBean commandBean) throws Exception {
        return template.parseTemplate(excelFile, commandBean);
    }
    
    private void writeOutput(JSONObject json, HttpServletResponse response) throws IOException{
        response.setCharacterEncoding("utf-8");
        Writer writer = response.getWriter();
        json.write(writer);
        writer.close();
    }

}
