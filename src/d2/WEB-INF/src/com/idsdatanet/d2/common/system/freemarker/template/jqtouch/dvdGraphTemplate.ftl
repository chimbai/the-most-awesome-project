
<#include "/templateHelper.ftl"/>
<#include "/core/d2Helper.ftl"/>

	
	<#if form??>
		<#if form.info.getCustomView("data")??>
				<#include form.info.getCustomView("data")/>
		</#if>
	</#if>
	
	<script type="text/javascript" charset="utf-8">
      var chart${getDynamicFieldValue(form.root, "@operationUid")} = new Highcharts.Chart({
         chart: {
            renderTo: 'chart-container-${getDynamicFieldValue(form.root, "@operationUid")}',
            width: 320,
            style: {
	         margin: '0 auto'
	      }
         },
         title: {
            text: 'Actual Day/Depth'
         },
         xAxis: {
         	title: {
         	   enabled: true,
               text: 'Days (d)'
            }
         },
         yAxis: {
         	reversed: true,
            title: {
               text: 'Depth (${getDynamicFieldValue(form.root, "@depthUomSymbol")})'
            }
         },
         legend: {
		      enabled: false
		   },
		 tooltip: {
	     formatter: function() {
		         return '<b>'+ (this.point.name || this.series.name) +'</b><br/>'+
		            'Day: ' + Highcharts.numberFormat(this.x, 2) + '<br/>' +
		            'Depth: ' + Highcharts.numberFormat(this.y, 2) + ' ${getDynamicFieldValue(form.root, "@depthUomSymbol")}';
		      }
		   },
		 plotOptions: {
		 	line: {
		 		marker: {
		 			enabled: false,
		            states: {
		               hover: {
		                  enabled: true,
		                  fillColor: '#FF0000',
		                  radius: 5
		               }
		            }
		 		}
		 	}
		 },
         series: [{
			name: 'Actual Day/Depth',
			data: [${getDynamicFieldValue(form.root, "@series")}]
		}]
      });
</script>