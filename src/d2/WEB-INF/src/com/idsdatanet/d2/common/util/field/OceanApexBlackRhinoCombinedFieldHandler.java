package com.idsdatanet.d2.common.util.field;

import java.util.ArrayList;

import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;

import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.common.util.RegexUtil;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class OceanApexBlackRhinoCombinedFieldHandler extends GenericFieldHandler {
    
    private String targetFields = null;
    private String targetTypes = null;
    private String delimiter = ":";

    @Override
    public Object translate(Object value, Object object, BaseCommandBean commandBean, ExcelMapping mapping, List<Object> objList, Workbook book, Cell cell) throws Exception {
        Object v = null;
        if (value != null && value instanceof String && StringUtils.isNotBlank(value.toString())) {
            if (targetFields == null || targetTypes == null) {
                commandBean.getSystemMessage().addError("Column: [" + mapping.getColumnIndex() + "] has invalid input.");
                return null;
            } else {
                String[] fields = this.targetFields.split(delimiter);
                String[] types = this.targetTypes.split(delimiter);
                if (fields.length == types.length) {
                    int numberOfFields = fields.length;
                    String[] values = value.toString().split(delimiter);
            		String checkValue = value.toString();
	            	for (int i = 0; i < numberOfFields; i++) {
	                    try {
	                    	if (i == 0) {
	                    		if (checkValue.endsWith("/")){
	                    			ArrayList<String> results = new ArrayList<String>();
	                            	Integer totalBefore = values.length-1;
	            	            	for (int x = 0; x <= totalBefore; x++) {
	            	            		results.add(values[x]);
	            	            	}
	            	            	String joinedString = StringUtils.join(results, '/');
	                            	Object rv = joinedString;
		                            if (StringUtils.isBlank(rv.toString())) {
		                                continue;
		                            }
		                            rv = RegexUtil.parseType(rv, types[0]); // Cell Type Handling
		                            if (mapping.getIds().equals(fields[0])) {
		                                v = rv;
		                            } else if (rv != null && PropertyUtils.isWriteable(object, fields[0])) {
		                                PropertyUtils.setProperty(object, fields[0], rv);
		                            }
	                    		} else if (values.length > 2 && !checkValue.endsWith("/")) {
	                            	ArrayList<String> results = new ArrayList<String>();
	                            	Integer totalBefore = values.length-1;
	            	            	for (int x = 0; x < totalBefore; x++) {
	            	            		results.add(values[x]);
	            	            	}
	            	            	String joinedString = StringUtils.join(results, '/');
	                            	Object rv = joinedString;
		                            if (StringUtils.isBlank(rv.toString())) {
		                                continue;
		                            }
		                            rv = RegexUtil.parseType(rv, types[0]); // Cell Type Handling
		                            if (mapping.getIds().equals(fields[0])) {
		                                v = rv;
		                            } else if (rv != null && PropertyUtils.isWriteable(object, fields[0])) {
		                                PropertyUtils.setProperty(object, fields[0], rv);
		                            }
	                            } else if (values.length < 3 && !checkValue.endsWith("/")){
	                            	Object rv = values[0];
		                            if (StringUtils.isBlank(rv.toString())) {
		                                continue;
		                            }
		                            rv = RegexUtil.parseType(rv, types[0]); // Cell Type Handling
		                            if (mapping.getIds().equals(fields[0])) {
		                                v = rv;
		                            } else if (rv != null && PropertyUtils.isWriteable(object, fields[0])) {
		                                PropertyUtils.setProperty(object, fields[0], rv);
		                            }
	                            }
	                    	} else {
	                    		if (checkValue.endsWith("/")) {
	                    			Object rv = "";
		                            if (StringUtils.isBlank(rv.toString())) {
		                                continue;
		                            }
	                    		} else {
		                    		Object rv = values[values.length-1];
		                    		String company = rv.toString();
		                    		String strSql = "SELECT lookupCompanyUid FROM LookupCompany WHERE (isDeleted is null or isDeleted = false) AND companyName = :thisCompanyName";
		                    		
		                    		String[] paramsFields = {"thisCompanyName"};
		                    		Object[] paramsValues = {company}; 
	
		                    		List<?> lstResult = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(strSql, paramsFields, paramsValues);
		                    		if (lstResult.size() > 0){
		                    			Object companyUid = lstResult.get(0);
		                    			if (values.length > 1) {
				                            if (StringUtils.isBlank(rv.toString())) {
				                                continue;
				                            }
				                            companyUid = RegexUtil.parseType(companyUid, types[1]); // Cell Type Handling
				                            if (mapping.getIds().equals(fields[1])) {
				                                v = companyUid;
				                            } else if (companyUid != null && PropertyUtils.isWriteable(object, fields[1])) {
				                                PropertyUtils.setProperty(object, fields[1], companyUid);
				                            } 
			                            }
		                    		} else {
			                    		if (values.length > 1) {
				                            if (StringUtils.isBlank(rv.toString())) {
				                                continue;
				                            }
				                            rv = RegexUtil.parseType(rv, types[1]); // Cell Type Handling
				                            if (mapping.getIds().equals(fields[1])) {
				                                v = rv;
				                            } else if (rv != null && PropertyUtils.isWriteable(object, fields[1])) {
				                                PropertyUtils.setProperty(object, fields[1], rv);
				                            } 
			                            }
		                    		}
		                    	}
	                    	}
	                    } catch (ArrayIndexOutOfBoundsException er) {
	                        break;
	                    } catch (NumberFormatException er) {
	                        commandBean.getSystemMessage().addError("Column: [" + mapping.getColumnIndex() + "] contains non-numbers. Provided: " + value);
	                        return null;
	                    }
	                }
                } else {
                    commandBean.getSystemMessage().addError("Column: [" + mapping.getColumnIndex() + "] has invalid setup definitions.");
                    return null;
                }
            }
        }
        return v;
    }

    public String getTargetFields() {
        return targetFields;
    }

    public void setTargetFields(String targetFields) {
        this.targetFields = targetFields;
    }

    public String getTargetTypes() {
        return targetTypes;
    }

    public void setTargetTypes(String targetTypes) {
        this.targetTypes = targetTypes;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }
}