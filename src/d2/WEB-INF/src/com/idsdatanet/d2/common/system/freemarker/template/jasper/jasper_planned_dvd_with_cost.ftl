<html>
    <head><style>
      body {
        margin: 0px;
        padding: 0px;
      }
    </style>
    <script type='text/javascript' src="${visualizejsUtils.getJasperServerBaseUrl()}/client/visualize.js?baseUrl=${visualizejsUtils.getJasperServerBaseUrl()?url}"></script>
    	<script src="http://code.jquery.com/jquery-2.1.0.js"></script>
		<script src="http://underscorejs.org/underscore-min.js"></script>
		
    	<script type="text/javascript" src="script/d3/schematic/jquery-latest.js"></script>
  		<script type="text/javascript" src="script/d3/schematic/jquery-ui-latest.js"></script>
 		 <script type="text/javascript" src="script/d3/schematic/jquery.layout-latest.js"></script>
 		 
    </head>
    <body>
    	
    	<script src="script/jasper/report_13.js"></script>
    	<div class="ui-layout-center">
    	<div id="divExport" style="float:left">
    		Export to: 
            <select id="outputType">
                <option value="pdf">PDF</option>
                <option value="xlsx">XLSX</option>
                <option value="xls">XLS</option>
                <option value="xml">XML</option>                
                <option value="docx">DOCX</option>
                <!--
                <option value="rtf">RTF</option>
                <option value="csv">CSV</option>
                <option value="odt">ODT</option>
                <option value="ods">ODS</option> -->
            </select>
 			
    	<button id="button" disabled>Export</button>
    	</div>
 		
    	<button id="previousPage" style="display:none; float:left; margin-left:5px" disabled>Previous Page</button>
    	<div id="pagination" style="display:none; float:left; margin-left:5px">Page</div>
		<input type="text" id="pageRange" style="display:none;float:left;margin-left:5px"></input>
		<div id="totalpagevalue" style="display:none;float:left;margin-left:5px"></div>
		<button id="nextPage" style="display:none;float:left;margin-left:5px" disabled>Next Page</button>
      <div id="container" style="float:left"></div>
  </div>
  <div class="ui-layout-west">
      Well Name: </P>
      <select id="wellOpts" multiple="multiple" style="width:100%"></select> </P>
      
      Operation Type: </P>
      <select id="operationTypeOpts" multiple="multiple" style="width:100%"></select> </P>
      
      Operation Name: </P>
      <select id="operationNameOpts" multiple="multiple" style="width:100%"></select> </P>
      <button id="btnRun">Run</button>
  </div>
    	
    	
		
    </body>
</html>