package com.idsdatanet.d2.common.scheduler.handler;

import java.util.Date;
import java.util.List;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.model.Daily;
import com.idsdatanet.d2.core.model.SchedulerJobDetail;
import com.idsdatanet.d2.drillnet.report.DDRReportModule;
import com.idsdatanet.d2.drillnet.report.DefaultReportModule;
import com.idsdatanet.d2.drillnet.report.ReportAutoEmail;
import com.idsdatanet.d2.infra.scheduler.handler.AutoServiceHandler;

public class AutoReportServiceHandler implements AutoServiceHandler {

	private ReportAutoEmail reportAutoEmail;
	
	public ReportAutoEmail getReportAutoEmail() {
		return reportAutoEmail;
	}

	public void setReportAutoEmail(ReportAutoEmail reportAutoEmail) {
		this.reportAutoEmail = reportAutoEmail;
	}

	public void execute(Object serviceBean, String loginUser, SchedulerJobDetail jobDetail) throws Exception{
		if (serviceBean instanceof DDRReportModule)
		{
			DDRReportModule reportModule = (DDRReportModule) serviceBean;
			List<Daily> dailies = CommonUtil.getConfiguredInstance().getDaysForReportAsAt(new Date());
			reportModule.generateDailyReportOnCurrentSystemDatetime(loginUser, reportAutoEmail, dailies);
		}else if (serviceBean instanceof DefaultReportModule)
		{
			DefaultReportModule reportModule = (DefaultReportModule) serviceBean;
			reportModule.generateDefaultReportOnCurrentSystemDatetime(loginUser, reportAutoEmail);
		}
		
	}

}
