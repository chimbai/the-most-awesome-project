package com.idsdatanet.d2.common.mobile;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;

import com.idsdatanet.d2.common.util.CommonUtil;
import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.ReportDaily;
import com.idsdatanet.d2.core.model.ReportFiles;
import com.idsdatanet.d2.core.util.CommonUtils;
import com.idsdatanet.d2.core.web.mvc.AbstractGenericWebServiceCommandBean;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.drillnet.operationinformationcenter.DDRReportList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class MobileReportLoaderWebServiceCommandBean extends AbstractGenericWebServiceCommandBean {

	public static final String PARAM_INVOCATION_METHOD = "method";
	public static final String METHOD_GET_DDR_LIST = "get_ddr_list";
	public static final String DATE_PARAM = "dateParam";
	public static Date selectedDate = new Date();
	@Override
	public void process(HttpServletRequest request, HttpServletResponse response, BindException bindError) throws Exception {
		
		JSONObject o = JSONObject.fromObject(request.getParameter("data"));
		String method = o.get(PARAM_INVOCATION_METHOD).toString();
		
		selectedDate = new SimpleDateFormat("yyyy-MM-dd").parse(o.get(DATE_PARAM).toString());
		if(METHOD_GET_DDR_LIST.equals(method)) {
			this.loadDailyReportList(request, response);
		} 
	}
	
	private void loadDailyReportList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		UserSession session = UserSession.getInstance(request);
		Operations accessibleOperations = session.getCachedAllAccessibleOperations();
		JSONObject jsonObj = new JSONObject();
    	JSONArray jsonArr = new JSONArray();
    	JSONObject jsonObj2 = new JSONObject();
    	JSONArray jsonArr2 = new JSONArray();
		String[] paramsFields = {"selectedDate"};
		Object[] paramsValues = {selectedDate};
		List<Object> items = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportDaily rd, Operation o WHERE rd.operationUid = o.operationUid and (rd.isDeleted = false or rd.isDeleted is null) and (o.isDeleted = false or o.isDeleted is null) and rd.reportType <> 'DGR' and rd.reportDatetime=:selectedDate ORDER BY o.operationName", paramsFields, paramsValues);
		
		for(Iterator i = items.iterator(); i.hasNext(); ){
			Object[] obj_array = (Object[]) i.next();
			ReportDaily thisDaily = (ReportDaily) obj_array[0];
			Operation thisOperation = (Operation) obj_array[1];
			if(!accessibleOperations.containsKey(thisOperation.getOperationUid())) break;
			String operationName = CommonUtil.getConfiguredInstance().getCompleteOperationName(thisOperation.getGroupUid(), thisOperation.getOperationUid(), null, true, " - ");
			Map<String, String> reportTypePriorityWhenDrllgNotExist = CommonUtils.getConfiguredInstance().getReportTypePriorityWhenDrllgNotExist();
			String operationType = thisOperation.getOperationCode();
			
			String strReportTypes = "DDR";
			
			if(!operationType.equals("DRLLG")) strReportTypes = reportTypePriorityWhenDrllgNotExist.get(operationType);
			
			String[] paramsField = {"dailyUid", "reportFilesReportType", "operationUid"};
			Object[] paramsValue = {thisDaily.getDailyUid(), strReportTypes, thisOperation.getOperationUid()};
			List<ReportFiles> reportFiles = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("FROM ReportFiles rf WHERE rf.dailyUid=:dailyUid and rf.reportOperationUid=:operationUid and (rf.isDeleted = false or rf.isDeleted is null) and rf.reportType=:reportFilesReportType", paramsField, paramsValue);
			
			String qcFlag = "";
			if(thisDaily.getQcFlag().equals("0")) {
				qcFlag = " (" + CommonUtil.getConfiguredInstance().getReportDailyQCStatus(thisDaily.getDailyUid(), thisDaily.getReportType()) + ")";
			}
			jsonObj = new JSONObject();
	    	jsonObj.element("operationName", operationName);
	    	jsonObj.element("qcFlag", qcFlag);
	    	jsonArr2 = new JSONArray();
	    	
			for (ReportFiles reportFile : reportFiles) {
				DDRReportList ddr = new DDRReportList();
				
				ddr.setData(reportFile, operationName);
				
				File report_file = new File(ApplicationConfig.getConfiguredInstance().getReportOutputPath() + "/" + reportFile.getReportFile());
				
				if(report_file.exists()){
					
					jsonObj2 = new JSONObject();
					jsonObj2.element(DDRReportList.class.getSimpleName(), ddr);
					jsonArr2.element(jsonObj2);
				}
				
				
			}
			jsonObj.element("ReportList", jsonArr2);
			jsonArr.element(jsonObj);
		}
		JSONObject jsonResponse = new JSONObject();
    	jsonResponse.element("ddrlist", jsonArr);
		this.writeOutput(jsonResponse, response);
	}
	
	private void writeOutput(JSONObject json, HttpServletResponse response) throws IOException {
		// need to include setContentType() since it doesn't go through XmlToJsonContentHandler
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		Writer writer = response.getWriter();
		json.write(writer);
		writer.close();
	}
}
