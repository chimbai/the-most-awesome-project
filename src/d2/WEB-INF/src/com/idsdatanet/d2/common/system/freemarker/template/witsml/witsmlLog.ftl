<br/>
<div align="center" class="custom-header-selection">
	<table>
		<tr>
			<td><strong>Capability:</strong></td>
			<td>
				<@showField node=form.root editmode=true editable=true options="onChange='document.form.submit();'" dynamic="@witsmlCapability" />
			</td>
			<td><strong>WML Type:</strong></td>
			<td>
				<@showField node=form.root editmode=true editable=true options="onChange='document.form.submit();'" dynamic="@wmlType" />
			</td>
			<td><strong>User:</strong></td>
			<td>
				<@showField node=form.root editmode=true editable=true options="onChange='document.form.submit();'" dynamic="@userUid" />
			</td>
		</tr>
	</table>
</div>
<br/>
<@showFormTitleInContentPage classType="WitsmlLog" />
<table class="inner-table">
	<tr>
		<th>Datetime</th>
		<th>Method</th>
		<th>Witsml Object</th>
		<th>Xml In</th>
		<th>Xml Out</th>
		<th>UserId</th>
		<#--
		<th>Password</th>
		-->
		<th>Error Message</th>
	</tr>
	<#if form.root.list.WitsmlLog?exists>
		<#list form.root.list.WitsmlLog?values as item>
			<tr>
				<td width="10%"><@showField node=item field="lastEditDatetime" /></td>
				<td width="10%"><@showField node=item field="witsmlCapability" /></td>
				<td width="10%"><@showField node=item field="wmlType" /></td>
				<td width="20%" style="word-wrap: break-word; overflow: auto;"><@showField node=item field="xmlInText" maxDisplayText=100 /></td>
				<td width="20%" style="word-wrap: break-word; overflow: auto;"><@showField node=item field="xmlOutText" maxDisplayText=100 /></td>
				<td width="5%"><@showField node=item field="userUid" /></td>
				<#--
				<td width="5%"><@showField node=item field="userPassword" /></td>
				-->
				<td width="20%" style="color:red;"><@showField node=item field="errorMessage" /></td>
			</tr>
		</#list>
	</#if>
</table>