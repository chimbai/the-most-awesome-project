package com.idsdatanet.d2.common.scheduler.jobs;

import java.sql.Connection;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.web.mvc.SystemExceptionHandler;

public class CleanUpDvdSummary {
	
	private Log log = LogFactory.getLog(this.getClass());
	private DataSource dataSource = null;
	
	public void setDataSource(DataSource dataSource){
		this.dataSource = dataSource;
	}	

	public void run() throws Exception{
		Connection connection = null; 
		Statement statement = null;
		
		try {
			if (this.dataSource == null){			
				throw new Exception("Database connection no found");					
			}
		
			connection = this.dataSource.getConnection();
			
			statement = connection.createStatement();
			
			statement.executeUpdate("DELETE FROM dvd_summary WHERE is_deleted IS TRUE");
			
		}catch (Exception e) {
			log.error(SystemExceptionHandler.printAsString(e));			
		}finally{
			if (connection !=null) connection.close();
			if (statement !=null) statement.close();							
		}
	}
}
