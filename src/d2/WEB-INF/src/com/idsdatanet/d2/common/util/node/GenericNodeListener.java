package com.idsdatanet.d2.common.util.node;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.CommandBeanTreeNode;
import com.idsdatanet.d2.core.web.mvc.EmptyCommandBeanListener;

public class GenericNodeListener implements NodeListenerInterface {
    
    @Override
    public Object beforeNodeCreate(Object object, BaseCommandBean commandBean, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        return object;
    }
    
    @Override
    public Object afterObjectCreate(Object object, BaseCommandBean commandBean, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        return object;
    }

    @Override
    public void afterNodeCreate(Object object, BaseCommandBean commandBean, EmptyCommandBeanListener commandBeanListener, HttpServletRequest request, CommandBeanTreeNode targetCommandBeanTreeNode, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        return;
    }
    
    @Override
    public List<Object> afterAllObjectCreate(List<Object> objList, BaseCommandBean commandBean, ExcelImportTemplate template, Map<Integer, ExcelMapping> columnMap) throws Exception {
        return objList;
    }
    
}