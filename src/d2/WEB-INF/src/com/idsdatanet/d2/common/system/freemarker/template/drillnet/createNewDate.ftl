<script type="text/javascript">
<!--
	var createNewDateSM = {
		validateForm: function() {
			var is_valid = createNewDateSM.createNewDateValidator.form();
			if(is_valid) {
				$("#createNewDateInnerForm").hide();
				$("#createnewdate-loading").show();
				var data = $("#createNewDateForm").serialize();
				
				$.getJSON("createNewDate.html", data, function(fromServer) {
					if(fromServer.refresh == "false") {
						$("#createNewDateMessages").empty().append(fromServer.message);
						$("#createnewdate-loading").hide();
						$("#createNewDateInnerForm").show();
					} else {
						window.location.href = window.location.href;
					}
				}); 
			}
		},
		
		toggleCancel: function() {
			createNewDateSM.createNewDateValidator.resetForm();
			$("#createNewDateForm")[0].reset();
			$("#createNewDateMessages").empty();
		}
	}

	$(document).ready(function(){
		createNewDateSM.createNewDateValidator = $("#createNewDateForm").validate();
	});
-->
</script>
<a id="createNewDateLink" class="thickbox hide" href="&#TB_inline?height=165&width=320&inlineId=createNewDate&modal=true"></a>
<div id="createNewDate" class="hide">
	<div id="createNewDateContent" align="center">
		<div id="createNewDateInnerForm">
			<form id="createNewDateForm">
				<table>
					<tr><td colspan="2"><div id="createNewDateMessages" align="center"></div></td></tr>
					<tr>
						<td class="label">Day #:</td><td><input type="input" name="dayNum" class="field" /></td>
					</tr>
					<tr>
						<td class="label">Day Date:</td><td><input type="input" name="dayDate" class="field required {required:true}" /></td>
					</tr>
					<tr>
						<td colspan="2" class="submit-action">
							<span class="buttons">
								<input type="button" value="Confirm" onclick="createNewDateSM.validateForm();" />
							</span> or <a class="delete-link" href="javascript:void(0);" onclick="createNewDateSM.toggleCancel(); self.parent.tb_remove();">Cancel</a>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<div id="createnewdate-loading">
			<img src="images/indicator.gif" /> Loading...
		</div>
	</div>
</div>