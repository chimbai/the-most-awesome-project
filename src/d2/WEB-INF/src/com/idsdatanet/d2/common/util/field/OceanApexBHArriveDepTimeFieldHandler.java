package com.idsdatanet.d2.common.util.field;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;

import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class OceanApexBHArriveDepTimeFieldHandler extends GenericFieldHandler{
    @Override
    public Object translate(Object value, Object object, BaseCommandBean commandBean, ExcelMapping mapping, List<Object> objList, Workbook book, Cell cell) throws Exception {
    		String dates = objList.get(0).toString();
    		String[] daymonthyear = StringUtils.split(dates, '-');
    		String day = daymonthyear [0];
    		String month = daymonthyear [1];
    		String year = daymonthyear [2];
    		String time = (String) value;
    		String[] hourminute = StringUtils.split(time, ':');
    		String hour = hourminute [0];
    		String minute = hourminute [1];
    		int months = 0;
    		if (month.equalsIgnoreCase("jan")) {
    			months = 0;
    		} else if (month.equalsIgnoreCase("feb")) {
    			months = 1;
    		} else if (month.equalsIgnoreCase("mar")) {
    			months = 2;
    		} else if (month.equalsIgnoreCase("apr")) {
    			months = 3;
    		} else if (month.equalsIgnoreCase("may")) {
    			months = 4;
    		} else if (month.equalsIgnoreCase("jun")) {
    			months = 5;
    		} else if (month.equalsIgnoreCase("jul")) {
    			months = 6;
    		} else if (month.equalsIgnoreCase("aug")) {
    			months = 7;
    		} else if (month.equalsIgnoreCase("sep")) {
    			months = 8;
    		} else if (month.equalsIgnoreCase("oct")) {
    			months = 9;
    		} else if (month.equalsIgnoreCase("nov")) {
    			months = 10;
    		} else if (month.equalsIgnoreCase("dec")) {
    			months = 11;
    		}
    		Integer days = Integer.valueOf(day);
    		Integer years = Integer.valueOf(year);
    		Integer hours = Integer.valueOf(hour);
    		Integer minutes = Integer.valueOf(minute);
    		Integer seconds = 0;
			Calendar cal = Calendar.getInstance();
			cal.set(years, months, days, hours, minutes, seconds);
			Date date = cal.getTime();
    		return date;
    }
}
