<html>
<body>
This is an <b>automated message</b>.
<br>

<#if qc?exists>
	<br> The following operation has DDR with empty/null QC status, or status 'QC Not Done':
	<#list qc?keys as key>
		<br>${qc[key]}
	</#list>
	<br>
</#if>


<p>Thank you.</p>
<p><b>IDS Datanet Service</b></p>
</body>
</html>