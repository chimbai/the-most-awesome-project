package com.idsdatanet.d2.common.scheduler.jobs;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.core.dao.DaoUserContext;
import com.idsdatanet.d2.core.job.D2Job;
import com.idsdatanet.d2.core.job.JobResponse;
import com.idsdatanet.d2.core.job.UserContext;
import com.idsdatanet.d2.core.spring.DefaultBeanSupport;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.model.DepotWellOperationMapping;
import com.idsdatanet.d2.core.model.DepotWitsmlLogRequest;
import com.idsdatanet.d2.core.model.DepotWitsmlMsg;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.depot.core.DepotConstants;
import com.idsdatanet.depot.core.DepotContext;
import com.idsdatanet.depot.core.DepotNode;
import com.idsdatanet.depot.core.mapping.DepotMappingModule;
import com.idsdatanet.depot.core.mapping.MappingAggregator;
import com.idsdatanet.depot.core.query.QueryParameter;
import com.idsdatanet.depot.core.query.QueryResult;
import com.idsdatanet.depot.core.util.DepotUtils;
import com.idsdatanet.depot.core.webservice.FakeHttpServletRequest;
import com.idsdatanet.depot.d2.util.D2DepotUtils;
import com.idsdatanet.depot.witsml.scheduler.job.RigStateImport;
import com.idsdatanet.depot.witsml.soap.WitsmlSoapQuery;

public class WitsmlMessageQuery extends DefaultBeanSupport {
	
	private static WitsmlMessageQuery configuredInstance = null;
	private static boolean isRunning = false;


	
	public static WitsmlMessageQuery getConfiguredInstance() throws Exception {
		if (configuredInstance == null) configuredInstance = getSingletonInstance(WitsmlMessageQuery.class);
		return configuredInstance;
	}
	
	public void run(String serverPropertiesUid) throws Exception {
		if (isRunning) return;
		try {
			isRunning = true;
				String dataXML = "";
				String queryString = "from DepotWellOperationMapping dwom where (dwom.schedulerTemplateUid IN ((Select st.schedulerTemplateUid from SchedulerTemplate st where st.dataObjects LIKE '%message%' and (st.isDeleted is null or st.isDeleted = false)))) and (dwom.isDeleted is null or dwom.isDeleted = false)  " ;
				List<DepotWellOperationMapping> list = ApplicationUtils.getConfiguredInstance().getDaoManager().find(queryString);
				if (list.size() > 0) {
					for (DepotWellOperationMapping dwom : list) {
						//if (listComponentResult[0] != null && listComponentResult[1] != null ){
							dataXML= getXMLOutput(dwom,serverPropertiesUid);
							if (StringUtils.isNotBlank(dataXML)){	
								  MappingAggregator ma = DepotMappingModule.getConfiguredInstance().getMappingAggregator(DepotConstants.WITSML, "message");
								  if(ma !=null) {
									   try {        
										   UserSelectionSnapshot userSelection = DepotUtils.getUserSelection();
										   userSelection.populateDataAccordingToWellboreUid(dwom.getWellboreUid());
										   userSelection.populateDataAccordingToWellUid(dwom.getWellUid());
										   userSelection.populateDataAccordingToOperationUid(dwom.getOperationUid());
										   QueryResult queryResult = ma.addToStore(new FakeHttpServletRequest(), userSelection, dataXML, false);
								    		if (queryResult.getResultCode() == 1) {
								    			//System.out.println("success");
								    		}
									   }catch (Exception e) {
										   
									   }finally {
										   ma.dispose();
									   }
								   }
								  
							}
						}
					}
				
				
			 
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			isRunning = false;
		}
		
	}

	public void dispose() {}
	
	public void process(WitsmlMessageQuery wmq) throws Exception {
	
	}
	private String getXMLOutput(DepotWellOperationMapping dwom,String serverPropertiesUid ) throws Exception{
		String xmlContent="";
		MappingAggregator mappingAggregator = DepotMappingModule.getConfiguredInstance().getMappingAggregator(DepotConstants.WITSML,"message");
		
		if (mappingAggregator!=null){
			DepotContext depotContext = new DepotContext(new FakeHttpServletRequest(), mappingAggregator.getStoreVersion(), mappingAggregator.getDefaultNamespace());
			depotContext.setWellUid(dwom.getDepotWellUid());
			depotContext.setWellboreUid(dwom.getDepotWellboreUid());
			String queryTemplate = mappingAggregator.getCustomQueryTemplateById("query-only");
			if(queryTemplate!=null){
				try {	
					queryTemplate = depotContext.resolveQueryTemplateParameters(queryTemplate);
					String operationUid=dwom.getOperationUid();
					String queryString = "SELECT MAX(depotChangeDatetime) FROM DepotWitsmlMsg WHERE operationUid=:operationUid and (isDeleted is null or isDeleted=false)";
					List<Date> list = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString,"operationUid",operationUid);
					if (list.get(0)!=null) {
						queryTemplate=StringUtils.replace(queryTemplate,"${dTimLastChange}", list.get(0).toString());
					}
					else {
						queryTemplate=StringUtils.replace(queryTemplate,"${dTimLastChange}", "");
					}
					
					DepotUtils.loginToD2();									    
					//QueryResult queryResult = mappingAggregator.getFromStore(new FakeHttpServletRequest(), DepotUtils.getUserSelection(), queryTemplate, true, false);
					ImportExportServerProperties serverProperties = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, serverPropertiesUid);
					QueryResult qResult = new QueryResult();
					qResult.setDepotObjectId("messages");
					WitsmlSoapQuery soapQuery = new WitsmlSoapQuery(serverProperties, DepotConstants.GET_FROM_STORE, mappingAggregator.getDataObjectId(), queryTemplate);
					soapQuery.invoke(qResult);
					if (qResult.getResultCode() == 1) {
						String xmlIn = (String)qResult.getOutput();
						xmlIn = this.setInternalUids(dwom, xmlIn);
						xmlContent=xmlIn;

					}else{
						qResult.setResultCode(-1);
						qResult.addError("Failed to query " + mappingAggregator.getDataObjectId() + " from " + serverProperties.getEndPoint());
					}
				}
				
				catch(Exception e) {
					throw new Exception(e.getMessage());
				}finally{
					DaoUserContext.getThreadLocalInstance().clearUserSelection();
				}			
			
		}
		
	}
		return xmlContent;
	}
	protected String setInternalUids(DepotWellOperationMapping dwom, String xml) {
		String wellReplaceToken = "uidWell=\"" + dwom.getDepotWellUid() + "\"";
		String wellReplaceWith = "uidWell=\"" + dwom.getWellUid() + "\"";
		
		String wellboreReplaceToken = "uidWellbore=\"" + dwom.getDepotWellboreUid() + "\"";
		String wellboreReplaceWith = "uidWellbore=\"" + dwom.getWellboreUid() + "\"";
		
		xml = StringUtils.replace(xml, wellReplaceToken, wellReplaceWith);
		xml = StringUtils.replace(xml, wellboreReplaceToken, wellboreReplaceWith);
		
		return xml;
	}
}