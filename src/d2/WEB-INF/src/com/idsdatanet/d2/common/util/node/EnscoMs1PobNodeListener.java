package com.idsdatanet.d2.common.util.node;

import java.util.List;
import java.util.Map;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.PersonnelOnSite;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;

public class EnscoMs1PobNodeListener extends GenericNodeListener {
    
    // Special Behaviour:
    // Seq: Auto-Populate Sequentially
    // Pax: Default at 1

    @Override
    public Object beforeNodeCreate(Object object, BaseCommandBean commandBean, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        return object;
    }
    
    @Override
    public Object afterObjectCreate(Object object, BaseCommandBean commandBean, ExcelImportTemplate template, List<Object> objList, Map<Integer, ExcelMapping> columnMap) throws Exception {
        if (object != null && object instanceof PersonnelOnSite) {
            PersonnelOnSite pob = (PersonnelOnSite) object;
            pob.setPax(1); // Default value as specified in specification
            return pob;
        }
        return object;
    }
    
    @Override
    public List<Object> afterAllObjectCreate(List<Object> objList, BaseCommandBean commandBean, ExcelImportTemplate template, Map<Integer, ExcelMapping> columnMap) throws Exception {
        if (objList == null || objList.isEmpty()) {
            return objList;
        }
        Integer sequence = 1;
        UserSelectionSnapshot userSelection = commandBean.getCurrentUserSelectionSnapshot();

        // Grab Biggest Sequence Value
        String sql = "FROM PersonnelOnSite WHERE (isDeleted = false OR isDeleted is null) AND dailyUid=:dailyUid AND operationUid=:operationUid ORDER BY sequence DESC";
        List<PersonnelOnSite> pobList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, new String[] { "dailyUid", "operationUid" }, new Object[] { userSelection.getDailyUid(), userSelection.getOperationUid() });
        if (!pobList.isEmpty()) {
            Integer seq = pobList.get(0).getSequence();
            if (seq != null) {
                sequence = seq + 1;
            }
        }
        for (Object object : objList) {
            if (object instanceof PersonnelOnSite) {
                PersonnelOnSite pob = (PersonnelOnSite) object;
                pob.setSequence(sequence++); // Stamp Sequence
            }
        }
        return objList;
    }
    
}