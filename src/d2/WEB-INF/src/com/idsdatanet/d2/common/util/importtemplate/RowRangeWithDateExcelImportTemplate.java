package com.idsdatanet.d2.common.util.importtemplate;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class RowRangeWithDateExcelImportTemplate extends ExcelImportTemplate {
    
    @Override
    public List<Object> parseTemplate(File excelFile, BaseCommandBean commandBean) throws Exception {
        List<Object> objectList = null;
        InputStream inputStream = new FileInputStream(excelFile);
        this.book = WorkbookFactory.create(inputStream);
        this.evaluator = book.getCreationHelper().createFormulaEvaluator();
        Sheet sheet = book.getSheetAt(0);
        
        if (sheet != null) {
            objectList = new ArrayList<>();
            Cell Date = sheet.getRow(4).getCell(0);
            objectList.add(Date);
            Row row = null;
            LinkedHashMap<Integer, ExcelMapping> columnMap = new LinkedHashMap<>();
            for (ExcelMapping map : this.getExcelMappingList()) {
                int column = map.getColumnIndex();
                columnMap.put(column, map);
            }
            for (int i = this.startRow; i <= this.endRow; i++) {
                row = sheet.getRow(i);
                if (row == null || (this.skipRows != null && this.skipRows.contains(i))) {
                    continue;
                }
                Object obj = this.parseRow(row, this.className, columnMap, commandBean, objectList);
                if (obj != null) {
                    objectList.add(obj);
                }
            }
            if (this.listener != null) {
                objectList = this.listener.afterAllObjectCreate(objectList, commandBean, this, columnMap);
            }
        }
        book.close();
        inputStream.close();
        return objectList;
    }
}