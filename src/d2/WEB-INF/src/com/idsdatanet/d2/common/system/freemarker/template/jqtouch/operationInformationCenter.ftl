<div id="home" class="ids1">
    <div class="toolbar">
        <h1>Operation Info. Center</h1>
    </div>
    <ul class="ids1">
    	<#if form.root.list.Operation?exists>
			<#list form.root.list.Operation?values as item>
				<#assign current_uom = item.info.getUOMForDynamicAttribute("@lastDepth")?default("")/>
				<li class="arrow"><a href="jqtouchdvdgraph.html?operationUid=${getFieldValue(item, "operationUid")}"><small>${_getFinalValue(getDynamicFieldValue(item, "@lastDepth"), "", "", defaultValue, false)}&nbsp;<#if current_uom != "">${item.info.formatUOMSymbol(current_uom, "@lastDepth")}</#if></small>${getFieldValue(item, "operationName")}<em>${_getLookupLabel(item.info.getLookup("@todayRig"), getDynamicFieldValue(item, "@todayRig"))}</em></a></li>
	        </#list>
        </#if>
    </ul>
    <div class="info">
        <p>Add this page to your home screen to view the custom icon, startup screen, and full screen mode.</p>
    </div>
</div>
<#if form.root.list.Operation?exists>
	<#list form.root.list.Operation?values as item>
		<div id="${getFieldValue(item, "operationUid")}">
		    <div class="toolbar">
		        <h1>${getFieldValue(item, "operationName")}</h1>
		        <a href="#" class="back">Back</a>
		    </div>
		</div>
	</#list>
</#if>