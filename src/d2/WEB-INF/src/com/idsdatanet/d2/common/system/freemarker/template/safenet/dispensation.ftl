<html>
<body>
<p>Dear Sir/Madam,</p>

<p>This message was sent to notify that you are responsible for a Dispensation Request or there are changes to the Dispensation Request under your responsibility:</p>
			
<p>DR #: DR-${mocDisp.mocDispNumber}</p>

<p>Click <a href="${mocDisp.hyperLink}">here</a> for further action.</p>

<p>Thanks,</p>
<p><b>IDS Support Group</b></p>
</body>
</html>