<html>
    <head>
    <#assign g_v_resources_path = "${form.getVirtualResourcePath(1)}/htmllib"/>
	<#assign v_resources_path = "${form.getVirtualResourcePath(1)}/htmllib/jasper"/>
    	<link rel="stylesheet" type="text/css" href="${g_v_resources_path}/assets/vendors/font-awesome/css/fontawesome-all.min.css">
    	<link rel="stylesheet" type="text/css" href="${g_v_resources_path}/jquery/css/jquery-ui-1.13.0.min.css"/>
    	<link rel="stylesheet" type="text/css" href="${g_v_resources_path}/script/bootstrap413/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="${g_v_resources_path}/datepicker/css/daterangepicker.min.css">
		<link rel="stylesheet" href="${v_resources_path}/resources/throbber.css" />
		<link rel="stylesheet" href="${v_resources_path}/resources/customMessageDialog.css" />
		
		
		
		<style>
      		body, html { 
      			background-color: #363636; 
      			-webkit-overflow-scrolling:touch !important;
      		}
			
      		.scroll_frame {
				overflow: scroll;
				-webkit-overflow-scrolling:touch !important;
			    overflow-x: hidden;
			    
			}
      		.nopadding {
				padding: 0 !important;
				margin: 0 !important;
			}
			
      		@media (max-width: 320px){
				.table_detail {
					width: 315x;
					max-width: 315px;
				}
			}
			
			/* Portrait */
			@media screen 
			  and (device-width: 360px) 
			  and (device-height: 740px) 
			  and (-webkit-device-pixel-ratio: 4) 
			  and (orientation: portrait) {
				.table_detail {
					width: 350x;
					max-width: 350px;
				}
				.container-fluid {
					#padding-left: 0px;
					#padding-right: 0px;
				}
				.container {
					padding-left: 15px;
					padding-right: 10px;
				}
				body {
					margin-bottom:75px;
				}
			}
			
			@media only screen 
			and (max-device-width : 360px) 
			and (orientation : portrait) {
				.table_detail {
					width: 350x;
					max-width: 350px;
				}
				.container-fluid {
					#padding-left: 0px;
					#padding-right: 0px;
				}
				.container {
					padding-left: 15px;
					padding-right: 10px;
				}
				body {
					margin-bottom:75px;
				}
			}
			
			@media only screen 
			and (max-device-width : 375px) 
			and (orientation : portrait) {
				.table_detail {
					width: 368x;
					max-width: 368px;
				}
				.container-fluid {
					padding-left: 0px;
					padding-right: 0px;
				}
				.container {
					padding-left: 15px;
					padding-right: 10px;
				}
				body {
					margin-bottom:75px;
				}
			}
			
			@media only screen 
			and (max-device-width : 414px) 
			and (orientation : portrait) {
				.table_detail {
					width: 408x;
					max-width: 408px;
				}
				body {
					margin-bottom:38px;
				}
			}
			
			footer {
			  position: absolute;
			  bottom: 0;
			  height: 100px;
			  width: 100%;
			}
			
			.container-background {
				background-color: #363636;
			}
			
			
			.content {
		        height: -webkit-fill-available;
		        #min-height:800px;
		    }
		    
		    a {
		    	-webkit-touch-callout: none;
				-webkit-user-select: none;
			}
			
			input {
			    background-color: transparent;
			    border: 1px solid white;
			    text-align: center;
			    color: white;
			}
      		
      		.float{
				position:fixed;
				width:40px;
				height:40px;
				top:10px;
				right:15px;
				background-color:#ccc;
				color:#333;
				border-radius:50px;
				text-align:center;
				z-index:1;
			  	display: flex;
				justify-content: space-around;
			}
			
			.my-float{
				margin-top:0px;
				align-self: center;
			}
			
			.textHeader {
				width: 100%;
				text-align:center;
				font-weight: 700;
				color: white;
			}
			
			.table-dark {
				background-color: #363636;
			}
			
			.table-dark td, .table-dark th, .table-dark thead th {
				border-color: white;
				vertical-align: middle;
				font-size: 13px;
			}
			
			.btnreport {
				min-width: 50px;
				font-size: 13px;
				padding-left: 0px;
				padding-right: 0px;
			}
			
			.nodata {
				font-family: Verdana; 
				color: #FCCE35; 
				font-size: 13px; 
				text-align: center;
			}
			
			.word-wrap {
		        word-break: break-word !important;
		    }
		    
		    .btn {
		    	text-align: justify;
		    	white-space: unset;
		    }
		    
		    .btn-link {
		    	color: #00FFFF;
		    }
		    
		    .btn-link:hover {
    			color: #008C8C
    		}
    		
    		a {
			  display: block;
			}
			
			
      	</style>
    </head>
    <body oncontextmenu="return false;" style="">
    <button class="float" id="resetHomeBtn" style="display:none;">
	<i class="fa fa-lg fa-home my-float"></i>
	</button>
    <div class="container container-background" style="margin-bottom: 40px;">   
		<div class="row" style="padding-bottom:5px;">
	    	<div class="col- textHeader">
				D&C Operations Update
			</div>
	    </div>
	    
	    <div class="row" style="padding-bottom:5px;margin-bottom:5px;margin-left:0px;margin-right:0px;">
	    	<div class="col-">
				<b style="color:white;">Date: </b><input id="daterange" class="daterangepicker-field" size="10" onfocus="blur()" onkeydown="return false"></input>
			</div>
	    </div>
	    	 
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-">
						<div class="table_detail nopadding" style="" id="operationDailyOverview" ></div>
					</div>
				</div>
				<div class="row" id="activityRow">
					<div class="col-">
						<div class="table_detail nopadding" style="" id="activitymob"></div>
					</div>
				</div>
				<br>
				<div id="reportmain" class="row" style="padding-bottom:5px;border-bottom: solid;border-color: white;border-width: thin;">
			    	<div class="col- textHeader" style="text-align:left;">
						Daily Reports
					</div>
			    </div>
				<div class="row" id="reportlist" style="padding-top: 5px;">
				
					<table id="reportlisting" class="table table-sm table-dark">
						<tbody id="myTB">
						</tbody>
					</table>
				
				</div>
			</div>
		</div>
		
	</div>
		
		
		<script type="text/javascript" src="${g_v_resources_path}/jquery/jquery-3.6.0.min.js"></script>
      	<script type="text/javascript" src="${g_v_resources_path}/script/bootstrap413/js/bootstrap.min.js"></script>
      	<script type="text/javascript" src="${g_v_resources_path}/jquery/jquery-ui-1.13.0.min.js"></script>
      	<script type='text/javascript' src="${visualizejsUtils.getJasperServerBaseUrl()}/client/visualize.js?_opt=true&baseUrl=${visualizejsUtils.getJasperServerBaseUrl()?url('utf-8')}"></script>
      	<script type="text/javascript" src="${g_v_resources_path}/datepicker/knockout-3.4.2.js"></script>
      	<script type="text/javascript" src="${g_v_resources_path}/datepicker/moment.js"></script> 	
   		<script type="text/javascript" src="${g_v_resources_path}/datepicker/js/daterangepicker.min.js"></script>
		<script type="text/javascript" src="${v_resources_path}/js/libs/d3.js"></script>
      	<script type="text/javascript" src="${v_resources_path}/js/CustomMessageDialog.js"></script>
		<script type="text/javascript" src="${v_resources_path}/js/JasperViewerEvent.js"></script>
      	<script type="text/javascript" src="styles/JrsReportController.js"></script>
      	
      	
      	
    	<script>
    		var hasDrillDownHistory;
    		var startDateParam;
    		var operationUidParam;
    		var controller;
    		var authenticate = ${visualizejsUtils.getAuthentication()};
    		var sessionParameters = ${form.getSessionParameters()};
			
			function showMessageDialog(message){
				if (this._dialog == null) this._dialog = new D3.CustomMessageDialog();
				this._dialog.show(message);
			}
					
			showMessageDialog("Loading Report");
    		  
    		  $(document).ready(function(){
    		  		var me = this;

		      		var jrsServerProperties = {
		      			auth: authenticate
		      		};
		      		
		      		var filterParams = {
		      			start_date_param: [startDateParam],
		      			user_uid: [sessionParameters.user_uid]
		      		};
					
					var reportUri = {
						containerId: "operationDailyOverviewReport",
						reportUri: "/Reports/Standard/mob/operation_daily_overview",
						filterParams: filterParams, 
						scale: "width",
						ddLinkOptions: [{
							reportUri: "/Reports/Standard/mob/day_summary",
							params: ["operation_uid_param", "start_date_param"] 
						},{
							reportUri: "/Reports/Standard/mob/activity_report_mob",
							containerId: "#activitymob",
							params: ["operation_uid_param", "start_date_param", "trigger_param"] 
						},{
							reportUri: "/Reports/Standard/mob/operation_daily_overview",
							containerId: "#operationDailyOverview",
							resetState: true,
							params: ["start_date_param"] 
						}]
					};
					
					
					// Page Initialization
					
					initPage();
		      		
		      		// Retrieve JRS client and get report
		      		visualize(jrsServerProperties, function(v){
						me.jrsClient = v;
						initDateRange();
						triggerReport();
					}, jrsRunErrorCallback);
					
					function initPage(){
						$("#resetHomeBtn").on("click", function(){
							me.operationDailyOverviewReportController.resetState();
							$("#resetHomeBtn").hide();
							$("#activitymob").hide();
							hasDrillDownHistory = true;
							var datefilterParams = {
					      		start_date_param: [startDateParam],
		      					user_uid: [sessionParameters.user_uid]
					      	};
					      	me.operationDailyOverviewReportController.reportParams(datefilterParams);
							me.operationDailyOverviewReportController.renderReport();
						});
						
						// show when has drill down report only
						$("#resetHomeBtn").hide();
						
						// page listeners
						// me.addEventListener(D3.JasperViewerEvent.OnReportCompleted, me.onReportCompletedHandler, me);
					}
					
					
					
					function hideMessageDialog(){
						if (this._dialog != null) this._dialog.hide();
					}
					
					function initDateRange(){
						$(".daterangepicker-field").daterangepicker({
						  forceUpdate: true,
						  single: true,
						  periods: ['day'],
						  timeZone: 'Australia/Perth',
						  startDate: moment().subtract(1, 'days').format('YYYY-MM-DD'),
						  endDate: '',
						  callback: function(startDate){
						  	var title = startDate.format('YYYY-MM-DD');
						    startDateParam = startDate.format('YYYY-MM-DD');
						    
						    $(this).val(title);
						    
				      		if(hasDrillDownHistory) {
				      			var datefilterParams = {
					      			start_date_param: [startDateParam],
					      			operation_uid_param: controller._reportConfig.params["operation_uid_param"]
					      		};
					      		if(controller._reportConfig.container=="#activitymob") {
					      			controller.drillUp();
					      			$('#activitymob').hide();
					      		}
					      		$('#reportlisting').hide();
					      		$('#reportmain').hide();
				      			controller.reportParams(datefilterParams);
				      			controller.renderReport();
				      			
				      		} else {
				      			var datefilterParams = {
					      			start_date_param: [startDateParam]
					      		};
					      		if(controller) {
									controller.reportParams(datefilterParams);
				      				controller.renderReport();					      		
					      		} else {
					      			triggerReport();
					      		}
				      			
				      		}
				      		var node = document.getElementById("myTB");
							while (node.hasChildNodes()) {
							  node.removeChild(node.lastChild);
							}
				      		getDDRList();
						  }
						});
					}
					
					function jrsRunErrorCallback(){
					}
					
					me.afterDrillDownReport = function(){
						$('#reportlisting').hide();
						$('#reportmain').hide();
						if(this._reportConfig.params.trigger_param == 'true'){
								$("#activitymob").show();
							}
					}
					
					me.onReportCompletedHandler = function(e){
						hideMessageDialog();
						if(this.hasDrillDownHistory()){
							hasDrillDownHistory = true;
							
							if(this._reportConfig.params.trigger_param == 'false') {
								$("#activitymob").hide();
								controller.drillUp();
							}else if(this._reportConfig.params.trigger_param == 'true'){
								
								var abc = $("#activitymob").offset().top;
								
								/*$('html, body').animate({
							        scrollTop: $("#activitymob").offset().top
							    }, 500);*/
								
							}
							$("#resetHomeBtn").show();
							$('#reportlisting').hide();
							$('#reportmain').hide();
						}else{
							$("#resetHomeBtn").hide();
							$("#activitymob").hide();
							hasDrillDownHistory = false;
							$('#reportlisting').show();
							$('#reportmain').show();
						}
					}
					function triggerReport(){
						var filterParams;
						if (startDateParam) {
							filterParams = {
								start_date_param: [startDateParam],
								user_uid: [sessionParameters.user_uid]
						    }
						    reportUri.filterParams = filterParams;
						}
						
						var containerId = "#operationDailyOverview";
						var config = {
							afterDrillDownCallback:me.afterDrillDownReport,
							jrsReportCompleted: me.onReportCompletedHandler
						};
						var ctrller = new D3.JrsReportController($(containerId), me.jrsClient, reportUri, config);
						ctrller.renderReport();
						me.operationDailyOverviewReportController = ctrller;
						controller = ctrller;
					}
					
					function getDDRList() {
						var formData = {
				        	method: "get_ddr_list",
				        	dateParam: startDateParam
						};
						
				        $.ajax({
				        	dataType: "json",
				        	type: "POST",
				        	url: "webservice/mobilereportloaderservice.html",
				        	data: {data: JSON.stringify(formData)}
				        }).done(function(response, textStatus, jqXHR) {
				            renderDDRList(response);
				        }).fail(function() {
				        	
				        });
					}
					
					function renderDDRList(response) {
					
						var noreport = true;
						var data = response.ddrlist;
						if(data.length > 0) {
						
							for(var i = 0; i < data.length; i++) {
							    var obj = data[i];
								var operationName = obj.operationName + obj.qcFlag;
								var reportList = obj.ReportList;
								
								if(reportList.length > 0) {
									$('#myTB').append('<div class="col-" style="font-size: 13px;"> ' + operationName + '</div>');
									var report = reportList.DDRReportList;
									$('#myTB').append('<div id="op' + i +'" class="col- word-wrap">');
									
									for(var j = 0; j < reportList.length; j++) {
										var reports = reportList[j];
										var report = reports.DDRReportList;
										
										var reportDisplayName = report.reportDisplayName;
										var fileUrl = "abaccess/download/" + encodeURIComponent(replaceInvalidCharInFileName(reportDisplayName)) + "." + report.fileExtension + "?handlerId=ddr&reportId=" + encodeURIComponent(report.reportFilesUid);
										$('#op' +i).append('<a href="'+ fileUrl +'" target="_blank" class="btn btn-link btn-sm btnreport" role="button"> ' + reportDisplayName + ' </a>');	
									}
									
									$('#myTB').append('</div>');
									noreport = false;
								}
							    
							}
						}
						
						if(noreport) {
							$('#myTB').append('<div class="nodata">No report is generated</div>');
						}
					}
					
					function replaceInvalidCharInFileName(name) {
						if(name == null) return null;
						return name.replace(/[\/\\:?*><|"]/g,"_");
					}
					
		       });
    	</script>
    </body>
    
</html> 