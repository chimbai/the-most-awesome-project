<html>
    <head><style>
      body {
        margin: 0px;
        padding: 0px;
      }
      a {
      	font-family : Verdana;
      	font-size : 10pt;
      }
    </style>
    <script type='text/javascript' src="${visualizejsUtils.getJasperServerBaseUrl()}/client/visualize.js?baseUrl=${visualizejsUtils.getJasperServerBaseUrl()?url}"></script>
	<script type="text/javascript" src="script/d3/schematic/jquery-1.11.2.js"></script>
	<script type="text/javascript" src="script/d3/schematic/jquery-ui.js"></script>
	<script type="text/javascript" src="script/d3/schematic/jquery.layout-latest.js"></script>
	<script type="text/javascript" src="script/generic/d3.js"></script>
	<script type="text/javascript" src="script/d3/schematic/accordion.js"></script>
	
	<link rel="stylesheet" href="script/d3/dist/themes/default/style.css" />
	<link rel="stylesheet" href="script/d3/schematic/reportFilter.css" />
	<script type="text/javascript" src="script/d3/dist/jstree.js"></script>
 		 
    </head>
    <body>
    	<script >
    		authenticate = ${visualizejsUtils.getAuthentication()};
    		
    		$(document).ready(function () {
		        $('body').layout({
		            applyDefaultStyles: true,
		
		            west__resizable: true,
		            west__minSize: 320,
		            west__closable: false,
		        });
		    });
    	</script>
    	
    	<div id='mainContainer' class="ui-layout-center">
		</div>
		<div id='filterContainer' class="ui-layout-west">
			
		
    	<script type="text/javascript" src="script/jasper/ReportViewer.js"></script>    	
    	<script type="text/javascript" src="script/jasper/PreFilter.js"></script>
    	<script type="text/javascript" src="script/jasper/ContextFilter.js"></script>
		<script src="script/jasper/WwoFilter.js"></script>
		<script src="script/jasper/sales_poc_test_report.js"></script>
		
    </body>
</html>