package com.idsdatanet.d2.common.util.field;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;

import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.core.model.CustomDescriptionMatrix;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class FtrCodeFieldHandler extends GenericFieldHandler {
    
    @Override
    public Object translate(Object value, Object object, BaseCommandBean commandBean, ExcelMapping mapping, List<Object> objList, Workbook book, Cell cell) throws Exception {
        if (value != null && value instanceof String) {
            String sql = "FROM CustomDescriptionMatrix WHERE (isDeleted = false OR isDeleted IS NULL) AND remarksCode = :remarksCode ";
            List<CustomDescriptionMatrix> CustomDescriptionMatrixList = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(sql, "remarksCode", (String) value);
            if (!CustomDescriptionMatrixList.isEmpty()) {
                return CustomDescriptionMatrixList.get(0).getCustomDescriptionMatrixUid();
            }
        }
        return value;
    }
}