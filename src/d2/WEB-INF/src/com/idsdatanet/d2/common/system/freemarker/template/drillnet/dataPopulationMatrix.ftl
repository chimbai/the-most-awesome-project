<script type="text/javascript">
<!--
	function goToModule(url) {
		window.location = url;
	}
	
	function alertAccessDenied() {
		alert('Your security setting does not allow access to the selected day.');
	}
	
	$(document).ready(function() {
		if(! $.browser.msie) {
			$("#ContentWrapperType1").height(self.innerHeight - 170 + "px");
			$("#ContentWrapperType1").width(self.innerWidth - 35 + "px");
		}
	});
-->
</script>

<#if form.getDays()??>
	<div id="matrixTitle" align="center">Click on the intersecting elements in the matrix to go to the corresponding day and module.</div>
	<div id="matrixDiv" align="center">
		<table id="matrixTable">
			<tr>
				<td class="header"><strong>Modules</strong></td>
				<#if form.getDays()??>
					<#list form.getDays() as day>
						<#if day.getDailyUid() == _sys_menu.selected_dailyUid?default('')>
							<td class="header-current-day">${day.getReportNumber()}</td>
						<#elseif userSession.withinAccessScope(day)>
							<td class="header tooltip" title="go to day ${day.getReportNumber()!""}"><a href="javascript:d2ScreenManager.submitSummary('${day.getDailyUid()!""}');">${day.getReportNumber()}</a></td>
						<#else>
							<td class="header" onclick="alertAccessDenied();">${day.getReportNumber()}</td>		
						</#if>
					</#list>
				</#if>
				<td class="header"><strong>Modules</strong></td>
			</tr>
			<#-- data is a linkedHashMap -->
			<#list form.data?keys as section>
				<#if section != "">
					<tr><td class="section"><b>${section}</b></td><td class="section" colspan="${form.days?size}">&nbsp;</td><td class="section"><b>${section}</b></td></tr>
				</#if>
				<#list form.data[section] as data>
					<#if data.getModuleName() != "Qc-Status">
						<tr>
							<td class="module-name">${data.getModuleName()!""}</td>
							<#if form.getDays()??>
								<#list form.getDays() as day>
									<#if data.exist(day.getDailyUid()!"")>
										<td class="<#if day.getDailyUid() == _sys_menu.selected_dailyUid?default('')>green-current-day<#else>green</#if> tooltip" title="${data.getModuleName()!""} on day ${day.getReportNumber()!""}"
										<#if userSession.withinAccessScope(day)>
											onclick="goToModule('${appendParam(data.getUrl(), 'gotoday=${day.getDailyUid()}')}');"
										<#else>
											onclick="alertAccessDenied();"
										</#if>
										>&nbsp;</td>
									<#else>
										<td class="<#if day.getDailyUid() == _sys_menu.selected_dailyUid?default('')>red-current-day<#else>red</#if> tooltip" title="${data.getModuleName()!""} has no data on day ${day.getReportNumber()!""}" 
										<#if userSession.withinAccessScope(day)>
											onclick="goToModule('${appendParam(data.getUrl(), 'gotoday=${day.getDailyUid()}')}');"
										<#else>
											onclick="alertAccessDenied();"
										</#if>
										>&nbsp;</td>
									</#if>							
								</#list>
							</#if>
							<td class="module-name">${data.getModuleName()!""}</td>
						</tr>
					<#else>
						<tr>
							<td class="module-name">${data.getModuleName()!""}</td>
							<#if form.getDays()??>
								<#list form.getDays() as day>							
									<#if day.getDailyUid() == _sys_menu.selected_dailyUid?default('')>
										<#if day.getQcFlag()?default('') == '1'>
										<td title="Rig side QC done" onclick="goToModule('${appendParam(data.getUrl(), 'gotoday=${day.getDailyUid()}')}');" style="border:1px solid #A52A2A">
											<font color="#A52A2A">									
												RQC
											</font>
										</td>
										<#elseif day.getQcFlag()?default('') == '2'>
										<td title="Town side QC done" onclick="goToModule('${appendParam(data.getUrl(), 'gotoday=${day.getDailyUid()}')}');" style="border:1px solid #008000">
											<font color="#008000">
												TQC
											</font>
										</td>
										<#else>
										<td title="Not Qc'd" onclick="goToModule('${appendParam(data.getUrl(), 'gotoday=${day.getDailyUid()}')}');" style="border:1px solid #A52A2A">									
											-									
										</td>
										</#if>
									<#elseif userSession.withinAccessScope(day)>
										<#if day.getQcFlag()?default('') == '1'>
										<td title="Rig side QC done" onclick="goToModule('${appendParam(data.getUrl(), 'gotoday=${day.getDailyUid()}')}');" style="border:1px solid #A52A2A">
											<font color="#A52A2A">									
												RQC	
											</font>
										</td>
										<#elseif day.getQcFlag()?default('') == '2'>
										<td title="Town side QC done" onclick="goToModule('${appendParam(data.getUrl(), 'gotoday=${day.getDailyUid()}')}');" style="border:1px solid #008000">
											<font color="#008000">
												TQC
											</font>
										</td>
										<#else>
										<td title="Not Qc'd" onclick="goToModule('${appendParam(data.getUrl(), 'gotoday=${day.getDailyUid()}')}');" style="border:1px solid #A52A2A">									
											-									
										</td>
										</#if>
									<#else>
										<#if day.getQcFlag()?default('') == '1'>
										<td title="Rig side QC done" onclick="alertAccessDenied();" style="border:1px solid #A52A2A">
											<font color="#A52A2A">									
												RQC	
											</font>
										</td>
										<#elseif day.getQcFlag()?default('') == '2'>
										<td title="Town side QC done" onclick="alertAccessDenied();" style="border:1px solid #008000">
											<font color="#008000">
												TQC
											</font>
										</td>
										<#else>
										<td title="Not Qc'd" onclick="alertAccessDenied();" style="border:1px solid #A52A2A">									
											-									
										</td>
										</#if>
									</#if>						
								</#list>
							</#if>
							<td class="module-name">${data.getModuleName()!""}</td>
						</tr>
					</#if>
				</#list>
			</#list>		
		</table>
	</div>
<#else>
	<div id="matrixTitle" align="center">This module requires that a well operation is selected which contains at least one day in the operation.</div>
</#if>

<#function appendParam url param>
	<#if url?index_of("?") &gt; 0>
		<#return url + "&" + param />
	<#else>
		<#return url + "?" + param />
	</#if>
</#function> 