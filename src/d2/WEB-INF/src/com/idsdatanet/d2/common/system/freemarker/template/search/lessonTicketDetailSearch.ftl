<!DOCTYPE HTML>
<html>
<body style="overflow-x: hidden; overflow-y: hidden;background-color:#ffffff;">
<#assign v_resources_path = "${form.getVirtualResourcePath(1)}/htmllib"/>

<#include "/core/defaultJavascriptAndStyleTemplate.ftl" />
<#include "/templateHelper.ftl"/>
<link rel="stylesheet" type="text/css" href="${v_resources_path}/search/lessonTicketDetailSearch.css"></script>
<script type="text/javascript" src="${v_resources_path}/search/lessonTicketDetailSearch.js"></script>

<style>
	.iTextBox {
	    width: 20%;
	    height: 30px;
	    display: inline;
	}

	input[type=button] {
	    width: 120px;
	    margin-left: 35px;
	    display: inline;
	}
	
	.pageNoWithLink{
		cursor:pointer;
		color:darkblue;
	}
	
	.pageNoWithoutLink{
		color:blue;
		font-weight:bold;
	}
	
	.highlighter{
		color:orange;
		font-weight:bold;
	}
	
	.outputDiv{
		height: 95vh;
		overflow:auto;
	}
	
	.zipIcon {
	  background-image: url("images/archive.gif");
	  width:16px;
	  height:16px;
	  vertical-align:middle;
	  display:inline-block;
	  border:0px;
	  margin-left:4px;
	  cursor: pointer;
	  margin-top: 2px;
	}
	
</style>
<script type="text/javascript">
	$(document).ready(function() {
		searchObject.initData(${form.collectOutputJson()});
		searchObject.renderTo($("#output")) ;
		searchObject.render(true);
	});
	$(window).resize(function(){
		searchObject.onWindowResize();
	});
</script>
	<div class="BaseNodesGroupContainerHeader">
		<div class="BaseNodesGroupContainerHeaderText">${getFormTitle("")}</div>
	</div>
	<div id="output" class="outputDiv"/>
</body>
</html>