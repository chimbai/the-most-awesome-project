<!DOCTYPE HTML>
<html>
<body style="overflow-x: hidden; overflow-y: hidden;background-color:#ffffff;">
<#assign v_resources_path = "${form.getVirtualResourcePath()}/htmllib"/>

<#include "/core/defaultJavascriptAndStyleTemplate.ftl" />
<#include "/core/d2Helper.ftl" />
<script type="text/javascript" src="scripts/d2.js"></script>
<script type="text/javascript" src="scripts/uuid.js"></script>
<style>
	.iTextBox {
	    width: 20%;
	    height: 30px;
	    display: inline;	   
	}

	input[type=button] {
	    width: 120px;
	    margin-left: 35px;
	    display: inline;
	}
	
	.pageNoWithLink{
		cursor:pointer;
		color:darkblue;
		
	}
	
	.pageNoWithoutLink{
		color:blue;
		font-weight:bold;
	}
	
	.highlighter{
		color:orange;
		font-weight:bold;
		
	}
</style>
<script>
	function updateMessage() {
		$.get("crawlerprogress.html?" + new UUID(), function(html){
		replace('partial', html);
	});
	}

	function replace(elementId, content) {
		$("#" + elementId).empty();
		if(typeof content!=='undefined' && content.length>0) content = content.replace(/;/g,"<br/>");
		$("#" + elementId).append(content);
	}


	$(document).ready(function() {
		 updateMessage();		 
		 populateScreen(${form.getIndexerCommandBean()});
		 $("#dcrScope").change(function(){
		 	var dcrScope = $(this).val();
		 	if(dcrScope.length>0){
		 		$("#dcrScopeValue").val("");
		 		$("#dcrScopeValue").show();
		 	}else{
		 		$("#dcrScopeValue").hide();
		 	}
		 	
		 	var param = $("#dcrBeanParamDetails").val();
		 	var data = JSON.parse(decodeURIComponent(param));			
			populateBeanDropdownList(data,dcrScope);	
		 });
	});	
	
	function populateScreen(data){
		var me = this;
		
		$("#dcrBeanParamDetails").val(encodeURIComponent(JSON.stringify(data)));
		populateBeanDropdownList(data,"");		
		var dateFrom = $("#dcrDateFrom");
		var dateTo = $("#dcrDateTo");
		dateFrom.datepicker({dateFormat: 'd MM yy' });
		dateTo.datepicker({dateFormat: 'd MM yy' });
	}
	
	function populateBeanDropdownList(data,dcrScope){
		var inputSelect = $("#dcrBeanSelection");
		if(typeof data!=='undefined' && inputSelect!=null){
			inputSelect.empty();
			$.each(data,function(key, object) {
				if(dcrScope.length==0 || dcrScope==object){
					var option = document.createElement("option");
					option.value = key;
					var fieldLabel =key;
					option.appendChild(document.createTextNode(fieldLabel));
					inputSelect.append(option);
				}
			});
			if(inputSelect.children('option').length==0){
				var option = document.createElement("option");
				option.appendChild(document.createTextNode("No bean found"));
				inputSelect.append(option);
				inputSelect.prop('disabled', 'disabled');
			}else{
				inputSelect.prop('disabled', false);
			}
		}		
	}
		
	function _submitSimpleAjax(sourceButton, labelWhenSubmit, systemAction){
	if($(sourceButton).attr("simpleAjax.state") != "waiting"){
		$(sourceButton).val(labelWhenSubmit);		
		
		$(sourceButton).ajaxError(function(request, settings){
			alert("Unable to get a response from server, please refresh current page");
					$(this).val($(sourceButton).attr("simpleAjaxOriginalLabel"));
			});
			
		var data = "simpleAjaxAction=" + systemAction + "&action=" + $(sourceButton).attr("simpleAjaxAction") + "&sourceElementId=" + sourceButton.id ;
		if ($("#dcrBeanSelection").val()!=null) {
			data += "&dcrBeanSelection=" + $("#dcrBeanSelection").val();
		}
		
		var scope = $("#dcrScope").val();
		var scopeValue = $("#dcrScopeValue").value;		
		if (scope!=null) 
			data += "&scope=" + scope;

		if (scopeValue!=null)
			data += "&scopeValue=" + scopeValue;
				
		var dateFrom = $("#dcrDateFrom").datepicker("getDate");
		var dateTo = $("#dcrDateTo").datepicker("getDate");		
		if (dateFrom!=null) 
			data += "&dcrDateFrom=" + $.datepicker.formatDate("dd MM yy", dateFrom);

		if (dateTo!=null)
			data += "&dcrDateTo=" + $.datepicker.formatDate("dd MM yy", dateTo);
		
		$.getJSON(document.URL, data, function(json){
			if("true" == json.responseSet){
				if(json.responseButtonLabel != ""){
					$(sourceButton).val(json.responseButtonLabel);
				}else{
					$(sourceButton).val($(sourceButton).attr("simpleAjaxOriginalLabel"));
				}
				if(json.responseAlert != "") alert(json.responseAlert);
				
				if(json.checkPendingAction == "true"){
					$(sourceButton).attr("simpleAjax.state", "waiting");
					var checkInterval = 10000;
					if(json.checkPendingActionInterval > 0) checkInterval = json.checkPendingActionInterval;
					setTimeout("checkSimpleAjaxPendingAction(" + checkInterval + ",'" + $(sourceButton).attr("id") + "','" + $(sourceButton).attr("simpleAjaxAction") + "','" + json.actionId + "')", checkInterval);
				}
			}else{
				$(sourceButton).val($(sourceButton).attr("simpleAjaxOriginalLabel"));
			}
		});
	}
}

function checkSimpleAjaxPendingAction(checkInterval, sourceElementId, action, actionId){
	var sourceButton = $("#" + sourceElementId);
	
	if(sourceButton.attr("ajax-highlight") == undefined){
		sourceButton.attr("ajax-highlight", "0");
	}

	if(sourceButton.attr("ajax-highlight") == "0"){
		sourceButton.attr("style","color:blue");
		sourceButton.attr("ajax-highlight", "1");
	}else{
		sourceButton.attr("style","color:white");
		sourceButton.attr("ajax-highlight", "0");
	}
	
	$.getJSON(document.URL, {simpleAjaxAction: "getRefreshInstruction", action: action, actionId: actionId, sourceElementId: sourceElementId}, function(json){
		if(json.refreshAlert != "") alert(json.refreshAlert);
		if("checkAgain" == json.refreshInstruction){
			updateMessage();
			setTimeout("checkSimpleAjaxPendingAction(" + checkInterval + ",'" + sourceElementId + "','" + action + "','" + actionId + "')", checkInterval);
		}else if("reloadScreen" == json.refreshInstruction){
			window.location.href = window.location.href;
		}else if("stopChecking" == json.refreshInstruction){
			sourceButton.attr("simpleAjax.state", "");
			sourceButton.val(sourceButton.attr("simpleAjaxOriginalLabel"));
			sourceButton.attr("style","color:white");
		} 
	});
}
</script>
<input type="hidden" name="dcrBeanParamDetails" id="dcrBeanParamDetails"/>


<table class="BaseNodesGroupContainerHeader" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td><div class="BaseNodesGroupContainerHeaderText">Data Crawler</div></td>
		</tr>		
	</tbody>
</table>
<table width="99%" align="center">
	<#if form.isEnabled()== 1>
	<tr>
		<td width="60%">
			<table style="width:100%; height:250px;border:1px solid black;box-shadow: 0px 5px 5px #888888;">
				<tbody>
					<tr>
						<td>Date Range (Default: any date)</td>
						<td>
							<input id="dcrDateFrom" name="dcrDateFrom" type="text"/> - <input id="dcrDateTo" name="dcrDateTo" type="text"/>
						</td>
					</tr>	
					<tr valign="top">
						<td>Scope</td>
						<td>
							<select name="scope" id="dcrScope">
								<option value="">All</option>
								<option value="wellUid">Well</option>
								<option value="wellboreUid">Wellbore</option>
								<option value="operationUid">Operation</option>
							</select>
							<input type="text" id="dcrScopeValue" name="scopeValue" placeHolder="Default: current selected" style="display:none"/>
						</td>
					</tr>	
					<tr valign="top">
						<td>Command Bean (Default: all)</td>
						<td>
							<select name="dcrBeanSelection" id="dcrBeanSelection" multiple size="10"></selct>
						</td>
					</tr>					
					<tr valign="top">
						<td>&nbsp;</td>
						<td>
							<@showSimpleAjaxButton name="startIndex" label="Start" action="startIndex"/>
						</td>
					</tr>					
				</tbody>
			</table>	
		</td>
		<td>
			<div id="partial" style="overflow: auto; width:100%; height:250px;border:1px solid black;box-shadow: 0px 5px 5px #888888;">
			
			</div>
		</td>
		<#else>
		<td>
			<span style="color:red"><b>Please enable Elastic search client</b></span>
		</td>
		</#if>
	</tr>
</table>
</body>
</html> 
    
    
    



