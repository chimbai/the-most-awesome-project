package com.idsdatanet.d2.common.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.idsdatanet.d2.common.util.field.GenericFieldHandler;

public class ExcelMapping {
    protected String name = null;
    protected String ids = null;
    protected boolean mandatory = false;
    protected String type = "String";
    protected GenericFieldHandler fieldHandler = null;
    protected String defaultUnit = null;
    
    private Integer column = null; // Never initialized b4 run time

    private int columnIndex = -1; // Used in Cell Mapping
    private int rowIndex = -1; // Used in Column Mapping or Cell Mapping
    private int unitColumn = -1; // Used in Column Mapping
    
    public static final List<String> UOM_TYPE_LIST = Collections.unmodifiableList(Arrays.asList("DoubleWithUnit", "DoubleIgnoreUnit"));
    public static final String DEFAULT_TYPE = "String";
    public static final boolean DEFAULT_MANDATORY = false;
    public static final int UNDEFINED = -1;
    
    public ExcelMapping() {
    }
    
    public ExcelMapping(String name, String ids) {
        this.init(name, ids, DEFAULT_MANDATORY, DEFAULT_TYPE);
    }
    
    public ExcelMapping(String name, String ids, String type) {
        this.init(name, ids, DEFAULT_MANDATORY, type);
    }
    
    public ExcelMapping(String name, String ids, boolean mandatory) {
        this.init(name, ids, mandatory, DEFAULT_TYPE);
    }
    
    public ExcelMapping(String name, String ids, boolean mandatory, String type) {
        this.init(name, ids, mandatory, type);
    }
    
    public void init(String name, String ids, boolean mandatory, String type) {
        this.name = name;
        this.ids = ids;
        this.mandatory = mandatory;
        this.type = type;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getIds() {
        return ids;
    }
    public void setIds(String ids) {
        this.ids = ids;
    }
    public boolean isMandatory() {
        return mandatory;
    }
    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }
    public Integer getColumn() {
        return column;
    }
    public void setColumn(Integer column) {
        this.column = column;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public GenericFieldHandler getFieldHandler() {
        return fieldHandler;
    }
    public void setFieldHandler(GenericFieldHandler fieldHandler) {
        this.fieldHandler = fieldHandler;
    }

    public String getDefaultUnit() {
        return defaultUnit;
    }

    public void setDefaultUnit(String defaultUnit) {
        this.defaultUnit = defaultUnit;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getUnitColumn() {
        return unitColumn;
    }

    public void setUnitColumn(int unitColumn) {
        this.unitColumn = unitColumn;
    }
}