<html>
<body>

<p><b>IDS Auto Report Service - Well Control Notification</b></p>

<p><i>presented for NOBLE</i></p>

<#if BopTestList?exists>
	<#list BopTestList?values as item>
		<#if item["logStatus"] == "OVERDUE">
			<p>The BOP for <b>${item["rigName"]}</b> was last tested on ${item["testingDatetime"]} (BOP Interval: ${item["bopInterval"]}). It has exceeded its Next BOP Test Due Date: <b>${item["nextTestDatetime"]}</b>.</p>
		<#elseif item["logStatus"] == "SOON">
			<p>The BOP for <b>${item["rigName"]}</b> was last tested on ${item["testingDatetime"]} (BOP Interval: ${item["bopInterval"]}). The Next Test Due Date is <b>${item["nextTestDatetime"]}</b>.</p>
		</#if>
	</#list>
</#if>

<p>&nbsp;</p>

-------------------------------------------		
<p>
User is working on <br/>
User workstation ID: 127.0.0.1<br/>
Server Date and Time: <#if serverDatetime?exists>${serverDatetime}</#if><br/>
Copyright 2002-2012 Independent Data Services
</p>

</body>
</html>