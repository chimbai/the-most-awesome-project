<html>
<head>
<title>IDS WesternZagros Daily Management Report</title>
</head>
<body>
Dear Sir/Madam, 
<p></p>
<#if emailContentItemList?exists>
<#list emailContentItemList? keys as operationUid>

		<p>
			<table border="1" style="font-size:10pt">
				<tr>
					<th colspan=6><b>Operation: ${emailContentItemList[operationUid]["operationName"]} (Day#${emailContentItemList[operationUid]["reportNumber"]} - ${emailContentItemList[operationUid]["reportDatetime"]})</b></th>
				</tr>
				
				<tr>
					<th align="left">Rig Name: </th><td>${emailContentItemList[operationUid]["rigName"]}</td>
					<th align="left">AFE Amount: </th><td>${emailContentItemList[operationUid]["totalAfe"]}</td>
					<th align="left">Spud Date: </th><td>${emailContentItemList[operationUid]["spudDate"]}</td>
				</tr>
				<tr>
					<th align="left">DSV Name: </th><td>${emailContentItemList[operationUid]["snrdrillsupervisor"]}</td>
					<th align="left">Cost to Date: </th><td>${emailContentItemList[operationUid]["cumCost"]}</td>
					<th align="left">Days From Spud: </th><td>${emailContentItemList[operationUid]["daysFromSpud"]}</td>
				</tr>
				<tr>
					<th colspan=3 align="left">24 Hr Summary: </th>
					<th align="left">24 Hr Progress: </th><td colspan=2>${emailContentItemList[operationUid]["progress"]}</td>
				</tr>
				<tr>
					<td colspan=6>${emailContentItemList[operationUid]["reportPeriodSummary"]}</td>
				</tr>
				<tr>
					<th colspan=3 align="left">24 Hr Plan: </th>
					<th align="left">Planned MD/TVD: </th>
					<td colspan=2>
						${emailContentItemList[operationUid]["plannedTdMdMsl"]}
						<#if emailContentItemList[operationUid]["plannedTdMdMsl"]!="" || emailContentItemList[operationUid]["plannedTdTvdMsl"]!="">
							/
						</#if>
					${emailContentItemList[operationUid]["plannedTdTvdMsl"]}
					</td>
				</tr>
				<tr>
					<td colspan=6>${emailContentItemList[operationUid]["reportPlanSummary"]}</td>
				</tr>
				<tr>
					<th colspan=6 align="left">Status at 06:00 Hrs: </th>
				</tr>
				<tr>
					<td colspan=6>${emailContentItemList[operationUid]["reportCurrentStatus"]}</td>
				</tr>
				<tr>
					<th colspan=6 align="left">HSE Comment: </th>
				</tr>
				<#if emailContentItemList[operationUid]["hseList"]?exists>
					<#list emailContentItemList[operationUid]["hseList"]? keys as hseIncidentUid>
						<tr>
							<td colspan=6>${emailContentItemList[operationUid]["hseList"][hseIncidentUid]["hseShortDescription"]}</td>
						</tr>
					</#list>
				</#if>
				
			</table>
		</p>	
		<p></p>	
</#list>
</#if>
-----------------------------------------
<p>Please do not reply to this email, as this inbox is not monitored</p>
<p><b>Regards, IDS</b></p>
</body>
</html>
