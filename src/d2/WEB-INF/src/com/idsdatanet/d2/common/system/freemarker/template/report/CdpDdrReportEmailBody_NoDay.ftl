<html>
<body>
This is an <b>automated message</b>.
<br>

<#if day?exists>
	<br> The following operation has no new DDR created: 
	<#list day?keys as key>
		<br>${day[key]}
	</#list>
	<br>
</#if>

<p>Thank you.</p>
<p><b>IDS Datanet Service</b></p>
</body>
</html>