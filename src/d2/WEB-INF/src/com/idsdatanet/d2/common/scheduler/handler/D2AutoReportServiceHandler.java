package com.idsdatanet.d2.common.scheduler.handler;

import org.apache.commons.lang.StringUtils;

import com.idsdatanet.d2.common.scheduler.jobs.CheckChangePassword;
import com.idsdatanet.d2.common.scheduler.jobs.CleanUpDvdSummary;
import com.idsdatanet.d2.common.scheduler.jobs.DBCascadeDelete;
import com.idsdatanet.d2.common.scheduler.jobs.DataImportExportHelper;
import com.idsdatanet.d2.common.scheduler.jobs.FileBridgeJob;
import com.idsdatanet.d2.common.scheduler.jobs.FileBridgeRESTJob;
import com.idsdatanet.d2.common.scheduler.jobs.FileBridgeWebSocketJob;
import com.idsdatanet.d2.common.scheduler.jobs.JobEngine;
import com.idsdatanet.d2.common.scheduler.jobs.PubSubJob;
import com.idsdatanet.d2.common.scheduler.jobs.STTJob;
import com.idsdatanet.d2.common.scheduler.jobs.WitsmlMessageQuery;
import com.idsdatanet.d2.core.model.SchedulerJobDetail;
import com.idsdatanet.d2.core.search.elastic.listener.IndexerWorker;
import com.idsdatanet.d2.core.stt.active.ActiveSTTWorker;
import com.idsdatanet.d2.drillnet.activityCodeSyncLogger.ActivityCodeSchedulerJob;
import com.idsdatanet.d2.drillnet.report.CombinedDDRReportModule;
import com.idsdatanet.d2.drillnet.report.ManagementReportScheduleParams;
import com.idsdatanet.d2.drillnet.report.ReportAutoEmail;
import com.idsdatanet.d2.drillnet.report.schedule.bop.NextBopTestReportExtractionService;
import com.idsdatanet.d2.drillnet.report.schedule.corportatedrillingportal.DailyReportExtractionService;
import com.idsdatanet.d2.drillnet.report.schedule.scheduledCustomMail.ScheduledCustomMailService;
import com.idsdatanet.d2.drillnet.report.schedule.scheduledCustomMail.ScheduledManagementReportMailService;
import com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.dailyReport.DailyManagementReportService;
import com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.dailyReport.DailyReportService;
import com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.dailyReport.DailyReportServiceNoAct;
import com.idsdatanet.d2.drillnet.report.schedule.scheduledMail.dailyReport.DefaultReportService;
import com.idsdatanet.d2.drillnet.wellExternalCatalog.OMVHipWellExternalCatalogManager;
import com.idsdatanet.d2.idsinvoice.stt.InvSTTJob;
import com.idsdatanet.d2.infra.scheduler.ScheduledAutoReportParams;
import com.idsdatanet.d2.infra.scheduler.handler.AutoServiceHandler;
import com.idsdatanet.d2.safenet.lessonTicketReach.schedule.LessonTicketReachEmailSchedule;
import com.idsdatanet.depot.edm.scheduler.job.EDMSyncJob;
import com.idsdatanet.depot.edm.scheduler.job.EDMSyncOnQCStatusJob;
import com.idsdatanet.depot.goldbox.GoldboxManager;
import com.idsdatanet.depot.hip.scheduler.job.OMVHIPGDBJob;
import com.idsdatanet.depot.idsbridge.scheduler.job.IDSBridgeSyncJob;
import com.idsdatanet.depot.witsml.publisher.WitsmlPublisherManager;
import com.idsdatanet.depot.wellview.WellViewAutoImporter;
import com.idsdatanet.depot.wellview.WellViewIndexer;
import com.idsdatanet.depot.wellview.WellViewSyncImporter;
import com.idsdatanet.depot.witsml.scheduler.job.D2CustomWitsmlExportJob;
import com.idsdatanet.depot.witsml.scheduler.job.KDI24HrsSnapshot;
import com.idsdatanet.depot.witsml.scheduler.job.KDIOpsReport;
import com.idsdatanet.depot.witsml.scheduler.job.RigStateImport;
import com.idsdatanet.depot.witsml.scheduler.job.RsdAutoStartNewDay;

public class D2AutoReportServiceHandler implements AutoServiceHandler {
	public void execute(Object serviceBean, String loginUser, SchedulerJobDetail jobDetail) throws Exception {
		this.executeInternal(serviceBean, loginUser, jobDetail);
	}
	
	private void executeInternal(Object serviceBean, String loginUser, SchedulerJobDetail jobDetail) throws Exception {
		ScheduledAutoReportParams params = getParams(jobDetail);
		if (serviceBean instanceof DailyReportService) {
			//get list of reports to process
			DailyReportService report = (DailyReportService) serviceBean;
			if (StringUtils.isNotEmpty(params.getLanguage())) report.getReportAutoEmail().setLanguage(params.getLanguage());
			else report.getReportAutoEmail().setLanguage("en");
			if (StringUtils.isNotEmpty(params.getAutoEmail().getEmailListKey())) report.getReportAutoEmail().setEmailListKey(params.getAutoEmail().getEmailListKey());
			if (StringUtils.isNotEmpty(params.getAutoEmail().getEmailSubject())) report.getReportAutoEmail().setEmailSubject(params.getAutoEmail().getEmailSubject());
			if (StringUtils.isNotEmpty(params.getAutoEmail().getContent())) {
				report.getReportAutoEmail().setContent(params.getAutoEmail().getContent());
				report.getReportAutoEmail().setEmailContentTemplateFile(null);
			}
			report.run();
		} else if (serviceBean instanceof DailyReportServiceNoAct) {
			//return list of report if activity exist
			DailyReportServiceNoAct reportIfExist = (DailyReportServiceNoAct) serviceBean;
			if (StringUtils.isNotEmpty(params.getLanguage())) reportIfExist.getReportAutoEmail().setLanguage(params.getLanguage());
			else reportIfExist.getReportAutoEmail().setLanguage("en");
			if (StringUtils.isNotEmpty(params.getAutoEmail().getEmailListKey())) reportIfExist.getReportAutoEmail().setEmailListKey(params.getAutoEmail().getEmailListKey());
			if (StringUtils.isNotEmpty(params.getAutoEmail().getEmailSubject())) reportIfExist.getReportAutoEmail().setEmailSubject(params.getAutoEmail().getEmailSubject());
			if (StringUtils.isNotEmpty(params.getAutoEmail().getContent())) {
				reportIfExist.getReportAutoEmail().setContent(params.getAutoEmail().getContent());
				reportIfExist.getReportAutoEmail().setEmailContentTemplateFile(null);
			}
			reportIfExist.run();
		}else if (serviceBean instanceof DailyManagementReportService) {
			DailyManagementReportService mgtReport = (DailyManagementReportService) serviceBean;
			if (StringUtils.isNotEmpty(params.getAutoEmail().getEmailListKey())) mgtReport.getReportAutoEmail().setEmailListKey(params.getAutoEmail().getEmailListKey());
			if (StringUtils.isNotEmpty(params.getAutoEmail().getEmailSubject())) mgtReport.getReportAutoEmail().setEmailSubject(params.getAutoEmail().getEmailSubject());
			if (StringUtils.isNotEmpty(params.getAutoEmail().getContent())) {
				mgtReport.getReportAutoEmail().setContent(params.getAutoEmail().getContent());
				mgtReport.getReportAutoEmail().setEmailContentTemplateFile(null);
			}
			mgtReport.run();	
		} else if (serviceBean instanceof DefaultReportService) {
			DefaultReportService defaultReport = (DefaultReportService) serviceBean;
			if (StringUtils.isNotEmpty(params.getAutoEmail().getEmailListKey())) defaultReport.getReportAutoEmail().setEmailListKey(params.getAutoEmail().getEmailListKey());
			if (StringUtils.isNotEmpty(params.getAutoEmail().getEmailSubject())) defaultReport.getReportAutoEmail().setEmailSubject(params.getAutoEmail().getEmailSubject());
			if (StringUtils.isNotEmpty(params.getAutoEmail().getContent())) {
				defaultReport.getReportAutoEmail().setContent(params.getAutoEmail().getContent());
				defaultReport.getReportAutoEmail().setEmailContentTemplateFile(null);
			}
			defaultReport.run();
		} else if (serviceBean instanceof DBCascadeDelete) {
			DBCascadeDelete dbCascadeDelete = (DBCascadeDelete) serviceBean;
			dbCascadeDelete.run();
		} else if (serviceBean instanceof CheckChangePassword) {
			CheckChangePassword checkChangePassword = (CheckChangePassword) serviceBean;
			checkChangePassword.run();
		} else if (serviceBean instanceof CleanUpDvdSummary) {
			CleanUpDvdSummary cleanUpDvdSummary = (CleanUpDvdSummary) serviceBean;
			cleanUpDvdSummary.run();
		} else if (serviceBean instanceof ActiveSTTWorker) {
			ActiveSTTWorker activeSTTWorker = (ActiveSTTWorker) serviceBean;
			String serviceKey = jobDetail.getServiceKey();
			if (serviceKey.equalsIgnoreCase("sendToTownOnly")) activeSTTWorker.sendToTownOnly();
			else activeSTTWorker.sendAndRefreshFromTown();		
		} else if (serviceBean instanceof InvSTTJob) {
			InvSTTJob invSTTJob = (InvSTTJob) serviceBean;
			invSTTJob.doSendToTown();
		} else if (serviceBean instanceof STTJob) {	
			STTJob sTTJob = (STTJob) serviceBean;
			sTTJob.doSendToTown();
		} else if (serviceBean instanceof LessonTicketReachEmailSchedule) {
			LessonTicketReachEmailSchedule lessonTicketReachEmailSchedule = (LessonTicketReachEmailSchedule) serviceBean;
			lessonTicketReachEmailSchedule.run();
		} else if (serviceBean instanceof ScheduledManagementReportMailService) {
			ScheduledManagementReportMailService scheduledManagementReportMailService = (ScheduledManagementReportMailService) serviceBean;
			if (StringUtils.isNotEmpty(params.getAutoEmail().getEmailListKey())) scheduledManagementReportMailService.setEmailDistributionListKey(params.getAutoEmail().getEmailListKey());
			scheduledManagementReportMailService.run();
		} else if (serviceBean instanceof ScheduledCustomMailService) {
			ScheduledCustomMailService scheduledCustomMailService = (ScheduledCustomMailService) serviceBean;
			scheduledCustomMailService.run();
		} else if (serviceBean instanceof IndexerWorker) {
			IndexerWorker indexerWorker = (IndexerWorker) serviceBean;
			indexerWorker.sendRecordToIndexingServer();
		} else if (serviceBean instanceof EDMSyncJob) {
			EDMSyncJob eDMSyncJob = (EDMSyncJob) serviceBean;
			String serviceKey = jobDetail.getServiceKey();
			if (serviceKey.equalsIgnoreCase("syncWithEdm")) eDMSyncJob.syncWithEdm();
			if (serviceKey.equalsIgnoreCase("syncEdmCatalog")) eDMSyncJob.syncEdmCatalog("GET");
			if (serviceKey.equalsIgnoreCase("refreshFromEDM")) eDMSyncJob.refreshFromEDM("GET");
		}else if (serviceBean instanceof EDMSyncOnQCStatusJob) {
			EDMSyncOnQCStatusJob QCSyncJob = (EDMSyncOnQCStatusJob) serviceBean;
			QCSyncJob.syncOnQCStatus();
		}else if (serviceBean instanceof IDSBridgeSyncJob) {
			IDSBridgeSyncJob idsBridgeSyncJob = (IDSBridgeSyncJob)serviceBean;
			idsBridgeSyncJob.syncWithIdsBridge();
		} else if (serviceBean instanceof ActivityCodeSchedulerJob) {
			ActivityCodeSchedulerJob activityCodeSchedulerJob = (ActivityCodeSchedulerJob) serviceBean;
			activityCodeSchedulerJob.run();
		} else if (serviceBean instanceof D2CustomWitsmlExportJob) {
			D2CustomWitsmlExportJob d2CustomWitsmlExportJob = (D2CustomWitsmlExportJob) serviceBean;
			d2CustomWitsmlExportJob.doExport();
		} else if (serviceBean instanceof NextBopTestReportExtractionService) {
			NextBopTestReportExtractionService nextBopTestReportExtractionService = (NextBopTestReportExtractionService) serviceBean;
			nextBopTestReportExtractionService.run();
		} else if (serviceBean instanceof DailyReportExtractionService) {
			DailyReportExtractionService dailyReportExtractionService = (DailyReportExtractionService) serviceBean;
			dailyReportExtractionService.run();
		} else if (serviceBean instanceof OMVHipWellExternalCatalogManager) {
			OMVHipWellExternalCatalogManager OMVHipWellExternalCatalog = (OMVHipWellExternalCatalogManager) serviceBean;
			OMVHipWellExternalCatalog.run();
			
		// WellView Indexer/Auto Importer/Sync Importer serviceBeans
		} else if (serviceBean instanceof WellViewIndexer) {
			WellViewIndexer wellViewIndexer = (WellViewIndexer) serviceBean;
			wellViewIndexer.run(jobDetail.getMiscConfig(), jobDetail.getServerPropertiesUid());
		} else if (serviceBean instanceof WellViewSyncImporter) {
			WellViewSyncImporter wellViewSyncImporter = (WellViewSyncImporter) serviceBean;
			wellViewSyncImporter.run(jobDetail.getServerPropertiesUid());
		} else if (serviceBean instanceof WellViewAutoImporter) {
			WellViewAutoImporter wellViewAutoImporter = (WellViewAutoImporter) serviceBean;
			wellViewAutoImporter.run(jobDetail.getMiscConfig(), jobDetail.getServerPropertiesUid());
		// Witsml Publisher initialization
		} else if (serviceBean instanceof WitsmlPublisherManager) {
			WitsmlPublisherManager witsmlPublisherManager = (WitsmlPublisherManager) serviceBean;
			witsmlPublisherManager.run(jobDetail.getServiceKey(),jobDetail.getServerPropertiesUid());
			
		} else if (serviceBean instanceof CombinedDDRReportModule) {
			CombinedDDRReportModule combinedDDRReportModule = (CombinedDDRReportModule) serviceBean;
			
			ManagementReportScheduleParams scheduleParam = new ManagementReportScheduleParams();
			scheduleParam.setReportUserUid("idsadmin");
			scheduleParam.setDistinctReport(true);

		    ReportAutoEmail reportAutoEmail = new ReportAutoEmail();
		    reportAutoEmail.setEmailSubject("Combine DDR");
		    reportAutoEmail.setEmailListKey("CombineDdrEmailList");
		    reportAutoEmail.setEmailContentTemplateFile("report/MgtReportEmailBody.ftl");
		    if (StringUtils.isNotEmpty(params.getAutoEmail().getEmailListKey())) reportAutoEmail.setEmailListKey(params.getAutoEmail().getEmailListKey());
			if (StringUtils.isNotEmpty(params.getAutoEmail().getEmailSubject())) reportAutoEmail.setEmailSubject(params.getAutoEmail().getEmailSubject());
			if (StringUtils.isNotEmpty(params.getAutoEmail().getContent())) {
				reportAutoEmail.setContent(params.getAutoEmail().getContent());
				reportAutoEmail.setEmailContentTemplateFile(null);
			}
		    
		    reportAutoEmail.setSendEmailAfterReportGeneration(true);
			combinedDDRReportModule.generateManagementReportOnCurrentSystemDatetime(scheduleParam, reportAutoEmail);
		} else if (serviceBean instanceof KDI24HrsSnapshot) {
			KDI24HrsSnapshot kdi24HrsSnapshotSchedulerJob = (KDI24HrsSnapshot) serviceBean;
			kdi24HrsSnapshotSchedulerJob.run();
		} else if (serviceBean instanceof DataImportExportHelper) {
			DataImportExportHelper dataImportExportHelper = (DataImportExportHelper) serviceBean;
			dataImportExportHelper.run(jobDetail.getServerPropertiesUid());
		}else if (serviceBean instanceof RigStateImport) {
			RigStateImport.getConfiguredInstance().run();
		}else if (serviceBean instanceof KDIOpsReport) {
			KDIOpsReport kdiOpsReport = (KDIOpsReport) serviceBean;
			kdiOpsReport.run();
		} else if (serviceBean instanceof FileBridgeJob) {
			//JobEngine.getConfiguredInstance().sendInBackground("FileBridgePool", new FileBridgeJob(), true);
			JobEngine.getConfiguredInstance().sendInBackground("FileBridgePool", new FileBridgeJob(jobDetail), true);
		} else if (serviceBean instanceof FileBridgeRESTJob) {
			JobEngine.getConfiguredInstance().sendInBackground("FileBridgePool", new FileBridgeRESTJob(jobDetail.getServerPropertiesUid()), true);
		} else if (serviceBean instanceof FileBridgeWebSocketJob) {
			JobEngine.getConfiguredInstance().sendInBackground("FileBridgePool", new FileBridgeWebSocketJob(jobDetail.getServerPropertiesUid(), jobDetail.getMiscConfig()), true);
		} else if (serviceBean instanceof RsdAutoStartNewDay) {
			RsdAutoStartNewDay.getConfiguredInstance().setGmtValue(params.getUserTimeZone());
			RsdAutoStartNewDay.getConfiguredInstance().run();
		}else if (serviceBean instanceof PubSubJob) {
			JobEngine.getConfiguredInstance().sendInBackground("PublishSubscribePool", new PubSubJob(), true);
		}
		else if (serviceBean instanceof WitsmlMessageQuery) {
			WitsmlMessageQuery witsmlMessageQuery = (WitsmlMessageQuery) serviceBean;
			witsmlMessageQuery.run(jobDetail.getServerPropertiesUid());
		} else if (serviceBean instanceof GoldboxManager) {
			String serviceKey = jobDetail.getServiceKey();
			if (StringUtils.equals("goldboxIndex", serviceKey)) {
				GoldboxManager.getConfiguredInstance().index();
			} else if (StringUtils.equals("goldboxPublish", serviceKey)) {
				GoldboxManager.getConfiguredInstance().publish();
			}
		} else if (serviceBean instanceof OMVHIPGDBJob) {
			OMVHIPGDBJob omvHIPGDB = (OMVHIPGDBJob) serviceBean;
			omvHIPGDB.run();
		}
	}
	
	protected ScheduledAutoReportParams getParams(SchedulerJobDetail jobDetail) throws Exception {
		return new ScheduledAutoReportParams(jobDetail);
	}
}