<html>
    <head><style>
      body {
        margin: 0px;
        padding: 0px;
      }
    </style></head>
    <body>
    	<script type='text/javascript' src="${visualizejsUtils.getJasperServerBaseUrl()}/client/visualize.js?baseUrl=${visualizejsUtils.getJasperServerBaseUrl()?url}"></script>
    	<script >
    		visualize(
    			{
    				auth: ${visualizejsUtils.getAuthentication()}
    			}, 
    			function (v) {
    				var report = v.report({
        				resource: "/public/IDS/Report2",
				        params: {
				            "companyName": ["ADC", "Baker Hughes INTEQ", "AET"]
				        },
				        container: "#container"
    				});
    			}
			);
    	</script>
    	<div id="container"></div>
    </body>
</html>