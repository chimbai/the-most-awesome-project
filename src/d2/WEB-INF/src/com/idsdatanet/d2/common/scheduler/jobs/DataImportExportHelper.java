package com.idsdatanet.d2.common.scheduler.jobs;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.ServletContextAware;

import com.idsdatanet.d2.core.cache.Operations;
import com.idsdatanet.d2.core.dao.DaoUserContext;
import com.idsdatanet.d2.core.model.ImportExportServerProperties;
import com.idsdatanet.d2.core.model.Operation;
import com.idsdatanet.d2.core.model.SysLogSendToTown;
import com.idsdatanet.d2.core.model.User;
import com.idsdatanet.d2.core.service.MailEngine;
import com.idsdatanet.d2.core.stt.STTDataDirection;
import com.idsdatanet.d2.core.stt.STTExportActionHandler;
import com.idsdatanet.d2.core.stt.STTExportParameter;
import com.idsdatanet.d2.core.stt.STTJobConfig;
import com.idsdatanet.d2.core.stt.STTStatus;
import com.idsdatanet.d2.core.stt.STTTransactionType;
import com.idsdatanet.d2.core.web.mvc.ActionHandlerResponse;
import com.idsdatanet.d2.core.web.mvc.ActionManager;
import com.idsdatanet.d2.core.web.mvc.ApplicationConfig;
import com.idsdatanet.d2.core.web.mvc.ApplicationUtils;
import com.idsdatanet.d2.core.web.mvc.UserSelectionSnapshot;
import com.idsdatanet.d2.core.web.mvc.UserSession;
import com.idsdatanet.d2.core.web.security.v2.LoginUserDetails;
import com.idsdatanet.d2.core.uom.mapping.UserUOMSelection;
import com.idsdatanet.depot.core.DepotConstants;
import com.idsdatanet.depot.core.query.QueryResult;
import com.idsdatanet.depot.core.util.DepotHttpClient;
import com.idsdatanet.depot.core.util.ServerPropertiesUtils;
import com.idsdatanet.depot.core.webservice.FakeHttpServletRequest;

public class DataImportExportHelper implements InitializingBean, ServletContextAware {
	protected Map<String, List<STTJobConfig>> jobConfigs;
	protected ServletContext servletContext = null;
	private MailEngine mailEngine = null;
	
	public String d2Username = null; 
	public String serviceName = null;
	public int hourOffset = 0;
	public String exportType = null; 
	public String emailSubjectWhenError = null; 
	public ActionManager actionManager;
	
	public void setMailEngine(MailEngine mailEngine) {
		this.mailEngine = mailEngine;
	}

	public MailEngine getMailEngine() {
		return mailEngine;
	}
	
	public void setActionManager(ActionManager actionManager) {
		this.actionManager = actionManager;
	}
	
	public String getD2Username(){
		return this.d2Username;
	}
	
	public void setD2Username(String value){
		this.d2Username = value;
	}
	
	public String getServiceName(){
		return this.serviceName;
	}
	
	public void setServiceName(String value){
		this.serviceName = value;
	}
	
	public int getHourOffset(){
		return this.hourOffset;
	}
	
	public void setHourOffset(int value){
		this.hourOffset = value;
	}
	
	public String getExportType(){
		return this.exportType;
	}
	
	public void setExportType(String value){
		this.exportType = value;
	}
	
	public String getEmailSubjectWhenError(){
		return this.emailSubjectWhenError;
	}
	
	public void setEmailSubjectWhenError(String value){
		this.emailSubjectWhenError = value;
	}
	
	public Map<String, List<STTJobConfig>> getJobConfigs() {
		return this.jobConfigs;
	}
	
	public void setJobConfigs(Map<String, List<STTJobConfig>> jobConfigs) {
		this.jobConfigs = jobConfigs;
	}
	
	public ServletContext getServletContext() {
		return this.servletContext;
	}
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	public void run(String serverPropertiesUid) throws Exception{
		if(StringUtils.isBlank(serverPropertiesUid)) throw new Exception("Server Properties NOT Set!!");
		//login
        List<User> users = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam("from User where userName = :username AND (isblocked=false or isblocked is null) AND (isDeleted = false OR isDeleted is null)", "username", d2Username);
        if(users == null || users.size() == 0) System.out.println("No user found");
        
        try{
			if (users != null && users.size() > 0) {
	        	User user = users.get(0);
	        	user.setLastLoginFormUrl("login.jsp");
	        	UserSelectionSnapshot userSelection = UserSelectionSnapshot.getInstanceForCustomLogin(d2Username); //populate on username verified
	        	Operations ops = new Operations();
				
	        	// simulate login for user into D2
	        	LoginUserDetails loginUserDetails = new LoginUserDetails(user);
	    		SecurityContextHolder.getContext().setAuthentication(
	    				new UsernamePasswordAuthenticationToken(loginUserDetails, loginUserDetails.getPassword())
	    				);
	    		DaoUserContext.getThreadLocalInstance().setUserSelection(userSelection);
	    		
	    		UserSession session = UserSession.getInstance(new FakeHttpServletRequest());
	    		Date reportDate = new Date(System.currentTimeMillis()- (hourOffset * (60*60*1000))); //reportdate=current day - 1 day
				ops = session.getCachedAllAccessibleOperations(); 
	    		String queryString = "SELECT DISTINCT(operationUid) FROM ReportDaily WHERE reportDatetime = date(:reportDate) AND (isDeleted = false OR isDeleted is null)";
	    		List<String> results = ApplicationUtils.getConfiguredInstance().getDaoManager().findByNamedParam(queryString, "reportDate", DateFormatUtils.format(reportDate, "yyyy-MM-dd"));
	
	    		if(results.size() <= 0) System.out.println("No operation record found");
	    		if(results.size()>0){
	    			System.out.println(new Date() + " Begin operation export");
	    			for(String operationUid : results){
	    				if(ops.containsKey(operationUid)){
	    					SysLogSendToTown sysLog = new SysLogSendToTown();
	    					ImportExportServerProperties importExportServerProperties = (ImportExportServerProperties)ApplicationUtils.getConfiguredInstance().getDaoManager().getObject(ImportExportServerProperties.class, serverPropertiesUid);
	    					STTStatus status = new STTStatus();
	    					String oldUomTemplateUid = UserUOMSelection.getThreadLocalInstance().getUOMTemplateUid();
	    					String targetUomTemplateUid = null;
	    					String fileOutputPath = servletContext.getRealPath("/") + "WEB-INF/importexport/";
	    					File f = null;
	    					try {
	    						Operation operation = ApplicationUtils.getConfiguredInstance().getCachedOperation(operationUid);
	    						session._setCurrentOperationUid(operation.getOperationUid());
	    						
		    					if(!StringUtils.isBlank(operation.getUomTemplateUid())){
		    						targetUomTemplateUid = operation.getUomTemplateUid();
		    					}
	    						session.setCurrentUOMTemplateUid(targetUomTemplateUid);
	    						UserUOMSelection.getThreadLocalInstance().setUOMTemplateUid(targetUomTemplateUid);
		    					STTExportActionHandler sTTExportActionHandler = new STTExportActionHandler(jobConfigs, servletContext);
		    					STTExportParameter param = new STTExportParameter();
		    					param.setExportType(this.exportType);
		    					param.setOutputFilePath(fileOutputPath);
		    					sTTExportActionHandler.setCustomParameters(param);
		    					ActionHandlerResponse response = sTTExportActionHandler.process(null, session, null, null, null, null);
		    					if (response.isSuccess()) {
		    						f = new File(param.getOutputFilePath());
		    						if(f != null && f.isFile() && f.exists()){
		    							sysLog.setFilename(f.getName());
		    							status.log(f.getName() + " is exported.");
		    							exportToWebservice(importExportServerProperties, f, user, status);
		    						}
		    					} else {
		    						String errorText = "File export for operation: " + operation.getOperationName() + " failed.";
		    						status.addError(errorText);
		    						status.log(errorText);
		    					}
	    						
	    					} catch(Exception e) {
	    						String error = "An exception has occurred when processing operation " + operationUid + ". Error Stack: " + e.toString();
	    						status.addError(error);
	    						status.log(error);
	    						e.printStackTrace();
	    					} finally {
	    						if(status.hasError()) sysLog.setStatus(STTStatus.FAIL);
	    						else sysLog.setStatus(STTStatus.OK);
	    						
	    						sysLog.setGroupUid(userSelection.getGroupUid());
	    						sysLog.setWellUid(userSelection.getWellUid());
	    						sysLog.setWellboreUid(userSelection.getWellboreUid());
	    						sysLog.setOperationUid(operationUid);
	    						sysLog.setRigInformationUid(userSelection.getRigInformationUid());
	    						sysLog.setDailyUid(userSelection.getDailyUid());
	    						sysLog.setJobconfigs(STTJobConfig.jobConfigsToString(jobConfigs.get(exportType)));
	    						sysLog.setTransactionDatetime(new Date());
	    						sysLog.setTransactionType(STTTransactionType.SEND_TO_IQX);
	    						sysLog.setDataDirection(STTDataDirection.OUTGOING);
	    						sysLog.setIsScheduled(true);
	    						sysLog.setRemoteAddr(importExportServerProperties.getEndPoint());
	    						sysLog.setUserName(d2Username);
	    						sysLog.setTransactionLog(status.getTransactionLog().toString());
	    						ApplicationUtils.getConfiguredInstance().getDaoManager().saveObject(sysLog);
	    						
	    						status.dispose();
	    						
	    						UserUOMSelection.getThreadLocalInstance().setUOMTemplateUid(oldUomTemplateUid);
	    					}
	    					
	    					if(f != null) {
	    						f.delete();
	    						f.getParentFile().delete();
	    					}
	    						
	    				}
	    			}
	    			
	    		}
	        }
        }catch(Exception e){
        	e.printStackTrace();
        }
        System.out.println(new Date() + " Done");
		//login end
	}
	
	public void exportToWebservice(ImportExportServerProperties importExportServerProperties, File fileToExport, User user, STTStatus status) {
		DepotHttpClient client = null;
		System.out.println(new Date() + " Sending to remote server");
		try{
			//send basic client authentication to endpoint
			QueryResult queryResult = new QueryResult();
			//ImportExportServerProperties importExportServerProperties = ServerPropertiesUtils.getConfiguredInstance().getServerPropertiesByServiceName(DepotConstants.IQX, serviceName);
			MultipartEntity entity = new MultipartEntity();
			ContentBody cbFile = new FileBody(fileToExport, "application/gzip");
			entity.addPart("exportFile", cbFile);
			client = new DepotHttpClient(importExportServerProperties.getEndPoint());
			client.setBasicAuthentication(importExportServerProperties.getUsername(), importExportServerProperties.getPassword());
			client.invokePostWithEntity(entity, queryResult);
			
			//if successful;
			if(queryResult.getResultCode() == 1){
				status.log(fileToExport.getName() + " sent successfully. File size: " + fileToExport.length() + " B");
			}
			else{
				//if failed
				String errorText = "Fail to send: " + fileToExport.getName() + " :: Error Code: " + queryResult.getResultCode() + ", Error: " + queryResult.getErrorMessages();
				status.addError(errorText);
				status.log(errorText);
				//send to user.email
				mailEngine.sendMail(new String[] {user.getEmail()}, ApplicationConfig.getConfiguredInstance().getSupportEmail(), ApplicationConfig.getConfiguredInstance().getSupportEmail(), 
						this.emailSubjectWhenError, " " + DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd HH:mm") + ": " + errorText);
			}	
		}catch(Exception e){
			String error = "An exception has occurred when transmitting to iQx server. Error Stack: " + e.toString();
			status.addError(error);
			status.log(error);
			e.printStackTrace();
		} finally {
			if (client != null) {
				client.dispose();
			}
		}
		
	}

	protected String sanitize(String str) {
		if (str != null) {
			return str.trim().replace('<', '_').replace('>', '_').replace(':', '_').replace('"', '_').replace('/', '_').replace('\\', '_').replace('|', '_').replace('?', '_').replace('*', '_');
		}
		return null;
	}
	
	public void afterPropertiesSet() throws Exception {
		
	}
	
}