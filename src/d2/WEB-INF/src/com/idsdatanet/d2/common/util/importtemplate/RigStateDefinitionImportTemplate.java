package com.idsdatanet.d2.common.util.importtemplate;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.idsdatanet.d2.common.util.ExcelImportTemplate;
import com.idsdatanet.d2.common.util.ExcelMapping;
import com.idsdatanet.d2.common.util.ImportUtil;
import com.idsdatanet.d2.common.util.node.GenericNodeListener;
import com.idsdatanet.d2.core.uom.CustomFieldUom;
import com.idsdatanet.d2.core.web.mvc.BaseCommandBean;

public class RigStateDefinitionImportTemplate extends ExcelImportTemplate {

	@Override
    public List<Object> parseTemplate(File excelFile, BaseCommandBean commandBean) throws Exception {
        List<Object> objectList = null;
        InputStream inputStream = new FileInputStream(excelFile);
        this.book = WorkbookFactory.create(inputStream);
        this.evaluator = book.getCreationHelper().createFormulaEvaluator();
        Sheet sheet = book.getSheetAt(0);

        if (sheet != null) {
            objectList = new ArrayList<>();
            Object obj = this.parseRigStateDefinition(sheet, this.headerRow, commandBean, objectList);
            if(obj != null) objectList.add(obj);
        }
        book.close();
        inputStream.close();
        return objectList;
    }

    public Object parseRigStateDefinition(Sheet sheet, Integer headerRow, BaseCommandBean commandBean, List<Object> objectList) throws Exception {
        Row row = sheet.getRow(headerRow);
        if (row == null) return null;
        Map<Integer, ExcelMapping> columnMap = this.parseHeader(row, commandBean);
        if (columnMap == null || columnMap.size() == 0) {
            return null;
        } else {
        	row = sheet.getRow(headerRow + 1);
        	return this.parseRow(row, this.className, columnMap, commandBean, objectList);
        }
    }
}