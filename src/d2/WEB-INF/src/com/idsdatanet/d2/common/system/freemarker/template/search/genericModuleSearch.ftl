<!DOCTYPE HTML>
<html>
<body style="overflow-x: hidden; overflow-y: hidden;background-color:#ffffff;">
<#assign v_resources_path = "${form.getVirtualResourcePath()}/htmllib"/>
<#include "/core/defaultJavascriptAndStyleTemplate.ftl" />
<#include "/templateHelper.ftl"/>
<script type="text/javascript" src="${v_resources_path}/search/genericSearch.js"></script>

<style>
	.iTextBox {
	    width: 20%;
	    height: 30px;
	    display: inline;	   
	}

	input[type=button] {
	    width: 120px;
	    margin-left: 35px;
	    display: inline;
	}
	
	.pageNoWithLink{
		cursor:pointer;
		color:darkblue;
		
	}
	
	.pageNoWithoutLink{
		color:blue;
		font-weight:bold;
	}
	
	.highlighter{
		color:orange;
		font-weight:bold;
		
	}
	
	.outputDiv{
		height: 95vh;
		overflow:auto;
	}
</style>
<script type="text/javascript">	
	$(document).ready(function() {			
		<#if form.loadParam??>
			searchObject.renderTo($("#output")) ;
			searchObject.render(false);
			searchObject.searchByParam("${form.getQueryParam()}");	
		<#else>
			searchObject.initData(${form.getSearchParamDetails()});
			searchObject.renderTo($("#output")) ;
			searchObject.render(true);	
		</#if>
	});
</script>
<table class="BaseNodesGroupContainerHeader" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td><div class="BaseNodesGroupContainerHeaderText">${getFormTitle("")}</div></td>
		</tr>
	</tbody>
</table>
<table width="100%">
	<tbody>
	<tr>
		<td><div id="output" class="outputDiv"/></td>
	</tr>
	</tbody>
</table>	
</body>
</html> 
    
    
    



