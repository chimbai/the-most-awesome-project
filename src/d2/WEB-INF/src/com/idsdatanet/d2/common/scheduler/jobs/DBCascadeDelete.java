package com.idsdatanet.d2.common.scheduler.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.List;

import com.idsdatanet.d2.core.util.WellNameUtil;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.idsdatanet.d2.core.web.mvc.SystemExceptionHandler;

public class DBCascadeDelete {

	private Log log = LogFactory.getLog(this.getClass());
	private List<String> dailyTableList = null;
	private DataSource dataSource = null;
	
	public void setDailyTableList(List<String> dailyTableList){
		this.dailyTableList = dailyTableList;
	}	
	
	public List<String> getDailyTableList() {
		return this.dailyTableList;
	}
	
	public void setDataSource(DataSource dataSource){
		this.dataSource = dataSource;
	}	
	
	synchronized public void run() throws Exception
	{
		String sql = null;
		Connection connection = null; 
		Statement statement = null;
			
		try {
			if (this.dataSource == null){			
				throw new Exception("Database connection no found");					
			}
		
			connection = this.dataSource.getConnection();		
		
			//String dbProductName = connection.getMetaData().getDatabaseProductName();
			
			statement = connection.createStatement();
			
			//marked wellbore record  with missing parent record as deleted
			statement.executeUpdate("update wellbore wb set wb.is_deleted=1,wb.sys_deleted=3 where (wb.is_deleted is null or wb.is_deleted=0) and (wb.well_uid not in (select well_uid from well w where w.is_deleted=0 or w.is_deleted is null))");
						
			//marked operation record with missing parent record as deleted
			sql = "update operation op set op.is_deleted=1,op.sys_deleted=3 where ( " +	
				  "(op.well_uid not in (select w.well_uid from well w where w.is_deleted=0 or w.is_deleted is null)) or " +
				  "(op.wellbore_uid not in (select wb.wellbore_uid from wellbore wb where wb.is_deleted=0 or wb.is_deleted is null)) " + 
			      ") and (op.is_deleted is null or op.is_deleted=0) and (op.is_campaign_overview_operation is null OR op.is_campaign_overview_operation=0)";
			statement.executeUpdate(sql);
			
			sql = "update operation op set op.is_deleted=1,op.sys_deleted=3 where (op.well_uid is null and op.wellbore_uid IS NULL AND op.site_uid IS NULL)";
			statement.executeUpdate(sql);			
			
			//marked daily record with missing parent record as deleted
			sql = "update daily d set d.is_deleted=1,d.sys_deleted=3 where ((d.well_uid not in (select w.well_uid from well w where w.is_deleted=0 or w.is_deleted is null)) or " +
				  "(d.wellbore_uid not in (select wb.wellbore_uid from wellbore wb where wb.is_deleted=0 or wb.is_deleted is null)) or " +
				  "(d.operation_uid not in (select op.operation_uid from operation op where op.is_deleted=0 or op.is_deleted is null))" +
				  ") and (d.is_deleted is null or d.is_deleted=0)";
			statement.executeUpdate(sql);	
					
			if ((dailyTableList != null) && (dailyTableList.size()>0)){
				
				String appendSql = " a set a.is_deleted=1,a.sys_deleted=3 where (a.is_deleted is null or a.is_deleted=0) AND (a.daily_uid not in (select d.daily_uid from daily d where (d.is_deleted=0 or d.is_deleted is null)));";
							
				for (String tableName : dailyTableList) {					
					sql = "update "  + tableName.concat(appendSql);
					statement.executeUpdate(sql);
				}
			}
			
			//auto populate padded wellName, wellboreName, operationName, rigName			
			sql = "SELECT well_uid, well_name FROM well";
			ResultSet resultRSW = statement.executeQuery(sql);
			String paddedName = "";
			
			if (resultRSW != null)
			{
				PreparedStatement pstmt = connection.prepareStatement("UPDATE well SET well_name_padded = ? WHERE well_uid = ?");
				
				while(resultRSW.next()){					
					paddedName = WellNameUtil.getPaddedStr(resultRSW.getString("well_name"));		
					pstmt.setString(1, paddedName.toLowerCase());
			       	pstmt.setString(2, resultRSW.getString("well_uid"));
			       	pstmt.executeUpdate();
				}	
				pstmt.close();
			}			
			
			
			sql = "SELECT wellbore_uid, wellbore_name FROM wellbore";
			ResultSet resultRSWB = statement.executeQuery(sql);
			
			if (resultRSWB != null)
			{
				PreparedStatement pstmt = connection.prepareStatement("UPDATE wellbore SET wellbore_name_padded = ? WHERE wellbore_uid = ?");
				
				while(resultRSWB.next()){					
					paddedName = WellNameUtil.getPaddedStr(resultRSWB.getString("wellbore_name"));		
					pstmt.setString(1, paddedName.toLowerCase());
			       	pstmt.setString(2, resultRSWB.getString("wellbore_uid"));
			       	pstmt.executeUpdate();
				}	
				pstmt.close();
			}			
			
			
			sql = "SELECT operation_uid, operation_name FROM operation";
			ResultSet resultRSOP = statement.executeQuery(sql);
			
			if (resultRSOP != null)
			{
				PreparedStatement pstmt = connection.prepareStatement("UPDATE operation SET operation_name_padded = ? WHERE operation_uid = ?");
				
				while(resultRSOP.next()){					
					paddedName = WellNameUtil.getPaddedStr(resultRSOP.getString("operation_name"));		
					pstmt.setString(1, paddedName.toLowerCase());
			       	pstmt.setString(2, resultRSOP.getString("operation_uid"));
			       	pstmt.executeUpdate();
				}	
				pstmt.close();
			}			
			
			
			sql = "SELECT rig_information_uid, rig_name FROM rig_information";
			ResultSet resultRSRig = statement.executeQuery(sql);
			
			if (resultRSRig != null)
			{
				PreparedStatement pstmt = connection.prepareStatement("UPDATE rig_information SET rig_name_padded = ? WHERE rig_information_uid = ?");
				
				while(resultRSRig.next()){					
					paddedName = WellNameUtil.getPaddedStr(resultRSRig.getString("rig_name"));		
					pstmt.setString(1, paddedName.toLowerCase());
			       	pstmt.setString(2, resultRSRig.getString("rig_information_uid"));
			       	pstmt.executeUpdate();
				}	
				pstmt.close();
			}
			
			resultRSW.close();
			resultRSWB.close();
			resultRSOP.close();
			resultRSRig.close();
			
			
		}catch (Exception e) {
			log.error(SystemExceptionHandler.printAsString(e));			
		}finally{
			if (connection !=null) connection.close();
			if (statement !=null) statement.close();							
		}	
	}
}
