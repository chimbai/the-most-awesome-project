<@showFormTitleInContentPage classType="PerforationInterval" />

<table class="inner-table">
	<#if form.root.list.PerforationInterval?exists>
		<#list form.root.list.PerforationInterval?values as item>
			<tr <@template item/>>
				<td class="record-actions">
					<@showRecordActions node=item />
				</td>
				<td>
					<table class="subtable">
						<tr>
							<td width="1%">
								<table class="no-border">
									<tr>
										<th class="label" width="50%">Date Open</th>									
										<td class="value" width="50%"><@showField node=item field="dateOpen" format="dd MMM yyyy"/></td>
									</tr>
									<tr>
										<th class="label" width="50%">Date Closed</th>									
										<td class="value" width="50%"><@showField node=item field="dateClose" format="dd MMM yyyy"/></td>
									</tr>
								</table>
							</td>
							<td width="1%">
								<table class="no-border">
									<tr>
										<th class="label" width="50%">Depth From</th>									
										<td class="value" width="50%"><@showField node=item field="topMdMsl"/></td>
									</tr>
									<tr>
										<th class="label" width="50%">Bottom TVD</th>									
										<td class="value" width="50%"><@showField node=item field="bottomTvdMsl"/></td>
									</tr>
								</table>
							</td>
							<td width="1%">
								<table class="no-border">
									<tr>
										<th class="label" width="50%">Depth To</th>									
										<td class="value" width="50%"><@showField node=item field="bottomMdMsl"/></td>
									</tr>
									<tr>
										<th class="label" width="50%">Top TVD</th>									
										<td class="value" width="50%"><@showField node=item field="topTvdMsl"/></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<table class="no-border">												
									<th class="label" valign="top" width="25%">Description</th>
									<td width="65%">
										<@showField node=item field="comments" fieldtype="textarea" options="cols='100' rows='10'"/>
									</td>
								</table>
							</td>
						</tr>
					</table>									
				</td>												
			</tr>
		</#list>
		<tr><td colspan="2" class="child-title"><div class="buttons bottom-button align-left"><@showButton node=form.root class="PerforationInterval" isAdd=true /></div></td></tr>
	</#if>
</table>

<#macro showSelectionForImportExport>
	<div id="objConfigDiv">
	</div>
</#macro>
